class CResourceManager extends egret.DisplayObjectContainer{
    
      public static s_containerRecycledConstructingAni:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

    public static NewConstructingAni():CConstructingAnimation
    {
        var ani:CConstructingAnimation = null;

        if ( CResourceManager.s_containerRecycledConstructingAni.numChildren > 0 )
        {
            ani = CResourceManager.s_containerRecycledConstructingAni.getChildAt(0) as CConstructingAnimation;
            CResourceManager.s_containerRecycledConstructingAni.removeChildAt(0);
            ani.visible = true;
        }
        else
        {
            ani = new CConstructingAnimation();
        }

        return ani;
    }

    public static DeleteConstructingAni( ani:CConstructingAnimation ):void
    {
        ani.visible = false;
        CResourceManager.s_containerRecycledConstructingAni.addChild( ani );
    }


    public static FixedUpdate():void
    {


    }

    public static Init():void
    {
       CResourceManager.s_bmpSegTop = new egret.Bitmap( RES.getRes( "Hurdle_Top_png" ) );
       CResourceManager.s_bmpSeg = new egret.Bitmap( RES.getRes( "Hurdle_Seg_png" ) );
    }

   public static s_bmpSegTop:egret.Bitmap = null;
   public static s_bmpSeg:egret.Bitmap = null; 

   protected static s_containerRecycledProgressBar:eui.Component = new eui.Component();
   public static NewProgressBar():CUIBaseProgressBar
   {
       var progressBar:CUIBaseProgressBar = null;

       if ( CResourceManager.s_containerRecycledProgressBar.numChildren > 0 )
       {
           progressBar = CResourceManager.s_containerRecycledProgressBar.getChildAt(0) as CUIBaseProgressBar;
           CResourceManager.s_containerRecycledProgressBar.removeChildAt(0);
       }
       else
       {
           progressBar = new CUIBaseProgressBar( "resource/assets/MyExml/CProgressBar.exml" );
       }

       return progressBar;
   }

   public static DeleteProgressBar(progressBar:CUIBaseProgressBar):void
   {
        CResourceManager.s_containerRecycledProgressBar.addChild(progressBar);
   }

   static s_containerRecycledDiBiao:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
   public static NewDiBiao():CObj
   {
       var dibiao:CObj = null;
       if ( CResourceManager.s_containerRecycledDiBiao.numChildren > 0 )
       {
           dibiao = CResourceManager.s_containerRecycledDiBiao.getChildAt(0) as CObj;
           CResourceManager.s_containerRecycledDiBiao.removeChildAt(0);
       }
       else
       {
           dibiao = new CObj();
       }

       return dibiao;
   }

   public static DeleteDiBiao( dibiao:CObj ):void
   {
       CResourceManager.s_containerRecycledDiBiao.addChild( dibiao );
   }

/////////////////////////
    protected static s_lstRecycledObjs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static  NewObj():CObj
    {
        var obj:CObj = null;
        if ( CResourceManager.s_lstRecycledObjs.numChildren > 0 )
        {
            obj = CResourceManager.s_lstRecycledObjs.getChildAt(0) as CObj;
            CResourceManager.s_lstRecycledObjs.removeChildAt(0);
            obj.Reset();
        }
        else
        {
            obj = new CObj();
        }

        return obj;
    }

    public static DeleteObj( obj:CObj ):void
    {
        CResourceManager.s_lstRecycledObjs.addChild( obj );
    }

    public static GetDiBiaoTextureByTownId( nTownId:number ):egret.Texture
    {
        var szResName:string = "dibiao_" + nTownId + "_png";
        console.log( "szResName=" + szResName );
       return RES.getRes( szResName );

        
    }

    public static GetTreeTextureByTownId(nTownId:number):egret.Texture
    {
        return RES.getRes( "tree_" + nTownId + "_png" );
    }
    
    public static GetMoutainTextureByTownId(nTownId:number):egret.Texture
    {
        return RES.getRes( "moutain_" + nTownId + "_png" );
    }

    public static GetBubbleTextureByTownId(nTownId:number ):egret.Texture
    {
        return RES.getRes( "bubble_" + nTownId + "_png" );
    }

    public static GetCloudTextureByTownId(nTownId:number, nCloudSubId:number = 0 ):egret.Texture
    {
         if ( nCloudSubId == 0 )
         {   
            return RES.getRes( "cloud_" + nTownId + "_png" );

          
         }
         else
         {
             return RES.getRes( "cloud_" + nTownId + "_" +  nCloudSubId +"_png" );
         }
    }

    public static GetNumberTexture( nTownId:number ):egret.Texture
    {
        return RES.getRes( nTownId + "_png" );
    }



} // end class