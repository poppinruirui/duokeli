class CUIWindTurbine extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();
     protected m_btnClose:CCosmosImage;

     protected m_labelLevel:eui.Label;
     protected m_labelEarningPerBuilding:eui.Label;
     protected m_labelTotalEarning:eui.Label;

     

    public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CWindTurbine.exml";
        this.addChild( this.m_uiContainer );

        this.m_labelEarningPerBuilding = this.m_uiContainer.getChildByName( "labelEarningPerBuilding" ) as eui.Label; 
        this.m_labelTotalEarning = this.m_uiContainer.getChildByName( "labelTotalEarning" ) as eui.Label; 

        var img1:eui.Image = this.m_uiContainer.getChildByName( "img1" ) as eui.Image; 
        img1.filters = [ CColorFucker.GetColorMatrixFilterByRGBA( 0.867, 0.867, 0.867, 1 ) ];

        var img2:eui.Image = this.m_uiContainer.getChildByName( "img2" ) as eui.Image; 
        img2.filters = [ CColorFucker.GetColorMatrixFilterByRGBA( 0.2, 0.2, 0.2, 1 ) ];

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
        this.m_btnClose = new CCosmosImage();
        this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
        this.m_uiContainer.addChild( this.m_btnClose );
        this.m_btnClose.x = imgTemp.x;
        this.m_btnClose.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnClose.UseColorSolution(0);
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

    }

    public SetParams( nEarningPerBuilding:number, nTotalEarning:number ):void
    {
        this.m_labelEarningPerBuilding.text = nEarningPerBuilding + " / 秒";
        this.m_labelTotalEarning.text = nTotalEarning + " / 秒";
    }


     private onButtonClick_Close( evt:egret.TouchEvent ):void
    {
          CUIManager.SetUiVisible( Global.eUiId.wind_turbine, false );
    }

} // end class