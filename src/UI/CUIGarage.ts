/*
     “车库”类
*/
class CUIGarage extends egret.DisplayObjectContainer {

    protected m_txtActiveCarTitle:eui.Label;
    protected m_txtEarnigTitle:eui.Label;
    protected m_txtEarnigValue:eui.Label;

    protected m_imgCoin:eui.Image;

    protected m_bmpBG:eui.Image;
    protected m_bmpFuckScroller:egret.Bitmap;

    protected m_groupForCarsList:eui.Group = new eui.Group();
    protected m_scrollForCarsList:eui.Scroller = new eui.Scroller();

    protected m_aryItems:Array<CUIGarageItem> = Array<CUIGarageItem>();

   protected m_btnClose:CUIBaseCommonButton;;

    public constructor() {
        super();

      
        

        this.m_bmpBG = new eui.Image( RES.getRes( "1X1_png" ) );
        this.addChild( this.m_bmpBG );
        this.m_bmpBG.width =600;
        this.m_bmpBG.height = 900;
       
        this.m_txtActiveCarTitle = new eui.Label();
        this.addChild( this.m_txtActiveCarTitle );
        this.m_txtActiveCarTitle.width = this.m_bmpBG.width;
        this.m_txtActiveCarTitle.textAlign = egret.HorizontalAlign.CENTER;
        this.m_txtActiveCarTitle.text = "活动的汽车数：10";
        this.m_txtActiveCarTitle.textColor = 0x000000;
        this.m_txtActiveCarTitle.y = 10;
        this.m_txtActiveCarTitle.bold = true;

        this.m_txtEarnigTitle = new eui.Label();
        this.addChild( this.m_txtEarnigTitle );
        this.m_txtEarnigTitle.textAlign = egret.HorizontalAlign.LEFT;
        this.m_txtEarnigTitle.text = "产出金币";
        this.m_txtEarnigTitle.textColor = 0x777777;
        this.m_txtEarnigTitle.x = 200;
        this.m_txtEarnigTitle.y = 50;
        this.m_txtEarnigTitle.size = 24;

        this.m_txtEarnigValue = new eui.Label();
        this.addChild( this.m_txtEarnigValue );
        this.m_txtEarnigValue.textAlign = egret.HorizontalAlign.LEFT;
        this.m_txtEarnigValue.text = "75/秒";
        this.m_txtEarnigValue.textColor = 0x777777;
        this.m_txtEarnigValue.x = 340;
        this.m_txtEarnigValue.y = 50;
        this.m_txtEarnigValue.size = 24;
        

        this.m_imgCoin = new eui.Image( CJinBiManager.GetFrameTexture(0) );
        this.addChild(this.m_imgCoin);
        this.m_imgCoin.scaleX = 0.4;
        this.m_imgCoin.scaleY = 0.4;
        this.m_imgCoin.x = 305;
        this.m_imgCoin.y = 40;

         var nCarNum:number = CConfigManager.s_nTotalCarNum;

      

        var contentContainer:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

        for ( var i:number = 0; i < nCarNum; i ++ )
        {
            var item:CUIGarageItem = new CUIGarageItem(); 
          //  this.m_groupForCarsList.addChild( item );
            contentContainer.addChild( item );
            item.y = i * ( CDef.s_fGarageItemHeight + 5 ) ;

            item.SetIndex( i );
            
           // item.SetLock( true );
       //    item.SetStatus();
            this.m_aryItems.push( item );

        } // end for i

       // poppin test
        var sv:egret.ScrollView = new egret.ScrollView();
        this.addChild( sv );
        sv.setContent( contentContainer );
        sv.x = 25;
        sv.y = 100;
        sv.width = 550;  
        sv.height = 7 * CDef.s_fGarageItemHeight;       
        sv.verticalScrollPolicy = "on";
        sv.horizontalScrollPolicy = "off";

        this.m_btnClose = new CUIBaseCommonButton();
        this.addChild( this.m_btnClose );
        this.m_btnClose.x = 100;
        this.m_btnClose.y = 910;
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

    } // end constructor

    private onButtonClick_Close( evt:egret.TouchEvent ):void
    {
          CUIManager.SetUiVisible( Global.eUiId.garage, false );
    }

    public MainLoop():void
    {
       
    }

    public UpdateStatus( nIndex:number, eStatus:Global.eGarageItemStatus ):void
    {
        /*
        for ( var i:number = 0; i < this.m_aryItems.length; i++ )
        {
            var item:CUIGarageItem = this.m_aryItems[i];
            item.SetStatus( Main.s_CurTown.GetGarageItemStatus( i ) );
        } // end i
        */
        var item:CUIGarageItem = this.m_aryItems[nIndex];
        item.SetStatus( Main.s_CurTown.GetGarageItemStatus( nIndex ) );
    }

    public SetMaskVisible( nIndex:number, bVisible:boolean ):void
    {
        if ( nIndex >= this.m_aryItems.length )
        {
            return;
        }
        var item:CUIGarageItem = this.m_aryItems[nIndex];   
        item.SetMaskVisible( bVisible );

    }



} // end class