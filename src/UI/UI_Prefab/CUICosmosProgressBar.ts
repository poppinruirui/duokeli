class CUICosmosProgressBar extends eui.Component {

    protected m_barInner:CUICosmosBar;
    protected m_barInnerBG:CUICosmosBar;
    protected m_barOut:CUICosmosBar;
    protected m_Mask:egret.Shape;

    protected m_nWidth:number = 0;
    protected m_nHeight:number = 0;
    protected m_nThickness:number = 0;

    protected m_nInnerColor:number = 0;
    protected m_nInnerBGColor:number = 0;
    protected m_nOutColor:number = 0;

    protected m_fPercent:number = 1;

    protected m_fAlpha:number = 1;

       public constructor() {
        super();

        this.m_barInner = new CUICosmosBar();
        this.m_barOut = new CUICosmosBar();
        this.m_barInnerBG = new CUICosmosBar();
        this.m_Mask = new egret.Shape();
        this.addChild( this.m_barOut );
        this.addChild( this.m_barInnerBG );
        this.addChild( this.m_barInner );
        this.addChild( this.m_Mask );

       } // constructor

       public SetParams( width:number, height:number, thickness:number,inner_color:number, out_color:number, inner_bg_color:number, alpha:number = 1 ):void
       {
           this.m_nWidth = width;
           this.m_nHeight = height;
           this.m_nThickness = thickness;
           this.m_nInnerColor = inner_color;
           this.m_nOutColor = out_color;
           this.m_nInnerBGColor = inner_bg_color;

        this.m_fAlpha = alpha;

           this.ReDraw();
       }

       public ReDraw():void
       {
           this.m_barInner.SetParams( this.m_nWidth, this.m_nHeight, this.m_nInnerColor, this.m_fAlpha );
           this.m_barInnerBG.SetParams( this.m_nWidth, this.m_nHeight, this.m_nInnerBGColor, this.m_fAlpha );
       
           this.m_barOut.SetParams( this.m_nWidth + 2 * this.m_nThickness, this.m_nHeight + 2 * this.m_nThickness, this.m_nOutColor , this.m_fAlpha);
           this.m_barOut.x = -this.m_nThickness;
           this.m_barOut.y = this.m_nHeight * 0.5;

           this.m_Mask.graphics.clear();
           this.m_Mask.graphics.beginFill( 0xFFFFFF, 1);
           this.m_Mask.graphics.drawRect(0, -0.5 * this.m_nHeight, this.m_nWidth * this.m_fPercent, this.m_nHeight);
           this.m_Mask.graphics.endFill();
           this.m_Mask.y = this.m_nHeight * 0.5;
  
         

  

           this.m_barInner.mask = this.m_Mask;
           this.m_barInner.y = this.m_nHeight * 0.5;

            this.m_barInnerBG.x = this.m_barInner.x;
           this.m_barInnerBG.y = this.m_barInner.y;
      }

       public SetPercent( fPercent:number ):void
       {
           this.m_fPercent = fPercent;

           this.ReDraw();
       }

} // end class 

