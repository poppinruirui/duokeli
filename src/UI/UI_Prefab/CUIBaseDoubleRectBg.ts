

class CUIBaseDoubleRectBg extends egret.DisplayObjectContainer {

     protected m_nWidth:number = 0;
     protected m_nHeight:number = 0;

     protected m_aryParts:Array<eui.Image> = new Array<eui.Image>();
     protected m_aryPartsWidthPercent:Array<number> = new Array<number>();
     protected m_aryPartsHeightPercent:Array<number> = new Array<number>();

     protected m_eType:Global.eDoubleRectBgType = Global.eDoubleRectBgType.up_down;

     public constructor() {
        super();

        for ( var i:number = 0; i < 2; i++ )
        {
            this.m_aryParts[i] = new eui.Image( RES.getRes( "1X1_png" ) );
            this.addChild( this.m_aryParts[i] );
        }
       
     }

     public SetType( eType:Global.eDoubleRectBgType ):void
     {
            this.m_eType = eType;
     
     }

     public SetPartSizePercent( nPartIndex:number, fWidthPercent:number, fHeightPercent:number ):void
     {
       this.m_aryPartsWidthPercent[nPartIndex] = fWidthPercent;
       this.m_aryPartsHeightPercent[nPartIndex] = fHeightPercent;

     
     }   

    
    public SetPartColor( nPartIndex:number,r:number, g:number, b:number, a:number ):void
    {
        this.m_aryParts[nPartIndex].filters = [CColorFucker.GetColorMatrixFilterByRGBA( r,g,b,a )];
    }

    

    public SetSize( nWidth:number, nHeight:number ):void{
    this.m_nWidth = nWidth;
    this.m_nHeight = nHeight;


       this.Update();
    }
    
    protected Update():void
    {
        if ( this.m_eType == Global.eDoubleRectBgType.up_down )
        {
            this.m_aryParts[0].width = this.m_nWidth;
            this.m_aryParts[0].height = this.m_nHeight * this.m_aryPartsHeightPercent[0];

            this.m_aryParts[1].width = this.m_nWidth;
            this.m_aryParts[1].height = this.m_nHeight - this.m_aryParts[0].height;
            this.m_aryParts[1].y = this.m_aryParts[0].height;
        }
    }








} // end class