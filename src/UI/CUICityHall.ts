/*
135， 70， 246
*/
class CUICityHall extends egret.DisplayObjectContainer {

        protected m_uiContainer:eui.Component = new eui.Component();
        protected m_btnClose:CCosmosImage;

        protected m_imgBorder:eui.Image;

        protected m_containerItems_city:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
        protected m_aryItems_City:Array<CUICityHallItem> = new Array<CUICityHallItem>();
        protected m_containerItems_game:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
        protected m_aryItems_Game:Array<CUICityHallItem> = new Array<CUICityHallItem>();

        protected m_nCurPage:number = 0; // 0 - city  1 - game
        protected m_btnSwitchCity:CCosmosImage;
        protected m_btnSwitchGame:CCosmosImage;

        protected m_ScrollView:egret.ScrollView = new egret.ScrollView();

       public constructor() {
        super();


        this.m_uiContainer.skinName = "resource/assets/MyExml/CCityHall.exml";
        this.addChild( this.m_uiContainer );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
        
        this.addChild( this.m_ScrollView );
       // sv.setContent( this.m_containerItems_city );
        //sv.setContent( this.m_containerItems_game );

        this.m_ScrollView.x = imgTemp.x;
        this.m_ScrollView.y = imgTemp.y;
        this.m_ScrollView.width = imgTemp.width;  
        this.m_ScrollView.height = imgTemp.height;       
        this.m_ScrollView.verticalScrollPolicy = "on";
        this.m_ScrollView.horizontalScrollPolicy = "off";
        this.m_uiContainer.removeChild( imgTemp );

        // init "city items
        var nPosY:number = 0;
        var nCount:number = 0;
        var nCityNo:number = 1;
        for ( var i:number = 0; i < 32; i++ )
        {
             var item:CUICityHallItem = new CUICityHallItem();
             this.m_containerItems_city.addChild( item );
             this.m_aryItems_City.push( item );
             item.SetBgColor( 42, 90, 246 );
          //   item.SetIcon( RES.getRes( "cityhall_city_" + i + "_png" ) );
             item.SetCityNo( nCityNo );
             item.SetItemIndex( 0, i );
             item.y = nPosY;
             nPosY += 115;
             nCount++;
             if ( nCount == 4 )
             {
                 var txtCityNo:eui.Label = new eui.Label();
                 this.m_containerItems_city.addChild( txtCityNo );
                 txtCityNo.y = nPosY;
                 nPosY += 50;
                 txtCityNo.height = 50;
                 txtCityNo.text = "城市" + (++nCityNo) + " 将开启如下选项：";
                 txtCityNo.size = 22;
                 txtCityNo.verticalAlign = egret.VerticalAlign.MIDDLE;
                 txtCityNo.textColor = 0x696969;
                 nCount = 0;
             }
        }

           // init "game" items
           nPosY = 0;
            for ( var i:number = 0; i < 14; i++ )
        {
             var item:CUICityHallItem = new CUICityHallItem();
             this.m_containerItems_game.addChild( item );
             item.SetBgColor( 135, 70, 246 );
          //   item.SetIcon( RES.getRes( "cityhall_game_" + i + "_png" ) );
             item.SetItemIndex( Global.eCityHallPageType.game, i);
             item.y = nPosY;
             nPosY += 115;
             item.SetLock( false );
             this.m_aryItems_Game.push( item );
        }
       ////////////////
     
        imgTemp = this.m_uiContainer.getChildByName( "img6" ) as eui.Image;
        this.m_btnSwitchCity = new CCosmosImage();
        this.m_btnSwitchCity.SetExml( "resource/assets/MyExml/CCosmosButton5.exml" );
        this.m_uiContainer.addChild( this.m_btnSwitchCity );
        this.m_btnSwitchCity.x = imgTemp.x;
        this.m_btnSwitchCity.y = imgTemp.y;
        this.m_btnSwitchCity.SetImageColor_255( null, 0, 63, 132, 247 );
       // this.m_btnSwitchCity.SetImageVisible( 1, false );
        this.m_btnSwitchCity.SetImageAlpha( 1, 0 );
        this.m_btnSwitchCity.SetLabelContent( 0, "城市" );
        this.m_btnSwitchCity.SetLabelContent( 1, "" );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnSwitchCity.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_SwitchPageCity, this);

        imgTemp = this.m_uiContainer.getChildByName( "img5" ) as eui.Image;
        this.m_btnSwitchGame = new CCosmosImage();
        this.m_btnSwitchGame.SetExml( "resource/assets/MyExml/CCosmosButton5.exml" );
        this.m_uiContainer.addChild( this.m_btnSwitchGame );
        this.m_btnSwitchGame.x = imgTemp.x;
        this.m_btnSwitchGame.y = imgTemp.y;
        this.m_btnSwitchGame.SetImageColor_255( null, 0, 152, 78, 246 );
        this.m_btnSwitchGame.SetLabelContent( 0, "" );
        this.m_btnSwitchGame.SetLabelContent( 1, "游戏(14)" );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnSwitchGame.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_SwitchPageGame, this);


        // 华丽丽的分割线
        this.m_imgBorder = this.m_uiContainer.getChildByName( "img7" ) as eui.Image;


        this.SwitchPage(0);


         imgTemp = this.m_uiContainer.getChildByName( "img8" ) as eui.Image;
        this.m_btnClose = new CCosmosImage();
        this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
        this.m_uiContainer.addChild( this.m_btnClose );
        this.m_btnClose.x = imgTemp.x;
        this.m_btnClose.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnClose.UseColorSolution(0);
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

       } // end constructor

       protected onButtonClick_Close( evt:egret.TouchEvent ):void
       {
         CUIManager.SetUiVisible( Global.eUiId.city_hall, false );
       }

       protected onButtonClick_SwitchPageCity( evt:egret.TouchEvent ):void
       {
         this.SwitchPage( 0 );
       }

       protected onButtonClick_SwitchPageGame( evt:egret.TouchEvent ):void
       {
        this.SwitchPage( 1 );
       }


       protected SwitchPage( nPageIndex:number )
       {
         if ( nPageIndex == 0 ) // city
         {
            this.m_ScrollView.setContent( this.m_containerItems_city );
           this.m_imgBorder.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 39, 83,227 ) ];
         }
         else if ( nPageIndex == 1 ) // game
         {
            this.m_ScrollView.setContent( this.m_containerItems_game );
          this.m_imgBorder.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 125, 65, 227 ) ];
         }


       }

       public UnlockItems(  nCityNo:number ):void
       {
          var container:egret.DisplayObjectContainer = this.m_containerItems_city;
     
          var nCount:number = 0;
          for ( var i:number = 0; i < container.numChildren; i++ )
          {
            if ( nCount == 4 ) // 跳过那个文本框
            {
              nCount = 0;
              continue;
            }
            var item:CUICityHallItem = container.getChildAt(i) as CUICityHallItem;
            if ( item.GetCityNo() <= nCityNo )
            {
              item.SetLock( false );
            }
            else
            {
              return;
            }
            nCount++;
          }
       }

       // 钱币数量在实时改变，因此应该实时刷新这个界面上的按钮状态
       public  UpdateStatus():void
       {
         for ( var i:number = 0; i < this.m_aryItems_City.length; i++ )
         {
           var item:CUICityHallItem = this.m_aryItems_City[i];
           item.UpdateStatus();
         }

         for ( var i:number = 0; i < this.m_aryItems_Game.length; i++ )
         {
           var item:CUICityHallItem = this.m_aryItems_Game[i];
           item.UpdateStatus();
         }

       }


} // end class