class CUIRevertLotConfirm extends egret.DisplayObjectContainer {

      protected m_uiContainer:eui.Component = new eui.Component();
      protected m_btnCancel:CCosmosSimpleButton;
      protected m_btnConfirm:CCosmosSimpleButton;


      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CBulldozeLotConfirm.exml";
        this.addChild( this.m_uiContainer );
            
        this.m_btnCancel = new CCosmosSimpleButton();
        this.m_uiContainer.addChild( this.m_btnCancel );
        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
        this.m_btnCancel.x = imgTemp.x;
        this.m_btnCancel.y = imgTemp.y;
        this.m_btnCancel.scaleX = imgTemp.scaleX;
        this.m_btnCancel.scaleY = imgTemp.scaleY;
        this.m_btnCancel.SetImgColor( 0, 121, 121, 121 );
        this.m_btnCancel.SetTextColor( 0 , 0xFFFFFF );
        this.m_btnCancel.SetTextContent( 0 , "取消" );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnCancel.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Cancel, this);

        this.m_btnConfirm = new CCosmosSimpleButton();
        this.m_uiContainer.addChild( this.m_btnConfirm );
        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img3" ) as eui.Image;
        this.m_btnConfirm.x = imgTemp.x;
        this.m_btnConfirm.y = imgTemp.y;
        this.m_btnConfirm.scaleX = imgTemp.scaleX;
        this.m_btnConfirm.scaleY = imgTemp.scaleY;
        this.m_btnConfirm.SetImgColor( 0, 0, 0, 0  );
        this.m_btnConfirm.SetTextColor( 0 , 0xFFFFFF );
        this.m_btnConfirm.SetTextContent( 0 , "确定" );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnConfirm.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Confirm, this);


        var img0:eui.Image = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
        img0.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 0, 0, 0 ) ];
        img0.alpha = 0.6;

      } // constrcuctor

      private onButtonClick_Cancel( evt:egret.TouchEvent ):void
      {
          this.visible = false;
      }

      private onButtonClick_Confirm( evt:egret.TouchEvent ):void
      {
          this.visible = false;
          CUIManager.SetUiVisible( Global.eUiId.lot_upgrade, false );

          Main.s_CurTown.RevertLot();
      }

} // end class
