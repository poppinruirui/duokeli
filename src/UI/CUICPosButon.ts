/*
    C位按钮
*/
class CUICPosBUtton extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();

     protected m_imgCButton:CCosmosImage;
     protected m_imgCityHall:CCosmosImage;
     protected m_imgTimeBoost:CCosmosImage;
     protected m_imgPig:CCosmosImage;
     protected m_imgSettings:CCosmosImage;
     
     protected m_imgDoubleTimeLeftBg:eui.Image; // 
     protected m_LabelDoubleTimeLeft:eui.Label;


      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/BottomButtons.exml";
        this.addChild( this.m_uiContainer );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName("btnC") as eui.Image;
        this.m_imgCButton = new CCosmosImage();
        this.m_imgCButton.SetExml( "resource/assets/MyExml/CPosButton.exml" );
        this.m_uiContainer.addChild( this.m_imgCButton );
        this.m_imgCButton.x = imgTemp.x;
        this.m_imgCButton.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp ); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了

        imgTemp = this.m_uiContainer.getChildByName("imgCityHall") as eui.Image;
        this.m_imgCityHall = new CCosmosImage();
        this.m_imgCityHall.SetExml( "resource/assets/MyExml/CButtonCityHall.exml" );
        this.m_uiContainer.addChild( this.m_imgCityHall );
        this.m_imgCityHall.x = imgTemp.x;
        this.m_imgCityHall.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp ); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        this.m_imgCityHall.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onTapCityHall, this );

        imgTemp = this.m_uiContainer.getChildByName("imgTimeBoost") as eui.Image;
        this.m_imgTimeBoost = new CCosmosImage();
        this.m_imgTimeBoost.SetExml( "resource/assets/MyExml/CButtomTimeBooste.exml" );
        this.m_uiContainer.addChild( this.m_imgTimeBoost );
        this.m_imgTimeBoost.x = imgTemp.x;
        this.m_imgTimeBoost.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp ); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        this.m_imgTimeBoost.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onTapBoost, this );

        imgTemp = this.m_uiContainer.getChildByName("imgBig") as eui.Image;
        this.m_imgPig = new CCosmosImage();
        this.m_imgPig.SetExml( "resource/assets/MyExml/CButtonPig.exml" );
        this.m_uiContainer.addChild( this.m_imgPig );
        this.m_imgPig.x = imgTemp.x;
        this.m_imgPig.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp ); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了

        imgTemp = this.m_uiContainer.getChildByName("imgSettings") as eui.Image;
        this.m_imgSettings = new CCosmosImage();
        this.m_imgSettings.SetExml( "resource/assets/MyExml/CButtonSettings.exml" );
        this.m_uiContainer.addChild( this.m_imgSettings );
        this.m_imgSettings.x = imgTemp.x;
        this.m_imgSettings.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp ); // 这个临时图片只是个占位符，便于编辑。现在可以从显示列表移除了
        this.m_imgSettings.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onTapSettings, this );

        this.m_imgDoubleTimeLeftBg = this.m_uiContainer.getChildByName("imgDoubleTimeLeftBg") as eui.Image;
        this.m_imgDoubleTimeLeftBg.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 67, 151, 66 )];
        this.m_LabelDoubleTimeLeft = this.m_uiContainer.getChildByName("txtDoubleTimeLeft") as eui.Label;
        this.m_uiContainer.addChild( this.m_imgDoubleTimeLeftBg );
        this.m_uiContainer.addChild( this.m_LabelDoubleTimeLeft );
        this.SetDoubleTimeLeftVisible( false );

      

      }// end constructor

      protected onTapSettings( evt:egret.TouchEvent ):void
      {
          CUIManager.s_uiDeveloperEditor.visible = !CUIManager.s_uiDeveloperEditor.visible;
      }

      protected onTapBoost( evt:egret.TouchEvent ):void
      {
          CUIManager.SetUiVisible( Global.eUiId.boost_panel, true );
      }

      protected onTapCityHall( evt:egret.TouchEvent ):void
      {
                           CUIManager.SetUiVisible( Global.eUiId.city_hall, true );
                 CUIManager.s_uiCityHall.UpdateStatus();
      }

      public SetDoubleTimeLeftVisible( bVisible:boolean ):void
      {
          this.m_imgDoubleTimeLeftBg.visible = bVisible;
          this.m_LabelDoubleTimeLeft.visible = bVisible;
      }

      public SetDoubleTimeLeft( szTimeLeftMin:string ):void
      {
          this.m_LabelDoubleTimeLeft.text = szTimeLeftMin + "分钟";
      }

} // end class
