 /*
  音频系统
 */
module Global{
    export enum eSE{ //  音效
        bank,
        big_coin,
        car,
        click_button,
        common_page_1,
        common_page_2,
        green_bux,
        small_coin,
        congratulations,

        num,
    }

}// end module Global

 class CSoundManager extends egret.DisplayObjectContainer {
 
     protected static s_soundBMG:egret.Sound;
     protected static s_arySE:Array<egret.Sound>;

      public constructor() {
        super();
      }

     public static Init():void
     {
         CSoundManager.s_soundBMG = new egret.Sound();
        CSoundManager.s_soundBMG.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event) {
           
             
        }, this);
        CSoundManager.s_soundBMG.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event:egret.IOErrorEvent) {
            console.log("loaded bmg error!");
        }, this);
        CSoundManager.s_soundBMG.load("resource/assets/Music/duokeli.mp3");
        
        CSoundManager.s_arySE = new Array<egret.Sound>();

        var sound:egret.Sound = new egret.Sound();
        sound.load("resource/assets/Audios/bank.mp3");
        CSoundManager.s_arySE.push(  sound); 
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/big_coin.mp3");
        CSoundManager.s_arySE.push(  sound); 
           sound = new egret.Sound();
        sound.load("resource/assets/Audios/car.mp3");
        CSoundManager.s_arySE.push(  sound); 
           sound = new egret.Sound();
        sound.load("resource/assets/Audios/click_button.mp3");
        CSoundManager.s_arySE.push(  sound); 
           sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_1.mp3");
        CSoundManager.s_arySE.push(  sound); 
           sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_2.mp3");
        CSoundManager.s_arySE.push(  sound); 
           sound = new egret.Sound();
        sound.load("resource/assets/Audios/green_bux.mp3");
        CSoundManager.s_arySE.push(  sound); 
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/small_coin.mp3");
        CSoundManager.s_arySE.push(  sound); 
         sound = new egret.Sound();
        sound.load("resource/assets/Audios/congratulations.mp3");
        CSoundManager.s_arySE.push(  sound); 
     }
 
     public static PlaySE( eId:Global.eSE ):void
     {
        CSoundManager.s_arySE[eId].play( 0, 1 );
     }

     protected static s_bBMGPlaying:boolean = false;
     public static PlayBMG():void
     {
         if ( CSoundManager.s_bBMGPlaying )
         {
             return;
         }
         CSoundManager.s_soundBMG.play();
         CSoundManager.s_bBMGPlaying = true;
     }

 } // end class