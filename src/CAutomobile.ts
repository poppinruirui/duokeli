
class CAutomobile extends CObj {

   
    protected m_bmpMoney:egret.Bitmap = new egret.Bitmap(); // （头顶）钱币

    protected m_Dir:Global.eAutomobileDir = Global.eAutomobileDir.youshang;

    protected m_CurRoad:CObj = null;
    protected m_NextRoad:CObj = null;

    protected m_shapeLocationPoint:egret.Shape = new egret.Shape();

    protected m_eStatus:Global.eAutomobileStatus = Global.eAutomobileStatus.none;
    protected m_nSubStatus:number = 0;
    protected m_nTempMoveAmount:number = 0;
    protected m_fTempGridPosX:number = 0;
    protected m_fTempGridPosY:number = 0;

    protected m_JinBi:CJinBi = null;

    protected m_bHasCoinNow:boolean = false;

    

    protected m_nCarIndex:number = 0;

    protected m_fBoundary_P1_X:number = 0;
    protected m_fBoundary_P1_Y:number = 0;
    protected m_fBoundary_P2_X:number = 0;
    protected m_fBoundary_P2_Y:number = 0;

    public constructor() {
        super();

        this.addChild( this.m_bmpMoney );
        this.addChild( this.m_bmpMainPic );

        this.scaleX = 0.5;
        this.scaleY = 0.5;

        //this.m_bmpMoney.texture = RES.getRes ( "" );

/*
         this.m_shapeLocationPoint.graphics.beginFill(0xFF0000, 0.5);
         this.m_shapeLocationPoint.graphics.drawRect(0, 0, 10, 10);
         this.m_shapeLocationPoint.graphics.endFill();
         this.addChild( this.m_shapeLocationPoint );
         */

        this.touchEnabled = true;
        this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapCar, this);
        
    } // end constructor()

    public onTapCar(e: egret.TouchEvent):void{
        if ( this.m_JinBi != null )
        {
            this.m_JinBi.OnClick();
        }
    }

    public ClearBoundJinBi():void
    {
        this.m_JinBi = null;
        this.SetHasCoin( false );
    }

    public Reset():void
    {
        this.m_CurRoad = null;
        this.m_NextRoad = null;
    }

    public SetJinBi( jinbi:CJinBi ):void
    {
        this.m_JinBi = jinbi;
    }

    public GetDir():Global.eAutomobileDir
    {
        return this.m_Dir;
    }

    public UpdateDir(dir:Global.eAutomobileDir):void
    {
        this.m_Dir = dir;

        this.m_bmpMainPic.texture = CAutomobile.GetAutoMobileResByDir( dir, this.GetCarIndex() );
       

        
    }

    public static GetAutoMobileResByDir( dir:Global.eAutomobileDir, nCarIndex:number ):egret.Texture
    {
          switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                return RES.getRes( "car_" + nCarIndex + "_2" + "_png" );
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                return RES.getRes( "car_" + nCarIndex + "_1" + "_png" );
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {  
                return RES.getRes( "car_" + nCarIndex + "_0" + "_png" );
            }
            break;

            case Global.eAutomobileDir.youxia:
            {
                return RES.getRes( "car_" + nCarIndex + "_3" + "_png" );
            }
            break;

        } // end switch
        return null;
    }

    public SetPos( posX:number, posY:number ):void
    {
        this.x = posX;
        this.y = posY;
    }

    public GetPosX():number
    {
        return this.x;
    }

    public GetPosY():number
    {
        return this.y;
    }

    public SetRaod( road:CObj ):void
    {
        this.m_CurRoad = road;

        // 预判即将踩到的下一块路面
        var nIndexX:number = road.GetLocationGrid().GetId()["nIndexX"];
        var nIndexY:number = road.GetLocationGrid().GetId()["nIndexY"];
        var nIndexX_Next:number = 0;
        var nIndexY_Next:number = 0;
        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                nIndexX_Next = nIndexX + 1;
                nIndexY_Next = nIndexY - 1;
            }
            break;
            case Global.eAutomobileDir.youxia:
            {
                nIndexX_Next = nIndexX + 1;
                nIndexY_Next = nIndexY + 1;
            }
            break;
            case Global.eAutomobileDir.zuoshang:
            {
                nIndexX_Next = nIndexX - 1;
                nIndexY_Next = nIndexY - 1;
            }
            break;
            case Global.eAutomobileDir.zuoxia:
            {
                 nIndexX_Next = nIndexX - 1;
                nIndexY_Next = nIndexY + 1;
            }
            break;


        } // end switch

        var grid_next:CGrid = Main.s_CurTown.GetGridByIndex( nIndexX_Next.toString(), nIndexY_Next.toString() );
        var road_next:CObj = null;
        if ( grid_next == undefined || grid_next == null )
        {
              
        }
        else
        {
            
            road_next = grid_next.GetBoundObj();
            if ( road_next == undefined || road_next == null || road_next.GetFuncType() != Global.eObjFunc.road )
            {
                 if ( road_next != null && road_next != undefined ){

                 }
                road_next = null;
            }
        }

        this.m_NextRoad = road_next; // null表示断头路：下一个地块不是马路，车辆不能通行

    }

    public GetRaod():CObj
    {
        return this.m_CurRoad;
    }

    public GetNextRoad():CObj{
        return this.m_NextRoad;
    }

    public GetHeight():number
    {
        return this.height;
    }

    public Turn_Right():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.turn_right )
        {
            return;
        }

        

        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

        // check if Turn-left completed

        var nRet:number = 0;
        var grid:CGrid = this.m_NextRoad.GetLocationGrid();
        var bCanCompleted:boolean = false;
        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_5),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_5)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youxia );

                    bCanCompleted = true;
                }
            }
            break;
                
            case Global.eAutomobileDir.youxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_6),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_6)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoxia );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_7),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_7)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youshang );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX( Global.eGridCornerPosType.one_fourth_1),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_1),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_4),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_4)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoshang );

                    bCanCompleted = true;
                }
            }
            break;

        } // end switch

        if ( bCanCompleted )
        {
            this.SetRaod( this.m_NextRoad );
            this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
            this.m_eStatus = Global.eAutomobileStatus.normal;

        }
       
    } // end Turn_Left

    public Turn_Left():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.turn_left )
        {
            return;
        }

       
        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

        // check if Turn-left completed

        var nRet:number = 0;
        var grid:CGrid = this.m_NextRoad.GetLocationGrid();
        var bCanCompleted:boolean = false;
        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_4),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_4),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_1),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_1)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoshang );

                    bCanCompleted = true;
                }
            }
            break;
                
            case Global.eAutomobileDir.youxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_7),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_7)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youshang );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_6),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_6)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoxia );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_5),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_5)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youxia );

                    bCanCompleted = true;
                }
            }
            break;

        } // end switch

        if ( bCanCompleted )
        {
            this.SetRaod( this.m_NextRoad );
            this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
            this.SetMoveStatus(  Global.eAutomobileStatus.normal );

        }
       
    } // end Turn_Left

    public U_Turn():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.u_turn )
        {
            return;
        }

        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];
       
       
        if ( this.m_nSubStatus == 0 ) // 掉头的第一阶段
        {
            this.m_nTempMoveAmount += CAutomobile.s_objectSpeedByDir["x"];
            if ( Math.abs( this.m_nTempMoveAmount ) >= CDef.s_nTileWidth * 0.25 )
            {
                this.m_nSubStatus = 1;
               
                switch( this.m_Dir )
                {
                    case Global.eAutomobileDir.youxia:
                    {
         
                        this.UpdateDir( Global.eAutomobileDir.youshang );
                    }
                    break;

                    case Global.eAutomobileDir.zuoshang:
                    {
                        this.UpdateDir( Global.eAutomobileDir.zuoxia );
                    }
                    break;

                    case Global.eAutomobileDir.zuoxia:
                    {
                        this.UpdateDir( Global.eAutomobileDir.youxia );
                    }
                    break;

                    case Global.eAutomobileDir.youshang:
                    {
                        this.UpdateDir( Global.eAutomobileDir.zuoshang );
                    }
                    break;
                }

                 this.SetStartPosByDir(  this.m_Dir, this.m_fTempGridPosX, this.m_fTempGridPosY )
            }

        } // end 掉头的第一阶段
        else if ( this.m_nSubStatus == 1 ) // 掉头的第二阶段
        {
               var bLeaveTempGrid = CAutomobile.CheckIfLeaveGrid( this.m_Dir, this.m_fTempGridPosY, this.y, this.GetHeight() )
                if ( bLeaveTempGrid )
                {
                    this.m_eStatus = Global.eAutomobileStatus.normal;
                    this.SetRaod( this.m_CurRoad ); // 这里必须再重新setroad一次，不然next-road不会刷新 
                  //  this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
                }
        }



    }

    public SetMoveStatus( eStatus:Global.eAutomobileStatus ):void
    {
      

        this.m_eStatus= eStatus;

    
    }

    public Move():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.normal )
        {
            return;
        }

        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

        this.UpdateRoadInfo();
        
    }

    private static s_objectSpeedByDir:Object = new Object();
    public static GetSpeedByDir( dir:Global.eAutomobileDir ):void
    {
        var speedX = CDef.s_fAutomobileMoveDirX;
        var speedY = CDef.s_fAutomobileMoveDirY;

        switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                speedY= - speedY;
            }
            break;
             case Global.eAutomobileDir.youxia:
            {

            }
            break;
            case Global.eAutomobileDir.zuoshang:
            {
                speedX= - speedX;
                speedY= - speedY;
            }
            break;
            case Global.eAutomobileDir.zuoxia:
            {
                speedX= - speedX;
            }
            break;           
        } // end switch

        CAutomobile.s_objectSpeedByDir["x"] = speedX;
        CAutomobile.s_objectSpeedByDir["y"] = speedY;
    }

    public GetLocationPosX():number
    {
        return this.x + this.m_shapeLocationPoint.x;
    }

    public GetLocationPosY():number
    {
        return this.y + this.m_shapeLocationPoint.y;
    }

    public SetStartPosByDir( dir:Global.eAutomobileDir, nGridPosX:number,  nGridPosY:number):void
    {       // poppin trcik
        

               if ( dir == Global.eAutomobileDir.youshang )
                {
                        this.x = nGridPosX + CDef.s_nTileWidth * 0.125;
                        this.y = nGridPosY - CDef.s_nTileHeight * 0.375 ;
                }
                else if ( dir == Global.eAutomobileDir.youxia )
                {
                       this.x = nGridPosX - CDef.s_nTileWidth  * 0.125;
                       this.y = nGridPosY - CDef.s_nTileHeight * 0.375 ;
                }
                else if ( dir == Global.eAutomobileDir.zuoshang )
                {
                        this.x = nGridPosX + CDef.s_nTileWidth * 0.125;
                        this.y = nGridPosY - CDef.s_nTileHeight * 0.625;       
                }
                else if ( dir == Global.eAutomobileDir.zuoxia )
                {
                        this.x = nGridPosX - CDef.s_nTileWidth  * 0.125;
                        this.y = nGridPosY - CDef.s_nTileHeight * 0.625;; 
                }

              this.y += CDef.s_nTileHeight / 2;

                
    }

    public GenerateCurBoundary():void
    {

    }

    public CheckIfLeaveCurGrid():boolean
    {
        var grid:CGrid = this.GetRaod().GetLocationGrid();
        var nRet:number = 0;

        switch( this.m_Dir )
        {
             case Global.eAutomobileDir.youshang:
             {  
               nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.top ),
               grid.GetCornerPosY(Global.eGridCornerPosType.top), 
               grid.GetCornerPosX(Global.eGridCornerPosType.right),
               grid.GetCornerPosY(Global.eGridCornerPosType.right)  );

               if ( nRet == -1 )
               {
                   return true;
               }
             }
             break;

             case Global.eAutomobileDir.youxia:
             {
                nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.bottom ),
               grid.GetCornerPosY(Global.eGridCornerPosType.bottom), 
               grid.GetCornerPosX(Global.eGridCornerPosType.right),
               grid.GetCornerPosY(Global.eGridCornerPosType.right)  );

                if ( nRet == -1 )
               {
                   return true;
               }
             }
             break;

             case Global.eAutomobileDir.zuoshang:
             {
                 nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.top ),
               grid.GetCornerPosY(Global.eGridCornerPosType.top), 
               grid.GetCornerPosX(Global.eGridCornerPosType.left),
               grid.GetCornerPosY(Global.eGridCornerPosType.left)  );

                if ( nRet == 1 )
               {
                   return true;
               }
             }
             break;

             case Global.eAutomobileDir.zuoxia:
             {
                 nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.left ),
               grid.GetCornerPosY(Global.eGridCornerPosType.left), 
               grid.GetCornerPosX(Global.eGridCornerPosType.bottom),
               grid.GetCornerPosY(Global.eGridCornerPosType.bottom)  );

                              if ( nRet == 1 )
               {
                   return true;
               }
             }
             break;

        }


        return false;
    }

    public static CheckIfLeaveGrid( dir:Global.eAutomobileDir, nGridPosY:number, nCarPosY:number, nCarHeight:number ):boolean
    {   


        /*
        switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
               
                if ( nCarPosY < ( nGridPosY - CDef.s_nTileHeight / 2 ) )
                {
                    //console.log( "离开" );
                     return true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
               
                if ( ( nCarPosY - 40 ) < ( nGridPosY - CDef.s_nTileHeight ) )
                {
                    //console.log( "离开" );
                    return  true;
                }
            }
            break;

            case Global.eAutomobileDir.youxia:
            {
               
                if ( ( nCarPosY - nCarHeight + 90 ) >  nGridPosY  )
                {
                    //console.log( "离开youxia" );
                    return  true;
                }
            }
            break;

             case Global.eAutomobileDir.zuoxia:
            {
               
                if ( ( nCarPosY - nCarHeight + 40 ) >  nGridPosY - CDef.s_nTileHeight / 2  )
                {
                   // console.log( "离开zuoxia" );
                   return true;
                }
            }
            break;
        } // end switch
        */


        return false;
    }

    protected UpdateRoadInfo():void
    {
        var bLeave:boolean = false;

        // 判断离开当前路块没有
       // bLeave = CAutomobile.CheckIfLeaveGrid( this.m_Dir, this.m_CurRoad.y, this.y, this.height);
       bLeave = this.CheckIfLeaveCurGrid();
      

        if ( !bLeave )
        {
            return;
        }


        if ( this.m_NextRoad == null ) // 没路了(原则上不会出现这种情况，地图编辑的时候不要出现断头路)
        {
            //  直接掉头
            this.JustUTurn();
            return;
        }
     

        ////  有路，判断是直行、左转、右转
        //  判断有没有直行的机会
        var nStraightIndexX:number = 0;
        var nStraightIndexY:number = 0;
        var nLeftIndexX:number = 0;
        var nLeftIndexY:number = 0;
        var nRightIndexX:number = 0;
        var nRightIndexY:number = 0;

        var nCurGridX:number = this.m_CurRoad.GetLocationGrid().GetId()["nIndexX"];
        var nCurGridY:number = this.m_CurRoad.GetLocationGrid().GetId()["nIndexY"];

        var grid_straight:CGrid = null;
        var road_straight:CObj = null;

        var grid_left:CGrid = null;
        var road_left:CObj = null;

        var grid_right:CGrid = null;
        var road_right:CObj = null;


        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youxia:
            {
                nStraightIndexX = nCurGridX + 2; 
                nStraightIndexY = nCurGridY + 2;
                nLeftIndexX = nCurGridX + 2;
                nLeftIndexY = nCurGridY;
                nRightIndexX = nCurGridX;
                nRightIndexY = nCurGridY + 2;
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {
                nStraightIndexX = nCurGridX - 2; 
                nStraightIndexY = nCurGridY + 2;
                nLeftIndexX = nCurGridX;
                nLeftIndexY = nCurGridY + 2;
                nRightIndexX = nCurGridX - 2;
                nRightIndexY = nCurGridY;
            }
            break;

            case Global.eAutomobileDir.youshang:
            {
                nStraightIndexX = nCurGridX + 2; 
                nStraightIndexY = nCurGridY - 2;
                nLeftIndexX = nCurGridX;
                nLeftIndexY = nCurGridY - 2;
                nRightIndexX = nCurGridX + 2;
                nRightIndexY = nCurGridY;
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                nStraightIndexX = nCurGridX - 2; 
                nStraightIndexY = nCurGridY - 2;
                nLeftIndexX = nCurGridX - 2;
                nLeftIndexY = nCurGridY;
                nRightIndexX = nCurGridX;
                nRightIndexY = nCurGridY - 2;
            }
            break;

        } // end switch

        grid_straight = Main.s_CurTown.GetGridByIndex( nStraightIndexX.toString(), nStraightIndexY.toString() );
        grid_left = Main.s_CurTown.GetGridByIndex( nLeftIndexX.toString(), nLeftIndexY.toString() );
        grid_right = Main.s_CurTown.GetGridByIndex( nRightIndexX.toString(), nRightIndexY.toString() );
 

        CAutomobile.s_aryTempChoice.length = 0;
        if ( grid_straight == null || grid_straight == undefined )
        {
            
        }
        else
        {
            road_straight = grid_straight.GetBoundObj();
            if ( road_straight != null && road_straight.GetFuncType() == Global.eObjFunc.road )
            {
                CAutomobile.s_aryTempChoice.push(0);
            }
            else
            {
                road_straight = null;
            }
        }
        
        if ( grid_left == null || grid_left == undefined )
        {
            
        }
        else
        {
            road_left = grid_left.GetBoundObj();
            if ( road_left != null &&road_left.GetFuncType() == Global.eObjFunc.road )
            {
                CAutomobile.s_aryTempChoice.push(1);
            }
            else
            {
                road_left = null;
            }
        }

        if ( grid_right == null || grid_right == undefined )
        {
            
        }
        else
        {
            road_right = grid_right.GetBoundObj();
            if ( road_right != null && road_right.GetFuncType() == Global.eObjFunc.road )
            {
                CAutomobile.s_aryTempChoice.push(2);
            }
            else
            {
                road_right = null;
            }
        }

       
      //  console.log( CAutomobile.s_aryTempChoice.length );
        if ( CAutomobile.s_aryTempChoice.length == 0 )
        {
          this.JustUTurn();
          return;
        }

        

        var nChoice:number = 0;
        if ( CAutomobile.s_aryTempChoice.length == 1 )
        {
            nChoice = CAutomobile.s_aryTempChoice[0];
        }
        else if ( CAutomobile.s_aryTempChoice.length == 2 )
        {
            var nRan:number = Math.random();
            if ( nRan < 0.5 )
            {
                nChoice = CAutomobile.s_aryTempChoice[0];
            }
            else
            {
                nChoice = CAutomobile.s_aryTempChoice[1];
            }
        } else if ( CAutomobile.s_aryTempChoice.length == 3 )
        {
            var nRan:number = Math.random();
            if ( nRan < 0.33 )
            {
                nChoice = CAutomobile.s_aryTempChoice[0];
            }
            else if ( nRan < 0.66 )
            {
                nChoice = CAutomobile.s_aryTempChoice[1];
            }
            else
            {
                nChoice = CAutomobile.s_aryTempChoice[2];
            }
        }

        if ( nChoice == 0 ) // 直行
        {
            this.SetRaod( this.m_NextRoad ); // 注意不是road_straight 

            // 继续维持直行状态。即便状态并没改变，也必须重新执行SetMoveStatus（）, 因为当前准备进入下一个地块了，牵涉的路面数据要更新
            this.SetMoveStatus( Global.eAutomobileStatus.normal ); 
        }
        else if ( nChoice == 1 ) // 左转
        {
            //this.JustTurnLeft();
            // this.SetRaod( this.m_NextRoad ); 
             this.SetMoveStatus( Global.eAutomobileStatus.turn_left ); 
        }
        else // 右转
        {
          // this.SetRaod( this.m_NextRoad ); 
             this.SetMoveStatus( Global.eAutomobileStatus.turn_right ); 
           
        } // end 右转

    } // end UpdateRoadInfo()
    
    public static s_aryTempChoice:Array<number> = new Array<number>();

    protected JustTurnLeft():void
    {
         this.m_eStatus = Global.eAutomobileStatus.turn_left;
         this.m_nSubStatus = 0;
            this.m_nTempMoveAmount = 0;
    }

    protected JustUTurn():void
    {

            switch( this.m_Dir )
            {
                case Global.eAutomobileDir.zuoxia:
                {
                    this.UpdateDir( Global.eAutomobileDir.youxia );
                    this.m_fTempGridPosX = this.m_CurRoad.x - CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y + CDef.s_nTileHeight * 0.5;
                }
                break;

                case Global.eAutomobileDir.youshang:
                {
                    this.UpdateDir( Global.eAutomobileDir.zuoshang );
                    this.m_fTempGridPosX = this.m_CurRoad.x + CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y - CDef.s_nTileHeight * 0.5;
                }
                break;

                case Global.eAutomobileDir.zuoshang:
                {
                    this.UpdateDir( Global.eAutomobileDir.zuoxia );
                    this.m_fTempGridPosX = this.m_CurRoad.x - CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y - CDef.s_nTileHeight * 0.5;
                }
                break;

               case Global.eAutomobileDir.youxia:
                {
                    this.UpdateDir( Global.eAutomobileDir.youshang );
                    this.m_fTempGridPosX = this.m_CurRoad.x + CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y + CDef.s_nTileHeight * 0.5;
                }
                break;
            } // end switch
            this.m_eStatus = Global.eAutomobileStatus.u_turn;
            this.m_nSubStatus = 0;
            this.m_nTempMoveAmount = 0;
    }

    protected m_fReInsertSortTimeElapse:number = 0;
    public ReInsertSort():void
    {
        this.m_fReInsertSortTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_fReInsertSortTimeElapse < CDef.s_fCarReInsertSortInterval )
        {
            return;
        }
        this.m_fReInsertSortTimeElapse = 0;
        this.SetSortPosY();
        Main.s_CurTown.ReInsertSortObj( this );

    }

    public SetHasCoin( bHas:boolean ):void
    {
        this.m_bHasCoinNow = bHas;
    }

    public GetHasCoin():boolean
    {
        return this.m_bHasCoinNow;
    }

    protected m_nGenCoinTimeElapse:number = 0;
    public GenerateCoinLoop():void
    {
        if ( this.GetHasCoin() )
        {
            return;
        }


        var bGenCoin:boolean = false;
        this.m_nGenCoinTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_nGenCoinTimeElapse < 20 )
        {
             return;          
        }
        this.m_nGenCoinTimeElapse = 0;

        var nTemp:number = Math.random();
        if ( true/*nTemp < 0.7*/ ) // 生成金币
        {
                var jinbi:CJinBi = CJinBiManager.NewJinBi();
                jinbi.name = "jinbi";
                this.addChild(jinbi);
                jinbi.x = this.width / 2;
                jinbi.y = -50;
                jinbi.SetBoundObj( this );
                this.SetJinBi( jinbi );
                jinbi.Update();
                jinbi.visible = true;   
                this.SetHasCoin( true );
                jinbi.scaleX = 1.5;
                jinbi.scaleY = 1.5;
                var nValue:number = CConfigManager.GetCarJinBiValueRate( Global.eMoneySubType.small_coin ) * Main.s_CurTown.GetCPS();
                jinbi.SetValue( Global.eMoneySubType.small_coin, nValue );
        }  
        else // 生成钻石
        {

        }
             
    } // end       GenerateCoinLoop     

    public JinBiLoop():void
    {
        if ( this.m_JinBi == null )
        {
            return;
        }
        this.m_JinBi.Update();
    }

    public SetCarIndex( nIndex:number ):void
    {
        this.m_nCarIndex = nIndex;
    }

    public GetCarIndex():number   
    {
        return this.m_nCarIndex;
    }

} // end class

