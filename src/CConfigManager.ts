class CConfigManager extends egret.DisplayObjectContainer {

    protected static s_dicLotPrice:Object = new Object(); // 每一个地块的价格
    protected static s_dicLot2Population:Object = new Object(); // 每一个地块增加的人口数
    protected static s_nPopulation2CoinXiShu:number = 0.05; // 人口对金币产出的系数。 比如当前人口3000，则金币产出能力是3000 * 0.05 = 150/秒

    protected static s_dicLevel2Coin:Object = new Object(); // 每个等级的建筑（其实跟建筑没多大关系，是地块）每秒产出金币数量

    protected static s_dicTempData:Object = new Object();

  //  protected static s_aryCoinForUnlockCar:Array<number> = new Array<number>();
    protected static s_aryCars:Array<CConfigCar> = new Array<CConfigCar>();
    protected static s_nCarTotalNum:number = 10;
    protected static s_aryCarName:string[] = 
    [
        "小汽车（蓝）",
        "小汽车（粉）",
        "SUV（白）",
        "面包车",
         "卡车",
        "跑车（红）",
        "警车",
        "出租车",
       "园丁车",
        "冰淇淋车",

    ];

    protected static s_aryCarDesc:string[] = 
    [
        "随处可见的小汽车（蓝）",
        "惊艳的颜色",
        "一家人去春游吧",
        "和面包没有一点关系",
         "喂？搬家公司吗？",
        "做一个风一样的男子",
        "乌拉乌拉乌拉，制服真帅气",
        "最地道的当地导游~",
       "路过都是花香",
        "哇~香甜的味道，我想要草莓牛奶味的。",
       
       
       
      
        
    ];

    public static s_nTotalCarNum:number = 14;

    //// 钱币相关配置 
    protected static  s_dicMoneyRateOfCPS:Object = new Object();

    //// 建筑升级的相关配置
    protected static s_nConstructingTime:number = 3; 
    protected static s_aryUpgadeTime:Array<number> = new Array<number>();
    protected static s_dicLotCPSByLevel:Object = new Object();
    protected static s_dicNeedDiamondsForInstantUpgrade:Object = new Object();

    //// “CityHall相关配置”
    protected static s_dicCityHallCity:Object = new Object();
    protected static s_dicCityHallGame:Object = new Object();


    //// 银行相关配置
    protected static s_nBankTimeChangeAmount:number = 15;
    protected static s_nBankSaveRate:number = 0.1;
    protected static s_nBankCostStartValue:number = 31623;
    protected static s_nBankCostChangeRate:number = 1.3162;
    protected static s_dicBankCost:Object = new Object();
    //// end  银行相关配置
   
    //// 城市升级相关配置
    protected static s_dicCityLevelConfig:Object = new Object();
    //// 


    //// 建筑相关配置
    protected static s_dicBuildings:Object = new Object();
    protected static s_dicSpecialBuildings:Object = new Object();

    //// end 建筑相关配置

    public constructor() {
        super();

    }

    public static s_aryBuildingConfig:Array<Object> = new Array<Object>();

    public static Init():void
    {
        var ary:Array<number> = new Array<number>();
        ary.push(2000);
        ary.push(6112);
        ary.push(68800);
        ary.push(207861);
        CConfigManager.s_dicLotPrice[Global.eObjSize.size_2x2] = ary;
       //  CConfigManager.s_dicLotPrice[Global.eObjSize.size_4x4] = ary;

        CConfigManager.s_dicLot2Population[Global.eObjSize.size_2x2] = 1000;
       // CConfigManager.s_dicLot2Population[Global.eObjSize.size_4x4] = ??;

       ary = new Array<number>();
       for ( var i:number = 1; i <= 100; i++ )
       {
           ary.push(  25 *  i  );
       }
       CConfigManager.s_dicLevel2Coin[Global.eObjSize.size_2x2] = ary;
      //  CConfigManager.s_dicLevel2Coin[Global.eObjSize.size_4x4] = ??;


      // 解锁汽车所需要的金币数
      /*
      CConfigManager.s_aryCoinForUnlockCar.push(12664);
      CConfigManager.s_aryCoinForUnlockCar.push(14843);
      CConfigManager.s_aryCoinForUnlockCar.push(22399);
       CConfigManager.s_aryCoinForUnlockCar.push(48594);
     CConfigManager.s_aryCoinForUnlockCar.push(139409);
        */   
        for ( var i:number = 0; i < CConfigManager.s_nCarTotalNum; i++ )
        {
           // CConfigManager.s_aryCoinForUnlockCar.push(100 * i);
           var car_config:CConfigCar = new CConfigCar();
           car_config.nPrice = 100 * (i + 1);
           car_config.szName = CConfigManager.s_aryCarName[i];
           car_config.szDesc = CConfigManager.s_aryCarDesc[i];
           CConfigManager.s_aryCars.push( car_config );
        }      

        // 钱币相关配置
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.small_coin] = 0.5;
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.big_coin] = 5;
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.small_diamond] = 1;
        CConfigManager.s_dicMoneyRateOfCPS[Global.eMoneySubType.big_diamond] = 10;

        // 升级相关配置
        for ( var i:number = 1; i < 1000; i++ )
        {
            var nTime:number = i;
            if (nTime < 3)
            {
                nTime = 3;
            }
            CConfigManager.s_aryUpgadeTime.push( nTime );
        }


        var aryLotCPSbyLevel:Array<number> = new Array<number>();
        for ( var i:number = 1; i < 100; i++ )
        {
            aryLotCPSbyLevel.push( i * 25 );
        }

        CConfigManager.s_dicLotCPSByLevel[Global.eObjSize.size_2x2] = aryLotCPSbyLevel;
        // this.s_dicLotCPSByLevel[Global.eObjSize.size_4x4] = aryLotCPSbyLevel;

        var aryDiamondForInstantUpgrade:Array<number> = new Array<number>();
        for ( var i:number = 1; i < 100; i++ )
        {
            aryDiamondForInstantUpgrade.push( Math.sqrt( 5 * i ) );
        }
        CConfigManager.s_dicNeedDiamondsForInstantUpgrade[Global.eObjSize.size_2x2] = aryDiamondForInstantUpgrade;
        // CConfigManager.s_dicNeedDiamondsForInstantUpgrade[Global.eObjSize.size_2x2] = aryDiamondForInstantUpgrade;

        CConfigManager.InitCityHall_City();
        CConfigManager.InitCityHall_Game();

        //// 银行相关
        var nCurBankCost:number = 0;
        for ( var i:number = 1; i < 50; i++ )
        {
            if ( i == 1 )
            {
              nCurBankCost = CConfigManager.s_nBankCostStartValue;   
            }
            else
            {
                nCurBankCost *= CConfigManager.s_nBankCostChangeRate;
                CConfigManager.s_dicBankCost[i] = nCurBankCost;
            }
             CConfigManager.s_dicBankCost[i] = nCurBankCost;
        }
        //// end 银行相关


        CConfigManager.InitCityLevel();

        //// 建筑相关
        CConfigManager.InitBuildings();
        CBuildingManager.LoadConfig();
        //// end 建筑相关 
    }

    public static GenShowStyle( eStyle:Global.eValueShowStyle, nValue:number ):string
    {
        if ( nValue < 0 )
        {
            nValue = -nValue;
        }

        var szContent:string = nValue.toString();
        switch(eStyle)
        {
            case Global.eValueShowStyle.percent:
            {
                var nShowValue = Math.floor( nValue * 100 );
                szContent = nShowValue + "%";
            }   
            break;

        }

        return szContent;
    }

    public static GetCityHallItemConfig( nPageIndex:number, nItemIndex:number ):CConfigCityHallItem
    {
        var dic:Object = null;
        if ( nPageIndex == 0 )
        {
            dic = CConfigManager.s_dicCityHallCity;
        }
        else
        {
            dic = CConfigManager.s_dicCityHallGame;
        }
        if ( dic == null || dic == undefined )
        {
            return null;
        }

        var config:CConfigCityHallItem = dic[nItemIndex];
        if ( config == undefined )
        {
            config = null;
        }

        return config;
    }

    public static InitCityHall_Game():void
    {
        var config:CConfigCityHallItem = new CConfigCityHallItem();
        config.szName = "日间交易";
        config.szDesc = "银行持续产出时间提升";
        config.nGain = 0.25;
        config.eShowStyle = Global.eValueShowStyle.percent;
        var nCurValue:number = 0;
        var bFirst:boolean = true;
        for ( var i:number = 0; i < 10; i++ )
        {
            if ( bFirst )
            {
                nCurValue = 40;
                bFirst  = false;
            }
            else
            {
                nCurValue = nCurValue * 1.397;
            }
            config.aryValuesCost.push( nCurValue );
        } // end for 

        CConfigManager.s_dicCityHallGame[0] = config;
    }

    public static InitCityHall_City():void
    {
        var config:CConfigCityHallItem = new CConfigCityHallItem();
        config.szName = "物业税";
        config.szDesc = "建筑的金币收益提升";
        config.nGain = 0.1;
        config.eShowStyle = Global.eValueShowStyle.percent;
        var nCurValue:number = 0;
        var bFirst:boolean = true;
        for ( var i:number = 0; i < 50; i++ )
        {
            if ( bFirst )
            {
                nCurValue = 25;
                bFirst  = false;
            }
            else
            {
                nCurValue = nCurValue * 1.16;
            }
            config.aryValuesCost.push( nCurValue );
        } // end for 

        CConfigManager.s_dicCityHallCity[0] = config;
    }

    public static GetBuildingConfigByType( eType:Global.eBuildinSpeical ):Object
    {
        return CConfigManager.s_aryBuildingConfig[eType];
    }

    public static GetBuyLotInfo( eSizeType:Global.eObjSize, nCurTotalNum:number ):Object
    {
       
        CConfigManager.s_dicTempData["BuyOnePrice"] = (CConfigManager.s_dicLotPrice[eSizeType])[nCurTotalNum];
      
        return CConfigManager.s_dicTempData;
    }
    
    public static GetLot2Poulation( eSizeType:Global.eObjSize ):number
    {
        if ( CConfigManager.s_dicLot2Population[eSizeType] == undefined )
        {
            return 0;
        }

        return CConfigManager.s_dicLot2Population[eSizeType];
    }

    public static GetBuildingCoinBySizeAndLevel( eSizeType:Global.eObjSize, nLotLevel:number ):number
    {
        if ( CConfigManager.s_dicLevel2Coin[eSizeType] == undefined )
        {
            return 0;
        }
        var ary:Array<number> = CConfigManager.s_dicLevel2Coin[eSizeType];
        var nIdx:number = nLotLevel - 1;
        if ( nIdx < 0 || nIdx >= ary.length )
        {
            return 0;
        }

        return ary[nIdx];

    }

    public static GetPopulation2CoinXiShu():number
    {
        return CConfigManager.s_nPopulation2CoinXiShu;
    }

    public static GetCoinForUnlockCar( nCarNum:number ):number
    {
        if ( nCarNum >= CConfigManager.s_aryCars.length )
        {
            return 10000000000;
        }

        var car_config:CConfigCar = CConfigManager.s_aryCars[nCarNum];

        return car_config.nPrice;
    }

    public static GetCarConfig( nIndex:number ):CConfigCar
    {
        if ( nIndex >= CConfigManager.s_aryCars.length )
        { 
            return null;
        }

        return CConfigManager.s_aryCars[nIndex];

    }

    public static GetCarJinBiValueRate( eType:Global.eMoneySubType ):number{
        return CConfigManager.s_dicMoneyRateOfCPS[eType];
    }

    public static GetConstructingTime():number
    {
        return CConfigManager.s_nConstructingTime;
    }

    public static GetBuildingTextureByLevel( nLotLevel:number ):string
    {
        return "yigoudi_png";
    }

    public static GetEmptyLotTextureBySize( eSizeType:Global.eObjSize ):egret.Texture
    {
        if ( eSizeType == Global.eObjSize.size_2x2 )
        {
            return RES.getRes( "dikuai_2_2_png" );
        }
        else if ( eSizeType == Global.eObjSize.size_4x4 )
        {
            return RES.getRes( "dikuai_4_4_png" );
        }

        return null;
    }
    public static GetUpgradeTimeByLotLevel( nLotLevel:number ):number
    {
        return CConfigManager.s_aryUpgadeTime[nLotLevel];
    }

    public static GetLotCPSByLevel( eSizeType:Global.eObjSize, nLotLevel:number ):number
    {
        var ary:Array<number> = CConfigManager.s_dicLotCPSByLevel[eSizeType];
        return ary[nLotLevel];
    }

    public static GetNeedDiamondBySizeAndLevel( eSizeType:Global.eObjSize, nLotLevel:number ):number
    {
         var ary:Array<number> = CConfigManager.s_dicNeedDiamondsForInstantUpgrade[eSizeType];
        return ary[nLotLevel];

    }

    public static GetFastforwardCost( nIndex:number ):number
    {
        switch( nIndex )
        {
            case 0:
            {
                return 50;
            }
            break;

            case 1:
            {
                return 250;
            }
            break;
        }
    }

    /////// 银行相关 
    public static GetBankTimeChangeAmount():number
    {
        return CConfigManager.s_nBankTimeChangeAmount;
    }

    public static GetBankProfitRate():number
    {
        return CConfigManager.s_nBankSaveRate;
    }

    public static GetBankCostByLevel( nLevel:number ):number
    {
        return CConfigManager.s_dicBankCost[nLevel];
    }

    /////// end 银行相关

    ////  城市升级相关
    public static InitCityLevel():void
    {
        // 先写死，稍后做读配置表流程
        var config:CCofigCityLevel = new CCofigCityLevel();
        config.nPopulationToLevelUp = 4000;
        config.nStartCoins = 50000;
        config.nKeys = 1;
        config.szName = "浮云小镇";
        CConfigManager.s_dicCityLevelConfig[1] = config;

        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 10000;
        config.nStartCoins = 100000;
        config.nKeys = 2;
        config.szName = "闪金平原";
        CConfigManager.s_dicCityLevelConfig[2] = config;

        config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 33000;
        config.nStartCoins = 150000;
        config.nKeys = 2;
        config.szName = "镖客镇";
        CConfigManager.s_dicCityLevelConfig[3] = config;


                config = new CCofigCityLevel();
        config.nPopulationToLevelUp = 69000;
        config.nStartCoins = 200000;
        config.nKeys = 2;
        config.szName = "亚特兰蒂斯";
        CConfigManager.s_dicCityLevelConfig[4] = config;
        
    }

    public static GetCityLevelConfig( nLevel:number ):CCofigCityLevel
    {
        return CConfigManager.s_dicCityLevelConfig[nLevel];   
    }

    //// end  城市升级相关
    static s_nBuildingConfigGuid:number = 0;
    public static GenerateBuildingConfigGuid():number
    {
        return (CConfigManager.s_nBuildingConfigGuid++);
    }

    public static PushBuildingConfig2Array( ary:Array<CConfigBuilding>, config:CConfigBuilding ):void
    {
        ary.push( config );
        config.nID = CConfigManager.GenerateBuildingConfigGuid();
        
    }

    //// 建筑物相关
    public static InitBuildings():void
    {
        //// 普通建筑

        var aryResidential:Array<CConfigBuilding> = new Array<CConfigBuilding>();
        var config:CConfigBuilding = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "yigoudi_png";
        config.szName = "荒芜之地";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array( aryResidential, config );

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "zhangpeng_png";
        config.szName = "帐篷";
       // aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array( aryResidential, config );

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "mingjun_png";
        config.szName = "小民居";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array( aryResidential, config );

        
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "shangpinglou_png";
        config.szName = "豪华公寓";
        //aryResidential.push(config);
        CConfigManager.PushBuildingConfig2Array( aryResidential, config );

        
        /*
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "bieshu_png";
        config.szName = "别墅";
        aryResidential.push(config);  
        */

        CConfigManager.s_dicBuildings[Global.eLotPsroperty.residential] = aryResidential;

        ////////
        var aryBusiness:Array<CConfigBuilding> = new Array<CConfigBuilding>();

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "zidongshouhuoji_png";
        config.szName = "自动售货机";
       // aryBusiness.push(config);  
        CConfigManager.PushBuildingConfig2Array( aryBusiness, config );
       

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "ditan-shucai_png";
        config.szName = "低碳蔬菜";
       // aryBusiness.push(config); 
        CConfigManager.PushBuildingConfig2Array( aryBusiness, config ); 

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "kuaichandian_png";
        config.szName = "快餐店";
       // aryBusiness.push(config); 
        CConfigManager.PushBuildingConfig2Array( aryBusiness, config ); 

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "bianlidian_png";
        config.szName = "便利店";
      //  aryBusiness.push(config);
        CConfigManager.PushBuildingConfig2Array( aryBusiness, config );  

/*
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "shaoshi_png";
        config.szName = "超市";
        aryBusiness.push(config);  
        */


        CConfigManager.s_dicBuildings[Global.eLotPsroperty.business] = aryBusiness;

        //////
                ////////
        var aryService:Array<CConfigBuilding> = new Array<CConfigBuilding>();

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "shuita_png";
        config.szName = "水塔";
       // aryService.push(config);  
       CConfigManager.PushBuildingConfig2Array( aryService, config );

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "changku_png";
        config.szName = "仓库";
        //aryService.push(config);  
        CConfigManager.PushBuildingConfig2Array( aryService, config );

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "xuexiao_png";
        config.szName = "学校";
       // aryService.push(config);  
       CConfigManager.PushBuildingConfig2Array( aryService, config );

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "youju_png";
        config.szName = "邮局";
       // aryService.push(config); 
       CConfigManager.PushBuildingConfig2Array( aryService, config ); 

        /*
        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "huodianchang_png";
        config.szName = "火电厂";
        aryService.push(config);  
        */


        CConfigManager.s_dicBuildings[Global.eLotPsroperty.service] = aryService;

        //// end 普通建筑 ==================================================

        //// ================================== 以下为“特殊建筑” ==========================================

        //// Service
        var aryService_Special:Array<CConfigBuilding> = new Array<CConfigBuilding>();

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.service;
        config.szResName = "huodianchang_png";
        config.szName = "火电厂";
        config.fGainRate = 1;
        config.nPrice = 3;
        config.bSpecial = true;

   
        CConfigManager.PushBuildingConfig2Array( aryService_Special, config ); 

        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.service] = aryService_Special;

        //// Business
        var aryBusiness_Special:Array<CConfigBuilding> = new Array<CConfigBuilding>();

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.business;
        config.szResName = "shaoshi_png";
        config.szName = "超市";
        config.fGainRate = 1;
        config.nPrice = 3;
        config.bSpecial = true;
 

 CConfigManager.PushBuildingConfig2Array( aryBusiness_Special, config ); 
        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.business] = aryBusiness_Special;

        //// Residential
        var aryResidential_Special:Array<CConfigBuilding> = new Array<CConfigBuilding>();

        config = new CConfigBuilding();
        config.ePropertyType = Global.eLotPsroperty.residential;
        config.szResName = "bieshu_png";
        config.szName = "别墅";
        config.fGainRate = 1;
        config.nPrice = 3;
        config.bSpecial = true;
  
CConfigManager.PushBuildingConfig2Array( aryResidential_Special, config ); 
        CConfigManager.s_dicSpecialBuildings[Global.eLotPsroperty.residential] = aryResidential_Special;



    }

    public static GetBuildingConfigDic():Object
    {
        return CConfigManager.s_dicBuildings;
    }

    public static GetSpecialBuildingConfigDic():Object
    {
        return CConfigManager.s_dicSpecialBuildings;
    }

    public static GetBuildingConfig( eType:Global.eLotPsroperty, nIndex:number ):CConfigBuilding
    {
        var ary:Array<CConfigBuilding> = CConfigManager.s_dicBuildings[eType];
        if ( ary == undefined || ary == null )
        {
            return null;
        }
        if ( ary.length == 0 )
        {
            return;
        }
        if ( nIndex >= ary.length )
        {
            nIndex = nIndex % ary.length;
        }
        return ary[nIndex];
    }

    //// end 建筑物相关
} // end class