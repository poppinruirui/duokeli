class CJinBi extends CObj {

      protected m_bmpMainPic:egret.Bitmap;
      protected m_BoundObj:CObj = null;

      protected m_nCurFrameIndex:number = 0;
      protected m_fAniTimeElapse:number = 0;

      protected m_eType:Global.eMoneySubType = Global.eMoneySubType.small_coin;
      protected m_nValue:number = 0;

     public constructor() {
        super();

        this.m_bmpMainPic = new egret.Bitmap( );
        this.addChild( this.m_bmpMainPic );

        this.touchEnabled = true;
        this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapJinBi, this);
    } //  end constructor

    public SetValue( eType: Global.eMoneySubType, nValue:number ):void
    {
        this.m_eType = eType;
        this.m_nValue = nValue;
    }

    public OnClick():void
    {
        var tiaozi:CTiaoZi = CTiaoZiManager.NewTiaoZi();
        tiaozi.BeginTiaoZi();
        tiaozi.x = this.GetBoundObj().x + 200;
        tiaozi.y = this.GetBoundObj().y - 150;
        tiaozi.SetTypeAndValue( this.m_eType, this.m_nValue );
        tiaozi.scaleX = 2;
        tiaozi.scaleY = 2;
        Main.s_CurTown.SetCoins( Main.s_CurTown.GetCoins() + this.m_nValue );

        (this.m_BoundObj as CAutomobile).ClearBoundJinBi();
       CJinBiManager.DeleteJinBi( this );

        CSoundManager.PlaySE( Global.eSE.small_coin );
    }

    public onTapJinBi(e: egret.TouchEvent):void{
       this.OnClick();
    }

    public BeginTiaoZi():void
    {

    }

    public SetBoundObj( obj:CObj ):void
    {
        this.m_BoundObj = obj;
    }

    public GetBoundObj( ):CObj
    {
        return this.m_BoundObj;
    }

   

    public UpdateJinBiAnimation():void
    {
        this.m_fAniTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_fAniTimeElapse < CDef.s_fJinBiAniInterval )
        {
            return;
        }
        this.m_fAniTimeElapse = 0;

        var nRealResIndex:number = this.m_nCurFrameIndex;
        if ( this.m_nCurFrameIndex == 2 )
        {
            nRealResIndex = 3;
        }
        if ( this.m_nCurFrameIndex == 3  )
        {
            nRealResIndex = 2;
        }
         if ( this.m_nCurFrameIndex == 4  )
        {
            nRealResIndex = 3;
        }
        this.m_bmpMainPic.texture = CJinBiManager.GetFrameTexture(nRealResIndex);
        
        this.m_nCurFrameIndex++
        if ( this.m_nCurFrameIndex >= CDef.s_fJinBiAniFrameNum )
        {
            this.m_nCurFrameIndex = 0;
        }

        this.anchorOffsetX = this.width * 0.5;
            this.anchorOffsetY = this.height;
    }

    public  Update():void
    {
        this.UpdateJinBiAnimation();
        //this.UpdatePos();
        
    }

} // end class