class CConfigBuilding extends egret.DisplayObjectContainer {

     public ePropertyType:Global.eLotPsroperty = Global.eLotPsroperty.business;
     public nID:number = 0;
     public szDesc:string = "";
     public szName:string = "";
     public nPrice:number = 0;
     public szResName:string = "";
     public bUnLocked:boolean = true;
     public fGainRate:number = 0; // 增益率
     public bSpecial:boolean = false;
     public bUsed:boolean = false; // 已使用（仅针对特殊建筑）

     public constructor() {
         super();

     } // end constructor

} // end class