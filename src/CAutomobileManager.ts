
class CAutomobileManager extends egret.DisplayObjectContainer {

     public constructor() {
        super();

        
    }

    public static NewAutomobile():CAutomobile
    {
        return new CAutomobile();
    }

    public static ChangeCarSpeedDueToDoubleTime( nDoubleTime:number ):void
    {
        CDef.s_fAutomobileMoveDirX *=  nDoubleTime;
        CDef.s_fAutomobileMoveDirY *=  nDoubleTime;
    }

} // end classs