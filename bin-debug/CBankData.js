var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CBankData = (function (_super) {
    __extends(CBankData, _super);
    function CBankData() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.nCurLevel = 1;
        _this.nCurAvailableTime = 0;
        _this.nCurProfitRate = 0;
        return _this;
    }
    CBankData.prototype.GetCurLevel = function () {
        return this.nCurLevel;
    };
    CBankData.prototype.SetCurLevel = function (nLevel) {
        this.nCurLevel = nLevel;
        // poppin to do 还有CityHall_Game中的加成，稍后做
        this.nCurAvailableTime = CConfigManager.GetBankTimeChangeAmount() * this.nCurLevel;
    };
    return CBankData;
}(egret.DisplayObjectContainer)); // end class
__reflect(CBankData.prototype, "CBankData");
//# sourceMappingURL=CBankData.js.map