var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
 音频系统
*/
var Global;
(function (Global) {
    var eSE;
    (function (eSE) {
        eSE[eSE["bank"] = 0] = "bank";
        eSE[eSE["big_coin"] = 1] = "big_coin";
        eSE[eSE["car"] = 2] = "car";
        eSE[eSE["click_button"] = 3] = "click_button";
        eSE[eSE["common_page_1"] = 4] = "common_page_1";
        eSE[eSE["common_page_2"] = 5] = "common_page_2";
        eSE[eSE["green_bux"] = 6] = "green_bux";
        eSE[eSE["small_coin"] = 7] = "small_coin";
        eSE[eSE["congratulations"] = 8] = "congratulations";
        eSE[eSE["num"] = 9] = "num";
    })(eSE = Global.eSE || (Global.eSE = {}));
})(Global || (Global = {})); // end module Global
var CSoundManager = (function (_super) {
    __extends(CSoundManager, _super);
    function CSoundManager() {
        return _super.call(this) || this;
    }
    CSoundManager.Init = function () {
        CSoundManager.s_soundBMG = new egret.Sound();
        CSoundManager.s_soundBMG.addEventListener(egret.Event.COMPLETE, function loadOver(event) {
        }, this);
        CSoundManager.s_soundBMG.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event) {
            console.log("loaded bmg error!");
        }, this);
        CSoundManager.s_soundBMG.load("resource/assets/Music/duokeli.mp3");
        CSoundManager.s_arySE = new Array();
        var sound = new egret.Sound();
        sound.load("resource/assets/Audios/bank.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/big_coin.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/car.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/click_button.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_1.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_2.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/green_bux.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/small_coin.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/congratulations.mp3");
        CSoundManager.s_arySE.push(sound);
    };
    CSoundManager.PlaySE = function (eId) {
        CSoundManager.s_arySE[eId].play(0, 1);
    };
    CSoundManager.PlayBMG = function () {
        if (CSoundManager.s_bBMGPlaying) {
            return;
        }
        CSoundManager.s_soundBMG.play();
        CSoundManager.s_bBMGPlaying = true;
    };
    CSoundManager.s_bBMGPlaying = false;
    return CSoundManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CSoundManager.prototype, "CSoundManager");
//# sourceMappingURL=CSoundManager.js.map