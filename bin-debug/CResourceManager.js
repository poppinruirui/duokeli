var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CResourceManager = (function (_super) {
    __extends(CResourceManager, _super);
    function CResourceManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CResourceManager.NewConstructingAni = function () {
        var ani = null;
        if (CResourceManager.s_containerRecycledConstructingAni.numChildren > 0) {
            ani = CResourceManager.s_containerRecycledConstructingAni.getChildAt(0);
            CResourceManager.s_containerRecycledConstructingAni.removeChildAt(0);
            ani.visible = true;
        }
        else {
            ani = new CConstructingAnimation();
        }
        return ani;
    };
    CResourceManager.DeleteConstructingAni = function (ani) {
        ani.visible = false;
        CResourceManager.s_containerRecycledConstructingAni.addChild(ani);
    };
    CResourceManager.FixedUpdate = function () {
    };
    CResourceManager.Init = function () {
        CResourceManager.s_bmpSegTop = new egret.Bitmap(RES.getRes("Hurdle_Top_png"));
        CResourceManager.s_bmpSeg = new egret.Bitmap(RES.getRes("Hurdle_Seg_png"));
    };
    CResourceManager.NewProgressBar = function () {
        var progressBar = null;
        if (CResourceManager.s_containerRecycledProgressBar.numChildren > 0) {
            progressBar = CResourceManager.s_containerRecycledProgressBar.getChildAt(0);
            CResourceManager.s_containerRecycledProgressBar.removeChildAt(0);
        }
        else {
            progressBar = new CUIBaseProgressBar("resource/assets/MyExml/CProgressBar.exml");
        }
        return progressBar;
    };
    CResourceManager.DeleteProgressBar = function (progressBar) {
        CResourceManager.s_containerRecycledProgressBar.addChild(progressBar);
    };
    CResourceManager.NewDiBiao = function () {
        var dibiao = null;
        if (CResourceManager.s_containerRecycledDiBiao.numChildren > 0) {
            dibiao = CResourceManager.s_containerRecycledDiBiao.getChildAt(0);
            CResourceManager.s_containerRecycledDiBiao.removeChildAt(0);
        }
        else {
            dibiao = new CObj();
        }
        return dibiao;
    };
    CResourceManager.DeleteDiBiao = function (dibiao) {
        CResourceManager.s_containerRecycledDiBiao.addChild(dibiao);
    };
    CResourceManager.NewObj = function () {
        var obj = null;
        if (CResourceManager.s_lstRecycledObjs.numChildren > 0) {
            obj = CResourceManager.s_lstRecycledObjs.getChildAt(0);
            CResourceManager.s_lstRecycledObjs.removeChildAt(0);
            obj.Reset();
        }
        else {
            obj = new CObj();
        }
        return obj;
    };
    CResourceManager.DeleteObj = function (obj) {
        CResourceManager.s_lstRecycledObjs.addChild(obj);
    };
    CResourceManager.GetDiBiaoTextureByTownId = function (nTownId) {
        var szResName = "dibiao_" + nTownId + "_png";
        console.log("szResName=" + szResName);
        return RES.getRes(szResName);
    };
    CResourceManager.GetTreeTextureByTownId = function (nTownId) {
        return RES.getRes("tree_" + nTownId + "_png");
    };
    CResourceManager.GetMoutainTextureByTownId = function (nTownId) {
        return RES.getRes("moutain_" + nTownId + "_png");
    };
    CResourceManager.GetBubbleTextureByTownId = function (nTownId) {
        return RES.getRes("bubble_" + nTownId + "_png");
    };
    CResourceManager.GetCloudTextureByTownId = function (nTownId, nCloudSubId) {
        if (nCloudSubId === void 0) { nCloudSubId = 0; }
        if (nCloudSubId == 0) {
            return RES.getRes("cloud_" + nTownId + "_png");
        }
        else {
            return RES.getRes("cloud_" + nTownId + "_" + nCloudSubId + "_png");
        }
    };
    CResourceManager.GetNumberTexture = function (nTownId) {
        return RES.getRes(nTownId + "_png");
    };
    CResourceManager.s_containerRecycledConstructingAni = new egret.DisplayObjectContainer();
    CResourceManager.s_bmpSegTop = null;
    CResourceManager.s_bmpSeg = null;
    CResourceManager.s_containerRecycledProgressBar = new eui.Component();
    CResourceManager.s_containerRecycledDiBiao = new egret.DisplayObjectContainer();
    /////////////////////////
    CResourceManager.s_lstRecycledObjs = new egret.DisplayObjectContainer();
    return CResourceManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CResourceManager.prototype, "CResourceManager");
//# sourceMappingURL=CResourceManager.js.map