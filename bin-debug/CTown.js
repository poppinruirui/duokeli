var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CTown = (function (_super) {
    __extends(CTown, _super);
    function CTown(nTownId, nStageWidth, nStageHeight) {
        var _this = _super.call(this) || this;
        _this.m_nStageWidth = 0;
        _this.m_nStageHeight = 0;
        _this.m_containerGrids = new egret.DisplayObjectContainer();
        _this.m_containerObjs = new egret.DisplayObjectContainer();
        _this.m_containerRoads = new egret.DisplayObjectContainer();
        _this.m_containerTrees = new egret.DisplayObjectContainer();
        _this.m_containerTurbine = new egret.DisplayObjectContainer();
        _this.m_containerProgressbar = new egret.DisplayObjectContainer();
        _this.m_containerDiBiao = new egret.DisplayObjectContainer();
        _this.m_CloudManager = null;
        _this.m_dicFunctionalBuildings = new Object();
        // 临时容器。正式版汽车和建筑必须放在同一个显示容器中，不然无法做遮挡排序
        _this.m_containerAutomobiles = new egret.DisplayObjectContainer();
        _this.m_aryAutomobiles = new Array();
        _this.m_dicObjContainer = new Object();
        _this.m_dicGrids = new Object();
        _this.m_dicRectsForTiles = new Object();
        _this.m_aryAllGrids = new Array();
        _this.m_szName = "";
        _this.m_objCurProcessingLot = null;
        _this.m_dicEmptyLotNum = new Object();
        _this.m_dicBulildLotNum = new Object(); // 不包括 市政厅、车站 等非自建建筑
        _this.m_dicPropertyPercent = new Object(); // 各种性质（住宅、商业、服务）的地块各自占的比例
        _this.m_nShowCoinChangeSpeed = 0;
        _this.s_nCPS = 0; // “Coin per Second”每秒产出的金币总数
        _this.s_nCPS_WithOutDoubleTime = 0; // “Coin per Second”每秒产出的金币总数, 不含DoubleTime加成
        _this.s_nCoins = 0; // 当前金币数量
        _this.s_nPopulation = 0; // 当前人口
        _this.m_nCurCPSableBuildingNum = 0; // 当前能产金币的建筑物个数
        //// 当前各种因素的金币产出率
        _this.m_nBuildingCPS = 0; // 建筑的金币产出率
        _this.m_nPopulationCPS = 0; // 人口的金币产出率
        _this.m_dicGarageInfo = new Object();
        _this.m_aryUpgradableBuildings = new Array();
        _this.m_nAutoUpgradeIndex = 0;
        _this.m_objUpgradingLot = null;
        _this.m_dicCityHallData_City = new Object();
        _this.m_dicCityHallData_Game = new Object();
        _this.m_dicCityHallItemAffect = new Object();
        _this.m_BankData = new CBankData();
        _this.m_nID = 1;
        _this.m_objTapTownPos = new Object();
        _this.m_aryNeighbourGrids = new Array();
        _this.m_lstNeighbourGrids = new egret.DisplayObjectContainer();
        _this.m_nGridGuid = 0;
        _this.m_bTapDown = false;
        _this.m_fTapBeginMouseX = 0;
        _this.m_fTapBeginMouseY = 0;
        // 添加一个汽车
        _this.m_aryTempRaods = new Array();
        _this.m_fWindTimeElapse = 0;
        _this.m_fBuildingLandLoopTimeElapse = 0;
        _this.m_aryEmptyGrid = new Array();
        _this.m_nDeavtovateTime = 0;
        _this.m_nBankSaving = 0;
        _this.m_bShowGrid = true;
        _this.m_nID = nTownId;
        _this.m_nStageWidth = nStageWidth;
        _this.m_nStageHeight = nStageHeight;
        // obj的容器
        _this.m_dicObjContainer = new Object();
        for (var i = 0; i < Global.eOperate.num; i++) {
            _this.m_dicObjContainer[i] = new Array();
        } // end i
        /*
        this.m_dicObjContainer[Global.eOperate.add_road_corner_shang] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_corner_xia] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_corner_you] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_corner_zuo] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_youshangzuoxia] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_road_zuoshangyouxia] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_tile_2x2] = new Array<string>();
        this.m_dicObjContainer[Global.eOperate.add_tile_4x4] = new Array<string>();
        */
        _this.scaleX = 0.5;
        _this.scaleY = 0.5;
        _this.x = 321;
        _this.y = 460;
        _this.addChild(_this.m_containerGrids);
        _this.addChild(_this.m_containerDiBiao);
        _this.addChild(_this.m_containerRoads);
        _this.addChild(_this.m_containerTrees);
        _this.addChild(_this.m_containerObjs);
        _this.addChild(_this.m_containerTurbine);
        //this.addChild( this.m_containerAutomobiles );
        _this.CreateGrids();
        _this.addChild(CJinBiManager.s_containerJinBi);
        _this.addChild(CTiaoZiManager.s_lstcontainerTiaoZi);
        _this.addChild(_this.m_containerProgressbar);
        return _this;
    } // end constructor
    CTown.prototype.AddProgressBar = function (bar) {
        this.m_containerProgressbar.addChild(bar);
    };
    CTown.prototype.Reset = function () {
        this.m_aryUpgradableBuildings.length = 0;
        this.m_nAutoUpgradeIndex = 0;
        this.m_objUpgradingLot = null;
    };
    CTown.prototype.GetId = function () {
        return this.m_nID;
    };
    CTown.prototype.Init = function (nTownId) {
        this.m_nID = nTownId;
        var config = CConfigManager.GetCityLevelConfig(Main.s_CurTown.GetLevel());
        Main.s_CurTown.SetCoins(config.nStartCoins, true);
        CUIManager.s_uiMainTitle.SetCityName(config.szName);
        this.m_CloudManager.InitClouds(this.m_nID, this.m_nStageWidth, this.m_nStageHeight);
        // 初始化汽车的信息
        for (var i = 0; i < CConfigManager.s_nTotalCarNum; i++) {
            this.m_dicGarageInfo[i] = Global.eGarageItemStatus.locked_and_can_unlock;
        }
        // 初始化银行信息    
        this.m_BankData.SetCurLevel(1);
        this.m_BankData.nCurProfitRate = CConfigManager.GetBankProfitRate();
        // poppin to do
        // CityHall中Game页第一条可以对银行的时间进行加成
    };
    CTown.prototype.GetBankData = function () {
        return this.m_BankData;
    };
    CTown.prototype.LoadTownData = function (nTownId) {
        this.m_nID = nTownId;
        CUIManager.s_uiMainTitle.SetCityId(this.m_nID);
        var szData = egret.localStorage.getItem("Town" + nTownId);
        //var szData:string = CDataManager.GetMapData(nTownId - 1);
        console.log("读档：" + szData);
        if (szData == null) {
            return;
        }
        var aryData = szData.split('|');
        for (var i = 0; i < aryData.length; i++) {
            if (aryData[i] == "") {
                continue;
            }
            var op = i;
            var szGridIds = aryData[i];
            /*
                    if ( op == Global.eOperate.set_inside_empty_grid || op == Global.eOperate.set_edge_empty_grid )
                    {
                            this.LoadData_EmptyGridType( op, szGridIds);
                            continue;
                    }
                    */
            var aryGridIs = szGridIds.split(',');
            for (var j = 0; j < aryGridIs.length; j++) {
                switch (op) {
                    case Global.eOperate.add_tree:
                        {
                            var aryParams = aryGridIs[j].split('_');
                            var nTreeResId = (Number)(aryParams[0]);
                            var fPosInTownX = (Number)(aryParams[1]);
                            var fPosInTownY = (Number)(aryParams[2]);
                            this.DoAddTree(nTreeResId, fPosInTownX, fPosInTownY);
                        }
                        break;
                    case Global.eOperate.add_windturbine:
                        {
                            var aryParams = aryGridIs[j].split('_');
                            var fPosInTownX = (Number)(aryParams[0]);
                            var fPosInTownY = (Number)(aryParams[1]);
                            this.DoAddWindTurbine(fPosInTownX, fPosInTownY);
                        }
                        break;
                    default:
                        {
                            var aryGridIndex = aryGridIs[j].split('_');
                            var szIndexX = aryGridIndex[0];
                            var szIndexY = aryGridIndex[1];
                            var grid = this.GetGridByIndex(szIndexX, szIndexY);
                            this.DoAddObj(grid, op);
                            if (op == 5) {
                                console.log("我日你个龟");
                            }
                        }
                        break;
                }
            } // end j
        } // end i
        //  this.GenerateGrass();
        this.Init(nTownId);
    };
    CTown.prototype.LoadData_EmptyGridType = function (op, szGridIds) {
        var aryGridIs = szGridIds.split(',');
        for (var j = 0; j < aryGridIs.length; j++) {
            var aryGridIndex = aryGridIs[j].split('_');
            var szIndexX = aryGridIndex[0];
            var szIndexY = aryGridIndex[1];
            var grid = this.GetGridByIndex(szIndexX, szIndexY);
            /*
               if ( op == Global.eOperate.set_inside_empty_grid )
               {

                       grid.SetEmptyGridType( Global.eEmptyGridType.inside_city );
               }else if ( op == Global.eOperate.set_edge_empty_grid)
               {
                       
                       grid.SetEmptyGridType( Global.eEmptyGridType.city_edge );
               }
               */
        } // end j 
    };
    CTown.prototype.SeekNeighbourGrids = function (nIndexX, nIndexY, nHierarchy) {
        this.m_aryNeighbourGrids.length = 0;
        var bExceedTownBorder = false;
        for (var k = 0; k >= -nHierarchy; k--) {
            for (var l = 0; l <= nHierarchy; l++) {
                var nNeighbourIndexX = nIndexX + k + l;
                var nNeighbourIndexY = nIndexY + k - l;
                // console.log( "seek neighbour:" +  nNeighbourIndexX + "_" + nNeighbourIndexY);
                var grid_neighbor = this.GetGridByIndex(nNeighbourIndexX.toString(), nNeighbourIndexY.toString());
                if (grid_neighbor == undefined) {
                    console.log(nNeighbourIndexX + "," + nNeighbourIndexY + "在边界外");
                    return false;
                }
                else {
                    this.m_aryNeighbourGrids.push(nNeighbourIndexX);
                    this.m_aryNeighbourGrids.push(nNeighbourIndexY);
                }
            } // end l
        } // end k
        return true;
    };
    CTown.prototype.EditObj = function (stageX, stageY) {
    };
    CTown.prototype.DelObj = function (obj) {
        if (obj.GetFuncType() == Global.eObjFunc.road) {
            this.m_containerRoads.removeChild(obj);
        }
        else {
            this.m_containerObjs.removeChild(obj);
        }
        CObjManager.DeleteObj(obj);
    };
    CTown.prototype.DoAddWindTurbine = function (fPosInTownX, fPosInTownY) {
        var turbine = new CWindTurbine();
        this.m_containerTurbine.addChild(turbine);
        turbine.anchorOffsetX = turbine.width / 2;
        turbine.anchorOffsetY = turbine.height;
        turbine.x = fPosInTownX;
        turbine.y = fPosInTownY;
        turbine.touchEnabled = true;
        turbine.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapTurbine, this);
    };
    CTown.prototype.DoAddTree = function (nTreeId, fPosInTownX, fPosInTownY) {
        var tree = CResourceManager.NewObj();
        this.m_containerTrees.addChild(tree);
        tree.x = fPosInTownX;
        tree.y = fPosInTownY;
        tree.SetTexture(CResourceManager.GetTreeTextureByTownId(this.m_nID) /*RES.getRes( "shu_png" )*/);
        tree.anchorOffsetX = tree.width / 2;
        tree.anchorOffsetY = tree.height;
        tree.scaleX = 0.5;
        tree.scaleY = 0.5;
    };
    CTown.prototype.DoAddObj = function (grid, op) {
        var szResName = "";
        var nIndexX = grid.GetId()["nIndexX"];
        var nIndexY = grid.GetId()["nIndexY"];
        // console.log( "点中了：" + nIndexX + "_" + nIndexY );
        var bCanAddObj = true;
        switch (op) {
            case Global.eOperate.add_cityhall: // 放置“市政厅”
            case Global.eOperate.add_bank: // 放置“银行”
            case Global.eOperate.add_garage:// 放置“车站”
                {
                    var objFunctionalBuilding = this.m_dicFunctionalBuildings[op];
                    if (objFunctionalBuilding == null || objFunctionalBuilding == undefined) {
                        objFunctionalBuilding = CResourceManager.NewObj();
                    }
                    objFunctionalBuilding.SetLocationGrid(grid);
                    objFunctionalBuilding.SetBoundGrid(grid);
                    objFunctionalBuilding.SetFuncType(Global.eObjFunc.building);
                    objFunctionalBuilding.SetOperateType(op);
                    objFunctionalBuilding.SetSizeType(Global.eObjSize.size_2x2);
                    objFunctionalBuilding.SetSortPosY();
                    //      console.log( op+ "__" + objFunctionalBuilding.GetSortPosY() + "__" + grid.GetPos()["y"] );
                    this.InsertObj(objFunctionalBuilding);
                    var bRet = objFunctionalBuilding.ProcessAddSpecialBuilding(op);
                    this.m_dicFunctionalBuildings[op] = objFunctionalBuilding;
                    objFunctionalBuilding.SetPos(grid.GetPos()["x"], grid.GetPos()["y"] + CDef.s_nTileHeight * 0.5);
                    grid.SetBoundObj(objFunctionalBuilding);
                }
                break;
            case Global.eOperate.add_road_youshangzuoxia:
            case Global.eOperate.add_road_zuoshangyouxia:
            case Global.eOperate.add_road_corner_shang:
            case Global.eOperate.add_road_corner_xia:
            case Global.eOperate.add_road_corner_you:
            case Global.eOperate.add_road_corner_zuo:
            case Global.eOperate.add_road_cross:
            case Global.eOperate.add_road_t_youshang:
            case Global.eOperate.add_road_t_youxia:
            case Global.eOperate.add_road_t_zuoshang:
            case Global.eOperate.add_road_t_zuoxia:
                {
                    if (op == Global.eOperate.add_road_zuoshangyouxia ||
                        op == Global.eOperate.add_road_youshangzuoxia) {
                        //   szResName = "malu_zuoshangyouxia_png";  
                        if (this.GetId() == 2) {
                            szResName = "road2_youshangzuoxia_png";
                        }
                        else {
                            szResName = "road1_youshangzuoxia_png";
                        }
                    }
                    if (op == Global.eOperate.add_road_corner_shang ||
                        op == Global.eOperate.add_road_corner_xia) {
                        //    szResName = "malu_chuizhizhuanzhe_png";  
                        if (this.GetId() == 2) {
                            szResName = "road2_corner_shang_png";
                        }
                        else {
                            szResName = "road1_corner_shang_png";
                        }
                    }
                    /*
                    if (  op == Global.eOperate.add_road_corner_xia)
                    {
                            szResName = "malu_chuizhizhuanzhe_png";
                    }
                    */
                    if (op == Global.eOperate.add_road_corner_zuo ||
                        op == Global.eOperate.add_road_corner_you) {
                        //szResName = "malu_shuipingzhuanzhe_png";  
                        if (this.GetId() == 2) {
                            szResName = "road2_corner_you_png";
                        }
                        else {
                            szResName = "road1_corner_you_png";
                        }
                    }
                    if (op == Global.eOperate.add_road_cross) {
                        szResName = "malu_zuoshangyouxia_png";
                    }
                    if (op == Global.eOperate.add_road_t_youshang ||
                        op == Global.eOperate.add_road_t_zuoshang) {
                        szResName = "malu_zuoshangyouxia_png";
                    }
                    if (op == Global.eOperate.add_road_t_youxia ||
                        op == Global.eOperate.add_road_t_zuoxia) {
                        szResName = "malu_zuoshangyouxia_png";
                    }
                    var dic = grid.GetPos();
                    var obj = CObjManager.NewObjByResName(szResName);
                    if (this.GetId() == 2) {
                        obj.SetColor(144, 102, 85);
                    }
                    else {
                        obj.SetColor(174, 170, 184);
                    }
                    obj.anchorOffsetX = obj.width / 2;
                    obj.anchorOffsetY = obj.height / 2;
                    obj.SetLocationGrid(grid);
                    // this.InsertObj( obj, op );
                    this.m_containerRoads.addChild(obj);
                    obj.SetOperateType(op);
                    obj.SetPos(dic["x"], dic["y"] /* - (obj.height * 0.25)*/);
                    obj.SetTown(this);
                    obj.SetFuncType(Global.eObjFunc.road);
                    obj.scaleX = 0.5;
                    obj.scaleY = 0.5;
                    if (op == Global.eOperate.add_road_zuoshangyouxia) {
                        obj.scaleX = -0.5;
                    }
                    else if (op == Global.eOperate.add_road_corner_zuo) {
                        obj.scaleX = -0.5;
                    }
                    else if (op == Global.eOperate.add_road_corner_xia) {
                        obj.scaleY = -0.5;
                    }
                    else if (op == Global.eOperate.add_road_t_zuoshang ||
                        op == Global.eOperate.add_road_t_zuoxia) {
                        obj.scaleX = -0.5;
                    }
                    grid.ClearBoundObj(); // 如果格子上当前已经摆放了物件，则自动清除当前物件
                    grid.SetBoundObj(obj);
                    obj.SetBoundGrid(grid);
                    obj.SetSizeType(Global.eObjSize.size_1x1);
                    obj.SetFuncType(Global.eObjFunc.road);
                    obj.SetSortPosY();
                }
                break;
            case Global.eOperate.add_dibiao:// 地表
                {
                    var dibiao = CResourceManager.NewDiBiao();
                    dibiao.SetTexture(CResourceManager.GetDiBiaoTextureByTownId(this.GetId()));
                    dibiao.anchorOffsetX = dibiao.width / 2;
                    dibiao.anchorOffsetY = 0;
                    dibiao.SetPos(grid.GetPos()["x"], grid.GetPos()["y"]);
                    this.InsertDiBiao(dibiao);
                    dibiao.SetLocationGrid(grid);
                    if (CDef.EDITOR_MODE) {
                        dibiao.touchEnabled = true;
                        dibiao.addEventListener(egret.TouchEvent.TOUCH_TAP, this.handleTapDibiao, this);
                    }
                }
                break;
            case Global.eOperate.add_tile_2x2: // 2x2型宗地
            case Global.eOperate.add_tile_4x4:// 4x4型宗地
                {
                    var nHierarchy = 1;
                    if (op == Global.eOperate.add_tile_2x2) {
                        szResName = "dikuai_png";
                        nHierarchy = 1;
                    }
                    else if (op == Global.eOperate.add_tile_4x4) {
                        szResName = "dikuai_png";
                        nHierarchy = 3;
                    }
                    bCanAddObj = this.SeekNeighbourGrids(nIndexX, nIndexY, nHierarchy);
                    if (!bCanAddObj) {
                        Main.s_panelMsgBox.ShowMsg("占地超过边界，不能放置");
                        return;
                    }
                    var dic = grid.GetPos();
                    var obj = CObjManager.NewObjByResName(szResName);
                    if (op == Global.eOperate.add_tile_2x2) {
                        obj.scaleX = 0.5;
                        obj.scaleY = 0.5;
                    }
                    obj.anchorOffsetX = obj.width / 2;
                    obj.anchorOffsetY = obj.height;
                    obj.SetLocationGrid(grid);
                    obj.SetOperateType(op);
                    obj.SetPos(dic["x"], dic["y"] + CDef.s_nTileHeight * 0.5);
                    obj.SetTown(this);
                    obj.SetFuncType(Global.eObjFunc.building);
                    obj.SetBuildingLandStatus(Global.eBuildingLandStatus.empty);
                    //  obj.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapBuildingLand, this );
                    if (op == Global.eOperate.add_tile_2x2) {
                        obj.SetSizeType(Global.eObjSize.size_2x2);
                    }
                    else if (op == Global.eOperate.add_tile_4x4) {
                        obj.SetSizeType(Global.eObjSize.size_4x4);
                    }
                    obj.SetFuncType(Global.eObjFunc.building);
                    obj.SetSortPosY();
                    this.InsertObj(obj);
                    for (var k = 0; k < this.m_aryNeighbourGrids.length - 1; k += 2) {
                        var related_grid = this.GetGridByIndex(this.m_aryNeighbourGrids[k].toString(), this.m_aryNeighbourGrids[k + 1].toString());
                        if (related_grid == null || related_grid == undefined) {
                            //  console.log( "有Bug" );
                            continue;
                        }
                        // 物件和格子互相绑定
                        related_grid.ClearBoundObj(); // 如果格子上当前已经摆放了物件，则自动清除当前物件
                        related_grid.SetBoundObj(obj);
                        obj.SetBoundGrid(related_grid);
                    }
                    //  console.log( "当前回收的obj:" + CObjManager.m_lstRecycledObjs.numChildren );
                }
                break;
        } // end switch
        return;
    };
    CTown.prototype.AddObj = function (grid, op) {
        this.m_lstNeighbourGrids.removeChildren();
        if (grid == null) {
            return;
        }
        this.DoAddObj(grid, op);
    };
    CTown.prototype.GetOperate = function () {
        return this.m_eCurOperate;
    };
    CTown.prototype.SetOperate = function (eOperate) {
        this.m_eCurOperate = eOperate;
        if (this.m_eCurOperate == Global.eOperate.edit_obj) {
            //  Main.s_panelObjEditPanel.Show();
        }
        else if (this.m_eCurOperate == Global.eOperate.del_obj) {
            //  Main.s_panelObjEditPanel.Show();
        }
        else if (this.m_eCurOperate == Global.eOperate.add_diamond) {
            CPlayer.SetDiamond(CPlayer.GetDiamond() + 1);
        }
        else if (this.m_eCurOperate == Global.eOperate.add_coin) {
            CPlayer.SetMoney(Global.eMoneyType.coin, CPlayer.GetMoney(Global.eMoneyType.coin) + 10000);
        }
        else if (this.m_eCurOperate == Global.eOperate.add_dibiao) {
        }
        else if (this.m_eCurOperate == Global.eOperate.toggle_grid) {
            this.ToggleGrids();
        }
        else {
            // this.AddObj( this.stage.stagx, this.stage.y,  eOperate);
        }
    };
    CTown.prototype.Processtap = function (grid, fPosInTownX, fPosInTownY) {
        if (this.m_eCurOperate == Global.eOperate.edit_obj) {
            //this.EditObj( stageX, stageY );
        }
        else if (this.m_eCurOperate == Global.eOperate.add_tree) {
            this.DoAddTree(0, fPosInTownX, fPosInTownY);
        }
        else if (this.m_eCurOperate == Global.eOperate.del_obj) {
            // to do
        }
        else if (this.m_eCurOperate == Global.eOperate.add_windturbine) {
            this.DoAddWindTurbine(fPosInTownX, fPosInTownY);
            /*
         var turbine:CWindTurbine = new CWindTurbine();
         this.m_containerTurbine.addChild( turbine );
         turbine.anchorOffsetX = turbine.width / 2;
         turbine.anchorOffsetY = turbine.height;
         var objPosInTown:Object = this.StagePos2TownPos( stageX, stageY );
         turbine.x = objPosInTown["x"];
         turbine.y = objPosInTown["y"];
         turbine.touchEnabled = true;
         turbine.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapTurbine, this );
         */
        }
        else {
            this.AddObj(grid, this.m_eCurOperate);
        }
    };
    CTown.prototype.CreateGrids = function () {
        var nWidth = CDef.s_nTileWidth;
        var nHeight = CDef.s_nTileHeight;
        var nHalfWidth = CDef.s_nTileWidth / 2;
        var nHalfHeight = CDef.s_nTileHeight / 2;
        // 规定：偶数行的列号都是偶数；奇数行的列号都是奇数
        for (var i = -CDef.s_nGridNumY; i <= CDef.s_nGridNumY; i++) {
            var nShit = Math.abs(i);
            for (var j = -CDef.s_nGridNumX + nShit; j <= CDef.s_nGridNumX - nShit; j++) {
                var posX = 0;
                var posY = 0;
                if (i % 2 == 0) {
                    if (j % 2 != 0) {
                        continue;
                    }
                    posX = (j / 2) * nWidth;
                    posY = i * nHalfHeight;
                }
                else {
                    if (j % 2 == 0) {
                        continue;
                    }
                    posX = (j / 2) * nWidth;
                    posY = i * nHalfHeight;
                }
                var grid = new CGrid();
                grid.SetGuid(this.m_nGridGuid++);
                this.m_containerGrids.addChild(grid);
                grid.touchEnabled = true;
                grid.anchorOffsetX = nHalfWidth;
                grid.anchorOffsetY = nHalfHeight; //;CDef.s_nTileHeight;
                grid.DrawRhombus(CDef.s_nTileWidth, CDef.s_nTileHeight, nHalfWidth, nHalfHeight);
                //grid.x = posX;
                //grid.y = posY;
                grid.SetPos(posX, posY);
                // j是x轴坐标，i是y轴坐标
                var szKey = j + "," + i;
                grid.SetText(szKey);
                grid.SetId(j, i);
                this.m_dicGrids[szKey] = grid;
                this.m_aryAllGrids.push(grid);
                // grid.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapGrid_Begin, this );
                grid.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapGridDown, this);
                grid.addEventListener(egret.TouchEvent.TOUCH_END, this.handleTapGridUp, this);
                grid.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.handleTapGridMove, this);
            } // end for j
        } // end for i    
    }; // end CreateGrids
    CTown.prototype.handleTapGridDown = function () {
        this.m_bTapDown = true;
    };
    CTown.prototype.handleTapGridMove = function () {
        this.m_bTapDown = false;
    };
    CTown.prototype.handleTapGridUp = function (evt) {
        if (!this.m_bTapDown) {
            return;
        }
        var real_tap_grid = null;
        var grid = evt.currentTarget;
        var nIndexX = grid.GetId()["nIndexX"];
        var nIndexY = grid.GetId()["nIndexY"];
        var nIndexX_Neighbour = 0;
        var nIndexY_Neighbour = 0;
        var grid_neighbour = null;
        // 先判断是不是真的点中自己了。因为这种点击事件是按矩形包围盒来判断的，很容易点到旁边的物体
        var bHit = grid.hitTestPoint(evt.stageX, evt.stageY, true);
        if (!bHit) {
            // 判断是不是点中了其左上角的格子
            nIndexX_Neighbour = nIndexX - 1;
            nIndexY_Neighbour = nIndexY - 1;
            grid_neighbour = this.GetGridByIndex(nIndexX_Neighbour.toString(), nIndexY_Neighbour.toString());
            if (grid_neighbour == null || grid_neighbour == undefined) {
                bHit = false;
            }
            else {
                bHit = grid_neighbour.hitTestPoint(evt.stageX, evt.stageY, true);
                if (!bHit) {
                    grid_neighbour = null;
                    // 判断是不是点中了其右上角的格子
                    nIndexX_Neighbour = nIndexX + 1;
                    nIndexY_Neighbour = nIndexY - 1;
                    grid_neighbour = this.GetGridByIndex(nIndexX_Neighbour.toString(), nIndexY_Neighbour.toString());
                    if (grid_neighbour == null || grid_neighbour == undefined) {
                        bHit = false;
                    }
                    else {
                        bHit = grid_neighbour.hitTestPoint(evt.stageX, evt.stageY, true);
                        if (!bHit) {
                        }
                        else {
                            real_tap_grid = grid_neighbour;
                        }
                    }
                }
                else {
                    real_tap_grid = grid_neighbour;
                }
            }
        }
        else {
            real_tap_grid = grid;
        }
        if (real_tap_grid == null || real_tap_grid == undefined) {
            console.log("有bug，怎么会没点中格子");
            return;
        }
        real_tap_grid.Processtap();
    };
    CTown.prototype.GetGridByIndex = function (nIndexX, nIndexY) {
        var szKey = nIndexX + "," + nIndexY;
        var grid = this.m_dicGrids[szKey];
        if (grid == undefined) {
            grid = null;
        }
        return grid;
    };
    CTown.prototype.handleTapGrid_Begin = function (evt) {
        this.m_fTapBeginMouseX = evt.stageX;
        this.m_fTapBeginMouseY = evt.stageY;
    };
    CTown.prototype.ReInsertSortObj = function (obj) {
        this.m_containerObjs.removeChild(obj);
        this.InsertObj(obj);
    };
    CTown.prototype.InsertDiBiao = function (obj) {
        var bInserted = false;
        for (var i = this.m_containerDiBiao.numChildren - 1; i >= 0; i--) {
            var node_obj = this.m_containerDiBiao.getChildAt(i);
            if (obj.y >= node_obj.y) {
                this.m_containerDiBiao.addChildAt(obj, i + 1);
                bInserted = true;
                return;
            }
        }
        if (!bInserted) {
            this.m_containerDiBiao.addChildAt(obj, 0);
        }
    };
    // “插入排序法”和“二分查找法”叠加使用，使性能达到最高
    CTown.prototype.InsertObj = function (obj) {
        //  this.m_containerObjs.addChild(obj); 
        var bInserted = false;
        for (var i = this.m_containerObjs.numChildren - 1; i >= 0; i--) {
            var node_obj = this.m_containerObjs.getChildAt(i);
            if (obj.GetSortPosY() >= node_obj.GetSortPosY()) {
                this.m_containerObjs.addChildAt(obj, i + 1);
                obj.SetDebugInfo((i + 1).toString());
                bInserted = true;
                return;
            }
        }
        if (!bInserted) {
            this.m_containerObjs.addChildAt(obj, 0);
            obj.SetDebugInfo("0");
        }
    };
    CTown.prototype.DragScene = function (fDeltaX, fDeltaY) {
        if (isNaN(fDeltaX) || isNaN(fDeltaY)) {
            return;
        }
        if (Math.abs(fDeltaX) > 20 || Math.abs(fDeltaY) > 20) {
            return;
        }
        this.x += fDeltaX;
        this.y += fDeltaY;
    };
    CTown.prototype.GenerateData = function () {
        console.log("存档又有");
        var nLastIndex = Global.eOperate.num - 1;
        var szData = "";
        for (var i = 0; i <= nLastIndex; i++) {
            var ary = this.m_dicObjContainer[i];
            ary.length = 0;
            this.m_dicObjContainer[i] = ary;
        }
        //   var ary_edge:Array<string> = this.m_dicObjContainer[Global.eOperate.set_edge_empty_grid];
        //   var ary_inside:Array<string> = this.m_dicObjContainer[Global.eOperate.set_inside_empty_grid];
        for (var i = 0; i < this.m_containerGrids.numChildren; i++) {
            var grid = this.m_containerGrids.getChildAt(i);
            if (grid.IsTaken()) {
                continue;
            }
            /*
                                if ( grid.GetEmptyGridType() == Global.eEmptyGridType.inside_city )
                                {
                               
                                    ary_inside.push(grid.GetIdByString());
                                }
                                else if ( grid.GetEmptyGridType() == Global.eEmptyGridType.city_edge )
                                {
                   
                                    ary_edge.push(grid.GetIdByString());
                                }
                                */
        }
        /// 风车
        var ary = this.m_dicObjContainer[Global.eOperate.add_windturbine];
        for (var i = 0; i < this.m_containerTurbine.numChildren; i++) {
            var windturbine = this.m_containerTurbine.getChildAt(i);
            ary.push(windturbine.x + "_" + windturbine.y);
        }
        ///  树
        var ary = this.m_dicObjContainer[Global.eOperate.add_tree];
        for (var i = 0; i < this.m_containerTrees.numChildren; i++) {
            var tree = this.m_containerTrees.getChildAt(i);
            ary.push(tree.GetTreeResId() + "_" + tree.x + "_" + tree.y);
        }
        /*
                      /// cityhall 市政厅
                         var ary_cityhall:Array<string> = this.m_dicObjContainer[Global.eOperate.add_cityhall];
                                 var obj:CObj = this.m_dicFunctionalBuildings[Global.eOperate.add_cityhall];
        
                          ary_cityhall.push( obj.GetLocationGrid().GetIdByString() );
                          console.log( "市政厅到底有几个：" + ary_cityhall.length );
        
                      /// garage
                   
                                var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_garage];
                                 var obj:CObj = this.m_dicFunctionalBuildings[Global.eOperate.add_garage];
                    
                          ary.push( obj.GetLocationGrid().GetIdByString() );
                      
        
                        /// bank
                         var ary:Array<string> = this.m_dicObjContainer[Global.eOperate.add_bank];
                                 var obj:CObj = this.m_dicFunctionalBuildings[Global.eOperate.add_bank];
        
                          ary.push( obj.GetLocationGrid().GetIdByString() );
                      */
        // 地标
        for (var i = 0; i < this.m_containerDiBiao.numChildren; i++) {
            var obj = this.m_containerDiBiao.getChildAt(i);
            var ary = this.m_dicObjContainer[Global.eOperate.add_dibiao];
            ary.push(obj.GetLocationGrid().GetIdByString());
        }
        // 注意，这里面包含了市政厅、车站、银行 等特殊职能建筑
        for (var i = 0; i < this.m_containerObjs.numChildren; i++) {
            var obj = this.m_containerObjs.getChildAt(i);
            var ary = this.m_dicObjContainer[obj.GetOperateType()];
            ary.push(obj.GetLocationGrid().GetIdByString());
            //  console.log( "op type=" + obj.GetOperateType() );
        }
        for (var i = 0; i < this.m_containerRoads.numChildren; i++) {
            var obj = this.m_containerRoads.getChildAt(i);
            var ary = this.m_dicObjContainer[obj.GetOperateType()];
            ary.push(obj.GetLocationGrid().GetIdByString());
        }
        for (var i = 0; i <= nLastIndex; i++) {
            var ary = this.m_dicObjContainer[i];
            for (var j = 0; j < ary.length; j++) {
                szData += ary[j];
                if (j != ary.length - 1) {
                    szData += ",";
                }
            } // end j
            if (i != nLastIndex) {
                szData += "|";
            }
        } // end i
        console.log("存档：" + szData);
        return szData;
    };
    CTown.prototype.FixedUpdate = function () {
        this.AutomobileLoop();
        this.BuildingLandLoop();
        this.AutoUpgradeLoop();
        this.WindTurbineLoop();
    };
    CTown.prototype.AddAutomobile = function (nIndex) {
        this.m_aryTempRaods.length = 0;
        // 随机搜索一个路面，作为汽车的诞生点
        var road = null;
        for (var i = 0; i < this.m_containerRoads.numChildren; i++) {
            var obj = this.m_containerRoads.getChildAt(i);
            if (obj.GetOperateType() == Global.eOperate.add_road_youshangzuoxia ||
                obj.GetOperateType() == Global.eOperate.add_road_zuoshangyouxia) {
                this.m_aryTempRaods.push(obj);
                // road = obj;
            }
        }
        if (this.m_aryTempRaods.length == 0) {
            Main.s_panelMsgBox.ShowMsg("必须建设有马路才能放置汽车");
            return;
        }
        var nShit = Math.floor(Math.random() * this.m_aryTempRaods.length);
        if (nShit < 0 || nShit >= this.m_aryTempRaods.length) {
            nShit = 0;
        }
        road = this.m_aryTempRaods[nShit];
        var car = CAutomobileManager.NewAutomobile();
        car.SetCarIndex(nIndex);
        car.scaleX = 0.3;
        car.scaleY = 0.3;
        this.m_aryAutomobiles.push(car);
        car.SetFuncType(Global.eObjFunc.car);
        if (road.GetOperateType() == Global.eOperate.add_road_youshangzuoxia) {
            car.UpdateDir(Global.eAutomobileDir.zuoxia);
        }
        else if (road.GetOperateType() == Global.eOperate.add_road_zuoshangyouxia) {
            car.UpdateDir(Global.eAutomobileDir.youxia);
        }
        car.SetStartPosByDir(car.GetDir(), road.x, road.y);
        // 汽车的铆点设置在图片正中心
        car.anchorOffsetX = car.width / 2;
        car.anchorOffsetY = car.height / 2;
        car.SetRaod(road);
        car.SetMoveStatus(Global.eAutomobileStatus.normal); // 注意，先SetRaod，再SetMoveStatus
        CSoundManager.PlaySE(Global.eSE.car);
        car.SetSortPosY();
        this.InsertObj(car);
    };
    CTown.prototype.WindTurbineLoop = function () {
        this.m_fWindTimeElapse += CDef.s_fFixedDeltaTime;
        if (this.m_fWindTimeElapse < 0.3) {
            return;
        }
        this.m_fWindTimeElapse = 0;
        for (var i = this.m_containerTurbine.numChildren - 1; i >= 0; i--) {
            var turbine = this.m_containerTurbine.getChildAt(i);
            turbine.Loop();
        } // end for  
    };
    CTown.prototype.AutomobileLoop = function () {
        for (var i = this.m_aryAutomobiles.length - 1; i >= 0; i--) {
            var car = this.m_aryAutomobiles[i];
            car.Move();
            //car.U_Turn();
            car.Turn_Left();
            car.Turn_Right();
            car.ReInsertSort();
            car.GenerateCoinLoop();
            car.JinBiLoop();
        } // end i
    };
    CTown.prototype.onTapBuildingLand = function (evt) {
        // 判断下是否真正点中了。这个事件是以矩形包围盒来判断点选的
        var bHit = evt.currentTarget.hitTestPoint(evt.stageX, evt.stageY, true);
        if (bHit) {
            var objBuildingLand = evt.currentTarget;
            switch (objBuildingLand.GetBuildingLandStatus()) {
                case Global.eBuildingLandStatus.empty:// 空地
                    {
                        this.ProcessTapEmptyLand(objBuildingLand);
                    }
                    break;
                case Global.eBuildingLandStatus.contructing:// 建设中
                    {
                    }
                    break;
                case Global.eBuildingLandStatus.building_exist:// 已建成
                    {
                    }
                    break;
            } // end switch
        }
    };
    CTown.prototype.ProcessTapEmptyLand = function (obj) {
        // obj.BeginConstrucingStatus();   
    };
    CTown.prototype.BuildingLandLoop = function () {
        var bBuildingLandLoop = false;
        this.m_fBuildingLandLoopTimeElapse += CDef.s_fFixedDeltaTime;
        if (this.m_fBuildingLandLoopTimeElapse >= CDef.s_fBuildingLandLoopInterval) {
            bBuildingLandLoop = true;
            this.m_fBuildingLandLoopTimeElapse = 0;
        }
        for (var i = 0; i < this.m_containerObjs.numChildren; i++) {
            var obj = this.m_containerObjs.getChildAt(i);
            if (obj.GetFuncType() != Global.eObjFunc.building) {
                continue;
            }
            obj.ProgressBarLoop();
            if (bBuildingLandLoop) {
                obj.BuildingLandLoop();
            }
        }
    };
    CTown.prototype.SetBlur = function (bBlur) {
        return;
        if (bBlur) {
            this.filters = [CColorFucker.GetBlurFilter()];
        }
        else {
            this.filters = null;
        }
    };
    CTown.prototype.GenerateGrass = function () {
        var nTreeCount = 0;
        var fTotalDis = 0;
        var nTotalNum = 0;
        this.m_aryEmptyGrid.length = 0;
        var fChaZhiEndDis = -1;
        // 遍历每一个格子
        for (var i = 0; i < this.m_containerGrids.numChildren; i++) {
            var grid = this.m_containerGrids.getChildAt(i);
            if (grid.IsTaken()) {
                continue;
            }
            else {
                var nX = grid.GetId()["nIndexX"];
                var nY = grid.GetId()["nIndexY"];
                var fMyDis = (nX * nX + nY * nY);
                if (grid.GetEmptyGridType() == Global.eEmptyGridType.city_edge) {
                    fTotalDis += fMyDis;
                    nTotalNum++;
                }
                if (fMyDis > fChaZhiEndDis) {
                    fChaZhiEndDis = fMyDis;
                }
                this.m_aryEmptyGrid.push(grid);
            }
        } // end for
        var AverStartDis = fTotalDis / nTotalNum;
        var fChaZhiStartDis = AverStartDis;
        var startR = 82;
        var startG = 173;
        var startB = 70;
        var EndR = 255;
        var EndG = 215;
        var EndB = 0;
        for (var i = 0; i < this.m_aryEmptyGrid.length; i++) {
            var grid = this.m_aryEmptyGrid[i];
            if (grid.IsTaken() || grid.GetEmptyGridType() == Global.eEmptyGridType.inside_city) {
                continue;
            }
            var nX = grid.GetId()["nIndexX"];
            var nY = grid.GetId()["nIndexY"];
            var fMyPos = (nX * nX + nY * nY) - fChaZhiStartDis;
            if (fMyPos < 0) {
                fMyPos = 0;
            }
            var t = fMyPos / (fChaZhiEndDis - fChaZhiStartDis);
            var r = (1 - t) * startR + t * EndR;
            var g = (1 - t) * startG + t * EndG;
            var b = (1 - t) * startB + t * EndB;
            grid.SetColor(r, g, b);
            // 生成一棵树
            if (grid.GetEmptyGridType() == Global.eEmptyGridType.outside_city) {
                nTreeCount++;
                if (nTreeCount > 3) {
                    nTreeCount = 0;
                    var tree = new CObj();
                    tree.SetTexture(RES.getRes("shu_png"));
                    this.m_containerTrees.addChild(tree);
                    tree.anchorOffsetX = tree.width / 2;
                    tree.anchorOffsetY = tree.height;
                    tree.x = grid.x;
                    tree.y = grid.y;
                    var fRandomScale = Math.random();
                    if (fRandomScale < 0.3) {
                        tree.scaleX += fRandomScale;
                        tree.scaleY += fRandomScale;
                    }
                    else if (fRandomScale > 0.7) {
                        tree.scaleX -= (1 - fRandomScale);
                        tree.scaleY -= (1 - fRandomScale);
                    }
                    tree.scaleX *= 0.5;
                    tree.scaleY *= 0.5;
                    if (nY < 0) {
                        tree.y += Math.random() * CDef.s_nTileHeight * 0.5;
                    }
                    else {
                        tree.y -= Math.random() * CDef.s_nTileHeight * 0.5;
                    }
                    if (nX < 0) {
                        tree.x += Math.random() * CDef.s_nTileWidth * 0.5;
                    }
                    else {
                        tree.x -= Math.random() * CDef.s_nTileWidth * 0.5;
                    }
                }
            }
        }
    };
    CTown.prototype.SetCurEditProcessingLot = function (obj) {
        this.m_objCurProcessingLot = obj;
    };
    CTown.prototype.GetCurEditProcessingLot = function () {
        return this.m_objCurProcessingLot;
    };
    CTown.prototype.ProcessBuyOneLot = function (nCost, eLotProperty) {
        if (this.m_objCurProcessingLot == null) {
            console.log("有bug");
            return;
        }
        this.m_objCurProcessingLot.SetLotProperty(eLotProperty);
        // 开始建造
        this.m_objCurProcessingLot.BeginConstrucingStatus();
        // 消耗金币
        this.SetCoins(this.GetCoins() - nCost, true);
        this.OnLotBuildingNumChanged();
    };
    CTown.prototype.GetCurBuildingLotNum = function (eSizeType) {
        return this.m_dicBulildLotNum[eSizeType];
    };
    // 用“统计法”计数，而不是用“增量法”计数。这样可以最大限度的减少出Bug
    CTown.prototype.OnLotBuildingNumChanged = function () {
        this.UpdateCurLotStatus();
        // 建筑物数量变了，则更新人口数
        // 规定：建筑数量是决定人口数量的唯一参数。
        var nCurPopulation = CConfigManager.GetLot2Poulation(Global.eObjSize.size_2x2) * this.GetBuildingNum(Global.eObjSize.size_2x2)
            + CConfigManager.GetLot2Poulation(Global.eObjSize.size_4x4) * this.GetBuildingNum(Global.eObjSize.size_4x4);
        this.OnBuildingCPSChanged();
        this.SetPopulation(nCurPopulation);
        var objectRet = this.CheckIfReachThePopulationToNextCity();
        if (objectRet["Result"]) {
            CSoundManager.PlaySE(Global.eSE.congratulations);
        }
    };
    CTown.prototype.GetBuildingNum = function (eSizeType) {
        if (this.m_dicBulildLotNum[eSizeType] == undefined) {
            return 0;
        }
        return this.m_dicBulildLotNum[eSizeType];
    };
    CTown.prototype.GetTotalCPSableBuilding = function () {
        return this.m_nCurCPSableBuildingNum;
    };
    // 典型的“统计法”而不是“增量法”。前者不容易出Bug
    CTown.prototype.UpdateCurLotStatus = function () {
        this.m_dicEmptyLotNum[Global.eObjSize.size_2x2] = 0;
        this.m_dicEmptyLotNum[Global.eObjSize.size_4x4] = 0;
        this.m_dicBulildLotNum[Global.eObjSize.size_2x2] = 0;
        this.m_dicBulildLotNum[Global.eObjSize.size_4x4] = 0;
        this.m_dicPropertyPercent[Global.eLotPsroperty.residential] = 0;
        this.m_dicPropertyPercent[Global.eLotPsroperty.business] = 0;
        this.m_dicPropertyPercent[Global.eLotPsroperty.service] = 0;
        // 更新本城镇建筑物的总产钱能力
        this.m_nBuildingCPS = 0;
        this.m_nCurCPSableBuildingNum = 0;
        for (var i = 0; i < this.m_containerObjs.numChildren; i++) {
            var obj = this.m_containerObjs.getChildAt(i);
            if (obj.GetFuncType() != Global.eObjFunc.building) {
                continue;
            }
            // 该地块已经有建筑了，不能再建(除非执行了“推平”功能)
            // 注：正在建设中的状态也算“已有建筑了”，同时这种状态就开始产钱了
            if (obj.HasBuilding()) {
                if (!obj.IsBuildinSpecial()) {
                    this.m_dicBulildLotNum[obj.GetSizeType()]++;
                    // 本建筑的cps
                    var nCps = obj.GetLotCPS(); /*(CConfigManager.GetBuildingCoinBySizeAndLevel( obj.GetSizeType(), obj.GetLotLevel() )*/
                    ;
                    this.m_nBuildingCPS += nCps;
                    this.m_nCurCPSableBuildingNum++;
                    this.m_dicPropertyPercent[obj.GetLotProperty()]++;
                }
                continue;
            }
            this.m_dicEmptyLotNum[obj.GetSizeType()]++;
        } // end i
        var nTotal = this.m_dicPropertyPercent[Global.eLotPsroperty.residential] +
            this.m_dicPropertyPercent[Global.eLotPsroperty.business] +
            this.m_dicPropertyPercent[Global.eLotPsroperty.service];
        this.m_dicPropertyPercent[Global.eLotPsroperty.residential] /= nTotal;
        this.m_dicPropertyPercent[Global.eLotPsroperty.business] /= nTotal;
        this.m_dicPropertyPercent[Global.eLotPsroperty.service] /= nTotal;
        //   console.log( "空地数：" +  this.m_dicEmptyLotNum[Global.eObjSize.size_2x2] + "_" +  this.m_dicEmptyLotNum[Global.eObjSize.size_4x4]);
        //   console.log( "有自建建筑数：" +  this.m_dicBulildLotNum[Global.eObjSize.size_2x2] + "_" +  this.m_dicBulildLotNum[Global.eObjSize.size_4x4]);
        //   console.log( "建筑CPS:" + this.m_nBuildingCPS );
    };
    CTown.prototype.OnPopulationChanged = function () {
        // 计算人口贡献的金币产出率
        var EaringPerBuilding = CConfigManager.GetPopulation2CoinXiShu() * this.GetPopulation();
        this.m_nPopulationCPS = EaringPerBuilding * this.GetTotalCPSableBuilding();
        console.log("人口CPS=" + this.m_nPopulationCPS);
        CUIManager.s_uiWindTurbine.SetParams(EaringPerBuilding, this.m_nPopulationCPS);
        this.UpdateCPS(); // 人口数变了，则总CPS更新
    };
    CTown.prototype.OnBuildingCPSChanged = function () {
        this.UpdateCPS(); // 建筑CPS变了，则总CPS更新
    };
    CTown.prototype.GetBuildingCPS = function () {
        return this.m_nBuildingCPS;
    };
    CTown.prototype.SetBuildingCPS = function (nBuildingCPS) {
        this.m_nBuildingCPS = nBuildingCPS;
    };
    CTown.prototype.GetPopulationCPS = function () {
        return this.m_nPopulationCPS;
    };
    CTown.prototype.SetPopulationCPS = function (nPopulationCPS) {
        this.m_nPopulationCPS = nPopulationCPS;
    };
    CTown.prototype.SetCoins = function (nCoins, bDirect) {
        if (bDirect === void 0) { bDirect = false; }
        this.s_nCoins = nCoins;
        CUIManager.s_uiMainTitle.SetCoins(nCoins, bDirect);
    };
    CTown.prototype.GetCoins = function () {
        return this.s_nCoins;
    };
    CTown.prototype.SetPopulation = function (nPopulation) {
        this.s_nPopulation = nPopulation;
        CUIManager.s_uiMainTitle.SetPopulation(nPopulation);
        this.OnPopulationChanged();
    };
    CTown.prototype.GetPopulation = function () {
        return this.s_nPopulation;
    };
    CTown.prototype.GetCPS = function () {
        return this.s_nCPS;
    };
    CTown.prototype.SetCPS = function (nCPS) {
        this.s_nCPS = nCPS;
    };
    CTown.prototype.GetCpsWithoutDoubleTime = function () {
        return this.s_nCPS_WithOutDoubleTime;
    };
    CTown.prototype.UpdateCPS = function () {
        this.s_nCPS = 0; // 以下开始累加
        // 遍历当前的建筑，每个建筑有产金币能力
        this.s_nCPS += Main.s_CurTown.GetBuildingCPS();
        //this计算人口对金币产出的贡献
        this.s_nCPS += Main.s_CurTown.GetPopulationCPS();
        this.s_nCPS_WithOutDoubleTime = this.s_nCPS;
        // ------------------------------ 统计完成
        // Double Time 状态的加成
        if (CPlayer.IsDoubleTime()) {
            this.s_nCPS *= 2;
        }
        // 更新界面显示
        CUIManager.s_uiMainTitle.SetCPS(this.s_nCPS);
        // 计算最新的金钱刷新速度
        this.m_nShowCoinChangeSpeed = this.s_nCPS;
    };
    CTown.prototype.GetShowCoinChangeSpeed = function () {
        return this.m_nShowCoinChangeSpeed;
    };
    // 一秒计算一次金币收入
    CTown.prototype.UpdateTotalCoins = function () {
        this.s_nCoins += this.s_nCPS;
        this.SetCoins(this.s_nCoins);
    };
    CTown.prototype.AddWindTurbine = function (grid) {
        var turbine = new CWindTurbine();
        this.m_containerTurbine.addChild(turbine);
        turbine.anchorOffsetX = turbine.width / 2;
        turbine.anchorOffsetY = turbine.height;
        turbine.x = grid.x;
        turbine.y = grid.y;
        turbine.touchEnabled = true;
        turbine.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapTurbine, this);
    };
    CTown.prototype.onTapTurbine = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.wind_turbine, true);
    };
    CTown.prototype.GetLotProperty = function (eProperty) {
        var fPercent = this.m_dicPropertyPercent[eProperty];
        if (fPercent == undefined || isNaN(fPercent)) {
            return 0;
        }
        return fPercent;
    };
    CTown.prototype.SetGarageItemStatus = function (nIndex, eStatus) {
        this.m_dicGarageInfo[nIndex] = eStatus;
    };
    CTown.prototype.GetGarageItemStatus = function (nIndex) {
        var eStatus = this.m_dicGarageInfo[nIndex];
        return eStatus;
    };
    CTown.prototype.CostCoinDueToUnlockCar = function (nIndex) {
        this.SetCoins(this.GetCoins() - CConfigManager.GetCoinForUnlockCar(nIndex), true);
    };
    CTown.prototype.UpdateGarageStatus = function () {
        for (var i = 0; i < CConfigManager.s_nTotalCarNum; i++) {
            if (this.GetGarageItemStatus(i) == Global.eGarageItemStatus.unlocked) {
                continue;
            }
            var nCoinForThisCar = CConfigManager.GetCoinForUnlockCar(i);
            if (this.GetCoins() < nCoinForThisCar) {
                this.SetGarageItemStatus(i, Global.eGarageItemStatus.can_not_unlock);
                CUIManager.s_uiGarage.UpdateStatus(i, Global.eGarageItemStatus.can_not_unlock);
            }
            else {
                this.SetGarageItemStatus(i, Global.eGarageItemStatus.locked_and_can_unlock);
                CUIManager.s_uiGarage.UpdateStatus(i, Global.eGarageItemStatus.locked_and_can_unlock);
            }
            return;
        }
    };
    CTown.prototype.AddUpgradableLot = function (lot) {
        this.m_aryUpgradableBuildings.push(lot);
    };
    CTown.prototype.RemoveUpgradableLot = function (lot) {
        for (var i = 0; i < this.m_aryUpgradableBuildings.length; i++) {
            if (lot == this.m_aryUpgradableBuildings[i]) {
                this.m_aryUpgradableBuildings.splice(i, 1);
                return;
            }
        }
    };
    CTown.prototype.AutoUpgradeLoop = function () {
        if (this.m_aryUpgradableBuildings.length == 0) {
            return;
        }
        if (this.m_objUpgradingLot != null) {
            this.m_objUpgradingLot.ProgressBarLoop();
        }
        else {
            if (this.m_nAutoUpgradeIndex >= this.m_aryUpgradableBuildings.length) {
                this.m_nAutoUpgradeIndex = 0;
            }
            this.SetupgradinLot(this.m_aryUpgradableBuildings[this.m_nAutoUpgradeIndex]);
            this.m_objUpgradingLot.BeginUpgrade();
            this.m_nAutoUpgradeIndex++;
            if (this.m_nAutoUpgradeIndex >= this.m_aryUpgradableBuildings.length) {
                this.m_nAutoUpgradeIndex = 0;
            }
        }
    };
    CTown.prototype.SetupgradinLot = function (lot) {
        this.m_objUpgradingLot = lot;
    };
    CTown.prototype.RevertLot = function () {
        var objCurEdit = this.GetCurEditProcessingLot();
        if (objCurEdit == null) {
            console.log("有Bug");
            return;
        }
        objCurEdit.Revert();
        this.OnLotBuildingNumChanged();
        this.SetCurEditProcessingLot(null);
    };
    CTown.prototype.GetupgradinLot = function () {
        return this.m_objUpgradingLot;
    };
    CTown.prototype.GetCityHallData = function (ePageIndex, nItemIndex) {
        var dic = null;
        if (ePageIndex == Global.eCityHallPageType.city) {
            dic = this.m_dicCityHallData_City;
        }
        else if (ePageIndex == Global.eCityHallPageType.game) {
            dic = this.m_dicCityHallData_Game;
        }
        var data = dic[nItemIndex];
        if (data == undefined) {
            data = new CCityHallData();
            dic[nItemIndex] = data;
        }
        return data;
    };
    CTown.prototype.SetCityHallItemAffect = function (nIndex, nValue) {
        this.m_dicCityHallItemAffect[nIndex] = nValue;
    };
    CTown.prototype.GetCityHallItemAffect = function (nIndex) {
        return this.m_dicCityHallItemAffect[nIndex];
    };
    CTown.prototype.ProcessBankSaving_Begin = function () {
        var date = new Date();
        this.m_nDeavtovateTime = date.getTime();
    };
    CTown.prototype.ProcessBankSaving_End = function () {
        var date = new Date();
        var nAwayTime = (date.getTime() - this.m_nDeavtovateTime) / 1000; // 单位是毫秒，所以要除以一千
        if (this.GetCPS() == 0) {
            return;
        }
        this.m_nBankSaving = Math.floor(nAwayTime * this.GetCPS() * this.m_BankData.nCurProfitRate);
        CUIManager.s_uiBankBack.visible = true;
        CUIManager.s_uiBankBack.SetBankSaving(this.m_nBankSaving);
    };
    CTown.prototype.CollectBankSaving = function () {
        CPlayer.SetMoney(Global.eMoneyType.coin, CPlayer.GetMoney(Global.eMoneyType.coin) + this.m_nBankSaving);
    };
    CTown.prototype.GetLevel = function () {
        return this.m_nID;
    };
    CTown.prototype.ProcessClick = function (fStageX, fStageY) {
        // 奇数行只有奇数列，偶数行只有偶数列。
        var fPosXInTown = (fStageX - this.x) / this.scaleX;
        var fPosYInTown = (fStageY - this.y) / this.scaleY;
        var nIndex_X = fPosXInTown / CDef.s_nTileWidth;
        if (fPosXInTown > 0) {
            nIndex_X = Math.floor(nIndex_X) * 2 + 1;
        }
        else {
            nIndex_X = Math.ceil(nIndex_X) * 2 - 1;
        }
        var nIndex_Y = fPosYInTown / CDef.s_nTileHeight;
        if (fPosYInTown > 0) {
            nIndex_Y = Math.floor(nIndex_Y) * 2 + 1;
        }
        else {
            nIndex_Y = Math.ceil(nIndex_Y) * 2 - 1;
        }
        var grid = this.GetGridByIndex(nIndex_X.toString(), nIndex_Y.toString());
        if (grid == null) {
            return;
        }
        var nRelatedGridIndexX = 0;
        var nRelatedGridIndexY = 0;
        var grid_related = null;
        var x1 = 0;
        var y1 = 0;
        var x2 = 0;
        var y2 = 0;
        var nRet = 0;
        var grid_selected = null;
        if (fPosXInTown >= grid.x) {
            if (fPosYInTown >= grid.y) {
                ///  console.log( nIndex_X + "," + nIndex_Y + ":" + "右下" );
                nRelatedGridIndexX = nIndex_X + 1;
                nRelatedGridIndexY = nIndex_Y + 1;
                grid_related = this.GetGridByIndex(nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString());
                x1 = grid.GetCornerPosX(Global.eGridCornerPosType.right);
                y1 = grid.GetCornerPosY(Global.eGridCornerPosType.right);
                x2 = grid.GetCornerPosX(Global.eGridCornerPosType.bottom);
                y2 = grid.GetCornerPosY(Global.eGridCornerPosType.bottom);
                nRet = CyberTreeMath.LeftOfLine(fPosXInTown, fPosYInTown, x1, y1, x2, y2);
                if (nRet == 1) {
                    grid_selected = grid;
                }
                else {
                    grid_selected = grid_related;
                }
            }
            else {
                //   console.log( nIndex_X + "," + nIndex_Y + ":" + "右上" );
                nRelatedGridIndexX = nIndex_X + 1;
                nRelatedGridIndexY = nIndex_Y - 1;
                grid_related = this.GetGridByIndex(nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString());
                x1 = grid.GetCornerPosX(Global.eGridCornerPosType.right);
                y1 = grid.GetCornerPosY(Global.eGridCornerPosType.right);
                x2 = grid.GetCornerPosX(Global.eGridCornerPosType.top);
                y2 = grid.GetCornerPosY(Global.eGridCornerPosType.top);
                nRet = CyberTreeMath.LeftOfLine(fPosXInTown, fPosYInTown, x1, y1, x2, y2);
                if (nRet == 1) {
                    grid_selected = grid;
                }
                else {
                    grid_selected = grid_related;
                }
            }
        }
        else {
            if (fPosYInTown >= grid.y) {
                //    console.log( nIndex_X + "," + nIndex_Y + ":" + "左下" );
                nRelatedGridIndexX = nIndex_X - 1;
                nRelatedGridIndexY = nIndex_Y + 1;
                grid_related = this.GetGridByIndex(nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString());
                x1 = grid.GetCornerPosX(Global.eGridCornerPosType.left);
                y1 = grid.GetCornerPosY(Global.eGridCornerPosType.left);
                x2 = grid.GetCornerPosX(Global.eGridCornerPosType.bottom);
                y2 = grid.GetCornerPosY(Global.eGridCornerPosType.bottom);
                nRet = CyberTreeMath.LeftOfLine(fPosXInTown, fPosYInTown, x1, y1, x2, y2);
                if (nRet == 1) {
                    grid_selected = grid_related;
                }
                else {
                    grid_selected = grid;
                }
            }
            else {
                //     console.log( nIndex_X + "," + nIndex_Y + ":" + "左上" );
                nRelatedGridIndexX = nIndex_X - 1;
                nRelatedGridIndexY = nIndex_Y - 1;
                grid_related = this.GetGridByIndex(nRelatedGridIndexX.toString(), nRelatedGridIndexY.toString());
                x1 = grid.GetCornerPosX(Global.eGridCornerPosType.left);
                y1 = grid.GetCornerPosY(Global.eGridCornerPosType.left);
                x2 = grid.GetCornerPosX(Global.eGridCornerPosType.top);
                y2 = grid.GetCornerPosY(Global.eGridCornerPosType.top);
                nRet = CyberTreeMath.LeftOfLine(fPosXInTown, fPosYInTown, x1, y1, x2, y2);
                if (nRet == 1) {
                    grid_selected = grid_related;
                }
                else {
                    grid_selected = grid;
                }
            }
        }
        if (grid_selected == null) {
            return;
        }
        // console.log( "选中grid：" + grid_selected.GetIdByString() );
        this.Processtap(grid_selected, fPosXInTown, fPosYInTown);
    };
    CTown.prototype.StagePos2TownPos = function (fStageX, fStageY) {
        this.m_objTapTownPos["x"] = (fStageX - this.x) / this.scaleX;
        this.m_objTapTownPos["y"] = (fStageY - this.y) / this.scaleY;
        return this.m_objTapTownPos;
    };
    CTown.prototype.handleTapDibiao = function (evt) {
        if (this.m_eCurOperate == Global.eOperate.del_dibiao) {
            var dibiao = evt.target;
            CResourceManager.DeleteDiBiao(dibiao);
        }
    };
    CTown.prototype.ToggleGrids = function () {
        this.m_bShowGrid = !this.m_bShowGrid;
        for (var i = 0; i < this.m_aryAllGrids.length; i++) {
            var grid = this.m_aryAllGrids[i];
            grid.SetGridVisible(this.m_bShowGrid);
        }
    };
    CTown.prototype.SetCloudManager = function (cloud_manager) {
        this.m_CloudManager = cloud_manager;
    };
    CTown.prototype.ClearAll = function () {
        for (var i = 0; i < 32; i++) {
            this.SetCityHallItemAffect(i, 0);
        }
        this.m_dicEmptyLotNum[Global.eObjSize.size_2x2] = 0;
        this.m_dicEmptyLotNum[Global.eObjSize.size_4x4] = 0;
        this.m_dicBulildLotNum[Global.eObjSize.size_2x2] = 0;
        this.m_dicBulildLotNum[Global.eObjSize.size_4x4] = 0;
        this.m_dicPropertyPercent[Global.eLotPsroperty.residential] = 0;
        this.m_dicPropertyPercent[Global.eLotPsroperty.business] = 0;
        this.m_dicPropertyPercent[Global.eLotPsroperty.service] = 0;
        this.m_objCurProcessingLot = null;
        // 清空市政厅中“城市页”数据；注意，“游戏页”数据不能清空，是跟玩家账号绑定的，而不是跟关卡绑定的
        // to do
        // 清空特殊职能建筑信息
        this.m_dicFunctionalBuildings[Global.eOperate.add_cityhall] = null;
        this.m_dicFunctionalBuildings[Global.eOperate.add_garage] = null;
        this.m_dicFunctionalBuildings[Global.eOperate.add_bank] = null;
        //  清空当前人口以及人口的金币产出率
        this.SetPopulation(0);
        this.SetPopulationCPS(0);
        // 清空当前的总CPS
        this.SetCPS(0);
        CUIManager.s_uiMainTitle.SetCPS(0);
        // 清空当前的建筑CPS
        this.SetBuildingCPS(0);
        // 清除Obj
        for (var i = this.m_containerObjs.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerObjs.getChildAt(i);
            if (obj.GetFuncType() == Global.eObjFunc.car) {
                var jinbi = obj.getChildByName("jinbi");
                if (jinbi != null && jinbi != undefined) {
                    CJinBiManager.DeleteJinBi(jinbi);
                }
            }
            CResourceManager.DeleteObj(obj);
        }
        // 清除路面
        for (var i = this.m_containerRoads.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerRoads.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除树木
        for (var i = this.m_containerTrees.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerTrees.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除地表
        for (var i = this.m_containerDiBiao.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerDiBiao.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除风车 
        for (var i = this.m_containerTurbine.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerTurbine.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除进度条
        for (var i = this.m_containerProgressbar.numChildren - 1; i >= 0; i--) {
            var bar = this.m_containerProgressbar.getChildAt(i);
            CResourceManager.DeleteProgressBar(bar);
        }
        // 清除汽车信息
        this.m_aryAutomobiles.length = 0;
        // 清除建筑工地动画
        // 清除飞机
        // 清除自动升级状态
        this.m_objUpgradingLot = null;
        this.m_aryUpgradableBuildings.length = 0;
        // 清除云彩和天空盒
        this.m_CloudManager.Clear();
        // 清空所有格子上的信息
        for (var i = this.m_containerGrids.numChildren - 1; i >= 0; i--) {
            var grid = this.m_containerGrids.getChildAt(i);
            grid.SetBoundObj(null);
        }
    };
    CTown.prototype.ChangeTown = function (nNewTownId) {
        if (this.GetId() == nNewTownId) {
            return;
        }
        this.ClearAll();
        this.LoadTownData(nNewTownId);
    };
    CTown.prototype.MoveToNextCity = function () {
        this.ChangeTown(this.GetId() + 1);
    };
    CTown.prototype.CheckIfReachThePopulationToNextCity = function () {
        var config = CConfigManager.GetCityLevelConfig(Main.s_CurTown.GetLevel());
        CTown.s_objectTemp["NeedPopulation"] = config.nPopulationToLevelUp;
        CTown.s_objectTemp["Result"] = Main.s_CurTown.GetPopulation() >= config.nPopulationToLevelUp;
        return CTown.s_objectTemp;
    };
    CTown.s_objectTemp = new Object();
    return CTown;
}(egret.DisplayObjectContainer)); // end class CTown
__reflect(CTown.prototype, "CTown");
//# sourceMappingURL=CTown.js.map