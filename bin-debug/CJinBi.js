var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CJinBi = (function (_super) {
    __extends(CJinBi, _super);
    function CJinBi() {
        var _this = _super.call(this) || this;
        _this.m_BoundObj = null;
        _this.m_nCurFrameIndex = 0;
        _this.m_fAniTimeElapse = 0;
        _this.m_eType = Global.eMoneySubType.small_coin;
        _this.m_nValue = 0;
        _this.m_bmpMainPic = new egret.Bitmap();
        _this.addChild(_this.m_bmpMainPic);
        _this.touchEnabled = true;
        _this.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapJinBi, _this);
        return _this;
    } //  end constructor
    CJinBi.prototype.SetValue = function (eType, nValue) {
        this.m_eType = eType;
        this.m_nValue = nValue;
    };
    CJinBi.prototype.OnClick = function () {
        var tiaozi = CTiaoZiManager.NewTiaoZi();
        tiaozi.BeginTiaoZi();
        tiaozi.x = this.GetBoundObj().x + 200;
        tiaozi.y = this.GetBoundObj().y - 150;
        tiaozi.SetTypeAndValue(this.m_eType, this.m_nValue);
        tiaozi.scaleX = 2;
        tiaozi.scaleY = 2;
        Main.s_CurTown.SetCoins(Main.s_CurTown.GetCoins() + this.m_nValue);
        this.m_BoundObj.ClearBoundJinBi();
        CJinBiManager.DeleteJinBi(this);
        CSoundManager.PlaySE(Global.eSE.small_coin);
    };
    CJinBi.prototype.onTapJinBi = function (e) {
        this.OnClick();
    };
    CJinBi.prototype.BeginTiaoZi = function () {
    };
    CJinBi.prototype.SetBoundObj = function (obj) {
        this.m_BoundObj = obj;
    };
    CJinBi.prototype.GetBoundObj = function () {
        return this.m_BoundObj;
    };
    CJinBi.prototype.UpdateJinBiAnimation = function () {
        this.m_fAniTimeElapse += CDef.s_fFixedDeltaTime;
        if (this.m_fAniTimeElapse < CDef.s_fJinBiAniInterval) {
            return;
        }
        this.m_fAniTimeElapse = 0;
        var nRealResIndex = this.m_nCurFrameIndex;
        if (this.m_nCurFrameIndex == 2) {
            nRealResIndex = 3;
        }
        if (this.m_nCurFrameIndex == 3) {
            nRealResIndex = 2;
        }
        if (this.m_nCurFrameIndex == 4) {
            nRealResIndex = 3;
        }
        this.m_bmpMainPic.texture = CJinBiManager.GetFrameTexture(nRealResIndex);
        this.m_nCurFrameIndex++;
        if (this.m_nCurFrameIndex >= CDef.s_fJinBiAniFrameNum) {
            this.m_nCurFrameIndex = 0;
        }
        this.anchorOffsetX = this.width * 0.5;
        this.anchorOffsetY = this.height;
    };
    CJinBi.prototype.Update = function () {
        this.UpdateJinBiAnimation();
        //this.UpdatePos();
    };
    return CJinBi;
}(CObj)); // end class
__reflect(CJinBi.prototype, "CJinBi");
//# sourceMappingURL=CJinBi.js.map