var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
    风车
*/
var CWindTurbine = (function (_super) {
    __extends(CWindTurbine, _super);
    function CWindTurbine() {
        var _this = _super.call(this) || this;
        _this.m_nFrameNum = 2;
        _this.m_nFrameIndex = 0;
        _this.m_bmpFan = new egret.Bitmap();
        _this.addChild(_this.m_bmpFan);
        _this.m_bmpMainPic.texture = RES.getRes("windturbine_stick_png");
        _this.m_bmpFan.x = 0;
        _this.m_bmpFan.y = 0;
        _this.scaleX = 0.5;
        _this.scaleY = 0.5;
        return _this;
    } // end constructor
    CWindTurbine.prototype.Reset = function () {
        _super.prototype.Reset.call(this);
        this.m_bmpFan.texture = null;
    };
    CWindTurbine.prototype.Loop = function () {
        if (this.m_nFrameIndex >= this.m_nFrameNum) {
            this.m_nFrameIndex = 0;
        }
        var szResName = "windturbine_fan" + this.m_nFrameIndex + "_png";
        this.m_bmpFan.texture = RES.getRes(szResName);
        this.m_nFrameIndex++;
    };
    return CWindTurbine;
}(CObj)); // end class
__reflect(CWindTurbine.prototype, "CWindTurbine");
//# sourceMappingURL=CWIndTurbine.js.map