var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
135， 70， 246
*/
var CUICityHall = (function (_super) {
    __extends(CUICityHall, _super);
    function CUICityHall() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_containerItems_city = new egret.DisplayObjectContainer();
        _this.m_aryItems_City = new Array();
        _this.m_containerItems_game = new egret.DisplayObjectContainer();
        _this.m_aryItems_Game = new Array();
        _this.m_nCurPage = 0; // 0 - city  1 - game
        _this.m_ScrollView = new egret.ScrollView();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CCityHall.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("img1");
        _this.addChild(_this.m_ScrollView);
        // sv.setContent( this.m_containerItems_city );
        //sv.setContent( this.m_containerItems_game );
        _this.m_ScrollView.x = imgTemp.x;
        _this.m_ScrollView.y = imgTemp.y;
        _this.m_ScrollView.width = imgTemp.width;
        _this.m_ScrollView.height = imgTemp.height;
        _this.m_ScrollView.verticalScrollPolicy = "on";
        _this.m_ScrollView.horizontalScrollPolicy = "off";
        _this.m_uiContainer.removeChild(imgTemp);
        // init "city items
        var nPosY = 0;
        var nCount = 0;
        var nCityNo = 1;
        for (var i = 0; i < 32; i++) {
            var item = new CUICityHallItem();
            _this.m_containerItems_city.addChild(item);
            _this.m_aryItems_City.push(item);
            item.SetBgColor(42, 90, 246);
            //   item.SetIcon( RES.getRes( "cityhall_city_" + i + "_png" ) );
            item.SetCityNo(nCityNo);
            item.SetItemIndex(0, i);
            item.y = nPosY;
            nPosY += 115;
            nCount++;
            if (nCount == 4) {
                var txtCityNo = new eui.Label();
                _this.m_containerItems_city.addChild(txtCityNo);
                txtCityNo.y = nPosY;
                nPosY += 50;
                txtCityNo.height = 50;
                txtCityNo.text = "城市" + (++nCityNo) + " 将开启如下选项：";
                txtCityNo.size = 22;
                txtCityNo.verticalAlign = egret.VerticalAlign.MIDDLE;
                txtCityNo.textColor = 0x696969;
                nCount = 0;
            }
        }
        // init "game" items
        nPosY = 0;
        for (var i = 0; i < 14; i++) {
            var item = new CUICityHallItem();
            _this.m_containerItems_game.addChild(item);
            item.SetBgColor(135, 70, 246);
            //   item.SetIcon( RES.getRes( "cityhall_game_" + i + "_png" ) );
            item.SetItemIndex(Global.eCityHallPageType.game, i);
            item.y = nPosY;
            nPosY += 115;
            item.SetLock(false);
            _this.m_aryItems_Game.push(item);
        }
        ////////////////
        imgTemp = _this.m_uiContainer.getChildByName("img6");
        _this.m_btnSwitchCity = new CCosmosImage();
        _this.m_btnSwitchCity.SetExml("resource/assets/MyExml/CCosmosButton5.exml");
        _this.m_uiContainer.addChild(_this.m_btnSwitchCity);
        _this.m_btnSwitchCity.x = imgTemp.x;
        _this.m_btnSwitchCity.y = imgTemp.y;
        _this.m_btnSwitchCity.SetImageColor_255(null, 0, 63, 132, 247);
        // this.m_btnSwitchCity.SetImageVisible( 1, false );
        _this.m_btnSwitchCity.SetImageAlpha(1, 0);
        _this.m_btnSwitchCity.SetLabelContent(0, "城市");
        _this.m_btnSwitchCity.SetLabelContent(1, "");
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnSwitchCity.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_SwitchPageCity, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img5");
        _this.m_btnSwitchGame = new CCosmosImage();
        _this.m_btnSwitchGame.SetExml("resource/assets/MyExml/CCosmosButton5.exml");
        _this.m_uiContainer.addChild(_this.m_btnSwitchGame);
        _this.m_btnSwitchGame.x = imgTemp.x;
        _this.m_btnSwitchGame.y = imgTemp.y;
        _this.m_btnSwitchGame.SetImageColor_255(null, 0, 152, 78, 246);
        _this.m_btnSwitchGame.SetLabelContent(0, "");
        _this.m_btnSwitchGame.SetLabelContent(1, "游戏(14)");
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnSwitchGame.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_SwitchPageGame, _this);
        // 华丽丽的分割线
        _this.m_imgBorder = _this.m_uiContainer.getChildByName("img7");
        _this.SwitchPage(0);
        imgTemp = _this.m_uiContainer.getChildByName("img8");
        _this.m_btnClose = new CCosmosImage();
        _this.m_btnClose.SetExml("resource/assets/MyExml/CCosmosButton1.exml");
        _this.m_uiContainer.addChild(_this.m_btnClose);
        _this.m_btnClose.x = imgTemp.x;
        _this.m_btnClose.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnClose.UseColorSolution(0);
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
    } // end constructor
    CUICityHall.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.city_hall, false);
    };
    CUICityHall.prototype.onButtonClick_SwitchPageCity = function (evt) {
        this.SwitchPage(0);
    };
    CUICityHall.prototype.onButtonClick_SwitchPageGame = function (evt) {
        this.SwitchPage(1);
    };
    CUICityHall.prototype.SwitchPage = function (nPageIndex) {
        if (nPageIndex == 0) {
            this.m_ScrollView.setContent(this.m_containerItems_city);
            this.m_imgBorder.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(39, 83, 227)];
        }
        else if (nPageIndex == 1) {
            this.m_ScrollView.setContent(this.m_containerItems_game);
            this.m_imgBorder.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(125, 65, 227)];
        }
    };
    CUICityHall.prototype.UnlockItems = function (nCityNo) {
        var container = this.m_containerItems_city;
        var nCount = 0;
        for (var i = 0; i < container.numChildren; i++) {
            if (nCount == 4) {
                nCount = 0;
                continue;
            }
            var item = container.getChildAt(i);
            if (item.GetCityNo() <= nCityNo) {
                item.SetLock(false);
            }
            else {
                return;
            }
            nCount++;
        }
    };
    // 钱币数量在实时改变，因此应该实时刷新这个界面上的按钮状态
    CUICityHall.prototype.UpdateStatus = function () {
        for (var i = 0; i < this.m_aryItems_City.length; i++) {
            var item = this.m_aryItems_City[i];
            item.UpdateStatus();
        }
        for (var i = 0; i < this.m_aryItems_Game.length; i++) {
            var item = this.m_aryItems_Game[i];
            item.UpdateStatus();
        }
    };
    return CUICityHall;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUICityHall.prototype, "CUICityHall");
//# sourceMappingURL=CUICityHall.js.map