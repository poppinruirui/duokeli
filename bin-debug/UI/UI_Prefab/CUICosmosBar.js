var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUICosmosBar = (function (_super) {
    __extends(CUICosmosBar, _super);
    function CUICosmosBar() {
        var _this = _super.call(this) || this;
        _this.m_LeftCircle = new egret.Shape();
        _this.m_RightCircle = new egret.Shape();
        _this.m_MiddleRect = new egret.Shape();
        _this.addChild(_this.m_LeftCircle);
        _this.addChild(_this.m_RightCircle);
        _this.addChild(_this.m_MiddleRect);
        return _this;
    } // end constructor
    CUICosmosBar.prototype.SetParams = function (width, height, color, alpha) {
        if (alpha === void 0) { alpha = 1; }
        var radius = 0.5 * height;
        this.m_LeftCircle.graphics.clear();
        this.m_LeftCircle.graphics.beginFill(color, alpha);
        this.m_LeftCircle.graphics.drawArc(0, 0, radius, Math.PI / 2, 1.5 * Math.PI, false);
        this.m_LeftCircle.graphics.endFill();
        this.m_LeftCircle.x = radius;
        this.m_MiddleRect.graphics.clear();
        var middle_width = width - height;
        this.m_MiddleRect.graphics.beginFill(color, alpha);
        this.m_MiddleRect.graphics.drawRect(0, -radius, middle_width, height);
        this.m_MiddleRect.graphics.endFill();
        this.m_MiddleRect.x = radius;
        this.m_RightCircle.graphics.clear();
        this.m_RightCircle.graphics.beginFill(color, alpha);
        this.m_RightCircle.graphics.drawArc(0, 0, radius, 1.5 * Math.PI, 0.5 * Math.PI, false);
        this.m_RightCircle.graphics.endFill();
        this.m_RightCircle.x = radius + middle_width;
    };
    return CUICosmosBar;
}(eui.Component)); // end class
__reflect(CUICosmosBar.prototype, "CUICosmosBar");
//# sourceMappingURL=CUICosmosBar.js.map