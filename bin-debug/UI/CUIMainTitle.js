var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIMainTitle = (function (_super) {
    __extends(CUIMainTitle, _super);
    function CUIMainTitle() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_nRealtimeCoin = 0;
        _this.m_nShowCoin = 0;
        _this.c_step = 5;
        _this.m_fShowCoinTimeElapse = 0;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/test_ui.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_txtCityName = _this.m_uiContainer.getChildByName("labelCityName");
        _this.m_txtCityName.text = "浮云小镇";
        /*
                this.m_imgBg1 = this.m_uiContainer.getChildByName( "imgBg1" ) as eui.Image;
                this.m_imgBg1.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 1,1,1,1 )];
                
                this.m_imgBg2 = this.m_uiContainer.getChildByName( "imgBg2" ) as eui.Image;
                this.m_imgBg2.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.8313,0.8313,0.8313,1 )];
              
                this.m_imgBg3 = this.m_uiContainer.getChildByName( "imgBg3" ) as eui.Image;
                this.m_imgBg3.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 1,1,1,1 )];
                
                this.m_imgBg4 = this.m_uiContainer.getChildByName( "imgBg4" ) as eui.Image;
                this.m_imgBg4.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.8313,0.8313,0.8313,1 )];
        */
        var imgTemp = _this.m_uiContainer.getChildByName("imgNextCity");
        _this.m_btnNextCity = new CCosmosImage();
        _this.m_uiContainer.addChild(_this.m_btnNextCity);
        _this.m_btnNextCity.SetExml("resource/assets/MyExml/CCosmosButton8.exml");
        _this.m_btnNextCity.x = imgTemp.x;
        _this.m_btnNextCity.y = imgTemp.y;
        _this.m_btnNextCity.scaleX = imgTemp.scaleX;
        _this.m_btnNextCity.scaleY = imgTemp.scaleY;
        _this.m_btnNextCity.SetImageTexture(0, RES.getRes("next_city_png"));
        _this.m_uiContainer.removeChild(imgTemp);
        imgTemp = _this.m_uiContainer.getChildByName("imgPopulationPercent");
        _this.m_pbPopulation = new CUICosmosProgressBar();
        _this.m_uiContainer.addChild(_this.m_pbPopulation);
        _this.m_pbPopulation.SetParams(imgTemp.width, imgTemp.height, 2, 0x00EE00, 0x000000, 0x006400);
        _this.m_pbPopulation.x = imgTemp.x;
        _this.m_pbPopulation.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_pbPopulation.SetPercent(0);
        _this.m_btnNextCity.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_NextCity, _this);
        imgTemp = _this.m_uiContainer.getChildByName("imgTask");
        _this.m_btnTask = new CCosmosImage();
        _this.m_btnTask.SetExml("resource/assets/MyExml/CTaskButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnTask);
        _this.m_btnTask.x = imgTemp.x;
        _this.m_btnTask.y = imgTemp.y;
        _this.m_btnTask.scaleX = imgTemp.scaleX;
        _this.m_btnTask.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnTaskExclamation = _this.m_uiContainer.getChildByName("imgExclamation");
        _this.m_uiContainer.addChild(_this.m_btnTaskExclamation);
        imgTemp = _this.m_uiContainer.getChildByName("pbCoins");
        _this.m_pbCoinBg = new CUICosmosProgressBar();
        _this.m_uiContainer.addChild(_this.m_pbCoinBg);
        _this.m_pbCoinBg.x = imgTemp.x;
        _this.m_pbCoinBg.y = imgTemp.y;
        _this.m_pbCoinBg.SetParams(imgTemp.width, imgTemp.height, 2, 0xFFFFFF, 0x000000, 0xFFFFFF);
        _this.m_uiContainer.removeChild(imgTemp);
        imgTemp = _this.m_uiContainer.getChildByName("pbDiamond");
        _this.m_pbDiamondBg = new CUICosmosProgressBar();
        _this.m_uiContainer.addChild(_this.m_pbDiamondBg);
        _this.m_pbDiamondBg.x = imgTemp.x;
        _this.m_pbDiamondBg.y = imgTemp.y;
        _this.m_pbDiamondBg.SetParams(imgTemp.width, imgTemp.height, 2, 0xFFFFFF, 0x000000, 0xFFFFFF);
        _this.m_uiContainer.removeChild(imgTemp);
        imgTemp = _this.m_uiContainer.getChildByName("pbPopulation");
        _this.m_pbPopulationBg = new CUICosmosProgressBar();
        _this.m_uiContainer.addChild(_this.m_pbPopulationBg);
        _this.m_pbPopulationBg.x = imgTemp.x;
        _this.m_pbPopulationBg.y = imgTemp.y;
        _this.m_pbPopulationBg.SetParams(imgTemp.width, imgTemp.height, 2, 0xFFFFFF, 0x000000, 0xFFFFFF);
        _this.m_uiContainer.removeChild(imgTemp);
        imgTemp = _this.m_uiContainer.getChildByName("pbCpsBg");
        _this.m_pbCpsBg = new CUICosmosProgressBar();
        _this.m_uiContainer.addChild(_this.m_pbCpsBg);
        _this.m_pbCpsBg.x = imgTemp.x;
        _this.m_pbCpsBg.y = imgTemp.y;
        _this.m_pbCpsBg.SetParams(imgTemp.width, imgTemp.height, 0, 0x000000, 0x000000, 0xFFFFFF, 0.3);
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_txtPopulation = _this.m_uiContainer.getChildByName("labelPopulation");
        _this.m_uiContainer.addChild(_this.m_txtPopulation);
        _this.m_txtCPS = _this.m_uiContainer.getChildByName("labelCPS");
        _this.m_uiContainer.addChild(_this.m_txtCPS);
        _this.m_txtDiamond = _this.m_uiContainer.getChildByName("labelDiamond");
        _this.m_uiContainer.addChild(_this.m_txtDiamond);
        _this.m_txtCoins = _this.m_uiContainer.getChildByName("labelTotoalcoin");
        _this.m_uiContainer.addChild(_this.m_txtCoins);
        imgTemp = _this.m_uiContainer.getChildByName("imgDiamond");
        _this.m_uiContainer.addChild(imgTemp);
        imgTemp = _this.m_uiContainer.getChildByName("imgCoinIcon");
        _this.m_uiContainer.addChild(imgTemp);
        imgTemp = _this.m_uiContainer.getChildByName("imgPopulation");
        _this.m_uiContainer.addChild(imgTemp);
        _this.m_uiContainer.addChild(_this.m_txtCityName);
        _this.m_txtKeysBoost = _this.m_uiContainer.getChildByName("txtKeysBoost");
        _this.m_uiContainer.addChild(_this.m_txtKeysBoost);
        var labelPerSec = _this.m_uiContainer.getChildByName("labelPerSec");
        _this.m_uiContainer.addChild(labelPerSec);
        return _this;
        /*
                this.m_btnNextCity = new CUICosmosCircle();
                this.m_uiContainer.addChild(this.m_btnNextCity);
                this.m_btnNextCity.x = 60;
                this.m_btnNextCity.y = 200;
                this.m_btnNextCity.SetParams( 45, 1, 0x363636, 0xFFFFFF, 38, 0, 0xFFFFFF, 0x00F5FF );
        */
        /*
        this.m_btnNextCity_New = new CUICosmosProgressBar();
        this.m_uiContainer.addChild(this.m_btnNextCity_New);
        this.m_btnNextCity_New.x = imgTemp.x;
        this.m_btnNextCity_New.y = imgTemp.y;
        this.m_btnNextCity_New.SetParams( 200, 30, 2, 0xFFFFFF, 0x000000, 0x00FF00 );
        this.m_btnNextCity_New.SetPercent( 0.5 );
        */
        /*
        this.m_btnCityHall_New = new CUICosmosCircle();
        this.m_uiContainer.addChild(this.m_btnCityHall_New);
        this.m_btnCityHall_New.x = imgTemp.x;
        this.m_btnCityHall_New.y = imgTemp.y;
        this.m_btnCityHall_New.SetParams(   60, 2, 0x000000, 0xFF0000, 30, 6, 0x00FF00, 0x0000FF );
        */
        // end poppin test
        /*
                this.m_btnNextCity.SetExml( "resource/assets/MyExml/CCosmosButton7.exml" );
                this.m_btnNextCity.x = imgTemp.x;
                this.m_btnNextCity.y = imgTemp.y;
                this.m_uiContainer.removeChild( imgTemp );
                this.m_btnNextCity.SetImageColor_New( 1, 30, 144, 255 );
                this.m_btnNextCity.CreateProgressBar( 2, 2, [30, 144, 255], [105, 105, 105] );
                this.m_btnNextCity.SetImageColor_New( 3, 211, 211, 211 );
                
                 this.m_btnNextCity.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_NextCity, this);
        */
    } // end contructor
    CUIMainTitle.prototype.SetPopulation = function (nPopulation) {
        this.m_txtPopulation.text = nPopulation.toString();
        var config = CConfigManager.GetCityLevelConfig(Main.s_CurTown.GetLevel());
        var fPercent = nPopulation / config.nPopulationToLevelUp;
        this.m_pbPopulation.SetPercent(fPercent);
    };
    CUIMainTitle.prototype.SetCoins = function (nCoins, bDirect) {
        if (bDirect === void 0) { bDirect = false; }
        this.m_nRealtimeCoin = nCoins;
        if (bDirect) {
            this.m_nShowCoin = nCoins;
        }
    };
    CUIMainTitle.prototype.UpdateShowCoin = function () {
        if (Main.s_CurTown == null) {
            return;
        }
        var fShowCoinChangeAmount = Main.s_CurTown.GetShowCoinChangeSpeed();
        fShowCoinChangeAmount *= CDef.s_fFixedDeltaTime;
        if (this.m_nShowCoin < this.m_nRealtimeCoin) {
            this.m_nShowCoin += fShowCoinChangeAmount;
            if (this.m_nShowCoin > this.m_nRealtimeCoin) {
                this.m_nShowCoin = this.m_nRealtimeCoin;
            }
        }
        else if (this.m_nShowCoin > this.m_nRealtimeCoin) {
            this.m_nShowCoin -= fShowCoinChangeAmount;
            if (this.m_nShowCoin < this.m_nRealtimeCoin) {
                this.m_nShowCoin = this.m_nRealtimeCoin;
            }
        }
        this.m_txtCoins.text = (Math.floor(this.m_nShowCoin)).toString();
    };
    CUIMainTitle.prototype.SetCPS = function (nCPS) {
        this.m_txtCPS.text = nCPS.toString();
    };
    CUIMainTitle.prototype.MainLoop = function () {
        this.UpdateShowCoin();
    };
    CUIMainTitle.prototype.SetDiamond = function (nDiamond) {
        this.m_txtDiamond.text = nDiamond.toString();
    };
    CUIMainTitle.prototype.SetCityName = function (szCityName) {
        this.m_txtCityName.text = szCityName;
    };
    CUIMainTitle.prototype.SetCityId = function (nTownId) {
        this.m_btnNextCity.SetImageTexture(1, CResourceManager.GetNumberTexture(nTownId));
    };
    CUIMainTitle.prototype.onButtonClick_NextCity = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.to_next_city, true);
        CUIManager.s_uiToNextCityPanel.UpdateInfo();
    };
    return CUIMainTitle;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIMainTitle.prototype, "CUIMainTitle");
//# sourceMappingURL=CUIMainTitle.js.map