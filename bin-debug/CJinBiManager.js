var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CJinBiManager = (function (_super) {
    __extends(CJinBiManager, _super);
    function CJinBiManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CJinBiManager.Init = function () {
        for (var i = 0; i < CDef.s_fJinBiAniFrameNum; i++) {
            var bmp = new egret.Bitmap(RES.getRes("jinbi_" + i + "_png"));
            CJinBiManager.s_aryJinBiAniFrames.push(bmp);
        }
    };
    CJinBiManager.GetFrameTexture = function (nFrameIndex) {
        return CJinBiManager.s_aryJinBiAniFrames[nFrameIndex].texture;
    };
    CJinBiManager.NewJinBi = function () {
        var jinbi = null;
        if (CJinBiManager.s_listRecycledJinBi.numChildren > 0) {
            jinbi = CJinBiManager.s_listRecycledJinBi.getChildAt(0);
            CJinBiManager.s_listRecycledJinBi.removeChildAt(0);
        }
        else {
            jinbi = new CJinBi();
        }
        //CJinBiManager.s_containerJinBi.addChild( jinbi );
        return jinbi;
    };
    CJinBiManager.DeleteJinBi = function (jinbi) {
        CJinBiManager.s_listRecycledJinBi.addChild(jinbi);
    };
    CJinBiManager.FixedUpdate = function () {
        return;
        for (var i = CJinBiManager.s_containerJinBi.numChildren - 1; i >= 0; i--) {
            var jinbi = CJinBiManager.s_containerJinBi.getChildAt(i);
            jinbi.Update();
        }
    };
    CJinBiManager.s_containerJinBi = new egret.DisplayObjectContainer();
    CJinBiManager.s_aryJinBiAniFrames = new Array();
    CJinBiManager.s_listRecycledJinBi = new egret.DisplayObjectContainer();
    return CJinBiManager;
}(CObj)); // end class
__reflect(CJinBiManager.prototype, "CJinBiManager");
//# sourceMappingURL=CJinBiManager.js.map