var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CCloudManager = (function (_super) {
    __extends(CCloudManager, _super);
    function CCloudManager() {
        var _this = _super.call(this) || this;
        _this.m_containerClouds = new egret.DisplayObjectContainer();
        _this.m_containerMoutains = new egret.DisplayObjectContainer();
        _this.m_containerLights = new egret.DisplayObjectContainer();
        _this.m_containerBubble = new egret.DisplayObjectContainer();
        _this.m_nStageWidth = 0;
        _this.m_nStageHeight = 0;
        _this.m_nCloudSpeed = 1;
        _this.m_nTownId = 0;
        _this.m_nMaxHeight = 0;
        _this.m_stageBg = null;
        _this.m_matrix = null;
        _this.m_fGenereateBubbleTimeElapse = 0;
        _this.addChild(_this.m_containerMoutains);
        _this.addChild(_this.m_containerClouds);
        _this.addChild(_this.m_containerLights);
        return _this;
        /// this.addChild( this.m_containerBubble );
    } // end constructor
    CCloudManager.prototype.GetBubbleContainer = function () {
        return this.m_containerBubble;
    };
    CCloudManager.prototype.Clear = function () {
        this.m_nTownId = 0;
        // 清除浮云
        for (var i = this.m_containerClouds.numChildren - 1; i >= 0; i--) {
            var cloud = this.m_containerClouds.getChildAt(i);
            CResourceManager.DeleteObj(cloud);
        }
        // 清除山体
        for (var i = this.m_containerMoutains.numChildren - 1; i >= 0; i--) {
            var moutain = this.m_containerMoutains.getChildAt(i);
            CResourceManager.DeleteObj(moutain);
        }
    };
    CCloudManager.prototype.SetStageBg = function (stageBg, matrix) {
        this.m_stageBg = stageBg;
        this.m_matrix = matrix;
    };
    CCloudManager.prototype.DrawStageBg = function (color1, color2) {
        this.m_stageBg.graphics.beginGradientFill(egret.GradientType.LINEAR, [color1, color2], [1, 1], [0, 255], this.m_matrix);
        this.m_stageBg.graphics.drawRect(0, 0, this.m_nStageHeight, this.m_nStageWidth);
        this.m_stageBg.graphics.endFill();
    };
    CCloudManager.prototype.InitLights = function (nTownId) {
        this.m_nTownId = nTownId;
        switch (this.m_nTownId) {
            case 4:
                {
                    this.InitLights_4();
                }
                break;
        } // end switch
    };
    CCloudManager.prototype.CloudLoop_1Sec = function () {
        this.GenerateBubbles();
    };
    CCloudManager.prototype.GenerateBubbles_4 = function () {
        this.m_fGenereateBubbleTimeElapse += 1;
        if (this.m_fGenereateBubbleTimeElapse < 5) {
            return;
        }
        this.m_fGenereateBubbleTimeElapse = 0;
        var bubble = CResourceManager.NewObj();
        this.m_containerBubble.addChild(bubble);
        bubble.SetTexture(CResourceManager.GetBubbleTextureByTownId(this.m_nTownId));
        bubble.x = 640 * Math.random();
        bubble.y = 1150;
        var fScale = 0.3 + 0.3 * Math.random();
        bubble.scaleX = fScale;
        bubble.scaleY = fScale;
        var fSpeed = 25 + 10 * Math.random();
        bubble.SetSpeed(fSpeed);
        bubble.SetTime(0);
        var params = bubble.GetParams();
        params["x"] = bubble.x;
    };
    CCloudManager.prototype.BubbleMoveLoop = function () {
        var fBubbleSpeed = 30;
        for (var i = this.m_containerBubble.numChildren - 1; i >= 0; i--) {
            var bubble = this.m_containerBubble.getChildAt(i);
            bubble.y -= bubble.GetSpeed() * CDef.s_fFixedDeltaTime;
            bubble.SetTime(bubble.GetTime() + 0.05);
            var params = bubble.GetParams();
            bubble.x = params["x"] + Math.sin(bubble.GetTime()) * 10;
            if (bubble.y < 400) {
                var fAlpha = (bubble.y - 200) / 200;
                bubble.alpha = fAlpha;
            }
            if (bubble.y < 200) {
                CResourceManager.DeleteObj(bubble);
            }
        }
    };
    // 气泡
    CCloudManager.prototype.GenerateBubbles = function () {
        switch (this.m_nTownId) {
            case 4:
                {
                    this.GenerateBubbles_4();
                }
                break;
        }
    };
    // 光效
    CCloudManager.prototype.InitLights_4 = function () {
        for (var i = 0; i < 3; i++) {
            var nLightWidth = 1200;
            var nLightHeight = 200;
            var light = new egret.Shape();
            switch (i) {
                case 0:
                    {
                        nLightHeight = 200;
                    }
                    break;
                case 1:
                    {
                        nLightHeight = 100;
                        light.x = 30;
                    }
                    break;
                case 2:
                    {
                        nLightHeight = 400;
                        light.x = 100;
                    }
                    break;
            } // end switch
            light.y = -50;
            var matrix = new egret.Matrix();
            matrix.createGradientBox(nLightWidth, nLightHeight);
            var fAlpha = 0.3;
            this.m_containerLights.addChild(light);
            light.graphics.beginGradientFill(egret.GradientType.LINEAR, [0x01b6ff, 0x01b6ff], [fAlpha, 0], [0, 255], this.m_matrix);
            light.graphics.drawRect(0, 0, nLightWidth, nLightHeight);
            light.graphics.endFill();
            light.rotation = 60;
        }
    };
    CCloudManager.prototype.InitClouds = function (nTownId, stageWidth, stageHeight) {
        this.m_nTownId = nTownId;
        this.m_nStageWidth = stageWidth;
        this.m_nStageHeight = stageHeight;
        switch (this.m_nTownId) {
            case 1:
                {
                    this.InitClouds_1();
                    this.DrawStageBg(0x54c5f1, 0x81aee7);
                }
                break;
            case 2:
                {
                    this.InitClouds_2();
                    this.InitMoutains_2();
                    this.DrawStageBg(0xffc283, 0xff9992);
                }
                break;
            case 3:
                {
                    this.InitClouds_3();
                    this.InitMoutains_3();
                    this.DrawStageBg(0x71e09c, 0x0dd1bd);
                }
                break;
            case 4:
                {
                    this.DrawStageBg(0x75db0, 0x092c52);
                    this.InitClounds_4();
                    this.InitLights_4();
                }
                break;
        } // end switch
    };
    CCloudManager.prototype.InitMoutains_2 = function () {
        for (var i = 0; i < 3; i++) {
            var moutain = CResourceManager.NewObj();
            this.m_containerMoutains.addChild(moutain);
            moutain.SetTexture(CResourceManager.GetMoutainTextureByTownId(this.m_nTownId));
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * i;
            moutain.y = this.m_nStageHeight;
        }
    };
    CCloudManager.prototype.InitMoutains_3 = function () {
        for (var i = 0; i < 3; i++) {
            var moutain = CResourceManager.NewObj();
            this.m_containerMoutains.addChild(moutain);
            moutain.SetTexture(CResourceManager.GetMoutainTextureByTownId(this.m_nTownId));
            moutain.anchorOffsetX = 0;
            moutain.anchorOffsetY = moutain.height;
            moutain.x = moutain.width * i;
            moutain.y = this.m_nStageHeight;
        }
    };
    CCloudManager.prototype.InitClouds_2 = function () {
        for (var i = 0; i < 6; i++) {
            var cloud = CResourceManager.NewObj();
            this.m_containerClouds.addChild(cloud);
            var nCloudSubId = 1;
            if (i % 2 == 0) {
                nCloudSubId = 2;
            }
            cloud.SetTexture(CResourceManager.GetCloudTextureByTownId(this.m_nTownId, nCloudSubId));
            var scale = 0.2 + 0.2 * Math.random();
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random();
            cloud.y = (this.m_nStageHeight - 200) * Math.random();
            cloud.SetSpeed(this.m_nCloudSpeed * scale);
            var fTemp = Math.random();
            if (fTemp > 0.5) {
                cloud.alpha = 0.6;
            }
        }
    };
    CCloudManager.prototype.GetCurAlphaByPos_4 = function (pos) {
        var max_alpha = 1;
        var min_alpha = 0.15;
        var max_pos_y = this.m_nMaxHeight;
        var min_pos_y = 0;
        var cur_pos = pos - min_pos_y;
        var total_dis = max_pos_y - min_pos_y;
        var total_alpha = max_alpha - min_alpha;
        var cur_alpha = min_alpha + (cur_pos / total_dis) * total_alpha;
        return cur_alpha;
    };
    CCloudManager.prototype.InitClounds_4 = function () {
        this.m_nMaxHeight = this.m_nStageHeight - 150;
        for (var i = 0; i < 15; i++) {
            var cloud = CResourceManager.NewObj();
            this.m_containerClouds.addChild(cloud);
            cloud.SetTexture(CResourceManager.GetCloudTextureByTownId(this.m_nTownId));
            var scale = 0.1 + 0.3 * Math.random();
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random();
            cloud.y = this.m_nMaxHeight * Math.random();
            cloud.SetSpeed(2 * scale);
            cloud.alpha = this.GetCurAlphaByPos_4(cloud.y);
        } //  end for
    };
    CCloudManager.prototype.InitClouds_3 = function () {
        for (var i = 0; i < 6; i++) {
            var cloud = CResourceManager.NewObj();
            this.m_containerClouds.addChild(cloud);
            var nCloudSubId = 1;
            if (i % 2 == 0) {
                nCloudSubId = 2;
            }
            cloud.SetTexture(CResourceManager.GetCloudTextureByTownId(this.m_nTownId - 1, nCloudSubId));
            var scale = 0.2 + 0.2 * Math.random();
            cloud.scaleX = scale;
            cloud.scaleY = scale;
            cloud.anchorOffsetX = 0;
            cloud.anchorOffsetY = 0;
            cloud.x = this.m_nStageWidth * Math.random();
            cloud.y = (this.m_nStageHeight - 300) * Math.random();
            cloud.SetSpeed(this.m_nCloudSpeed * scale);
            var fTemp = Math.random();
            if (fTemp > 0.5) {
                cloud.alpha = 0.6;
            }
        }
    };
    CCloudManager.prototype.InitClouds_1 = function () {
        for (var i = 0; i < 10; i++) {
            this.GenerateCloud_1();
        }
    };
    CCloudManager.prototype.GenerateCloud_1 = function () {
        var cloud = CResourceManager.NewObj();
        this.m_containerClouds.addChild(cloud);
        cloud.SetTexture(CResourceManager.GetCloudTextureByTownId(this.m_nTownId));
        var scale = 0.1 + 0.2 * Math.random();
        cloud.scaleX = scale;
        cloud.scaleY = scale;
        cloud.anchorOffsetX = 0;
        cloud.anchorOffsetY = 0;
        cloud.x = this.m_nStageWidth * Math.random();
        cloud.y = this.m_nStageHeight * Math.random();
        cloud.SetSpeed(this.m_nCloudSpeed * scale);
        var fTemp = Math.random();
        if (fTemp > 0.5) {
            cloud.alpha = 0.6;
        }
    };
    CCloudManager.prototype.CloudLoop = function () {
        if (this.m_nTownId == 0) {
            return;
        }
        this.BubbleMoveLoop();
        switch (this.m_nTownId) {
            case 1:
                {
                    this.CloudLoop_1();
                }
                break;
            case 2:
                {
                    this.CloudLoop_2();
                }
                break;
            case 3:
                {
                    this.CloudLoop_3();
                }
                break;
            case 4:
                {
                    this.CloudLoop_4();
                }
                break;
        } // end switch
    };
    CCloudManager.prototype.CloudLoop_3 = function () {
        for (var i = this.m_containerClouds.numChildren - 1; i >= 0; i--) {
            var cloud = this.m_containerClouds.getChildAt(i);
            cloud.x -= cloud.GetSpeed();
            if (cloud.x < -cloud.width * cloud.scaleX) {
                cloud.x = this.m_nStageWidth;
                cloud.y = (this.m_nStageHeight - 200) * Math.random();
            }
        } // end for  
    };
    CCloudManager.prototype.CloudLoop_4 = function () {
        for (var i = this.m_containerClouds.numChildren - 1; i >= 0; i--) {
            var cloud = this.m_containerClouds.getChildAt(i);
            cloud.x -= cloud.GetSpeed();
            if (cloud.x < -cloud.width * cloud.scaleX) {
                cloud.x = this.m_nStageWidth;
                cloud.y = this.m_nMaxHeight * Math.random();
                cloud.alpha = this.GetCurAlphaByPos_4(cloud.y);
            }
        } // end for  
    };
    CCloudManager.prototype.CloudLoop_2 = function () {
        for (var i = this.m_containerClouds.numChildren - 1; i >= 0; i--) {
            var cloud = this.m_containerClouds.getChildAt(i);
            cloud.x -= cloud.GetSpeed();
            if (cloud.x < -cloud.width * cloud.scaleX) {
                cloud.x = this.m_nStageWidth;
                cloud.y = (this.m_nStageHeight - 200) * Math.random();
            }
        } // end for  
    };
    CCloudManager.prototype.CloudLoop_1 = function () {
        for (var i = this.m_containerClouds.numChildren - 1; i >= 0; i--) {
            var cloud = this.m_containerClouds.getChildAt(i);
            cloud.x -= cloud.GetSpeed();
            if (cloud.x < -cloud.width * cloud.scaleX) {
                cloud.x = this.m_nStageWidth;
                cloud.y = this.m_nStageHeight * Math.random();
            }
        } // end for
    };
    return CCloudManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CCloudManager.prototype, "CCloudManager");
//# sourceMappingURL=CCloudManager.js.map