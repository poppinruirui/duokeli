var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CAutomobileManager = (function (_super) {
    __extends(CAutomobileManager, _super);
    function CAutomobileManager() {
        return _super.call(this) || this;
    }
    CAutomobileManager.NewAutomobile = function () {
        return new CAutomobile();
    };
    CAutomobileManager.ChangeCarSpeedDueToDoubleTime = function (nDoubleTime) {
        CDef.s_fAutomobileMoveDirX *= nDoubleTime;
        CDef.s_fAutomobileMoveDirY *= nDoubleTime;
    };
    return CAutomobileManager;
}(egret.DisplayObjectContainer)); // end classs
__reflect(CAutomobileManager.prototype, "CAutomobileManager");
//# sourceMappingURL=CAutomobileManager.js.map