//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the 
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * 创建场景界面
         * Create scene interface
         */
        _this.m_containerUI = new egret.DisplayObjectContainer();
        _this.m_listOperate = new eui.List();
        _this.m_timerMainLoop = new egret.Timer(CDef.s_fFixedDeltaTime * 1000, 0);
        _this.m_timerMainLoop_1 = new egret.Timer(1000, 0);
        _this.m_timerMainLoop_60 = new egret.Timer(60000, 0);
        _this.m_CloudManager = new CCloudManager();
        _this.m_bDraggingStage = false;
        _this.m_objectLastMousePosOnStage = new Object();
        _this.m_objectMouseDownPos = new Object();
        return _this;
    }
    Main.prototype.createChildren = function () {
        _super.prototype.createChildren.call(this);
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
        });
        egret.lifecycle.onPause = function () {
            egret.ticker.pause();
        };
        egret.lifecycle.onResume = function () {
            egret.ticker.resume();
        };
        //inject the custom material parser
        //注入自定义的素材解析器
        var assetAdapter = new AssetAdapter();
        egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());
        this.runGame().catch(function (e) {
            console.log(e);
        });
    };
    Main.prototype.runGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, userInfo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadResource()];
                    case 1:
                        _a.sent();
                        this.createGameScene();
                        return [4 /*yield*/, RES.getResAsync("description_json")
                            //   this.startAnimation(result);
                        ];
                    case 2:
                        result = _a.sent();
                        //   this.startAnimation(result);
                        return [4 /*yield*/, platform.login()];
                    case 3:
                        //   this.startAnimation(result);
                        _a.sent();
                        return [4 /*yield*/, platform.getUserInfo()];
                    case 4:
                        userInfo = _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadResource = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        //  const loadingView = new LoadingUI();
                        //this.stage.addChild(loadingView);
                        return [4 /*yield*/, RES.loadConfig("resource/default.res.json", "resource/")];
                    case 1:
                        //  const loadingView = new LoadingUI();
                        //this.stage.addChild(loadingView);
                        _a.sent();
                        return [4 /*yield*/, this.loadTheme()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, RES.loadGroup("preload", 0, null)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadTheme = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            var theme = new eui.Theme("resource/default.thm.json", _this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, function () {
                resolve();
            }, _this);
        });
    };
    Main.prototype.CreateUI = function () {
        this.addChild(this.m_containerUI);
        Main.s_panelMsgBox = new CUIMsgBox();
        this.m_containerUI.addChild(Main.s_panelMsgBox);
        Main.s_panelMsgBox.x = 50;
        Main.s_panelMsgBox.y = 200;
        Main.s_panelMsgBox.visible = false;
        CUIManager.s_containerUIs = new egret.DisplayObjectContainer();
        // this.addChild( CUIManager.s_containerUIs );
        CUIManager.Init();
        return this.m_containerUI;
    };
    Main.SeekResByName = function (szContent) {
        Main.s_aryMatchedResName.length = 0;
        for (var i = 0; i < Main.s_aryPreloadResName.length; i++) {
            var szResName = Main.s_aryPreloadResName[i];
            if (szResName.search(szContent) != -1) {
                Main.s_aryMatchedResName.push(szResName);
            }
        }
        return Main.s_aryMatchedResName;
    };
    Main.GetTownByName = function (szName) {
        return Main.s_dicTowns[szName];
    };
    // 一秒钟一次的轮询
    Main.prototype.FixedUpdate_1 = function () {
        if (Main.s_CurTown) {
            Main.s_CurTown.UpdateTotalCoins();
        }
        CPlayer.MainLoop_1();
        CUIManager.MainLoop_1();
        if (this.m_CloudManager) {
            this.m_CloudManager.CloudLoop_1Sec();
        }
    };
    // 一分钟一次的轮询
    Main.prototype.FixedUpdate_60 = function () {
    };
    // 最小间隔的轮询，以CDef中的定义为准。目前是25毫秒
    // 注意，这种轮询一定要谨慎，不然可能有性能问题
    Main.prototype.FixedUpdate = function () {
        if (Main.s_CurTown) {
            Main.s_CurTown.FixedUpdate();
        }
        CJinBiManager.FixedUpdate();
        CTiaoZiManager.FixedUpdate();
        CUIManager.MainLoop();
        if (this.m_CloudManager) {
            this.m_CloudManager.CloudLoop();
        }
    };
    Main.prototype.createGameScene = function () {
        //   console.log( this.stage.stageWidth + "_" + this.stage.stageHeight );
        CDataManager.LoadMapData();
        var stageBg = new egret.Shape();
        this.addChild(stageBg);
        /*
        stageBg.graphics.beginFill( 0x4169E1, 1);
        stageBg.graphics.drawRect( 0, 0, this.stage.stageWidth, this.stage.stageHeight );
        stageBg.graphics.endFill();
        */
        // 绘制渐变色
        var matrix = new egret.Matrix();
        matrix.createGradientBox(this.stage.stageHeight, this.stage.stageWidth);
        //stageBg.graphics.beginGradientFill( egret.GradientType.LINEAR, [0x54c5f1, 0x81aee7], [1,1], [0,255],matrix );
        //stageBg.graphics.drawRect( 0, 0, this.stage.stageHeight, this.stage.stageWidth  );
        //stageBg.graphics.endFill();
        stageBg.rotation = 90;
        stageBg.x = this.stage.stageWidth;
        stageBg.y = 0;
        // end 绘制渐变色
        CSoundManager.Init();
        CJinBiManager.Init();
        CColorFucker.Init();
        this.m_timerMainLoop.addEventListener(egret.TimerEvent.TIMER, this.FixedUpdate, this);
        this.m_timerMainLoop.start();
        this.m_timerMainLoop_1.addEventListener(egret.TimerEvent.TIMER, this.FixedUpdate_1, this);
        this.m_timerMainLoop_1.start();
        this.m_timerMainLoop_60.addEventListener(egret.TimerEvent.TIMER, this.FixedUpdate_60, this);
        this.m_timerMainLoop_60.start();
        var fDi = Math.sqrt(CDef.s_nTileWidth * CDef.s_nTileWidth + CDef.s_nTileHeight * CDef.s_nTileHeight);
        var fCos = CDef.s_nTileWidth / fDi;
        var fSin = CDef.s_nTileHeight / fDi;
        CDef.s_fAutomobileMoveDirX = fCos * CDef.s_fAutomobileMoveSpeed;
        CDef.s_fAutomobileMoveDirY = fSin * CDef.s_fAutomobileMoveSpeed;
        // 配置文件管理器
        CConfigManager.Init();
        CTiaoZiManager.Init(); // 跳字管理器 初始化
        // 资源管理器，初始化
        CResourceManager.Init();
        var aryPreloadResources = RES.getGroupByName("preload");
        for (var i = 0; i < aryPreloadResources.length; i++) {
            var item = aryPreloadResources[i];
            Main.s_aryPreloadResName.push(item.name);
        }
        /*
                let button = new eui.Button();
                button.label = "Click!";
                button.horizontalCenter = 0;
                button.verticalCenter = 0;
                this.addChild(button);
                button.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick, this);
                */
        this.m_CloudManager = new CCloudManager();
        // this.m_CloudManager.InitClouds(1, this.stage.stageWidth, this.stage.stageHeight);
        this.addChild(this.m_CloudManager);
        this.m_CloudManager.SetStageBg(stageBg, matrix);
        // 创建ui
        var containerUI = this.CreateUI();
        // 创建一个城镇
        Main.s_CurTown = new CTown(1, this.stage.stageWidth, this.stage.stageHeight);
        this.addChild(Main.s_CurTown);
        Main.s_CurTown.SetCloudManager(this.m_CloudManager);
        Main.s_CurTown.LoadTownData(1);
        // Main.s_CurTown.addEventListener( egret.TouchEvent.TOUCH_BEGIN, this.handleTownDown, this );
        //  Main.s_CurTown.addEventListener( egret.TouchEvent.TOUCH_END, this.handleTownUp, this );
        //Main.s_CurTown.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.handleTownMove, this )
        // poppin test
        this.stage.addEventListener(egret.FocusEvent.ACTIVATE, this.onActivate, this);
        this.stage.addEventListener(egret.FocusEvent.DEACTIVATE, this.onDeactivate, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTownDown, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.handleStageUp, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.handleStageMove, this);
        // Main.s_CurTown.SetCoins( 50000, true );
        // poppin test
        CUIManager.s_uiCityHall.UnlockItems(1);
        this.addChild(this.m_CloudManager.GetBubbleContainer());
        // UI在一切之上，所以最后add进显示列表
        this.addChild(containerUI);
        this.addChild(CUIManager.s_containerUIs);
    }; // end CreateGameScene
    Main.prototype.onDeactivate = function (evt) {
        // console.log( "游戏失去焦点" );
        if (Main.s_CurTown) {
            Main.s_CurTown.ProcessBankSaving_Begin();
        }
    };
    Main.prototype.onActivate = function (evt) {
        // console.log( "游戏获得焦点" );
        if (Main.s_CurTown) {
            Main.s_CurTown.ProcessBankSaving_End();
        }
    };
    Main.prototype.handleTownDown = function (evt) {
        CSoundManager.PlayBMG();
        this.m_bDraggingStage = true;
        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
        this.m_objectLastMousePosOnStage["y"] = evt.stageY;
        this.m_objectMouseDownPos["x"] = evt.stageX;
        this.m_objectMouseDownPos["y"] = evt.stageY;
        // poppin test 8.6
        if (Main.s_CurTown) {
            Main.s_CurTown.ProcessClick(evt.stageX, evt.stageY);
        }
    };
    Main.prototype.handleTownUp = function (evt) {
        //this.m_timerDragScene.stop();
        this.m_bDraggingStage = false;
        var huadongDeltaX = Math.abs(this.m_objectMouseDownPos["x"] - evt.stageX);
        var huadongDeltaY = Math.abs(this.m_objectMouseDownPos["y"] - evt.stageY);
        if ((huadongDeltaX >= 20) || (huadongDeltaY >= 20)) {
            //  console.log( "滑动了，不算点击：" );
            return;
        }
        //  console.log( "有点击" );
        //   Main.s_CurTown.Processtap( evt.stageX, evt.stageY );
    };
    Main.prototype.handleTownMove = function (evt) {
        return; // poppin test
        var deltaX = evt.stageX - this.m_objectLastMousePosOnStage["x"];
        var deltaY = evt.stageY - this.m_objectLastMousePosOnStage["y"];
        // console.log( "Delat: " + deltaX + "_" + deltaY );  
        if (Main.s_CurTown != null) {
            Main.s_CurTown.DragScene(deltaX, deltaY);
        }
        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
        this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    };
    Main.prototype.handleStageDown = function (evt) {
        // this.m_timerDragScene.start();
        this.m_bDraggingStage = true;
        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
        this.m_objectLastMousePosOnStage["y"] = evt.stageY;
        this.m_objectMouseDownPos["x"] = evt.stageX;
        this.m_objectMouseDownPos["y"] = evt.stageY;
    };
    Main.prototype.handleStageUp = function (evt) {
        //this.m_timerDragScene.stop();
        this.m_bDraggingStage = false;
        var huadongDeltaX = Math.abs(this.m_objectMouseDownPos["x"] - evt.stageX);
        var huadongDeltaY = Math.abs(this.m_objectMouseDownPos["y"] - evt.stageY);
        if ((huadongDeltaX >= 20) || (huadongDeltaY >= 20)) {
            //  console.log( "滑动了，不算点击：" );
            return;
        }
        //  console.log( "有点击" );
        // Main.s_CurTown.Processtap( evt.stageX, evt.stageY );
    };
    Main.prototype.handleStageMove = function (evt) {
        var deltaX = evt.stageX - this.m_objectLastMousePosOnStage["x"];
        var deltaY = evt.stageY - this.m_objectLastMousePosOnStage["y"];
        // console.log( "Delat: " + deltaX + "_" + deltaY );  
        if (Main.s_CurTown != null) {
            Main.s_CurTown.DragScene(deltaX, deltaY);
        }
        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
        this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    Main.prototype.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    Main.prototype.startAnimation = function (result) {
        var _this = this;
        var parser = new egret.HtmlTextParser();
        var textflowArr = result.map(function (text) { return parser.parse(text); });
        var textfield = this.textfield;
        var count = -1;
        var change = function () {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            var textFlow = textflowArr[count];
            // 切换描述内容
            // Switch to described content
            textfield.textFlow = textFlow;
            var tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, _this);
        };
        change();
    };
    /**
     * 点击按钮
     * Click the button
     */
    Main.prototype.onButtonClick = function (e) {
        var panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    };
    Main.s_aryPreloadResName = new Array();
    Main.s_aryMatchedResName = new Array();
    Main.s_dicTowns = new Object();
    return Main;
}(eui.UILayer));
__reflect(Main.prototype, "Main");
//# sourceMappingURL=Main.js.map