template <typename T> void RegisterClass();
template <typename T> void RegisterStrippedType(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_CloudWebServices();
	RegisterModule_CloudWebServices();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_Video();
	RegisterModule_Video();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_ImageConversion();
	RegisterModule_ImageConversion();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

	void RegisterModule_GameCenter();
	RegisterModule_GameCenter();

	void RegisterModule_Core();
	RegisterModule_Core();

	void RegisterModule_SharedInternals();
	RegisterModule_SharedInternals();

}

class EditorExtension; template <> void RegisterClass<EditorExtension>();
namespace Unity { class Component; } template <> void RegisterClass<Unity::Component>();
class Behaviour; template <> void RegisterClass<Behaviour>();
class Animation; 
class Animator; template <> void RegisterClass<Animator>();
class AudioBehaviour; template <> void RegisterClass<AudioBehaviour>();
class AudioListener; template <> void RegisterClass<AudioListener>();
class AudioSource; template <> void RegisterClass<AudioSource>();
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterClass<Camera>();
namespace UI { class Canvas; } template <> void RegisterClass<UI::Canvas>();
namespace UI { class CanvasGroup; } template <> void RegisterClass<UI::CanvasGroup>();
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterClass<Collider2D>();
class BoxCollider2D; template <> void RegisterClass<BoxCollider2D>();
class CapsuleCollider2D; 
class CircleCollider2D; template <> void RegisterClass<CircleCollider2D>();
class CompositeCollider2D; 
class EdgeCollider2D; template <> void RegisterClass<EdgeCollider2D>();
class PolygonCollider2D; template <> void RegisterClass<PolygonCollider2D>();
class TilemapCollider2D; 
class ConstantForce; 
class Effector2D; 
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; 
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; template <> void RegisterClass<FlareLayer>();
class GUIElement; template <> void RegisterClass<GUIElement>();
namespace TextRenderingPrivate { class GUIText; } 
class GUITexture; 
class GUILayer; template <> void RegisterClass<GUILayer>();
class GridLayout; 
class Grid; 
class Tilemap; 
class Halo; 
class HaloLayer; 
class Joint2D; template <> void RegisterClass<Joint2D>();
class AnchoredJoint2D; template <> void RegisterClass<AnchoredJoint2D>();
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; template <> void RegisterClass<HingeJoint2D>();
class SliderJoint2D; 
class SpringJoint2D; 
class WheelJoint2D; 
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterClass<Light>();
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterClass<MonoBehaviour>();
class NavMeshAgent; 
class NavMeshObstacle; 
class NetworkView; 
class OffMeshLink; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class PlayableDirector; 
class Projector; 
class ReflectionProbe; 
class Skybox; 
class SortingGroup; template <> void RegisterClass<SortingGroup>();
class Terrain; 
class VideoPlayer; template <> void RegisterClass<VideoPlayer>();
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterClass<UI::CanvasRenderer>();
class Collider; template <> void RegisterClass<Collider>();
class BoxCollider; template <> void RegisterClass<BoxCollider>();
class CapsuleCollider; 
class CharacterController; template <> void RegisterClass<CharacterController>();
class MeshCollider; 
class SphereCollider; template <> void RegisterClass<SphereCollider>();
class TerrainCollider; 
class WheelCollider; 
namespace Unity { class Joint; } template <> void RegisterClass<Unity::Joint>();
namespace Unity { class CharacterJoint; } 
namespace Unity { class ConfigurableJoint; } 
namespace Unity { class FixedJoint; } 
namespace Unity { class HingeJoint; } template <> void RegisterClass<Unity::HingeJoint>();
namespace Unity { class SpringJoint; } 
class LODGroup; 
class MeshFilter; template <> void RegisterClass<MeshFilter>();
class OcclusionArea; 
class OcclusionPortal; 
class ParticleAnimator; 
class ParticleEmitter; 
class EllipsoidParticleEmitter; 
class MeshParticleEmitter; 
class ParticleSystem; 
class Renderer; template <> void RegisterClass<Renderer>();
class BillboardRenderer; 
class LineRenderer; template <> void RegisterClass<LineRenderer>();
class MeshRenderer; template <> void RegisterClass<MeshRenderer>();
class ParticleRenderer; 
class ParticleSystemRenderer; 
class SkinnedMeshRenderer; 
class SpriteMask; 
class SpriteRenderer; template <> void RegisterClass<SpriteRenderer>();
class TilemapRenderer; 
class TrailRenderer; 
class Rigidbody; template <> void RegisterClass<Rigidbody>();
class Rigidbody2D; template <> void RegisterClass<Rigidbody2D>();
namespace TextRenderingPrivate { class TextMesh; } template <> void RegisterClass<TextRenderingPrivate::TextMesh>();
class Transform; template <> void RegisterClass<Transform>();
namespace UI { class RectTransform; } template <> void RegisterClass<UI::RectTransform>();
class Tree; 
class WorldAnchor; 
class WorldParticleCollider; 
class GameObject; template <> void RegisterClass<GameObject>();
class NamedObject; template <> void RegisterClass<NamedObject>();
class AssetBundle; template <> void RegisterClass<AssetBundle>();
class AssetBundleManifest; template <> void RegisterClass<AssetBundleManifest>();
class ScriptedImporter; 
class AudioMixer; 
class AudioMixerController; 
class AudioMixerGroup; 
class AudioMixerGroupController; 
class AudioMixerSnapshot; 
class AudioMixerSnapshotController; 
class Avatar; 
class AvatarMask; 
class BillboardAsset; 
class ComputeShader; 
class Flare; 
namespace TextRendering { class Font; } template <> void RegisterClass<TextRendering::Font>();
class GameObjectRecorder; 
class LightProbes; template <> void RegisterClass<LightProbes>();
class Material; template <> void RegisterClass<Material>();
class ProceduralMaterial; 
class Mesh; template <> void RegisterClass<Mesh>();
class Motion; template <> void RegisterClass<Motion>();
class AnimationClip; template <> void RegisterClass<AnimationClip>();
class PreviewAnimationClip; 
class NavMeshData; 
class OcclusionCullingData; 
class PhysicMaterial; 
class PhysicsMaterial2D; 
class PreloadData; template <> void RegisterClass<PreloadData>();
class RuntimeAnimatorController; template <> void RegisterClass<RuntimeAnimatorController>();
class AnimatorController; 
class AnimatorOverrideController; 
class SampleClip; template <> void RegisterClass<SampleClip>();
class AudioClip; template <> void RegisterClass<AudioClip>();
class Shader; template <> void RegisterClass<Shader>();
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterClass<Sprite>();
class SpriteAtlas; 
class SubstanceArchive; 
class TerrainData; 
class TextAsset; template <> void RegisterClass<TextAsset>();
class CGProgram; 
class MonoScript; template <> void RegisterClass<MonoScript>();
class Texture; template <> void RegisterClass<Texture>();
class BaseVideoTexture; 
class MovieTexture; 
class WebCamTexture; 
class CubemapArray; 
class LowerResBlitTexture; template <> void RegisterClass<LowerResBlitTexture>();
class ProceduralTexture; 
class RenderTexture; template <> void RegisterClass<RenderTexture>();
class CustomRenderTexture; 
class SparseTexture; 
class Texture2D; template <> void RegisterClass<Texture2D>();
class Cubemap; template <> void RegisterClass<Cubemap>();
class Texture2DArray; template <> void RegisterClass<Texture2DArray>();
class Texture3D; template <> void RegisterClass<Texture3D>();
class VideoClip; template <> void RegisterClass<VideoClip>();
class GameManager; template <> void RegisterClass<GameManager>();
class GlobalGameManager; template <> void RegisterClass<GlobalGameManager>();
class AudioManager; template <> void RegisterClass<AudioManager>();
class BuildSettings; template <> void RegisterClass<BuildSettings>();
class CloudWebServicesManager; template <> void RegisterClass<CloudWebServicesManager>();
class CrashReportManager; 
class DelayedCallManager; template <> void RegisterClass<DelayedCallManager>();
class GraphicsSettings; template <> void RegisterClass<GraphicsSettings>();
class InputManager; template <> void RegisterClass<InputManager>();
class MasterServerInterface; template <> void RegisterClass<MasterServerInterface>();
class MonoManager; template <> void RegisterClass<MonoManager>();
class NavMeshProjectSettings; 
class NetworkManager; template <> void RegisterClass<NetworkManager>();
class PerformanceReportingManager; 
class Physics2DSettings; template <> void RegisterClass<Physics2DSettings>();
class PhysicsManager; template <> void RegisterClass<PhysicsManager>();
class PlayerSettings; template <> void RegisterClass<PlayerSettings>();
class QualitySettings; template <> void RegisterClass<QualitySettings>();
class ResourceManager; template <> void RegisterClass<ResourceManager>();
class RuntimeInitializeOnLoadManager; template <> void RegisterClass<RuntimeInitializeOnLoadManager>();
class ScriptMapper; template <> void RegisterClass<ScriptMapper>();
class TagManager; template <> void RegisterClass<TagManager>();
class TimeManager; template <> void RegisterClass<TimeManager>();
class UnityAnalyticsManager; 
class UnityConnectSettings; template <> void RegisterClass<UnityConnectSettings>();
class LevelGameManager; template <> void RegisterClass<LevelGameManager>();
class LightmapSettings; template <> void RegisterClass<LightmapSettings>();
class NavMeshSettings; 
class OcclusionCullingSettings; 
class RenderSettings; template <> void RegisterClass<RenderSettings>();
class RenderPassAttachment; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 92 non stripped classes
	//0. Rigidbody
	RegisterClass<Rigidbody>();
	//1. Unity::Component
	RegisterClass<Unity::Component>();
	//2. EditorExtension
	RegisterClass<EditorExtension>();
	//3. Unity::Joint
	RegisterClass<Unity::Joint>();
	//4. Unity::HingeJoint
	RegisterClass<Unity::HingeJoint>();
	//5. BoxCollider
	RegisterClass<BoxCollider>();
	//6. Collider
	RegisterClass<Collider>();
	//7. SphereCollider
	RegisterClass<SphereCollider>();
	//8. CharacterController
	RegisterClass<CharacterController>();
	//9. AssetBundle
	RegisterClass<AssetBundle>();
	//10. NamedObject
	RegisterClass<NamedObject>();
	//11. AssetBundleManifest
	RegisterClass<AssetBundleManifest>();
	//12. Behaviour
	RegisterClass<Behaviour>();
	//13. Camera
	RegisterClass<Camera>();
	//14. GameObject
	RegisterClass<GameObject>();
	//15. RenderSettings
	RegisterClass<RenderSettings>();
	//16. LevelGameManager
	RegisterClass<LevelGameManager>();
	//17. GameManager
	RegisterClass<GameManager>();
	//18. QualitySettings
	RegisterClass<QualitySettings>();
	//19. GlobalGameManager
	RegisterClass<GlobalGameManager>();
	//20. Renderer
	RegisterClass<Renderer>();
	//21. LineRenderer
	RegisterClass<LineRenderer>();
	//22. GUIElement
	RegisterClass<GUIElement>();
	//23. GUILayer
	RegisterClass<GUILayer>();
	//24. MonoBehaviour
	RegisterClass<MonoBehaviour>();
	//25. Shader
	RegisterClass<Shader>();
	//26. Material
	RegisterClass<Material>();
	//27. Sprite
	RegisterClass<Sprite>();
	//28. SpriteRenderer
	RegisterClass<SpriteRenderer>();
	//29. TextAsset
	RegisterClass<TextAsset>();
	//30. Texture
	RegisterClass<Texture>();
	//31. Texture2D
	RegisterClass<Texture2D>();
	//32. Texture3D
	RegisterClass<Texture3D>();
	//33. RenderTexture
	RegisterClass<RenderTexture>();
	//34. Transform
	RegisterClass<Transform>();
	//35. UI::RectTransform
	RegisterClass<UI::RectTransform>();
	//36. SortingGroup
	RegisterClass<SortingGroup>();
	//37. MeshFilter
	RegisterClass<MeshFilter>();
	//38. Mesh
	RegisterClass<Mesh>();
	//39. AnimationClip
	RegisterClass<AnimationClip>();
	//40. Motion
	RegisterClass<Motion>();
	//41. Animator
	RegisterClass<Animator>();
	//42. TextRenderingPrivate::TextMesh
	RegisterClass<TextRenderingPrivate::TextMesh>();
	//43. TextRendering::Font
	RegisterClass<TextRendering::Font>();
	//44. UI::Canvas
	RegisterClass<UI::Canvas>();
	//45. UI::CanvasGroup
	RegisterClass<UI::CanvasGroup>();
	//46. UI::CanvasRenderer
	RegisterClass<UI::CanvasRenderer>();
	//47. AudioClip
	RegisterClass<AudioClip>();
	//48. SampleClip
	RegisterClass<SampleClip>();
	//49. AudioListener
	RegisterClass<AudioListener>();
	//50. AudioBehaviour
	RegisterClass<AudioBehaviour>();
	//51. AudioSource
	RegisterClass<AudioSource>();
	//52. Rigidbody2D
	RegisterClass<Rigidbody2D>();
	//53. Collider2D
	RegisterClass<Collider2D>();
	//54. EdgeCollider2D
	RegisterClass<EdgeCollider2D>();
	//55. PolygonCollider2D
	RegisterClass<PolygonCollider2D>();
	//56. CircleCollider2D
	RegisterClass<CircleCollider2D>();
	//57. BoxCollider2D
	RegisterClass<BoxCollider2D>();
	//58. Joint2D
	RegisterClass<Joint2D>();
	//59. AnchoredJoint2D
	RegisterClass<AnchoredJoint2D>();
	//60. HingeJoint2D
	RegisterClass<HingeJoint2D>();
	//61. VideoPlayer
	RegisterClass<VideoPlayer>();
	//62. MeshRenderer
	RegisterClass<MeshRenderer>();
	//63. RuntimeAnimatorController
	RegisterClass<RuntimeAnimatorController>();
	//64. PreloadData
	RegisterClass<PreloadData>();
	//65. Cubemap
	RegisterClass<Cubemap>();
	//66. Texture2DArray
	RegisterClass<Texture2DArray>();
	//67. LowerResBlitTexture
	RegisterClass<LowerResBlitTexture>();
	//68. BuildSettings
	RegisterClass<BuildSettings>();
	//69. DelayedCallManager
	RegisterClass<DelayedCallManager>();
	//70. GraphicsSettings
	RegisterClass<GraphicsSettings>();
	//71. InputManager
	RegisterClass<InputManager>();
	//72. PlayerSettings
	RegisterClass<PlayerSettings>();
	//73. ResourceManager
	RegisterClass<ResourceManager>();
	//74. RuntimeInitializeOnLoadManager
	RegisterClass<RuntimeInitializeOnLoadManager>();
	//75. ScriptMapper
	RegisterClass<ScriptMapper>();
	//76. TagManager
	RegisterClass<TagManager>();
	//77. TimeManager
	RegisterClass<TimeManager>();
	//78. MonoManager
	RegisterClass<MonoManager>();
	//79. MasterServerInterface
	RegisterClass<MasterServerInterface>();
	//80. NetworkManager
	RegisterClass<NetworkManager>();
	//81. MonoScript
	RegisterClass<MonoScript>();
	//82. PhysicsManager
	RegisterClass<PhysicsManager>();
	//83. UnityConnectSettings
	RegisterClass<UnityConnectSettings>();
	//84. CloudWebServicesManager
	RegisterClass<CloudWebServicesManager>();
	//85. AudioManager
	RegisterClass<AudioManager>();
	//86. Physics2DSettings
	RegisterClass<Physics2DSettings>();
	//87. FlareLayer
	RegisterClass<FlareLayer>();
	//88. LightProbes
	RegisterClass<LightProbes>();
	//89. LightmapSettings
	RegisterClass<LightmapSettings>();
	//90. VideoClip
	RegisterClass<VideoClip>();
	//91. Light
	RegisterClass<Light>();

}
