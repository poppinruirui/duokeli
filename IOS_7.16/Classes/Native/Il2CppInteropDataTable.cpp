﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t2148130204_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t682969308();
extern const RuntimeType AppDomainInitializer_t682969308_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2822380397();
extern const RuntimeType Swapper_t2822380397_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3123975638_0_0_0;
extern "C" void Slot_t3975888750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3975888750_0_0_0;
extern "C" void Slot_t384495010_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t384495010_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t4135868527_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const RuntimeType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const RuntimeType WriteDelegate_t4270993571_0_0_0;
extern "C" void MonoIOStat_t592533987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t592533987_0_0_0;
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3694469084_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t2325775114_0_0_0;
extern "C" void MonoResource_t4103430009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t4103430009_0_0_0;
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t484390987_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t51292791_0_0_0;
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t2872965302_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const RuntimeType CrossContextDelegate_t387175271_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3280319253();
extern const RuntimeType CallbackHandler_t3280319253_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t648286436_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1885824122_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1728406613_0_0_0;
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t1422462475_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const RuntimeType ThreadStart_t1006689297_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3640485471_0_0_0;
extern "C" void DelegatePInvokeWrapper_AsyncReadHandler_t1188682440();
extern const RuntimeType AsyncReadHandler_t1188682440_0_0_0;
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcessAsyncReader_t337580163_0_0_0;
extern "C" void ProcInfo_t2917059746_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcInfo_t2917059746_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t893206259();
extern const RuntimeType ReadMethod_t893206259_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624();
extern const RuntimeType UnmanagedReadOrWrite_t876388624_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t2538911768();
extern const RuntimeType WriteMethod_t2538911768_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t2469437439();
extern const RuntimeType ReadDelegate_t2469437439_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1613340939();
extern const RuntimeType WriteDelegate_t1613340939_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4266946825();
extern const RuntimeType ReadDelegate_t4266946825_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2016697242();
extern const RuntimeType WriteDelegate_t2016697242_0_0_0;
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t1521370843();
extern const RuntimeType SocketAsyncCall_t1521370843_0_0_0;
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketAsyncResult_t2080034863_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t133602714_0_0_0;
extern "C" void IntStack_t2189327687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t2189327687_0_0_0;
extern "C" void Interval_t1802865632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t1802865632_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1722821004();
extern const RuntimeType CostDelegate_t1722821004_0_0_0;
extern "C" void UriScheme_t722425697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t722425697_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const RuntimeType Action_t1264377477_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3046754366_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const RuntimeType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const RuntimeType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t3119663542_0_0_0;
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t699759206_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1445031843_0_0_0;
extern "C" void OrderBlock_t1585977831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OrderBlock_t1585977831_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3829159415_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t1554030124();
extern const RuntimeType CSSMeasureFunc_t1554030124_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const RuntimeType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const RuntimeType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const RuntimeType UnityAction_t3245792599_0_0_0;
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t547604379_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3067099924_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t631007953_0_0_0;
extern "C" void PlayableBinding_t354260709_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t354260709_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1369453676_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t3229609740_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t1199777556_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279();
extern const RuntimeType RequestAtlasCallback_t3100554279_0_0_0;
extern "C" void WorkRequest_t1354518612_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1354518612_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t1699091251_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t403091072_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const RuntimeType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const RuntimeType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const RuntimeType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const RuntimeType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t3211863866_0_0_0;
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandler_t2937767557_0_0_0;
extern "C" void DownloadHandlerAssetBundle_t197128434_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerAssetBundle_t197128434_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerAssetBundle_t197128434_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerAssetBundle_t197128434_0_0_0;
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerBuffer_t2928496527_0_0_0;
extern "C" void DownloadHandlerFile_t2537991672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerFile_t2537991672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerFile_t2537991672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerFile_t2537991672_0_0_0;
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequest_t463507806_0_0_0;
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequestAsyncOperation_t3852015985_0_0_0;
extern "C" void UploadHandler_t2993558019_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandler_t2993558019_0_0_0;
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandlerRaw_t25761545_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t1536042487_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t2465339518_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t4134054672_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t2719720026_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const RuntimeType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const RuntimeType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t1397964415_0_0_0;
extern "C" void SliderHandler_t1154919399_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SliderHandler_t1154919399_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SliderHandler_t1154919399_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SliderHandler_t1154919399_0_0_0;
extern "C" void Collision2D_t2842956331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t2842956331_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t3805203441_0_0_0;
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t2279581989_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t4262080450_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t240592346_0_0_0;
extern "C" void RaycastHit_t1056001966_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t1056001966_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const RuntimeType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t3163629820();
extern const RuntimeType SessionStateChanged_t3163629820_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const RuntimeType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void DelegatePInvokeWrapper_MyAction_t2462891903();
extern const RuntimeType MyAction_t2462891903_0_0_0;
extern "C" void DelegatePInvokeWrapper_IntegerMillisecondsDelegate_t651311252();
extern const RuntimeType IntegerMillisecondsDelegate_t651311252_0_0_0;
extern "C" void TagName_t2891256255_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TagName_t2891256255_0_0_0;
extern "C" void QNameValueType_t615604793_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType QNameValueType_t615604793_0_0_0;
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringArrayValueType_t3147326440_0_0_0;
extern "C" void StringValueType_t261329552_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringValueType_t261329552_0_0_0;
extern "C" void UriValueType_t2866347695_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriValueType_t2866347695_0_0_0;
extern "C" void NsDecl_t3938094415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsDecl_t3938094415_0_0_0;
extern "C" void NsScope_t3958624705_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsScope_t3958624705_0_0_0;
extern "C" void DelegatePInvokeWrapper_CharGetter_t1703763694();
extern const RuntimeType CharGetter_t1703763694_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const RuntimeType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t3049316579_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t1362986479_0_0_0;
extern "C" void EatNode_t1329646118_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EatNode_t1329646118_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EatNode_t1329646118_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EatNode_t1329646118_0_0_0;
extern "C" void sBeanInfo_t3146055137_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sBeanInfo_t3146055137_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sBeanInfo_t3146055137_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sBeanInfo_t3146055137_0_0_0;
extern "C" void sInitBeanNode_t1761059340_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sInitBeanNode_t1761059340_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sInitBeanNode_t1761059340_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sInitBeanNode_t1761059340_0_0_0;
extern "C" void sBuffConfig_t769791949_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sBuffConfig_t769791949_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sBuffConfig_t769791949_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sBuffConfig_t769791949_0_0_0;
extern "C" void sClassConfig_t764718558_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sClassConfig_t764718558_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sClassConfig_t764718558_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sClassConfig_t764718558_0_0_0;
extern "C" void sMannualMonsterConfig_t1844217021_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sMannualMonsterConfig_t1844217021_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sMannualMonsterConfig_t1844217021_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sMannualMonsterConfig_t1844217021_0_0_0;
extern "C" void sThornOfThisClassConfig_t350464872_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sThornOfThisClassConfig_t350464872_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sThornOfThisClassConfig_t350464872_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sThornOfThisClassConfig_t350464872_0_0_0;
extern "C" void sGrassConfig_t73951773_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sGrassConfig_t73951773_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sGrassConfig_t73951773_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sGrassConfig_t73951773_0_0_0;
extern "C" void sItemConfig_t2971202336_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sItemConfig_t2971202336_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sItemConfig_t2971202336_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sItemConfig_t2971202336_0_0_0;
extern "C" void sJiShaSystemMsgParam_t2334122302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sJiShaSystemMsgParam_t2334122302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sJiShaSystemMsgParam_t2334122302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sJiShaSystemMsgParam_t2334122302_0_0_0;
extern "C" void sGenerateBeanNode_t810489850_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sGenerateBeanNode_t810489850_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sGenerateBeanNode_t810489850_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sGenerateBeanNode_t810489850_0_0_0;
extern "C" void sThornConfig_t1242444451_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sThornConfig_t1242444451_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sThornConfig_t1242444451_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sThornConfig_t1242444451_0_0_0;
extern "C" void DelegatePInvokeWrapper_DelegateMethod_OnSelected_t3195943432();
extern const RuntimeType DelegateMethod_OnSelected_t3195943432_0_0_0;
extern "C" void sPlayerAccount_t3053572449_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sPlayerAccount_t3053572449_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sPlayerAccount_t3053572449_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sPlayerAccount_t3053572449_0_0_0;
extern "C" void sPlayerAccount_t97667272_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sPlayerAccount_t97667272_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sPlayerAccount_t97667272_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sPlayerAccount_t97667272_0_0_0;
extern "C" void sOrphanInfo_t2789095025_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sOrphanInfo_t2789095025_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sOrphanInfo_t2789095025_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sOrphanInfo_t2789095025_0_0_0;
extern "C" void DelegatePInvokeWrapper_DelegateMethod_OK_t3925705237();
extern const RuntimeType DelegateMethod_OK_t3925705237_0_0_0;
extern "C" void sAccountInfo_t751877243_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sAccountInfo_t751877243_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sAccountInfo_t751877243_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sAccountInfo_t751877243_0_0_0;
extern "C" void sSkillDetail_t1561700181_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sSkillDetail_t1561700181_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sSkillDetail_t1561700181_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sSkillDetail_t1561700181_0_0_0;
extern "C" void sSkillConfig_t1112470633_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sSkillConfig_t1112470633_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sSkillConfig_t1112470633_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sSkillConfig_t1112470633_0_0_0;
extern "C" void sSkillParam_t731668951_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sSkillParam_t731668951_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sSkillParam_t731668951_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sSkillParam_t731668951_0_0_0;
extern "C" void sSprayConfig_t1865836932_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sSprayConfig_t1865836932_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sSprayConfig_t1865836932_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sSprayConfig_t1865836932_0_0_0;
extern "C" void DelegatePInvokeWrapper_RoomIndexingChanged_t602949928();
extern const RuntimeType RoomIndexingChanged_t602949928_0_0_0;
extern "C" void DelegatePInvokeWrapper_EasyTouchIsReadyHandler_t733707701();
extern const RuntimeType EasyTouchIsReadyHandler_t733707701_0_0_0;
extern "C" void sBeiShuDouZiConfig_t599764670_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sBeiShuDouZiConfig_t599764670_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sBeiShuDouZiConfig_t599764670_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sBeiShuDouZiConfig_t599764670_0_0_0;
extern "C" void DelegatePInvokeWrapper_DelegateMethod_OnClickButton_t3139999742();
extern const RuntimeType DelegateMethod_OnClickButton_t3139999742_0_0_0;
extern "C" void PhotonMessageInfo_t3855471533_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PhotonMessageInfo_t3855471533_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PhotonMessageInfo_t3855471533_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PhotonMessageInfo_t3855471533_0_0_0;
extern "C" void sSplitInfo_t3300999634_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sSplitInfo_t3300999634_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sSplitInfo_t3300999634_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sSplitInfo_t3300999634_0_0_0;
extern "C" void AnimationPair_t1784808777_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationPair_t1784808777_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationPair_t1784808777_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationPair_t1784808777_0_0_0;
extern "C" void EventQueueEntry_t351831961_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EventQueueEntry_t351831961_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EventQueueEntry_t351831961_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EventQueueEntry_t351831961_0_0_0;
extern "C" void AttachmentKeyTuple_t1548106539_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AttachmentKeyTuple_t1548106539_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AttachmentKeyTuple_t1548106539_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AttachmentKeyTuple_t1548106539_0_0_0;
extern "C" void Settings_t612870457_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t612870457_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t612870457_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t612870457_0_0_0;
extern "C" void MeshGeneratorBuffers_t1424700926_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MeshGeneratorBuffers_t1424700926_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MeshGeneratorBuffers_t1424700926_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MeshGeneratorBuffers_t1424700926_0_0_0;
extern "C" void AtlasMaterialOverride_t2435041389_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AtlasMaterialOverride_t2435041389_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AtlasMaterialOverride_t2435041389_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AtlasMaterialOverride_t2435041389_0_0_0;
extern "C" void SlotMaterialOverride_t1001979181_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SlotMaterialOverride_t1001979181_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SlotMaterialOverride_t1001979181_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SlotMaterialOverride_t1001979181_0_0_0;
extern "C" void TransformPair_t3795417756_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransformPair_t3795417756_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransformPair_t3795417756_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TransformPair_t3795417756_0_0_0;
extern "C" void MaterialTexturePair_t1637207048_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MaterialTexturePair_t1637207048_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MaterialTexturePair_t1637207048_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MaterialTexturePair_t1637207048_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkeletonUtilityDelegate_t487527757();
extern const RuntimeType SkeletonUtilityDelegate_t487527757_0_0_0;
extern "C" void Hierarchy_t2161623951_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Hierarchy_t2161623951_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Hierarchy_t2161623951_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Hierarchy_t2161623951_0_0_0;
extern "C" void SubmeshInstruction_t52121370_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SubmeshInstruction_t52121370_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SubmeshInstruction_t52121370_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SubmeshInstruction_t52121370_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnitySendMessageDelegate_t3447265919();
extern const RuntimeType UnitySendMessageDelegate_t3447265919_0_0_0;
extern "C" void UniWebViewMessage_t2441068380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UniWebViewMessage_t2441068380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UniWebViewMessage_t2441068380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UniWebViewMessage_t2441068380_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[176] = 
{
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t682969308, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t682969308_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2822380397, NULL, NULL, NULL, NULL, NULL, &Swapper_t2822380397_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3975888750_marshal_pinvoke, Slot_t3975888750_marshal_pinvoke_back, Slot_t3975888750_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3975888750_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t384495010_marshal_pinvoke, Slot_t384495010_marshal_pinvoke_back, Slot_t384495010_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t384495010_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t592533987_marshal_pinvoke, MonoIOStat_t592533987_marshal_pinvoke_back, MonoIOStat_t592533987_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t592533987_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3694469084_marshal_pinvoke, MonoEnumInfo_t3694469084_marshal_pinvoke_back, MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3694469084_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t2325775114_marshal_pinvoke, ILTokenInfo_t2325775114_marshal_pinvoke_back, ILTokenInfo_t2325775114_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t2325775114_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t4103430009_marshal_pinvoke, MonoResource_t4103430009_marshal_pinvoke_back, MonoResource_t4103430009_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t4103430009_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, RefEmitPermissionSet_t484390987_marshal_pinvoke, RefEmitPermissionSet_t484390987_marshal_pinvoke_back, RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t484390987_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t51292791_marshal_pinvoke, ResourceCacheItem_t51292791_marshal_pinvoke_back, ResourceCacheItem_t51292791_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t51292791_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2872965302_marshal_pinvoke, ResourceInfo_t2872965302_marshal_pinvoke_back, ResourceInfo_t2872965302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2872965302_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3280319253, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3280319253_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t1422462475_marshal_pinvoke, SecurityFrame_t1422462475_marshal_pinvoke_back, SecurityFrame_t1422462475_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1422462475_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ DelegatePInvokeWrapper_AsyncReadHandler_t1188682440, NULL, NULL, NULL, NULL, NULL, &AsyncReadHandler_t1188682440_0_0_0 } /* System.Diagnostics.Process/AsyncReadHandler */,
	{ NULL, ProcessAsyncReader_t337580163_marshal_pinvoke, ProcessAsyncReader_t337580163_marshal_pinvoke_back, ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup, NULL, NULL, &ProcessAsyncReader_t337580163_0_0_0 } /* System.Diagnostics.Process/ProcessAsyncReader */,
	{ NULL, ProcInfo_t2917059746_marshal_pinvoke, ProcInfo_t2917059746_marshal_pinvoke_back, ProcInfo_t2917059746_marshal_pinvoke_cleanup, NULL, NULL, &ProcInfo_t2917059746_0_0_0 } /* System.Diagnostics.Process/ProcInfo */,
	{ DelegatePInvokeWrapper_ReadMethod_t893206259, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t893206259_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t876388624_0_0_0 } /* System.IO.Compression.DeflateStream/UnmanagedReadOrWrite */,
	{ DelegatePInvokeWrapper_WriteMethod_t2538911768, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t2538911768_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ DelegatePInvokeWrapper_ReadDelegate_t2469437439, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t2469437439_0_0_0 } /* System.IO.MonoSyncFileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1613340939, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1613340939_0_0_0 } /* System.IO.MonoSyncFileStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4266946825, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4266946825_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t2016697242, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t2016697242_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_SocketAsyncCall_t1521370843, NULL, NULL, NULL, NULL, NULL, &SocketAsyncCall_t1521370843_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncCall */,
	{ NULL, SocketAsyncResult_t2080034863_marshal_pinvoke, SocketAsyncResult_t2080034863_marshal_pinvoke_back, SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t2080034863_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncResult */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t2189327687_marshal_pinvoke, IntStack_t2189327687_marshal_pinvoke_back, IntStack_t2189327687_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t2189327687_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t1802865632_marshal_pinvoke, Interval_t1802865632_marshal_pinvoke_back, Interval_t1802865632_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t1802865632_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1722821004, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1722821004_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t722425697_marshal_pinvoke, UriScheme_t722425697_marshal_pinvoke_back, UriScheme_t722425697_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t722425697_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t3119663542_marshal_pinvoke, AssetBundleCreateRequest_t3119663542_marshal_pinvoke_back, AssetBundleCreateRequest_t3119663542_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t3119663542_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t699759206_marshal_pinvoke, AssetBundleRequest_t699759206_marshal_pinvoke_back, AssetBundleRequest_t699759206_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t699759206_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, OrderBlock_t1585977831_marshal_pinvoke, OrderBlock_t1585977831_marshal_pinvoke_back, OrderBlock_t1585977831_marshal_pinvoke_cleanup, NULL, NULL, &OrderBlock_t1585977831_0_0_0 } /* UnityEngine.BeforeRenderHelper/OrderBlock */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t1554030124, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t1554030124_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t547604379_marshal_pinvoke, FailedToLoadScriptObject_t547604379_marshal_pinvoke_back, FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t547604379_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t354260709_marshal_pinvoke, PlayableBinding_t354260709_marshal_pinvoke_back, PlayableBinding_t354260709_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t354260709_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3100554279_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1354518612_marshal_pinvoke, WorkRequest_t1354518612_marshal_pinvoke_back, WorkRequest_t1354518612_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1354518612_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, DownloadHandler_t2937767557_marshal_pinvoke, DownloadHandler_t2937767557_marshal_pinvoke_back, DownloadHandler_t2937767557_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t2937767557_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerAssetBundle_t197128434_marshal_pinvoke, DownloadHandlerAssetBundle_t197128434_marshal_pinvoke_back, DownloadHandlerAssetBundle_t197128434_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerAssetBundle_t197128434_0_0_0 } /* UnityEngine.Networking.DownloadHandlerAssetBundle */,
	{ NULL, DownloadHandlerBuffer_t2928496527_marshal_pinvoke, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t2928496527_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, DownloadHandlerFile_t2537991672_marshal_pinvoke, DownloadHandlerFile_t2537991672_marshal_pinvoke_back, DownloadHandlerFile_t2537991672_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerFile_t2537991672_0_0_0 } /* UnityEngine.Networking.DownloadHandlerFile */,
	{ NULL, UnityWebRequest_t463507806_marshal_pinvoke, UnityWebRequest_t463507806_marshal_pinvoke_back, UnityWebRequest_t463507806_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t463507806_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequestAsyncOperation_t3852015985_0_0_0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation */,
	{ NULL, UploadHandler_t2993558019_marshal_pinvoke, UploadHandler_t2993558019_marshal_pinvoke_back, UploadHandler_t2993558019_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t2993558019_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, UploadHandlerRaw_t25761545_marshal_pinvoke, UploadHandlerRaw_t25761545_marshal_pinvoke_back, UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandlerRaw_t25761545_0_0_0 } /* UnityEngine.Networking.UploadHandlerRaw */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, SliderHandler_t1154919399_marshal_pinvoke, SliderHandler_t1154919399_marshal_pinvoke_back, SliderHandler_t1154919399_marshal_pinvoke_cleanup, NULL, NULL, &SliderHandler_t1154919399_0_0_0 } /* UnityEngine.SliderHandler */,
	{ NULL, Collision2D_t2842956331_marshal_pinvoke, Collision2D_t2842956331_marshal_pinvoke_back, Collision2D_t2842956331_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t2842956331_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, RaycastHit2D_t2279581989_marshal_pinvoke, RaycastHit2D_t2279581989_marshal_pinvoke_back, RaycastHit2D_t2279581989_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2279581989_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, RaycastHit_t1056001966_marshal_pinvoke, RaycastHit_t1056001966_marshal_pinvoke_back, RaycastHit_t1056001966_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1056001966_0_0_0 } /* UnityEngine.RaycastHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t3163629820, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t3163629820_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ DelegatePInvokeWrapper_MyAction_t2462891903, NULL, NULL, NULL, NULL, NULL, &MyAction_t2462891903_0_0_0 } /* ExitGames.Client.Photon.PeerBase/MyAction */,
	{ DelegatePInvokeWrapper_IntegerMillisecondsDelegate_t651311252, NULL, NULL, NULL, NULL, NULL, &IntegerMillisecondsDelegate_t651311252_0_0_0 } /* ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate */,
	{ NULL, TagName_t2891256255_marshal_pinvoke, TagName_t2891256255_marshal_pinvoke_back, TagName_t2891256255_marshal_pinvoke_cleanup, NULL, NULL, &TagName_t2891256255_0_0_0 } /* Mono.Xml2.XmlTextReader/TagName */,
	{ NULL, QNameValueType_t615604793_marshal_pinvoke, QNameValueType_t615604793_marshal_pinvoke_back, QNameValueType_t615604793_marshal_pinvoke_cleanup, NULL, NULL, &QNameValueType_t615604793_0_0_0 } /* System.Xml.Schema.QNameValueType */,
	{ NULL, StringArrayValueType_t3147326440_marshal_pinvoke, StringArrayValueType_t3147326440_marshal_pinvoke_back, StringArrayValueType_t3147326440_marshal_pinvoke_cleanup, NULL, NULL, &StringArrayValueType_t3147326440_0_0_0 } /* System.Xml.Schema.StringArrayValueType */,
	{ NULL, StringValueType_t261329552_marshal_pinvoke, StringValueType_t261329552_marshal_pinvoke_back, StringValueType_t261329552_marshal_pinvoke_cleanup, NULL, NULL, &StringValueType_t261329552_0_0_0 } /* System.Xml.Schema.StringValueType */,
	{ NULL, UriValueType_t2866347695_marshal_pinvoke, UriValueType_t2866347695_marshal_pinvoke_back, UriValueType_t2866347695_marshal_pinvoke_cleanup, NULL, NULL, &UriValueType_t2866347695_0_0_0 } /* System.Xml.Schema.UriValueType */,
	{ NULL, NsDecl_t3938094415_marshal_pinvoke, NsDecl_t3938094415_marshal_pinvoke_back, NsDecl_t3938094415_marshal_pinvoke_cleanup, NULL, NULL, &NsDecl_t3938094415_0_0_0 } /* System.Xml.XmlNamespaceManager/NsDecl */,
	{ NULL, NsScope_t3958624705_marshal_pinvoke, NsScope_t3958624705_marshal_pinvoke_back, NsScope_t3958624705_marshal_pinvoke_cleanup, NULL, NULL, &NsScope_t3958624705_0_0_0 } /* System.Xml.XmlNamespaceManager/NsScope */,
	{ DelegatePInvokeWrapper_CharGetter_t1703763694, NULL, NULL, NULL, NULL, NULL, &CharGetter_t1703763694_0_0_0 } /* System.Xml.XmlReaderBinarySupport/CharGetter */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, EatNode_t1329646118_marshal_pinvoke, EatNode_t1329646118_marshal_pinvoke_back, EatNode_t1329646118_marshal_pinvoke_cleanup, NULL, NULL, &EatNode_t1329646118_0_0_0 } /* Ball/EatNode */,
	{ NULL, sBeanInfo_t3146055137_marshal_pinvoke, sBeanInfo_t3146055137_marshal_pinvoke_back, sBeanInfo_t3146055137_marshal_pinvoke_cleanup, NULL, NULL, &sBeanInfo_t3146055137_0_0_0 } /* CBeanCollection/sBeanInfo */,
	{ NULL, sInitBeanNode_t1761059340_marshal_pinvoke, sInitBeanNode_t1761059340_marshal_pinvoke_back, sInitBeanNode_t1761059340_marshal_pinvoke_cleanup, NULL, NULL, &sInitBeanNode_t1761059340_0_0_0 } /* CBeanManager/sInitBeanNode */,
	{ NULL, sBuffConfig_t769791949_marshal_pinvoke, sBuffConfig_t769791949_marshal_pinvoke_back, sBuffConfig_t769791949_marshal_pinvoke_cleanup, NULL, NULL, &sBuffConfig_t769791949_0_0_0 } /* CBuffEditor/sBuffConfig */,
	{ NULL, sClassConfig_t764718558_marshal_pinvoke, sClassConfig_t764718558_marshal_pinvoke_back, sClassConfig_t764718558_marshal_pinvoke_cleanup, NULL, NULL, &sClassConfig_t764718558_0_0_0 } /* CClassEditor/sClassConfig */,
	{ NULL, sMannualMonsterConfig_t1844217021_marshal_pinvoke, sMannualMonsterConfig_t1844217021_marshal_pinvoke_back, sMannualMonsterConfig_t1844217021_marshal_pinvoke_cleanup, NULL, NULL, &sMannualMonsterConfig_t1844217021_0_0_0 } /* CClassEditor/sMannualMonsterConfig */,
	{ NULL, sThornOfThisClassConfig_t350464872_marshal_pinvoke, sThornOfThisClassConfig_t350464872_marshal_pinvoke_back, sThornOfThisClassConfig_t350464872_marshal_pinvoke_cleanup, NULL, NULL, &sThornOfThisClassConfig_t350464872_0_0_0 } /* CClassEditor/sThornOfThisClassConfig */,
	{ NULL, sGrassConfig_t73951773_marshal_pinvoke, sGrassConfig_t73951773_marshal_pinvoke_back, sGrassConfig_t73951773_marshal_pinvoke_cleanup, NULL, NULL, &sGrassConfig_t73951773_0_0_0 } /* CGrassEditor/sGrassConfig */,
	{ NULL, sItemConfig_t2971202336_marshal_pinvoke, sItemConfig_t2971202336_marshal_pinvoke_back, sItemConfig_t2971202336_marshal_pinvoke_cleanup, NULL, NULL, &sItemConfig_t2971202336_0_0_0 } /* CItemSystem/sItemConfig */,
	{ NULL, sJiShaSystemMsgParam_t2334122302_marshal_pinvoke, sJiShaSystemMsgParam_t2334122302_marshal_pinvoke_back, sJiShaSystemMsgParam_t2334122302_marshal_pinvoke_cleanup, NULL, NULL, &sJiShaSystemMsgParam_t2334122302_0_0_0 } /* CJiShaInfo/sJiShaSystemMsgParam */,
	{ NULL, sGenerateBeanNode_t810489850_marshal_pinvoke, sGenerateBeanNode_t810489850_marshal_pinvoke_back, sGenerateBeanNode_t810489850_marshal_pinvoke_cleanup, NULL, NULL, &sGenerateBeanNode_t810489850_0_0_0 } /* CMonsterEditor/sGenerateBeanNode */,
	{ NULL, sThornConfig_t1242444451_marshal_pinvoke, sThornConfig_t1242444451_marshal_pinvoke_back, sThornConfig_t1242444451_marshal_pinvoke_cleanup, NULL, NULL, &sThornConfig_t1242444451_0_0_0 } /* CMonsterEditor/sThornConfig */,
	{ DelegatePInvokeWrapper_DelegateMethod_OnSelected_t3195943432, NULL, NULL, NULL, NULL, NULL, &DelegateMethod_OnSelected_t3195943432_0_0_0 } /* ComoList/DelegateMethod_OnSelected */,
	{ NULL, sPlayerAccount_t3053572449_marshal_pinvoke, sPlayerAccount_t3053572449_marshal_pinvoke_back, sPlayerAccount_t3053572449_marshal_pinvoke_cleanup, NULL, NULL, &sPlayerAccount_t3053572449_0_0_0 } /* CPaiHangBang/sPlayerAccount */,
	{ NULL, sPlayerAccount_t97667272_marshal_pinvoke, sPlayerAccount_t97667272_marshal_pinvoke_back, sPlayerAccount_t97667272_marshal_pinvoke_cleanup, NULL, NULL, &sPlayerAccount_t97667272_0_0_0 } /* CPaiHangBang_Mobile/sPlayerAccount */,
	{ NULL, sOrphanInfo_t2789095025_marshal_pinvoke, sOrphanInfo_t2789095025_marshal_pinvoke_back, sOrphanInfo_t2789095025_marshal_pinvoke_cleanup, NULL, NULL, &sOrphanInfo_t2789095025_0_0_0 } /* CPlayerManager/sOrphanInfo */,
	{ DelegatePInvokeWrapper_DelegateMethod_OK_t3925705237, NULL, NULL, NULL, NULL, NULL, &DelegateMethod_OK_t3925705237_0_0_0 } /* CreateNewShit/DelegateMethod_OK */,
	{ NULL, sAccountInfo_t751877243_marshal_pinvoke, sAccountInfo_t751877243_marshal_pinvoke_back, sAccountInfo_t751877243_marshal_pinvoke_cleanup, NULL, NULL, &sAccountInfo_t751877243_0_0_0 } /* CRegister/sAccountInfo */,
	{ NULL, sSkillDetail_t1561700181_marshal_pinvoke, sSkillDetail_t1561700181_marshal_pinvoke_back, sSkillDetail_t1561700181_marshal_pinvoke_cleanup, NULL, NULL, &sSkillDetail_t1561700181_0_0_0 } /* CSelectSkill/sSkillDetail */,
	{ NULL, sSkillConfig_t1112470633_marshal_pinvoke, sSkillConfig_t1112470633_marshal_pinvoke_back, sSkillConfig_t1112470633_marshal_pinvoke_cleanup, NULL, NULL, &sSkillConfig_t1112470633_0_0_0 } /* CSkillSystem/sSkillConfig */,
	{ NULL, sSkillParam_t731668951_marshal_pinvoke, sSkillParam_t731668951_marshal_pinvoke_back, sSkillParam_t731668951_marshal_pinvoke_cleanup, NULL, NULL, &sSkillParam_t731668951_0_0_0 } /* CSkillSystem/sSkillParam */,
	{ NULL, sSprayConfig_t1865836932_marshal_pinvoke, sSprayConfig_t1865836932_marshal_pinvoke_back, sSprayConfig_t1865836932_marshal_pinvoke_cleanup, NULL, NULL, &sSprayConfig_t1865836932_0_0_0 } /* CSpryEditor/sSprayConfig */,
	{ DelegatePInvokeWrapper_RoomIndexingChanged_t602949928, NULL, NULL, NULL, NULL, NULL, &RoomIndexingChanged_t602949928_0_0_0 } /* ExitGames.UtilityScripts.PlayerRoomIndexing/RoomIndexingChanged */,
	{ DelegatePInvokeWrapper_EasyTouchIsReadyHandler_t733707701, NULL, NULL, NULL, NULL, NULL, &EasyTouchIsReadyHandler_t733707701_0_0_0 } /* HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler */,
	{ NULL, sBeiShuDouZiConfig_t599764670_marshal_pinvoke, sBeiShuDouZiConfig_t599764670_marshal_pinvoke_back, sBeiShuDouZiConfig_t599764670_marshal_pinvoke_cleanup, NULL, NULL, &sBeiShuDouZiConfig_t599764670_0_0_0 } /* MapEditor/sBeiShuDouZiConfig */,
	{ DelegatePInvokeWrapper_DelegateMethod_OnClickButton_t3139999742, NULL, NULL, NULL, NULL, NULL, &DelegateMethod_OnClickButton_t3139999742_0_0_0 } /* MsgBox/DelegateMethod_OnClickButton */,
	{ NULL, PhotonMessageInfo_t3855471533_marshal_pinvoke, PhotonMessageInfo_t3855471533_marshal_pinvoke_back, PhotonMessageInfo_t3855471533_marshal_pinvoke_cleanup, NULL, NULL, &PhotonMessageInfo_t3855471533_0_0_0 } /* PhotonMessageInfo */,
	{ NULL, sSplitInfo_t3300999634_marshal_pinvoke, sSplitInfo_t3300999634_marshal_pinvoke_back, sSplitInfo_t3300999634_marshal_pinvoke_cleanup, NULL, NULL, &sSplitInfo_t3300999634_0_0_0 } /* Player/sSplitInfo */,
	{ NULL, AnimationPair_t1784808777_marshal_pinvoke, AnimationPair_t1784808777_marshal_pinvoke_back, AnimationPair_t1784808777_marshal_pinvoke_cleanup, NULL, NULL, &AnimationPair_t1784808777_0_0_0 } /* Spine.AnimationStateData/AnimationPair */,
	{ NULL, EventQueueEntry_t351831961_marshal_pinvoke, EventQueueEntry_t351831961_marshal_pinvoke_back, EventQueueEntry_t351831961_marshal_pinvoke_cleanup, NULL, NULL, &EventQueueEntry_t351831961_0_0_0 } /* Spine.EventQueue/EventQueueEntry */,
	{ NULL, AttachmentKeyTuple_t1548106539_marshal_pinvoke, AttachmentKeyTuple_t1548106539_marshal_pinvoke_back, AttachmentKeyTuple_t1548106539_marshal_pinvoke_cleanup, NULL, NULL, &AttachmentKeyTuple_t1548106539_0_0_0 } /* Spine.Skin/AttachmentKeyTuple */,
	{ NULL, Settings_t612870457_marshal_pinvoke, Settings_t612870457_marshal_pinvoke_back, Settings_t612870457_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t612870457_0_0_0 } /* Spine.Unity.MeshGenerator/Settings */,
	{ NULL, MeshGeneratorBuffers_t1424700926_marshal_pinvoke, MeshGeneratorBuffers_t1424700926_marshal_pinvoke_back, MeshGeneratorBuffers_t1424700926_marshal_pinvoke_cleanup, NULL, NULL, &MeshGeneratorBuffers_t1424700926_0_0_0 } /* Spine.Unity.MeshGeneratorBuffers */,
	{ NULL, AtlasMaterialOverride_t2435041389_marshal_pinvoke, AtlasMaterialOverride_t2435041389_marshal_pinvoke_back, AtlasMaterialOverride_t2435041389_marshal_pinvoke_cleanup, NULL, NULL, &AtlasMaterialOverride_t2435041389_0_0_0 } /* Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride */,
	{ NULL, SlotMaterialOverride_t1001979181_marshal_pinvoke, SlotMaterialOverride_t1001979181_marshal_pinvoke_back, SlotMaterialOverride_t1001979181_marshal_pinvoke_cleanup, NULL, NULL, &SlotMaterialOverride_t1001979181_0_0_0 } /* Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride */,
	{ NULL, TransformPair_t3795417756_marshal_pinvoke, TransformPair_t3795417756_marshal_pinvoke_back, TransformPair_t3795417756_marshal_pinvoke_cleanup, NULL, NULL, &TransformPair_t3795417756_0_0_0 } /* Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair */,
	{ NULL, MaterialTexturePair_t1637207048_marshal_pinvoke, MaterialTexturePair_t1637207048_marshal_pinvoke_back, MaterialTexturePair_t1637207048_marshal_pinvoke_cleanup, NULL, NULL, &MaterialTexturePair_t1637207048_0_0_0 } /* Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair */,
	{ DelegatePInvokeWrapper_SkeletonUtilityDelegate_t487527757, NULL, NULL, NULL, NULL, NULL, &SkeletonUtilityDelegate_t487527757_0_0_0 } /* Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate */,
	{ NULL, Hierarchy_t2161623951_marshal_pinvoke, Hierarchy_t2161623951_marshal_pinvoke_back, Hierarchy_t2161623951_marshal_pinvoke_cleanup, NULL, NULL, &Hierarchy_t2161623951_0_0_0 } /* Spine.Unity.SpineAttachment/Hierarchy */,
	{ NULL, SubmeshInstruction_t52121370_marshal_pinvoke, SubmeshInstruction_t52121370_marshal_pinvoke_back, SubmeshInstruction_t52121370_marshal_pinvoke_cleanup, NULL, NULL, &SubmeshInstruction_t52121370_0_0_0 } /* Spine.Unity.SubmeshInstruction */,
	{ DelegatePInvokeWrapper_UnitySendMessageDelegate_t3447265919, NULL, NULL, NULL, NULL, NULL, &UnitySendMessageDelegate_t3447265919_0_0_0 } /* UniWebViewInterface/UnitySendMessageDelegate */,
	{ NULL, UniWebViewMessage_t2441068380_marshal_pinvoke, UniWebViewMessage_t2441068380_marshal_pinvoke_back, UniWebViewMessage_t2441068380_marshal_pinvoke_cleanup, NULL, NULL, &UniWebViewMessage_t2441068380_0_0_0 } /* UniWebViewMessage */,
	NULL,
};
