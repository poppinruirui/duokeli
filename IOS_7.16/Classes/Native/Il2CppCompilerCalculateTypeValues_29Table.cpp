﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// RoomOptions
struct RoomOptions_t1787645948;
// TypedLobby
struct TypedLobby_t3336582029;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t1048209202;
// System.String[]
struct StringU5BU5D_t1281789340;
// Polygon
struct Polygon_t12692255;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// MapEditor
struct MapEditor_t878680970;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// ExitGames.Client.Photon.SerializeStreamMethod
struct SerializeStreamMethod_t2169445464;
// ExitGames.Client.Photon.DeserializeStreamMethod
struct DeserializeStreamMethod_t3070531629;
// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>
struct Dictionary_2_t1466267835;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// System.Type
struct Type_t;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1405253484;
// PhotonView
struct PhotonView_t2207721820;
// UnityEngine.UI.Text
struct Text_t1901882714;
// Ball
struct Ball_t2206666566;
// WorldBorder
struct WorldBorder_t1275663748;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;
// System.Collections.Generic.Dictionary`2<System.Int32,Polygon>
struct Dictionary_2_t3196372882;
// MapObj
struct MapObj_t1733252447;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Button
struct Button_t4055032469;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t736164020;
// CMonsterEditor
struct CMonsterEditor_t2594843249;
// CClassEditor
struct CClassEditor_t335744478;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// ComoList
struct ComoList_t2152284863;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Collections.Generic.Dictionary`2<System.Int32,Thorn/ColorThornParam>
struct Dictionary_2_t1249762;
// System.Collections.Generic.List`1<MapEditor/sBeiShuDouZiConfig>
struct List_1_t2071839412;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Dictionary_2_t2517017596;
// System.Collections.Generic.List`1<Polygon>
struct List_1_t1484766997;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<Spray>
struct List_1_t2194232156;
// System.Collections.Generic.List`1<MapEditor/sRebornRanPos>
struct List_1_t1506737460;
// MsgBox/DelegateMethod_OnClickButton
struct DelegateMethod_OnClickButton_t3139999742;
// System.Collections.Generic.Dictionary`2<System.String,ETCAxis>
struct Dictionary_2_t3891096642;
// System.Collections.Generic.Dictionary`2<System.String,ETCBase>
struct Dictionary_2_t4120536841;
// ETCBase
struct ETCBase_t40313246;
// ETCAxis
struct ETCAxis_t4105840343;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// ETCTouchPad/OnMoveStartHandler
struct OnMoveStartHandler_t2512529097;
// ETCTouchPad/OnMoveHandler
struct OnMoveHandler_t3659216741;
// ETCTouchPad/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t3603010941;
// ETCTouchPad/OnMoveEndHandler
struct OnMoveEndHandler_t330401481;
// ETCTouchPad/OnTouchStartHandler
struct OnTouchStartHandler_t3223438894;
// ETCTouchPad/OnTouchUPHandler
struct OnTouchUPHandler_t2186573936;
// ETCTouchPad/OnDownUpHandler
struct OnDownUpHandler_t3325091394;
// ETCTouchPad/OnDownDownHandler
struct OnDownDownHandler_t3809127321;
// ETCTouchPad/OnDownLeftHandler
struct OnDownLeftHandler_t2754992532;
// ETCTouchPad/OnDownRightHandler
struct OnDownRightHandler_t1395399745;
// ETCJoystick/OnMoveStartHandler
struct OnMoveStartHandler_t2779627305;
// ETCJoystick/OnMoveHandler
struct OnMoveHandler_t1792703951;
// ETCJoystick/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t961423656;
// ETCJoystick/OnMoveEndHandler
struct OnMoveEndHandler_t3637094994;
// ETCJoystick/OnTouchStartHandler
struct OnTouchStartHandler_t3335545810;
// ETCJoystick/OnTouchUpHandler
struct OnTouchUpHandler_t4157265644;
// ETCJoystick/OnDownUpHandler
struct OnDownUpHandler_t3611259663;
// ETCJoystick/OnDownDownHandler
struct OnDownDownHandler_t2311908456;
// ETCJoystick/OnDownLeftHandler
struct OnDownLeftHandler_t2607413051;
// ETCJoystick/OnDownRightHandler
struct OnDownRightHandler_t881666752;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// CCheat
struct CCheat_t3466918192;
// UnityEngine.Camera
struct Camera_t4157153871;
// SystemMsg
struct SystemMsg_t1823497383;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// PlayerAction
struct PlayerAction_t3782228511;
// CMonsterBall
struct CMonsterBall_t654437011;
// CMsgList
struct CMsgList_t4144960221;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// Player
struct Player_t3266647312;
// System.Collections.Generic.List`1<PhotonView>
struct List_1_t3679796562;
// System.String[][]
struct StringU5BU5DU5BU5D_t2611993717;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t1752731834;
// System.Collections.Generic.List`1<Player>
struct List_1_t443754758;
// System.Collections.Generic.Dictionary`2<System.Int32,CMonster>
struct Dictionary_2_t830184269;
// System.Collections.Generic.List`1<Ball>
struct List_1_t3678741308;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EVENTCODE_T3064356988_H
#define EVENTCODE_T3064356988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventCode
struct  EventCode_t3064356988  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCODE_T3064356988_H
#ifndef GIZMOTYPEDRAWER_T184707570_H
#define GIZMOTYPEDRAWER_T184707570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.GUI.GizmoTypeDrawer
struct  GizmoTypeDrawer_t184707570  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOTYPEDRAWER_T184707570_H
#ifndef ENTERROOMPARAMS_T3960472384_H
#define ENTERROOMPARAMS_T3960472384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnterRoomParams
struct  EnterRoomParams_t3960472384  : public RuntimeObject
{
public:
	// System.String EnterRoomParams::RoomName
	String_t* ___RoomName_0;
	// RoomOptions EnterRoomParams::RoomOptions
	RoomOptions_t1787645948 * ___RoomOptions_1;
	// TypedLobby EnterRoomParams::Lobby
	TypedLobby_t3336582029 * ___Lobby_2;
	// ExitGames.Client.Photon.Hashtable EnterRoomParams::PlayerProperties
	Hashtable_t1048209202 * ___PlayerProperties_3;
	// System.Boolean EnterRoomParams::OnGameServer
	bool ___OnGameServer_4;
	// System.Boolean EnterRoomParams::CreateIfNotExists
	bool ___CreateIfNotExists_5;
	// System.Boolean EnterRoomParams::RejoinOnly
	bool ___RejoinOnly_6;
	// System.String[] EnterRoomParams::ExpectedUsers
	StringU5BU5D_t1281789340* ___ExpectedUsers_7;

public:
	inline static int32_t get_offset_of_RoomName_0() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___RoomName_0)); }
	inline String_t* get_RoomName_0() const { return ___RoomName_0; }
	inline String_t** get_address_of_RoomName_0() { return &___RoomName_0; }
	inline void set_RoomName_0(String_t* value)
	{
		___RoomName_0 = value;
		Il2CppCodeGenWriteBarrier((&___RoomName_0), value);
	}

	inline static int32_t get_offset_of_RoomOptions_1() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___RoomOptions_1)); }
	inline RoomOptions_t1787645948 * get_RoomOptions_1() const { return ___RoomOptions_1; }
	inline RoomOptions_t1787645948 ** get_address_of_RoomOptions_1() { return &___RoomOptions_1; }
	inline void set_RoomOptions_1(RoomOptions_t1787645948 * value)
	{
		___RoomOptions_1 = value;
		Il2CppCodeGenWriteBarrier((&___RoomOptions_1), value);
	}

	inline static int32_t get_offset_of_Lobby_2() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___Lobby_2)); }
	inline TypedLobby_t3336582029 * get_Lobby_2() const { return ___Lobby_2; }
	inline TypedLobby_t3336582029 ** get_address_of_Lobby_2() { return &___Lobby_2; }
	inline void set_Lobby_2(TypedLobby_t3336582029 * value)
	{
		___Lobby_2 = value;
		Il2CppCodeGenWriteBarrier((&___Lobby_2), value);
	}

	inline static int32_t get_offset_of_PlayerProperties_3() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___PlayerProperties_3)); }
	inline Hashtable_t1048209202 * get_PlayerProperties_3() const { return ___PlayerProperties_3; }
	inline Hashtable_t1048209202 ** get_address_of_PlayerProperties_3() { return &___PlayerProperties_3; }
	inline void set_PlayerProperties_3(Hashtable_t1048209202 * value)
	{
		___PlayerProperties_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerProperties_3), value);
	}

	inline static int32_t get_offset_of_OnGameServer_4() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___OnGameServer_4)); }
	inline bool get_OnGameServer_4() const { return ___OnGameServer_4; }
	inline bool* get_address_of_OnGameServer_4() { return &___OnGameServer_4; }
	inline void set_OnGameServer_4(bool value)
	{
		___OnGameServer_4 = value;
	}

	inline static int32_t get_offset_of_CreateIfNotExists_5() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___CreateIfNotExists_5)); }
	inline bool get_CreateIfNotExists_5() const { return ___CreateIfNotExists_5; }
	inline bool* get_address_of_CreateIfNotExists_5() { return &___CreateIfNotExists_5; }
	inline void set_CreateIfNotExists_5(bool value)
	{
		___CreateIfNotExists_5 = value;
	}

	inline static int32_t get_offset_of_RejoinOnly_6() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___RejoinOnly_6)); }
	inline bool get_RejoinOnly_6() const { return ___RejoinOnly_6; }
	inline bool* get_address_of_RejoinOnly_6() { return &___RejoinOnly_6; }
	inline void set_RejoinOnly_6(bool value)
	{
		___RejoinOnly_6 = value;
	}

	inline static int32_t get_offset_of_ExpectedUsers_7() { return static_cast<int32_t>(offsetof(EnterRoomParams_t3960472384, ___ExpectedUsers_7)); }
	inline StringU5BU5D_t1281789340* get_ExpectedUsers_7() const { return ___ExpectedUsers_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ExpectedUsers_7() { return &___ExpectedUsers_7; }
	inline void set_ExpectedUsers_7(StringU5BU5D_t1281789340* value)
	{
		___ExpectedUsers_7 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedUsers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTERROOMPARAMS_T3960472384_H
#ifndef ERRORCODE_T835159227_H
#define ERRORCODE_T835159227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorCode
struct  ErrorCode_t835159227  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCODE_T835159227_H
#ifndef ACTORPROPERTIES_T3254499384_H
#define ACTORPROPERTIES_T3254499384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActorProperties
struct  ActorProperties_t3254499384  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTORPROPERTIES_T3254499384_H
#ifndef GAMEPROPERTYKEY_T1877143020_H
#define GAMEPROPERTYKEY_T1877143020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePropertyKey
struct  GamePropertyKey_t1877143020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPROPERTYKEY_T1877143020_H
#ifndef U3CLOADPOLYGONBYXMLU3EC__ITERATOR0_T910644365_H
#define U3CLOADPOLYGONBYXMLU3EC__ITERATOR0_T910644365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/<LoadPolygonByXml>c__Iterator0
struct  U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365  : public RuntimeObject
{
public:
	// System.String MapEditor/<LoadPolygonByXml>c__Iterator0::szPolygonName
	String_t* ___szPolygonName_0;
	// Polygon MapEditor/<LoadPolygonByXml>c__Iterator0::polygon
	Polygon_t12692255 * ___polygon_1;
	// System.String MapEditor/<LoadPolygonByXml>c__Iterator0::<szFileName>__0
	String_t* ___U3CszFileNameU3E__0_2;
	// UnityEngine.WWW MapEditor/<LoadPolygonByXml>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_3;
	// System.Xml.XmlNode MapEditor/<LoadPolygonByXml>c__Iterator0::<root>__0
	XmlNode_t3767805227 * ___U3CrootU3E__0_4;
	// System.Xml.XmlDocument MapEditor/<LoadPolygonByXml>c__Iterator0::<myXmlDoc>__0
	XmlDocument_t2837193595 * ___U3CmyXmlDocU3E__0_5;
	// System.String[] MapEditor/<LoadPolygonByXml>c__Iterator0::<aryPointPos>__0
	StringU5BU5D_t1281789340* ___U3CaryPointPosU3E__0_6;
	// System.Single MapEditor/<LoadPolygonByXml>c__Iterator0::fPosX
	float ___fPosX_7;
	// System.Single MapEditor/<LoadPolygonByXml>c__Iterator0::fPosY
	float ___fPosY_8;
	// System.Single MapEditor/<LoadPolygonByXml>c__Iterator0::fScaleX
	float ___fScaleX_9;
	// System.Single MapEditor/<LoadPolygonByXml>c__Iterator0::fScaleY
	float ___fScaleY_10;
	// System.Single MapEditor/<LoadPolygonByXml>c__Iterator0::fRotation
	float ___fRotation_11;
	// System.Boolean MapEditor/<LoadPolygonByXml>c__Iterator0::bIsGrass
	bool ___bIsGrass_12;
	// System.Int32 MapEditor/<LoadPolygonByXml>c__Iterator0::nHardObstacle
	int32_t ___nHardObstacle_13;
	// System.Boolean MapEditor/<LoadPolygonByXml>c__Iterator0::bIsGrassSeed
	bool ___bIsGrassSeed_14;
	// MapEditor MapEditor/<LoadPolygonByXml>c__Iterator0::$this
	MapEditor_t878680970 * ___U24this_15;
	// System.Object MapEditor/<LoadPolygonByXml>c__Iterator0::$current
	RuntimeObject * ___U24current_16;
	// System.Boolean MapEditor/<LoadPolygonByXml>c__Iterator0::$disposing
	bool ___U24disposing_17;
	// System.Int32 MapEditor/<LoadPolygonByXml>c__Iterator0::$PC
	int32_t ___U24PC_18;

public:
	inline static int32_t get_offset_of_szPolygonName_0() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___szPolygonName_0)); }
	inline String_t* get_szPolygonName_0() const { return ___szPolygonName_0; }
	inline String_t** get_address_of_szPolygonName_0() { return &___szPolygonName_0; }
	inline void set_szPolygonName_0(String_t* value)
	{
		___szPolygonName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szPolygonName_0), value);
	}

	inline static int32_t get_offset_of_polygon_1() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___polygon_1)); }
	inline Polygon_t12692255 * get_polygon_1() const { return ___polygon_1; }
	inline Polygon_t12692255 ** get_address_of_polygon_1() { return &___polygon_1; }
	inline void set_polygon_1(Polygon_t12692255 * value)
	{
		___polygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___polygon_1), value);
	}

	inline static int32_t get_offset_of_U3CszFileNameU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U3CszFileNameU3E__0_2)); }
	inline String_t* get_U3CszFileNameU3E__0_2() const { return ___U3CszFileNameU3E__0_2; }
	inline String_t** get_address_of_U3CszFileNameU3E__0_2() { return &___U3CszFileNameU3E__0_2; }
	inline void set_U3CszFileNameU3E__0_2(String_t* value)
	{
		___U3CszFileNameU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CszFileNameU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U3CwwwU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_3() const { return ___U3CwwwU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_3() { return &___U3CwwwU3E__0_3; }
	inline void set_U3CwwwU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E__0_4() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U3CrootU3E__0_4)); }
	inline XmlNode_t3767805227 * get_U3CrootU3E__0_4() const { return ___U3CrootU3E__0_4; }
	inline XmlNode_t3767805227 ** get_address_of_U3CrootU3E__0_4() { return &___U3CrootU3E__0_4; }
	inline void set_U3CrootU3E__0_4(XmlNode_t3767805227 * value)
	{
		___U3CrootU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CmyXmlDocU3E__0_5() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U3CmyXmlDocU3E__0_5)); }
	inline XmlDocument_t2837193595 * get_U3CmyXmlDocU3E__0_5() const { return ___U3CmyXmlDocU3E__0_5; }
	inline XmlDocument_t2837193595 ** get_address_of_U3CmyXmlDocU3E__0_5() { return &___U3CmyXmlDocU3E__0_5; }
	inline void set_U3CmyXmlDocU3E__0_5(XmlDocument_t2837193595 * value)
	{
		___U3CmyXmlDocU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmyXmlDocU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CaryPointPosU3E__0_6() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U3CaryPointPosU3E__0_6)); }
	inline StringU5BU5D_t1281789340* get_U3CaryPointPosU3E__0_6() const { return ___U3CaryPointPosU3E__0_6; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CaryPointPosU3E__0_6() { return &___U3CaryPointPosU3E__0_6; }
	inline void set_U3CaryPointPosU3E__0_6(StringU5BU5D_t1281789340* value)
	{
		___U3CaryPointPosU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaryPointPosU3E__0_6), value);
	}

	inline static int32_t get_offset_of_fPosX_7() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___fPosX_7)); }
	inline float get_fPosX_7() const { return ___fPosX_7; }
	inline float* get_address_of_fPosX_7() { return &___fPosX_7; }
	inline void set_fPosX_7(float value)
	{
		___fPosX_7 = value;
	}

	inline static int32_t get_offset_of_fPosY_8() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___fPosY_8)); }
	inline float get_fPosY_8() const { return ___fPosY_8; }
	inline float* get_address_of_fPosY_8() { return &___fPosY_8; }
	inline void set_fPosY_8(float value)
	{
		___fPosY_8 = value;
	}

	inline static int32_t get_offset_of_fScaleX_9() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___fScaleX_9)); }
	inline float get_fScaleX_9() const { return ___fScaleX_9; }
	inline float* get_address_of_fScaleX_9() { return &___fScaleX_9; }
	inline void set_fScaleX_9(float value)
	{
		___fScaleX_9 = value;
	}

	inline static int32_t get_offset_of_fScaleY_10() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___fScaleY_10)); }
	inline float get_fScaleY_10() const { return ___fScaleY_10; }
	inline float* get_address_of_fScaleY_10() { return &___fScaleY_10; }
	inline void set_fScaleY_10(float value)
	{
		___fScaleY_10 = value;
	}

	inline static int32_t get_offset_of_fRotation_11() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___fRotation_11)); }
	inline float get_fRotation_11() const { return ___fRotation_11; }
	inline float* get_address_of_fRotation_11() { return &___fRotation_11; }
	inline void set_fRotation_11(float value)
	{
		___fRotation_11 = value;
	}

	inline static int32_t get_offset_of_bIsGrass_12() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___bIsGrass_12)); }
	inline bool get_bIsGrass_12() const { return ___bIsGrass_12; }
	inline bool* get_address_of_bIsGrass_12() { return &___bIsGrass_12; }
	inline void set_bIsGrass_12(bool value)
	{
		___bIsGrass_12 = value;
	}

	inline static int32_t get_offset_of_nHardObstacle_13() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___nHardObstacle_13)); }
	inline int32_t get_nHardObstacle_13() const { return ___nHardObstacle_13; }
	inline int32_t* get_address_of_nHardObstacle_13() { return &___nHardObstacle_13; }
	inline void set_nHardObstacle_13(int32_t value)
	{
		___nHardObstacle_13 = value;
	}

	inline static int32_t get_offset_of_bIsGrassSeed_14() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___bIsGrassSeed_14)); }
	inline bool get_bIsGrassSeed_14() const { return ___bIsGrassSeed_14; }
	inline bool* get_address_of_bIsGrassSeed_14() { return &___bIsGrassSeed_14; }
	inline void set_bIsGrassSeed_14(bool value)
	{
		___bIsGrassSeed_14 = value;
	}

	inline static int32_t get_offset_of_U24this_15() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U24this_15)); }
	inline MapEditor_t878680970 * get_U24this_15() const { return ___U24this_15; }
	inline MapEditor_t878680970 ** get_address_of_U24this_15() { return &___U24this_15; }
	inline void set_U24this_15(MapEditor_t878680970 * value)
	{
		___U24this_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_15), value);
	}

	inline static int32_t get_offset_of_U24current_16() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U24current_16)); }
	inline RuntimeObject * get_U24current_16() const { return ___U24current_16; }
	inline RuntimeObject ** get_address_of_U24current_16() { return &___U24current_16; }
	inline void set_U24current_16(RuntimeObject * value)
	{
		___U24current_16 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_16), value);
	}

	inline static int32_t get_offset_of_U24disposing_17() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U24disposing_17)); }
	inline bool get_U24disposing_17() const { return ___U24disposing_17; }
	inline bool* get_address_of_U24disposing_17() { return &___U24disposing_17; }
	inline void set_U24disposing_17(bool value)
	{
		___U24disposing_17 = value;
	}

	inline static int32_t get_offset_of_U24PC_18() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365, ___U24PC_18)); }
	inline int32_t get_U24PC_18() const { return ___U24PC_18; }
	inline int32_t* get_address_of_U24PC_18() { return &___U24PC_18; }
	inline void set_U24PC_18(int32_t value)
	{
		___U24PC_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPOLYGONBYXMLU3EC__ITERATOR0_T910644365_H
#ifndef PARAMETERCODE_T2914727942_H
#define PARAMETERCODE_T2914727942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParameterCode
struct  ParameterCode_t2914727942  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERCODE_T2914727942_H
#ifndef OPERATIONCODE_T3680831435_H
#define OPERATIONCODE_T3680831435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OperationCode
struct  OperationCode_t3680831435  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONCODE_T3680831435_H
#ifndef FRIENDINFO_T533296844_H
#define FRIENDINFO_T533296844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendInfo
struct  FriendInfo_t533296844  : public RuntimeObject
{
public:
	// System.String FriendInfo::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean FriendInfo::<IsOnline>k__BackingField
	bool ___U3CIsOnlineU3Ek__BackingField_1;
	// System.String FriendInfo::<Room>k__BackingField
	String_t* ___U3CRoomU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FriendInfo_t533296844, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsOnlineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FriendInfo_t533296844, ___U3CIsOnlineU3Ek__BackingField_1)); }
	inline bool get_U3CIsOnlineU3Ek__BackingField_1() const { return ___U3CIsOnlineU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsOnlineU3Ek__BackingField_1() { return &___U3CIsOnlineU3Ek__BackingField_1; }
	inline void set_U3CIsOnlineU3Ek__BackingField_1(bool value)
	{
		___U3CIsOnlineU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRoomU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FriendInfo_t533296844, ___U3CRoomU3Ek__BackingField_2)); }
	inline String_t* get_U3CRoomU3Ek__BackingField_2() const { return ___U3CRoomU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRoomU3Ek__BackingField_2() { return &___U3CRoomU3Ek__BackingField_2; }
	inline void set_U3CRoomU3Ek__BackingField_2(String_t* value)
	{
		___U3CRoomU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRoomU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDINFO_T533296844_H
#ifndef U3CGENERATEMAPU3EC__ITERATOR1_T2170335178_H
#define U3CGENERATEMAPU3EC__ITERATOR1_T2170335178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/<GenerateMap>c__Iterator1
struct  U3CGenerateMapU3Ec__Iterator1_t2170335178  : public RuntimeObject
{
public:
	// System.String MapEditor/<GenerateMap>c__Iterator1::szFileName
	String_t* ___szFileName_0;
	// UnityEngine.WWW MapEditor/<GenerateMap>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<root>__0
	XmlNode_t3767805227 * ___U3CrootU3E__0_2;
	// System.Xml.XmlDocument MapEditor/<GenerateMap>c__Iterator1::<myXmlDoc>__0
	XmlDocument_t2837193595 * ___U3CmyXmlDocU3E__0_3;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeCommon>__0
	XmlNode_t3767805227 * ___U3CnodeCommonU3E__0_4;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeCommon2>__0
	XmlNode_t3767805227 * ___U3CnodeCommon2U3E__0_5;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeThorn>__0
	XmlNode_t3767805227 * ___U3CnodeThornU3E__0_6;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeClass>__0
	XmlNode_t3767805227 * ___U3CnodeClassU3E__0_7;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeMannualMonster>__0
	XmlNode_t3767805227 * ___U3CnodeMannualMonsterU3E__0_8;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeGrow>__0
	XmlNode_t3767805227 * ___U3CnodeGrowU3E__0_9;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeItem>__0
	XmlNode_t3767805227 * ___U3CnodeItemU3E__0_10;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeSkill>__0
	XmlNode_t3767805227 * ___U3CnodeSkillU3E__0_11;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeSkillDesc>__0
	XmlNode_t3767805227 * ___U3CnodeSkillDescU3E__0_12;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeGesture>__0
	XmlNode_t3767805227 * ___U3CnodeGestureU3E__0_13;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeEatMode>__0
	XmlNode_t3767805227 * ___U3CnodeEatModeU3E__0_14;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodePolygon>__0
	XmlNode_t3767805227 * ___U3CnodePolygonU3E__0_15;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeGrass>__0
	XmlNode_t3767805227 * ___U3CnodeGrassU3E__0_16;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeLababa>__0
	XmlNode_t3767805227 * ___U3CnodeLababaU3E__0_17;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeSpray>__0
	XmlNode_t3767805227 * ___U3CnodeSprayU3E__0_18;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeDust>__0
	XmlNode_t3767805227 * ___U3CnodeDustU3E__0_19;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeDeadReborn>__0
	XmlNode_t3767805227 * ___U3CnodeDeadRebornU3E__0_20;
	// System.Xml.XmlNode MapEditor/<GenerateMap>c__Iterator1::<nodeControl>__0
	XmlNode_t3767805227 * ___U3CnodeControlU3E__0_21;
	// MapEditor MapEditor/<GenerateMap>c__Iterator1::$this
	MapEditor_t878680970 * ___U24this_22;
	// System.Object MapEditor/<GenerateMap>c__Iterator1::$current
	RuntimeObject * ___U24current_23;
	// System.Boolean MapEditor/<GenerateMap>c__Iterator1::$disposing
	bool ___U24disposing_24;
	// System.Int32 MapEditor/<GenerateMap>c__Iterator1::$PC
	int32_t ___U24PC_25;

public:
	inline static int32_t get_offset_of_szFileName_0() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___szFileName_0)); }
	inline String_t* get_szFileName_0() const { return ___szFileName_0; }
	inline String_t** get_address_of_szFileName_0() { return &___szFileName_0; }
	inline void set_szFileName_0(String_t* value)
	{
		___szFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CrootU3E__0_2)); }
	inline XmlNode_t3767805227 * get_U3CrootU3E__0_2() const { return ___U3CrootU3E__0_2; }
	inline XmlNode_t3767805227 ** get_address_of_U3CrootU3E__0_2() { return &___U3CrootU3E__0_2; }
	inline void set_U3CrootU3E__0_2(XmlNode_t3767805227 * value)
	{
		___U3CrootU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CmyXmlDocU3E__0_3() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CmyXmlDocU3E__0_3)); }
	inline XmlDocument_t2837193595 * get_U3CmyXmlDocU3E__0_3() const { return ___U3CmyXmlDocU3E__0_3; }
	inline XmlDocument_t2837193595 ** get_address_of_U3CmyXmlDocU3E__0_3() { return &___U3CmyXmlDocU3E__0_3; }
	inline void set_U3CmyXmlDocU3E__0_3(XmlDocument_t2837193595 * value)
	{
		___U3CmyXmlDocU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmyXmlDocU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeCommonU3E__0_4() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeCommonU3E__0_4)); }
	inline XmlNode_t3767805227 * get_U3CnodeCommonU3E__0_4() const { return ___U3CnodeCommonU3E__0_4; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeCommonU3E__0_4() { return &___U3CnodeCommonU3E__0_4; }
	inline void set_U3CnodeCommonU3E__0_4(XmlNode_t3767805227 * value)
	{
		___U3CnodeCommonU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeCommonU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CnodeCommon2U3E__0_5() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeCommon2U3E__0_5)); }
	inline XmlNode_t3767805227 * get_U3CnodeCommon2U3E__0_5() const { return ___U3CnodeCommon2U3E__0_5; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeCommon2U3E__0_5() { return &___U3CnodeCommon2U3E__0_5; }
	inline void set_U3CnodeCommon2U3E__0_5(XmlNode_t3767805227 * value)
	{
		___U3CnodeCommon2U3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeCommon2U3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CnodeThornU3E__0_6() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeThornU3E__0_6)); }
	inline XmlNode_t3767805227 * get_U3CnodeThornU3E__0_6() const { return ___U3CnodeThornU3E__0_6; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeThornU3E__0_6() { return &___U3CnodeThornU3E__0_6; }
	inline void set_U3CnodeThornU3E__0_6(XmlNode_t3767805227 * value)
	{
		___U3CnodeThornU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeThornU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CnodeClassU3E__0_7() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeClassU3E__0_7)); }
	inline XmlNode_t3767805227 * get_U3CnodeClassU3E__0_7() const { return ___U3CnodeClassU3E__0_7; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeClassU3E__0_7() { return &___U3CnodeClassU3E__0_7; }
	inline void set_U3CnodeClassU3E__0_7(XmlNode_t3767805227 * value)
	{
		___U3CnodeClassU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeClassU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3CnodeMannualMonsterU3E__0_8() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeMannualMonsterU3E__0_8)); }
	inline XmlNode_t3767805227 * get_U3CnodeMannualMonsterU3E__0_8() const { return ___U3CnodeMannualMonsterU3E__0_8; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeMannualMonsterU3E__0_8() { return &___U3CnodeMannualMonsterU3E__0_8; }
	inline void set_U3CnodeMannualMonsterU3E__0_8(XmlNode_t3767805227 * value)
	{
		___U3CnodeMannualMonsterU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeMannualMonsterU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U3CnodeGrowU3E__0_9() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeGrowU3E__0_9)); }
	inline XmlNode_t3767805227 * get_U3CnodeGrowU3E__0_9() const { return ___U3CnodeGrowU3E__0_9; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeGrowU3E__0_9() { return &___U3CnodeGrowU3E__0_9; }
	inline void set_U3CnodeGrowU3E__0_9(XmlNode_t3767805227 * value)
	{
		___U3CnodeGrowU3E__0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeGrowU3E__0_9), value);
	}

	inline static int32_t get_offset_of_U3CnodeItemU3E__0_10() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeItemU3E__0_10)); }
	inline XmlNode_t3767805227 * get_U3CnodeItemU3E__0_10() const { return ___U3CnodeItemU3E__0_10; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeItemU3E__0_10() { return &___U3CnodeItemU3E__0_10; }
	inline void set_U3CnodeItemU3E__0_10(XmlNode_t3767805227 * value)
	{
		___U3CnodeItemU3E__0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeItemU3E__0_10), value);
	}

	inline static int32_t get_offset_of_U3CnodeSkillU3E__0_11() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeSkillU3E__0_11)); }
	inline XmlNode_t3767805227 * get_U3CnodeSkillU3E__0_11() const { return ___U3CnodeSkillU3E__0_11; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeSkillU3E__0_11() { return &___U3CnodeSkillU3E__0_11; }
	inline void set_U3CnodeSkillU3E__0_11(XmlNode_t3767805227 * value)
	{
		___U3CnodeSkillU3E__0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeSkillU3E__0_11), value);
	}

	inline static int32_t get_offset_of_U3CnodeSkillDescU3E__0_12() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeSkillDescU3E__0_12)); }
	inline XmlNode_t3767805227 * get_U3CnodeSkillDescU3E__0_12() const { return ___U3CnodeSkillDescU3E__0_12; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeSkillDescU3E__0_12() { return &___U3CnodeSkillDescU3E__0_12; }
	inline void set_U3CnodeSkillDescU3E__0_12(XmlNode_t3767805227 * value)
	{
		___U3CnodeSkillDescU3E__0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeSkillDescU3E__0_12), value);
	}

	inline static int32_t get_offset_of_U3CnodeGestureU3E__0_13() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeGestureU3E__0_13)); }
	inline XmlNode_t3767805227 * get_U3CnodeGestureU3E__0_13() const { return ___U3CnodeGestureU3E__0_13; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeGestureU3E__0_13() { return &___U3CnodeGestureU3E__0_13; }
	inline void set_U3CnodeGestureU3E__0_13(XmlNode_t3767805227 * value)
	{
		___U3CnodeGestureU3E__0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeGestureU3E__0_13), value);
	}

	inline static int32_t get_offset_of_U3CnodeEatModeU3E__0_14() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeEatModeU3E__0_14)); }
	inline XmlNode_t3767805227 * get_U3CnodeEatModeU3E__0_14() const { return ___U3CnodeEatModeU3E__0_14; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeEatModeU3E__0_14() { return &___U3CnodeEatModeU3E__0_14; }
	inline void set_U3CnodeEatModeU3E__0_14(XmlNode_t3767805227 * value)
	{
		___U3CnodeEatModeU3E__0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeEatModeU3E__0_14), value);
	}

	inline static int32_t get_offset_of_U3CnodePolygonU3E__0_15() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodePolygonU3E__0_15)); }
	inline XmlNode_t3767805227 * get_U3CnodePolygonU3E__0_15() const { return ___U3CnodePolygonU3E__0_15; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodePolygonU3E__0_15() { return &___U3CnodePolygonU3E__0_15; }
	inline void set_U3CnodePolygonU3E__0_15(XmlNode_t3767805227 * value)
	{
		___U3CnodePolygonU3E__0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodePolygonU3E__0_15), value);
	}

	inline static int32_t get_offset_of_U3CnodeGrassU3E__0_16() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeGrassU3E__0_16)); }
	inline XmlNode_t3767805227 * get_U3CnodeGrassU3E__0_16() const { return ___U3CnodeGrassU3E__0_16; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeGrassU3E__0_16() { return &___U3CnodeGrassU3E__0_16; }
	inline void set_U3CnodeGrassU3E__0_16(XmlNode_t3767805227 * value)
	{
		___U3CnodeGrassU3E__0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeGrassU3E__0_16), value);
	}

	inline static int32_t get_offset_of_U3CnodeLababaU3E__0_17() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeLababaU3E__0_17)); }
	inline XmlNode_t3767805227 * get_U3CnodeLababaU3E__0_17() const { return ___U3CnodeLababaU3E__0_17; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeLababaU3E__0_17() { return &___U3CnodeLababaU3E__0_17; }
	inline void set_U3CnodeLababaU3E__0_17(XmlNode_t3767805227 * value)
	{
		___U3CnodeLababaU3E__0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeLababaU3E__0_17), value);
	}

	inline static int32_t get_offset_of_U3CnodeSprayU3E__0_18() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeSprayU3E__0_18)); }
	inline XmlNode_t3767805227 * get_U3CnodeSprayU3E__0_18() const { return ___U3CnodeSprayU3E__0_18; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeSprayU3E__0_18() { return &___U3CnodeSprayU3E__0_18; }
	inline void set_U3CnodeSprayU3E__0_18(XmlNode_t3767805227 * value)
	{
		___U3CnodeSprayU3E__0_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeSprayU3E__0_18), value);
	}

	inline static int32_t get_offset_of_U3CnodeDustU3E__0_19() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeDustU3E__0_19)); }
	inline XmlNode_t3767805227 * get_U3CnodeDustU3E__0_19() const { return ___U3CnodeDustU3E__0_19; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeDustU3E__0_19() { return &___U3CnodeDustU3E__0_19; }
	inline void set_U3CnodeDustU3E__0_19(XmlNode_t3767805227 * value)
	{
		___U3CnodeDustU3E__0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeDustU3E__0_19), value);
	}

	inline static int32_t get_offset_of_U3CnodeDeadRebornU3E__0_20() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeDeadRebornU3E__0_20)); }
	inline XmlNode_t3767805227 * get_U3CnodeDeadRebornU3E__0_20() const { return ___U3CnodeDeadRebornU3E__0_20; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeDeadRebornU3E__0_20() { return &___U3CnodeDeadRebornU3E__0_20; }
	inline void set_U3CnodeDeadRebornU3E__0_20(XmlNode_t3767805227 * value)
	{
		___U3CnodeDeadRebornU3E__0_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeDeadRebornU3E__0_20), value);
	}

	inline static int32_t get_offset_of_U3CnodeControlU3E__0_21() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U3CnodeControlU3E__0_21)); }
	inline XmlNode_t3767805227 * get_U3CnodeControlU3E__0_21() const { return ___U3CnodeControlU3E__0_21; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeControlU3E__0_21() { return &___U3CnodeControlU3E__0_21; }
	inline void set_U3CnodeControlU3E__0_21(XmlNode_t3767805227 * value)
	{
		___U3CnodeControlU3E__0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeControlU3E__0_21), value);
	}

	inline static int32_t get_offset_of_U24this_22() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U24this_22)); }
	inline MapEditor_t878680970 * get_U24this_22() const { return ___U24this_22; }
	inline MapEditor_t878680970 ** get_address_of_U24this_22() { return &___U24this_22; }
	inline void set_U24this_22(MapEditor_t878680970 * value)
	{
		___U24this_22 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_22), value);
	}

	inline static int32_t get_offset_of_U24current_23() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U24current_23)); }
	inline RuntimeObject * get_U24current_23() const { return ___U24current_23; }
	inline RuntimeObject ** get_address_of_U24current_23() { return &___U24current_23; }
	inline void set_U24current_23(RuntimeObject * value)
	{
		___U24current_23 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_23), value);
	}

	inline static int32_t get_offset_of_U24disposing_24() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U24disposing_24)); }
	inline bool get_U24disposing_24() const { return ___U24disposing_24; }
	inline bool* get_address_of_U24disposing_24() { return &___U24disposing_24; }
	inline void set_U24disposing_24(bool value)
	{
		___U24disposing_24 = value;
	}

	inline static int32_t get_offset_of_U24PC_25() { return static_cast<int32_t>(offsetof(U3CGenerateMapU3Ec__Iterator1_t2170335178, ___U24PC_25)); }
	inline int32_t get_U24PC_25() const { return ___U24PC_25; }
	inline int32_t* get_address_of_U24PC_25() { return &___U24PC_25; }
	inline void set_U24PC_25(int32_t value)
	{
		___U24PC_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEMAPU3EC__ITERATOR1_T2170335178_H
#ifndef U3CDOSAVEMAPXMLFILEU3EC__ITERATOR2_T1738818793_H
#define U3CDOSAVEMAPXMLFILEU3EC__ITERATOR2_T1738818793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/<DoSaveMapXmlFile>c__Iterator2
struct  U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MapEditor/<DoSaveMapXmlFile>c__Iterator2::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_0;
	// System.String MapEditor/<DoSaveMapXmlFile>c__Iterator2::szRoomName
	String_t* ___szRoomName_1;
	// System.String MapEditor/<DoSaveMapXmlFile>c__Iterator2::szContent
	String_t* ___szContent_2;
	// UnityEngine.WWW MapEditor/<DoSaveMapXmlFile>c__Iterator2::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_3;
	// System.Object MapEditor/<DoSaveMapXmlFile>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MapEditor/<DoSaveMapXmlFile>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 MapEditor/<DoSaveMapXmlFile>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwwwFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___U3CwwwFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_0() const { return ___U3CwwwFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_0() { return &___U3CwwwFormU3E__0_0; }
	inline void set_U3CwwwFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_szRoomName_1() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___szRoomName_1)); }
	inline String_t* get_szRoomName_1() const { return ___szRoomName_1; }
	inline String_t** get_address_of_szRoomName_1() { return &___szRoomName_1; }
	inline void set_szRoomName_1(String_t* value)
	{
		___szRoomName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szRoomName_1), value);
	}

	inline static int32_t get_offset_of_szContent_2() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___szContent_2)); }
	inline String_t* get_szContent_2() const { return ___szContent_2; }
	inline String_t** get_address_of_szContent_2() { return &___szContent_2; }
	inline void set_szContent_2(String_t* value)
	{
		___szContent_2 = value;
		Il2CppCodeGenWriteBarrier((&___szContent_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___U3CwwwU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_3() const { return ___U3CwwwU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_3() { return &___U3CwwwU3E__0_3; }
	inline void set_U3CwwwU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSAVEMAPXMLFILEU3EC__ITERATOR2_T1738818793_H
#ifndef U3CLOADSCENEU3EC__ITERATOR3_T4020263557_H
#define U3CLOADSCENEU3EC__ITERATOR3_T4020263557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/<LoadScene>c__Iterator3
struct  U3CLoadSceneU3Ec__Iterator3_t4020263557  : public RuntimeObject
{
public:
	// System.String MapEditor/<LoadScene>c__Iterator3::scene_name
	String_t* ___scene_name_0;
	// System.Object MapEditor/<LoadScene>c__Iterator3::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MapEditor/<LoadScene>c__Iterator3::$disposing
	bool ___U24disposing_2;
	// System.Int32 MapEditor/<LoadScene>c__Iterator3::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_scene_name_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator3_t4020263557, ___scene_name_0)); }
	inline String_t* get_scene_name_0() const { return ___scene_name_0; }
	inline String_t** get_address_of_scene_name_0() { return &___scene_name_0; }
	inline void set_scene_name_0(String_t* value)
	{
		___scene_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_name_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator3_t4020263557, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator3_t4020263557, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator3_t4020263557, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3EC__ITERATOR3_T4020263557_H
#ifndef U3CLOADPOLYGONLISTU3EC__ITERATOR4_T453975090_H
#define U3CLOADPOLYGONLISTU3EC__ITERATOR4_T453975090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/<LoadPolygonList>c__Iterator4
struct  U3CLoadPolygonListU3Ec__Iterator4_t453975090  : public RuntimeObject
{
public:
	// System.String MapEditor/<LoadPolygonList>c__Iterator4::<szFileName>__0
	String_t* ___U3CszFileNameU3E__0_0;
	// System.Xml.XmlNode MapEditor/<LoadPolygonList>c__Iterator4::<root>__0
	XmlNode_t3767805227 * ___U3CrootU3E__0_1;
	// UnityEngine.WWW MapEditor/<LoadPolygonList>c__Iterator4::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Xml.XmlDocument MapEditor/<LoadPolygonList>c__Iterator4::<XmlDoc>__0
	XmlDocument_t2837193595 * ___U3CXmlDocU3E__0_3;
	// MapEditor MapEditor/<LoadPolygonList>c__Iterator4::$this
	MapEditor_t878680970 * ___U24this_4;
	// System.Object MapEditor/<LoadPolygonList>c__Iterator4::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean MapEditor/<LoadPolygonList>c__Iterator4::$disposing
	bool ___U24disposing_6;
	// System.Int32 MapEditor/<LoadPolygonList>c__Iterator4::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CszFileNameU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U3CszFileNameU3E__0_0)); }
	inline String_t* get_U3CszFileNameU3E__0_0() const { return ___U3CszFileNameU3E__0_0; }
	inline String_t** get_address_of_U3CszFileNameU3E__0_0() { return &___U3CszFileNameU3E__0_0; }
	inline void set_U3CszFileNameU3E__0_0(String_t* value)
	{
		___U3CszFileNameU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CszFileNameU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U3CrootU3E__0_1)); }
	inline XmlNode_t3767805227 * get_U3CrootU3E__0_1() const { return ___U3CrootU3E__0_1; }
	inline XmlNode_t3767805227 ** get_address_of_U3CrootU3E__0_1() { return &___U3CrootU3E__0_1; }
	inline void set_U3CrootU3E__0_1(XmlNode_t3767805227 * value)
	{
		___U3CrootU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CXmlDocU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U3CXmlDocU3E__0_3)); }
	inline XmlDocument_t2837193595 * get_U3CXmlDocU3E__0_3() const { return ___U3CXmlDocU3E__0_3; }
	inline XmlDocument_t2837193595 ** get_address_of_U3CXmlDocU3E__0_3() { return &___U3CXmlDocU3E__0_3; }
	inline void set_U3CXmlDocU3E__0_3(XmlDocument_t2837193595 * value)
	{
		___U3CXmlDocU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CXmlDocU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U24this_4)); }
	inline MapEditor_t878680970 * get_U24this_4() const { return ___U24this_4; }
	inline MapEditor_t878680970 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MapEditor_t878680970 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator4_t453975090, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPOLYGONLISTU3EC__ITERATOR4_T453975090_H
#ifndef U3CLOADSCENEU3EC__ITERATOR0_T636698714_H
#define U3CLOADSCENEU3EC__ITERATOR0_T636698714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/<LoadScene>c__Iterator0
struct  U3CLoadSceneU3Ec__Iterator0_t636698714  : public RuntimeObject
{
public:
	// System.String Main/<LoadScene>c__Iterator0::scene_name
	String_t* ___scene_name_0;
	// System.Object Main/<LoadScene>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Main/<LoadScene>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Main/<LoadScene>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_scene_name_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t636698714, ___scene_name_0)); }
	inline String_t* get_scene_name_0() const { return ___scene_name_0; }
	inline String_t** get_address_of_scene_name_0() { return &___scene_name_0; }
	inline void set_scene_name_0(String_t* value)
	{
		___scene_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_name_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t636698714, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t636698714, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t636698714, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3EC__ITERATOR0_T636698714_H
#ifndef CUSTOMTYPES_T2914914968_H
#define CUSTOMTYPES_T2914914968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTypes
struct  CustomTypes_t2914914968  : public RuntimeObject
{
public:

public:
};

struct CustomTypes_t2914914968_StaticFields
{
public:
	// System.Byte[] CustomTypes::memVector3
	ByteU5BU5D_t4116647657* ___memVector3_0;
	// System.Byte[] CustomTypes::memVector2
	ByteU5BU5D_t4116647657* ___memVector2_1;
	// System.Byte[] CustomTypes::memQuarternion
	ByteU5BU5D_t4116647657* ___memQuarternion_2;
	// System.Byte[] CustomTypes::memPlayer
	ByteU5BU5D_t4116647657* ___memPlayer_3;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache0
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache0_4;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache1
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache1_5;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache2
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache2_6;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache3
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache3_7;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache4
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache4_8;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache5
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache5_9;
	// ExitGames.Client.Photon.SerializeStreamMethod CustomTypes::<>f__mg$cache6
	SerializeStreamMethod_t2169445464 * ___U3CU3Ef__mgU24cache6_10;
	// ExitGames.Client.Photon.DeserializeStreamMethod CustomTypes::<>f__mg$cache7
	DeserializeStreamMethod_t3070531629 * ___U3CU3Ef__mgU24cache7_11;

public:
	inline static int32_t get_offset_of_memVector3_0() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memVector3_0)); }
	inline ByteU5BU5D_t4116647657* get_memVector3_0() const { return ___memVector3_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_memVector3_0() { return &___memVector3_0; }
	inline void set_memVector3_0(ByteU5BU5D_t4116647657* value)
	{
		___memVector3_0 = value;
		Il2CppCodeGenWriteBarrier((&___memVector3_0), value);
	}

	inline static int32_t get_offset_of_memVector2_1() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memVector2_1)); }
	inline ByteU5BU5D_t4116647657* get_memVector2_1() const { return ___memVector2_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_memVector2_1() { return &___memVector2_1; }
	inline void set_memVector2_1(ByteU5BU5D_t4116647657* value)
	{
		___memVector2_1 = value;
		Il2CppCodeGenWriteBarrier((&___memVector2_1), value);
	}

	inline static int32_t get_offset_of_memQuarternion_2() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memQuarternion_2)); }
	inline ByteU5BU5D_t4116647657* get_memQuarternion_2() const { return ___memQuarternion_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_memQuarternion_2() { return &___memQuarternion_2; }
	inline void set_memQuarternion_2(ByteU5BU5D_t4116647657* value)
	{
		___memQuarternion_2 = value;
		Il2CppCodeGenWriteBarrier((&___memQuarternion_2), value);
	}

	inline static int32_t get_offset_of_memPlayer_3() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___memPlayer_3)); }
	inline ByteU5BU5D_t4116647657* get_memPlayer_3() const { return ___memPlayer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_memPlayer_3() { return &___memPlayer_3; }
	inline void set_memPlayer_3(ByteU5BU5D_t4116647657* value)
	{
		___memPlayer_3 = value;
		Il2CppCodeGenWriteBarrier((&___memPlayer_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_6() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache2_6)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache2_6() const { return ___U3CU3Ef__mgU24cache2_6; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache2_6() { return &___U3CU3Ef__mgU24cache2_6; }
	inline void set_U3CU3Ef__mgU24cache2_6(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_7() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache3_7)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache3_7() const { return ___U3CU3Ef__mgU24cache3_7; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache3_7() { return &___U3CU3Ef__mgU24cache3_7; }
	inline void set_U3CU3Ef__mgU24cache3_7(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_8() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache4_8)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache4_8() const { return ___U3CU3Ef__mgU24cache4_8; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache4_8() { return &___U3CU3Ef__mgU24cache4_8; }
	inline void set_U3CU3Ef__mgU24cache4_8(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_9() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache5_9)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache5_9() const { return ___U3CU3Ef__mgU24cache5_9; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache5_9() { return &___U3CU3Ef__mgU24cache5_9; }
	inline void set_U3CU3Ef__mgU24cache5_9(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_10() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache6_10)); }
	inline SerializeStreamMethod_t2169445464 * get_U3CU3Ef__mgU24cache6_10() const { return ___U3CU3Ef__mgU24cache6_10; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_U3CU3Ef__mgU24cache6_10() { return &___U3CU3Ef__mgU24cache6_10; }
	inline void set_U3CU3Ef__mgU24cache6_10(SerializeStreamMethod_t2169445464 * value)
	{
		___U3CU3Ef__mgU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_11() { return static_cast<int32_t>(offsetof(CustomTypes_t2914914968_StaticFields, ___U3CU3Ef__mgU24cache7_11)); }
	inline DeserializeStreamMethod_t3070531629 * get_U3CU3Ef__mgU24cache7_11() const { return ___U3CU3Ef__mgU24cache7_11; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_U3CU3Ef__mgU24cache7_11() { return &___U3CU3Ef__mgU24cache7_11; }
	inline void set_U3CU3Ef__mgU24cache7_11(DeserializeStreamMethod_t3070531629 * value)
	{
		___U3CU3Ef__mgU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTYPES_T2914914968_H
#ifndef ENCRYPTIONDATAPARAMETERS_T25380775_H
#define ENCRYPTIONDATAPARAMETERS_T25380775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EncryptionDataParameters
struct  EncryptionDataParameters_t25380775  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONDATAPARAMETERS_T25380775_H
#ifndef EXTENSIONS_T2612146612_H
#define EXTENSIONS_T2612146612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Extensions
struct  Extensions_t2612146612  : public RuntimeObject
{
public:

public:
};

struct Extensions_t2612146612_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]> Extensions::ParametersOfMethods
	Dictionary_2_t1466267835 * ___ParametersOfMethods_0;

public:
	inline static int32_t get_offset_of_ParametersOfMethods_0() { return static_cast<int32_t>(offsetof(Extensions_t2612146612_StaticFields, ___ParametersOfMethods_0)); }
	inline Dictionary_2_t1466267835 * get_ParametersOfMethods_0() const { return ___ParametersOfMethods_0; }
	inline Dictionary_2_t1466267835 ** get_address_of_ParametersOfMethods_0() { return &___ParametersOfMethods_0; }
	inline void set_ParametersOfMethods_0(Dictionary_2_t1466267835 * value)
	{
		___ParametersOfMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&___ParametersOfMethods_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T2612146612_H
#ifndef GAMEOBJECTEXTENSIONS_T182180175_H
#define GAMEOBJECTEXTENSIONS_T182180175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtensions
struct  GameObjectExtensions_t182180175  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T182180175_H
#ifndef ROOMOPTIONS_T1787645948_H
#define ROOMOPTIONS_T1787645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomOptions
struct  RoomOptions_t1787645948  : public RuntimeObject
{
public:
	// System.Boolean RoomOptions::isVisibleField
	bool ___isVisibleField_0;
	// System.Boolean RoomOptions::isOpenField
	bool ___isOpenField_1;
	// System.Byte RoomOptions::MaxPlayers
	uint8_t ___MaxPlayers_2;
	// System.Int32 RoomOptions::PlayerTtl
	int32_t ___PlayerTtl_3;
	// System.Int32 RoomOptions::EmptyRoomTtl
	int32_t ___EmptyRoomTtl_4;
	// System.Boolean RoomOptions::cleanupCacheOnLeaveField
	bool ___cleanupCacheOnLeaveField_5;
	// ExitGames.Client.Photon.Hashtable RoomOptions::CustomRoomProperties
	Hashtable_t1048209202 * ___CustomRoomProperties_6;
	// System.String[] RoomOptions::CustomRoomPropertiesForLobby
	StringU5BU5D_t1281789340* ___CustomRoomPropertiesForLobby_7;
	// System.String[] RoomOptions::Plugins
	StringU5BU5D_t1281789340* ___Plugins_8;
	// System.Boolean RoomOptions::suppressRoomEventsField
	bool ___suppressRoomEventsField_9;
	// System.Boolean RoomOptions::publishUserIdField
	bool ___publishUserIdField_10;
	// System.Boolean RoomOptions::deleteNullPropertiesField
	bool ___deleteNullPropertiesField_11;

public:
	inline static int32_t get_offset_of_isVisibleField_0() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___isVisibleField_0)); }
	inline bool get_isVisibleField_0() const { return ___isVisibleField_0; }
	inline bool* get_address_of_isVisibleField_0() { return &___isVisibleField_0; }
	inline void set_isVisibleField_0(bool value)
	{
		___isVisibleField_0 = value;
	}

	inline static int32_t get_offset_of_isOpenField_1() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___isOpenField_1)); }
	inline bool get_isOpenField_1() const { return ___isOpenField_1; }
	inline bool* get_address_of_isOpenField_1() { return &___isOpenField_1; }
	inline void set_isOpenField_1(bool value)
	{
		___isOpenField_1 = value;
	}

	inline static int32_t get_offset_of_MaxPlayers_2() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___MaxPlayers_2)); }
	inline uint8_t get_MaxPlayers_2() const { return ___MaxPlayers_2; }
	inline uint8_t* get_address_of_MaxPlayers_2() { return &___MaxPlayers_2; }
	inline void set_MaxPlayers_2(uint8_t value)
	{
		___MaxPlayers_2 = value;
	}

	inline static int32_t get_offset_of_PlayerTtl_3() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___PlayerTtl_3)); }
	inline int32_t get_PlayerTtl_3() const { return ___PlayerTtl_3; }
	inline int32_t* get_address_of_PlayerTtl_3() { return &___PlayerTtl_3; }
	inline void set_PlayerTtl_3(int32_t value)
	{
		___PlayerTtl_3 = value;
	}

	inline static int32_t get_offset_of_EmptyRoomTtl_4() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___EmptyRoomTtl_4)); }
	inline int32_t get_EmptyRoomTtl_4() const { return ___EmptyRoomTtl_4; }
	inline int32_t* get_address_of_EmptyRoomTtl_4() { return &___EmptyRoomTtl_4; }
	inline void set_EmptyRoomTtl_4(int32_t value)
	{
		___EmptyRoomTtl_4 = value;
	}

	inline static int32_t get_offset_of_cleanupCacheOnLeaveField_5() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___cleanupCacheOnLeaveField_5)); }
	inline bool get_cleanupCacheOnLeaveField_5() const { return ___cleanupCacheOnLeaveField_5; }
	inline bool* get_address_of_cleanupCacheOnLeaveField_5() { return &___cleanupCacheOnLeaveField_5; }
	inline void set_cleanupCacheOnLeaveField_5(bool value)
	{
		___cleanupCacheOnLeaveField_5 = value;
	}

	inline static int32_t get_offset_of_CustomRoomProperties_6() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___CustomRoomProperties_6)); }
	inline Hashtable_t1048209202 * get_CustomRoomProperties_6() const { return ___CustomRoomProperties_6; }
	inline Hashtable_t1048209202 ** get_address_of_CustomRoomProperties_6() { return &___CustomRoomProperties_6; }
	inline void set_CustomRoomProperties_6(Hashtable_t1048209202 * value)
	{
		___CustomRoomProperties_6 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRoomProperties_6), value);
	}

	inline static int32_t get_offset_of_CustomRoomPropertiesForLobby_7() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___CustomRoomPropertiesForLobby_7)); }
	inline StringU5BU5D_t1281789340* get_CustomRoomPropertiesForLobby_7() const { return ___CustomRoomPropertiesForLobby_7; }
	inline StringU5BU5D_t1281789340** get_address_of_CustomRoomPropertiesForLobby_7() { return &___CustomRoomPropertiesForLobby_7; }
	inline void set_CustomRoomPropertiesForLobby_7(StringU5BU5D_t1281789340* value)
	{
		___CustomRoomPropertiesForLobby_7 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRoomPropertiesForLobby_7), value);
	}

	inline static int32_t get_offset_of_Plugins_8() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___Plugins_8)); }
	inline StringU5BU5D_t1281789340* get_Plugins_8() const { return ___Plugins_8; }
	inline StringU5BU5D_t1281789340** get_address_of_Plugins_8() { return &___Plugins_8; }
	inline void set_Plugins_8(StringU5BU5D_t1281789340* value)
	{
		___Plugins_8 = value;
		Il2CppCodeGenWriteBarrier((&___Plugins_8), value);
	}

	inline static int32_t get_offset_of_suppressRoomEventsField_9() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___suppressRoomEventsField_9)); }
	inline bool get_suppressRoomEventsField_9() const { return ___suppressRoomEventsField_9; }
	inline bool* get_address_of_suppressRoomEventsField_9() { return &___suppressRoomEventsField_9; }
	inline void set_suppressRoomEventsField_9(bool value)
	{
		___suppressRoomEventsField_9 = value;
	}

	inline static int32_t get_offset_of_publishUserIdField_10() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___publishUserIdField_10)); }
	inline bool get_publishUserIdField_10() const { return ___publishUserIdField_10; }
	inline bool* get_address_of_publishUserIdField_10() { return &___publishUserIdField_10; }
	inline void set_publishUserIdField_10(bool value)
	{
		___publishUserIdField_10 = value;
	}

	inline static int32_t get_offset_of_deleteNullPropertiesField_11() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___deleteNullPropertiesField_11)); }
	inline bool get_deleteNullPropertiesField_11() const { return ___deleteNullPropertiesField_11; }
	inline bool* get_address_of_deleteNullPropertiesField_11() { return &___deleteNullPropertiesField_11; }
	inline void set_deleteNullPropertiesField_11(bool value)
	{
		___deleteNullPropertiesField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMOPTIONS_T1787645948_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENT_1_T3037889027_H
#define UNITYEVENT_1_T3037889027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t3037889027  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3037889027, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3037889027_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SBEISHUDOUZICONFIG_T599764670_H
#define SBEISHUDOUZICONFIG_T599764670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/sBeiShuDouZiConfig
struct  sBeiShuDouZiConfig_t599764670 
{
public:
	// System.Int32 MapEditor/sBeiShuDouZiConfig::id
	int32_t ___id_0;
	// System.Single MapEditor/sBeiShuDouZiConfig::value
	float ___value_1;
	// System.Int32 MapEditor/sBeiShuDouZiConfig::num
	int32_t ___num_2;
	// System.String MapEditor/sBeiShuDouZiConfig::name
	String_t* ___name_3;
	// System.String MapEditor/sBeiShuDouZiConfig::desc
	String_t* ___desc_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(sBeiShuDouZiConfig_t599764670, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(sBeiShuDouZiConfig_t599764670, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_num_2() { return static_cast<int32_t>(offsetof(sBeiShuDouZiConfig_t599764670, ___num_2)); }
	inline int32_t get_num_2() const { return ___num_2; }
	inline int32_t* get_address_of_num_2() { return &___num_2; }
	inline void set_num_2(int32_t value)
	{
		___num_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(sBeiShuDouZiConfig_t599764670, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_desc_4() { return static_cast<int32_t>(offsetof(sBeiShuDouZiConfig_t599764670, ___desc_4)); }
	inline String_t* get_desc_4() const { return ___desc_4; }
	inline String_t** get_address_of_desc_4() { return &___desc_4; }
	inline void set_desc_4(String_t* value)
	{
		___desc_4 = value;
		Il2CppCodeGenWriteBarrier((&___desc_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MapEditor/sBeiShuDouZiConfig
struct sBeiShuDouZiConfig_t599764670_marshaled_pinvoke
{
	int32_t ___id_0;
	float ___value_1;
	int32_t ___num_2;
	char* ___name_3;
	char* ___desc_4;
};
// Native definition for COM marshalling of MapEditor/sBeiShuDouZiConfig
struct sBeiShuDouZiConfig_t599764670_marshaled_com
{
	int32_t ___id_0;
	float ___value_1;
	int32_t ___num_2;
	Il2CppChar* ___name_3;
	Il2CppChar* ___desc_4;
};
#endif // SBEISHUDOUZICONFIG_T599764670_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef DPADAXIS_T1048722470_H
#define DPADAXIS_T1048722470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/DPadAxis
struct  DPadAxis_t1048722470 
{
public:
	// System.Int32 ETCBase/DPadAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DPadAxis_t1048722470, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPADAXIS_T1048722470_H
#ifndef PHOTONTARGETS_T2730697525_H
#define PHOTONTARGETS_T2730697525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonTargets
struct  PhotonTargets_t2730697525 
{
public:
	// System.Int32 PhotonTargets::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonTargets_t2730697525, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONTARGETS_T2730697525_H
#ifndef PHOTONLOGLEVEL_T4226222036_H
#define PHOTONLOGLEVEL_T4226222036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonLogLevel
struct  PhotonLogLevel_t4226222036 
{
public:
	// System.Int32 PhotonLogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonLogLevel_t4226222036, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONLOGLEVEL_T4226222036_H
#ifndef PHOTONNETWORKINGMESSAGE_T1476457985_H
#define PHOTONNETWORKINGMESSAGE_T1476457985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetworkingMessage
struct  PhotonNetworkingMessage_t1476457985 
{
public:
	// System.Int32 PhotonNetworkingMessage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonNetworkingMessage_t1476457985, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONNETWORKINGMESSAGE_T1476457985_H
#ifndef EMSGBOXTYPE_T2231895019_H
#define EMSGBOXTYPE_T2231895019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MsgBox/eMsgBoxType
struct  eMsgBoxType_t2231895019 
{
public:
	// System.Int32 MsgBox/eMsgBoxType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMsgBoxType_t2231895019, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMSGBOXTYPE_T2231895019_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef EMAPOBJTYPE_T1356972478_H
#define EMAPOBJTYPE_T1356972478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapObj/eMapObjType
struct  eMapObjType_t1356972478 
{
public:
	// System.Int32 MapObj/eMapObjType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMapObjType_t1356972478, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAPOBJTYPE_T1356972478_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef CLOUDREGIONCODE_T1925019500_H
#define CLOUDREGIONCODE_T1925019500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionCode
struct  CloudRegionCode_t1925019500 
{
public:
	// System.Int32 CloudRegionCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionCode_t1925019500, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONCODE_T1925019500_H
#ifndef JOINMODE_T2253954680_H
#define JOINMODE_T2253954680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinMode
struct  JoinMode_t2253954680 
{
public:
	// System.Byte JoinMode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinMode_t2253954680, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINMODE_T2253954680_H
#ifndef MATCHMAKINGMODE_T1165119589_H
#define MATCHMAKINGMODE_T1165119589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatchmakingMode
struct  MatchmakingMode_t1165119589 
{
public:
	// System.Byte MatchmakingMode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MatchmakingMode_t1165119589, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHMAKINGMODE_T1165119589_H
#ifndef RECEIVERGROUP_T3206452792_H
#define RECEIVERGROUP_T3206452792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReceiverGroup
struct  ReceiverGroup_t3206452792 
{
public:
	// System.Byte ReceiverGroup::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReceiverGroup_t3206452792, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVERGROUP_T3206452792_H
#ifndef EVENTCACHING_T168509732_H
#define EVENTCACHING_T168509732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventCaching
struct  EventCaching_t168509732 
{
public:
	// System.Byte EventCaching::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventCaching_t168509732, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCACHING_T168509732_H
#ifndef PROPERTYTYPEFLAG_T3957538841_H
#define PROPERTYTYPEFLAG_T3957538841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyTypeFlag
struct  PropertyTypeFlag_t3957538841 
{
public:
	// System.Byte PropertyTypeFlag::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyTypeFlag_t3957538841, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPEFLAG_T3957538841_H
#ifndef LOBBYTYPE_T3695323860_H
#define LOBBYTYPE_T3695323860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyType
struct  LobbyType_t3695323860 
{
public:
	// System.Byte LobbyType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LobbyType_t3695323860, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYTYPE_T3695323860_H
#ifndef ROOMOPTIONBIT_T4007729377_H
#define ROOMOPTIONBIT_T4007729377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadBalancingPeer/RoomOptionBit
struct  RoomOptionBit_t4007729377 
{
public:
	// System.Int32 LoadBalancingPeer/RoomOptionBit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RoomOptionBit_t4007729377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMOPTIONBIT_T4007729377_H
#ifndef CLOUDREGIONFLAG_T3756941471_H
#define CLOUDREGIONFLAG_T3756941471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionFlag
struct  CloudRegionFlag_t3756941471 
{
public:
	// System.Int32 CloudRegionFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionFlag_t3756941471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONFLAG_T3756941471_H
#ifndef CONNECTIONSTATE_T836644691_H
#define CONNECTIONSTATE_T836644691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectionState
struct  ConnectionState_t836644691 
{
public:
	// System.Int32 ConnectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionState_t836644691, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATE_T836644691_H
#ifndef ENCRYPTIONMODE_T4213192103_H
#define ENCRYPTIONMODE_T4213192103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EncryptionMode
struct  EncryptionMode_t4213192103 
{
public:
	// System.Int32 EncryptionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EncryptionMode_t4213192103, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONMODE_T4213192103_H
#ifndef CAMERATARGETMODE_T4109937804_H
#define CAMERATARGETMODE_T4109937804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/CameraTargetMode
struct  CameraTargetMode_t4109937804 
{
public:
	// System.Int32 ETCBase/CameraTargetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraTargetMode_t4109937804, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATARGETMODE_T4109937804_H
#ifndef CAMERAMODE_T364125053_H
#define CAMERAMODE_T364125053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/CameraMode
struct  CameraMode_t364125053 
{
public:
	// System.Int32 ETCBase/CameraMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraMode_t364125053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODE_T364125053_H
#ifndef RECTANCHOR_T2504559136_H
#define RECTANCHOR_T2504559136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/RectAnchor
struct  RectAnchor_t2504559136 
{
public:
	// System.Int32 ETCBase/RectAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RectAnchor_t2504559136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANCHOR_T2504559136_H
#ifndef GIZMOTYPE_T3704257101_H
#define GIZMOTYPE_T3704257101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.GUI.GizmoType
struct  GizmoType_t3704257101 
{
public:
	// System.Int32 ExitGames.Client.GUI.GizmoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GizmoType_t3704257101, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOTYPE_T3704257101_H
#ifndef ONTOUCHSTARTHANDLER_T1924473215_H
#define ONTOUCHSTARTHANDLER_T1924473215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnTouchStartHandler
struct  OnTouchStartHandler_t1924473215  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSTARTHANDLER_T1924473215_H
#ifndef ONPRESSUPHANDLER_T2844311173_H
#define ONPRESSUPHANDLER_T2844311173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnPressUpHandler
struct  OnPressUpHandler_t2844311173  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSUPHANDLER_T2844311173_H
#ifndef ONDOWNRIGHTHANDLER_T881666752_H
#define ONDOWNRIGHTHANDLER_T881666752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnDownRightHandler
struct  OnDownRightHandler_t881666752  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNRIGHTHANDLER_T881666752_H
#ifndef ONDOWNLEFTHANDLER_T2607413051_H
#define ONDOWNLEFTHANDLER_T2607413051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnDownLeftHandler
struct  OnDownLeftHandler_t2607413051  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLEFTHANDLER_T2607413051_H
#ifndef ONDOWNDOWNHANDLER_T2311908456_H
#define ONDOWNDOWNHANDLER_T2311908456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnDownDownHandler
struct  OnDownDownHandler_t2311908456  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNDOWNHANDLER_T2311908456_H
#ifndef ONDOWNUPHANDLER_T3611259663_H
#define ONDOWNUPHANDLER_T3611259663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnDownUpHandler
struct  OnDownUpHandler_t3611259663  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNUPHANDLER_T3611259663_H
#ifndef ONTOUCHUPHANDLER_T4157265644_H
#define ONTOUCHUPHANDLER_T4157265644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnTouchUpHandler
struct  OnTouchUpHandler_t4157265644  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHUPHANDLER_T4157265644_H
#ifndef ONPRESSDOWNHANDLER_T3635540872_H
#define ONPRESSDOWNHANDLER_T3635540872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnPressDownHandler
struct  OnPressDownHandler_t3635540872  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSDOWNHANDLER_T3635540872_H
#ifndef ONMOVESTARTHANDLER_T2512529097_H
#define ONMOVESTARTHANDLER_T2512529097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnMoveStartHandler
struct  OnMoveStartHandler_t2512529097  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESTARTHANDLER_T2512529097_H
#ifndef RADIUSBASE_T1799921463_H
#define RADIUSBASE_T1799921463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/RadiusBase
struct  RadiusBase_t1799921463 
{
public:
	// System.Int32 ETCJoystick/RadiusBase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RadiusBase_t1799921463, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIUSBASE_T1799921463_H
#ifndef JOYSTICKTYPE_T2334749223_H
#define JOYSTICKTYPE_T2334749223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/JoystickType
struct  JoystickType_t2334749223 
{
public:
	// System.Int32 ETCJoystick/JoystickType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoystickType_t2334749223, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKTYPE_T2334749223_H
#ifndef JOYSTICKAREA_T453609051_H
#define JOYSTICKAREA_T453609051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/JoystickArea
struct  JoystickArea_t453609051 
{
public:
	// System.Int32 ETCJoystick/JoystickArea::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoystickArea_t453609051, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKAREA_T453609051_H
#ifndef ONPRESSRIGHTHANDLER_T1812712090_H
#define ONPRESSRIGHTHANDLER_T1812712090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnPressRightHandler
struct  OnPressRightHandler_t1812712090  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSRIGHTHANDLER_T1812712090_H
#ifndef ONPRESSLEFTHANDLER_T137900968_H
#define ONPRESSLEFTHANDLER_T137900968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnPressLeftHandler
struct  OnPressLeftHandler_t137900968  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSLEFTHANDLER_T137900968_H
#ifndef ONTOUCHSTARTHANDLER_T3335545810_H
#define ONTOUCHSTARTHANDLER_T3335545810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnTouchStartHandler
struct  OnTouchStartHandler_t3335545810  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSTARTHANDLER_T3335545810_H
#ifndef ONPRESSUPHANDLER_T847273595_H
#define ONPRESSUPHANDLER_T847273595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnPressUpHandler
struct  OnPressUpHandler_t847273595  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSUPHANDLER_T847273595_H
#ifndef ONDOWNRIGHTHANDLER_T491698460_H
#define ONDOWNRIGHTHANDLER_T491698460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnDownRightHandler
struct  OnDownRightHandler_t491698460  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNRIGHTHANDLER_T491698460_H
#ifndef ONDOWNLEFTHANDLER_T3608179835_H
#define ONDOWNLEFTHANDLER_T3608179835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnDownLeftHandler
struct  OnDownLeftHandler_t3608179835  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLEFTHANDLER_T3608179835_H
#ifndef ONDOWNDOWNHANDLER_T266120898_H
#define ONDOWNDOWNHANDLER_T266120898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnDownDownHandler
struct  OnDownDownHandler_t266120898  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNDOWNHANDLER_T266120898_H
#ifndef ONDOWNUPHANDLER_T3788923810_H
#define ONDOWNUPHANDLER_T3788923810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnDownUpHandler
struct  OnDownUpHandler_t3788923810  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNUPHANDLER_T3788923810_H
#ifndef ONTOUCHUPHANDLER_T3432258596_H
#define ONTOUCHUPHANDLER_T3432258596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnTouchUPHandler
struct  OnTouchUPHandler_t3432258596  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHUPHANDLER_T3432258596_H
#ifndef ONPRESSDOWNHANDLER_T249715675_H
#define ONPRESSDOWNHANDLER_T249715675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnPressDownHandler
struct  OnPressDownHandler_t249715675  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSDOWNHANDLER_T249715675_H
#ifndef ONMOVEENDHANDLER_T3637094994_H
#define ONMOVEENDHANDLER_T3637094994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnMoveEndHandler
struct  OnMoveEndHandler_t3637094994  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEENDHANDLER_T3637094994_H
#ifndef ONMOVEHANDLER_T1792703951_H
#define ONMOVEHANDLER_T1792703951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnMoveHandler
struct  OnMoveHandler_t1792703951  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEHANDLER_T1792703951_H
#ifndef ONMOVESPEEDHANDLER_T961423656_H
#define ONMOVESPEEDHANDLER_T961423656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnMoveSpeedHandler
struct  OnMoveSpeedHandler_t961423656  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESPEEDHANDLER_T961423656_H
#ifndef ONMOVESTARTHANDLER_T2779627305_H
#define ONMOVESTARTHANDLER_T2779627305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick/OnMoveStartHandler
struct  OnMoveStartHandler_t2779627305  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESTARTHANDLER_T2779627305_H
#ifndef ONPRESSRIGHTHANDLER_T2352804955_H
#define ONPRESSRIGHTHANDLER_T2352804955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnPressRightHandler
struct  OnPressRightHandler_t2352804955  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSRIGHTHANDLER_T2352804955_H
#ifndef ONPRESSLEFTHANDLER_T1832992367_H
#define ONPRESSLEFTHANDLER_T1832992367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnPressLeftHandler
struct  OnPressLeftHandler_t1832992367  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSLEFTHANDLER_T1832992367_H
#ifndef ONMOVEHANDLER_T3659216741_H
#define ONMOVEHANDLER_T3659216741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnMoveHandler
struct  OnMoveHandler_t3659216741  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEHANDLER_T3659216741_H
#ifndef ONPRESSUPHANDLER_T3688972238_H
#define ONPRESSUPHANDLER_T3688972238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnPressUpHandler
struct  OnPressUpHandler_t3688972238  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSUPHANDLER_T3688972238_H
#ifndef ONPRESSDOWNHANDLER_T3681978565_H
#define ONPRESSDOWNHANDLER_T3681978565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnPressDownHandler
struct  OnPressDownHandler_t3681978565  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSDOWNHANDLER_T3681978565_H
#ifndef ONPRESSLEFTHANDLER_T2603277495_H
#define ONPRESSLEFTHANDLER_T2603277495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnPressLeftHandler
struct  OnPressLeftHandler_t2603277495  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSLEFTHANDLER_T2603277495_H
#ifndef ONPRESSRIGHTHANDLER_T1078071762_H
#define ONPRESSRIGHTHANDLER_T1078071762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnPressRightHandler
struct  OnPressRightHandler_t1078071762  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSRIGHTHANDLER_T1078071762_H
#ifndef EMAPEDITOROP_T2959825511_H
#define EMAPEDITOROP_T2959825511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/eMapEditorOp
struct  eMapEditorOp_t2959825511 
{
public:
	// System.Int32 MapEditor/eMapEditorOp::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMapEditorOp_t2959825511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAPEDITOROP_T2959825511_H
#ifndef EEATMODE_T3780619607_H
#define EEATMODE_T3780619607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/eEatMode
struct  eEatMode_t3780619607 
{
public:
	// System.Int32 MapEditor/eEatMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eEatMode_t3780619607, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEATMODE_T3780619607_H
#ifndef EGAMEMODE_T104648515_H
#define EGAMEMODE_T104648515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/eGameMode
struct  eGameMode_t104648515 
{
public:
	// System.Int32 Launcher/eGameMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eGameMode_t104648515, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGAMEMODE_T104648515_H
#ifndef ESCENEMODE_T4177333499_H
#define ESCENEMODE_T4177333499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/eSceneMode
struct  eSceneMode_t4177333499 
{
public:
	// System.Int32 Launcher/eSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSceneMode_t4177333499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCENEMODE_T4177333499_H
#ifndef EINSTANTIATETYPE_T163196549_H
#define EINSTANTIATETYPE_T163196549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/eInstantiateType
struct  eInstantiateType_t163196549 
{
public:
	// System.Int32 Main/eInstantiateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eInstantiateType_t163196549, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EINSTANTIATETYPE_T163196549_H
#ifndef EPLATFORMTYPE_T2884586383_H
#define EPLATFORMTYPE_T2884586383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/ePlatformType
struct  ePlatformType_t2884586383 
{
public:
	// System.Int32 Main/ePlatformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ePlatformType_t2884586383, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPLATFORMTYPE_T2884586383_H
#ifndef ONDOWNRIGHTHANDLER_T1395399745_H
#define ONDOWNRIGHTHANDLER_T1395399745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnDownRightHandler
struct  OnDownRightHandler_t1395399745  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNRIGHTHANDLER_T1395399745_H
#ifndef ONTOUCHUPHANDLER_T2186573936_H
#define ONTOUCHUPHANDLER_T2186573936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnTouchUPHandler
struct  OnTouchUPHandler_t2186573936  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHUPHANDLER_T2186573936_H
#ifndef ONDOWNUPHANDLER_T3325091394_H
#define ONDOWNUPHANDLER_T3325091394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnDownUpHandler
struct  OnDownUpHandler_t3325091394  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNUPHANDLER_T3325091394_H
#ifndef ONTOUCHSTARTHANDLER_T3223438894_H
#define ONTOUCHSTARTHANDLER_T3223438894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnTouchStartHandler
struct  OnTouchStartHandler_t3223438894  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSTARTHANDLER_T3223438894_H
#ifndef ONMOVEENDHANDLER_T330401481_H
#define ONMOVEENDHANDLER_T330401481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnMoveEndHandler
struct  OnMoveEndHandler_t330401481  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEENDHANDLER_T330401481_H
#ifndef ONDOWNDOWNHANDLER_T3809127321_H
#define ONDOWNDOWNHANDLER_T3809127321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnDownDownHandler
struct  OnDownDownHandler_t3809127321  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNDOWNHANDLER_T3809127321_H
#ifndef ONDOWNLEFTHANDLER_T2754992532_H
#define ONDOWNLEFTHANDLER_T2754992532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnDownLeftHandler
struct  OnDownLeftHandler_t2754992532  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLEFTHANDLER_T2754992532_H
#ifndef ONMOVESPEEDHANDLER_T3603010941_H
#define ONMOVESPEEDHANDLER_T3603010941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad/OnMoveSpeedHandler
struct  OnMoveSpeedHandler_t3603010941  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESPEEDHANDLER_T3603010941_H
#ifndef SREBORNRANPOS_T34662718_H
#define SREBORNRANPOS_T34662718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor/sRebornRanPos
struct  sRebornRanPos_t34662718 
{
public:
	// UnityEngine.Vector3 MapEditor/sRebornRanPos::vecMinPos
	Vector3_t3722313464  ___vecMinPos_0;
	// UnityEngine.Vector3 MapEditor/sRebornRanPos::vecMaxPos
	Vector3_t3722313464  ___vecMaxPos_1;

public:
	inline static int32_t get_offset_of_vecMinPos_0() { return static_cast<int32_t>(offsetof(sRebornRanPos_t34662718, ___vecMinPos_0)); }
	inline Vector3_t3722313464  get_vecMinPos_0() const { return ___vecMinPos_0; }
	inline Vector3_t3722313464 * get_address_of_vecMinPos_0() { return &___vecMinPos_0; }
	inline void set_vecMinPos_0(Vector3_t3722313464  value)
	{
		___vecMinPos_0 = value;
	}

	inline static int32_t get_offset_of_vecMaxPos_1() { return static_cast<int32_t>(offsetof(sRebornRanPos_t34662718, ___vecMaxPos_1)); }
	inline Vector3_t3722313464  get_vecMaxPos_1() const { return ___vecMaxPos_1; }
	inline Vector3_t3722313464 * get_address_of_vecMaxPos_1() { return &___vecMaxPos_1; }
	inline void set_vecMaxPos_1(Vector3_t3722313464  value)
	{
		___vecMaxPos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SREBORNRANPOS_T34662718_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef OPJOINRANDOMROOMPARAMS_T491090087_H
#define OPJOINRANDOMROOMPARAMS_T491090087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpJoinRandomRoomParams
struct  OpJoinRandomRoomParams_t491090087  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.Hashtable OpJoinRandomRoomParams::ExpectedCustomRoomProperties
	Hashtable_t1048209202 * ___ExpectedCustomRoomProperties_0;
	// System.Byte OpJoinRandomRoomParams::ExpectedMaxPlayers
	uint8_t ___ExpectedMaxPlayers_1;
	// MatchmakingMode OpJoinRandomRoomParams::MatchingType
	uint8_t ___MatchingType_2;
	// TypedLobby OpJoinRandomRoomParams::TypedLobby
	TypedLobby_t3336582029 * ___TypedLobby_3;
	// System.String OpJoinRandomRoomParams::SqlLobbyFilter
	String_t* ___SqlLobbyFilter_4;
	// System.String[] OpJoinRandomRoomParams::ExpectedUsers
	StringU5BU5D_t1281789340* ___ExpectedUsers_5;

public:
	inline static int32_t get_offset_of_ExpectedCustomRoomProperties_0() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___ExpectedCustomRoomProperties_0)); }
	inline Hashtable_t1048209202 * get_ExpectedCustomRoomProperties_0() const { return ___ExpectedCustomRoomProperties_0; }
	inline Hashtable_t1048209202 ** get_address_of_ExpectedCustomRoomProperties_0() { return &___ExpectedCustomRoomProperties_0; }
	inline void set_ExpectedCustomRoomProperties_0(Hashtable_t1048209202 * value)
	{
		___ExpectedCustomRoomProperties_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedCustomRoomProperties_0), value);
	}

	inline static int32_t get_offset_of_ExpectedMaxPlayers_1() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___ExpectedMaxPlayers_1)); }
	inline uint8_t get_ExpectedMaxPlayers_1() const { return ___ExpectedMaxPlayers_1; }
	inline uint8_t* get_address_of_ExpectedMaxPlayers_1() { return &___ExpectedMaxPlayers_1; }
	inline void set_ExpectedMaxPlayers_1(uint8_t value)
	{
		___ExpectedMaxPlayers_1 = value;
	}

	inline static int32_t get_offset_of_MatchingType_2() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___MatchingType_2)); }
	inline uint8_t get_MatchingType_2() const { return ___MatchingType_2; }
	inline uint8_t* get_address_of_MatchingType_2() { return &___MatchingType_2; }
	inline void set_MatchingType_2(uint8_t value)
	{
		___MatchingType_2 = value;
	}

	inline static int32_t get_offset_of_TypedLobby_3() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___TypedLobby_3)); }
	inline TypedLobby_t3336582029 * get_TypedLobby_3() const { return ___TypedLobby_3; }
	inline TypedLobby_t3336582029 ** get_address_of_TypedLobby_3() { return &___TypedLobby_3; }
	inline void set_TypedLobby_3(TypedLobby_t3336582029 * value)
	{
		___TypedLobby_3 = value;
		Il2CppCodeGenWriteBarrier((&___TypedLobby_3), value);
	}

	inline static int32_t get_offset_of_SqlLobbyFilter_4() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___SqlLobbyFilter_4)); }
	inline String_t* get_SqlLobbyFilter_4() const { return ___SqlLobbyFilter_4; }
	inline String_t** get_address_of_SqlLobbyFilter_4() { return &___SqlLobbyFilter_4; }
	inline void set_SqlLobbyFilter_4(String_t* value)
	{
		___SqlLobbyFilter_4 = value;
		Il2CppCodeGenWriteBarrier((&___SqlLobbyFilter_4), value);
	}

	inline static int32_t get_offset_of_ExpectedUsers_5() { return static_cast<int32_t>(offsetof(OpJoinRandomRoomParams_t491090087, ___ExpectedUsers_5)); }
	inline StringU5BU5D_t1281789340* get_ExpectedUsers_5() const { return ___ExpectedUsers_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ExpectedUsers_5() { return &___ExpectedUsers_5; }
	inline void set_ExpectedUsers_5(StringU5BU5D_t1281789340* value)
	{
		___ExpectedUsers_5 = value;
		Il2CppCodeGenWriteBarrier((&___ExpectedUsers_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPJOINRANDOMROOMPARAMS_T491090087_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_6;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_7;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_8;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_9;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_10;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_11;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_12;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_13;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_14;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::commandLogSize
	int32_t ___commandLogSize_15;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_16;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_17;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_21;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_22;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_23;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_24;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_25;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_26;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_27;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_29;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_30;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_31;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_34;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_35;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::encryptor
	Encryptor_t200327285 * ___encryptor_36;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::decryptor
	Decryptor_t2116099858 * ___decryptor_37;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_5), value);
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_6)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_6() const { return ___SocketImplementationConfig_6; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_6() { return &___SocketImplementationConfig_6; }
	inline void set_SocketImplementationConfig_6(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_6), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_7)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_7() const { return ___U3CSocketImplementationU3Ek__BackingField_7; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_7() { return &___U3CSocketImplementationU3Ek__BackingField_7; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_7(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_DebugOut_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_8)); }
	inline uint8_t get_DebugOut_8() const { return ___DebugOut_8; }
	inline uint8_t* get_address_of_DebugOut_8() { return &___DebugOut_8; }
	inline void set_DebugOut_8(uint8_t value)
	{
		___DebugOut_8 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_9() const { return ___U3CListenerU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_9() { return &___U3CListenerU3Ek__BackingField_9; }
	inline void set_U3CListenerU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_10)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_10() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_10; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_10() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_10; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_10(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_11)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_11() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_11; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_11() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_11; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_11(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_12)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_12() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_12; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_12() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_12; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_12(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_13)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_13() const { return ___trafficStatsStopwatch_13; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_13() { return &___trafficStatsStopwatch_13; }
	inline void set_trafficStatsStopwatch_13(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_13 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_13), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_14)); }
	inline bool get_trafficStatsEnabled_14() const { return ___trafficStatsEnabled_14; }
	inline bool* get_address_of_trafficStatsEnabled_14() { return &___trafficStatsEnabled_14; }
	inline void set_trafficStatsEnabled_14(bool value)
	{
		___trafficStatsEnabled_14 = value;
	}

	inline static int32_t get_offset_of_commandLogSize_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___commandLogSize_15)); }
	inline int32_t get_commandLogSize_15() const { return ___commandLogSize_15; }
	inline int32_t* get_address_of_commandLogSize_15() { return &___commandLogSize_15; }
	inline void set_commandLogSize_15(int32_t value)
	{
		___commandLogSize_15 = value;
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_16)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_16() const { return ___U3CEnableServerTracingU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_16() { return &___U3CEnableServerTracingU3Ek__BackingField_16; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_16(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_17)); }
	inline uint8_t get_quickResendAttempts_17() const { return ___quickResendAttempts_17; }
	inline uint8_t* get_address_of_quickResendAttempts_17() { return &___quickResendAttempts_17; }
	inline void set_quickResendAttempts_17(uint8_t value)
	{
		___quickResendAttempts_17 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_18)); }
	inline int32_t get_RhttpMinConnections_18() const { return ___RhttpMinConnections_18; }
	inline int32_t* get_address_of_RhttpMinConnections_18() { return &___RhttpMinConnections_18; }
	inline void set_RhttpMinConnections_18(int32_t value)
	{
		___RhttpMinConnections_18 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_19)); }
	inline int32_t get_RhttpMaxConnections_19() const { return ___RhttpMaxConnections_19; }
	inline int32_t* get_address_of_RhttpMaxConnections_19() { return &___RhttpMaxConnections_19; }
	inline void set_RhttpMaxConnections_19(int32_t value)
	{
		___RhttpMaxConnections_19 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_21)); }
	inline uint8_t get_ChannelCount_21() const { return ___ChannelCount_21; }
	inline uint8_t* get_address_of_ChannelCount_21() { return &___ChannelCount_21; }
	inline void set_ChannelCount_21(uint8_t value)
	{
		___ChannelCount_21 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_22)); }
	inline bool get_crcEnabled_22() const { return ___crcEnabled_22; }
	inline bool* get_address_of_crcEnabled_22() { return &___crcEnabled_22; }
	inline void set_crcEnabled_22(bool value)
	{
		___crcEnabled_22 = value;
	}

	inline static int32_t get_offset_of_WarningSize_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_23)); }
	inline int32_t get_WarningSize_23() const { return ___WarningSize_23; }
	inline int32_t* get_address_of_WarningSize_23() { return &___WarningSize_23; }
	inline void set_WarningSize_23(int32_t value)
	{
		___WarningSize_23 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_24)); }
	inline int32_t get_SentCountAllowance_24() const { return ___SentCountAllowance_24; }
	inline int32_t* get_address_of_SentCountAllowance_24() { return &___SentCountAllowance_24; }
	inline void set_SentCountAllowance_24(int32_t value)
	{
		___SentCountAllowance_24 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_25)); }
	inline int32_t get_TimePingInterval_25() const { return ___TimePingInterval_25; }
	inline int32_t* get_address_of_TimePingInterval_25() { return &___TimePingInterval_25; }
	inline void set_TimePingInterval_25(int32_t value)
	{
		___TimePingInterval_25 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_26)); }
	inline int32_t get_DisconnectTimeout_26() const { return ___DisconnectTimeout_26; }
	inline int32_t* get_address_of_DisconnectTimeout_26() { return &___DisconnectTimeout_26; }
	inline void set_DisconnectTimeout_26(int32_t value)
	{
		___DisconnectTimeout_26 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_27)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_27() const { return ___U3CTransportProtocolU3Ek__BackingField_27; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_27() { return &___U3CTransportProtocolU3Ek__BackingField_27; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_27(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_mtu_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_29)); }
	inline int32_t get_mtu_29() const { return ___mtu_29; }
	inline int32_t* get_address_of_mtu_29() { return &___mtu_29; }
	inline void set_mtu_29(int32_t value)
	{
		___mtu_29 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_30)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_30() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_30() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_30; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_30(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_peerBase_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_31)); }
	inline PeerBase_t2956237011 * get_peerBase_31() const { return ___peerBase_31; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_31() { return &___peerBase_31; }
	inline void set_peerBase_31(PeerBase_t2956237011 * value)
	{
		___peerBase_31 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_31), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_32)); }
	inline RuntimeObject * get_SendOutgoingLockObject_32() const { return ___SendOutgoingLockObject_32; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_32() { return &___SendOutgoingLockObject_32; }
	inline void set_SendOutgoingLockObject_32(RuntimeObject * value)
	{
		___SendOutgoingLockObject_32 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_32), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_33)); }
	inline RuntimeObject * get_DispatchLockObject_33() const { return ___DispatchLockObject_33; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_33() { return &___DispatchLockObject_33; }
	inline void set_DispatchLockObject_33(RuntimeObject * value)
	{
		___DispatchLockObject_33 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_33), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_34)); }
	inline RuntimeObject * get_EnqueueLock_34() const { return ___EnqueueLock_34; }
	inline RuntimeObject ** get_address_of_EnqueueLock_34() { return &___EnqueueLock_34; }
	inline void set_EnqueueLock_34(RuntimeObject * value)
	{
		___EnqueueLock_34 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_34), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_35)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_35() const { return ___PayloadEncryptionSecret_35; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_35() { return &___PayloadEncryptionSecret_35; }
	inline void set_PayloadEncryptionSecret_35(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_35 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_35), value);
	}

	inline static int32_t get_offset_of_encryptor_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___encryptor_36)); }
	inline Encryptor_t200327285 * get_encryptor_36() const { return ___encryptor_36; }
	inline Encryptor_t200327285 ** get_address_of_encryptor_36() { return &___encryptor_36; }
	inline void set_encryptor_36(Encryptor_t200327285 * value)
	{
		___encryptor_36 = value;
		Il2CppCodeGenWriteBarrier((&___encryptor_36), value);
	}

	inline static int32_t get_offset_of_decryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___decryptor_37)); }
	inline Decryptor_t2116099858 * get_decryptor_37() const { return ___decryptor_37; }
	inline Decryptor_t2116099858 ** get_address_of_decryptor_37() { return &___decryptor_37; }
	inline void set_decryptor_37(Decryptor_t2116099858 * value)
	{
		___decryptor_37 = value;
		Il2CppCodeGenWriteBarrier((&___decryptor_37), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_28;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_28)); }
	inline int32_t get_OutgoingStreamBufferSize_28() const { return ___OutgoingStreamBufferSize_28; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_28() { return &___OutgoingStreamBufferSize_28; }
	inline void set_OutgoingStreamBufferSize_28(int32_t value)
	{
		___OutgoingStreamBufferSize_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef RAISEEVENTOPTIONS_T1229553678_H
#define RAISEEVENTOPTIONS_T1229553678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaiseEventOptions
struct  RaiseEventOptions_t1229553678  : public RuntimeObject
{
public:
	// EventCaching RaiseEventOptions::CachingOption
	uint8_t ___CachingOption_1;
	// System.Byte RaiseEventOptions::InterestGroup
	uint8_t ___InterestGroup_2;
	// System.Int32[] RaiseEventOptions::TargetActors
	Int32U5BU5D_t385246372* ___TargetActors_3;
	// ReceiverGroup RaiseEventOptions::Receivers
	uint8_t ___Receivers_4;
	// System.Byte RaiseEventOptions::SequenceChannel
	uint8_t ___SequenceChannel_5;
	// System.Boolean RaiseEventOptions::ForwardToWebhook
	bool ___ForwardToWebhook_6;
	// System.Boolean RaiseEventOptions::Encrypt
	bool ___Encrypt_7;

public:
	inline static int32_t get_offset_of_CachingOption_1() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___CachingOption_1)); }
	inline uint8_t get_CachingOption_1() const { return ___CachingOption_1; }
	inline uint8_t* get_address_of_CachingOption_1() { return &___CachingOption_1; }
	inline void set_CachingOption_1(uint8_t value)
	{
		___CachingOption_1 = value;
	}

	inline static int32_t get_offset_of_InterestGroup_2() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___InterestGroup_2)); }
	inline uint8_t get_InterestGroup_2() const { return ___InterestGroup_2; }
	inline uint8_t* get_address_of_InterestGroup_2() { return &___InterestGroup_2; }
	inline void set_InterestGroup_2(uint8_t value)
	{
		___InterestGroup_2 = value;
	}

	inline static int32_t get_offset_of_TargetActors_3() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___TargetActors_3)); }
	inline Int32U5BU5D_t385246372* get_TargetActors_3() const { return ___TargetActors_3; }
	inline Int32U5BU5D_t385246372** get_address_of_TargetActors_3() { return &___TargetActors_3; }
	inline void set_TargetActors_3(Int32U5BU5D_t385246372* value)
	{
		___TargetActors_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetActors_3), value);
	}

	inline static int32_t get_offset_of_Receivers_4() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___Receivers_4)); }
	inline uint8_t get_Receivers_4() const { return ___Receivers_4; }
	inline uint8_t* get_address_of_Receivers_4() { return &___Receivers_4; }
	inline void set_Receivers_4(uint8_t value)
	{
		___Receivers_4 = value;
	}

	inline static int32_t get_offset_of_SequenceChannel_5() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___SequenceChannel_5)); }
	inline uint8_t get_SequenceChannel_5() const { return ___SequenceChannel_5; }
	inline uint8_t* get_address_of_SequenceChannel_5() { return &___SequenceChannel_5; }
	inline void set_SequenceChannel_5(uint8_t value)
	{
		___SequenceChannel_5 = value;
	}

	inline static int32_t get_offset_of_ForwardToWebhook_6() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___ForwardToWebhook_6)); }
	inline bool get_ForwardToWebhook_6() const { return ___ForwardToWebhook_6; }
	inline bool* get_address_of_ForwardToWebhook_6() { return &___ForwardToWebhook_6; }
	inline void set_ForwardToWebhook_6(bool value)
	{
		___ForwardToWebhook_6 = value;
	}

	inline static int32_t get_offset_of_Encrypt_7() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___Encrypt_7)); }
	inline bool get_Encrypt_7() const { return ___Encrypt_7; }
	inline bool* get_address_of_Encrypt_7() { return &___Encrypt_7; }
	inline void set_Encrypt_7(bool value)
	{
		___Encrypt_7 = value;
	}
};

struct RaiseEventOptions_t1229553678_StaticFields
{
public:
	// RaiseEventOptions RaiseEventOptions::Default
	RaiseEventOptions_t1229553678 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678_StaticFields, ___Default_0)); }
	inline RaiseEventOptions_t1229553678 * get_Default_0() const { return ___Default_0; }
	inline RaiseEventOptions_t1229553678 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(RaiseEventOptions_t1229553678 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAISEEVENTOPTIONS_T1229553678_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef DELEGATEMETHOD_ONCLICKBUTTON_T3139999742_H
#define DELEGATEMETHOD_ONCLICKBUTTON_T3139999742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MsgBox/DelegateMethod_OnClickButton
struct  DelegateMethod_OnClickButton_t3139999742  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEMETHOD_ONCLICKBUTTON_T3139999742_H
#ifndef LOADBALANCINGPEER_T3218467959_H
#define LOADBALANCINGPEER_T3218467959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadBalancingPeer
struct  LoadBalancingPeer_t3218467959  : public PhotonPeer_t1608153861
{
public:
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> LoadBalancingPeer::opParameters
	Dictionary_2_t1405253484 * ___opParameters_38;

public:
	inline static int32_t get_offset_of_opParameters_38() { return static_cast<int32_t>(offsetof(LoadBalancingPeer_t3218467959, ___opParameters_38)); }
	inline Dictionary_2_t1405253484 * get_opParameters_38() const { return ___opParameters_38; }
	inline Dictionary_2_t1405253484 ** get_address_of_opParameters_38() { return &___opParameters_38; }
	inline void set_opParameters_38(Dictionary_2_t1405253484 * value)
	{
		___opParameters_38 = value;
		Il2CppCodeGenWriteBarrier((&___opParameters_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADBALANCINGPEER_T3218467959_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef MAPEDITOR_T878680970_H
#define MAPEDITOR_T878680970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapEditor
struct  MapEditor_t878680970  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MapEditor::_txtDebugInfo
	Text_t1901882714 * ____txtDebugInfo_3;
	// UnityEngine.UI.Text MapEditor::_txtDebugInfoInEditor
	Text_t1901882714 * ____txtDebugInfoInEditor_4;
	// System.Single MapEditor::m_fMinCameraSize
	float ___m_fMinCameraSize_6;
	// System.Single MapEditor::m_fMaxCameraSize
	float ___m_fMaxCameraSize_7;
	// System.Single MapEditor::m_fCameraZoomSpeed
	float ___m_fCameraZoomSpeed_8;
	// System.Single MapEditor::m_fBallSizeAffectCamSize
	float ___m_fBallSizeAffectCamSize_9;
	// Ball MapEditor::m_ReferBall
	Ball_t2206666566 * ___m_ReferBall_10;
	// System.Single MapEditor::m_fWorldSizeX
	float ___m_fWorldSizeX_11;
	// System.Single MapEditor::m_fWorldSizeY
	float ___m_fWorldSizeY_12;
	// System.Single MapEditor::m_fWorldWidth
	float ___m_fWorldWidth_13;
	// System.Single MapEditor::m_fWorldHeight
	float ___m_fWorldHeight_14;
	// System.Single MapEditor::m_fHalfWorldWidth
	float ___m_fHalfWorldWidth_15;
	// System.Single MapEditor::m_fHalfWorldHeight
	float ___m_fHalfWorldHeight_16;
	// WorldBorder MapEditor::m_goWorldBorderTop
	WorldBorder_t1275663748 * ___m_goWorldBorderTop_17;
	// WorldBorder MapEditor::m_goWorldBorderBottom
	WorldBorder_t1275663748 * ___m_goWorldBorderBottom_18;
	// WorldBorder MapEditor::m_goWorldBorderLeft
	WorldBorder_t1275663748 * ___m_goWorldBorderLeft_19;
	// WorldBorder MapEditor::m_goWorldBorderRight
	WorldBorder_t1275663748 * ___m_goWorldBorderRight_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> MapEditor::m_lstRebornPos
	List_1_t899420910 * ___m_lstRebornPos_21;
	// UnityEngine.Vector3 MapEditor::vecTemp
	Vector3_t3722313464  ___vecTemp_22;
	// UnityEngine.Vector3 MapEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_23;
	// UnityEngine.Vector3 MapEditor::vecTempScale
	Vector3_t3722313464  ___vecTempScale_24;
	// UnityEngine.GameObject MapEditor::m_preGrassTile
	GameObject_t1113636619 * ___m_preGrassTile_25;
	// UnityEngine.GameObject MapEditor::m_preGridLine
	GameObject_t1113636619 * ___m_preGridLine_26;
	// UnityEngine.GameObject MapEditor::m_goGridLineContainer
	GameObject_t1113636619 * ___m_goGridLineContainer_27;
	// UnityEngine.GameObject MapEditor::m_goGrassContainer
	GameObject_t1113636619 * ___m_goGrassContainer_28;
	// UnityEngine.GameObject MapEditor::m_goPolygonContainer
	GameObject_t1113636619 * ___m_goPolygonContainer_29;
	// UnityEngine.GameObject MapEditor::m_goBeanSprayContainer
	GameObject_t1113636619 * ___m_goBeanSprayContainer_30;
	// UnityEngine.GameObject MapEditor::m_goRebornSpotContainer
	GameObject_t1113636619 * ___m_goRebornSpotContainer_31;
	// UnityEngine.GameObject MapEditor::m_goColorThornContainer
	GameObject_t1113636619 * ___m_goColorThornContainer_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> MapEditor::m_dicDelProtectTime
	Dictionary_2_t1182523073 * ___m_dicDelProtectTime_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,Polygon> MapEditor::m_dicGrass
	Dictionary_2_t3196372882 * ___m_dicGrass_35;
	// System.Single MapEditor::m_fGrassTileWidth
	float ___m_fGrassTileWidth_37;
	// System.Single MapEditor::m_fGrassTileHeight
	float ___m_fGrassTileHeight_38;
	// System.Single MapEditor::m_fGrassHalfTileWidth
	float ___m_fGrassHalfTileWidth_39;
	// System.Single MapEditor::m_fGrassHalfTileHeight
	float ___m_fGrassHalfTileHeight_40;
	// MapObj MapEditor::m_CurSelectedObj
	MapObj_t1733252447 * ___m_CurSelectedObj_41;
	// MapObj MapEditor::m_CurMovingObj
	MapObj_t1733252447 * ___m_CurMovingObj_42;
	// UnityEngine.Vector3 MapEditor::m_vecLastMousePos
	Vector3_t3722313464  ___m_vecLastMousePos_43;
	// UnityEngine.GameObject MapEditor::Screen_Cursor
	GameObject_t1113636619 * ___Screen_Cursor_44;
	// UnityEngine.GameObject MapEditor::UI_MiniMap
	GameObject_t1113636619 * ___UI_MiniMap_45;
	// UnityEngine.GameObject[] MapEditor::m_arySubPanel
	GameObjectU5BU5D_t3328599146* ___m_arySubPanel_46;
	// UnityEngine.UI.Dropdown MapEditor::dropdownItem
	Dropdown_t2274391225 * ___dropdownItem_47;
	// UnityEngine.GameObject MapEditor::_panelLogin
	GameObject_t1113636619 * ____panelLogin_48;
	// UnityEngine.AudioSource MapEditor::_musicMain
	AudioSource_t3935305588 * ____musicMain_49;
	// UnityEngine.UI.InputField MapEditor::_inputTest
	InputField_t3762917431 * ____inputTest_50;
	// UnityEngine.UI.Button MapEditor::_btnTest
	Button_t4055032469 * ____btnTest_51;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> MapEditor::_dicTest
	Dictionary_2_t736164020 * ____dicTest_52;
	// UnityEngine.UI.InputField MapEditor::_inputA
	InputField_t3762917431 * ____inputA_53;
	// UnityEngine.UI.InputField MapEditor::_inputB
	InputField_t3762917431 * ____inputB_54;
	// UnityEngine.UI.InputField MapEditor::_inputC
	InputField_t3762917431 * ____inputC_55;
	// UnityEngine.UI.InputField MapEditor::_inputX
	InputField_t3762917431 * ____inputX_56;
	// UnityEngine.UI.InputField MapEditor::_inputTimeBeforeShowDeadUI
	InputField_t3762917431 * ____inputTimeBeforeShowDeadUI_57;
	// UnityEngine.UI.InputField MapEditor::_inputTimeTimeOfOneRound
	InputField_t3762917431 * ____inputTimeTimeOfOneRound_58;
	// UnityEngine.UI.InputField MapEditor::_inputBaseVolumeDecreasedPercentWhenDead
	InputField_t3762917431 * ____inputBaseVolumeDecreasedPercentWhenDead_59;
	// UnityEngine.UI.InputField MapEditor::_inputClassCircleRadius
	InputField_t3762917431 * ____inputClassCircleRadius_60;
	// UnityEngine.UI.InputField MapEditor::_inputClassThreshold
	InputField_t3762917431 * ____inputClassThreshold_61;
	// UnityEngine.UI.InputField MapEditor::_inputWorldSizeX
	InputField_t3762917431 * ____inputWorldSizeX_62;
	// UnityEngine.UI.InputField MapEditor::_inputWorldSizeY
	InputField_t3762917431 * ____inputWorldSizeY_63;
	// UnityEngine.UI.InputField MapEditor::_inputfieldCamSizeMax
	InputField_t3762917431 * ____inputfieldCamSizeMax_64;
	// UnityEngine.UI.InputField MapEditor::_inputfieldCamSizeMin
	InputField_t3762917431 * ____inputfieldCamSizeMin_65;
	// UnityEngine.UI.InputField MapEditor::_inputCamShangFuXiShu
	InputField_t3762917431 * ____inputCamShangFuXiShu_66;
	// UnityEngine.UI.InputField MapEditor::_inputCamRaiseSpeed
	InputField_t3762917431 * ____inputCamRaiseSpeed_67;
	// UnityEngine.UI.InputField MapEditor::_inputCamShangFuAccelerate
	InputField_t3762917431 * ____inputCamShangFuAccelerate_68;
	// UnityEngine.UI.InputField MapEditor::_inputCamChangeDelaySpit
	InputField_t3762917431 * ____inputCamChangeDelaySpit_69;
	// UnityEngine.UI.InputField MapEditor::_inputCamChangeDelayExplode
	InputField_t3762917431 * ____inputCamChangeDelayExplode_70;
	// UnityEngine.UI.InputField MapEditor::_inputfieldCamRespondSpeed
	InputField_t3762917431 * ____inputfieldCamRespondSpeed_71;
	// UnityEngine.UI.InputField MapEditor::_inputCamTimeBeforeDown
	InputField_t3762917431 * ____inputCamTimeBeforeDown_72;
	// UnityEngine.UI.InputField MapEditor::_inputCamDownSpeed
	InputField_t3762917431 * ____inputCamDownSpeed_73;
	// UnityEngine.UI.InputField MapEditor::_inputfieldCamSizeXiShu
	InputField_t3762917431 * ____inputfieldCamSizeXiShu_74;
	// UnityEngine.UI.InputField MapEditor::_inputfieldCamZoomSpeed
	InputField_t3762917431 * ____inputfieldCamZoomSpeed_75;
	// UnityEngine.UI.InputField MapEditor::_inputBornProtectTime
	InputField_t3762917431 * ____inputBornProtectTime_76;
	// UnityEngine.UI.InputField MapEditor::_inputfieldBallSizeAffectCamSize
	InputField_t3762917431 * ____inputfieldBallSizeAffectCamSize_77;
	// UnityEngine.UI.InputField MapEditor::_inputfieldBeanNumPerRange
	InputField_t3762917431 * ____inputfieldBeanNumPerRange_78;
	// UnityEngine.UI.InputField MapEditor::_inputfieldBallBaseSpeed
	InputField_t3762917431 * ____inputfieldBallBaseSpeed_79;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMaxSpeed
	InputField_t3762917431 * ____inputfieldMaxSpeed_80;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpeedRadiusKaiFangFenMu
	InputField_t3762917431 * ____inputfieldSpeedRadiusKaiFangFenMu_81;
	// UnityEngine.UI.InputField MapEditor::_inputfieldBornMinDiameter
	InputField_t3762917431 * ____inputfieldBornMinDiameter_82;
	// UnityEngine.UI.InputField MapEditor::_inputfieldThornNumPerRange
	InputField_t3762917431 * ____inputfieldThornNumPerRange_83;
	// UnityEngine.UI.InputField MapEditor::_inputfieldGenerateNewBeanTimeInterval
	InputField_t3762917431 * ____inputfieldGenerateNewBeanTimeInterval_84;
	// UnityEngine.UI.InputField MapEditor::_inputfieldGenerateNewThornTimeInterval
	InputField_t3762917431 * ____inputfieldGenerateNewThornTimeInterval_85;
	// UnityEngine.UI.InputField MapEditor::_inputfieldBeanSize
	InputField_t3762917431 * ____inputfieldBeanSize_86;
	// UnityEngine.UI.InputField MapEditor::_inputfieldThornSize
	InputField_t3762917431 * ____inputfieldThornSize_87;
	// UnityEngine.UI.InputField MapEditor::_inputfieldThornContributeToBallSize
	InputField_t3762917431 * ____inputfieldThornContributeToBallSize_88;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitSporeCostMP
	InputField_t3762917431 * ____inputfieldSpitSporeCostMP_89;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitSporeThornId
	InputField_t3762917431 * ____inputfieldSpitSporeThornId_90;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitSporeRunDistance
	InputField_t3762917431 * ____inputfieldSpitSporeRunDistance_91;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitSporeBeanType
	InputField_t3762917431 * ____inputfieldSpitSporeBeanType_92;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitRunTime
	InputField_t3762917431 * ____inputfieldSpitRunTime_93;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitBallRunTime
	InputField_t3762917431 * ____inputfieldSpitBallRunTime_94;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitBallRunDistanceMultiple
	InputField_t3762917431 * ____inputfieldSpitBallRunDistanceMultiple_95;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitBallCostMP
	InputField_t3762917431 * ____inputfieldSpitBallCostMP_96;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitBallStayTime
	InputField_t3762917431 * ____inputfieldSpitBallStayTime_97;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitBallSpeed
	InputField_t3762917431 * ____inputfieldSpitBallSpeed_98;
	// UnityEngine.UI.InputField MapEditor::_inputfieldAutoAttenuate
	InputField_t3762917431 * ____inputfieldAutoAttenuate_99;
	// UnityEngine.UI.InputField MapEditor::_inputfieldThornAttenuate
	InputField_t3762917431 * ____inputfieldThornAttenuate_100;
	// UnityEngine.UI.InputField MapEditor::_inputfieldAutoAttenuateTimeInterval
	InputField_t3762917431 * ____inputfieldAutoAttenuateTimeInterval_101;
	// UnityEngine.UI.InputField MapEditor::_inputfieldForceSpitTotalTime
	InputField_t3762917431 * ____inputfieldForceSpitTotalTime_102;
	// UnityEngine.UI.InputField MapEditor::_inputfieldShellShrinkTotalTime
	InputField_t3762917431 * ____inputfieldShellShrinkTotalTime_103;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitShellTotalTime
	InputField_t3762917431 * ____inputfieldSplitShellTotalTime_104;
	// UnityEngine.UI.InputField MapEditor::_inputfieldExplodeShellTotalTime
	InputField_t3762917431 * ____inputfieldExplodeShellTotalTime_105;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitBallTimeInterval
	InputField_t3762917431 * ____inputfieldSpitBallTimeInterval_106;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpitSporeTimeInterval
	InputField_t3762917431 * ____inputfieldSpitSporeTimeInterval_107;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMinDiameterToEatThorn
	InputField_t3762917431 * ____inputfieldMinDiameterToEatThorn_108;
	// UnityEngine.UI.InputField MapEditor::_inputfieldExplodeRunDistanceMultiple
	InputField_t3762917431 * ____inputfieldExplodeRunDistanceMultiple_109;
	// UnityEngine.UI.InputField MapEditor::_inputfieldExplodeStayTime
	InputField_t3762917431 * ____inputfieldExplodeStayTime_110;
	// UnityEngine.UI.InputField MapEditor::_inputfieldExplodePercent
	InputField_t3762917431 * ____inputfieldExplodePercent_111;
	// UnityEngine.UI.InputField MapEditor::_inputfieldExplodeExpectNum
	InputField_t3762917431 * ____inputfieldExplodeExpectNum_112;
	// UnityEngine.UI.InputField MapEditor::_inputfieldExplodeRunTime
	InputField_t3762917431 * ____inputfieldExplodeRunTime_113;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMaxBallNumPerPlayer
	InputField_t3762917431 * ____inputfieldMaxBallNumPerPlayer_114;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitNum
	InputField_t3762917431 * ____inputfieldSplitNum_115;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitRunTime
	InputField_t3762917431 * ____inputfieldSplitRunTime_116;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitMaxDistance
	InputField_t3762917431 * ____inputfieldSplitMaxDistance_117;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitTimeInterval
	InputField_t3762917431 * ____inputfieldSplitTimeInterval_118;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitCostMP
	InputField_t3762917431 * ____inputfieldSplitCostMP_119;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSplitStayTime
	InputField_t3762917431 * ____inputfieldSplitStayTime_120;
	// CMonsterEditor MapEditor::_MonsterEditor
	CMonsterEditor_t2594843249 * ____MonsterEditor_121;
	// CClassEditor MapEditor::_ClassEditor
	CClassEditor_t335744478 * ____ClassEditor_122;
	// UnityEngine.UI.Dropdown MapEditor::_dropdownEatMode
	Dropdown_t2274391225 * ____dropdownEatMode_123;
	// UnityEngine.UI.InputField MapEditor::_inputUnfoldCostMP
	InputField_t3762917431 * ____inputUnfoldCostMP_124;
	// UnityEngine.UI.InputField MapEditor::_inputMainTriggerPreUnfoldTime
	InputField_t3762917431 * ____inputMainTriggerPreUnfoldTime_125;
	// UnityEngine.UI.InputField MapEditor::_inputUnfoldSale
	InputField_t3762917431 * ____inputUnfoldSale_126;
	// UnityEngine.UI.InputField MapEditor::_inputMainTriggerUnfoldTime
	InputField_t3762917431 * ____inputMainTriggerUnfoldTime_127;
	// UnityEngine.UI.InputField MapEditor::_inputUnfoldTimeInterval
	InputField_t3762917431 * ____inputUnfoldTimeInterval_128;
	// UnityEngine.UI.InputField MapEditor::_inputYaQiuTiaoJian
	InputField_t3762917431 * ____inputYaQiuTiaoJian_129;
	// UnityEngine.UI.InputField MapEditor::_inputYaQiuBaiFenBi
	InputField_t3762917431 * ____inputYaQiuBaiFenBi_130;
	// UnityEngine.UI.InputField MapEditor::_inputYaQiuBaiFenBi_Self
	InputField_t3762917431 * ____inputYaQiuBaiFenBi_Self_131;
	// UnityEngine.UI.InputField MapEditor::_inputChiCiBaiFenBi
	InputField_t3762917431 * ____inputChiCiBaiFenBi_132;
	// UnityEngine.UI.InputField MapEditor::_inputXiQiuSuDu
	InputField_t3762917431 * ____inputXiQiuSuDu_133;
	// UnityEngine.UI.Slider MapEditor::_sliderScaleX
	Slider_t3903728902 * ____sliderScaleX_134;
	// UnityEngine.UI.Slider MapEditor::_sliderScaleY
	Slider_t3903728902 * ____sliderScaleY_135;
	// UnityEngine.UI.Slider MapEditor::_sliderRotation
	Slider_t3903728902 * ____sliderRotation_136;
	// UnityEngine.UI.Text MapEditor::_txtPolygonTitle
	Text_t1901882714 * ____txtPolygonTitle_137;
	// UnityEngine.UI.Toggle MapEditor::_toogleIsGrass
	Toggle_t2735377061 * ____toogleIsGrass_138;
	// UnityEngine.UI.Toggle MapEditor::_toogleIsGrassSeed
	Toggle_t2735377061 * ____toogleIsGrassSeed_139;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSeedGrassLifeTime
	InputField_t3762917431 * ____inputfieldSeedGrassLifeTime_140;
	// UnityEngine.UI.InputField MapEditor::_inputPolygonScaleX
	InputField_t3762917431 * ____inputPolygonScaleX_141;
	// UnityEngine.UI.InputField MapEditor::_inputPolygonScaleY
	InputField_t3762917431 * ____inputPolygonScaleY_142;
	// UnityEngine.UI.InputField MapEditor::_inputPolygonRotation
	InputField_t3762917431 * ____inputPolygonRotation_143;
	// UnityEngine.UI.Toggle MapEditor::_toogleIsHardObstacle
	Toggle_t2735377061 * ____toogleIsHardObstacle_144;
	// ComoList MapEditor::_PolygonList
	ComoList_t2152284863 * ____PolygonList_145;
	// UnityEngine.UI.Slider MapEditor::_sliderDensity
	Slider_t3903728902 * ____sliderDensity_146;
	// UnityEngine.UI.Slider MapEditor::_sliderMaxDis
	Slider_t3903728902 * ____sliderMaxDis_147;
	// UnityEngine.UI.Slider MapEditor::_sliderBeanLifeTime
	Slider_t3903728902 * ____sliderBeanLifeTime_148;
	// UnityEngine.UI.Text MapEditor::_txtDensity
	Text_t1901882714 * ____txtDensity_149;
	// UnityEngine.UI.Text MapEditor::_txtMaxDis
	Text_t1901882714 * ____txtMaxDis_150;
	// UnityEngine.UI.Text MapEditor::_txtBeanLifeTime
	Text_t1901882714 * ____txtBeanLifeTime_151;
	// UnityEngine.UI.Toggle MapEditor::_toogleShowRealtimeBeanSpray
	Toggle_t2735377061 * ____toogleShowRealtimeBeanSpray_152;
	// UnityEngine.UI.InputField MapEditor::_inputfieldDensity
	InputField_t3762917431 * ____inputfieldDensity_153;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMeatDensity
	InputField_t3762917431 * ____inputfieldMeatDensity_154;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMaxDis
	InputField_t3762917431 * ____inputfieldMaxDis_155;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMinDis
	InputField_t3762917431 * ____inputfieldMinDis_156;
	// UnityEngine.UI.InputField MapEditor::_inputfieldBeanLifeTime
	InputField_t3762917431 * ____inputfieldBeanLifeTime_157;
	// UnityEngine.UI.InputField MapEditor::_inputfieldMeat2BeanSprayTime
	InputField_t3762917431 * ____inputfieldMeat2BeanSprayTime_158;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSpraySize
	InputField_t3762917431 * ____inputfieldSpraySize_159;
	// UnityEngine.UI.InputField MapEditor::_inputfieldDrawSpeed
	InputField_t3762917431 * ____inputfieldDrawSpeed_160;
	// UnityEngine.UI.InputField MapEditor::_inputfieldDrawArea
	InputField_t3762917431 * ____inputfieldDrawArea_161;
	// UnityEngine.UI.InputField MapEditor::_inputfieldPreDrawTime
	InputField_t3762917431 * ____inputfieldPreDrawTime_162;
	// UnityEngine.UI.InputField MapEditor::_inputfieldDrawTime
	InputField_t3762917431 * ____inputfieldDrawTime_163;
	// UnityEngine.UI.InputField MapEditor::_inputfieldDrawInterval
	InputField_t3762917431 * ____inputfieldDrawInterval_164;
	// UnityEngine.UI.InputField MapEditor::_inputfieldThornChance
	InputField_t3762917431 * ____inputfieldThornChance_165;
	// UnityEngine.UI.InputField MapEditor::_inputfieldSprayInterval
	InputField_t3762917431 * ____inputfieldSprayInterval_166;
	// UnityEngine.UI.Text MapEditor::_txtThornChace
	Text_t1901882714 * ____txtThornChace_167;
	// System.Int32 MapEditor::m_nThornChance
	int32_t ___m_nThornChance_168;
	// UnityEngine.UI.Dropdown MapEditor::dropdownColorThorn
	Dropdown_t2274391225 * ___dropdownColorThorn_169;
	// UnityEngine.UI.Image MapEditor::m_imgColorThorn
	Image_t2670269651 * ___m_imgColorThorn_170;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldColorThornThornSize
	InputField_t3762917431 * ___m_inputfieldColorThornThornSize_171;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldColorThornBallSize
	InputField_t3762917431 * ___m_inputfieldColorThornBallSize_172;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldColorThornRebornTime
	InputField_t3762917431 * ___m_inputfieldColorThornRebornTime_173;
	// UnityEngine.UI.Toggle MapEditor::m_toggleAddThorn
	Toggle_t2735377061 * ___m_toggleAddThorn_174;
	// UnityEngine.UI.Toggle MapEditor::m_toggleRemoveThorn
	Toggle_t2735377061 * ___m_toggleRemoveThorn_175;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldLababaAreaThreshold
	InputField_t3762917431 * ___m_inputfieldLababaAreaThreshold_177;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldLababaRunTime
	InputField_t3762917431 * ___m_inputfieldLababaRunTime_178;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldLababaRunDistance
	InputField_t3762917431 * ___m_inputfieldLababaRunDistance_179;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldLababaColdDown
	InputField_t3762917431 * ___m_inputfieldLababaColdDown_180;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldLababaAreaCostPercent
	InputField_t3762917431 * ___m_inputfieldLababaAreaCostPercent_181;
	// UnityEngine.UI.InputField MapEditor::m_inputfieldLababaLiveTime
	InputField_t3762917431 * ___m_inputfieldLababaLiveTime_182;
	// UnityEngine.UI.Dropdown MapEditor::_dropdownFoodType
	Dropdown_t2274391225 * ____dropdownFoodType_183;
	// UnityEngine.UI.InputField MapEditor::_inputName
	InputField_t3762917431 * ____inputName_184;
	// UnityEngine.UI.InputField MapEditor::_inputDesc
	InputField_t3762917431 * ____inputDesc_185;
	// UnityEngine.UI.InputField MapEditor::_inputValue
	InputField_t3762917431 * ____inputValue_186;
	// UnityEngine.UI.InputField MapEditor::_inputNum
	InputField_t3762917431 * ____inputNum_187;
	// UnityEngine.UI.InputField MapEditor::_inputTotalTime
	InputField_t3762917431 * ____inputTotalTime_188;
	// UnityEngine.UI.InputField MapEditor::_inputMinScale
	InputField_t3762917431 * ____inputMinScale_189;
	// UnityEngine.UI.Slider MapEditor::_sliderCircleSizeRange
	Slider_t3903728902 * ____sliderCircleSizeRange_190;
	// MapEditor/sBeiShuDouZiConfig MapEditor::m_CurEditingBeiShuDouZiConfig
	sBeiShuDouZiConfig_t599764670  ___m_CurEditingBeiShuDouZiConfig_191;
	// System.Collections.Generic.List`1<MapEditor/sBeiShuDouZiConfig> MapEditor::m_lstBeiShuDouZi
	List_1_t2071839412 * ___m_lstBeiShuDouZi_192;
	// System.Single MapEditor::m_fYaQiuTiaoJian
	float ___m_fYaQiuTiaoJian_193;
	// System.Single MapEditor::m_fYaQiuBaiFenBi
	float ___m_fYaQiuBaiFenBi_194;
	// System.Single MapEditor::m_fYaQiuBaiFenBi_Self
	float ___m_fYaQiuBaiFenBi_Self_195;
	// System.Single MapEditor::m_fChiCiBaiFenBi
	float ___m_fChiCiBaiFenBi_196;
	// System.Single MapEditor::m_fXiQiuSuDu
	float ___m_fXiQiuSuDu_197;
	// MapEditor/eMapEditorOp MapEditor::m_eOp
	int32_t ___m_eOp_198;
	// System.Boolean MapEditor::m_bMapInitCompleted
	bool ___m_bMapInitCompleted_199;
	// System.Int32 MapEditor::m_nCurSelectedColorThornIdx
	int32_t ___m_nCurSelectedColorThornIdx_200;
	// System.Boolean MapEditor::m_bFirstMouseLefDown
	bool ___m_bFirstMouseLefDown_201;
	// System.Boolean MapEditor::m_bFirstMouseRightDown
	bool ___m_bFirstMouseRightDown_202;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> MapEditor::m_dicEatModeConfig
	Dictionary_2_t1182523073 * ___m_dicEatModeConfig_203;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> MapEditor::m_dicCommonConfig
	Dictionary_2_t1182523073 * ___m_dicCommonConfig_204;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MapEditor::m_dicConfigCommon2
	Dictionary_2_t1632706988 * ___m_dicConfigCommon2_205;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> MapEditor::m_lstFakeRandomThornPos
	List_1_t3628304265 * ___m_lstFakeRandomThornPos_206;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>> MapEditor::m_dicExplodeDir
	Dictionary_2_t2517017596 * ___m_dicExplodeDir_207;
	// System.Int32 MapEditor::m_nGrassGUID
	int32_t ___m_nGrassGUID_208;
	// System.Collections.Generic.List`1<Polygon> MapEditor::m_lstGrass
	List_1_t1484766997 * ___m_lstGrass_209;
	// System.Collections.Generic.List`1<System.String> MapEditor::m_lstPolygons
	List_1_t3319525431 * ___m_lstPolygons_212;
	// System.Collections.Generic.List`1<Spray> MapEditor::m_lstSprays
	List_1_t2194232156 * ___m_lstSprays_213;
	// System.Boolean MapEditor::m_bShowRealtimeBeanSpray
	bool ___m_bShowRealtimeBeanSpray_214;
	// System.Single MapEditor::m_fDaTaoShaSize
	float ___m_fDaTaoShaSize_215;
	// System.Single MapEditor::m_fThornAttenuateSpeed
	float ___m_fThornAttenuateSpeed_216;
	// System.Collections.Generic.List`1<MapEditor/sRebornRanPos> MapEditor::m_lstRebornRandomPos
	List_1_t1506737460 * ___m_lstRebornRandomPos_217;
	// UnityEngine.Vector3 MapEditor::m_vecRebornSpotRandomPos
	Vector3_t3722313464  ___m_vecRebornSpotRandomPos_218;
	// UnityEngine.Vector3 MapEditor::vecScreenLeftBottom
	Vector3_t3722313464  ___vecScreenLeftBottom_219;
	// UnityEngine.Vector3 MapEditor::vecScreenRightTop
	Vector3_t3722313464  ___vecScreenRightTop_220;
	// System.Single MapEditor::m_fWorldLeft
	float ___m_fWorldLeft_223;
	// System.Single MapEditor::m_fWorldRight
	float ___m_fWorldRight_224;
	// System.Single MapEditor::m_fWorldTop
	float ___m_fWorldTop_225;
	// System.Single MapEditor::m_fWorldBottom
	float ___m_fWorldBottom_226;
	// UnityEngine.UI.InputField MapEditor::_inputBallMinArea
	InputField_t3762917431 * ____inputBallMinArea_227;
	// UnityEngine.UI.InputField MapEditor::_inputPlayerNumToStartGame
	InputField_t3762917431 * ____inputPlayerNumToStartGame_228;
	// System.Single MapEditor::m_fEjectParamA
	float ___m_fEjectParamA_230;
	// System.Single MapEditor::m_fEjectParamB
	float ___m_fEjectParamB_231;
	// System.Single MapEditor::m_fEjectParamC
	float ___m_fEjectParamC_232;
	// System.Single MapEditor::m_fEjectParamX
	float ___m_fEjectParamX_233;

public:
	inline static int32_t get_offset_of__txtDebugInfo_3() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtDebugInfo_3)); }
	inline Text_t1901882714 * get__txtDebugInfo_3() const { return ____txtDebugInfo_3; }
	inline Text_t1901882714 ** get_address_of__txtDebugInfo_3() { return &____txtDebugInfo_3; }
	inline void set__txtDebugInfo_3(Text_t1901882714 * value)
	{
		____txtDebugInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfo_3), value);
	}

	inline static int32_t get_offset_of__txtDebugInfoInEditor_4() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtDebugInfoInEditor_4)); }
	inline Text_t1901882714 * get__txtDebugInfoInEditor_4() const { return ____txtDebugInfoInEditor_4; }
	inline Text_t1901882714 ** get_address_of__txtDebugInfoInEditor_4() { return &____txtDebugInfoInEditor_4; }
	inline void set__txtDebugInfoInEditor_4(Text_t1901882714 * value)
	{
		____txtDebugInfoInEditor_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfoInEditor_4), value);
	}

	inline static int32_t get_offset_of_m_fMinCameraSize_6() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fMinCameraSize_6)); }
	inline float get_m_fMinCameraSize_6() const { return ___m_fMinCameraSize_6; }
	inline float* get_address_of_m_fMinCameraSize_6() { return &___m_fMinCameraSize_6; }
	inline void set_m_fMinCameraSize_6(float value)
	{
		___m_fMinCameraSize_6 = value;
	}

	inline static int32_t get_offset_of_m_fMaxCameraSize_7() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fMaxCameraSize_7)); }
	inline float get_m_fMaxCameraSize_7() const { return ___m_fMaxCameraSize_7; }
	inline float* get_address_of_m_fMaxCameraSize_7() { return &___m_fMaxCameraSize_7; }
	inline void set_m_fMaxCameraSize_7(float value)
	{
		___m_fMaxCameraSize_7 = value;
	}

	inline static int32_t get_offset_of_m_fCameraZoomSpeed_8() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fCameraZoomSpeed_8)); }
	inline float get_m_fCameraZoomSpeed_8() const { return ___m_fCameraZoomSpeed_8; }
	inline float* get_address_of_m_fCameraZoomSpeed_8() { return &___m_fCameraZoomSpeed_8; }
	inline void set_m_fCameraZoomSpeed_8(float value)
	{
		___m_fCameraZoomSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_fBallSizeAffectCamSize_9() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fBallSizeAffectCamSize_9)); }
	inline float get_m_fBallSizeAffectCamSize_9() const { return ___m_fBallSizeAffectCamSize_9; }
	inline float* get_address_of_m_fBallSizeAffectCamSize_9() { return &___m_fBallSizeAffectCamSize_9; }
	inline void set_m_fBallSizeAffectCamSize_9(float value)
	{
		___m_fBallSizeAffectCamSize_9 = value;
	}

	inline static int32_t get_offset_of_m_ReferBall_10() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_ReferBall_10)); }
	inline Ball_t2206666566 * get_m_ReferBall_10() const { return ___m_ReferBall_10; }
	inline Ball_t2206666566 ** get_address_of_m_ReferBall_10() { return &___m_ReferBall_10; }
	inline void set_m_ReferBall_10(Ball_t2206666566 * value)
	{
		___m_ReferBall_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReferBall_10), value);
	}

	inline static int32_t get_offset_of_m_fWorldSizeX_11() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldSizeX_11)); }
	inline float get_m_fWorldSizeX_11() const { return ___m_fWorldSizeX_11; }
	inline float* get_address_of_m_fWorldSizeX_11() { return &___m_fWorldSizeX_11; }
	inline void set_m_fWorldSizeX_11(float value)
	{
		___m_fWorldSizeX_11 = value;
	}

	inline static int32_t get_offset_of_m_fWorldSizeY_12() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldSizeY_12)); }
	inline float get_m_fWorldSizeY_12() const { return ___m_fWorldSizeY_12; }
	inline float* get_address_of_m_fWorldSizeY_12() { return &___m_fWorldSizeY_12; }
	inline void set_m_fWorldSizeY_12(float value)
	{
		___m_fWorldSizeY_12 = value;
	}

	inline static int32_t get_offset_of_m_fWorldWidth_13() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldWidth_13)); }
	inline float get_m_fWorldWidth_13() const { return ___m_fWorldWidth_13; }
	inline float* get_address_of_m_fWorldWidth_13() { return &___m_fWorldWidth_13; }
	inline void set_m_fWorldWidth_13(float value)
	{
		___m_fWorldWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_fWorldHeight_14() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldHeight_14)); }
	inline float get_m_fWorldHeight_14() const { return ___m_fWorldHeight_14; }
	inline float* get_address_of_m_fWorldHeight_14() { return &___m_fWorldHeight_14; }
	inline void set_m_fWorldHeight_14(float value)
	{
		___m_fWorldHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_fHalfWorldWidth_15() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fHalfWorldWidth_15)); }
	inline float get_m_fHalfWorldWidth_15() const { return ___m_fHalfWorldWidth_15; }
	inline float* get_address_of_m_fHalfWorldWidth_15() { return &___m_fHalfWorldWidth_15; }
	inline void set_m_fHalfWorldWidth_15(float value)
	{
		___m_fHalfWorldWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_fHalfWorldHeight_16() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fHalfWorldHeight_16)); }
	inline float get_m_fHalfWorldHeight_16() const { return ___m_fHalfWorldHeight_16; }
	inline float* get_address_of_m_fHalfWorldHeight_16() { return &___m_fHalfWorldHeight_16; }
	inline void set_m_fHalfWorldHeight_16(float value)
	{
		___m_fHalfWorldHeight_16 = value;
	}

	inline static int32_t get_offset_of_m_goWorldBorderTop_17() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goWorldBorderTop_17)); }
	inline WorldBorder_t1275663748 * get_m_goWorldBorderTop_17() const { return ___m_goWorldBorderTop_17; }
	inline WorldBorder_t1275663748 ** get_address_of_m_goWorldBorderTop_17() { return &___m_goWorldBorderTop_17; }
	inline void set_m_goWorldBorderTop_17(WorldBorder_t1275663748 * value)
	{
		___m_goWorldBorderTop_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_goWorldBorderTop_17), value);
	}

	inline static int32_t get_offset_of_m_goWorldBorderBottom_18() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goWorldBorderBottom_18)); }
	inline WorldBorder_t1275663748 * get_m_goWorldBorderBottom_18() const { return ___m_goWorldBorderBottom_18; }
	inline WorldBorder_t1275663748 ** get_address_of_m_goWorldBorderBottom_18() { return &___m_goWorldBorderBottom_18; }
	inline void set_m_goWorldBorderBottom_18(WorldBorder_t1275663748 * value)
	{
		___m_goWorldBorderBottom_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_goWorldBorderBottom_18), value);
	}

	inline static int32_t get_offset_of_m_goWorldBorderLeft_19() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goWorldBorderLeft_19)); }
	inline WorldBorder_t1275663748 * get_m_goWorldBorderLeft_19() const { return ___m_goWorldBorderLeft_19; }
	inline WorldBorder_t1275663748 ** get_address_of_m_goWorldBorderLeft_19() { return &___m_goWorldBorderLeft_19; }
	inline void set_m_goWorldBorderLeft_19(WorldBorder_t1275663748 * value)
	{
		___m_goWorldBorderLeft_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_goWorldBorderLeft_19), value);
	}

	inline static int32_t get_offset_of_m_goWorldBorderRight_20() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goWorldBorderRight_20)); }
	inline WorldBorder_t1275663748 * get_m_goWorldBorderRight_20() const { return ___m_goWorldBorderRight_20; }
	inline WorldBorder_t1275663748 ** get_address_of_m_goWorldBorderRight_20() { return &___m_goWorldBorderRight_20; }
	inline void set_m_goWorldBorderRight_20(WorldBorder_t1275663748 * value)
	{
		___m_goWorldBorderRight_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_goWorldBorderRight_20), value);
	}

	inline static int32_t get_offset_of_m_lstRebornPos_21() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstRebornPos_21)); }
	inline List_1_t899420910 * get_m_lstRebornPos_21() const { return ___m_lstRebornPos_21; }
	inline List_1_t899420910 ** get_address_of_m_lstRebornPos_21() { return &___m_lstRebornPos_21; }
	inline void set_m_lstRebornPos_21(List_1_t899420910 * value)
	{
		___m_lstRebornPos_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRebornPos_21), value);
	}

	inline static int32_t get_offset_of_vecTemp_22() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___vecTemp_22)); }
	inline Vector3_t3722313464  get_vecTemp_22() const { return ___vecTemp_22; }
	inline Vector3_t3722313464 * get_address_of_vecTemp_22() { return &___vecTemp_22; }
	inline void set_vecTemp_22(Vector3_t3722313464  value)
	{
		___vecTemp_22 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_23() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___vecTempPos_23)); }
	inline Vector3_t3722313464  get_vecTempPos_23() const { return ___vecTempPos_23; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_23() { return &___vecTempPos_23; }
	inline void set_vecTempPos_23(Vector3_t3722313464  value)
	{
		___vecTempPos_23 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_24() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___vecTempScale_24)); }
	inline Vector3_t3722313464  get_vecTempScale_24() const { return ___vecTempScale_24; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_24() { return &___vecTempScale_24; }
	inline void set_vecTempScale_24(Vector3_t3722313464  value)
	{
		___vecTempScale_24 = value;
	}

	inline static int32_t get_offset_of_m_preGrassTile_25() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_preGrassTile_25)); }
	inline GameObject_t1113636619 * get_m_preGrassTile_25() const { return ___m_preGrassTile_25; }
	inline GameObject_t1113636619 ** get_address_of_m_preGrassTile_25() { return &___m_preGrassTile_25; }
	inline void set_m_preGrassTile_25(GameObject_t1113636619 * value)
	{
		___m_preGrassTile_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGrassTile_25), value);
	}

	inline static int32_t get_offset_of_m_preGridLine_26() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_preGridLine_26)); }
	inline GameObject_t1113636619 * get_m_preGridLine_26() const { return ___m_preGridLine_26; }
	inline GameObject_t1113636619 ** get_address_of_m_preGridLine_26() { return &___m_preGridLine_26; }
	inline void set_m_preGridLine_26(GameObject_t1113636619 * value)
	{
		___m_preGridLine_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGridLine_26), value);
	}

	inline static int32_t get_offset_of_m_goGridLineContainer_27() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goGridLineContainer_27)); }
	inline GameObject_t1113636619 * get_m_goGridLineContainer_27() const { return ___m_goGridLineContainer_27; }
	inline GameObject_t1113636619 ** get_address_of_m_goGridLineContainer_27() { return &___m_goGridLineContainer_27; }
	inline void set_m_goGridLineContainer_27(GameObject_t1113636619 * value)
	{
		___m_goGridLineContainer_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_goGridLineContainer_27), value);
	}

	inline static int32_t get_offset_of_m_goGrassContainer_28() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goGrassContainer_28)); }
	inline GameObject_t1113636619 * get_m_goGrassContainer_28() const { return ___m_goGrassContainer_28; }
	inline GameObject_t1113636619 ** get_address_of_m_goGrassContainer_28() { return &___m_goGrassContainer_28; }
	inline void set_m_goGrassContainer_28(GameObject_t1113636619 * value)
	{
		___m_goGrassContainer_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_goGrassContainer_28), value);
	}

	inline static int32_t get_offset_of_m_goPolygonContainer_29() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goPolygonContainer_29)); }
	inline GameObject_t1113636619 * get_m_goPolygonContainer_29() const { return ___m_goPolygonContainer_29; }
	inline GameObject_t1113636619 ** get_address_of_m_goPolygonContainer_29() { return &___m_goPolygonContainer_29; }
	inline void set_m_goPolygonContainer_29(GameObject_t1113636619 * value)
	{
		___m_goPolygonContainer_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_goPolygonContainer_29), value);
	}

	inline static int32_t get_offset_of_m_goBeanSprayContainer_30() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goBeanSprayContainer_30)); }
	inline GameObject_t1113636619 * get_m_goBeanSprayContainer_30() const { return ___m_goBeanSprayContainer_30; }
	inline GameObject_t1113636619 ** get_address_of_m_goBeanSprayContainer_30() { return &___m_goBeanSprayContainer_30; }
	inline void set_m_goBeanSprayContainer_30(GameObject_t1113636619 * value)
	{
		___m_goBeanSprayContainer_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBeanSprayContainer_30), value);
	}

	inline static int32_t get_offset_of_m_goRebornSpotContainer_31() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goRebornSpotContainer_31)); }
	inline GameObject_t1113636619 * get_m_goRebornSpotContainer_31() const { return ___m_goRebornSpotContainer_31; }
	inline GameObject_t1113636619 ** get_address_of_m_goRebornSpotContainer_31() { return &___m_goRebornSpotContainer_31; }
	inline void set_m_goRebornSpotContainer_31(GameObject_t1113636619 * value)
	{
		___m_goRebornSpotContainer_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRebornSpotContainer_31), value);
	}

	inline static int32_t get_offset_of_m_goColorThornContainer_32() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_goColorThornContainer_32)); }
	inline GameObject_t1113636619 * get_m_goColorThornContainer_32() const { return ___m_goColorThornContainer_32; }
	inline GameObject_t1113636619 ** get_address_of_m_goColorThornContainer_32() { return &___m_goColorThornContainer_32; }
	inline void set_m_goColorThornContainer_32(GameObject_t1113636619 * value)
	{
		___m_goColorThornContainer_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_goColorThornContainer_32), value);
	}

	inline static int32_t get_offset_of_m_dicDelProtectTime_34() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_dicDelProtectTime_34)); }
	inline Dictionary_2_t1182523073 * get_m_dicDelProtectTime_34() const { return ___m_dicDelProtectTime_34; }
	inline Dictionary_2_t1182523073 ** get_address_of_m_dicDelProtectTime_34() { return &___m_dicDelProtectTime_34; }
	inline void set_m_dicDelProtectTime_34(Dictionary_2_t1182523073 * value)
	{
		___m_dicDelProtectTime_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicDelProtectTime_34), value);
	}

	inline static int32_t get_offset_of_m_dicGrass_35() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_dicGrass_35)); }
	inline Dictionary_2_t3196372882 * get_m_dicGrass_35() const { return ___m_dicGrass_35; }
	inline Dictionary_2_t3196372882 ** get_address_of_m_dicGrass_35() { return &___m_dicGrass_35; }
	inline void set_m_dicGrass_35(Dictionary_2_t3196372882 * value)
	{
		___m_dicGrass_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicGrass_35), value);
	}

	inline static int32_t get_offset_of_m_fGrassTileWidth_37() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fGrassTileWidth_37)); }
	inline float get_m_fGrassTileWidth_37() const { return ___m_fGrassTileWidth_37; }
	inline float* get_address_of_m_fGrassTileWidth_37() { return &___m_fGrassTileWidth_37; }
	inline void set_m_fGrassTileWidth_37(float value)
	{
		___m_fGrassTileWidth_37 = value;
	}

	inline static int32_t get_offset_of_m_fGrassTileHeight_38() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fGrassTileHeight_38)); }
	inline float get_m_fGrassTileHeight_38() const { return ___m_fGrassTileHeight_38; }
	inline float* get_address_of_m_fGrassTileHeight_38() { return &___m_fGrassTileHeight_38; }
	inline void set_m_fGrassTileHeight_38(float value)
	{
		___m_fGrassTileHeight_38 = value;
	}

	inline static int32_t get_offset_of_m_fGrassHalfTileWidth_39() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fGrassHalfTileWidth_39)); }
	inline float get_m_fGrassHalfTileWidth_39() const { return ___m_fGrassHalfTileWidth_39; }
	inline float* get_address_of_m_fGrassHalfTileWidth_39() { return &___m_fGrassHalfTileWidth_39; }
	inline void set_m_fGrassHalfTileWidth_39(float value)
	{
		___m_fGrassHalfTileWidth_39 = value;
	}

	inline static int32_t get_offset_of_m_fGrassHalfTileHeight_40() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fGrassHalfTileHeight_40)); }
	inline float get_m_fGrassHalfTileHeight_40() const { return ___m_fGrassHalfTileHeight_40; }
	inline float* get_address_of_m_fGrassHalfTileHeight_40() { return &___m_fGrassHalfTileHeight_40; }
	inline void set_m_fGrassHalfTileHeight_40(float value)
	{
		___m_fGrassHalfTileHeight_40 = value;
	}

	inline static int32_t get_offset_of_m_CurSelectedObj_41() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_CurSelectedObj_41)); }
	inline MapObj_t1733252447 * get_m_CurSelectedObj_41() const { return ___m_CurSelectedObj_41; }
	inline MapObj_t1733252447 ** get_address_of_m_CurSelectedObj_41() { return &___m_CurSelectedObj_41; }
	inline void set_m_CurSelectedObj_41(MapObj_t1733252447 * value)
	{
		___m_CurSelectedObj_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedObj_41), value);
	}

	inline static int32_t get_offset_of_m_CurMovingObj_42() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_CurMovingObj_42)); }
	inline MapObj_t1733252447 * get_m_CurMovingObj_42() const { return ___m_CurMovingObj_42; }
	inline MapObj_t1733252447 ** get_address_of_m_CurMovingObj_42() { return &___m_CurMovingObj_42; }
	inline void set_m_CurMovingObj_42(MapObj_t1733252447 * value)
	{
		___m_CurMovingObj_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurMovingObj_42), value);
	}

	inline static int32_t get_offset_of_m_vecLastMousePos_43() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_vecLastMousePos_43)); }
	inline Vector3_t3722313464  get_m_vecLastMousePos_43() const { return ___m_vecLastMousePos_43; }
	inline Vector3_t3722313464 * get_address_of_m_vecLastMousePos_43() { return &___m_vecLastMousePos_43; }
	inline void set_m_vecLastMousePos_43(Vector3_t3722313464  value)
	{
		___m_vecLastMousePos_43 = value;
	}

	inline static int32_t get_offset_of_Screen_Cursor_44() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___Screen_Cursor_44)); }
	inline GameObject_t1113636619 * get_Screen_Cursor_44() const { return ___Screen_Cursor_44; }
	inline GameObject_t1113636619 ** get_address_of_Screen_Cursor_44() { return &___Screen_Cursor_44; }
	inline void set_Screen_Cursor_44(GameObject_t1113636619 * value)
	{
		___Screen_Cursor_44 = value;
		Il2CppCodeGenWriteBarrier((&___Screen_Cursor_44), value);
	}

	inline static int32_t get_offset_of_UI_MiniMap_45() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___UI_MiniMap_45)); }
	inline GameObject_t1113636619 * get_UI_MiniMap_45() const { return ___UI_MiniMap_45; }
	inline GameObject_t1113636619 ** get_address_of_UI_MiniMap_45() { return &___UI_MiniMap_45; }
	inline void set_UI_MiniMap_45(GameObject_t1113636619 * value)
	{
		___UI_MiniMap_45 = value;
		Il2CppCodeGenWriteBarrier((&___UI_MiniMap_45), value);
	}

	inline static int32_t get_offset_of_m_arySubPanel_46() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_arySubPanel_46)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySubPanel_46() const { return ___m_arySubPanel_46; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySubPanel_46() { return &___m_arySubPanel_46; }
	inline void set_m_arySubPanel_46(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySubPanel_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySubPanel_46), value);
	}

	inline static int32_t get_offset_of_dropdownItem_47() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___dropdownItem_47)); }
	inline Dropdown_t2274391225 * get_dropdownItem_47() const { return ___dropdownItem_47; }
	inline Dropdown_t2274391225 ** get_address_of_dropdownItem_47() { return &___dropdownItem_47; }
	inline void set_dropdownItem_47(Dropdown_t2274391225 * value)
	{
		___dropdownItem_47 = value;
		Il2CppCodeGenWriteBarrier((&___dropdownItem_47), value);
	}

	inline static int32_t get_offset_of__panelLogin_48() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____panelLogin_48)); }
	inline GameObject_t1113636619 * get__panelLogin_48() const { return ____panelLogin_48; }
	inline GameObject_t1113636619 ** get_address_of__panelLogin_48() { return &____panelLogin_48; }
	inline void set__panelLogin_48(GameObject_t1113636619 * value)
	{
		____panelLogin_48 = value;
		Il2CppCodeGenWriteBarrier((&____panelLogin_48), value);
	}

	inline static int32_t get_offset_of__musicMain_49() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____musicMain_49)); }
	inline AudioSource_t3935305588 * get__musicMain_49() const { return ____musicMain_49; }
	inline AudioSource_t3935305588 ** get_address_of__musicMain_49() { return &____musicMain_49; }
	inline void set__musicMain_49(AudioSource_t3935305588 * value)
	{
		____musicMain_49 = value;
		Il2CppCodeGenWriteBarrier((&____musicMain_49), value);
	}

	inline static int32_t get_offset_of__inputTest_50() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputTest_50)); }
	inline InputField_t3762917431 * get__inputTest_50() const { return ____inputTest_50; }
	inline InputField_t3762917431 ** get_address_of__inputTest_50() { return &____inputTest_50; }
	inline void set__inputTest_50(InputField_t3762917431 * value)
	{
		____inputTest_50 = value;
		Il2CppCodeGenWriteBarrier((&____inputTest_50), value);
	}

	inline static int32_t get_offset_of__btnTest_51() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____btnTest_51)); }
	inline Button_t4055032469 * get__btnTest_51() const { return ____btnTest_51; }
	inline Button_t4055032469 ** get_address_of__btnTest_51() { return &____btnTest_51; }
	inline void set__btnTest_51(Button_t4055032469 * value)
	{
		____btnTest_51 = value;
		Il2CppCodeGenWriteBarrier((&____btnTest_51), value);
	}

	inline static int32_t get_offset_of__dicTest_52() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____dicTest_52)); }
	inline Dictionary_2_t736164020 * get__dicTest_52() const { return ____dicTest_52; }
	inline Dictionary_2_t736164020 ** get_address_of__dicTest_52() { return &____dicTest_52; }
	inline void set__dicTest_52(Dictionary_2_t736164020 * value)
	{
		____dicTest_52 = value;
		Il2CppCodeGenWriteBarrier((&____dicTest_52), value);
	}

	inline static int32_t get_offset_of__inputA_53() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputA_53)); }
	inline InputField_t3762917431 * get__inputA_53() const { return ____inputA_53; }
	inline InputField_t3762917431 ** get_address_of__inputA_53() { return &____inputA_53; }
	inline void set__inputA_53(InputField_t3762917431 * value)
	{
		____inputA_53 = value;
		Il2CppCodeGenWriteBarrier((&____inputA_53), value);
	}

	inline static int32_t get_offset_of__inputB_54() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputB_54)); }
	inline InputField_t3762917431 * get__inputB_54() const { return ____inputB_54; }
	inline InputField_t3762917431 ** get_address_of__inputB_54() { return &____inputB_54; }
	inline void set__inputB_54(InputField_t3762917431 * value)
	{
		____inputB_54 = value;
		Il2CppCodeGenWriteBarrier((&____inputB_54), value);
	}

	inline static int32_t get_offset_of__inputC_55() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputC_55)); }
	inline InputField_t3762917431 * get__inputC_55() const { return ____inputC_55; }
	inline InputField_t3762917431 ** get_address_of__inputC_55() { return &____inputC_55; }
	inline void set__inputC_55(InputField_t3762917431 * value)
	{
		____inputC_55 = value;
		Il2CppCodeGenWriteBarrier((&____inputC_55), value);
	}

	inline static int32_t get_offset_of__inputX_56() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputX_56)); }
	inline InputField_t3762917431 * get__inputX_56() const { return ____inputX_56; }
	inline InputField_t3762917431 ** get_address_of__inputX_56() { return &____inputX_56; }
	inline void set__inputX_56(InputField_t3762917431 * value)
	{
		____inputX_56 = value;
		Il2CppCodeGenWriteBarrier((&____inputX_56), value);
	}

	inline static int32_t get_offset_of__inputTimeBeforeShowDeadUI_57() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputTimeBeforeShowDeadUI_57)); }
	inline InputField_t3762917431 * get__inputTimeBeforeShowDeadUI_57() const { return ____inputTimeBeforeShowDeadUI_57; }
	inline InputField_t3762917431 ** get_address_of__inputTimeBeforeShowDeadUI_57() { return &____inputTimeBeforeShowDeadUI_57; }
	inline void set__inputTimeBeforeShowDeadUI_57(InputField_t3762917431 * value)
	{
		____inputTimeBeforeShowDeadUI_57 = value;
		Il2CppCodeGenWriteBarrier((&____inputTimeBeforeShowDeadUI_57), value);
	}

	inline static int32_t get_offset_of__inputTimeTimeOfOneRound_58() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputTimeTimeOfOneRound_58)); }
	inline InputField_t3762917431 * get__inputTimeTimeOfOneRound_58() const { return ____inputTimeTimeOfOneRound_58; }
	inline InputField_t3762917431 ** get_address_of__inputTimeTimeOfOneRound_58() { return &____inputTimeTimeOfOneRound_58; }
	inline void set__inputTimeTimeOfOneRound_58(InputField_t3762917431 * value)
	{
		____inputTimeTimeOfOneRound_58 = value;
		Il2CppCodeGenWriteBarrier((&____inputTimeTimeOfOneRound_58), value);
	}

	inline static int32_t get_offset_of__inputBaseVolumeDecreasedPercentWhenDead_59() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputBaseVolumeDecreasedPercentWhenDead_59)); }
	inline InputField_t3762917431 * get__inputBaseVolumeDecreasedPercentWhenDead_59() const { return ____inputBaseVolumeDecreasedPercentWhenDead_59; }
	inline InputField_t3762917431 ** get_address_of__inputBaseVolumeDecreasedPercentWhenDead_59() { return &____inputBaseVolumeDecreasedPercentWhenDead_59; }
	inline void set__inputBaseVolumeDecreasedPercentWhenDead_59(InputField_t3762917431 * value)
	{
		____inputBaseVolumeDecreasedPercentWhenDead_59 = value;
		Il2CppCodeGenWriteBarrier((&____inputBaseVolumeDecreasedPercentWhenDead_59), value);
	}

	inline static int32_t get_offset_of__inputClassCircleRadius_60() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputClassCircleRadius_60)); }
	inline InputField_t3762917431 * get__inputClassCircleRadius_60() const { return ____inputClassCircleRadius_60; }
	inline InputField_t3762917431 ** get_address_of__inputClassCircleRadius_60() { return &____inputClassCircleRadius_60; }
	inline void set__inputClassCircleRadius_60(InputField_t3762917431 * value)
	{
		____inputClassCircleRadius_60 = value;
		Il2CppCodeGenWriteBarrier((&____inputClassCircleRadius_60), value);
	}

	inline static int32_t get_offset_of__inputClassThreshold_61() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputClassThreshold_61)); }
	inline InputField_t3762917431 * get__inputClassThreshold_61() const { return ____inputClassThreshold_61; }
	inline InputField_t3762917431 ** get_address_of__inputClassThreshold_61() { return &____inputClassThreshold_61; }
	inline void set__inputClassThreshold_61(InputField_t3762917431 * value)
	{
		____inputClassThreshold_61 = value;
		Il2CppCodeGenWriteBarrier((&____inputClassThreshold_61), value);
	}

	inline static int32_t get_offset_of__inputWorldSizeX_62() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputWorldSizeX_62)); }
	inline InputField_t3762917431 * get__inputWorldSizeX_62() const { return ____inputWorldSizeX_62; }
	inline InputField_t3762917431 ** get_address_of__inputWorldSizeX_62() { return &____inputWorldSizeX_62; }
	inline void set__inputWorldSizeX_62(InputField_t3762917431 * value)
	{
		____inputWorldSizeX_62 = value;
		Il2CppCodeGenWriteBarrier((&____inputWorldSizeX_62), value);
	}

	inline static int32_t get_offset_of__inputWorldSizeY_63() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputWorldSizeY_63)); }
	inline InputField_t3762917431 * get__inputWorldSizeY_63() const { return ____inputWorldSizeY_63; }
	inline InputField_t3762917431 ** get_address_of__inputWorldSizeY_63() { return &____inputWorldSizeY_63; }
	inline void set__inputWorldSizeY_63(InputField_t3762917431 * value)
	{
		____inputWorldSizeY_63 = value;
		Il2CppCodeGenWriteBarrier((&____inputWorldSizeY_63), value);
	}

	inline static int32_t get_offset_of__inputfieldCamSizeMax_64() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldCamSizeMax_64)); }
	inline InputField_t3762917431 * get__inputfieldCamSizeMax_64() const { return ____inputfieldCamSizeMax_64; }
	inline InputField_t3762917431 ** get_address_of__inputfieldCamSizeMax_64() { return &____inputfieldCamSizeMax_64; }
	inline void set__inputfieldCamSizeMax_64(InputField_t3762917431 * value)
	{
		____inputfieldCamSizeMax_64 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldCamSizeMax_64), value);
	}

	inline static int32_t get_offset_of__inputfieldCamSizeMin_65() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldCamSizeMin_65)); }
	inline InputField_t3762917431 * get__inputfieldCamSizeMin_65() const { return ____inputfieldCamSizeMin_65; }
	inline InputField_t3762917431 ** get_address_of__inputfieldCamSizeMin_65() { return &____inputfieldCamSizeMin_65; }
	inline void set__inputfieldCamSizeMin_65(InputField_t3762917431 * value)
	{
		____inputfieldCamSizeMin_65 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldCamSizeMin_65), value);
	}

	inline static int32_t get_offset_of__inputCamShangFuXiShu_66() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamShangFuXiShu_66)); }
	inline InputField_t3762917431 * get__inputCamShangFuXiShu_66() const { return ____inputCamShangFuXiShu_66; }
	inline InputField_t3762917431 ** get_address_of__inputCamShangFuXiShu_66() { return &____inputCamShangFuXiShu_66; }
	inline void set__inputCamShangFuXiShu_66(InputField_t3762917431 * value)
	{
		____inputCamShangFuXiShu_66 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamShangFuXiShu_66), value);
	}

	inline static int32_t get_offset_of__inputCamRaiseSpeed_67() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamRaiseSpeed_67)); }
	inline InputField_t3762917431 * get__inputCamRaiseSpeed_67() const { return ____inputCamRaiseSpeed_67; }
	inline InputField_t3762917431 ** get_address_of__inputCamRaiseSpeed_67() { return &____inputCamRaiseSpeed_67; }
	inline void set__inputCamRaiseSpeed_67(InputField_t3762917431 * value)
	{
		____inputCamRaiseSpeed_67 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamRaiseSpeed_67), value);
	}

	inline static int32_t get_offset_of__inputCamShangFuAccelerate_68() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamShangFuAccelerate_68)); }
	inline InputField_t3762917431 * get__inputCamShangFuAccelerate_68() const { return ____inputCamShangFuAccelerate_68; }
	inline InputField_t3762917431 ** get_address_of__inputCamShangFuAccelerate_68() { return &____inputCamShangFuAccelerate_68; }
	inline void set__inputCamShangFuAccelerate_68(InputField_t3762917431 * value)
	{
		____inputCamShangFuAccelerate_68 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamShangFuAccelerate_68), value);
	}

	inline static int32_t get_offset_of__inputCamChangeDelaySpit_69() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamChangeDelaySpit_69)); }
	inline InputField_t3762917431 * get__inputCamChangeDelaySpit_69() const { return ____inputCamChangeDelaySpit_69; }
	inline InputField_t3762917431 ** get_address_of__inputCamChangeDelaySpit_69() { return &____inputCamChangeDelaySpit_69; }
	inline void set__inputCamChangeDelaySpit_69(InputField_t3762917431 * value)
	{
		____inputCamChangeDelaySpit_69 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamChangeDelaySpit_69), value);
	}

	inline static int32_t get_offset_of__inputCamChangeDelayExplode_70() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamChangeDelayExplode_70)); }
	inline InputField_t3762917431 * get__inputCamChangeDelayExplode_70() const { return ____inputCamChangeDelayExplode_70; }
	inline InputField_t3762917431 ** get_address_of__inputCamChangeDelayExplode_70() { return &____inputCamChangeDelayExplode_70; }
	inline void set__inputCamChangeDelayExplode_70(InputField_t3762917431 * value)
	{
		____inputCamChangeDelayExplode_70 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamChangeDelayExplode_70), value);
	}

	inline static int32_t get_offset_of__inputfieldCamRespondSpeed_71() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldCamRespondSpeed_71)); }
	inline InputField_t3762917431 * get__inputfieldCamRespondSpeed_71() const { return ____inputfieldCamRespondSpeed_71; }
	inline InputField_t3762917431 ** get_address_of__inputfieldCamRespondSpeed_71() { return &____inputfieldCamRespondSpeed_71; }
	inline void set__inputfieldCamRespondSpeed_71(InputField_t3762917431 * value)
	{
		____inputfieldCamRespondSpeed_71 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldCamRespondSpeed_71), value);
	}

	inline static int32_t get_offset_of__inputCamTimeBeforeDown_72() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamTimeBeforeDown_72)); }
	inline InputField_t3762917431 * get__inputCamTimeBeforeDown_72() const { return ____inputCamTimeBeforeDown_72; }
	inline InputField_t3762917431 ** get_address_of__inputCamTimeBeforeDown_72() { return &____inputCamTimeBeforeDown_72; }
	inline void set__inputCamTimeBeforeDown_72(InputField_t3762917431 * value)
	{
		____inputCamTimeBeforeDown_72 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamTimeBeforeDown_72), value);
	}

	inline static int32_t get_offset_of__inputCamDownSpeed_73() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputCamDownSpeed_73)); }
	inline InputField_t3762917431 * get__inputCamDownSpeed_73() const { return ____inputCamDownSpeed_73; }
	inline InputField_t3762917431 ** get_address_of__inputCamDownSpeed_73() { return &____inputCamDownSpeed_73; }
	inline void set__inputCamDownSpeed_73(InputField_t3762917431 * value)
	{
		____inputCamDownSpeed_73 = value;
		Il2CppCodeGenWriteBarrier((&____inputCamDownSpeed_73), value);
	}

	inline static int32_t get_offset_of__inputfieldCamSizeXiShu_74() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldCamSizeXiShu_74)); }
	inline InputField_t3762917431 * get__inputfieldCamSizeXiShu_74() const { return ____inputfieldCamSizeXiShu_74; }
	inline InputField_t3762917431 ** get_address_of__inputfieldCamSizeXiShu_74() { return &____inputfieldCamSizeXiShu_74; }
	inline void set__inputfieldCamSizeXiShu_74(InputField_t3762917431 * value)
	{
		____inputfieldCamSizeXiShu_74 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldCamSizeXiShu_74), value);
	}

	inline static int32_t get_offset_of__inputfieldCamZoomSpeed_75() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldCamZoomSpeed_75)); }
	inline InputField_t3762917431 * get__inputfieldCamZoomSpeed_75() const { return ____inputfieldCamZoomSpeed_75; }
	inline InputField_t3762917431 ** get_address_of__inputfieldCamZoomSpeed_75() { return &____inputfieldCamZoomSpeed_75; }
	inline void set__inputfieldCamZoomSpeed_75(InputField_t3762917431 * value)
	{
		____inputfieldCamZoomSpeed_75 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldCamZoomSpeed_75), value);
	}

	inline static int32_t get_offset_of__inputBornProtectTime_76() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputBornProtectTime_76)); }
	inline InputField_t3762917431 * get__inputBornProtectTime_76() const { return ____inputBornProtectTime_76; }
	inline InputField_t3762917431 ** get_address_of__inputBornProtectTime_76() { return &____inputBornProtectTime_76; }
	inline void set__inputBornProtectTime_76(InputField_t3762917431 * value)
	{
		____inputBornProtectTime_76 = value;
		Il2CppCodeGenWriteBarrier((&____inputBornProtectTime_76), value);
	}

	inline static int32_t get_offset_of__inputfieldBallSizeAffectCamSize_77() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldBallSizeAffectCamSize_77)); }
	inline InputField_t3762917431 * get__inputfieldBallSizeAffectCamSize_77() const { return ____inputfieldBallSizeAffectCamSize_77; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBallSizeAffectCamSize_77() { return &____inputfieldBallSizeAffectCamSize_77; }
	inline void set__inputfieldBallSizeAffectCamSize_77(InputField_t3762917431 * value)
	{
		____inputfieldBallSizeAffectCamSize_77 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBallSizeAffectCamSize_77), value);
	}

	inline static int32_t get_offset_of__inputfieldBeanNumPerRange_78() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldBeanNumPerRange_78)); }
	inline InputField_t3762917431 * get__inputfieldBeanNumPerRange_78() const { return ____inputfieldBeanNumPerRange_78; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBeanNumPerRange_78() { return &____inputfieldBeanNumPerRange_78; }
	inline void set__inputfieldBeanNumPerRange_78(InputField_t3762917431 * value)
	{
		____inputfieldBeanNumPerRange_78 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBeanNumPerRange_78), value);
	}

	inline static int32_t get_offset_of__inputfieldBallBaseSpeed_79() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldBallBaseSpeed_79)); }
	inline InputField_t3762917431 * get__inputfieldBallBaseSpeed_79() const { return ____inputfieldBallBaseSpeed_79; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBallBaseSpeed_79() { return &____inputfieldBallBaseSpeed_79; }
	inline void set__inputfieldBallBaseSpeed_79(InputField_t3762917431 * value)
	{
		____inputfieldBallBaseSpeed_79 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBallBaseSpeed_79), value);
	}

	inline static int32_t get_offset_of__inputfieldMaxSpeed_80() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMaxSpeed_80)); }
	inline InputField_t3762917431 * get__inputfieldMaxSpeed_80() const { return ____inputfieldMaxSpeed_80; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMaxSpeed_80() { return &____inputfieldMaxSpeed_80; }
	inline void set__inputfieldMaxSpeed_80(InputField_t3762917431 * value)
	{
		____inputfieldMaxSpeed_80 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMaxSpeed_80), value);
	}

	inline static int32_t get_offset_of__inputfieldSpeedRadiusKaiFangFenMu_81() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpeedRadiusKaiFangFenMu_81)); }
	inline InputField_t3762917431 * get__inputfieldSpeedRadiusKaiFangFenMu_81() const { return ____inputfieldSpeedRadiusKaiFangFenMu_81; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpeedRadiusKaiFangFenMu_81() { return &____inputfieldSpeedRadiusKaiFangFenMu_81; }
	inline void set__inputfieldSpeedRadiusKaiFangFenMu_81(InputField_t3762917431 * value)
	{
		____inputfieldSpeedRadiusKaiFangFenMu_81 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpeedRadiusKaiFangFenMu_81), value);
	}

	inline static int32_t get_offset_of__inputfieldBornMinDiameter_82() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldBornMinDiameter_82)); }
	inline InputField_t3762917431 * get__inputfieldBornMinDiameter_82() const { return ____inputfieldBornMinDiameter_82; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBornMinDiameter_82() { return &____inputfieldBornMinDiameter_82; }
	inline void set__inputfieldBornMinDiameter_82(InputField_t3762917431 * value)
	{
		____inputfieldBornMinDiameter_82 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBornMinDiameter_82), value);
	}

	inline static int32_t get_offset_of__inputfieldThornNumPerRange_83() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldThornNumPerRange_83)); }
	inline InputField_t3762917431 * get__inputfieldThornNumPerRange_83() const { return ____inputfieldThornNumPerRange_83; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornNumPerRange_83() { return &____inputfieldThornNumPerRange_83; }
	inline void set__inputfieldThornNumPerRange_83(InputField_t3762917431 * value)
	{
		____inputfieldThornNumPerRange_83 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornNumPerRange_83), value);
	}

	inline static int32_t get_offset_of__inputfieldGenerateNewBeanTimeInterval_84() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldGenerateNewBeanTimeInterval_84)); }
	inline InputField_t3762917431 * get__inputfieldGenerateNewBeanTimeInterval_84() const { return ____inputfieldGenerateNewBeanTimeInterval_84; }
	inline InputField_t3762917431 ** get_address_of__inputfieldGenerateNewBeanTimeInterval_84() { return &____inputfieldGenerateNewBeanTimeInterval_84; }
	inline void set__inputfieldGenerateNewBeanTimeInterval_84(InputField_t3762917431 * value)
	{
		____inputfieldGenerateNewBeanTimeInterval_84 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldGenerateNewBeanTimeInterval_84), value);
	}

	inline static int32_t get_offset_of__inputfieldGenerateNewThornTimeInterval_85() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldGenerateNewThornTimeInterval_85)); }
	inline InputField_t3762917431 * get__inputfieldGenerateNewThornTimeInterval_85() const { return ____inputfieldGenerateNewThornTimeInterval_85; }
	inline InputField_t3762917431 ** get_address_of__inputfieldGenerateNewThornTimeInterval_85() { return &____inputfieldGenerateNewThornTimeInterval_85; }
	inline void set__inputfieldGenerateNewThornTimeInterval_85(InputField_t3762917431 * value)
	{
		____inputfieldGenerateNewThornTimeInterval_85 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldGenerateNewThornTimeInterval_85), value);
	}

	inline static int32_t get_offset_of__inputfieldBeanSize_86() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldBeanSize_86)); }
	inline InputField_t3762917431 * get__inputfieldBeanSize_86() const { return ____inputfieldBeanSize_86; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBeanSize_86() { return &____inputfieldBeanSize_86; }
	inline void set__inputfieldBeanSize_86(InputField_t3762917431 * value)
	{
		____inputfieldBeanSize_86 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBeanSize_86), value);
	}

	inline static int32_t get_offset_of__inputfieldThornSize_87() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldThornSize_87)); }
	inline InputField_t3762917431 * get__inputfieldThornSize_87() const { return ____inputfieldThornSize_87; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornSize_87() { return &____inputfieldThornSize_87; }
	inline void set__inputfieldThornSize_87(InputField_t3762917431 * value)
	{
		____inputfieldThornSize_87 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornSize_87), value);
	}

	inline static int32_t get_offset_of__inputfieldThornContributeToBallSize_88() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldThornContributeToBallSize_88)); }
	inline InputField_t3762917431 * get__inputfieldThornContributeToBallSize_88() const { return ____inputfieldThornContributeToBallSize_88; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornContributeToBallSize_88() { return &____inputfieldThornContributeToBallSize_88; }
	inline void set__inputfieldThornContributeToBallSize_88(InputField_t3762917431 * value)
	{
		____inputfieldThornContributeToBallSize_88 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornContributeToBallSize_88), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitSporeCostMP_89() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitSporeCostMP_89)); }
	inline InputField_t3762917431 * get__inputfieldSpitSporeCostMP_89() const { return ____inputfieldSpitSporeCostMP_89; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitSporeCostMP_89() { return &____inputfieldSpitSporeCostMP_89; }
	inline void set__inputfieldSpitSporeCostMP_89(InputField_t3762917431 * value)
	{
		____inputfieldSpitSporeCostMP_89 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitSporeCostMP_89), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitSporeThornId_90() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitSporeThornId_90)); }
	inline InputField_t3762917431 * get__inputfieldSpitSporeThornId_90() const { return ____inputfieldSpitSporeThornId_90; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitSporeThornId_90() { return &____inputfieldSpitSporeThornId_90; }
	inline void set__inputfieldSpitSporeThornId_90(InputField_t3762917431 * value)
	{
		____inputfieldSpitSporeThornId_90 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitSporeThornId_90), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitSporeRunDistance_91() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitSporeRunDistance_91)); }
	inline InputField_t3762917431 * get__inputfieldSpitSporeRunDistance_91() const { return ____inputfieldSpitSporeRunDistance_91; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitSporeRunDistance_91() { return &____inputfieldSpitSporeRunDistance_91; }
	inline void set__inputfieldSpitSporeRunDistance_91(InputField_t3762917431 * value)
	{
		____inputfieldSpitSporeRunDistance_91 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitSporeRunDistance_91), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitSporeBeanType_92() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitSporeBeanType_92)); }
	inline InputField_t3762917431 * get__inputfieldSpitSporeBeanType_92() const { return ____inputfieldSpitSporeBeanType_92; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitSporeBeanType_92() { return &____inputfieldSpitSporeBeanType_92; }
	inline void set__inputfieldSpitSporeBeanType_92(InputField_t3762917431 * value)
	{
		____inputfieldSpitSporeBeanType_92 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitSporeBeanType_92), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitRunTime_93() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitRunTime_93)); }
	inline InputField_t3762917431 * get__inputfieldSpitRunTime_93() const { return ____inputfieldSpitRunTime_93; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitRunTime_93() { return &____inputfieldSpitRunTime_93; }
	inline void set__inputfieldSpitRunTime_93(InputField_t3762917431 * value)
	{
		____inputfieldSpitRunTime_93 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitRunTime_93), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitBallRunTime_94() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitBallRunTime_94)); }
	inline InputField_t3762917431 * get__inputfieldSpitBallRunTime_94() const { return ____inputfieldSpitBallRunTime_94; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitBallRunTime_94() { return &____inputfieldSpitBallRunTime_94; }
	inline void set__inputfieldSpitBallRunTime_94(InputField_t3762917431 * value)
	{
		____inputfieldSpitBallRunTime_94 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitBallRunTime_94), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitBallRunDistanceMultiple_95() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitBallRunDistanceMultiple_95)); }
	inline InputField_t3762917431 * get__inputfieldSpitBallRunDistanceMultiple_95() const { return ____inputfieldSpitBallRunDistanceMultiple_95; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitBallRunDistanceMultiple_95() { return &____inputfieldSpitBallRunDistanceMultiple_95; }
	inline void set__inputfieldSpitBallRunDistanceMultiple_95(InputField_t3762917431 * value)
	{
		____inputfieldSpitBallRunDistanceMultiple_95 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitBallRunDistanceMultiple_95), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitBallCostMP_96() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitBallCostMP_96)); }
	inline InputField_t3762917431 * get__inputfieldSpitBallCostMP_96() const { return ____inputfieldSpitBallCostMP_96; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitBallCostMP_96() { return &____inputfieldSpitBallCostMP_96; }
	inline void set__inputfieldSpitBallCostMP_96(InputField_t3762917431 * value)
	{
		____inputfieldSpitBallCostMP_96 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitBallCostMP_96), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitBallStayTime_97() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitBallStayTime_97)); }
	inline InputField_t3762917431 * get__inputfieldSpitBallStayTime_97() const { return ____inputfieldSpitBallStayTime_97; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitBallStayTime_97() { return &____inputfieldSpitBallStayTime_97; }
	inline void set__inputfieldSpitBallStayTime_97(InputField_t3762917431 * value)
	{
		____inputfieldSpitBallStayTime_97 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitBallStayTime_97), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitBallSpeed_98() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitBallSpeed_98)); }
	inline InputField_t3762917431 * get__inputfieldSpitBallSpeed_98() const { return ____inputfieldSpitBallSpeed_98; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitBallSpeed_98() { return &____inputfieldSpitBallSpeed_98; }
	inline void set__inputfieldSpitBallSpeed_98(InputField_t3762917431 * value)
	{
		____inputfieldSpitBallSpeed_98 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitBallSpeed_98), value);
	}

	inline static int32_t get_offset_of__inputfieldAutoAttenuate_99() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldAutoAttenuate_99)); }
	inline InputField_t3762917431 * get__inputfieldAutoAttenuate_99() const { return ____inputfieldAutoAttenuate_99; }
	inline InputField_t3762917431 ** get_address_of__inputfieldAutoAttenuate_99() { return &____inputfieldAutoAttenuate_99; }
	inline void set__inputfieldAutoAttenuate_99(InputField_t3762917431 * value)
	{
		____inputfieldAutoAttenuate_99 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldAutoAttenuate_99), value);
	}

	inline static int32_t get_offset_of__inputfieldThornAttenuate_100() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldThornAttenuate_100)); }
	inline InputField_t3762917431 * get__inputfieldThornAttenuate_100() const { return ____inputfieldThornAttenuate_100; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornAttenuate_100() { return &____inputfieldThornAttenuate_100; }
	inline void set__inputfieldThornAttenuate_100(InputField_t3762917431 * value)
	{
		____inputfieldThornAttenuate_100 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornAttenuate_100), value);
	}

	inline static int32_t get_offset_of__inputfieldAutoAttenuateTimeInterval_101() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldAutoAttenuateTimeInterval_101)); }
	inline InputField_t3762917431 * get__inputfieldAutoAttenuateTimeInterval_101() const { return ____inputfieldAutoAttenuateTimeInterval_101; }
	inline InputField_t3762917431 ** get_address_of__inputfieldAutoAttenuateTimeInterval_101() { return &____inputfieldAutoAttenuateTimeInterval_101; }
	inline void set__inputfieldAutoAttenuateTimeInterval_101(InputField_t3762917431 * value)
	{
		____inputfieldAutoAttenuateTimeInterval_101 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldAutoAttenuateTimeInterval_101), value);
	}

	inline static int32_t get_offset_of__inputfieldForceSpitTotalTime_102() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldForceSpitTotalTime_102)); }
	inline InputField_t3762917431 * get__inputfieldForceSpitTotalTime_102() const { return ____inputfieldForceSpitTotalTime_102; }
	inline InputField_t3762917431 ** get_address_of__inputfieldForceSpitTotalTime_102() { return &____inputfieldForceSpitTotalTime_102; }
	inline void set__inputfieldForceSpitTotalTime_102(InputField_t3762917431 * value)
	{
		____inputfieldForceSpitTotalTime_102 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldForceSpitTotalTime_102), value);
	}

	inline static int32_t get_offset_of__inputfieldShellShrinkTotalTime_103() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldShellShrinkTotalTime_103)); }
	inline InputField_t3762917431 * get__inputfieldShellShrinkTotalTime_103() const { return ____inputfieldShellShrinkTotalTime_103; }
	inline InputField_t3762917431 ** get_address_of__inputfieldShellShrinkTotalTime_103() { return &____inputfieldShellShrinkTotalTime_103; }
	inline void set__inputfieldShellShrinkTotalTime_103(InputField_t3762917431 * value)
	{
		____inputfieldShellShrinkTotalTime_103 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldShellShrinkTotalTime_103), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitShellTotalTime_104() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitShellTotalTime_104)); }
	inline InputField_t3762917431 * get__inputfieldSplitShellTotalTime_104() const { return ____inputfieldSplitShellTotalTime_104; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitShellTotalTime_104() { return &____inputfieldSplitShellTotalTime_104; }
	inline void set__inputfieldSplitShellTotalTime_104(InputField_t3762917431 * value)
	{
		____inputfieldSplitShellTotalTime_104 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitShellTotalTime_104), value);
	}

	inline static int32_t get_offset_of__inputfieldExplodeShellTotalTime_105() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldExplodeShellTotalTime_105)); }
	inline InputField_t3762917431 * get__inputfieldExplodeShellTotalTime_105() const { return ____inputfieldExplodeShellTotalTime_105; }
	inline InputField_t3762917431 ** get_address_of__inputfieldExplodeShellTotalTime_105() { return &____inputfieldExplodeShellTotalTime_105; }
	inline void set__inputfieldExplodeShellTotalTime_105(InputField_t3762917431 * value)
	{
		____inputfieldExplodeShellTotalTime_105 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldExplodeShellTotalTime_105), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitBallTimeInterval_106() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitBallTimeInterval_106)); }
	inline InputField_t3762917431 * get__inputfieldSpitBallTimeInterval_106() const { return ____inputfieldSpitBallTimeInterval_106; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitBallTimeInterval_106() { return &____inputfieldSpitBallTimeInterval_106; }
	inline void set__inputfieldSpitBallTimeInterval_106(InputField_t3762917431 * value)
	{
		____inputfieldSpitBallTimeInterval_106 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitBallTimeInterval_106), value);
	}

	inline static int32_t get_offset_of__inputfieldSpitSporeTimeInterval_107() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpitSporeTimeInterval_107)); }
	inline InputField_t3762917431 * get__inputfieldSpitSporeTimeInterval_107() const { return ____inputfieldSpitSporeTimeInterval_107; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpitSporeTimeInterval_107() { return &____inputfieldSpitSporeTimeInterval_107; }
	inline void set__inputfieldSpitSporeTimeInterval_107(InputField_t3762917431 * value)
	{
		____inputfieldSpitSporeTimeInterval_107 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpitSporeTimeInterval_107), value);
	}

	inline static int32_t get_offset_of__inputfieldMinDiameterToEatThorn_108() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMinDiameterToEatThorn_108)); }
	inline InputField_t3762917431 * get__inputfieldMinDiameterToEatThorn_108() const { return ____inputfieldMinDiameterToEatThorn_108; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMinDiameterToEatThorn_108() { return &____inputfieldMinDiameterToEatThorn_108; }
	inline void set__inputfieldMinDiameterToEatThorn_108(InputField_t3762917431 * value)
	{
		____inputfieldMinDiameterToEatThorn_108 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMinDiameterToEatThorn_108), value);
	}

	inline static int32_t get_offset_of__inputfieldExplodeRunDistanceMultiple_109() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldExplodeRunDistanceMultiple_109)); }
	inline InputField_t3762917431 * get__inputfieldExplodeRunDistanceMultiple_109() const { return ____inputfieldExplodeRunDistanceMultiple_109; }
	inline InputField_t3762917431 ** get_address_of__inputfieldExplodeRunDistanceMultiple_109() { return &____inputfieldExplodeRunDistanceMultiple_109; }
	inline void set__inputfieldExplodeRunDistanceMultiple_109(InputField_t3762917431 * value)
	{
		____inputfieldExplodeRunDistanceMultiple_109 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldExplodeRunDistanceMultiple_109), value);
	}

	inline static int32_t get_offset_of__inputfieldExplodeStayTime_110() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldExplodeStayTime_110)); }
	inline InputField_t3762917431 * get__inputfieldExplodeStayTime_110() const { return ____inputfieldExplodeStayTime_110; }
	inline InputField_t3762917431 ** get_address_of__inputfieldExplodeStayTime_110() { return &____inputfieldExplodeStayTime_110; }
	inline void set__inputfieldExplodeStayTime_110(InputField_t3762917431 * value)
	{
		____inputfieldExplodeStayTime_110 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldExplodeStayTime_110), value);
	}

	inline static int32_t get_offset_of__inputfieldExplodePercent_111() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldExplodePercent_111)); }
	inline InputField_t3762917431 * get__inputfieldExplodePercent_111() const { return ____inputfieldExplodePercent_111; }
	inline InputField_t3762917431 ** get_address_of__inputfieldExplodePercent_111() { return &____inputfieldExplodePercent_111; }
	inline void set__inputfieldExplodePercent_111(InputField_t3762917431 * value)
	{
		____inputfieldExplodePercent_111 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldExplodePercent_111), value);
	}

	inline static int32_t get_offset_of__inputfieldExplodeExpectNum_112() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldExplodeExpectNum_112)); }
	inline InputField_t3762917431 * get__inputfieldExplodeExpectNum_112() const { return ____inputfieldExplodeExpectNum_112; }
	inline InputField_t3762917431 ** get_address_of__inputfieldExplodeExpectNum_112() { return &____inputfieldExplodeExpectNum_112; }
	inline void set__inputfieldExplodeExpectNum_112(InputField_t3762917431 * value)
	{
		____inputfieldExplodeExpectNum_112 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldExplodeExpectNum_112), value);
	}

	inline static int32_t get_offset_of__inputfieldExplodeRunTime_113() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldExplodeRunTime_113)); }
	inline InputField_t3762917431 * get__inputfieldExplodeRunTime_113() const { return ____inputfieldExplodeRunTime_113; }
	inline InputField_t3762917431 ** get_address_of__inputfieldExplodeRunTime_113() { return &____inputfieldExplodeRunTime_113; }
	inline void set__inputfieldExplodeRunTime_113(InputField_t3762917431 * value)
	{
		____inputfieldExplodeRunTime_113 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldExplodeRunTime_113), value);
	}

	inline static int32_t get_offset_of__inputfieldMaxBallNumPerPlayer_114() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMaxBallNumPerPlayer_114)); }
	inline InputField_t3762917431 * get__inputfieldMaxBallNumPerPlayer_114() const { return ____inputfieldMaxBallNumPerPlayer_114; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMaxBallNumPerPlayer_114() { return &____inputfieldMaxBallNumPerPlayer_114; }
	inline void set__inputfieldMaxBallNumPerPlayer_114(InputField_t3762917431 * value)
	{
		____inputfieldMaxBallNumPerPlayer_114 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMaxBallNumPerPlayer_114), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitNum_115() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitNum_115)); }
	inline InputField_t3762917431 * get__inputfieldSplitNum_115() const { return ____inputfieldSplitNum_115; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitNum_115() { return &____inputfieldSplitNum_115; }
	inline void set__inputfieldSplitNum_115(InputField_t3762917431 * value)
	{
		____inputfieldSplitNum_115 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitNum_115), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitRunTime_116() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitRunTime_116)); }
	inline InputField_t3762917431 * get__inputfieldSplitRunTime_116() const { return ____inputfieldSplitRunTime_116; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitRunTime_116() { return &____inputfieldSplitRunTime_116; }
	inline void set__inputfieldSplitRunTime_116(InputField_t3762917431 * value)
	{
		____inputfieldSplitRunTime_116 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitRunTime_116), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitMaxDistance_117() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitMaxDistance_117)); }
	inline InputField_t3762917431 * get__inputfieldSplitMaxDistance_117() const { return ____inputfieldSplitMaxDistance_117; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitMaxDistance_117() { return &____inputfieldSplitMaxDistance_117; }
	inline void set__inputfieldSplitMaxDistance_117(InputField_t3762917431 * value)
	{
		____inputfieldSplitMaxDistance_117 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitMaxDistance_117), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitTimeInterval_118() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitTimeInterval_118)); }
	inline InputField_t3762917431 * get__inputfieldSplitTimeInterval_118() const { return ____inputfieldSplitTimeInterval_118; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitTimeInterval_118() { return &____inputfieldSplitTimeInterval_118; }
	inline void set__inputfieldSplitTimeInterval_118(InputField_t3762917431 * value)
	{
		____inputfieldSplitTimeInterval_118 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitTimeInterval_118), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitCostMP_119() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitCostMP_119)); }
	inline InputField_t3762917431 * get__inputfieldSplitCostMP_119() const { return ____inputfieldSplitCostMP_119; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitCostMP_119() { return &____inputfieldSplitCostMP_119; }
	inline void set__inputfieldSplitCostMP_119(InputField_t3762917431 * value)
	{
		____inputfieldSplitCostMP_119 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitCostMP_119), value);
	}

	inline static int32_t get_offset_of__inputfieldSplitStayTime_120() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSplitStayTime_120)); }
	inline InputField_t3762917431 * get__inputfieldSplitStayTime_120() const { return ____inputfieldSplitStayTime_120; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSplitStayTime_120() { return &____inputfieldSplitStayTime_120; }
	inline void set__inputfieldSplitStayTime_120(InputField_t3762917431 * value)
	{
		____inputfieldSplitStayTime_120 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSplitStayTime_120), value);
	}

	inline static int32_t get_offset_of__MonsterEditor_121() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____MonsterEditor_121)); }
	inline CMonsterEditor_t2594843249 * get__MonsterEditor_121() const { return ____MonsterEditor_121; }
	inline CMonsterEditor_t2594843249 ** get_address_of__MonsterEditor_121() { return &____MonsterEditor_121; }
	inline void set__MonsterEditor_121(CMonsterEditor_t2594843249 * value)
	{
		____MonsterEditor_121 = value;
		Il2CppCodeGenWriteBarrier((&____MonsterEditor_121), value);
	}

	inline static int32_t get_offset_of__ClassEditor_122() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____ClassEditor_122)); }
	inline CClassEditor_t335744478 * get__ClassEditor_122() const { return ____ClassEditor_122; }
	inline CClassEditor_t335744478 ** get_address_of__ClassEditor_122() { return &____ClassEditor_122; }
	inline void set__ClassEditor_122(CClassEditor_t335744478 * value)
	{
		____ClassEditor_122 = value;
		Il2CppCodeGenWriteBarrier((&____ClassEditor_122), value);
	}

	inline static int32_t get_offset_of__dropdownEatMode_123() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____dropdownEatMode_123)); }
	inline Dropdown_t2274391225 * get__dropdownEatMode_123() const { return ____dropdownEatMode_123; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownEatMode_123() { return &____dropdownEatMode_123; }
	inline void set__dropdownEatMode_123(Dropdown_t2274391225 * value)
	{
		____dropdownEatMode_123 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownEatMode_123), value);
	}

	inline static int32_t get_offset_of__inputUnfoldCostMP_124() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputUnfoldCostMP_124)); }
	inline InputField_t3762917431 * get__inputUnfoldCostMP_124() const { return ____inputUnfoldCostMP_124; }
	inline InputField_t3762917431 ** get_address_of__inputUnfoldCostMP_124() { return &____inputUnfoldCostMP_124; }
	inline void set__inputUnfoldCostMP_124(InputField_t3762917431 * value)
	{
		____inputUnfoldCostMP_124 = value;
		Il2CppCodeGenWriteBarrier((&____inputUnfoldCostMP_124), value);
	}

	inline static int32_t get_offset_of__inputMainTriggerPreUnfoldTime_125() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputMainTriggerPreUnfoldTime_125)); }
	inline InputField_t3762917431 * get__inputMainTriggerPreUnfoldTime_125() const { return ____inputMainTriggerPreUnfoldTime_125; }
	inline InputField_t3762917431 ** get_address_of__inputMainTriggerPreUnfoldTime_125() { return &____inputMainTriggerPreUnfoldTime_125; }
	inline void set__inputMainTriggerPreUnfoldTime_125(InputField_t3762917431 * value)
	{
		____inputMainTriggerPreUnfoldTime_125 = value;
		Il2CppCodeGenWriteBarrier((&____inputMainTriggerPreUnfoldTime_125), value);
	}

	inline static int32_t get_offset_of__inputUnfoldSale_126() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputUnfoldSale_126)); }
	inline InputField_t3762917431 * get__inputUnfoldSale_126() const { return ____inputUnfoldSale_126; }
	inline InputField_t3762917431 ** get_address_of__inputUnfoldSale_126() { return &____inputUnfoldSale_126; }
	inline void set__inputUnfoldSale_126(InputField_t3762917431 * value)
	{
		____inputUnfoldSale_126 = value;
		Il2CppCodeGenWriteBarrier((&____inputUnfoldSale_126), value);
	}

	inline static int32_t get_offset_of__inputMainTriggerUnfoldTime_127() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputMainTriggerUnfoldTime_127)); }
	inline InputField_t3762917431 * get__inputMainTriggerUnfoldTime_127() const { return ____inputMainTriggerUnfoldTime_127; }
	inline InputField_t3762917431 ** get_address_of__inputMainTriggerUnfoldTime_127() { return &____inputMainTriggerUnfoldTime_127; }
	inline void set__inputMainTriggerUnfoldTime_127(InputField_t3762917431 * value)
	{
		____inputMainTriggerUnfoldTime_127 = value;
		Il2CppCodeGenWriteBarrier((&____inputMainTriggerUnfoldTime_127), value);
	}

	inline static int32_t get_offset_of__inputUnfoldTimeInterval_128() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputUnfoldTimeInterval_128)); }
	inline InputField_t3762917431 * get__inputUnfoldTimeInterval_128() const { return ____inputUnfoldTimeInterval_128; }
	inline InputField_t3762917431 ** get_address_of__inputUnfoldTimeInterval_128() { return &____inputUnfoldTimeInterval_128; }
	inline void set__inputUnfoldTimeInterval_128(InputField_t3762917431 * value)
	{
		____inputUnfoldTimeInterval_128 = value;
		Il2CppCodeGenWriteBarrier((&____inputUnfoldTimeInterval_128), value);
	}

	inline static int32_t get_offset_of__inputYaQiuTiaoJian_129() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputYaQiuTiaoJian_129)); }
	inline InputField_t3762917431 * get__inputYaQiuTiaoJian_129() const { return ____inputYaQiuTiaoJian_129; }
	inline InputField_t3762917431 ** get_address_of__inputYaQiuTiaoJian_129() { return &____inputYaQiuTiaoJian_129; }
	inline void set__inputYaQiuTiaoJian_129(InputField_t3762917431 * value)
	{
		____inputYaQiuTiaoJian_129 = value;
		Il2CppCodeGenWriteBarrier((&____inputYaQiuTiaoJian_129), value);
	}

	inline static int32_t get_offset_of__inputYaQiuBaiFenBi_130() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputYaQiuBaiFenBi_130)); }
	inline InputField_t3762917431 * get__inputYaQiuBaiFenBi_130() const { return ____inputYaQiuBaiFenBi_130; }
	inline InputField_t3762917431 ** get_address_of__inputYaQiuBaiFenBi_130() { return &____inputYaQiuBaiFenBi_130; }
	inline void set__inputYaQiuBaiFenBi_130(InputField_t3762917431 * value)
	{
		____inputYaQiuBaiFenBi_130 = value;
		Il2CppCodeGenWriteBarrier((&____inputYaQiuBaiFenBi_130), value);
	}

	inline static int32_t get_offset_of__inputYaQiuBaiFenBi_Self_131() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputYaQiuBaiFenBi_Self_131)); }
	inline InputField_t3762917431 * get__inputYaQiuBaiFenBi_Self_131() const { return ____inputYaQiuBaiFenBi_Self_131; }
	inline InputField_t3762917431 ** get_address_of__inputYaQiuBaiFenBi_Self_131() { return &____inputYaQiuBaiFenBi_Self_131; }
	inline void set__inputYaQiuBaiFenBi_Self_131(InputField_t3762917431 * value)
	{
		____inputYaQiuBaiFenBi_Self_131 = value;
		Il2CppCodeGenWriteBarrier((&____inputYaQiuBaiFenBi_Self_131), value);
	}

	inline static int32_t get_offset_of__inputChiCiBaiFenBi_132() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputChiCiBaiFenBi_132)); }
	inline InputField_t3762917431 * get__inputChiCiBaiFenBi_132() const { return ____inputChiCiBaiFenBi_132; }
	inline InputField_t3762917431 ** get_address_of__inputChiCiBaiFenBi_132() { return &____inputChiCiBaiFenBi_132; }
	inline void set__inputChiCiBaiFenBi_132(InputField_t3762917431 * value)
	{
		____inputChiCiBaiFenBi_132 = value;
		Il2CppCodeGenWriteBarrier((&____inputChiCiBaiFenBi_132), value);
	}

	inline static int32_t get_offset_of__inputXiQiuSuDu_133() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputXiQiuSuDu_133)); }
	inline InputField_t3762917431 * get__inputXiQiuSuDu_133() const { return ____inputXiQiuSuDu_133; }
	inline InputField_t3762917431 ** get_address_of__inputXiQiuSuDu_133() { return &____inputXiQiuSuDu_133; }
	inline void set__inputXiQiuSuDu_133(InputField_t3762917431 * value)
	{
		____inputXiQiuSuDu_133 = value;
		Il2CppCodeGenWriteBarrier((&____inputXiQiuSuDu_133), value);
	}

	inline static int32_t get_offset_of__sliderScaleX_134() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderScaleX_134)); }
	inline Slider_t3903728902 * get__sliderScaleX_134() const { return ____sliderScaleX_134; }
	inline Slider_t3903728902 ** get_address_of__sliderScaleX_134() { return &____sliderScaleX_134; }
	inline void set__sliderScaleX_134(Slider_t3903728902 * value)
	{
		____sliderScaleX_134 = value;
		Il2CppCodeGenWriteBarrier((&____sliderScaleX_134), value);
	}

	inline static int32_t get_offset_of__sliderScaleY_135() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderScaleY_135)); }
	inline Slider_t3903728902 * get__sliderScaleY_135() const { return ____sliderScaleY_135; }
	inline Slider_t3903728902 ** get_address_of__sliderScaleY_135() { return &____sliderScaleY_135; }
	inline void set__sliderScaleY_135(Slider_t3903728902 * value)
	{
		____sliderScaleY_135 = value;
		Il2CppCodeGenWriteBarrier((&____sliderScaleY_135), value);
	}

	inline static int32_t get_offset_of__sliderRotation_136() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderRotation_136)); }
	inline Slider_t3903728902 * get__sliderRotation_136() const { return ____sliderRotation_136; }
	inline Slider_t3903728902 ** get_address_of__sliderRotation_136() { return &____sliderRotation_136; }
	inline void set__sliderRotation_136(Slider_t3903728902 * value)
	{
		____sliderRotation_136 = value;
		Il2CppCodeGenWriteBarrier((&____sliderRotation_136), value);
	}

	inline static int32_t get_offset_of__txtPolygonTitle_137() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtPolygonTitle_137)); }
	inline Text_t1901882714 * get__txtPolygonTitle_137() const { return ____txtPolygonTitle_137; }
	inline Text_t1901882714 ** get_address_of__txtPolygonTitle_137() { return &____txtPolygonTitle_137; }
	inline void set__txtPolygonTitle_137(Text_t1901882714 * value)
	{
		____txtPolygonTitle_137 = value;
		Il2CppCodeGenWriteBarrier((&____txtPolygonTitle_137), value);
	}

	inline static int32_t get_offset_of__toogleIsGrass_138() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____toogleIsGrass_138)); }
	inline Toggle_t2735377061 * get__toogleIsGrass_138() const { return ____toogleIsGrass_138; }
	inline Toggle_t2735377061 ** get_address_of__toogleIsGrass_138() { return &____toogleIsGrass_138; }
	inline void set__toogleIsGrass_138(Toggle_t2735377061 * value)
	{
		____toogleIsGrass_138 = value;
		Il2CppCodeGenWriteBarrier((&____toogleIsGrass_138), value);
	}

	inline static int32_t get_offset_of__toogleIsGrassSeed_139() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____toogleIsGrassSeed_139)); }
	inline Toggle_t2735377061 * get__toogleIsGrassSeed_139() const { return ____toogleIsGrassSeed_139; }
	inline Toggle_t2735377061 ** get_address_of__toogleIsGrassSeed_139() { return &____toogleIsGrassSeed_139; }
	inline void set__toogleIsGrassSeed_139(Toggle_t2735377061 * value)
	{
		____toogleIsGrassSeed_139 = value;
		Il2CppCodeGenWriteBarrier((&____toogleIsGrassSeed_139), value);
	}

	inline static int32_t get_offset_of__inputfieldSeedGrassLifeTime_140() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSeedGrassLifeTime_140)); }
	inline InputField_t3762917431 * get__inputfieldSeedGrassLifeTime_140() const { return ____inputfieldSeedGrassLifeTime_140; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSeedGrassLifeTime_140() { return &____inputfieldSeedGrassLifeTime_140; }
	inline void set__inputfieldSeedGrassLifeTime_140(InputField_t3762917431 * value)
	{
		____inputfieldSeedGrassLifeTime_140 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSeedGrassLifeTime_140), value);
	}

	inline static int32_t get_offset_of__inputPolygonScaleX_141() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputPolygonScaleX_141)); }
	inline InputField_t3762917431 * get__inputPolygonScaleX_141() const { return ____inputPolygonScaleX_141; }
	inline InputField_t3762917431 ** get_address_of__inputPolygonScaleX_141() { return &____inputPolygonScaleX_141; }
	inline void set__inputPolygonScaleX_141(InputField_t3762917431 * value)
	{
		____inputPolygonScaleX_141 = value;
		Il2CppCodeGenWriteBarrier((&____inputPolygonScaleX_141), value);
	}

	inline static int32_t get_offset_of__inputPolygonScaleY_142() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputPolygonScaleY_142)); }
	inline InputField_t3762917431 * get__inputPolygonScaleY_142() const { return ____inputPolygonScaleY_142; }
	inline InputField_t3762917431 ** get_address_of__inputPolygonScaleY_142() { return &____inputPolygonScaleY_142; }
	inline void set__inputPolygonScaleY_142(InputField_t3762917431 * value)
	{
		____inputPolygonScaleY_142 = value;
		Il2CppCodeGenWriteBarrier((&____inputPolygonScaleY_142), value);
	}

	inline static int32_t get_offset_of__inputPolygonRotation_143() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputPolygonRotation_143)); }
	inline InputField_t3762917431 * get__inputPolygonRotation_143() const { return ____inputPolygonRotation_143; }
	inline InputField_t3762917431 ** get_address_of__inputPolygonRotation_143() { return &____inputPolygonRotation_143; }
	inline void set__inputPolygonRotation_143(InputField_t3762917431 * value)
	{
		____inputPolygonRotation_143 = value;
		Il2CppCodeGenWriteBarrier((&____inputPolygonRotation_143), value);
	}

	inline static int32_t get_offset_of__toogleIsHardObstacle_144() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____toogleIsHardObstacle_144)); }
	inline Toggle_t2735377061 * get__toogleIsHardObstacle_144() const { return ____toogleIsHardObstacle_144; }
	inline Toggle_t2735377061 ** get_address_of__toogleIsHardObstacle_144() { return &____toogleIsHardObstacle_144; }
	inline void set__toogleIsHardObstacle_144(Toggle_t2735377061 * value)
	{
		____toogleIsHardObstacle_144 = value;
		Il2CppCodeGenWriteBarrier((&____toogleIsHardObstacle_144), value);
	}

	inline static int32_t get_offset_of__PolygonList_145() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____PolygonList_145)); }
	inline ComoList_t2152284863 * get__PolygonList_145() const { return ____PolygonList_145; }
	inline ComoList_t2152284863 ** get_address_of__PolygonList_145() { return &____PolygonList_145; }
	inline void set__PolygonList_145(ComoList_t2152284863 * value)
	{
		____PolygonList_145 = value;
		Il2CppCodeGenWriteBarrier((&____PolygonList_145), value);
	}

	inline static int32_t get_offset_of__sliderDensity_146() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderDensity_146)); }
	inline Slider_t3903728902 * get__sliderDensity_146() const { return ____sliderDensity_146; }
	inline Slider_t3903728902 ** get_address_of__sliderDensity_146() { return &____sliderDensity_146; }
	inline void set__sliderDensity_146(Slider_t3903728902 * value)
	{
		____sliderDensity_146 = value;
		Il2CppCodeGenWriteBarrier((&____sliderDensity_146), value);
	}

	inline static int32_t get_offset_of__sliderMaxDis_147() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderMaxDis_147)); }
	inline Slider_t3903728902 * get__sliderMaxDis_147() const { return ____sliderMaxDis_147; }
	inline Slider_t3903728902 ** get_address_of__sliderMaxDis_147() { return &____sliderMaxDis_147; }
	inline void set__sliderMaxDis_147(Slider_t3903728902 * value)
	{
		____sliderMaxDis_147 = value;
		Il2CppCodeGenWriteBarrier((&____sliderMaxDis_147), value);
	}

	inline static int32_t get_offset_of__sliderBeanLifeTime_148() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderBeanLifeTime_148)); }
	inline Slider_t3903728902 * get__sliderBeanLifeTime_148() const { return ____sliderBeanLifeTime_148; }
	inline Slider_t3903728902 ** get_address_of__sliderBeanLifeTime_148() { return &____sliderBeanLifeTime_148; }
	inline void set__sliderBeanLifeTime_148(Slider_t3903728902 * value)
	{
		____sliderBeanLifeTime_148 = value;
		Il2CppCodeGenWriteBarrier((&____sliderBeanLifeTime_148), value);
	}

	inline static int32_t get_offset_of__txtDensity_149() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtDensity_149)); }
	inline Text_t1901882714 * get__txtDensity_149() const { return ____txtDensity_149; }
	inline Text_t1901882714 ** get_address_of__txtDensity_149() { return &____txtDensity_149; }
	inline void set__txtDensity_149(Text_t1901882714 * value)
	{
		____txtDensity_149 = value;
		Il2CppCodeGenWriteBarrier((&____txtDensity_149), value);
	}

	inline static int32_t get_offset_of__txtMaxDis_150() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtMaxDis_150)); }
	inline Text_t1901882714 * get__txtMaxDis_150() const { return ____txtMaxDis_150; }
	inline Text_t1901882714 ** get_address_of__txtMaxDis_150() { return &____txtMaxDis_150; }
	inline void set__txtMaxDis_150(Text_t1901882714 * value)
	{
		____txtMaxDis_150 = value;
		Il2CppCodeGenWriteBarrier((&____txtMaxDis_150), value);
	}

	inline static int32_t get_offset_of__txtBeanLifeTime_151() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtBeanLifeTime_151)); }
	inline Text_t1901882714 * get__txtBeanLifeTime_151() const { return ____txtBeanLifeTime_151; }
	inline Text_t1901882714 ** get_address_of__txtBeanLifeTime_151() { return &____txtBeanLifeTime_151; }
	inline void set__txtBeanLifeTime_151(Text_t1901882714 * value)
	{
		____txtBeanLifeTime_151 = value;
		Il2CppCodeGenWriteBarrier((&____txtBeanLifeTime_151), value);
	}

	inline static int32_t get_offset_of__toogleShowRealtimeBeanSpray_152() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____toogleShowRealtimeBeanSpray_152)); }
	inline Toggle_t2735377061 * get__toogleShowRealtimeBeanSpray_152() const { return ____toogleShowRealtimeBeanSpray_152; }
	inline Toggle_t2735377061 ** get_address_of__toogleShowRealtimeBeanSpray_152() { return &____toogleShowRealtimeBeanSpray_152; }
	inline void set__toogleShowRealtimeBeanSpray_152(Toggle_t2735377061 * value)
	{
		____toogleShowRealtimeBeanSpray_152 = value;
		Il2CppCodeGenWriteBarrier((&____toogleShowRealtimeBeanSpray_152), value);
	}

	inline static int32_t get_offset_of__inputfieldDensity_153() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldDensity_153)); }
	inline InputField_t3762917431 * get__inputfieldDensity_153() const { return ____inputfieldDensity_153; }
	inline InputField_t3762917431 ** get_address_of__inputfieldDensity_153() { return &____inputfieldDensity_153; }
	inline void set__inputfieldDensity_153(InputField_t3762917431 * value)
	{
		____inputfieldDensity_153 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldDensity_153), value);
	}

	inline static int32_t get_offset_of__inputfieldMeatDensity_154() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMeatDensity_154)); }
	inline InputField_t3762917431 * get__inputfieldMeatDensity_154() const { return ____inputfieldMeatDensity_154; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMeatDensity_154() { return &____inputfieldMeatDensity_154; }
	inline void set__inputfieldMeatDensity_154(InputField_t3762917431 * value)
	{
		____inputfieldMeatDensity_154 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMeatDensity_154), value);
	}

	inline static int32_t get_offset_of__inputfieldMaxDis_155() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMaxDis_155)); }
	inline InputField_t3762917431 * get__inputfieldMaxDis_155() const { return ____inputfieldMaxDis_155; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMaxDis_155() { return &____inputfieldMaxDis_155; }
	inline void set__inputfieldMaxDis_155(InputField_t3762917431 * value)
	{
		____inputfieldMaxDis_155 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMaxDis_155), value);
	}

	inline static int32_t get_offset_of__inputfieldMinDis_156() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMinDis_156)); }
	inline InputField_t3762917431 * get__inputfieldMinDis_156() const { return ____inputfieldMinDis_156; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMinDis_156() { return &____inputfieldMinDis_156; }
	inline void set__inputfieldMinDis_156(InputField_t3762917431 * value)
	{
		____inputfieldMinDis_156 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMinDis_156), value);
	}

	inline static int32_t get_offset_of__inputfieldBeanLifeTime_157() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldBeanLifeTime_157)); }
	inline InputField_t3762917431 * get__inputfieldBeanLifeTime_157() const { return ____inputfieldBeanLifeTime_157; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBeanLifeTime_157() { return &____inputfieldBeanLifeTime_157; }
	inline void set__inputfieldBeanLifeTime_157(InputField_t3762917431 * value)
	{
		____inputfieldBeanLifeTime_157 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBeanLifeTime_157), value);
	}

	inline static int32_t get_offset_of__inputfieldMeat2BeanSprayTime_158() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldMeat2BeanSprayTime_158)); }
	inline InputField_t3762917431 * get__inputfieldMeat2BeanSprayTime_158() const { return ____inputfieldMeat2BeanSprayTime_158; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMeat2BeanSprayTime_158() { return &____inputfieldMeat2BeanSprayTime_158; }
	inline void set__inputfieldMeat2BeanSprayTime_158(InputField_t3762917431 * value)
	{
		____inputfieldMeat2BeanSprayTime_158 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMeat2BeanSprayTime_158), value);
	}

	inline static int32_t get_offset_of__inputfieldSpraySize_159() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSpraySize_159)); }
	inline InputField_t3762917431 * get__inputfieldSpraySize_159() const { return ____inputfieldSpraySize_159; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpraySize_159() { return &____inputfieldSpraySize_159; }
	inline void set__inputfieldSpraySize_159(InputField_t3762917431 * value)
	{
		____inputfieldSpraySize_159 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpraySize_159), value);
	}

	inline static int32_t get_offset_of__inputfieldDrawSpeed_160() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldDrawSpeed_160)); }
	inline InputField_t3762917431 * get__inputfieldDrawSpeed_160() const { return ____inputfieldDrawSpeed_160; }
	inline InputField_t3762917431 ** get_address_of__inputfieldDrawSpeed_160() { return &____inputfieldDrawSpeed_160; }
	inline void set__inputfieldDrawSpeed_160(InputField_t3762917431 * value)
	{
		____inputfieldDrawSpeed_160 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldDrawSpeed_160), value);
	}

	inline static int32_t get_offset_of__inputfieldDrawArea_161() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldDrawArea_161)); }
	inline InputField_t3762917431 * get__inputfieldDrawArea_161() const { return ____inputfieldDrawArea_161; }
	inline InputField_t3762917431 ** get_address_of__inputfieldDrawArea_161() { return &____inputfieldDrawArea_161; }
	inline void set__inputfieldDrawArea_161(InputField_t3762917431 * value)
	{
		____inputfieldDrawArea_161 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldDrawArea_161), value);
	}

	inline static int32_t get_offset_of__inputfieldPreDrawTime_162() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldPreDrawTime_162)); }
	inline InputField_t3762917431 * get__inputfieldPreDrawTime_162() const { return ____inputfieldPreDrawTime_162; }
	inline InputField_t3762917431 ** get_address_of__inputfieldPreDrawTime_162() { return &____inputfieldPreDrawTime_162; }
	inline void set__inputfieldPreDrawTime_162(InputField_t3762917431 * value)
	{
		____inputfieldPreDrawTime_162 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldPreDrawTime_162), value);
	}

	inline static int32_t get_offset_of__inputfieldDrawTime_163() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldDrawTime_163)); }
	inline InputField_t3762917431 * get__inputfieldDrawTime_163() const { return ____inputfieldDrawTime_163; }
	inline InputField_t3762917431 ** get_address_of__inputfieldDrawTime_163() { return &____inputfieldDrawTime_163; }
	inline void set__inputfieldDrawTime_163(InputField_t3762917431 * value)
	{
		____inputfieldDrawTime_163 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldDrawTime_163), value);
	}

	inline static int32_t get_offset_of__inputfieldDrawInterval_164() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldDrawInterval_164)); }
	inline InputField_t3762917431 * get__inputfieldDrawInterval_164() const { return ____inputfieldDrawInterval_164; }
	inline InputField_t3762917431 ** get_address_of__inputfieldDrawInterval_164() { return &____inputfieldDrawInterval_164; }
	inline void set__inputfieldDrawInterval_164(InputField_t3762917431 * value)
	{
		____inputfieldDrawInterval_164 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldDrawInterval_164), value);
	}

	inline static int32_t get_offset_of__inputfieldThornChance_165() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldThornChance_165)); }
	inline InputField_t3762917431 * get__inputfieldThornChance_165() const { return ____inputfieldThornChance_165; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornChance_165() { return &____inputfieldThornChance_165; }
	inline void set__inputfieldThornChance_165(InputField_t3762917431 * value)
	{
		____inputfieldThornChance_165 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornChance_165), value);
	}

	inline static int32_t get_offset_of__inputfieldSprayInterval_166() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputfieldSprayInterval_166)); }
	inline InputField_t3762917431 * get__inputfieldSprayInterval_166() const { return ____inputfieldSprayInterval_166; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSprayInterval_166() { return &____inputfieldSprayInterval_166; }
	inline void set__inputfieldSprayInterval_166(InputField_t3762917431 * value)
	{
		____inputfieldSprayInterval_166 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSprayInterval_166), value);
	}

	inline static int32_t get_offset_of__txtThornChace_167() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____txtThornChace_167)); }
	inline Text_t1901882714 * get__txtThornChace_167() const { return ____txtThornChace_167; }
	inline Text_t1901882714 ** get_address_of__txtThornChace_167() { return &____txtThornChace_167; }
	inline void set__txtThornChace_167(Text_t1901882714 * value)
	{
		____txtThornChace_167 = value;
		Il2CppCodeGenWriteBarrier((&____txtThornChace_167), value);
	}

	inline static int32_t get_offset_of_m_nThornChance_168() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_nThornChance_168)); }
	inline int32_t get_m_nThornChance_168() const { return ___m_nThornChance_168; }
	inline int32_t* get_address_of_m_nThornChance_168() { return &___m_nThornChance_168; }
	inline void set_m_nThornChance_168(int32_t value)
	{
		___m_nThornChance_168 = value;
	}

	inline static int32_t get_offset_of_dropdownColorThorn_169() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___dropdownColorThorn_169)); }
	inline Dropdown_t2274391225 * get_dropdownColorThorn_169() const { return ___dropdownColorThorn_169; }
	inline Dropdown_t2274391225 ** get_address_of_dropdownColorThorn_169() { return &___dropdownColorThorn_169; }
	inline void set_dropdownColorThorn_169(Dropdown_t2274391225 * value)
	{
		___dropdownColorThorn_169 = value;
		Il2CppCodeGenWriteBarrier((&___dropdownColorThorn_169), value);
	}

	inline static int32_t get_offset_of_m_imgColorThorn_170() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_imgColorThorn_170)); }
	inline Image_t2670269651 * get_m_imgColorThorn_170() const { return ___m_imgColorThorn_170; }
	inline Image_t2670269651 ** get_address_of_m_imgColorThorn_170() { return &___m_imgColorThorn_170; }
	inline void set_m_imgColorThorn_170(Image_t2670269651 * value)
	{
		___m_imgColorThorn_170 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgColorThorn_170), value);
	}

	inline static int32_t get_offset_of_m_inputfieldColorThornThornSize_171() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldColorThornThornSize_171)); }
	inline InputField_t3762917431 * get_m_inputfieldColorThornThornSize_171() const { return ___m_inputfieldColorThornThornSize_171; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldColorThornThornSize_171() { return &___m_inputfieldColorThornThornSize_171; }
	inline void set_m_inputfieldColorThornThornSize_171(InputField_t3762917431 * value)
	{
		___m_inputfieldColorThornThornSize_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldColorThornThornSize_171), value);
	}

	inline static int32_t get_offset_of_m_inputfieldColorThornBallSize_172() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldColorThornBallSize_172)); }
	inline InputField_t3762917431 * get_m_inputfieldColorThornBallSize_172() const { return ___m_inputfieldColorThornBallSize_172; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldColorThornBallSize_172() { return &___m_inputfieldColorThornBallSize_172; }
	inline void set_m_inputfieldColorThornBallSize_172(InputField_t3762917431 * value)
	{
		___m_inputfieldColorThornBallSize_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldColorThornBallSize_172), value);
	}

	inline static int32_t get_offset_of_m_inputfieldColorThornRebornTime_173() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldColorThornRebornTime_173)); }
	inline InputField_t3762917431 * get_m_inputfieldColorThornRebornTime_173() const { return ___m_inputfieldColorThornRebornTime_173; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldColorThornRebornTime_173() { return &___m_inputfieldColorThornRebornTime_173; }
	inline void set_m_inputfieldColorThornRebornTime_173(InputField_t3762917431 * value)
	{
		___m_inputfieldColorThornRebornTime_173 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldColorThornRebornTime_173), value);
	}

	inline static int32_t get_offset_of_m_toggleAddThorn_174() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_toggleAddThorn_174)); }
	inline Toggle_t2735377061 * get_m_toggleAddThorn_174() const { return ___m_toggleAddThorn_174; }
	inline Toggle_t2735377061 ** get_address_of_m_toggleAddThorn_174() { return &___m_toggleAddThorn_174; }
	inline void set_m_toggleAddThorn_174(Toggle_t2735377061 * value)
	{
		___m_toggleAddThorn_174 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggleAddThorn_174), value);
	}

	inline static int32_t get_offset_of_m_toggleRemoveThorn_175() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_toggleRemoveThorn_175)); }
	inline Toggle_t2735377061 * get_m_toggleRemoveThorn_175() const { return ___m_toggleRemoveThorn_175; }
	inline Toggle_t2735377061 ** get_address_of_m_toggleRemoveThorn_175() { return &___m_toggleRemoveThorn_175; }
	inline void set_m_toggleRemoveThorn_175(Toggle_t2735377061 * value)
	{
		___m_toggleRemoveThorn_175 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggleRemoveThorn_175), value);
	}

	inline static int32_t get_offset_of_m_inputfieldLababaAreaThreshold_177() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldLababaAreaThreshold_177)); }
	inline InputField_t3762917431 * get_m_inputfieldLababaAreaThreshold_177() const { return ___m_inputfieldLababaAreaThreshold_177; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldLababaAreaThreshold_177() { return &___m_inputfieldLababaAreaThreshold_177; }
	inline void set_m_inputfieldLababaAreaThreshold_177(InputField_t3762917431 * value)
	{
		___m_inputfieldLababaAreaThreshold_177 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldLababaAreaThreshold_177), value);
	}

	inline static int32_t get_offset_of_m_inputfieldLababaRunTime_178() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldLababaRunTime_178)); }
	inline InputField_t3762917431 * get_m_inputfieldLababaRunTime_178() const { return ___m_inputfieldLababaRunTime_178; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldLababaRunTime_178() { return &___m_inputfieldLababaRunTime_178; }
	inline void set_m_inputfieldLababaRunTime_178(InputField_t3762917431 * value)
	{
		___m_inputfieldLababaRunTime_178 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldLababaRunTime_178), value);
	}

	inline static int32_t get_offset_of_m_inputfieldLababaRunDistance_179() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldLababaRunDistance_179)); }
	inline InputField_t3762917431 * get_m_inputfieldLababaRunDistance_179() const { return ___m_inputfieldLababaRunDistance_179; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldLababaRunDistance_179() { return &___m_inputfieldLababaRunDistance_179; }
	inline void set_m_inputfieldLababaRunDistance_179(InputField_t3762917431 * value)
	{
		___m_inputfieldLababaRunDistance_179 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldLababaRunDistance_179), value);
	}

	inline static int32_t get_offset_of_m_inputfieldLababaColdDown_180() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldLababaColdDown_180)); }
	inline InputField_t3762917431 * get_m_inputfieldLababaColdDown_180() const { return ___m_inputfieldLababaColdDown_180; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldLababaColdDown_180() { return &___m_inputfieldLababaColdDown_180; }
	inline void set_m_inputfieldLababaColdDown_180(InputField_t3762917431 * value)
	{
		___m_inputfieldLababaColdDown_180 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldLababaColdDown_180), value);
	}

	inline static int32_t get_offset_of_m_inputfieldLababaAreaCostPercent_181() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldLababaAreaCostPercent_181)); }
	inline InputField_t3762917431 * get_m_inputfieldLababaAreaCostPercent_181() const { return ___m_inputfieldLababaAreaCostPercent_181; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldLababaAreaCostPercent_181() { return &___m_inputfieldLababaAreaCostPercent_181; }
	inline void set_m_inputfieldLababaAreaCostPercent_181(InputField_t3762917431 * value)
	{
		___m_inputfieldLababaAreaCostPercent_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldLababaAreaCostPercent_181), value);
	}

	inline static int32_t get_offset_of_m_inputfieldLababaLiveTime_182() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_inputfieldLababaLiveTime_182)); }
	inline InputField_t3762917431 * get_m_inputfieldLababaLiveTime_182() const { return ___m_inputfieldLababaLiveTime_182; }
	inline InputField_t3762917431 ** get_address_of_m_inputfieldLababaLiveTime_182() { return &___m_inputfieldLababaLiveTime_182; }
	inline void set_m_inputfieldLababaLiveTime_182(InputField_t3762917431 * value)
	{
		___m_inputfieldLababaLiveTime_182 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputfieldLababaLiveTime_182), value);
	}

	inline static int32_t get_offset_of__dropdownFoodType_183() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____dropdownFoodType_183)); }
	inline Dropdown_t2274391225 * get__dropdownFoodType_183() const { return ____dropdownFoodType_183; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownFoodType_183() { return &____dropdownFoodType_183; }
	inline void set__dropdownFoodType_183(Dropdown_t2274391225 * value)
	{
		____dropdownFoodType_183 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownFoodType_183), value);
	}

	inline static int32_t get_offset_of__inputName_184() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputName_184)); }
	inline InputField_t3762917431 * get__inputName_184() const { return ____inputName_184; }
	inline InputField_t3762917431 ** get_address_of__inputName_184() { return &____inputName_184; }
	inline void set__inputName_184(InputField_t3762917431 * value)
	{
		____inputName_184 = value;
		Il2CppCodeGenWriteBarrier((&____inputName_184), value);
	}

	inline static int32_t get_offset_of__inputDesc_185() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputDesc_185)); }
	inline InputField_t3762917431 * get__inputDesc_185() const { return ____inputDesc_185; }
	inline InputField_t3762917431 ** get_address_of__inputDesc_185() { return &____inputDesc_185; }
	inline void set__inputDesc_185(InputField_t3762917431 * value)
	{
		____inputDesc_185 = value;
		Il2CppCodeGenWriteBarrier((&____inputDesc_185), value);
	}

	inline static int32_t get_offset_of__inputValue_186() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputValue_186)); }
	inline InputField_t3762917431 * get__inputValue_186() const { return ____inputValue_186; }
	inline InputField_t3762917431 ** get_address_of__inputValue_186() { return &____inputValue_186; }
	inline void set__inputValue_186(InputField_t3762917431 * value)
	{
		____inputValue_186 = value;
		Il2CppCodeGenWriteBarrier((&____inputValue_186), value);
	}

	inline static int32_t get_offset_of__inputNum_187() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputNum_187)); }
	inline InputField_t3762917431 * get__inputNum_187() const { return ____inputNum_187; }
	inline InputField_t3762917431 ** get_address_of__inputNum_187() { return &____inputNum_187; }
	inline void set__inputNum_187(InputField_t3762917431 * value)
	{
		____inputNum_187 = value;
		Il2CppCodeGenWriteBarrier((&____inputNum_187), value);
	}

	inline static int32_t get_offset_of__inputTotalTime_188() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputTotalTime_188)); }
	inline InputField_t3762917431 * get__inputTotalTime_188() const { return ____inputTotalTime_188; }
	inline InputField_t3762917431 ** get_address_of__inputTotalTime_188() { return &____inputTotalTime_188; }
	inline void set__inputTotalTime_188(InputField_t3762917431 * value)
	{
		____inputTotalTime_188 = value;
		Il2CppCodeGenWriteBarrier((&____inputTotalTime_188), value);
	}

	inline static int32_t get_offset_of__inputMinScale_189() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputMinScale_189)); }
	inline InputField_t3762917431 * get__inputMinScale_189() const { return ____inputMinScale_189; }
	inline InputField_t3762917431 ** get_address_of__inputMinScale_189() { return &____inputMinScale_189; }
	inline void set__inputMinScale_189(InputField_t3762917431 * value)
	{
		____inputMinScale_189 = value;
		Il2CppCodeGenWriteBarrier((&____inputMinScale_189), value);
	}

	inline static int32_t get_offset_of__sliderCircleSizeRange_190() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____sliderCircleSizeRange_190)); }
	inline Slider_t3903728902 * get__sliderCircleSizeRange_190() const { return ____sliderCircleSizeRange_190; }
	inline Slider_t3903728902 ** get_address_of__sliderCircleSizeRange_190() { return &____sliderCircleSizeRange_190; }
	inline void set__sliderCircleSizeRange_190(Slider_t3903728902 * value)
	{
		____sliderCircleSizeRange_190 = value;
		Il2CppCodeGenWriteBarrier((&____sliderCircleSizeRange_190), value);
	}

	inline static int32_t get_offset_of_m_CurEditingBeiShuDouZiConfig_191() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_CurEditingBeiShuDouZiConfig_191)); }
	inline sBeiShuDouZiConfig_t599764670  get_m_CurEditingBeiShuDouZiConfig_191() const { return ___m_CurEditingBeiShuDouZiConfig_191; }
	inline sBeiShuDouZiConfig_t599764670 * get_address_of_m_CurEditingBeiShuDouZiConfig_191() { return &___m_CurEditingBeiShuDouZiConfig_191; }
	inline void set_m_CurEditingBeiShuDouZiConfig_191(sBeiShuDouZiConfig_t599764670  value)
	{
		___m_CurEditingBeiShuDouZiConfig_191 = value;
	}

	inline static int32_t get_offset_of_m_lstBeiShuDouZi_192() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstBeiShuDouZi_192)); }
	inline List_1_t2071839412 * get_m_lstBeiShuDouZi_192() const { return ___m_lstBeiShuDouZi_192; }
	inline List_1_t2071839412 ** get_address_of_m_lstBeiShuDouZi_192() { return &___m_lstBeiShuDouZi_192; }
	inline void set_m_lstBeiShuDouZi_192(List_1_t2071839412 * value)
	{
		___m_lstBeiShuDouZi_192 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeiShuDouZi_192), value);
	}

	inline static int32_t get_offset_of_m_fYaQiuTiaoJian_193() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fYaQiuTiaoJian_193)); }
	inline float get_m_fYaQiuTiaoJian_193() const { return ___m_fYaQiuTiaoJian_193; }
	inline float* get_address_of_m_fYaQiuTiaoJian_193() { return &___m_fYaQiuTiaoJian_193; }
	inline void set_m_fYaQiuTiaoJian_193(float value)
	{
		___m_fYaQiuTiaoJian_193 = value;
	}

	inline static int32_t get_offset_of_m_fYaQiuBaiFenBi_194() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fYaQiuBaiFenBi_194)); }
	inline float get_m_fYaQiuBaiFenBi_194() const { return ___m_fYaQiuBaiFenBi_194; }
	inline float* get_address_of_m_fYaQiuBaiFenBi_194() { return &___m_fYaQiuBaiFenBi_194; }
	inline void set_m_fYaQiuBaiFenBi_194(float value)
	{
		___m_fYaQiuBaiFenBi_194 = value;
	}

	inline static int32_t get_offset_of_m_fYaQiuBaiFenBi_Self_195() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fYaQiuBaiFenBi_Self_195)); }
	inline float get_m_fYaQiuBaiFenBi_Self_195() const { return ___m_fYaQiuBaiFenBi_Self_195; }
	inline float* get_address_of_m_fYaQiuBaiFenBi_Self_195() { return &___m_fYaQiuBaiFenBi_Self_195; }
	inline void set_m_fYaQiuBaiFenBi_Self_195(float value)
	{
		___m_fYaQiuBaiFenBi_Self_195 = value;
	}

	inline static int32_t get_offset_of_m_fChiCiBaiFenBi_196() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fChiCiBaiFenBi_196)); }
	inline float get_m_fChiCiBaiFenBi_196() const { return ___m_fChiCiBaiFenBi_196; }
	inline float* get_address_of_m_fChiCiBaiFenBi_196() { return &___m_fChiCiBaiFenBi_196; }
	inline void set_m_fChiCiBaiFenBi_196(float value)
	{
		___m_fChiCiBaiFenBi_196 = value;
	}

	inline static int32_t get_offset_of_m_fXiQiuSuDu_197() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fXiQiuSuDu_197)); }
	inline float get_m_fXiQiuSuDu_197() const { return ___m_fXiQiuSuDu_197; }
	inline float* get_address_of_m_fXiQiuSuDu_197() { return &___m_fXiQiuSuDu_197; }
	inline void set_m_fXiQiuSuDu_197(float value)
	{
		___m_fXiQiuSuDu_197 = value;
	}

	inline static int32_t get_offset_of_m_eOp_198() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_eOp_198)); }
	inline int32_t get_m_eOp_198() const { return ___m_eOp_198; }
	inline int32_t* get_address_of_m_eOp_198() { return &___m_eOp_198; }
	inline void set_m_eOp_198(int32_t value)
	{
		___m_eOp_198 = value;
	}

	inline static int32_t get_offset_of_m_bMapInitCompleted_199() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_bMapInitCompleted_199)); }
	inline bool get_m_bMapInitCompleted_199() const { return ___m_bMapInitCompleted_199; }
	inline bool* get_address_of_m_bMapInitCompleted_199() { return &___m_bMapInitCompleted_199; }
	inline void set_m_bMapInitCompleted_199(bool value)
	{
		___m_bMapInitCompleted_199 = value;
	}

	inline static int32_t get_offset_of_m_nCurSelectedColorThornIdx_200() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_nCurSelectedColorThornIdx_200)); }
	inline int32_t get_m_nCurSelectedColorThornIdx_200() const { return ___m_nCurSelectedColorThornIdx_200; }
	inline int32_t* get_address_of_m_nCurSelectedColorThornIdx_200() { return &___m_nCurSelectedColorThornIdx_200; }
	inline void set_m_nCurSelectedColorThornIdx_200(int32_t value)
	{
		___m_nCurSelectedColorThornIdx_200 = value;
	}

	inline static int32_t get_offset_of_m_bFirstMouseLefDown_201() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_bFirstMouseLefDown_201)); }
	inline bool get_m_bFirstMouseLefDown_201() const { return ___m_bFirstMouseLefDown_201; }
	inline bool* get_address_of_m_bFirstMouseLefDown_201() { return &___m_bFirstMouseLefDown_201; }
	inline void set_m_bFirstMouseLefDown_201(bool value)
	{
		___m_bFirstMouseLefDown_201 = value;
	}

	inline static int32_t get_offset_of_m_bFirstMouseRightDown_202() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_bFirstMouseRightDown_202)); }
	inline bool get_m_bFirstMouseRightDown_202() const { return ___m_bFirstMouseRightDown_202; }
	inline bool* get_address_of_m_bFirstMouseRightDown_202() { return &___m_bFirstMouseRightDown_202; }
	inline void set_m_bFirstMouseRightDown_202(bool value)
	{
		___m_bFirstMouseRightDown_202 = value;
	}

	inline static int32_t get_offset_of_m_dicEatModeConfig_203() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_dicEatModeConfig_203)); }
	inline Dictionary_2_t1182523073 * get_m_dicEatModeConfig_203() const { return ___m_dicEatModeConfig_203; }
	inline Dictionary_2_t1182523073 ** get_address_of_m_dicEatModeConfig_203() { return &___m_dicEatModeConfig_203; }
	inline void set_m_dicEatModeConfig_203(Dictionary_2_t1182523073 * value)
	{
		___m_dicEatModeConfig_203 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicEatModeConfig_203), value);
	}

	inline static int32_t get_offset_of_m_dicCommonConfig_204() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_dicCommonConfig_204)); }
	inline Dictionary_2_t1182523073 * get_m_dicCommonConfig_204() const { return ___m_dicCommonConfig_204; }
	inline Dictionary_2_t1182523073 ** get_address_of_m_dicCommonConfig_204() { return &___m_dicCommonConfig_204; }
	inline void set_m_dicCommonConfig_204(Dictionary_2_t1182523073 * value)
	{
		___m_dicCommonConfig_204 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCommonConfig_204), value);
	}

	inline static int32_t get_offset_of_m_dicConfigCommon2_205() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_dicConfigCommon2_205)); }
	inline Dictionary_2_t1632706988 * get_m_dicConfigCommon2_205() const { return ___m_dicConfigCommon2_205; }
	inline Dictionary_2_t1632706988 ** get_address_of_m_dicConfigCommon2_205() { return &___m_dicConfigCommon2_205; }
	inline void set_m_dicConfigCommon2_205(Dictionary_2_t1632706988 * value)
	{
		___m_dicConfigCommon2_205 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicConfigCommon2_205), value);
	}

	inline static int32_t get_offset_of_m_lstFakeRandomThornPos_206() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstFakeRandomThornPos_206)); }
	inline List_1_t3628304265 * get_m_lstFakeRandomThornPos_206() const { return ___m_lstFakeRandomThornPos_206; }
	inline List_1_t3628304265 ** get_address_of_m_lstFakeRandomThornPos_206() { return &___m_lstFakeRandomThornPos_206; }
	inline void set_m_lstFakeRandomThornPos_206(List_1_t3628304265 * value)
	{
		___m_lstFakeRandomThornPos_206 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstFakeRandomThornPos_206), value);
	}

	inline static int32_t get_offset_of_m_dicExplodeDir_207() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_dicExplodeDir_207)); }
	inline Dictionary_2_t2517017596 * get_m_dicExplodeDir_207() const { return ___m_dicExplodeDir_207; }
	inline Dictionary_2_t2517017596 ** get_address_of_m_dicExplodeDir_207() { return &___m_dicExplodeDir_207; }
	inline void set_m_dicExplodeDir_207(Dictionary_2_t2517017596 * value)
	{
		___m_dicExplodeDir_207 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicExplodeDir_207), value);
	}

	inline static int32_t get_offset_of_m_nGrassGUID_208() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_nGrassGUID_208)); }
	inline int32_t get_m_nGrassGUID_208() const { return ___m_nGrassGUID_208; }
	inline int32_t* get_address_of_m_nGrassGUID_208() { return &___m_nGrassGUID_208; }
	inline void set_m_nGrassGUID_208(int32_t value)
	{
		___m_nGrassGUID_208 = value;
	}

	inline static int32_t get_offset_of_m_lstGrass_209() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstGrass_209)); }
	inline List_1_t1484766997 * get_m_lstGrass_209() const { return ___m_lstGrass_209; }
	inline List_1_t1484766997 ** get_address_of_m_lstGrass_209() { return &___m_lstGrass_209; }
	inline void set_m_lstGrass_209(List_1_t1484766997 * value)
	{
		___m_lstGrass_209 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstGrass_209), value);
	}

	inline static int32_t get_offset_of_m_lstPolygons_212() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstPolygons_212)); }
	inline List_1_t3319525431 * get_m_lstPolygons_212() const { return ___m_lstPolygons_212; }
	inline List_1_t3319525431 ** get_address_of_m_lstPolygons_212() { return &___m_lstPolygons_212; }
	inline void set_m_lstPolygons_212(List_1_t3319525431 * value)
	{
		___m_lstPolygons_212 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPolygons_212), value);
	}

	inline static int32_t get_offset_of_m_lstSprays_213() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstSprays_213)); }
	inline List_1_t2194232156 * get_m_lstSprays_213() const { return ___m_lstSprays_213; }
	inline List_1_t2194232156 ** get_address_of_m_lstSprays_213() { return &___m_lstSprays_213; }
	inline void set_m_lstSprays_213(List_1_t2194232156 * value)
	{
		___m_lstSprays_213 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSprays_213), value);
	}

	inline static int32_t get_offset_of_m_bShowRealtimeBeanSpray_214() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_bShowRealtimeBeanSpray_214)); }
	inline bool get_m_bShowRealtimeBeanSpray_214() const { return ___m_bShowRealtimeBeanSpray_214; }
	inline bool* get_address_of_m_bShowRealtimeBeanSpray_214() { return &___m_bShowRealtimeBeanSpray_214; }
	inline void set_m_bShowRealtimeBeanSpray_214(bool value)
	{
		___m_bShowRealtimeBeanSpray_214 = value;
	}

	inline static int32_t get_offset_of_m_fDaTaoShaSize_215() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fDaTaoShaSize_215)); }
	inline float get_m_fDaTaoShaSize_215() const { return ___m_fDaTaoShaSize_215; }
	inline float* get_address_of_m_fDaTaoShaSize_215() { return &___m_fDaTaoShaSize_215; }
	inline void set_m_fDaTaoShaSize_215(float value)
	{
		___m_fDaTaoShaSize_215 = value;
	}

	inline static int32_t get_offset_of_m_fThornAttenuateSpeed_216() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fThornAttenuateSpeed_216)); }
	inline float get_m_fThornAttenuateSpeed_216() const { return ___m_fThornAttenuateSpeed_216; }
	inline float* get_address_of_m_fThornAttenuateSpeed_216() { return &___m_fThornAttenuateSpeed_216; }
	inline void set_m_fThornAttenuateSpeed_216(float value)
	{
		___m_fThornAttenuateSpeed_216 = value;
	}

	inline static int32_t get_offset_of_m_lstRebornRandomPos_217() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_lstRebornRandomPos_217)); }
	inline List_1_t1506737460 * get_m_lstRebornRandomPos_217() const { return ___m_lstRebornRandomPos_217; }
	inline List_1_t1506737460 ** get_address_of_m_lstRebornRandomPos_217() { return &___m_lstRebornRandomPos_217; }
	inline void set_m_lstRebornRandomPos_217(List_1_t1506737460 * value)
	{
		___m_lstRebornRandomPos_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRebornRandomPos_217), value);
	}

	inline static int32_t get_offset_of_m_vecRebornSpotRandomPos_218() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_vecRebornSpotRandomPos_218)); }
	inline Vector3_t3722313464  get_m_vecRebornSpotRandomPos_218() const { return ___m_vecRebornSpotRandomPos_218; }
	inline Vector3_t3722313464 * get_address_of_m_vecRebornSpotRandomPos_218() { return &___m_vecRebornSpotRandomPos_218; }
	inline void set_m_vecRebornSpotRandomPos_218(Vector3_t3722313464  value)
	{
		___m_vecRebornSpotRandomPos_218 = value;
	}

	inline static int32_t get_offset_of_vecScreenLeftBottom_219() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___vecScreenLeftBottom_219)); }
	inline Vector3_t3722313464  get_vecScreenLeftBottom_219() const { return ___vecScreenLeftBottom_219; }
	inline Vector3_t3722313464 * get_address_of_vecScreenLeftBottom_219() { return &___vecScreenLeftBottom_219; }
	inline void set_vecScreenLeftBottom_219(Vector3_t3722313464  value)
	{
		___vecScreenLeftBottom_219 = value;
	}

	inline static int32_t get_offset_of_vecScreenRightTop_220() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___vecScreenRightTop_220)); }
	inline Vector3_t3722313464  get_vecScreenRightTop_220() const { return ___vecScreenRightTop_220; }
	inline Vector3_t3722313464 * get_address_of_vecScreenRightTop_220() { return &___vecScreenRightTop_220; }
	inline void set_vecScreenRightTop_220(Vector3_t3722313464  value)
	{
		___vecScreenRightTop_220 = value;
	}

	inline static int32_t get_offset_of_m_fWorldLeft_223() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldLeft_223)); }
	inline float get_m_fWorldLeft_223() const { return ___m_fWorldLeft_223; }
	inline float* get_address_of_m_fWorldLeft_223() { return &___m_fWorldLeft_223; }
	inline void set_m_fWorldLeft_223(float value)
	{
		___m_fWorldLeft_223 = value;
	}

	inline static int32_t get_offset_of_m_fWorldRight_224() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldRight_224)); }
	inline float get_m_fWorldRight_224() const { return ___m_fWorldRight_224; }
	inline float* get_address_of_m_fWorldRight_224() { return &___m_fWorldRight_224; }
	inline void set_m_fWorldRight_224(float value)
	{
		___m_fWorldRight_224 = value;
	}

	inline static int32_t get_offset_of_m_fWorldTop_225() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldTop_225)); }
	inline float get_m_fWorldTop_225() const { return ___m_fWorldTop_225; }
	inline float* get_address_of_m_fWorldTop_225() { return &___m_fWorldTop_225; }
	inline void set_m_fWorldTop_225(float value)
	{
		___m_fWorldTop_225 = value;
	}

	inline static int32_t get_offset_of_m_fWorldBottom_226() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fWorldBottom_226)); }
	inline float get_m_fWorldBottom_226() const { return ___m_fWorldBottom_226; }
	inline float* get_address_of_m_fWorldBottom_226() { return &___m_fWorldBottom_226; }
	inline void set_m_fWorldBottom_226(float value)
	{
		___m_fWorldBottom_226 = value;
	}

	inline static int32_t get_offset_of__inputBallMinArea_227() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputBallMinArea_227)); }
	inline InputField_t3762917431 * get__inputBallMinArea_227() const { return ____inputBallMinArea_227; }
	inline InputField_t3762917431 ** get_address_of__inputBallMinArea_227() { return &____inputBallMinArea_227; }
	inline void set__inputBallMinArea_227(InputField_t3762917431 * value)
	{
		____inputBallMinArea_227 = value;
		Il2CppCodeGenWriteBarrier((&____inputBallMinArea_227), value);
	}

	inline static int32_t get_offset_of__inputPlayerNumToStartGame_228() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ____inputPlayerNumToStartGame_228)); }
	inline InputField_t3762917431 * get__inputPlayerNumToStartGame_228() const { return ____inputPlayerNumToStartGame_228; }
	inline InputField_t3762917431 ** get_address_of__inputPlayerNumToStartGame_228() { return &____inputPlayerNumToStartGame_228; }
	inline void set__inputPlayerNumToStartGame_228(InputField_t3762917431 * value)
	{
		____inputPlayerNumToStartGame_228 = value;
		Il2CppCodeGenWriteBarrier((&____inputPlayerNumToStartGame_228), value);
	}

	inline static int32_t get_offset_of_m_fEjectParamA_230() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fEjectParamA_230)); }
	inline float get_m_fEjectParamA_230() const { return ___m_fEjectParamA_230; }
	inline float* get_address_of_m_fEjectParamA_230() { return &___m_fEjectParamA_230; }
	inline void set_m_fEjectParamA_230(float value)
	{
		___m_fEjectParamA_230 = value;
	}

	inline static int32_t get_offset_of_m_fEjectParamB_231() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fEjectParamB_231)); }
	inline float get_m_fEjectParamB_231() const { return ___m_fEjectParamB_231; }
	inline float* get_address_of_m_fEjectParamB_231() { return &___m_fEjectParamB_231; }
	inline void set_m_fEjectParamB_231(float value)
	{
		___m_fEjectParamB_231 = value;
	}

	inline static int32_t get_offset_of_m_fEjectParamC_232() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fEjectParamC_232)); }
	inline float get_m_fEjectParamC_232() const { return ___m_fEjectParamC_232; }
	inline float* get_address_of_m_fEjectParamC_232() { return &___m_fEjectParamC_232; }
	inline void set_m_fEjectParamC_232(float value)
	{
		___m_fEjectParamC_232 = value;
	}

	inline static int32_t get_offset_of_m_fEjectParamX_233() { return static_cast<int32_t>(offsetof(MapEditor_t878680970, ___m_fEjectParamX_233)); }
	inline float get_m_fEjectParamX_233() const { return ___m_fEjectParamX_233; }
	inline float* get_address_of_m_fEjectParamX_233() { return &___m_fEjectParamX_233; }
	inline void set_m_fEjectParamX_233(float value)
	{
		___m_fEjectParamX_233 = value;
	}
};

struct MapEditor_t878680970_StaticFields
{
public:
	// MapEditor MapEditor::s_Instance
	MapEditor_t878680970 * ___s_Instance_2;
	// MapEditor/eEatMode MapEditor::m_eEatMode
	int32_t ___m_eEatMode_5;
	// UnityEngine.Vector3 MapEditor::s_vecTempPos
	Vector3_t3722313464  ___s_vecTempPos_36;
	// System.Collections.Generic.Dictionary`2<System.Int32,Thorn/ColorThornParam> MapEditor::m_dicColorThornParam
	Dictionary_2_t1249762 * ___m_dicColorThornParam_176;
	// System.String MapEditor::s_szRealRoomName
	String_t* ___s_szRealRoomName_210;
	// System.Xml.XmlDocument MapEditor::s_xmldocMapData
	XmlDocument_t2837193595 * ___s_xmldocMapData_211;
	// System.String MapEditor::m_szCurRoom
	String_t* ___m_szCurRoom_221;
	// System.String MapEditor::m_szMainPlayerName
	String_t* ___m_szMainPlayerName_222;
	// System.Int32 MapEditor::m_nPlayerNumToStartGame
	int32_t ___m_nPlayerNumToStartGame_229;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___s_Instance_2)); }
	inline MapEditor_t878680970 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline MapEditor_t878680970 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(MapEditor_t878680970 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_m_eEatMode_5() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___m_eEatMode_5)); }
	inline int32_t get_m_eEatMode_5() const { return ___m_eEatMode_5; }
	inline int32_t* get_address_of_m_eEatMode_5() { return &___m_eEatMode_5; }
	inline void set_m_eEatMode_5(int32_t value)
	{
		___m_eEatMode_5 = value;
	}

	inline static int32_t get_offset_of_s_vecTempPos_36() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___s_vecTempPos_36)); }
	inline Vector3_t3722313464  get_s_vecTempPos_36() const { return ___s_vecTempPos_36; }
	inline Vector3_t3722313464 * get_address_of_s_vecTempPos_36() { return &___s_vecTempPos_36; }
	inline void set_s_vecTempPos_36(Vector3_t3722313464  value)
	{
		___s_vecTempPos_36 = value;
	}

	inline static int32_t get_offset_of_m_dicColorThornParam_176() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___m_dicColorThornParam_176)); }
	inline Dictionary_2_t1249762 * get_m_dicColorThornParam_176() const { return ___m_dicColorThornParam_176; }
	inline Dictionary_2_t1249762 ** get_address_of_m_dicColorThornParam_176() { return &___m_dicColorThornParam_176; }
	inline void set_m_dicColorThornParam_176(Dictionary_2_t1249762 * value)
	{
		___m_dicColorThornParam_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicColorThornParam_176), value);
	}

	inline static int32_t get_offset_of_s_szRealRoomName_210() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___s_szRealRoomName_210)); }
	inline String_t* get_s_szRealRoomName_210() const { return ___s_szRealRoomName_210; }
	inline String_t** get_address_of_s_szRealRoomName_210() { return &___s_szRealRoomName_210; }
	inline void set_s_szRealRoomName_210(String_t* value)
	{
		___s_szRealRoomName_210 = value;
		Il2CppCodeGenWriteBarrier((&___s_szRealRoomName_210), value);
	}

	inline static int32_t get_offset_of_s_xmldocMapData_211() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___s_xmldocMapData_211)); }
	inline XmlDocument_t2837193595 * get_s_xmldocMapData_211() const { return ___s_xmldocMapData_211; }
	inline XmlDocument_t2837193595 ** get_address_of_s_xmldocMapData_211() { return &___s_xmldocMapData_211; }
	inline void set_s_xmldocMapData_211(XmlDocument_t2837193595 * value)
	{
		___s_xmldocMapData_211 = value;
		Il2CppCodeGenWriteBarrier((&___s_xmldocMapData_211), value);
	}

	inline static int32_t get_offset_of_m_szCurRoom_221() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___m_szCurRoom_221)); }
	inline String_t* get_m_szCurRoom_221() const { return ___m_szCurRoom_221; }
	inline String_t** get_address_of_m_szCurRoom_221() { return &___m_szCurRoom_221; }
	inline void set_m_szCurRoom_221(String_t* value)
	{
		___m_szCurRoom_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCurRoom_221), value);
	}

	inline static int32_t get_offset_of_m_szMainPlayerName_222() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___m_szMainPlayerName_222)); }
	inline String_t* get_m_szMainPlayerName_222() const { return ___m_szMainPlayerName_222; }
	inline String_t** get_address_of_m_szMainPlayerName_222() { return &___m_szMainPlayerName_222; }
	inline void set_m_szMainPlayerName_222(String_t* value)
	{
		___m_szMainPlayerName_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_szMainPlayerName_222), value);
	}

	inline static int32_t get_offset_of_m_nPlayerNumToStartGame_229() { return static_cast<int32_t>(offsetof(MapEditor_t878680970_StaticFields, ___m_nPlayerNumToStartGame_229)); }
	inline int32_t get_m_nPlayerNumToStartGame_229() const { return ___m_nPlayerNumToStartGame_229; }
	inline int32_t* get_address_of_m_nPlayerNumToStartGame_229() { return &___m_nPlayerNumToStartGame_229; }
	inline void set_m_nPlayerNumToStartGame_229(int32_t value)
	{
		___m_nPlayerNumToStartGame_229 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPEDITOR_T878680970_H
#ifndef MSGBOX_T1665650521_H
#define MSGBOX_T1665650521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MsgBox
struct  MsgBox_t1665650521  : public MonoBehaviour_t3962482529
{
public:
	// MsgBox/eMsgBoxType MsgBox::m_eType
	int32_t ___m_eType_2;
	// UnityEngine.UI.Button MsgBox::_btnOk
	Button_t4055032469 * ____btnOk_4;
	// UnityEngine.UI.Button MsgBox::_btnYes
	Button_t4055032469 * ____btnYes_5;
	// UnityEngine.UI.Button MsgBox::_btnNo
	Button_t4055032469 * ____btnNo_6;
	// UnityEngine.UI.Text MsgBox::_txtContent
	Text_t1901882714 * ____txtContent_7;
	// MsgBox/DelegateMethod_OnClickButton MsgBox::delegateMethodOnClickButton
	DelegateMethod_OnClickButton_t3139999742 * ___delegateMethodOnClickButton_8;
	// System.Int32 MsgBox::m_nParam
	int32_t ___m_nParam_9;

public:
	inline static int32_t get_offset_of_m_eType_2() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ___m_eType_2)); }
	inline int32_t get_m_eType_2() const { return ___m_eType_2; }
	inline int32_t* get_address_of_m_eType_2() { return &___m_eType_2; }
	inline void set_m_eType_2(int32_t value)
	{
		___m_eType_2 = value;
	}

	inline static int32_t get_offset_of__btnOk_4() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ____btnOk_4)); }
	inline Button_t4055032469 * get__btnOk_4() const { return ____btnOk_4; }
	inline Button_t4055032469 ** get_address_of__btnOk_4() { return &____btnOk_4; }
	inline void set__btnOk_4(Button_t4055032469 * value)
	{
		____btnOk_4 = value;
		Il2CppCodeGenWriteBarrier((&____btnOk_4), value);
	}

	inline static int32_t get_offset_of__btnYes_5() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ____btnYes_5)); }
	inline Button_t4055032469 * get__btnYes_5() const { return ____btnYes_5; }
	inline Button_t4055032469 ** get_address_of__btnYes_5() { return &____btnYes_5; }
	inline void set__btnYes_5(Button_t4055032469 * value)
	{
		____btnYes_5 = value;
		Il2CppCodeGenWriteBarrier((&____btnYes_5), value);
	}

	inline static int32_t get_offset_of__btnNo_6() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ____btnNo_6)); }
	inline Button_t4055032469 * get__btnNo_6() const { return ____btnNo_6; }
	inline Button_t4055032469 ** get_address_of__btnNo_6() { return &____btnNo_6; }
	inline void set__btnNo_6(Button_t4055032469 * value)
	{
		____btnNo_6 = value;
		Il2CppCodeGenWriteBarrier((&____btnNo_6), value);
	}

	inline static int32_t get_offset_of__txtContent_7() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ____txtContent_7)); }
	inline Text_t1901882714 * get__txtContent_7() const { return ____txtContent_7; }
	inline Text_t1901882714 ** get_address_of__txtContent_7() { return &____txtContent_7; }
	inline void set__txtContent_7(Text_t1901882714 * value)
	{
		____txtContent_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_7), value);
	}

	inline static int32_t get_offset_of_delegateMethodOnClickButton_8() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ___delegateMethodOnClickButton_8)); }
	inline DelegateMethod_OnClickButton_t3139999742 * get_delegateMethodOnClickButton_8() const { return ___delegateMethodOnClickButton_8; }
	inline DelegateMethod_OnClickButton_t3139999742 ** get_address_of_delegateMethodOnClickButton_8() { return &___delegateMethodOnClickButton_8; }
	inline void set_delegateMethodOnClickButton_8(DelegateMethod_OnClickButton_t3139999742 * value)
	{
		___delegateMethodOnClickButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___delegateMethodOnClickButton_8), value);
	}

	inline static int32_t get_offset_of_m_nParam_9() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521, ___m_nParam_9)); }
	inline int32_t get_m_nParam_9() const { return ___m_nParam_9; }
	inline int32_t* get_address_of_m_nParam_9() { return &___m_nParam_9; }
	inline void set_m_nParam_9(int32_t value)
	{
		___m_nParam_9 = value;
	}
};

struct MsgBox_t1665650521_StaticFields
{
public:
	// MsgBox MsgBox::s_Instance
	MsgBox_t1665650521 * ___s_Instance_3;

public:
	inline static int32_t get_offset_of_s_Instance_3() { return static_cast<int32_t>(offsetof(MsgBox_t1665650521_StaticFields, ___s_Instance_3)); }
	inline MsgBox_t1665650521 * get_s_Instance_3() const { return ___s_Instance_3; }
	inline MsgBox_t1665650521 ** get_address_of_s_Instance_3() { return &___s_Instance_3; }
	inline void set_s_Instance_3(MsgBox_t1665650521 * value)
	{
		___s_Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MSGBOX_T1665650521_H
#ifndef ETCINPUT_T19426123_H
#define ETCINPUT_T19426123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCInput
struct  ETCInput_t19426123  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ETCAxis> ETCInput::axes
	Dictionary_2_t3891096642 * ___axes_3;
	// System.Collections.Generic.Dictionary`2<System.String,ETCBase> ETCInput::controls
	Dictionary_2_t4120536841 * ___controls_4;

public:
	inline static int32_t get_offset_of_axes_3() { return static_cast<int32_t>(offsetof(ETCInput_t19426123, ___axes_3)); }
	inline Dictionary_2_t3891096642 * get_axes_3() const { return ___axes_3; }
	inline Dictionary_2_t3891096642 ** get_address_of_axes_3() { return &___axes_3; }
	inline void set_axes_3(Dictionary_2_t3891096642 * value)
	{
		___axes_3 = value;
		Il2CppCodeGenWriteBarrier((&___axes_3), value);
	}

	inline static int32_t get_offset_of_controls_4() { return static_cast<int32_t>(offsetof(ETCInput_t19426123, ___controls_4)); }
	inline Dictionary_2_t4120536841 * get_controls_4() const { return ___controls_4; }
	inline Dictionary_2_t4120536841 ** get_address_of_controls_4() { return &___controls_4; }
	inline void set_controls_4(Dictionary_2_t4120536841 * value)
	{
		___controls_4 = value;
		Il2CppCodeGenWriteBarrier((&___controls_4), value);
	}
};

struct ETCInput_t19426123_StaticFields
{
public:
	// ETCInput ETCInput::_instance
	ETCInput_t19426123 * ____instance_2;
	// ETCBase ETCInput::control
	ETCBase_t40313246 * ___control_5;
	// ETCAxis ETCInput::axis
	ETCAxis_t4105840343 * ___axis_6;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(ETCInput_t19426123_StaticFields, ____instance_2)); }
	inline ETCInput_t19426123 * get__instance_2() const { return ____instance_2; }
	inline ETCInput_t19426123 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ETCInput_t19426123 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of_control_5() { return static_cast<int32_t>(offsetof(ETCInput_t19426123_StaticFields, ___control_5)); }
	inline ETCBase_t40313246 * get_control_5() const { return ___control_5; }
	inline ETCBase_t40313246 ** get_address_of_control_5() { return &___control_5; }
	inline void set_control_5(ETCBase_t40313246 * value)
	{
		___control_5 = value;
		Il2CppCodeGenWriteBarrier((&___control_5), value);
	}

	inline static int32_t get_offset_of_axis_6() { return static_cast<int32_t>(offsetof(ETCInput_t19426123_StaticFields, ___axis_6)); }
	inline ETCAxis_t4105840343 * get_axis_6() const { return ___axis_6; }
	inline ETCAxis_t4105840343 ** get_address_of_axis_6() { return &___axis_6; }
	inline void set_axis_6(ETCAxis_t4105840343 * value)
	{
		___axis_6 = value;
		Il2CppCodeGenWriteBarrier((&___axis_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCINPUT_T19426123_H
#ifndef ETCBASE_T40313246_H
#define ETCBASE_T40313246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase
struct  ETCBase_t40313246  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform ETCBase::cachedRectTransform
	RectTransform_t3704657025 * ___cachedRectTransform_2;
	// UnityEngine.Canvas ETCBase::cachedRootCanvas
	Canvas_t3310196443 * ___cachedRootCanvas_3;
	// System.Boolean ETCBase::isUnregisterAtDisable
	bool ___isUnregisterAtDisable_4;
	// System.Boolean ETCBase::visibleAtStart
	bool ___visibleAtStart_5;
	// System.Boolean ETCBase::activatedAtStart
	bool ___activatedAtStart_6;
	// ETCBase/RectAnchor ETCBase::_anchor
	int32_t ____anchor_7;
	// UnityEngine.Vector2 ETCBase::_anchorOffet
	Vector2_t2156229523  ____anchorOffet_8;
	// System.Boolean ETCBase::_visible
	bool ____visible_9;
	// System.Boolean ETCBase::_activated
	bool ____activated_10;
	// System.Boolean ETCBase::enableCamera
	bool ___enableCamera_11;
	// ETCBase/CameraMode ETCBase::cameraMode
	int32_t ___cameraMode_12;
	// System.String ETCBase::camTargetTag
	String_t* ___camTargetTag_13;
	// System.Boolean ETCBase::autoLinkTagCam
	bool ___autoLinkTagCam_14;
	// System.String ETCBase::autoCamTag
	String_t* ___autoCamTag_15;
	// UnityEngine.Transform ETCBase::cameraTransform
	Transform_t3600365921 * ___cameraTransform_16;
	// ETCBase/CameraTargetMode ETCBase::cameraTargetMode
	int32_t ___cameraTargetMode_17;
	// System.Boolean ETCBase::enableWallDetection
	bool ___enableWallDetection_18;
	// UnityEngine.LayerMask ETCBase::wallLayer
	LayerMask_t3493934918  ___wallLayer_19;
	// UnityEngine.Transform ETCBase::cameraLookAt
	Transform_t3600365921 * ___cameraLookAt_20;
	// UnityEngine.CharacterController ETCBase::cameraLookAtCC
	CharacterController_t1138636865 * ___cameraLookAtCC_21;
	// UnityEngine.Vector3 ETCBase::followOffset
	Vector3_t3722313464  ___followOffset_22;
	// System.Single ETCBase::followDistance
	float ___followDistance_23;
	// System.Single ETCBase::followHeight
	float ___followHeight_24;
	// System.Single ETCBase::followRotationDamping
	float ___followRotationDamping_25;
	// System.Single ETCBase::followHeightDamping
	float ___followHeightDamping_26;
	// System.Int32 ETCBase::pointId
	int32_t ___pointId_27;
	// System.Boolean ETCBase::enableKeySimulation
	bool ___enableKeySimulation_28;
	// System.Boolean ETCBase::allowSimulationStandalone
	bool ___allowSimulationStandalone_29;
	// System.Boolean ETCBase::visibleOnStandalone
	bool ___visibleOnStandalone_30;
	// ETCBase/DPadAxis ETCBase::dPadAxisCount
	int32_t ___dPadAxisCount_31;
	// System.Boolean ETCBase::useFixedUpdate
	bool ___useFixedUpdate_32;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> ETCBase::uiRaycastResultCache
	List_1_t537414295 * ___uiRaycastResultCache_33;
	// UnityEngine.EventSystems.PointerEventData ETCBase::uiPointerEventData
	PointerEventData_t3807901092 * ___uiPointerEventData_34;
	// UnityEngine.EventSystems.EventSystem ETCBase::uiEventSystem
	EventSystem_t1003666588 * ___uiEventSystem_35;
	// System.Boolean ETCBase::isOnDrag
	bool ___isOnDrag_36;
	// System.Boolean ETCBase::isSwipeIn
	bool ___isSwipeIn_37;
	// System.Boolean ETCBase::isSwipeOut
	bool ___isSwipeOut_38;
	// System.Boolean ETCBase::showPSInspector
	bool ___showPSInspector_39;
	// System.Boolean ETCBase::showSpriteInspector
	bool ___showSpriteInspector_40;
	// System.Boolean ETCBase::showEventInspector
	bool ___showEventInspector_41;
	// System.Boolean ETCBase::showBehaviourInspector
	bool ___showBehaviourInspector_42;
	// System.Boolean ETCBase::showAxesInspector
	bool ___showAxesInspector_43;
	// System.Boolean ETCBase::showTouchEventInspector
	bool ___showTouchEventInspector_44;
	// System.Boolean ETCBase::showDownEventInspector
	bool ___showDownEventInspector_45;
	// System.Boolean ETCBase::showPressEventInspector
	bool ___showPressEventInspector_46;
	// System.Boolean ETCBase::showCameraInspector
	bool ___showCameraInspector_47;

public:
	inline static int32_t get_offset_of_cachedRectTransform_2() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cachedRectTransform_2)); }
	inline RectTransform_t3704657025 * get_cachedRectTransform_2() const { return ___cachedRectTransform_2; }
	inline RectTransform_t3704657025 ** get_address_of_cachedRectTransform_2() { return &___cachedRectTransform_2; }
	inline void set_cachedRectTransform_2(RectTransform_t3704657025 * value)
	{
		___cachedRectTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_2), value);
	}

	inline static int32_t get_offset_of_cachedRootCanvas_3() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cachedRootCanvas_3)); }
	inline Canvas_t3310196443 * get_cachedRootCanvas_3() const { return ___cachedRootCanvas_3; }
	inline Canvas_t3310196443 ** get_address_of_cachedRootCanvas_3() { return &___cachedRootCanvas_3; }
	inline void set_cachedRootCanvas_3(Canvas_t3310196443 * value)
	{
		___cachedRootCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRootCanvas_3), value);
	}

	inline static int32_t get_offset_of_isUnregisterAtDisable_4() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isUnregisterAtDisable_4)); }
	inline bool get_isUnregisterAtDisable_4() const { return ___isUnregisterAtDisable_4; }
	inline bool* get_address_of_isUnregisterAtDisable_4() { return &___isUnregisterAtDisable_4; }
	inline void set_isUnregisterAtDisable_4(bool value)
	{
		___isUnregisterAtDisable_4 = value;
	}

	inline static int32_t get_offset_of_visibleAtStart_5() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___visibleAtStart_5)); }
	inline bool get_visibleAtStart_5() const { return ___visibleAtStart_5; }
	inline bool* get_address_of_visibleAtStart_5() { return &___visibleAtStart_5; }
	inline void set_visibleAtStart_5(bool value)
	{
		___visibleAtStart_5 = value;
	}

	inline static int32_t get_offset_of_activatedAtStart_6() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___activatedAtStart_6)); }
	inline bool get_activatedAtStart_6() const { return ___activatedAtStart_6; }
	inline bool* get_address_of_activatedAtStart_6() { return &___activatedAtStart_6; }
	inline void set_activatedAtStart_6(bool value)
	{
		___activatedAtStart_6 = value;
	}

	inline static int32_t get_offset_of__anchor_7() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____anchor_7)); }
	inline int32_t get__anchor_7() const { return ____anchor_7; }
	inline int32_t* get_address_of__anchor_7() { return &____anchor_7; }
	inline void set__anchor_7(int32_t value)
	{
		____anchor_7 = value;
	}

	inline static int32_t get_offset_of__anchorOffet_8() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____anchorOffet_8)); }
	inline Vector2_t2156229523  get__anchorOffet_8() const { return ____anchorOffet_8; }
	inline Vector2_t2156229523 * get_address_of__anchorOffet_8() { return &____anchorOffet_8; }
	inline void set__anchorOffet_8(Vector2_t2156229523  value)
	{
		____anchorOffet_8 = value;
	}

	inline static int32_t get_offset_of__visible_9() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____visible_9)); }
	inline bool get__visible_9() const { return ____visible_9; }
	inline bool* get_address_of__visible_9() { return &____visible_9; }
	inline void set__visible_9(bool value)
	{
		____visible_9 = value;
	}

	inline static int32_t get_offset_of__activated_10() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____activated_10)); }
	inline bool get__activated_10() const { return ____activated_10; }
	inline bool* get_address_of__activated_10() { return &____activated_10; }
	inline void set__activated_10(bool value)
	{
		____activated_10 = value;
	}

	inline static int32_t get_offset_of_enableCamera_11() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___enableCamera_11)); }
	inline bool get_enableCamera_11() const { return ___enableCamera_11; }
	inline bool* get_address_of_enableCamera_11() { return &___enableCamera_11; }
	inline void set_enableCamera_11(bool value)
	{
		___enableCamera_11 = value;
	}

	inline static int32_t get_offset_of_cameraMode_12() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraMode_12)); }
	inline int32_t get_cameraMode_12() const { return ___cameraMode_12; }
	inline int32_t* get_address_of_cameraMode_12() { return &___cameraMode_12; }
	inline void set_cameraMode_12(int32_t value)
	{
		___cameraMode_12 = value;
	}

	inline static int32_t get_offset_of_camTargetTag_13() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___camTargetTag_13)); }
	inline String_t* get_camTargetTag_13() const { return ___camTargetTag_13; }
	inline String_t** get_address_of_camTargetTag_13() { return &___camTargetTag_13; }
	inline void set_camTargetTag_13(String_t* value)
	{
		___camTargetTag_13 = value;
		Il2CppCodeGenWriteBarrier((&___camTargetTag_13), value);
	}

	inline static int32_t get_offset_of_autoLinkTagCam_14() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___autoLinkTagCam_14)); }
	inline bool get_autoLinkTagCam_14() const { return ___autoLinkTagCam_14; }
	inline bool* get_address_of_autoLinkTagCam_14() { return &___autoLinkTagCam_14; }
	inline void set_autoLinkTagCam_14(bool value)
	{
		___autoLinkTagCam_14 = value;
	}

	inline static int32_t get_offset_of_autoCamTag_15() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___autoCamTag_15)); }
	inline String_t* get_autoCamTag_15() const { return ___autoCamTag_15; }
	inline String_t** get_address_of_autoCamTag_15() { return &___autoCamTag_15; }
	inline void set_autoCamTag_15(String_t* value)
	{
		___autoCamTag_15 = value;
		Il2CppCodeGenWriteBarrier((&___autoCamTag_15), value);
	}

	inline static int32_t get_offset_of_cameraTransform_16() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraTransform_16)); }
	inline Transform_t3600365921 * get_cameraTransform_16() const { return ___cameraTransform_16; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_16() { return &___cameraTransform_16; }
	inline void set_cameraTransform_16(Transform_t3600365921 * value)
	{
		___cameraTransform_16 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_16), value);
	}

	inline static int32_t get_offset_of_cameraTargetMode_17() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraTargetMode_17)); }
	inline int32_t get_cameraTargetMode_17() const { return ___cameraTargetMode_17; }
	inline int32_t* get_address_of_cameraTargetMode_17() { return &___cameraTargetMode_17; }
	inline void set_cameraTargetMode_17(int32_t value)
	{
		___cameraTargetMode_17 = value;
	}

	inline static int32_t get_offset_of_enableWallDetection_18() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___enableWallDetection_18)); }
	inline bool get_enableWallDetection_18() const { return ___enableWallDetection_18; }
	inline bool* get_address_of_enableWallDetection_18() { return &___enableWallDetection_18; }
	inline void set_enableWallDetection_18(bool value)
	{
		___enableWallDetection_18 = value;
	}

	inline static int32_t get_offset_of_wallLayer_19() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___wallLayer_19)); }
	inline LayerMask_t3493934918  get_wallLayer_19() const { return ___wallLayer_19; }
	inline LayerMask_t3493934918 * get_address_of_wallLayer_19() { return &___wallLayer_19; }
	inline void set_wallLayer_19(LayerMask_t3493934918  value)
	{
		___wallLayer_19 = value;
	}

	inline static int32_t get_offset_of_cameraLookAt_20() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraLookAt_20)); }
	inline Transform_t3600365921 * get_cameraLookAt_20() const { return ___cameraLookAt_20; }
	inline Transform_t3600365921 ** get_address_of_cameraLookAt_20() { return &___cameraLookAt_20; }
	inline void set_cameraLookAt_20(Transform_t3600365921 * value)
	{
		___cameraLookAt_20 = value;
		Il2CppCodeGenWriteBarrier((&___cameraLookAt_20), value);
	}

	inline static int32_t get_offset_of_cameraLookAtCC_21() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraLookAtCC_21)); }
	inline CharacterController_t1138636865 * get_cameraLookAtCC_21() const { return ___cameraLookAtCC_21; }
	inline CharacterController_t1138636865 ** get_address_of_cameraLookAtCC_21() { return &___cameraLookAtCC_21; }
	inline void set_cameraLookAtCC_21(CharacterController_t1138636865 * value)
	{
		___cameraLookAtCC_21 = value;
		Il2CppCodeGenWriteBarrier((&___cameraLookAtCC_21), value);
	}

	inline static int32_t get_offset_of_followOffset_22() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followOffset_22)); }
	inline Vector3_t3722313464  get_followOffset_22() const { return ___followOffset_22; }
	inline Vector3_t3722313464 * get_address_of_followOffset_22() { return &___followOffset_22; }
	inline void set_followOffset_22(Vector3_t3722313464  value)
	{
		___followOffset_22 = value;
	}

	inline static int32_t get_offset_of_followDistance_23() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followDistance_23)); }
	inline float get_followDistance_23() const { return ___followDistance_23; }
	inline float* get_address_of_followDistance_23() { return &___followDistance_23; }
	inline void set_followDistance_23(float value)
	{
		___followDistance_23 = value;
	}

	inline static int32_t get_offset_of_followHeight_24() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followHeight_24)); }
	inline float get_followHeight_24() const { return ___followHeight_24; }
	inline float* get_address_of_followHeight_24() { return &___followHeight_24; }
	inline void set_followHeight_24(float value)
	{
		___followHeight_24 = value;
	}

	inline static int32_t get_offset_of_followRotationDamping_25() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followRotationDamping_25)); }
	inline float get_followRotationDamping_25() const { return ___followRotationDamping_25; }
	inline float* get_address_of_followRotationDamping_25() { return &___followRotationDamping_25; }
	inline void set_followRotationDamping_25(float value)
	{
		___followRotationDamping_25 = value;
	}

	inline static int32_t get_offset_of_followHeightDamping_26() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followHeightDamping_26)); }
	inline float get_followHeightDamping_26() const { return ___followHeightDamping_26; }
	inline float* get_address_of_followHeightDamping_26() { return &___followHeightDamping_26; }
	inline void set_followHeightDamping_26(float value)
	{
		___followHeightDamping_26 = value;
	}

	inline static int32_t get_offset_of_pointId_27() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___pointId_27)); }
	inline int32_t get_pointId_27() const { return ___pointId_27; }
	inline int32_t* get_address_of_pointId_27() { return &___pointId_27; }
	inline void set_pointId_27(int32_t value)
	{
		___pointId_27 = value;
	}

	inline static int32_t get_offset_of_enableKeySimulation_28() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___enableKeySimulation_28)); }
	inline bool get_enableKeySimulation_28() const { return ___enableKeySimulation_28; }
	inline bool* get_address_of_enableKeySimulation_28() { return &___enableKeySimulation_28; }
	inline void set_enableKeySimulation_28(bool value)
	{
		___enableKeySimulation_28 = value;
	}

	inline static int32_t get_offset_of_allowSimulationStandalone_29() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___allowSimulationStandalone_29)); }
	inline bool get_allowSimulationStandalone_29() const { return ___allowSimulationStandalone_29; }
	inline bool* get_address_of_allowSimulationStandalone_29() { return &___allowSimulationStandalone_29; }
	inline void set_allowSimulationStandalone_29(bool value)
	{
		___allowSimulationStandalone_29 = value;
	}

	inline static int32_t get_offset_of_visibleOnStandalone_30() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___visibleOnStandalone_30)); }
	inline bool get_visibleOnStandalone_30() const { return ___visibleOnStandalone_30; }
	inline bool* get_address_of_visibleOnStandalone_30() { return &___visibleOnStandalone_30; }
	inline void set_visibleOnStandalone_30(bool value)
	{
		___visibleOnStandalone_30 = value;
	}

	inline static int32_t get_offset_of_dPadAxisCount_31() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___dPadAxisCount_31)); }
	inline int32_t get_dPadAxisCount_31() const { return ___dPadAxisCount_31; }
	inline int32_t* get_address_of_dPadAxisCount_31() { return &___dPadAxisCount_31; }
	inline void set_dPadAxisCount_31(int32_t value)
	{
		___dPadAxisCount_31 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_32() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___useFixedUpdate_32)); }
	inline bool get_useFixedUpdate_32() const { return ___useFixedUpdate_32; }
	inline bool* get_address_of_useFixedUpdate_32() { return &___useFixedUpdate_32; }
	inline void set_useFixedUpdate_32(bool value)
	{
		___useFixedUpdate_32 = value;
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_33() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___uiRaycastResultCache_33)); }
	inline List_1_t537414295 * get_uiRaycastResultCache_33() const { return ___uiRaycastResultCache_33; }
	inline List_1_t537414295 ** get_address_of_uiRaycastResultCache_33() { return &___uiRaycastResultCache_33; }
	inline void set_uiRaycastResultCache_33(List_1_t537414295 * value)
	{
		___uiRaycastResultCache_33 = value;
		Il2CppCodeGenWriteBarrier((&___uiRaycastResultCache_33), value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_34() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___uiPointerEventData_34)); }
	inline PointerEventData_t3807901092 * get_uiPointerEventData_34() const { return ___uiPointerEventData_34; }
	inline PointerEventData_t3807901092 ** get_address_of_uiPointerEventData_34() { return &___uiPointerEventData_34; }
	inline void set_uiPointerEventData_34(PointerEventData_t3807901092 * value)
	{
		___uiPointerEventData_34 = value;
		Il2CppCodeGenWriteBarrier((&___uiPointerEventData_34), value);
	}

	inline static int32_t get_offset_of_uiEventSystem_35() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___uiEventSystem_35)); }
	inline EventSystem_t1003666588 * get_uiEventSystem_35() const { return ___uiEventSystem_35; }
	inline EventSystem_t1003666588 ** get_address_of_uiEventSystem_35() { return &___uiEventSystem_35; }
	inline void set_uiEventSystem_35(EventSystem_t1003666588 * value)
	{
		___uiEventSystem_35 = value;
		Il2CppCodeGenWriteBarrier((&___uiEventSystem_35), value);
	}

	inline static int32_t get_offset_of_isOnDrag_36() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isOnDrag_36)); }
	inline bool get_isOnDrag_36() const { return ___isOnDrag_36; }
	inline bool* get_address_of_isOnDrag_36() { return &___isOnDrag_36; }
	inline void set_isOnDrag_36(bool value)
	{
		___isOnDrag_36 = value;
	}

	inline static int32_t get_offset_of_isSwipeIn_37() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isSwipeIn_37)); }
	inline bool get_isSwipeIn_37() const { return ___isSwipeIn_37; }
	inline bool* get_address_of_isSwipeIn_37() { return &___isSwipeIn_37; }
	inline void set_isSwipeIn_37(bool value)
	{
		___isSwipeIn_37 = value;
	}

	inline static int32_t get_offset_of_isSwipeOut_38() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isSwipeOut_38)); }
	inline bool get_isSwipeOut_38() const { return ___isSwipeOut_38; }
	inline bool* get_address_of_isSwipeOut_38() { return &___isSwipeOut_38; }
	inline void set_isSwipeOut_38(bool value)
	{
		___isSwipeOut_38 = value;
	}

	inline static int32_t get_offset_of_showPSInspector_39() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showPSInspector_39)); }
	inline bool get_showPSInspector_39() const { return ___showPSInspector_39; }
	inline bool* get_address_of_showPSInspector_39() { return &___showPSInspector_39; }
	inline void set_showPSInspector_39(bool value)
	{
		___showPSInspector_39 = value;
	}

	inline static int32_t get_offset_of_showSpriteInspector_40() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showSpriteInspector_40)); }
	inline bool get_showSpriteInspector_40() const { return ___showSpriteInspector_40; }
	inline bool* get_address_of_showSpriteInspector_40() { return &___showSpriteInspector_40; }
	inline void set_showSpriteInspector_40(bool value)
	{
		___showSpriteInspector_40 = value;
	}

	inline static int32_t get_offset_of_showEventInspector_41() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showEventInspector_41)); }
	inline bool get_showEventInspector_41() const { return ___showEventInspector_41; }
	inline bool* get_address_of_showEventInspector_41() { return &___showEventInspector_41; }
	inline void set_showEventInspector_41(bool value)
	{
		___showEventInspector_41 = value;
	}

	inline static int32_t get_offset_of_showBehaviourInspector_42() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showBehaviourInspector_42)); }
	inline bool get_showBehaviourInspector_42() const { return ___showBehaviourInspector_42; }
	inline bool* get_address_of_showBehaviourInspector_42() { return &___showBehaviourInspector_42; }
	inline void set_showBehaviourInspector_42(bool value)
	{
		___showBehaviourInspector_42 = value;
	}

	inline static int32_t get_offset_of_showAxesInspector_43() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showAxesInspector_43)); }
	inline bool get_showAxesInspector_43() const { return ___showAxesInspector_43; }
	inline bool* get_address_of_showAxesInspector_43() { return &___showAxesInspector_43; }
	inline void set_showAxesInspector_43(bool value)
	{
		___showAxesInspector_43 = value;
	}

	inline static int32_t get_offset_of_showTouchEventInspector_44() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showTouchEventInspector_44)); }
	inline bool get_showTouchEventInspector_44() const { return ___showTouchEventInspector_44; }
	inline bool* get_address_of_showTouchEventInspector_44() { return &___showTouchEventInspector_44; }
	inline void set_showTouchEventInspector_44(bool value)
	{
		___showTouchEventInspector_44 = value;
	}

	inline static int32_t get_offset_of_showDownEventInspector_45() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showDownEventInspector_45)); }
	inline bool get_showDownEventInspector_45() const { return ___showDownEventInspector_45; }
	inline bool* get_address_of_showDownEventInspector_45() { return &___showDownEventInspector_45; }
	inline void set_showDownEventInspector_45(bool value)
	{
		___showDownEventInspector_45 = value;
	}

	inline static int32_t get_offset_of_showPressEventInspector_46() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showPressEventInspector_46)); }
	inline bool get_showPressEventInspector_46() const { return ___showPressEventInspector_46; }
	inline bool* get_address_of_showPressEventInspector_46() { return &___showPressEventInspector_46; }
	inline void set_showPressEventInspector_46(bool value)
	{
		___showPressEventInspector_46 = value;
	}

	inline static int32_t get_offset_of_showCameraInspector_47() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showCameraInspector_47)); }
	inline bool get_showCameraInspector_47() const { return ___showCameraInspector_47; }
	inline bool* get_address_of_showCameraInspector_47() { return &___showCameraInspector_47; }
	inline void set_showCameraInspector_47(bool value)
	{
		___showCameraInspector_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCBASE_T40313246_H
#ifndef LISTITEM_T4166427101_H
#define LISTITEM_T4166427101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ListItem
struct  ListItem_t4166427101  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ListItem::_txtContent
	Text_t1901882714 * ____txtContent_2;
	// UnityEngine.UI.Button ListItem::_btnSelect
	Button_t4055032469 * ____btnSelect_3;
	// System.String ListItem::m_szContent
	String_t* ___m_szContent_4;
	// System.Int32 ListItem::m_nIndex
	int32_t ___m_nIndex_5;
	// ComoList ListItem::_MyList
	ComoList_t2152284863 * ____MyList_6;

public:
	inline static int32_t get_offset_of__txtContent_2() { return static_cast<int32_t>(offsetof(ListItem_t4166427101, ____txtContent_2)); }
	inline Text_t1901882714 * get__txtContent_2() const { return ____txtContent_2; }
	inline Text_t1901882714 ** get_address_of__txtContent_2() { return &____txtContent_2; }
	inline void set__txtContent_2(Text_t1901882714 * value)
	{
		____txtContent_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_2), value);
	}

	inline static int32_t get_offset_of__btnSelect_3() { return static_cast<int32_t>(offsetof(ListItem_t4166427101, ____btnSelect_3)); }
	inline Button_t4055032469 * get__btnSelect_3() const { return ____btnSelect_3; }
	inline Button_t4055032469 ** get_address_of__btnSelect_3() { return &____btnSelect_3; }
	inline void set__btnSelect_3(Button_t4055032469 * value)
	{
		____btnSelect_3 = value;
		Il2CppCodeGenWriteBarrier((&____btnSelect_3), value);
	}

	inline static int32_t get_offset_of_m_szContent_4() { return static_cast<int32_t>(offsetof(ListItem_t4166427101, ___m_szContent_4)); }
	inline String_t* get_m_szContent_4() const { return ___m_szContent_4; }
	inline String_t** get_address_of_m_szContent_4() { return &___m_szContent_4; }
	inline void set_m_szContent_4(String_t* value)
	{
		___m_szContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_szContent_4), value);
	}

	inline static int32_t get_offset_of_m_nIndex_5() { return static_cast<int32_t>(offsetof(ListItem_t4166427101, ___m_nIndex_5)); }
	inline int32_t get_m_nIndex_5() const { return ___m_nIndex_5; }
	inline int32_t* get_address_of_m_nIndex_5() { return &___m_nIndex_5; }
	inline void set_m_nIndex_5(int32_t value)
	{
		___m_nIndex_5 = value;
	}

	inline static int32_t get_offset_of__MyList_6() { return static_cast<int32_t>(offsetof(ListItem_t4166427101, ____MyList_6)); }
	inline ComoList_t2152284863 * get__MyList_6() const { return ____MyList_6; }
	inline ComoList_t2152284863 ** get_address_of__MyList_6() { return &____MyList_6; }
	inline void set__MyList_6(ComoList_t2152284863 * value)
	{
		____MyList_6 = value;
		Il2CppCodeGenWriteBarrier((&____MyList_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTITEM_T4166427101_H
#ifndef IOMANAGER_T475421879_H
#define IOMANAGER_T475421879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOManager
struct  IOManager_t475421879  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOMANAGER_T475421879_H
#ifndef MAPOBJ_T1733252447_H
#define MAPOBJ_T1733252447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapObj
struct  MapObj_t1733252447  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MapObj::m_fRotatoinZ
	float ___m_fRotatoinZ_2;
	// System.Single MapObj::m_fLiveTime
	float ___m_fLiveTime_3;
	// System.Boolean MapObj::m_bImortal
	bool ___m_bImortal_4;
	// UnityEngine.Vector3 MapObj::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Vector2 MapObj::_speed
	Vector2_t2156229523  ____speed_6;
	// System.Boolean MapObj::m_bDestroyed
	bool ___m_bDestroyed_7;
	// UnityEngine.Vector2 MapObj::m_Direction
	Vector2_t2156229523  ___m_Direction_8;
	// MapObj/eMapObjType MapObj::m_eObjType
	int32_t ___m_eObjType_9;
	// System.Single MapObj::m_fInitSpeed
	float ___m_fInitSpeed_10;
	// System.Single MapObj::m_fAccelerate
	float ___m_fAccelerate_11;
	// System.Single MapObj::m_fDirection
	float ___m_fDirection_12;
	// System.Boolean MapObj::m_bMoving
	bool ___m_bMoving_13;
	// System.Boolean MapObj::m_bEjecting
	bool ___m_bEjecting_14;
	// System.Single MapObj::m_fEjectDistance
	float ___m_fEjectDistance_15;
	// UnityEngine.Vector2 MapObj::m_vEjectStartPos
	Vector2_t2156229523  ___m_vEjectStartPos_16;

public:
	inline static int32_t get_offset_of_m_fRotatoinZ_2() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fRotatoinZ_2)); }
	inline float get_m_fRotatoinZ_2() const { return ___m_fRotatoinZ_2; }
	inline float* get_address_of_m_fRotatoinZ_2() { return &___m_fRotatoinZ_2; }
	inline void set_m_fRotatoinZ_2(float value)
	{
		___m_fRotatoinZ_2 = value;
	}

	inline static int32_t get_offset_of_m_fLiveTime_3() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fLiveTime_3)); }
	inline float get_m_fLiveTime_3() const { return ___m_fLiveTime_3; }
	inline float* get_address_of_m_fLiveTime_3() { return &___m_fLiveTime_3; }
	inline void set_m_fLiveTime_3(float value)
	{
		___m_fLiveTime_3 = value;
	}

	inline static int32_t get_offset_of_m_bImortal_4() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bImortal_4)); }
	inline bool get_m_bImortal_4() const { return ___m_bImortal_4; }
	inline bool* get_address_of_m_bImortal_4() { return &___m_bImortal_4; }
	inline void set_m_bImortal_4(bool value)
	{
		___m_bImortal_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ____speed_6)); }
	inline Vector2_t2156229523  get__speed_6() const { return ____speed_6; }
	inline Vector2_t2156229523 * get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(Vector2_t2156229523  value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of_m_bDestroyed_7() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bDestroyed_7)); }
	inline bool get_m_bDestroyed_7() const { return ___m_bDestroyed_7; }
	inline bool* get_address_of_m_bDestroyed_7() { return &___m_bDestroyed_7; }
	inline void set_m_bDestroyed_7(bool value)
	{
		___m_bDestroyed_7 = value;
	}

	inline static int32_t get_offset_of_m_Direction_8() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_Direction_8)); }
	inline Vector2_t2156229523  get_m_Direction_8() const { return ___m_Direction_8; }
	inline Vector2_t2156229523 * get_address_of_m_Direction_8() { return &___m_Direction_8; }
	inline void set_m_Direction_8(Vector2_t2156229523  value)
	{
		___m_Direction_8 = value;
	}

	inline static int32_t get_offset_of_m_eObjType_9() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_eObjType_9)); }
	inline int32_t get_m_eObjType_9() const { return ___m_eObjType_9; }
	inline int32_t* get_address_of_m_eObjType_9() { return &___m_eObjType_9; }
	inline void set_m_eObjType_9(int32_t value)
	{
		___m_eObjType_9 = value;
	}

	inline static int32_t get_offset_of_m_fInitSpeed_10() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fInitSpeed_10)); }
	inline float get_m_fInitSpeed_10() const { return ___m_fInitSpeed_10; }
	inline float* get_address_of_m_fInitSpeed_10() { return &___m_fInitSpeed_10; }
	inline void set_m_fInitSpeed_10(float value)
	{
		___m_fInitSpeed_10 = value;
	}

	inline static int32_t get_offset_of_m_fAccelerate_11() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fAccelerate_11)); }
	inline float get_m_fAccelerate_11() const { return ___m_fAccelerate_11; }
	inline float* get_address_of_m_fAccelerate_11() { return &___m_fAccelerate_11; }
	inline void set_m_fAccelerate_11(float value)
	{
		___m_fAccelerate_11 = value;
	}

	inline static int32_t get_offset_of_m_fDirection_12() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fDirection_12)); }
	inline float get_m_fDirection_12() const { return ___m_fDirection_12; }
	inline float* get_address_of_m_fDirection_12() { return &___m_fDirection_12; }
	inline void set_m_fDirection_12(float value)
	{
		___m_fDirection_12 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_13() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bMoving_13)); }
	inline bool get_m_bMoving_13() const { return ___m_bMoving_13; }
	inline bool* get_address_of_m_bMoving_13() { return &___m_bMoving_13; }
	inline void set_m_bMoving_13(bool value)
	{
		___m_bMoving_13 = value;
	}

	inline static int32_t get_offset_of_m_bEjecting_14() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bEjecting_14)); }
	inline bool get_m_bEjecting_14() const { return ___m_bEjecting_14; }
	inline bool* get_address_of_m_bEjecting_14() { return &___m_bEjecting_14; }
	inline void set_m_bEjecting_14(bool value)
	{
		___m_bEjecting_14 = value;
	}

	inline static int32_t get_offset_of_m_fEjectDistance_15() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fEjectDistance_15)); }
	inline float get_m_fEjectDistance_15() const { return ___m_fEjectDistance_15; }
	inline float* get_address_of_m_fEjectDistance_15() { return &___m_fEjectDistance_15; }
	inline void set_m_fEjectDistance_15(float value)
	{
		___m_fEjectDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_vEjectStartPos_16() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_vEjectStartPos_16)); }
	inline Vector2_t2156229523  get_m_vEjectStartPos_16() const { return ___m_vEjectStartPos_16; }
	inline Vector2_t2156229523 * get_address_of_m_vEjectStartPos_16() { return &___m_vEjectStartPos_16; }
	inline void set_m_vEjectStartPos_16(Vector2_t2156229523  value)
	{
		___m_vEjectStartPos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPOBJ_T1733252447_H
#ifndef ETCTOUCHPAD_T480692142_H
#define ETCTOUCHPAD_T480692142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad
struct  ETCTouchPad_t480692142  : public ETCBase_t40313246
{
public:
	// ETCTouchPad/OnMoveStartHandler ETCTouchPad::onMoveStart
	OnMoveStartHandler_t2512529097 * ___onMoveStart_48;
	// ETCTouchPad/OnMoveHandler ETCTouchPad::onMove
	OnMoveHandler_t3659216741 * ___onMove_49;
	// ETCTouchPad/OnMoveSpeedHandler ETCTouchPad::onMoveSpeed
	OnMoveSpeedHandler_t3603010941 * ___onMoveSpeed_50;
	// ETCTouchPad/OnMoveEndHandler ETCTouchPad::onMoveEnd
	OnMoveEndHandler_t330401481 * ___onMoveEnd_51;
	// ETCTouchPad/OnTouchStartHandler ETCTouchPad::onTouchStart
	OnTouchStartHandler_t3223438894 * ___onTouchStart_52;
	// ETCTouchPad/OnTouchUPHandler ETCTouchPad::onTouchUp
	OnTouchUPHandler_t2186573936 * ___onTouchUp_53;
	// ETCTouchPad/OnDownUpHandler ETCTouchPad::OnDownUp
	OnDownUpHandler_t3325091394 * ___OnDownUp_54;
	// ETCTouchPad/OnDownDownHandler ETCTouchPad::OnDownDown
	OnDownDownHandler_t3809127321 * ___OnDownDown_55;
	// ETCTouchPad/OnDownLeftHandler ETCTouchPad::OnDownLeft
	OnDownLeftHandler_t2754992532 * ___OnDownLeft_56;
	// ETCTouchPad/OnDownRightHandler ETCTouchPad::OnDownRight
	OnDownRightHandler_t1395399745 * ___OnDownRight_57;
	// ETCTouchPad/OnDownUpHandler ETCTouchPad::OnPressUp
	OnDownUpHandler_t3325091394 * ___OnPressUp_58;
	// ETCTouchPad/OnDownDownHandler ETCTouchPad::OnPressDown
	OnDownDownHandler_t3809127321 * ___OnPressDown_59;
	// ETCTouchPad/OnDownLeftHandler ETCTouchPad::OnPressLeft
	OnDownLeftHandler_t2754992532 * ___OnPressLeft_60;
	// ETCTouchPad/OnDownRightHandler ETCTouchPad::OnPressRight
	OnDownRightHandler_t1395399745 * ___OnPressRight_61;
	// ETCAxis ETCTouchPad::axisX
	ETCAxis_t4105840343 * ___axisX_62;
	// ETCAxis ETCTouchPad::axisY
	ETCAxis_t4105840343 * ___axisY_63;
	// System.Boolean ETCTouchPad::isDPI
	bool ___isDPI_64;
	// UnityEngine.UI.Image ETCTouchPad::cachedImage
	Image_t2670269651 * ___cachedImage_65;
	// UnityEngine.Vector2 ETCTouchPad::tmpAxis
	Vector2_t2156229523  ___tmpAxis_66;
	// UnityEngine.Vector2 ETCTouchPad::OldTmpAxis
	Vector2_t2156229523  ___OldTmpAxis_67;
	// UnityEngine.GameObject ETCTouchPad::previousDargObject
	GameObject_t1113636619 * ___previousDargObject_68;
	// System.Boolean ETCTouchPad::isOut
	bool ___isOut_69;
	// System.Boolean ETCTouchPad::isOnTouch
	bool ___isOnTouch_70;
	// System.Boolean ETCTouchPad::cachedVisible
	bool ___cachedVisible_71;

public:
	inline static int32_t get_offset_of_onMoveStart_48() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___onMoveStart_48)); }
	inline OnMoveStartHandler_t2512529097 * get_onMoveStart_48() const { return ___onMoveStart_48; }
	inline OnMoveStartHandler_t2512529097 ** get_address_of_onMoveStart_48() { return &___onMoveStart_48; }
	inline void set_onMoveStart_48(OnMoveStartHandler_t2512529097 * value)
	{
		___onMoveStart_48 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveStart_48), value);
	}

	inline static int32_t get_offset_of_onMove_49() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___onMove_49)); }
	inline OnMoveHandler_t3659216741 * get_onMove_49() const { return ___onMove_49; }
	inline OnMoveHandler_t3659216741 ** get_address_of_onMove_49() { return &___onMove_49; }
	inline void set_onMove_49(OnMoveHandler_t3659216741 * value)
	{
		___onMove_49 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_49), value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_50() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___onMoveSpeed_50)); }
	inline OnMoveSpeedHandler_t3603010941 * get_onMoveSpeed_50() const { return ___onMoveSpeed_50; }
	inline OnMoveSpeedHandler_t3603010941 ** get_address_of_onMoveSpeed_50() { return &___onMoveSpeed_50; }
	inline void set_onMoveSpeed_50(OnMoveSpeedHandler_t3603010941 * value)
	{
		___onMoveSpeed_50 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveSpeed_50), value);
	}

	inline static int32_t get_offset_of_onMoveEnd_51() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___onMoveEnd_51)); }
	inline OnMoveEndHandler_t330401481 * get_onMoveEnd_51() const { return ___onMoveEnd_51; }
	inline OnMoveEndHandler_t330401481 ** get_address_of_onMoveEnd_51() { return &___onMoveEnd_51; }
	inline void set_onMoveEnd_51(OnMoveEndHandler_t330401481 * value)
	{
		___onMoveEnd_51 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEnd_51), value);
	}

	inline static int32_t get_offset_of_onTouchStart_52() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___onTouchStart_52)); }
	inline OnTouchStartHandler_t3223438894 * get_onTouchStart_52() const { return ___onTouchStart_52; }
	inline OnTouchStartHandler_t3223438894 ** get_address_of_onTouchStart_52() { return &___onTouchStart_52; }
	inline void set_onTouchStart_52(OnTouchStartHandler_t3223438894 * value)
	{
		___onTouchStart_52 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchStart_52), value);
	}

	inline static int32_t get_offset_of_onTouchUp_53() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___onTouchUp_53)); }
	inline OnTouchUPHandler_t2186573936 * get_onTouchUp_53() const { return ___onTouchUp_53; }
	inline OnTouchUPHandler_t2186573936 ** get_address_of_onTouchUp_53() { return &___onTouchUp_53; }
	inline void set_onTouchUp_53(OnTouchUPHandler_t2186573936 * value)
	{
		___onTouchUp_53 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUp_53), value);
	}

	inline static int32_t get_offset_of_OnDownUp_54() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnDownUp_54)); }
	inline OnDownUpHandler_t3325091394 * get_OnDownUp_54() const { return ___OnDownUp_54; }
	inline OnDownUpHandler_t3325091394 ** get_address_of_OnDownUp_54() { return &___OnDownUp_54; }
	inline void set_OnDownUp_54(OnDownUpHandler_t3325091394 * value)
	{
		___OnDownUp_54 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownUp_54), value);
	}

	inline static int32_t get_offset_of_OnDownDown_55() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnDownDown_55)); }
	inline OnDownDownHandler_t3809127321 * get_OnDownDown_55() const { return ___OnDownDown_55; }
	inline OnDownDownHandler_t3809127321 ** get_address_of_OnDownDown_55() { return &___OnDownDown_55; }
	inline void set_OnDownDown_55(OnDownDownHandler_t3809127321 * value)
	{
		___OnDownDown_55 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownDown_55), value);
	}

	inline static int32_t get_offset_of_OnDownLeft_56() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnDownLeft_56)); }
	inline OnDownLeftHandler_t2754992532 * get_OnDownLeft_56() const { return ___OnDownLeft_56; }
	inline OnDownLeftHandler_t2754992532 ** get_address_of_OnDownLeft_56() { return &___OnDownLeft_56; }
	inline void set_OnDownLeft_56(OnDownLeftHandler_t2754992532 * value)
	{
		___OnDownLeft_56 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownLeft_56), value);
	}

	inline static int32_t get_offset_of_OnDownRight_57() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnDownRight_57)); }
	inline OnDownRightHandler_t1395399745 * get_OnDownRight_57() const { return ___OnDownRight_57; }
	inline OnDownRightHandler_t1395399745 ** get_address_of_OnDownRight_57() { return &___OnDownRight_57; }
	inline void set_OnDownRight_57(OnDownRightHandler_t1395399745 * value)
	{
		___OnDownRight_57 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownRight_57), value);
	}

	inline static int32_t get_offset_of_OnPressUp_58() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnPressUp_58)); }
	inline OnDownUpHandler_t3325091394 * get_OnPressUp_58() const { return ___OnPressUp_58; }
	inline OnDownUpHandler_t3325091394 ** get_address_of_OnPressUp_58() { return &___OnPressUp_58; }
	inline void set_OnPressUp_58(OnDownUpHandler_t3325091394 * value)
	{
		___OnPressUp_58 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressUp_58), value);
	}

	inline static int32_t get_offset_of_OnPressDown_59() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnPressDown_59)); }
	inline OnDownDownHandler_t3809127321 * get_OnPressDown_59() const { return ___OnPressDown_59; }
	inline OnDownDownHandler_t3809127321 ** get_address_of_OnPressDown_59() { return &___OnPressDown_59; }
	inline void set_OnPressDown_59(OnDownDownHandler_t3809127321 * value)
	{
		___OnPressDown_59 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressDown_59), value);
	}

	inline static int32_t get_offset_of_OnPressLeft_60() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnPressLeft_60)); }
	inline OnDownLeftHandler_t2754992532 * get_OnPressLeft_60() const { return ___OnPressLeft_60; }
	inline OnDownLeftHandler_t2754992532 ** get_address_of_OnPressLeft_60() { return &___OnPressLeft_60; }
	inline void set_OnPressLeft_60(OnDownLeftHandler_t2754992532 * value)
	{
		___OnPressLeft_60 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressLeft_60), value);
	}

	inline static int32_t get_offset_of_OnPressRight_61() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OnPressRight_61)); }
	inline OnDownRightHandler_t1395399745 * get_OnPressRight_61() const { return ___OnPressRight_61; }
	inline OnDownRightHandler_t1395399745 ** get_address_of_OnPressRight_61() { return &___OnPressRight_61; }
	inline void set_OnPressRight_61(OnDownRightHandler_t1395399745 * value)
	{
		___OnPressRight_61 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressRight_61), value);
	}

	inline static int32_t get_offset_of_axisX_62() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___axisX_62)); }
	inline ETCAxis_t4105840343 * get_axisX_62() const { return ___axisX_62; }
	inline ETCAxis_t4105840343 ** get_address_of_axisX_62() { return &___axisX_62; }
	inline void set_axisX_62(ETCAxis_t4105840343 * value)
	{
		___axisX_62 = value;
		Il2CppCodeGenWriteBarrier((&___axisX_62), value);
	}

	inline static int32_t get_offset_of_axisY_63() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___axisY_63)); }
	inline ETCAxis_t4105840343 * get_axisY_63() const { return ___axisY_63; }
	inline ETCAxis_t4105840343 ** get_address_of_axisY_63() { return &___axisY_63; }
	inline void set_axisY_63(ETCAxis_t4105840343 * value)
	{
		___axisY_63 = value;
		Il2CppCodeGenWriteBarrier((&___axisY_63), value);
	}

	inline static int32_t get_offset_of_isDPI_64() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___isDPI_64)); }
	inline bool get_isDPI_64() const { return ___isDPI_64; }
	inline bool* get_address_of_isDPI_64() { return &___isDPI_64; }
	inline void set_isDPI_64(bool value)
	{
		___isDPI_64 = value;
	}

	inline static int32_t get_offset_of_cachedImage_65() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___cachedImage_65)); }
	inline Image_t2670269651 * get_cachedImage_65() const { return ___cachedImage_65; }
	inline Image_t2670269651 ** get_address_of_cachedImage_65() { return &___cachedImage_65; }
	inline void set_cachedImage_65(Image_t2670269651 * value)
	{
		___cachedImage_65 = value;
		Il2CppCodeGenWriteBarrier((&___cachedImage_65), value);
	}

	inline static int32_t get_offset_of_tmpAxis_66() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___tmpAxis_66)); }
	inline Vector2_t2156229523  get_tmpAxis_66() const { return ___tmpAxis_66; }
	inline Vector2_t2156229523 * get_address_of_tmpAxis_66() { return &___tmpAxis_66; }
	inline void set_tmpAxis_66(Vector2_t2156229523  value)
	{
		___tmpAxis_66 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_67() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___OldTmpAxis_67)); }
	inline Vector2_t2156229523  get_OldTmpAxis_67() const { return ___OldTmpAxis_67; }
	inline Vector2_t2156229523 * get_address_of_OldTmpAxis_67() { return &___OldTmpAxis_67; }
	inline void set_OldTmpAxis_67(Vector2_t2156229523  value)
	{
		___OldTmpAxis_67 = value;
	}

	inline static int32_t get_offset_of_previousDargObject_68() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___previousDargObject_68)); }
	inline GameObject_t1113636619 * get_previousDargObject_68() const { return ___previousDargObject_68; }
	inline GameObject_t1113636619 ** get_address_of_previousDargObject_68() { return &___previousDargObject_68; }
	inline void set_previousDargObject_68(GameObject_t1113636619 * value)
	{
		___previousDargObject_68 = value;
		Il2CppCodeGenWriteBarrier((&___previousDargObject_68), value);
	}

	inline static int32_t get_offset_of_isOut_69() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___isOut_69)); }
	inline bool get_isOut_69() const { return ___isOut_69; }
	inline bool* get_address_of_isOut_69() { return &___isOut_69; }
	inline void set_isOut_69(bool value)
	{
		___isOut_69 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_70() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___isOnTouch_70)); }
	inline bool get_isOnTouch_70() const { return ___isOnTouch_70; }
	inline bool* get_address_of_isOnTouch_70() { return &___isOnTouch_70; }
	inline void set_isOnTouch_70(bool value)
	{
		___isOnTouch_70 = value;
	}

	inline static int32_t get_offset_of_cachedVisible_71() { return static_cast<int32_t>(offsetof(ETCTouchPad_t480692142, ___cachedVisible_71)); }
	inline bool get_cachedVisible_71() const { return ___cachedVisible_71; }
	inline bool* get_address_of_cachedVisible_71() { return &___cachedVisible_71; }
	inline void set_cachedVisible_71(bool value)
	{
		___cachedVisible_71 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCTOUCHPAD_T480692142_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef ETCJOYSTICK_T3250784002_H
#define ETCJOYSTICK_T3250784002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick
struct  ETCJoystick_t3250784002  : public ETCBase_t40313246
{
public:
	// ETCJoystick/OnMoveStartHandler ETCJoystick::onMoveStart
	OnMoveStartHandler_t2779627305 * ___onMoveStart_48;
	// ETCJoystick/OnMoveHandler ETCJoystick::onMove
	OnMoveHandler_t1792703951 * ___onMove_49;
	// ETCJoystick/OnMoveSpeedHandler ETCJoystick::onMoveSpeed
	OnMoveSpeedHandler_t961423656 * ___onMoveSpeed_50;
	// ETCJoystick/OnMoveEndHandler ETCJoystick::onMoveEnd
	OnMoveEndHandler_t3637094994 * ___onMoveEnd_51;
	// ETCJoystick/OnTouchStartHandler ETCJoystick::onTouchStart
	OnTouchStartHandler_t3335545810 * ___onTouchStart_52;
	// ETCJoystick/OnTouchUpHandler ETCJoystick::onTouchUp
	OnTouchUpHandler_t4157265644 * ___onTouchUp_53;
	// ETCJoystick/OnDownUpHandler ETCJoystick::OnDownUp
	OnDownUpHandler_t3611259663 * ___OnDownUp_54;
	// ETCJoystick/OnDownDownHandler ETCJoystick::OnDownDown
	OnDownDownHandler_t2311908456 * ___OnDownDown_55;
	// ETCJoystick/OnDownLeftHandler ETCJoystick::OnDownLeft
	OnDownLeftHandler_t2607413051 * ___OnDownLeft_56;
	// ETCJoystick/OnDownRightHandler ETCJoystick::OnDownRight
	OnDownRightHandler_t881666752 * ___OnDownRight_57;
	// ETCJoystick/OnDownUpHandler ETCJoystick::OnPressUp
	OnDownUpHandler_t3611259663 * ___OnPressUp_58;
	// ETCJoystick/OnDownDownHandler ETCJoystick::OnPressDown
	OnDownDownHandler_t2311908456 * ___OnPressDown_59;
	// ETCJoystick/OnDownLeftHandler ETCJoystick::OnPressLeft
	OnDownLeftHandler_t2607413051 * ___OnPressLeft_60;
	// ETCJoystick/OnDownRightHandler ETCJoystick::OnPressRight
	OnDownRightHandler_t881666752 * ___OnPressRight_61;
	// ETCJoystick/JoystickType ETCJoystick::joystickType
	int32_t ___joystickType_62;
	// System.Boolean ETCJoystick::allowJoystickOverTouchPad
	bool ___allowJoystickOverTouchPad_63;
	// ETCJoystick/RadiusBase ETCJoystick::radiusBase
	int32_t ___radiusBase_64;
	// System.Single ETCJoystick::radiusBaseValue
	float ___radiusBaseValue_65;
	// ETCAxis ETCJoystick::axisX
	ETCAxis_t4105840343 * ___axisX_66;
	// ETCAxis ETCJoystick::axisY
	ETCAxis_t4105840343 * ___axisY_67;
	// UnityEngine.RectTransform ETCJoystick::thumb
	RectTransform_t3704657025 * ___thumb_68;
	// ETCJoystick/JoystickArea ETCJoystick::joystickArea
	int32_t ___joystickArea_69;
	// UnityEngine.RectTransform ETCJoystick::userArea
	RectTransform_t3704657025 * ___userArea_70;
	// System.Boolean ETCJoystick::isTurnAndMove
	bool ___isTurnAndMove_71;
	// System.Single ETCJoystick::tmSpeed
	float ___tmSpeed_72;
	// System.Single ETCJoystick::tmAdditionnalRotation
	float ___tmAdditionnalRotation_73;
	// UnityEngine.AnimationCurve ETCJoystick::tmMoveCurve
	AnimationCurve_t3046754366 * ___tmMoveCurve_74;
	// System.Boolean ETCJoystick::tmLockInJump
	bool ___tmLockInJump_75;
	// UnityEngine.Vector3 ETCJoystick::tmLastMove
	Vector3_t3722313464  ___tmLastMove_76;
	// UnityEngine.Vector2 ETCJoystick::thumbPosition
	Vector2_t2156229523  ___thumbPosition_77;
	// System.Boolean ETCJoystick::isDynamicActif
	bool ___isDynamicActif_78;
	// UnityEngine.Vector2 ETCJoystick::tmpAxis
	Vector2_t2156229523  ___tmpAxis_79;
	// UnityEngine.Vector2 ETCJoystick::OldTmpAxis
	Vector2_t2156229523  ___OldTmpAxis_80;
	// System.Boolean ETCJoystick::isOnTouch
	bool ___isOnTouch_81;
	// System.Boolean ETCJoystick::isNoReturnThumb
	bool ___isNoReturnThumb_82;
	// UnityEngine.Vector2 ETCJoystick::noReturnPosition
	Vector2_t2156229523  ___noReturnPosition_83;
	// UnityEngine.Vector2 ETCJoystick::noReturnOffset
	Vector2_t2156229523  ___noReturnOffset_84;
	// System.Boolean ETCJoystick::isNoOffsetThumb
	bool ___isNoOffsetThumb_85;

public:
	inline static int32_t get_offset_of_onMoveStart_48() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___onMoveStart_48)); }
	inline OnMoveStartHandler_t2779627305 * get_onMoveStart_48() const { return ___onMoveStart_48; }
	inline OnMoveStartHandler_t2779627305 ** get_address_of_onMoveStart_48() { return &___onMoveStart_48; }
	inline void set_onMoveStart_48(OnMoveStartHandler_t2779627305 * value)
	{
		___onMoveStart_48 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveStart_48), value);
	}

	inline static int32_t get_offset_of_onMove_49() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___onMove_49)); }
	inline OnMoveHandler_t1792703951 * get_onMove_49() const { return ___onMove_49; }
	inline OnMoveHandler_t1792703951 ** get_address_of_onMove_49() { return &___onMove_49; }
	inline void set_onMove_49(OnMoveHandler_t1792703951 * value)
	{
		___onMove_49 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_49), value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_50() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___onMoveSpeed_50)); }
	inline OnMoveSpeedHandler_t961423656 * get_onMoveSpeed_50() const { return ___onMoveSpeed_50; }
	inline OnMoveSpeedHandler_t961423656 ** get_address_of_onMoveSpeed_50() { return &___onMoveSpeed_50; }
	inline void set_onMoveSpeed_50(OnMoveSpeedHandler_t961423656 * value)
	{
		___onMoveSpeed_50 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveSpeed_50), value);
	}

	inline static int32_t get_offset_of_onMoveEnd_51() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___onMoveEnd_51)); }
	inline OnMoveEndHandler_t3637094994 * get_onMoveEnd_51() const { return ___onMoveEnd_51; }
	inline OnMoveEndHandler_t3637094994 ** get_address_of_onMoveEnd_51() { return &___onMoveEnd_51; }
	inline void set_onMoveEnd_51(OnMoveEndHandler_t3637094994 * value)
	{
		___onMoveEnd_51 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEnd_51), value);
	}

	inline static int32_t get_offset_of_onTouchStart_52() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___onTouchStart_52)); }
	inline OnTouchStartHandler_t3335545810 * get_onTouchStart_52() const { return ___onTouchStart_52; }
	inline OnTouchStartHandler_t3335545810 ** get_address_of_onTouchStart_52() { return &___onTouchStart_52; }
	inline void set_onTouchStart_52(OnTouchStartHandler_t3335545810 * value)
	{
		___onTouchStart_52 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchStart_52), value);
	}

	inline static int32_t get_offset_of_onTouchUp_53() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___onTouchUp_53)); }
	inline OnTouchUpHandler_t4157265644 * get_onTouchUp_53() const { return ___onTouchUp_53; }
	inline OnTouchUpHandler_t4157265644 ** get_address_of_onTouchUp_53() { return &___onTouchUp_53; }
	inline void set_onTouchUp_53(OnTouchUpHandler_t4157265644 * value)
	{
		___onTouchUp_53 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUp_53), value);
	}

	inline static int32_t get_offset_of_OnDownUp_54() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnDownUp_54)); }
	inline OnDownUpHandler_t3611259663 * get_OnDownUp_54() const { return ___OnDownUp_54; }
	inline OnDownUpHandler_t3611259663 ** get_address_of_OnDownUp_54() { return &___OnDownUp_54; }
	inline void set_OnDownUp_54(OnDownUpHandler_t3611259663 * value)
	{
		___OnDownUp_54 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownUp_54), value);
	}

	inline static int32_t get_offset_of_OnDownDown_55() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnDownDown_55)); }
	inline OnDownDownHandler_t2311908456 * get_OnDownDown_55() const { return ___OnDownDown_55; }
	inline OnDownDownHandler_t2311908456 ** get_address_of_OnDownDown_55() { return &___OnDownDown_55; }
	inline void set_OnDownDown_55(OnDownDownHandler_t2311908456 * value)
	{
		___OnDownDown_55 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownDown_55), value);
	}

	inline static int32_t get_offset_of_OnDownLeft_56() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnDownLeft_56)); }
	inline OnDownLeftHandler_t2607413051 * get_OnDownLeft_56() const { return ___OnDownLeft_56; }
	inline OnDownLeftHandler_t2607413051 ** get_address_of_OnDownLeft_56() { return &___OnDownLeft_56; }
	inline void set_OnDownLeft_56(OnDownLeftHandler_t2607413051 * value)
	{
		___OnDownLeft_56 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownLeft_56), value);
	}

	inline static int32_t get_offset_of_OnDownRight_57() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnDownRight_57)); }
	inline OnDownRightHandler_t881666752 * get_OnDownRight_57() const { return ___OnDownRight_57; }
	inline OnDownRightHandler_t881666752 ** get_address_of_OnDownRight_57() { return &___OnDownRight_57; }
	inline void set_OnDownRight_57(OnDownRightHandler_t881666752 * value)
	{
		___OnDownRight_57 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownRight_57), value);
	}

	inline static int32_t get_offset_of_OnPressUp_58() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnPressUp_58)); }
	inline OnDownUpHandler_t3611259663 * get_OnPressUp_58() const { return ___OnPressUp_58; }
	inline OnDownUpHandler_t3611259663 ** get_address_of_OnPressUp_58() { return &___OnPressUp_58; }
	inline void set_OnPressUp_58(OnDownUpHandler_t3611259663 * value)
	{
		___OnPressUp_58 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressUp_58), value);
	}

	inline static int32_t get_offset_of_OnPressDown_59() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnPressDown_59)); }
	inline OnDownDownHandler_t2311908456 * get_OnPressDown_59() const { return ___OnPressDown_59; }
	inline OnDownDownHandler_t2311908456 ** get_address_of_OnPressDown_59() { return &___OnPressDown_59; }
	inline void set_OnPressDown_59(OnDownDownHandler_t2311908456 * value)
	{
		___OnPressDown_59 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressDown_59), value);
	}

	inline static int32_t get_offset_of_OnPressLeft_60() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnPressLeft_60)); }
	inline OnDownLeftHandler_t2607413051 * get_OnPressLeft_60() const { return ___OnPressLeft_60; }
	inline OnDownLeftHandler_t2607413051 ** get_address_of_OnPressLeft_60() { return &___OnPressLeft_60; }
	inline void set_OnPressLeft_60(OnDownLeftHandler_t2607413051 * value)
	{
		___OnPressLeft_60 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressLeft_60), value);
	}

	inline static int32_t get_offset_of_OnPressRight_61() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OnPressRight_61)); }
	inline OnDownRightHandler_t881666752 * get_OnPressRight_61() const { return ___OnPressRight_61; }
	inline OnDownRightHandler_t881666752 ** get_address_of_OnPressRight_61() { return &___OnPressRight_61; }
	inline void set_OnPressRight_61(OnDownRightHandler_t881666752 * value)
	{
		___OnPressRight_61 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressRight_61), value);
	}

	inline static int32_t get_offset_of_joystickType_62() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___joystickType_62)); }
	inline int32_t get_joystickType_62() const { return ___joystickType_62; }
	inline int32_t* get_address_of_joystickType_62() { return &___joystickType_62; }
	inline void set_joystickType_62(int32_t value)
	{
		___joystickType_62 = value;
	}

	inline static int32_t get_offset_of_allowJoystickOverTouchPad_63() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___allowJoystickOverTouchPad_63)); }
	inline bool get_allowJoystickOverTouchPad_63() const { return ___allowJoystickOverTouchPad_63; }
	inline bool* get_address_of_allowJoystickOverTouchPad_63() { return &___allowJoystickOverTouchPad_63; }
	inline void set_allowJoystickOverTouchPad_63(bool value)
	{
		___allowJoystickOverTouchPad_63 = value;
	}

	inline static int32_t get_offset_of_radiusBase_64() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___radiusBase_64)); }
	inline int32_t get_radiusBase_64() const { return ___radiusBase_64; }
	inline int32_t* get_address_of_radiusBase_64() { return &___radiusBase_64; }
	inline void set_radiusBase_64(int32_t value)
	{
		___radiusBase_64 = value;
	}

	inline static int32_t get_offset_of_radiusBaseValue_65() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___radiusBaseValue_65)); }
	inline float get_radiusBaseValue_65() const { return ___radiusBaseValue_65; }
	inline float* get_address_of_radiusBaseValue_65() { return &___radiusBaseValue_65; }
	inline void set_radiusBaseValue_65(float value)
	{
		___radiusBaseValue_65 = value;
	}

	inline static int32_t get_offset_of_axisX_66() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___axisX_66)); }
	inline ETCAxis_t4105840343 * get_axisX_66() const { return ___axisX_66; }
	inline ETCAxis_t4105840343 ** get_address_of_axisX_66() { return &___axisX_66; }
	inline void set_axisX_66(ETCAxis_t4105840343 * value)
	{
		___axisX_66 = value;
		Il2CppCodeGenWriteBarrier((&___axisX_66), value);
	}

	inline static int32_t get_offset_of_axisY_67() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___axisY_67)); }
	inline ETCAxis_t4105840343 * get_axisY_67() const { return ___axisY_67; }
	inline ETCAxis_t4105840343 ** get_address_of_axisY_67() { return &___axisY_67; }
	inline void set_axisY_67(ETCAxis_t4105840343 * value)
	{
		___axisY_67 = value;
		Il2CppCodeGenWriteBarrier((&___axisY_67), value);
	}

	inline static int32_t get_offset_of_thumb_68() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___thumb_68)); }
	inline RectTransform_t3704657025 * get_thumb_68() const { return ___thumb_68; }
	inline RectTransform_t3704657025 ** get_address_of_thumb_68() { return &___thumb_68; }
	inline void set_thumb_68(RectTransform_t3704657025 * value)
	{
		___thumb_68 = value;
		Il2CppCodeGenWriteBarrier((&___thumb_68), value);
	}

	inline static int32_t get_offset_of_joystickArea_69() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___joystickArea_69)); }
	inline int32_t get_joystickArea_69() const { return ___joystickArea_69; }
	inline int32_t* get_address_of_joystickArea_69() { return &___joystickArea_69; }
	inline void set_joystickArea_69(int32_t value)
	{
		___joystickArea_69 = value;
	}

	inline static int32_t get_offset_of_userArea_70() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___userArea_70)); }
	inline RectTransform_t3704657025 * get_userArea_70() const { return ___userArea_70; }
	inline RectTransform_t3704657025 ** get_address_of_userArea_70() { return &___userArea_70; }
	inline void set_userArea_70(RectTransform_t3704657025 * value)
	{
		___userArea_70 = value;
		Il2CppCodeGenWriteBarrier((&___userArea_70), value);
	}

	inline static int32_t get_offset_of_isTurnAndMove_71() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___isTurnAndMove_71)); }
	inline bool get_isTurnAndMove_71() const { return ___isTurnAndMove_71; }
	inline bool* get_address_of_isTurnAndMove_71() { return &___isTurnAndMove_71; }
	inline void set_isTurnAndMove_71(bool value)
	{
		___isTurnAndMove_71 = value;
	}

	inline static int32_t get_offset_of_tmSpeed_72() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___tmSpeed_72)); }
	inline float get_tmSpeed_72() const { return ___tmSpeed_72; }
	inline float* get_address_of_tmSpeed_72() { return &___tmSpeed_72; }
	inline void set_tmSpeed_72(float value)
	{
		___tmSpeed_72 = value;
	}

	inline static int32_t get_offset_of_tmAdditionnalRotation_73() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___tmAdditionnalRotation_73)); }
	inline float get_tmAdditionnalRotation_73() const { return ___tmAdditionnalRotation_73; }
	inline float* get_address_of_tmAdditionnalRotation_73() { return &___tmAdditionnalRotation_73; }
	inline void set_tmAdditionnalRotation_73(float value)
	{
		___tmAdditionnalRotation_73 = value;
	}

	inline static int32_t get_offset_of_tmMoveCurve_74() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___tmMoveCurve_74)); }
	inline AnimationCurve_t3046754366 * get_tmMoveCurve_74() const { return ___tmMoveCurve_74; }
	inline AnimationCurve_t3046754366 ** get_address_of_tmMoveCurve_74() { return &___tmMoveCurve_74; }
	inline void set_tmMoveCurve_74(AnimationCurve_t3046754366 * value)
	{
		___tmMoveCurve_74 = value;
		Il2CppCodeGenWriteBarrier((&___tmMoveCurve_74), value);
	}

	inline static int32_t get_offset_of_tmLockInJump_75() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___tmLockInJump_75)); }
	inline bool get_tmLockInJump_75() const { return ___tmLockInJump_75; }
	inline bool* get_address_of_tmLockInJump_75() { return &___tmLockInJump_75; }
	inline void set_tmLockInJump_75(bool value)
	{
		___tmLockInJump_75 = value;
	}

	inline static int32_t get_offset_of_tmLastMove_76() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___tmLastMove_76)); }
	inline Vector3_t3722313464  get_tmLastMove_76() const { return ___tmLastMove_76; }
	inline Vector3_t3722313464 * get_address_of_tmLastMove_76() { return &___tmLastMove_76; }
	inline void set_tmLastMove_76(Vector3_t3722313464  value)
	{
		___tmLastMove_76 = value;
	}

	inline static int32_t get_offset_of_thumbPosition_77() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___thumbPosition_77)); }
	inline Vector2_t2156229523  get_thumbPosition_77() const { return ___thumbPosition_77; }
	inline Vector2_t2156229523 * get_address_of_thumbPosition_77() { return &___thumbPosition_77; }
	inline void set_thumbPosition_77(Vector2_t2156229523  value)
	{
		___thumbPosition_77 = value;
	}

	inline static int32_t get_offset_of_isDynamicActif_78() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___isDynamicActif_78)); }
	inline bool get_isDynamicActif_78() const { return ___isDynamicActif_78; }
	inline bool* get_address_of_isDynamicActif_78() { return &___isDynamicActif_78; }
	inline void set_isDynamicActif_78(bool value)
	{
		___isDynamicActif_78 = value;
	}

	inline static int32_t get_offset_of_tmpAxis_79() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___tmpAxis_79)); }
	inline Vector2_t2156229523  get_tmpAxis_79() const { return ___tmpAxis_79; }
	inline Vector2_t2156229523 * get_address_of_tmpAxis_79() { return &___tmpAxis_79; }
	inline void set_tmpAxis_79(Vector2_t2156229523  value)
	{
		___tmpAxis_79 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_80() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___OldTmpAxis_80)); }
	inline Vector2_t2156229523  get_OldTmpAxis_80() const { return ___OldTmpAxis_80; }
	inline Vector2_t2156229523 * get_address_of_OldTmpAxis_80() { return &___OldTmpAxis_80; }
	inline void set_OldTmpAxis_80(Vector2_t2156229523  value)
	{
		___OldTmpAxis_80 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_81() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___isOnTouch_81)); }
	inline bool get_isOnTouch_81() const { return ___isOnTouch_81; }
	inline bool* get_address_of_isOnTouch_81() { return &___isOnTouch_81; }
	inline void set_isOnTouch_81(bool value)
	{
		___isOnTouch_81 = value;
	}

	inline static int32_t get_offset_of_isNoReturnThumb_82() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___isNoReturnThumb_82)); }
	inline bool get_isNoReturnThumb_82() const { return ___isNoReturnThumb_82; }
	inline bool* get_address_of_isNoReturnThumb_82() { return &___isNoReturnThumb_82; }
	inline void set_isNoReturnThumb_82(bool value)
	{
		___isNoReturnThumb_82 = value;
	}

	inline static int32_t get_offset_of_noReturnPosition_83() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___noReturnPosition_83)); }
	inline Vector2_t2156229523  get_noReturnPosition_83() const { return ___noReturnPosition_83; }
	inline Vector2_t2156229523 * get_address_of_noReturnPosition_83() { return &___noReturnPosition_83; }
	inline void set_noReturnPosition_83(Vector2_t2156229523  value)
	{
		___noReturnPosition_83 = value;
	}

	inline static int32_t get_offset_of_noReturnOffset_84() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___noReturnOffset_84)); }
	inline Vector2_t2156229523  get_noReturnOffset_84() const { return ___noReturnOffset_84; }
	inline Vector2_t2156229523 * get_address_of_noReturnOffset_84() { return &___noReturnOffset_84; }
	inline void set_noReturnOffset_84(Vector2_t2156229523  value)
	{
		___noReturnOffset_84 = value;
	}

	inline static int32_t get_offset_of_isNoOffsetThumb_85() { return static_cast<int32_t>(offsetof(ETCJoystick_t3250784002, ___isNoOffsetThumb_85)); }
	inline bool get_isNoOffsetThumb_85() const { return ___isNoOffsetThumb_85; }
	inline bool* get_address_of_isNoOffsetThumb_85() { return &___isNoOffsetThumb_85; }
	inline void set_isNoOffsetThumb_85(bool value)
	{
		___isNoOffsetThumb_85 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCJOYSTICK_T3250784002_H
#ifndef MAIN_T2227614074_H
#define MAIN_T2227614074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t2227614074  : public PunBehaviour_t987309092
{
public:
	// System.Single Main::m_fEjectHuanTing
	float ___m_fEjectHuanTing_3;
	// System.Single Main::m_fA
	float ___m_fA_4;
	// System.Single Main::m_fB
	float ___m_fB_5;
	// System.Single Main::m_fX
	float ___m_fX_6;
	// System.Single Main::m_fPlayerNameBaseSize
	float ___m_fPlayerNameBaseSize_9;
	// System.Single Main::m_fPlayerNameMaxThreshold
	float ___m_fPlayerNameMaxThreshold_10;
	// System.Single Main::m_fSporeMaxSize
	float ___m_fSporeMaxSize_11;
	// System.Single Main::m_fGameOverPanelTime
	float ___m_fGameOverPanelTime_12;
	// System.Int32 Main::m_nPlatform
	int32_t ___m_nPlatform_13;
	// System.Single Main::m_fDistanceToSlowDown
	float ___m_fDistanceToSlowDown_14;
	// System.Single Main::m_fBiggestBallAsCenterThreshold
	float ___m_fBiggestBallAsCenterThreshold_15;
	// System.Single Main::m_fSizeChangeSpeed
	float ___m_fSizeChangeSpeed_16;
	// System.Single Main::m_fSizeChangeSpeed_UnfoldSkill
	float ___m_fSizeChangeSpeed_UnfoldSkill_17;
	// System.Int32[] Main::m_aryUnfoldArcStartAngle
	Int32U5BU5D_t385246372* ___m_aryUnfoldArcStartAngle_18;
	// UnityEngine.GameObject Main::_goGameOver
	GameObject_t1113636619 * ____goGameOver_19;
	// UnityEngine.GameObject Main::_goBallsCenter
	GameObject_t1113636619 * ____goBallsCenter_20;
	// System.Single Main::m_fCamRaiseAccelerate
	float ___m_fCamRaiseAccelerate_21;
	// System.Single Main::m_fCamDownAccelerate
	float ___m_fCamDownAccelerate_22;
	// System.Single Main::m_fCamChangeDelaySpit
	float ___m_fCamChangeDelaySpit_23;
	// System.Single Main::m_fCamChangeDelayExplode
	float ___m_fCamChangeDelayExplode_24;
	// System.Single Main::m_fRaiseSpeed
	float ___m_fRaiseSpeed_25;
	// System.Single Main::m_fDownSpeed
	float ___m_fDownSpeed_26;
	// System.Single Main::m_fCamRespondSpeed
	float ___m_fCamRespondSpeed_27;
	// System.Single Main::m_fTimeBeforeCamDown
	float ___m_fTimeBeforeCamDown_28;
	// System.Single Main::m_fCamMinSize
	float ___m_fCamMinSize_29;
	// System.Single Main::m_fCamChangeSpeed
	float ___m_fCamChangeSpeed_30;
	// System.Single Main::m_fCamGuiWeiSpeed
	float ___m_fCamGuiWeiSpeed_31;
	// System.Single Main::m_fShangFuXiShu
	float ___m_fShangFuXiShu_32;
	// System.Single Main::m_fYiJianHeQiuSpeed
	float ___m_fYiJianHeQiuSpeed_33;
	// System.Single Main::m_fCamSizeXiShu
	float ___m_fCamSizeXiShu_34;
	// CCheat Main::_goCheat
	CCheat_t3466918192 * ____goCheat_35;
	// UnityEngine.GameObject Main::m_goJueShengQiu
	GameObject_t1113636619 * ___m_goJueShengQiu_36;
	// UnityEngine.Camera Main::m_camWarFog
	Camera_t4157153871 * ___m_camWarFog_37;
	// UnityEngine.Camera Main::m_camForCalc
	Camera_t4157153871 * ___m_camForCalc_38;
	// SystemMsg Main::g_SystemMsg
	SystemMsg_t1823497383 * ___g_SystemMsg_39;
	// UnityEngine.Sprite[] Main::m_aryBallSprite
	SpriteU5BU5D_t2581906349* ___m_aryBallSprite_40;
	// UnityEngine.GameObject Main::m_goHongBao
	GameObject_t1113636619 * ___m_goHongBao_42;
	// PlayerAction Main::_playeraction
	PlayerAction_t3782228511 * ____playeraction_43;
	// CMonsterBall Main::m_ShengFuPanDingMonster
	CMonsterBall_t654437011 * ___m_ShengFuPanDingMonster_44;
	// UnityEngine.GameObject Main::m_preGridLine
	GameObject_t1113636619 * ___m_preGridLine_45;
	// UnityEngine.GameObject Main::m_preBallLine
	GameObject_t1113636619 * ___m_preBallLine_46;
	// UnityEngine.GameObject Main::m_preBall
	GameObject_t1113636619 * ___m_preBall_47;
	// UnityEngine.GameObject Main::m_preBallTest
	GameObject_t1113636619 * ___m_preBallTest_48;
	// UnityEngine.GameObject Main::m_preElectronicBall
	GameObject_t1113636619 * ___m_preElectronicBall_49;
	// UnityEngine.GameObject Main::m_prePlayer
	GameObject_t1113636619 * ___m_prePlayer_50;
	// UnityEngine.GameObject Main::m_preThorn
	GameObject_t1113636619 * ___m_preThorn_51;
	// UnityEngine.GameObject Main::m_preRhythm
	GameObject_t1113636619 * ___m_preRhythm_52;
	// UnityEngine.GameObject Main::m_preBean
	GameObject_t1113636619 * ___m_preBean_53;
	// UnityEngine.GameObject Main::m_preEffectPreExplode
	GameObject_t1113636619 * ___m_preEffectPreExplode_54;
	// UnityEngine.GameObject Main::m_preEffectExplode
	GameObject_t1113636619 * ___m_preEffectExplode_55;
	// UnityEngine.GameObject Main::m_preBlackHole
	GameObject_t1113636619 * ___m_preBlackHole_56;
	// UnityEngine.GameObject Main::m_preSpitBallTarget
	GameObject_t1113636619 * ___m_preSpitBallTarget_57;
	// UnityEngine.GameObject Main::m_preSpore
	GameObject_t1113636619 * ___m_preSpore_58;
	// UnityEngine.GameObject Main::m_preNail
	GameObject_t1113636619 * ___m_preNail_59;
	// UnityEngine.GameObject Main::m_preVoice
	GameObject_t1113636619 * ___m_preVoice_60;
	// UnityEngine.GameObject Main::m_preGroup
	GameObject_t1113636619 * ___m_preGroup_61;
	// UnityEngine.GameObject Main::m_preOneWayObstacle
	GameObject_t1113636619 * ___m_preOneWayObstacle_62;
	// UnityEngine.GameObject Main::m_preExplodeNode
	GameObject_t1113636619 * ___m_preExplodeNode_63;
	// UnityEngine.GameObject Main::_panelRank
	GameObject_t1113636619 * ____panelRank_64;
	// UnityEngine.GameObject Main::_panelSkillPoint
	GameObject_t1113636619 * ____panelSkillPoint_65;
	// CMsgList Main::_msglistKillPeople
	CMsgList_t4144960221 * ____msglistKillPeople_66;
	// UnityEngine.UI.Text Main::_txtTimeLeft
	Text_t1901882714 * ____txtTimeLeft_67;
	// UnityEngine.UI.Text Main::_txtTimeLeft_MOBILE
	Text_t1901882714 * ____txtTimeLeft_MOBILE_68;
	// UnityEngine.UI.Text Main::_txtTimeLeft_PC
	Text_t1901882714 * ____txtTimeLeft_PC_69;
	// UnityEngine.GameObject Main::_uiDead
	GameObject_t1113636619 * ____uiDead_70;
	// UnityEngine.GameObject Main::_uiDeadNew
	GameObject_t1113636619 * ____uiDeadNew_71;
	// UnityEngine.GameObject Main::_uiGameOver
	GameObject_t1113636619 * ____uiGameOver_72;
	// UnityEngine.UI.Text Main::m_txtDebugInfo
	Text_t1901882714 * ___m_txtDebugInfo_73;
	// UnityEngine.UI.Slider Main::m_sliderStickSize
	Slider_t3903728902 * ___m_sliderStickSize_74;
	// UnityEngine.UI.Slider Main::m_sliderAccelerate
	Slider_t3903728902 * ___m_sliderAccelerate_75;
	// UnityEngine.UI.Button Main::m_btnSpit
	Button_t4055032469 * ___m_btnSpit_76;
	// UnityEngine.UI.Scrollbar Main::m_sbSpitSpore
	Scrollbar_t1494447233 * ___m_sbSpitSpore_77;
	// UnityEngine.UI.Scrollbar Main::m_sbSpitBall
	Scrollbar_t1494447233 * ___m_sbSpitBall_78;
	// UnityEngine.UI.Scrollbar Main::m_sbUnfold
	Scrollbar_t1494447233 * ___m_sbUnfold_79;
	// UnityEngine.GameObject Main::m_uiGamePanel
	GameObject_t1113636619 * ___m_uiGamePanel_80;
	// UnityEngine.GameObject Main::m_uiMapEditorPanel
	GameObject_t1113636619 * ___m_uiMapEditorPanel_81;
	// UnityEngine.UI.Image Main::m_imgSpitBall_Fill
	Image_t2670269651 * ___m_imgSpitBall_Fill_82;
	// UnityEngine.UI.Text Main::m_txtColdDown_W
	Text_t1901882714 * ___m_txtColdDown_W_83;
	// UnityEngine.UI.Text Main::m_txtColdDown_E
	Text_t1901882714 * ___m_txtColdDown_E_84;
	// UnityEngine.UI.Text Main::m_txtColdDown_R
	Text_t1901882714 * ___m_txtColdDown_R_85;
	// UnityEngine.UI.Text Main::m_txtColdDown_W_Mobile
	Text_t1901882714 * ___m_txtColdDown_W_Mobile_86;
	// UnityEngine.UI.Text Main::m_txtColdDown_E_Mobile
	Text_t1901882714 * ___m_txtColdDown_E_Mobile_87;
	// UnityEngine.UI.Text Main::m_txtColdDown_R_Mobile
	Text_t1901882714 * ___m_txtColdDown_R_Mobile_88;
	// UnityEngine.UI.Image Main::m_imgSpitSpore_Fill
	Image_t2670269651 * ___m_imgSpitSpore_Fill_89;
	// UnityEngine.UI.Image Main::m_imgUnfold_Fill
	Image_t2670269651 * ___m_imgUnfold_Fill_90;
	// UnityEngine.UI.Image Main::m_imgOneBtnSplit_Fill
	Image_t2670269651 * ___m_imgOneBtnSplit_Fill_91;
	// UnityEngine.UI.Image Main::m_imgSelectedSkill_Fill
	Image_t2670269651 * ___m_imgSelectedSkill_Fill_92;
	// System.Single Main::m_SelectedSkillColdDownTimeCount
	float ___m_SelectedSkillColdDownTimeCount_93;
	// System.Single Main::m_SelectedSkillColdDownInterval
	float ___m_SelectedSkillColdDownInterval_94;
	// UnityEngine.UI.Image Main::m_imgLababa_Fill
	Image_t2670269651 * ___m_imgLababa_Fill_95;
	// System.Single Main::m_fBallTriggerBasrRadius
	float ___m_fBallTriggerBasrRadius_96;
	// System.Single Main::m_fPixel2Scale
	float ___m_fPixel2Scale_97;
	// System.Single Main::m_fScale2Radius
	float ___m_fScale2Radius_98;
	// System.Single Main::m_fRadius2Scale
	float ___m_fRadius2Scale_99;
	// System.Int32 Main::m_nShit
	int32_t ___m_nShit_100;
	// System.Single Main::m_fBallBaseSpeed
	float ___m_fBallBaseSpeed_104;
	// System.Single Main::m_fBallSpeedRadiusKaiFangFenMu
	float ___m_fBallSpeedRadiusKaiFangFenMu_105;
	// System.Single Main::m_fBallMoveToCenterBaseSpeed
	float ___m_fBallMoveToCenterBaseSpeed_106;
	// System.Int32 Main::m_nRebornPosIdx
	int32_t ___m_nRebornPosIdx_107;
	// System.Single Main::m_fBaseVolumeyByItemDecreasedPercentWhenDead
	float ___m_fBaseVolumeyByItemDecreasedPercentWhenDead_108;
	// System.Single Main::m_fMaxAudioVolume
	float ___m_fMaxAudioVolume_109;
	// System.Single Main::m_fMinAudioVolume
	float ___m_fMinAudioVolume_110;
	// System.Single Main::m_fMaxPlayerSpeed
	float ___m_fMaxPlayerSpeed_111;
	// System.Single Main::m_fShengFuPanDingSize
	float ___m_fShengFuPanDingSize_112;
	// System.Single Main::m_fClassCircleThreshold0
	float ___m_fClassCircleThreshold0_113;
	// System.Single Main::m_fClassCircleThreshold1
	float ___m_fClassCircleThreshold1_114;
	// System.Single Main::m_fClassCircleLength0
	float ___m_fClassCircleLength0_115;
	// System.Single Main::m_fClassCircleLength1
	float ___m_fClassCircleLength1_116;
	// UnityEngine.Vector3 Main::m_vecClassCircleCenterPos
	Vector3_t3722313464  ___m_vecClassCircleCenterPos_117;
	// System.Single Main::m_fLaBabaRunTime
	float ___m_fLaBabaRunTime_118;
	// System.Single Main::m_fLaBabaRunDistance
	float ___m_fLaBabaRunDistance_119;
	// System.Single Main::m_fLababaColdDown
	float ___m_fLababaColdDown_120;
	// System.Single Main::m_fLababaLiveTime
	float ___m_fLababaLiveTime_121;
	// System.Single Main::m_fLababaSizeCostPercent
	float ___m_fLababaSizeCostPercent_122;
	// System.Single Main::m_fLababaAreaThreshold
	float ___m_fLababaAreaThreshold_123;
	// System.Single Main::m_fTimeOfOneGame
	float ___m_fTimeOfOneGame_124;
	// System.Single Main::m_fAdjustMoveProtectInterval
	float ___m_fAdjustMoveProtectInterval_125;
	// System.Single Main::m_fMaxMoveDistancePerFrame
	float ___m_fMaxMoveDistancePerFrame_126;
	// System.Int32 Main::m_nObservor
	int32_t ___m_nObservor_127;
	// System.Single Main::m_fShellAdjustShit
	float ___m_fShellAdjustShit_128;
	// System.Single Main::m_fBaseSpeedInGroupLocal
	float ___m_fBaseSpeedInGroupLocal_129;
	// System.Single Main::m_fSyncMoveCountInterval
	float ___m_fSyncMoveCountInterval_130;
	// System.Single Main::m_fAdjustPosDelayThreshold
	float ___m_fAdjustPosDelayThreshold_131;
	// System.Single Main::m_nCheatMode
	float ___m_nCheatMode_132;
	// System.Single Main::m_fBornProtectTime
	float ___m_fBornProtectTime_133;
	// System.Single Main::m_fMinRunTime
	float ___m_fMinRunTime_134;
	// System.Single Main::m_fGridLineGap
	float ___m_fGridLineGap_135;
	// System.Single Main::m_fTimeBeforeUiDead
	float ___m_fTimeBeforeUiDead_136;
	// System.Single Main::m_fBeanNumPerRange
	float ___m_fBeanNumPerRange_137;
	// System.Single Main::m_fThornNumPerRange
	float ___m_fThornNumPerRange_138;
	// System.Single Main::m_fBeanSize
	float ___m_fBeanSize_139;
	// System.Single Main::m_fThornSize
	float ___m_fThornSize_140;
	// System.Single Main::m_fShellShrinkTotalTime
	float ___m_fShellShrinkTotalTime_141;
	// System.Single Main::m_fExplodeShellShrinkTotalTime
	float ___m_fExplodeShellShrinkTotalTime_142;
	// System.Single Main::m_fSplitShellShrinkTotalTime
	float ___m_fSplitShellShrinkTotalTime_143;
	// System.Single Main::m_fThornContributeToBallSize
	float ___m_fThornContributeToBallSize_144;
	// System.Single Main::m_fAutoAttenuate
	float ___m_fAutoAttenuate_145;
	// System.Single Main::m_fAutoAttenuateTimeInterval
	float ___m_fAutoAttenuateTimeInterval_146;
	// System.Single Main::m_fThornAttenuate
	float ___m_fThornAttenuate_147;
	// System.Single Main::m_fArenaWidth
	float ___m_fArenaWidth_148;
	// System.Single Main::m_fArenaHeight
	float ___m_fArenaHeight_149;
	// System.Single Main::m_fGenerateNewBeanTimeInterval
	float ___m_fGenerateNewBeanTimeInterval_150;
	// System.Single Main::m_fGenerateNewThornTimeInterval
	float ___m_fGenerateNewThornTimeInterval_151;
	// System.Single Main::m_fSpitSporeTimeInterval
	float ___m_fSpitSporeTimeInterval_152;
	// System.Single Main::m_fSpitBallTimeInterval
	float ___m_fSpitBallTimeInterval_153;
	// System.Single Main::m_fUnfoldCostMP
	float ___m_fUnfoldCostMP_154;
	// System.Single Main::m_fMainTriggerUnfoldTime
	float ___m_fMainTriggerUnfoldTime_155;
	// System.Single Main::m_fMainTriggerPreUnfoldTime
	float ___m_fMainTriggerPreUnfoldTime_156;
	// System.Single Main::m_fUnfoldTimeInterval
	float ___m_fUnfoldTimeInterval_157;
	// System.Single Main::m_fUnfoldSale
	float ___m_fUnfoldSale_158;
	// System.Single Main::m_fUnfoldCircleSale
	float ___m_fUnfoldCircleSale_159;
	// System.Single Main::m_fForceSpitTotalTime
	float ___m_fForceSpitTotalTime_160;
	// System.Single Main::m_fSpitRunTime
	float ___m_fSpitRunTime_161;
	// System.Single Main::m_fSpitBallRunTime
	float ___m_fSpitBallRunTime_162;
	// System.String Main::m_szSpitSporeBeanType
	String_t* ___m_szSpitSporeBeanType_163;
	// System.Single Main::m_fSpitSporeCostMP
	float ___m_fSpitSporeCostMP_164;
	// System.Single Main::m_fSpitSporeRunDistance
	float ___m_fSpitSporeRunDistance_165;
	// System.Single Main::m_fSpitSporeInitSpeed
	float ___m_fSpitSporeInitSpeed_166;
	// System.Single Main::m_fSpitSporeAccelerate
	float ___m_fSpitSporeAccelerate_167;
	// System.Single Main::m_fSpittedObjrunRunTime
	float ___m_fSpittedObjrunRunTime_168;
	// System.Single Main::m_fSpittedObjrunRunDistance
	float ___m_fSpittedObjrunRunDistance_169;
	// System.Single Main::m_fSpitBallHalfRunTime
	float ___m_fSpitBallHalfRunTime_170;
	// System.Single Main::m_fSpitBallHalfRunDistance
	float ___m_fSpitBallHalfRunDistance_171;
	// System.Single Main::m_fSpitBallHalfCostMP
	float ___m_fSpitBallHalfCostMP_172;
	// System.Single Main::m_fSpitBallHalfColddown
	float ___m_fSpitBallHalfColddown_173;
	// System.Single Main::m_fSpitBallHalfQianYao
	float ___m_fSpitBallHalfQianYao_174;
	// System.Single Main::m_fSpitBallRunSpeed
	float ___m_fSpitBallRunSpeed_175;
	// System.Single Main::m_fSpitBallRunDistanceMultiple
	float ___m_fSpitBallRunDistanceMultiple_176;
	// System.Single Main::m_fSpitBallCostMP
	float ___m_fSpitBallCostMP_177;
	// System.Single Main::m_fSpitBallStayTime
	float ___m_fSpitBallStayTime_178;
	// System.Single Main::m_fBornMinTiJi
	float ___m_fBornMinTiJi_179;
	// System.Single Main::m_fMinDiameterToEatThorn
	float ___m_fMinDiameterToEatThorn_180;
	// System.Single Main::m_fExplodeRunDistanceMultiple
	float ___m_fExplodeRunDistanceMultiple_181;
	// System.Single Main::m_fExplodeStayTime
	float ___m_fExplodeStayTime_182;
	// System.Single Main::m_fExplodePercent
	float ___m_fExplodePercent_183;
	// System.Single Main::m_fExpectExplodeNum
	float ___m_fExpectExplodeNum_184;
	// System.Single Main::m_fExplodeRunTime
	float ___m_fExplodeRunTime_185;
	// System.Single Main::m_nExplodeMinNum
	float ___m_nExplodeMinNum_186;
	// System.Single Main::m_fMaxBallNumPerPlayer
	float ___m_fMaxBallNumPerPlayer_187;
	// System.Single Main::m_fSprayBeanMaxInitSpeed
	float ___m_fSprayBeanMaxInitSpeed_188;
	// System.Single Main::m_fSprayBeanMinInitSpeed
	float ___m_fSprayBeanMinInitSpeed_189;
	// System.Single Main::m_fSprayBeanAccelerate
	float ___m_fSprayBeanAccelerate_190;
	// System.Single Main::m_fSprayBeanLiveTime
	float ___m_fSprayBeanLiveTime_191;
	// System.Single Main::m_fCrucifix
	float ___m_fCrucifix_192;
	// System.Int32 Main::m_nSplitNum
	int32_t ___m_nSplitNum_193;
	// System.Single Main::m_fSplitCostMP
	float ___m_fSplitCostMP_194;
	// System.Single Main::m_fSplitStayTime
	float ___m_fSplitStayTime_195;
	// System.Single Main::m_fSplitRunTime
	float ___m_fSplitRunTime_196;
	// System.Single Main::m_fSplitMaxDistance
	float ___m_fSplitMaxDistance_197;
	// System.Single Main::m_fSplitTotalTime
	float ___m_fSplitTotalTime_198;
	// System.Single Main::m_fSplitTimeInterval
	float ___m_fSplitTimeInterval_199;
	// System.Single Main::m_fTriggerRadius
	float ___m_fTriggerRadius_200;
	// UnityEngine.GameObject Main::the_spray
	GameObject_t1113636619 * ___the_spray_201;
	// UnityEngine.GameObject Main::m_goSprays
	GameObject_t1113636619 * ___m_goSprays_202;
	// System.Single Main::m_fSpitNailTimeInterval
	float ___m_fSpitNailTimeInterval_203;
	// System.Single Main::m_fNailLiveTime
	float ___m_fNailLiveTime_204;
	// UnityEngine.LineRenderer Main::m_lineWorldBorderTop
	LineRenderer_t3154350270 * ___m_lineWorldBorderTop_205;
	// UnityEngine.LineRenderer Main::m_lineWorldBorderBottom
	LineRenderer_t3154350270 * ___m_lineWorldBorderBottom_206;
	// UnityEngine.LineRenderer Main::m_lineWorldBorderLeft
	LineRenderer_t3154350270 * ___m_lineWorldBorderLeft_207;
	// UnityEngine.LineRenderer Main::m_lineWorldBorderRight
	LineRenderer_t3154350270 * ___m_lineWorldBorderRight_208;
	// UnityEngine.GameObject Main::m_goBorderLeft
	GameObject_t1113636619 * ___m_goBorderLeft_209;
	// UnityEngine.GameObject Main::m_goBorderRight
	GameObject_t1113636619 * ___m_goBorderRight_210;
	// UnityEngine.GameObject Main::m_goBorderTop
	GameObject_t1113636619 * ___m_goBorderTop_211;
	// UnityEngine.GameObject Main::m_goBorderBottom
	GameObject_t1113636619 * ___m_goBorderBottom_212;
	// UnityEngine.Vector3 Main::vecTempPos
	Vector3_t3722313464  ___vecTempPos_213;
	// UnityEngine.Vector3 Main::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_214;
	// UnityEngine.GameObject Main::m_goThornsSpit
	GameObject_t1113636619 * ___m_goThornsSpit_215;
	// UnityEngine.GameObject Main::m_goThorns
	GameObject_t1113636619 * ___m_goThorns_216;
	// UnityEngine.GameObject Main::m_goSpores
	GameObject_t1113636619 * ___m_goSpores_217;
	// UnityEngine.GameObject Main::m_goBeans
	GameObject_t1113636619 * ___m_goBeans_218;
	// UnityEngine.GameObject Main::m_goSpitBallTargets
	GameObject_t1113636619 * ___m_goSpitBallTargets_219;
	// UnityEngine.GameObject Main::m_goNails
	GameObject_t1113636619 * ___m_goNails_220;
	// UnityEngine.GameObject Main::m_goBallsBeNailed
	GameObject_t1113636619 * ___m_goBallsBeNailed_221;
	// UnityEngine.GameObject Main::m_goDestroyedBalls
	GameObject_t1113636619 * ___m_goDestroyedBalls_222;
	// UnityEngine.GameObject Main::m_goBalls
	GameObject_t1113636619 * ___m_goBalls_223;
	// UnityEngine.GameObject Main::m_goJoyStick
	GameObject_t1113636619 * ___m_goJoyStick_224;
	// System.Int32 Main::s_nBallTotalNum
	int32_t ___s_nBallTotalNum_225;
	// Player Main::m_MainPlayer
	Player_t3266647312 * ___m_MainPlayer_227;
	// UnityEngine.GameObject Main::m_goMainPlayer
	GameObject_t1113636619 * ___m_goMainPlayer_228;
	// PhotonView Main::m_photonViewMainPlayer
	PhotonView_t2207721820 * ___m_photonViewMainPlayer_229;
	// UnityEngine.UI.Text Main::m_textDebugInfo
	Text_t1901882714 * ___m_textDebugInfo_230;
	// System.Object[] Main::s_Params
	ObjectU5BU5D_t2843939325* ___s_Params_231;
	// System.Boolean Main::m_bInitingAllBalls
	bool ___m_bInitingAllBalls_232;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> Main::m_dicGamePlayDataConfig
	Dictionary_2_t1182523073 * ___m_dicGamePlayDataConfig_234;
	// System.String[][] Main::Array
	StringU5BU5DU5BU5D_t2611993717* ___Array_235;
	// System.Int32 Main::woca
	int32_t ___woca_236;
	// System.Single Main::m_fDeadProgressTimeCount
	float ___m_fDeadProgressTimeCount_237;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Main::lstAssistAvatar
	List_1_t1752731834 * ___lstAssistAvatar_238;
	// System.Single Main::m_fDeadUICount
	float ___m_fDeadUICount_239;
	// System.Int32 Main::m_nDeadStatus
	int32_t ___m_nDeadStatus_240;
	// System.Boolean Main::m_bCtrlDown
	bool ___m_bCtrlDown_242;
	// System.Boolean Main::m_bShowRank
	bool ___m_bShowRank_243;
	// System.Boolean Main::m_bKeyDown_R
	bool ___m_bKeyDown_R_244;
	// System.Boolean Main::m_bKeyDown_E
	bool ___m_bKeyDown_E_245;
	// System.Single Main::m_fSplitTimeCount
	float ___m_fSplitTimeCount_246;
	// System.Single Main::m_fLababTimeCount
	float ___m_fLababTimeCount_247;
	// System.Single Main::m_fArenaRange
	float ___m_fArenaRange_248;
	// System.Single Main::m_nBeanTotalNumDueToRange
	float ___m_nBeanTotalNumDueToRange_249;
	// System.Single Main::m_nCurBeanTotalNum
	float ___m_nCurBeanTotalNum_250;
	// System.Single Main::m_nCurGeneraedByMeBeanTotalNum
	float ___m_nCurGeneraedByMeBeanTotalNum_251;
	// System.Int32 Main::m_nCurGeneraedByMeThornTotalNum
	int32_t ___m_nCurGeneraedByMeThornTotalNum_252;
	// System.Single Main::m_nThornTotalNumDueToRange
	float ___m_nThornTotalNumDueToRange_253;
	// System.Single Main::m_nCurThornTotalNum
	float ___m_nCurThornTotalNum_254;
	// System.Single Main::m_fGenerateBeanTime
	float ___m_fGenerateBeanTime_255;
	// System.Single Main::m_fGenerateThornTime
	float ___m_fGenerateThornTime_256;
	// System.Single Main::m_fWorldLeft
	float ___m_fWorldLeft_258;
	// System.Single Main::m_fWorldRight
	float ___m_fWorldRight_259;
	// System.Single Main::m_fWorldBottom
	float ___m_fWorldBottom_260;
	// System.Single Main::m_fWorldTop
	float ___m_fWorldTop_261;
	// UnityEngine.Vector3 Main::vecScreenMin
	Vector3_t3722313464  ___vecScreenMin_262;
	// UnityEngine.Vector3 Main::vecScreenMax
	Vector3_t3722313464  ___vecScreenMax_263;
	// UnityEngine.Vector3 Main::vecWorldMin
	Vector3_t3722313464  ___vecWorldMin_264;
	// UnityEngine.Vector3 Main::vecWorldMax
	Vector3_t3722313464  ___vecWorldMax_265;
	// System.Collections.Generic.Dictionary`2<System.Int32,CMonster> Main::m_dicAllThorns
	Dictionary_2_t830184269 * ___m_dicAllThorns_266;
	// System.Single Main::m_fForceSpitPercent
	float ___m_fForceSpitPercent_267;
	// System.Boolean Main::m_bForceSpit
	bool ___m_bForceSpit_268;
	// System.Single Main::m_fSpitBallCurColdDownTime
	float ___m_fSpitBallCurColdDownTime_269;
	// System.Single Main::m_fCurCamSize
	float ___m_fCurCamSize_270;
	// System.Single Main::m_fCamSizeProtectTime
	float ___m_fCamSizeProtectTime_271;
	// System.Single Main::m_fForceSpitCurTime
	float ___m_fForceSpitCurTime_272;
	// System.Single Main::m_fCurColdDownTime_BecomeThor_Count
	float ___m_fCurColdDownTime_BecomeThor_Count_273;
	// System.Single Main::m_fCurColdDownTime_BecomeThorn
	float ___m_fCurColdDownTime_BecomeThorn_274;
	// System.Single Main::m_fUnfoldCurColdDownTime
	float ___m_fUnfoldCurColdDownTime_275;
	// System.Single Main::m_fSpitSporeCurColdDownTime
	float ___m_fSpitSporeCurColdDownTime_276;
	// System.Single Main::m_fSpitNailCurColdDownTime
	float ___m_fSpitNailCurColdDownTime_277;
	// System.Collections.Generic.List`1<Ball> Main::lstTemp
	List_1_t3678741308 * ___lstTemp_278;
	// System.Boolean Main::m_bTestFucking
	bool ___m_bTestFucking_279;
	// System.Int32 Main::m_nFuckCount
	int32_t ___m_nFuckCount_280;
	// System.Single Main::m_fFuckTime
	float ___m_fFuckTime_281;
	// System.Collections.Generic.List`1<Player> Main::m_lstPlayers
	List_1_t443754758 * ___m_lstPlayers_282;
	// System.Single Main::m_fSkillColdDownCount_Gold
	float ___m_fSkillColdDownCount_Gold_283;
	// System.Single Main::m_fSkillColdDown_Gold
	float ___m_fSkillColdDown_Gold_284;
	// System.Single Main::m_fSkillColdDownCount_Henzy
	float ___m_fSkillColdDownCount_Henzy_285;
	// System.Single Main::m_fSkillColdDown_Henzy
	float ___m_fSkillColdDown_Henzy_286;
	// System.Single Main::m_fSkillColdDownCount_MergeAll
	float ___m_fSkillColdDownCount_MergeAll_287;
	// System.Single Main::m_fSkillColdDown_MergeAll
	float ___m_fSkillColdDown_MergeAll_288;
	// System.Single Main::m_fSkillColdDownCount_MagicShield
	float ___m_fSkillColdDownCount_MagicShield_289;
	// System.Single Main::m_fSkillColdDown_MagicShield
	float ___m_fSkillColdDown_MagicShield_290;
	// System.Single Main::m_fSkillColdDown_Annihilate
	float ___m_fSkillColdDown_Annihilate_291;
	// System.Single Main::m_fSkillColdDownCount_Annihilate
	float ___m_fSkillColdDownCount_Annihilate_292;
	// System.Single Main::m_fSkillColdDownCount_Sneak
	float ___m_fSkillColdDownCount_Sneak_293;
	// System.Single Main::m_fSkillColdDownCount
	float ___m_fSkillColdDownCount_294;
	// System.Single Main::m_fSpitBallHalfTimeCount
	float ___m_fSpitBallHalfTimeCount_295;
	// System.Boolean Main::m_bPreOneBtnSplit
	bool ___m_bPreOneBtnSplit_296;
	// System.Single Main::m_fPreOneBtnSplitTimeCount
	float ___m_fPreOneBtnSplitTimeCount_297;
	// System.Single Main::m_fCurOneBtnSplitDis
	float ___m_fCurOneBtnSplitDis_298;
	// System.Int32 Main::m_nSplitTouchFingerId
	int32_t ___m_nSplitTouchFingerId_299;
	// UnityEngine.Vector3 Main::m_vecSplitTouchStartPos
	Vector3_t3722313464  ___m_vecSplitTouchStartPos_300;
	// UnityEngine.Vector3 Main::m_vecCurSplitTouchPos
	Vector3_t3722313464  ___m_vecCurSplitTouchPos_301;
	// UnityEngine.Vector2 Main::m_vecSplitDirction
	Vector2_t2156229523  ___m_vecSplitDirction_302;
	// System.Int32 Main::m_nInitingBallsCount
	int32_t ___m_nInitingBallsCount_303;
	// System.Single Main::m_fInitingBallsTime
	float ___m_fInitingBallsTime_304;
	// UnityEngine.Vector3 Main::m_vecStartPos
	Vector3_t3722313464  ___m_vecStartPos_305;
	// System.Int32 Main::m_nFingerId
	int32_t ___m_nFingerId_306;
	// System.Boolean Main::m_bSpitTouching
	bool ___m_bSpitTouching_307;
	// UnityEngine.Vector3 Main::m_vecCurPos
	Vector3_t3722313464  ___m_vecCurPos_308;
	// UnityEngine.Vector2 Main::m_vecSpitDirction
	Vector2_t2156229523  ___m_vecSpitDirction_309;
	// System.Boolean Main::m_bSpitTouching_Test
	bool ___m_bSpitTouching_Test_310;
	// System.Single Main::m_fLeftTime
	float ___m_fLeftTime_311;
	// System.Single Main::m_fGameStartTime
	float ___m_fGameStartTime_312;
	// System.Single Main::m_fTimeEclapse
	float ___m_fTimeEclapse_313;
	// System.Single Main::m_fProcessLeftTimeCount
	float ___m_fProcessLeftTimeCount_314;
	// System.Boolean Main::m_bGameOver
	bool ___m_bGameOver_315;
	// System.Boolean Main::m_bGameOverCounting
	bool ___m_bGameOverCounting_316;
	// System.Single Main::m_fGameOverPanelTimeLapse
	float ___m_fGameOverPanelTimeLapse_317;
	// System.Single Main::m_fScreenWidth
	float ___m_fScreenWidth_318;
	// System.Single Main::m_fScreenHeight
	float ___m_fScreenHeight_319;
	// UnityEngine.UI.InputField Main::_inputCheatPanelExplodeThornId
	InputField_t3762917431 * ____inputCheatPanelExplodeThornId_320;
	// System.String Main::m_szCheatExplodeThornId
	String_t* ___m_szCheatExplodeThornId_321;

public:
	inline static int32_t get_offset_of_m_fEjectHuanTing_3() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fEjectHuanTing_3)); }
	inline float get_m_fEjectHuanTing_3() const { return ___m_fEjectHuanTing_3; }
	inline float* get_address_of_m_fEjectHuanTing_3() { return &___m_fEjectHuanTing_3; }
	inline void set_m_fEjectHuanTing_3(float value)
	{
		___m_fEjectHuanTing_3 = value;
	}

	inline static int32_t get_offset_of_m_fA_4() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fA_4)); }
	inline float get_m_fA_4() const { return ___m_fA_4; }
	inline float* get_address_of_m_fA_4() { return &___m_fA_4; }
	inline void set_m_fA_4(float value)
	{
		___m_fA_4 = value;
	}

	inline static int32_t get_offset_of_m_fB_5() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fB_5)); }
	inline float get_m_fB_5() const { return ___m_fB_5; }
	inline float* get_address_of_m_fB_5() { return &___m_fB_5; }
	inline void set_m_fB_5(float value)
	{
		___m_fB_5 = value;
	}

	inline static int32_t get_offset_of_m_fX_6() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fX_6)); }
	inline float get_m_fX_6() const { return ___m_fX_6; }
	inline float* get_address_of_m_fX_6() { return &___m_fX_6; }
	inline void set_m_fX_6(float value)
	{
		___m_fX_6 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerNameBaseSize_9() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fPlayerNameBaseSize_9)); }
	inline float get_m_fPlayerNameBaseSize_9() const { return ___m_fPlayerNameBaseSize_9; }
	inline float* get_address_of_m_fPlayerNameBaseSize_9() { return &___m_fPlayerNameBaseSize_9; }
	inline void set_m_fPlayerNameBaseSize_9(float value)
	{
		___m_fPlayerNameBaseSize_9 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerNameMaxThreshold_10() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fPlayerNameMaxThreshold_10)); }
	inline float get_m_fPlayerNameMaxThreshold_10() const { return ___m_fPlayerNameMaxThreshold_10; }
	inline float* get_address_of_m_fPlayerNameMaxThreshold_10() { return &___m_fPlayerNameMaxThreshold_10; }
	inline void set_m_fPlayerNameMaxThreshold_10(float value)
	{
		___m_fPlayerNameMaxThreshold_10 = value;
	}

	inline static int32_t get_offset_of_m_fSporeMaxSize_11() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSporeMaxSize_11)); }
	inline float get_m_fSporeMaxSize_11() const { return ___m_fSporeMaxSize_11; }
	inline float* get_address_of_m_fSporeMaxSize_11() { return &___m_fSporeMaxSize_11; }
	inline void set_m_fSporeMaxSize_11(float value)
	{
		___m_fSporeMaxSize_11 = value;
	}

	inline static int32_t get_offset_of_m_fGameOverPanelTime_12() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGameOverPanelTime_12)); }
	inline float get_m_fGameOverPanelTime_12() const { return ___m_fGameOverPanelTime_12; }
	inline float* get_address_of_m_fGameOverPanelTime_12() { return &___m_fGameOverPanelTime_12; }
	inline void set_m_fGameOverPanelTime_12(float value)
	{
		___m_fGameOverPanelTime_12 = value;
	}

	inline static int32_t get_offset_of_m_nPlatform_13() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nPlatform_13)); }
	inline int32_t get_m_nPlatform_13() const { return ___m_nPlatform_13; }
	inline int32_t* get_address_of_m_nPlatform_13() { return &___m_nPlatform_13; }
	inline void set_m_nPlatform_13(int32_t value)
	{
		___m_nPlatform_13 = value;
	}

	inline static int32_t get_offset_of_m_fDistanceToSlowDown_14() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fDistanceToSlowDown_14)); }
	inline float get_m_fDistanceToSlowDown_14() const { return ___m_fDistanceToSlowDown_14; }
	inline float* get_address_of_m_fDistanceToSlowDown_14() { return &___m_fDistanceToSlowDown_14; }
	inline void set_m_fDistanceToSlowDown_14(float value)
	{
		___m_fDistanceToSlowDown_14 = value;
	}

	inline static int32_t get_offset_of_m_fBiggestBallAsCenterThreshold_15() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBiggestBallAsCenterThreshold_15)); }
	inline float get_m_fBiggestBallAsCenterThreshold_15() const { return ___m_fBiggestBallAsCenterThreshold_15; }
	inline float* get_address_of_m_fBiggestBallAsCenterThreshold_15() { return &___m_fBiggestBallAsCenterThreshold_15; }
	inline void set_m_fBiggestBallAsCenterThreshold_15(float value)
	{
		___m_fBiggestBallAsCenterThreshold_15 = value;
	}

	inline static int32_t get_offset_of_m_fSizeChangeSpeed_16() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSizeChangeSpeed_16)); }
	inline float get_m_fSizeChangeSpeed_16() const { return ___m_fSizeChangeSpeed_16; }
	inline float* get_address_of_m_fSizeChangeSpeed_16() { return &___m_fSizeChangeSpeed_16; }
	inline void set_m_fSizeChangeSpeed_16(float value)
	{
		___m_fSizeChangeSpeed_16 = value;
	}

	inline static int32_t get_offset_of_m_fSizeChangeSpeed_UnfoldSkill_17() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSizeChangeSpeed_UnfoldSkill_17)); }
	inline float get_m_fSizeChangeSpeed_UnfoldSkill_17() const { return ___m_fSizeChangeSpeed_UnfoldSkill_17; }
	inline float* get_address_of_m_fSizeChangeSpeed_UnfoldSkill_17() { return &___m_fSizeChangeSpeed_UnfoldSkill_17; }
	inline void set_m_fSizeChangeSpeed_UnfoldSkill_17(float value)
	{
		___m_fSizeChangeSpeed_UnfoldSkill_17 = value;
	}

	inline static int32_t get_offset_of_m_aryUnfoldArcStartAngle_18() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_aryUnfoldArcStartAngle_18)); }
	inline Int32U5BU5D_t385246372* get_m_aryUnfoldArcStartAngle_18() const { return ___m_aryUnfoldArcStartAngle_18; }
	inline Int32U5BU5D_t385246372** get_address_of_m_aryUnfoldArcStartAngle_18() { return &___m_aryUnfoldArcStartAngle_18; }
	inline void set_m_aryUnfoldArcStartAngle_18(Int32U5BU5D_t385246372* value)
	{
		___m_aryUnfoldArcStartAngle_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUnfoldArcStartAngle_18), value);
	}

	inline static int32_t get_offset_of__goGameOver_19() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____goGameOver_19)); }
	inline GameObject_t1113636619 * get__goGameOver_19() const { return ____goGameOver_19; }
	inline GameObject_t1113636619 ** get_address_of__goGameOver_19() { return &____goGameOver_19; }
	inline void set__goGameOver_19(GameObject_t1113636619 * value)
	{
		____goGameOver_19 = value;
		Il2CppCodeGenWriteBarrier((&____goGameOver_19), value);
	}

	inline static int32_t get_offset_of__goBallsCenter_20() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____goBallsCenter_20)); }
	inline GameObject_t1113636619 * get__goBallsCenter_20() const { return ____goBallsCenter_20; }
	inline GameObject_t1113636619 ** get_address_of__goBallsCenter_20() { return &____goBallsCenter_20; }
	inline void set__goBallsCenter_20(GameObject_t1113636619 * value)
	{
		____goBallsCenter_20 = value;
		Il2CppCodeGenWriteBarrier((&____goBallsCenter_20), value);
	}

	inline static int32_t get_offset_of_m_fCamRaiseAccelerate_21() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamRaiseAccelerate_21)); }
	inline float get_m_fCamRaiseAccelerate_21() const { return ___m_fCamRaiseAccelerate_21; }
	inline float* get_address_of_m_fCamRaiseAccelerate_21() { return &___m_fCamRaiseAccelerate_21; }
	inline void set_m_fCamRaiseAccelerate_21(float value)
	{
		___m_fCamRaiseAccelerate_21 = value;
	}

	inline static int32_t get_offset_of_m_fCamDownAccelerate_22() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamDownAccelerate_22)); }
	inline float get_m_fCamDownAccelerate_22() const { return ___m_fCamDownAccelerate_22; }
	inline float* get_address_of_m_fCamDownAccelerate_22() { return &___m_fCamDownAccelerate_22; }
	inline void set_m_fCamDownAccelerate_22(float value)
	{
		___m_fCamDownAccelerate_22 = value;
	}

	inline static int32_t get_offset_of_m_fCamChangeDelaySpit_23() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamChangeDelaySpit_23)); }
	inline float get_m_fCamChangeDelaySpit_23() const { return ___m_fCamChangeDelaySpit_23; }
	inline float* get_address_of_m_fCamChangeDelaySpit_23() { return &___m_fCamChangeDelaySpit_23; }
	inline void set_m_fCamChangeDelaySpit_23(float value)
	{
		___m_fCamChangeDelaySpit_23 = value;
	}

	inline static int32_t get_offset_of_m_fCamChangeDelayExplode_24() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamChangeDelayExplode_24)); }
	inline float get_m_fCamChangeDelayExplode_24() const { return ___m_fCamChangeDelayExplode_24; }
	inline float* get_address_of_m_fCamChangeDelayExplode_24() { return &___m_fCamChangeDelayExplode_24; }
	inline void set_m_fCamChangeDelayExplode_24(float value)
	{
		___m_fCamChangeDelayExplode_24 = value;
	}

	inline static int32_t get_offset_of_m_fRaiseSpeed_25() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fRaiseSpeed_25)); }
	inline float get_m_fRaiseSpeed_25() const { return ___m_fRaiseSpeed_25; }
	inline float* get_address_of_m_fRaiseSpeed_25() { return &___m_fRaiseSpeed_25; }
	inline void set_m_fRaiseSpeed_25(float value)
	{
		___m_fRaiseSpeed_25 = value;
	}

	inline static int32_t get_offset_of_m_fDownSpeed_26() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fDownSpeed_26)); }
	inline float get_m_fDownSpeed_26() const { return ___m_fDownSpeed_26; }
	inline float* get_address_of_m_fDownSpeed_26() { return &___m_fDownSpeed_26; }
	inline void set_m_fDownSpeed_26(float value)
	{
		___m_fDownSpeed_26 = value;
	}

	inline static int32_t get_offset_of_m_fCamRespondSpeed_27() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamRespondSpeed_27)); }
	inline float get_m_fCamRespondSpeed_27() const { return ___m_fCamRespondSpeed_27; }
	inline float* get_address_of_m_fCamRespondSpeed_27() { return &___m_fCamRespondSpeed_27; }
	inline void set_m_fCamRespondSpeed_27(float value)
	{
		___m_fCamRespondSpeed_27 = value;
	}

	inline static int32_t get_offset_of_m_fTimeBeforeCamDown_28() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fTimeBeforeCamDown_28)); }
	inline float get_m_fTimeBeforeCamDown_28() const { return ___m_fTimeBeforeCamDown_28; }
	inline float* get_address_of_m_fTimeBeforeCamDown_28() { return &___m_fTimeBeforeCamDown_28; }
	inline void set_m_fTimeBeforeCamDown_28(float value)
	{
		___m_fTimeBeforeCamDown_28 = value;
	}

	inline static int32_t get_offset_of_m_fCamMinSize_29() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamMinSize_29)); }
	inline float get_m_fCamMinSize_29() const { return ___m_fCamMinSize_29; }
	inline float* get_address_of_m_fCamMinSize_29() { return &___m_fCamMinSize_29; }
	inline void set_m_fCamMinSize_29(float value)
	{
		___m_fCamMinSize_29 = value;
	}

	inline static int32_t get_offset_of_m_fCamChangeSpeed_30() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamChangeSpeed_30)); }
	inline float get_m_fCamChangeSpeed_30() const { return ___m_fCamChangeSpeed_30; }
	inline float* get_address_of_m_fCamChangeSpeed_30() { return &___m_fCamChangeSpeed_30; }
	inline void set_m_fCamChangeSpeed_30(float value)
	{
		___m_fCamChangeSpeed_30 = value;
	}

	inline static int32_t get_offset_of_m_fCamGuiWeiSpeed_31() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamGuiWeiSpeed_31)); }
	inline float get_m_fCamGuiWeiSpeed_31() const { return ___m_fCamGuiWeiSpeed_31; }
	inline float* get_address_of_m_fCamGuiWeiSpeed_31() { return &___m_fCamGuiWeiSpeed_31; }
	inline void set_m_fCamGuiWeiSpeed_31(float value)
	{
		___m_fCamGuiWeiSpeed_31 = value;
	}

	inline static int32_t get_offset_of_m_fShangFuXiShu_32() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fShangFuXiShu_32)); }
	inline float get_m_fShangFuXiShu_32() const { return ___m_fShangFuXiShu_32; }
	inline float* get_address_of_m_fShangFuXiShu_32() { return &___m_fShangFuXiShu_32; }
	inline void set_m_fShangFuXiShu_32(float value)
	{
		___m_fShangFuXiShu_32 = value;
	}

	inline static int32_t get_offset_of_m_fYiJianHeQiuSpeed_33() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fYiJianHeQiuSpeed_33)); }
	inline float get_m_fYiJianHeQiuSpeed_33() const { return ___m_fYiJianHeQiuSpeed_33; }
	inline float* get_address_of_m_fYiJianHeQiuSpeed_33() { return &___m_fYiJianHeQiuSpeed_33; }
	inline void set_m_fYiJianHeQiuSpeed_33(float value)
	{
		___m_fYiJianHeQiuSpeed_33 = value;
	}

	inline static int32_t get_offset_of_m_fCamSizeXiShu_34() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamSizeXiShu_34)); }
	inline float get_m_fCamSizeXiShu_34() const { return ___m_fCamSizeXiShu_34; }
	inline float* get_address_of_m_fCamSizeXiShu_34() { return &___m_fCamSizeXiShu_34; }
	inline void set_m_fCamSizeXiShu_34(float value)
	{
		___m_fCamSizeXiShu_34 = value;
	}

	inline static int32_t get_offset_of__goCheat_35() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____goCheat_35)); }
	inline CCheat_t3466918192 * get__goCheat_35() const { return ____goCheat_35; }
	inline CCheat_t3466918192 ** get_address_of__goCheat_35() { return &____goCheat_35; }
	inline void set__goCheat_35(CCheat_t3466918192 * value)
	{
		____goCheat_35 = value;
		Il2CppCodeGenWriteBarrier((&____goCheat_35), value);
	}

	inline static int32_t get_offset_of_m_goJueShengQiu_36() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goJueShengQiu_36)); }
	inline GameObject_t1113636619 * get_m_goJueShengQiu_36() const { return ___m_goJueShengQiu_36; }
	inline GameObject_t1113636619 ** get_address_of_m_goJueShengQiu_36() { return &___m_goJueShengQiu_36; }
	inline void set_m_goJueShengQiu_36(GameObject_t1113636619 * value)
	{
		___m_goJueShengQiu_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_goJueShengQiu_36), value);
	}

	inline static int32_t get_offset_of_m_camWarFog_37() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_camWarFog_37)); }
	inline Camera_t4157153871 * get_m_camWarFog_37() const { return ___m_camWarFog_37; }
	inline Camera_t4157153871 ** get_address_of_m_camWarFog_37() { return &___m_camWarFog_37; }
	inline void set_m_camWarFog_37(Camera_t4157153871 * value)
	{
		___m_camWarFog_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_camWarFog_37), value);
	}

	inline static int32_t get_offset_of_m_camForCalc_38() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_camForCalc_38)); }
	inline Camera_t4157153871 * get_m_camForCalc_38() const { return ___m_camForCalc_38; }
	inline Camera_t4157153871 ** get_address_of_m_camForCalc_38() { return &___m_camForCalc_38; }
	inline void set_m_camForCalc_38(Camera_t4157153871 * value)
	{
		___m_camForCalc_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_camForCalc_38), value);
	}

	inline static int32_t get_offset_of_g_SystemMsg_39() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___g_SystemMsg_39)); }
	inline SystemMsg_t1823497383 * get_g_SystemMsg_39() const { return ___g_SystemMsg_39; }
	inline SystemMsg_t1823497383 ** get_address_of_g_SystemMsg_39() { return &___g_SystemMsg_39; }
	inline void set_g_SystemMsg_39(SystemMsg_t1823497383 * value)
	{
		___g_SystemMsg_39 = value;
		Il2CppCodeGenWriteBarrier((&___g_SystemMsg_39), value);
	}

	inline static int32_t get_offset_of_m_aryBallSprite_40() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_aryBallSprite_40)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryBallSprite_40() const { return ___m_aryBallSprite_40; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryBallSprite_40() { return &___m_aryBallSprite_40; }
	inline void set_m_aryBallSprite_40(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryBallSprite_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBallSprite_40), value);
	}

	inline static int32_t get_offset_of_m_goHongBao_42() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goHongBao_42)); }
	inline GameObject_t1113636619 * get_m_goHongBao_42() const { return ___m_goHongBao_42; }
	inline GameObject_t1113636619 ** get_address_of_m_goHongBao_42() { return &___m_goHongBao_42; }
	inline void set_m_goHongBao_42(GameObject_t1113636619 * value)
	{
		___m_goHongBao_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_goHongBao_42), value);
	}

	inline static int32_t get_offset_of__playeraction_43() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____playeraction_43)); }
	inline PlayerAction_t3782228511 * get__playeraction_43() const { return ____playeraction_43; }
	inline PlayerAction_t3782228511 ** get_address_of__playeraction_43() { return &____playeraction_43; }
	inline void set__playeraction_43(PlayerAction_t3782228511 * value)
	{
		____playeraction_43 = value;
		Il2CppCodeGenWriteBarrier((&____playeraction_43), value);
	}

	inline static int32_t get_offset_of_m_ShengFuPanDingMonster_44() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_ShengFuPanDingMonster_44)); }
	inline CMonsterBall_t654437011 * get_m_ShengFuPanDingMonster_44() const { return ___m_ShengFuPanDingMonster_44; }
	inline CMonsterBall_t654437011 ** get_address_of_m_ShengFuPanDingMonster_44() { return &___m_ShengFuPanDingMonster_44; }
	inline void set_m_ShengFuPanDingMonster_44(CMonsterBall_t654437011 * value)
	{
		___m_ShengFuPanDingMonster_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_ShengFuPanDingMonster_44), value);
	}

	inline static int32_t get_offset_of_m_preGridLine_45() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preGridLine_45)); }
	inline GameObject_t1113636619 * get_m_preGridLine_45() const { return ___m_preGridLine_45; }
	inline GameObject_t1113636619 ** get_address_of_m_preGridLine_45() { return &___m_preGridLine_45; }
	inline void set_m_preGridLine_45(GameObject_t1113636619 * value)
	{
		___m_preGridLine_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGridLine_45), value);
	}

	inline static int32_t get_offset_of_m_preBallLine_46() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preBallLine_46)); }
	inline GameObject_t1113636619 * get_m_preBallLine_46() const { return ___m_preBallLine_46; }
	inline GameObject_t1113636619 ** get_address_of_m_preBallLine_46() { return &___m_preBallLine_46; }
	inline void set_m_preBallLine_46(GameObject_t1113636619 * value)
	{
		___m_preBallLine_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBallLine_46), value);
	}

	inline static int32_t get_offset_of_m_preBall_47() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preBall_47)); }
	inline GameObject_t1113636619 * get_m_preBall_47() const { return ___m_preBall_47; }
	inline GameObject_t1113636619 ** get_address_of_m_preBall_47() { return &___m_preBall_47; }
	inline void set_m_preBall_47(GameObject_t1113636619 * value)
	{
		___m_preBall_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBall_47), value);
	}

	inline static int32_t get_offset_of_m_preBallTest_48() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preBallTest_48)); }
	inline GameObject_t1113636619 * get_m_preBallTest_48() const { return ___m_preBallTest_48; }
	inline GameObject_t1113636619 ** get_address_of_m_preBallTest_48() { return &___m_preBallTest_48; }
	inline void set_m_preBallTest_48(GameObject_t1113636619 * value)
	{
		___m_preBallTest_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBallTest_48), value);
	}

	inline static int32_t get_offset_of_m_preElectronicBall_49() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preElectronicBall_49)); }
	inline GameObject_t1113636619 * get_m_preElectronicBall_49() const { return ___m_preElectronicBall_49; }
	inline GameObject_t1113636619 ** get_address_of_m_preElectronicBall_49() { return &___m_preElectronicBall_49; }
	inline void set_m_preElectronicBall_49(GameObject_t1113636619 * value)
	{
		___m_preElectronicBall_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_preElectronicBall_49), value);
	}

	inline static int32_t get_offset_of_m_prePlayer_50() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_prePlayer_50)); }
	inline GameObject_t1113636619 * get_m_prePlayer_50() const { return ___m_prePlayer_50; }
	inline GameObject_t1113636619 ** get_address_of_m_prePlayer_50() { return &___m_prePlayer_50; }
	inline void set_m_prePlayer_50(GameObject_t1113636619 * value)
	{
		___m_prePlayer_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePlayer_50), value);
	}

	inline static int32_t get_offset_of_m_preThorn_51() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preThorn_51)); }
	inline GameObject_t1113636619 * get_m_preThorn_51() const { return ___m_preThorn_51; }
	inline GameObject_t1113636619 ** get_address_of_m_preThorn_51() { return &___m_preThorn_51; }
	inline void set_m_preThorn_51(GameObject_t1113636619 * value)
	{
		___m_preThorn_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_preThorn_51), value);
	}

	inline static int32_t get_offset_of_m_preRhythm_52() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preRhythm_52)); }
	inline GameObject_t1113636619 * get_m_preRhythm_52() const { return ___m_preRhythm_52; }
	inline GameObject_t1113636619 ** get_address_of_m_preRhythm_52() { return &___m_preRhythm_52; }
	inline void set_m_preRhythm_52(GameObject_t1113636619 * value)
	{
		___m_preRhythm_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_preRhythm_52), value);
	}

	inline static int32_t get_offset_of_m_preBean_53() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preBean_53)); }
	inline GameObject_t1113636619 * get_m_preBean_53() const { return ___m_preBean_53; }
	inline GameObject_t1113636619 ** get_address_of_m_preBean_53() { return &___m_preBean_53; }
	inline void set_m_preBean_53(GameObject_t1113636619 * value)
	{
		___m_preBean_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBean_53), value);
	}

	inline static int32_t get_offset_of_m_preEffectPreExplode_54() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preEffectPreExplode_54)); }
	inline GameObject_t1113636619 * get_m_preEffectPreExplode_54() const { return ___m_preEffectPreExplode_54; }
	inline GameObject_t1113636619 ** get_address_of_m_preEffectPreExplode_54() { return &___m_preEffectPreExplode_54; }
	inline void set_m_preEffectPreExplode_54(GameObject_t1113636619 * value)
	{
		___m_preEffectPreExplode_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_preEffectPreExplode_54), value);
	}

	inline static int32_t get_offset_of_m_preEffectExplode_55() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preEffectExplode_55)); }
	inline GameObject_t1113636619 * get_m_preEffectExplode_55() const { return ___m_preEffectExplode_55; }
	inline GameObject_t1113636619 ** get_address_of_m_preEffectExplode_55() { return &___m_preEffectExplode_55; }
	inline void set_m_preEffectExplode_55(GameObject_t1113636619 * value)
	{
		___m_preEffectExplode_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_preEffectExplode_55), value);
	}

	inline static int32_t get_offset_of_m_preBlackHole_56() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preBlackHole_56)); }
	inline GameObject_t1113636619 * get_m_preBlackHole_56() const { return ___m_preBlackHole_56; }
	inline GameObject_t1113636619 ** get_address_of_m_preBlackHole_56() { return &___m_preBlackHole_56; }
	inline void set_m_preBlackHole_56(GameObject_t1113636619 * value)
	{
		___m_preBlackHole_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBlackHole_56), value);
	}

	inline static int32_t get_offset_of_m_preSpitBallTarget_57() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preSpitBallTarget_57)); }
	inline GameObject_t1113636619 * get_m_preSpitBallTarget_57() const { return ___m_preSpitBallTarget_57; }
	inline GameObject_t1113636619 ** get_address_of_m_preSpitBallTarget_57() { return &___m_preSpitBallTarget_57; }
	inline void set_m_preSpitBallTarget_57(GameObject_t1113636619 * value)
	{
		___m_preSpitBallTarget_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSpitBallTarget_57), value);
	}

	inline static int32_t get_offset_of_m_preSpore_58() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preSpore_58)); }
	inline GameObject_t1113636619 * get_m_preSpore_58() const { return ___m_preSpore_58; }
	inline GameObject_t1113636619 ** get_address_of_m_preSpore_58() { return &___m_preSpore_58; }
	inline void set_m_preSpore_58(GameObject_t1113636619 * value)
	{
		___m_preSpore_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSpore_58), value);
	}

	inline static int32_t get_offset_of_m_preNail_59() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preNail_59)); }
	inline GameObject_t1113636619 * get_m_preNail_59() const { return ___m_preNail_59; }
	inline GameObject_t1113636619 ** get_address_of_m_preNail_59() { return &___m_preNail_59; }
	inline void set_m_preNail_59(GameObject_t1113636619 * value)
	{
		___m_preNail_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_preNail_59), value);
	}

	inline static int32_t get_offset_of_m_preVoice_60() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preVoice_60)); }
	inline GameObject_t1113636619 * get_m_preVoice_60() const { return ___m_preVoice_60; }
	inline GameObject_t1113636619 ** get_address_of_m_preVoice_60() { return &___m_preVoice_60; }
	inline void set_m_preVoice_60(GameObject_t1113636619 * value)
	{
		___m_preVoice_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_preVoice_60), value);
	}

	inline static int32_t get_offset_of_m_preGroup_61() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preGroup_61)); }
	inline GameObject_t1113636619 * get_m_preGroup_61() const { return ___m_preGroup_61; }
	inline GameObject_t1113636619 ** get_address_of_m_preGroup_61() { return &___m_preGroup_61; }
	inline void set_m_preGroup_61(GameObject_t1113636619 * value)
	{
		___m_preGroup_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGroup_61), value);
	}

	inline static int32_t get_offset_of_m_preOneWayObstacle_62() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preOneWayObstacle_62)); }
	inline GameObject_t1113636619 * get_m_preOneWayObstacle_62() const { return ___m_preOneWayObstacle_62; }
	inline GameObject_t1113636619 ** get_address_of_m_preOneWayObstacle_62() { return &___m_preOneWayObstacle_62; }
	inline void set_m_preOneWayObstacle_62(GameObject_t1113636619 * value)
	{
		___m_preOneWayObstacle_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_preOneWayObstacle_62), value);
	}

	inline static int32_t get_offset_of_m_preExplodeNode_63() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_preExplodeNode_63)); }
	inline GameObject_t1113636619 * get_m_preExplodeNode_63() const { return ___m_preExplodeNode_63; }
	inline GameObject_t1113636619 ** get_address_of_m_preExplodeNode_63() { return &___m_preExplodeNode_63; }
	inline void set_m_preExplodeNode_63(GameObject_t1113636619 * value)
	{
		___m_preExplodeNode_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_preExplodeNode_63), value);
	}

	inline static int32_t get_offset_of__panelRank_64() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____panelRank_64)); }
	inline GameObject_t1113636619 * get__panelRank_64() const { return ____panelRank_64; }
	inline GameObject_t1113636619 ** get_address_of__panelRank_64() { return &____panelRank_64; }
	inline void set__panelRank_64(GameObject_t1113636619 * value)
	{
		____panelRank_64 = value;
		Il2CppCodeGenWriteBarrier((&____panelRank_64), value);
	}

	inline static int32_t get_offset_of__panelSkillPoint_65() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____panelSkillPoint_65)); }
	inline GameObject_t1113636619 * get__panelSkillPoint_65() const { return ____panelSkillPoint_65; }
	inline GameObject_t1113636619 ** get_address_of__panelSkillPoint_65() { return &____panelSkillPoint_65; }
	inline void set__panelSkillPoint_65(GameObject_t1113636619 * value)
	{
		____panelSkillPoint_65 = value;
		Il2CppCodeGenWriteBarrier((&____panelSkillPoint_65), value);
	}

	inline static int32_t get_offset_of__msglistKillPeople_66() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____msglistKillPeople_66)); }
	inline CMsgList_t4144960221 * get__msglistKillPeople_66() const { return ____msglistKillPeople_66; }
	inline CMsgList_t4144960221 ** get_address_of__msglistKillPeople_66() { return &____msglistKillPeople_66; }
	inline void set__msglistKillPeople_66(CMsgList_t4144960221 * value)
	{
		____msglistKillPeople_66 = value;
		Il2CppCodeGenWriteBarrier((&____msglistKillPeople_66), value);
	}

	inline static int32_t get_offset_of__txtTimeLeft_67() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____txtTimeLeft_67)); }
	inline Text_t1901882714 * get__txtTimeLeft_67() const { return ____txtTimeLeft_67; }
	inline Text_t1901882714 ** get_address_of__txtTimeLeft_67() { return &____txtTimeLeft_67; }
	inline void set__txtTimeLeft_67(Text_t1901882714 * value)
	{
		____txtTimeLeft_67 = value;
		Il2CppCodeGenWriteBarrier((&____txtTimeLeft_67), value);
	}

	inline static int32_t get_offset_of__txtTimeLeft_MOBILE_68() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____txtTimeLeft_MOBILE_68)); }
	inline Text_t1901882714 * get__txtTimeLeft_MOBILE_68() const { return ____txtTimeLeft_MOBILE_68; }
	inline Text_t1901882714 ** get_address_of__txtTimeLeft_MOBILE_68() { return &____txtTimeLeft_MOBILE_68; }
	inline void set__txtTimeLeft_MOBILE_68(Text_t1901882714 * value)
	{
		____txtTimeLeft_MOBILE_68 = value;
		Il2CppCodeGenWriteBarrier((&____txtTimeLeft_MOBILE_68), value);
	}

	inline static int32_t get_offset_of__txtTimeLeft_PC_69() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____txtTimeLeft_PC_69)); }
	inline Text_t1901882714 * get__txtTimeLeft_PC_69() const { return ____txtTimeLeft_PC_69; }
	inline Text_t1901882714 ** get_address_of__txtTimeLeft_PC_69() { return &____txtTimeLeft_PC_69; }
	inline void set__txtTimeLeft_PC_69(Text_t1901882714 * value)
	{
		____txtTimeLeft_PC_69 = value;
		Il2CppCodeGenWriteBarrier((&____txtTimeLeft_PC_69), value);
	}

	inline static int32_t get_offset_of__uiDead_70() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____uiDead_70)); }
	inline GameObject_t1113636619 * get__uiDead_70() const { return ____uiDead_70; }
	inline GameObject_t1113636619 ** get_address_of__uiDead_70() { return &____uiDead_70; }
	inline void set__uiDead_70(GameObject_t1113636619 * value)
	{
		____uiDead_70 = value;
		Il2CppCodeGenWriteBarrier((&____uiDead_70), value);
	}

	inline static int32_t get_offset_of__uiDeadNew_71() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____uiDeadNew_71)); }
	inline GameObject_t1113636619 * get__uiDeadNew_71() const { return ____uiDeadNew_71; }
	inline GameObject_t1113636619 ** get_address_of__uiDeadNew_71() { return &____uiDeadNew_71; }
	inline void set__uiDeadNew_71(GameObject_t1113636619 * value)
	{
		____uiDeadNew_71 = value;
		Il2CppCodeGenWriteBarrier((&____uiDeadNew_71), value);
	}

	inline static int32_t get_offset_of__uiGameOver_72() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____uiGameOver_72)); }
	inline GameObject_t1113636619 * get__uiGameOver_72() const { return ____uiGameOver_72; }
	inline GameObject_t1113636619 ** get_address_of__uiGameOver_72() { return &____uiGameOver_72; }
	inline void set__uiGameOver_72(GameObject_t1113636619 * value)
	{
		____uiGameOver_72 = value;
		Il2CppCodeGenWriteBarrier((&____uiGameOver_72), value);
	}

	inline static int32_t get_offset_of_m_txtDebugInfo_73() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtDebugInfo_73)); }
	inline Text_t1901882714 * get_m_txtDebugInfo_73() const { return ___m_txtDebugInfo_73; }
	inline Text_t1901882714 ** get_address_of_m_txtDebugInfo_73() { return &___m_txtDebugInfo_73; }
	inline void set_m_txtDebugInfo_73(Text_t1901882714 * value)
	{
		___m_txtDebugInfo_73 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtDebugInfo_73), value);
	}

	inline static int32_t get_offset_of_m_sliderStickSize_74() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_sliderStickSize_74)); }
	inline Slider_t3903728902 * get_m_sliderStickSize_74() const { return ___m_sliderStickSize_74; }
	inline Slider_t3903728902 ** get_address_of_m_sliderStickSize_74() { return &___m_sliderStickSize_74; }
	inline void set_m_sliderStickSize_74(Slider_t3903728902 * value)
	{
		___m_sliderStickSize_74 = value;
		Il2CppCodeGenWriteBarrier((&___m_sliderStickSize_74), value);
	}

	inline static int32_t get_offset_of_m_sliderAccelerate_75() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_sliderAccelerate_75)); }
	inline Slider_t3903728902 * get_m_sliderAccelerate_75() const { return ___m_sliderAccelerate_75; }
	inline Slider_t3903728902 ** get_address_of_m_sliderAccelerate_75() { return &___m_sliderAccelerate_75; }
	inline void set_m_sliderAccelerate_75(Slider_t3903728902 * value)
	{
		___m_sliderAccelerate_75 = value;
		Il2CppCodeGenWriteBarrier((&___m_sliderAccelerate_75), value);
	}

	inline static int32_t get_offset_of_m_btnSpit_76() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_btnSpit_76)); }
	inline Button_t4055032469 * get_m_btnSpit_76() const { return ___m_btnSpit_76; }
	inline Button_t4055032469 ** get_address_of_m_btnSpit_76() { return &___m_btnSpit_76; }
	inline void set_m_btnSpit_76(Button_t4055032469 * value)
	{
		___m_btnSpit_76 = value;
		Il2CppCodeGenWriteBarrier((&___m_btnSpit_76), value);
	}

	inline static int32_t get_offset_of_m_sbSpitSpore_77() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_sbSpitSpore_77)); }
	inline Scrollbar_t1494447233 * get_m_sbSpitSpore_77() const { return ___m_sbSpitSpore_77; }
	inline Scrollbar_t1494447233 ** get_address_of_m_sbSpitSpore_77() { return &___m_sbSpitSpore_77; }
	inline void set_m_sbSpitSpore_77(Scrollbar_t1494447233 * value)
	{
		___m_sbSpitSpore_77 = value;
		Il2CppCodeGenWriteBarrier((&___m_sbSpitSpore_77), value);
	}

	inline static int32_t get_offset_of_m_sbSpitBall_78() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_sbSpitBall_78)); }
	inline Scrollbar_t1494447233 * get_m_sbSpitBall_78() const { return ___m_sbSpitBall_78; }
	inline Scrollbar_t1494447233 ** get_address_of_m_sbSpitBall_78() { return &___m_sbSpitBall_78; }
	inline void set_m_sbSpitBall_78(Scrollbar_t1494447233 * value)
	{
		___m_sbSpitBall_78 = value;
		Il2CppCodeGenWriteBarrier((&___m_sbSpitBall_78), value);
	}

	inline static int32_t get_offset_of_m_sbUnfold_79() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_sbUnfold_79)); }
	inline Scrollbar_t1494447233 * get_m_sbUnfold_79() const { return ___m_sbUnfold_79; }
	inline Scrollbar_t1494447233 ** get_address_of_m_sbUnfold_79() { return &___m_sbUnfold_79; }
	inline void set_m_sbUnfold_79(Scrollbar_t1494447233 * value)
	{
		___m_sbUnfold_79 = value;
		Il2CppCodeGenWriteBarrier((&___m_sbUnfold_79), value);
	}

	inline static int32_t get_offset_of_m_uiGamePanel_80() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_uiGamePanel_80)); }
	inline GameObject_t1113636619 * get_m_uiGamePanel_80() const { return ___m_uiGamePanel_80; }
	inline GameObject_t1113636619 ** get_address_of_m_uiGamePanel_80() { return &___m_uiGamePanel_80; }
	inline void set_m_uiGamePanel_80(GameObject_t1113636619 * value)
	{
		___m_uiGamePanel_80 = value;
		Il2CppCodeGenWriteBarrier((&___m_uiGamePanel_80), value);
	}

	inline static int32_t get_offset_of_m_uiMapEditorPanel_81() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_uiMapEditorPanel_81)); }
	inline GameObject_t1113636619 * get_m_uiMapEditorPanel_81() const { return ___m_uiMapEditorPanel_81; }
	inline GameObject_t1113636619 ** get_address_of_m_uiMapEditorPanel_81() { return &___m_uiMapEditorPanel_81; }
	inline void set_m_uiMapEditorPanel_81(GameObject_t1113636619 * value)
	{
		___m_uiMapEditorPanel_81 = value;
		Il2CppCodeGenWriteBarrier((&___m_uiMapEditorPanel_81), value);
	}

	inline static int32_t get_offset_of_m_imgSpitBall_Fill_82() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_imgSpitBall_Fill_82)); }
	inline Image_t2670269651 * get_m_imgSpitBall_Fill_82() const { return ___m_imgSpitBall_Fill_82; }
	inline Image_t2670269651 ** get_address_of_m_imgSpitBall_Fill_82() { return &___m_imgSpitBall_Fill_82; }
	inline void set_m_imgSpitBall_Fill_82(Image_t2670269651 * value)
	{
		___m_imgSpitBall_Fill_82 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgSpitBall_Fill_82), value);
	}

	inline static int32_t get_offset_of_m_txtColdDown_W_83() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtColdDown_W_83)); }
	inline Text_t1901882714 * get_m_txtColdDown_W_83() const { return ___m_txtColdDown_W_83; }
	inline Text_t1901882714 ** get_address_of_m_txtColdDown_W_83() { return &___m_txtColdDown_W_83; }
	inline void set_m_txtColdDown_W_83(Text_t1901882714 * value)
	{
		___m_txtColdDown_W_83 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtColdDown_W_83), value);
	}

	inline static int32_t get_offset_of_m_txtColdDown_E_84() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtColdDown_E_84)); }
	inline Text_t1901882714 * get_m_txtColdDown_E_84() const { return ___m_txtColdDown_E_84; }
	inline Text_t1901882714 ** get_address_of_m_txtColdDown_E_84() { return &___m_txtColdDown_E_84; }
	inline void set_m_txtColdDown_E_84(Text_t1901882714 * value)
	{
		___m_txtColdDown_E_84 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtColdDown_E_84), value);
	}

	inline static int32_t get_offset_of_m_txtColdDown_R_85() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtColdDown_R_85)); }
	inline Text_t1901882714 * get_m_txtColdDown_R_85() const { return ___m_txtColdDown_R_85; }
	inline Text_t1901882714 ** get_address_of_m_txtColdDown_R_85() { return &___m_txtColdDown_R_85; }
	inline void set_m_txtColdDown_R_85(Text_t1901882714 * value)
	{
		___m_txtColdDown_R_85 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtColdDown_R_85), value);
	}

	inline static int32_t get_offset_of_m_txtColdDown_W_Mobile_86() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtColdDown_W_Mobile_86)); }
	inline Text_t1901882714 * get_m_txtColdDown_W_Mobile_86() const { return ___m_txtColdDown_W_Mobile_86; }
	inline Text_t1901882714 ** get_address_of_m_txtColdDown_W_Mobile_86() { return &___m_txtColdDown_W_Mobile_86; }
	inline void set_m_txtColdDown_W_Mobile_86(Text_t1901882714 * value)
	{
		___m_txtColdDown_W_Mobile_86 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtColdDown_W_Mobile_86), value);
	}

	inline static int32_t get_offset_of_m_txtColdDown_E_Mobile_87() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtColdDown_E_Mobile_87)); }
	inline Text_t1901882714 * get_m_txtColdDown_E_Mobile_87() const { return ___m_txtColdDown_E_Mobile_87; }
	inline Text_t1901882714 ** get_address_of_m_txtColdDown_E_Mobile_87() { return &___m_txtColdDown_E_Mobile_87; }
	inline void set_m_txtColdDown_E_Mobile_87(Text_t1901882714 * value)
	{
		___m_txtColdDown_E_Mobile_87 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtColdDown_E_Mobile_87), value);
	}

	inline static int32_t get_offset_of_m_txtColdDown_R_Mobile_88() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_txtColdDown_R_Mobile_88)); }
	inline Text_t1901882714 * get_m_txtColdDown_R_Mobile_88() const { return ___m_txtColdDown_R_Mobile_88; }
	inline Text_t1901882714 ** get_address_of_m_txtColdDown_R_Mobile_88() { return &___m_txtColdDown_R_Mobile_88; }
	inline void set_m_txtColdDown_R_Mobile_88(Text_t1901882714 * value)
	{
		___m_txtColdDown_R_Mobile_88 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtColdDown_R_Mobile_88), value);
	}

	inline static int32_t get_offset_of_m_imgSpitSpore_Fill_89() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_imgSpitSpore_Fill_89)); }
	inline Image_t2670269651 * get_m_imgSpitSpore_Fill_89() const { return ___m_imgSpitSpore_Fill_89; }
	inline Image_t2670269651 ** get_address_of_m_imgSpitSpore_Fill_89() { return &___m_imgSpitSpore_Fill_89; }
	inline void set_m_imgSpitSpore_Fill_89(Image_t2670269651 * value)
	{
		___m_imgSpitSpore_Fill_89 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgSpitSpore_Fill_89), value);
	}

	inline static int32_t get_offset_of_m_imgUnfold_Fill_90() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_imgUnfold_Fill_90)); }
	inline Image_t2670269651 * get_m_imgUnfold_Fill_90() const { return ___m_imgUnfold_Fill_90; }
	inline Image_t2670269651 ** get_address_of_m_imgUnfold_Fill_90() { return &___m_imgUnfold_Fill_90; }
	inline void set_m_imgUnfold_Fill_90(Image_t2670269651 * value)
	{
		___m_imgUnfold_Fill_90 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgUnfold_Fill_90), value);
	}

	inline static int32_t get_offset_of_m_imgOneBtnSplit_Fill_91() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_imgOneBtnSplit_Fill_91)); }
	inline Image_t2670269651 * get_m_imgOneBtnSplit_Fill_91() const { return ___m_imgOneBtnSplit_Fill_91; }
	inline Image_t2670269651 ** get_address_of_m_imgOneBtnSplit_Fill_91() { return &___m_imgOneBtnSplit_Fill_91; }
	inline void set_m_imgOneBtnSplit_Fill_91(Image_t2670269651 * value)
	{
		___m_imgOneBtnSplit_Fill_91 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgOneBtnSplit_Fill_91), value);
	}

	inline static int32_t get_offset_of_m_imgSelectedSkill_Fill_92() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_imgSelectedSkill_Fill_92)); }
	inline Image_t2670269651 * get_m_imgSelectedSkill_Fill_92() const { return ___m_imgSelectedSkill_Fill_92; }
	inline Image_t2670269651 ** get_address_of_m_imgSelectedSkill_Fill_92() { return &___m_imgSelectedSkill_Fill_92; }
	inline void set_m_imgSelectedSkill_Fill_92(Image_t2670269651 * value)
	{
		___m_imgSelectedSkill_Fill_92 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgSelectedSkill_Fill_92), value);
	}

	inline static int32_t get_offset_of_m_SelectedSkillColdDownTimeCount_93() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_SelectedSkillColdDownTimeCount_93)); }
	inline float get_m_SelectedSkillColdDownTimeCount_93() const { return ___m_SelectedSkillColdDownTimeCount_93; }
	inline float* get_address_of_m_SelectedSkillColdDownTimeCount_93() { return &___m_SelectedSkillColdDownTimeCount_93; }
	inline void set_m_SelectedSkillColdDownTimeCount_93(float value)
	{
		___m_SelectedSkillColdDownTimeCount_93 = value;
	}

	inline static int32_t get_offset_of_m_SelectedSkillColdDownInterval_94() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_SelectedSkillColdDownInterval_94)); }
	inline float get_m_SelectedSkillColdDownInterval_94() const { return ___m_SelectedSkillColdDownInterval_94; }
	inline float* get_address_of_m_SelectedSkillColdDownInterval_94() { return &___m_SelectedSkillColdDownInterval_94; }
	inline void set_m_SelectedSkillColdDownInterval_94(float value)
	{
		___m_SelectedSkillColdDownInterval_94 = value;
	}

	inline static int32_t get_offset_of_m_imgLababa_Fill_95() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_imgLababa_Fill_95)); }
	inline Image_t2670269651 * get_m_imgLababa_Fill_95() const { return ___m_imgLababa_Fill_95; }
	inline Image_t2670269651 ** get_address_of_m_imgLababa_Fill_95() { return &___m_imgLababa_Fill_95; }
	inline void set_m_imgLababa_Fill_95(Image_t2670269651 * value)
	{
		___m_imgLababa_Fill_95 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgLababa_Fill_95), value);
	}

	inline static int32_t get_offset_of_m_fBallTriggerBasrRadius_96() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBallTriggerBasrRadius_96)); }
	inline float get_m_fBallTriggerBasrRadius_96() const { return ___m_fBallTriggerBasrRadius_96; }
	inline float* get_address_of_m_fBallTriggerBasrRadius_96() { return &___m_fBallTriggerBasrRadius_96; }
	inline void set_m_fBallTriggerBasrRadius_96(float value)
	{
		___m_fBallTriggerBasrRadius_96 = value;
	}

	inline static int32_t get_offset_of_m_fPixel2Scale_97() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fPixel2Scale_97)); }
	inline float get_m_fPixel2Scale_97() const { return ___m_fPixel2Scale_97; }
	inline float* get_address_of_m_fPixel2Scale_97() { return &___m_fPixel2Scale_97; }
	inline void set_m_fPixel2Scale_97(float value)
	{
		___m_fPixel2Scale_97 = value;
	}

	inline static int32_t get_offset_of_m_fScale2Radius_98() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fScale2Radius_98)); }
	inline float get_m_fScale2Radius_98() const { return ___m_fScale2Radius_98; }
	inline float* get_address_of_m_fScale2Radius_98() { return &___m_fScale2Radius_98; }
	inline void set_m_fScale2Radius_98(float value)
	{
		___m_fScale2Radius_98 = value;
	}

	inline static int32_t get_offset_of_m_fRadius2Scale_99() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fRadius2Scale_99)); }
	inline float get_m_fRadius2Scale_99() const { return ___m_fRadius2Scale_99; }
	inline float* get_address_of_m_fRadius2Scale_99() { return &___m_fRadius2Scale_99; }
	inline void set_m_fRadius2Scale_99(float value)
	{
		___m_fRadius2Scale_99 = value;
	}

	inline static int32_t get_offset_of_m_nShit_100() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nShit_100)); }
	inline int32_t get_m_nShit_100() const { return ___m_nShit_100; }
	inline int32_t* get_address_of_m_nShit_100() { return &___m_nShit_100; }
	inline void set_m_nShit_100(int32_t value)
	{
		___m_nShit_100 = value;
	}

	inline static int32_t get_offset_of_m_fBallBaseSpeed_104() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBallBaseSpeed_104)); }
	inline float get_m_fBallBaseSpeed_104() const { return ___m_fBallBaseSpeed_104; }
	inline float* get_address_of_m_fBallBaseSpeed_104() { return &___m_fBallBaseSpeed_104; }
	inline void set_m_fBallBaseSpeed_104(float value)
	{
		___m_fBallBaseSpeed_104 = value;
	}

	inline static int32_t get_offset_of_m_fBallSpeedRadiusKaiFangFenMu_105() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBallSpeedRadiusKaiFangFenMu_105)); }
	inline float get_m_fBallSpeedRadiusKaiFangFenMu_105() const { return ___m_fBallSpeedRadiusKaiFangFenMu_105; }
	inline float* get_address_of_m_fBallSpeedRadiusKaiFangFenMu_105() { return &___m_fBallSpeedRadiusKaiFangFenMu_105; }
	inline void set_m_fBallSpeedRadiusKaiFangFenMu_105(float value)
	{
		___m_fBallSpeedRadiusKaiFangFenMu_105 = value;
	}

	inline static int32_t get_offset_of_m_fBallMoveToCenterBaseSpeed_106() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBallMoveToCenterBaseSpeed_106)); }
	inline float get_m_fBallMoveToCenterBaseSpeed_106() const { return ___m_fBallMoveToCenterBaseSpeed_106; }
	inline float* get_address_of_m_fBallMoveToCenterBaseSpeed_106() { return &___m_fBallMoveToCenterBaseSpeed_106; }
	inline void set_m_fBallMoveToCenterBaseSpeed_106(float value)
	{
		___m_fBallMoveToCenterBaseSpeed_106 = value;
	}

	inline static int32_t get_offset_of_m_nRebornPosIdx_107() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nRebornPosIdx_107)); }
	inline int32_t get_m_nRebornPosIdx_107() const { return ___m_nRebornPosIdx_107; }
	inline int32_t* get_address_of_m_nRebornPosIdx_107() { return &___m_nRebornPosIdx_107; }
	inline void set_m_nRebornPosIdx_107(int32_t value)
	{
		___m_nRebornPosIdx_107 = value;
	}

	inline static int32_t get_offset_of_m_fBaseVolumeyByItemDecreasedPercentWhenDead_108() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBaseVolumeyByItemDecreasedPercentWhenDead_108)); }
	inline float get_m_fBaseVolumeyByItemDecreasedPercentWhenDead_108() const { return ___m_fBaseVolumeyByItemDecreasedPercentWhenDead_108; }
	inline float* get_address_of_m_fBaseVolumeyByItemDecreasedPercentWhenDead_108() { return &___m_fBaseVolumeyByItemDecreasedPercentWhenDead_108; }
	inline void set_m_fBaseVolumeyByItemDecreasedPercentWhenDead_108(float value)
	{
		___m_fBaseVolumeyByItemDecreasedPercentWhenDead_108 = value;
	}

	inline static int32_t get_offset_of_m_fMaxAudioVolume_109() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMaxAudioVolume_109)); }
	inline float get_m_fMaxAudioVolume_109() const { return ___m_fMaxAudioVolume_109; }
	inline float* get_address_of_m_fMaxAudioVolume_109() { return &___m_fMaxAudioVolume_109; }
	inline void set_m_fMaxAudioVolume_109(float value)
	{
		___m_fMaxAudioVolume_109 = value;
	}

	inline static int32_t get_offset_of_m_fMinAudioVolume_110() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMinAudioVolume_110)); }
	inline float get_m_fMinAudioVolume_110() const { return ___m_fMinAudioVolume_110; }
	inline float* get_address_of_m_fMinAudioVolume_110() { return &___m_fMinAudioVolume_110; }
	inline void set_m_fMinAudioVolume_110(float value)
	{
		___m_fMinAudioVolume_110 = value;
	}

	inline static int32_t get_offset_of_m_fMaxPlayerSpeed_111() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMaxPlayerSpeed_111)); }
	inline float get_m_fMaxPlayerSpeed_111() const { return ___m_fMaxPlayerSpeed_111; }
	inline float* get_address_of_m_fMaxPlayerSpeed_111() { return &___m_fMaxPlayerSpeed_111; }
	inline void set_m_fMaxPlayerSpeed_111(float value)
	{
		___m_fMaxPlayerSpeed_111 = value;
	}

	inline static int32_t get_offset_of_m_fShengFuPanDingSize_112() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fShengFuPanDingSize_112)); }
	inline float get_m_fShengFuPanDingSize_112() const { return ___m_fShengFuPanDingSize_112; }
	inline float* get_address_of_m_fShengFuPanDingSize_112() { return &___m_fShengFuPanDingSize_112; }
	inline void set_m_fShengFuPanDingSize_112(float value)
	{
		___m_fShengFuPanDingSize_112 = value;
	}

	inline static int32_t get_offset_of_m_fClassCircleThreshold0_113() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fClassCircleThreshold0_113)); }
	inline float get_m_fClassCircleThreshold0_113() const { return ___m_fClassCircleThreshold0_113; }
	inline float* get_address_of_m_fClassCircleThreshold0_113() { return &___m_fClassCircleThreshold0_113; }
	inline void set_m_fClassCircleThreshold0_113(float value)
	{
		___m_fClassCircleThreshold0_113 = value;
	}

	inline static int32_t get_offset_of_m_fClassCircleThreshold1_114() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fClassCircleThreshold1_114)); }
	inline float get_m_fClassCircleThreshold1_114() const { return ___m_fClassCircleThreshold1_114; }
	inline float* get_address_of_m_fClassCircleThreshold1_114() { return &___m_fClassCircleThreshold1_114; }
	inline void set_m_fClassCircleThreshold1_114(float value)
	{
		___m_fClassCircleThreshold1_114 = value;
	}

	inline static int32_t get_offset_of_m_fClassCircleLength0_115() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fClassCircleLength0_115)); }
	inline float get_m_fClassCircleLength0_115() const { return ___m_fClassCircleLength0_115; }
	inline float* get_address_of_m_fClassCircleLength0_115() { return &___m_fClassCircleLength0_115; }
	inline void set_m_fClassCircleLength0_115(float value)
	{
		___m_fClassCircleLength0_115 = value;
	}

	inline static int32_t get_offset_of_m_fClassCircleLength1_116() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fClassCircleLength1_116)); }
	inline float get_m_fClassCircleLength1_116() const { return ___m_fClassCircleLength1_116; }
	inline float* get_address_of_m_fClassCircleLength1_116() { return &___m_fClassCircleLength1_116; }
	inline void set_m_fClassCircleLength1_116(float value)
	{
		___m_fClassCircleLength1_116 = value;
	}

	inline static int32_t get_offset_of_m_vecClassCircleCenterPos_117() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecClassCircleCenterPos_117)); }
	inline Vector3_t3722313464  get_m_vecClassCircleCenterPos_117() const { return ___m_vecClassCircleCenterPos_117; }
	inline Vector3_t3722313464 * get_address_of_m_vecClassCircleCenterPos_117() { return &___m_vecClassCircleCenterPos_117; }
	inline void set_m_vecClassCircleCenterPos_117(Vector3_t3722313464  value)
	{
		___m_vecClassCircleCenterPos_117 = value;
	}

	inline static int32_t get_offset_of_m_fLaBabaRunTime_118() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLaBabaRunTime_118)); }
	inline float get_m_fLaBabaRunTime_118() const { return ___m_fLaBabaRunTime_118; }
	inline float* get_address_of_m_fLaBabaRunTime_118() { return &___m_fLaBabaRunTime_118; }
	inline void set_m_fLaBabaRunTime_118(float value)
	{
		___m_fLaBabaRunTime_118 = value;
	}

	inline static int32_t get_offset_of_m_fLaBabaRunDistance_119() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLaBabaRunDistance_119)); }
	inline float get_m_fLaBabaRunDistance_119() const { return ___m_fLaBabaRunDistance_119; }
	inline float* get_address_of_m_fLaBabaRunDistance_119() { return &___m_fLaBabaRunDistance_119; }
	inline void set_m_fLaBabaRunDistance_119(float value)
	{
		___m_fLaBabaRunDistance_119 = value;
	}

	inline static int32_t get_offset_of_m_fLababaColdDown_120() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLababaColdDown_120)); }
	inline float get_m_fLababaColdDown_120() const { return ___m_fLababaColdDown_120; }
	inline float* get_address_of_m_fLababaColdDown_120() { return &___m_fLababaColdDown_120; }
	inline void set_m_fLababaColdDown_120(float value)
	{
		___m_fLababaColdDown_120 = value;
	}

	inline static int32_t get_offset_of_m_fLababaLiveTime_121() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLababaLiveTime_121)); }
	inline float get_m_fLababaLiveTime_121() const { return ___m_fLababaLiveTime_121; }
	inline float* get_address_of_m_fLababaLiveTime_121() { return &___m_fLababaLiveTime_121; }
	inline void set_m_fLababaLiveTime_121(float value)
	{
		___m_fLababaLiveTime_121 = value;
	}

	inline static int32_t get_offset_of_m_fLababaSizeCostPercent_122() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLababaSizeCostPercent_122)); }
	inline float get_m_fLababaSizeCostPercent_122() const { return ___m_fLababaSizeCostPercent_122; }
	inline float* get_address_of_m_fLababaSizeCostPercent_122() { return &___m_fLababaSizeCostPercent_122; }
	inline void set_m_fLababaSizeCostPercent_122(float value)
	{
		___m_fLababaSizeCostPercent_122 = value;
	}

	inline static int32_t get_offset_of_m_fLababaAreaThreshold_123() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLababaAreaThreshold_123)); }
	inline float get_m_fLababaAreaThreshold_123() const { return ___m_fLababaAreaThreshold_123; }
	inline float* get_address_of_m_fLababaAreaThreshold_123() { return &___m_fLababaAreaThreshold_123; }
	inline void set_m_fLababaAreaThreshold_123(float value)
	{
		___m_fLababaAreaThreshold_123 = value;
	}

	inline static int32_t get_offset_of_m_fTimeOfOneGame_124() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fTimeOfOneGame_124)); }
	inline float get_m_fTimeOfOneGame_124() const { return ___m_fTimeOfOneGame_124; }
	inline float* get_address_of_m_fTimeOfOneGame_124() { return &___m_fTimeOfOneGame_124; }
	inline void set_m_fTimeOfOneGame_124(float value)
	{
		___m_fTimeOfOneGame_124 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustMoveProtectInterval_125() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fAdjustMoveProtectInterval_125)); }
	inline float get_m_fAdjustMoveProtectInterval_125() const { return ___m_fAdjustMoveProtectInterval_125; }
	inline float* get_address_of_m_fAdjustMoveProtectInterval_125() { return &___m_fAdjustMoveProtectInterval_125; }
	inline void set_m_fAdjustMoveProtectInterval_125(float value)
	{
		___m_fAdjustMoveProtectInterval_125 = value;
	}

	inline static int32_t get_offset_of_m_fMaxMoveDistancePerFrame_126() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMaxMoveDistancePerFrame_126)); }
	inline float get_m_fMaxMoveDistancePerFrame_126() const { return ___m_fMaxMoveDistancePerFrame_126; }
	inline float* get_address_of_m_fMaxMoveDistancePerFrame_126() { return &___m_fMaxMoveDistancePerFrame_126; }
	inline void set_m_fMaxMoveDistancePerFrame_126(float value)
	{
		___m_fMaxMoveDistancePerFrame_126 = value;
	}

	inline static int32_t get_offset_of_m_nObservor_127() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nObservor_127)); }
	inline int32_t get_m_nObservor_127() const { return ___m_nObservor_127; }
	inline int32_t* get_address_of_m_nObservor_127() { return &___m_nObservor_127; }
	inline void set_m_nObservor_127(int32_t value)
	{
		___m_nObservor_127 = value;
	}

	inline static int32_t get_offset_of_m_fShellAdjustShit_128() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fShellAdjustShit_128)); }
	inline float get_m_fShellAdjustShit_128() const { return ___m_fShellAdjustShit_128; }
	inline float* get_address_of_m_fShellAdjustShit_128() { return &___m_fShellAdjustShit_128; }
	inline void set_m_fShellAdjustShit_128(float value)
	{
		___m_fShellAdjustShit_128 = value;
	}

	inline static int32_t get_offset_of_m_fBaseSpeedInGroupLocal_129() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBaseSpeedInGroupLocal_129)); }
	inline float get_m_fBaseSpeedInGroupLocal_129() const { return ___m_fBaseSpeedInGroupLocal_129; }
	inline float* get_address_of_m_fBaseSpeedInGroupLocal_129() { return &___m_fBaseSpeedInGroupLocal_129; }
	inline void set_m_fBaseSpeedInGroupLocal_129(float value)
	{
		___m_fBaseSpeedInGroupLocal_129 = value;
	}

	inline static int32_t get_offset_of_m_fSyncMoveCountInterval_130() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSyncMoveCountInterval_130)); }
	inline float get_m_fSyncMoveCountInterval_130() const { return ___m_fSyncMoveCountInterval_130; }
	inline float* get_address_of_m_fSyncMoveCountInterval_130() { return &___m_fSyncMoveCountInterval_130; }
	inline void set_m_fSyncMoveCountInterval_130(float value)
	{
		___m_fSyncMoveCountInterval_130 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustPosDelayThreshold_131() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fAdjustPosDelayThreshold_131)); }
	inline float get_m_fAdjustPosDelayThreshold_131() const { return ___m_fAdjustPosDelayThreshold_131; }
	inline float* get_address_of_m_fAdjustPosDelayThreshold_131() { return &___m_fAdjustPosDelayThreshold_131; }
	inline void set_m_fAdjustPosDelayThreshold_131(float value)
	{
		___m_fAdjustPosDelayThreshold_131 = value;
	}

	inline static int32_t get_offset_of_m_nCheatMode_132() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nCheatMode_132)); }
	inline float get_m_nCheatMode_132() const { return ___m_nCheatMode_132; }
	inline float* get_address_of_m_nCheatMode_132() { return &___m_nCheatMode_132; }
	inline void set_m_nCheatMode_132(float value)
	{
		___m_nCheatMode_132 = value;
	}

	inline static int32_t get_offset_of_m_fBornProtectTime_133() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBornProtectTime_133)); }
	inline float get_m_fBornProtectTime_133() const { return ___m_fBornProtectTime_133; }
	inline float* get_address_of_m_fBornProtectTime_133() { return &___m_fBornProtectTime_133; }
	inline void set_m_fBornProtectTime_133(float value)
	{
		___m_fBornProtectTime_133 = value;
	}

	inline static int32_t get_offset_of_m_fMinRunTime_134() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMinRunTime_134)); }
	inline float get_m_fMinRunTime_134() const { return ___m_fMinRunTime_134; }
	inline float* get_address_of_m_fMinRunTime_134() { return &___m_fMinRunTime_134; }
	inline void set_m_fMinRunTime_134(float value)
	{
		___m_fMinRunTime_134 = value;
	}

	inline static int32_t get_offset_of_m_fGridLineGap_135() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGridLineGap_135)); }
	inline float get_m_fGridLineGap_135() const { return ___m_fGridLineGap_135; }
	inline float* get_address_of_m_fGridLineGap_135() { return &___m_fGridLineGap_135; }
	inline void set_m_fGridLineGap_135(float value)
	{
		___m_fGridLineGap_135 = value;
	}

	inline static int32_t get_offset_of_m_fTimeBeforeUiDead_136() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fTimeBeforeUiDead_136)); }
	inline float get_m_fTimeBeforeUiDead_136() const { return ___m_fTimeBeforeUiDead_136; }
	inline float* get_address_of_m_fTimeBeforeUiDead_136() { return &___m_fTimeBeforeUiDead_136; }
	inline void set_m_fTimeBeforeUiDead_136(float value)
	{
		___m_fTimeBeforeUiDead_136 = value;
	}

	inline static int32_t get_offset_of_m_fBeanNumPerRange_137() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBeanNumPerRange_137)); }
	inline float get_m_fBeanNumPerRange_137() const { return ___m_fBeanNumPerRange_137; }
	inline float* get_address_of_m_fBeanNumPerRange_137() { return &___m_fBeanNumPerRange_137; }
	inline void set_m_fBeanNumPerRange_137(float value)
	{
		___m_fBeanNumPerRange_137 = value;
	}

	inline static int32_t get_offset_of_m_fThornNumPerRange_138() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fThornNumPerRange_138)); }
	inline float get_m_fThornNumPerRange_138() const { return ___m_fThornNumPerRange_138; }
	inline float* get_address_of_m_fThornNumPerRange_138() { return &___m_fThornNumPerRange_138; }
	inline void set_m_fThornNumPerRange_138(float value)
	{
		___m_fThornNumPerRange_138 = value;
	}

	inline static int32_t get_offset_of_m_fBeanSize_139() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBeanSize_139)); }
	inline float get_m_fBeanSize_139() const { return ___m_fBeanSize_139; }
	inline float* get_address_of_m_fBeanSize_139() { return &___m_fBeanSize_139; }
	inline void set_m_fBeanSize_139(float value)
	{
		___m_fBeanSize_139 = value;
	}

	inline static int32_t get_offset_of_m_fThornSize_140() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fThornSize_140)); }
	inline float get_m_fThornSize_140() const { return ___m_fThornSize_140; }
	inline float* get_address_of_m_fThornSize_140() { return &___m_fThornSize_140; }
	inline void set_m_fThornSize_140(float value)
	{
		___m_fThornSize_140 = value;
	}

	inline static int32_t get_offset_of_m_fShellShrinkTotalTime_141() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fShellShrinkTotalTime_141)); }
	inline float get_m_fShellShrinkTotalTime_141() const { return ___m_fShellShrinkTotalTime_141; }
	inline float* get_address_of_m_fShellShrinkTotalTime_141() { return &___m_fShellShrinkTotalTime_141; }
	inline void set_m_fShellShrinkTotalTime_141(float value)
	{
		___m_fShellShrinkTotalTime_141 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeShellShrinkTotalTime_142() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fExplodeShellShrinkTotalTime_142)); }
	inline float get_m_fExplodeShellShrinkTotalTime_142() const { return ___m_fExplodeShellShrinkTotalTime_142; }
	inline float* get_address_of_m_fExplodeShellShrinkTotalTime_142() { return &___m_fExplodeShellShrinkTotalTime_142; }
	inline void set_m_fExplodeShellShrinkTotalTime_142(float value)
	{
		___m_fExplodeShellShrinkTotalTime_142 = value;
	}

	inline static int32_t get_offset_of_m_fSplitShellShrinkTotalTime_143() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitShellShrinkTotalTime_143)); }
	inline float get_m_fSplitShellShrinkTotalTime_143() const { return ___m_fSplitShellShrinkTotalTime_143; }
	inline float* get_address_of_m_fSplitShellShrinkTotalTime_143() { return &___m_fSplitShellShrinkTotalTime_143; }
	inline void set_m_fSplitShellShrinkTotalTime_143(float value)
	{
		___m_fSplitShellShrinkTotalTime_143 = value;
	}

	inline static int32_t get_offset_of_m_fThornContributeToBallSize_144() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fThornContributeToBallSize_144)); }
	inline float get_m_fThornContributeToBallSize_144() const { return ___m_fThornContributeToBallSize_144; }
	inline float* get_address_of_m_fThornContributeToBallSize_144() { return &___m_fThornContributeToBallSize_144; }
	inline void set_m_fThornContributeToBallSize_144(float value)
	{
		___m_fThornContributeToBallSize_144 = value;
	}

	inline static int32_t get_offset_of_m_fAutoAttenuate_145() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fAutoAttenuate_145)); }
	inline float get_m_fAutoAttenuate_145() const { return ___m_fAutoAttenuate_145; }
	inline float* get_address_of_m_fAutoAttenuate_145() { return &___m_fAutoAttenuate_145; }
	inline void set_m_fAutoAttenuate_145(float value)
	{
		___m_fAutoAttenuate_145 = value;
	}

	inline static int32_t get_offset_of_m_fAutoAttenuateTimeInterval_146() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fAutoAttenuateTimeInterval_146)); }
	inline float get_m_fAutoAttenuateTimeInterval_146() const { return ___m_fAutoAttenuateTimeInterval_146; }
	inline float* get_address_of_m_fAutoAttenuateTimeInterval_146() { return &___m_fAutoAttenuateTimeInterval_146; }
	inline void set_m_fAutoAttenuateTimeInterval_146(float value)
	{
		___m_fAutoAttenuateTimeInterval_146 = value;
	}

	inline static int32_t get_offset_of_m_fThornAttenuate_147() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fThornAttenuate_147)); }
	inline float get_m_fThornAttenuate_147() const { return ___m_fThornAttenuate_147; }
	inline float* get_address_of_m_fThornAttenuate_147() { return &___m_fThornAttenuate_147; }
	inline void set_m_fThornAttenuate_147(float value)
	{
		___m_fThornAttenuate_147 = value;
	}

	inline static int32_t get_offset_of_m_fArenaWidth_148() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fArenaWidth_148)); }
	inline float get_m_fArenaWidth_148() const { return ___m_fArenaWidth_148; }
	inline float* get_address_of_m_fArenaWidth_148() { return &___m_fArenaWidth_148; }
	inline void set_m_fArenaWidth_148(float value)
	{
		___m_fArenaWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_fArenaHeight_149() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fArenaHeight_149)); }
	inline float get_m_fArenaHeight_149() const { return ___m_fArenaHeight_149; }
	inline float* get_address_of_m_fArenaHeight_149() { return &___m_fArenaHeight_149; }
	inline void set_m_fArenaHeight_149(float value)
	{
		___m_fArenaHeight_149 = value;
	}

	inline static int32_t get_offset_of_m_fGenerateNewBeanTimeInterval_150() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGenerateNewBeanTimeInterval_150)); }
	inline float get_m_fGenerateNewBeanTimeInterval_150() const { return ___m_fGenerateNewBeanTimeInterval_150; }
	inline float* get_address_of_m_fGenerateNewBeanTimeInterval_150() { return &___m_fGenerateNewBeanTimeInterval_150; }
	inline void set_m_fGenerateNewBeanTimeInterval_150(float value)
	{
		___m_fGenerateNewBeanTimeInterval_150 = value;
	}

	inline static int32_t get_offset_of_m_fGenerateNewThornTimeInterval_151() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGenerateNewThornTimeInterval_151)); }
	inline float get_m_fGenerateNewThornTimeInterval_151() const { return ___m_fGenerateNewThornTimeInterval_151; }
	inline float* get_address_of_m_fGenerateNewThornTimeInterval_151() { return &___m_fGenerateNewThornTimeInterval_151; }
	inline void set_m_fGenerateNewThornTimeInterval_151(float value)
	{
		___m_fGenerateNewThornTimeInterval_151 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeTimeInterval_152() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitSporeTimeInterval_152)); }
	inline float get_m_fSpitSporeTimeInterval_152() const { return ___m_fSpitSporeTimeInterval_152; }
	inline float* get_address_of_m_fSpitSporeTimeInterval_152() { return &___m_fSpitSporeTimeInterval_152; }
	inline void set_m_fSpitSporeTimeInterval_152(float value)
	{
		___m_fSpitSporeTimeInterval_152 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallTimeInterval_153() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallTimeInterval_153)); }
	inline float get_m_fSpitBallTimeInterval_153() const { return ___m_fSpitBallTimeInterval_153; }
	inline float* get_address_of_m_fSpitBallTimeInterval_153() { return &___m_fSpitBallTimeInterval_153; }
	inline void set_m_fSpitBallTimeInterval_153(float value)
	{
		___m_fSpitBallTimeInterval_153 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldCostMP_154() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fUnfoldCostMP_154)); }
	inline float get_m_fUnfoldCostMP_154() const { return ___m_fUnfoldCostMP_154; }
	inline float* get_address_of_m_fUnfoldCostMP_154() { return &___m_fUnfoldCostMP_154; }
	inline void set_m_fUnfoldCostMP_154(float value)
	{
		___m_fUnfoldCostMP_154 = value;
	}

	inline static int32_t get_offset_of_m_fMainTriggerUnfoldTime_155() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMainTriggerUnfoldTime_155)); }
	inline float get_m_fMainTriggerUnfoldTime_155() const { return ___m_fMainTriggerUnfoldTime_155; }
	inline float* get_address_of_m_fMainTriggerUnfoldTime_155() { return &___m_fMainTriggerUnfoldTime_155; }
	inline void set_m_fMainTriggerUnfoldTime_155(float value)
	{
		___m_fMainTriggerUnfoldTime_155 = value;
	}

	inline static int32_t get_offset_of_m_fMainTriggerPreUnfoldTime_156() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMainTriggerPreUnfoldTime_156)); }
	inline float get_m_fMainTriggerPreUnfoldTime_156() const { return ___m_fMainTriggerPreUnfoldTime_156; }
	inline float* get_address_of_m_fMainTriggerPreUnfoldTime_156() { return &___m_fMainTriggerPreUnfoldTime_156; }
	inline void set_m_fMainTriggerPreUnfoldTime_156(float value)
	{
		___m_fMainTriggerPreUnfoldTime_156 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldTimeInterval_157() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fUnfoldTimeInterval_157)); }
	inline float get_m_fUnfoldTimeInterval_157() const { return ___m_fUnfoldTimeInterval_157; }
	inline float* get_address_of_m_fUnfoldTimeInterval_157() { return &___m_fUnfoldTimeInterval_157; }
	inline void set_m_fUnfoldTimeInterval_157(float value)
	{
		___m_fUnfoldTimeInterval_157 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldSale_158() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fUnfoldSale_158)); }
	inline float get_m_fUnfoldSale_158() const { return ___m_fUnfoldSale_158; }
	inline float* get_address_of_m_fUnfoldSale_158() { return &___m_fUnfoldSale_158; }
	inline void set_m_fUnfoldSale_158(float value)
	{
		___m_fUnfoldSale_158 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldCircleSale_159() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fUnfoldCircleSale_159)); }
	inline float get_m_fUnfoldCircleSale_159() const { return ___m_fUnfoldCircleSale_159; }
	inline float* get_address_of_m_fUnfoldCircleSale_159() { return &___m_fUnfoldCircleSale_159; }
	inline void set_m_fUnfoldCircleSale_159(float value)
	{
		___m_fUnfoldCircleSale_159 = value;
	}

	inline static int32_t get_offset_of_m_fForceSpitTotalTime_160() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fForceSpitTotalTime_160)); }
	inline float get_m_fForceSpitTotalTime_160() const { return ___m_fForceSpitTotalTime_160; }
	inline float* get_address_of_m_fForceSpitTotalTime_160() { return &___m_fForceSpitTotalTime_160; }
	inline void set_m_fForceSpitTotalTime_160(float value)
	{
		___m_fForceSpitTotalTime_160 = value;
	}

	inline static int32_t get_offset_of_m_fSpitRunTime_161() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitRunTime_161)); }
	inline float get_m_fSpitRunTime_161() const { return ___m_fSpitRunTime_161; }
	inline float* get_address_of_m_fSpitRunTime_161() { return &___m_fSpitRunTime_161; }
	inline void set_m_fSpitRunTime_161(float value)
	{
		___m_fSpitRunTime_161 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallRunTime_162() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallRunTime_162)); }
	inline float get_m_fSpitBallRunTime_162() const { return ___m_fSpitBallRunTime_162; }
	inline float* get_address_of_m_fSpitBallRunTime_162() { return &___m_fSpitBallRunTime_162; }
	inline void set_m_fSpitBallRunTime_162(float value)
	{
		___m_fSpitBallRunTime_162 = value;
	}

	inline static int32_t get_offset_of_m_szSpitSporeBeanType_163() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_szSpitSporeBeanType_163)); }
	inline String_t* get_m_szSpitSporeBeanType_163() const { return ___m_szSpitSporeBeanType_163; }
	inline String_t** get_address_of_m_szSpitSporeBeanType_163() { return &___m_szSpitSporeBeanType_163; }
	inline void set_m_szSpitSporeBeanType_163(String_t* value)
	{
		___m_szSpitSporeBeanType_163 = value;
		Il2CppCodeGenWriteBarrier((&___m_szSpitSporeBeanType_163), value);
	}

	inline static int32_t get_offset_of_m_fSpitSporeCostMP_164() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitSporeCostMP_164)); }
	inline float get_m_fSpitSporeCostMP_164() const { return ___m_fSpitSporeCostMP_164; }
	inline float* get_address_of_m_fSpitSporeCostMP_164() { return &___m_fSpitSporeCostMP_164; }
	inline void set_m_fSpitSporeCostMP_164(float value)
	{
		___m_fSpitSporeCostMP_164 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeRunDistance_165() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitSporeRunDistance_165)); }
	inline float get_m_fSpitSporeRunDistance_165() const { return ___m_fSpitSporeRunDistance_165; }
	inline float* get_address_of_m_fSpitSporeRunDistance_165() { return &___m_fSpitSporeRunDistance_165; }
	inline void set_m_fSpitSporeRunDistance_165(float value)
	{
		___m_fSpitSporeRunDistance_165 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeInitSpeed_166() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitSporeInitSpeed_166)); }
	inline float get_m_fSpitSporeInitSpeed_166() const { return ___m_fSpitSporeInitSpeed_166; }
	inline float* get_address_of_m_fSpitSporeInitSpeed_166() { return &___m_fSpitSporeInitSpeed_166; }
	inline void set_m_fSpitSporeInitSpeed_166(float value)
	{
		___m_fSpitSporeInitSpeed_166 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeAccelerate_167() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitSporeAccelerate_167)); }
	inline float get_m_fSpitSporeAccelerate_167() const { return ___m_fSpitSporeAccelerate_167; }
	inline float* get_address_of_m_fSpitSporeAccelerate_167() { return &___m_fSpitSporeAccelerate_167; }
	inline void set_m_fSpitSporeAccelerate_167(float value)
	{
		___m_fSpitSporeAccelerate_167 = value;
	}

	inline static int32_t get_offset_of_m_fSpittedObjrunRunTime_168() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpittedObjrunRunTime_168)); }
	inline float get_m_fSpittedObjrunRunTime_168() const { return ___m_fSpittedObjrunRunTime_168; }
	inline float* get_address_of_m_fSpittedObjrunRunTime_168() { return &___m_fSpittedObjrunRunTime_168; }
	inline void set_m_fSpittedObjrunRunTime_168(float value)
	{
		___m_fSpittedObjrunRunTime_168 = value;
	}

	inline static int32_t get_offset_of_m_fSpittedObjrunRunDistance_169() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpittedObjrunRunDistance_169)); }
	inline float get_m_fSpittedObjrunRunDistance_169() const { return ___m_fSpittedObjrunRunDistance_169; }
	inline float* get_address_of_m_fSpittedObjrunRunDistance_169() { return &___m_fSpittedObjrunRunDistance_169; }
	inline void set_m_fSpittedObjrunRunDistance_169(float value)
	{
		___m_fSpittedObjrunRunDistance_169 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfRunTime_170() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallHalfRunTime_170)); }
	inline float get_m_fSpitBallHalfRunTime_170() const { return ___m_fSpitBallHalfRunTime_170; }
	inline float* get_address_of_m_fSpitBallHalfRunTime_170() { return &___m_fSpitBallHalfRunTime_170; }
	inline void set_m_fSpitBallHalfRunTime_170(float value)
	{
		___m_fSpitBallHalfRunTime_170 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfRunDistance_171() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallHalfRunDistance_171)); }
	inline float get_m_fSpitBallHalfRunDistance_171() const { return ___m_fSpitBallHalfRunDistance_171; }
	inline float* get_address_of_m_fSpitBallHalfRunDistance_171() { return &___m_fSpitBallHalfRunDistance_171; }
	inline void set_m_fSpitBallHalfRunDistance_171(float value)
	{
		___m_fSpitBallHalfRunDistance_171 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfCostMP_172() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallHalfCostMP_172)); }
	inline float get_m_fSpitBallHalfCostMP_172() const { return ___m_fSpitBallHalfCostMP_172; }
	inline float* get_address_of_m_fSpitBallHalfCostMP_172() { return &___m_fSpitBallHalfCostMP_172; }
	inline void set_m_fSpitBallHalfCostMP_172(float value)
	{
		___m_fSpitBallHalfCostMP_172 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfColddown_173() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallHalfColddown_173)); }
	inline float get_m_fSpitBallHalfColddown_173() const { return ___m_fSpitBallHalfColddown_173; }
	inline float* get_address_of_m_fSpitBallHalfColddown_173() { return &___m_fSpitBallHalfColddown_173; }
	inline void set_m_fSpitBallHalfColddown_173(float value)
	{
		___m_fSpitBallHalfColddown_173 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfQianYao_174() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallHalfQianYao_174)); }
	inline float get_m_fSpitBallHalfQianYao_174() const { return ___m_fSpitBallHalfQianYao_174; }
	inline float* get_address_of_m_fSpitBallHalfQianYao_174() { return &___m_fSpitBallHalfQianYao_174; }
	inline void set_m_fSpitBallHalfQianYao_174(float value)
	{
		___m_fSpitBallHalfQianYao_174 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallRunSpeed_175() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallRunSpeed_175)); }
	inline float get_m_fSpitBallRunSpeed_175() const { return ___m_fSpitBallRunSpeed_175; }
	inline float* get_address_of_m_fSpitBallRunSpeed_175() { return &___m_fSpitBallRunSpeed_175; }
	inline void set_m_fSpitBallRunSpeed_175(float value)
	{
		___m_fSpitBallRunSpeed_175 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallRunDistanceMultiple_176() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallRunDistanceMultiple_176)); }
	inline float get_m_fSpitBallRunDistanceMultiple_176() const { return ___m_fSpitBallRunDistanceMultiple_176; }
	inline float* get_address_of_m_fSpitBallRunDistanceMultiple_176() { return &___m_fSpitBallRunDistanceMultiple_176; }
	inline void set_m_fSpitBallRunDistanceMultiple_176(float value)
	{
		___m_fSpitBallRunDistanceMultiple_176 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallCostMP_177() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallCostMP_177)); }
	inline float get_m_fSpitBallCostMP_177() const { return ___m_fSpitBallCostMP_177; }
	inline float* get_address_of_m_fSpitBallCostMP_177() { return &___m_fSpitBallCostMP_177; }
	inline void set_m_fSpitBallCostMP_177(float value)
	{
		___m_fSpitBallCostMP_177 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallStayTime_178() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallStayTime_178)); }
	inline float get_m_fSpitBallStayTime_178() const { return ___m_fSpitBallStayTime_178; }
	inline float* get_address_of_m_fSpitBallStayTime_178() { return &___m_fSpitBallStayTime_178; }
	inline void set_m_fSpitBallStayTime_178(float value)
	{
		___m_fSpitBallStayTime_178 = value;
	}

	inline static int32_t get_offset_of_m_fBornMinTiJi_179() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fBornMinTiJi_179)); }
	inline float get_m_fBornMinTiJi_179() const { return ___m_fBornMinTiJi_179; }
	inline float* get_address_of_m_fBornMinTiJi_179() { return &___m_fBornMinTiJi_179; }
	inline void set_m_fBornMinTiJi_179(float value)
	{
		___m_fBornMinTiJi_179 = value;
	}

	inline static int32_t get_offset_of_m_fMinDiameterToEatThorn_180() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMinDiameterToEatThorn_180)); }
	inline float get_m_fMinDiameterToEatThorn_180() const { return ___m_fMinDiameterToEatThorn_180; }
	inline float* get_address_of_m_fMinDiameterToEatThorn_180() { return &___m_fMinDiameterToEatThorn_180; }
	inline void set_m_fMinDiameterToEatThorn_180(float value)
	{
		___m_fMinDiameterToEatThorn_180 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeRunDistanceMultiple_181() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fExplodeRunDistanceMultiple_181)); }
	inline float get_m_fExplodeRunDistanceMultiple_181() const { return ___m_fExplodeRunDistanceMultiple_181; }
	inline float* get_address_of_m_fExplodeRunDistanceMultiple_181() { return &___m_fExplodeRunDistanceMultiple_181; }
	inline void set_m_fExplodeRunDistanceMultiple_181(float value)
	{
		___m_fExplodeRunDistanceMultiple_181 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeStayTime_182() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fExplodeStayTime_182)); }
	inline float get_m_fExplodeStayTime_182() const { return ___m_fExplodeStayTime_182; }
	inline float* get_address_of_m_fExplodeStayTime_182() { return &___m_fExplodeStayTime_182; }
	inline void set_m_fExplodeStayTime_182(float value)
	{
		___m_fExplodeStayTime_182 = value;
	}

	inline static int32_t get_offset_of_m_fExplodePercent_183() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fExplodePercent_183)); }
	inline float get_m_fExplodePercent_183() const { return ___m_fExplodePercent_183; }
	inline float* get_address_of_m_fExplodePercent_183() { return &___m_fExplodePercent_183; }
	inline void set_m_fExplodePercent_183(float value)
	{
		___m_fExplodePercent_183 = value;
	}

	inline static int32_t get_offset_of_m_fExpectExplodeNum_184() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fExpectExplodeNum_184)); }
	inline float get_m_fExpectExplodeNum_184() const { return ___m_fExpectExplodeNum_184; }
	inline float* get_address_of_m_fExpectExplodeNum_184() { return &___m_fExpectExplodeNum_184; }
	inline void set_m_fExpectExplodeNum_184(float value)
	{
		___m_fExpectExplodeNum_184 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeRunTime_185() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fExplodeRunTime_185)); }
	inline float get_m_fExplodeRunTime_185() const { return ___m_fExplodeRunTime_185; }
	inline float* get_address_of_m_fExplodeRunTime_185() { return &___m_fExplodeRunTime_185; }
	inline void set_m_fExplodeRunTime_185(float value)
	{
		___m_fExplodeRunTime_185 = value;
	}

	inline static int32_t get_offset_of_m_nExplodeMinNum_186() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nExplodeMinNum_186)); }
	inline float get_m_nExplodeMinNum_186() const { return ___m_nExplodeMinNum_186; }
	inline float* get_address_of_m_nExplodeMinNum_186() { return &___m_nExplodeMinNum_186; }
	inline void set_m_nExplodeMinNum_186(float value)
	{
		___m_nExplodeMinNum_186 = value;
	}

	inline static int32_t get_offset_of_m_fMaxBallNumPerPlayer_187() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fMaxBallNumPerPlayer_187)); }
	inline float get_m_fMaxBallNumPerPlayer_187() const { return ___m_fMaxBallNumPerPlayer_187; }
	inline float* get_address_of_m_fMaxBallNumPerPlayer_187() { return &___m_fMaxBallNumPerPlayer_187; }
	inline void set_m_fMaxBallNumPerPlayer_187(float value)
	{
		___m_fMaxBallNumPerPlayer_187 = value;
	}

	inline static int32_t get_offset_of_m_fSprayBeanMaxInitSpeed_188() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSprayBeanMaxInitSpeed_188)); }
	inline float get_m_fSprayBeanMaxInitSpeed_188() const { return ___m_fSprayBeanMaxInitSpeed_188; }
	inline float* get_address_of_m_fSprayBeanMaxInitSpeed_188() { return &___m_fSprayBeanMaxInitSpeed_188; }
	inline void set_m_fSprayBeanMaxInitSpeed_188(float value)
	{
		___m_fSprayBeanMaxInitSpeed_188 = value;
	}

	inline static int32_t get_offset_of_m_fSprayBeanMinInitSpeed_189() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSprayBeanMinInitSpeed_189)); }
	inline float get_m_fSprayBeanMinInitSpeed_189() const { return ___m_fSprayBeanMinInitSpeed_189; }
	inline float* get_address_of_m_fSprayBeanMinInitSpeed_189() { return &___m_fSprayBeanMinInitSpeed_189; }
	inline void set_m_fSprayBeanMinInitSpeed_189(float value)
	{
		___m_fSprayBeanMinInitSpeed_189 = value;
	}

	inline static int32_t get_offset_of_m_fSprayBeanAccelerate_190() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSprayBeanAccelerate_190)); }
	inline float get_m_fSprayBeanAccelerate_190() const { return ___m_fSprayBeanAccelerate_190; }
	inline float* get_address_of_m_fSprayBeanAccelerate_190() { return &___m_fSprayBeanAccelerate_190; }
	inline void set_m_fSprayBeanAccelerate_190(float value)
	{
		___m_fSprayBeanAccelerate_190 = value;
	}

	inline static int32_t get_offset_of_m_fSprayBeanLiveTime_191() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSprayBeanLiveTime_191)); }
	inline float get_m_fSprayBeanLiveTime_191() const { return ___m_fSprayBeanLiveTime_191; }
	inline float* get_address_of_m_fSprayBeanLiveTime_191() { return &___m_fSprayBeanLiveTime_191; }
	inline void set_m_fSprayBeanLiveTime_191(float value)
	{
		___m_fSprayBeanLiveTime_191 = value;
	}

	inline static int32_t get_offset_of_m_fCrucifix_192() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCrucifix_192)); }
	inline float get_m_fCrucifix_192() const { return ___m_fCrucifix_192; }
	inline float* get_address_of_m_fCrucifix_192() { return &___m_fCrucifix_192; }
	inline void set_m_fCrucifix_192(float value)
	{
		___m_fCrucifix_192 = value;
	}

	inline static int32_t get_offset_of_m_nSplitNum_193() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nSplitNum_193)); }
	inline int32_t get_m_nSplitNum_193() const { return ___m_nSplitNum_193; }
	inline int32_t* get_address_of_m_nSplitNum_193() { return &___m_nSplitNum_193; }
	inline void set_m_nSplitNum_193(int32_t value)
	{
		___m_nSplitNum_193 = value;
	}

	inline static int32_t get_offset_of_m_fSplitCostMP_194() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitCostMP_194)); }
	inline float get_m_fSplitCostMP_194() const { return ___m_fSplitCostMP_194; }
	inline float* get_address_of_m_fSplitCostMP_194() { return &___m_fSplitCostMP_194; }
	inline void set_m_fSplitCostMP_194(float value)
	{
		___m_fSplitCostMP_194 = value;
	}

	inline static int32_t get_offset_of_m_fSplitStayTime_195() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitStayTime_195)); }
	inline float get_m_fSplitStayTime_195() const { return ___m_fSplitStayTime_195; }
	inline float* get_address_of_m_fSplitStayTime_195() { return &___m_fSplitStayTime_195; }
	inline void set_m_fSplitStayTime_195(float value)
	{
		___m_fSplitStayTime_195 = value;
	}

	inline static int32_t get_offset_of_m_fSplitRunTime_196() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitRunTime_196)); }
	inline float get_m_fSplitRunTime_196() const { return ___m_fSplitRunTime_196; }
	inline float* get_address_of_m_fSplitRunTime_196() { return &___m_fSplitRunTime_196; }
	inline void set_m_fSplitRunTime_196(float value)
	{
		___m_fSplitRunTime_196 = value;
	}

	inline static int32_t get_offset_of_m_fSplitMaxDistance_197() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitMaxDistance_197)); }
	inline float get_m_fSplitMaxDistance_197() const { return ___m_fSplitMaxDistance_197; }
	inline float* get_address_of_m_fSplitMaxDistance_197() { return &___m_fSplitMaxDistance_197; }
	inline void set_m_fSplitMaxDistance_197(float value)
	{
		___m_fSplitMaxDistance_197 = value;
	}

	inline static int32_t get_offset_of_m_fSplitTotalTime_198() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitTotalTime_198)); }
	inline float get_m_fSplitTotalTime_198() const { return ___m_fSplitTotalTime_198; }
	inline float* get_address_of_m_fSplitTotalTime_198() { return &___m_fSplitTotalTime_198; }
	inline void set_m_fSplitTotalTime_198(float value)
	{
		___m_fSplitTotalTime_198 = value;
	}

	inline static int32_t get_offset_of_m_fSplitTimeInterval_199() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitTimeInterval_199)); }
	inline float get_m_fSplitTimeInterval_199() const { return ___m_fSplitTimeInterval_199; }
	inline float* get_address_of_m_fSplitTimeInterval_199() { return &___m_fSplitTimeInterval_199; }
	inline void set_m_fSplitTimeInterval_199(float value)
	{
		___m_fSplitTimeInterval_199 = value;
	}

	inline static int32_t get_offset_of_m_fTriggerRadius_200() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fTriggerRadius_200)); }
	inline float get_m_fTriggerRadius_200() const { return ___m_fTriggerRadius_200; }
	inline float* get_address_of_m_fTriggerRadius_200() { return &___m_fTriggerRadius_200; }
	inline void set_m_fTriggerRadius_200(float value)
	{
		___m_fTriggerRadius_200 = value;
	}

	inline static int32_t get_offset_of_the_spray_201() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___the_spray_201)); }
	inline GameObject_t1113636619 * get_the_spray_201() const { return ___the_spray_201; }
	inline GameObject_t1113636619 ** get_address_of_the_spray_201() { return &___the_spray_201; }
	inline void set_the_spray_201(GameObject_t1113636619 * value)
	{
		___the_spray_201 = value;
		Il2CppCodeGenWriteBarrier((&___the_spray_201), value);
	}

	inline static int32_t get_offset_of_m_goSprays_202() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goSprays_202)); }
	inline GameObject_t1113636619 * get_m_goSprays_202() const { return ___m_goSprays_202; }
	inline GameObject_t1113636619 ** get_address_of_m_goSprays_202() { return &___m_goSprays_202; }
	inline void set_m_goSprays_202(GameObject_t1113636619 * value)
	{
		___m_goSprays_202 = value;
		Il2CppCodeGenWriteBarrier((&___m_goSprays_202), value);
	}

	inline static int32_t get_offset_of_m_fSpitNailTimeInterval_203() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitNailTimeInterval_203)); }
	inline float get_m_fSpitNailTimeInterval_203() const { return ___m_fSpitNailTimeInterval_203; }
	inline float* get_address_of_m_fSpitNailTimeInterval_203() { return &___m_fSpitNailTimeInterval_203; }
	inline void set_m_fSpitNailTimeInterval_203(float value)
	{
		___m_fSpitNailTimeInterval_203 = value;
	}

	inline static int32_t get_offset_of_m_fNailLiveTime_204() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fNailLiveTime_204)); }
	inline float get_m_fNailLiveTime_204() const { return ___m_fNailLiveTime_204; }
	inline float* get_address_of_m_fNailLiveTime_204() { return &___m_fNailLiveTime_204; }
	inline void set_m_fNailLiveTime_204(float value)
	{
		___m_fNailLiveTime_204 = value;
	}

	inline static int32_t get_offset_of_m_lineWorldBorderTop_205() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_lineWorldBorderTop_205)); }
	inline LineRenderer_t3154350270 * get_m_lineWorldBorderTop_205() const { return ___m_lineWorldBorderTop_205; }
	inline LineRenderer_t3154350270 ** get_address_of_m_lineWorldBorderTop_205() { return &___m_lineWorldBorderTop_205; }
	inline void set_m_lineWorldBorderTop_205(LineRenderer_t3154350270 * value)
	{
		___m_lineWorldBorderTop_205 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWorldBorderTop_205), value);
	}

	inline static int32_t get_offset_of_m_lineWorldBorderBottom_206() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_lineWorldBorderBottom_206)); }
	inline LineRenderer_t3154350270 * get_m_lineWorldBorderBottom_206() const { return ___m_lineWorldBorderBottom_206; }
	inline LineRenderer_t3154350270 ** get_address_of_m_lineWorldBorderBottom_206() { return &___m_lineWorldBorderBottom_206; }
	inline void set_m_lineWorldBorderBottom_206(LineRenderer_t3154350270 * value)
	{
		___m_lineWorldBorderBottom_206 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWorldBorderBottom_206), value);
	}

	inline static int32_t get_offset_of_m_lineWorldBorderLeft_207() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_lineWorldBorderLeft_207)); }
	inline LineRenderer_t3154350270 * get_m_lineWorldBorderLeft_207() const { return ___m_lineWorldBorderLeft_207; }
	inline LineRenderer_t3154350270 ** get_address_of_m_lineWorldBorderLeft_207() { return &___m_lineWorldBorderLeft_207; }
	inline void set_m_lineWorldBorderLeft_207(LineRenderer_t3154350270 * value)
	{
		___m_lineWorldBorderLeft_207 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWorldBorderLeft_207), value);
	}

	inline static int32_t get_offset_of_m_lineWorldBorderRight_208() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_lineWorldBorderRight_208)); }
	inline LineRenderer_t3154350270 * get_m_lineWorldBorderRight_208() const { return ___m_lineWorldBorderRight_208; }
	inline LineRenderer_t3154350270 ** get_address_of_m_lineWorldBorderRight_208() { return &___m_lineWorldBorderRight_208; }
	inline void set_m_lineWorldBorderRight_208(LineRenderer_t3154350270 * value)
	{
		___m_lineWorldBorderRight_208 = value;
		Il2CppCodeGenWriteBarrier((&___m_lineWorldBorderRight_208), value);
	}

	inline static int32_t get_offset_of_m_goBorderLeft_209() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBorderLeft_209)); }
	inline GameObject_t1113636619 * get_m_goBorderLeft_209() const { return ___m_goBorderLeft_209; }
	inline GameObject_t1113636619 ** get_address_of_m_goBorderLeft_209() { return &___m_goBorderLeft_209; }
	inline void set_m_goBorderLeft_209(GameObject_t1113636619 * value)
	{
		___m_goBorderLeft_209 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBorderLeft_209), value);
	}

	inline static int32_t get_offset_of_m_goBorderRight_210() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBorderRight_210)); }
	inline GameObject_t1113636619 * get_m_goBorderRight_210() const { return ___m_goBorderRight_210; }
	inline GameObject_t1113636619 ** get_address_of_m_goBorderRight_210() { return &___m_goBorderRight_210; }
	inline void set_m_goBorderRight_210(GameObject_t1113636619 * value)
	{
		___m_goBorderRight_210 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBorderRight_210), value);
	}

	inline static int32_t get_offset_of_m_goBorderTop_211() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBorderTop_211)); }
	inline GameObject_t1113636619 * get_m_goBorderTop_211() const { return ___m_goBorderTop_211; }
	inline GameObject_t1113636619 ** get_address_of_m_goBorderTop_211() { return &___m_goBorderTop_211; }
	inline void set_m_goBorderTop_211(GameObject_t1113636619 * value)
	{
		___m_goBorderTop_211 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBorderTop_211), value);
	}

	inline static int32_t get_offset_of_m_goBorderBottom_212() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBorderBottom_212)); }
	inline GameObject_t1113636619 * get_m_goBorderBottom_212() const { return ___m_goBorderBottom_212; }
	inline GameObject_t1113636619 ** get_address_of_m_goBorderBottom_212() { return &___m_goBorderBottom_212; }
	inline void set_m_goBorderBottom_212(GameObject_t1113636619 * value)
	{
		___m_goBorderBottom_212 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBorderBottom_212), value);
	}

	inline static int32_t get_offset_of_vecTempPos_213() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___vecTempPos_213)); }
	inline Vector3_t3722313464  get_vecTempPos_213() const { return ___vecTempPos_213; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_213() { return &___vecTempPos_213; }
	inline void set_vecTempPos_213(Vector3_t3722313464  value)
	{
		___vecTempPos_213 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_214() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___vecTempPos1_214)); }
	inline Vector3_t3722313464  get_vecTempPos1_214() const { return ___vecTempPos1_214; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_214() { return &___vecTempPos1_214; }
	inline void set_vecTempPos1_214(Vector3_t3722313464  value)
	{
		___vecTempPos1_214 = value;
	}

	inline static int32_t get_offset_of_m_goThornsSpit_215() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goThornsSpit_215)); }
	inline GameObject_t1113636619 * get_m_goThornsSpit_215() const { return ___m_goThornsSpit_215; }
	inline GameObject_t1113636619 ** get_address_of_m_goThornsSpit_215() { return &___m_goThornsSpit_215; }
	inline void set_m_goThornsSpit_215(GameObject_t1113636619 * value)
	{
		___m_goThornsSpit_215 = value;
		Il2CppCodeGenWriteBarrier((&___m_goThornsSpit_215), value);
	}

	inline static int32_t get_offset_of_m_goThorns_216() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goThorns_216)); }
	inline GameObject_t1113636619 * get_m_goThorns_216() const { return ___m_goThorns_216; }
	inline GameObject_t1113636619 ** get_address_of_m_goThorns_216() { return &___m_goThorns_216; }
	inline void set_m_goThorns_216(GameObject_t1113636619 * value)
	{
		___m_goThorns_216 = value;
		Il2CppCodeGenWriteBarrier((&___m_goThorns_216), value);
	}

	inline static int32_t get_offset_of_m_goSpores_217() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goSpores_217)); }
	inline GameObject_t1113636619 * get_m_goSpores_217() const { return ___m_goSpores_217; }
	inline GameObject_t1113636619 ** get_address_of_m_goSpores_217() { return &___m_goSpores_217; }
	inline void set_m_goSpores_217(GameObject_t1113636619 * value)
	{
		___m_goSpores_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_goSpores_217), value);
	}

	inline static int32_t get_offset_of_m_goBeans_218() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBeans_218)); }
	inline GameObject_t1113636619 * get_m_goBeans_218() const { return ___m_goBeans_218; }
	inline GameObject_t1113636619 ** get_address_of_m_goBeans_218() { return &___m_goBeans_218; }
	inline void set_m_goBeans_218(GameObject_t1113636619 * value)
	{
		___m_goBeans_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBeans_218), value);
	}

	inline static int32_t get_offset_of_m_goSpitBallTargets_219() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goSpitBallTargets_219)); }
	inline GameObject_t1113636619 * get_m_goSpitBallTargets_219() const { return ___m_goSpitBallTargets_219; }
	inline GameObject_t1113636619 ** get_address_of_m_goSpitBallTargets_219() { return &___m_goSpitBallTargets_219; }
	inline void set_m_goSpitBallTargets_219(GameObject_t1113636619 * value)
	{
		___m_goSpitBallTargets_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_goSpitBallTargets_219), value);
	}

	inline static int32_t get_offset_of_m_goNails_220() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goNails_220)); }
	inline GameObject_t1113636619 * get_m_goNails_220() const { return ___m_goNails_220; }
	inline GameObject_t1113636619 ** get_address_of_m_goNails_220() { return &___m_goNails_220; }
	inline void set_m_goNails_220(GameObject_t1113636619 * value)
	{
		___m_goNails_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_goNails_220), value);
	}

	inline static int32_t get_offset_of_m_goBallsBeNailed_221() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBallsBeNailed_221)); }
	inline GameObject_t1113636619 * get_m_goBallsBeNailed_221() const { return ___m_goBallsBeNailed_221; }
	inline GameObject_t1113636619 ** get_address_of_m_goBallsBeNailed_221() { return &___m_goBallsBeNailed_221; }
	inline void set_m_goBallsBeNailed_221(GameObject_t1113636619 * value)
	{
		___m_goBallsBeNailed_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBallsBeNailed_221), value);
	}

	inline static int32_t get_offset_of_m_goDestroyedBalls_222() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goDestroyedBalls_222)); }
	inline GameObject_t1113636619 * get_m_goDestroyedBalls_222() const { return ___m_goDestroyedBalls_222; }
	inline GameObject_t1113636619 ** get_address_of_m_goDestroyedBalls_222() { return &___m_goDestroyedBalls_222; }
	inline void set_m_goDestroyedBalls_222(GameObject_t1113636619 * value)
	{
		___m_goDestroyedBalls_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_goDestroyedBalls_222), value);
	}

	inline static int32_t get_offset_of_m_goBalls_223() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goBalls_223)); }
	inline GameObject_t1113636619 * get_m_goBalls_223() const { return ___m_goBalls_223; }
	inline GameObject_t1113636619 ** get_address_of_m_goBalls_223() { return &___m_goBalls_223; }
	inline void set_m_goBalls_223(GameObject_t1113636619 * value)
	{
		___m_goBalls_223 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBalls_223), value);
	}

	inline static int32_t get_offset_of_m_goJoyStick_224() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goJoyStick_224)); }
	inline GameObject_t1113636619 * get_m_goJoyStick_224() const { return ___m_goJoyStick_224; }
	inline GameObject_t1113636619 ** get_address_of_m_goJoyStick_224() { return &___m_goJoyStick_224; }
	inline void set_m_goJoyStick_224(GameObject_t1113636619 * value)
	{
		___m_goJoyStick_224 = value;
		Il2CppCodeGenWriteBarrier((&___m_goJoyStick_224), value);
	}

	inline static int32_t get_offset_of_s_nBallTotalNum_225() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___s_nBallTotalNum_225)); }
	inline int32_t get_s_nBallTotalNum_225() const { return ___s_nBallTotalNum_225; }
	inline int32_t* get_address_of_s_nBallTotalNum_225() { return &___s_nBallTotalNum_225; }
	inline void set_s_nBallTotalNum_225(int32_t value)
	{
		___s_nBallTotalNum_225 = value;
	}

	inline static int32_t get_offset_of_m_MainPlayer_227() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_MainPlayer_227)); }
	inline Player_t3266647312 * get_m_MainPlayer_227() const { return ___m_MainPlayer_227; }
	inline Player_t3266647312 ** get_address_of_m_MainPlayer_227() { return &___m_MainPlayer_227; }
	inline void set_m_MainPlayer_227(Player_t3266647312 * value)
	{
		___m_MainPlayer_227 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainPlayer_227), value);
	}

	inline static int32_t get_offset_of_m_goMainPlayer_228() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_goMainPlayer_228)); }
	inline GameObject_t1113636619 * get_m_goMainPlayer_228() const { return ___m_goMainPlayer_228; }
	inline GameObject_t1113636619 ** get_address_of_m_goMainPlayer_228() { return &___m_goMainPlayer_228; }
	inline void set_m_goMainPlayer_228(GameObject_t1113636619 * value)
	{
		___m_goMainPlayer_228 = value;
		Il2CppCodeGenWriteBarrier((&___m_goMainPlayer_228), value);
	}

	inline static int32_t get_offset_of_m_photonViewMainPlayer_229() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_photonViewMainPlayer_229)); }
	inline PhotonView_t2207721820 * get_m_photonViewMainPlayer_229() const { return ___m_photonViewMainPlayer_229; }
	inline PhotonView_t2207721820 ** get_address_of_m_photonViewMainPlayer_229() { return &___m_photonViewMainPlayer_229; }
	inline void set_m_photonViewMainPlayer_229(PhotonView_t2207721820 * value)
	{
		___m_photonViewMainPlayer_229 = value;
		Il2CppCodeGenWriteBarrier((&___m_photonViewMainPlayer_229), value);
	}

	inline static int32_t get_offset_of_m_textDebugInfo_230() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_textDebugInfo_230)); }
	inline Text_t1901882714 * get_m_textDebugInfo_230() const { return ___m_textDebugInfo_230; }
	inline Text_t1901882714 ** get_address_of_m_textDebugInfo_230() { return &___m_textDebugInfo_230; }
	inline void set_m_textDebugInfo_230(Text_t1901882714 * value)
	{
		___m_textDebugInfo_230 = value;
		Il2CppCodeGenWriteBarrier((&___m_textDebugInfo_230), value);
	}

	inline static int32_t get_offset_of_s_Params_231() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___s_Params_231)); }
	inline ObjectU5BU5D_t2843939325* get_s_Params_231() const { return ___s_Params_231; }
	inline ObjectU5BU5D_t2843939325** get_address_of_s_Params_231() { return &___s_Params_231; }
	inline void set_s_Params_231(ObjectU5BU5D_t2843939325* value)
	{
		___s_Params_231 = value;
		Il2CppCodeGenWriteBarrier((&___s_Params_231), value);
	}

	inline static int32_t get_offset_of_m_bInitingAllBalls_232() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bInitingAllBalls_232)); }
	inline bool get_m_bInitingAllBalls_232() const { return ___m_bInitingAllBalls_232; }
	inline bool* get_address_of_m_bInitingAllBalls_232() { return &___m_bInitingAllBalls_232; }
	inline void set_m_bInitingAllBalls_232(bool value)
	{
		___m_bInitingAllBalls_232 = value;
	}

	inline static int32_t get_offset_of_m_dicGamePlayDataConfig_234() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_dicGamePlayDataConfig_234)); }
	inline Dictionary_2_t1182523073 * get_m_dicGamePlayDataConfig_234() const { return ___m_dicGamePlayDataConfig_234; }
	inline Dictionary_2_t1182523073 ** get_address_of_m_dicGamePlayDataConfig_234() { return &___m_dicGamePlayDataConfig_234; }
	inline void set_m_dicGamePlayDataConfig_234(Dictionary_2_t1182523073 * value)
	{
		___m_dicGamePlayDataConfig_234 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicGamePlayDataConfig_234), value);
	}

	inline static int32_t get_offset_of_Array_235() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___Array_235)); }
	inline StringU5BU5DU5BU5D_t2611993717* get_Array_235() const { return ___Array_235; }
	inline StringU5BU5DU5BU5D_t2611993717** get_address_of_Array_235() { return &___Array_235; }
	inline void set_Array_235(StringU5BU5DU5BU5D_t2611993717* value)
	{
		___Array_235 = value;
		Il2CppCodeGenWriteBarrier((&___Array_235), value);
	}

	inline static int32_t get_offset_of_woca_236() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___woca_236)); }
	inline int32_t get_woca_236() const { return ___woca_236; }
	inline int32_t* get_address_of_woca_236() { return &___woca_236; }
	inline void set_woca_236(int32_t value)
	{
		___woca_236 = value;
	}

	inline static int32_t get_offset_of_m_fDeadProgressTimeCount_237() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fDeadProgressTimeCount_237)); }
	inline float get_m_fDeadProgressTimeCount_237() const { return ___m_fDeadProgressTimeCount_237; }
	inline float* get_address_of_m_fDeadProgressTimeCount_237() { return &___m_fDeadProgressTimeCount_237; }
	inline void set_m_fDeadProgressTimeCount_237(float value)
	{
		___m_fDeadProgressTimeCount_237 = value;
	}

	inline static int32_t get_offset_of_lstAssistAvatar_238() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___lstAssistAvatar_238)); }
	inline List_1_t1752731834 * get_lstAssistAvatar_238() const { return ___lstAssistAvatar_238; }
	inline List_1_t1752731834 ** get_address_of_lstAssistAvatar_238() { return &___lstAssistAvatar_238; }
	inline void set_lstAssistAvatar_238(List_1_t1752731834 * value)
	{
		___lstAssistAvatar_238 = value;
		Il2CppCodeGenWriteBarrier((&___lstAssistAvatar_238), value);
	}

	inline static int32_t get_offset_of_m_fDeadUICount_239() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fDeadUICount_239)); }
	inline float get_m_fDeadUICount_239() const { return ___m_fDeadUICount_239; }
	inline float* get_address_of_m_fDeadUICount_239() { return &___m_fDeadUICount_239; }
	inline void set_m_fDeadUICount_239(float value)
	{
		___m_fDeadUICount_239 = value;
	}

	inline static int32_t get_offset_of_m_nDeadStatus_240() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nDeadStatus_240)); }
	inline int32_t get_m_nDeadStatus_240() const { return ___m_nDeadStatus_240; }
	inline int32_t* get_address_of_m_nDeadStatus_240() { return &___m_nDeadStatus_240; }
	inline void set_m_nDeadStatus_240(int32_t value)
	{
		___m_nDeadStatus_240 = value;
	}

	inline static int32_t get_offset_of_m_bCtrlDown_242() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bCtrlDown_242)); }
	inline bool get_m_bCtrlDown_242() const { return ___m_bCtrlDown_242; }
	inline bool* get_address_of_m_bCtrlDown_242() { return &___m_bCtrlDown_242; }
	inline void set_m_bCtrlDown_242(bool value)
	{
		___m_bCtrlDown_242 = value;
	}

	inline static int32_t get_offset_of_m_bShowRank_243() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bShowRank_243)); }
	inline bool get_m_bShowRank_243() const { return ___m_bShowRank_243; }
	inline bool* get_address_of_m_bShowRank_243() { return &___m_bShowRank_243; }
	inline void set_m_bShowRank_243(bool value)
	{
		___m_bShowRank_243 = value;
	}

	inline static int32_t get_offset_of_m_bKeyDown_R_244() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bKeyDown_R_244)); }
	inline bool get_m_bKeyDown_R_244() const { return ___m_bKeyDown_R_244; }
	inline bool* get_address_of_m_bKeyDown_R_244() { return &___m_bKeyDown_R_244; }
	inline void set_m_bKeyDown_R_244(bool value)
	{
		___m_bKeyDown_R_244 = value;
	}

	inline static int32_t get_offset_of_m_bKeyDown_E_245() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bKeyDown_E_245)); }
	inline bool get_m_bKeyDown_E_245() const { return ___m_bKeyDown_E_245; }
	inline bool* get_address_of_m_bKeyDown_E_245() { return &___m_bKeyDown_E_245; }
	inline void set_m_bKeyDown_E_245(bool value)
	{
		___m_bKeyDown_E_245 = value;
	}

	inline static int32_t get_offset_of_m_fSplitTimeCount_246() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSplitTimeCount_246)); }
	inline float get_m_fSplitTimeCount_246() const { return ___m_fSplitTimeCount_246; }
	inline float* get_address_of_m_fSplitTimeCount_246() { return &___m_fSplitTimeCount_246; }
	inline void set_m_fSplitTimeCount_246(float value)
	{
		___m_fSplitTimeCount_246 = value;
	}

	inline static int32_t get_offset_of_m_fLababTimeCount_247() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLababTimeCount_247)); }
	inline float get_m_fLababTimeCount_247() const { return ___m_fLababTimeCount_247; }
	inline float* get_address_of_m_fLababTimeCount_247() { return &___m_fLababTimeCount_247; }
	inline void set_m_fLababTimeCount_247(float value)
	{
		___m_fLababTimeCount_247 = value;
	}

	inline static int32_t get_offset_of_m_fArenaRange_248() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fArenaRange_248)); }
	inline float get_m_fArenaRange_248() const { return ___m_fArenaRange_248; }
	inline float* get_address_of_m_fArenaRange_248() { return &___m_fArenaRange_248; }
	inline void set_m_fArenaRange_248(float value)
	{
		___m_fArenaRange_248 = value;
	}

	inline static int32_t get_offset_of_m_nBeanTotalNumDueToRange_249() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nBeanTotalNumDueToRange_249)); }
	inline float get_m_nBeanTotalNumDueToRange_249() const { return ___m_nBeanTotalNumDueToRange_249; }
	inline float* get_address_of_m_nBeanTotalNumDueToRange_249() { return &___m_nBeanTotalNumDueToRange_249; }
	inline void set_m_nBeanTotalNumDueToRange_249(float value)
	{
		___m_nBeanTotalNumDueToRange_249 = value;
	}

	inline static int32_t get_offset_of_m_nCurBeanTotalNum_250() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nCurBeanTotalNum_250)); }
	inline float get_m_nCurBeanTotalNum_250() const { return ___m_nCurBeanTotalNum_250; }
	inline float* get_address_of_m_nCurBeanTotalNum_250() { return &___m_nCurBeanTotalNum_250; }
	inline void set_m_nCurBeanTotalNum_250(float value)
	{
		___m_nCurBeanTotalNum_250 = value;
	}

	inline static int32_t get_offset_of_m_nCurGeneraedByMeBeanTotalNum_251() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nCurGeneraedByMeBeanTotalNum_251)); }
	inline float get_m_nCurGeneraedByMeBeanTotalNum_251() const { return ___m_nCurGeneraedByMeBeanTotalNum_251; }
	inline float* get_address_of_m_nCurGeneraedByMeBeanTotalNum_251() { return &___m_nCurGeneraedByMeBeanTotalNum_251; }
	inline void set_m_nCurGeneraedByMeBeanTotalNum_251(float value)
	{
		___m_nCurGeneraedByMeBeanTotalNum_251 = value;
	}

	inline static int32_t get_offset_of_m_nCurGeneraedByMeThornTotalNum_252() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nCurGeneraedByMeThornTotalNum_252)); }
	inline int32_t get_m_nCurGeneraedByMeThornTotalNum_252() const { return ___m_nCurGeneraedByMeThornTotalNum_252; }
	inline int32_t* get_address_of_m_nCurGeneraedByMeThornTotalNum_252() { return &___m_nCurGeneraedByMeThornTotalNum_252; }
	inline void set_m_nCurGeneraedByMeThornTotalNum_252(int32_t value)
	{
		___m_nCurGeneraedByMeThornTotalNum_252 = value;
	}

	inline static int32_t get_offset_of_m_nThornTotalNumDueToRange_253() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nThornTotalNumDueToRange_253)); }
	inline float get_m_nThornTotalNumDueToRange_253() const { return ___m_nThornTotalNumDueToRange_253; }
	inline float* get_address_of_m_nThornTotalNumDueToRange_253() { return &___m_nThornTotalNumDueToRange_253; }
	inline void set_m_nThornTotalNumDueToRange_253(float value)
	{
		___m_nThornTotalNumDueToRange_253 = value;
	}

	inline static int32_t get_offset_of_m_nCurThornTotalNum_254() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nCurThornTotalNum_254)); }
	inline float get_m_nCurThornTotalNum_254() const { return ___m_nCurThornTotalNum_254; }
	inline float* get_address_of_m_nCurThornTotalNum_254() { return &___m_nCurThornTotalNum_254; }
	inline void set_m_nCurThornTotalNum_254(float value)
	{
		___m_nCurThornTotalNum_254 = value;
	}

	inline static int32_t get_offset_of_m_fGenerateBeanTime_255() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGenerateBeanTime_255)); }
	inline float get_m_fGenerateBeanTime_255() const { return ___m_fGenerateBeanTime_255; }
	inline float* get_address_of_m_fGenerateBeanTime_255() { return &___m_fGenerateBeanTime_255; }
	inline void set_m_fGenerateBeanTime_255(float value)
	{
		___m_fGenerateBeanTime_255 = value;
	}

	inline static int32_t get_offset_of_m_fGenerateThornTime_256() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGenerateThornTime_256)); }
	inline float get_m_fGenerateThornTime_256() const { return ___m_fGenerateThornTime_256; }
	inline float* get_address_of_m_fGenerateThornTime_256() { return &___m_fGenerateThornTime_256; }
	inline void set_m_fGenerateThornTime_256(float value)
	{
		___m_fGenerateThornTime_256 = value;
	}

	inline static int32_t get_offset_of_m_fWorldLeft_258() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fWorldLeft_258)); }
	inline float get_m_fWorldLeft_258() const { return ___m_fWorldLeft_258; }
	inline float* get_address_of_m_fWorldLeft_258() { return &___m_fWorldLeft_258; }
	inline void set_m_fWorldLeft_258(float value)
	{
		___m_fWorldLeft_258 = value;
	}

	inline static int32_t get_offset_of_m_fWorldRight_259() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fWorldRight_259)); }
	inline float get_m_fWorldRight_259() const { return ___m_fWorldRight_259; }
	inline float* get_address_of_m_fWorldRight_259() { return &___m_fWorldRight_259; }
	inline void set_m_fWorldRight_259(float value)
	{
		___m_fWorldRight_259 = value;
	}

	inline static int32_t get_offset_of_m_fWorldBottom_260() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fWorldBottom_260)); }
	inline float get_m_fWorldBottom_260() const { return ___m_fWorldBottom_260; }
	inline float* get_address_of_m_fWorldBottom_260() { return &___m_fWorldBottom_260; }
	inline void set_m_fWorldBottom_260(float value)
	{
		___m_fWorldBottom_260 = value;
	}

	inline static int32_t get_offset_of_m_fWorldTop_261() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fWorldTop_261)); }
	inline float get_m_fWorldTop_261() const { return ___m_fWorldTop_261; }
	inline float* get_address_of_m_fWorldTop_261() { return &___m_fWorldTop_261; }
	inline void set_m_fWorldTop_261(float value)
	{
		___m_fWorldTop_261 = value;
	}

	inline static int32_t get_offset_of_vecScreenMin_262() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___vecScreenMin_262)); }
	inline Vector3_t3722313464  get_vecScreenMin_262() const { return ___vecScreenMin_262; }
	inline Vector3_t3722313464 * get_address_of_vecScreenMin_262() { return &___vecScreenMin_262; }
	inline void set_vecScreenMin_262(Vector3_t3722313464  value)
	{
		___vecScreenMin_262 = value;
	}

	inline static int32_t get_offset_of_vecScreenMax_263() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___vecScreenMax_263)); }
	inline Vector3_t3722313464  get_vecScreenMax_263() const { return ___vecScreenMax_263; }
	inline Vector3_t3722313464 * get_address_of_vecScreenMax_263() { return &___vecScreenMax_263; }
	inline void set_vecScreenMax_263(Vector3_t3722313464  value)
	{
		___vecScreenMax_263 = value;
	}

	inline static int32_t get_offset_of_vecWorldMin_264() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___vecWorldMin_264)); }
	inline Vector3_t3722313464  get_vecWorldMin_264() const { return ___vecWorldMin_264; }
	inline Vector3_t3722313464 * get_address_of_vecWorldMin_264() { return &___vecWorldMin_264; }
	inline void set_vecWorldMin_264(Vector3_t3722313464  value)
	{
		___vecWorldMin_264 = value;
	}

	inline static int32_t get_offset_of_vecWorldMax_265() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___vecWorldMax_265)); }
	inline Vector3_t3722313464  get_vecWorldMax_265() const { return ___vecWorldMax_265; }
	inline Vector3_t3722313464 * get_address_of_vecWorldMax_265() { return &___vecWorldMax_265; }
	inline void set_vecWorldMax_265(Vector3_t3722313464  value)
	{
		___vecWorldMax_265 = value;
	}

	inline static int32_t get_offset_of_m_dicAllThorns_266() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_dicAllThorns_266)); }
	inline Dictionary_2_t830184269 * get_m_dicAllThorns_266() const { return ___m_dicAllThorns_266; }
	inline Dictionary_2_t830184269 ** get_address_of_m_dicAllThorns_266() { return &___m_dicAllThorns_266; }
	inline void set_m_dicAllThorns_266(Dictionary_2_t830184269 * value)
	{
		___m_dicAllThorns_266 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAllThorns_266), value);
	}

	inline static int32_t get_offset_of_m_fForceSpitPercent_267() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fForceSpitPercent_267)); }
	inline float get_m_fForceSpitPercent_267() const { return ___m_fForceSpitPercent_267; }
	inline float* get_address_of_m_fForceSpitPercent_267() { return &___m_fForceSpitPercent_267; }
	inline void set_m_fForceSpitPercent_267(float value)
	{
		___m_fForceSpitPercent_267 = value;
	}

	inline static int32_t get_offset_of_m_bForceSpit_268() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bForceSpit_268)); }
	inline bool get_m_bForceSpit_268() const { return ___m_bForceSpit_268; }
	inline bool* get_address_of_m_bForceSpit_268() { return &___m_bForceSpit_268; }
	inline void set_m_bForceSpit_268(bool value)
	{
		___m_bForceSpit_268 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallCurColdDownTime_269() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallCurColdDownTime_269)); }
	inline float get_m_fSpitBallCurColdDownTime_269() const { return ___m_fSpitBallCurColdDownTime_269; }
	inline float* get_address_of_m_fSpitBallCurColdDownTime_269() { return &___m_fSpitBallCurColdDownTime_269; }
	inline void set_m_fSpitBallCurColdDownTime_269(float value)
	{
		___m_fSpitBallCurColdDownTime_269 = value;
	}

	inline static int32_t get_offset_of_m_fCurCamSize_270() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCurCamSize_270)); }
	inline float get_m_fCurCamSize_270() const { return ___m_fCurCamSize_270; }
	inline float* get_address_of_m_fCurCamSize_270() { return &___m_fCurCamSize_270; }
	inline void set_m_fCurCamSize_270(float value)
	{
		___m_fCurCamSize_270 = value;
	}

	inline static int32_t get_offset_of_m_fCamSizeProtectTime_271() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCamSizeProtectTime_271)); }
	inline float get_m_fCamSizeProtectTime_271() const { return ___m_fCamSizeProtectTime_271; }
	inline float* get_address_of_m_fCamSizeProtectTime_271() { return &___m_fCamSizeProtectTime_271; }
	inline void set_m_fCamSizeProtectTime_271(float value)
	{
		___m_fCamSizeProtectTime_271 = value;
	}

	inline static int32_t get_offset_of_m_fForceSpitCurTime_272() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fForceSpitCurTime_272)); }
	inline float get_m_fForceSpitCurTime_272() const { return ___m_fForceSpitCurTime_272; }
	inline float* get_address_of_m_fForceSpitCurTime_272() { return &___m_fForceSpitCurTime_272; }
	inline void set_m_fForceSpitCurTime_272(float value)
	{
		___m_fForceSpitCurTime_272 = value;
	}

	inline static int32_t get_offset_of_m_fCurColdDownTime_BecomeThor_Count_273() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCurColdDownTime_BecomeThor_Count_273)); }
	inline float get_m_fCurColdDownTime_BecomeThor_Count_273() const { return ___m_fCurColdDownTime_BecomeThor_Count_273; }
	inline float* get_address_of_m_fCurColdDownTime_BecomeThor_Count_273() { return &___m_fCurColdDownTime_BecomeThor_Count_273; }
	inline void set_m_fCurColdDownTime_BecomeThor_Count_273(float value)
	{
		___m_fCurColdDownTime_BecomeThor_Count_273 = value;
	}

	inline static int32_t get_offset_of_m_fCurColdDownTime_BecomeThorn_274() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCurColdDownTime_BecomeThorn_274)); }
	inline float get_m_fCurColdDownTime_BecomeThorn_274() const { return ___m_fCurColdDownTime_BecomeThorn_274; }
	inline float* get_address_of_m_fCurColdDownTime_BecomeThorn_274() { return &___m_fCurColdDownTime_BecomeThorn_274; }
	inline void set_m_fCurColdDownTime_BecomeThorn_274(float value)
	{
		___m_fCurColdDownTime_BecomeThorn_274 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldCurColdDownTime_275() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fUnfoldCurColdDownTime_275)); }
	inline float get_m_fUnfoldCurColdDownTime_275() const { return ___m_fUnfoldCurColdDownTime_275; }
	inline float* get_address_of_m_fUnfoldCurColdDownTime_275() { return &___m_fUnfoldCurColdDownTime_275; }
	inline void set_m_fUnfoldCurColdDownTime_275(float value)
	{
		___m_fUnfoldCurColdDownTime_275 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeCurColdDownTime_276() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitSporeCurColdDownTime_276)); }
	inline float get_m_fSpitSporeCurColdDownTime_276() const { return ___m_fSpitSporeCurColdDownTime_276; }
	inline float* get_address_of_m_fSpitSporeCurColdDownTime_276() { return &___m_fSpitSporeCurColdDownTime_276; }
	inline void set_m_fSpitSporeCurColdDownTime_276(float value)
	{
		___m_fSpitSporeCurColdDownTime_276 = value;
	}

	inline static int32_t get_offset_of_m_fSpitNailCurColdDownTime_277() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitNailCurColdDownTime_277)); }
	inline float get_m_fSpitNailCurColdDownTime_277() const { return ___m_fSpitNailCurColdDownTime_277; }
	inline float* get_address_of_m_fSpitNailCurColdDownTime_277() { return &___m_fSpitNailCurColdDownTime_277; }
	inline void set_m_fSpitNailCurColdDownTime_277(float value)
	{
		___m_fSpitNailCurColdDownTime_277 = value;
	}

	inline static int32_t get_offset_of_lstTemp_278() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___lstTemp_278)); }
	inline List_1_t3678741308 * get_lstTemp_278() const { return ___lstTemp_278; }
	inline List_1_t3678741308 ** get_address_of_lstTemp_278() { return &___lstTemp_278; }
	inline void set_lstTemp_278(List_1_t3678741308 * value)
	{
		___lstTemp_278 = value;
		Il2CppCodeGenWriteBarrier((&___lstTemp_278), value);
	}

	inline static int32_t get_offset_of_m_bTestFucking_279() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bTestFucking_279)); }
	inline bool get_m_bTestFucking_279() const { return ___m_bTestFucking_279; }
	inline bool* get_address_of_m_bTestFucking_279() { return &___m_bTestFucking_279; }
	inline void set_m_bTestFucking_279(bool value)
	{
		___m_bTestFucking_279 = value;
	}

	inline static int32_t get_offset_of_m_nFuckCount_280() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nFuckCount_280)); }
	inline int32_t get_m_nFuckCount_280() const { return ___m_nFuckCount_280; }
	inline int32_t* get_address_of_m_nFuckCount_280() { return &___m_nFuckCount_280; }
	inline void set_m_nFuckCount_280(int32_t value)
	{
		___m_nFuckCount_280 = value;
	}

	inline static int32_t get_offset_of_m_fFuckTime_281() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fFuckTime_281)); }
	inline float get_m_fFuckTime_281() const { return ___m_fFuckTime_281; }
	inline float* get_address_of_m_fFuckTime_281() { return &___m_fFuckTime_281; }
	inline void set_m_fFuckTime_281(float value)
	{
		___m_fFuckTime_281 = value;
	}

	inline static int32_t get_offset_of_m_lstPlayers_282() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_lstPlayers_282)); }
	inline List_1_t443754758 * get_m_lstPlayers_282() const { return ___m_lstPlayers_282; }
	inline List_1_t443754758 ** get_address_of_m_lstPlayers_282() { return &___m_lstPlayers_282; }
	inline void set_m_lstPlayers_282(List_1_t443754758 * value)
	{
		___m_lstPlayers_282 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPlayers_282), value);
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_Gold_283() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_Gold_283)); }
	inline float get_m_fSkillColdDownCount_Gold_283() const { return ___m_fSkillColdDownCount_Gold_283; }
	inline float* get_address_of_m_fSkillColdDownCount_Gold_283() { return &___m_fSkillColdDownCount_Gold_283; }
	inline void set_m_fSkillColdDownCount_Gold_283(float value)
	{
		___m_fSkillColdDownCount_Gold_283 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDown_Gold_284() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDown_Gold_284)); }
	inline float get_m_fSkillColdDown_Gold_284() const { return ___m_fSkillColdDown_Gold_284; }
	inline float* get_address_of_m_fSkillColdDown_Gold_284() { return &___m_fSkillColdDown_Gold_284; }
	inline void set_m_fSkillColdDown_Gold_284(float value)
	{
		___m_fSkillColdDown_Gold_284 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_Henzy_285() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_Henzy_285)); }
	inline float get_m_fSkillColdDownCount_Henzy_285() const { return ___m_fSkillColdDownCount_Henzy_285; }
	inline float* get_address_of_m_fSkillColdDownCount_Henzy_285() { return &___m_fSkillColdDownCount_Henzy_285; }
	inline void set_m_fSkillColdDownCount_Henzy_285(float value)
	{
		___m_fSkillColdDownCount_Henzy_285 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDown_Henzy_286() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDown_Henzy_286)); }
	inline float get_m_fSkillColdDown_Henzy_286() const { return ___m_fSkillColdDown_Henzy_286; }
	inline float* get_address_of_m_fSkillColdDown_Henzy_286() { return &___m_fSkillColdDown_Henzy_286; }
	inline void set_m_fSkillColdDown_Henzy_286(float value)
	{
		___m_fSkillColdDown_Henzy_286 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_MergeAll_287() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_MergeAll_287)); }
	inline float get_m_fSkillColdDownCount_MergeAll_287() const { return ___m_fSkillColdDownCount_MergeAll_287; }
	inline float* get_address_of_m_fSkillColdDownCount_MergeAll_287() { return &___m_fSkillColdDownCount_MergeAll_287; }
	inline void set_m_fSkillColdDownCount_MergeAll_287(float value)
	{
		___m_fSkillColdDownCount_MergeAll_287 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDown_MergeAll_288() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDown_MergeAll_288)); }
	inline float get_m_fSkillColdDown_MergeAll_288() const { return ___m_fSkillColdDown_MergeAll_288; }
	inline float* get_address_of_m_fSkillColdDown_MergeAll_288() { return &___m_fSkillColdDown_MergeAll_288; }
	inline void set_m_fSkillColdDown_MergeAll_288(float value)
	{
		___m_fSkillColdDown_MergeAll_288 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_MagicShield_289() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_MagicShield_289)); }
	inline float get_m_fSkillColdDownCount_MagicShield_289() const { return ___m_fSkillColdDownCount_MagicShield_289; }
	inline float* get_address_of_m_fSkillColdDownCount_MagicShield_289() { return &___m_fSkillColdDownCount_MagicShield_289; }
	inline void set_m_fSkillColdDownCount_MagicShield_289(float value)
	{
		___m_fSkillColdDownCount_MagicShield_289 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDown_MagicShield_290() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDown_MagicShield_290)); }
	inline float get_m_fSkillColdDown_MagicShield_290() const { return ___m_fSkillColdDown_MagicShield_290; }
	inline float* get_address_of_m_fSkillColdDown_MagicShield_290() { return &___m_fSkillColdDown_MagicShield_290; }
	inline void set_m_fSkillColdDown_MagicShield_290(float value)
	{
		___m_fSkillColdDown_MagicShield_290 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDown_Annihilate_291() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDown_Annihilate_291)); }
	inline float get_m_fSkillColdDown_Annihilate_291() const { return ___m_fSkillColdDown_Annihilate_291; }
	inline float* get_address_of_m_fSkillColdDown_Annihilate_291() { return &___m_fSkillColdDown_Annihilate_291; }
	inline void set_m_fSkillColdDown_Annihilate_291(float value)
	{
		___m_fSkillColdDown_Annihilate_291 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_Annihilate_292() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_Annihilate_292)); }
	inline float get_m_fSkillColdDownCount_Annihilate_292() const { return ___m_fSkillColdDownCount_Annihilate_292; }
	inline float* get_address_of_m_fSkillColdDownCount_Annihilate_292() { return &___m_fSkillColdDownCount_Annihilate_292; }
	inline void set_m_fSkillColdDownCount_Annihilate_292(float value)
	{
		___m_fSkillColdDownCount_Annihilate_292 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_Sneak_293() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_Sneak_293)); }
	inline float get_m_fSkillColdDownCount_Sneak_293() const { return ___m_fSkillColdDownCount_Sneak_293; }
	inline float* get_address_of_m_fSkillColdDownCount_Sneak_293() { return &___m_fSkillColdDownCount_Sneak_293; }
	inline void set_m_fSkillColdDownCount_Sneak_293(float value)
	{
		___m_fSkillColdDownCount_Sneak_293 = value;
	}

	inline static int32_t get_offset_of_m_fSkillColdDownCount_294() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSkillColdDownCount_294)); }
	inline float get_m_fSkillColdDownCount_294() const { return ___m_fSkillColdDownCount_294; }
	inline float* get_address_of_m_fSkillColdDownCount_294() { return &___m_fSkillColdDownCount_294; }
	inline void set_m_fSkillColdDownCount_294(float value)
	{
		___m_fSkillColdDownCount_294 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfTimeCount_295() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fSpitBallHalfTimeCount_295)); }
	inline float get_m_fSpitBallHalfTimeCount_295() const { return ___m_fSpitBallHalfTimeCount_295; }
	inline float* get_address_of_m_fSpitBallHalfTimeCount_295() { return &___m_fSpitBallHalfTimeCount_295; }
	inline void set_m_fSpitBallHalfTimeCount_295(float value)
	{
		___m_fSpitBallHalfTimeCount_295 = value;
	}

	inline static int32_t get_offset_of_m_bPreOneBtnSplit_296() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bPreOneBtnSplit_296)); }
	inline bool get_m_bPreOneBtnSplit_296() const { return ___m_bPreOneBtnSplit_296; }
	inline bool* get_address_of_m_bPreOneBtnSplit_296() { return &___m_bPreOneBtnSplit_296; }
	inline void set_m_bPreOneBtnSplit_296(bool value)
	{
		___m_bPreOneBtnSplit_296 = value;
	}

	inline static int32_t get_offset_of_m_fPreOneBtnSplitTimeCount_297() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fPreOneBtnSplitTimeCount_297)); }
	inline float get_m_fPreOneBtnSplitTimeCount_297() const { return ___m_fPreOneBtnSplitTimeCount_297; }
	inline float* get_address_of_m_fPreOneBtnSplitTimeCount_297() { return &___m_fPreOneBtnSplitTimeCount_297; }
	inline void set_m_fPreOneBtnSplitTimeCount_297(float value)
	{
		___m_fPreOneBtnSplitTimeCount_297 = value;
	}

	inline static int32_t get_offset_of_m_fCurOneBtnSplitDis_298() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fCurOneBtnSplitDis_298)); }
	inline float get_m_fCurOneBtnSplitDis_298() const { return ___m_fCurOneBtnSplitDis_298; }
	inline float* get_address_of_m_fCurOneBtnSplitDis_298() { return &___m_fCurOneBtnSplitDis_298; }
	inline void set_m_fCurOneBtnSplitDis_298(float value)
	{
		___m_fCurOneBtnSplitDis_298 = value;
	}

	inline static int32_t get_offset_of_m_nSplitTouchFingerId_299() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nSplitTouchFingerId_299)); }
	inline int32_t get_m_nSplitTouchFingerId_299() const { return ___m_nSplitTouchFingerId_299; }
	inline int32_t* get_address_of_m_nSplitTouchFingerId_299() { return &___m_nSplitTouchFingerId_299; }
	inline void set_m_nSplitTouchFingerId_299(int32_t value)
	{
		___m_nSplitTouchFingerId_299 = value;
	}

	inline static int32_t get_offset_of_m_vecSplitTouchStartPos_300() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecSplitTouchStartPos_300)); }
	inline Vector3_t3722313464  get_m_vecSplitTouchStartPos_300() const { return ___m_vecSplitTouchStartPos_300; }
	inline Vector3_t3722313464 * get_address_of_m_vecSplitTouchStartPos_300() { return &___m_vecSplitTouchStartPos_300; }
	inline void set_m_vecSplitTouchStartPos_300(Vector3_t3722313464  value)
	{
		___m_vecSplitTouchStartPos_300 = value;
	}

	inline static int32_t get_offset_of_m_vecCurSplitTouchPos_301() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecCurSplitTouchPos_301)); }
	inline Vector3_t3722313464  get_m_vecCurSplitTouchPos_301() const { return ___m_vecCurSplitTouchPos_301; }
	inline Vector3_t3722313464 * get_address_of_m_vecCurSplitTouchPos_301() { return &___m_vecCurSplitTouchPos_301; }
	inline void set_m_vecCurSplitTouchPos_301(Vector3_t3722313464  value)
	{
		___m_vecCurSplitTouchPos_301 = value;
	}

	inline static int32_t get_offset_of_m_vecSplitDirction_302() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecSplitDirction_302)); }
	inline Vector2_t2156229523  get_m_vecSplitDirction_302() const { return ___m_vecSplitDirction_302; }
	inline Vector2_t2156229523 * get_address_of_m_vecSplitDirction_302() { return &___m_vecSplitDirction_302; }
	inline void set_m_vecSplitDirction_302(Vector2_t2156229523  value)
	{
		___m_vecSplitDirction_302 = value;
	}

	inline static int32_t get_offset_of_m_nInitingBallsCount_303() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nInitingBallsCount_303)); }
	inline int32_t get_m_nInitingBallsCount_303() const { return ___m_nInitingBallsCount_303; }
	inline int32_t* get_address_of_m_nInitingBallsCount_303() { return &___m_nInitingBallsCount_303; }
	inline void set_m_nInitingBallsCount_303(int32_t value)
	{
		___m_nInitingBallsCount_303 = value;
	}

	inline static int32_t get_offset_of_m_fInitingBallsTime_304() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fInitingBallsTime_304)); }
	inline float get_m_fInitingBallsTime_304() const { return ___m_fInitingBallsTime_304; }
	inline float* get_address_of_m_fInitingBallsTime_304() { return &___m_fInitingBallsTime_304; }
	inline void set_m_fInitingBallsTime_304(float value)
	{
		___m_fInitingBallsTime_304 = value;
	}

	inline static int32_t get_offset_of_m_vecStartPos_305() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecStartPos_305)); }
	inline Vector3_t3722313464  get_m_vecStartPos_305() const { return ___m_vecStartPos_305; }
	inline Vector3_t3722313464 * get_address_of_m_vecStartPos_305() { return &___m_vecStartPos_305; }
	inline void set_m_vecStartPos_305(Vector3_t3722313464  value)
	{
		___m_vecStartPos_305 = value;
	}

	inline static int32_t get_offset_of_m_nFingerId_306() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_nFingerId_306)); }
	inline int32_t get_m_nFingerId_306() const { return ___m_nFingerId_306; }
	inline int32_t* get_address_of_m_nFingerId_306() { return &___m_nFingerId_306; }
	inline void set_m_nFingerId_306(int32_t value)
	{
		___m_nFingerId_306 = value;
	}

	inline static int32_t get_offset_of_m_bSpitTouching_307() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bSpitTouching_307)); }
	inline bool get_m_bSpitTouching_307() const { return ___m_bSpitTouching_307; }
	inline bool* get_address_of_m_bSpitTouching_307() { return &___m_bSpitTouching_307; }
	inline void set_m_bSpitTouching_307(bool value)
	{
		___m_bSpitTouching_307 = value;
	}

	inline static int32_t get_offset_of_m_vecCurPos_308() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecCurPos_308)); }
	inline Vector3_t3722313464  get_m_vecCurPos_308() const { return ___m_vecCurPos_308; }
	inline Vector3_t3722313464 * get_address_of_m_vecCurPos_308() { return &___m_vecCurPos_308; }
	inline void set_m_vecCurPos_308(Vector3_t3722313464  value)
	{
		___m_vecCurPos_308 = value;
	}

	inline static int32_t get_offset_of_m_vecSpitDirction_309() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_vecSpitDirction_309)); }
	inline Vector2_t2156229523  get_m_vecSpitDirction_309() const { return ___m_vecSpitDirction_309; }
	inline Vector2_t2156229523 * get_address_of_m_vecSpitDirction_309() { return &___m_vecSpitDirction_309; }
	inline void set_m_vecSpitDirction_309(Vector2_t2156229523  value)
	{
		___m_vecSpitDirction_309 = value;
	}

	inline static int32_t get_offset_of_m_bSpitTouching_Test_310() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bSpitTouching_Test_310)); }
	inline bool get_m_bSpitTouching_Test_310() const { return ___m_bSpitTouching_Test_310; }
	inline bool* get_address_of_m_bSpitTouching_Test_310() { return &___m_bSpitTouching_Test_310; }
	inline void set_m_bSpitTouching_Test_310(bool value)
	{
		___m_bSpitTouching_Test_310 = value;
	}

	inline static int32_t get_offset_of_m_fLeftTime_311() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fLeftTime_311)); }
	inline float get_m_fLeftTime_311() const { return ___m_fLeftTime_311; }
	inline float* get_address_of_m_fLeftTime_311() { return &___m_fLeftTime_311; }
	inline void set_m_fLeftTime_311(float value)
	{
		___m_fLeftTime_311 = value;
	}

	inline static int32_t get_offset_of_m_fGameStartTime_312() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGameStartTime_312)); }
	inline float get_m_fGameStartTime_312() const { return ___m_fGameStartTime_312; }
	inline float* get_address_of_m_fGameStartTime_312() { return &___m_fGameStartTime_312; }
	inline void set_m_fGameStartTime_312(float value)
	{
		___m_fGameStartTime_312 = value;
	}

	inline static int32_t get_offset_of_m_fTimeEclapse_313() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fTimeEclapse_313)); }
	inline float get_m_fTimeEclapse_313() const { return ___m_fTimeEclapse_313; }
	inline float* get_address_of_m_fTimeEclapse_313() { return &___m_fTimeEclapse_313; }
	inline void set_m_fTimeEclapse_313(float value)
	{
		___m_fTimeEclapse_313 = value;
	}

	inline static int32_t get_offset_of_m_fProcessLeftTimeCount_314() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fProcessLeftTimeCount_314)); }
	inline float get_m_fProcessLeftTimeCount_314() const { return ___m_fProcessLeftTimeCount_314; }
	inline float* get_address_of_m_fProcessLeftTimeCount_314() { return &___m_fProcessLeftTimeCount_314; }
	inline void set_m_fProcessLeftTimeCount_314(float value)
	{
		___m_fProcessLeftTimeCount_314 = value;
	}

	inline static int32_t get_offset_of_m_bGameOver_315() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bGameOver_315)); }
	inline bool get_m_bGameOver_315() const { return ___m_bGameOver_315; }
	inline bool* get_address_of_m_bGameOver_315() { return &___m_bGameOver_315; }
	inline void set_m_bGameOver_315(bool value)
	{
		___m_bGameOver_315 = value;
	}

	inline static int32_t get_offset_of_m_bGameOverCounting_316() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_bGameOverCounting_316)); }
	inline bool get_m_bGameOverCounting_316() const { return ___m_bGameOverCounting_316; }
	inline bool* get_address_of_m_bGameOverCounting_316() { return &___m_bGameOverCounting_316; }
	inline void set_m_bGameOverCounting_316(bool value)
	{
		___m_bGameOverCounting_316 = value;
	}

	inline static int32_t get_offset_of_m_fGameOverPanelTimeLapse_317() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fGameOverPanelTimeLapse_317)); }
	inline float get_m_fGameOverPanelTimeLapse_317() const { return ___m_fGameOverPanelTimeLapse_317; }
	inline float* get_address_of_m_fGameOverPanelTimeLapse_317() { return &___m_fGameOverPanelTimeLapse_317; }
	inline void set_m_fGameOverPanelTimeLapse_317(float value)
	{
		___m_fGameOverPanelTimeLapse_317 = value;
	}

	inline static int32_t get_offset_of_m_fScreenWidth_318() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fScreenWidth_318)); }
	inline float get_m_fScreenWidth_318() const { return ___m_fScreenWidth_318; }
	inline float* get_address_of_m_fScreenWidth_318() { return &___m_fScreenWidth_318; }
	inline void set_m_fScreenWidth_318(float value)
	{
		___m_fScreenWidth_318 = value;
	}

	inline static int32_t get_offset_of_m_fScreenHeight_319() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_fScreenHeight_319)); }
	inline float get_m_fScreenHeight_319() const { return ___m_fScreenHeight_319; }
	inline float* get_address_of_m_fScreenHeight_319() { return &___m_fScreenHeight_319; }
	inline void set_m_fScreenHeight_319(float value)
	{
		___m_fScreenHeight_319 = value;
	}

	inline static int32_t get_offset_of__inputCheatPanelExplodeThornId_320() { return static_cast<int32_t>(offsetof(Main_t2227614074, ____inputCheatPanelExplodeThornId_320)); }
	inline InputField_t3762917431 * get__inputCheatPanelExplodeThornId_320() const { return ____inputCheatPanelExplodeThornId_320; }
	inline InputField_t3762917431 ** get_address_of__inputCheatPanelExplodeThornId_320() { return &____inputCheatPanelExplodeThornId_320; }
	inline void set__inputCheatPanelExplodeThornId_320(InputField_t3762917431 * value)
	{
		____inputCheatPanelExplodeThornId_320 = value;
		Il2CppCodeGenWriteBarrier((&____inputCheatPanelExplodeThornId_320), value);
	}

	inline static int32_t get_offset_of_m_szCheatExplodeThornId_321() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___m_szCheatExplodeThornId_321)); }
	inline String_t* get_m_szCheatExplodeThornId_321() const { return ___m_szCheatExplodeThornId_321; }
	inline String_t** get_address_of_m_szCheatExplodeThornId_321() { return &___m_szCheatExplodeThornId_321; }
	inline void set_m_szCheatExplodeThornId_321(String_t* value)
	{
		___m_szCheatExplodeThornId_321 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCheatExplodeThornId_321), value);
	}
};

struct Main_t2227614074_StaticFields
{
public:
	// Main Main::s_Instance
	Main_t2227614074 * ___s_Instance_41;
	// System.Single Main::BALL_MIN_SIZE
	float ___BALL_MIN_SIZE_101;
	// System.Single Main::m_fBallMinVolume
	float ___m_fBallMinVolume_103;
	// System.Collections.Generic.List`1<PhotonView> Main::m_lstDelayInit
	List_1_t3679796562 * ___m_lstDelayInit_233;
	// System.Collections.Generic.List`1<Player> Main::m_lstDelay
	List_1_t443754758 * ___m_lstDelay_241;

public:
	inline static int32_t get_offset_of_s_Instance_41() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___s_Instance_41)); }
	inline Main_t2227614074 * get_s_Instance_41() const { return ___s_Instance_41; }
	inline Main_t2227614074 ** get_address_of_s_Instance_41() { return &___s_Instance_41; }
	inline void set_s_Instance_41(Main_t2227614074 * value)
	{
		___s_Instance_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_41), value);
	}

	inline static int32_t get_offset_of_BALL_MIN_SIZE_101() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___BALL_MIN_SIZE_101)); }
	inline float get_BALL_MIN_SIZE_101() const { return ___BALL_MIN_SIZE_101; }
	inline float* get_address_of_BALL_MIN_SIZE_101() { return &___BALL_MIN_SIZE_101; }
	inline void set_BALL_MIN_SIZE_101(float value)
	{
		___BALL_MIN_SIZE_101 = value;
	}

	inline static int32_t get_offset_of_m_fBallMinVolume_103() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___m_fBallMinVolume_103)); }
	inline float get_m_fBallMinVolume_103() const { return ___m_fBallMinVolume_103; }
	inline float* get_address_of_m_fBallMinVolume_103() { return &___m_fBallMinVolume_103; }
	inline void set_m_fBallMinVolume_103(float value)
	{
		___m_fBallMinVolume_103 = value;
	}

	inline static int32_t get_offset_of_m_lstDelayInit_233() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___m_lstDelayInit_233)); }
	inline List_1_t3679796562 * get_m_lstDelayInit_233() const { return ___m_lstDelayInit_233; }
	inline List_1_t3679796562 ** get_address_of_m_lstDelayInit_233() { return &___m_lstDelayInit_233; }
	inline void set_m_lstDelayInit_233(List_1_t3679796562 * value)
	{
		___m_lstDelayInit_233 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstDelayInit_233), value);
	}

	inline static int32_t get_offset_of_m_lstDelay_241() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___m_lstDelay_241)); }
	inline List_1_t443754758 * get_m_lstDelay_241() const { return ___m_lstDelay_241; }
	inline List_1_t443754758 ** get_address_of_m_lstDelay_241() { return &___m_lstDelay_241; }
	inline void set_m_lstDelay_241(List_1_t443754758 * value)
	{
		___m_lstDelay_241 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstDelay_241), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T2227614074_H
#ifndef LAUNCHER_T75719789_H
#define LAUNCHER_T75719789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher
struct  Launcher_t75719789  : public PunBehaviour_t987309092
{
public:
	// System.Byte Launcher::MaxPlayersPerRoom
	uint8_t ___MaxPlayersPerRoom_3;
	// UnityEngine.GameObject Launcher::controlPanel
	GameObject_t1113636619 * ___controlPanel_4;
	// UnityEngine.GameObject Launcher::progressLabel
	GameObject_t1113636619 * ___progressLabel_5;
	// UnityEngine.UI.InputField Launcher::_inputRoomName
	InputField_t3762917431 * ____inputRoomName_6;
	// System.String Launcher::_gameVersion
	String_t* ____gameVersion_7;
	// System.Boolean Launcher::isConnecting
	bool ___isConnecting_8;
	// UnityEngine.UI.Toggle Launcher::_toggleKuoZhang
	Toggle_t2735377061 * ____toggleKuoZhang_9;
	// UnityEngine.UI.Toggle Launcher::_toggleYaQiu
	Toggle_t2735377061 * ____toggleYaQiu_10;
	// UnityEngine.UI.Toggle Launcher::_toggleXiQiu
	Toggle_t2735377061 * ____toggleXiQiu_11;

public:
	inline static int32_t get_offset_of_MaxPlayersPerRoom_3() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ___MaxPlayersPerRoom_3)); }
	inline uint8_t get_MaxPlayersPerRoom_3() const { return ___MaxPlayersPerRoom_3; }
	inline uint8_t* get_address_of_MaxPlayersPerRoom_3() { return &___MaxPlayersPerRoom_3; }
	inline void set_MaxPlayersPerRoom_3(uint8_t value)
	{
		___MaxPlayersPerRoom_3 = value;
	}

	inline static int32_t get_offset_of_controlPanel_4() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ___controlPanel_4)); }
	inline GameObject_t1113636619 * get_controlPanel_4() const { return ___controlPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_controlPanel_4() { return &___controlPanel_4; }
	inline void set_controlPanel_4(GameObject_t1113636619 * value)
	{
		___controlPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___controlPanel_4), value);
	}

	inline static int32_t get_offset_of_progressLabel_5() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ___progressLabel_5)); }
	inline GameObject_t1113636619 * get_progressLabel_5() const { return ___progressLabel_5; }
	inline GameObject_t1113636619 ** get_address_of_progressLabel_5() { return &___progressLabel_5; }
	inline void set_progressLabel_5(GameObject_t1113636619 * value)
	{
		___progressLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___progressLabel_5), value);
	}

	inline static int32_t get_offset_of__inputRoomName_6() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ____inputRoomName_6)); }
	inline InputField_t3762917431 * get__inputRoomName_6() const { return ____inputRoomName_6; }
	inline InputField_t3762917431 ** get_address_of__inputRoomName_6() { return &____inputRoomName_6; }
	inline void set__inputRoomName_6(InputField_t3762917431 * value)
	{
		____inputRoomName_6 = value;
		Il2CppCodeGenWriteBarrier((&____inputRoomName_6), value);
	}

	inline static int32_t get_offset_of__gameVersion_7() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ____gameVersion_7)); }
	inline String_t* get__gameVersion_7() const { return ____gameVersion_7; }
	inline String_t** get_address_of__gameVersion_7() { return &____gameVersion_7; }
	inline void set__gameVersion_7(String_t* value)
	{
		____gameVersion_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameVersion_7), value);
	}

	inline static int32_t get_offset_of_isConnecting_8() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ___isConnecting_8)); }
	inline bool get_isConnecting_8() const { return ___isConnecting_8; }
	inline bool* get_address_of_isConnecting_8() { return &___isConnecting_8; }
	inline void set_isConnecting_8(bool value)
	{
		___isConnecting_8 = value;
	}

	inline static int32_t get_offset_of__toggleKuoZhang_9() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ____toggleKuoZhang_9)); }
	inline Toggle_t2735377061 * get__toggleKuoZhang_9() const { return ____toggleKuoZhang_9; }
	inline Toggle_t2735377061 ** get_address_of__toggleKuoZhang_9() { return &____toggleKuoZhang_9; }
	inline void set__toggleKuoZhang_9(Toggle_t2735377061 * value)
	{
		____toggleKuoZhang_9 = value;
		Il2CppCodeGenWriteBarrier((&____toggleKuoZhang_9), value);
	}

	inline static int32_t get_offset_of__toggleYaQiu_10() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ____toggleYaQiu_10)); }
	inline Toggle_t2735377061 * get__toggleYaQiu_10() const { return ____toggleYaQiu_10; }
	inline Toggle_t2735377061 ** get_address_of__toggleYaQiu_10() { return &____toggleYaQiu_10; }
	inline void set__toggleYaQiu_10(Toggle_t2735377061 * value)
	{
		____toggleYaQiu_10 = value;
		Il2CppCodeGenWriteBarrier((&____toggleYaQiu_10), value);
	}

	inline static int32_t get_offset_of__toggleXiQiu_11() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ____toggleXiQiu_11)); }
	inline Toggle_t2735377061 * get__toggleXiQiu_11() const { return ____toggleXiQiu_11; }
	inline Toggle_t2735377061 ** get_address_of__toggleXiQiu_11() { return &____toggleXiQiu_11; }
	inline void set__toggleXiQiu_11(Toggle_t2735377061 * value)
	{
		____toggleXiQiu_11 = value;
		Il2CppCodeGenWriteBarrier((&____toggleXiQiu_11), value);
	}
};

struct Launcher_t75719789_StaticFields
{
public:
	// Launcher/eGameMode Launcher::m_eGameMode
	int32_t ___m_eGameMode_12;
	// Launcher/eSceneMode Launcher::m_eSceneMode
	int32_t ___m_eSceneMode_13;

public:
	inline static int32_t get_offset_of_m_eGameMode_12() { return static_cast<int32_t>(offsetof(Launcher_t75719789_StaticFields, ___m_eGameMode_12)); }
	inline int32_t get_m_eGameMode_12() const { return ___m_eGameMode_12; }
	inline int32_t* get_address_of_m_eGameMode_12() { return &___m_eGameMode_12; }
	inline void set_m_eGameMode_12(int32_t value)
	{
		___m_eGameMode_12 = value;
	}

	inline static int32_t get_offset_of_m_eSceneMode_13() { return static_cast<int32_t>(offsetof(Launcher_t75719789_StaticFields, ___m_eSceneMode_13)); }
	inline int32_t get_m_eSceneMode_13() const { return ___m_eSceneMode_13; }
	inline int32_t* get_address_of_m_eSceneMode_13() { return &___m_eSceneMode_13; }
	inline void set_m_eSceneMode_13(int32_t value)
	{
		___m_eSceneMode_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHER_T75719789_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (OnTouchStartHandler_t1924473215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (OnTouchUPHandler_t3432258596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (OnDownUpHandler_t3788923810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (OnDownDownHandler_t266120898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (OnDownLeftHandler_t3608179835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (OnDownRightHandler_t491698460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (OnPressUpHandler_t847273595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (OnPressDownHandler_t249715675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (OnPressLeftHandler_t1832992367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (OnPressRightHandler_t2352804955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (ETCInput_t19426123), -1, sizeof(ETCInput_t19426123_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2910[5] = 
{
	ETCInput_t19426123_StaticFields::get_offset_of__instance_2(),
	ETCInput_t19426123::get_offset_of_axes_3(),
	ETCInput_t19426123::get_offset_of_controls_4(),
	ETCInput_t19426123_StaticFields::get_offset_of_control_5(),
	ETCInput_t19426123_StaticFields::get_offset_of_axis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (ETCJoystick_t3250784002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[38] = 
{
	ETCJoystick_t3250784002::get_offset_of_onMoveStart_48(),
	ETCJoystick_t3250784002::get_offset_of_onMove_49(),
	ETCJoystick_t3250784002::get_offset_of_onMoveSpeed_50(),
	ETCJoystick_t3250784002::get_offset_of_onMoveEnd_51(),
	ETCJoystick_t3250784002::get_offset_of_onTouchStart_52(),
	ETCJoystick_t3250784002::get_offset_of_onTouchUp_53(),
	ETCJoystick_t3250784002::get_offset_of_OnDownUp_54(),
	ETCJoystick_t3250784002::get_offset_of_OnDownDown_55(),
	ETCJoystick_t3250784002::get_offset_of_OnDownLeft_56(),
	ETCJoystick_t3250784002::get_offset_of_OnDownRight_57(),
	ETCJoystick_t3250784002::get_offset_of_OnPressUp_58(),
	ETCJoystick_t3250784002::get_offset_of_OnPressDown_59(),
	ETCJoystick_t3250784002::get_offset_of_OnPressLeft_60(),
	ETCJoystick_t3250784002::get_offset_of_OnPressRight_61(),
	ETCJoystick_t3250784002::get_offset_of_joystickType_62(),
	ETCJoystick_t3250784002::get_offset_of_allowJoystickOverTouchPad_63(),
	ETCJoystick_t3250784002::get_offset_of_radiusBase_64(),
	ETCJoystick_t3250784002::get_offset_of_radiusBaseValue_65(),
	ETCJoystick_t3250784002::get_offset_of_axisX_66(),
	ETCJoystick_t3250784002::get_offset_of_axisY_67(),
	ETCJoystick_t3250784002::get_offset_of_thumb_68(),
	ETCJoystick_t3250784002::get_offset_of_joystickArea_69(),
	ETCJoystick_t3250784002::get_offset_of_userArea_70(),
	ETCJoystick_t3250784002::get_offset_of_isTurnAndMove_71(),
	ETCJoystick_t3250784002::get_offset_of_tmSpeed_72(),
	ETCJoystick_t3250784002::get_offset_of_tmAdditionnalRotation_73(),
	ETCJoystick_t3250784002::get_offset_of_tmMoveCurve_74(),
	ETCJoystick_t3250784002::get_offset_of_tmLockInJump_75(),
	ETCJoystick_t3250784002::get_offset_of_tmLastMove_76(),
	ETCJoystick_t3250784002::get_offset_of_thumbPosition_77(),
	ETCJoystick_t3250784002::get_offset_of_isDynamicActif_78(),
	ETCJoystick_t3250784002::get_offset_of_tmpAxis_79(),
	ETCJoystick_t3250784002::get_offset_of_OldTmpAxis_80(),
	ETCJoystick_t3250784002::get_offset_of_isOnTouch_81(),
	ETCJoystick_t3250784002::get_offset_of_isNoReturnThumb_82(),
	ETCJoystick_t3250784002::get_offset_of_noReturnPosition_83(),
	ETCJoystick_t3250784002::get_offset_of_noReturnOffset_84(),
	ETCJoystick_t3250784002::get_offset_of_isNoOffsetThumb_85(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (OnMoveStartHandler_t2779627305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (OnMoveSpeedHandler_t961423656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (OnMoveHandler_t1792703951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (OnMoveEndHandler_t3637094994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (OnTouchStartHandler_t3335545810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (OnTouchUpHandler_t4157265644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (OnDownUpHandler_t3611259663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (OnDownDownHandler_t2311908456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (OnDownLeftHandler_t2607413051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (OnDownRightHandler_t881666752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (OnPressUpHandler_t2844311173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (OnPressDownHandler_t3635540872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (OnPressLeftHandler_t137900968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (OnPressRightHandler_t1812712090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (JoystickArea_t453609051)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2926[11] = 
{
	JoystickArea_t453609051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (JoystickType_t2334749223)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2927[3] = 
{
	JoystickType_t2334749223::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (RadiusBase_t1799921463)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2928[4] = 
{
	RadiusBase_t1799921463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (ETCTouchPad_t480692142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[24] = 
{
	ETCTouchPad_t480692142::get_offset_of_onMoveStart_48(),
	ETCTouchPad_t480692142::get_offset_of_onMove_49(),
	ETCTouchPad_t480692142::get_offset_of_onMoveSpeed_50(),
	ETCTouchPad_t480692142::get_offset_of_onMoveEnd_51(),
	ETCTouchPad_t480692142::get_offset_of_onTouchStart_52(),
	ETCTouchPad_t480692142::get_offset_of_onTouchUp_53(),
	ETCTouchPad_t480692142::get_offset_of_OnDownUp_54(),
	ETCTouchPad_t480692142::get_offset_of_OnDownDown_55(),
	ETCTouchPad_t480692142::get_offset_of_OnDownLeft_56(),
	ETCTouchPad_t480692142::get_offset_of_OnDownRight_57(),
	ETCTouchPad_t480692142::get_offset_of_OnPressUp_58(),
	ETCTouchPad_t480692142::get_offset_of_OnPressDown_59(),
	ETCTouchPad_t480692142::get_offset_of_OnPressLeft_60(),
	ETCTouchPad_t480692142::get_offset_of_OnPressRight_61(),
	ETCTouchPad_t480692142::get_offset_of_axisX_62(),
	ETCTouchPad_t480692142::get_offset_of_axisY_63(),
	ETCTouchPad_t480692142::get_offset_of_isDPI_64(),
	ETCTouchPad_t480692142::get_offset_of_cachedImage_65(),
	ETCTouchPad_t480692142::get_offset_of_tmpAxis_66(),
	ETCTouchPad_t480692142::get_offset_of_OldTmpAxis_67(),
	ETCTouchPad_t480692142::get_offset_of_previousDargObject_68(),
	ETCTouchPad_t480692142::get_offset_of_isOut_69(),
	ETCTouchPad_t480692142::get_offset_of_isOnTouch_70(),
	ETCTouchPad_t480692142::get_offset_of_cachedVisible_71(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (OnMoveStartHandler_t2512529097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (OnMoveHandler_t3659216741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (OnMoveSpeedHandler_t3603010941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (OnMoveEndHandler_t330401481), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (OnTouchStartHandler_t3223438894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (OnTouchUPHandler_t2186573936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (OnDownUpHandler_t3325091394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (OnDownDownHandler_t3809127321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (OnDownLeftHandler_t2754992532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (OnDownRightHandler_t1395399745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (OnPressUpHandler_t3688972238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (OnPressDownHandler_t3681978565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (OnPressLeftHandler_t2603277495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (OnPressRightHandler_t1078071762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (IOManager_t475421879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (Launcher_t75719789), -1, sizeof(Launcher_t75719789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2945[11] = 
{
	Launcher_t75719789::get_offset_of_MaxPlayersPerRoom_3(),
	Launcher_t75719789::get_offset_of_controlPanel_4(),
	Launcher_t75719789::get_offset_of_progressLabel_5(),
	Launcher_t75719789::get_offset_of__inputRoomName_6(),
	Launcher_t75719789::get_offset_of__gameVersion_7(),
	Launcher_t75719789::get_offset_of_isConnecting_8(),
	Launcher_t75719789::get_offset_of__toggleKuoZhang_9(),
	Launcher_t75719789::get_offset_of__toggleYaQiu_10(),
	Launcher_t75719789::get_offset_of__toggleXiQiu_11(),
	Launcher_t75719789_StaticFields::get_offset_of_m_eGameMode_12(),
	Launcher_t75719789_StaticFields::get_offset_of_m_eSceneMode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (eGameMode_t104648515)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2946[4] = 
{
	eGameMode_t104648515::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (eSceneMode_t4177333499)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2947[3] = 
{
	eSceneMode_t4177333499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (ListItem_t4166427101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[5] = 
{
	ListItem_t4166427101::get_offset_of__txtContent_2(),
	ListItem_t4166427101::get_offset_of__btnSelect_3(),
	ListItem_t4166427101::get_offset_of_m_szContent_4(),
	ListItem_t4166427101::get_offset_of_m_nIndex_5(),
	ListItem_t4166427101::get_offset_of__MyList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (Main_t2227614074), -1, sizeof(Main_t2227614074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2949[319] = 
{
	Main_t2227614074::get_offset_of_m_fEjectHuanTing_3(),
	Main_t2227614074::get_offset_of_m_fA_4(),
	Main_t2227614074::get_offset_of_m_fB_5(),
	Main_t2227614074::get_offset_of_m_fX_6(),
	0,
	0,
	Main_t2227614074::get_offset_of_m_fPlayerNameBaseSize_9(),
	Main_t2227614074::get_offset_of_m_fPlayerNameMaxThreshold_10(),
	Main_t2227614074::get_offset_of_m_fSporeMaxSize_11(),
	Main_t2227614074::get_offset_of_m_fGameOverPanelTime_12(),
	Main_t2227614074::get_offset_of_m_nPlatform_13(),
	Main_t2227614074::get_offset_of_m_fDistanceToSlowDown_14(),
	Main_t2227614074::get_offset_of_m_fBiggestBallAsCenterThreshold_15(),
	Main_t2227614074::get_offset_of_m_fSizeChangeSpeed_16(),
	Main_t2227614074::get_offset_of_m_fSizeChangeSpeed_UnfoldSkill_17(),
	Main_t2227614074::get_offset_of_m_aryUnfoldArcStartAngle_18(),
	Main_t2227614074::get_offset_of__goGameOver_19(),
	Main_t2227614074::get_offset_of__goBallsCenter_20(),
	Main_t2227614074::get_offset_of_m_fCamRaiseAccelerate_21(),
	Main_t2227614074::get_offset_of_m_fCamDownAccelerate_22(),
	Main_t2227614074::get_offset_of_m_fCamChangeDelaySpit_23(),
	Main_t2227614074::get_offset_of_m_fCamChangeDelayExplode_24(),
	Main_t2227614074::get_offset_of_m_fRaiseSpeed_25(),
	Main_t2227614074::get_offset_of_m_fDownSpeed_26(),
	Main_t2227614074::get_offset_of_m_fCamRespondSpeed_27(),
	Main_t2227614074::get_offset_of_m_fTimeBeforeCamDown_28(),
	Main_t2227614074::get_offset_of_m_fCamMinSize_29(),
	Main_t2227614074::get_offset_of_m_fCamChangeSpeed_30(),
	Main_t2227614074::get_offset_of_m_fCamGuiWeiSpeed_31(),
	Main_t2227614074::get_offset_of_m_fShangFuXiShu_32(),
	Main_t2227614074::get_offset_of_m_fYiJianHeQiuSpeed_33(),
	Main_t2227614074::get_offset_of_m_fCamSizeXiShu_34(),
	Main_t2227614074::get_offset_of__goCheat_35(),
	Main_t2227614074::get_offset_of_m_goJueShengQiu_36(),
	Main_t2227614074::get_offset_of_m_camWarFog_37(),
	Main_t2227614074::get_offset_of_m_camForCalc_38(),
	Main_t2227614074::get_offset_of_g_SystemMsg_39(),
	Main_t2227614074::get_offset_of_m_aryBallSprite_40(),
	Main_t2227614074_StaticFields::get_offset_of_s_Instance_41(),
	Main_t2227614074::get_offset_of_m_goHongBao_42(),
	Main_t2227614074::get_offset_of__playeraction_43(),
	Main_t2227614074::get_offset_of_m_ShengFuPanDingMonster_44(),
	Main_t2227614074::get_offset_of_m_preGridLine_45(),
	Main_t2227614074::get_offset_of_m_preBallLine_46(),
	Main_t2227614074::get_offset_of_m_preBall_47(),
	Main_t2227614074::get_offset_of_m_preBallTest_48(),
	Main_t2227614074::get_offset_of_m_preElectronicBall_49(),
	Main_t2227614074::get_offset_of_m_prePlayer_50(),
	Main_t2227614074::get_offset_of_m_preThorn_51(),
	Main_t2227614074::get_offset_of_m_preRhythm_52(),
	Main_t2227614074::get_offset_of_m_preBean_53(),
	Main_t2227614074::get_offset_of_m_preEffectPreExplode_54(),
	Main_t2227614074::get_offset_of_m_preEffectExplode_55(),
	Main_t2227614074::get_offset_of_m_preBlackHole_56(),
	Main_t2227614074::get_offset_of_m_preSpitBallTarget_57(),
	Main_t2227614074::get_offset_of_m_preSpore_58(),
	Main_t2227614074::get_offset_of_m_preNail_59(),
	Main_t2227614074::get_offset_of_m_preVoice_60(),
	Main_t2227614074::get_offset_of_m_preGroup_61(),
	Main_t2227614074::get_offset_of_m_preOneWayObstacle_62(),
	Main_t2227614074::get_offset_of_m_preExplodeNode_63(),
	Main_t2227614074::get_offset_of__panelRank_64(),
	Main_t2227614074::get_offset_of__panelSkillPoint_65(),
	Main_t2227614074::get_offset_of__msglistKillPeople_66(),
	Main_t2227614074::get_offset_of__txtTimeLeft_67(),
	Main_t2227614074::get_offset_of__txtTimeLeft_MOBILE_68(),
	Main_t2227614074::get_offset_of__txtTimeLeft_PC_69(),
	Main_t2227614074::get_offset_of__uiDead_70(),
	Main_t2227614074::get_offset_of__uiDeadNew_71(),
	Main_t2227614074::get_offset_of__uiGameOver_72(),
	Main_t2227614074::get_offset_of_m_txtDebugInfo_73(),
	Main_t2227614074::get_offset_of_m_sliderStickSize_74(),
	Main_t2227614074::get_offset_of_m_sliderAccelerate_75(),
	Main_t2227614074::get_offset_of_m_btnSpit_76(),
	Main_t2227614074::get_offset_of_m_sbSpitSpore_77(),
	Main_t2227614074::get_offset_of_m_sbSpitBall_78(),
	Main_t2227614074::get_offset_of_m_sbUnfold_79(),
	Main_t2227614074::get_offset_of_m_uiGamePanel_80(),
	Main_t2227614074::get_offset_of_m_uiMapEditorPanel_81(),
	Main_t2227614074::get_offset_of_m_imgSpitBall_Fill_82(),
	Main_t2227614074::get_offset_of_m_txtColdDown_W_83(),
	Main_t2227614074::get_offset_of_m_txtColdDown_E_84(),
	Main_t2227614074::get_offset_of_m_txtColdDown_R_85(),
	Main_t2227614074::get_offset_of_m_txtColdDown_W_Mobile_86(),
	Main_t2227614074::get_offset_of_m_txtColdDown_E_Mobile_87(),
	Main_t2227614074::get_offset_of_m_txtColdDown_R_Mobile_88(),
	Main_t2227614074::get_offset_of_m_imgSpitSpore_Fill_89(),
	Main_t2227614074::get_offset_of_m_imgUnfold_Fill_90(),
	Main_t2227614074::get_offset_of_m_imgOneBtnSplit_Fill_91(),
	Main_t2227614074::get_offset_of_m_imgSelectedSkill_Fill_92(),
	Main_t2227614074::get_offset_of_m_SelectedSkillColdDownTimeCount_93(),
	Main_t2227614074::get_offset_of_m_SelectedSkillColdDownInterval_94(),
	Main_t2227614074::get_offset_of_m_imgLababa_Fill_95(),
	Main_t2227614074::get_offset_of_m_fBallTriggerBasrRadius_96(),
	Main_t2227614074::get_offset_of_m_fPixel2Scale_97(),
	Main_t2227614074::get_offset_of_m_fScale2Radius_98(),
	Main_t2227614074::get_offset_of_m_fRadius2Scale_99(),
	Main_t2227614074::get_offset_of_m_nShit_100(),
	Main_t2227614074_StaticFields::get_offset_of_BALL_MIN_SIZE_101(),
	0,
	Main_t2227614074_StaticFields::get_offset_of_m_fBallMinVolume_103(),
	Main_t2227614074::get_offset_of_m_fBallBaseSpeed_104(),
	Main_t2227614074::get_offset_of_m_fBallSpeedRadiusKaiFangFenMu_105(),
	Main_t2227614074::get_offset_of_m_fBallMoveToCenterBaseSpeed_106(),
	Main_t2227614074::get_offset_of_m_nRebornPosIdx_107(),
	Main_t2227614074::get_offset_of_m_fBaseVolumeyByItemDecreasedPercentWhenDead_108(),
	Main_t2227614074::get_offset_of_m_fMaxAudioVolume_109(),
	Main_t2227614074::get_offset_of_m_fMinAudioVolume_110(),
	Main_t2227614074::get_offset_of_m_fMaxPlayerSpeed_111(),
	Main_t2227614074::get_offset_of_m_fShengFuPanDingSize_112(),
	Main_t2227614074::get_offset_of_m_fClassCircleThreshold0_113(),
	Main_t2227614074::get_offset_of_m_fClassCircleThreshold1_114(),
	Main_t2227614074::get_offset_of_m_fClassCircleLength0_115(),
	Main_t2227614074::get_offset_of_m_fClassCircleLength1_116(),
	Main_t2227614074::get_offset_of_m_vecClassCircleCenterPos_117(),
	Main_t2227614074::get_offset_of_m_fLaBabaRunTime_118(),
	Main_t2227614074::get_offset_of_m_fLaBabaRunDistance_119(),
	Main_t2227614074::get_offset_of_m_fLababaColdDown_120(),
	Main_t2227614074::get_offset_of_m_fLababaLiveTime_121(),
	Main_t2227614074::get_offset_of_m_fLababaSizeCostPercent_122(),
	Main_t2227614074::get_offset_of_m_fLababaAreaThreshold_123(),
	Main_t2227614074::get_offset_of_m_fTimeOfOneGame_124(),
	Main_t2227614074::get_offset_of_m_fAdjustMoveProtectInterval_125(),
	Main_t2227614074::get_offset_of_m_fMaxMoveDistancePerFrame_126(),
	Main_t2227614074::get_offset_of_m_nObservor_127(),
	Main_t2227614074::get_offset_of_m_fShellAdjustShit_128(),
	Main_t2227614074::get_offset_of_m_fBaseSpeedInGroupLocal_129(),
	Main_t2227614074::get_offset_of_m_fSyncMoveCountInterval_130(),
	Main_t2227614074::get_offset_of_m_fAdjustPosDelayThreshold_131(),
	Main_t2227614074::get_offset_of_m_nCheatMode_132(),
	Main_t2227614074::get_offset_of_m_fBornProtectTime_133(),
	Main_t2227614074::get_offset_of_m_fMinRunTime_134(),
	Main_t2227614074::get_offset_of_m_fGridLineGap_135(),
	Main_t2227614074::get_offset_of_m_fTimeBeforeUiDead_136(),
	Main_t2227614074::get_offset_of_m_fBeanNumPerRange_137(),
	Main_t2227614074::get_offset_of_m_fThornNumPerRange_138(),
	Main_t2227614074::get_offset_of_m_fBeanSize_139(),
	Main_t2227614074::get_offset_of_m_fThornSize_140(),
	Main_t2227614074::get_offset_of_m_fShellShrinkTotalTime_141(),
	Main_t2227614074::get_offset_of_m_fExplodeShellShrinkTotalTime_142(),
	Main_t2227614074::get_offset_of_m_fSplitShellShrinkTotalTime_143(),
	Main_t2227614074::get_offset_of_m_fThornContributeToBallSize_144(),
	Main_t2227614074::get_offset_of_m_fAutoAttenuate_145(),
	Main_t2227614074::get_offset_of_m_fAutoAttenuateTimeInterval_146(),
	Main_t2227614074::get_offset_of_m_fThornAttenuate_147(),
	Main_t2227614074::get_offset_of_m_fArenaWidth_148(),
	Main_t2227614074::get_offset_of_m_fArenaHeight_149(),
	Main_t2227614074::get_offset_of_m_fGenerateNewBeanTimeInterval_150(),
	Main_t2227614074::get_offset_of_m_fGenerateNewThornTimeInterval_151(),
	Main_t2227614074::get_offset_of_m_fSpitSporeTimeInterval_152(),
	Main_t2227614074::get_offset_of_m_fSpitBallTimeInterval_153(),
	Main_t2227614074::get_offset_of_m_fUnfoldCostMP_154(),
	Main_t2227614074::get_offset_of_m_fMainTriggerUnfoldTime_155(),
	Main_t2227614074::get_offset_of_m_fMainTriggerPreUnfoldTime_156(),
	Main_t2227614074::get_offset_of_m_fUnfoldTimeInterval_157(),
	Main_t2227614074::get_offset_of_m_fUnfoldSale_158(),
	Main_t2227614074::get_offset_of_m_fUnfoldCircleSale_159(),
	Main_t2227614074::get_offset_of_m_fForceSpitTotalTime_160(),
	Main_t2227614074::get_offset_of_m_fSpitRunTime_161(),
	Main_t2227614074::get_offset_of_m_fSpitBallRunTime_162(),
	Main_t2227614074::get_offset_of_m_szSpitSporeBeanType_163(),
	Main_t2227614074::get_offset_of_m_fSpitSporeCostMP_164(),
	Main_t2227614074::get_offset_of_m_fSpitSporeRunDistance_165(),
	Main_t2227614074::get_offset_of_m_fSpitSporeInitSpeed_166(),
	Main_t2227614074::get_offset_of_m_fSpitSporeAccelerate_167(),
	Main_t2227614074::get_offset_of_m_fSpittedObjrunRunTime_168(),
	Main_t2227614074::get_offset_of_m_fSpittedObjrunRunDistance_169(),
	Main_t2227614074::get_offset_of_m_fSpitBallHalfRunTime_170(),
	Main_t2227614074::get_offset_of_m_fSpitBallHalfRunDistance_171(),
	Main_t2227614074::get_offset_of_m_fSpitBallHalfCostMP_172(),
	Main_t2227614074::get_offset_of_m_fSpitBallHalfColddown_173(),
	Main_t2227614074::get_offset_of_m_fSpitBallHalfQianYao_174(),
	Main_t2227614074::get_offset_of_m_fSpitBallRunSpeed_175(),
	Main_t2227614074::get_offset_of_m_fSpitBallRunDistanceMultiple_176(),
	Main_t2227614074::get_offset_of_m_fSpitBallCostMP_177(),
	Main_t2227614074::get_offset_of_m_fSpitBallStayTime_178(),
	Main_t2227614074::get_offset_of_m_fBornMinTiJi_179(),
	Main_t2227614074::get_offset_of_m_fMinDiameterToEatThorn_180(),
	Main_t2227614074::get_offset_of_m_fExplodeRunDistanceMultiple_181(),
	Main_t2227614074::get_offset_of_m_fExplodeStayTime_182(),
	Main_t2227614074::get_offset_of_m_fExplodePercent_183(),
	Main_t2227614074::get_offset_of_m_fExpectExplodeNum_184(),
	Main_t2227614074::get_offset_of_m_fExplodeRunTime_185(),
	Main_t2227614074::get_offset_of_m_nExplodeMinNum_186(),
	Main_t2227614074::get_offset_of_m_fMaxBallNumPerPlayer_187(),
	Main_t2227614074::get_offset_of_m_fSprayBeanMaxInitSpeed_188(),
	Main_t2227614074::get_offset_of_m_fSprayBeanMinInitSpeed_189(),
	Main_t2227614074::get_offset_of_m_fSprayBeanAccelerate_190(),
	Main_t2227614074::get_offset_of_m_fSprayBeanLiveTime_191(),
	Main_t2227614074::get_offset_of_m_fCrucifix_192(),
	Main_t2227614074::get_offset_of_m_nSplitNum_193(),
	Main_t2227614074::get_offset_of_m_fSplitCostMP_194(),
	Main_t2227614074::get_offset_of_m_fSplitStayTime_195(),
	Main_t2227614074::get_offset_of_m_fSplitRunTime_196(),
	Main_t2227614074::get_offset_of_m_fSplitMaxDistance_197(),
	Main_t2227614074::get_offset_of_m_fSplitTotalTime_198(),
	Main_t2227614074::get_offset_of_m_fSplitTimeInterval_199(),
	Main_t2227614074::get_offset_of_m_fTriggerRadius_200(),
	Main_t2227614074::get_offset_of_the_spray_201(),
	Main_t2227614074::get_offset_of_m_goSprays_202(),
	Main_t2227614074::get_offset_of_m_fSpitNailTimeInterval_203(),
	Main_t2227614074::get_offset_of_m_fNailLiveTime_204(),
	Main_t2227614074::get_offset_of_m_lineWorldBorderTop_205(),
	Main_t2227614074::get_offset_of_m_lineWorldBorderBottom_206(),
	Main_t2227614074::get_offset_of_m_lineWorldBorderLeft_207(),
	Main_t2227614074::get_offset_of_m_lineWorldBorderRight_208(),
	Main_t2227614074::get_offset_of_m_goBorderLeft_209(),
	Main_t2227614074::get_offset_of_m_goBorderRight_210(),
	Main_t2227614074::get_offset_of_m_goBorderTop_211(),
	Main_t2227614074::get_offset_of_m_goBorderBottom_212(),
	Main_t2227614074::get_offset_of_vecTempPos_213(),
	Main_t2227614074::get_offset_of_vecTempPos1_214(),
	Main_t2227614074::get_offset_of_m_goThornsSpit_215(),
	Main_t2227614074::get_offset_of_m_goThorns_216(),
	Main_t2227614074::get_offset_of_m_goSpores_217(),
	Main_t2227614074::get_offset_of_m_goBeans_218(),
	Main_t2227614074::get_offset_of_m_goSpitBallTargets_219(),
	Main_t2227614074::get_offset_of_m_goNails_220(),
	Main_t2227614074::get_offset_of_m_goBallsBeNailed_221(),
	Main_t2227614074::get_offset_of_m_goDestroyedBalls_222(),
	Main_t2227614074::get_offset_of_m_goBalls_223(),
	Main_t2227614074::get_offset_of_m_goJoyStick_224(),
	Main_t2227614074::get_offset_of_s_nBallTotalNum_225(),
	0,
	Main_t2227614074::get_offset_of_m_MainPlayer_227(),
	Main_t2227614074::get_offset_of_m_goMainPlayer_228(),
	Main_t2227614074::get_offset_of_m_photonViewMainPlayer_229(),
	Main_t2227614074::get_offset_of_m_textDebugInfo_230(),
	Main_t2227614074::get_offset_of_s_Params_231(),
	Main_t2227614074::get_offset_of_m_bInitingAllBalls_232(),
	Main_t2227614074_StaticFields::get_offset_of_m_lstDelayInit_233(),
	Main_t2227614074::get_offset_of_m_dicGamePlayDataConfig_234(),
	Main_t2227614074::get_offset_of_Array_235(),
	Main_t2227614074::get_offset_of_woca_236(),
	Main_t2227614074::get_offset_of_m_fDeadProgressTimeCount_237(),
	Main_t2227614074::get_offset_of_lstAssistAvatar_238(),
	Main_t2227614074::get_offset_of_m_fDeadUICount_239(),
	Main_t2227614074::get_offset_of_m_nDeadStatus_240(),
	Main_t2227614074_StaticFields::get_offset_of_m_lstDelay_241(),
	Main_t2227614074::get_offset_of_m_bCtrlDown_242(),
	Main_t2227614074::get_offset_of_m_bShowRank_243(),
	Main_t2227614074::get_offset_of_m_bKeyDown_R_244(),
	Main_t2227614074::get_offset_of_m_bKeyDown_E_245(),
	Main_t2227614074::get_offset_of_m_fSplitTimeCount_246(),
	Main_t2227614074::get_offset_of_m_fLababTimeCount_247(),
	Main_t2227614074::get_offset_of_m_fArenaRange_248(),
	Main_t2227614074::get_offset_of_m_nBeanTotalNumDueToRange_249(),
	Main_t2227614074::get_offset_of_m_nCurBeanTotalNum_250(),
	Main_t2227614074::get_offset_of_m_nCurGeneraedByMeBeanTotalNum_251(),
	Main_t2227614074::get_offset_of_m_nCurGeneraedByMeThornTotalNum_252(),
	Main_t2227614074::get_offset_of_m_nThornTotalNumDueToRange_253(),
	Main_t2227614074::get_offset_of_m_nCurThornTotalNum_254(),
	Main_t2227614074::get_offset_of_m_fGenerateBeanTime_255(),
	Main_t2227614074::get_offset_of_m_fGenerateThornTime_256(),
	0,
	Main_t2227614074::get_offset_of_m_fWorldLeft_258(),
	Main_t2227614074::get_offset_of_m_fWorldRight_259(),
	Main_t2227614074::get_offset_of_m_fWorldBottom_260(),
	Main_t2227614074::get_offset_of_m_fWorldTop_261(),
	Main_t2227614074::get_offset_of_vecScreenMin_262(),
	Main_t2227614074::get_offset_of_vecScreenMax_263(),
	Main_t2227614074::get_offset_of_vecWorldMin_264(),
	Main_t2227614074::get_offset_of_vecWorldMax_265(),
	Main_t2227614074::get_offset_of_m_dicAllThorns_266(),
	Main_t2227614074::get_offset_of_m_fForceSpitPercent_267(),
	Main_t2227614074::get_offset_of_m_bForceSpit_268(),
	Main_t2227614074::get_offset_of_m_fSpitBallCurColdDownTime_269(),
	Main_t2227614074::get_offset_of_m_fCurCamSize_270(),
	Main_t2227614074::get_offset_of_m_fCamSizeProtectTime_271(),
	Main_t2227614074::get_offset_of_m_fForceSpitCurTime_272(),
	Main_t2227614074::get_offset_of_m_fCurColdDownTime_BecomeThor_Count_273(),
	Main_t2227614074::get_offset_of_m_fCurColdDownTime_BecomeThorn_274(),
	Main_t2227614074::get_offset_of_m_fUnfoldCurColdDownTime_275(),
	Main_t2227614074::get_offset_of_m_fSpitSporeCurColdDownTime_276(),
	Main_t2227614074::get_offset_of_m_fSpitNailCurColdDownTime_277(),
	Main_t2227614074::get_offset_of_lstTemp_278(),
	Main_t2227614074::get_offset_of_m_bTestFucking_279(),
	Main_t2227614074::get_offset_of_m_nFuckCount_280(),
	Main_t2227614074::get_offset_of_m_fFuckTime_281(),
	Main_t2227614074::get_offset_of_m_lstPlayers_282(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_Gold_283(),
	Main_t2227614074::get_offset_of_m_fSkillColdDown_Gold_284(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_Henzy_285(),
	Main_t2227614074::get_offset_of_m_fSkillColdDown_Henzy_286(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_MergeAll_287(),
	Main_t2227614074::get_offset_of_m_fSkillColdDown_MergeAll_288(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_MagicShield_289(),
	Main_t2227614074::get_offset_of_m_fSkillColdDown_MagicShield_290(),
	Main_t2227614074::get_offset_of_m_fSkillColdDown_Annihilate_291(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_Annihilate_292(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_Sneak_293(),
	Main_t2227614074::get_offset_of_m_fSkillColdDownCount_294(),
	Main_t2227614074::get_offset_of_m_fSpitBallHalfTimeCount_295(),
	Main_t2227614074::get_offset_of_m_bPreOneBtnSplit_296(),
	Main_t2227614074::get_offset_of_m_fPreOneBtnSplitTimeCount_297(),
	Main_t2227614074::get_offset_of_m_fCurOneBtnSplitDis_298(),
	Main_t2227614074::get_offset_of_m_nSplitTouchFingerId_299(),
	Main_t2227614074::get_offset_of_m_vecSplitTouchStartPos_300(),
	Main_t2227614074::get_offset_of_m_vecCurSplitTouchPos_301(),
	Main_t2227614074::get_offset_of_m_vecSplitDirction_302(),
	Main_t2227614074::get_offset_of_m_nInitingBallsCount_303(),
	Main_t2227614074::get_offset_of_m_fInitingBallsTime_304(),
	Main_t2227614074::get_offset_of_m_vecStartPos_305(),
	Main_t2227614074::get_offset_of_m_nFingerId_306(),
	Main_t2227614074::get_offset_of_m_bSpitTouching_307(),
	Main_t2227614074::get_offset_of_m_vecCurPos_308(),
	Main_t2227614074::get_offset_of_m_vecSpitDirction_309(),
	Main_t2227614074::get_offset_of_m_bSpitTouching_Test_310(),
	Main_t2227614074::get_offset_of_m_fLeftTime_311(),
	Main_t2227614074::get_offset_of_m_fGameStartTime_312(),
	Main_t2227614074::get_offset_of_m_fTimeEclapse_313(),
	Main_t2227614074::get_offset_of_m_fProcessLeftTimeCount_314(),
	Main_t2227614074::get_offset_of_m_bGameOver_315(),
	Main_t2227614074::get_offset_of_m_bGameOverCounting_316(),
	Main_t2227614074::get_offset_of_m_fGameOverPanelTimeLapse_317(),
	Main_t2227614074::get_offset_of_m_fScreenWidth_318(),
	Main_t2227614074::get_offset_of_m_fScreenHeight_319(),
	Main_t2227614074::get_offset_of__inputCheatPanelExplodeThornId_320(),
	Main_t2227614074::get_offset_of_m_szCheatExplodeThornId_321(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (eInstantiateType_t163196549)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2950[5] = 
{
	eInstantiateType_t163196549::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (ePlatformType_t2884586383)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2951[3] = 
{
	ePlatformType_t2884586383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (U3CLoadSceneU3Ec__Iterator0_t636698714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[4] = 
{
	U3CLoadSceneU3Ec__Iterator0_t636698714::get_offset_of_scene_name_0(),
	U3CLoadSceneU3Ec__Iterator0_t636698714::get_offset_of_U24current_1(),
	U3CLoadSceneU3Ec__Iterator0_t636698714::get_offset_of_U24disposing_2(),
	U3CLoadSceneU3Ec__Iterator0_t636698714::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (MapEditor_t878680970), -1, sizeof(MapEditor_t878680970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2953[232] = 
{
	MapEditor_t878680970_StaticFields::get_offset_of_s_Instance_2(),
	MapEditor_t878680970::get_offset_of__txtDebugInfo_3(),
	MapEditor_t878680970::get_offset_of__txtDebugInfoInEditor_4(),
	MapEditor_t878680970_StaticFields::get_offset_of_m_eEatMode_5(),
	MapEditor_t878680970::get_offset_of_m_fMinCameraSize_6(),
	MapEditor_t878680970::get_offset_of_m_fMaxCameraSize_7(),
	MapEditor_t878680970::get_offset_of_m_fCameraZoomSpeed_8(),
	MapEditor_t878680970::get_offset_of_m_fBallSizeAffectCamSize_9(),
	MapEditor_t878680970::get_offset_of_m_ReferBall_10(),
	MapEditor_t878680970::get_offset_of_m_fWorldSizeX_11(),
	MapEditor_t878680970::get_offset_of_m_fWorldSizeY_12(),
	MapEditor_t878680970::get_offset_of_m_fWorldWidth_13(),
	MapEditor_t878680970::get_offset_of_m_fWorldHeight_14(),
	MapEditor_t878680970::get_offset_of_m_fHalfWorldWidth_15(),
	MapEditor_t878680970::get_offset_of_m_fHalfWorldHeight_16(),
	MapEditor_t878680970::get_offset_of_m_goWorldBorderTop_17(),
	MapEditor_t878680970::get_offset_of_m_goWorldBorderBottom_18(),
	MapEditor_t878680970::get_offset_of_m_goWorldBorderLeft_19(),
	MapEditor_t878680970::get_offset_of_m_goWorldBorderRight_20(),
	MapEditor_t878680970::get_offset_of_m_lstRebornPos_21(),
	MapEditor_t878680970::get_offset_of_vecTemp_22(),
	MapEditor_t878680970::get_offset_of_vecTempPos_23(),
	MapEditor_t878680970::get_offset_of_vecTempScale_24(),
	MapEditor_t878680970::get_offset_of_m_preGrassTile_25(),
	MapEditor_t878680970::get_offset_of_m_preGridLine_26(),
	MapEditor_t878680970::get_offset_of_m_goGridLineContainer_27(),
	MapEditor_t878680970::get_offset_of_m_goGrassContainer_28(),
	MapEditor_t878680970::get_offset_of_m_goPolygonContainer_29(),
	MapEditor_t878680970::get_offset_of_m_goBeanSprayContainer_30(),
	MapEditor_t878680970::get_offset_of_m_goRebornSpotContainer_31(),
	MapEditor_t878680970::get_offset_of_m_goColorThornContainer_32(),
	0,
	MapEditor_t878680970::get_offset_of_m_dicDelProtectTime_34(),
	MapEditor_t878680970::get_offset_of_m_dicGrass_35(),
	MapEditor_t878680970_StaticFields::get_offset_of_s_vecTempPos_36(),
	MapEditor_t878680970::get_offset_of_m_fGrassTileWidth_37(),
	MapEditor_t878680970::get_offset_of_m_fGrassTileHeight_38(),
	MapEditor_t878680970::get_offset_of_m_fGrassHalfTileWidth_39(),
	MapEditor_t878680970::get_offset_of_m_fGrassHalfTileHeight_40(),
	MapEditor_t878680970::get_offset_of_m_CurSelectedObj_41(),
	MapEditor_t878680970::get_offset_of_m_CurMovingObj_42(),
	MapEditor_t878680970::get_offset_of_m_vecLastMousePos_43(),
	MapEditor_t878680970::get_offset_of_Screen_Cursor_44(),
	MapEditor_t878680970::get_offset_of_UI_MiniMap_45(),
	MapEditor_t878680970::get_offset_of_m_arySubPanel_46(),
	MapEditor_t878680970::get_offset_of_dropdownItem_47(),
	MapEditor_t878680970::get_offset_of__panelLogin_48(),
	MapEditor_t878680970::get_offset_of__musicMain_49(),
	MapEditor_t878680970::get_offset_of__inputTest_50(),
	MapEditor_t878680970::get_offset_of__btnTest_51(),
	MapEditor_t878680970::get_offset_of__dicTest_52(),
	MapEditor_t878680970::get_offset_of__inputA_53(),
	MapEditor_t878680970::get_offset_of__inputB_54(),
	MapEditor_t878680970::get_offset_of__inputC_55(),
	MapEditor_t878680970::get_offset_of__inputX_56(),
	MapEditor_t878680970::get_offset_of__inputTimeBeforeShowDeadUI_57(),
	MapEditor_t878680970::get_offset_of__inputTimeTimeOfOneRound_58(),
	MapEditor_t878680970::get_offset_of__inputBaseVolumeDecreasedPercentWhenDead_59(),
	MapEditor_t878680970::get_offset_of__inputClassCircleRadius_60(),
	MapEditor_t878680970::get_offset_of__inputClassThreshold_61(),
	MapEditor_t878680970::get_offset_of__inputWorldSizeX_62(),
	MapEditor_t878680970::get_offset_of__inputWorldSizeY_63(),
	MapEditor_t878680970::get_offset_of__inputfieldCamSizeMax_64(),
	MapEditor_t878680970::get_offset_of__inputfieldCamSizeMin_65(),
	MapEditor_t878680970::get_offset_of__inputCamShangFuXiShu_66(),
	MapEditor_t878680970::get_offset_of__inputCamRaiseSpeed_67(),
	MapEditor_t878680970::get_offset_of__inputCamShangFuAccelerate_68(),
	MapEditor_t878680970::get_offset_of__inputCamChangeDelaySpit_69(),
	MapEditor_t878680970::get_offset_of__inputCamChangeDelayExplode_70(),
	MapEditor_t878680970::get_offset_of__inputfieldCamRespondSpeed_71(),
	MapEditor_t878680970::get_offset_of__inputCamTimeBeforeDown_72(),
	MapEditor_t878680970::get_offset_of__inputCamDownSpeed_73(),
	MapEditor_t878680970::get_offset_of__inputfieldCamSizeXiShu_74(),
	MapEditor_t878680970::get_offset_of__inputfieldCamZoomSpeed_75(),
	MapEditor_t878680970::get_offset_of__inputBornProtectTime_76(),
	MapEditor_t878680970::get_offset_of__inputfieldBallSizeAffectCamSize_77(),
	MapEditor_t878680970::get_offset_of__inputfieldBeanNumPerRange_78(),
	MapEditor_t878680970::get_offset_of__inputfieldBallBaseSpeed_79(),
	MapEditor_t878680970::get_offset_of__inputfieldMaxSpeed_80(),
	MapEditor_t878680970::get_offset_of__inputfieldSpeedRadiusKaiFangFenMu_81(),
	MapEditor_t878680970::get_offset_of__inputfieldBornMinDiameter_82(),
	MapEditor_t878680970::get_offset_of__inputfieldThornNumPerRange_83(),
	MapEditor_t878680970::get_offset_of__inputfieldGenerateNewBeanTimeInterval_84(),
	MapEditor_t878680970::get_offset_of__inputfieldGenerateNewThornTimeInterval_85(),
	MapEditor_t878680970::get_offset_of__inputfieldBeanSize_86(),
	MapEditor_t878680970::get_offset_of__inputfieldThornSize_87(),
	MapEditor_t878680970::get_offset_of__inputfieldThornContributeToBallSize_88(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitSporeCostMP_89(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitSporeThornId_90(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitSporeRunDistance_91(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitSporeBeanType_92(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitRunTime_93(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitBallRunTime_94(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitBallRunDistanceMultiple_95(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitBallCostMP_96(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitBallStayTime_97(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitBallSpeed_98(),
	MapEditor_t878680970::get_offset_of__inputfieldAutoAttenuate_99(),
	MapEditor_t878680970::get_offset_of__inputfieldThornAttenuate_100(),
	MapEditor_t878680970::get_offset_of__inputfieldAutoAttenuateTimeInterval_101(),
	MapEditor_t878680970::get_offset_of__inputfieldForceSpitTotalTime_102(),
	MapEditor_t878680970::get_offset_of__inputfieldShellShrinkTotalTime_103(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitShellTotalTime_104(),
	MapEditor_t878680970::get_offset_of__inputfieldExplodeShellTotalTime_105(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitBallTimeInterval_106(),
	MapEditor_t878680970::get_offset_of__inputfieldSpitSporeTimeInterval_107(),
	MapEditor_t878680970::get_offset_of__inputfieldMinDiameterToEatThorn_108(),
	MapEditor_t878680970::get_offset_of__inputfieldExplodeRunDistanceMultiple_109(),
	MapEditor_t878680970::get_offset_of__inputfieldExplodeStayTime_110(),
	MapEditor_t878680970::get_offset_of__inputfieldExplodePercent_111(),
	MapEditor_t878680970::get_offset_of__inputfieldExplodeExpectNum_112(),
	MapEditor_t878680970::get_offset_of__inputfieldExplodeRunTime_113(),
	MapEditor_t878680970::get_offset_of__inputfieldMaxBallNumPerPlayer_114(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitNum_115(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitRunTime_116(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitMaxDistance_117(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitTimeInterval_118(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitCostMP_119(),
	MapEditor_t878680970::get_offset_of__inputfieldSplitStayTime_120(),
	MapEditor_t878680970::get_offset_of__MonsterEditor_121(),
	MapEditor_t878680970::get_offset_of__ClassEditor_122(),
	MapEditor_t878680970::get_offset_of__dropdownEatMode_123(),
	MapEditor_t878680970::get_offset_of__inputUnfoldCostMP_124(),
	MapEditor_t878680970::get_offset_of__inputMainTriggerPreUnfoldTime_125(),
	MapEditor_t878680970::get_offset_of__inputUnfoldSale_126(),
	MapEditor_t878680970::get_offset_of__inputMainTriggerUnfoldTime_127(),
	MapEditor_t878680970::get_offset_of__inputUnfoldTimeInterval_128(),
	MapEditor_t878680970::get_offset_of__inputYaQiuTiaoJian_129(),
	MapEditor_t878680970::get_offset_of__inputYaQiuBaiFenBi_130(),
	MapEditor_t878680970::get_offset_of__inputYaQiuBaiFenBi_Self_131(),
	MapEditor_t878680970::get_offset_of__inputChiCiBaiFenBi_132(),
	MapEditor_t878680970::get_offset_of__inputXiQiuSuDu_133(),
	MapEditor_t878680970::get_offset_of__sliderScaleX_134(),
	MapEditor_t878680970::get_offset_of__sliderScaleY_135(),
	MapEditor_t878680970::get_offset_of__sliderRotation_136(),
	MapEditor_t878680970::get_offset_of__txtPolygonTitle_137(),
	MapEditor_t878680970::get_offset_of__toogleIsGrass_138(),
	MapEditor_t878680970::get_offset_of__toogleIsGrassSeed_139(),
	MapEditor_t878680970::get_offset_of__inputfieldSeedGrassLifeTime_140(),
	MapEditor_t878680970::get_offset_of__inputPolygonScaleX_141(),
	MapEditor_t878680970::get_offset_of__inputPolygonScaleY_142(),
	MapEditor_t878680970::get_offset_of__inputPolygonRotation_143(),
	MapEditor_t878680970::get_offset_of__toogleIsHardObstacle_144(),
	MapEditor_t878680970::get_offset_of__PolygonList_145(),
	MapEditor_t878680970::get_offset_of__sliderDensity_146(),
	MapEditor_t878680970::get_offset_of__sliderMaxDis_147(),
	MapEditor_t878680970::get_offset_of__sliderBeanLifeTime_148(),
	MapEditor_t878680970::get_offset_of__txtDensity_149(),
	MapEditor_t878680970::get_offset_of__txtMaxDis_150(),
	MapEditor_t878680970::get_offset_of__txtBeanLifeTime_151(),
	MapEditor_t878680970::get_offset_of__toogleShowRealtimeBeanSpray_152(),
	MapEditor_t878680970::get_offset_of__inputfieldDensity_153(),
	MapEditor_t878680970::get_offset_of__inputfieldMeatDensity_154(),
	MapEditor_t878680970::get_offset_of__inputfieldMaxDis_155(),
	MapEditor_t878680970::get_offset_of__inputfieldMinDis_156(),
	MapEditor_t878680970::get_offset_of__inputfieldBeanLifeTime_157(),
	MapEditor_t878680970::get_offset_of__inputfieldMeat2BeanSprayTime_158(),
	MapEditor_t878680970::get_offset_of__inputfieldSpraySize_159(),
	MapEditor_t878680970::get_offset_of__inputfieldDrawSpeed_160(),
	MapEditor_t878680970::get_offset_of__inputfieldDrawArea_161(),
	MapEditor_t878680970::get_offset_of__inputfieldPreDrawTime_162(),
	MapEditor_t878680970::get_offset_of__inputfieldDrawTime_163(),
	MapEditor_t878680970::get_offset_of__inputfieldDrawInterval_164(),
	MapEditor_t878680970::get_offset_of__inputfieldThornChance_165(),
	MapEditor_t878680970::get_offset_of__inputfieldSprayInterval_166(),
	MapEditor_t878680970::get_offset_of__txtThornChace_167(),
	MapEditor_t878680970::get_offset_of_m_nThornChance_168(),
	MapEditor_t878680970::get_offset_of_dropdownColorThorn_169(),
	MapEditor_t878680970::get_offset_of_m_imgColorThorn_170(),
	MapEditor_t878680970::get_offset_of_m_inputfieldColorThornThornSize_171(),
	MapEditor_t878680970::get_offset_of_m_inputfieldColorThornBallSize_172(),
	MapEditor_t878680970::get_offset_of_m_inputfieldColorThornRebornTime_173(),
	MapEditor_t878680970::get_offset_of_m_toggleAddThorn_174(),
	MapEditor_t878680970::get_offset_of_m_toggleRemoveThorn_175(),
	MapEditor_t878680970_StaticFields::get_offset_of_m_dicColorThornParam_176(),
	MapEditor_t878680970::get_offset_of_m_inputfieldLababaAreaThreshold_177(),
	MapEditor_t878680970::get_offset_of_m_inputfieldLababaRunTime_178(),
	MapEditor_t878680970::get_offset_of_m_inputfieldLababaRunDistance_179(),
	MapEditor_t878680970::get_offset_of_m_inputfieldLababaColdDown_180(),
	MapEditor_t878680970::get_offset_of_m_inputfieldLababaAreaCostPercent_181(),
	MapEditor_t878680970::get_offset_of_m_inputfieldLababaLiveTime_182(),
	MapEditor_t878680970::get_offset_of__dropdownFoodType_183(),
	MapEditor_t878680970::get_offset_of__inputName_184(),
	MapEditor_t878680970::get_offset_of__inputDesc_185(),
	MapEditor_t878680970::get_offset_of__inputValue_186(),
	MapEditor_t878680970::get_offset_of__inputNum_187(),
	MapEditor_t878680970::get_offset_of__inputTotalTime_188(),
	MapEditor_t878680970::get_offset_of__inputMinScale_189(),
	MapEditor_t878680970::get_offset_of__sliderCircleSizeRange_190(),
	MapEditor_t878680970::get_offset_of_m_CurEditingBeiShuDouZiConfig_191(),
	MapEditor_t878680970::get_offset_of_m_lstBeiShuDouZi_192(),
	MapEditor_t878680970::get_offset_of_m_fYaQiuTiaoJian_193(),
	MapEditor_t878680970::get_offset_of_m_fYaQiuBaiFenBi_194(),
	MapEditor_t878680970::get_offset_of_m_fYaQiuBaiFenBi_Self_195(),
	MapEditor_t878680970::get_offset_of_m_fChiCiBaiFenBi_196(),
	MapEditor_t878680970::get_offset_of_m_fXiQiuSuDu_197(),
	MapEditor_t878680970::get_offset_of_m_eOp_198(),
	MapEditor_t878680970::get_offset_of_m_bMapInitCompleted_199(),
	MapEditor_t878680970::get_offset_of_m_nCurSelectedColorThornIdx_200(),
	MapEditor_t878680970::get_offset_of_m_bFirstMouseLefDown_201(),
	MapEditor_t878680970::get_offset_of_m_bFirstMouseRightDown_202(),
	MapEditor_t878680970::get_offset_of_m_dicEatModeConfig_203(),
	MapEditor_t878680970::get_offset_of_m_dicCommonConfig_204(),
	MapEditor_t878680970::get_offset_of_m_dicConfigCommon2_205(),
	MapEditor_t878680970::get_offset_of_m_lstFakeRandomThornPos_206(),
	MapEditor_t878680970::get_offset_of_m_dicExplodeDir_207(),
	MapEditor_t878680970::get_offset_of_m_nGrassGUID_208(),
	MapEditor_t878680970::get_offset_of_m_lstGrass_209(),
	MapEditor_t878680970_StaticFields::get_offset_of_s_szRealRoomName_210(),
	MapEditor_t878680970_StaticFields::get_offset_of_s_xmldocMapData_211(),
	MapEditor_t878680970::get_offset_of_m_lstPolygons_212(),
	MapEditor_t878680970::get_offset_of_m_lstSprays_213(),
	MapEditor_t878680970::get_offset_of_m_bShowRealtimeBeanSpray_214(),
	MapEditor_t878680970::get_offset_of_m_fDaTaoShaSize_215(),
	MapEditor_t878680970::get_offset_of_m_fThornAttenuateSpeed_216(),
	MapEditor_t878680970::get_offset_of_m_lstRebornRandomPos_217(),
	MapEditor_t878680970::get_offset_of_m_vecRebornSpotRandomPos_218(),
	MapEditor_t878680970::get_offset_of_vecScreenLeftBottom_219(),
	MapEditor_t878680970::get_offset_of_vecScreenRightTop_220(),
	MapEditor_t878680970_StaticFields::get_offset_of_m_szCurRoom_221(),
	MapEditor_t878680970_StaticFields::get_offset_of_m_szMainPlayerName_222(),
	MapEditor_t878680970::get_offset_of_m_fWorldLeft_223(),
	MapEditor_t878680970::get_offset_of_m_fWorldRight_224(),
	MapEditor_t878680970::get_offset_of_m_fWorldTop_225(),
	MapEditor_t878680970::get_offset_of_m_fWorldBottom_226(),
	MapEditor_t878680970::get_offset_of__inputBallMinArea_227(),
	MapEditor_t878680970::get_offset_of__inputPlayerNumToStartGame_228(),
	MapEditor_t878680970_StaticFields::get_offset_of_m_nPlayerNumToStartGame_229(),
	MapEditor_t878680970::get_offset_of_m_fEjectParamA_230(),
	MapEditor_t878680970::get_offset_of_m_fEjectParamB_231(),
	MapEditor_t878680970::get_offset_of_m_fEjectParamC_232(),
	MapEditor_t878680970::get_offset_of_m_fEjectParamX_233(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (eEatMode_t3780619607)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2954[3] = 
{
	eEatMode_t3780619607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (sBeiShuDouZiConfig_t599764670)+ sizeof (RuntimeObject), sizeof(sBeiShuDouZiConfig_t599764670_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2955[5] = 
{
	sBeiShuDouZiConfig_t599764670::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeiShuDouZiConfig_t599764670::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeiShuDouZiConfig_t599764670::get_offset_of_num_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeiShuDouZiConfig_t599764670::get_offset_of_name_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeiShuDouZiConfig_t599764670::get_offset_of_desc_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (eMapEditorOp_t2959825511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2956[10] = 
{
	eMapEditorOp_t2959825511::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (sRebornRanPos_t34662718)+ sizeof (RuntimeObject), sizeof(sRebornRanPos_t34662718 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2957[2] = 
{
	sRebornRanPos_t34662718::get_offset_of_vecMinPos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sRebornRanPos_t34662718::get_offset_of_vecMaxPos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[19] = 
{
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_szPolygonName_0(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_polygon_1(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U3CszFileNameU3E__0_2(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U3CwwwU3E__0_3(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U3CrootU3E__0_4(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U3CmyXmlDocU3E__0_5(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U3CaryPointPosU3E__0_6(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_fPosX_7(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_fPosY_8(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_fScaleX_9(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_fScaleY_10(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_fRotation_11(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_bIsGrass_12(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_nHardObstacle_13(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_bIsGrassSeed_14(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U24this_15(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U24current_16(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U24disposing_17(),
	U3CLoadPolygonByXmlU3Ec__Iterator0_t910644365::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (U3CGenerateMapU3Ec__Iterator1_t2170335178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[26] = 
{
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_szFileName_0(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CwwwU3E__0_1(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CrootU3E__0_2(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CmyXmlDocU3E__0_3(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeCommonU3E__0_4(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeCommon2U3E__0_5(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeThornU3E__0_6(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeClassU3E__0_7(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeMannualMonsterU3E__0_8(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeGrowU3E__0_9(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeItemU3E__0_10(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeSkillU3E__0_11(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeSkillDescU3E__0_12(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeGestureU3E__0_13(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeEatModeU3E__0_14(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodePolygonU3E__0_15(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeGrassU3E__0_16(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeLababaU3E__0_17(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeSprayU3E__0_18(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeDustU3E__0_19(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeDeadRebornU3E__0_20(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U3CnodeControlU3E__0_21(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U24this_22(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U24current_23(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U24disposing_24(),
	U3CGenerateMapU3Ec__Iterator1_t2170335178::get_offset_of_U24PC_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[7] = 
{
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_U3CwwwFormU3E__0_0(),
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_szRoomName_1(),
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_szContent_2(),
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_U3CwwwU3E__0_3(),
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_U24current_4(),
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_U24disposing_5(),
	U3CDoSaveMapXmlFileU3Ec__Iterator2_t1738818793::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (U3CLoadSceneU3Ec__Iterator3_t4020263557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[4] = 
{
	U3CLoadSceneU3Ec__Iterator3_t4020263557::get_offset_of_scene_name_0(),
	U3CLoadSceneU3Ec__Iterator3_t4020263557::get_offset_of_U24current_1(),
	U3CLoadSceneU3Ec__Iterator3_t4020263557::get_offset_of_U24disposing_2(),
	U3CLoadSceneU3Ec__Iterator3_t4020263557::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (U3CLoadPolygonListU3Ec__Iterator4_t453975090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[8] = 
{
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U3CszFileNameU3E__0_0(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U3CrootU3E__0_1(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U3CwwwU3E__0_2(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U3CXmlDocU3E__0_3(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U24this_4(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U24current_5(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U24disposing_6(),
	U3CLoadPolygonListU3Ec__Iterator4_t453975090::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (MapObj_t1733252447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[15] = 
{
	MapObj_t1733252447::get_offset_of_m_fRotatoinZ_2(),
	MapObj_t1733252447::get_offset_of_m_fLiveTime_3(),
	MapObj_t1733252447::get_offset_of_m_bImortal_4(),
	MapObj_t1733252447::get_offset_of_vecTempPos_5(),
	MapObj_t1733252447::get_offset_of__speed_6(),
	MapObj_t1733252447::get_offset_of_m_bDestroyed_7(),
	MapObj_t1733252447::get_offset_of_m_Direction_8(),
	MapObj_t1733252447::get_offset_of_m_eObjType_9(),
	MapObj_t1733252447::get_offset_of_m_fInitSpeed_10(),
	MapObj_t1733252447::get_offset_of_m_fAccelerate_11(),
	MapObj_t1733252447::get_offset_of_m_fDirection_12(),
	MapObj_t1733252447::get_offset_of_m_bMoving_13(),
	MapObj_t1733252447::get_offset_of_m_bEjecting_14(),
	MapObj_t1733252447::get_offset_of_m_fEjectDistance_15(),
	MapObj_t1733252447::get_offset_of_m_vEjectStartPos_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (eMapObjType_t1356972478)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2964[7] = 
{
	eMapObjType_t1356972478::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (MsgBox_t1665650521), -1, sizeof(MsgBox_t1665650521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2965[8] = 
{
	MsgBox_t1665650521::get_offset_of_m_eType_2(),
	MsgBox_t1665650521_StaticFields::get_offset_of_s_Instance_3(),
	MsgBox_t1665650521::get_offset_of__btnOk_4(),
	MsgBox_t1665650521::get_offset_of__btnYes_5(),
	MsgBox_t1665650521::get_offset_of__btnNo_6(),
	MsgBox_t1665650521::get_offset_of__txtContent_7(),
	MsgBox_t1665650521::get_offset_of_delegateMethodOnClickButton_8(),
	MsgBox_t1665650521::get_offset_of_m_nParam_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (eMsgBoxType_t2231895019)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2966[4] = 
{
	eMsgBoxType_t2231895019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (DelegateMethod_OnClickButton_t3139999742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (CustomTypes_t2914914968), -1, sizeof(CustomTypes_t2914914968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2968[12] = 
{
	CustomTypes_t2914914968_StaticFields::get_offset_of_memVector3_0(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_memVector2_1(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_memQuarternion_2(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_memPlayer_3(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	CustomTypes_t2914914968_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (PhotonNetworkingMessage_t1476457985)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2969[31] = 
{
	PhotonNetworkingMessage_t1476457985::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (PhotonLogLevel_t4226222036)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2970[4] = 
{
	PhotonLogLevel_t4226222036::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (PhotonTargets_t2730697525)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2971[8] = 
{
	PhotonTargets_t2730697525::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (CloudRegionCode_t1925019500)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2972[13] = 
{
	CloudRegionCode_t1925019500::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (CloudRegionFlag_t3756941471)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2973[12] = 
{
	CloudRegionFlag_t3756941471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (ConnectionState_t836644691)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2974[6] = 
{
	ConnectionState_t836644691::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (EncryptionMode_t4213192103)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2975[3] = 
{
	EncryptionMode_t4213192103::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (EncryptionDataParameters_t25380775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (Extensions_t2612146612), -1, sizeof(Extensions_t2612146612_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2977[1] = 
{
	Extensions_t2612146612_StaticFields::get_offset_of_ParametersOfMethods_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (GameObjectExtensions_t182180175), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (FriendInfo_t533296844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[3] = 
{
	FriendInfo_t533296844::get_offset_of_U3CNameU3Ek__BackingField_0(),
	FriendInfo_t533296844::get_offset_of_U3CIsOnlineU3Ek__BackingField_1(),
	FriendInfo_t533296844::get_offset_of_U3CRoomU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (GizmoType_t3704257101)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2980[5] = 
{
	GizmoType_t3704257101::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (GizmoTypeDrawer_t184707570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (LoadBalancingPeer_t3218467959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[1] = 
{
	LoadBalancingPeer_t3218467959::get_offset_of_opParameters_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (RoomOptionBit_t4007729377)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2983[7] = 
{
	RoomOptionBit_t4007729377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (OpJoinRandomRoomParams_t491090087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[6] = 
{
	OpJoinRandomRoomParams_t491090087::get_offset_of_ExpectedCustomRoomProperties_0(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_ExpectedMaxPlayers_1(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_MatchingType_2(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_TypedLobby_3(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_SqlLobbyFilter_4(),
	OpJoinRandomRoomParams_t491090087::get_offset_of_ExpectedUsers_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (EnterRoomParams_t3960472384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[8] = 
{
	EnterRoomParams_t3960472384::get_offset_of_RoomName_0(),
	EnterRoomParams_t3960472384::get_offset_of_RoomOptions_1(),
	EnterRoomParams_t3960472384::get_offset_of_Lobby_2(),
	EnterRoomParams_t3960472384::get_offset_of_PlayerProperties_3(),
	EnterRoomParams_t3960472384::get_offset_of_OnGameServer_4(),
	EnterRoomParams_t3960472384::get_offset_of_CreateIfNotExists_5(),
	EnterRoomParams_t3960472384::get_offset_of_RejoinOnly_6(),
	EnterRoomParams_t3960472384::get_offset_of_ExpectedUsers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (ErrorCode_t835159227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (ActorProperties_t3254499384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (GamePropertyKey_t1877143020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (EventCode_t3064356988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (ParameterCode_t2914727942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[67] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (OperationCode_t3680831435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (JoinMode_t2253954680)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2992[5] = 
{
	JoinMode_t2253954680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (MatchmakingMode_t1165119589)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2993[4] = 
{
	MatchmakingMode_t1165119589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (ReceiverGroup_t3206452792)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2994[4] = 
{
	ReceiverGroup_t3206452792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (EventCaching_t168509732)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2995[13] = 
{
	EventCaching_t168509732::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (PropertyTypeFlag_t3957538841)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2996[5] = 
{
	PropertyTypeFlag_t3957538841::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (RoomOptions_t1787645948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[12] = 
{
	RoomOptions_t1787645948::get_offset_of_isVisibleField_0(),
	RoomOptions_t1787645948::get_offset_of_isOpenField_1(),
	RoomOptions_t1787645948::get_offset_of_MaxPlayers_2(),
	RoomOptions_t1787645948::get_offset_of_PlayerTtl_3(),
	RoomOptions_t1787645948::get_offset_of_EmptyRoomTtl_4(),
	RoomOptions_t1787645948::get_offset_of_cleanupCacheOnLeaveField_5(),
	RoomOptions_t1787645948::get_offset_of_CustomRoomProperties_6(),
	RoomOptions_t1787645948::get_offset_of_CustomRoomPropertiesForLobby_7(),
	RoomOptions_t1787645948::get_offset_of_Plugins_8(),
	RoomOptions_t1787645948::get_offset_of_suppressRoomEventsField_9(),
	RoomOptions_t1787645948::get_offset_of_publishUserIdField_10(),
	RoomOptions_t1787645948::get_offset_of_deleteNullPropertiesField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (RaiseEventOptions_t1229553678), -1, sizeof(RaiseEventOptions_t1229553678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2998[8] = 
{
	RaiseEventOptions_t1229553678_StaticFields::get_offset_of_Default_0(),
	RaiseEventOptions_t1229553678::get_offset_of_CachingOption_1(),
	RaiseEventOptions_t1229553678::get_offset_of_InterestGroup_2(),
	RaiseEventOptions_t1229553678::get_offset_of_TargetActors_3(),
	RaiseEventOptions_t1229553678::get_offset_of_Receivers_4(),
	RaiseEventOptions_t1229553678::get_offset_of_SequenceChannel_5(),
	RaiseEventOptions_t1229553678::get_offset_of_ForwardToWebhook_6(),
	RaiseEventOptions_t1229553678::get_offset_of_Encrypt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (LobbyType_t3695323860)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2999[4] = 
{
	LobbyType_t3695323860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
