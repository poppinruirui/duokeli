﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// Spine.ExposedList`1<Spine.Timeline>
struct ExposedList_1_t383950901;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// Spine.Animation
struct Animation_t615783283;
// Spine.AnimationStateData
struct AnimationStateData_t3010651567;
// Spine.Pool`1<Spine.TrackEntry>
struct Pool_1_t287387900;
// Spine.ExposedList`1<Spine.TrackEntry>
struct ExposedList_1_t4023600307;
// Spine.ExposedList`1<Spine.Event>
struct ExposedList_1_t4085685707;
// Spine.EventQueue
struct EventQueue_t808091267;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t1515895227;
// Spine.AnimationState/TrackEntryDelegate
struct TrackEntryDelegate_t363257942;
// Spine.AnimationState/TrackEntryEventDelegate
struct TrackEntryEventDelegate_t1653995044;
// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry>
struct List_1_t1823906703;
// Spine.AnimationState
struct AnimationState_t3637309382;
// System.Action
struct Action_t1264377477;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1363090323;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_t4104378640;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3365920845;
// Spine.Event[]
struct EventU5BU5D_t1966750348;
// System.Collections.Generic.List`1<CClassEditor/sThornOfThisClassConfig>
struct List_1_t1822539614;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Single[][]
struct SingleU5BU5DU5BU5D_t3206712258;
// Spine.VertexAttachment
struct VertexAttachment_t4074366829;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// Spine.TrackEntry
struct TrackEntry_t1316488441;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Spine.Event
struct Event_t1378573841;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Collections.Generic.List`1<CDust>
struct List_1_t198291272;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// CDust
struct CDust_t3021183826;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t285980105;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t1744877482;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Button
struct Button_t4055032469;
// System.Collections.Generic.List`1<CSpore>
struct List_1_t1482975291;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// System.Collections.Generic.Dictionary`2<System.String,CSpore>
struct Dictionary_2_t4091124144;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<CBoWenGrid>
struct List_1_t3035967681;
// UnityEngine.Rendering.SortingGroup
struct SortingGroup_t3239126932;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// UnityEngine.TextMesh[]
struct TextMeshU5BU5D_t2627012368;
// Ball
struct Ball_t2206666566;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// System.Collections.Generic.List`1<CTiaoZi>
struct List_1_t3123561718;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Collections.Generic.List`1<CMsgListItem>
struct List_1_t2611397916;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// CFrameAnimationEffect
struct CFrameAnimationEffect_t443605508;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// System.Collections.Generic.Dictionary`2<System.String,CMonsterEditor/sThornConfig>
struct Dictionary_2_t1027700750;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<CBeanCollection>
struct List_1_t4044148548;
// System.Collections.Generic.List`1<UnityEngine.Vector2[]>
struct List_1_t2929260728;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Color[]>
struct Dictionary_2_t727172712;
// System.Collections.Generic.Dictionary`2<System.String,CBeanCollection>
struct Dictionary_2_t2357330105;
// System.Collections.Generic.Dictionary`2<System.Int32,CBeanCollection>
struct Dictionary_2_t1460787137;
// System.Collections.Generic.List`1<CBeanCollection/sBeanRebornInfo>
struct List_1_t4257725728;
// System.Collections.Generic.List`1<CMonsterEditor/sGenerateBeanNode>
struct List_1_t2282564592;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// CMonsterEditor
struct CMonsterEditor_t2594843249;
// CMonster
struct CMonster_t1941470938;
// System.Collections.Generic.Dictionary`2<System.Int32,CClassEditor/sClassConfig>
struct Dictionary_2_t3948399185;
// System.Collections.Generic.Dictionary`2<System.String,CClassEditor/sMannualMonsterConfig>
struct Dictionary_2_t1629473320;
// System.Collections.Generic.Dictionary`2<System.String,CMonster>
struct Dictionary_2_t1726727237;
// System.Collections.Generic.Dictionary`2<System.Int64,CMonster>
struct Dictionary_2_t3004110722;
// System.Collections.Generic.List`1<CMonster>
struct List_1_t3413545680;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.Dictionary`2<System.Int32,CMonster>
struct Dictionary_2_t830184269;
// System.Collections.Generic.Dictionary`2<UnityEngine.Color,UnityEngine.MaterialPropertyBlock>
struct Dictionary_2_t2658501330;
// System.Collections.Generic.List`1<CCyberTreeListItem>
struct List_1_t4114524192;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t2999697109;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Collections.Generic.List`1<CWave>
struct List_1_t187246237;
// System.Collections.Generic.Dictionary`2<System.Int32,CWaveGroup>
struct Dictionary_2_t1648771995;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t1172778254;
// System.Collections.Generic.List`1<UnityEngine.UI.InputField[]>
struct List_1_t2644852996;
// System.Collections.Generic.List`1<System.String[]>
struct List_1_t2753864082;
// System.Collections.Generic.List`1<CSkillSystem/sSkillParam>
struct List_1_t2203743693;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2767979955;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// ETCJoystick
struct ETCJoystick_t3250784002;
// CArrow
struct CArrow_t1475493256;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADSCENEU3EC__ITERATOR0_T2187349195_H
#define U3CLOADSCENEU3EC__ITERATOR0_T2187349195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJIeSuanSystem/<LoadScene>c__Iterator0
struct  U3CLoadSceneU3Ec__Iterator0_t2187349195  : public RuntimeObject
{
public:
	// System.String CJIeSuanSystem/<LoadScene>c__Iterator0::scene_name
	String_t* ___scene_name_0;
	// System.Object CJIeSuanSystem/<LoadScene>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CJIeSuanSystem/<LoadScene>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CJIeSuanSystem/<LoadScene>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_scene_name_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2187349195, ___scene_name_0)); }
	inline String_t* get_scene_name_0() const { return ___scene_name_0; }
	inline String_t** get_address_of_scene_name_0() { return &___scene_name_0; }
	inline void set_scene_name_0(String_t* value)
	{
		___scene_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_name_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2187349195, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2187349195, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2187349195, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3EC__ITERATOR0_T2187349195_H
#ifndef ANIMATION_T615783283_H
#define ANIMATION_T615783283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Animation
struct  Animation_t615783283  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Timeline> Spine.Animation::timelines
	ExposedList_1_t383950901 * ___timelines_0;
	// System.Single Spine.Animation::duration
	float ___duration_1;
	// System.String Spine.Animation::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_timelines_0() { return static_cast<int32_t>(offsetof(Animation_t615783283, ___timelines_0)); }
	inline ExposedList_1_t383950901 * get_timelines_0() const { return ___timelines_0; }
	inline ExposedList_1_t383950901 ** get_address_of_timelines_0() { return &___timelines_0; }
	inline void set_timelines_0(ExposedList_1_t383950901 * value)
	{
		___timelines_0 = value;
		Il2CppCodeGenWriteBarrier((&___timelines_0), value);
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(Animation_t615783283, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Animation_t615783283, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T615783283_H
#ifndef CURVETIMELINE_T3673209699_H
#define CURVETIMELINE_T3673209699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.CurveTimeline
struct  CurveTimeline_t3673209699  : public RuntimeObject
{
public:
	// System.Single[] Spine.CurveTimeline::curves
	SingleU5BU5D_t1444911251* ___curves_4;

public:
	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(CurveTimeline_t3673209699, ___curves_4)); }
	inline SingleU5BU5D_t1444911251* get_curves_4() const { return ___curves_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(SingleU5BU5D_t1444911251* value)
	{
		___curves_4 = value;
		Il2CppCodeGenWriteBarrier((&___curves_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVETIMELINE_T3673209699_H
#ifndef ANIMATIONSTATE_T3637309382_H
#define ANIMATIONSTATE_T3637309382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState
struct  AnimationState_t3637309382  : public RuntimeObject
{
public:
	// Spine.AnimationStateData Spine.AnimationState::data
	AnimationStateData_t3010651567 * ___data_5;
	// Spine.Pool`1<Spine.TrackEntry> Spine.AnimationState::trackEntryPool
	Pool_1_t287387900 * ___trackEntryPool_6;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::tracks
	ExposedList_1_t4023600307 * ___tracks_7;
	// Spine.ExposedList`1<Spine.Event> Spine.AnimationState::events
	ExposedList_1_t4085685707 * ___events_8;
	// Spine.EventQueue Spine.AnimationState::queue
	EventQueue_t808091267 * ___queue_9;
	// System.Collections.Generic.HashSet`1<System.Int32> Spine.AnimationState::propertyIDs
	HashSet_1_t1515895227 * ___propertyIDs_10;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::mixingTo
	ExposedList_1_t4023600307 * ___mixingTo_11;
	// System.Boolean Spine.AnimationState::animationsChanged
	bool ___animationsChanged_12;
	// System.Single Spine.AnimationState::timeScale
	float ___timeScale_13;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Interrupt
	TrackEntryDelegate_t363257942 * ___Interrupt_14;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::End
	TrackEntryDelegate_t363257942 * ___End_15;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Dispose
	TrackEntryDelegate_t363257942 * ___Dispose_16;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Complete
	TrackEntryDelegate_t363257942 * ___Complete_17;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Start
	TrackEntryDelegate_t363257942 * ___Start_18;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.AnimationState::Event
	TrackEntryEventDelegate_t1653995044 * ___Event_19;

public:
	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___data_5)); }
	inline AnimationStateData_t3010651567 * get_data_5() const { return ___data_5; }
	inline AnimationStateData_t3010651567 ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(AnimationStateData_t3010651567 * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier((&___data_5), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_6() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___trackEntryPool_6)); }
	inline Pool_1_t287387900 * get_trackEntryPool_6() const { return ___trackEntryPool_6; }
	inline Pool_1_t287387900 ** get_address_of_trackEntryPool_6() { return &___trackEntryPool_6; }
	inline void set_trackEntryPool_6(Pool_1_t287387900 * value)
	{
		___trackEntryPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_6), value);
	}

	inline static int32_t get_offset_of_tracks_7() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___tracks_7)); }
	inline ExposedList_1_t4023600307 * get_tracks_7() const { return ___tracks_7; }
	inline ExposedList_1_t4023600307 ** get_address_of_tracks_7() { return &___tracks_7; }
	inline void set_tracks_7(ExposedList_1_t4023600307 * value)
	{
		___tracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracks_7), value);
	}

	inline static int32_t get_offset_of_events_8() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___events_8)); }
	inline ExposedList_1_t4085685707 * get_events_8() const { return ___events_8; }
	inline ExposedList_1_t4085685707 ** get_address_of_events_8() { return &___events_8; }
	inline void set_events_8(ExposedList_1_t4085685707 * value)
	{
		___events_8 = value;
		Il2CppCodeGenWriteBarrier((&___events_8), value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___queue_9)); }
	inline EventQueue_t808091267 * get_queue_9() const { return ___queue_9; }
	inline EventQueue_t808091267 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(EventQueue_t808091267 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___queue_9), value);
	}

	inline static int32_t get_offset_of_propertyIDs_10() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___propertyIDs_10)); }
	inline HashSet_1_t1515895227 * get_propertyIDs_10() const { return ___propertyIDs_10; }
	inline HashSet_1_t1515895227 ** get_address_of_propertyIDs_10() { return &___propertyIDs_10; }
	inline void set_propertyIDs_10(HashSet_1_t1515895227 * value)
	{
		___propertyIDs_10 = value;
		Il2CppCodeGenWriteBarrier((&___propertyIDs_10), value);
	}

	inline static int32_t get_offset_of_mixingTo_11() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___mixingTo_11)); }
	inline ExposedList_1_t4023600307 * get_mixingTo_11() const { return ___mixingTo_11; }
	inline ExposedList_1_t4023600307 ** get_address_of_mixingTo_11() { return &___mixingTo_11; }
	inline void set_mixingTo_11(ExposedList_1_t4023600307 * value)
	{
		___mixingTo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mixingTo_11), value);
	}

	inline static int32_t get_offset_of_animationsChanged_12() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___animationsChanged_12)); }
	inline bool get_animationsChanged_12() const { return ___animationsChanged_12; }
	inline bool* get_address_of_animationsChanged_12() { return &___animationsChanged_12; }
	inline void set_animationsChanged_12(bool value)
	{
		___animationsChanged_12 = value;
	}

	inline static int32_t get_offset_of_timeScale_13() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___timeScale_13)); }
	inline float get_timeScale_13() const { return ___timeScale_13; }
	inline float* get_address_of_timeScale_13() { return &___timeScale_13; }
	inline void set_timeScale_13(float value)
	{
		___timeScale_13 = value;
	}

	inline static int32_t get_offset_of_Interrupt_14() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Interrupt_14)); }
	inline TrackEntryDelegate_t363257942 * get_Interrupt_14() const { return ___Interrupt_14; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Interrupt_14() { return &___Interrupt_14; }
	inline void set_Interrupt_14(TrackEntryDelegate_t363257942 * value)
	{
		___Interrupt_14 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_14), value);
	}

	inline static int32_t get_offset_of_End_15() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___End_15)); }
	inline TrackEntryDelegate_t363257942 * get_End_15() const { return ___End_15; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_End_15() { return &___End_15; }
	inline void set_End_15(TrackEntryDelegate_t363257942 * value)
	{
		___End_15 = value;
		Il2CppCodeGenWriteBarrier((&___End_15), value);
	}

	inline static int32_t get_offset_of_Dispose_16() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Dispose_16)); }
	inline TrackEntryDelegate_t363257942 * get_Dispose_16() const { return ___Dispose_16; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Dispose_16() { return &___Dispose_16; }
	inline void set_Dispose_16(TrackEntryDelegate_t363257942 * value)
	{
		___Dispose_16 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_16), value);
	}

	inline static int32_t get_offset_of_Complete_17() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Complete_17)); }
	inline TrackEntryDelegate_t363257942 * get_Complete_17() const { return ___Complete_17; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Complete_17() { return &___Complete_17; }
	inline void set_Complete_17(TrackEntryDelegate_t363257942 * value)
	{
		___Complete_17 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_17), value);
	}

	inline static int32_t get_offset_of_Start_18() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Start_18)); }
	inline TrackEntryDelegate_t363257942 * get_Start_18() const { return ___Start_18; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Start_18() { return &___Start_18; }
	inline void set_Start_18(TrackEntryDelegate_t363257942 * value)
	{
		___Start_18 = value;
		Il2CppCodeGenWriteBarrier((&___Start_18), value);
	}

	inline static int32_t get_offset_of_Event_19() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Event_19)); }
	inline TrackEntryEventDelegate_t1653995044 * get_Event_19() const { return ___Event_19; }
	inline TrackEntryEventDelegate_t1653995044 ** get_address_of_Event_19() { return &___Event_19; }
	inline void set_Event_19(TrackEntryEventDelegate_t1653995044 * value)
	{
		___Event_19 = value;
		Il2CppCodeGenWriteBarrier((&___Event_19), value);
	}
};

struct AnimationState_t3637309382_StaticFields
{
public:
	// Spine.Animation Spine.AnimationState::EmptyAnimation
	Animation_t615783283 * ___EmptyAnimation_0;

public:
	inline static int32_t get_offset_of_EmptyAnimation_0() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382_StaticFields, ___EmptyAnimation_0)); }
	inline Animation_t615783283 * get_EmptyAnimation_0() const { return ___EmptyAnimation_0; }
	inline Animation_t615783283 ** get_address_of_EmptyAnimation_0() { return &___EmptyAnimation_0; }
	inline void set_EmptyAnimation_0(Animation_t615783283 * value)
	{
		___EmptyAnimation_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAnimation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T3637309382_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EVENTQUEUE_T808091267_H
#define EVENTQUEUE_T808091267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue
struct  EventQueue_t808091267  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry> Spine.EventQueue::eventQueueEntries
	List_1_t1823906703 * ___eventQueueEntries_0;
	// System.Boolean Spine.EventQueue::drainDisabled
	bool ___drainDisabled_1;
	// Spine.AnimationState Spine.EventQueue::state
	AnimationState_t3637309382 * ___state_2;
	// Spine.Pool`1<Spine.TrackEntry> Spine.EventQueue::trackEntryPool
	Pool_1_t287387900 * ___trackEntryPool_3;
	// System.Action Spine.EventQueue::AnimationsChanged
	Action_t1264377477 * ___AnimationsChanged_4;

public:
	inline static int32_t get_offset_of_eventQueueEntries_0() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___eventQueueEntries_0)); }
	inline List_1_t1823906703 * get_eventQueueEntries_0() const { return ___eventQueueEntries_0; }
	inline List_1_t1823906703 ** get_address_of_eventQueueEntries_0() { return &___eventQueueEntries_0; }
	inline void set_eventQueueEntries_0(List_1_t1823906703 * value)
	{
		___eventQueueEntries_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventQueueEntries_0), value);
	}

	inline static int32_t get_offset_of_drainDisabled_1() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___drainDisabled_1)); }
	inline bool get_drainDisabled_1() const { return ___drainDisabled_1; }
	inline bool* get_address_of_drainDisabled_1() { return &___drainDisabled_1; }
	inline void set_drainDisabled_1(bool value)
	{
		___drainDisabled_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___state_2)); }
	inline AnimationState_t3637309382 * get_state_2() const { return ___state_2; }
	inline AnimationState_t3637309382 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(AnimationState_t3637309382 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_3() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___trackEntryPool_3)); }
	inline Pool_1_t287387900 * get_trackEntryPool_3() const { return ___trackEntryPool_3; }
	inline Pool_1_t287387900 ** get_address_of_trackEntryPool_3() { return &___trackEntryPool_3; }
	inline void set_trackEntryPool_3(Pool_1_t287387900 * value)
	{
		___trackEntryPool_3 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_3), value);
	}

	inline static int32_t get_offset_of_AnimationsChanged_4() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___AnimationsChanged_4)); }
	inline Action_t1264377477 * get_AnimationsChanged_4() const { return ___AnimationsChanged_4; }
	inline Action_t1264377477 ** get_address_of_AnimationsChanged_4() { return &___AnimationsChanged_4; }
	inline void set_AnimationsChanged_4(Action_t1264377477 * value)
	{
		___AnimationsChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsChanged_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTQUEUE_T808091267_H
#ifndef TRACKENTRY_T1316488441_H
#define TRACKENTRY_T1316488441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TrackEntry
struct  TrackEntry_t1316488441  : public RuntimeObject
{
public:
	// Spine.Animation Spine.TrackEntry::animation
	Animation_t615783283 * ___animation_0;
	// Spine.TrackEntry Spine.TrackEntry::next
	TrackEntry_t1316488441 * ___next_1;
	// Spine.TrackEntry Spine.TrackEntry::mixingFrom
	TrackEntry_t1316488441 * ___mixingFrom_2;
	// System.Int32 Spine.TrackEntry::trackIndex
	int32_t ___trackIndex_3;
	// System.Boolean Spine.TrackEntry::loop
	bool ___loop_4;
	// System.Single Spine.TrackEntry::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.TrackEntry::attachmentThreshold
	float ___attachmentThreshold_6;
	// System.Single Spine.TrackEntry::drawOrderThreshold
	float ___drawOrderThreshold_7;
	// System.Single Spine.TrackEntry::animationStart
	float ___animationStart_8;
	// System.Single Spine.TrackEntry::animationEnd
	float ___animationEnd_9;
	// System.Single Spine.TrackEntry::animationLast
	float ___animationLast_10;
	// System.Single Spine.TrackEntry::nextAnimationLast
	float ___nextAnimationLast_11;
	// System.Single Spine.TrackEntry::delay
	float ___delay_12;
	// System.Single Spine.TrackEntry::trackTime
	float ___trackTime_13;
	// System.Single Spine.TrackEntry::trackLast
	float ___trackLast_14;
	// System.Single Spine.TrackEntry::nextTrackLast
	float ___nextTrackLast_15;
	// System.Single Spine.TrackEntry::trackEnd
	float ___trackEnd_16;
	// System.Single Spine.TrackEntry::timeScale
	float ___timeScale_17;
	// System.Single Spine.TrackEntry::alpha
	float ___alpha_18;
	// System.Single Spine.TrackEntry::mixTime
	float ___mixTime_19;
	// System.Single Spine.TrackEntry::mixDuration
	float ___mixDuration_20;
	// System.Single Spine.TrackEntry::interruptAlpha
	float ___interruptAlpha_21;
	// System.Single Spine.TrackEntry::totalAlpha
	float ___totalAlpha_22;
	// Spine.ExposedList`1<System.Int32> Spine.TrackEntry::timelineData
	ExposedList_1_t1363090323 * ___timelineData_23;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.TrackEntry::timelineDipMix
	ExposedList_1_t4023600307 * ___timelineDipMix_24;
	// Spine.ExposedList`1<System.Single> Spine.TrackEntry::timelinesRotation
	ExposedList_1_t4104378640 * ___timelinesRotation_25;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Interrupt
	TrackEntryDelegate_t363257942 * ___Interrupt_26;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::End
	TrackEntryDelegate_t363257942 * ___End_27;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Dispose
	TrackEntryDelegate_t363257942 * ___Dispose_28;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Complete
	TrackEntryDelegate_t363257942 * ___Complete_29;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Start
	TrackEntryDelegate_t363257942 * ___Start_30;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.TrackEntry::Event
	TrackEntryEventDelegate_t1653995044 * ___Event_31;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animation_0)); }
	inline Animation_t615783283 * get_animation_0() const { return ___animation_0; }
	inline Animation_t615783283 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_t615783283 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___next_1)); }
	inline TrackEntry_t1316488441 * get_next_1() const { return ___next_1; }
	inline TrackEntry_t1316488441 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(TrackEntry_t1316488441 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_mixingFrom_2() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___mixingFrom_2)); }
	inline TrackEntry_t1316488441 * get_mixingFrom_2() const { return ___mixingFrom_2; }
	inline TrackEntry_t1316488441 ** get_address_of_mixingFrom_2() { return &___mixingFrom_2; }
	inline void set_mixingFrom_2(TrackEntry_t1316488441 * value)
	{
		___mixingFrom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mixingFrom_2), value);
	}

	inline static int32_t get_offset_of_trackIndex_3() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackIndex_3)); }
	inline int32_t get_trackIndex_3() const { return ___trackIndex_3; }
	inline int32_t* get_address_of_trackIndex_3() { return &___trackIndex_3; }
	inline void set_trackIndex_3(int32_t value)
	{
		___trackIndex_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_6() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___attachmentThreshold_6)); }
	inline float get_attachmentThreshold_6() const { return ___attachmentThreshold_6; }
	inline float* get_address_of_attachmentThreshold_6() { return &___attachmentThreshold_6; }
	inline void set_attachmentThreshold_6(float value)
	{
		___attachmentThreshold_6 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_7() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___drawOrderThreshold_7)); }
	inline float get_drawOrderThreshold_7() const { return ___drawOrderThreshold_7; }
	inline float* get_address_of_drawOrderThreshold_7() { return &___drawOrderThreshold_7; }
	inline void set_drawOrderThreshold_7(float value)
	{
		___drawOrderThreshold_7 = value;
	}

	inline static int32_t get_offset_of_animationStart_8() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animationStart_8)); }
	inline float get_animationStart_8() const { return ___animationStart_8; }
	inline float* get_address_of_animationStart_8() { return &___animationStart_8; }
	inline void set_animationStart_8(float value)
	{
		___animationStart_8 = value;
	}

	inline static int32_t get_offset_of_animationEnd_9() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animationEnd_9)); }
	inline float get_animationEnd_9() const { return ___animationEnd_9; }
	inline float* get_address_of_animationEnd_9() { return &___animationEnd_9; }
	inline void set_animationEnd_9(float value)
	{
		___animationEnd_9 = value;
	}

	inline static int32_t get_offset_of_animationLast_10() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animationLast_10)); }
	inline float get_animationLast_10() const { return ___animationLast_10; }
	inline float* get_address_of_animationLast_10() { return &___animationLast_10; }
	inline void set_animationLast_10(float value)
	{
		___animationLast_10 = value;
	}

	inline static int32_t get_offset_of_nextAnimationLast_11() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___nextAnimationLast_11)); }
	inline float get_nextAnimationLast_11() const { return ___nextAnimationLast_11; }
	inline float* get_address_of_nextAnimationLast_11() { return &___nextAnimationLast_11; }
	inline void set_nextAnimationLast_11(float value)
	{
		___nextAnimationLast_11 = value;
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___delay_12)); }
	inline float get_delay_12() const { return ___delay_12; }
	inline float* get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(float value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_trackTime_13() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackTime_13)); }
	inline float get_trackTime_13() const { return ___trackTime_13; }
	inline float* get_address_of_trackTime_13() { return &___trackTime_13; }
	inline void set_trackTime_13(float value)
	{
		___trackTime_13 = value;
	}

	inline static int32_t get_offset_of_trackLast_14() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackLast_14)); }
	inline float get_trackLast_14() const { return ___trackLast_14; }
	inline float* get_address_of_trackLast_14() { return &___trackLast_14; }
	inline void set_trackLast_14(float value)
	{
		___trackLast_14 = value;
	}

	inline static int32_t get_offset_of_nextTrackLast_15() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___nextTrackLast_15)); }
	inline float get_nextTrackLast_15() const { return ___nextTrackLast_15; }
	inline float* get_address_of_nextTrackLast_15() { return &___nextTrackLast_15; }
	inline void set_nextTrackLast_15(float value)
	{
		___nextTrackLast_15 = value;
	}

	inline static int32_t get_offset_of_trackEnd_16() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackEnd_16)); }
	inline float get_trackEnd_16() const { return ___trackEnd_16; }
	inline float* get_address_of_trackEnd_16() { return &___trackEnd_16; }
	inline void set_trackEnd_16(float value)
	{
		___trackEnd_16 = value;
	}

	inline static int32_t get_offset_of_timeScale_17() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timeScale_17)); }
	inline float get_timeScale_17() const { return ___timeScale_17; }
	inline float* get_address_of_timeScale_17() { return &___timeScale_17; }
	inline void set_timeScale_17(float value)
	{
		___timeScale_17 = value;
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___alpha_18)); }
	inline float get_alpha_18() const { return ___alpha_18; }
	inline float* get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(float value)
	{
		___alpha_18 = value;
	}

	inline static int32_t get_offset_of_mixTime_19() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___mixTime_19)); }
	inline float get_mixTime_19() const { return ___mixTime_19; }
	inline float* get_address_of_mixTime_19() { return &___mixTime_19; }
	inline void set_mixTime_19(float value)
	{
		___mixTime_19 = value;
	}

	inline static int32_t get_offset_of_mixDuration_20() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___mixDuration_20)); }
	inline float get_mixDuration_20() const { return ___mixDuration_20; }
	inline float* get_address_of_mixDuration_20() { return &___mixDuration_20; }
	inline void set_mixDuration_20(float value)
	{
		___mixDuration_20 = value;
	}

	inline static int32_t get_offset_of_interruptAlpha_21() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___interruptAlpha_21)); }
	inline float get_interruptAlpha_21() const { return ___interruptAlpha_21; }
	inline float* get_address_of_interruptAlpha_21() { return &___interruptAlpha_21; }
	inline void set_interruptAlpha_21(float value)
	{
		___interruptAlpha_21 = value;
	}

	inline static int32_t get_offset_of_totalAlpha_22() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___totalAlpha_22)); }
	inline float get_totalAlpha_22() const { return ___totalAlpha_22; }
	inline float* get_address_of_totalAlpha_22() { return &___totalAlpha_22; }
	inline void set_totalAlpha_22(float value)
	{
		___totalAlpha_22 = value;
	}

	inline static int32_t get_offset_of_timelineData_23() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timelineData_23)); }
	inline ExposedList_1_t1363090323 * get_timelineData_23() const { return ___timelineData_23; }
	inline ExposedList_1_t1363090323 ** get_address_of_timelineData_23() { return &___timelineData_23; }
	inline void set_timelineData_23(ExposedList_1_t1363090323 * value)
	{
		___timelineData_23 = value;
		Il2CppCodeGenWriteBarrier((&___timelineData_23), value);
	}

	inline static int32_t get_offset_of_timelineDipMix_24() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timelineDipMix_24)); }
	inline ExposedList_1_t4023600307 * get_timelineDipMix_24() const { return ___timelineDipMix_24; }
	inline ExposedList_1_t4023600307 ** get_address_of_timelineDipMix_24() { return &___timelineDipMix_24; }
	inline void set_timelineDipMix_24(ExposedList_1_t4023600307 * value)
	{
		___timelineDipMix_24 = value;
		Il2CppCodeGenWriteBarrier((&___timelineDipMix_24), value);
	}

	inline static int32_t get_offset_of_timelinesRotation_25() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timelinesRotation_25)); }
	inline ExposedList_1_t4104378640 * get_timelinesRotation_25() const { return ___timelinesRotation_25; }
	inline ExposedList_1_t4104378640 ** get_address_of_timelinesRotation_25() { return &___timelinesRotation_25; }
	inline void set_timelinesRotation_25(ExposedList_1_t4104378640 * value)
	{
		___timelinesRotation_25 = value;
		Il2CppCodeGenWriteBarrier((&___timelinesRotation_25), value);
	}

	inline static int32_t get_offset_of_Interrupt_26() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Interrupt_26)); }
	inline TrackEntryDelegate_t363257942 * get_Interrupt_26() const { return ___Interrupt_26; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Interrupt_26() { return &___Interrupt_26; }
	inline void set_Interrupt_26(TrackEntryDelegate_t363257942 * value)
	{
		___Interrupt_26 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_26), value);
	}

	inline static int32_t get_offset_of_End_27() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___End_27)); }
	inline TrackEntryDelegate_t363257942 * get_End_27() const { return ___End_27; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_End_27() { return &___End_27; }
	inline void set_End_27(TrackEntryDelegate_t363257942 * value)
	{
		___End_27 = value;
		Il2CppCodeGenWriteBarrier((&___End_27), value);
	}

	inline static int32_t get_offset_of_Dispose_28() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Dispose_28)); }
	inline TrackEntryDelegate_t363257942 * get_Dispose_28() const { return ___Dispose_28; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Dispose_28() { return &___Dispose_28; }
	inline void set_Dispose_28(TrackEntryDelegate_t363257942 * value)
	{
		___Dispose_28 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_28), value);
	}

	inline static int32_t get_offset_of_Complete_29() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Complete_29)); }
	inline TrackEntryDelegate_t363257942 * get_Complete_29() const { return ___Complete_29; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Complete_29() { return &___Complete_29; }
	inline void set_Complete_29(TrackEntryDelegate_t363257942 * value)
	{
		___Complete_29 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_29), value);
	}

	inline static int32_t get_offset_of_Start_30() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Start_30)); }
	inline TrackEntryDelegate_t363257942 * get_Start_30() const { return ___Start_30; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Start_30() { return &___Start_30; }
	inline void set_Start_30(TrackEntryDelegate_t363257942 * value)
	{
		___Start_30 = value;
		Il2CppCodeGenWriteBarrier((&___Start_30), value);
	}

	inline static int32_t get_offset_of_Event_31() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Event_31)); }
	inline TrackEntryEventDelegate_t1653995044 * get_Event_31() const { return ___Event_31; }
	inline TrackEntryEventDelegate_t1653995044 ** get_address_of_Event_31() { return &___Event_31; }
	inline void set_Event_31(TrackEntryEventDelegate_t1653995044 * value)
	{
		___Event_31 = value;
		Il2CppCodeGenWriteBarrier((&___Event_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRY_T1316488441_H
#ifndef ATTACHMENTTIMELINE_T2048728856_H
#define ATTACHMENTTIMELINE_T2048728856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentTimeline
struct  AttachmentTimeline_t2048728856  : public RuntimeObject
{
public:
	// System.Int32 Spine.AttachmentTimeline::slotIndex
	int32_t ___slotIndex_0;
	// System.Single[] Spine.AttachmentTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_1;
	// System.String[] Spine.AttachmentTimeline::attachmentNames
	StringU5BU5D_t1281789340* ___attachmentNames_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2048728856, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2048728856, ___frames_1)); }
	inline SingleU5BU5D_t1444911251* get_frames_1() const { return ___frames_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(SingleU5BU5D_t1444911251* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_attachmentNames_2() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2048728856, ___attachmentNames_2)); }
	inline StringU5BU5D_t1281789340* get_attachmentNames_2() const { return ___attachmentNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_attachmentNames_2() { return &___attachmentNames_2; }
	inline void set_attachmentNames_2(StringU5BU5D_t1281789340* value)
	{
		___attachmentNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentNames_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTIMELINE_T2048728856_H
#ifndef DRAWORDERTIMELINE_T1092982325_H
#define DRAWORDERTIMELINE_T1092982325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DrawOrderTimeline
struct  DrawOrderTimeline_t1092982325  : public RuntimeObject
{
public:
	// System.Single[] Spine.DrawOrderTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_0;
	// System.Int32[][] Spine.DrawOrderTimeline::drawOrders
	Int32U5BU5DU5BU5D_t3365920845* ___drawOrders_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t1092982325, ___frames_0)); }
	inline SingleU5BU5D_t1444911251* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_t1444911251* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_drawOrders_1() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t1092982325, ___drawOrders_1)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get_drawOrders_1() const { return ___drawOrders_1; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of_drawOrders_1() { return &___drawOrders_1; }
	inline void set_drawOrders_1(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		___drawOrders_1 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrders_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWORDERTIMELINE_T1092982325_H
#ifndef EVENTTIMELINE_T2341881094_H
#define EVENTTIMELINE_T2341881094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventTimeline
struct  EventTimeline_t2341881094  : public RuntimeObject
{
public:
	// System.Single[] Spine.EventTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_0;
	// Spine.Event[] Spine.EventTimeline::events
	EventU5BU5D_t1966750348* ___events_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(EventTimeline_t2341881094, ___frames_0)); }
	inline SingleU5BU5D_t1444911251* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_t1444911251* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_events_1() { return static_cast<int32_t>(offsetof(EventTimeline_t2341881094, ___events_1)); }
	inline EventU5BU5D_t1966750348* get_events_1() const { return ___events_1; }
	inline EventU5BU5D_t1966750348** get_address_of_events_1() { return &___events_1; }
	inline void set_events_1(EventU5BU5D_t1966750348* value)
	{
		___events_1 = value;
		Il2CppCodeGenWriteBarrier((&___events_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTIMELINE_T2341881094_H
#ifndef TRANSLATETIMELINE_T2733426737_H
#define TRANSLATETIMELINE_T2733426737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TranslateTimeline
struct  TranslateTimeline_t2733426737  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.TranslateTimeline::boneIndex
	int32_t ___boneIndex_11;
	// System.Single[] Spine.TranslateTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_12;

public:
	inline static int32_t get_offset_of_boneIndex_11() { return static_cast<int32_t>(offsetof(TranslateTimeline_t2733426737, ___boneIndex_11)); }
	inline int32_t get_boneIndex_11() const { return ___boneIndex_11; }
	inline int32_t* get_address_of_boneIndex_11() { return &___boneIndex_11; }
	inline void set_boneIndex_11(int32_t value)
	{
		___boneIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(TranslateTimeline_t2733426737, ___frames_12)); }
	inline SingleU5BU5D_t1444911251* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t1444911251* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATETIMELINE_T2733426737_H
#ifndef TWOCOLORTIMELINE_T1834727230_H
#define TWOCOLORTIMELINE_T1834727230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TwoColorTimeline
struct  TwoColorTimeline_t1834727230  : public CurveTimeline_t3673209699
{
public:
	// System.Single[] Spine.TwoColorTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_21;
	// System.Int32 Spine.TwoColorTimeline::slotIndex
	int32_t ___slotIndex_22;

public:
	inline static int32_t get_offset_of_frames_21() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t1834727230, ___frames_21)); }
	inline SingleU5BU5D_t1444911251* get_frames_21() const { return ___frames_21; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_21() { return &___frames_21; }
	inline void set_frames_21(SingleU5BU5D_t1444911251* value)
	{
		___frames_21 = value;
		Il2CppCodeGenWriteBarrier((&___frames_21), value);
	}

	inline static int32_t get_offset_of_slotIndex_22() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t1834727230, ___slotIndex_22)); }
	inline int32_t get_slotIndex_22() const { return ___slotIndex_22; }
	inline int32_t* get_address_of_slotIndex_22() { return &___slotIndex_22; }
	inline void set_slotIndex_22(int32_t value)
	{
		___slotIndex_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOCOLORTIMELINE_T1834727230_H
#ifndef COLORTIMELINE_T3754116780_H
#define COLORTIMELINE_T3754116780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ColorTimeline
struct  ColorTimeline_t3754116780  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.ColorTimeline::slotIndex
	int32_t ___slotIndex_15;
	// System.Single[] Spine.ColorTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_16;

public:
	inline static int32_t get_offset_of_slotIndex_15() { return static_cast<int32_t>(offsetof(ColorTimeline_t3754116780, ___slotIndex_15)); }
	inline int32_t get_slotIndex_15() const { return ___slotIndex_15; }
	inline int32_t* get_address_of_slotIndex_15() { return &___slotIndex_15; }
	inline void set_slotIndex_15(int32_t value)
	{
		___slotIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(ColorTimeline_t3754116780, ___frames_16)); }
	inline SingleU5BU5D_t1444911251* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_t1444911251* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTIMELINE_T3754116780_H
#ifndef SCLASSCONFIG_T764718558_H
#define SCLASSCONFIG_T764718558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CClassEditor/sClassConfig
struct  sClassConfig_t764718558 
{
public:
	// System.Int32 CClassEditor/sClassConfig::nId
	int32_t ___nId_0;
	// System.Single CClassEditor/sClassConfig::fSizeThreshold
	float ___fSizeThreshold_1;
	// System.Single CClassEditor/sClassConfig::fNormalAttenuate
	float ___fNormalAttenuate_2;
	// System.Single CClassEditor/sClassConfig::fInvasionAttenuate
	float ___fInvasionAttenuate_3;
	// System.Single CClassEditor/sClassConfig::fInvasionSpeedSlowdown
	float ___fInvasionSpeedSlowdown_4;
	// System.Collections.Generic.List`1<CClassEditor/sThornOfThisClassConfig> CClassEditor/sClassConfig::lstThorns
	List_1_t1822539614 * ___lstThorns_5;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sClassConfig_t764718558, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_fSizeThreshold_1() { return static_cast<int32_t>(offsetof(sClassConfig_t764718558, ___fSizeThreshold_1)); }
	inline float get_fSizeThreshold_1() const { return ___fSizeThreshold_1; }
	inline float* get_address_of_fSizeThreshold_1() { return &___fSizeThreshold_1; }
	inline void set_fSizeThreshold_1(float value)
	{
		___fSizeThreshold_1 = value;
	}

	inline static int32_t get_offset_of_fNormalAttenuate_2() { return static_cast<int32_t>(offsetof(sClassConfig_t764718558, ___fNormalAttenuate_2)); }
	inline float get_fNormalAttenuate_2() const { return ___fNormalAttenuate_2; }
	inline float* get_address_of_fNormalAttenuate_2() { return &___fNormalAttenuate_2; }
	inline void set_fNormalAttenuate_2(float value)
	{
		___fNormalAttenuate_2 = value;
	}

	inline static int32_t get_offset_of_fInvasionAttenuate_3() { return static_cast<int32_t>(offsetof(sClassConfig_t764718558, ___fInvasionAttenuate_3)); }
	inline float get_fInvasionAttenuate_3() const { return ___fInvasionAttenuate_3; }
	inline float* get_address_of_fInvasionAttenuate_3() { return &___fInvasionAttenuate_3; }
	inline void set_fInvasionAttenuate_3(float value)
	{
		___fInvasionAttenuate_3 = value;
	}

	inline static int32_t get_offset_of_fInvasionSpeedSlowdown_4() { return static_cast<int32_t>(offsetof(sClassConfig_t764718558, ___fInvasionSpeedSlowdown_4)); }
	inline float get_fInvasionSpeedSlowdown_4() const { return ___fInvasionSpeedSlowdown_4; }
	inline float* get_address_of_fInvasionSpeedSlowdown_4() { return &___fInvasionSpeedSlowdown_4; }
	inline void set_fInvasionSpeedSlowdown_4(float value)
	{
		___fInvasionSpeedSlowdown_4 = value;
	}

	inline static int32_t get_offset_of_lstThorns_5() { return static_cast<int32_t>(offsetof(sClassConfig_t764718558, ___lstThorns_5)); }
	inline List_1_t1822539614 * get_lstThorns_5() const { return ___lstThorns_5; }
	inline List_1_t1822539614 ** get_address_of_lstThorns_5() { return &___lstThorns_5; }
	inline void set_lstThorns_5(List_1_t1822539614 * value)
	{
		___lstThorns_5 = value;
		Il2CppCodeGenWriteBarrier((&___lstThorns_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CClassEditor/sClassConfig
struct sClassConfig_t764718558_marshaled_pinvoke
{
	int32_t ___nId_0;
	float ___fSizeThreshold_1;
	float ___fNormalAttenuate_2;
	float ___fInvasionAttenuate_3;
	float ___fInvasionSpeedSlowdown_4;
	List_1_t1822539614 * ___lstThorns_5;
};
// Native definition for COM marshalling of CClassEditor/sClassConfig
struct sClassConfig_t764718558_marshaled_com
{
	int32_t ___nId_0;
	float ___fSizeThreshold_1;
	float ___fNormalAttenuate_2;
	float ___fInvasionAttenuate_3;
	float ___fInvasionSpeedSlowdown_4;
	List_1_t1822539614 * ___lstThorns_5;
};
#endif // SCLASSCONFIG_T764718558_H
#ifndef STHORNOFTHISCLASSCONFIG_T350464872_H
#define STHORNOFTHISCLASSCONFIG_T350464872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CClassEditor/sThornOfThisClassConfig
struct  sThornOfThisClassConfig_t350464872 
{
public:
	// System.String CClassEditor/sThornOfThisClassConfig::szThornId
	String_t* ___szThornId_0;
	// System.Single CClassEditor/sThornOfThisClassConfig::fDensity
	float ___fDensity_1;
	// System.Single CClassEditor/sThornOfThisClassConfig::fRebornTime
	float ___fRebornTime_2;

public:
	inline static int32_t get_offset_of_szThornId_0() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___szThornId_0)); }
	inline String_t* get_szThornId_0() const { return ___szThornId_0; }
	inline String_t** get_address_of_szThornId_0() { return &___szThornId_0; }
	inline void set_szThornId_0(String_t* value)
	{
		___szThornId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szThornId_0), value);
	}

	inline static int32_t get_offset_of_fDensity_1() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___fDensity_1)); }
	inline float get_fDensity_1() const { return ___fDensity_1; }
	inline float* get_address_of_fDensity_1() { return &___fDensity_1; }
	inline void set_fDensity_1(float value)
	{
		___fDensity_1 = value;
	}

	inline static int32_t get_offset_of_fRebornTime_2() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___fRebornTime_2)); }
	inline float get_fRebornTime_2() const { return ___fRebornTime_2; }
	inline float* get_address_of_fRebornTime_2() { return &___fRebornTime_2; }
	inline void set_fRebornTime_2(float value)
	{
		___fRebornTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CClassEditor/sThornOfThisClassConfig
struct sThornOfThisClassConfig_t350464872_marshaled_pinvoke
{
	char* ___szThornId_0;
	float ___fDensity_1;
	float ___fRebornTime_2;
};
// Native definition for COM marshalling of CClassEditor/sThornOfThisClassConfig
struct sThornOfThisClassConfig_t350464872_marshaled_com
{
	Il2CppChar* ___szThornId_0;
	float ___fDensity_1;
	float ___fRebornTime_2;
};
#endif // STHORNOFTHISCLASSCONFIG_T350464872_H
#ifndef SMANNUALMONSTERCONFIG_T1844217021_H
#define SMANNUALMONSTERCONFIG_T1844217021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CClassEditor/sMannualMonsterConfig
struct  sMannualMonsterConfig_t1844217021 
{
public:
	// System.String CClassEditor/sMannualMonsterConfig::szMonsterId
	String_t* ___szMonsterId_0;
	// System.Single CClassEditor/sMannualMonsterConfig::fRebornTime
	float ___fRebornTime_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CClassEditor/sMannualMonsterConfig::lstPos
	List_1_t899420910 * ___lstPos_2;

public:
	inline static int32_t get_offset_of_szMonsterId_0() { return static_cast<int32_t>(offsetof(sMannualMonsterConfig_t1844217021, ___szMonsterId_0)); }
	inline String_t* get_szMonsterId_0() const { return ___szMonsterId_0; }
	inline String_t** get_address_of_szMonsterId_0() { return &___szMonsterId_0; }
	inline void set_szMonsterId_0(String_t* value)
	{
		___szMonsterId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szMonsterId_0), value);
	}

	inline static int32_t get_offset_of_fRebornTime_1() { return static_cast<int32_t>(offsetof(sMannualMonsterConfig_t1844217021, ___fRebornTime_1)); }
	inline float get_fRebornTime_1() const { return ___fRebornTime_1; }
	inline float* get_address_of_fRebornTime_1() { return &___fRebornTime_1; }
	inline void set_fRebornTime_1(float value)
	{
		___fRebornTime_1 = value;
	}

	inline static int32_t get_offset_of_lstPos_2() { return static_cast<int32_t>(offsetof(sMannualMonsterConfig_t1844217021, ___lstPos_2)); }
	inline List_1_t899420910 * get_lstPos_2() const { return ___lstPos_2; }
	inline List_1_t899420910 ** get_address_of_lstPos_2() { return &___lstPos_2; }
	inline void set_lstPos_2(List_1_t899420910 * value)
	{
		___lstPos_2 = value;
		Il2CppCodeGenWriteBarrier((&___lstPos_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CClassEditor/sMannualMonsterConfig
struct sMannualMonsterConfig_t1844217021_marshaled_pinvoke
{
	char* ___szMonsterId_0;
	float ___fRebornTime_1;
	List_1_t899420910 * ___lstPos_2;
};
// Native definition for COM marshalling of CClassEditor/sMannualMonsterConfig
struct sMannualMonsterConfig_t1844217021_marshaled_com
{
	Il2CppChar* ___szMonsterId_0;
	float ___fRebornTime_1;
	List_1_t899420910 * ___lstPos_2;
};
#endif // SMANNUALMONSTERCONFIG_T1844217021_H
#ifndef STHORNCONFIG_T1242444451_H
#define STHORNCONFIG_T1242444451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/sThornConfig
struct  sThornConfig_t1242444451 
{
public:
	// System.String CMonsterEditor/sThornConfig::szId
	String_t* ___szId_0;
	// System.Int32 CMonsterEditor/sThornConfig::nType
	int32_t ___nType_1;
	// System.Single CMonsterEditor/sThornConfig::fFoodSize
	float ___fFoodSize_2;
	// System.String CMonsterEditor/sThornConfig::szColor
	String_t* ___szColor_3;
	// System.String CMonsterEditor/sThornConfig::szDesc
	String_t* ___szDesc_4;
	// System.Single CMonsterEditor/sThornConfig::fSelfSize
	float ___fSelfSize_5;
	// System.Single CMonsterEditor/sThornConfig::fExp
	float ___fExp_6;
	// System.Single CMonsterEditor/sThornConfig::fMoney
	float ___fMoney_7;
	// System.Single CMonsterEditor/sThornConfig::fBuffId
	float ___fBuffId_8;
	// System.Single CMonsterEditor/sThornConfig::fExplodeChildNum
	float ___fExplodeChildNum_9;
	// System.Single CMonsterEditor/sThornConfig::fExplodeMotherLeftPercent
	float ___fExplodeMotherLeftPercent_10;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunDistance
	float ___fExplodeRunDistance_11;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunTime
	float ___fExplodeRunTime_12;
	// System.Single CMonsterEditor/sThornConfig::fExplodeStayTime
	float ___fExplodeStayTime_13;
	// System.Single CMonsterEditor/sThornConfig::fExplodeShellTime
	float ___fExplodeShellTime_14;
	// System.Single CMonsterEditor/sThornConfig::fExplodeFormationId
	float ___fExplodeFormationId_15;
	// System.Int32 CMonsterEditor/sThornConfig::nSkillId
	int32_t ___nSkillId_16;

public:
	inline static int32_t get_offset_of_szId_0() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szId_0)); }
	inline String_t* get_szId_0() const { return ___szId_0; }
	inline String_t** get_address_of_szId_0() { return &___szId_0; }
	inline void set_szId_0(String_t* value)
	{
		___szId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szId_0), value);
	}

	inline static int32_t get_offset_of_nType_1() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nType_1)); }
	inline int32_t get_nType_1() const { return ___nType_1; }
	inline int32_t* get_address_of_nType_1() { return &___nType_1; }
	inline void set_nType_1(int32_t value)
	{
		___nType_1 = value;
	}

	inline static int32_t get_offset_of_fFoodSize_2() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fFoodSize_2)); }
	inline float get_fFoodSize_2() const { return ___fFoodSize_2; }
	inline float* get_address_of_fFoodSize_2() { return &___fFoodSize_2; }
	inline void set_fFoodSize_2(float value)
	{
		___fFoodSize_2 = value;
	}

	inline static int32_t get_offset_of_szColor_3() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szColor_3)); }
	inline String_t* get_szColor_3() const { return ___szColor_3; }
	inline String_t** get_address_of_szColor_3() { return &___szColor_3; }
	inline void set_szColor_3(String_t* value)
	{
		___szColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___szColor_3), value);
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_fSelfSize_5() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fSelfSize_5)); }
	inline float get_fSelfSize_5() const { return ___fSelfSize_5; }
	inline float* get_address_of_fSelfSize_5() { return &___fSelfSize_5; }
	inline void set_fSelfSize_5(float value)
	{
		___fSelfSize_5 = value;
	}

	inline static int32_t get_offset_of_fExp_6() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExp_6)); }
	inline float get_fExp_6() const { return ___fExp_6; }
	inline float* get_address_of_fExp_6() { return &___fExp_6; }
	inline void set_fExp_6(float value)
	{
		___fExp_6 = value;
	}

	inline static int32_t get_offset_of_fMoney_7() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fMoney_7)); }
	inline float get_fMoney_7() const { return ___fMoney_7; }
	inline float* get_address_of_fMoney_7() { return &___fMoney_7; }
	inline void set_fMoney_7(float value)
	{
		___fMoney_7 = value;
	}

	inline static int32_t get_offset_of_fBuffId_8() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fBuffId_8)); }
	inline float get_fBuffId_8() const { return ___fBuffId_8; }
	inline float* get_address_of_fBuffId_8() { return &___fBuffId_8; }
	inline void set_fBuffId_8(float value)
	{
		___fBuffId_8 = value;
	}

	inline static int32_t get_offset_of_fExplodeChildNum_9() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeChildNum_9)); }
	inline float get_fExplodeChildNum_9() const { return ___fExplodeChildNum_9; }
	inline float* get_address_of_fExplodeChildNum_9() { return &___fExplodeChildNum_9; }
	inline void set_fExplodeChildNum_9(float value)
	{
		___fExplodeChildNum_9 = value;
	}

	inline static int32_t get_offset_of_fExplodeMotherLeftPercent_10() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeMotherLeftPercent_10)); }
	inline float get_fExplodeMotherLeftPercent_10() const { return ___fExplodeMotherLeftPercent_10; }
	inline float* get_address_of_fExplodeMotherLeftPercent_10() { return &___fExplodeMotherLeftPercent_10; }
	inline void set_fExplodeMotherLeftPercent_10(float value)
	{
		___fExplodeMotherLeftPercent_10 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunDistance_11() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunDistance_11)); }
	inline float get_fExplodeRunDistance_11() const { return ___fExplodeRunDistance_11; }
	inline float* get_address_of_fExplodeRunDistance_11() { return &___fExplodeRunDistance_11; }
	inline void set_fExplodeRunDistance_11(float value)
	{
		___fExplodeRunDistance_11 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunTime_12() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunTime_12)); }
	inline float get_fExplodeRunTime_12() const { return ___fExplodeRunTime_12; }
	inline float* get_address_of_fExplodeRunTime_12() { return &___fExplodeRunTime_12; }
	inline void set_fExplodeRunTime_12(float value)
	{
		___fExplodeRunTime_12 = value;
	}

	inline static int32_t get_offset_of_fExplodeStayTime_13() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeStayTime_13)); }
	inline float get_fExplodeStayTime_13() const { return ___fExplodeStayTime_13; }
	inline float* get_address_of_fExplodeStayTime_13() { return &___fExplodeStayTime_13; }
	inline void set_fExplodeStayTime_13(float value)
	{
		___fExplodeStayTime_13 = value;
	}

	inline static int32_t get_offset_of_fExplodeShellTime_14() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeShellTime_14)); }
	inline float get_fExplodeShellTime_14() const { return ___fExplodeShellTime_14; }
	inline float* get_address_of_fExplodeShellTime_14() { return &___fExplodeShellTime_14; }
	inline void set_fExplodeShellTime_14(float value)
	{
		___fExplodeShellTime_14 = value;
	}

	inline static int32_t get_offset_of_fExplodeFormationId_15() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeFormationId_15)); }
	inline float get_fExplodeFormationId_15() const { return ___fExplodeFormationId_15; }
	inline float* get_address_of_fExplodeFormationId_15() { return &___fExplodeFormationId_15; }
	inline void set_fExplodeFormationId_15(float value)
	{
		___fExplodeFormationId_15 = value;
	}

	inline static int32_t get_offset_of_nSkillId_16() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nSkillId_16)); }
	inline int32_t get_nSkillId_16() const { return ___nSkillId_16; }
	inline int32_t* get_address_of_nSkillId_16() { return &___nSkillId_16; }
	inline void set_nSkillId_16(int32_t value)
	{
		___nSkillId_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_pinvoke
{
	char* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	char* ___szColor_3;
	char* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
// Native definition for COM marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_com
{
	Il2CppChar* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	Il2CppChar* ___szColor_3;
	Il2CppChar* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
#endif // STHORNCONFIG_T1242444451_H
#ifndef SSKILLCONFIG_T1112470633_H
#define SSKILLCONFIG_T1112470633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/sSkillConfig
struct  sSkillConfig_t1112470633 
{
public:
	// System.Int32 CSkillSystem/sSkillConfig::nSkillId
	int32_t ___nSkillId_0;
	// System.String CSkillSystem/sSkillConfig::szName
	String_t* ___szName_1;
	// System.String CSkillSystem/sSkillConfig::szDesc
	String_t* ___szDesc_2;
	// System.Int32 CSkillSystem/sSkillConfig::m_nMaxPoint
	int32_t ___m_nMaxPoint_3;

public:
	inline static int32_t get_offset_of_nSkillId_0() { return static_cast<int32_t>(offsetof(sSkillConfig_t1112470633, ___nSkillId_0)); }
	inline int32_t get_nSkillId_0() const { return ___nSkillId_0; }
	inline int32_t* get_address_of_nSkillId_0() { return &___nSkillId_0; }
	inline void set_nSkillId_0(int32_t value)
	{
		___nSkillId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sSkillConfig_t1112470633, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_szDesc_2() { return static_cast<int32_t>(offsetof(sSkillConfig_t1112470633, ___szDesc_2)); }
	inline String_t* get_szDesc_2() const { return ___szDesc_2; }
	inline String_t** get_address_of_szDesc_2() { return &___szDesc_2; }
	inline void set_szDesc_2(String_t* value)
	{
		___szDesc_2 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_2), value);
	}

	inline static int32_t get_offset_of_m_nMaxPoint_3() { return static_cast<int32_t>(offsetof(sSkillConfig_t1112470633, ___m_nMaxPoint_3)); }
	inline int32_t get_m_nMaxPoint_3() const { return ___m_nMaxPoint_3; }
	inline int32_t* get_address_of_m_nMaxPoint_3() { return &___m_nMaxPoint_3; }
	inline void set_m_nMaxPoint_3(int32_t value)
	{
		___m_nMaxPoint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CSkillSystem/sSkillConfig
struct sSkillConfig_t1112470633_marshaled_pinvoke
{
	int32_t ___nSkillId_0;
	char* ___szName_1;
	char* ___szDesc_2;
	int32_t ___m_nMaxPoint_3;
};
// Native definition for COM marshalling of CSkillSystem/sSkillConfig
struct sSkillConfig_t1112470633_marshaled_com
{
	int32_t ___nSkillId_0;
	Il2CppChar* ___szName_1;
	Il2CppChar* ___szDesc_2;
	int32_t ___m_nMaxPoint_3;
};
#endif // SSKILLCONFIG_T1112470633_H
#ifndef EBALLINFO_T935743328_H
#define EBALLINFO_T935743328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CWarFog/eBallInfo
struct  eBallInfo_t935743328 
{
public:
	// System.Single CWarFog/eBallInfo::radius
	float ___radius_0;
	// System.Single CWarFog/eBallInfo::posx
	float ___posx_1;
	// System.Single CWarFog/eBallInfo::posy
	float ___posy_2;

public:
	inline static int32_t get_offset_of_radius_0() { return static_cast<int32_t>(offsetof(eBallInfo_t935743328, ___radius_0)); }
	inline float get_radius_0() const { return ___radius_0; }
	inline float* get_address_of_radius_0() { return &___radius_0; }
	inline void set_radius_0(float value)
	{
		___radius_0 = value;
	}

	inline static int32_t get_offset_of_posx_1() { return static_cast<int32_t>(offsetof(eBallInfo_t935743328, ___posx_1)); }
	inline float get_posx_1() const { return ___posx_1; }
	inline float* get_address_of_posx_1() { return &___posx_1; }
	inline void set_posx_1(float value)
	{
		___posx_1 = value;
	}

	inline static int32_t get_offset_of_posy_2() { return static_cast<int32_t>(offsetof(eBallInfo_t935743328, ___posy_2)); }
	inline float get_posy_2() const { return ___posy_2; }
	inline float* get_address_of_posy_2() { return &___posy_2; }
	inline void set_posy_2(float value)
	{
		___posy_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBALLINFO_T935743328_H
#ifndef ROTATETIMELINE_T860518009_H
#define ROTATETIMELINE_T860518009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateTimeline
struct  RotateTimeline_t860518009  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.RotateTimeline::boneIndex
	int32_t ___boneIndex_9;
	// System.Single[] Spine.RotateTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_10;

public:
	inline static int32_t get_offset_of_boneIndex_9() { return static_cast<int32_t>(offsetof(RotateTimeline_t860518009, ___boneIndex_9)); }
	inline int32_t get_boneIndex_9() const { return ___boneIndex_9; }
	inline int32_t* get_address_of_boneIndex_9() { return &___boneIndex_9; }
	inline void set_boneIndex_9(int32_t value)
	{
		___boneIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(RotateTimeline_t860518009, ___frames_10)); }
	inline SingleU5BU5D_t1444911251* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_t1444911251* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATETIMELINE_T860518009_H
#ifndef DEFORMTIMELINE_T3552075840_H
#define DEFORMTIMELINE_T3552075840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DeformTimeline
struct  DeformTimeline_t3552075840  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.DeformTimeline::slotIndex
	int32_t ___slotIndex_5;
	// System.Single[] Spine.DeformTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_6;
	// System.Single[][] Spine.DeformTimeline::frameVertices
	SingleU5BU5DU5BU5D_t3206712258* ___frameVertices_7;
	// Spine.VertexAttachment Spine.DeformTimeline::attachment
	VertexAttachment_t4074366829 * ___attachment_8;

public:
	inline static int32_t get_offset_of_slotIndex_5() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___slotIndex_5)); }
	inline int32_t get_slotIndex_5() const { return ___slotIndex_5; }
	inline int32_t* get_address_of_slotIndex_5() { return &___slotIndex_5; }
	inline void set_slotIndex_5(int32_t value)
	{
		___slotIndex_5 = value;
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___frames_6)); }
	inline SingleU5BU5D_t1444911251* get_frames_6() const { return ___frames_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(SingleU5BU5D_t1444911251* value)
	{
		___frames_6 = value;
		Il2CppCodeGenWriteBarrier((&___frames_6), value);
	}

	inline static int32_t get_offset_of_frameVertices_7() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___frameVertices_7)); }
	inline SingleU5BU5DU5BU5D_t3206712258* get_frameVertices_7() const { return ___frameVertices_7; }
	inline SingleU5BU5DU5BU5D_t3206712258** get_address_of_frameVertices_7() { return &___frameVertices_7; }
	inline void set_frameVertices_7(SingleU5BU5DU5BU5D_t3206712258* value)
	{
		___frameVertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameVertices_7), value);
	}

	inline static int32_t get_offset_of_attachment_8() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___attachment_8)); }
	inline VertexAttachment_t4074366829 * get_attachment_8() const { return ___attachment_8; }
	inline VertexAttachment_t4074366829 ** get_address_of_attachment_8() { return &___attachment_8; }
	inline void set_attachment_8(VertexAttachment_t4074366829 * value)
	{
		___attachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFORMTIMELINE_T3552075840_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef IKCONSTRAINTTIMELINE_T1094182104_H
#define IKCONSTRAINTTIMELINE_T1094182104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintTimeline
struct  IkConstraintTimeline_t1094182104  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.IkConstraintTimeline::ikConstraintIndex
	int32_t ___ikConstraintIndex_11;
	// System.Single[] Spine.IkConstraintTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_12;

public:
	inline static int32_t get_offset_of_ikConstraintIndex_11() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t1094182104, ___ikConstraintIndex_11)); }
	inline int32_t get_ikConstraintIndex_11() const { return ___ikConstraintIndex_11; }
	inline int32_t* get_address_of_ikConstraintIndex_11() { return &___ikConstraintIndex_11; }
	inline void set_ikConstraintIndex_11(int32_t value)
	{
		___ikConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t1094182104, ___frames_12)); }
	inline SingleU5BU5D_t1444911251* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t1444911251* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTTIMELINE_T1094182104_H
#ifndef TRANSFORMCONSTRAINTTIMELINE_T410042030_H
#define TRANSFORMCONSTRAINTTIMELINE_T410042030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintTimeline
struct  TransformConstraintTimeline_t410042030  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.TransformConstraintTimeline::transformConstraintIndex
	int32_t ___transformConstraintIndex_15;
	// System.Single[] Spine.TransformConstraintTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_16;

public:
	inline static int32_t get_offset_of_transformConstraintIndex_15() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t410042030, ___transformConstraintIndex_15)); }
	inline int32_t get_transformConstraintIndex_15() const { return ___transformConstraintIndex_15; }
	inline int32_t* get_address_of_transformConstraintIndex_15() { return &___transformConstraintIndex_15; }
	inline void set_transformConstraintIndex_15(int32_t value)
	{
		___transformConstraintIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t410042030, ___frames_16)); }
	inline SingleU5BU5D_t1444911251* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_t1444911251* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTTIMELINE_T410042030_H
#ifndef PATHCONSTRAINTPOSITIONTIMELINE_T1963796833_H
#define PATHCONSTRAINTPOSITIONTIMELINE_T1963796833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintPositionTimeline
struct  PathConstraintPositionTimeline_t1963796833  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.PathConstraintPositionTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_9;
	// System.Single[] Spine.PathConstraintPositionTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_10;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_9() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t1963796833, ___pathConstraintIndex_9)); }
	inline int32_t get_pathConstraintIndex_9() const { return ___pathConstraintIndex_9; }
	inline int32_t* get_address_of_pathConstraintIndex_9() { return &___pathConstraintIndex_9; }
	inline void set_pathConstraintIndex_9(int32_t value)
	{
		___pathConstraintIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t1963796833, ___frames_10)); }
	inline SingleU5BU5D_t1444911251* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_t1444911251* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTPOSITIONTIMELINE_T1963796833_H
#ifndef PATHCONSTRAINTMIXTIMELINE_T534890125_H
#define PATHCONSTRAINTMIXTIMELINE_T534890125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintMixTimeline
struct  PathConstraintMixTimeline_t534890125  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.PathConstraintMixTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_11;
	// System.Single[] Spine.PathConstraintMixTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_12;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_11() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t534890125, ___pathConstraintIndex_11)); }
	inline int32_t get_pathConstraintIndex_11() const { return ___pathConstraintIndex_11; }
	inline int32_t* get_address_of_pathConstraintIndex_11() { return &___pathConstraintIndex_11; }
	inline void set_pathConstraintIndex_11(int32_t value)
	{
		___pathConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t534890125, ___frames_12)); }
	inline SingleU5BU5D_t1444911251* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t1444911251* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTMIXTIMELINE_T534890125_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef SSPITSPOEREPARAM_T3807642459_H
#define SSPITSPOEREPARAM_T3807642459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/sSpitSpoereParam
struct  sSpitSpoereParam_t3807642459 
{
public:
	// System.Single CSkillSystem/sSpitSpoereParam::fRadiusMultiple
	float ___fRadiusMultiple_0;
	// System.Single CSkillSystem/sSpitSpoereParam::fCostMotherVolumePercent
	float ___fCostMotherVolumePercent_1;
	// System.Int32 CSkillSystem/sSpitSpoereParam::nNumPerSecond
	int32_t ___nNumPerSecond_2;
	// System.Single CSkillSystem/sSpitSpoereParam::fSpitInterval
	float ___fSpitInterval_3;
	// System.Single CSkillSystem/sSpitSpoereParam::fEjectSpeed
	float ___fEjectSpeed_4;
	// System.Single CSkillSystem/sSpitSpoereParam::fPushThornDis
	float ___fPushThornDis_5;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeMinVolume
	float ___fSporeMinVolume_6;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeMaxVolume
	float ___fSporeMaxVolume_7;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeMaxShowRadius
	float ___fSporeMaxShowRadius_8;
	// System.Single CSkillSystem/sSpitSpoereParam::fHuanTing
	float ___fHuanTing_9;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeA
	float ___fSporeA_10;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeX
	float ___fSporeX_11;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeB
	float ___fSporeB_12;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeC
	float ___fSporeC_13;

public:
	inline static int32_t get_offset_of_fRadiusMultiple_0() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fRadiusMultiple_0)); }
	inline float get_fRadiusMultiple_0() const { return ___fRadiusMultiple_0; }
	inline float* get_address_of_fRadiusMultiple_0() { return &___fRadiusMultiple_0; }
	inline void set_fRadiusMultiple_0(float value)
	{
		___fRadiusMultiple_0 = value;
	}

	inline static int32_t get_offset_of_fCostMotherVolumePercent_1() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fCostMotherVolumePercent_1)); }
	inline float get_fCostMotherVolumePercent_1() const { return ___fCostMotherVolumePercent_1; }
	inline float* get_address_of_fCostMotherVolumePercent_1() { return &___fCostMotherVolumePercent_1; }
	inline void set_fCostMotherVolumePercent_1(float value)
	{
		___fCostMotherVolumePercent_1 = value;
	}

	inline static int32_t get_offset_of_nNumPerSecond_2() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___nNumPerSecond_2)); }
	inline int32_t get_nNumPerSecond_2() const { return ___nNumPerSecond_2; }
	inline int32_t* get_address_of_nNumPerSecond_2() { return &___nNumPerSecond_2; }
	inline void set_nNumPerSecond_2(int32_t value)
	{
		___nNumPerSecond_2 = value;
	}

	inline static int32_t get_offset_of_fSpitInterval_3() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSpitInterval_3)); }
	inline float get_fSpitInterval_3() const { return ___fSpitInterval_3; }
	inline float* get_address_of_fSpitInterval_3() { return &___fSpitInterval_3; }
	inline void set_fSpitInterval_3(float value)
	{
		___fSpitInterval_3 = value;
	}

	inline static int32_t get_offset_of_fEjectSpeed_4() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fEjectSpeed_4)); }
	inline float get_fEjectSpeed_4() const { return ___fEjectSpeed_4; }
	inline float* get_address_of_fEjectSpeed_4() { return &___fEjectSpeed_4; }
	inline void set_fEjectSpeed_4(float value)
	{
		___fEjectSpeed_4 = value;
	}

	inline static int32_t get_offset_of_fPushThornDis_5() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fPushThornDis_5)); }
	inline float get_fPushThornDis_5() const { return ___fPushThornDis_5; }
	inline float* get_address_of_fPushThornDis_5() { return &___fPushThornDis_5; }
	inline void set_fPushThornDis_5(float value)
	{
		___fPushThornDis_5 = value;
	}

	inline static int32_t get_offset_of_fSporeMinVolume_6() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeMinVolume_6)); }
	inline float get_fSporeMinVolume_6() const { return ___fSporeMinVolume_6; }
	inline float* get_address_of_fSporeMinVolume_6() { return &___fSporeMinVolume_6; }
	inline void set_fSporeMinVolume_6(float value)
	{
		___fSporeMinVolume_6 = value;
	}

	inline static int32_t get_offset_of_fSporeMaxVolume_7() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeMaxVolume_7)); }
	inline float get_fSporeMaxVolume_7() const { return ___fSporeMaxVolume_7; }
	inline float* get_address_of_fSporeMaxVolume_7() { return &___fSporeMaxVolume_7; }
	inline void set_fSporeMaxVolume_7(float value)
	{
		___fSporeMaxVolume_7 = value;
	}

	inline static int32_t get_offset_of_fSporeMaxShowRadius_8() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeMaxShowRadius_8)); }
	inline float get_fSporeMaxShowRadius_8() const { return ___fSporeMaxShowRadius_8; }
	inline float* get_address_of_fSporeMaxShowRadius_8() { return &___fSporeMaxShowRadius_8; }
	inline void set_fSporeMaxShowRadius_8(float value)
	{
		___fSporeMaxShowRadius_8 = value;
	}

	inline static int32_t get_offset_of_fHuanTing_9() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fHuanTing_9)); }
	inline float get_fHuanTing_9() const { return ___fHuanTing_9; }
	inline float* get_address_of_fHuanTing_9() { return &___fHuanTing_9; }
	inline void set_fHuanTing_9(float value)
	{
		___fHuanTing_9 = value;
	}

	inline static int32_t get_offset_of_fSporeA_10() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeA_10)); }
	inline float get_fSporeA_10() const { return ___fSporeA_10; }
	inline float* get_address_of_fSporeA_10() { return &___fSporeA_10; }
	inline void set_fSporeA_10(float value)
	{
		___fSporeA_10 = value;
	}

	inline static int32_t get_offset_of_fSporeX_11() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeX_11)); }
	inline float get_fSporeX_11() const { return ___fSporeX_11; }
	inline float* get_address_of_fSporeX_11() { return &___fSporeX_11; }
	inline void set_fSporeX_11(float value)
	{
		___fSporeX_11 = value;
	}

	inline static int32_t get_offset_of_fSporeB_12() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeB_12)); }
	inline float get_fSporeB_12() const { return ___fSporeB_12; }
	inline float* get_address_of_fSporeB_12() { return &___fSporeB_12; }
	inline void set_fSporeB_12(float value)
	{
		___fSporeB_12 = value;
	}

	inline static int32_t get_offset_of_fSporeC_13() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeC_13)); }
	inline float get_fSporeC_13() const { return ___fSporeC_13; }
	inline float* get_address_of_fSporeC_13() { return &___fSporeC_13; }
	inline void set_fSporeC_13(float value)
	{
		___fSporeC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSPITSPOEREPARAM_T3807642459_H
#ifndef SSKILLINFO_T192725586_H
#define SSKILLINFO_T192725586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/sSkillInfo
struct  sSkillInfo_t192725586 
{
public:
	// System.Int32 CSkillSystem/sSkillInfo::nSkillId
	int32_t ___nSkillId_0;
	// System.Int32 CSkillSystem/sSkillInfo::nMaxPoint
	int32_t ___nMaxPoint_1;
	// System.Int32 CSkillSystem/sSkillInfo::nCurPoint
	int32_t ___nCurPoint_2;

public:
	inline static int32_t get_offset_of_nSkillId_0() { return static_cast<int32_t>(offsetof(sSkillInfo_t192725586, ___nSkillId_0)); }
	inline int32_t get_nSkillId_0() const { return ___nSkillId_0; }
	inline int32_t* get_address_of_nSkillId_0() { return &___nSkillId_0; }
	inline void set_nSkillId_0(int32_t value)
	{
		___nSkillId_0 = value;
	}

	inline static int32_t get_offset_of_nMaxPoint_1() { return static_cast<int32_t>(offsetof(sSkillInfo_t192725586, ___nMaxPoint_1)); }
	inline int32_t get_nMaxPoint_1() const { return ___nMaxPoint_1; }
	inline int32_t* get_address_of_nMaxPoint_1() { return &___nMaxPoint_1; }
	inline void set_nMaxPoint_1(int32_t value)
	{
		___nMaxPoint_1 = value;
	}

	inline static int32_t get_offset_of_nCurPoint_2() { return static_cast<int32_t>(offsetof(sSkillInfo_t192725586, ___nCurPoint_2)); }
	inline int32_t get_nCurPoint_2() const { return ___nCurPoint_2; }
	inline int32_t* get_address_of_nCurPoint_2() { return &___nCurPoint_2; }
	inline void set_nCurPoint_2(int32_t value)
	{
		___nCurPoint_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSKILLINFO_T192725586_H
#ifndef ERESOURCETYPE_T1087091141_H
#define ERESOURCETYPE_T1087091141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZiManager/eResourceType
struct  eResourceType_t1087091141 
{
public:
	// System.Int32 CTiaoZiManager/eResourceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eResourceType_t1087091141, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERESOURCETYPE_T1087091141_H
#ifndef PATHCONSTRAINTSPACINGTIMELINE_T1125597164_H
#define PATHCONSTRAINTSPACINGTIMELINE_T1125597164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintSpacingTimeline
struct  PathConstraintSpacingTimeline_t1125597164  : public PathConstraintPositionTimeline_t1963796833
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTSPACINGTIMELINE_T1125597164_H
#ifndef EJIESUANTYPE_T2754244357_H
#define EJIESUANTYPE_T2754244357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJIeSuanSystem/eJieSuanType
struct  eJieSuanType_t2754244357 
{
public:
	// System.Int32 CJIeSuanSystem/eJieSuanType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eJieSuanType_t2754244357, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EJIESUANTYPE_T2754244357_H
#ifndef SCALETIMELINE_T1810411048_H
#define SCALETIMELINE_T1810411048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ScaleTimeline
struct  ScaleTimeline_t1810411048  : public TranslateTimeline_t2733426737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALETIMELINE_T1810411048_H
#ifndef EBALLSPINETYPE_T3164819254_H
#define EBALLSPINETYPE_T3164819254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpineManager/eBallSpineType
struct  eBallSpineType_t3164819254 
{
public:
	// System.Int32 CSpineManager/eBallSpineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBallSpineType_t3164819254, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBALLSPINETYPE_T3164819254_H
#ifndef ETIAOZITYPE_T2622546907_H
#define ETIAOZITYPE_T2622546907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZiManager/eTiaoZiType
struct  eTiaoZiType_t2622546907 
{
public:
	// System.Int32 CTiaoZiManager/eTiaoZiType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eTiaoZiType_t2622546907, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETIAOZITYPE_T2622546907_H
#ifndef SHEARTIMELINE_T3812859450_H
#define SHEARTIMELINE_T3812859450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ShearTimeline
struct  ShearTimeline_t3812859450  : public TranslateTimeline_t2733426737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHEARTIMELINE_T3812859450_H
#ifndef STIAOZIQUEUEITEM_T3790450940_H
#define STIAOZIQUEUEITEM_T3790450940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZiManager/sTiaoZiQueueItem
struct  sTiaoZiQueueItem_t3790450940 
{
public:
	// UnityEngine.Vector3 CTiaoZiManager/sTiaoZiQueueItem::pos
	Vector3_t3722313464  ___pos_0;
	// System.Single CTiaoZiManager/sTiaoZiQueueItem::fScale
	float ___fScale_1;
	// UnityEngine.Color CTiaoZiManager/sTiaoZiQueueItem::Color
	Color_t2555686324  ___Color_2;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(sTiaoZiQueueItem_t3790450940, ___pos_0)); }
	inline Vector3_t3722313464  get_pos_0() const { return ___pos_0; }
	inline Vector3_t3722313464 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector3_t3722313464  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_fScale_1() { return static_cast<int32_t>(offsetof(sTiaoZiQueueItem_t3790450940, ___fScale_1)); }
	inline float get_fScale_1() const { return ___fScale_1; }
	inline float* get_address_of_fScale_1() { return &___fScale_1; }
	inline void set_fScale_1(float value)
	{
		___fScale_1 = value;
	}

	inline static int32_t get_offset_of_Color_2() { return static_cast<int32_t>(offsetof(sTiaoZiQueueItem_t3790450940, ___Color_2)); }
	inline Color_t2555686324  get_Color_2() const { return ___Color_2; }
	inline Color_t2555686324 * get_address_of_Color_2() { return &___Color_2; }
	inline void set_Color_2(Color_t2555686324  value)
	{
		___Color_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STIAOZIQUEUEITEM_T3790450940_H
#ifndef TIMELINETYPE_T3142998273_H
#define TIMELINETYPE_T3142998273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TimelineType
struct  TimelineType_t3142998273 
{
public:
	// System.Int32 Spine.TimelineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TimelineType_t3142998273, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINETYPE_T3142998273_H
#ifndef MIXDIRECTION_T3149175205_H
#define MIXDIRECTION_T3149175205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixDirection
struct  MixDirection_t3149175205 
{
public:
	// System.Int32 Spine.MixDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixDirection_t3149175205, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXDIRECTION_T3149175205_H
#ifndef MIXPOSE_T983230599_H
#define MIXPOSE_T983230599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixPose
struct  MixPose_t983230599 
{
public:
	// System.Int32 Spine.MixPose::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixPose_t983230599, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXPOSE_T983230599_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef EZIYUANTYPE_T1731312856_H
#define EZIYUANTYPE_T1731312856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZiManager/eZiYuanType
struct  eZiYuanType_t1731312856 
{
public:
	// System.Int32 CTiaoZiManager/eZiYuanType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eZiYuanType_t1731312856, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EZIYUANTYPE_T1731312856_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef EMONSTERBUILDINGTYPE_T3379255469_H
#define EMONSTERBUILDINGTYPE_T3379255469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/eMonsterBuildingType
struct  eMonsterBuildingType_t3379255469 
{
public:
	// System.Int32 CMonsterEditor/eMonsterBuildingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMonsterBuildingType_t3379255469, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONSTERBUILDINGTYPE_T3379255469_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef EBUTTONSTATUS_T48935231_H
#define EBUTTONSTATUS_T48935231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CButtonClick/eButtonStatus
struct  eButtonStatus_t48935231 
{
public:
	// System.Int32 CButtonClick/eButtonStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eButtonStatus_t48935231, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBUTTONSTATUS_T48935231_H
#ifndef ESKILLSYSPROGRESSBARTYPE_T1408520632_H
#define ESKILLSYSPROGRESSBARTYPE_T1408520632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/eSkillSysProgressBarType
struct  eSkillSysProgressBarType_t1408520632 
{
public:
	// System.Int32 CSkillSystem/eSkillSysProgressBarType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillSysProgressBarType_t1408520632, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLSYSPROGRESSBARTYPE_T1408520632_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SGENERATEBEANNODE_T810489850_H
#define SGENERATEBEANNODE_T810489850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/sGenerateBeanNode
struct  sGenerateBeanNode_t810489850 
{
public:
	// CMonsterEditor/sThornConfig CMonsterEditor/sGenerateBeanNode::thorn_buildin_config
	sThornConfig_t1242444451  ___thorn_buildin_config_0;
	// CClassEditor/sThornOfThisClassConfig CMonsterEditor/sGenerateBeanNode::thorn_in_class_config
	sThornOfThisClassConfig_t350464872  ___thorn_in_class_config_1;
	// System.String[] CMonsterEditor/sGenerateBeanNode::aryPos
	StringU5BU5D_t1281789340* ___aryPos_2;
	// System.Int32 CMonsterEditor/sGenerateBeanNode::nCurNum
	int32_t ___nCurNum_3;

public:
	inline static int32_t get_offset_of_thorn_buildin_config_0() { return static_cast<int32_t>(offsetof(sGenerateBeanNode_t810489850, ___thorn_buildin_config_0)); }
	inline sThornConfig_t1242444451  get_thorn_buildin_config_0() const { return ___thorn_buildin_config_0; }
	inline sThornConfig_t1242444451 * get_address_of_thorn_buildin_config_0() { return &___thorn_buildin_config_0; }
	inline void set_thorn_buildin_config_0(sThornConfig_t1242444451  value)
	{
		___thorn_buildin_config_0 = value;
	}

	inline static int32_t get_offset_of_thorn_in_class_config_1() { return static_cast<int32_t>(offsetof(sGenerateBeanNode_t810489850, ___thorn_in_class_config_1)); }
	inline sThornOfThisClassConfig_t350464872  get_thorn_in_class_config_1() const { return ___thorn_in_class_config_1; }
	inline sThornOfThisClassConfig_t350464872 * get_address_of_thorn_in_class_config_1() { return &___thorn_in_class_config_1; }
	inline void set_thorn_in_class_config_1(sThornOfThisClassConfig_t350464872  value)
	{
		___thorn_in_class_config_1 = value;
	}

	inline static int32_t get_offset_of_aryPos_2() { return static_cast<int32_t>(offsetof(sGenerateBeanNode_t810489850, ___aryPos_2)); }
	inline StringU5BU5D_t1281789340* get_aryPos_2() const { return ___aryPos_2; }
	inline StringU5BU5D_t1281789340** get_address_of_aryPos_2() { return &___aryPos_2; }
	inline void set_aryPos_2(StringU5BU5D_t1281789340* value)
	{
		___aryPos_2 = value;
		Il2CppCodeGenWriteBarrier((&___aryPos_2), value);
	}

	inline static int32_t get_offset_of_nCurNum_3() { return static_cast<int32_t>(offsetof(sGenerateBeanNode_t810489850, ___nCurNum_3)); }
	inline int32_t get_nCurNum_3() const { return ___nCurNum_3; }
	inline int32_t* get_address_of_nCurNum_3() { return &___nCurNum_3; }
	inline void set_nCurNum_3(int32_t value)
	{
		___nCurNum_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CMonsterEditor/sGenerateBeanNode
struct sGenerateBeanNode_t810489850_marshaled_pinvoke
{
	sThornConfig_t1242444451_marshaled_pinvoke ___thorn_buildin_config_0;
	sThornOfThisClassConfig_t350464872_marshaled_pinvoke ___thorn_in_class_config_1;
	char** ___aryPos_2;
	int32_t ___nCurNum_3;
};
// Native definition for COM marshalling of CMonsterEditor/sGenerateBeanNode
struct sGenerateBeanNode_t810489850_marshaled_com
{
	sThornConfig_t1242444451_marshaled_com ___thorn_buildin_config_0;
	sThornOfThisClassConfig_t350464872_marshaled_com ___thorn_in_class_config_1;
	Il2CppChar** ___aryPos_2;
	int32_t ___nCurNum_3;
};
#endif // SGENERATEBEANNODE_T810489850_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TRACKENTRYDELEGATE_T363257942_H
#define TRACKENTRYDELEGATE_T363257942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryDelegate
struct  TrackEntryDelegate_t363257942  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYDELEGATE_T363257942_H
#ifndef TRACKENTRYEVENTDELEGATE_T1653995044_H
#define TRACKENTRYEVENTDELEGATE_T1653995044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryEventDelegate
struct  TrackEntryEventDelegate_t1653995044  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYEVENTDELEGATE_T1653995044_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CSTARDUST_T2438358744_H
#define CSTARDUST_T2438358744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CStarDust
struct  CStarDust_t2438358744  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField CStarDust::_inputDustNum
	InputField_t3762917431 * ____inputDustNum_2;
	// UnityEngine.UI.InputField CStarDust::_inputDustMaxSize
	InputField_t3762917431 * ____inputDustMaxSize_3;
	// UnityEngine.UI.InputField CStarDust::_inputDustMinSize
	InputField_t3762917431 * ____inputDustMinSize_4;
	// UnityEngine.UI.InputField CStarDust::_inputDustBeltSize
	InputField_t3762917431 * ____inputDustBeltSize_5;
	// UnityEngine.UI.InputField CStarDust::_inputDustAngularVelocity
	InputField_t3762917431 * ____inputDustAngularVelocity_6;
	// UnityEngine.UI.InputField CStarDust::_inputDustBackToOrbitVelocity
	InputField_t3762917431 * ____inputDustBackToOrbitVelocity_7;
	// UnityEngine.UI.InputField CStarDust::_inputDustMass
	InputField_t3762917431 * ____inputDustMass_8;
	// UnityEngine.GameObject[] CStarDust::m_aryDustPrefabs
	GameObjectU5BU5D_t3328599146* ___m_aryDustPrefabs_9;
	// UnityEngine.Vector2 CStarDust::m_vecOrbitCenter
	Vector2_t2156229523  ___m_vecOrbitCenter_12;
	// System.Single CStarDust::m_fOrbitRaduis
	float ___m_fOrbitRaduis_13;
	// System.Single CStarDust::m_fAngularVElocity
	float ___m_fAngularVElocity_14;
	// System.Single CStarDust::m_fMoveToOrbitSpeed
	float ___m_fMoveToOrbitSpeed_15;
	// System.Single CStarDust::m_fAngularVElocityX
	float ___m_fAngularVElocityX_16;
	// System.Single CStarDust::m_fAngularVElocityY
	float ___m_fAngularVElocityY_17;
	// System.Collections.Generic.List`1<CDust> CStarDust::m_lstDust
	List_1_t198291272 * ___m_lstDust_19;
	// System.Xml.XmlNode CStarDust::m_xmlNode
	XmlNode_t3767805227 * ___m_xmlNode_20;
	// CDust CStarDust::m_ShitDust0
	CDust_t3021183826 * ___m_ShitDust0_21;
	// CDust CStarDust::m_ShitDust1
	CDust_t3021183826 * ___m_ShitDust1_22;
	// System.Single CStarDust::m_fMoveTimeCount
	float ___m_fMoveTimeCount_24;
	// System.Int32 CStarDust::m_nMovingIndex
	int32_t ___m_nMovingIndex_25;
	// System.Boolean CStarDust::m_bGenerated
	bool ___m_bGenerated_26;
	// System.Boolean CStarDust::m_bMapConfigLoaded
	bool ___m_bMapConfigLoaded_27;
	// System.Boolean CStarDust::m_bGameStartTimeSet
	bool ___m_bGameStartTimeSet_28;
	// System.Single CStarDust::m_fSycnTimeCount
	float ___m_fSycnTimeCount_30;
	// System.Int32 CStarDust::m_nShit
	int32_t ___m_nShit_31;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> CStarDust::m_dicBeingSyncedDust
	Dictionary_2_t285980105 * ___m_dicBeingSyncedDust_32;
	// System.Int32 CStarDust::m_nDustNum
	int32_t ___m_nDustNum_33;
	// System.Single CStarDust::m_fDustMaxSize
	float ___m_fDustMaxSize_34;
	// System.Single CStarDust::m_fDustMinSize
	float ___m_fDustMinSize_35;
	// System.Single CStarDust::m_fDustBeltSize
	float ___m_fDustBeltSize_36;
	// System.Single CStarDust::m_fDustAngularVelocity
	float ___m_fDustAngularVelocity_37;
	// System.Single CStarDust::m_fDustMass
	float ___m_fDustMass_38;
	// System.Single CStarDust::m_fDustBackToOrbitVelocity
	float ___m_fDustBackToOrbitVelocity_39;

public:
	inline static int32_t get_offset_of__inputDustNum_2() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustNum_2)); }
	inline InputField_t3762917431 * get__inputDustNum_2() const { return ____inputDustNum_2; }
	inline InputField_t3762917431 ** get_address_of__inputDustNum_2() { return &____inputDustNum_2; }
	inline void set__inputDustNum_2(InputField_t3762917431 * value)
	{
		____inputDustNum_2 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustNum_2), value);
	}

	inline static int32_t get_offset_of__inputDustMaxSize_3() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustMaxSize_3)); }
	inline InputField_t3762917431 * get__inputDustMaxSize_3() const { return ____inputDustMaxSize_3; }
	inline InputField_t3762917431 ** get_address_of__inputDustMaxSize_3() { return &____inputDustMaxSize_3; }
	inline void set__inputDustMaxSize_3(InputField_t3762917431 * value)
	{
		____inputDustMaxSize_3 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustMaxSize_3), value);
	}

	inline static int32_t get_offset_of__inputDustMinSize_4() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustMinSize_4)); }
	inline InputField_t3762917431 * get__inputDustMinSize_4() const { return ____inputDustMinSize_4; }
	inline InputField_t3762917431 ** get_address_of__inputDustMinSize_4() { return &____inputDustMinSize_4; }
	inline void set__inputDustMinSize_4(InputField_t3762917431 * value)
	{
		____inputDustMinSize_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustMinSize_4), value);
	}

	inline static int32_t get_offset_of__inputDustBeltSize_5() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustBeltSize_5)); }
	inline InputField_t3762917431 * get__inputDustBeltSize_5() const { return ____inputDustBeltSize_5; }
	inline InputField_t3762917431 ** get_address_of__inputDustBeltSize_5() { return &____inputDustBeltSize_5; }
	inline void set__inputDustBeltSize_5(InputField_t3762917431 * value)
	{
		____inputDustBeltSize_5 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustBeltSize_5), value);
	}

	inline static int32_t get_offset_of__inputDustAngularVelocity_6() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustAngularVelocity_6)); }
	inline InputField_t3762917431 * get__inputDustAngularVelocity_6() const { return ____inputDustAngularVelocity_6; }
	inline InputField_t3762917431 ** get_address_of__inputDustAngularVelocity_6() { return &____inputDustAngularVelocity_6; }
	inline void set__inputDustAngularVelocity_6(InputField_t3762917431 * value)
	{
		____inputDustAngularVelocity_6 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustAngularVelocity_6), value);
	}

	inline static int32_t get_offset_of__inputDustBackToOrbitVelocity_7() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustBackToOrbitVelocity_7)); }
	inline InputField_t3762917431 * get__inputDustBackToOrbitVelocity_7() const { return ____inputDustBackToOrbitVelocity_7; }
	inline InputField_t3762917431 ** get_address_of__inputDustBackToOrbitVelocity_7() { return &____inputDustBackToOrbitVelocity_7; }
	inline void set__inputDustBackToOrbitVelocity_7(InputField_t3762917431 * value)
	{
		____inputDustBackToOrbitVelocity_7 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustBackToOrbitVelocity_7), value);
	}

	inline static int32_t get_offset_of__inputDustMass_8() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ____inputDustMass_8)); }
	inline InputField_t3762917431 * get__inputDustMass_8() const { return ____inputDustMass_8; }
	inline InputField_t3762917431 ** get_address_of__inputDustMass_8() { return &____inputDustMass_8; }
	inline void set__inputDustMass_8(InputField_t3762917431 * value)
	{
		____inputDustMass_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputDustMass_8), value);
	}

	inline static int32_t get_offset_of_m_aryDustPrefabs_9() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_aryDustPrefabs_9)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryDustPrefabs_9() const { return ___m_aryDustPrefabs_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryDustPrefabs_9() { return &___m_aryDustPrefabs_9; }
	inline void set_m_aryDustPrefabs_9(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryDustPrefabs_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDustPrefabs_9), value);
	}

	inline static int32_t get_offset_of_m_vecOrbitCenter_12() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_vecOrbitCenter_12)); }
	inline Vector2_t2156229523  get_m_vecOrbitCenter_12() const { return ___m_vecOrbitCenter_12; }
	inline Vector2_t2156229523 * get_address_of_m_vecOrbitCenter_12() { return &___m_vecOrbitCenter_12; }
	inline void set_m_vecOrbitCenter_12(Vector2_t2156229523  value)
	{
		___m_vecOrbitCenter_12 = value;
	}

	inline static int32_t get_offset_of_m_fOrbitRaduis_13() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fOrbitRaduis_13)); }
	inline float get_m_fOrbitRaduis_13() const { return ___m_fOrbitRaduis_13; }
	inline float* get_address_of_m_fOrbitRaduis_13() { return &___m_fOrbitRaduis_13; }
	inline void set_m_fOrbitRaduis_13(float value)
	{
		___m_fOrbitRaduis_13 = value;
	}

	inline static int32_t get_offset_of_m_fAngularVElocity_14() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fAngularVElocity_14)); }
	inline float get_m_fAngularVElocity_14() const { return ___m_fAngularVElocity_14; }
	inline float* get_address_of_m_fAngularVElocity_14() { return &___m_fAngularVElocity_14; }
	inline void set_m_fAngularVElocity_14(float value)
	{
		___m_fAngularVElocity_14 = value;
	}

	inline static int32_t get_offset_of_m_fMoveToOrbitSpeed_15() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fMoveToOrbitSpeed_15)); }
	inline float get_m_fMoveToOrbitSpeed_15() const { return ___m_fMoveToOrbitSpeed_15; }
	inline float* get_address_of_m_fMoveToOrbitSpeed_15() { return &___m_fMoveToOrbitSpeed_15; }
	inline void set_m_fMoveToOrbitSpeed_15(float value)
	{
		___m_fMoveToOrbitSpeed_15 = value;
	}

	inline static int32_t get_offset_of_m_fAngularVElocityX_16() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fAngularVElocityX_16)); }
	inline float get_m_fAngularVElocityX_16() const { return ___m_fAngularVElocityX_16; }
	inline float* get_address_of_m_fAngularVElocityX_16() { return &___m_fAngularVElocityX_16; }
	inline void set_m_fAngularVElocityX_16(float value)
	{
		___m_fAngularVElocityX_16 = value;
	}

	inline static int32_t get_offset_of_m_fAngularVElocityY_17() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fAngularVElocityY_17)); }
	inline float get_m_fAngularVElocityY_17() const { return ___m_fAngularVElocityY_17; }
	inline float* get_address_of_m_fAngularVElocityY_17() { return &___m_fAngularVElocityY_17; }
	inline void set_m_fAngularVElocityY_17(float value)
	{
		___m_fAngularVElocityY_17 = value;
	}

	inline static int32_t get_offset_of_m_lstDust_19() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_lstDust_19)); }
	inline List_1_t198291272 * get_m_lstDust_19() const { return ___m_lstDust_19; }
	inline List_1_t198291272 ** get_address_of_m_lstDust_19() { return &___m_lstDust_19; }
	inline void set_m_lstDust_19(List_1_t198291272 * value)
	{
		___m_lstDust_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstDust_19), value);
	}

	inline static int32_t get_offset_of_m_xmlNode_20() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_xmlNode_20)); }
	inline XmlNode_t3767805227 * get_m_xmlNode_20() const { return ___m_xmlNode_20; }
	inline XmlNode_t3767805227 ** get_address_of_m_xmlNode_20() { return &___m_xmlNode_20; }
	inline void set_m_xmlNode_20(XmlNode_t3767805227 * value)
	{
		___m_xmlNode_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlNode_20), value);
	}

	inline static int32_t get_offset_of_m_ShitDust0_21() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_ShitDust0_21)); }
	inline CDust_t3021183826 * get_m_ShitDust0_21() const { return ___m_ShitDust0_21; }
	inline CDust_t3021183826 ** get_address_of_m_ShitDust0_21() { return &___m_ShitDust0_21; }
	inline void set_m_ShitDust0_21(CDust_t3021183826 * value)
	{
		___m_ShitDust0_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ShitDust0_21), value);
	}

	inline static int32_t get_offset_of_m_ShitDust1_22() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_ShitDust1_22)); }
	inline CDust_t3021183826 * get_m_ShitDust1_22() const { return ___m_ShitDust1_22; }
	inline CDust_t3021183826 ** get_address_of_m_ShitDust1_22() { return &___m_ShitDust1_22; }
	inline void set_m_ShitDust1_22(CDust_t3021183826 * value)
	{
		___m_ShitDust1_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ShitDust1_22), value);
	}

	inline static int32_t get_offset_of_m_fMoveTimeCount_24() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fMoveTimeCount_24)); }
	inline float get_m_fMoveTimeCount_24() const { return ___m_fMoveTimeCount_24; }
	inline float* get_address_of_m_fMoveTimeCount_24() { return &___m_fMoveTimeCount_24; }
	inline void set_m_fMoveTimeCount_24(float value)
	{
		___m_fMoveTimeCount_24 = value;
	}

	inline static int32_t get_offset_of_m_nMovingIndex_25() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_nMovingIndex_25)); }
	inline int32_t get_m_nMovingIndex_25() const { return ___m_nMovingIndex_25; }
	inline int32_t* get_address_of_m_nMovingIndex_25() { return &___m_nMovingIndex_25; }
	inline void set_m_nMovingIndex_25(int32_t value)
	{
		___m_nMovingIndex_25 = value;
	}

	inline static int32_t get_offset_of_m_bGenerated_26() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_bGenerated_26)); }
	inline bool get_m_bGenerated_26() const { return ___m_bGenerated_26; }
	inline bool* get_address_of_m_bGenerated_26() { return &___m_bGenerated_26; }
	inline void set_m_bGenerated_26(bool value)
	{
		___m_bGenerated_26 = value;
	}

	inline static int32_t get_offset_of_m_bMapConfigLoaded_27() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_bMapConfigLoaded_27)); }
	inline bool get_m_bMapConfigLoaded_27() const { return ___m_bMapConfigLoaded_27; }
	inline bool* get_address_of_m_bMapConfigLoaded_27() { return &___m_bMapConfigLoaded_27; }
	inline void set_m_bMapConfigLoaded_27(bool value)
	{
		___m_bMapConfigLoaded_27 = value;
	}

	inline static int32_t get_offset_of_m_bGameStartTimeSet_28() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_bGameStartTimeSet_28)); }
	inline bool get_m_bGameStartTimeSet_28() const { return ___m_bGameStartTimeSet_28; }
	inline bool* get_address_of_m_bGameStartTimeSet_28() { return &___m_bGameStartTimeSet_28; }
	inline void set_m_bGameStartTimeSet_28(bool value)
	{
		___m_bGameStartTimeSet_28 = value;
	}

	inline static int32_t get_offset_of_m_fSycnTimeCount_30() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fSycnTimeCount_30)); }
	inline float get_m_fSycnTimeCount_30() const { return ___m_fSycnTimeCount_30; }
	inline float* get_address_of_m_fSycnTimeCount_30() { return &___m_fSycnTimeCount_30; }
	inline void set_m_fSycnTimeCount_30(float value)
	{
		___m_fSycnTimeCount_30 = value;
	}

	inline static int32_t get_offset_of_m_nShit_31() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_nShit_31)); }
	inline int32_t get_m_nShit_31() const { return ___m_nShit_31; }
	inline int32_t* get_address_of_m_nShit_31() { return &___m_nShit_31; }
	inline void set_m_nShit_31(int32_t value)
	{
		___m_nShit_31 = value;
	}

	inline static int32_t get_offset_of_m_dicBeingSyncedDust_32() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_dicBeingSyncedDust_32)); }
	inline Dictionary_2_t285980105 * get_m_dicBeingSyncedDust_32() const { return ___m_dicBeingSyncedDust_32; }
	inline Dictionary_2_t285980105 ** get_address_of_m_dicBeingSyncedDust_32() { return &___m_dicBeingSyncedDust_32; }
	inline void set_m_dicBeingSyncedDust_32(Dictionary_2_t285980105 * value)
	{
		___m_dicBeingSyncedDust_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBeingSyncedDust_32), value);
	}

	inline static int32_t get_offset_of_m_nDustNum_33() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_nDustNum_33)); }
	inline int32_t get_m_nDustNum_33() const { return ___m_nDustNum_33; }
	inline int32_t* get_address_of_m_nDustNum_33() { return &___m_nDustNum_33; }
	inline void set_m_nDustNum_33(int32_t value)
	{
		___m_nDustNum_33 = value;
	}

	inline static int32_t get_offset_of_m_fDustMaxSize_34() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fDustMaxSize_34)); }
	inline float get_m_fDustMaxSize_34() const { return ___m_fDustMaxSize_34; }
	inline float* get_address_of_m_fDustMaxSize_34() { return &___m_fDustMaxSize_34; }
	inline void set_m_fDustMaxSize_34(float value)
	{
		___m_fDustMaxSize_34 = value;
	}

	inline static int32_t get_offset_of_m_fDustMinSize_35() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fDustMinSize_35)); }
	inline float get_m_fDustMinSize_35() const { return ___m_fDustMinSize_35; }
	inline float* get_address_of_m_fDustMinSize_35() { return &___m_fDustMinSize_35; }
	inline void set_m_fDustMinSize_35(float value)
	{
		___m_fDustMinSize_35 = value;
	}

	inline static int32_t get_offset_of_m_fDustBeltSize_36() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fDustBeltSize_36)); }
	inline float get_m_fDustBeltSize_36() const { return ___m_fDustBeltSize_36; }
	inline float* get_address_of_m_fDustBeltSize_36() { return &___m_fDustBeltSize_36; }
	inline void set_m_fDustBeltSize_36(float value)
	{
		___m_fDustBeltSize_36 = value;
	}

	inline static int32_t get_offset_of_m_fDustAngularVelocity_37() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fDustAngularVelocity_37)); }
	inline float get_m_fDustAngularVelocity_37() const { return ___m_fDustAngularVelocity_37; }
	inline float* get_address_of_m_fDustAngularVelocity_37() { return &___m_fDustAngularVelocity_37; }
	inline void set_m_fDustAngularVelocity_37(float value)
	{
		___m_fDustAngularVelocity_37 = value;
	}

	inline static int32_t get_offset_of_m_fDustMass_38() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fDustMass_38)); }
	inline float get_m_fDustMass_38() const { return ___m_fDustMass_38; }
	inline float* get_address_of_m_fDustMass_38() { return &___m_fDustMass_38; }
	inline void set_m_fDustMass_38(float value)
	{
		___m_fDustMass_38 = value;
	}

	inline static int32_t get_offset_of_m_fDustBackToOrbitVelocity_39() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744, ___m_fDustBackToOrbitVelocity_39)); }
	inline float get_m_fDustBackToOrbitVelocity_39() const { return ___m_fDustBackToOrbitVelocity_39; }
	inline float* get_address_of_m_fDustBackToOrbitVelocity_39() { return &___m_fDustBackToOrbitVelocity_39; }
	inline void set_m_fDustBackToOrbitVelocity_39(float value)
	{
		___m_fDustBackToOrbitVelocity_39 = value;
	}
};

struct CStarDust_t2438358744_StaticFields
{
public:
	// UnityEngine.Vector3 CStarDust::vecTempPos
	Vector3_t3722313464  ___vecTempPos_10;
	// UnityEngine.Vector2 CStarDust::vec2TempPos
	Vector2_t2156229523  ___vec2TempPos_11;
	// CStarDust CStarDust::s_Instance
	CStarDust_t2438358744 * ___s_Instance_18;
	// System.Byte[] CStarDust::_bytesDustFullInfo
	ByteU5BU5D_t4116647657* ____bytesDustFullInfo_29;

public:
	inline static int32_t get_offset_of_vecTempPos_10() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744_StaticFields, ___vecTempPos_10)); }
	inline Vector3_t3722313464  get_vecTempPos_10() const { return ___vecTempPos_10; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_10() { return &___vecTempPos_10; }
	inline void set_vecTempPos_10(Vector3_t3722313464  value)
	{
		___vecTempPos_10 = value;
	}

	inline static int32_t get_offset_of_vec2TempPos_11() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744_StaticFields, ___vec2TempPos_11)); }
	inline Vector2_t2156229523  get_vec2TempPos_11() const { return ___vec2TempPos_11; }
	inline Vector2_t2156229523 * get_address_of_vec2TempPos_11() { return &___vec2TempPos_11; }
	inline void set_vec2TempPos_11(Vector2_t2156229523  value)
	{
		___vec2TempPos_11 = value;
	}

	inline static int32_t get_offset_of_s_Instance_18() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744_StaticFields, ___s_Instance_18)); }
	inline CStarDust_t2438358744 * get_s_Instance_18() const { return ___s_Instance_18; }
	inline CStarDust_t2438358744 ** get_address_of_s_Instance_18() { return &___s_Instance_18; }
	inline void set_s_Instance_18(CStarDust_t2438358744 * value)
	{
		___s_Instance_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_18), value);
	}

	inline static int32_t get_offset_of__bytesDustFullInfo_29() { return static_cast<int32_t>(offsetof(CStarDust_t2438358744_StaticFields, ____bytesDustFullInfo_29)); }
	inline ByteU5BU5D_t4116647657* get__bytesDustFullInfo_29() const { return ____bytesDustFullInfo_29; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesDustFullInfo_29() { return &____bytesDustFullInfo_29; }
	inline void set__bytesDustFullInfo_29(ByteU5BU5D_t4116647657* value)
	{
		____bytesDustFullInfo_29 = value;
		Il2CppCodeGenWriteBarrier((&____bytesDustFullInfo_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSTARDUST_T2438358744_H
#ifndef CSPINEMANAGER_T1663211032_H
#define CSPINEMANAGER_T1663211032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpineManager
struct  CSpineManager_t1663211032  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CSpineManager_t1663211032_StaticFields
{
public:
	// UnityEngine.Quaternion CSpineManager::vecTempRotation
	Quaternion_t2301928331  ___vecTempRotation_2;
	// UnityEngine.Vector3 CSpineManager::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_3;
	// CSpineManager CSpineManager::s_Instance
	CSpineManager_t1663211032 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_vecTempRotation_2() { return static_cast<int32_t>(offsetof(CSpineManager_t1663211032_StaticFields, ___vecTempRotation_2)); }
	inline Quaternion_t2301928331  get_vecTempRotation_2() const { return ___vecTempRotation_2; }
	inline Quaternion_t2301928331 * get_address_of_vecTempRotation_2() { return &___vecTempRotation_2; }
	inline void set_vecTempRotation_2(Quaternion_t2301928331  value)
	{
		___vecTempRotation_2 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_3() { return static_cast<int32_t>(offsetof(CSpineManager_t1663211032_StaticFields, ___vecTempPos1_3)); }
	inline Vector3_t3722313464  get_vecTempPos1_3() const { return ___vecTempPos1_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_3() { return &___vecTempPos1_3; }
	inline void set_vecTempPos1_3(Vector3_t3722313464  value)
	{
		___vecTempPos1_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CSpineManager_t1663211032_StaticFields, ___s_Instance_4)); }
	inline CSpineManager_t1663211032 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CSpineManager_t1663211032 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CSpineManager_t1663211032 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPINEMANAGER_T1663211032_H
#ifndef CCOSMOSSKELETON_T2697667825_H
#define CCOSMOSSKELETON_T2697667825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCosmosSkeleton
struct  CCosmosSkeleton_t2697667825  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonGraphic CCosmosSkeleton::_skeletonGraphic
	SkeletonGraphic_t1744877482 * ____skeletonGraphic_2;

public:
	inline static int32_t get_offset_of__skeletonGraphic_2() { return static_cast<int32_t>(offsetof(CCosmosSkeleton_t2697667825, ____skeletonGraphic_2)); }
	inline SkeletonGraphic_t1744877482 * get__skeletonGraphic_2() const { return ____skeletonGraphic_2; }
	inline SkeletonGraphic_t1744877482 ** get_address_of__skeletonGraphic_2() { return &____skeletonGraphic_2; }
	inline void set__skeletonGraphic_2(SkeletonGraphic_t1744877482 * value)
	{
		____skeletonGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonGraphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOSMOSSKELETON_T2697667825_H
#ifndef CUISKILL_T4025687907_H
#define CUISKILL_T4025687907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CUISkill
struct  CUISkill_t4025687907  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CUISkill::_txtNum
	Text_t1901882714 * ____txtNum_2;
	// UnityEngine.UI.Button CUISkill::_btnAddPoint
	Button_t4055032469 * ____btnAddPoint_3;
	// System.Int32 CUISkill::m_nSkillId
	int32_t ___m_nSkillId_4;
	// System.Int32 CUISkill::m_nCurPoint
	int32_t ___m_nCurPoint_5;
	// System.Int32 CUISkill::m_nMaxPoint
	int32_t ___m_nMaxPoint_6;

public:
	inline static int32_t get_offset_of__txtNum_2() { return static_cast<int32_t>(offsetof(CUISkill_t4025687907, ____txtNum_2)); }
	inline Text_t1901882714 * get__txtNum_2() const { return ____txtNum_2; }
	inline Text_t1901882714 ** get_address_of__txtNum_2() { return &____txtNum_2; }
	inline void set__txtNum_2(Text_t1901882714 * value)
	{
		____txtNum_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_2), value);
	}

	inline static int32_t get_offset_of__btnAddPoint_3() { return static_cast<int32_t>(offsetof(CUISkill_t4025687907, ____btnAddPoint_3)); }
	inline Button_t4055032469 * get__btnAddPoint_3() const { return ____btnAddPoint_3; }
	inline Button_t4055032469 ** get_address_of__btnAddPoint_3() { return &____btnAddPoint_3; }
	inline void set__btnAddPoint_3(Button_t4055032469 * value)
	{
		____btnAddPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&____btnAddPoint_3), value);
	}

	inline static int32_t get_offset_of_m_nSkillId_4() { return static_cast<int32_t>(offsetof(CUISkill_t4025687907, ___m_nSkillId_4)); }
	inline int32_t get_m_nSkillId_4() const { return ___m_nSkillId_4; }
	inline int32_t* get_address_of_m_nSkillId_4() { return &___m_nSkillId_4; }
	inline void set_m_nSkillId_4(int32_t value)
	{
		___m_nSkillId_4 = value;
	}

	inline static int32_t get_offset_of_m_nCurPoint_5() { return static_cast<int32_t>(offsetof(CUISkill_t4025687907, ___m_nCurPoint_5)); }
	inline int32_t get_m_nCurPoint_5() const { return ___m_nCurPoint_5; }
	inline int32_t* get_address_of_m_nCurPoint_5() { return &___m_nCurPoint_5; }
	inline void set_m_nCurPoint_5(int32_t value)
	{
		___m_nCurPoint_5 = value;
	}

	inline static int32_t get_offset_of_m_nMaxPoint_6() { return static_cast<int32_t>(offsetof(CUISkill_t4025687907, ___m_nMaxPoint_6)); }
	inline int32_t get_m_nMaxPoint_6() const { return ___m_nMaxPoint_6; }
	inline int32_t* get_address_of_m_nMaxPoint_6() { return &___m_nMaxPoint_6; }
	inline void set_m_nMaxPoint_6(int32_t value)
	{
		___m_nMaxPoint_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUISKILL_T4025687907_H
#ifndef CSPOREMANAGER_T3848038884_H
#define CSPOREMANAGER_T3848038884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSporeManager
struct  CSporeManager_t3848038884  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<CSpore> CSporeManager::m_lstRecycledSporeZ
	List_1_t1482975291 * ___m_lstRecycledSporeZ_3;
	// UnityEngine.Sprite[] CSporeManager::m_arySporeSkins
	SpriteU5BU5D_t2581906349* ___m_arySporeSkins_4;
	// System.Int32 CSporeManager::m_nCurSelectedSporeSkinId
	int32_t ___m_nCurSelectedSporeSkinId_5;
	// UnityEngine.Sprite CSporeManager::m_sprDefaultSporeSkin
	Sprite_t280657092 * ___m_sprDefaultSporeSkin_6;
	// System.Collections.Generic.Dictionary`2<System.String,CSpore> CSporeManager::m_dicSpores
	Dictionary_2_t4091124144 * ___m_dicSpores_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CSporeManager::m_dicAlreadyDestroyed
	Dictionary_2_t2736202052 * ___m_dicAlreadyDestroyed_8;

public:
	inline static int32_t get_offset_of_m_lstRecycledSporeZ_3() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884, ___m_lstRecycledSporeZ_3)); }
	inline List_1_t1482975291 * get_m_lstRecycledSporeZ_3() const { return ___m_lstRecycledSporeZ_3; }
	inline List_1_t1482975291 ** get_address_of_m_lstRecycledSporeZ_3() { return &___m_lstRecycledSporeZ_3; }
	inline void set_m_lstRecycledSporeZ_3(List_1_t1482975291 * value)
	{
		___m_lstRecycledSporeZ_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledSporeZ_3), value);
	}

	inline static int32_t get_offset_of_m_arySporeSkins_4() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884, ___m_arySporeSkins_4)); }
	inline SpriteU5BU5D_t2581906349* get_m_arySporeSkins_4() const { return ___m_arySporeSkins_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_arySporeSkins_4() { return &___m_arySporeSkins_4; }
	inline void set_m_arySporeSkins_4(SpriteU5BU5D_t2581906349* value)
	{
		___m_arySporeSkins_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySporeSkins_4), value);
	}

	inline static int32_t get_offset_of_m_nCurSelectedSporeSkinId_5() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884, ___m_nCurSelectedSporeSkinId_5)); }
	inline int32_t get_m_nCurSelectedSporeSkinId_5() const { return ___m_nCurSelectedSporeSkinId_5; }
	inline int32_t* get_address_of_m_nCurSelectedSporeSkinId_5() { return &___m_nCurSelectedSporeSkinId_5; }
	inline void set_m_nCurSelectedSporeSkinId_5(int32_t value)
	{
		___m_nCurSelectedSporeSkinId_5 = value;
	}

	inline static int32_t get_offset_of_m_sprDefaultSporeSkin_6() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884, ___m_sprDefaultSporeSkin_6)); }
	inline Sprite_t280657092 * get_m_sprDefaultSporeSkin_6() const { return ___m_sprDefaultSporeSkin_6; }
	inline Sprite_t280657092 ** get_address_of_m_sprDefaultSporeSkin_6() { return &___m_sprDefaultSporeSkin_6; }
	inline void set_m_sprDefaultSporeSkin_6(Sprite_t280657092 * value)
	{
		___m_sprDefaultSporeSkin_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprDefaultSporeSkin_6), value);
	}

	inline static int32_t get_offset_of_m_dicSpores_7() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884, ___m_dicSpores_7)); }
	inline Dictionary_2_t4091124144 * get_m_dicSpores_7() const { return ___m_dicSpores_7; }
	inline Dictionary_2_t4091124144 ** get_address_of_m_dicSpores_7() { return &___m_dicSpores_7; }
	inline void set_m_dicSpores_7(Dictionary_2_t4091124144 * value)
	{
		___m_dicSpores_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSpores_7), value);
	}

	inline static int32_t get_offset_of_m_dicAlreadyDestroyed_8() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884, ___m_dicAlreadyDestroyed_8)); }
	inline Dictionary_2_t2736202052 * get_m_dicAlreadyDestroyed_8() const { return ___m_dicAlreadyDestroyed_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_m_dicAlreadyDestroyed_8() { return &___m_dicAlreadyDestroyed_8; }
	inline void set_m_dicAlreadyDestroyed_8(Dictionary_2_t2736202052 * value)
	{
		___m_dicAlreadyDestroyed_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAlreadyDestroyed_8), value);
	}
};

struct CSporeManager_t3848038884_StaticFields
{
public:
	// CSporeManager CSporeManager::s_Instance
	CSporeManager_t3848038884 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CSporeManager_t3848038884_StaticFields, ___s_Instance_2)); }
	inline CSporeManager_t3848038884 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CSporeManager_t3848038884 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CSporeManager_t3848038884 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPOREMANAGER_T3848038884_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef CBOWENGROUP_T4269360863_H
#define CBOWENGROUP_T4269360863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBoWenGroup
struct  CBoWenGroup_t4269360863  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CBoWenGroup_t4269360863_StaticFields
{
public:
	// UnityEngine.Vector3 CBoWenGroup::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CBoWenGroup::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CBoWenGroup_t4269360863_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CBoWenGroup_t4269360863_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBOWENGROUP_T4269360863_H
#ifndef CBOWENGRID_T1563892939_H
#define CBOWENGRID_T1563892939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBoWenGrid
struct  CBoWenGrid_t1563892939  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CBoWenGrid_t1563892939_StaticFields
{
public:
	// UnityEngine.Vector3 CBoWenGrid::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CBoWenGrid::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CBoWenGrid_t1563892939_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CBoWenGrid_t1563892939_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBOWENGRID_T1563892939_H
#ifndef CLINE_T1113746895_H
#define CLINE_T1113746895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLine
struct  CLine_t1113746895  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LineRenderer CLine::_lrMain
	LineRenderer_t3154350270 * ____lrMain_2;

public:
	inline static int32_t get_offset_of__lrMain_2() { return static_cast<int32_t>(offsetof(CLine_t1113746895, ____lrMain_2)); }
	inline LineRenderer_t3154350270 * get__lrMain_2() const { return ____lrMain_2; }
	inline LineRenderer_t3154350270 ** get_address_of__lrMain_2() { return &____lrMain_2; }
	inline void set__lrMain_2(LineRenderer_t3154350270 * value)
	{
		____lrMain_2 = value;
		Il2CppCodeGenWriteBarrier((&____lrMain_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLINE_T1113746895_H
#ifndef CLINEGROUP_T1725554608_H
#define CLINEGROUP_T1725554608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLineGroup
struct  CLineGroup_t1725554608  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer CLineGroup::_srMain
	SpriteRenderer_t3235626157 * ____srMain_3;

public:
	inline static int32_t get_offset_of__srMain_3() { return static_cast<int32_t>(offsetof(CLineGroup_t1725554608, ____srMain_3)); }
	inline SpriteRenderer_t3235626157 * get__srMain_3() const { return ____srMain_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_3() { return &____srMain_3; }
	inline void set__srMain_3(SpriteRenderer_t3235626157 * value)
	{
		____srMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_3), value);
	}
};

struct CLineGroup_t1725554608_StaticFields
{
public:
	// UnityEngine.Vector3 CLineGroup::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CLineGroup_t1725554608_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLINEGROUP_T1725554608_H
#ifndef CBOWEN_T1312958833_H
#define CBOWEN_T1312958833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBoWen
struct  CBoWen_t1312958833  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer CBoWen::_srMain
	SpriteRenderer_t3235626157 * ____srMain_3;

public:
	inline static int32_t get_offset_of__srMain_3() { return static_cast<int32_t>(offsetof(CBoWen_t1312958833, ____srMain_3)); }
	inline SpriteRenderer_t3235626157 * get__srMain_3() const { return ____srMain_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_3() { return &____srMain_3; }
	inline void set__srMain_3(SpriteRenderer_t3235626157 * value)
	{
		____srMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_3), value);
	}
};

struct CBoWen_t1312958833_StaticFields
{
public:
	// UnityEngine.Vector3 CBoWen::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CBoWen_t1312958833_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBOWEN_T1312958833_H
#ifndef CSTRIPEMANAGER_T2410268600_H
#define CSTRIPEMANAGER_T2410268600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CStripeManager
struct  CStripeManager_t2410268600  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CStripeManager::m_preBoWenGroup
	GameObject_t1113636619 * ___m_preBoWenGroup_8;
	// UnityEngine.GameObject CStripeManager::m_preBoWenGrid
	GameObject_t1113636619 * ___m_preBoWenGrid_9;
	// UnityEngine.GameObject CStripeManager::m_preBoWen
	GameObject_t1113636619 * ___m_preBoWen_10;
	// System.Collections.Generic.List`1<CBoWenGrid> CStripeManager::m_lstGroups
	List_1_t3035967681 * ___m_lstGroups_11;
	// UnityEngine.Color CStripeManager::m_colorBoWen
	Color_t2555686324  ___m_colorBoWen_12;
	// System.Single CStripeManager::m_fCurGridScale
	float ___m_fCurGridScale_13;
	// System.Single CStripeManager::m_fGridCam2ScaleXiShu
	float ___m_fGridCam2ScaleXiShu_14;
	// System.Single CStripeManager::m_fCurCamSize
	float ___m_fCurCamSize_15;
	// UnityEngine.Vector3 CStripeManager::m_vecOriginPoints
	Vector3_t3722313464  ___m_vecOriginPoints_16;
	// System.Single[] CStripeManager::m_aryThreshold
	SingleU5BU5D_t1444911251* ___m_aryThreshold_17;
	// System.Single CStripeManager::m_fLastCamSize
	float ___m_fLastCamSize_18;

public:
	inline static int32_t get_offset_of_m_preBoWenGroup_8() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_preBoWenGroup_8)); }
	inline GameObject_t1113636619 * get_m_preBoWenGroup_8() const { return ___m_preBoWenGroup_8; }
	inline GameObject_t1113636619 ** get_address_of_m_preBoWenGroup_8() { return &___m_preBoWenGroup_8; }
	inline void set_m_preBoWenGroup_8(GameObject_t1113636619 * value)
	{
		___m_preBoWenGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBoWenGroup_8), value);
	}

	inline static int32_t get_offset_of_m_preBoWenGrid_9() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_preBoWenGrid_9)); }
	inline GameObject_t1113636619 * get_m_preBoWenGrid_9() const { return ___m_preBoWenGrid_9; }
	inline GameObject_t1113636619 ** get_address_of_m_preBoWenGrid_9() { return &___m_preBoWenGrid_9; }
	inline void set_m_preBoWenGrid_9(GameObject_t1113636619 * value)
	{
		___m_preBoWenGrid_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBoWenGrid_9), value);
	}

	inline static int32_t get_offset_of_m_preBoWen_10() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_preBoWen_10)); }
	inline GameObject_t1113636619 * get_m_preBoWen_10() const { return ___m_preBoWen_10; }
	inline GameObject_t1113636619 ** get_address_of_m_preBoWen_10() { return &___m_preBoWen_10; }
	inline void set_m_preBoWen_10(GameObject_t1113636619 * value)
	{
		___m_preBoWen_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBoWen_10), value);
	}

	inline static int32_t get_offset_of_m_lstGroups_11() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_lstGroups_11)); }
	inline List_1_t3035967681 * get_m_lstGroups_11() const { return ___m_lstGroups_11; }
	inline List_1_t3035967681 ** get_address_of_m_lstGroups_11() { return &___m_lstGroups_11; }
	inline void set_m_lstGroups_11(List_1_t3035967681 * value)
	{
		___m_lstGroups_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstGroups_11), value);
	}

	inline static int32_t get_offset_of_m_colorBoWen_12() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_colorBoWen_12)); }
	inline Color_t2555686324  get_m_colorBoWen_12() const { return ___m_colorBoWen_12; }
	inline Color_t2555686324 * get_address_of_m_colorBoWen_12() { return &___m_colorBoWen_12; }
	inline void set_m_colorBoWen_12(Color_t2555686324  value)
	{
		___m_colorBoWen_12 = value;
	}

	inline static int32_t get_offset_of_m_fCurGridScale_13() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_fCurGridScale_13)); }
	inline float get_m_fCurGridScale_13() const { return ___m_fCurGridScale_13; }
	inline float* get_address_of_m_fCurGridScale_13() { return &___m_fCurGridScale_13; }
	inline void set_m_fCurGridScale_13(float value)
	{
		___m_fCurGridScale_13 = value;
	}

	inline static int32_t get_offset_of_m_fGridCam2ScaleXiShu_14() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_fGridCam2ScaleXiShu_14)); }
	inline float get_m_fGridCam2ScaleXiShu_14() const { return ___m_fGridCam2ScaleXiShu_14; }
	inline float* get_address_of_m_fGridCam2ScaleXiShu_14() { return &___m_fGridCam2ScaleXiShu_14; }
	inline void set_m_fGridCam2ScaleXiShu_14(float value)
	{
		___m_fGridCam2ScaleXiShu_14 = value;
	}

	inline static int32_t get_offset_of_m_fCurCamSize_15() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_fCurCamSize_15)); }
	inline float get_m_fCurCamSize_15() const { return ___m_fCurCamSize_15; }
	inline float* get_address_of_m_fCurCamSize_15() { return &___m_fCurCamSize_15; }
	inline void set_m_fCurCamSize_15(float value)
	{
		___m_fCurCamSize_15 = value;
	}

	inline static int32_t get_offset_of_m_vecOriginPoints_16() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_vecOriginPoints_16)); }
	inline Vector3_t3722313464  get_m_vecOriginPoints_16() const { return ___m_vecOriginPoints_16; }
	inline Vector3_t3722313464 * get_address_of_m_vecOriginPoints_16() { return &___m_vecOriginPoints_16; }
	inline void set_m_vecOriginPoints_16(Vector3_t3722313464  value)
	{
		___m_vecOriginPoints_16 = value;
	}

	inline static int32_t get_offset_of_m_aryThreshold_17() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_aryThreshold_17)); }
	inline SingleU5BU5D_t1444911251* get_m_aryThreshold_17() const { return ___m_aryThreshold_17; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryThreshold_17() { return &___m_aryThreshold_17; }
	inline void set_m_aryThreshold_17(SingleU5BU5D_t1444911251* value)
	{
		___m_aryThreshold_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryThreshold_17), value);
	}

	inline static int32_t get_offset_of_m_fLastCamSize_18() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600, ___m_fLastCamSize_18)); }
	inline float get_m_fLastCamSize_18() const { return ___m_fLastCamSize_18; }
	inline float* get_address_of_m_fLastCamSize_18() { return &___m_fLastCamSize_18; }
	inline void set_m_fLastCamSize_18(float value)
	{
		___m_fLastCamSize_18 = value;
	}
};

struct CStripeManager_t2410268600_StaticFields
{
public:
	// CStripeManager CStripeManager::s_Instance
	CStripeManager_t2410268600 * ___s_Instance_2;
	// UnityEngine.Vector3 CStripeManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CStripeManager::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_4;
	// UnityEngine.Vector3 CStripeManager::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_5;
	// UnityEngine.Vector3 CStripeManager::vecTempScale
	Vector3_t3722313464  ___vecTempScale_6;
	// UnityEngine.Vector2 CStripeManager::vecTempDir
	Vector2_t2156229523  ___vecTempDir_7;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600_StaticFields, ___s_Instance_2)); }
	inline CStripeManager_t2410268600 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CStripeManager_t2410268600 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CStripeManager_t2410268600 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_4() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600_StaticFields, ___vecTempPos1_4)); }
	inline Vector3_t3722313464  get_vecTempPos1_4() const { return ___vecTempPos1_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_4() { return &___vecTempPos1_4; }
	inline void set_vecTempPos1_4(Vector3_t3722313464  value)
	{
		___vecTempPos1_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_5() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600_StaticFields, ___vecTempPos2_5)); }
	inline Vector3_t3722313464  get_vecTempPos2_5() const { return ___vecTempPos2_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_5() { return &___vecTempPos2_5; }
	inline void set_vecTempPos2_5(Vector3_t3722313464  value)
	{
		___vecTempPos2_5 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_6() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600_StaticFields, ___vecTempScale_6)); }
	inline Vector3_t3722313464  get_vecTempScale_6() const { return ___vecTempScale_6; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_6() { return &___vecTempScale_6; }
	inline void set_vecTempScale_6(Vector3_t3722313464  value)
	{
		___vecTempScale_6 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_7() { return static_cast<int32_t>(offsetof(CStripeManager_t2410268600_StaticFields, ___vecTempDir_7)); }
	inline Vector2_t2156229523  get_vecTempDir_7() const { return ___vecTempDir_7; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_7() { return &___vecTempDir_7; }
	inline void set_vecTempDir_7(Vector2_t2156229523  value)
	{
		___vecTempDir_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSTRIPEMANAGER_T2410268600_H
#ifndef CTIAOZI_T1651486976_H
#define CTIAOZI_T1651486976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZi
struct  CTiaoZi_t1651486976  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rendering.SortingGroup CTiaoZi::_sortingGroup
	SortingGroup_t3239126932 * ____sortingGroup_2;
	// UnityEngine.TextMesh CTiaoZi::_tmMain
	TextMesh_t1536577757 * ____tmMain_6;
	// UnityEngine.CanvasGroup CTiaoZi::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_7;
	// UnityEngine.SpriteRenderer CTiaoZi::_srBg
	SpriteRenderer_t3235626157 * ____srBg_8;
	// UnityEngine.SpriteRenderer CTiaoZi::_srTitle
	SpriteRenderer_t3235626157 * ____srTitle_9;
	// UnityEngine.UI.Text[] CTiaoZi::_txtContent
	TextU5BU5D_t422084607* ____txtContent_10;
	// UnityEngine.TextMesh[] CTiaoZi::_txtMeshContent
	TextMeshU5BU5D_t2627012368* ____txtMeshContent_11;
	// UnityEngine.TextMesh[] CTiaoZi::_txtMeshContent_MiaoBian
	TextMeshU5BU5D_t2627012368* ____txtMeshContent_MiaoBian_12;
	// CTiaoZiManager/eTiaoZiType CTiaoZi::m_eType
	int32_t ___m_eType_13;
	// Ball CTiaoZi::m_Ball
	Ball_t2206666566 * ___m_Ball_14;
	// System.Boolean CTiaoZi::m_bActive
	bool ___m_bActive_15;
	// System.Int32 CTiaoZi::m_nStatus
	int32_t ___m_nStatus_16;
	// UnityEngine.Vector2 CTiaoZi::m_vecParentV
	Vector2_t2156229523  ___m_vecParentV_17;
	// System.Single CTiaoZi::m_fPaoWuXianVx0
	float ___m_fPaoWuXianVx0_18;
	// System.Single CTiaoZi::m_fPaoWuXianVy0
	float ___m_fPaoWuXianVy0_19;
	// System.Single CTiaoZi::m_fPaoWuXianT
	float ___m_fPaoWuXianT_20;
	// System.Single CTiaoZi::m_fPaoWuXianA
	float ___m_fPaoWuXianA_21;
	// System.Single CTiaoZi::m_fPaoWuXianAlphaA
	float ___m_fPaoWuXianAlphaA_22;
	// System.Single CTiaoZi::m_fBianDaMaxScale
	float ___m_fBianDaMaxScale_23;
	// System.Single CTiaoZi::m_fBianDaA
	float ___m_fBianDaA_24;
	// System.Single CTiaoZi::m_fSuoXiaoA
	float ___m_fSuoXiaoA_25;
	// System.Single CTiaoZi::m_fShangShengT
	float ___m_fShangShengT_26;
	// System.Single CTiaoZi::m_fShangShengBanJingXiShu
	float ___m_fShangShengBanJingXiShu_27;
	// System.Single CTiaoZi::m_fShangShengV0
	float ___m_fShangShengV0_28;
	// System.Single CTiaoZi::m_fShangShengA
	float ___m_fShangShengA_29;
	// System.Single CTiaoZi::m_fShangShengAlphaA
	float ___m_fShangShengAlphaA_30;
	// System.Single CTiaoZi::m_fType3_ScaleXiShu
	float ___m_fType3_ScaleXiShu_31;
	// System.Single CTiaoZi::m_fType3_BianDaA
	float ___m_fType3_BianDaA_32;
	// System.Single CTiaoZi::m_fType3_BianDaT
	float ___m_fType3_BianDaT_33;
	// System.Single CTiaoZi::m_fType3_ChiXuT
	float ___m_fType3_ChiXuT_34;
	// System.Single CTiaoZi::m_fType3_BianXiaoA
	float ___m_fType3_BianXiaoA_35;
	// System.Single CTiaoZi::m_fType3_BianXiaoT
	float ___m_fType3_BianXiaoT_36;
	// System.Single CTiaoZi::m_fType3_MaxScale
	float ___m_fType3_MaxScale_37;
	// System.Single CTiaoZi::m_fType3_CurChiXuTime
	float ___m_fType3_CurChiXuTime_38;
	// System.Single CTiaoZi::m_fType4_CurChiXuTime
	float ___m_fType4_CurChiXuTime_39;
	// System.Single CTiaoZi::m_fType4_ChiXuTime
	float ___m_fType4_ChiXuTime_40;
	// System.Single CTiaoZi::m_fYanMie_MaxScale
	float ___m_fYanMie_MaxScale_41;
	// System.Single CTiaoZi::m_fYanMie_BianXiaoEndScale
	float ___m_fYanMie_BianXiaoEndScale_42;
	// System.Single CTiaoZi::m_fYanMie_ChiXuT
	float ___m_fYanMie_ChiXuT_43;
	// System.Single CTiaoZi::m_fYanMie_XiaJiangDistance
	float ___m_fYanMie_XiaJiangDistance_44;
	// System.Single CTiaoZi::m_fYanMie_BianDaA
	float ___m_fYanMie_BianDaA_45;
	// System.Single CTiaoZi::m_fYanMie_BianXiaoA
	float ___m_fYanMie_BianXiaoA_46;
	// System.Single CTiaoZi::m_fYanMie_BianXiaoAlphaA
	float ___m_fYanMie_BianXiaoAlphaA_47;
	// System.Single CTiaoZi::m_fYanMie_XiaJiangA
	float ___m_fYanMie_XiaJiangA_48;
	// System.Single CTiaoZi::m_fYanMie_XiaJiangV0
	float ___m_fYanMie_XiaJiangV0_49;
	// System.Single CTiaoZi::m_fYanMie_XiaJiangV
	float ___m_fYanMie_XiaJiangV_50;
	// System.Single CTiaoZi::m_fYanMie_CurChiXuTime
	float ___m_fYanMie_CurChiXuTime_51;
	// System.Single CTiaoZi::m_fTunShi_XiaJiang_V
	float ___m_fTunShi_XiaJiang_V_52;
	// System.Single CTiaoZi::m_fTunShi_Aplha_V
	float ___m_fTunShi_Aplha_V_53;
	// System.Single CTiaoZi::m_fTunShi_Scale_V
	float ___m_fTunShi_Scale_V_54;
	// System.Single CTiaoZi::m_fTunShi_ChiXu
	float ___m_fTunShi_ChiXu_55;
	// System.Single CTiaoZi::m_fTunShi_CurChiXuTime
	float ___m_fTunShi_CurChiXuTime_56;
	// UnityEngine.Vector3 CTiaoZi::m_vecStartPos
	Vector3_t3722313464  ___m_vecStartPos_57;
	// UnityEngine.Vector3 CTiaoZi::m_vecCurPos
	Vector3_t3722313464  ___m_vecCurPos_58;
	// System.Single CTiaoZi::m_fCurAlpha
	float ___m_fCurAlpha_59;
	// System.Single CTiaoZi::m_fCurScale
	float ___m_fCurScale_60;
	// System.Single CTiaoZi::m_fBgAlpha
	float ___m_fBgAlpha_61;
	// CTiaoZiManager/eTiaoZiType CTiaoZi::m_eZiYuanType
	int32_t ___m_eZiYuanType_62;
	// System.Single CTiaoZi::m_fType1_CurChiXuTime
	float ___m_fType1_CurChiXuTime_63;
	// System.Int32 CTiaoZi::m_nIndex
	int32_t ___m_nIndex_64;

public:
	inline static int32_t get_offset_of__sortingGroup_2() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____sortingGroup_2)); }
	inline SortingGroup_t3239126932 * get__sortingGroup_2() const { return ____sortingGroup_2; }
	inline SortingGroup_t3239126932 ** get_address_of__sortingGroup_2() { return &____sortingGroup_2; }
	inline void set__sortingGroup_2(SortingGroup_t3239126932 * value)
	{
		____sortingGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&____sortingGroup_2), value);
	}

	inline static int32_t get_offset_of__tmMain_6() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____tmMain_6)); }
	inline TextMesh_t1536577757 * get__tmMain_6() const { return ____tmMain_6; }
	inline TextMesh_t1536577757 ** get_address_of__tmMain_6() { return &____tmMain_6; }
	inline void set__tmMain_6(TextMesh_t1536577757 * value)
	{
		____tmMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____tmMain_6), value);
	}

	inline static int32_t get_offset_of__canvasGroup_7() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____canvasGroup_7)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_7() const { return ____canvasGroup_7; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_7() { return &____canvasGroup_7; }
	inline void set__canvasGroup_7(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_7), value);
	}

	inline static int32_t get_offset_of__srBg_8() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____srBg_8)); }
	inline SpriteRenderer_t3235626157 * get__srBg_8() const { return ____srBg_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srBg_8() { return &____srBg_8; }
	inline void set__srBg_8(SpriteRenderer_t3235626157 * value)
	{
		____srBg_8 = value;
		Il2CppCodeGenWriteBarrier((&____srBg_8), value);
	}

	inline static int32_t get_offset_of__srTitle_9() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____srTitle_9)); }
	inline SpriteRenderer_t3235626157 * get__srTitle_9() const { return ____srTitle_9; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srTitle_9() { return &____srTitle_9; }
	inline void set__srTitle_9(SpriteRenderer_t3235626157 * value)
	{
		____srTitle_9 = value;
		Il2CppCodeGenWriteBarrier((&____srTitle_9), value);
	}

	inline static int32_t get_offset_of__txtContent_10() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____txtContent_10)); }
	inline TextU5BU5D_t422084607* get__txtContent_10() const { return ____txtContent_10; }
	inline TextU5BU5D_t422084607** get_address_of__txtContent_10() { return &____txtContent_10; }
	inline void set__txtContent_10(TextU5BU5D_t422084607* value)
	{
		____txtContent_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_10), value);
	}

	inline static int32_t get_offset_of__txtMeshContent_11() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____txtMeshContent_11)); }
	inline TextMeshU5BU5D_t2627012368* get__txtMeshContent_11() const { return ____txtMeshContent_11; }
	inline TextMeshU5BU5D_t2627012368** get_address_of__txtMeshContent_11() { return &____txtMeshContent_11; }
	inline void set__txtMeshContent_11(TextMeshU5BU5D_t2627012368* value)
	{
		____txtMeshContent_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtMeshContent_11), value);
	}

	inline static int32_t get_offset_of__txtMeshContent_MiaoBian_12() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ____txtMeshContent_MiaoBian_12)); }
	inline TextMeshU5BU5D_t2627012368* get__txtMeshContent_MiaoBian_12() const { return ____txtMeshContent_MiaoBian_12; }
	inline TextMeshU5BU5D_t2627012368** get_address_of__txtMeshContent_MiaoBian_12() { return &____txtMeshContent_MiaoBian_12; }
	inline void set__txtMeshContent_MiaoBian_12(TextMeshU5BU5D_t2627012368* value)
	{
		____txtMeshContent_MiaoBian_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtMeshContent_MiaoBian_12), value);
	}

	inline static int32_t get_offset_of_m_eType_13() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_eType_13)); }
	inline int32_t get_m_eType_13() const { return ___m_eType_13; }
	inline int32_t* get_address_of_m_eType_13() { return &___m_eType_13; }
	inline void set_m_eType_13(int32_t value)
	{
		___m_eType_13 = value;
	}

	inline static int32_t get_offset_of_m_Ball_14() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_Ball_14)); }
	inline Ball_t2206666566 * get_m_Ball_14() const { return ___m_Ball_14; }
	inline Ball_t2206666566 ** get_address_of_m_Ball_14() { return &___m_Ball_14; }
	inline void set_m_Ball_14(Ball_t2206666566 * value)
	{
		___m_Ball_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Ball_14), value);
	}

	inline static int32_t get_offset_of_m_bActive_15() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_bActive_15)); }
	inline bool get_m_bActive_15() const { return ___m_bActive_15; }
	inline bool* get_address_of_m_bActive_15() { return &___m_bActive_15; }
	inline void set_m_bActive_15(bool value)
	{
		___m_bActive_15 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_16() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_nStatus_16)); }
	inline int32_t get_m_nStatus_16() const { return ___m_nStatus_16; }
	inline int32_t* get_address_of_m_nStatus_16() { return &___m_nStatus_16; }
	inline void set_m_nStatus_16(int32_t value)
	{
		___m_nStatus_16 = value;
	}

	inline static int32_t get_offset_of_m_vecParentV_17() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_vecParentV_17)); }
	inline Vector2_t2156229523  get_m_vecParentV_17() const { return ___m_vecParentV_17; }
	inline Vector2_t2156229523 * get_address_of_m_vecParentV_17() { return &___m_vecParentV_17; }
	inline void set_m_vecParentV_17(Vector2_t2156229523  value)
	{
		___m_vecParentV_17 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianVx0_18() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fPaoWuXianVx0_18)); }
	inline float get_m_fPaoWuXianVx0_18() const { return ___m_fPaoWuXianVx0_18; }
	inline float* get_address_of_m_fPaoWuXianVx0_18() { return &___m_fPaoWuXianVx0_18; }
	inline void set_m_fPaoWuXianVx0_18(float value)
	{
		___m_fPaoWuXianVx0_18 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianVy0_19() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fPaoWuXianVy0_19)); }
	inline float get_m_fPaoWuXianVy0_19() const { return ___m_fPaoWuXianVy0_19; }
	inline float* get_address_of_m_fPaoWuXianVy0_19() { return &___m_fPaoWuXianVy0_19; }
	inline void set_m_fPaoWuXianVy0_19(float value)
	{
		___m_fPaoWuXianVy0_19 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianT_20() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fPaoWuXianT_20)); }
	inline float get_m_fPaoWuXianT_20() const { return ___m_fPaoWuXianT_20; }
	inline float* get_address_of_m_fPaoWuXianT_20() { return &___m_fPaoWuXianT_20; }
	inline void set_m_fPaoWuXianT_20(float value)
	{
		___m_fPaoWuXianT_20 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianA_21() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fPaoWuXianA_21)); }
	inline float get_m_fPaoWuXianA_21() const { return ___m_fPaoWuXianA_21; }
	inline float* get_address_of_m_fPaoWuXianA_21() { return &___m_fPaoWuXianA_21; }
	inline void set_m_fPaoWuXianA_21(float value)
	{
		___m_fPaoWuXianA_21 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianAlphaA_22() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fPaoWuXianAlphaA_22)); }
	inline float get_m_fPaoWuXianAlphaA_22() const { return ___m_fPaoWuXianAlphaA_22; }
	inline float* get_address_of_m_fPaoWuXianAlphaA_22() { return &___m_fPaoWuXianAlphaA_22; }
	inline void set_m_fPaoWuXianAlphaA_22(float value)
	{
		___m_fPaoWuXianAlphaA_22 = value;
	}

	inline static int32_t get_offset_of_m_fBianDaMaxScale_23() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fBianDaMaxScale_23)); }
	inline float get_m_fBianDaMaxScale_23() const { return ___m_fBianDaMaxScale_23; }
	inline float* get_address_of_m_fBianDaMaxScale_23() { return &___m_fBianDaMaxScale_23; }
	inline void set_m_fBianDaMaxScale_23(float value)
	{
		___m_fBianDaMaxScale_23 = value;
	}

	inline static int32_t get_offset_of_m_fBianDaA_24() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fBianDaA_24)); }
	inline float get_m_fBianDaA_24() const { return ___m_fBianDaA_24; }
	inline float* get_address_of_m_fBianDaA_24() { return &___m_fBianDaA_24; }
	inline void set_m_fBianDaA_24(float value)
	{
		___m_fBianDaA_24 = value;
	}

	inline static int32_t get_offset_of_m_fSuoXiaoA_25() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fSuoXiaoA_25)); }
	inline float get_m_fSuoXiaoA_25() const { return ___m_fSuoXiaoA_25; }
	inline float* get_address_of_m_fSuoXiaoA_25() { return &___m_fSuoXiaoA_25; }
	inline void set_m_fSuoXiaoA_25(float value)
	{
		___m_fSuoXiaoA_25 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengT_26() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fShangShengT_26)); }
	inline float get_m_fShangShengT_26() const { return ___m_fShangShengT_26; }
	inline float* get_address_of_m_fShangShengT_26() { return &___m_fShangShengT_26; }
	inline void set_m_fShangShengT_26(float value)
	{
		___m_fShangShengT_26 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengBanJingXiShu_27() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fShangShengBanJingXiShu_27)); }
	inline float get_m_fShangShengBanJingXiShu_27() const { return ___m_fShangShengBanJingXiShu_27; }
	inline float* get_address_of_m_fShangShengBanJingXiShu_27() { return &___m_fShangShengBanJingXiShu_27; }
	inline void set_m_fShangShengBanJingXiShu_27(float value)
	{
		___m_fShangShengBanJingXiShu_27 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengV0_28() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fShangShengV0_28)); }
	inline float get_m_fShangShengV0_28() const { return ___m_fShangShengV0_28; }
	inline float* get_address_of_m_fShangShengV0_28() { return &___m_fShangShengV0_28; }
	inline void set_m_fShangShengV0_28(float value)
	{
		___m_fShangShengV0_28 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengA_29() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fShangShengA_29)); }
	inline float get_m_fShangShengA_29() const { return ___m_fShangShengA_29; }
	inline float* get_address_of_m_fShangShengA_29() { return &___m_fShangShengA_29; }
	inline void set_m_fShangShengA_29(float value)
	{
		___m_fShangShengA_29 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengAlphaA_30() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fShangShengAlphaA_30)); }
	inline float get_m_fShangShengAlphaA_30() const { return ___m_fShangShengAlphaA_30; }
	inline float* get_address_of_m_fShangShengAlphaA_30() { return &___m_fShangShengAlphaA_30; }
	inline void set_m_fShangShengAlphaA_30(float value)
	{
		___m_fShangShengAlphaA_30 = value;
	}

	inline static int32_t get_offset_of_m_fType3_ScaleXiShu_31() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_ScaleXiShu_31)); }
	inline float get_m_fType3_ScaleXiShu_31() const { return ___m_fType3_ScaleXiShu_31; }
	inline float* get_address_of_m_fType3_ScaleXiShu_31() { return &___m_fType3_ScaleXiShu_31; }
	inline void set_m_fType3_ScaleXiShu_31(float value)
	{
		___m_fType3_ScaleXiShu_31 = value;
	}

	inline static int32_t get_offset_of_m_fType3_BianDaA_32() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_BianDaA_32)); }
	inline float get_m_fType3_BianDaA_32() const { return ___m_fType3_BianDaA_32; }
	inline float* get_address_of_m_fType3_BianDaA_32() { return &___m_fType3_BianDaA_32; }
	inline void set_m_fType3_BianDaA_32(float value)
	{
		___m_fType3_BianDaA_32 = value;
	}

	inline static int32_t get_offset_of_m_fType3_BianDaT_33() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_BianDaT_33)); }
	inline float get_m_fType3_BianDaT_33() const { return ___m_fType3_BianDaT_33; }
	inline float* get_address_of_m_fType3_BianDaT_33() { return &___m_fType3_BianDaT_33; }
	inline void set_m_fType3_BianDaT_33(float value)
	{
		___m_fType3_BianDaT_33 = value;
	}

	inline static int32_t get_offset_of_m_fType3_ChiXuT_34() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_ChiXuT_34)); }
	inline float get_m_fType3_ChiXuT_34() const { return ___m_fType3_ChiXuT_34; }
	inline float* get_address_of_m_fType3_ChiXuT_34() { return &___m_fType3_ChiXuT_34; }
	inline void set_m_fType3_ChiXuT_34(float value)
	{
		___m_fType3_ChiXuT_34 = value;
	}

	inline static int32_t get_offset_of_m_fType3_BianXiaoA_35() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_BianXiaoA_35)); }
	inline float get_m_fType3_BianXiaoA_35() const { return ___m_fType3_BianXiaoA_35; }
	inline float* get_address_of_m_fType3_BianXiaoA_35() { return &___m_fType3_BianXiaoA_35; }
	inline void set_m_fType3_BianXiaoA_35(float value)
	{
		___m_fType3_BianXiaoA_35 = value;
	}

	inline static int32_t get_offset_of_m_fType3_BianXiaoT_36() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_BianXiaoT_36)); }
	inline float get_m_fType3_BianXiaoT_36() const { return ___m_fType3_BianXiaoT_36; }
	inline float* get_address_of_m_fType3_BianXiaoT_36() { return &___m_fType3_BianXiaoT_36; }
	inline void set_m_fType3_BianXiaoT_36(float value)
	{
		___m_fType3_BianXiaoT_36 = value;
	}

	inline static int32_t get_offset_of_m_fType3_MaxScale_37() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_MaxScale_37)); }
	inline float get_m_fType3_MaxScale_37() const { return ___m_fType3_MaxScale_37; }
	inline float* get_address_of_m_fType3_MaxScale_37() { return &___m_fType3_MaxScale_37; }
	inline void set_m_fType3_MaxScale_37(float value)
	{
		___m_fType3_MaxScale_37 = value;
	}

	inline static int32_t get_offset_of_m_fType3_CurChiXuTime_38() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType3_CurChiXuTime_38)); }
	inline float get_m_fType3_CurChiXuTime_38() const { return ___m_fType3_CurChiXuTime_38; }
	inline float* get_address_of_m_fType3_CurChiXuTime_38() { return &___m_fType3_CurChiXuTime_38; }
	inline void set_m_fType3_CurChiXuTime_38(float value)
	{
		___m_fType3_CurChiXuTime_38 = value;
	}

	inline static int32_t get_offset_of_m_fType4_CurChiXuTime_39() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType4_CurChiXuTime_39)); }
	inline float get_m_fType4_CurChiXuTime_39() const { return ___m_fType4_CurChiXuTime_39; }
	inline float* get_address_of_m_fType4_CurChiXuTime_39() { return &___m_fType4_CurChiXuTime_39; }
	inline void set_m_fType4_CurChiXuTime_39(float value)
	{
		___m_fType4_CurChiXuTime_39 = value;
	}

	inline static int32_t get_offset_of_m_fType4_ChiXuTime_40() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType4_ChiXuTime_40)); }
	inline float get_m_fType4_ChiXuTime_40() const { return ___m_fType4_ChiXuTime_40; }
	inline float* get_address_of_m_fType4_ChiXuTime_40() { return &___m_fType4_ChiXuTime_40; }
	inline void set_m_fType4_ChiXuTime_40(float value)
	{
		___m_fType4_ChiXuTime_40 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_MaxScale_41() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_MaxScale_41)); }
	inline float get_m_fYanMie_MaxScale_41() const { return ___m_fYanMie_MaxScale_41; }
	inline float* get_address_of_m_fYanMie_MaxScale_41() { return &___m_fYanMie_MaxScale_41; }
	inline void set_m_fYanMie_MaxScale_41(float value)
	{
		___m_fYanMie_MaxScale_41 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianXiaoEndScale_42() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_BianXiaoEndScale_42)); }
	inline float get_m_fYanMie_BianXiaoEndScale_42() const { return ___m_fYanMie_BianXiaoEndScale_42; }
	inline float* get_address_of_m_fYanMie_BianXiaoEndScale_42() { return &___m_fYanMie_BianXiaoEndScale_42; }
	inline void set_m_fYanMie_BianXiaoEndScale_42(float value)
	{
		___m_fYanMie_BianXiaoEndScale_42 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_ChiXuT_43() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_ChiXuT_43)); }
	inline float get_m_fYanMie_ChiXuT_43() const { return ___m_fYanMie_ChiXuT_43; }
	inline float* get_address_of_m_fYanMie_ChiXuT_43() { return &___m_fYanMie_ChiXuT_43; }
	inline void set_m_fYanMie_ChiXuT_43(float value)
	{
		___m_fYanMie_ChiXuT_43 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_XiaJiangDistance_44() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_XiaJiangDistance_44)); }
	inline float get_m_fYanMie_XiaJiangDistance_44() const { return ___m_fYanMie_XiaJiangDistance_44; }
	inline float* get_address_of_m_fYanMie_XiaJiangDistance_44() { return &___m_fYanMie_XiaJiangDistance_44; }
	inline void set_m_fYanMie_XiaJiangDistance_44(float value)
	{
		___m_fYanMie_XiaJiangDistance_44 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianDaA_45() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_BianDaA_45)); }
	inline float get_m_fYanMie_BianDaA_45() const { return ___m_fYanMie_BianDaA_45; }
	inline float* get_address_of_m_fYanMie_BianDaA_45() { return &___m_fYanMie_BianDaA_45; }
	inline void set_m_fYanMie_BianDaA_45(float value)
	{
		___m_fYanMie_BianDaA_45 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianXiaoA_46() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_BianXiaoA_46)); }
	inline float get_m_fYanMie_BianXiaoA_46() const { return ___m_fYanMie_BianXiaoA_46; }
	inline float* get_address_of_m_fYanMie_BianXiaoA_46() { return &___m_fYanMie_BianXiaoA_46; }
	inline void set_m_fYanMie_BianXiaoA_46(float value)
	{
		___m_fYanMie_BianXiaoA_46 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianXiaoAlphaA_47() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_BianXiaoAlphaA_47)); }
	inline float get_m_fYanMie_BianXiaoAlphaA_47() const { return ___m_fYanMie_BianXiaoAlphaA_47; }
	inline float* get_address_of_m_fYanMie_BianXiaoAlphaA_47() { return &___m_fYanMie_BianXiaoAlphaA_47; }
	inline void set_m_fYanMie_BianXiaoAlphaA_47(float value)
	{
		___m_fYanMie_BianXiaoAlphaA_47 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_XiaJiangA_48() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_XiaJiangA_48)); }
	inline float get_m_fYanMie_XiaJiangA_48() const { return ___m_fYanMie_XiaJiangA_48; }
	inline float* get_address_of_m_fYanMie_XiaJiangA_48() { return &___m_fYanMie_XiaJiangA_48; }
	inline void set_m_fYanMie_XiaJiangA_48(float value)
	{
		___m_fYanMie_XiaJiangA_48 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_XiaJiangV0_49() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_XiaJiangV0_49)); }
	inline float get_m_fYanMie_XiaJiangV0_49() const { return ___m_fYanMie_XiaJiangV0_49; }
	inline float* get_address_of_m_fYanMie_XiaJiangV0_49() { return &___m_fYanMie_XiaJiangV0_49; }
	inline void set_m_fYanMie_XiaJiangV0_49(float value)
	{
		___m_fYanMie_XiaJiangV0_49 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_XiaJiangV_50() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_XiaJiangV_50)); }
	inline float get_m_fYanMie_XiaJiangV_50() const { return ___m_fYanMie_XiaJiangV_50; }
	inline float* get_address_of_m_fYanMie_XiaJiangV_50() { return &___m_fYanMie_XiaJiangV_50; }
	inline void set_m_fYanMie_XiaJiangV_50(float value)
	{
		___m_fYanMie_XiaJiangV_50 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_CurChiXuTime_51() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fYanMie_CurChiXuTime_51)); }
	inline float get_m_fYanMie_CurChiXuTime_51() const { return ___m_fYanMie_CurChiXuTime_51; }
	inline float* get_address_of_m_fYanMie_CurChiXuTime_51() { return &___m_fYanMie_CurChiXuTime_51; }
	inline void set_m_fYanMie_CurChiXuTime_51(float value)
	{
		___m_fYanMie_CurChiXuTime_51 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_XiaJiang_V_52() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fTunShi_XiaJiang_V_52)); }
	inline float get_m_fTunShi_XiaJiang_V_52() const { return ___m_fTunShi_XiaJiang_V_52; }
	inline float* get_address_of_m_fTunShi_XiaJiang_V_52() { return &___m_fTunShi_XiaJiang_V_52; }
	inline void set_m_fTunShi_XiaJiang_V_52(float value)
	{
		___m_fTunShi_XiaJiang_V_52 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_Aplha_V_53() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fTunShi_Aplha_V_53)); }
	inline float get_m_fTunShi_Aplha_V_53() const { return ___m_fTunShi_Aplha_V_53; }
	inline float* get_address_of_m_fTunShi_Aplha_V_53() { return &___m_fTunShi_Aplha_V_53; }
	inline void set_m_fTunShi_Aplha_V_53(float value)
	{
		___m_fTunShi_Aplha_V_53 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_Scale_V_54() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fTunShi_Scale_V_54)); }
	inline float get_m_fTunShi_Scale_V_54() const { return ___m_fTunShi_Scale_V_54; }
	inline float* get_address_of_m_fTunShi_Scale_V_54() { return &___m_fTunShi_Scale_V_54; }
	inline void set_m_fTunShi_Scale_V_54(float value)
	{
		___m_fTunShi_Scale_V_54 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_ChiXu_55() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fTunShi_ChiXu_55)); }
	inline float get_m_fTunShi_ChiXu_55() const { return ___m_fTunShi_ChiXu_55; }
	inline float* get_address_of_m_fTunShi_ChiXu_55() { return &___m_fTunShi_ChiXu_55; }
	inline void set_m_fTunShi_ChiXu_55(float value)
	{
		___m_fTunShi_ChiXu_55 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_CurChiXuTime_56() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fTunShi_CurChiXuTime_56)); }
	inline float get_m_fTunShi_CurChiXuTime_56() const { return ___m_fTunShi_CurChiXuTime_56; }
	inline float* get_address_of_m_fTunShi_CurChiXuTime_56() { return &___m_fTunShi_CurChiXuTime_56; }
	inline void set_m_fTunShi_CurChiXuTime_56(float value)
	{
		___m_fTunShi_CurChiXuTime_56 = value;
	}

	inline static int32_t get_offset_of_m_vecStartPos_57() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_vecStartPos_57)); }
	inline Vector3_t3722313464  get_m_vecStartPos_57() const { return ___m_vecStartPos_57; }
	inline Vector3_t3722313464 * get_address_of_m_vecStartPos_57() { return &___m_vecStartPos_57; }
	inline void set_m_vecStartPos_57(Vector3_t3722313464  value)
	{
		___m_vecStartPos_57 = value;
	}

	inline static int32_t get_offset_of_m_vecCurPos_58() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_vecCurPos_58)); }
	inline Vector3_t3722313464  get_m_vecCurPos_58() const { return ___m_vecCurPos_58; }
	inline Vector3_t3722313464 * get_address_of_m_vecCurPos_58() { return &___m_vecCurPos_58; }
	inline void set_m_vecCurPos_58(Vector3_t3722313464  value)
	{
		___m_vecCurPos_58 = value;
	}

	inline static int32_t get_offset_of_m_fCurAlpha_59() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fCurAlpha_59)); }
	inline float get_m_fCurAlpha_59() const { return ___m_fCurAlpha_59; }
	inline float* get_address_of_m_fCurAlpha_59() { return &___m_fCurAlpha_59; }
	inline void set_m_fCurAlpha_59(float value)
	{
		___m_fCurAlpha_59 = value;
	}

	inline static int32_t get_offset_of_m_fCurScale_60() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fCurScale_60)); }
	inline float get_m_fCurScale_60() const { return ___m_fCurScale_60; }
	inline float* get_address_of_m_fCurScale_60() { return &___m_fCurScale_60; }
	inline void set_m_fCurScale_60(float value)
	{
		___m_fCurScale_60 = value;
	}

	inline static int32_t get_offset_of_m_fBgAlpha_61() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fBgAlpha_61)); }
	inline float get_m_fBgAlpha_61() const { return ___m_fBgAlpha_61; }
	inline float* get_address_of_m_fBgAlpha_61() { return &___m_fBgAlpha_61; }
	inline void set_m_fBgAlpha_61(float value)
	{
		___m_fBgAlpha_61 = value;
	}

	inline static int32_t get_offset_of_m_eZiYuanType_62() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_eZiYuanType_62)); }
	inline int32_t get_m_eZiYuanType_62() const { return ___m_eZiYuanType_62; }
	inline int32_t* get_address_of_m_eZiYuanType_62() { return &___m_eZiYuanType_62; }
	inline void set_m_eZiYuanType_62(int32_t value)
	{
		___m_eZiYuanType_62 = value;
	}

	inline static int32_t get_offset_of_m_fType1_CurChiXuTime_63() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_fType1_CurChiXuTime_63)); }
	inline float get_m_fType1_CurChiXuTime_63() const { return ___m_fType1_CurChiXuTime_63; }
	inline float* get_address_of_m_fType1_CurChiXuTime_63() { return &___m_fType1_CurChiXuTime_63; }
	inline void set_m_fType1_CurChiXuTime_63(float value)
	{
		___m_fType1_CurChiXuTime_63 = value;
	}

	inline static int32_t get_offset_of_m_nIndex_64() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976, ___m_nIndex_64)); }
	inline int32_t get_m_nIndex_64() const { return ___m_nIndex_64; }
	inline int32_t* get_address_of_m_nIndex_64() { return &___m_nIndex_64; }
	inline void set_m_nIndex_64(int32_t value)
	{
		___m_nIndex_64 = value;
	}
};

struct CTiaoZi_t1651486976_StaticFields
{
public:
	// UnityEngine.Vector3 CTiaoZi::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// UnityEngine.Vector3 CTiaoZi::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// UnityEngine.Color CTiaoZi::vecTempColor
	Color_t2555686324  ___vecTempColor_5;

public:
	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976_StaticFields, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempColor_5() { return static_cast<int32_t>(offsetof(CTiaoZi_t1651486976_StaticFields, ___vecTempColor_5)); }
	inline Color_t2555686324  get_vecTempColor_5() const { return ___vecTempColor_5; }
	inline Color_t2555686324 * get_address_of_vecTempColor_5() { return &___vecTempColor_5; }
	inline void set_vecTempColor_5(Color_t2555686324  value)
	{
		___vecTempColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTIAOZI_T1651486976_H
#ifndef CTIAOZIMANAGER_T3709983989_H
#define CTIAOZIMANAGER_T3709983989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZiManager
struct  CTiaoZiManager_t3709983989  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color[] CTiaoZiManager::m_aryColors
	ColorU5BU5D_t941916413* ___m_aryColors_3;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_TunShi
	float ___m_fTiaoZiCamThreshold_TunShi_4;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_JinBi
	float ___m_fTiaoZiCamThreshold_JinBi_5;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_ZhanCi_TiJi
	float ___m_fTiaoZiCamThreshold_ZhanCi_TiJi_6;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_ZhanCi_JinQian
	float ___m_fTiaoZiCamThreshold_ZhanCi_JinQian_7;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_TanKai
	float ___m_fTiaoZiCamThreshold_TanKai_8;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_ShengJi
	float ___m_fTiaoZiCamThreshold_ShengJi_9;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_CiMianYi
	float ___m_fTiaoZiCamThreshold_CiMianYi_10;
	// System.Single CTiaoZiManager::m_fTiaoZiCamThreshold_YanMieMianYi
	float ___m_fTiaoZiCamThreshold_YanMieMianYi_11;
	// UnityEngine.GameObject CTiaoZiManager::m_preTiaoZi_Test
	GameObject_t1113636619 * ___m_preTiaoZi_Test_12;
	// UnityEngine.GameObject CTiaoZiManager::m_preTiaoZi
	GameObject_t1113636619 * ___m_preTiaoZi_13;
	// UnityEngine.GameObject CTiaoZiManager::m_preTiaoZi_YanMie
	GameObject_t1113636619 * ___m_preTiaoZi_YanMie_14;
	// UnityEngine.GameObject CTiaoZiManager::m_preTiaoZi_TunShi
	GameObject_t1113636619 * ___m_preTiaoZi_TunShi_15;
	// UnityEngine.GameObject CTiaoZiManager::m_preTiaoZi_JinBi
	GameObject_t1113636619 * ___m_preTiaoZi_JinBi_16;
	// System.Single CTiaoZiManager::m_fPaoWuXianVx0
	float ___m_fPaoWuXianVx0_17;
	// System.Single CTiaoZiManager::m_fPaoWuXianVy0
	float ___m_fPaoWuXianVy0_18;
	// System.Single CTiaoZiManager::m_fPaoWuXianT
	float ___m_fPaoWuXianT_19;
	// System.Single CTiaoZiManager::m_fPaoWuXianA
	float ___m_fPaoWuXianA_20;
	// System.Single CTiaoZiManager::m_fPaoWuXianBanJingXiShu
	float ___m_fPaoWuXianBanJingXiShu_21;
	// System.Single CTiaoZiManager::m_fPaoWuXianSizeXiShu
	float ___m_fPaoWuXianSizeXiShu_22;
	// System.Single CTiaoZiManager::m_fBianDaT
	float ___m_fBianDaT_23;
	// System.Single CTiaoZiManager::m_fShangShengT
	float ___m_fShangShengT_24;
	// System.Single CTiaoZiManager::m_fBianDaBanJingXiShu
	float ___m_fBianDaBanJingXiShu_25;
	// System.Single CTiaoZiManager::m_fShangShengBanJingXiShu
	float ___m_fShangShengBanJingXiShu_26;
	// System.Single CTiaoZiManager::m_fType3_ScaleXiShu
	float ___m_fType3_ScaleXiShu_27;
	// System.Single CTiaoZiManager::m_fType3_BianDaT
	float ___m_fType3_BianDaT_28;
	// System.Single CTiaoZiManager::m_fType3_BianXiaoT
	float ___m_fType3_BianXiaoT_29;
	// System.Single CTiaoZiManager::m_fType3_ChiXuT
	float ___m_fType3_ChiXuT_30;
	// System.Single CTiaoZiManager::m_fType3_OffsetXiShuX
	float ___m_fType3_OffsetXiShuX_31;
	// System.Single CTiaoZiManager::m_fType3_OffsetXiShuY
	float ___m_fType3_OffsetXiShuY_32;
	// System.Single CTiaoZiManager::ShengJi_ScaleXiShu
	float ___ShengJi_ScaleXiShu_33;
	// System.Single CTiaoZiManager::ShengJi_BianDaT
	float ___ShengJi_BianDaT_34;
	// System.Single CTiaoZiManager::ShengJi_BianXiaoT
	float ___ShengJi_BianXiaoT_35;
	// System.Single CTiaoZiManager::ShengJi_ChiXuT
	float ___ShengJi_ChiXuT_36;
	// System.Single CTiaoZiManager::ShengJi_OffsetXiShuX
	float ___ShengJi_OffsetXiShuX_37;
	// System.Single CTiaoZiManager::ShengJi_OffsetXiShuY
	float ___ShengJi_OffsetXiShuY_38;
	// System.Single CTiaoZiManager::m_fYanMie_ScaleXiShu
	float ___m_fYanMie_ScaleXiShu_39;
	// System.Single CTiaoZiManager::m_fYanMie_DaWeiYiXiShu
	float ___m_fYanMie_DaWeiYiXiShu_40;
	// System.Single CTiaoZiManager::m_fYanMie_BianDaT
	float ___m_fYanMie_BianDaT_41;
	// System.Single CTiaoZiManager::m_fYanMie_BianXiaoT
	float ___m_fYanMie_BianXiaoT_42;
	// System.Single CTiaoZiManager::m_fYanMie_ChiXuT
	float ___m_fYanMie_ChiXuT_43;
	// System.Single CTiaoZiManager::m_fYanMie_XiaJiangXiShu
	float ___m_fYanMie_XiaJiangXiShu_44;
	// System.Single CTiaoZiManager::m_fYanMie_BianXiaoEndXiShu
	float ___m_fYanMie_BianXiaoEndXiShu_45;
	// System.Single CTiaoZiManager::m_fTunShi_ScaleXiShu
	float ___m_fTunShi_ScaleXiShu_47;
	// System.Single CTiaoZiManager::m_fTunShi_DistanceXiShu
	float ___m_fTunShi_DistanceXiShu_48;
	// System.Single CTiaoZiManager::m_fTunShi_XiaJiangXiShu
	float ___m_fTunShi_XiaJiangXiShu_49;
	// System.Single CTiaoZiManager::m_fTunShi_XiaJiangT
	float ___m_fTunShi_XiaJiangT_50;
	// System.Single CTiaoZiManager::m_fTunShi_ChiXuT
	float ___m_fTunShi_ChiXuT_51;
	// System.Single CTiaoZiManager::m_fTunShi_DirX
	float ___m_fTunShi_DirX_52;
	// System.Single CTiaoZiManager::m_fTunShi_DirY
	float ___m_fTunShi_DirY_53;
	// System.Single CTiaoZiManager::m_fTunShi_PushOffsetY
	float ___m_fTunShi_PushOffsetY_54;
	// System.Single CTiaoZiManager::m_fJinBi_ScaleXiShu
	float ___m_fJinBi_ScaleXiShu_55;
	// System.Single CTiaoZiManager::m_fShengJi_ChiXuTime
	float ___m_fShengJi_ChiXuTime_56;
	// System.Single CTiaoZiManager::m_fShengJi_DirX
	float ___m_fShengJi_DirX_57;
	// System.Single CTiaoZiManager::m_fShengJi_DirY
	float ___m_fShengJi_DirY_58;
	// System.Single CTiaoZiManager::m_fCiMianYi_ChiXuTime
	float ___m_fCiMianYi_ChiXuTime_59;
	// System.Single CTiaoZiManager::m_fYanMieMianYi_ChiXuTime
	float ___m_fYanMieMianYi_ChiXuTime_60;
	// System.Collections.Generic.List`1<CTiaoZi> CTiaoZiManager::m_lstTiaoZi
	List_1_t3123561718 * ___m_lstTiaoZi_61;
	// System.Collections.Generic.List`1<CTiaoZi> CTiaoZiManager::m_lstTiaoZi_YanMie
	List_1_t3123561718 * ___m_lstTiaoZi_YanMie_62;
	// System.Collections.Generic.List`1<CTiaoZi> CTiaoZiManager::m_lstTiaoZi_TunShi
	List_1_t3123561718 * ___m_lstTiaoZi_TunShi_63;
	// System.Collections.Generic.List`1<CTiaoZi> CTiaoZiManager::m_lstTiaoZi_JinBi
	List_1_t3123561718 * ___m_lstTiaoZi_JinBi_64;
	// System.Collections.Generic.List`1<CTiaoZi> CTiaoZiManager::m_lstQueue
	List_1_t3123561718 * ___m_lstQueue_65;

public:
	inline static int32_t get_offset_of_m_aryColors_3() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_aryColors_3)); }
	inline ColorU5BU5D_t941916413* get_m_aryColors_3() const { return ___m_aryColors_3; }
	inline ColorU5BU5D_t941916413** get_address_of_m_aryColors_3() { return &___m_aryColors_3; }
	inline void set_m_aryColors_3(ColorU5BU5D_t941916413* value)
	{
		___m_aryColors_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryColors_3), value);
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_TunShi_4() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_TunShi_4)); }
	inline float get_m_fTiaoZiCamThreshold_TunShi_4() const { return ___m_fTiaoZiCamThreshold_TunShi_4; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_TunShi_4() { return &___m_fTiaoZiCamThreshold_TunShi_4; }
	inline void set_m_fTiaoZiCamThreshold_TunShi_4(float value)
	{
		___m_fTiaoZiCamThreshold_TunShi_4 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_JinBi_5() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_JinBi_5)); }
	inline float get_m_fTiaoZiCamThreshold_JinBi_5() const { return ___m_fTiaoZiCamThreshold_JinBi_5; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_JinBi_5() { return &___m_fTiaoZiCamThreshold_JinBi_5; }
	inline void set_m_fTiaoZiCamThreshold_JinBi_5(float value)
	{
		___m_fTiaoZiCamThreshold_JinBi_5 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_ZhanCi_TiJi_6() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_ZhanCi_TiJi_6)); }
	inline float get_m_fTiaoZiCamThreshold_ZhanCi_TiJi_6() const { return ___m_fTiaoZiCamThreshold_ZhanCi_TiJi_6; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_ZhanCi_TiJi_6() { return &___m_fTiaoZiCamThreshold_ZhanCi_TiJi_6; }
	inline void set_m_fTiaoZiCamThreshold_ZhanCi_TiJi_6(float value)
	{
		___m_fTiaoZiCamThreshold_ZhanCi_TiJi_6 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_ZhanCi_JinQian_7() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_ZhanCi_JinQian_7)); }
	inline float get_m_fTiaoZiCamThreshold_ZhanCi_JinQian_7() const { return ___m_fTiaoZiCamThreshold_ZhanCi_JinQian_7; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_ZhanCi_JinQian_7() { return &___m_fTiaoZiCamThreshold_ZhanCi_JinQian_7; }
	inline void set_m_fTiaoZiCamThreshold_ZhanCi_JinQian_7(float value)
	{
		___m_fTiaoZiCamThreshold_ZhanCi_JinQian_7 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_TanKai_8() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_TanKai_8)); }
	inline float get_m_fTiaoZiCamThreshold_TanKai_8() const { return ___m_fTiaoZiCamThreshold_TanKai_8; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_TanKai_8() { return &___m_fTiaoZiCamThreshold_TanKai_8; }
	inline void set_m_fTiaoZiCamThreshold_TanKai_8(float value)
	{
		___m_fTiaoZiCamThreshold_TanKai_8 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_ShengJi_9() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_ShengJi_9)); }
	inline float get_m_fTiaoZiCamThreshold_ShengJi_9() const { return ___m_fTiaoZiCamThreshold_ShengJi_9; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_ShengJi_9() { return &___m_fTiaoZiCamThreshold_ShengJi_9; }
	inline void set_m_fTiaoZiCamThreshold_ShengJi_9(float value)
	{
		___m_fTiaoZiCamThreshold_ShengJi_9 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_CiMianYi_10() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_CiMianYi_10)); }
	inline float get_m_fTiaoZiCamThreshold_CiMianYi_10() const { return ___m_fTiaoZiCamThreshold_CiMianYi_10; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_CiMianYi_10() { return &___m_fTiaoZiCamThreshold_CiMianYi_10; }
	inline void set_m_fTiaoZiCamThreshold_CiMianYi_10(float value)
	{
		___m_fTiaoZiCamThreshold_CiMianYi_10 = value;
	}

	inline static int32_t get_offset_of_m_fTiaoZiCamThreshold_YanMieMianYi_11() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTiaoZiCamThreshold_YanMieMianYi_11)); }
	inline float get_m_fTiaoZiCamThreshold_YanMieMianYi_11() const { return ___m_fTiaoZiCamThreshold_YanMieMianYi_11; }
	inline float* get_address_of_m_fTiaoZiCamThreshold_YanMieMianYi_11() { return &___m_fTiaoZiCamThreshold_YanMieMianYi_11; }
	inline void set_m_fTiaoZiCamThreshold_YanMieMianYi_11(float value)
	{
		___m_fTiaoZiCamThreshold_YanMieMianYi_11 = value;
	}

	inline static int32_t get_offset_of_m_preTiaoZi_Test_12() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_preTiaoZi_Test_12)); }
	inline GameObject_t1113636619 * get_m_preTiaoZi_Test_12() const { return ___m_preTiaoZi_Test_12; }
	inline GameObject_t1113636619 ** get_address_of_m_preTiaoZi_Test_12() { return &___m_preTiaoZi_Test_12; }
	inline void set_m_preTiaoZi_Test_12(GameObject_t1113636619 * value)
	{
		___m_preTiaoZi_Test_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTiaoZi_Test_12), value);
	}

	inline static int32_t get_offset_of_m_preTiaoZi_13() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_preTiaoZi_13)); }
	inline GameObject_t1113636619 * get_m_preTiaoZi_13() const { return ___m_preTiaoZi_13; }
	inline GameObject_t1113636619 ** get_address_of_m_preTiaoZi_13() { return &___m_preTiaoZi_13; }
	inline void set_m_preTiaoZi_13(GameObject_t1113636619 * value)
	{
		___m_preTiaoZi_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTiaoZi_13), value);
	}

	inline static int32_t get_offset_of_m_preTiaoZi_YanMie_14() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_preTiaoZi_YanMie_14)); }
	inline GameObject_t1113636619 * get_m_preTiaoZi_YanMie_14() const { return ___m_preTiaoZi_YanMie_14; }
	inline GameObject_t1113636619 ** get_address_of_m_preTiaoZi_YanMie_14() { return &___m_preTiaoZi_YanMie_14; }
	inline void set_m_preTiaoZi_YanMie_14(GameObject_t1113636619 * value)
	{
		___m_preTiaoZi_YanMie_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTiaoZi_YanMie_14), value);
	}

	inline static int32_t get_offset_of_m_preTiaoZi_TunShi_15() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_preTiaoZi_TunShi_15)); }
	inline GameObject_t1113636619 * get_m_preTiaoZi_TunShi_15() const { return ___m_preTiaoZi_TunShi_15; }
	inline GameObject_t1113636619 ** get_address_of_m_preTiaoZi_TunShi_15() { return &___m_preTiaoZi_TunShi_15; }
	inline void set_m_preTiaoZi_TunShi_15(GameObject_t1113636619 * value)
	{
		___m_preTiaoZi_TunShi_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTiaoZi_TunShi_15), value);
	}

	inline static int32_t get_offset_of_m_preTiaoZi_JinBi_16() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_preTiaoZi_JinBi_16)); }
	inline GameObject_t1113636619 * get_m_preTiaoZi_JinBi_16() const { return ___m_preTiaoZi_JinBi_16; }
	inline GameObject_t1113636619 ** get_address_of_m_preTiaoZi_JinBi_16() { return &___m_preTiaoZi_JinBi_16; }
	inline void set_m_preTiaoZi_JinBi_16(GameObject_t1113636619 * value)
	{
		___m_preTiaoZi_JinBi_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTiaoZi_JinBi_16), value);
	}

	inline static int32_t get_offset_of_m_fPaoWuXianVx0_17() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fPaoWuXianVx0_17)); }
	inline float get_m_fPaoWuXianVx0_17() const { return ___m_fPaoWuXianVx0_17; }
	inline float* get_address_of_m_fPaoWuXianVx0_17() { return &___m_fPaoWuXianVx0_17; }
	inline void set_m_fPaoWuXianVx0_17(float value)
	{
		___m_fPaoWuXianVx0_17 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianVy0_18() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fPaoWuXianVy0_18)); }
	inline float get_m_fPaoWuXianVy0_18() const { return ___m_fPaoWuXianVy0_18; }
	inline float* get_address_of_m_fPaoWuXianVy0_18() { return &___m_fPaoWuXianVy0_18; }
	inline void set_m_fPaoWuXianVy0_18(float value)
	{
		___m_fPaoWuXianVy0_18 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianT_19() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fPaoWuXianT_19)); }
	inline float get_m_fPaoWuXianT_19() const { return ___m_fPaoWuXianT_19; }
	inline float* get_address_of_m_fPaoWuXianT_19() { return &___m_fPaoWuXianT_19; }
	inline void set_m_fPaoWuXianT_19(float value)
	{
		___m_fPaoWuXianT_19 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianA_20() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fPaoWuXianA_20)); }
	inline float get_m_fPaoWuXianA_20() const { return ___m_fPaoWuXianA_20; }
	inline float* get_address_of_m_fPaoWuXianA_20() { return &___m_fPaoWuXianA_20; }
	inline void set_m_fPaoWuXianA_20(float value)
	{
		___m_fPaoWuXianA_20 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianBanJingXiShu_21() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fPaoWuXianBanJingXiShu_21)); }
	inline float get_m_fPaoWuXianBanJingXiShu_21() const { return ___m_fPaoWuXianBanJingXiShu_21; }
	inline float* get_address_of_m_fPaoWuXianBanJingXiShu_21() { return &___m_fPaoWuXianBanJingXiShu_21; }
	inline void set_m_fPaoWuXianBanJingXiShu_21(float value)
	{
		___m_fPaoWuXianBanJingXiShu_21 = value;
	}

	inline static int32_t get_offset_of_m_fPaoWuXianSizeXiShu_22() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fPaoWuXianSizeXiShu_22)); }
	inline float get_m_fPaoWuXianSizeXiShu_22() const { return ___m_fPaoWuXianSizeXiShu_22; }
	inline float* get_address_of_m_fPaoWuXianSizeXiShu_22() { return &___m_fPaoWuXianSizeXiShu_22; }
	inline void set_m_fPaoWuXianSizeXiShu_22(float value)
	{
		___m_fPaoWuXianSizeXiShu_22 = value;
	}

	inline static int32_t get_offset_of_m_fBianDaT_23() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fBianDaT_23)); }
	inline float get_m_fBianDaT_23() const { return ___m_fBianDaT_23; }
	inline float* get_address_of_m_fBianDaT_23() { return &___m_fBianDaT_23; }
	inline void set_m_fBianDaT_23(float value)
	{
		___m_fBianDaT_23 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengT_24() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fShangShengT_24)); }
	inline float get_m_fShangShengT_24() const { return ___m_fShangShengT_24; }
	inline float* get_address_of_m_fShangShengT_24() { return &___m_fShangShengT_24; }
	inline void set_m_fShangShengT_24(float value)
	{
		___m_fShangShengT_24 = value;
	}

	inline static int32_t get_offset_of_m_fBianDaBanJingXiShu_25() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fBianDaBanJingXiShu_25)); }
	inline float get_m_fBianDaBanJingXiShu_25() const { return ___m_fBianDaBanJingXiShu_25; }
	inline float* get_address_of_m_fBianDaBanJingXiShu_25() { return &___m_fBianDaBanJingXiShu_25; }
	inline void set_m_fBianDaBanJingXiShu_25(float value)
	{
		___m_fBianDaBanJingXiShu_25 = value;
	}

	inline static int32_t get_offset_of_m_fShangShengBanJingXiShu_26() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fShangShengBanJingXiShu_26)); }
	inline float get_m_fShangShengBanJingXiShu_26() const { return ___m_fShangShengBanJingXiShu_26; }
	inline float* get_address_of_m_fShangShengBanJingXiShu_26() { return &___m_fShangShengBanJingXiShu_26; }
	inline void set_m_fShangShengBanJingXiShu_26(float value)
	{
		___m_fShangShengBanJingXiShu_26 = value;
	}

	inline static int32_t get_offset_of_m_fType3_ScaleXiShu_27() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fType3_ScaleXiShu_27)); }
	inline float get_m_fType3_ScaleXiShu_27() const { return ___m_fType3_ScaleXiShu_27; }
	inline float* get_address_of_m_fType3_ScaleXiShu_27() { return &___m_fType3_ScaleXiShu_27; }
	inline void set_m_fType3_ScaleXiShu_27(float value)
	{
		___m_fType3_ScaleXiShu_27 = value;
	}

	inline static int32_t get_offset_of_m_fType3_BianDaT_28() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fType3_BianDaT_28)); }
	inline float get_m_fType3_BianDaT_28() const { return ___m_fType3_BianDaT_28; }
	inline float* get_address_of_m_fType3_BianDaT_28() { return &___m_fType3_BianDaT_28; }
	inline void set_m_fType3_BianDaT_28(float value)
	{
		___m_fType3_BianDaT_28 = value;
	}

	inline static int32_t get_offset_of_m_fType3_BianXiaoT_29() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fType3_BianXiaoT_29)); }
	inline float get_m_fType3_BianXiaoT_29() const { return ___m_fType3_BianXiaoT_29; }
	inline float* get_address_of_m_fType3_BianXiaoT_29() { return &___m_fType3_BianXiaoT_29; }
	inline void set_m_fType3_BianXiaoT_29(float value)
	{
		___m_fType3_BianXiaoT_29 = value;
	}

	inline static int32_t get_offset_of_m_fType3_ChiXuT_30() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fType3_ChiXuT_30)); }
	inline float get_m_fType3_ChiXuT_30() const { return ___m_fType3_ChiXuT_30; }
	inline float* get_address_of_m_fType3_ChiXuT_30() { return &___m_fType3_ChiXuT_30; }
	inline void set_m_fType3_ChiXuT_30(float value)
	{
		___m_fType3_ChiXuT_30 = value;
	}

	inline static int32_t get_offset_of_m_fType3_OffsetXiShuX_31() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fType3_OffsetXiShuX_31)); }
	inline float get_m_fType3_OffsetXiShuX_31() const { return ___m_fType3_OffsetXiShuX_31; }
	inline float* get_address_of_m_fType3_OffsetXiShuX_31() { return &___m_fType3_OffsetXiShuX_31; }
	inline void set_m_fType3_OffsetXiShuX_31(float value)
	{
		___m_fType3_OffsetXiShuX_31 = value;
	}

	inline static int32_t get_offset_of_m_fType3_OffsetXiShuY_32() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fType3_OffsetXiShuY_32)); }
	inline float get_m_fType3_OffsetXiShuY_32() const { return ___m_fType3_OffsetXiShuY_32; }
	inline float* get_address_of_m_fType3_OffsetXiShuY_32() { return &___m_fType3_OffsetXiShuY_32; }
	inline void set_m_fType3_OffsetXiShuY_32(float value)
	{
		___m_fType3_OffsetXiShuY_32 = value;
	}

	inline static int32_t get_offset_of_ShengJi_ScaleXiShu_33() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___ShengJi_ScaleXiShu_33)); }
	inline float get_ShengJi_ScaleXiShu_33() const { return ___ShengJi_ScaleXiShu_33; }
	inline float* get_address_of_ShengJi_ScaleXiShu_33() { return &___ShengJi_ScaleXiShu_33; }
	inline void set_ShengJi_ScaleXiShu_33(float value)
	{
		___ShengJi_ScaleXiShu_33 = value;
	}

	inline static int32_t get_offset_of_ShengJi_BianDaT_34() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___ShengJi_BianDaT_34)); }
	inline float get_ShengJi_BianDaT_34() const { return ___ShengJi_BianDaT_34; }
	inline float* get_address_of_ShengJi_BianDaT_34() { return &___ShengJi_BianDaT_34; }
	inline void set_ShengJi_BianDaT_34(float value)
	{
		___ShengJi_BianDaT_34 = value;
	}

	inline static int32_t get_offset_of_ShengJi_BianXiaoT_35() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___ShengJi_BianXiaoT_35)); }
	inline float get_ShengJi_BianXiaoT_35() const { return ___ShengJi_BianXiaoT_35; }
	inline float* get_address_of_ShengJi_BianXiaoT_35() { return &___ShengJi_BianXiaoT_35; }
	inline void set_ShengJi_BianXiaoT_35(float value)
	{
		___ShengJi_BianXiaoT_35 = value;
	}

	inline static int32_t get_offset_of_ShengJi_ChiXuT_36() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___ShengJi_ChiXuT_36)); }
	inline float get_ShengJi_ChiXuT_36() const { return ___ShengJi_ChiXuT_36; }
	inline float* get_address_of_ShengJi_ChiXuT_36() { return &___ShengJi_ChiXuT_36; }
	inline void set_ShengJi_ChiXuT_36(float value)
	{
		___ShengJi_ChiXuT_36 = value;
	}

	inline static int32_t get_offset_of_ShengJi_OffsetXiShuX_37() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___ShengJi_OffsetXiShuX_37)); }
	inline float get_ShengJi_OffsetXiShuX_37() const { return ___ShengJi_OffsetXiShuX_37; }
	inline float* get_address_of_ShengJi_OffsetXiShuX_37() { return &___ShengJi_OffsetXiShuX_37; }
	inline void set_ShengJi_OffsetXiShuX_37(float value)
	{
		___ShengJi_OffsetXiShuX_37 = value;
	}

	inline static int32_t get_offset_of_ShengJi_OffsetXiShuY_38() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___ShengJi_OffsetXiShuY_38)); }
	inline float get_ShengJi_OffsetXiShuY_38() const { return ___ShengJi_OffsetXiShuY_38; }
	inline float* get_address_of_ShengJi_OffsetXiShuY_38() { return &___ShengJi_OffsetXiShuY_38; }
	inline void set_ShengJi_OffsetXiShuY_38(float value)
	{
		___ShengJi_OffsetXiShuY_38 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_ScaleXiShu_39() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_ScaleXiShu_39)); }
	inline float get_m_fYanMie_ScaleXiShu_39() const { return ___m_fYanMie_ScaleXiShu_39; }
	inline float* get_address_of_m_fYanMie_ScaleXiShu_39() { return &___m_fYanMie_ScaleXiShu_39; }
	inline void set_m_fYanMie_ScaleXiShu_39(float value)
	{
		___m_fYanMie_ScaleXiShu_39 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_DaWeiYiXiShu_40() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_DaWeiYiXiShu_40)); }
	inline float get_m_fYanMie_DaWeiYiXiShu_40() const { return ___m_fYanMie_DaWeiYiXiShu_40; }
	inline float* get_address_of_m_fYanMie_DaWeiYiXiShu_40() { return &___m_fYanMie_DaWeiYiXiShu_40; }
	inline void set_m_fYanMie_DaWeiYiXiShu_40(float value)
	{
		___m_fYanMie_DaWeiYiXiShu_40 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianDaT_41() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_BianDaT_41)); }
	inline float get_m_fYanMie_BianDaT_41() const { return ___m_fYanMie_BianDaT_41; }
	inline float* get_address_of_m_fYanMie_BianDaT_41() { return &___m_fYanMie_BianDaT_41; }
	inline void set_m_fYanMie_BianDaT_41(float value)
	{
		___m_fYanMie_BianDaT_41 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianXiaoT_42() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_BianXiaoT_42)); }
	inline float get_m_fYanMie_BianXiaoT_42() const { return ___m_fYanMie_BianXiaoT_42; }
	inline float* get_address_of_m_fYanMie_BianXiaoT_42() { return &___m_fYanMie_BianXiaoT_42; }
	inline void set_m_fYanMie_BianXiaoT_42(float value)
	{
		___m_fYanMie_BianXiaoT_42 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_ChiXuT_43() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_ChiXuT_43)); }
	inline float get_m_fYanMie_ChiXuT_43() const { return ___m_fYanMie_ChiXuT_43; }
	inline float* get_address_of_m_fYanMie_ChiXuT_43() { return &___m_fYanMie_ChiXuT_43; }
	inline void set_m_fYanMie_ChiXuT_43(float value)
	{
		___m_fYanMie_ChiXuT_43 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_XiaJiangXiShu_44() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_XiaJiangXiShu_44)); }
	inline float get_m_fYanMie_XiaJiangXiShu_44() const { return ___m_fYanMie_XiaJiangXiShu_44; }
	inline float* get_address_of_m_fYanMie_XiaJiangXiShu_44() { return &___m_fYanMie_XiaJiangXiShu_44; }
	inline void set_m_fYanMie_XiaJiangXiShu_44(float value)
	{
		___m_fYanMie_XiaJiangXiShu_44 = value;
	}

	inline static int32_t get_offset_of_m_fYanMie_BianXiaoEndXiShu_45() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMie_BianXiaoEndXiShu_45)); }
	inline float get_m_fYanMie_BianXiaoEndXiShu_45() const { return ___m_fYanMie_BianXiaoEndXiShu_45; }
	inline float* get_address_of_m_fYanMie_BianXiaoEndXiShu_45() { return &___m_fYanMie_BianXiaoEndXiShu_45; }
	inline void set_m_fYanMie_BianXiaoEndXiShu_45(float value)
	{
		___m_fYanMie_BianXiaoEndXiShu_45 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_ScaleXiShu_47() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_ScaleXiShu_47)); }
	inline float get_m_fTunShi_ScaleXiShu_47() const { return ___m_fTunShi_ScaleXiShu_47; }
	inline float* get_address_of_m_fTunShi_ScaleXiShu_47() { return &___m_fTunShi_ScaleXiShu_47; }
	inline void set_m_fTunShi_ScaleXiShu_47(float value)
	{
		___m_fTunShi_ScaleXiShu_47 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_DistanceXiShu_48() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_DistanceXiShu_48)); }
	inline float get_m_fTunShi_DistanceXiShu_48() const { return ___m_fTunShi_DistanceXiShu_48; }
	inline float* get_address_of_m_fTunShi_DistanceXiShu_48() { return &___m_fTunShi_DistanceXiShu_48; }
	inline void set_m_fTunShi_DistanceXiShu_48(float value)
	{
		___m_fTunShi_DistanceXiShu_48 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_XiaJiangXiShu_49() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_XiaJiangXiShu_49)); }
	inline float get_m_fTunShi_XiaJiangXiShu_49() const { return ___m_fTunShi_XiaJiangXiShu_49; }
	inline float* get_address_of_m_fTunShi_XiaJiangXiShu_49() { return &___m_fTunShi_XiaJiangXiShu_49; }
	inline void set_m_fTunShi_XiaJiangXiShu_49(float value)
	{
		___m_fTunShi_XiaJiangXiShu_49 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_XiaJiangT_50() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_XiaJiangT_50)); }
	inline float get_m_fTunShi_XiaJiangT_50() const { return ___m_fTunShi_XiaJiangT_50; }
	inline float* get_address_of_m_fTunShi_XiaJiangT_50() { return &___m_fTunShi_XiaJiangT_50; }
	inline void set_m_fTunShi_XiaJiangT_50(float value)
	{
		___m_fTunShi_XiaJiangT_50 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_ChiXuT_51() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_ChiXuT_51)); }
	inline float get_m_fTunShi_ChiXuT_51() const { return ___m_fTunShi_ChiXuT_51; }
	inline float* get_address_of_m_fTunShi_ChiXuT_51() { return &___m_fTunShi_ChiXuT_51; }
	inline void set_m_fTunShi_ChiXuT_51(float value)
	{
		___m_fTunShi_ChiXuT_51 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_DirX_52() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_DirX_52)); }
	inline float get_m_fTunShi_DirX_52() const { return ___m_fTunShi_DirX_52; }
	inline float* get_address_of_m_fTunShi_DirX_52() { return &___m_fTunShi_DirX_52; }
	inline void set_m_fTunShi_DirX_52(float value)
	{
		___m_fTunShi_DirX_52 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_DirY_53() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_DirY_53)); }
	inline float get_m_fTunShi_DirY_53() const { return ___m_fTunShi_DirY_53; }
	inline float* get_address_of_m_fTunShi_DirY_53() { return &___m_fTunShi_DirY_53; }
	inline void set_m_fTunShi_DirY_53(float value)
	{
		___m_fTunShi_DirY_53 = value;
	}

	inline static int32_t get_offset_of_m_fTunShi_PushOffsetY_54() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fTunShi_PushOffsetY_54)); }
	inline float get_m_fTunShi_PushOffsetY_54() const { return ___m_fTunShi_PushOffsetY_54; }
	inline float* get_address_of_m_fTunShi_PushOffsetY_54() { return &___m_fTunShi_PushOffsetY_54; }
	inline void set_m_fTunShi_PushOffsetY_54(float value)
	{
		___m_fTunShi_PushOffsetY_54 = value;
	}

	inline static int32_t get_offset_of_m_fJinBi_ScaleXiShu_55() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fJinBi_ScaleXiShu_55)); }
	inline float get_m_fJinBi_ScaleXiShu_55() const { return ___m_fJinBi_ScaleXiShu_55; }
	inline float* get_address_of_m_fJinBi_ScaleXiShu_55() { return &___m_fJinBi_ScaleXiShu_55; }
	inline void set_m_fJinBi_ScaleXiShu_55(float value)
	{
		___m_fJinBi_ScaleXiShu_55 = value;
	}

	inline static int32_t get_offset_of_m_fShengJi_ChiXuTime_56() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fShengJi_ChiXuTime_56)); }
	inline float get_m_fShengJi_ChiXuTime_56() const { return ___m_fShengJi_ChiXuTime_56; }
	inline float* get_address_of_m_fShengJi_ChiXuTime_56() { return &___m_fShengJi_ChiXuTime_56; }
	inline void set_m_fShengJi_ChiXuTime_56(float value)
	{
		___m_fShengJi_ChiXuTime_56 = value;
	}

	inline static int32_t get_offset_of_m_fShengJi_DirX_57() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fShengJi_DirX_57)); }
	inline float get_m_fShengJi_DirX_57() const { return ___m_fShengJi_DirX_57; }
	inline float* get_address_of_m_fShengJi_DirX_57() { return &___m_fShengJi_DirX_57; }
	inline void set_m_fShengJi_DirX_57(float value)
	{
		___m_fShengJi_DirX_57 = value;
	}

	inline static int32_t get_offset_of_m_fShengJi_DirY_58() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fShengJi_DirY_58)); }
	inline float get_m_fShengJi_DirY_58() const { return ___m_fShengJi_DirY_58; }
	inline float* get_address_of_m_fShengJi_DirY_58() { return &___m_fShengJi_DirY_58; }
	inline void set_m_fShengJi_DirY_58(float value)
	{
		___m_fShengJi_DirY_58 = value;
	}

	inline static int32_t get_offset_of_m_fCiMianYi_ChiXuTime_59() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fCiMianYi_ChiXuTime_59)); }
	inline float get_m_fCiMianYi_ChiXuTime_59() const { return ___m_fCiMianYi_ChiXuTime_59; }
	inline float* get_address_of_m_fCiMianYi_ChiXuTime_59() { return &___m_fCiMianYi_ChiXuTime_59; }
	inline void set_m_fCiMianYi_ChiXuTime_59(float value)
	{
		___m_fCiMianYi_ChiXuTime_59 = value;
	}

	inline static int32_t get_offset_of_m_fYanMieMianYi_ChiXuTime_60() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_fYanMieMianYi_ChiXuTime_60)); }
	inline float get_m_fYanMieMianYi_ChiXuTime_60() const { return ___m_fYanMieMianYi_ChiXuTime_60; }
	inline float* get_address_of_m_fYanMieMianYi_ChiXuTime_60() { return &___m_fYanMieMianYi_ChiXuTime_60; }
	inline void set_m_fYanMieMianYi_ChiXuTime_60(float value)
	{
		___m_fYanMieMianYi_ChiXuTime_60 = value;
	}

	inline static int32_t get_offset_of_m_lstTiaoZi_61() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_lstTiaoZi_61)); }
	inline List_1_t3123561718 * get_m_lstTiaoZi_61() const { return ___m_lstTiaoZi_61; }
	inline List_1_t3123561718 ** get_address_of_m_lstTiaoZi_61() { return &___m_lstTiaoZi_61; }
	inline void set_m_lstTiaoZi_61(List_1_t3123561718 * value)
	{
		___m_lstTiaoZi_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTiaoZi_61), value);
	}

	inline static int32_t get_offset_of_m_lstTiaoZi_YanMie_62() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_lstTiaoZi_YanMie_62)); }
	inline List_1_t3123561718 * get_m_lstTiaoZi_YanMie_62() const { return ___m_lstTiaoZi_YanMie_62; }
	inline List_1_t3123561718 ** get_address_of_m_lstTiaoZi_YanMie_62() { return &___m_lstTiaoZi_YanMie_62; }
	inline void set_m_lstTiaoZi_YanMie_62(List_1_t3123561718 * value)
	{
		___m_lstTiaoZi_YanMie_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTiaoZi_YanMie_62), value);
	}

	inline static int32_t get_offset_of_m_lstTiaoZi_TunShi_63() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_lstTiaoZi_TunShi_63)); }
	inline List_1_t3123561718 * get_m_lstTiaoZi_TunShi_63() const { return ___m_lstTiaoZi_TunShi_63; }
	inline List_1_t3123561718 ** get_address_of_m_lstTiaoZi_TunShi_63() { return &___m_lstTiaoZi_TunShi_63; }
	inline void set_m_lstTiaoZi_TunShi_63(List_1_t3123561718 * value)
	{
		___m_lstTiaoZi_TunShi_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTiaoZi_TunShi_63), value);
	}

	inline static int32_t get_offset_of_m_lstTiaoZi_JinBi_64() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_lstTiaoZi_JinBi_64)); }
	inline List_1_t3123561718 * get_m_lstTiaoZi_JinBi_64() const { return ___m_lstTiaoZi_JinBi_64; }
	inline List_1_t3123561718 ** get_address_of_m_lstTiaoZi_JinBi_64() { return &___m_lstTiaoZi_JinBi_64; }
	inline void set_m_lstTiaoZi_JinBi_64(List_1_t3123561718 * value)
	{
		___m_lstTiaoZi_JinBi_64 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTiaoZi_JinBi_64), value);
	}

	inline static int32_t get_offset_of_m_lstQueue_65() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989, ___m_lstQueue_65)); }
	inline List_1_t3123561718 * get_m_lstQueue_65() const { return ___m_lstQueue_65; }
	inline List_1_t3123561718 ** get_address_of_m_lstQueue_65() { return &___m_lstQueue_65; }
	inline void set_m_lstQueue_65(List_1_t3123561718 * value)
	{
		___m_lstQueue_65 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstQueue_65), value);
	}
};

struct CTiaoZiManager_t3709983989_StaticFields
{
public:
	// CTiaoZiManager CTiaoZiManager::s_Instance
	CTiaoZiManager_t3709983989 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CTiaoZiManager_t3709983989_StaticFields, ___s_Instance_2)); }
	inline CTiaoZiManager_t3709983989 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CTiaoZiManager_t3709983989 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CTiaoZiManager_t3709983989 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTIAOZIMANAGER_T3709983989_H
#ifndef CDUST_T3021183826_H
#define CDUST_T3021183826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CDust
struct  CDust_t3021183826  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer CDust::_srLight
	SpriteRenderer_t3235626157 * ____srLight_2;
	// UnityEngine.Rigidbody2D CDust::_rigid
	Rigidbody2D_t939494601 * ____rigid_7;
	// UnityEngine.Collider2D CDust::_collider
	Collider2D_t2806799626 * ____collider_8;
	// UnityEngine.Vector2 CDust::m_vec2StartPos
	Vector2_t2156229523  ___m_vec2StartPos_9;
	// UnityEngine.Vector3 CDust::m_vecPosInOrbit
	Vector3_t3722313464  ___m_vecPosInOrbit_10;
	// System.Single CDust::m_fRotation
	float ___m_fRotation_11;
	// UnityEngine.Vector3 CDust::vecTempDirection
	Vector3_t3722313464  ___vecTempDirection_12;
	// System.Single CDust::m_fOrbitRaduis
	float ___m_fOrbitRaduis_13;
	// System.Single CDust::m_fDeltaX
	float ___m_fDeltaX_14;
	// System.Single CDust::m_fDeltaY
	float ___m_fDeltaY_15;
	// UnityEngine.SpriteRenderer CDust::_srMain
	SpriteRenderer_t3235626157 * ____srMain_16;
	// System.Int32 CDust::m_nId
	int32_t ___m_nId_17;
	// System.Single CDust::m_fCollisionTime
	float ___m_fCollisionTime_18;
	// System.Boolean CDust::m_bActive
	bool ___m_bActive_19;
	// System.Boolean CDust::m_bInCamView
	bool ___m_bInCamView_20;
	// System.Single CDust::m_fSyncedTime
	float ___m_fSyncedTime_21;
	// System.Single CDust::m_fBlingTime
	float ___m_fBlingTime_22;
	// System.Int32 CDust::m_nOp
	int32_t ___m_nOp_23;
	// System.Int32 CDust::m_nOpScale
	int32_t ___m_nOpScale_24;

public:
	inline static int32_t get_offset_of__srLight_2() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ____srLight_2)); }
	inline SpriteRenderer_t3235626157 * get__srLight_2() const { return ____srLight_2; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srLight_2() { return &____srLight_2; }
	inline void set__srLight_2(SpriteRenderer_t3235626157 * value)
	{
		____srLight_2 = value;
		Il2CppCodeGenWriteBarrier((&____srLight_2), value);
	}

	inline static int32_t get_offset_of__rigid_7() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ____rigid_7)); }
	inline Rigidbody2D_t939494601 * get__rigid_7() const { return ____rigid_7; }
	inline Rigidbody2D_t939494601 ** get_address_of__rigid_7() { return &____rigid_7; }
	inline void set__rigid_7(Rigidbody2D_t939494601 * value)
	{
		____rigid_7 = value;
		Il2CppCodeGenWriteBarrier((&____rigid_7), value);
	}

	inline static int32_t get_offset_of__collider_8() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ____collider_8)); }
	inline Collider2D_t2806799626 * get__collider_8() const { return ____collider_8; }
	inline Collider2D_t2806799626 ** get_address_of__collider_8() { return &____collider_8; }
	inline void set__collider_8(Collider2D_t2806799626 * value)
	{
		____collider_8 = value;
		Il2CppCodeGenWriteBarrier((&____collider_8), value);
	}

	inline static int32_t get_offset_of_m_vec2StartPos_9() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_vec2StartPos_9)); }
	inline Vector2_t2156229523  get_m_vec2StartPos_9() const { return ___m_vec2StartPos_9; }
	inline Vector2_t2156229523 * get_address_of_m_vec2StartPos_9() { return &___m_vec2StartPos_9; }
	inline void set_m_vec2StartPos_9(Vector2_t2156229523  value)
	{
		___m_vec2StartPos_9 = value;
	}

	inline static int32_t get_offset_of_m_vecPosInOrbit_10() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_vecPosInOrbit_10)); }
	inline Vector3_t3722313464  get_m_vecPosInOrbit_10() const { return ___m_vecPosInOrbit_10; }
	inline Vector3_t3722313464 * get_address_of_m_vecPosInOrbit_10() { return &___m_vecPosInOrbit_10; }
	inline void set_m_vecPosInOrbit_10(Vector3_t3722313464  value)
	{
		___m_vecPosInOrbit_10 = value;
	}

	inline static int32_t get_offset_of_m_fRotation_11() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fRotation_11)); }
	inline float get_m_fRotation_11() const { return ___m_fRotation_11; }
	inline float* get_address_of_m_fRotation_11() { return &___m_fRotation_11; }
	inline void set_m_fRotation_11(float value)
	{
		___m_fRotation_11 = value;
	}

	inline static int32_t get_offset_of_vecTempDirection_12() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___vecTempDirection_12)); }
	inline Vector3_t3722313464  get_vecTempDirection_12() const { return ___vecTempDirection_12; }
	inline Vector3_t3722313464 * get_address_of_vecTempDirection_12() { return &___vecTempDirection_12; }
	inline void set_vecTempDirection_12(Vector3_t3722313464  value)
	{
		___vecTempDirection_12 = value;
	}

	inline static int32_t get_offset_of_m_fOrbitRaduis_13() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fOrbitRaduis_13)); }
	inline float get_m_fOrbitRaduis_13() const { return ___m_fOrbitRaduis_13; }
	inline float* get_address_of_m_fOrbitRaduis_13() { return &___m_fOrbitRaduis_13; }
	inline void set_m_fOrbitRaduis_13(float value)
	{
		___m_fOrbitRaduis_13 = value;
	}

	inline static int32_t get_offset_of_m_fDeltaX_14() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fDeltaX_14)); }
	inline float get_m_fDeltaX_14() const { return ___m_fDeltaX_14; }
	inline float* get_address_of_m_fDeltaX_14() { return &___m_fDeltaX_14; }
	inline void set_m_fDeltaX_14(float value)
	{
		___m_fDeltaX_14 = value;
	}

	inline static int32_t get_offset_of_m_fDeltaY_15() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fDeltaY_15)); }
	inline float get_m_fDeltaY_15() const { return ___m_fDeltaY_15; }
	inline float* get_address_of_m_fDeltaY_15() { return &___m_fDeltaY_15; }
	inline void set_m_fDeltaY_15(float value)
	{
		___m_fDeltaY_15 = value;
	}

	inline static int32_t get_offset_of__srMain_16() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ____srMain_16)); }
	inline SpriteRenderer_t3235626157 * get__srMain_16() const { return ____srMain_16; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_16() { return &____srMain_16; }
	inline void set__srMain_16(SpriteRenderer_t3235626157 * value)
	{
		____srMain_16 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_16), value);
	}

	inline static int32_t get_offset_of_m_nId_17() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_nId_17)); }
	inline int32_t get_m_nId_17() const { return ___m_nId_17; }
	inline int32_t* get_address_of_m_nId_17() { return &___m_nId_17; }
	inline void set_m_nId_17(int32_t value)
	{
		___m_nId_17 = value;
	}

	inline static int32_t get_offset_of_m_fCollisionTime_18() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fCollisionTime_18)); }
	inline float get_m_fCollisionTime_18() const { return ___m_fCollisionTime_18; }
	inline float* get_address_of_m_fCollisionTime_18() { return &___m_fCollisionTime_18; }
	inline void set_m_fCollisionTime_18(float value)
	{
		___m_fCollisionTime_18 = value;
	}

	inline static int32_t get_offset_of_m_bActive_19() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_bActive_19)); }
	inline bool get_m_bActive_19() const { return ___m_bActive_19; }
	inline bool* get_address_of_m_bActive_19() { return &___m_bActive_19; }
	inline void set_m_bActive_19(bool value)
	{
		___m_bActive_19 = value;
	}

	inline static int32_t get_offset_of_m_bInCamView_20() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_bInCamView_20)); }
	inline bool get_m_bInCamView_20() const { return ___m_bInCamView_20; }
	inline bool* get_address_of_m_bInCamView_20() { return &___m_bInCamView_20; }
	inline void set_m_bInCamView_20(bool value)
	{
		___m_bInCamView_20 = value;
	}

	inline static int32_t get_offset_of_m_fSyncedTime_21() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fSyncedTime_21)); }
	inline float get_m_fSyncedTime_21() const { return ___m_fSyncedTime_21; }
	inline float* get_address_of_m_fSyncedTime_21() { return &___m_fSyncedTime_21; }
	inline void set_m_fSyncedTime_21(float value)
	{
		___m_fSyncedTime_21 = value;
	}

	inline static int32_t get_offset_of_m_fBlingTime_22() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_fBlingTime_22)); }
	inline float get_m_fBlingTime_22() const { return ___m_fBlingTime_22; }
	inline float* get_address_of_m_fBlingTime_22() { return &___m_fBlingTime_22; }
	inline void set_m_fBlingTime_22(float value)
	{
		___m_fBlingTime_22 = value;
	}

	inline static int32_t get_offset_of_m_nOp_23() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_nOp_23)); }
	inline int32_t get_m_nOp_23() const { return ___m_nOp_23; }
	inline int32_t* get_address_of_m_nOp_23() { return &___m_nOp_23; }
	inline void set_m_nOp_23(int32_t value)
	{
		___m_nOp_23 = value;
	}

	inline static int32_t get_offset_of_m_nOpScale_24() { return static_cast<int32_t>(offsetof(CDust_t3021183826, ___m_nOpScale_24)); }
	inline int32_t get_m_nOpScale_24() const { return ___m_nOpScale_24; }
	inline int32_t* get_address_of_m_nOpScale_24() { return &___m_nOpScale_24; }
	inline void set_m_nOpScale_24(int32_t value)
	{
		___m_nOpScale_24 = value;
	}
};

struct CDust_t3021183826_StaticFields
{
public:
	// UnityEngine.Color CDust::colorTemp
	Color_t2555686324  ___colorTemp_3;
	// UnityEngine.Vector3 CDust::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;
	// UnityEngine.Vector3 CDust::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Quaternion CDust::vecTempRotation
	Quaternion_t2301928331  ___vecTempRotation_6;

public:
	inline static int32_t get_offset_of_colorTemp_3() { return static_cast<int32_t>(offsetof(CDust_t3021183826_StaticFields, ___colorTemp_3)); }
	inline Color_t2555686324  get_colorTemp_3() const { return ___colorTemp_3; }
	inline Color_t2555686324 * get_address_of_colorTemp_3() { return &___colorTemp_3; }
	inline void set_colorTemp_3(Color_t2555686324  value)
	{
		___colorTemp_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CDust_t3021183826_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CDust_t3021183826_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_vecTempRotation_6() { return static_cast<int32_t>(offsetof(CDust_t3021183826_StaticFields, ___vecTempRotation_6)); }
	inline Quaternion_t2301928331  get_vecTempRotation_6() const { return ___vecTempRotation_6; }
	inline Quaternion_t2301928331 * get_address_of_vecTempRotation_6() { return &___vecTempRotation_6; }
	inline void set_vecTempRotation_6(Quaternion_t2301928331  value)
	{
		___vecTempRotation_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CDUST_T3021183826_H
#ifndef CMSGBOX_T3123528476_H
#define CMSGBOX_T3123528476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMsgBox
struct  CMsgBox_t3123528476  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CMsgBox::m_fTotalShowTime
	float ___m_fTotalShowTime_3;
	// System.Single CMsgBox::m_fCurShowTimeLapse
	float ___m_fCurShowTimeLapse_4;
	// UnityEngine.GameObject CMsgBox::_goContainer
	GameObject_t1113636619 * ____goContainer_5;
	// UnityEngine.UI.Text CMsgBox::_txtContent
	Text_t1901882714 * ____txtContent_6;

public:
	inline static int32_t get_offset_of_m_fTotalShowTime_3() { return static_cast<int32_t>(offsetof(CMsgBox_t3123528476, ___m_fTotalShowTime_3)); }
	inline float get_m_fTotalShowTime_3() const { return ___m_fTotalShowTime_3; }
	inline float* get_address_of_m_fTotalShowTime_3() { return &___m_fTotalShowTime_3; }
	inline void set_m_fTotalShowTime_3(float value)
	{
		___m_fTotalShowTime_3 = value;
	}

	inline static int32_t get_offset_of_m_fCurShowTimeLapse_4() { return static_cast<int32_t>(offsetof(CMsgBox_t3123528476, ___m_fCurShowTimeLapse_4)); }
	inline float get_m_fCurShowTimeLapse_4() const { return ___m_fCurShowTimeLapse_4; }
	inline float* get_address_of_m_fCurShowTimeLapse_4() { return &___m_fCurShowTimeLapse_4; }
	inline void set_m_fCurShowTimeLapse_4(float value)
	{
		___m_fCurShowTimeLapse_4 = value;
	}

	inline static int32_t get_offset_of__goContainer_5() { return static_cast<int32_t>(offsetof(CMsgBox_t3123528476, ____goContainer_5)); }
	inline GameObject_t1113636619 * get__goContainer_5() const { return ____goContainer_5; }
	inline GameObject_t1113636619 ** get_address_of__goContainer_5() { return &____goContainer_5; }
	inline void set__goContainer_5(GameObject_t1113636619 * value)
	{
		____goContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____goContainer_5), value);
	}

	inline static int32_t get_offset_of__txtContent_6() { return static_cast<int32_t>(offsetof(CMsgBox_t3123528476, ____txtContent_6)); }
	inline Text_t1901882714 * get__txtContent_6() const { return ____txtContent_6; }
	inline Text_t1901882714 ** get_address_of__txtContent_6() { return &____txtContent_6; }
	inline void set__txtContent_6(Text_t1901882714 * value)
	{
		____txtContent_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_6), value);
	}
};

struct CMsgBox_t3123528476_StaticFields
{
public:
	// CMsgBox CMsgBox::s_Instance
	CMsgBox_t3123528476 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CMsgBox_t3123528476_StaticFields, ___s_Instance_2)); }
	inline CMsgBox_t3123528476 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CMsgBox_t3123528476 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CMsgBox_t3123528476 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSGBOX_T3123528476_H
#ifndef CMSGLIST_T4144960221_H
#define CMSGLIST_T4144960221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMsgList
struct  CMsgList_t4144960221  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CMsgList::m_preMsgListItem
	GameObject_t1113636619 * ___m_preMsgListItem_2;
	// UnityEngine.UI.Image CMsgList::m_preAssistAvatar
	Image_t2670269651 * ___m_preAssistAvatar_3;
	// System.Single CMsgList::m_fVertSapce
	float ___m_fVertSapce_4;
	// System.Single CMsgList::m_fItemScale
	float ___m_fItemScale_5;
	// System.Single CMsgList::m_fMoveInterval
	float ___m_fMoveInterval_6;
	// System.Single CMsgList::m_fMoveTime
	float ___m_fMoveTime_7;
	// System.Single CMsgList::m_fMoveSpeed
	float ___m_fMoveSpeed_8;
	// System.Collections.Generic.List`1<CMsgListItem> CMsgList::m_lstItems
	List_1_t2611397916 * ___m_lstItems_9;
	// System.Collections.Generic.List`1<CMsgListItem> CMsgList::m_lstRecycledItems
	List_1_t2611397916 * ___m_lstRecycledItems_10;
	// System.Boolean CMsgList::m_bMoving
	bool ___m_bMoving_14;
	// System.Single CMsgList::m_fMoveIntervalCount
	float ___m_fMoveIntervalCount_15;
	// System.Single CMsgList::m_fMoveTimeCount
	float ___m_fMoveTimeCount_16;

public:
	inline static int32_t get_offset_of_m_preMsgListItem_2() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_preMsgListItem_2)); }
	inline GameObject_t1113636619 * get_m_preMsgListItem_2() const { return ___m_preMsgListItem_2; }
	inline GameObject_t1113636619 ** get_address_of_m_preMsgListItem_2() { return &___m_preMsgListItem_2; }
	inline void set_m_preMsgListItem_2(GameObject_t1113636619 * value)
	{
		___m_preMsgListItem_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_preMsgListItem_2), value);
	}

	inline static int32_t get_offset_of_m_preAssistAvatar_3() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_preAssistAvatar_3)); }
	inline Image_t2670269651 * get_m_preAssistAvatar_3() const { return ___m_preAssistAvatar_3; }
	inline Image_t2670269651 ** get_address_of_m_preAssistAvatar_3() { return &___m_preAssistAvatar_3; }
	inline void set_m_preAssistAvatar_3(Image_t2670269651 * value)
	{
		___m_preAssistAvatar_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_preAssistAvatar_3), value);
	}

	inline static int32_t get_offset_of_m_fVertSapce_4() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fVertSapce_4)); }
	inline float get_m_fVertSapce_4() const { return ___m_fVertSapce_4; }
	inline float* get_address_of_m_fVertSapce_4() { return &___m_fVertSapce_4; }
	inline void set_m_fVertSapce_4(float value)
	{
		___m_fVertSapce_4 = value;
	}

	inline static int32_t get_offset_of_m_fItemScale_5() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fItemScale_5)); }
	inline float get_m_fItemScale_5() const { return ___m_fItemScale_5; }
	inline float* get_address_of_m_fItemScale_5() { return &___m_fItemScale_5; }
	inline void set_m_fItemScale_5(float value)
	{
		___m_fItemScale_5 = value;
	}

	inline static int32_t get_offset_of_m_fMoveInterval_6() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fMoveInterval_6)); }
	inline float get_m_fMoveInterval_6() const { return ___m_fMoveInterval_6; }
	inline float* get_address_of_m_fMoveInterval_6() { return &___m_fMoveInterval_6; }
	inline void set_m_fMoveInterval_6(float value)
	{
		___m_fMoveInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_fMoveTime_7() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fMoveTime_7)); }
	inline float get_m_fMoveTime_7() const { return ___m_fMoveTime_7; }
	inline float* get_address_of_m_fMoveTime_7() { return &___m_fMoveTime_7; }
	inline void set_m_fMoveTime_7(float value)
	{
		___m_fMoveTime_7 = value;
	}

	inline static int32_t get_offset_of_m_fMoveSpeed_8() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fMoveSpeed_8)); }
	inline float get_m_fMoveSpeed_8() const { return ___m_fMoveSpeed_8; }
	inline float* get_address_of_m_fMoveSpeed_8() { return &___m_fMoveSpeed_8; }
	inline void set_m_fMoveSpeed_8(float value)
	{
		___m_fMoveSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_lstItems_9() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_lstItems_9)); }
	inline List_1_t2611397916 * get_m_lstItems_9() const { return ___m_lstItems_9; }
	inline List_1_t2611397916 ** get_address_of_m_lstItems_9() { return &___m_lstItems_9; }
	inline void set_m_lstItems_9(List_1_t2611397916 * value)
	{
		___m_lstItems_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_9), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledItems_10() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_lstRecycledItems_10)); }
	inline List_1_t2611397916 * get_m_lstRecycledItems_10() const { return ___m_lstRecycledItems_10; }
	inline List_1_t2611397916 ** get_address_of_m_lstRecycledItems_10() { return &___m_lstRecycledItems_10; }
	inline void set_m_lstRecycledItems_10(List_1_t2611397916 * value)
	{
		___m_lstRecycledItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledItems_10), value);
	}

	inline static int32_t get_offset_of_m_bMoving_14() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_bMoving_14)); }
	inline bool get_m_bMoving_14() const { return ___m_bMoving_14; }
	inline bool* get_address_of_m_bMoving_14() { return &___m_bMoving_14; }
	inline void set_m_bMoving_14(bool value)
	{
		___m_bMoving_14 = value;
	}

	inline static int32_t get_offset_of_m_fMoveIntervalCount_15() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fMoveIntervalCount_15)); }
	inline float get_m_fMoveIntervalCount_15() const { return ___m_fMoveIntervalCount_15; }
	inline float* get_address_of_m_fMoveIntervalCount_15() { return &___m_fMoveIntervalCount_15; }
	inline void set_m_fMoveIntervalCount_15(float value)
	{
		___m_fMoveIntervalCount_15 = value;
	}

	inline static int32_t get_offset_of_m_fMoveTimeCount_16() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221, ___m_fMoveTimeCount_16)); }
	inline float get_m_fMoveTimeCount_16() const { return ___m_fMoveTimeCount_16; }
	inline float* get_address_of_m_fMoveTimeCount_16() { return &___m_fMoveTimeCount_16; }
	inline void set_m_fMoveTimeCount_16(float value)
	{
		___m_fMoveTimeCount_16 = value;
	}
};

struct CMsgList_t4144960221_StaticFields
{
public:
	// UnityEngine.Vector3 CMsgList::vecTempPos
	Vector3_t3722313464  ___vecTempPos_11;
	// UnityEngine.Vector3 CMsgList::vecTempScale
	Vector3_t3722313464  ___vecTempScale_12;
	// CMsgList CMsgList::s_Instance
	CMsgList_t4144960221 * ___s_Instance_13;

public:
	inline static int32_t get_offset_of_vecTempPos_11() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221_StaticFields, ___vecTempPos_11)); }
	inline Vector3_t3722313464  get_vecTempPos_11() const { return ___vecTempPos_11; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_11() { return &___vecTempPos_11; }
	inline void set_vecTempPos_11(Vector3_t3722313464  value)
	{
		___vecTempPos_11 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_12() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221_StaticFields, ___vecTempScale_12)); }
	inline Vector3_t3722313464  get_vecTempScale_12() const { return ___vecTempScale_12; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_12() { return &___vecTempScale_12; }
	inline void set_vecTempScale_12(Vector3_t3722313464  value)
	{
		___vecTempScale_12 = value;
	}

	inline static int32_t get_offset_of_s_Instance_13() { return static_cast<int32_t>(offsetof(CMsgList_t4144960221_StaticFields, ___s_Instance_13)); }
	inline CMsgList_t4144960221 * get_s_Instance_13() const { return ___s_Instance_13; }
	inline CMsgList_t4144960221 ** get_address_of_s_Instance_13() { return &___s_Instance_13; }
	inline void set_s_Instance_13(CMsgList_t4144960221 * value)
	{
		___s_Instance_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSGLIST_T4144960221_H
#ifndef CMSGLISTITEM_T1139323174_H
#define CMSGLISTITEM_T1139323174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMsgListItem
struct  CMsgListItem_t1139323174  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CMsgListItem::_txt1
	Text_t1901882714 * ____txt1_2;
	// UnityEngine.UI.Text CMsgListItem::_txt2
	Text_t1901882714 * ____txt2_3;
	// UnityEngine.UI.Image CMsgListItem::img
	Image_t2670269651 * ___img_4;
	// UnityEngine.UI.Image CMsgListItem::imgHighLight
	Image_t2670269651 * ___imgHighLight_5;
	// UnityEngine.UI.Image CMsgListItem::_imgKiller
	Image_t2670269651 * ____imgKiller_6;
	// UnityEngine.UI.Image CMsgListItem::_imgDeadMeat
	Image_t2670269651 * ____imgDeadMeat_7;
	// System.Single CMsgListItem::m_fAlpha
	float ___m_fAlpha_8;
	// System.Boolean CMsgListItem::m_bFading
	bool ___m_bFading_9;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> CMsgListItem::m_lstAssistAvatar
	List_1_t4142344393 * ___m_lstAssistAvatar_14;

public:
	inline static int32_t get_offset_of__txt1_2() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ____txt1_2)); }
	inline Text_t1901882714 * get__txt1_2() const { return ____txt1_2; }
	inline Text_t1901882714 ** get_address_of__txt1_2() { return &____txt1_2; }
	inline void set__txt1_2(Text_t1901882714 * value)
	{
		____txt1_2 = value;
		Il2CppCodeGenWriteBarrier((&____txt1_2), value);
	}

	inline static int32_t get_offset_of__txt2_3() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ____txt2_3)); }
	inline Text_t1901882714 * get__txt2_3() const { return ____txt2_3; }
	inline Text_t1901882714 ** get_address_of__txt2_3() { return &____txt2_3; }
	inline void set__txt2_3(Text_t1901882714 * value)
	{
		____txt2_3 = value;
		Il2CppCodeGenWriteBarrier((&____txt2_3), value);
	}

	inline static int32_t get_offset_of_img_4() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ___img_4)); }
	inline Image_t2670269651 * get_img_4() const { return ___img_4; }
	inline Image_t2670269651 ** get_address_of_img_4() { return &___img_4; }
	inline void set_img_4(Image_t2670269651 * value)
	{
		___img_4 = value;
		Il2CppCodeGenWriteBarrier((&___img_4), value);
	}

	inline static int32_t get_offset_of_imgHighLight_5() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ___imgHighLight_5)); }
	inline Image_t2670269651 * get_imgHighLight_5() const { return ___imgHighLight_5; }
	inline Image_t2670269651 ** get_address_of_imgHighLight_5() { return &___imgHighLight_5; }
	inline void set_imgHighLight_5(Image_t2670269651 * value)
	{
		___imgHighLight_5 = value;
		Il2CppCodeGenWriteBarrier((&___imgHighLight_5), value);
	}

	inline static int32_t get_offset_of__imgKiller_6() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ____imgKiller_6)); }
	inline Image_t2670269651 * get__imgKiller_6() const { return ____imgKiller_6; }
	inline Image_t2670269651 ** get_address_of__imgKiller_6() { return &____imgKiller_6; }
	inline void set__imgKiller_6(Image_t2670269651 * value)
	{
		____imgKiller_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgKiller_6), value);
	}

	inline static int32_t get_offset_of__imgDeadMeat_7() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ____imgDeadMeat_7)); }
	inline Image_t2670269651 * get__imgDeadMeat_7() const { return ____imgDeadMeat_7; }
	inline Image_t2670269651 ** get_address_of__imgDeadMeat_7() { return &____imgDeadMeat_7; }
	inline void set__imgDeadMeat_7(Image_t2670269651 * value)
	{
		____imgDeadMeat_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgDeadMeat_7), value);
	}

	inline static int32_t get_offset_of_m_fAlpha_8() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ___m_fAlpha_8)); }
	inline float get_m_fAlpha_8() const { return ___m_fAlpha_8; }
	inline float* get_address_of_m_fAlpha_8() { return &___m_fAlpha_8; }
	inline void set_m_fAlpha_8(float value)
	{
		___m_fAlpha_8 = value;
	}

	inline static int32_t get_offset_of_m_bFading_9() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ___m_bFading_9)); }
	inline bool get_m_bFading_9() const { return ___m_bFading_9; }
	inline bool* get_address_of_m_bFading_9() { return &___m_bFading_9; }
	inline void set_m_bFading_9(bool value)
	{
		___m_bFading_9 = value;
	}

	inline static int32_t get_offset_of_m_lstAssistAvatar_14() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174, ___m_lstAssistAvatar_14)); }
	inline List_1_t4142344393 * get_m_lstAssistAvatar_14() const { return ___m_lstAssistAvatar_14; }
	inline List_1_t4142344393 ** get_address_of_m_lstAssistAvatar_14() { return &___m_lstAssistAvatar_14; }
	inline void set_m_lstAssistAvatar_14(List_1_t4142344393 * value)
	{
		___m_lstAssistAvatar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAssistAvatar_14), value);
	}
};

struct CMsgListItem_t1139323174_StaticFields
{
public:
	// UnityEngine.Color CMsgListItem::cTempColor
	Color_t2555686324  ___cTempColor_10;
	// UnityEngine.Vector3 CMsgListItem::vecTempPos
	Vector3_t3722313464  ___vecTempPos_11;
	// UnityEngine.Vector3 CMsgListItem::vecTempScale
	Vector3_t3722313464  ___vecTempScale_12;
	// UnityEngine.Rect CMsgListItem::tempRect
	Rect_t2360479859  ___tempRect_13;

public:
	inline static int32_t get_offset_of_cTempColor_10() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174_StaticFields, ___cTempColor_10)); }
	inline Color_t2555686324  get_cTempColor_10() const { return ___cTempColor_10; }
	inline Color_t2555686324 * get_address_of_cTempColor_10() { return &___cTempColor_10; }
	inline void set_cTempColor_10(Color_t2555686324  value)
	{
		___cTempColor_10 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_11() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174_StaticFields, ___vecTempPos_11)); }
	inline Vector3_t3722313464  get_vecTempPos_11() const { return ___vecTempPos_11; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_11() { return &___vecTempPos_11; }
	inline void set_vecTempPos_11(Vector3_t3722313464  value)
	{
		___vecTempPos_11 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_12() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174_StaticFields, ___vecTempScale_12)); }
	inline Vector3_t3722313464  get_vecTempScale_12() const { return ___vecTempScale_12; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_12() { return &___vecTempScale_12; }
	inline void set_vecTempScale_12(Vector3_t3722313464  value)
	{
		___vecTempScale_12 = value;
	}

	inline static int32_t get_offset_of_tempRect_13() { return static_cast<int32_t>(offsetof(CMsgListItem_t1139323174_StaticFields, ___tempRect_13)); }
	inline Rect_t2360479859  get_tempRect_13() const { return ___tempRect_13; }
	inline Rect_t2360479859 * get_address_of_tempRect_13() { return &___tempRect_13; }
	inline void set_tempRect_13(Rect_t2360479859  value)
	{
		___tempRect_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSGLISTITEM_T1139323174_H
#ifndef CPROGRESSBAR_T881982788_H
#define CPROGRESSBAR_T881982788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CProgressBar
struct  CProgressBar_t881982788  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CProgressBar::_txtCaption
	Text_t1901882714 * ____txtCaption_2;
	// UnityEngine.UI.Image CProgressBar::_imgBar
	Image_t2670269651 * ____imgBar_3;

public:
	inline static int32_t get_offset_of__txtCaption_2() { return static_cast<int32_t>(offsetof(CProgressBar_t881982788, ____txtCaption_2)); }
	inline Text_t1901882714 * get__txtCaption_2() const { return ____txtCaption_2; }
	inline Text_t1901882714 ** get_address_of__txtCaption_2() { return &____txtCaption_2; }
	inline void set__txtCaption_2(Text_t1901882714 * value)
	{
		____txtCaption_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtCaption_2), value);
	}

	inline static int32_t get_offset_of__imgBar_3() { return static_cast<int32_t>(offsetof(CProgressBar_t881982788, ____imgBar_3)); }
	inline Image_t2670269651 * get__imgBar_3() const { return ____imgBar_3; }
	inline Image_t2670269651 ** get_address_of__imgBar_3() { return &____imgBar_3; }
	inline void set__imgBar_3(Image_t2670269651 * value)
	{
		____imgBar_3 = value;
		Il2CppCodeGenWriteBarrier((&____imgBar_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPROGRESSBAR_T881982788_H
#ifndef CSHUANGYAOGAN_T3597386049_H
#define CSHUANGYAOGAN_T3597386049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CShuangYaoGan
struct  CShuangYaoGan_t3597386049  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CShuangYaoGan::_imgKnob
	Image_t2670269651 * ____imgKnob_3;
	// System.Single CShuangYaoGan::m_fRadius
	float ___m_fRadius_4;
	// System.Boolean CShuangYaoGan::m_bShowing
	bool ___m_bShowing_5;
	// UnityEngine.Vector2 CShuangYaoGan::m_vecDir
	Vector2_t2156229523  ___m_vecDir_6;

public:
	inline static int32_t get_offset_of__imgKnob_3() { return static_cast<int32_t>(offsetof(CShuangYaoGan_t3597386049, ____imgKnob_3)); }
	inline Image_t2670269651 * get__imgKnob_3() const { return ____imgKnob_3; }
	inline Image_t2670269651 ** get_address_of__imgKnob_3() { return &____imgKnob_3; }
	inline void set__imgKnob_3(Image_t2670269651 * value)
	{
		____imgKnob_3 = value;
		Il2CppCodeGenWriteBarrier((&____imgKnob_3), value);
	}

	inline static int32_t get_offset_of_m_fRadius_4() { return static_cast<int32_t>(offsetof(CShuangYaoGan_t3597386049, ___m_fRadius_4)); }
	inline float get_m_fRadius_4() const { return ___m_fRadius_4; }
	inline float* get_address_of_m_fRadius_4() { return &___m_fRadius_4; }
	inline void set_m_fRadius_4(float value)
	{
		___m_fRadius_4 = value;
	}

	inline static int32_t get_offset_of_m_bShowing_5() { return static_cast<int32_t>(offsetof(CShuangYaoGan_t3597386049, ___m_bShowing_5)); }
	inline bool get_m_bShowing_5() const { return ___m_bShowing_5; }
	inline bool* get_address_of_m_bShowing_5() { return &___m_bShowing_5; }
	inline void set_m_bShowing_5(bool value)
	{
		___m_bShowing_5 = value;
	}

	inline static int32_t get_offset_of_m_vecDir_6() { return static_cast<int32_t>(offsetof(CShuangYaoGan_t3597386049, ___m_vecDir_6)); }
	inline Vector2_t2156229523  get_m_vecDir_6() const { return ___m_vecDir_6; }
	inline Vector2_t2156229523 * get_address_of_m_vecDir_6() { return &___m_vecDir_6; }
	inline void set_m_vecDir_6(Vector2_t2156229523  value)
	{
		___m_vecDir_6 = value;
	}
};

struct CShuangYaoGan_t3597386049_StaticFields
{
public:
	// UnityEngine.Vector3 CShuangYaoGan::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CShuangYaoGan_t3597386049_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHUANGYAOGAN_T3597386049_H
#ifndef CBISHUATUOWEI_T3092616324_H
#define CBISHUATUOWEI_T3092616324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBiShuaTuoWei
struct  CBiShuaTuoWei_t3092616324  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CBiShuaTuoWei::_imgMask
	Image_t2670269651 * ____imgMask_2;

public:
	inline static int32_t get_offset_of__imgMask_2() { return static_cast<int32_t>(offsetof(CBiShuaTuoWei_t3092616324, ____imgMask_2)); }
	inline Image_t2670269651 * get__imgMask_2() const { return ____imgMask_2; }
	inline Image_t2670269651 ** get_address_of__imgMask_2() { return &____imgMask_2; }
	inline void set__imgMask_2(Image_t2670269651 * value)
	{
		____imgMask_2 = value;
		Il2CppCodeGenWriteBarrier((&____imgMask_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBISHUATUOWEI_T3092616324_H
#ifndef CGERENJIESUANCOUNTER_T645084407_H
#define CGERENJIESUANCOUNTER_T645084407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGeRenJieSuanCounter
struct  CGeRenJieSuanCounter_t645084407  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CGeRenJieSuanCounter::_txtValue
	Text_t1901882714 * ____txtValue_2;

public:
	inline static int32_t get_offset_of__txtValue_2() { return static_cast<int32_t>(offsetof(CGeRenJieSuanCounter_t645084407, ____txtValue_2)); }
	inline Text_t1901882714 * get__txtValue_2() const { return ____txtValue_2; }
	inline Text_t1901882714 ** get_address_of__txtValue_2() { return &____txtValue_2; }
	inline void set__txtValue_2(Text_t1901882714 * value)
	{
		____txtValue_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGERENJIESUANCOUNTER_T645084407_H
#ifndef CJIESUANCOUNTER_T4266685435_H
#define CJIESUANCOUNTER_T4266685435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJieSuanCounter
struct  CJieSuanCounter_t4266685435  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CJieSuanCounter::_goZuiJiaKuang
	GameObject_t1113636619 * ____goZuiJiaKuang_2;
	// Spine.Unity.SkeletonGraphic CJieSuanCounter::_skeleLike
	SkeletonGraphic_t1744877482 * ____skeleLike_3;
	// UnityEngine.UI.Image CJieSuanCounter::_imgLiked_Click
	Image_t2670269651 * ____imgLiked_Click_4;
	// UnityEngine.UI.Image CJieSuanCounter::_imgLiked_NotClick
	Image_t2670269651 * ____imgLiked_NotClick_5;
	// CFrameAnimationEffect CJieSuanCounter::_effectAvatar
	CFrameAnimationEffect_t443605508 * ____effectAvatar_6;
	// CFrameAnimationEffect CJieSuanCounter::_effectOuterBorder
	CFrameAnimationEffect_t443605508 * ____effectOuterBorder_7;
	// CFrameAnimationEffect CJieSuanCounter::_effectQuanChangZuiJia_QianZou
	CFrameAnimationEffect_t443605508 * ____effectQuanChangZuiJia_QianZou_8;
	// CFrameAnimationEffect CJieSuanCounter::_effectQuanChangZuiJia_ChiXu
	CFrameAnimationEffect_t443605508 * ____effectQuanChangZuiJia_ChiXu_9;
	// UnityEngine.UI.Image CJieSuanCounter::_imgAvatar
	Image_t2670269651 * ____imgAvatar_10;
	// UnityEngine.UI.Text CJieSuanCounter::_txtName
	Text_t1901882714 * ____txtName_11;
	// UnityEngine.UI.Text CJieSuanCounter::_txtVolume
	Text_t1901882714 * ____txtVolume_12;
	// UnityEngine.UI.Text CJieSuanCounter::_CommonInfo
	Text_t1901882714 * ____CommonInfo_13;
	// UnityEngine.UI.Text CJieSuanCounter::_EatVolume
	Text_t1901882714 * ____EatVolume_14;
	// UnityEngine.UI.Text CJieSuanCounter::_txtLikeNum
	Text_t1901882714 * ____txtLikeNum_15;
	// System.Int32 CJieSuanCounter::m_nZuiJiaKuangStatus
	int32_t ___m_nZuiJiaKuangStatus_16;
	// System.Int32 CJieSuanCounter::m_nLikeBtnStatus
	int32_t ___m_nLikeBtnStatus_17;
	// System.Single CJieSuanCounter::m_fLikeStatusTimeLapse
	float ___m_fLikeStatusTimeLapse_18;
	// System.Int32 CJieSuanCounter::m_nPlayerId
	int32_t ___m_nPlayerId_20;
	// System.Int32 CJieSuanCounter::m_nCounterId
	int32_t ___m_nCounterId_21;
	// System.Int32 CJieSuanCounter::m_nLikeNum
	int32_t ___m_nLikeNum_22;

public:
	inline static int32_t get_offset_of__goZuiJiaKuang_2() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____goZuiJiaKuang_2)); }
	inline GameObject_t1113636619 * get__goZuiJiaKuang_2() const { return ____goZuiJiaKuang_2; }
	inline GameObject_t1113636619 ** get_address_of__goZuiJiaKuang_2() { return &____goZuiJiaKuang_2; }
	inline void set__goZuiJiaKuang_2(GameObject_t1113636619 * value)
	{
		____goZuiJiaKuang_2 = value;
		Il2CppCodeGenWriteBarrier((&____goZuiJiaKuang_2), value);
	}

	inline static int32_t get_offset_of__skeleLike_3() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____skeleLike_3)); }
	inline SkeletonGraphic_t1744877482 * get__skeleLike_3() const { return ____skeleLike_3; }
	inline SkeletonGraphic_t1744877482 ** get_address_of__skeleLike_3() { return &____skeleLike_3; }
	inline void set__skeleLike_3(SkeletonGraphic_t1744877482 * value)
	{
		____skeleLike_3 = value;
		Il2CppCodeGenWriteBarrier((&____skeleLike_3), value);
	}

	inline static int32_t get_offset_of__imgLiked_Click_4() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____imgLiked_Click_4)); }
	inline Image_t2670269651 * get__imgLiked_Click_4() const { return ____imgLiked_Click_4; }
	inline Image_t2670269651 ** get_address_of__imgLiked_Click_4() { return &____imgLiked_Click_4; }
	inline void set__imgLiked_Click_4(Image_t2670269651 * value)
	{
		____imgLiked_Click_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgLiked_Click_4), value);
	}

	inline static int32_t get_offset_of__imgLiked_NotClick_5() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____imgLiked_NotClick_5)); }
	inline Image_t2670269651 * get__imgLiked_NotClick_5() const { return ____imgLiked_NotClick_5; }
	inline Image_t2670269651 ** get_address_of__imgLiked_NotClick_5() { return &____imgLiked_NotClick_5; }
	inline void set__imgLiked_NotClick_5(Image_t2670269651 * value)
	{
		____imgLiked_NotClick_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgLiked_NotClick_5), value);
	}

	inline static int32_t get_offset_of__effectAvatar_6() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____effectAvatar_6)); }
	inline CFrameAnimationEffect_t443605508 * get__effectAvatar_6() const { return ____effectAvatar_6; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectAvatar_6() { return &____effectAvatar_6; }
	inline void set__effectAvatar_6(CFrameAnimationEffect_t443605508 * value)
	{
		____effectAvatar_6 = value;
		Il2CppCodeGenWriteBarrier((&____effectAvatar_6), value);
	}

	inline static int32_t get_offset_of__effectOuterBorder_7() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____effectOuterBorder_7)); }
	inline CFrameAnimationEffect_t443605508 * get__effectOuterBorder_7() const { return ____effectOuterBorder_7; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectOuterBorder_7() { return &____effectOuterBorder_7; }
	inline void set__effectOuterBorder_7(CFrameAnimationEffect_t443605508 * value)
	{
		____effectOuterBorder_7 = value;
		Il2CppCodeGenWriteBarrier((&____effectOuterBorder_7), value);
	}

	inline static int32_t get_offset_of__effectQuanChangZuiJia_QianZou_8() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____effectQuanChangZuiJia_QianZou_8)); }
	inline CFrameAnimationEffect_t443605508 * get__effectQuanChangZuiJia_QianZou_8() const { return ____effectQuanChangZuiJia_QianZou_8; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectQuanChangZuiJia_QianZou_8() { return &____effectQuanChangZuiJia_QianZou_8; }
	inline void set__effectQuanChangZuiJia_QianZou_8(CFrameAnimationEffect_t443605508 * value)
	{
		____effectQuanChangZuiJia_QianZou_8 = value;
		Il2CppCodeGenWriteBarrier((&____effectQuanChangZuiJia_QianZou_8), value);
	}

	inline static int32_t get_offset_of__effectQuanChangZuiJia_ChiXu_9() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____effectQuanChangZuiJia_ChiXu_9)); }
	inline CFrameAnimationEffect_t443605508 * get__effectQuanChangZuiJia_ChiXu_9() const { return ____effectQuanChangZuiJia_ChiXu_9; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectQuanChangZuiJia_ChiXu_9() { return &____effectQuanChangZuiJia_ChiXu_9; }
	inline void set__effectQuanChangZuiJia_ChiXu_9(CFrameAnimationEffect_t443605508 * value)
	{
		____effectQuanChangZuiJia_ChiXu_9 = value;
		Il2CppCodeGenWriteBarrier((&____effectQuanChangZuiJia_ChiXu_9), value);
	}

	inline static int32_t get_offset_of__imgAvatar_10() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____imgAvatar_10)); }
	inline Image_t2670269651 * get__imgAvatar_10() const { return ____imgAvatar_10; }
	inline Image_t2670269651 ** get_address_of__imgAvatar_10() { return &____imgAvatar_10; }
	inline void set__imgAvatar_10(Image_t2670269651 * value)
	{
		____imgAvatar_10 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_10), value);
	}

	inline static int32_t get_offset_of__txtName_11() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____txtName_11)); }
	inline Text_t1901882714 * get__txtName_11() const { return ____txtName_11; }
	inline Text_t1901882714 ** get_address_of__txtName_11() { return &____txtName_11; }
	inline void set__txtName_11(Text_t1901882714 * value)
	{
		____txtName_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_11), value);
	}

	inline static int32_t get_offset_of__txtVolume_12() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____txtVolume_12)); }
	inline Text_t1901882714 * get__txtVolume_12() const { return ____txtVolume_12; }
	inline Text_t1901882714 ** get_address_of__txtVolume_12() { return &____txtVolume_12; }
	inline void set__txtVolume_12(Text_t1901882714 * value)
	{
		____txtVolume_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtVolume_12), value);
	}

	inline static int32_t get_offset_of__CommonInfo_13() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____CommonInfo_13)); }
	inline Text_t1901882714 * get__CommonInfo_13() const { return ____CommonInfo_13; }
	inline Text_t1901882714 ** get_address_of__CommonInfo_13() { return &____CommonInfo_13; }
	inline void set__CommonInfo_13(Text_t1901882714 * value)
	{
		____CommonInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&____CommonInfo_13), value);
	}

	inline static int32_t get_offset_of__EatVolume_14() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____EatVolume_14)); }
	inline Text_t1901882714 * get__EatVolume_14() const { return ____EatVolume_14; }
	inline Text_t1901882714 ** get_address_of__EatVolume_14() { return &____EatVolume_14; }
	inline void set__EatVolume_14(Text_t1901882714 * value)
	{
		____EatVolume_14 = value;
		Il2CppCodeGenWriteBarrier((&____EatVolume_14), value);
	}

	inline static int32_t get_offset_of__txtLikeNum_15() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ____txtLikeNum_15)); }
	inline Text_t1901882714 * get__txtLikeNum_15() const { return ____txtLikeNum_15; }
	inline Text_t1901882714 ** get_address_of__txtLikeNum_15() { return &____txtLikeNum_15; }
	inline void set__txtLikeNum_15(Text_t1901882714 * value)
	{
		____txtLikeNum_15 = value;
		Il2CppCodeGenWriteBarrier((&____txtLikeNum_15), value);
	}

	inline static int32_t get_offset_of_m_nZuiJiaKuangStatus_16() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ___m_nZuiJiaKuangStatus_16)); }
	inline int32_t get_m_nZuiJiaKuangStatus_16() const { return ___m_nZuiJiaKuangStatus_16; }
	inline int32_t* get_address_of_m_nZuiJiaKuangStatus_16() { return &___m_nZuiJiaKuangStatus_16; }
	inline void set_m_nZuiJiaKuangStatus_16(int32_t value)
	{
		___m_nZuiJiaKuangStatus_16 = value;
	}

	inline static int32_t get_offset_of_m_nLikeBtnStatus_17() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ___m_nLikeBtnStatus_17)); }
	inline int32_t get_m_nLikeBtnStatus_17() const { return ___m_nLikeBtnStatus_17; }
	inline int32_t* get_address_of_m_nLikeBtnStatus_17() { return &___m_nLikeBtnStatus_17; }
	inline void set_m_nLikeBtnStatus_17(int32_t value)
	{
		___m_nLikeBtnStatus_17 = value;
	}

	inline static int32_t get_offset_of_m_fLikeStatusTimeLapse_18() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ___m_fLikeStatusTimeLapse_18)); }
	inline float get_m_fLikeStatusTimeLapse_18() const { return ___m_fLikeStatusTimeLapse_18; }
	inline float* get_address_of_m_fLikeStatusTimeLapse_18() { return &___m_fLikeStatusTimeLapse_18; }
	inline void set_m_fLikeStatusTimeLapse_18(float value)
	{
		___m_fLikeStatusTimeLapse_18 = value;
	}

	inline static int32_t get_offset_of_m_nPlayerId_20() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ___m_nPlayerId_20)); }
	inline int32_t get_m_nPlayerId_20() const { return ___m_nPlayerId_20; }
	inline int32_t* get_address_of_m_nPlayerId_20() { return &___m_nPlayerId_20; }
	inline void set_m_nPlayerId_20(int32_t value)
	{
		___m_nPlayerId_20 = value;
	}

	inline static int32_t get_offset_of_m_nCounterId_21() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ___m_nCounterId_21)); }
	inline int32_t get_m_nCounterId_21() const { return ___m_nCounterId_21; }
	inline int32_t* get_address_of_m_nCounterId_21() { return &___m_nCounterId_21; }
	inline void set_m_nCounterId_21(int32_t value)
	{
		___m_nCounterId_21 = value;
	}

	inline static int32_t get_offset_of_m_nLikeNum_22() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435, ___m_nLikeNum_22)); }
	inline int32_t get_m_nLikeNum_22() const { return ___m_nLikeNum_22; }
	inline int32_t* get_address_of_m_nLikeNum_22() { return &___m_nLikeNum_22; }
	inline void set_m_nLikeNum_22(int32_t value)
	{
		___m_nLikeNum_22 = value;
	}
};

struct CJieSuanCounter_t4266685435_StaticFields
{
public:
	// UnityEngine.Vector3 CJieSuanCounter::vecTempScale
	Vector3_t3722313464  ___vecTempScale_19;

public:
	inline static int32_t get_offset_of_vecTempScale_19() { return static_cast<int32_t>(offsetof(CJieSuanCounter_t4266685435_StaticFields, ___vecTempScale_19)); }
	inline Vector3_t3722313464  get_vecTempScale_19() const { return ___vecTempScale_19; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_19() { return &___vecTempScale_19; }
	inline void set_vecTempScale_19(Vector3_t3722313464  value)
	{
		___vecTempScale_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJIESUANCOUNTER_T4266685435_H
#ifndef CJIESUANMAIN_T410608331_H
#define CJIESUANMAIN_T410608331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJieSuanMain
struct  CJieSuanMain_t410608331  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CJieSuanMain::_txtJieSuanInfo
	Text_t1901882714 * ____txtJieSuanInfo_2;
	// System.Single CJieSuanMain::m_fCount
	float ___m_fCount_3;
	// System.Boolean CJieSuanMain::m_bCounting
	bool ___m_bCounting_4;

public:
	inline static int32_t get_offset_of__txtJieSuanInfo_2() { return static_cast<int32_t>(offsetof(CJieSuanMain_t410608331, ____txtJieSuanInfo_2)); }
	inline Text_t1901882714 * get__txtJieSuanInfo_2() const { return ____txtJieSuanInfo_2; }
	inline Text_t1901882714 ** get_address_of__txtJieSuanInfo_2() { return &____txtJieSuanInfo_2; }
	inline void set__txtJieSuanInfo_2(Text_t1901882714 * value)
	{
		____txtJieSuanInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtJieSuanInfo_2), value);
	}

	inline static int32_t get_offset_of_m_fCount_3() { return static_cast<int32_t>(offsetof(CJieSuanMain_t410608331, ___m_fCount_3)); }
	inline float get_m_fCount_3() const { return ___m_fCount_3; }
	inline float* get_address_of_m_fCount_3() { return &___m_fCount_3; }
	inline void set_m_fCount_3(float value)
	{
		___m_fCount_3 = value;
	}

	inline static int32_t get_offset_of_m_bCounting_4() { return static_cast<int32_t>(offsetof(CJieSuanMain_t410608331, ___m_bCounting_4)); }
	inline bool get_m_bCounting_4() const { return ___m_bCounting_4; }
	inline bool* get_address_of_m_bCounting_4() { return &___m_bCounting_4; }
	inline void set_m_bCounting_4(bool value)
	{
		___m_bCounting_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJIESUANMAIN_T410608331_H
#ifndef CMONSTEREDITOR_T2594843249_H
#define CMONSTEREDITOR_T2594843249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor
struct  CMonsterEditor_t2594843249  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] CMonsterEditor::m_aryBeanPic
	SpriteU5BU5D_t2581906349* ___m_aryBeanPic_2;
	// UnityEngine.Sprite[] CMonsterEditor::m_arySprites
	SpriteU5BU5D_t2581906349* ___m_arySprites_3;
	// UnityEngine.Sprite[] CMonsterEditor::m_arySprites_Thorn
	SpriteU5BU5D_t2581906349* ___m_arySprites_Thorn_4;
	// UnityEngine.UI.Dropdown CMonsterEditor::_dropdownThorszId
	Dropdown_t2274391225 * ____dropdownThorszId_5;
	// UnityEngine.UI.Dropdown CMonsterEditor::_dropdownThornType
	Dropdown_t2274391225 * ____dropdownThornType_6;
	// UnityEngine.UI.Dropdown CMonsterEditor::_dropdownSkill
	Dropdown_t2274391225 * ____dropdownSkill_7;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornDesc
	InputField_t3762917431 * ____inputfieldThornDesc_8;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornFoodSize
	InputField_t3762917431 * ____inputfieldThornFoodSize_9;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldSelfSize
	InputField_t3762917431 * ____inputfieldSelfSize_10;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornColor
	InputField_t3762917431 * ____inputfieldThornColor_11;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExp
	InputField_t3762917431 * ____inputfieldThornExp_12;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornMoney
	InputField_t3762917431 * ____inputfieldThornMoney_13;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornBuffId
	InputField_t3762917431 * ____inputfieldThornBuffId_14;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeChildNum
	InputField_t3762917431 * ____inputfieldThornExplodeChildNum_15;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeMotherLeftPercent
	InputField_t3762917431 * ____inputfieldThornExplodeMotherLeftPercent_16;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeRunDistance
	InputField_t3762917431 * ____inputfieldThornExplodeRunDistance_17;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeRunTime
	InputField_t3762917431 * ____inputfieldThornExplodeRunTime_18;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeStay
	InputField_t3762917431 * ____inputfieldThornExplodeStay_19;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeShellTime
	InputField_t3762917431 * ____inputfieldThornExplodeShellTime_20;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornExplodeFormatioszId
	InputField_t3762917431 * ____inputfieldThornExplodeFormatioszId_21;
	// UnityEngine.UI.InputField CMonsterEditor::_inputfieldThornSkillName
	InputField_t3762917431 * ____inputfieldThornSkillName_22;
	// System.Collections.Generic.Dictionary`2<System.String,CMonsterEditor/sThornConfig> CMonsterEditor::m_dicMonsterConfig
	Dictionary_2_t1027700750 * ___m_dicMonsterConfig_25;
	// CMonsterEditor/sThornConfig CMonsterEditor::tempMonsterConfig
	sThornConfig_t1242444451  ___tempMonsterConfig_26;
	// CMonsterEditor/sThornConfig CMonsterEditor::m_CurThornConfig
	sThornConfig_t1242444451  ___m_CurThornConfig_27;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CMonsterEditor::dicTemp
	Dictionary_2_t1632706988 * ___dicTemp_28;
	// UnityEngine.GameObject CMonsterEditor::m_goBeanContainer
	GameObject_t1113636619 * ___m_goBeanContainer_34;
	// UnityEngine.Sprite CMonsterEditor::m_sprBeanPic
	Sprite_t280657092 * ___m_sprBeanPic_35;
	// UnityEngine.Sprite CMonsterEditor::m_sprBeanDeadPic
	Sprite_t280657092 * ___m_sprBeanDeadPic_36;
	// UnityEngine.Sprite CMonsterEditor::m_sprBeanCollection
	Sprite_t280657092 * ___m_sprBeanCollection_37;
	// UnityEngine.GameObject CMonsterEditor::m_preBeanCollection
	GameObject_t1113636619 * ___m_preBeanCollection_38;
	// UnityEngine.Color[] CMonsterEditor::m_ColorBeanDead
	ColorU5BU5D_t941916413* ___m_ColorBeanDead_39;
	// UnityEngine.Color[] CMonsterEditor::m_ColorBeanLive
	ColorU5BU5D_t941916413* ___m_ColorBeanLive_40;
	// UnityEngine.Color[] CMonsterEditor::m_ColorAllEmpty
	ColorU5BU5D_t941916413* ___m_ColorAllEmpty_41;
	// System.Collections.Generic.List`1<CBeanCollection> CMonsterEditor::m_lstBeanCollection
	List_1_t4044148548 * ___m_lstBeanCollection_42;
	// System.Int32 CMonsterEditor::m_nCollectionGuid
	int32_t ___m_nCollectionGuid_43;
	// System.Collections.Generic.List`1<UnityEngine.Vector2[]> CMonsterEditor::m_lstLocalPos
	List_1_t2929260728 * ___m_lstLocalPos_44;
	// System.Collections.Generic.List`1<UnityEngine.Vector2[]> CMonsterEditor::m_lstTexPos
	List_1_t2929260728 * ___m_lstTexPos_45;
	// UnityEngine.Sprite CMonsterEditor::m_sprBeanCollectionEmpty
	Sprite_t280657092 * ___m_sprBeanCollectionEmpty_46;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Color[]> CMonsterEditor::m_dicBeanColor
	Dictionary_2_t727172712 * ___m_dicBeanColor_47;
	// System.Collections.Generic.Dictionary`2<System.String,CBeanCollection> CMonsterEditor::m_dicBeanCollections
	Dictionary_2_t2357330105 * ___m_dicBeanCollections_48;
	// System.Collections.Generic.Dictionary`2<System.Int32,CBeanCollection> CMonsterEditor::m_dicBeanCollectionsByGuid
	Dictionary_2_t1460787137 * ___m_dicBeanCollectionsByGuid_49;
	// System.Int32 CMonsterEditor::m_nCollectionIndex
	int32_t ___m_nCollectionIndex_50;
	// System.Int32 CMonsterEditor::m_nQuKuai
	int32_t ___m_nQuKuai_51;
	// System.Int32 CMonsterEditor::m_nQuKuaiIndex_i
	int32_t ___m_nQuKuaiIndex_i_52;
	// System.Int32 CMonsterEditor::m_nQuKuaiIndex_j
	int32_t ___m_nQuKuaiIndex_j_53;
	// System.Boolean CMonsterEditor::m_bInitQuKuaiCompleted
	bool ___m_bInitQuKuaiCompleted_54;
	// System.Int32 CMonsterEditor::m_nTotalNUm
	int32_t ___m_nTotalNUm_55;
	// System.Boolean CMonsterEditor::m_bInitQuKuaiStarted
	bool ___m_bInitQuKuaiStarted_56;
	// System.Single CMonsterEditor::m_fBeanInteractBallTimeCount
	float ___m_fBeanInteractBallTimeCount_57;
	// System.Collections.Generic.List`1<CBeanCollection/sBeanRebornInfo> CMonsterEditor::m_lstBeanRebornList
	List_1_t4257725728 * ___m_lstBeanRebornList_58;
	// System.Single CMonsterEditor::m_fBeanRebornLoopTimeCount
	float ___m_fBeanRebornLoopTimeCount_59;
	// System.Collections.Generic.List`1<CBeanCollection> CMonsterEditor::m_lstInteractingCollection
	List_1_t4044148548 * ___m_lstInteractingCollection_60;
	// System.Collections.Generic.List`1<CMonsterEditor/sGenerateBeanNode> CMonsterEditor::m_lstGenerateBeanNodeList
	List_1_t2282564592 * ___m_lstGenerateBeanNodeList_62;

public:
	inline static int32_t get_offset_of_m_aryBeanPic_2() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_aryBeanPic_2)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryBeanPic_2() const { return ___m_aryBeanPic_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryBeanPic_2() { return &___m_aryBeanPic_2; }
	inline void set_m_aryBeanPic_2(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryBeanPic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBeanPic_2), value);
	}

	inline static int32_t get_offset_of_m_arySprites_3() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_arySprites_3)); }
	inline SpriteU5BU5D_t2581906349* get_m_arySprites_3() const { return ___m_arySprites_3; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_arySprites_3() { return &___m_arySprites_3; }
	inline void set_m_arySprites_3(SpriteU5BU5D_t2581906349* value)
	{
		___m_arySprites_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySprites_3), value);
	}

	inline static int32_t get_offset_of_m_arySprites_Thorn_4() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_arySprites_Thorn_4)); }
	inline SpriteU5BU5D_t2581906349* get_m_arySprites_Thorn_4() const { return ___m_arySprites_Thorn_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_arySprites_Thorn_4() { return &___m_arySprites_Thorn_4; }
	inline void set_m_arySprites_Thorn_4(SpriteU5BU5D_t2581906349* value)
	{
		___m_arySprites_Thorn_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySprites_Thorn_4), value);
	}

	inline static int32_t get_offset_of__dropdownThorszId_5() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____dropdownThorszId_5)); }
	inline Dropdown_t2274391225 * get__dropdownThorszId_5() const { return ____dropdownThorszId_5; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownThorszId_5() { return &____dropdownThorszId_5; }
	inline void set__dropdownThorszId_5(Dropdown_t2274391225 * value)
	{
		____dropdownThorszId_5 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownThorszId_5), value);
	}

	inline static int32_t get_offset_of__dropdownThornType_6() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____dropdownThornType_6)); }
	inline Dropdown_t2274391225 * get__dropdownThornType_6() const { return ____dropdownThornType_6; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownThornType_6() { return &____dropdownThornType_6; }
	inline void set__dropdownThornType_6(Dropdown_t2274391225 * value)
	{
		____dropdownThornType_6 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownThornType_6), value);
	}

	inline static int32_t get_offset_of__dropdownSkill_7() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____dropdownSkill_7)); }
	inline Dropdown_t2274391225 * get__dropdownSkill_7() const { return ____dropdownSkill_7; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownSkill_7() { return &____dropdownSkill_7; }
	inline void set__dropdownSkill_7(Dropdown_t2274391225 * value)
	{
		____dropdownSkill_7 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownSkill_7), value);
	}

	inline static int32_t get_offset_of__inputfieldThornDesc_8() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornDesc_8)); }
	inline InputField_t3762917431 * get__inputfieldThornDesc_8() const { return ____inputfieldThornDesc_8; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornDesc_8() { return &____inputfieldThornDesc_8; }
	inline void set__inputfieldThornDesc_8(InputField_t3762917431 * value)
	{
		____inputfieldThornDesc_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornDesc_8), value);
	}

	inline static int32_t get_offset_of__inputfieldThornFoodSize_9() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornFoodSize_9)); }
	inline InputField_t3762917431 * get__inputfieldThornFoodSize_9() const { return ____inputfieldThornFoodSize_9; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornFoodSize_9() { return &____inputfieldThornFoodSize_9; }
	inline void set__inputfieldThornFoodSize_9(InputField_t3762917431 * value)
	{
		____inputfieldThornFoodSize_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornFoodSize_9), value);
	}

	inline static int32_t get_offset_of__inputfieldSelfSize_10() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldSelfSize_10)); }
	inline InputField_t3762917431 * get__inputfieldSelfSize_10() const { return ____inputfieldSelfSize_10; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSelfSize_10() { return &____inputfieldSelfSize_10; }
	inline void set__inputfieldSelfSize_10(InputField_t3762917431 * value)
	{
		____inputfieldSelfSize_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSelfSize_10), value);
	}

	inline static int32_t get_offset_of__inputfieldThornColor_11() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornColor_11)); }
	inline InputField_t3762917431 * get__inputfieldThornColor_11() const { return ____inputfieldThornColor_11; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornColor_11() { return &____inputfieldThornColor_11; }
	inline void set__inputfieldThornColor_11(InputField_t3762917431 * value)
	{
		____inputfieldThornColor_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornColor_11), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExp_12() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExp_12)); }
	inline InputField_t3762917431 * get__inputfieldThornExp_12() const { return ____inputfieldThornExp_12; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExp_12() { return &____inputfieldThornExp_12; }
	inline void set__inputfieldThornExp_12(InputField_t3762917431 * value)
	{
		____inputfieldThornExp_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExp_12), value);
	}

	inline static int32_t get_offset_of__inputfieldThornMoney_13() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornMoney_13)); }
	inline InputField_t3762917431 * get__inputfieldThornMoney_13() const { return ____inputfieldThornMoney_13; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornMoney_13() { return &____inputfieldThornMoney_13; }
	inline void set__inputfieldThornMoney_13(InputField_t3762917431 * value)
	{
		____inputfieldThornMoney_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornMoney_13), value);
	}

	inline static int32_t get_offset_of__inputfieldThornBuffId_14() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornBuffId_14)); }
	inline InputField_t3762917431 * get__inputfieldThornBuffId_14() const { return ____inputfieldThornBuffId_14; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornBuffId_14() { return &____inputfieldThornBuffId_14; }
	inline void set__inputfieldThornBuffId_14(InputField_t3762917431 * value)
	{
		____inputfieldThornBuffId_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornBuffId_14), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeChildNum_15() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeChildNum_15)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeChildNum_15() const { return ____inputfieldThornExplodeChildNum_15; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeChildNum_15() { return &____inputfieldThornExplodeChildNum_15; }
	inline void set__inputfieldThornExplodeChildNum_15(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeChildNum_15 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeChildNum_15), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeMotherLeftPercent_16() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeMotherLeftPercent_16)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeMotherLeftPercent_16() const { return ____inputfieldThornExplodeMotherLeftPercent_16; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeMotherLeftPercent_16() { return &____inputfieldThornExplodeMotherLeftPercent_16; }
	inline void set__inputfieldThornExplodeMotherLeftPercent_16(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeMotherLeftPercent_16 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeMotherLeftPercent_16), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeRunDistance_17() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeRunDistance_17)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeRunDistance_17() const { return ____inputfieldThornExplodeRunDistance_17; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeRunDistance_17() { return &____inputfieldThornExplodeRunDistance_17; }
	inline void set__inputfieldThornExplodeRunDistance_17(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeRunDistance_17 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeRunDistance_17), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeRunTime_18() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeRunTime_18)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeRunTime_18() const { return ____inputfieldThornExplodeRunTime_18; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeRunTime_18() { return &____inputfieldThornExplodeRunTime_18; }
	inline void set__inputfieldThornExplodeRunTime_18(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeRunTime_18 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeRunTime_18), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeStay_19() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeStay_19)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeStay_19() const { return ____inputfieldThornExplodeStay_19; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeStay_19() { return &____inputfieldThornExplodeStay_19; }
	inline void set__inputfieldThornExplodeStay_19(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeStay_19 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeStay_19), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeShellTime_20() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeShellTime_20)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeShellTime_20() const { return ____inputfieldThornExplodeShellTime_20; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeShellTime_20() { return &____inputfieldThornExplodeShellTime_20; }
	inline void set__inputfieldThornExplodeShellTime_20(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeShellTime_20 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeShellTime_20), value);
	}

	inline static int32_t get_offset_of__inputfieldThornExplodeFormatioszId_21() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornExplodeFormatioszId_21)); }
	inline InputField_t3762917431 * get__inputfieldThornExplodeFormatioszId_21() const { return ____inputfieldThornExplodeFormatioszId_21; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornExplodeFormatioszId_21() { return &____inputfieldThornExplodeFormatioszId_21; }
	inline void set__inputfieldThornExplodeFormatioszId_21(InputField_t3762917431 * value)
	{
		____inputfieldThornExplodeFormatioszId_21 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornExplodeFormatioszId_21), value);
	}

	inline static int32_t get_offset_of__inputfieldThornSkillName_22() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ____inputfieldThornSkillName_22)); }
	inline InputField_t3762917431 * get__inputfieldThornSkillName_22() const { return ____inputfieldThornSkillName_22; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornSkillName_22() { return &____inputfieldThornSkillName_22; }
	inline void set__inputfieldThornSkillName_22(InputField_t3762917431 * value)
	{
		____inputfieldThornSkillName_22 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornSkillName_22), value);
	}

	inline static int32_t get_offset_of_m_dicMonsterConfig_25() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_dicMonsterConfig_25)); }
	inline Dictionary_2_t1027700750 * get_m_dicMonsterConfig_25() const { return ___m_dicMonsterConfig_25; }
	inline Dictionary_2_t1027700750 ** get_address_of_m_dicMonsterConfig_25() { return &___m_dicMonsterConfig_25; }
	inline void set_m_dicMonsterConfig_25(Dictionary_2_t1027700750 * value)
	{
		___m_dicMonsterConfig_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicMonsterConfig_25), value);
	}

	inline static int32_t get_offset_of_tempMonsterConfig_26() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___tempMonsterConfig_26)); }
	inline sThornConfig_t1242444451  get_tempMonsterConfig_26() const { return ___tempMonsterConfig_26; }
	inline sThornConfig_t1242444451 * get_address_of_tempMonsterConfig_26() { return &___tempMonsterConfig_26; }
	inline void set_tempMonsterConfig_26(sThornConfig_t1242444451  value)
	{
		___tempMonsterConfig_26 = value;
	}

	inline static int32_t get_offset_of_m_CurThornConfig_27() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_CurThornConfig_27)); }
	inline sThornConfig_t1242444451  get_m_CurThornConfig_27() const { return ___m_CurThornConfig_27; }
	inline sThornConfig_t1242444451 * get_address_of_m_CurThornConfig_27() { return &___m_CurThornConfig_27; }
	inline void set_m_CurThornConfig_27(sThornConfig_t1242444451  value)
	{
		___m_CurThornConfig_27 = value;
	}

	inline static int32_t get_offset_of_dicTemp_28() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___dicTemp_28)); }
	inline Dictionary_2_t1632706988 * get_dicTemp_28() const { return ___dicTemp_28; }
	inline Dictionary_2_t1632706988 ** get_address_of_dicTemp_28() { return &___dicTemp_28; }
	inline void set_dicTemp_28(Dictionary_2_t1632706988 * value)
	{
		___dicTemp_28 = value;
		Il2CppCodeGenWriteBarrier((&___dicTemp_28), value);
	}

	inline static int32_t get_offset_of_m_goBeanContainer_34() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_goBeanContainer_34)); }
	inline GameObject_t1113636619 * get_m_goBeanContainer_34() const { return ___m_goBeanContainer_34; }
	inline GameObject_t1113636619 ** get_address_of_m_goBeanContainer_34() { return &___m_goBeanContainer_34; }
	inline void set_m_goBeanContainer_34(GameObject_t1113636619 * value)
	{
		___m_goBeanContainer_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBeanContainer_34), value);
	}

	inline static int32_t get_offset_of_m_sprBeanPic_35() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_sprBeanPic_35)); }
	inline Sprite_t280657092 * get_m_sprBeanPic_35() const { return ___m_sprBeanPic_35; }
	inline Sprite_t280657092 ** get_address_of_m_sprBeanPic_35() { return &___m_sprBeanPic_35; }
	inline void set_m_sprBeanPic_35(Sprite_t280657092 * value)
	{
		___m_sprBeanPic_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBeanPic_35), value);
	}

	inline static int32_t get_offset_of_m_sprBeanDeadPic_36() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_sprBeanDeadPic_36)); }
	inline Sprite_t280657092 * get_m_sprBeanDeadPic_36() const { return ___m_sprBeanDeadPic_36; }
	inline Sprite_t280657092 ** get_address_of_m_sprBeanDeadPic_36() { return &___m_sprBeanDeadPic_36; }
	inline void set_m_sprBeanDeadPic_36(Sprite_t280657092 * value)
	{
		___m_sprBeanDeadPic_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBeanDeadPic_36), value);
	}

	inline static int32_t get_offset_of_m_sprBeanCollection_37() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_sprBeanCollection_37)); }
	inline Sprite_t280657092 * get_m_sprBeanCollection_37() const { return ___m_sprBeanCollection_37; }
	inline Sprite_t280657092 ** get_address_of_m_sprBeanCollection_37() { return &___m_sprBeanCollection_37; }
	inline void set_m_sprBeanCollection_37(Sprite_t280657092 * value)
	{
		___m_sprBeanCollection_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBeanCollection_37), value);
	}

	inline static int32_t get_offset_of_m_preBeanCollection_38() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_preBeanCollection_38)); }
	inline GameObject_t1113636619 * get_m_preBeanCollection_38() const { return ___m_preBeanCollection_38; }
	inline GameObject_t1113636619 ** get_address_of_m_preBeanCollection_38() { return &___m_preBeanCollection_38; }
	inline void set_m_preBeanCollection_38(GameObject_t1113636619 * value)
	{
		___m_preBeanCollection_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBeanCollection_38), value);
	}

	inline static int32_t get_offset_of_m_ColorBeanDead_39() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_ColorBeanDead_39)); }
	inline ColorU5BU5D_t941916413* get_m_ColorBeanDead_39() const { return ___m_ColorBeanDead_39; }
	inline ColorU5BU5D_t941916413** get_address_of_m_ColorBeanDead_39() { return &___m_ColorBeanDead_39; }
	inline void set_m_ColorBeanDead_39(ColorU5BU5D_t941916413* value)
	{
		___m_ColorBeanDead_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorBeanDead_39), value);
	}

	inline static int32_t get_offset_of_m_ColorBeanLive_40() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_ColorBeanLive_40)); }
	inline ColorU5BU5D_t941916413* get_m_ColorBeanLive_40() const { return ___m_ColorBeanLive_40; }
	inline ColorU5BU5D_t941916413** get_address_of_m_ColorBeanLive_40() { return &___m_ColorBeanLive_40; }
	inline void set_m_ColorBeanLive_40(ColorU5BU5D_t941916413* value)
	{
		___m_ColorBeanLive_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorBeanLive_40), value);
	}

	inline static int32_t get_offset_of_m_ColorAllEmpty_41() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_ColorAllEmpty_41)); }
	inline ColorU5BU5D_t941916413* get_m_ColorAllEmpty_41() const { return ___m_ColorAllEmpty_41; }
	inline ColorU5BU5D_t941916413** get_address_of_m_ColorAllEmpty_41() { return &___m_ColorAllEmpty_41; }
	inline void set_m_ColorAllEmpty_41(ColorU5BU5D_t941916413* value)
	{
		___m_ColorAllEmpty_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorAllEmpty_41), value);
	}

	inline static int32_t get_offset_of_m_lstBeanCollection_42() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_lstBeanCollection_42)); }
	inline List_1_t4044148548 * get_m_lstBeanCollection_42() const { return ___m_lstBeanCollection_42; }
	inline List_1_t4044148548 ** get_address_of_m_lstBeanCollection_42() { return &___m_lstBeanCollection_42; }
	inline void set_m_lstBeanCollection_42(List_1_t4044148548 * value)
	{
		___m_lstBeanCollection_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeanCollection_42), value);
	}

	inline static int32_t get_offset_of_m_nCollectionGuid_43() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_nCollectionGuid_43)); }
	inline int32_t get_m_nCollectionGuid_43() const { return ___m_nCollectionGuid_43; }
	inline int32_t* get_address_of_m_nCollectionGuid_43() { return &___m_nCollectionGuid_43; }
	inline void set_m_nCollectionGuid_43(int32_t value)
	{
		___m_nCollectionGuid_43 = value;
	}

	inline static int32_t get_offset_of_m_lstLocalPos_44() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_lstLocalPos_44)); }
	inline List_1_t2929260728 * get_m_lstLocalPos_44() const { return ___m_lstLocalPos_44; }
	inline List_1_t2929260728 ** get_address_of_m_lstLocalPos_44() { return &___m_lstLocalPos_44; }
	inline void set_m_lstLocalPos_44(List_1_t2929260728 * value)
	{
		___m_lstLocalPos_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstLocalPos_44), value);
	}

	inline static int32_t get_offset_of_m_lstTexPos_45() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_lstTexPos_45)); }
	inline List_1_t2929260728 * get_m_lstTexPos_45() const { return ___m_lstTexPos_45; }
	inline List_1_t2929260728 ** get_address_of_m_lstTexPos_45() { return &___m_lstTexPos_45; }
	inline void set_m_lstTexPos_45(List_1_t2929260728 * value)
	{
		___m_lstTexPos_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTexPos_45), value);
	}

	inline static int32_t get_offset_of_m_sprBeanCollectionEmpty_46() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_sprBeanCollectionEmpty_46)); }
	inline Sprite_t280657092 * get_m_sprBeanCollectionEmpty_46() const { return ___m_sprBeanCollectionEmpty_46; }
	inline Sprite_t280657092 ** get_address_of_m_sprBeanCollectionEmpty_46() { return &___m_sprBeanCollectionEmpty_46; }
	inline void set_m_sprBeanCollectionEmpty_46(Sprite_t280657092 * value)
	{
		___m_sprBeanCollectionEmpty_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBeanCollectionEmpty_46), value);
	}

	inline static int32_t get_offset_of_m_dicBeanColor_47() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_dicBeanColor_47)); }
	inline Dictionary_2_t727172712 * get_m_dicBeanColor_47() const { return ___m_dicBeanColor_47; }
	inline Dictionary_2_t727172712 ** get_address_of_m_dicBeanColor_47() { return &___m_dicBeanColor_47; }
	inline void set_m_dicBeanColor_47(Dictionary_2_t727172712 * value)
	{
		___m_dicBeanColor_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBeanColor_47), value);
	}

	inline static int32_t get_offset_of_m_dicBeanCollections_48() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_dicBeanCollections_48)); }
	inline Dictionary_2_t2357330105 * get_m_dicBeanCollections_48() const { return ___m_dicBeanCollections_48; }
	inline Dictionary_2_t2357330105 ** get_address_of_m_dicBeanCollections_48() { return &___m_dicBeanCollections_48; }
	inline void set_m_dicBeanCollections_48(Dictionary_2_t2357330105 * value)
	{
		___m_dicBeanCollections_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBeanCollections_48), value);
	}

	inline static int32_t get_offset_of_m_dicBeanCollectionsByGuid_49() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_dicBeanCollectionsByGuid_49)); }
	inline Dictionary_2_t1460787137 * get_m_dicBeanCollectionsByGuid_49() const { return ___m_dicBeanCollectionsByGuid_49; }
	inline Dictionary_2_t1460787137 ** get_address_of_m_dicBeanCollectionsByGuid_49() { return &___m_dicBeanCollectionsByGuid_49; }
	inline void set_m_dicBeanCollectionsByGuid_49(Dictionary_2_t1460787137 * value)
	{
		___m_dicBeanCollectionsByGuid_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBeanCollectionsByGuid_49), value);
	}

	inline static int32_t get_offset_of_m_nCollectionIndex_50() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_nCollectionIndex_50)); }
	inline int32_t get_m_nCollectionIndex_50() const { return ___m_nCollectionIndex_50; }
	inline int32_t* get_address_of_m_nCollectionIndex_50() { return &___m_nCollectionIndex_50; }
	inline void set_m_nCollectionIndex_50(int32_t value)
	{
		___m_nCollectionIndex_50 = value;
	}

	inline static int32_t get_offset_of_m_nQuKuai_51() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_nQuKuai_51)); }
	inline int32_t get_m_nQuKuai_51() const { return ___m_nQuKuai_51; }
	inline int32_t* get_address_of_m_nQuKuai_51() { return &___m_nQuKuai_51; }
	inline void set_m_nQuKuai_51(int32_t value)
	{
		___m_nQuKuai_51 = value;
	}

	inline static int32_t get_offset_of_m_nQuKuaiIndex_i_52() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_nQuKuaiIndex_i_52)); }
	inline int32_t get_m_nQuKuaiIndex_i_52() const { return ___m_nQuKuaiIndex_i_52; }
	inline int32_t* get_address_of_m_nQuKuaiIndex_i_52() { return &___m_nQuKuaiIndex_i_52; }
	inline void set_m_nQuKuaiIndex_i_52(int32_t value)
	{
		___m_nQuKuaiIndex_i_52 = value;
	}

	inline static int32_t get_offset_of_m_nQuKuaiIndex_j_53() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_nQuKuaiIndex_j_53)); }
	inline int32_t get_m_nQuKuaiIndex_j_53() const { return ___m_nQuKuaiIndex_j_53; }
	inline int32_t* get_address_of_m_nQuKuaiIndex_j_53() { return &___m_nQuKuaiIndex_j_53; }
	inline void set_m_nQuKuaiIndex_j_53(int32_t value)
	{
		___m_nQuKuaiIndex_j_53 = value;
	}

	inline static int32_t get_offset_of_m_bInitQuKuaiCompleted_54() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_bInitQuKuaiCompleted_54)); }
	inline bool get_m_bInitQuKuaiCompleted_54() const { return ___m_bInitQuKuaiCompleted_54; }
	inline bool* get_address_of_m_bInitQuKuaiCompleted_54() { return &___m_bInitQuKuaiCompleted_54; }
	inline void set_m_bInitQuKuaiCompleted_54(bool value)
	{
		___m_bInitQuKuaiCompleted_54 = value;
	}

	inline static int32_t get_offset_of_m_nTotalNUm_55() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_nTotalNUm_55)); }
	inline int32_t get_m_nTotalNUm_55() const { return ___m_nTotalNUm_55; }
	inline int32_t* get_address_of_m_nTotalNUm_55() { return &___m_nTotalNUm_55; }
	inline void set_m_nTotalNUm_55(int32_t value)
	{
		___m_nTotalNUm_55 = value;
	}

	inline static int32_t get_offset_of_m_bInitQuKuaiStarted_56() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_bInitQuKuaiStarted_56)); }
	inline bool get_m_bInitQuKuaiStarted_56() const { return ___m_bInitQuKuaiStarted_56; }
	inline bool* get_address_of_m_bInitQuKuaiStarted_56() { return &___m_bInitQuKuaiStarted_56; }
	inline void set_m_bInitQuKuaiStarted_56(bool value)
	{
		___m_bInitQuKuaiStarted_56 = value;
	}

	inline static int32_t get_offset_of_m_fBeanInteractBallTimeCount_57() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_fBeanInteractBallTimeCount_57)); }
	inline float get_m_fBeanInteractBallTimeCount_57() const { return ___m_fBeanInteractBallTimeCount_57; }
	inline float* get_address_of_m_fBeanInteractBallTimeCount_57() { return &___m_fBeanInteractBallTimeCount_57; }
	inline void set_m_fBeanInteractBallTimeCount_57(float value)
	{
		___m_fBeanInteractBallTimeCount_57 = value;
	}

	inline static int32_t get_offset_of_m_lstBeanRebornList_58() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_lstBeanRebornList_58)); }
	inline List_1_t4257725728 * get_m_lstBeanRebornList_58() const { return ___m_lstBeanRebornList_58; }
	inline List_1_t4257725728 ** get_address_of_m_lstBeanRebornList_58() { return &___m_lstBeanRebornList_58; }
	inline void set_m_lstBeanRebornList_58(List_1_t4257725728 * value)
	{
		___m_lstBeanRebornList_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeanRebornList_58), value);
	}

	inline static int32_t get_offset_of_m_fBeanRebornLoopTimeCount_59() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_fBeanRebornLoopTimeCount_59)); }
	inline float get_m_fBeanRebornLoopTimeCount_59() const { return ___m_fBeanRebornLoopTimeCount_59; }
	inline float* get_address_of_m_fBeanRebornLoopTimeCount_59() { return &___m_fBeanRebornLoopTimeCount_59; }
	inline void set_m_fBeanRebornLoopTimeCount_59(float value)
	{
		___m_fBeanRebornLoopTimeCount_59 = value;
	}

	inline static int32_t get_offset_of_m_lstInteractingCollection_60() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_lstInteractingCollection_60)); }
	inline List_1_t4044148548 * get_m_lstInteractingCollection_60() const { return ___m_lstInteractingCollection_60; }
	inline List_1_t4044148548 ** get_address_of_m_lstInteractingCollection_60() { return &___m_lstInteractingCollection_60; }
	inline void set_m_lstInteractingCollection_60(List_1_t4044148548 * value)
	{
		___m_lstInteractingCollection_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstInteractingCollection_60), value);
	}

	inline static int32_t get_offset_of_m_lstGenerateBeanNodeList_62() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249, ___m_lstGenerateBeanNodeList_62)); }
	inline List_1_t2282564592 * get_m_lstGenerateBeanNodeList_62() const { return ___m_lstGenerateBeanNodeList_62; }
	inline List_1_t2282564592 ** get_address_of_m_lstGenerateBeanNodeList_62() { return &___m_lstGenerateBeanNodeList_62; }
	inline void set_m_lstGenerateBeanNodeList_62(List_1_t2282564592 * value)
	{
		___m_lstGenerateBeanNodeList_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstGenerateBeanNodeList_62), value);
	}
};

struct CMonsterEditor_t2594843249_StaticFields
{
public:
	// UnityEngine.Vector3 CMonsterEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_23;
	// CMonsterEditor CMonsterEditor::s_Instance
	CMonsterEditor_t2594843249 * ___s_Instance_24;

public:
	inline static int32_t get_offset_of_vecTempPos_23() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249_StaticFields, ___vecTempPos_23)); }
	inline Vector3_t3722313464  get_vecTempPos_23() const { return ___vecTempPos_23; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_23() { return &___vecTempPos_23; }
	inline void set_vecTempPos_23(Vector3_t3722313464  value)
	{
		___vecTempPos_23 = value;
	}

	inline static int32_t get_offset_of_s_Instance_24() { return static_cast<int32_t>(offsetof(CMonsterEditor_t2594843249_StaticFields, ___s_Instance_24)); }
	inline CMonsterEditor_t2594843249 * get_s_Instance_24() const { return ___s_Instance_24; }
	inline CMonsterEditor_t2594843249 ** get_address_of_s_Instance_24() { return &___s_Instance_24; }
	inline void set_s_Instance_24(CMonsterEditor_t2594843249 * value)
	{
		___s_Instance_24 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMONSTEREDITOR_T2594843249_H
#ifndef CPLAYERINFO_T53183828_H
#define CPLAYERINFO_T53183828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerInfo
struct  CPlayerInfo_t53183828  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CPlayerInfo::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_2;
	// UnityEngine.UI.Text CPlayerInfo::_txtPlayerLevel
	Text_t1901882714 * ____txtPlayerLevel_3;

public:
	inline static int32_t get_offset_of__txtPlayerName_2() { return static_cast<int32_t>(offsetof(CPlayerInfo_t53183828, ____txtPlayerName_2)); }
	inline Text_t1901882714 * get__txtPlayerName_2() const { return ____txtPlayerName_2; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_2() { return &____txtPlayerName_2; }
	inline void set__txtPlayerName_2(Text_t1901882714 * value)
	{
		____txtPlayerName_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_2), value);
	}

	inline static int32_t get_offset_of__txtPlayerLevel_3() { return static_cast<int32_t>(offsetof(CPlayerInfo_t53183828, ____txtPlayerLevel_3)); }
	inline Text_t1901882714 * get__txtPlayerLevel_3() const { return ____txtPlayerLevel_3; }
	inline Text_t1901882714 ** get_address_of__txtPlayerLevel_3() { return &____txtPlayerLevel_3; }
	inline void set__txtPlayerLevel_3(Text_t1901882714 * value)
	{
		____txtPlayerLevel_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerLevel_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPLAYERINFO_T53183828_H
#ifndef CWIFI_T3890179523_H
#define CWIFI_T3890179523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CWifi
struct  CWifi_t3890179523  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CWifi::m_aryGrid
	GameObjectU5BU5D_t3328599146* ___m_aryGrid_2;

public:
	inline static int32_t get_offset_of_m_aryGrid_2() { return static_cast<int32_t>(offsetof(CWifi_t3890179523, ___m_aryGrid_2)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryGrid_2() const { return ___m_aryGrid_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryGrid_2() { return &___m_aryGrid_2; }
	inline void set_m_aryGrid_2(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryGrid_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryGrid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CWIFI_T3890179523_H
#ifndef CYBERTREESCROLLVIEW_T257211719_H
#define CYBERTREESCROLLVIEW_T257211719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CyberTreeScrollView
struct  CyberTreeScrollView_t257211719  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CyberTreeScrollView::m_goContent
	GameObject_t1113636619 * ___m_goContent_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CyberTreeScrollView::m_lstItems
	List_1_t2585711361 * ___m_lstItems_4;

public:
	inline static int32_t get_offset_of_m_goContent_3() { return static_cast<int32_t>(offsetof(CyberTreeScrollView_t257211719, ___m_goContent_3)); }
	inline GameObject_t1113636619 * get_m_goContent_3() const { return ___m_goContent_3; }
	inline GameObject_t1113636619 ** get_address_of_m_goContent_3() { return &___m_goContent_3; }
	inline void set_m_goContent_3(GameObject_t1113636619 * value)
	{
		___m_goContent_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContent_3), value);
	}

	inline static int32_t get_offset_of_m_lstItems_4() { return static_cast<int32_t>(offsetof(CyberTreeScrollView_t257211719, ___m_lstItems_4)); }
	inline List_1_t2585711361 * get_m_lstItems_4() const { return ___m_lstItems_4; }
	inline List_1_t2585711361 ** get_address_of_m_lstItems_4() { return &___m_lstItems_4; }
	inline void set_m_lstItems_4(List_1_t2585711361 * value)
	{
		___m_lstItems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_4), value);
	}
};

struct CyberTreeScrollView_t257211719_StaticFields
{
public:
	// UnityEngine.Vector3 CyberTreeScrollView::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CyberTreeScrollView_t257211719_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYBERTREESCROLLVIEW_T257211719_H
#ifndef CBUTTONCLICK_T2942357293_H
#define CBUTTONCLICK_T2942357293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CButtonClick
struct  CButtonClick_t2942357293  : public MonoBehaviour_t3962482529
{
public:
	// CFrameAnimationEffect CButtonClick::_effectClick
	CFrameAnimationEffect_t443605508 * ____effectClick_2;
	// UnityEngine.UI.Image CButtonClick::_imgMain
	Image_t2670269651 * ____imgMain_3;
	// UnityEngine.UI.Text CButtonClick::_txtTitle
	Text_t1901882714 * ____txtTitle_4;
	// UnityEngine.Sprite[] CButtonClick::_aryButtonStatus
	SpriteU5BU5D_t2581906349* ____aryButtonStatus_5;
	// System.String[] CButtonClick::_arySzButtonColors
	StringU5BU5D_t1281789340* ____arySzButtonColors_6;
	// UnityEngine.Color[] CButtonClick::_aryButtonColors
	ColorU5BU5D_t941916413* ____aryButtonColors_7;

public:
	inline static int32_t get_offset_of__effectClick_2() { return static_cast<int32_t>(offsetof(CButtonClick_t2942357293, ____effectClick_2)); }
	inline CFrameAnimationEffect_t443605508 * get__effectClick_2() const { return ____effectClick_2; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectClick_2() { return &____effectClick_2; }
	inline void set__effectClick_2(CFrameAnimationEffect_t443605508 * value)
	{
		____effectClick_2 = value;
		Il2CppCodeGenWriteBarrier((&____effectClick_2), value);
	}

	inline static int32_t get_offset_of__imgMain_3() { return static_cast<int32_t>(offsetof(CButtonClick_t2942357293, ____imgMain_3)); }
	inline Image_t2670269651 * get__imgMain_3() const { return ____imgMain_3; }
	inline Image_t2670269651 ** get_address_of__imgMain_3() { return &____imgMain_3; }
	inline void set__imgMain_3(Image_t2670269651 * value)
	{
		____imgMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_3), value);
	}

	inline static int32_t get_offset_of__txtTitle_4() { return static_cast<int32_t>(offsetof(CButtonClick_t2942357293, ____txtTitle_4)); }
	inline Text_t1901882714 * get__txtTitle_4() const { return ____txtTitle_4; }
	inline Text_t1901882714 ** get_address_of__txtTitle_4() { return &____txtTitle_4; }
	inline void set__txtTitle_4(Text_t1901882714 * value)
	{
		____txtTitle_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitle_4), value);
	}

	inline static int32_t get_offset_of__aryButtonStatus_5() { return static_cast<int32_t>(offsetof(CButtonClick_t2942357293, ____aryButtonStatus_5)); }
	inline SpriteU5BU5D_t2581906349* get__aryButtonStatus_5() const { return ____aryButtonStatus_5; }
	inline SpriteU5BU5D_t2581906349** get_address_of__aryButtonStatus_5() { return &____aryButtonStatus_5; }
	inline void set__aryButtonStatus_5(SpriteU5BU5D_t2581906349* value)
	{
		____aryButtonStatus_5 = value;
		Il2CppCodeGenWriteBarrier((&____aryButtonStatus_5), value);
	}

	inline static int32_t get_offset_of__arySzButtonColors_6() { return static_cast<int32_t>(offsetof(CButtonClick_t2942357293, ____arySzButtonColors_6)); }
	inline StringU5BU5D_t1281789340* get__arySzButtonColors_6() const { return ____arySzButtonColors_6; }
	inline StringU5BU5D_t1281789340** get_address_of__arySzButtonColors_6() { return &____arySzButtonColors_6; }
	inline void set__arySzButtonColors_6(StringU5BU5D_t1281789340* value)
	{
		____arySzButtonColors_6 = value;
		Il2CppCodeGenWriteBarrier((&____arySzButtonColors_6), value);
	}

	inline static int32_t get_offset_of__aryButtonColors_7() { return static_cast<int32_t>(offsetof(CButtonClick_t2942357293, ____aryButtonColors_7)); }
	inline ColorU5BU5D_t941916413* get__aryButtonColors_7() const { return ____aryButtonColors_7; }
	inline ColorU5BU5D_t941916413** get_address_of__aryButtonColors_7() { return &____aryButtonColors_7; }
	inline void set__aryButtonColors_7(ColorU5BU5D_t941916413* value)
	{
		____aryButtonColors_7 = value;
		Il2CppCodeGenWriteBarrier((&____aryButtonColors_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBUTTONCLICK_T2942357293_H
#ifndef CCLASSEDITOR_T335744478_H
#define CCLASSEDITOR_T335744478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CClassEditor
struct  CClassEditor_t335744478  : public MonoBehaviour_t3962482529
{
public:
	// CMonsterEditor CClassEditor::_MonsterEditor
	CMonsterEditor_t2594843249 * ____MonsterEditor_2;
	// UnityEngine.GameObject CClassEditor::_goClassCircle
	GameObject_t1113636619 * ____goClassCircle_7;
	// CMonster CClassEditor::_cShengFuPanDingBall
	CMonster_t1941470938 * ____cShengFuPanDingBall_8;
	// UnityEngine.GameObject CClassEditor::_goContainer_MannualMonster
	GameObject_t1113636619 * ____goContainer_MannualMonster_9;
	// UnityEngine.UI.InputField CClassEditor::_inputTimeOfOneGame
	InputField_t3762917431 * ____inputTimeOfOneGame_10;
	// UnityEngine.UI.InputField CClassEditor::_inputAttenuateRate
	InputField_t3762917431 * ____inputAttenuateRate_11;
	// UnityEngine.UI.InputField CClassEditor::_inputClassCircleRadius
	InputField_t3762917431 * ____inputClassCircleRadius_12;
	// UnityEngine.UI.InputField CClassEditor::_inputShengFuPanDingBallSize
	InputField_t3762917431 * ____inputShengFuPanDingBallSize_13;
	// UnityEngine.UI.Dropdown CClassEditor::_dropArea
	Dropdown_t2274391225 * ____dropArea_14;
	// UnityEngine.UI.Dropdown CClassEditor::_dropThornType
	Dropdown_t2274391225 * ____dropThornType_15;
	// UnityEngine.UI.Dropdown CClassEditor::_dropThornId
	Dropdown_t2274391225 * ____dropThornId_16;
	// UnityEngine.UI.InputField CClassEditor::_inputSizeThreshold
	InputField_t3762917431 * ____inputSizeThreshold_17;
	// UnityEngine.UI.InputField CClassEditor::_inputDensity
	InputField_t3762917431 * ____inputDensity_18;
	// UnityEngine.UI.InputField CClassEditor::_inputRebornTime
	InputField_t3762917431 * ____inputRebornTime_19;
	// UnityEngine.UI.InputField CClassEditor::_inputRebornTimeMannual
	InputField_t3762917431 * ____inputRebornTimeMannual_20;
	// UnityEngine.UI.InputField CClassEditor::_inputNormalAttenuate
	InputField_t3762917431 * ____inputNormalAttenuate_21;
	// UnityEngine.UI.InputField CClassEditor::_inputInvasionAttenuate
	InputField_t3762917431 * ____inputInvasionAttenuate_22;
	// UnityEngine.UI.InputField CClassEditor::_inputInvasionSpeedSlowDown
	InputField_t3762917431 * ____inputInvasionSpeedSlowDown_23;
	// UnityEngine.UI.InputField CClassEditor::_inputWarFogRadius
	InputField_t3762917431 * ____inputWarFogRadius_24;
	// UnityEngine.UI.Button CClassEditor::_btnAddOneMonster
	Button_t4055032469 * ____btnAddOneMonster_25;
	// UnityEngine.UI.Button CClassEditor::_btnDeleteOneMonster
	Button_t4055032469 * ____btnDeleteOneMonster_26;
	// UnityEngine.UI.Text CClassEditor::_txtMonsterNum
	Text_t1901882714 * ____txtMonsterNum_27;
	// System.Single CClassEditor::m_fTimeOfOneGame
	float ___m_fTimeOfOneGame_28;
	// System.Single CClassEditor::m_fClassCircleRadius
	float ___m_fClassCircleRadius_29;
	// System.Single CClassEditor::m_fShengFuPanDingBallSize
	float ___m_fShengFuPanDingBallSize_30;
	// System.Single CClassEditor::m_fLowThreshold
	float ___m_fLowThreshold_31;
	// System.Single CClassEditor::m_fMiddleThreshold
	float ___m_fMiddleThreshold_32;
	// System.Single CClassEditor::m_fHighThreshold
	float ___m_fHighThreshold_33;
	// System.Single CClassEditor::m_fWarFogRadius
	float ___m_fWarFogRadius_34;
	// System.Single CClassEditor::m_fAttenuateRate
	float ___m_fAttenuateRate_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,CClassEditor/sClassConfig> CClassEditor::m_dicClassConfig
	Dictionary_2_t3948399185 * ___m_dicClassConfig_36;
	// CClassEditor/sClassConfig CClassEditor::m_CurConfig
	sClassConfig_t764718558  ___m_CurConfig_37;
	// CClassEditor/sThornOfThisClassConfig CClassEditor::m_CurThornConfig
	sThornOfThisClassConfig_t350464872  ___m_CurThornConfig_38;
	// CClassEditor/sMannualMonsterConfig CClassEditor::m_CurMannualMonsterConfig
	sMannualMonsterConfig_t1844217021  ___m_CurMannualMonsterConfig_39;
	// System.Collections.Generic.Dictionary`2<System.String,CClassEditor/sMannualMonsterConfig> CClassEditor::m_dicMannualMonsterConfig
	Dictionary_2_t1629473320 * ___m_dicMannualMonsterConfig_40;
	// CClassEditor/sMannualMonsterConfig CClassEditor::tempMannualMonsterConfig
	sMannualMonsterConfig_t1844217021  ___tempMannualMonsterConfig_41;
	// CMonster CClassEditor::m_CurMovingMonster
	CMonster_t1941470938 * ___m_CurMovingMonster_43;
	// CMonster CClassEditor::m_CurSelectedMonster
	CMonster_t1941470938 * ___m_CurSelectedMonster_44;
	// System.Boolean CClassEditor::m_bMouseDown
	bool ___m_bMouseDown_45;
	// UnityEngine.Vector3 CClassEditor::m_vecLastMouseWorldPos
	Vector3_t3722313464  ___m_vecLastMouseWorldPos_46;
	// CClassEditor/sThornOfThisClassConfig CClassEditor::tempThornConfig
	sThornOfThisClassConfig_t350464872  ___tempThornConfig_47;
	// System.Collections.Generic.Dictionary`2<System.String,CMonster> CClassEditor::m_dicAllSpores
	Dictionary_2_t1726727237 * ___m_dicAllSpores_49;
	// System.Collections.Generic.Dictionary`2<System.Int64,CMonster> CClassEditor::m_dicAllThorns
	Dictionary_2_t3004110722 * ___m_dicAllThorns_50;
	// System.Collections.Generic.List`1<CMonster> CClassEditor::m_lstMonstersX
	List_1_t3413545680 * ___m_lstMonstersX_51;
	// System.Collections.Generic.List`1<CMonster> CClassEditor::m_lstMonstersY
	List_1_t3413545680 * ___m_lstMonstersY_52;
	// System.Int64 CClassEditor::m_lGuidCount
	int64_t ___m_lGuidCount_53;
	// System.Single CClassEditor::m_fCheckInCamViewTimeCount
	float ___m_fCheckInCamViewTimeCount_55;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CClassEditor::dicTemp
	Dictionary_2_t1839659084 * ___dicTemp_56;
	// System.Collections.Generic.List`1<CMonster> CClassEditor::m_lstActiveMonster
	List_1_t3413545680 * ___m_lstActiveMonster_57;
	// System.Collections.Generic.Dictionary`2<System.Int32,CMonster> CClassEditor::dicTempActiveMonsterOfThisRound
	Dictionary_2_t830184269 * ___dicTempActiveMonsterOfThisRound_58;
	// System.Collections.Generic.List`1<CMonster> CClassEditor::m_lstSyncPublicList
	List_1_t3413545680 * ___m_lstSyncPublicList_59;
	// System.Collections.Generic.List`1<CMonster> CClassEditor::m_lstToBeReborn
	List_1_t3413545680 * ___m_lstToBeReborn_60;
	// System.Collections.Generic.List`1<CMonster> CClassEditor::m_lstCiChuXianThorn
	List_1_t3413545680 * ___m_lstCiChuXianThorn_61;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Color,UnityEngine.MaterialPropertyBlock> CClassEditor::m_dicThornColor
	Dictionary_2_t2658501330 * ___m_dicThornColor_62;
	// System.Single CClassEditor::c_RebornInterval
	float ___c_RebornInterval_63;
	// System.Single CClassEditor::m_fRebornCount
	float ___m_fRebornCount_64;
	// System.Collections.Generic.Dictionary`2<System.Int32,CMonster> CClassEditor::m_dicPushedThornList
	Dictionary_2_t830184269 * ___m_dicPushedThornList_65;
	// System.Int64 CClassEditor::m_lSceneBallGuid
	int64_t ___m_lSceneBallGuid_68;
	// System.Collections.Generic.Dictionary`2<System.Int64,CMonster> CClassEditor::m_dicSceneBalls
	Dictionary_2_t3004110722 * ___m_dicSceneBalls_69;

public:
	inline static int32_t get_offset_of__MonsterEditor_2() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____MonsterEditor_2)); }
	inline CMonsterEditor_t2594843249 * get__MonsterEditor_2() const { return ____MonsterEditor_2; }
	inline CMonsterEditor_t2594843249 ** get_address_of__MonsterEditor_2() { return &____MonsterEditor_2; }
	inline void set__MonsterEditor_2(CMonsterEditor_t2594843249 * value)
	{
		____MonsterEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&____MonsterEditor_2), value);
	}

	inline static int32_t get_offset_of__goClassCircle_7() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____goClassCircle_7)); }
	inline GameObject_t1113636619 * get__goClassCircle_7() const { return ____goClassCircle_7; }
	inline GameObject_t1113636619 ** get_address_of__goClassCircle_7() { return &____goClassCircle_7; }
	inline void set__goClassCircle_7(GameObject_t1113636619 * value)
	{
		____goClassCircle_7 = value;
		Il2CppCodeGenWriteBarrier((&____goClassCircle_7), value);
	}

	inline static int32_t get_offset_of__cShengFuPanDingBall_8() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____cShengFuPanDingBall_8)); }
	inline CMonster_t1941470938 * get__cShengFuPanDingBall_8() const { return ____cShengFuPanDingBall_8; }
	inline CMonster_t1941470938 ** get_address_of__cShengFuPanDingBall_8() { return &____cShengFuPanDingBall_8; }
	inline void set__cShengFuPanDingBall_8(CMonster_t1941470938 * value)
	{
		____cShengFuPanDingBall_8 = value;
		Il2CppCodeGenWriteBarrier((&____cShengFuPanDingBall_8), value);
	}

	inline static int32_t get_offset_of__goContainer_MannualMonster_9() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____goContainer_MannualMonster_9)); }
	inline GameObject_t1113636619 * get__goContainer_MannualMonster_9() const { return ____goContainer_MannualMonster_9; }
	inline GameObject_t1113636619 ** get_address_of__goContainer_MannualMonster_9() { return &____goContainer_MannualMonster_9; }
	inline void set__goContainer_MannualMonster_9(GameObject_t1113636619 * value)
	{
		____goContainer_MannualMonster_9 = value;
		Il2CppCodeGenWriteBarrier((&____goContainer_MannualMonster_9), value);
	}

	inline static int32_t get_offset_of__inputTimeOfOneGame_10() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputTimeOfOneGame_10)); }
	inline InputField_t3762917431 * get__inputTimeOfOneGame_10() const { return ____inputTimeOfOneGame_10; }
	inline InputField_t3762917431 ** get_address_of__inputTimeOfOneGame_10() { return &____inputTimeOfOneGame_10; }
	inline void set__inputTimeOfOneGame_10(InputField_t3762917431 * value)
	{
		____inputTimeOfOneGame_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputTimeOfOneGame_10), value);
	}

	inline static int32_t get_offset_of__inputAttenuateRate_11() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputAttenuateRate_11)); }
	inline InputField_t3762917431 * get__inputAttenuateRate_11() const { return ____inputAttenuateRate_11; }
	inline InputField_t3762917431 ** get_address_of__inputAttenuateRate_11() { return &____inputAttenuateRate_11; }
	inline void set__inputAttenuateRate_11(InputField_t3762917431 * value)
	{
		____inputAttenuateRate_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputAttenuateRate_11), value);
	}

	inline static int32_t get_offset_of__inputClassCircleRadius_12() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputClassCircleRadius_12)); }
	inline InputField_t3762917431 * get__inputClassCircleRadius_12() const { return ____inputClassCircleRadius_12; }
	inline InputField_t3762917431 ** get_address_of__inputClassCircleRadius_12() { return &____inputClassCircleRadius_12; }
	inline void set__inputClassCircleRadius_12(InputField_t3762917431 * value)
	{
		____inputClassCircleRadius_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputClassCircleRadius_12), value);
	}

	inline static int32_t get_offset_of__inputShengFuPanDingBallSize_13() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputShengFuPanDingBallSize_13)); }
	inline InputField_t3762917431 * get__inputShengFuPanDingBallSize_13() const { return ____inputShengFuPanDingBallSize_13; }
	inline InputField_t3762917431 ** get_address_of__inputShengFuPanDingBallSize_13() { return &____inputShengFuPanDingBallSize_13; }
	inline void set__inputShengFuPanDingBallSize_13(InputField_t3762917431 * value)
	{
		____inputShengFuPanDingBallSize_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputShengFuPanDingBallSize_13), value);
	}

	inline static int32_t get_offset_of__dropArea_14() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____dropArea_14)); }
	inline Dropdown_t2274391225 * get__dropArea_14() const { return ____dropArea_14; }
	inline Dropdown_t2274391225 ** get_address_of__dropArea_14() { return &____dropArea_14; }
	inline void set__dropArea_14(Dropdown_t2274391225 * value)
	{
		____dropArea_14 = value;
		Il2CppCodeGenWriteBarrier((&____dropArea_14), value);
	}

	inline static int32_t get_offset_of__dropThornType_15() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____dropThornType_15)); }
	inline Dropdown_t2274391225 * get__dropThornType_15() const { return ____dropThornType_15; }
	inline Dropdown_t2274391225 ** get_address_of__dropThornType_15() { return &____dropThornType_15; }
	inline void set__dropThornType_15(Dropdown_t2274391225 * value)
	{
		____dropThornType_15 = value;
		Il2CppCodeGenWriteBarrier((&____dropThornType_15), value);
	}

	inline static int32_t get_offset_of__dropThornId_16() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____dropThornId_16)); }
	inline Dropdown_t2274391225 * get__dropThornId_16() const { return ____dropThornId_16; }
	inline Dropdown_t2274391225 ** get_address_of__dropThornId_16() { return &____dropThornId_16; }
	inline void set__dropThornId_16(Dropdown_t2274391225 * value)
	{
		____dropThornId_16 = value;
		Il2CppCodeGenWriteBarrier((&____dropThornId_16), value);
	}

	inline static int32_t get_offset_of__inputSizeThreshold_17() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputSizeThreshold_17)); }
	inline InputField_t3762917431 * get__inputSizeThreshold_17() const { return ____inputSizeThreshold_17; }
	inline InputField_t3762917431 ** get_address_of__inputSizeThreshold_17() { return &____inputSizeThreshold_17; }
	inline void set__inputSizeThreshold_17(InputField_t3762917431 * value)
	{
		____inputSizeThreshold_17 = value;
		Il2CppCodeGenWriteBarrier((&____inputSizeThreshold_17), value);
	}

	inline static int32_t get_offset_of__inputDensity_18() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputDensity_18)); }
	inline InputField_t3762917431 * get__inputDensity_18() const { return ____inputDensity_18; }
	inline InputField_t3762917431 ** get_address_of__inputDensity_18() { return &____inputDensity_18; }
	inline void set__inputDensity_18(InputField_t3762917431 * value)
	{
		____inputDensity_18 = value;
		Il2CppCodeGenWriteBarrier((&____inputDensity_18), value);
	}

	inline static int32_t get_offset_of__inputRebornTime_19() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputRebornTime_19)); }
	inline InputField_t3762917431 * get__inputRebornTime_19() const { return ____inputRebornTime_19; }
	inline InputField_t3762917431 ** get_address_of__inputRebornTime_19() { return &____inputRebornTime_19; }
	inline void set__inputRebornTime_19(InputField_t3762917431 * value)
	{
		____inputRebornTime_19 = value;
		Il2CppCodeGenWriteBarrier((&____inputRebornTime_19), value);
	}

	inline static int32_t get_offset_of__inputRebornTimeMannual_20() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputRebornTimeMannual_20)); }
	inline InputField_t3762917431 * get__inputRebornTimeMannual_20() const { return ____inputRebornTimeMannual_20; }
	inline InputField_t3762917431 ** get_address_of__inputRebornTimeMannual_20() { return &____inputRebornTimeMannual_20; }
	inline void set__inputRebornTimeMannual_20(InputField_t3762917431 * value)
	{
		____inputRebornTimeMannual_20 = value;
		Il2CppCodeGenWriteBarrier((&____inputRebornTimeMannual_20), value);
	}

	inline static int32_t get_offset_of__inputNormalAttenuate_21() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputNormalAttenuate_21)); }
	inline InputField_t3762917431 * get__inputNormalAttenuate_21() const { return ____inputNormalAttenuate_21; }
	inline InputField_t3762917431 ** get_address_of__inputNormalAttenuate_21() { return &____inputNormalAttenuate_21; }
	inline void set__inputNormalAttenuate_21(InputField_t3762917431 * value)
	{
		____inputNormalAttenuate_21 = value;
		Il2CppCodeGenWriteBarrier((&____inputNormalAttenuate_21), value);
	}

	inline static int32_t get_offset_of__inputInvasionAttenuate_22() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputInvasionAttenuate_22)); }
	inline InputField_t3762917431 * get__inputInvasionAttenuate_22() const { return ____inputInvasionAttenuate_22; }
	inline InputField_t3762917431 ** get_address_of__inputInvasionAttenuate_22() { return &____inputInvasionAttenuate_22; }
	inline void set__inputInvasionAttenuate_22(InputField_t3762917431 * value)
	{
		____inputInvasionAttenuate_22 = value;
		Il2CppCodeGenWriteBarrier((&____inputInvasionAttenuate_22), value);
	}

	inline static int32_t get_offset_of__inputInvasionSpeedSlowDown_23() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputInvasionSpeedSlowDown_23)); }
	inline InputField_t3762917431 * get__inputInvasionSpeedSlowDown_23() const { return ____inputInvasionSpeedSlowDown_23; }
	inline InputField_t3762917431 ** get_address_of__inputInvasionSpeedSlowDown_23() { return &____inputInvasionSpeedSlowDown_23; }
	inline void set__inputInvasionSpeedSlowDown_23(InputField_t3762917431 * value)
	{
		____inputInvasionSpeedSlowDown_23 = value;
		Il2CppCodeGenWriteBarrier((&____inputInvasionSpeedSlowDown_23), value);
	}

	inline static int32_t get_offset_of__inputWarFogRadius_24() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____inputWarFogRadius_24)); }
	inline InputField_t3762917431 * get__inputWarFogRadius_24() const { return ____inputWarFogRadius_24; }
	inline InputField_t3762917431 ** get_address_of__inputWarFogRadius_24() { return &____inputWarFogRadius_24; }
	inline void set__inputWarFogRadius_24(InputField_t3762917431 * value)
	{
		____inputWarFogRadius_24 = value;
		Il2CppCodeGenWriteBarrier((&____inputWarFogRadius_24), value);
	}

	inline static int32_t get_offset_of__btnAddOneMonster_25() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____btnAddOneMonster_25)); }
	inline Button_t4055032469 * get__btnAddOneMonster_25() const { return ____btnAddOneMonster_25; }
	inline Button_t4055032469 ** get_address_of__btnAddOneMonster_25() { return &____btnAddOneMonster_25; }
	inline void set__btnAddOneMonster_25(Button_t4055032469 * value)
	{
		____btnAddOneMonster_25 = value;
		Il2CppCodeGenWriteBarrier((&____btnAddOneMonster_25), value);
	}

	inline static int32_t get_offset_of__btnDeleteOneMonster_26() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____btnDeleteOneMonster_26)); }
	inline Button_t4055032469 * get__btnDeleteOneMonster_26() const { return ____btnDeleteOneMonster_26; }
	inline Button_t4055032469 ** get_address_of__btnDeleteOneMonster_26() { return &____btnDeleteOneMonster_26; }
	inline void set__btnDeleteOneMonster_26(Button_t4055032469 * value)
	{
		____btnDeleteOneMonster_26 = value;
		Il2CppCodeGenWriteBarrier((&____btnDeleteOneMonster_26), value);
	}

	inline static int32_t get_offset_of__txtMonsterNum_27() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ____txtMonsterNum_27)); }
	inline Text_t1901882714 * get__txtMonsterNum_27() const { return ____txtMonsterNum_27; }
	inline Text_t1901882714 ** get_address_of__txtMonsterNum_27() { return &____txtMonsterNum_27; }
	inline void set__txtMonsterNum_27(Text_t1901882714 * value)
	{
		____txtMonsterNum_27 = value;
		Il2CppCodeGenWriteBarrier((&____txtMonsterNum_27), value);
	}

	inline static int32_t get_offset_of_m_fTimeOfOneGame_28() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fTimeOfOneGame_28)); }
	inline float get_m_fTimeOfOneGame_28() const { return ___m_fTimeOfOneGame_28; }
	inline float* get_address_of_m_fTimeOfOneGame_28() { return &___m_fTimeOfOneGame_28; }
	inline void set_m_fTimeOfOneGame_28(float value)
	{
		___m_fTimeOfOneGame_28 = value;
	}

	inline static int32_t get_offset_of_m_fClassCircleRadius_29() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fClassCircleRadius_29)); }
	inline float get_m_fClassCircleRadius_29() const { return ___m_fClassCircleRadius_29; }
	inline float* get_address_of_m_fClassCircleRadius_29() { return &___m_fClassCircleRadius_29; }
	inline void set_m_fClassCircleRadius_29(float value)
	{
		___m_fClassCircleRadius_29 = value;
	}

	inline static int32_t get_offset_of_m_fShengFuPanDingBallSize_30() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fShengFuPanDingBallSize_30)); }
	inline float get_m_fShengFuPanDingBallSize_30() const { return ___m_fShengFuPanDingBallSize_30; }
	inline float* get_address_of_m_fShengFuPanDingBallSize_30() { return &___m_fShengFuPanDingBallSize_30; }
	inline void set_m_fShengFuPanDingBallSize_30(float value)
	{
		___m_fShengFuPanDingBallSize_30 = value;
	}

	inline static int32_t get_offset_of_m_fLowThreshold_31() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fLowThreshold_31)); }
	inline float get_m_fLowThreshold_31() const { return ___m_fLowThreshold_31; }
	inline float* get_address_of_m_fLowThreshold_31() { return &___m_fLowThreshold_31; }
	inline void set_m_fLowThreshold_31(float value)
	{
		___m_fLowThreshold_31 = value;
	}

	inline static int32_t get_offset_of_m_fMiddleThreshold_32() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fMiddleThreshold_32)); }
	inline float get_m_fMiddleThreshold_32() const { return ___m_fMiddleThreshold_32; }
	inline float* get_address_of_m_fMiddleThreshold_32() { return &___m_fMiddleThreshold_32; }
	inline void set_m_fMiddleThreshold_32(float value)
	{
		___m_fMiddleThreshold_32 = value;
	}

	inline static int32_t get_offset_of_m_fHighThreshold_33() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fHighThreshold_33)); }
	inline float get_m_fHighThreshold_33() const { return ___m_fHighThreshold_33; }
	inline float* get_address_of_m_fHighThreshold_33() { return &___m_fHighThreshold_33; }
	inline void set_m_fHighThreshold_33(float value)
	{
		___m_fHighThreshold_33 = value;
	}

	inline static int32_t get_offset_of_m_fWarFogRadius_34() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fWarFogRadius_34)); }
	inline float get_m_fWarFogRadius_34() const { return ___m_fWarFogRadius_34; }
	inline float* get_address_of_m_fWarFogRadius_34() { return &___m_fWarFogRadius_34; }
	inline void set_m_fWarFogRadius_34(float value)
	{
		___m_fWarFogRadius_34 = value;
	}

	inline static int32_t get_offset_of_m_fAttenuateRate_35() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fAttenuateRate_35)); }
	inline float get_m_fAttenuateRate_35() const { return ___m_fAttenuateRate_35; }
	inline float* get_address_of_m_fAttenuateRate_35() { return &___m_fAttenuateRate_35; }
	inline void set_m_fAttenuateRate_35(float value)
	{
		___m_fAttenuateRate_35 = value;
	}

	inline static int32_t get_offset_of_m_dicClassConfig_36() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicClassConfig_36)); }
	inline Dictionary_2_t3948399185 * get_m_dicClassConfig_36() const { return ___m_dicClassConfig_36; }
	inline Dictionary_2_t3948399185 ** get_address_of_m_dicClassConfig_36() { return &___m_dicClassConfig_36; }
	inline void set_m_dicClassConfig_36(Dictionary_2_t3948399185 * value)
	{
		___m_dicClassConfig_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicClassConfig_36), value);
	}

	inline static int32_t get_offset_of_m_CurConfig_37() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_CurConfig_37)); }
	inline sClassConfig_t764718558  get_m_CurConfig_37() const { return ___m_CurConfig_37; }
	inline sClassConfig_t764718558 * get_address_of_m_CurConfig_37() { return &___m_CurConfig_37; }
	inline void set_m_CurConfig_37(sClassConfig_t764718558  value)
	{
		___m_CurConfig_37 = value;
	}

	inline static int32_t get_offset_of_m_CurThornConfig_38() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_CurThornConfig_38)); }
	inline sThornOfThisClassConfig_t350464872  get_m_CurThornConfig_38() const { return ___m_CurThornConfig_38; }
	inline sThornOfThisClassConfig_t350464872 * get_address_of_m_CurThornConfig_38() { return &___m_CurThornConfig_38; }
	inline void set_m_CurThornConfig_38(sThornOfThisClassConfig_t350464872  value)
	{
		___m_CurThornConfig_38 = value;
	}

	inline static int32_t get_offset_of_m_CurMannualMonsterConfig_39() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_CurMannualMonsterConfig_39)); }
	inline sMannualMonsterConfig_t1844217021  get_m_CurMannualMonsterConfig_39() const { return ___m_CurMannualMonsterConfig_39; }
	inline sMannualMonsterConfig_t1844217021 * get_address_of_m_CurMannualMonsterConfig_39() { return &___m_CurMannualMonsterConfig_39; }
	inline void set_m_CurMannualMonsterConfig_39(sMannualMonsterConfig_t1844217021  value)
	{
		___m_CurMannualMonsterConfig_39 = value;
	}

	inline static int32_t get_offset_of_m_dicMannualMonsterConfig_40() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicMannualMonsterConfig_40)); }
	inline Dictionary_2_t1629473320 * get_m_dicMannualMonsterConfig_40() const { return ___m_dicMannualMonsterConfig_40; }
	inline Dictionary_2_t1629473320 ** get_address_of_m_dicMannualMonsterConfig_40() { return &___m_dicMannualMonsterConfig_40; }
	inline void set_m_dicMannualMonsterConfig_40(Dictionary_2_t1629473320 * value)
	{
		___m_dicMannualMonsterConfig_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicMannualMonsterConfig_40), value);
	}

	inline static int32_t get_offset_of_tempMannualMonsterConfig_41() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___tempMannualMonsterConfig_41)); }
	inline sMannualMonsterConfig_t1844217021  get_tempMannualMonsterConfig_41() const { return ___tempMannualMonsterConfig_41; }
	inline sMannualMonsterConfig_t1844217021 * get_address_of_tempMannualMonsterConfig_41() { return &___tempMannualMonsterConfig_41; }
	inline void set_tempMannualMonsterConfig_41(sMannualMonsterConfig_t1844217021  value)
	{
		___tempMannualMonsterConfig_41 = value;
	}

	inline static int32_t get_offset_of_m_CurMovingMonster_43() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_CurMovingMonster_43)); }
	inline CMonster_t1941470938 * get_m_CurMovingMonster_43() const { return ___m_CurMovingMonster_43; }
	inline CMonster_t1941470938 ** get_address_of_m_CurMovingMonster_43() { return &___m_CurMovingMonster_43; }
	inline void set_m_CurMovingMonster_43(CMonster_t1941470938 * value)
	{
		___m_CurMovingMonster_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurMovingMonster_43), value);
	}

	inline static int32_t get_offset_of_m_CurSelectedMonster_44() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_CurSelectedMonster_44)); }
	inline CMonster_t1941470938 * get_m_CurSelectedMonster_44() const { return ___m_CurSelectedMonster_44; }
	inline CMonster_t1941470938 ** get_address_of_m_CurSelectedMonster_44() { return &___m_CurSelectedMonster_44; }
	inline void set_m_CurSelectedMonster_44(CMonster_t1941470938 * value)
	{
		___m_CurSelectedMonster_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedMonster_44), value);
	}

	inline static int32_t get_offset_of_m_bMouseDown_45() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_bMouseDown_45)); }
	inline bool get_m_bMouseDown_45() const { return ___m_bMouseDown_45; }
	inline bool* get_address_of_m_bMouseDown_45() { return &___m_bMouseDown_45; }
	inline void set_m_bMouseDown_45(bool value)
	{
		___m_bMouseDown_45 = value;
	}

	inline static int32_t get_offset_of_m_vecLastMouseWorldPos_46() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_vecLastMouseWorldPos_46)); }
	inline Vector3_t3722313464  get_m_vecLastMouseWorldPos_46() const { return ___m_vecLastMouseWorldPos_46; }
	inline Vector3_t3722313464 * get_address_of_m_vecLastMouseWorldPos_46() { return &___m_vecLastMouseWorldPos_46; }
	inline void set_m_vecLastMouseWorldPos_46(Vector3_t3722313464  value)
	{
		___m_vecLastMouseWorldPos_46 = value;
	}

	inline static int32_t get_offset_of_tempThornConfig_47() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___tempThornConfig_47)); }
	inline sThornOfThisClassConfig_t350464872  get_tempThornConfig_47() const { return ___tempThornConfig_47; }
	inline sThornOfThisClassConfig_t350464872 * get_address_of_tempThornConfig_47() { return &___tempThornConfig_47; }
	inline void set_tempThornConfig_47(sThornOfThisClassConfig_t350464872  value)
	{
		___tempThornConfig_47 = value;
	}

	inline static int32_t get_offset_of_m_dicAllSpores_49() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicAllSpores_49)); }
	inline Dictionary_2_t1726727237 * get_m_dicAllSpores_49() const { return ___m_dicAllSpores_49; }
	inline Dictionary_2_t1726727237 ** get_address_of_m_dicAllSpores_49() { return &___m_dicAllSpores_49; }
	inline void set_m_dicAllSpores_49(Dictionary_2_t1726727237 * value)
	{
		___m_dicAllSpores_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAllSpores_49), value);
	}

	inline static int32_t get_offset_of_m_dicAllThorns_50() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicAllThorns_50)); }
	inline Dictionary_2_t3004110722 * get_m_dicAllThorns_50() const { return ___m_dicAllThorns_50; }
	inline Dictionary_2_t3004110722 ** get_address_of_m_dicAllThorns_50() { return &___m_dicAllThorns_50; }
	inline void set_m_dicAllThorns_50(Dictionary_2_t3004110722 * value)
	{
		___m_dicAllThorns_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAllThorns_50), value);
	}

	inline static int32_t get_offset_of_m_lstMonstersX_51() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lstMonstersX_51)); }
	inline List_1_t3413545680 * get_m_lstMonstersX_51() const { return ___m_lstMonstersX_51; }
	inline List_1_t3413545680 ** get_address_of_m_lstMonstersX_51() { return &___m_lstMonstersX_51; }
	inline void set_m_lstMonstersX_51(List_1_t3413545680 * value)
	{
		___m_lstMonstersX_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMonstersX_51), value);
	}

	inline static int32_t get_offset_of_m_lstMonstersY_52() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lstMonstersY_52)); }
	inline List_1_t3413545680 * get_m_lstMonstersY_52() const { return ___m_lstMonstersY_52; }
	inline List_1_t3413545680 ** get_address_of_m_lstMonstersY_52() { return &___m_lstMonstersY_52; }
	inline void set_m_lstMonstersY_52(List_1_t3413545680 * value)
	{
		___m_lstMonstersY_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMonstersY_52), value);
	}

	inline static int32_t get_offset_of_m_lGuidCount_53() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lGuidCount_53)); }
	inline int64_t get_m_lGuidCount_53() const { return ___m_lGuidCount_53; }
	inline int64_t* get_address_of_m_lGuidCount_53() { return &___m_lGuidCount_53; }
	inline void set_m_lGuidCount_53(int64_t value)
	{
		___m_lGuidCount_53 = value;
	}

	inline static int32_t get_offset_of_m_fCheckInCamViewTimeCount_55() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fCheckInCamViewTimeCount_55)); }
	inline float get_m_fCheckInCamViewTimeCount_55() const { return ___m_fCheckInCamViewTimeCount_55; }
	inline float* get_address_of_m_fCheckInCamViewTimeCount_55() { return &___m_fCheckInCamViewTimeCount_55; }
	inline void set_m_fCheckInCamViewTimeCount_55(float value)
	{
		___m_fCheckInCamViewTimeCount_55 = value;
	}

	inline static int32_t get_offset_of_dicTemp_56() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___dicTemp_56)); }
	inline Dictionary_2_t1839659084 * get_dicTemp_56() const { return ___dicTemp_56; }
	inline Dictionary_2_t1839659084 ** get_address_of_dicTemp_56() { return &___dicTemp_56; }
	inline void set_dicTemp_56(Dictionary_2_t1839659084 * value)
	{
		___dicTemp_56 = value;
		Il2CppCodeGenWriteBarrier((&___dicTemp_56), value);
	}

	inline static int32_t get_offset_of_m_lstActiveMonster_57() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lstActiveMonster_57)); }
	inline List_1_t3413545680 * get_m_lstActiveMonster_57() const { return ___m_lstActiveMonster_57; }
	inline List_1_t3413545680 ** get_address_of_m_lstActiveMonster_57() { return &___m_lstActiveMonster_57; }
	inline void set_m_lstActiveMonster_57(List_1_t3413545680 * value)
	{
		___m_lstActiveMonster_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstActiveMonster_57), value);
	}

	inline static int32_t get_offset_of_dicTempActiveMonsterOfThisRound_58() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___dicTempActiveMonsterOfThisRound_58)); }
	inline Dictionary_2_t830184269 * get_dicTempActiveMonsterOfThisRound_58() const { return ___dicTempActiveMonsterOfThisRound_58; }
	inline Dictionary_2_t830184269 ** get_address_of_dicTempActiveMonsterOfThisRound_58() { return &___dicTempActiveMonsterOfThisRound_58; }
	inline void set_dicTempActiveMonsterOfThisRound_58(Dictionary_2_t830184269 * value)
	{
		___dicTempActiveMonsterOfThisRound_58 = value;
		Il2CppCodeGenWriteBarrier((&___dicTempActiveMonsterOfThisRound_58), value);
	}

	inline static int32_t get_offset_of_m_lstSyncPublicList_59() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lstSyncPublicList_59)); }
	inline List_1_t3413545680 * get_m_lstSyncPublicList_59() const { return ___m_lstSyncPublicList_59; }
	inline List_1_t3413545680 ** get_address_of_m_lstSyncPublicList_59() { return &___m_lstSyncPublicList_59; }
	inline void set_m_lstSyncPublicList_59(List_1_t3413545680 * value)
	{
		___m_lstSyncPublicList_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSyncPublicList_59), value);
	}

	inline static int32_t get_offset_of_m_lstToBeReborn_60() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lstToBeReborn_60)); }
	inline List_1_t3413545680 * get_m_lstToBeReborn_60() const { return ___m_lstToBeReborn_60; }
	inline List_1_t3413545680 ** get_address_of_m_lstToBeReborn_60() { return &___m_lstToBeReborn_60; }
	inline void set_m_lstToBeReborn_60(List_1_t3413545680 * value)
	{
		___m_lstToBeReborn_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstToBeReborn_60), value);
	}

	inline static int32_t get_offset_of_m_lstCiChuXianThorn_61() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lstCiChuXianThorn_61)); }
	inline List_1_t3413545680 * get_m_lstCiChuXianThorn_61() const { return ___m_lstCiChuXianThorn_61; }
	inline List_1_t3413545680 ** get_address_of_m_lstCiChuXianThorn_61() { return &___m_lstCiChuXianThorn_61; }
	inline void set_m_lstCiChuXianThorn_61(List_1_t3413545680 * value)
	{
		___m_lstCiChuXianThorn_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstCiChuXianThorn_61), value);
	}

	inline static int32_t get_offset_of_m_dicThornColor_62() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicThornColor_62)); }
	inline Dictionary_2_t2658501330 * get_m_dicThornColor_62() const { return ___m_dicThornColor_62; }
	inline Dictionary_2_t2658501330 ** get_address_of_m_dicThornColor_62() { return &___m_dicThornColor_62; }
	inline void set_m_dicThornColor_62(Dictionary_2_t2658501330 * value)
	{
		___m_dicThornColor_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicThornColor_62), value);
	}

	inline static int32_t get_offset_of_c_RebornInterval_63() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___c_RebornInterval_63)); }
	inline float get_c_RebornInterval_63() const { return ___c_RebornInterval_63; }
	inline float* get_address_of_c_RebornInterval_63() { return &___c_RebornInterval_63; }
	inline void set_c_RebornInterval_63(float value)
	{
		___c_RebornInterval_63 = value;
	}

	inline static int32_t get_offset_of_m_fRebornCount_64() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_fRebornCount_64)); }
	inline float get_m_fRebornCount_64() const { return ___m_fRebornCount_64; }
	inline float* get_address_of_m_fRebornCount_64() { return &___m_fRebornCount_64; }
	inline void set_m_fRebornCount_64(float value)
	{
		___m_fRebornCount_64 = value;
	}

	inline static int32_t get_offset_of_m_dicPushedThornList_65() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicPushedThornList_65)); }
	inline Dictionary_2_t830184269 * get_m_dicPushedThornList_65() const { return ___m_dicPushedThornList_65; }
	inline Dictionary_2_t830184269 ** get_address_of_m_dicPushedThornList_65() { return &___m_dicPushedThornList_65; }
	inline void set_m_dicPushedThornList_65(Dictionary_2_t830184269 * value)
	{
		___m_dicPushedThornList_65 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPushedThornList_65), value);
	}

	inline static int32_t get_offset_of_m_lSceneBallGuid_68() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_lSceneBallGuid_68)); }
	inline int64_t get_m_lSceneBallGuid_68() const { return ___m_lSceneBallGuid_68; }
	inline int64_t* get_address_of_m_lSceneBallGuid_68() { return &___m_lSceneBallGuid_68; }
	inline void set_m_lSceneBallGuid_68(int64_t value)
	{
		___m_lSceneBallGuid_68 = value;
	}

	inline static int32_t get_offset_of_m_dicSceneBalls_69() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478, ___m_dicSceneBalls_69)); }
	inline Dictionary_2_t3004110722 * get_m_dicSceneBalls_69() const { return ___m_dicSceneBalls_69; }
	inline Dictionary_2_t3004110722 ** get_address_of_m_dicSceneBalls_69() { return &___m_dicSceneBalls_69; }
	inline void set_m_dicSceneBalls_69(Dictionary_2_t3004110722 * value)
	{
		___m_dicSceneBalls_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSceneBalls_69), value);
	}
};

struct CClassEditor_t335744478_StaticFields
{
public:
	// UnityEngine.Vector3 CClassEditor::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// UnityEngine.Vector3 CClassEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// UnityEngine.Vector3 CClassEditor::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_5;
	// UnityEngine.Vector2 CClassEditor::vec2Temp
	Vector2_t2156229523  ___vec2Temp_6;
	// CClassEditor CClassEditor::s_Instance
	CClassEditor_t335744478 * ___s_Instance_42;
	// System.Int32 CClassEditor::s_nTotalBeanNum
	int32_t ___s_nTotalBeanNum_48;
	// System.Int32 CClassEditor::s_LowCount
	int32_t ___s_LowCount_66;

public:
	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_5() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___vecTempPos1_5)); }
	inline Vector3_t3722313464  get_vecTempPos1_5() const { return ___vecTempPos1_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_5() { return &___vecTempPos1_5; }
	inline void set_vecTempPos1_5(Vector3_t3722313464  value)
	{
		___vecTempPos1_5 = value;
	}

	inline static int32_t get_offset_of_vec2Temp_6() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___vec2Temp_6)); }
	inline Vector2_t2156229523  get_vec2Temp_6() const { return ___vec2Temp_6; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_6() { return &___vec2Temp_6; }
	inline void set_vec2Temp_6(Vector2_t2156229523  value)
	{
		___vec2Temp_6 = value;
	}

	inline static int32_t get_offset_of_s_Instance_42() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___s_Instance_42)); }
	inline CClassEditor_t335744478 * get_s_Instance_42() const { return ___s_Instance_42; }
	inline CClassEditor_t335744478 ** get_address_of_s_Instance_42() { return &___s_Instance_42; }
	inline void set_s_Instance_42(CClassEditor_t335744478 * value)
	{
		___s_Instance_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_42), value);
	}

	inline static int32_t get_offset_of_s_nTotalBeanNum_48() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___s_nTotalBeanNum_48)); }
	inline int32_t get_s_nTotalBeanNum_48() const { return ___s_nTotalBeanNum_48; }
	inline int32_t* get_address_of_s_nTotalBeanNum_48() { return &___s_nTotalBeanNum_48; }
	inline void set_s_nTotalBeanNum_48(int32_t value)
	{
		___s_nTotalBeanNum_48 = value;
	}

	inline static int32_t get_offset_of_s_LowCount_66() { return static_cast<int32_t>(offsetof(CClassEditor_t335744478_StaticFields, ___s_LowCount_66)); }
	inline int32_t get_s_LowCount_66() const { return ___s_LowCount_66; }
	inline int32_t* get_address_of_s_LowCount_66() { return &___s_LowCount_66; }
	inline void set_s_LowCount_66(int32_t value)
	{
		___s_LowCount_66 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCLASSEDITOR_T335744478_H
#ifndef CCYBERTREEPOPO_T261854377_H
#define CCYBERTREEPOPO_T261854377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCyberTreePoPo
struct  CCyberTreePoPo_t261854377  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CCyberTreePoPo::_imgLeft
	Image_t2670269651 * ____imgLeft_4;
	// UnityEngine.UI.Image CCyberTreePoPo::_imgCenter
	Image_t2670269651 * ____imgCenter_5;
	// UnityEngine.UI.Image CCyberTreePoPo::_imgRight
	Image_t2670269651 * ____imgRight_6;
	// UnityEngine.UI.Text CCyberTreePoPo::_txtContent
	Text_t1901882714 * ____txtContent_7;
	// System.Single CCyberTreePoPo::m_fUnitWidth
	float ___m_fUnitWidth_8;
	// System.Single CCyberTreePoPo::m_fFontSize
	float ___m_fFontSize_9;
	// System.Boolean CCyberTreePoPo::m_bActive
	bool ___m_bActive_10;

public:
	inline static int32_t get_offset_of__imgLeft_4() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ____imgLeft_4)); }
	inline Image_t2670269651 * get__imgLeft_4() const { return ____imgLeft_4; }
	inline Image_t2670269651 ** get_address_of__imgLeft_4() { return &____imgLeft_4; }
	inline void set__imgLeft_4(Image_t2670269651 * value)
	{
		____imgLeft_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgLeft_4), value);
	}

	inline static int32_t get_offset_of__imgCenter_5() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ____imgCenter_5)); }
	inline Image_t2670269651 * get__imgCenter_5() const { return ____imgCenter_5; }
	inline Image_t2670269651 ** get_address_of__imgCenter_5() { return &____imgCenter_5; }
	inline void set__imgCenter_5(Image_t2670269651 * value)
	{
		____imgCenter_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgCenter_5), value);
	}

	inline static int32_t get_offset_of__imgRight_6() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ____imgRight_6)); }
	inline Image_t2670269651 * get__imgRight_6() const { return ____imgRight_6; }
	inline Image_t2670269651 ** get_address_of__imgRight_6() { return &____imgRight_6; }
	inline void set__imgRight_6(Image_t2670269651 * value)
	{
		____imgRight_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgRight_6), value);
	}

	inline static int32_t get_offset_of__txtContent_7() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ____txtContent_7)); }
	inline Text_t1901882714 * get__txtContent_7() const { return ____txtContent_7; }
	inline Text_t1901882714 ** get_address_of__txtContent_7() { return &____txtContent_7; }
	inline void set__txtContent_7(Text_t1901882714 * value)
	{
		____txtContent_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_7), value);
	}

	inline static int32_t get_offset_of_m_fUnitWidth_8() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ___m_fUnitWidth_8)); }
	inline float get_m_fUnitWidth_8() const { return ___m_fUnitWidth_8; }
	inline float* get_address_of_m_fUnitWidth_8() { return &___m_fUnitWidth_8; }
	inline void set_m_fUnitWidth_8(float value)
	{
		___m_fUnitWidth_8 = value;
	}

	inline static int32_t get_offset_of_m_fFontSize_9() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ___m_fFontSize_9)); }
	inline float get_m_fFontSize_9() const { return ___m_fFontSize_9; }
	inline float* get_address_of_m_fFontSize_9() { return &___m_fFontSize_9; }
	inline void set_m_fFontSize_9(float value)
	{
		___m_fFontSize_9 = value;
	}

	inline static int32_t get_offset_of_m_bActive_10() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377, ___m_bActive_10)); }
	inline bool get_m_bActive_10() const { return ___m_bActive_10; }
	inline bool* get_address_of_m_bActive_10() { return &___m_bActive_10; }
	inline void set_m_bActive_10(bool value)
	{
		___m_bActive_10 = value;
	}
};

struct CCyberTreePoPo_t261854377_StaticFields
{
public:
	// UnityEngine.Vector3 CCyberTreePoPo::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCyberTreePoPo::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCyberTreePoPo_t261854377_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCYBERTREEPOPO_T261854377_H
#ifndef CCYBERTREELISTITEM_T2642449450_H
#define CCYBERTREELISTITEM_T2642449450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCyberTreeListItem
struct  CCyberTreeListItem_t2642449450  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CCyberTreeListItem::m_nIndex
	int32_t ___m_nIndex_2;

public:
	inline static int32_t get_offset_of_m_nIndex_2() { return static_cast<int32_t>(offsetof(CCyberTreeListItem_t2642449450, ___m_nIndex_2)); }
	inline int32_t get_m_nIndex_2() const { return ___m_nIndex_2; }
	inline int32_t* get_address_of_m_nIndex_2() { return &___m_nIndex_2; }
	inline void set_m_nIndex_2(int32_t value)
	{
		___m_nIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCYBERTREELISTITEM_T2642449450_H
#ifndef CCYBERTREELIST_T4040119676_H
#define CCYBERTREELIST_T4040119676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCyberTreeList
struct  CCyberTreeList_t4040119676  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CCyberTreeList::m_bCurItemNum
	int32_t ___m_bCurItemNum_2;
	// System.Single CCyberTreeList::m_fItemSize
	float ___m_fItemSize_3;
	// UnityEngine.GameObject CCyberTreeList::m_goContainer
	GameObject_t1113636619 * ___m_goContainer_4;
	// System.Collections.Generic.List`1<CCyberTreeListItem> CCyberTreeList::m_lstItems
	List_1_t4114524192 * ___m_lstItems_5;
	// System.Single CCyberTreeList::m_fMoveSpeed
	float ___m_fMoveSpeed_9;
	// System.Boolean CCyberTreeList::m_bVert
	bool ___m_bVert_10;
	// System.Single CCyberTreeList::m_fStartTop
	float ___m_fStartTop_11;
	// System.Single CCyberTreeList::m_fMinTop
	float ___m_fMinTop_12;
	// System.Single CCyberTreeList::m_fStartBottom
	float ___m_fStartBottom_13;
	// System.Single CCyberTreeList::m_fMaxBottom
	float ___m_fMaxBottom_14;
	// System.Int32 CCyberTreeList::m_nDontAutoBackIfItemsNumLessThan
	int32_t ___m_nDontAutoBackIfItemsNumLessThan_15;
	// System.Single CCyberTreeList::m_fAutoBackSpeed
	float ___m_fAutoBackSpeed_16;
	// UnityEngine.UI.GraphicRaycaster CCyberTreeList::_graphicRaycaster
	GraphicRaycaster_t2999697109 * ____graphicRaycaster_17;
	// UnityEngine.EventSystems.EventSystem CCyberTreeList::eventSystem
	EventSystem_t1003666588 * ___eventSystem_18;
	// System.Boolean CCyberTreeList::m_bDraging
	bool ___m_bDraging_19;
	// UnityEngine.Vector3 CCyberTreeList::m_vecLastFrameMousePos
	Vector3_t3722313464  ___m_vecLastFrameMousePos_20;
	// UnityEngine.Vector3 CCyberTreeList::m_vecLastMovement
	Vector3_t3722313464  ___m_vecLastMovement_21;
	// System.Boolean CCyberTreeList::m_bSliding
	bool ___m_bSliding_22;
	// System.Single CCyberTreeList::m_fV0
	float ___m_fV0_23;
	// System.Single CCyberTreeList::m_fA
	float ___m_fA_24;
	// System.Int32 CCyberTreeList::m_nAutoBackDir
	int32_t ___m_nAutoBackDir_25;
	// System.Single CCyberTreeList::m_fMaxExceedAmount
	float ___m_fMaxExceedAmount_26;
	// System.Boolean CCyberTreeList::m_bExceedLeftThreshold
	bool ___m_bExceedLeftThreshold_27;
	// System.Single CCyberTreeList::m_fExceedLeftAmount
	float ___m_fExceedLeftAmount_28;
	// System.Boolean CCyberTreeList::m_bExceedRightThreshold
	bool ___m_bExceedRightThreshold_29;
	// System.Single CCyberTreeList::m_fExceedRightAmount
	float ___m_fExceedRightAmount_30;
	// System.Boolean CCyberTreeList::m_bExceedTopThreshold
	bool ___m_bExceedTopThreshold_31;
	// System.Boolean CCyberTreeList::m_bExceedBottomThreshold
	bool ___m_bExceedBottomThreshold_32;

public:
	inline static int32_t get_offset_of_m_bCurItemNum_2() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bCurItemNum_2)); }
	inline int32_t get_m_bCurItemNum_2() const { return ___m_bCurItemNum_2; }
	inline int32_t* get_address_of_m_bCurItemNum_2() { return &___m_bCurItemNum_2; }
	inline void set_m_bCurItemNum_2(int32_t value)
	{
		___m_bCurItemNum_2 = value;
	}

	inline static int32_t get_offset_of_m_fItemSize_3() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fItemSize_3)); }
	inline float get_m_fItemSize_3() const { return ___m_fItemSize_3; }
	inline float* get_address_of_m_fItemSize_3() { return &___m_fItemSize_3; }
	inline void set_m_fItemSize_3(float value)
	{
		___m_fItemSize_3 = value;
	}

	inline static int32_t get_offset_of_m_goContainer_4() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_goContainer_4)); }
	inline GameObject_t1113636619 * get_m_goContainer_4() const { return ___m_goContainer_4; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainer_4() { return &___m_goContainer_4; }
	inline void set_m_goContainer_4(GameObject_t1113636619 * value)
	{
		___m_goContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainer_4), value);
	}

	inline static int32_t get_offset_of_m_lstItems_5() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_lstItems_5)); }
	inline List_1_t4114524192 * get_m_lstItems_5() const { return ___m_lstItems_5; }
	inline List_1_t4114524192 ** get_address_of_m_lstItems_5() { return &___m_lstItems_5; }
	inline void set_m_lstItems_5(List_1_t4114524192 * value)
	{
		___m_lstItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_5), value);
	}

	inline static int32_t get_offset_of_m_fMoveSpeed_9() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fMoveSpeed_9)); }
	inline float get_m_fMoveSpeed_9() const { return ___m_fMoveSpeed_9; }
	inline float* get_address_of_m_fMoveSpeed_9() { return &___m_fMoveSpeed_9; }
	inline void set_m_fMoveSpeed_9(float value)
	{
		___m_fMoveSpeed_9 = value;
	}

	inline static int32_t get_offset_of_m_bVert_10() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bVert_10)); }
	inline bool get_m_bVert_10() const { return ___m_bVert_10; }
	inline bool* get_address_of_m_bVert_10() { return &___m_bVert_10; }
	inline void set_m_bVert_10(bool value)
	{
		___m_bVert_10 = value;
	}

	inline static int32_t get_offset_of_m_fStartTop_11() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fStartTop_11)); }
	inline float get_m_fStartTop_11() const { return ___m_fStartTop_11; }
	inline float* get_address_of_m_fStartTop_11() { return &___m_fStartTop_11; }
	inline void set_m_fStartTop_11(float value)
	{
		___m_fStartTop_11 = value;
	}

	inline static int32_t get_offset_of_m_fMinTop_12() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fMinTop_12)); }
	inline float get_m_fMinTop_12() const { return ___m_fMinTop_12; }
	inline float* get_address_of_m_fMinTop_12() { return &___m_fMinTop_12; }
	inline void set_m_fMinTop_12(float value)
	{
		___m_fMinTop_12 = value;
	}

	inline static int32_t get_offset_of_m_fStartBottom_13() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fStartBottom_13)); }
	inline float get_m_fStartBottom_13() const { return ___m_fStartBottom_13; }
	inline float* get_address_of_m_fStartBottom_13() { return &___m_fStartBottom_13; }
	inline void set_m_fStartBottom_13(float value)
	{
		___m_fStartBottom_13 = value;
	}

	inline static int32_t get_offset_of_m_fMaxBottom_14() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fMaxBottom_14)); }
	inline float get_m_fMaxBottom_14() const { return ___m_fMaxBottom_14; }
	inline float* get_address_of_m_fMaxBottom_14() { return &___m_fMaxBottom_14; }
	inline void set_m_fMaxBottom_14(float value)
	{
		___m_fMaxBottom_14 = value;
	}

	inline static int32_t get_offset_of_m_nDontAutoBackIfItemsNumLessThan_15() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_nDontAutoBackIfItemsNumLessThan_15)); }
	inline int32_t get_m_nDontAutoBackIfItemsNumLessThan_15() const { return ___m_nDontAutoBackIfItemsNumLessThan_15; }
	inline int32_t* get_address_of_m_nDontAutoBackIfItemsNumLessThan_15() { return &___m_nDontAutoBackIfItemsNumLessThan_15; }
	inline void set_m_nDontAutoBackIfItemsNumLessThan_15(int32_t value)
	{
		___m_nDontAutoBackIfItemsNumLessThan_15 = value;
	}

	inline static int32_t get_offset_of_m_fAutoBackSpeed_16() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fAutoBackSpeed_16)); }
	inline float get_m_fAutoBackSpeed_16() const { return ___m_fAutoBackSpeed_16; }
	inline float* get_address_of_m_fAutoBackSpeed_16() { return &___m_fAutoBackSpeed_16; }
	inline void set_m_fAutoBackSpeed_16(float value)
	{
		___m_fAutoBackSpeed_16 = value;
	}

	inline static int32_t get_offset_of__graphicRaycaster_17() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ____graphicRaycaster_17)); }
	inline GraphicRaycaster_t2999697109 * get__graphicRaycaster_17() const { return ____graphicRaycaster_17; }
	inline GraphicRaycaster_t2999697109 ** get_address_of__graphicRaycaster_17() { return &____graphicRaycaster_17; }
	inline void set__graphicRaycaster_17(GraphicRaycaster_t2999697109 * value)
	{
		____graphicRaycaster_17 = value;
		Il2CppCodeGenWriteBarrier((&____graphicRaycaster_17), value);
	}

	inline static int32_t get_offset_of_eventSystem_18() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___eventSystem_18)); }
	inline EventSystem_t1003666588 * get_eventSystem_18() const { return ___eventSystem_18; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_18() { return &___eventSystem_18; }
	inline void set_eventSystem_18(EventSystem_t1003666588 * value)
	{
		___eventSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_18), value);
	}

	inline static int32_t get_offset_of_m_bDraging_19() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bDraging_19)); }
	inline bool get_m_bDraging_19() const { return ___m_bDraging_19; }
	inline bool* get_address_of_m_bDraging_19() { return &___m_bDraging_19; }
	inline void set_m_bDraging_19(bool value)
	{
		___m_bDraging_19 = value;
	}

	inline static int32_t get_offset_of_m_vecLastFrameMousePos_20() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_vecLastFrameMousePos_20)); }
	inline Vector3_t3722313464  get_m_vecLastFrameMousePos_20() const { return ___m_vecLastFrameMousePos_20; }
	inline Vector3_t3722313464 * get_address_of_m_vecLastFrameMousePos_20() { return &___m_vecLastFrameMousePos_20; }
	inline void set_m_vecLastFrameMousePos_20(Vector3_t3722313464  value)
	{
		___m_vecLastFrameMousePos_20 = value;
	}

	inline static int32_t get_offset_of_m_vecLastMovement_21() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_vecLastMovement_21)); }
	inline Vector3_t3722313464  get_m_vecLastMovement_21() const { return ___m_vecLastMovement_21; }
	inline Vector3_t3722313464 * get_address_of_m_vecLastMovement_21() { return &___m_vecLastMovement_21; }
	inline void set_m_vecLastMovement_21(Vector3_t3722313464  value)
	{
		___m_vecLastMovement_21 = value;
	}

	inline static int32_t get_offset_of_m_bSliding_22() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bSliding_22)); }
	inline bool get_m_bSliding_22() const { return ___m_bSliding_22; }
	inline bool* get_address_of_m_bSliding_22() { return &___m_bSliding_22; }
	inline void set_m_bSliding_22(bool value)
	{
		___m_bSliding_22 = value;
	}

	inline static int32_t get_offset_of_m_fV0_23() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fV0_23)); }
	inline float get_m_fV0_23() const { return ___m_fV0_23; }
	inline float* get_address_of_m_fV0_23() { return &___m_fV0_23; }
	inline void set_m_fV0_23(float value)
	{
		___m_fV0_23 = value;
	}

	inline static int32_t get_offset_of_m_fA_24() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fA_24)); }
	inline float get_m_fA_24() const { return ___m_fA_24; }
	inline float* get_address_of_m_fA_24() { return &___m_fA_24; }
	inline void set_m_fA_24(float value)
	{
		___m_fA_24 = value;
	}

	inline static int32_t get_offset_of_m_nAutoBackDir_25() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_nAutoBackDir_25)); }
	inline int32_t get_m_nAutoBackDir_25() const { return ___m_nAutoBackDir_25; }
	inline int32_t* get_address_of_m_nAutoBackDir_25() { return &___m_nAutoBackDir_25; }
	inline void set_m_nAutoBackDir_25(int32_t value)
	{
		___m_nAutoBackDir_25 = value;
	}

	inline static int32_t get_offset_of_m_fMaxExceedAmount_26() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fMaxExceedAmount_26)); }
	inline float get_m_fMaxExceedAmount_26() const { return ___m_fMaxExceedAmount_26; }
	inline float* get_address_of_m_fMaxExceedAmount_26() { return &___m_fMaxExceedAmount_26; }
	inline void set_m_fMaxExceedAmount_26(float value)
	{
		___m_fMaxExceedAmount_26 = value;
	}

	inline static int32_t get_offset_of_m_bExceedLeftThreshold_27() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bExceedLeftThreshold_27)); }
	inline bool get_m_bExceedLeftThreshold_27() const { return ___m_bExceedLeftThreshold_27; }
	inline bool* get_address_of_m_bExceedLeftThreshold_27() { return &___m_bExceedLeftThreshold_27; }
	inline void set_m_bExceedLeftThreshold_27(bool value)
	{
		___m_bExceedLeftThreshold_27 = value;
	}

	inline static int32_t get_offset_of_m_fExceedLeftAmount_28() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fExceedLeftAmount_28)); }
	inline float get_m_fExceedLeftAmount_28() const { return ___m_fExceedLeftAmount_28; }
	inline float* get_address_of_m_fExceedLeftAmount_28() { return &___m_fExceedLeftAmount_28; }
	inline void set_m_fExceedLeftAmount_28(float value)
	{
		___m_fExceedLeftAmount_28 = value;
	}

	inline static int32_t get_offset_of_m_bExceedRightThreshold_29() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bExceedRightThreshold_29)); }
	inline bool get_m_bExceedRightThreshold_29() const { return ___m_bExceedRightThreshold_29; }
	inline bool* get_address_of_m_bExceedRightThreshold_29() { return &___m_bExceedRightThreshold_29; }
	inline void set_m_bExceedRightThreshold_29(bool value)
	{
		___m_bExceedRightThreshold_29 = value;
	}

	inline static int32_t get_offset_of_m_fExceedRightAmount_30() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_fExceedRightAmount_30)); }
	inline float get_m_fExceedRightAmount_30() const { return ___m_fExceedRightAmount_30; }
	inline float* get_address_of_m_fExceedRightAmount_30() { return &___m_fExceedRightAmount_30; }
	inline void set_m_fExceedRightAmount_30(float value)
	{
		___m_fExceedRightAmount_30 = value;
	}

	inline static int32_t get_offset_of_m_bExceedTopThreshold_31() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bExceedTopThreshold_31)); }
	inline bool get_m_bExceedTopThreshold_31() const { return ___m_bExceedTopThreshold_31; }
	inline bool* get_address_of_m_bExceedTopThreshold_31() { return &___m_bExceedTopThreshold_31; }
	inline void set_m_bExceedTopThreshold_31(bool value)
	{
		___m_bExceedTopThreshold_31 = value;
	}

	inline static int32_t get_offset_of_m_bExceedBottomThreshold_32() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676, ___m_bExceedBottomThreshold_32)); }
	inline bool get_m_bExceedBottomThreshold_32() const { return ___m_bExceedBottomThreshold_32; }
	inline bool* get_address_of_m_bExceedBottomThreshold_32() { return &___m_bExceedBottomThreshold_32; }
	inline void set_m_bExceedBottomThreshold_32(bool value)
	{
		___m_bExceedBottomThreshold_32 = value;
	}
};

struct CCyberTreeList_t4040119676_StaticFields
{
public:
	// UnityEngine.Vector3 CCyberTreeList::vecTempPos
	Vector3_t3722313464  ___vecTempPos_6;
	// UnityEngine.Vector3 CCyberTreeList::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_7;
	// UnityEngine.Vector3 CCyberTreeList::vecTempScale
	Vector3_t3722313464  ___vecTempScale_8;

public:
	inline static int32_t get_offset_of_vecTempPos_6() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676_StaticFields, ___vecTempPos_6)); }
	inline Vector3_t3722313464  get_vecTempPos_6() const { return ___vecTempPos_6; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_6() { return &___vecTempPos_6; }
	inline void set_vecTempPos_6(Vector3_t3722313464  value)
	{
		___vecTempPos_6 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_7() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676_StaticFields, ___vecTempPos2_7)); }
	inline Vector3_t3722313464  get_vecTempPos2_7() const { return ___vecTempPos2_7; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_7() { return &___vecTempPos2_7; }
	inline void set_vecTempPos2_7(Vector3_t3722313464  value)
	{
		___vecTempPos2_7 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_8() { return static_cast<int32_t>(offsetof(CCyberTreeList_t4040119676_StaticFields, ___vecTempScale_8)); }
	inline Vector3_t3722313464  get_vecTempScale_8() const { return ___vecTempScale_8; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_8() { return &___vecTempScale_8; }
	inline void set_vecTempScale_8(Vector3_t3722313464  value)
	{
		___vecTempScale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCYBERTREELIST_T4040119676_H
#ifndef CDEADMANAGER_T731087622_H
#define CDEADMANAGER_T731087622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CDeadManager
struct  CDeadManager_t731087622  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CDeadManager::_panleMain
	GameObject_t1113636619 * ____panleMain_3;
	// UnityEngine.UI.Text CDeadManager::_txtRebornWaitingTimeLeft
	Text_t1901882714 * ____txtRebornWaitingTimeLeft_4;
	// System.Single CDeadManager::m_fRebornWaitingTimeLeft
	float ___m_fRebornWaitingTimeLeft_5;

public:
	inline static int32_t get_offset_of__panleMain_3() { return static_cast<int32_t>(offsetof(CDeadManager_t731087622, ____panleMain_3)); }
	inline GameObject_t1113636619 * get__panleMain_3() const { return ____panleMain_3; }
	inline GameObject_t1113636619 ** get_address_of__panleMain_3() { return &____panleMain_3; }
	inline void set__panleMain_3(GameObject_t1113636619 * value)
	{
		____panleMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____panleMain_3), value);
	}

	inline static int32_t get_offset_of__txtRebornWaitingTimeLeft_4() { return static_cast<int32_t>(offsetof(CDeadManager_t731087622, ____txtRebornWaitingTimeLeft_4)); }
	inline Text_t1901882714 * get__txtRebornWaitingTimeLeft_4() const { return ____txtRebornWaitingTimeLeft_4; }
	inline Text_t1901882714 ** get_address_of__txtRebornWaitingTimeLeft_4() { return &____txtRebornWaitingTimeLeft_4; }
	inline void set__txtRebornWaitingTimeLeft_4(Text_t1901882714 * value)
	{
		____txtRebornWaitingTimeLeft_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtRebornWaitingTimeLeft_4), value);
	}

	inline static int32_t get_offset_of_m_fRebornWaitingTimeLeft_5() { return static_cast<int32_t>(offsetof(CDeadManager_t731087622, ___m_fRebornWaitingTimeLeft_5)); }
	inline float get_m_fRebornWaitingTimeLeft_5() const { return ___m_fRebornWaitingTimeLeft_5; }
	inline float* get_address_of_m_fRebornWaitingTimeLeft_5() { return &___m_fRebornWaitingTimeLeft_5; }
	inline void set_m_fRebornWaitingTimeLeft_5(float value)
	{
		___m_fRebornWaitingTimeLeft_5 = value;
	}
};

struct CDeadManager_t731087622_StaticFields
{
public:
	// CDeadManager CDeadManager::s_Instance
	CDeadManager_t731087622 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CDeadManager_t731087622_StaticFields, ___s_Instance_2)); }
	inline CDeadManager_t731087622 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CDeadManager_t731087622 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CDeadManager_t731087622 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CDEADMANAGER_T731087622_H
#ifndef CJIESUANSYSTEM_T715337267_H
#define CJIESUANSYSTEM_T715337267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJIeSuanSystem
struct  CJIeSuanSystem_t715337267  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CJIeSuanSystem::m_aryJieSuanPanels
	GameObjectU5BU5D_t3328599146* ___m_aryJieSuanPanels_3;

public:
	inline static int32_t get_offset_of_m_aryJieSuanPanels_3() { return static_cast<int32_t>(offsetof(CJIeSuanSystem_t715337267, ___m_aryJieSuanPanels_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryJieSuanPanels_3() const { return ___m_aryJieSuanPanels_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryJieSuanPanels_3() { return &___m_aryJieSuanPanels_3; }
	inline void set_m_aryJieSuanPanels_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryJieSuanPanels_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryJieSuanPanels_3), value);
	}
};

struct CJIeSuanSystem_t715337267_StaticFields
{
public:
	// CJIeSuanSystem CJIeSuanSystem::s_Instance
	CJIeSuanSystem_t715337267 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CJIeSuanSystem_t715337267_StaticFields, ___s_Instance_2)); }
	inline CJIeSuanSystem_t715337267 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CJIeSuanSystem_t715337267 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CJIeSuanSystem_t715337267 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJIESUANSYSTEM_T715337267_H
#ifndef CTRIGGERFOREATBEAN_T2846052271_H
#define CTRIGGERFOREATBEAN_T2846052271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTriggerForEatBean
struct  CTriggerForEatBean_t2846052271  : public MonoBehaviour_t3962482529
{
public:
	// Ball CTriggerForEatBean::_ball
	Ball_t2206666566 * ____ball_2;

public:
	inline static int32_t get_offset_of__ball_2() { return static_cast<int32_t>(offsetof(CTriggerForEatBean_t2846052271, ____ball_2)); }
	inline Ball_t2206666566 * get__ball_2() const { return ____ball_2; }
	inline Ball_t2206666566 ** get_address_of__ball_2() { return &____ball_2; }
	inline void set__ball_2(Ball_t2206666566 * value)
	{
		____ball_2 = value;
		Il2CppCodeGenWriteBarrier((&____ball_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTRIGGERFOREATBEAN_T2846052271_H
#ifndef CMYWAVEGRID_T3025636012_H
#define CMYWAVEGRID_T3025636012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMyWaveGrid
struct  CMyWaveGrid_t3025636012  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMYWAVEGRID_T3025636012_H
#ifndef CWAVE_T3010138791_H
#define CWAVE_T3010138791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CWave
struct  CWave_t3010138791  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CWave::m_nIndex
	int32_t ___m_nIndex_5;
	// UnityEngine.SpriteRenderer CWave::_srMain
	SpriteRenderer_t3235626157 * ____srMain_6;

public:
	inline static int32_t get_offset_of_m_nIndex_5() { return static_cast<int32_t>(offsetof(CWave_t3010138791, ___m_nIndex_5)); }
	inline int32_t get_m_nIndex_5() const { return ___m_nIndex_5; }
	inline int32_t* get_address_of_m_nIndex_5() { return &___m_nIndex_5; }
	inline void set_m_nIndex_5(int32_t value)
	{
		___m_nIndex_5 = value;
	}

	inline static int32_t get_offset_of__srMain_6() { return static_cast<int32_t>(offsetof(CWave_t3010138791, ____srMain_6)); }
	inline SpriteRenderer_t3235626157 * get__srMain_6() const { return ____srMain_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_6() { return &____srMain_6; }
	inline void set__srMain_6(SpriteRenderer_t3235626157 * value)
	{
		____srMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_6), value);
	}
};

struct CWave_t3010138791_StaticFields
{
public:
	// UnityEngine.Vector3 CWave::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CWave::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// UnityEngine.Color CWave::colorTemp
	Color_t2555686324  ___colorTemp_4;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CWave_t3010138791_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CWave_t3010138791_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_colorTemp_4() { return static_cast<int32_t>(offsetof(CWave_t3010138791_StaticFields, ___colorTemp_4)); }
	inline Color_t2555686324  get_colorTemp_4() const { return ___colorTemp_4; }
	inline Color_t2555686324 * get_address_of_colorTemp_4() { return &___colorTemp_4; }
	inline void set_colorTemp_4(Color_t2555686324  value)
	{
		___colorTemp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CWAVE_T3010138791_H
#ifndef CWAVEGROUP_T2760058664_H
#define CWAVEGROUP_T2760058664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CWaveGroup
struct  CWaveGroup_t2760058664  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<CWave> CWaveGroup::m_lst
	List_1_t187246237 * ___m_lst_4;

public:
	inline static int32_t get_offset_of_m_lst_4() { return static_cast<int32_t>(offsetof(CWaveGroup_t2760058664, ___m_lst_4)); }
	inline List_1_t187246237 * get_m_lst_4() const { return ___m_lst_4; }
	inline List_1_t187246237 ** get_address_of_m_lst_4() { return &___m_lst_4; }
	inline void set_m_lst_4(List_1_t187246237 * value)
	{
		___m_lst_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_lst_4), value);
	}
};

struct CWaveGroup_t2760058664_StaticFields
{
public:
	// UnityEngine.Vector3 CWaveGroup::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CWaveGroup::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CWaveGroup_t2760058664_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CWaveGroup_t2760058664_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CWAVEGROUP_T2760058664_H
#ifndef CWAVEMANAGER_T224343356_H
#define CWAVEMANAGER_T224343356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CWaveManager
struct  CWaveManager_t224343356  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CWaveManager::m_preWave
	GameObject_t1113636619 * ___m_preWave_5;
	// UnityEngine.GameObject CWaveManager::m_preWaveGroup
	GameObject_t1113636619 * ___m_preWaveGroup_6;
	// UnityEngine.GameObject CWaveManager::m_preWaveGrid
	GameObject_t1113636619 * ___m_preWaveGrid_7;
	// System.Single CWaveManager::m_fDeltaXInGroup
	float ___m_fDeltaXInGroup_8;
	// System.Single CWaveManager::m_fDeltaYInGroup
	float ___m_fDeltaYInGroup_9;
	// System.Single CWaveManager::m_fGroupInitDeltaX
	float ___m_fGroupInitDeltaX_10;
	// System.Single CWaveManager::m_fGroupInitDeltaY
	float ___m_fGroupInitDeltaY_11;
	// System.Single CWaveManager::m_fCurScale
	float ___m_fCurScale_12;
	// System.Single CWaveManager::m_fSizeXiShu
	float ___m_fSizeXiShu_13;
	// System.Single CWaveManager::m_fIntervalXiShu
	float ___m_fIntervalXiShu_14;
	// System.Single[] CWaveManager::m_aryThreshold
	SingleU5BU5D_t1444911251* ___m_aryThreshold_15;
	// System.Single[] CWaveManager::m_aryThresholdValue
	SingleU5BU5D_t1444911251* ___m_aryThresholdValue_16;
	// System.Single CWaveManager::m_fLastCamSize
	float ___m_fLastCamSize_17;
	// System.Single CWaveManager::m_fGridWidth
	float ___m_fGridWidth_18;
	// System.Single CWaveManager::m_fGridHeight
	float ___m_fGridHeight_19;
	// System.Single CWaveManager::m_fGridOffsetX
	float ___m_fGridOffsetX_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,CWaveGroup> CWaveManager::m_dicGroups
	Dictionary_2_t1648771995 * ___m_dicGroups_21;
	// System.Collections.Generic.List`1<CWave> CWaveManager::m_lstRecycledWave
	List_1_t187246237 * ___m_lstRecycledWave_22;
	// System.Single CWaveManager::m_fCurAlpha
	float ___m_fCurAlpha_23;
	// System.Boolean CWaveManager::m_bChangingOut
	bool ___m_bChangingOut_24;
	// System.Boolean CWaveManager::m_bChangingIn
	bool ___m_bChangingIn_25;
	// System.Int32 CWaveManager::m_nDestLevel
	int32_t ___m_nDestLevel_26;

public:
	inline static int32_t get_offset_of_m_preWave_5() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_preWave_5)); }
	inline GameObject_t1113636619 * get_m_preWave_5() const { return ___m_preWave_5; }
	inline GameObject_t1113636619 ** get_address_of_m_preWave_5() { return &___m_preWave_5; }
	inline void set_m_preWave_5(GameObject_t1113636619 * value)
	{
		___m_preWave_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_preWave_5), value);
	}

	inline static int32_t get_offset_of_m_preWaveGroup_6() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_preWaveGroup_6)); }
	inline GameObject_t1113636619 * get_m_preWaveGroup_6() const { return ___m_preWaveGroup_6; }
	inline GameObject_t1113636619 ** get_address_of_m_preWaveGroup_6() { return &___m_preWaveGroup_6; }
	inline void set_m_preWaveGroup_6(GameObject_t1113636619 * value)
	{
		___m_preWaveGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preWaveGroup_6), value);
	}

	inline static int32_t get_offset_of_m_preWaveGrid_7() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_preWaveGrid_7)); }
	inline GameObject_t1113636619 * get_m_preWaveGrid_7() const { return ___m_preWaveGrid_7; }
	inline GameObject_t1113636619 ** get_address_of_m_preWaveGrid_7() { return &___m_preWaveGrid_7; }
	inline void set_m_preWaveGrid_7(GameObject_t1113636619 * value)
	{
		___m_preWaveGrid_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_preWaveGrid_7), value);
	}

	inline static int32_t get_offset_of_m_fDeltaXInGroup_8() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fDeltaXInGroup_8)); }
	inline float get_m_fDeltaXInGroup_8() const { return ___m_fDeltaXInGroup_8; }
	inline float* get_address_of_m_fDeltaXInGroup_8() { return &___m_fDeltaXInGroup_8; }
	inline void set_m_fDeltaXInGroup_8(float value)
	{
		___m_fDeltaXInGroup_8 = value;
	}

	inline static int32_t get_offset_of_m_fDeltaYInGroup_9() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fDeltaYInGroup_9)); }
	inline float get_m_fDeltaYInGroup_9() const { return ___m_fDeltaYInGroup_9; }
	inline float* get_address_of_m_fDeltaYInGroup_9() { return &___m_fDeltaYInGroup_9; }
	inline void set_m_fDeltaYInGroup_9(float value)
	{
		___m_fDeltaYInGroup_9 = value;
	}

	inline static int32_t get_offset_of_m_fGroupInitDeltaX_10() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fGroupInitDeltaX_10)); }
	inline float get_m_fGroupInitDeltaX_10() const { return ___m_fGroupInitDeltaX_10; }
	inline float* get_address_of_m_fGroupInitDeltaX_10() { return &___m_fGroupInitDeltaX_10; }
	inline void set_m_fGroupInitDeltaX_10(float value)
	{
		___m_fGroupInitDeltaX_10 = value;
	}

	inline static int32_t get_offset_of_m_fGroupInitDeltaY_11() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fGroupInitDeltaY_11)); }
	inline float get_m_fGroupInitDeltaY_11() const { return ___m_fGroupInitDeltaY_11; }
	inline float* get_address_of_m_fGroupInitDeltaY_11() { return &___m_fGroupInitDeltaY_11; }
	inline void set_m_fGroupInitDeltaY_11(float value)
	{
		___m_fGroupInitDeltaY_11 = value;
	}

	inline static int32_t get_offset_of_m_fCurScale_12() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fCurScale_12)); }
	inline float get_m_fCurScale_12() const { return ___m_fCurScale_12; }
	inline float* get_address_of_m_fCurScale_12() { return &___m_fCurScale_12; }
	inline void set_m_fCurScale_12(float value)
	{
		___m_fCurScale_12 = value;
	}

	inline static int32_t get_offset_of_m_fSizeXiShu_13() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fSizeXiShu_13)); }
	inline float get_m_fSizeXiShu_13() const { return ___m_fSizeXiShu_13; }
	inline float* get_address_of_m_fSizeXiShu_13() { return &___m_fSizeXiShu_13; }
	inline void set_m_fSizeXiShu_13(float value)
	{
		___m_fSizeXiShu_13 = value;
	}

	inline static int32_t get_offset_of_m_fIntervalXiShu_14() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fIntervalXiShu_14)); }
	inline float get_m_fIntervalXiShu_14() const { return ___m_fIntervalXiShu_14; }
	inline float* get_address_of_m_fIntervalXiShu_14() { return &___m_fIntervalXiShu_14; }
	inline void set_m_fIntervalXiShu_14(float value)
	{
		___m_fIntervalXiShu_14 = value;
	}

	inline static int32_t get_offset_of_m_aryThreshold_15() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_aryThreshold_15)); }
	inline SingleU5BU5D_t1444911251* get_m_aryThreshold_15() const { return ___m_aryThreshold_15; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryThreshold_15() { return &___m_aryThreshold_15; }
	inline void set_m_aryThreshold_15(SingleU5BU5D_t1444911251* value)
	{
		___m_aryThreshold_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryThreshold_15), value);
	}

	inline static int32_t get_offset_of_m_aryThresholdValue_16() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_aryThresholdValue_16)); }
	inline SingleU5BU5D_t1444911251* get_m_aryThresholdValue_16() const { return ___m_aryThresholdValue_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryThresholdValue_16() { return &___m_aryThresholdValue_16; }
	inline void set_m_aryThresholdValue_16(SingleU5BU5D_t1444911251* value)
	{
		___m_aryThresholdValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryThresholdValue_16), value);
	}

	inline static int32_t get_offset_of_m_fLastCamSize_17() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fLastCamSize_17)); }
	inline float get_m_fLastCamSize_17() const { return ___m_fLastCamSize_17; }
	inline float* get_address_of_m_fLastCamSize_17() { return &___m_fLastCamSize_17; }
	inline void set_m_fLastCamSize_17(float value)
	{
		___m_fLastCamSize_17 = value;
	}

	inline static int32_t get_offset_of_m_fGridWidth_18() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fGridWidth_18)); }
	inline float get_m_fGridWidth_18() const { return ___m_fGridWidth_18; }
	inline float* get_address_of_m_fGridWidth_18() { return &___m_fGridWidth_18; }
	inline void set_m_fGridWidth_18(float value)
	{
		___m_fGridWidth_18 = value;
	}

	inline static int32_t get_offset_of_m_fGridHeight_19() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fGridHeight_19)); }
	inline float get_m_fGridHeight_19() const { return ___m_fGridHeight_19; }
	inline float* get_address_of_m_fGridHeight_19() { return &___m_fGridHeight_19; }
	inline void set_m_fGridHeight_19(float value)
	{
		___m_fGridHeight_19 = value;
	}

	inline static int32_t get_offset_of_m_fGridOffsetX_20() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fGridOffsetX_20)); }
	inline float get_m_fGridOffsetX_20() const { return ___m_fGridOffsetX_20; }
	inline float* get_address_of_m_fGridOffsetX_20() { return &___m_fGridOffsetX_20; }
	inline void set_m_fGridOffsetX_20(float value)
	{
		___m_fGridOffsetX_20 = value;
	}

	inline static int32_t get_offset_of_m_dicGroups_21() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_dicGroups_21)); }
	inline Dictionary_2_t1648771995 * get_m_dicGroups_21() const { return ___m_dicGroups_21; }
	inline Dictionary_2_t1648771995 ** get_address_of_m_dicGroups_21() { return &___m_dicGroups_21; }
	inline void set_m_dicGroups_21(Dictionary_2_t1648771995 * value)
	{
		___m_dicGroups_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicGroups_21), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledWave_22() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_lstRecycledWave_22)); }
	inline List_1_t187246237 * get_m_lstRecycledWave_22() const { return ___m_lstRecycledWave_22; }
	inline List_1_t187246237 ** get_address_of_m_lstRecycledWave_22() { return &___m_lstRecycledWave_22; }
	inline void set_m_lstRecycledWave_22(List_1_t187246237 * value)
	{
		___m_lstRecycledWave_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledWave_22), value);
	}

	inline static int32_t get_offset_of_m_fCurAlpha_23() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_fCurAlpha_23)); }
	inline float get_m_fCurAlpha_23() const { return ___m_fCurAlpha_23; }
	inline float* get_address_of_m_fCurAlpha_23() { return &___m_fCurAlpha_23; }
	inline void set_m_fCurAlpha_23(float value)
	{
		___m_fCurAlpha_23 = value;
	}

	inline static int32_t get_offset_of_m_bChangingOut_24() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_bChangingOut_24)); }
	inline bool get_m_bChangingOut_24() const { return ___m_bChangingOut_24; }
	inline bool* get_address_of_m_bChangingOut_24() { return &___m_bChangingOut_24; }
	inline void set_m_bChangingOut_24(bool value)
	{
		___m_bChangingOut_24 = value;
	}

	inline static int32_t get_offset_of_m_bChangingIn_25() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_bChangingIn_25)); }
	inline bool get_m_bChangingIn_25() const { return ___m_bChangingIn_25; }
	inline bool* get_address_of_m_bChangingIn_25() { return &___m_bChangingIn_25; }
	inline void set_m_bChangingIn_25(bool value)
	{
		___m_bChangingIn_25 = value;
	}

	inline static int32_t get_offset_of_m_nDestLevel_26() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356, ___m_nDestLevel_26)); }
	inline int32_t get_m_nDestLevel_26() const { return ___m_nDestLevel_26; }
	inline int32_t* get_address_of_m_nDestLevel_26() { return &___m_nDestLevel_26; }
	inline void set_m_nDestLevel_26(int32_t value)
	{
		___m_nDestLevel_26 = value;
	}
};

struct CWaveManager_t224343356_StaticFields
{
public:
	// CWaveManager CWaveManager::s_Instance
	CWaveManager_t224343356 * ___s_Instance_2;
	// UnityEngine.Vector3 CWaveManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CWaveManager::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356_StaticFields, ___s_Instance_2)); }
	inline CWaveManager_t224343356 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CWaveManager_t224343356 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CWaveManager_t224343356 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CWaveManager_t224343356_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CWAVEMANAGER_T224343356_H
#ifndef OTHERBALLTRIGGER_T3316840920_H
#define OTHERBALLTRIGGER_T3316840920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherBallTrigger
struct  OtherBallTrigger_t3316840920  : public MonoBehaviour_t3962482529
{
public:
	// Ball OtherBallTrigger::_ball
	Ball_t2206666566 * ____ball_2;

public:
	inline static int32_t get_offset_of__ball_2() { return static_cast<int32_t>(offsetof(OtherBallTrigger_t3316840920, ____ball_2)); }
	inline Ball_t2206666566 * get__ball_2() const { return ____ball_2; }
	inline Ball_t2206666566 ** get_address_of__ball_2() { return &____ball_2; }
	inline void set__ball_2(Ball_t2206666566 * value)
	{
		____ball_2 = value;
		Il2CppCodeGenWriteBarrier((&____ball_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERBALLTRIGGER_T3316840920_H
#ifndef CTRIGGERFOREATSPORE_T1580798230_H
#define CTRIGGERFOREATSPORE_T1580798230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTriggerForEatSpore
struct  CTriggerForEatSpore_t1580798230  : public MonoBehaviour_t3962482529
{
public:
	// Ball CTriggerForEatSpore::_ball
	Ball_t2206666566 * ____ball_2;

public:
	inline static int32_t get_offset_of__ball_2() { return static_cast<int32_t>(offsetof(CTriggerForEatSpore_t1580798230, ____ball_2)); }
	inline Ball_t2206666566 * get__ball_2() const { return ____ball_2; }
	inline Ball_t2206666566 ** get_address_of__ball_2() { return &____ball_2; }
	inline void set__ball_2(Ball_t2206666566 * value)
	{
		____ball_2 = value;
		Il2CppCodeGenWriteBarrier((&____ball_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTRIGGERFOREATSPORE_T1580798230_H
#ifndef CPAOMADENG_T1722089677_H
#define CPAOMADENG_T1722089677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaoMaDeng
struct  CPaoMaDeng_t1722089677  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CPaoMaDeng::m_fSpeed
	float ___m_fSpeed_3;
	// System.Single CPaoMaDeng::m_fStartPos
	float ___m_fStartPos_4;
	// System.Single CPaoMaDeng::m_fDestPos
	float ___m_fDestPos_5;
	// System.Single CPaoMaDeng::m_fInterval
	float ___m_fInterval_6;
	// UnityEngine.UI.Text CPaoMaDeng::_txtContent
	Text_t1901882714 * ____txtContent_7;
	// System.Collections.Generic.List`1<System.String> CPaoMaDeng::m_lstContents
	List_1_t3319525431 * ___m_lstContents_8;
	// System.String[] CPaoMaDeng::m_aryContents
	StringU5BU5D_t1281789340* ___m_aryContents_9;
	// System.Boolean CPaoMaDeng::m_bWaiting
	bool ___m_bWaiting_10;
	// System.Boolean CPaoMaDeng::m_bRunnig
	bool ___m_bRunnig_11;
	// System.Int32 CPaoMaDeng::m_nIndex
	int32_t ___m_nIndex_12;
	// System.Single CPaoMaDeng::m_fTimeElapse
	float ___m_fTimeElapse_13;

public:
	inline static int32_t get_offset_of_m_fSpeed_3() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_fSpeed_3)); }
	inline float get_m_fSpeed_3() const { return ___m_fSpeed_3; }
	inline float* get_address_of_m_fSpeed_3() { return &___m_fSpeed_3; }
	inline void set_m_fSpeed_3(float value)
	{
		___m_fSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_fStartPos_4() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_fStartPos_4)); }
	inline float get_m_fStartPos_4() const { return ___m_fStartPos_4; }
	inline float* get_address_of_m_fStartPos_4() { return &___m_fStartPos_4; }
	inline void set_m_fStartPos_4(float value)
	{
		___m_fStartPos_4 = value;
	}

	inline static int32_t get_offset_of_m_fDestPos_5() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_fDestPos_5)); }
	inline float get_m_fDestPos_5() const { return ___m_fDestPos_5; }
	inline float* get_address_of_m_fDestPos_5() { return &___m_fDestPos_5; }
	inline void set_m_fDestPos_5(float value)
	{
		___m_fDestPos_5 = value;
	}

	inline static int32_t get_offset_of_m_fInterval_6() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_fInterval_6)); }
	inline float get_m_fInterval_6() const { return ___m_fInterval_6; }
	inline float* get_address_of_m_fInterval_6() { return &___m_fInterval_6; }
	inline void set_m_fInterval_6(float value)
	{
		___m_fInterval_6 = value;
	}

	inline static int32_t get_offset_of__txtContent_7() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ____txtContent_7)); }
	inline Text_t1901882714 * get__txtContent_7() const { return ____txtContent_7; }
	inline Text_t1901882714 ** get_address_of__txtContent_7() { return &____txtContent_7; }
	inline void set__txtContent_7(Text_t1901882714 * value)
	{
		____txtContent_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_7), value);
	}

	inline static int32_t get_offset_of_m_lstContents_8() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_lstContents_8)); }
	inline List_1_t3319525431 * get_m_lstContents_8() const { return ___m_lstContents_8; }
	inline List_1_t3319525431 ** get_address_of_m_lstContents_8() { return &___m_lstContents_8; }
	inline void set_m_lstContents_8(List_1_t3319525431 * value)
	{
		___m_lstContents_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstContents_8), value);
	}

	inline static int32_t get_offset_of_m_aryContents_9() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_aryContents_9)); }
	inline StringU5BU5D_t1281789340* get_m_aryContents_9() const { return ___m_aryContents_9; }
	inline StringU5BU5D_t1281789340** get_address_of_m_aryContents_9() { return &___m_aryContents_9; }
	inline void set_m_aryContents_9(StringU5BU5D_t1281789340* value)
	{
		___m_aryContents_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryContents_9), value);
	}

	inline static int32_t get_offset_of_m_bWaiting_10() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_bWaiting_10)); }
	inline bool get_m_bWaiting_10() const { return ___m_bWaiting_10; }
	inline bool* get_address_of_m_bWaiting_10() { return &___m_bWaiting_10; }
	inline void set_m_bWaiting_10(bool value)
	{
		___m_bWaiting_10 = value;
	}

	inline static int32_t get_offset_of_m_bRunnig_11() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_bRunnig_11)); }
	inline bool get_m_bRunnig_11() const { return ___m_bRunnig_11; }
	inline bool* get_address_of_m_bRunnig_11() { return &___m_bRunnig_11; }
	inline void set_m_bRunnig_11(bool value)
	{
		___m_bRunnig_11 = value;
	}

	inline static int32_t get_offset_of_m_nIndex_12() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_nIndex_12)); }
	inline int32_t get_m_nIndex_12() const { return ___m_nIndex_12; }
	inline int32_t* get_address_of_m_nIndex_12() { return &___m_nIndex_12; }
	inline void set_m_nIndex_12(int32_t value)
	{
		___m_nIndex_12 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_13() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677, ___m_fTimeElapse_13)); }
	inline float get_m_fTimeElapse_13() const { return ___m_fTimeElapse_13; }
	inline float* get_address_of_m_fTimeElapse_13() { return &___m_fTimeElapse_13; }
	inline void set_m_fTimeElapse_13(float value)
	{
		___m_fTimeElapse_13 = value;
	}
};

struct CPaoMaDeng_t1722089677_StaticFields
{
public:
	// UnityEngine.Vector3 CPaoMaDeng::VecTempPos
	Vector3_t3722313464  ___VecTempPos_2;

public:
	inline static int32_t get_offset_of_VecTempPos_2() { return static_cast<int32_t>(offsetof(CPaoMaDeng_t1722089677_StaticFields, ___VecTempPos_2)); }
	inline Vector3_t3722313464  get_VecTempPos_2() const { return ___VecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_VecTempPos_2() { return &___VecTempPos_2; }
	inline void set_VecTempPos_2(Vector3_t3722313464  value)
	{
		___VecTempPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPAOMADENG_T1722089677_H
#ifndef BALLMERGESELFTRIGGER_T2821461748_H
#define BALLMERGESELFTRIGGER_T2821461748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMergeSelfTrigger
struct  BallMergeSelfTrigger_t2821461748  : public MonoBehaviour_t3962482529
{
public:
	// Ball BallMergeSelfTrigger::_ball
	Ball_t2206666566 * ____ball_2;
	// UnityEngine.Collider2D BallMergeSelfTrigger::_TriggerSelf
	Collider2D_t2806799626 * ____TriggerSelf_3;

public:
	inline static int32_t get_offset_of__ball_2() { return static_cast<int32_t>(offsetof(BallMergeSelfTrigger_t2821461748, ____ball_2)); }
	inline Ball_t2206666566 * get__ball_2() const { return ____ball_2; }
	inline Ball_t2206666566 ** get_address_of__ball_2() { return &____ball_2; }
	inline void set__ball_2(Ball_t2206666566 * value)
	{
		____ball_2 = value;
		Il2CppCodeGenWriteBarrier((&____ball_2), value);
	}

	inline static int32_t get_offset_of__TriggerSelf_3() { return static_cast<int32_t>(offsetof(BallMergeSelfTrigger_t2821461748, ____TriggerSelf_3)); }
	inline Collider2D_t2806799626 * get__TriggerSelf_3() const { return ____TriggerSelf_3; }
	inline Collider2D_t2806799626 ** get_address_of__TriggerSelf_3() { return &____TriggerSelf_3; }
	inline void set__TriggerSelf_3(Collider2D_t2806799626 * value)
	{
		____TriggerSelf_3 = value;
		Il2CppCodeGenWriteBarrier((&____TriggerSelf_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMERGESELFTRIGGER_T2821461748_H
#ifndef CARROW_T1475493256_H
#define CARROW_T1475493256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CArrow
struct  CArrow_t1475493256  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CArrow::m_bUI
	bool ___m_bUI_2;
	// UnityEngine.SpriteRenderer CArrow::_srMain
	SpriteRenderer_t3235626157 * ____srMain_3;
	// UnityEngine.UI.Image CArrow::_imgMain
	Image_t2670269651 * ____imgMain_4;

public:
	inline static int32_t get_offset_of_m_bUI_2() { return static_cast<int32_t>(offsetof(CArrow_t1475493256, ___m_bUI_2)); }
	inline bool get_m_bUI_2() const { return ___m_bUI_2; }
	inline bool* get_address_of_m_bUI_2() { return &___m_bUI_2; }
	inline void set_m_bUI_2(bool value)
	{
		___m_bUI_2 = value;
	}

	inline static int32_t get_offset_of__srMain_3() { return static_cast<int32_t>(offsetof(CArrow_t1475493256, ____srMain_3)); }
	inline SpriteRenderer_t3235626157 * get__srMain_3() const { return ____srMain_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_3() { return &____srMain_3; }
	inline void set__srMain_3(SpriteRenderer_t3235626157 * value)
	{
		____srMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_3), value);
	}

	inline static int32_t get_offset_of__imgMain_4() { return static_cast<int32_t>(offsetof(CArrow_t1475493256, ____imgMain_4)); }
	inline Image_t2670269651 * get__imgMain_4() const { return ____imgMain_4; }
	inline Image_t2670269651 ** get_address_of__imgMain_4() { return &____imgMain_4; }
	inline void set__imgMain_4(Image_t2670269651 * value)
	{
		____imgMain_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARROW_T1475493256_H
#ifndef CCOMMONJINDUTIAO_T2836400377_H
#define CCOMMONJINDUTIAO_T2836400377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCommonJinDuTiao
struct  CCommonJinDuTiao_t2836400377  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CCommonJinDuTiao::_txtInfo
	Text_t1901882714 * ____txtInfo_4;
	// UnityEngine.UI.Text CCommonJinDuTiao::_txtJinDu
	Text_t1901882714 * ____txtJinDu_5;
	// UnityEngine.UI.Image CCommonJinDuTiao::_imgJinDu
	Image_t2670269651 * ____imgJinDu_6;
	// UnityEngine.GameObject CCommonJinDuTiao::_containerWave
	GameObject_t1113636619 * ____containerWave_7;
	// System.Single CCommonJinDuTiao::m_fCurPercent
	float ___m_fCurPercent_8;
	// UnityEngine.GameObject CCommonJinDuTiao::m_preBoWen
	GameObject_t1113636619 * ___m_preBoWen_9;
	// UnityEngine.GameObject CCommonJinDuTiao::m_preBoWenGroup
	GameObject_t1113636619 * ___m_preBoWenGroup_10;
	// System.Single CCommonJinDuTiao::m_fStartPosX
	float ___m_fStartPosX_11;
	// System.Single CCommonJinDuTiao::m_fIntervalX
	float ___m_fIntervalX_12;
	// System.Single CCommonJinDuTiao::m_fPosY
	float ___m_fPosY_13;

public:
	inline static int32_t get_offset_of__txtInfo_4() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ____txtInfo_4)); }
	inline Text_t1901882714 * get__txtInfo_4() const { return ____txtInfo_4; }
	inline Text_t1901882714 ** get_address_of__txtInfo_4() { return &____txtInfo_4; }
	inline void set__txtInfo_4(Text_t1901882714 * value)
	{
		____txtInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtInfo_4), value);
	}

	inline static int32_t get_offset_of__txtJinDu_5() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ____txtJinDu_5)); }
	inline Text_t1901882714 * get__txtJinDu_5() const { return ____txtJinDu_5; }
	inline Text_t1901882714 ** get_address_of__txtJinDu_5() { return &____txtJinDu_5; }
	inline void set__txtJinDu_5(Text_t1901882714 * value)
	{
		____txtJinDu_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtJinDu_5), value);
	}

	inline static int32_t get_offset_of__imgJinDu_6() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ____imgJinDu_6)); }
	inline Image_t2670269651 * get__imgJinDu_6() const { return ____imgJinDu_6; }
	inline Image_t2670269651 ** get_address_of__imgJinDu_6() { return &____imgJinDu_6; }
	inline void set__imgJinDu_6(Image_t2670269651 * value)
	{
		____imgJinDu_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgJinDu_6), value);
	}

	inline static int32_t get_offset_of__containerWave_7() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ____containerWave_7)); }
	inline GameObject_t1113636619 * get__containerWave_7() const { return ____containerWave_7; }
	inline GameObject_t1113636619 ** get_address_of__containerWave_7() { return &____containerWave_7; }
	inline void set__containerWave_7(GameObject_t1113636619 * value)
	{
		____containerWave_7 = value;
		Il2CppCodeGenWriteBarrier((&____containerWave_7), value);
	}

	inline static int32_t get_offset_of_m_fCurPercent_8() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ___m_fCurPercent_8)); }
	inline float get_m_fCurPercent_8() const { return ___m_fCurPercent_8; }
	inline float* get_address_of_m_fCurPercent_8() { return &___m_fCurPercent_8; }
	inline void set_m_fCurPercent_8(float value)
	{
		___m_fCurPercent_8 = value;
	}

	inline static int32_t get_offset_of_m_preBoWen_9() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ___m_preBoWen_9)); }
	inline GameObject_t1113636619 * get_m_preBoWen_9() const { return ___m_preBoWen_9; }
	inline GameObject_t1113636619 ** get_address_of_m_preBoWen_9() { return &___m_preBoWen_9; }
	inline void set_m_preBoWen_9(GameObject_t1113636619 * value)
	{
		___m_preBoWen_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBoWen_9), value);
	}

	inline static int32_t get_offset_of_m_preBoWenGroup_10() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ___m_preBoWenGroup_10)); }
	inline GameObject_t1113636619 * get_m_preBoWenGroup_10() const { return ___m_preBoWenGroup_10; }
	inline GameObject_t1113636619 ** get_address_of_m_preBoWenGroup_10() { return &___m_preBoWenGroup_10; }
	inline void set_m_preBoWenGroup_10(GameObject_t1113636619 * value)
	{
		___m_preBoWenGroup_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBoWenGroup_10), value);
	}

	inline static int32_t get_offset_of_m_fStartPosX_11() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ___m_fStartPosX_11)); }
	inline float get_m_fStartPosX_11() const { return ___m_fStartPosX_11; }
	inline float* get_address_of_m_fStartPosX_11() { return &___m_fStartPosX_11; }
	inline void set_m_fStartPosX_11(float value)
	{
		___m_fStartPosX_11 = value;
	}

	inline static int32_t get_offset_of_m_fIntervalX_12() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ___m_fIntervalX_12)); }
	inline float get_m_fIntervalX_12() const { return ___m_fIntervalX_12; }
	inline float* get_address_of_m_fIntervalX_12() { return &___m_fIntervalX_12; }
	inline void set_m_fIntervalX_12(float value)
	{
		___m_fIntervalX_12 = value;
	}

	inline static int32_t get_offset_of_m_fPosY_13() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377, ___m_fPosY_13)); }
	inline float get_m_fPosY_13() const { return ___m_fPosY_13; }
	inline float* get_address_of_m_fPosY_13() { return &___m_fPosY_13; }
	inline void set_m_fPosY_13(float value)
	{
		___m_fPosY_13 = value;
	}
};

struct CCommonJinDuTiao_t2836400377_StaticFields
{
public:
	// UnityEngine.Vector3 CCommonJinDuTiao::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCommonJinDuTiao::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCommonJinDuTiao_t2836400377_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOMMONJINDUTIAO_T2836400377_H
#ifndef CAVATAR_T1252771393_H
#define CAVATAR_T1252771393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAvatar
struct  CAvatar_t1252771393  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CAvatar::_imgAvatar
	Image_t2670269651 * ____imgAvatar_2;

public:
	inline static int32_t get_offset_of__imgAvatar_2() { return static_cast<int32_t>(offsetof(CAvatar_t1252771393, ____imgAvatar_2)); }
	inline Image_t2670269651 * get__imgAvatar_2() const { return ____imgAvatar_2; }
	inline Image_t2670269651 ** get_address_of__imgAvatar_2() { return &____imgAvatar_2; }
	inline void set__imgAvatar_2(Image_t2670269651 * value)
	{
		____imgAvatar_2 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAVATAR_T1252771393_H
#ifndef UISKILLCASTBUTTON_T1516657334_H
#define UISKILLCASTBUTTON_T1516657334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISkillCastButton
struct  UISkillCastButton_t1516657334  : public MonoBehaviour_t3962482529
{
public:
	// CFrameAnimationEffect UISkillCastButton::_effectColddownCompleted
	CFrameAnimationEffect_t443605508 * ____effectColddownCompleted_2;
	// UnityEngine.UI.Button UISkillCastButton::_btnAddPoint
	Button_t4055032469 * ____btnAddPoint_3;
	// UnityEngine.UI.Text UISkillCastButton::_txtCurLevel
	Text_t1901882714 * ____txtCurLevel_4;
	// UnityEngine.UI.Image UISkillCastButton::_imgMain
	Image_t2670269651 * ____imgMain_5;
	// UnityEngine.UI.Text UISkillCastButton::_txtName
	Text_t1901882714 * ____txtName_6;
	// UnityEngine.UI.Image UISkillCastButton::_imgLevelUp
	Image_t2670269651 * ____imgLevelUp_7;
	// UnityEngine.UI.Image UISkillCastButton::_imgArrowAppear
	Image_t2670269651 * ____imgArrowAppear_8;
	// UnityEngine.UI.Text UISkillCastButton::_txtColdDown
	Text_t1901882714 * ____txtColdDown_9;
	// UnityEngine.UI.Image UISkillCastButton::_imgColdDown
	Image_t2670269651 * ____imgColdDown_10;
	// UnityEngine.UI.Image UISkillCastButton::_imgLevelRing
	Image_t2670269651 * ____imgLevelRing_11;
	// CFrameAnimationEffect UISkillCastButton::_effectCanLevelUp
	CFrameAnimationEffect_t443605508 * ____effectCanLevelUp_12;
	// CFrameAnimationEffect UISkillCastButton::_effectAddBtnFly
	CFrameAnimationEffect_t443605508 * ____effectAddBtnFly_13;
	// System.Int32 UISkillCastButton::m_nCurNum
	int32_t ___m_nCurNum_14;
	// System.Int32 UISkillCastButton::m_nMaxNum
	int32_t ___m_nMaxNum_15;
	// System.Int32 UISkillCastButton::m_nSkillId
	int32_t ___m_nSkillId_16;
	// System.Boolean UISkillCastButton::m_bAddBtnVisible
	bool ___m_bAddBtnVisible_18;
	// System.Boolean UISkillCastButton::m_bFlying
	bool ___m_bFlying_19;
	// System.Single UISkillCastButton::m_fArrowAppearEffectTimeCount
	float ___m_fArrowAppearEffectTimeCount_20;
	// System.Int32 UISkillCastButton::m_nArrowAppearEffectSpriteIdx
	int32_t ___m_nArrowAppearEffectSpriteIdx_21;
	// System.Int32 UISkillCastButton::m_nBreatheSpriteIdx
	int32_t ___m_nBreatheSpriteIdx_22;
	// System.Single UISkillCastButton::m_fBreatheTimeCount
	float ___m_fBreatheTimeCount_23;
	// System.Int32 UISkillCastButton::m_nLevelUpEffectSpriteIdx
	int32_t ___m_nLevelUpEffectSpriteIdx_24;
	// System.Single UISkillCastButton::m_fLevelUpEffectTimeCount
	float ___m_fLevelUpEffectTimeCount_25;

public:
	inline static int32_t get_offset_of__effectColddownCompleted_2() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____effectColddownCompleted_2)); }
	inline CFrameAnimationEffect_t443605508 * get__effectColddownCompleted_2() const { return ____effectColddownCompleted_2; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectColddownCompleted_2() { return &____effectColddownCompleted_2; }
	inline void set__effectColddownCompleted_2(CFrameAnimationEffect_t443605508 * value)
	{
		____effectColddownCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&____effectColddownCompleted_2), value);
	}

	inline static int32_t get_offset_of__btnAddPoint_3() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____btnAddPoint_3)); }
	inline Button_t4055032469 * get__btnAddPoint_3() const { return ____btnAddPoint_3; }
	inline Button_t4055032469 ** get_address_of__btnAddPoint_3() { return &____btnAddPoint_3; }
	inline void set__btnAddPoint_3(Button_t4055032469 * value)
	{
		____btnAddPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&____btnAddPoint_3), value);
	}

	inline static int32_t get_offset_of__txtCurLevel_4() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____txtCurLevel_4)); }
	inline Text_t1901882714 * get__txtCurLevel_4() const { return ____txtCurLevel_4; }
	inline Text_t1901882714 ** get_address_of__txtCurLevel_4() { return &____txtCurLevel_4; }
	inline void set__txtCurLevel_4(Text_t1901882714 * value)
	{
		____txtCurLevel_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurLevel_4), value);
	}

	inline static int32_t get_offset_of__imgMain_5() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____imgMain_5)); }
	inline Image_t2670269651 * get__imgMain_5() const { return ____imgMain_5; }
	inline Image_t2670269651 ** get_address_of__imgMain_5() { return &____imgMain_5; }
	inline void set__imgMain_5(Image_t2670269651 * value)
	{
		____imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_5), value);
	}

	inline static int32_t get_offset_of__txtName_6() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____txtName_6)); }
	inline Text_t1901882714 * get__txtName_6() const { return ____txtName_6; }
	inline Text_t1901882714 ** get_address_of__txtName_6() { return &____txtName_6; }
	inline void set__txtName_6(Text_t1901882714 * value)
	{
		____txtName_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_6), value);
	}

	inline static int32_t get_offset_of__imgLevelUp_7() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____imgLevelUp_7)); }
	inline Image_t2670269651 * get__imgLevelUp_7() const { return ____imgLevelUp_7; }
	inline Image_t2670269651 ** get_address_of__imgLevelUp_7() { return &____imgLevelUp_7; }
	inline void set__imgLevelUp_7(Image_t2670269651 * value)
	{
		____imgLevelUp_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgLevelUp_7), value);
	}

	inline static int32_t get_offset_of__imgArrowAppear_8() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____imgArrowAppear_8)); }
	inline Image_t2670269651 * get__imgArrowAppear_8() const { return ____imgArrowAppear_8; }
	inline Image_t2670269651 ** get_address_of__imgArrowAppear_8() { return &____imgArrowAppear_8; }
	inline void set__imgArrowAppear_8(Image_t2670269651 * value)
	{
		____imgArrowAppear_8 = value;
		Il2CppCodeGenWriteBarrier((&____imgArrowAppear_8), value);
	}

	inline static int32_t get_offset_of__txtColdDown_9() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____txtColdDown_9)); }
	inline Text_t1901882714 * get__txtColdDown_9() const { return ____txtColdDown_9; }
	inline Text_t1901882714 ** get_address_of__txtColdDown_9() { return &____txtColdDown_9; }
	inline void set__txtColdDown_9(Text_t1901882714 * value)
	{
		____txtColdDown_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtColdDown_9), value);
	}

	inline static int32_t get_offset_of__imgColdDown_10() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____imgColdDown_10)); }
	inline Image_t2670269651 * get__imgColdDown_10() const { return ____imgColdDown_10; }
	inline Image_t2670269651 ** get_address_of__imgColdDown_10() { return &____imgColdDown_10; }
	inline void set__imgColdDown_10(Image_t2670269651 * value)
	{
		____imgColdDown_10 = value;
		Il2CppCodeGenWriteBarrier((&____imgColdDown_10), value);
	}

	inline static int32_t get_offset_of__imgLevelRing_11() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____imgLevelRing_11)); }
	inline Image_t2670269651 * get__imgLevelRing_11() const { return ____imgLevelRing_11; }
	inline Image_t2670269651 ** get_address_of__imgLevelRing_11() { return &____imgLevelRing_11; }
	inline void set__imgLevelRing_11(Image_t2670269651 * value)
	{
		____imgLevelRing_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgLevelRing_11), value);
	}

	inline static int32_t get_offset_of__effectCanLevelUp_12() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____effectCanLevelUp_12)); }
	inline CFrameAnimationEffect_t443605508 * get__effectCanLevelUp_12() const { return ____effectCanLevelUp_12; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectCanLevelUp_12() { return &____effectCanLevelUp_12; }
	inline void set__effectCanLevelUp_12(CFrameAnimationEffect_t443605508 * value)
	{
		____effectCanLevelUp_12 = value;
		Il2CppCodeGenWriteBarrier((&____effectCanLevelUp_12), value);
	}

	inline static int32_t get_offset_of__effectAddBtnFly_13() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ____effectAddBtnFly_13)); }
	inline CFrameAnimationEffect_t443605508 * get__effectAddBtnFly_13() const { return ____effectAddBtnFly_13; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectAddBtnFly_13() { return &____effectAddBtnFly_13; }
	inline void set__effectAddBtnFly_13(CFrameAnimationEffect_t443605508 * value)
	{
		____effectAddBtnFly_13 = value;
		Il2CppCodeGenWriteBarrier((&____effectAddBtnFly_13), value);
	}

	inline static int32_t get_offset_of_m_nCurNum_14() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_nCurNum_14)); }
	inline int32_t get_m_nCurNum_14() const { return ___m_nCurNum_14; }
	inline int32_t* get_address_of_m_nCurNum_14() { return &___m_nCurNum_14; }
	inline void set_m_nCurNum_14(int32_t value)
	{
		___m_nCurNum_14 = value;
	}

	inline static int32_t get_offset_of_m_nMaxNum_15() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_nMaxNum_15)); }
	inline int32_t get_m_nMaxNum_15() const { return ___m_nMaxNum_15; }
	inline int32_t* get_address_of_m_nMaxNum_15() { return &___m_nMaxNum_15; }
	inline void set_m_nMaxNum_15(int32_t value)
	{
		___m_nMaxNum_15 = value;
	}

	inline static int32_t get_offset_of_m_nSkillId_16() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_nSkillId_16)); }
	inline int32_t get_m_nSkillId_16() const { return ___m_nSkillId_16; }
	inline int32_t* get_address_of_m_nSkillId_16() { return &___m_nSkillId_16; }
	inline void set_m_nSkillId_16(int32_t value)
	{
		___m_nSkillId_16 = value;
	}

	inline static int32_t get_offset_of_m_bAddBtnVisible_18() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_bAddBtnVisible_18)); }
	inline bool get_m_bAddBtnVisible_18() const { return ___m_bAddBtnVisible_18; }
	inline bool* get_address_of_m_bAddBtnVisible_18() { return &___m_bAddBtnVisible_18; }
	inline void set_m_bAddBtnVisible_18(bool value)
	{
		___m_bAddBtnVisible_18 = value;
	}

	inline static int32_t get_offset_of_m_bFlying_19() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_bFlying_19)); }
	inline bool get_m_bFlying_19() const { return ___m_bFlying_19; }
	inline bool* get_address_of_m_bFlying_19() { return &___m_bFlying_19; }
	inline void set_m_bFlying_19(bool value)
	{
		___m_bFlying_19 = value;
	}

	inline static int32_t get_offset_of_m_fArrowAppearEffectTimeCount_20() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_fArrowAppearEffectTimeCount_20)); }
	inline float get_m_fArrowAppearEffectTimeCount_20() const { return ___m_fArrowAppearEffectTimeCount_20; }
	inline float* get_address_of_m_fArrowAppearEffectTimeCount_20() { return &___m_fArrowAppearEffectTimeCount_20; }
	inline void set_m_fArrowAppearEffectTimeCount_20(float value)
	{
		___m_fArrowAppearEffectTimeCount_20 = value;
	}

	inline static int32_t get_offset_of_m_nArrowAppearEffectSpriteIdx_21() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_nArrowAppearEffectSpriteIdx_21)); }
	inline int32_t get_m_nArrowAppearEffectSpriteIdx_21() const { return ___m_nArrowAppearEffectSpriteIdx_21; }
	inline int32_t* get_address_of_m_nArrowAppearEffectSpriteIdx_21() { return &___m_nArrowAppearEffectSpriteIdx_21; }
	inline void set_m_nArrowAppearEffectSpriteIdx_21(int32_t value)
	{
		___m_nArrowAppearEffectSpriteIdx_21 = value;
	}

	inline static int32_t get_offset_of_m_nBreatheSpriteIdx_22() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_nBreatheSpriteIdx_22)); }
	inline int32_t get_m_nBreatheSpriteIdx_22() const { return ___m_nBreatheSpriteIdx_22; }
	inline int32_t* get_address_of_m_nBreatheSpriteIdx_22() { return &___m_nBreatheSpriteIdx_22; }
	inline void set_m_nBreatheSpriteIdx_22(int32_t value)
	{
		___m_nBreatheSpriteIdx_22 = value;
	}

	inline static int32_t get_offset_of_m_fBreatheTimeCount_23() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_fBreatheTimeCount_23)); }
	inline float get_m_fBreatheTimeCount_23() const { return ___m_fBreatheTimeCount_23; }
	inline float* get_address_of_m_fBreatheTimeCount_23() { return &___m_fBreatheTimeCount_23; }
	inline void set_m_fBreatheTimeCount_23(float value)
	{
		___m_fBreatheTimeCount_23 = value;
	}

	inline static int32_t get_offset_of_m_nLevelUpEffectSpriteIdx_24() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_nLevelUpEffectSpriteIdx_24)); }
	inline int32_t get_m_nLevelUpEffectSpriteIdx_24() const { return ___m_nLevelUpEffectSpriteIdx_24; }
	inline int32_t* get_address_of_m_nLevelUpEffectSpriteIdx_24() { return &___m_nLevelUpEffectSpriteIdx_24; }
	inline void set_m_nLevelUpEffectSpriteIdx_24(int32_t value)
	{
		___m_nLevelUpEffectSpriteIdx_24 = value;
	}

	inline static int32_t get_offset_of_m_fLevelUpEffectTimeCount_25() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334, ___m_fLevelUpEffectTimeCount_25)); }
	inline float get_m_fLevelUpEffectTimeCount_25() const { return ___m_fLevelUpEffectTimeCount_25; }
	inline float* get_address_of_m_fLevelUpEffectTimeCount_25() { return &___m_fLevelUpEffectTimeCount_25; }
	inline void set_m_fLevelUpEffectTimeCount_25(float value)
	{
		___m_fLevelUpEffectTimeCount_25 = value;
	}
};

struct UISkillCastButton_t1516657334_StaticFields
{
public:
	// UnityEngine.Color UISkillCastButton::tempColor
	Color_t2555686324  ___tempColor_17;

public:
	inline static int32_t get_offset_of_tempColor_17() { return static_cast<int32_t>(offsetof(UISkillCastButton_t1516657334_StaticFields, ___tempColor_17)); }
	inline Color_t2555686324  get_tempColor_17() const { return ___tempColor_17; }
	inline Color_t2555686324 * get_address_of_tempColor_17() { return &___tempColor_17; }
	inline void set_tempColor_17(Color_t2555686324  value)
	{
		___tempColor_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISKILLCASTBUTTON_T1516657334_H
#ifndef UISKILLCONFIG_T139965773_H
#define UISKILLCONFIG_T139965773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISkillConfig
struct  UISkillConfig_t139965773  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField[] UISkillConfig::_aryValue0
	InputFieldU5BU5D_t1172778254* ____aryValue0_4;
	// UnityEngine.UI.InputField[] UISkillConfig::_aryValue1
	InputFieldU5BU5D_t1172778254* ____aryValue1_5;
	// UnityEngine.UI.InputField[] UISkillConfig::_aryValue2
	InputFieldU5BU5D_t1172778254* ____aryValue2_6;
	// UnityEngine.UI.InputField[] UISkillConfig::_aryValue3
	InputFieldU5BU5D_t1172778254* ____aryValue3_7;
	// UnityEngine.UI.InputField[] UISkillConfig::_aryValue4
	InputFieldU5BU5D_t1172778254* ____aryValue4_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.InputField[]> UISkillConfig::m_lstInputFieldValues
	List_1_t2644852996 * ___m_lstInputFieldValues_9;
	// System.Collections.Generic.List`1<System.String[]> UISkillConfig::m_lstValues
	List_1_t2753864082 * ___m_lstValues_10;
	// System.Boolean UISkillConfig::m_bInited
	bool ___m_bInited_11;
	// System.Collections.Generic.List`1<CSkillSystem/sSkillParam> UISkillConfig::m_lstSkillParam
	List_1_t2203743693 * ___m_lstSkillParam_12;

public:
	inline static int32_t get_offset_of__aryValue0_4() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ____aryValue0_4)); }
	inline InputFieldU5BU5D_t1172778254* get__aryValue0_4() const { return ____aryValue0_4; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryValue0_4() { return &____aryValue0_4; }
	inline void set__aryValue0_4(InputFieldU5BU5D_t1172778254* value)
	{
		____aryValue0_4 = value;
		Il2CppCodeGenWriteBarrier((&____aryValue0_4), value);
	}

	inline static int32_t get_offset_of__aryValue1_5() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ____aryValue1_5)); }
	inline InputFieldU5BU5D_t1172778254* get__aryValue1_5() const { return ____aryValue1_5; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryValue1_5() { return &____aryValue1_5; }
	inline void set__aryValue1_5(InputFieldU5BU5D_t1172778254* value)
	{
		____aryValue1_5 = value;
		Il2CppCodeGenWriteBarrier((&____aryValue1_5), value);
	}

	inline static int32_t get_offset_of__aryValue2_6() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ____aryValue2_6)); }
	inline InputFieldU5BU5D_t1172778254* get__aryValue2_6() const { return ____aryValue2_6; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryValue2_6() { return &____aryValue2_6; }
	inline void set__aryValue2_6(InputFieldU5BU5D_t1172778254* value)
	{
		____aryValue2_6 = value;
		Il2CppCodeGenWriteBarrier((&____aryValue2_6), value);
	}

	inline static int32_t get_offset_of__aryValue3_7() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ____aryValue3_7)); }
	inline InputFieldU5BU5D_t1172778254* get__aryValue3_7() const { return ____aryValue3_7; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryValue3_7() { return &____aryValue3_7; }
	inline void set__aryValue3_7(InputFieldU5BU5D_t1172778254* value)
	{
		____aryValue3_7 = value;
		Il2CppCodeGenWriteBarrier((&____aryValue3_7), value);
	}

	inline static int32_t get_offset_of__aryValue4_8() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ____aryValue4_8)); }
	inline InputFieldU5BU5D_t1172778254* get__aryValue4_8() const { return ____aryValue4_8; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryValue4_8() { return &____aryValue4_8; }
	inline void set__aryValue4_8(InputFieldU5BU5D_t1172778254* value)
	{
		____aryValue4_8 = value;
		Il2CppCodeGenWriteBarrier((&____aryValue4_8), value);
	}

	inline static int32_t get_offset_of_m_lstInputFieldValues_9() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ___m_lstInputFieldValues_9)); }
	inline List_1_t2644852996 * get_m_lstInputFieldValues_9() const { return ___m_lstInputFieldValues_9; }
	inline List_1_t2644852996 ** get_address_of_m_lstInputFieldValues_9() { return &___m_lstInputFieldValues_9; }
	inline void set_m_lstInputFieldValues_9(List_1_t2644852996 * value)
	{
		___m_lstInputFieldValues_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstInputFieldValues_9), value);
	}

	inline static int32_t get_offset_of_m_lstValues_10() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ___m_lstValues_10)); }
	inline List_1_t2753864082 * get_m_lstValues_10() const { return ___m_lstValues_10; }
	inline List_1_t2753864082 ** get_address_of_m_lstValues_10() { return &___m_lstValues_10; }
	inline void set_m_lstValues_10(List_1_t2753864082 * value)
	{
		___m_lstValues_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstValues_10), value);
	}

	inline static int32_t get_offset_of_m_bInited_11() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ___m_bInited_11)); }
	inline bool get_m_bInited_11() const { return ___m_bInited_11; }
	inline bool* get_address_of_m_bInited_11() { return &___m_bInited_11; }
	inline void set_m_bInited_11(bool value)
	{
		___m_bInited_11 = value;
	}

	inline static int32_t get_offset_of_m_lstSkillParam_12() { return static_cast<int32_t>(offsetof(UISkillConfig_t139965773, ___m_lstSkillParam_12)); }
	inline List_1_t2203743693 * get_m_lstSkillParam_12() const { return ___m_lstSkillParam_12; }
	inline List_1_t2203743693 ** get_address_of_m_lstSkillParam_12() { return &___m_lstSkillParam_12; }
	inline void set_m_lstSkillParam_12(List_1_t2203743693 * value)
	{
		___m_lstSkillParam_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSkillParam_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISKILLCONFIG_T139965773_H
#ifndef SYSTEMMSG_T1823497383_H
#define SYSTEMMSG_T1823497383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SystemMsg
struct  SystemMsg_t1823497383  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text SystemMsg::_txtContent
	Text_t1901882714 * ____txtContent_3;
	// System.Single SystemMsg::m_fCouting
	float ___m_fCouting_6;
	// System.Boolean SystemMsg::m_bShowing
	bool ___m_bShowing_7;
	// System.Boolean SystemMsg::m_bFadinging
	bool ___m_bFadinging_8;

public:
	inline static int32_t get_offset_of__txtContent_3() { return static_cast<int32_t>(offsetof(SystemMsg_t1823497383, ____txtContent_3)); }
	inline Text_t1901882714 * get__txtContent_3() const { return ____txtContent_3; }
	inline Text_t1901882714 ** get_address_of__txtContent_3() { return &____txtContent_3; }
	inline void set__txtContent_3(Text_t1901882714 * value)
	{
		____txtContent_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_3), value);
	}

	inline static int32_t get_offset_of_m_fCouting_6() { return static_cast<int32_t>(offsetof(SystemMsg_t1823497383, ___m_fCouting_6)); }
	inline float get_m_fCouting_6() const { return ___m_fCouting_6; }
	inline float* get_address_of_m_fCouting_6() { return &___m_fCouting_6; }
	inline void set_m_fCouting_6(float value)
	{
		___m_fCouting_6 = value;
	}

	inline static int32_t get_offset_of_m_bShowing_7() { return static_cast<int32_t>(offsetof(SystemMsg_t1823497383, ___m_bShowing_7)); }
	inline bool get_m_bShowing_7() const { return ___m_bShowing_7; }
	inline bool* get_address_of_m_bShowing_7() { return &___m_bShowing_7; }
	inline void set_m_bShowing_7(bool value)
	{
		___m_bShowing_7 = value;
	}

	inline static int32_t get_offset_of_m_bFadinging_8() { return static_cast<int32_t>(offsetof(SystemMsg_t1823497383, ___m_bFadinging_8)); }
	inline bool get_m_bFadinging_8() const { return ___m_bFadinging_8; }
	inline bool* get_address_of_m_bFadinging_8() { return &___m_bFadinging_8; }
	inline void set_m_bFadinging_8(bool value)
	{
		___m_bFadinging_8 = value;
	}
};

struct SystemMsg_t1823497383_StaticFields
{
public:
	// UnityEngine.Color SystemMsg::cTempColor
	Color_t2555686324  ___cTempColor_2;

public:
	inline static int32_t get_offset_of_cTempColor_2() { return static_cast<int32_t>(offsetof(SystemMsg_t1823497383_StaticFields, ___cTempColor_2)); }
	inline Color_t2555686324  get_cTempColor_2() const { return ___cTempColor_2; }
	inline Color_t2555686324 * get_address_of_cTempColor_2() { return &___cTempColor_2; }
	inline void set_cTempColor_2(Color_t2555686324  value)
	{
		___cTempColor_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMMSG_T1823497383_H
#ifndef UIMANAGER_T1042050227_H
#define UIMANAGER_T1042050227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t1042050227  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIManager::_uiMainCtrl
	GameObject_t1113636619 * ____uiMainCtrl_2;
	// UnityEngine.GameObject[] UIManager::m_aryUIPanelGame_PC
	GameObjectU5BU5D_t3328599146* ___m_aryUIPanelGame_PC_9;
	// UnityEngine.GameObject[] UIManager::m_aryUIPanelGame_MOBIE
	GameObjectU5BU5D_t3328599146* ___m_aryUIPanelGame_MOBIE_10;
	// UnityEngine.UI.CanvasScaler UIManager::_canvasScaler
	CanvasScaler_t2767979955 * ____canvasScaler_12;
	// UnityEngine.Canvas UIManager::_canvas
	Canvas_t3310196443 * ____canvas_13;
	// UnityEngine.UI.Image UIManager::_imgYaoGanKnob
	Image_t2670269651 * ____imgYaoGanKnob_14;
	// System.Boolean UIManager::m_bYaoGanKnobVisible
	bool ___m_bYaoGanKnobVisible_15;
	// ETCJoystick UIManager::_etcJoyStick
	ETCJoystick_t3250784002 * ____etcJoyStick_16;
	// CArrow UIManager::_arrowYaoGan
	CArrow_t1475493256 * ____arrowYaoGan_17;
	// UnityEngine.UI.GraphicRaycaster UIManager::_graphicRaycaster
	GraphicRaycaster_t2999697109 * ____graphicRaycaster_18;
	// UnityEngine.EventSystems.EventSystem UIManager::eventSystem
	EventSystem_t1003666588 * ___eventSystem_19;
	// UnityEngine.GameObject UIManager::_uiAll
	GameObject_t1113636619 * ____uiAll_20;

public:
	inline static int32_t get_offset_of__uiMainCtrl_2() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____uiMainCtrl_2)); }
	inline GameObject_t1113636619 * get__uiMainCtrl_2() const { return ____uiMainCtrl_2; }
	inline GameObject_t1113636619 ** get_address_of__uiMainCtrl_2() { return &____uiMainCtrl_2; }
	inline void set__uiMainCtrl_2(GameObject_t1113636619 * value)
	{
		____uiMainCtrl_2 = value;
		Il2CppCodeGenWriteBarrier((&____uiMainCtrl_2), value);
	}

	inline static int32_t get_offset_of_m_aryUIPanelGame_PC_9() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___m_aryUIPanelGame_PC_9)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryUIPanelGame_PC_9() const { return ___m_aryUIPanelGame_PC_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryUIPanelGame_PC_9() { return &___m_aryUIPanelGame_PC_9; }
	inline void set_m_aryUIPanelGame_PC_9(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryUIPanelGame_PC_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUIPanelGame_PC_9), value);
	}

	inline static int32_t get_offset_of_m_aryUIPanelGame_MOBIE_10() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___m_aryUIPanelGame_MOBIE_10)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryUIPanelGame_MOBIE_10() const { return ___m_aryUIPanelGame_MOBIE_10; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryUIPanelGame_MOBIE_10() { return &___m_aryUIPanelGame_MOBIE_10; }
	inline void set_m_aryUIPanelGame_MOBIE_10(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryUIPanelGame_MOBIE_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUIPanelGame_MOBIE_10), value);
	}

	inline static int32_t get_offset_of__canvasScaler_12() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____canvasScaler_12)); }
	inline CanvasScaler_t2767979955 * get__canvasScaler_12() const { return ____canvasScaler_12; }
	inline CanvasScaler_t2767979955 ** get_address_of__canvasScaler_12() { return &____canvasScaler_12; }
	inline void set__canvasScaler_12(CanvasScaler_t2767979955 * value)
	{
		____canvasScaler_12 = value;
		Il2CppCodeGenWriteBarrier((&____canvasScaler_12), value);
	}

	inline static int32_t get_offset_of__canvas_13() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____canvas_13)); }
	inline Canvas_t3310196443 * get__canvas_13() const { return ____canvas_13; }
	inline Canvas_t3310196443 ** get_address_of__canvas_13() { return &____canvas_13; }
	inline void set__canvas_13(Canvas_t3310196443 * value)
	{
		____canvas_13 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_13), value);
	}

	inline static int32_t get_offset_of__imgYaoGanKnob_14() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____imgYaoGanKnob_14)); }
	inline Image_t2670269651 * get__imgYaoGanKnob_14() const { return ____imgYaoGanKnob_14; }
	inline Image_t2670269651 ** get_address_of__imgYaoGanKnob_14() { return &____imgYaoGanKnob_14; }
	inline void set__imgYaoGanKnob_14(Image_t2670269651 * value)
	{
		____imgYaoGanKnob_14 = value;
		Il2CppCodeGenWriteBarrier((&____imgYaoGanKnob_14), value);
	}

	inline static int32_t get_offset_of_m_bYaoGanKnobVisible_15() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___m_bYaoGanKnobVisible_15)); }
	inline bool get_m_bYaoGanKnobVisible_15() const { return ___m_bYaoGanKnobVisible_15; }
	inline bool* get_address_of_m_bYaoGanKnobVisible_15() { return &___m_bYaoGanKnobVisible_15; }
	inline void set_m_bYaoGanKnobVisible_15(bool value)
	{
		___m_bYaoGanKnobVisible_15 = value;
	}

	inline static int32_t get_offset_of__etcJoyStick_16() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____etcJoyStick_16)); }
	inline ETCJoystick_t3250784002 * get__etcJoyStick_16() const { return ____etcJoyStick_16; }
	inline ETCJoystick_t3250784002 ** get_address_of__etcJoyStick_16() { return &____etcJoyStick_16; }
	inline void set__etcJoyStick_16(ETCJoystick_t3250784002 * value)
	{
		____etcJoyStick_16 = value;
		Il2CppCodeGenWriteBarrier((&____etcJoyStick_16), value);
	}

	inline static int32_t get_offset_of__arrowYaoGan_17() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____arrowYaoGan_17)); }
	inline CArrow_t1475493256 * get__arrowYaoGan_17() const { return ____arrowYaoGan_17; }
	inline CArrow_t1475493256 ** get_address_of__arrowYaoGan_17() { return &____arrowYaoGan_17; }
	inline void set__arrowYaoGan_17(CArrow_t1475493256 * value)
	{
		____arrowYaoGan_17 = value;
		Il2CppCodeGenWriteBarrier((&____arrowYaoGan_17), value);
	}

	inline static int32_t get_offset_of__graphicRaycaster_18() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____graphicRaycaster_18)); }
	inline GraphicRaycaster_t2999697109 * get__graphicRaycaster_18() const { return ____graphicRaycaster_18; }
	inline GraphicRaycaster_t2999697109 ** get_address_of__graphicRaycaster_18() { return &____graphicRaycaster_18; }
	inline void set__graphicRaycaster_18(GraphicRaycaster_t2999697109 * value)
	{
		____graphicRaycaster_18 = value;
		Il2CppCodeGenWriteBarrier((&____graphicRaycaster_18), value);
	}

	inline static int32_t get_offset_of_eventSystem_19() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___eventSystem_19)); }
	inline EventSystem_t1003666588 * get_eventSystem_19() const { return ___eventSystem_19; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_19() { return &___eventSystem_19; }
	inline void set_eventSystem_19(EventSystem_t1003666588 * value)
	{
		___eventSystem_19 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_19), value);
	}

	inline static int32_t get_offset_of__uiAll_20() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ____uiAll_20)); }
	inline GameObject_t1113636619 * get__uiAll_20() const { return ____uiAll_20; }
	inline GameObject_t1113636619 ** get_address_of__uiAll_20() { return &____uiAll_20; }
	inline void set__uiAll_20(GameObject_t1113636619 * value)
	{
		____uiAll_20 = value;
		Il2CppCodeGenWriteBarrier((&____uiAll_20), value);
	}
};

struct UIManager_t1042050227_StaticFields
{
public:
	// UnityEngine.Vector3 UIManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_7;
	// UnityEngine.Vector3 UIManager::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_8;
	// UIManager UIManager::s_Instance
	UIManager_t1042050227 * ___s_Instance_11;

public:
	inline static int32_t get_offset_of_vecTempPos_7() { return static_cast<int32_t>(offsetof(UIManager_t1042050227_StaticFields, ___vecTempPos_7)); }
	inline Vector3_t3722313464  get_vecTempPos_7() const { return ___vecTempPos_7; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_7() { return &___vecTempPos_7; }
	inline void set_vecTempPos_7(Vector3_t3722313464  value)
	{
		___vecTempPos_7 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_8() { return static_cast<int32_t>(offsetof(UIManager_t1042050227_StaticFields, ___vecTempPos2_8)); }
	inline Vector3_t3722313464  get_vecTempPos2_8() const { return ___vecTempPos2_8; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_8() { return &___vecTempPos2_8; }
	inline void set_vecTempPos2_8(Vector3_t3722313464  value)
	{
		___vecTempPos2_8 = value;
	}

	inline static int32_t get_offset_of_s_Instance_11() { return static_cast<int32_t>(offsetof(UIManager_t1042050227_StaticFields, ___s_Instance_11)); }
	inline UIManager_t1042050227 * get_s_Instance_11() const { return ___s_Instance_11; }
	inline UIManager_t1042050227 ** get_address_of_s_Instance_11() { return &___s_Instance_11; }
	inline void set_s_Instance_11(UIManager_t1042050227 * value)
	{
		___s_Instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_T1042050227_H
#ifndef CWARFOG_T4291097357_H
#define CWARFOG_T4291097357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CWarFog
struct  CWarFog_t4291097357  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CWarFog::m_goWarFog
	GameObject_t1113636619 * ___m_goWarFog_5;
	// UnityEngine.Material CWarFog::m_matFog
	Material_t340375123 * ___m_matFog_6;
	// System.Single CWarFog::m_fWorldSzie
	float ___m_fWorldSzie_7;
	// System.Single CWarFog::m_fHalfWorldSizeX
	float ___m_fHalfWorldSizeX_8;
	// System.Single CWarFog::m_fHalfWorldSizeY
	float ___m_fHalfWorldSizeY_9;
	// System.Collections.Generic.List`1<System.Single> CWarFog::m_lstBallRadius
	List_1_t2869341516 * ___m_lstBallRadius_10;
	// System.Collections.Generic.List`1<System.Single> CWarFog::m_lstBallPosX
	List_1_t2869341516 * ___m_lstBallPosX_11;
	// System.Collections.Generic.List`1<System.Single> CWarFog::m_lstBallPosY
	List_1_t2869341516 * ___m_lstBallPosY_12;

public:
	inline static int32_t get_offset_of_m_goWarFog_5() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_goWarFog_5)); }
	inline GameObject_t1113636619 * get_m_goWarFog_5() const { return ___m_goWarFog_5; }
	inline GameObject_t1113636619 ** get_address_of_m_goWarFog_5() { return &___m_goWarFog_5; }
	inline void set_m_goWarFog_5(GameObject_t1113636619 * value)
	{
		___m_goWarFog_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_goWarFog_5), value);
	}

	inline static int32_t get_offset_of_m_matFog_6() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_matFog_6)); }
	inline Material_t340375123 * get_m_matFog_6() const { return ___m_matFog_6; }
	inline Material_t340375123 ** get_address_of_m_matFog_6() { return &___m_matFog_6; }
	inline void set_m_matFog_6(Material_t340375123 * value)
	{
		___m_matFog_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_matFog_6), value);
	}

	inline static int32_t get_offset_of_m_fWorldSzie_7() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_fWorldSzie_7)); }
	inline float get_m_fWorldSzie_7() const { return ___m_fWorldSzie_7; }
	inline float* get_address_of_m_fWorldSzie_7() { return &___m_fWorldSzie_7; }
	inline void set_m_fWorldSzie_7(float value)
	{
		___m_fWorldSzie_7 = value;
	}

	inline static int32_t get_offset_of_m_fHalfWorldSizeX_8() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_fHalfWorldSizeX_8)); }
	inline float get_m_fHalfWorldSizeX_8() const { return ___m_fHalfWorldSizeX_8; }
	inline float* get_address_of_m_fHalfWorldSizeX_8() { return &___m_fHalfWorldSizeX_8; }
	inline void set_m_fHalfWorldSizeX_8(float value)
	{
		___m_fHalfWorldSizeX_8 = value;
	}

	inline static int32_t get_offset_of_m_fHalfWorldSizeY_9() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_fHalfWorldSizeY_9)); }
	inline float get_m_fHalfWorldSizeY_9() const { return ___m_fHalfWorldSizeY_9; }
	inline float* get_address_of_m_fHalfWorldSizeY_9() { return &___m_fHalfWorldSizeY_9; }
	inline void set_m_fHalfWorldSizeY_9(float value)
	{
		___m_fHalfWorldSizeY_9 = value;
	}

	inline static int32_t get_offset_of_m_lstBallRadius_10() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_lstBallRadius_10)); }
	inline List_1_t2869341516 * get_m_lstBallRadius_10() const { return ___m_lstBallRadius_10; }
	inline List_1_t2869341516 ** get_address_of_m_lstBallRadius_10() { return &___m_lstBallRadius_10; }
	inline void set_m_lstBallRadius_10(List_1_t2869341516 * value)
	{
		___m_lstBallRadius_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBallRadius_10), value);
	}

	inline static int32_t get_offset_of_m_lstBallPosX_11() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_lstBallPosX_11)); }
	inline List_1_t2869341516 * get_m_lstBallPosX_11() const { return ___m_lstBallPosX_11; }
	inline List_1_t2869341516 ** get_address_of_m_lstBallPosX_11() { return &___m_lstBallPosX_11; }
	inline void set_m_lstBallPosX_11(List_1_t2869341516 * value)
	{
		___m_lstBallPosX_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBallPosX_11), value);
	}

	inline static int32_t get_offset_of_m_lstBallPosY_12() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357, ___m_lstBallPosY_12)); }
	inline List_1_t2869341516 * get_m_lstBallPosY_12() const { return ___m_lstBallPosY_12; }
	inline List_1_t2869341516 ** get_address_of_m_lstBallPosY_12() { return &___m_lstBallPosY_12; }
	inline void set_m_lstBallPosY_12(List_1_t2869341516 * value)
	{
		___m_lstBallPosY_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBallPosY_12), value);
	}
};

struct CWarFog_t4291097357_StaticFields
{
public:
	// UnityEngine.Vector3 CWarFog::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CWarFog::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// CWarFog CWarFog::s_Instance
	CWarFog_t4291097357 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CWarFog_t4291097357_StaticFields, ___s_Instance_4)); }
	inline CWarFog_t4291097357 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CWarFog_t4291097357 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CWarFog_t4291097357 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CWARFOG_T4291097357_H
#ifndef UIMANAGER_NEW_T1948142528_H
#define UIMANAGER_NEW_T1948142528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager_New
struct  UIManager_New_t1948142528  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.CanvasScaler UIManager_New::_CanvasScaler
	CanvasScaler_t2767979955 * ____CanvasScaler_3;
	// UnityEngine.Vector3 UIManager_New::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// UnityEngine.Vector3 UIManager_New::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_5;
	// UnityEngine.Vector3 UIManager_New::vecTempPos3
	Vector3_t3722313464  ___vecTempPos3_6;
	// UnityEngine.Vector3 UIManager_New::vecTempPos4
	Vector3_t3722313464  ___vecTempPos4_7;
	// UnityEngine.GameObject UIManager_New::_uiItemSystem
	GameObject_t1113636619 * ____uiItemSystem_8;
	// UnityEngine.GameObject UIManager_New::_uiChiQiuAndJiSha
	GameObject_t1113636619 * ____uiChiQiuAndJiSha_9;
	// UnityEngine.GameObject UIManager_New::_uiItemLevel
	GameObject_t1113636619 * ____uiItemLevel_10;
	// UnityEngine.GameObject UIManager_New::_uiMainCtrl
	GameObject_t1113636619 * ____uiMainCtrl_11;
	// UnityEngine.GameObject UIManager_New::_uiExpAndLevel
	GameObject_t1113636619 * ____uiExpAndLevel_12;
	// UnityEngine.GameObject UIManager_New::_uiTime
	GameObject_t1113636619 * ____uiTime_13;
	// UnityEngine.GameObject UIManager_New::_uiFightInfo
	GameObject_t1113636619 * ____uiFightInfo_14;
	// UnityEngine.GameObject UIManager_New::_uiPaiHangBang
	GameObject_t1113636619 * ____uiPaiHangBang_15;
	// UnityEngine.GameObject UIManager_New::_uiYaoGanKnob
	GameObject_t1113636619 * ____uiYaoGanKnob_16;
	// UnityEngine.GameObject UIManager_New::_uiCheatBtn
	GameObject_t1113636619 * ____uiCheatBtn_17;
	// UnityEngine.GameObject UIManager_New::_uiJoyStick
	GameObject_t1113636619 * ____uiJoyStick_18;
	// UnityEngine.Vector2 UIManager_New::m_vecItemSystem_CommonPos
	Vector2_t2156229523  ___m_vecItemSystem_CommonPos_19;
	// UnityEngine.Vector2 UIManager_New::m_vecLevelSystem_CommonPos
	Vector2_t2156229523  ___m_vecLevelSystem_CommonPos_20;
	// UnityEngine.Vector2 UIManager_New::m_vecIPhoneXOffset
	Vector2_t2156229523  ___m_vecIPhoneXOffset_21;
	// UnityEngine.Vector2 UIManager_New::m_vecIPhoneXOffset_MainCtrl
	Vector2_t2156229523  ___m_vecIPhoneXOffset_MainCtrl_22;
	// UnityEngine.Vector2 UIManager_New::m_vecIPhoneXOffset_ChiQiuAndJiSha
	Vector2_t2156229523  ___m_vecIPhoneXOffset_ChiQiuAndJiSha_23;

public:
	inline static int32_t get_offset_of__CanvasScaler_3() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____CanvasScaler_3)); }
	inline CanvasScaler_t2767979955 * get__CanvasScaler_3() const { return ____CanvasScaler_3; }
	inline CanvasScaler_t2767979955 ** get_address_of__CanvasScaler_3() { return &____CanvasScaler_3; }
	inline void set__CanvasScaler_3(CanvasScaler_t2767979955 * value)
	{
		____CanvasScaler_3 = value;
		Il2CppCodeGenWriteBarrier((&____CanvasScaler_3), value);
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_5() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___vecTempPos2_5)); }
	inline Vector3_t3722313464  get_vecTempPos2_5() const { return ___vecTempPos2_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_5() { return &___vecTempPos2_5; }
	inline void set_vecTempPos2_5(Vector3_t3722313464  value)
	{
		___vecTempPos2_5 = value;
	}

	inline static int32_t get_offset_of_vecTempPos3_6() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___vecTempPos3_6)); }
	inline Vector3_t3722313464  get_vecTempPos3_6() const { return ___vecTempPos3_6; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos3_6() { return &___vecTempPos3_6; }
	inline void set_vecTempPos3_6(Vector3_t3722313464  value)
	{
		___vecTempPos3_6 = value;
	}

	inline static int32_t get_offset_of_vecTempPos4_7() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___vecTempPos4_7)); }
	inline Vector3_t3722313464  get_vecTempPos4_7() const { return ___vecTempPos4_7; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos4_7() { return &___vecTempPos4_7; }
	inline void set_vecTempPos4_7(Vector3_t3722313464  value)
	{
		___vecTempPos4_7 = value;
	}

	inline static int32_t get_offset_of__uiItemSystem_8() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiItemSystem_8)); }
	inline GameObject_t1113636619 * get__uiItemSystem_8() const { return ____uiItemSystem_8; }
	inline GameObject_t1113636619 ** get_address_of__uiItemSystem_8() { return &____uiItemSystem_8; }
	inline void set__uiItemSystem_8(GameObject_t1113636619 * value)
	{
		____uiItemSystem_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiItemSystem_8), value);
	}

	inline static int32_t get_offset_of__uiChiQiuAndJiSha_9() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiChiQiuAndJiSha_9)); }
	inline GameObject_t1113636619 * get__uiChiQiuAndJiSha_9() const { return ____uiChiQiuAndJiSha_9; }
	inline GameObject_t1113636619 ** get_address_of__uiChiQiuAndJiSha_9() { return &____uiChiQiuAndJiSha_9; }
	inline void set__uiChiQiuAndJiSha_9(GameObject_t1113636619 * value)
	{
		____uiChiQiuAndJiSha_9 = value;
		Il2CppCodeGenWriteBarrier((&____uiChiQiuAndJiSha_9), value);
	}

	inline static int32_t get_offset_of__uiItemLevel_10() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiItemLevel_10)); }
	inline GameObject_t1113636619 * get__uiItemLevel_10() const { return ____uiItemLevel_10; }
	inline GameObject_t1113636619 ** get_address_of__uiItemLevel_10() { return &____uiItemLevel_10; }
	inline void set__uiItemLevel_10(GameObject_t1113636619 * value)
	{
		____uiItemLevel_10 = value;
		Il2CppCodeGenWriteBarrier((&____uiItemLevel_10), value);
	}

	inline static int32_t get_offset_of__uiMainCtrl_11() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiMainCtrl_11)); }
	inline GameObject_t1113636619 * get__uiMainCtrl_11() const { return ____uiMainCtrl_11; }
	inline GameObject_t1113636619 ** get_address_of__uiMainCtrl_11() { return &____uiMainCtrl_11; }
	inline void set__uiMainCtrl_11(GameObject_t1113636619 * value)
	{
		____uiMainCtrl_11 = value;
		Il2CppCodeGenWriteBarrier((&____uiMainCtrl_11), value);
	}

	inline static int32_t get_offset_of__uiExpAndLevel_12() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiExpAndLevel_12)); }
	inline GameObject_t1113636619 * get__uiExpAndLevel_12() const { return ____uiExpAndLevel_12; }
	inline GameObject_t1113636619 ** get_address_of__uiExpAndLevel_12() { return &____uiExpAndLevel_12; }
	inline void set__uiExpAndLevel_12(GameObject_t1113636619 * value)
	{
		____uiExpAndLevel_12 = value;
		Il2CppCodeGenWriteBarrier((&____uiExpAndLevel_12), value);
	}

	inline static int32_t get_offset_of__uiTime_13() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiTime_13)); }
	inline GameObject_t1113636619 * get__uiTime_13() const { return ____uiTime_13; }
	inline GameObject_t1113636619 ** get_address_of__uiTime_13() { return &____uiTime_13; }
	inline void set__uiTime_13(GameObject_t1113636619 * value)
	{
		____uiTime_13 = value;
		Il2CppCodeGenWriteBarrier((&____uiTime_13), value);
	}

	inline static int32_t get_offset_of__uiFightInfo_14() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiFightInfo_14)); }
	inline GameObject_t1113636619 * get__uiFightInfo_14() const { return ____uiFightInfo_14; }
	inline GameObject_t1113636619 ** get_address_of__uiFightInfo_14() { return &____uiFightInfo_14; }
	inline void set__uiFightInfo_14(GameObject_t1113636619 * value)
	{
		____uiFightInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&____uiFightInfo_14), value);
	}

	inline static int32_t get_offset_of__uiPaiHangBang_15() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiPaiHangBang_15)); }
	inline GameObject_t1113636619 * get__uiPaiHangBang_15() const { return ____uiPaiHangBang_15; }
	inline GameObject_t1113636619 ** get_address_of__uiPaiHangBang_15() { return &____uiPaiHangBang_15; }
	inline void set__uiPaiHangBang_15(GameObject_t1113636619 * value)
	{
		____uiPaiHangBang_15 = value;
		Il2CppCodeGenWriteBarrier((&____uiPaiHangBang_15), value);
	}

	inline static int32_t get_offset_of__uiYaoGanKnob_16() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiYaoGanKnob_16)); }
	inline GameObject_t1113636619 * get__uiYaoGanKnob_16() const { return ____uiYaoGanKnob_16; }
	inline GameObject_t1113636619 ** get_address_of__uiYaoGanKnob_16() { return &____uiYaoGanKnob_16; }
	inline void set__uiYaoGanKnob_16(GameObject_t1113636619 * value)
	{
		____uiYaoGanKnob_16 = value;
		Il2CppCodeGenWriteBarrier((&____uiYaoGanKnob_16), value);
	}

	inline static int32_t get_offset_of__uiCheatBtn_17() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiCheatBtn_17)); }
	inline GameObject_t1113636619 * get__uiCheatBtn_17() const { return ____uiCheatBtn_17; }
	inline GameObject_t1113636619 ** get_address_of__uiCheatBtn_17() { return &____uiCheatBtn_17; }
	inline void set__uiCheatBtn_17(GameObject_t1113636619 * value)
	{
		____uiCheatBtn_17 = value;
		Il2CppCodeGenWriteBarrier((&____uiCheatBtn_17), value);
	}

	inline static int32_t get_offset_of__uiJoyStick_18() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ____uiJoyStick_18)); }
	inline GameObject_t1113636619 * get__uiJoyStick_18() const { return ____uiJoyStick_18; }
	inline GameObject_t1113636619 ** get_address_of__uiJoyStick_18() { return &____uiJoyStick_18; }
	inline void set__uiJoyStick_18(GameObject_t1113636619 * value)
	{
		____uiJoyStick_18 = value;
		Il2CppCodeGenWriteBarrier((&____uiJoyStick_18), value);
	}

	inline static int32_t get_offset_of_m_vecItemSystem_CommonPos_19() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___m_vecItemSystem_CommonPos_19)); }
	inline Vector2_t2156229523  get_m_vecItemSystem_CommonPos_19() const { return ___m_vecItemSystem_CommonPos_19; }
	inline Vector2_t2156229523 * get_address_of_m_vecItemSystem_CommonPos_19() { return &___m_vecItemSystem_CommonPos_19; }
	inline void set_m_vecItemSystem_CommonPos_19(Vector2_t2156229523  value)
	{
		___m_vecItemSystem_CommonPos_19 = value;
	}

	inline static int32_t get_offset_of_m_vecLevelSystem_CommonPos_20() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___m_vecLevelSystem_CommonPos_20)); }
	inline Vector2_t2156229523  get_m_vecLevelSystem_CommonPos_20() const { return ___m_vecLevelSystem_CommonPos_20; }
	inline Vector2_t2156229523 * get_address_of_m_vecLevelSystem_CommonPos_20() { return &___m_vecLevelSystem_CommonPos_20; }
	inline void set_m_vecLevelSystem_CommonPos_20(Vector2_t2156229523  value)
	{
		___m_vecLevelSystem_CommonPos_20 = value;
	}

	inline static int32_t get_offset_of_m_vecIPhoneXOffset_21() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___m_vecIPhoneXOffset_21)); }
	inline Vector2_t2156229523  get_m_vecIPhoneXOffset_21() const { return ___m_vecIPhoneXOffset_21; }
	inline Vector2_t2156229523 * get_address_of_m_vecIPhoneXOffset_21() { return &___m_vecIPhoneXOffset_21; }
	inline void set_m_vecIPhoneXOffset_21(Vector2_t2156229523  value)
	{
		___m_vecIPhoneXOffset_21 = value;
	}

	inline static int32_t get_offset_of_m_vecIPhoneXOffset_MainCtrl_22() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___m_vecIPhoneXOffset_MainCtrl_22)); }
	inline Vector2_t2156229523  get_m_vecIPhoneXOffset_MainCtrl_22() const { return ___m_vecIPhoneXOffset_MainCtrl_22; }
	inline Vector2_t2156229523 * get_address_of_m_vecIPhoneXOffset_MainCtrl_22() { return &___m_vecIPhoneXOffset_MainCtrl_22; }
	inline void set_m_vecIPhoneXOffset_MainCtrl_22(Vector2_t2156229523  value)
	{
		___m_vecIPhoneXOffset_MainCtrl_22 = value;
	}

	inline static int32_t get_offset_of_m_vecIPhoneXOffset_ChiQiuAndJiSha_23() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528, ___m_vecIPhoneXOffset_ChiQiuAndJiSha_23)); }
	inline Vector2_t2156229523  get_m_vecIPhoneXOffset_ChiQiuAndJiSha_23() const { return ___m_vecIPhoneXOffset_ChiQiuAndJiSha_23; }
	inline Vector2_t2156229523 * get_address_of_m_vecIPhoneXOffset_ChiQiuAndJiSha_23() { return &___m_vecIPhoneXOffset_ChiQiuAndJiSha_23; }
	inline void set_m_vecIPhoneXOffset_ChiQiuAndJiSha_23(Vector2_t2156229523  value)
	{
		___m_vecIPhoneXOffset_ChiQiuAndJiSha_23 = value;
	}
};

struct UIManager_New_t1948142528_StaticFields
{
public:
	// UIManager_New UIManager_New::s_Instance
	UIManager_New_t1948142528 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(UIManager_New_t1948142528_StaticFields, ___s_Instance_2)); }
	inline UIManager_New_t1948142528 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline UIManager_New_t1948142528 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(UIManager_New_t1948142528 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_NEW_T1948142528_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef CTIAOZI_JINBI_T3595309615_H
#define CTIAOZI_JINBI_T3595309615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZi_JinBi
struct  CTiaoZi_JinBi_t3595309615  : public CTiaoZi_t1651486976
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTIAOZI_JINBI_T3595309615_H
#ifndef CTIAOZI_YANMIE_T3565687838_H
#define CTIAOZI_YANMIE_T3565687838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CTiaoZi_YanMie
struct  CTiaoZi_YanMie_t3565687838  : public CTiaoZi_t1651486976
{
public:
	// UnityEngine.GameObject CTiaoZi_YanMie::_goMainContainer
	GameObject_t1113636619 * ____goMainContainer_66;
	// UnityEngine.GameObject CTiaoZi_YanMie::_goTxt
	GameObject_t1113636619 * ____goTxt_67;

public:
	inline static int32_t get_offset_of__goMainContainer_66() { return static_cast<int32_t>(offsetof(CTiaoZi_YanMie_t3565687838, ____goMainContainer_66)); }
	inline GameObject_t1113636619 * get__goMainContainer_66() const { return ____goMainContainer_66; }
	inline GameObject_t1113636619 ** get_address_of__goMainContainer_66() { return &____goMainContainer_66; }
	inline void set__goMainContainer_66(GameObject_t1113636619 * value)
	{
		____goMainContainer_66 = value;
		Il2CppCodeGenWriteBarrier((&____goMainContainer_66), value);
	}

	inline static int32_t get_offset_of__goTxt_67() { return static_cast<int32_t>(offsetof(CTiaoZi_YanMie_t3565687838, ____goTxt_67)); }
	inline GameObject_t1113636619 * get__goTxt_67() const { return ____goTxt_67; }
	inline GameObject_t1113636619 ** get_address_of__goTxt_67() { return &____goTxt_67; }
	inline void set__goTxt_67(GameObject_t1113636619 * value)
	{
		____goTxt_67 = value;
		Il2CppCodeGenWriteBarrier((&____goTxt_67), value);
	}
};

struct CTiaoZi_YanMie_t3565687838_StaticFields
{
public:
	// UnityEngine.Vector3 CTiaoZi_YanMie::vecTempScale
	Vector3_t3722313464  ___vecTempScale_65;

public:
	inline static int32_t get_offset_of_vecTempScale_65() { return static_cast<int32_t>(offsetof(CTiaoZi_YanMie_t3565687838_StaticFields, ___vecTempScale_65)); }
	inline Vector3_t3722313464  get_vecTempScale_65() const { return ___vecTempScale_65; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_65() { return &___vecTempScale_65; }
	inline void set_vecTempScale_65(Vector3_t3722313464  value)
	{
		___vecTempScale_65 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTIAOZI_YANMIE_T3565687838_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_16;

public:
	inline static int32_t get_offset_of_m_OnClick_16() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_16)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_16() const { return ___m_OnClick_16; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_16() { return &___m_OnClick_16; }
	inline void set_m_OnClick_16(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef CCOSMOSBUTTON_T2450447709_H
#define CCOSMOSBUTTON_T2450447709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCosmosButton
struct  CCosmosButton_t2450447709  : public Button_t4055032469
{
public:

public:
};

struct CCosmosButton_t2450447709_StaticFields
{
public:
	// UnityEngine.Vector3 CCosmosButton::vecTempScale
	Vector3_t3722313464  ___vecTempScale_17;

public:
	inline static int32_t get_offset_of_vecTempScale_17() { return static_cast<int32_t>(offsetof(CCosmosButton_t2450447709_StaticFields, ___vecTempScale_17)); }
	inline Vector3_t3722313464  get_vecTempScale_17() const { return ___vecTempScale_17; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_17() { return &___vecTempScale_17; }
	inline void set_vecTempScale_17(Vector3_t3722313464  value)
	{
		___vecTempScale_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOSMOSBUTTON_T2450447709_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (sSkillConfig_t1112470633)+ sizeof (RuntimeObject), sizeof(sSkillConfig_t1112470633_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3500[4] = 
{
	sSkillConfig_t1112470633::get_offset_of_nSkillId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillConfig_t1112470633::get_offset_of_szName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillConfig_t1112470633::get_offset_of_szDesc_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillConfig_t1112470633::get_offset_of_m_nMaxPoint_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (sSkillInfo_t192725586)+ sizeof (RuntimeObject), sizeof(sSkillInfo_t192725586 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3501[3] = 
{
	sSkillInfo_t192725586::get_offset_of_nSkillId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillInfo_t192725586::get_offset_of_nMaxPoint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillInfo_t192725586::get_offset_of_nCurPoint_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (eSkillSysProgressBarType_t1408520632)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3502[5] = 
{
	eSkillSysProgressBarType_t1408520632::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (sSpitSpoereParam_t3807642459)+ sizeof (RuntimeObject), sizeof(sSpitSpoereParam_t3807642459 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3503[14] = 
{
	sSpitSpoereParam_t3807642459::get_offset_of_fRadiusMultiple_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fCostMotherVolumePercent_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_nNumPerSecond_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSpitInterval_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fEjectSpeed_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fPushThornDis_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeMinVolume_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeMaxVolume_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeMaxShowRadius_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fHuanTing_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeA_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeX_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeB_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSpitSpoereParam_t3807642459::get_offset_of_fSporeC_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (CSporeManager_t3848038884), -1, sizeof(CSporeManager_t3848038884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3504[7] = 
{
	CSporeManager_t3848038884_StaticFields::get_offset_of_s_Instance_2(),
	CSporeManager_t3848038884::get_offset_of_m_lstRecycledSporeZ_3(),
	CSporeManager_t3848038884::get_offset_of_m_arySporeSkins_4(),
	CSporeManager_t3848038884::get_offset_of_m_nCurSelectedSporeSkinId_5(),
	CSporeManager_t3848038884::get_offset_of_m_sprDefaultSporeSkin_6(),
	CSporeManager_t3848038884::get_offset_of_m_dicSpores_7(),
	CSporeManager_t3848038884::get_offset_of_m_dicAlreadyDestroyed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (CUISkill_t4025687907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[5] = 
{
	CUISkill_t4025687907::get_offset_of__txtNum_2(),
	CUISkill_t4025687907::get_offset_of__btnAddPoint_3(),
	CUISkill_t4025687907::get_offset_of_m_nSkillId_4(),
	CUISkill_t4025687907::get_offset_of_m_nCurPoint_5(),
	CUISkill_t4025687907::get_offset_of_m_nMaxPoint_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (CCosmosSkeleton_t2697667825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3506[1] = 
{
	CCosmosSkeleton_t2697667825::get_offset_of__skeletonGraphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (CSpineManager_t1663211032), -1, sizeof(CSpineManager_t1663211032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3507[3] = 
{
	CSpineManager_t1663211032_StaticFields::get_offset_of_vecTempRotation_2(),
	CSpineManager_t1663211032_StaticFields::get_offset_of_vecTempPos1_3(),
	CSpineManager_t1663211032_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (eBallSpineType_t3164819254)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3508[4] = 
{
	eBallSpineType_t3164819254::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (CDust_t3021183826), -1, sizeof(CDust_t3021183826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3509[25] = 
{
	CDust_t3021183826::get_offset_of__srLight_2(),
	CDust_t3021183826_StaticFields::get_offset_of_colorTemp_3(),
	CDust_t3021183826_StaticFields::get_offset_of_vecTempScale_4(),
	CDust_t3021183826_StaticFields::get_offset_of_vecTempPos_5(),
	CDust_t3021183826_StaticFields::get_offset_of_vecTempRotation_6(),
	CDust_t3021183826::get_offset_of__rigid_7(),
	CDust_t3021183826::get_offset_of__collider_8(),
	CDust_t3021183826::get_offset_of_m_vec2StartPos_9(),
	CDust_t3021183826::get_offset_of_m_vecPosInOrbit_10(),
	CDust_t3021183826::get_offset_of_m_fRotation_11(),
	CDust_t3021183826::get_offset_of_vecTempDirection_12(),
	CDust_t3021183826::get_offset_of_m_fOrbitRaduis_13(),
	CDust_t3021183826::get_offset_of_m_fDeltaX_14(),
	CDust_t3021183826::get_offset_of_m_fDeltaY_15(),
	CDust_t3021183826::get_offset_of__srMain_16(),
	CDust_t3021183826::get_offset_of_m_nId_17(),
	CDust_t3021183826::get_offset_of_m_fCollisionTime_18(),
	CDust_t3021183826::get_offset_of_m_bActive_19(),
	CDust_t3021183826::get_offset_of_m_bInCamView_20(),
	CDust_t3021183826::get_offset_of_m_fSyncedTime_21(),
	CDust_t3021183826::get_offset_of_m_fBlingTime_22(),
	CDust_t3021183826::get_offset_of_m_nOp_23(),
	CDust_t3021183826::get_offset_of_m_nOpScale_24(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (CStarDust_t2438358744), -1, sizeof(CStarDust_t2438358744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3510[38] = 
{
	CStarDust_t2438358744::get_offset_of__inputDustNum_2(),
	CStarDust_t2438358744::get_offset_of__inputDustMaxSize_3(),
	CStarDust_t2438358744::get_offset_of__inputDustMinSize_4(),
	CStarDust_t2438358744::get_offset_of__inputDustBeltSize_5(),
	CStarDust_t2438358744::get_offset_of__inputDustAngularVelocity_6(),
	CStarDust_t2438358744::get_offset_of__inputDustBackToOrbitVelocity_7(),
	CStarDust_t2438358744::get_offset_of__inputDustMass_8(),
	CStarDust_t2438358744::get_offset_of_m_aryDustPrefabs_9(),
	CStarDust_t2438358744_StaticFields::get_offset_of_vecTempPos_10(),
	CStarDust_t2438358744_StaticFields::get_offset_of_vec2TempPos_11(),
	CStarDust_t2438358744::get_offset_of_m_vecOrbitCenter_12(),
	CStarDust_t2438358744::get_offset_of_m_fOrbitRaduis_13(),
	CStarDust_t2438358744::get_offset_of_m_fAngularVElocity_14(),
	CStarDust_t2438358744::get_offset_of_m_fMoveToOrbitSpeed_15(),
	CStarDust_t2438358744::get_offset_of_m_fAngularVElocityX_16(),
	CStarDust_t2438358744::get_offset_of_m_fAngularVElocityY_17(),
	CStarDust_t2438358744_StaticFields::get_offset_of_s_Instance_18(),
	CStarDust_t2438358744::get_offset_of_m_lstDust_19(),
	CStarDust_t2438358744::get_offset_of_m_xmlNode_20(),
	CStarDust_t2438358744::get_offset_of_m_ShitDust0_21(),
	CStarDust_t2438358744::get_offset_of_m_ShitDust1_22(),
	0,
	CStarDust_t2438358744::get_offset_of_m_fMoveTimeCount_24(),
	CStarDust_t2438358744::get_offset_of_m_nMovingIndex_25(),
	CStarDust_t2438358744::get_offset_of_m_bGenerated_26(),
	CStarDust_t2438358744::get_offset_of_m_bMapConfigLoaded_27(),
	CStarDust_t2438358744::get_offset_of_m_bGameStartTimeSet_28(),
	CStarDust_t2438358744_StaticFields::get_offset_of__bytesDustFullInfo_29(),
	CStarDust_t2438358744::get_offset_of_m_fSycnTimeCount_30(),
	CStarDust_t2438358744::get_offset_of_m_nShit_31(),
	CStarDust_t2438358744::get_offset_of_m_dicBeingSyncedDust_32(),
	CStarDust_t2438358744::get_offset_of_m_nDustNum_33(),
	CStarDust_t2438358744::get_offset_of_m_fDustMaxSize_34(),
	CStarDust_t2438358744::get_offset_of_m_fDustMinSize_35(),
	CStarDust_t2438358744::get_offset_of_m_fDustBeltSize_36(),
	CStarDust_t2438358744::get_offset_of_m_fDustAngularVelocity_37(),
	CStarDust_t2438358744::get_offset_of_m_fDustMass_38(),
	CStarDust_t2438358744::get_offset_of_m_fDustBackToOrbitVelocity_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (CBoWen_t1312958833), -1, sizeof(CBoWen_t1312958833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3511[2] = 
{
	CBoWen_t1312958833_StaticFields::get_offset_of_vecTempScale_2(),
	CBoWen_t1312958833::get_offset_of__srMain_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (CBoWenGrid_t1563892939), -1, sizeof(CBoWenGrid_t1563892939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3512[2] = 
{
	CBoWenGrid_t1563892939_StaticFields::get_offset_of_vecTempPos_2(),
	CBoWenGrid_t1563892939_StaticFields::get_offset_of_vecTempScale_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (CBoWenGroup_t4269360863), -1, sizeof(CBoWenGroup_t4269360863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3513[2] = 
{
	CBoWenGroup_t4269360863_StaticFields::get_offset_of_vecTempPos_2(),
	CBoWenGroup_t4269360863_StaticFields::get_offset_of_vecTempScale_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (CLine_t1113746895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3514[1] = 
{
	CLine_t1113746895::get_offset_of__lrMain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (CLineGroup_t1725554608), -1, sizeof(CLineGroup_t1725554608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3515[2] = 
{
	CLineGroup_t1725554608_StaticFields::get_offset_of_vecTempScale_2(),
	CLineGroup_t1725554608::get_offset_of__srMain_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (CStripeManager_t2410268600), -1, sizeof(CStripeManager_t2410268600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3516[18] = 
{
	CStripeManager_t2410268600_StaticFields::get_offset_of_s_Instance_2(),
	CStripeManager_t2410268600_StaticFields::get_offset_of_vecTempPos_3(),
	CStripeManager_t2410268600_StaticFields::get_offset_of_vecTempPos1_4(),
	CStripeManager_t2410268600_StaticFields::get_offset_of_vecTempPos2_5(),
	CStripeManager_t2410268600_StaticFields::get_offset_of_vecTempScale_6(),
	CStripeManager_t2410268600_StaticFields::get_offset_of_vecTempDir_7(),
	CStripeManager_t2410268600::get_offset_of_m_preBoWenGroup_8(),
	CStripeManager_t2410268600::get_offset_of_m_preBoWenGrid_9(),
	CStripeManager_t2410268600::get_offset_of_m_preBoWen_10(),
	CStripeManager_t2410268600::get_offset_of_m_lstGroups_11(),
	CStripeManager_t2410268600::get_offset_of_m_colorBoWen_12(),
	CStripeManager_t2410268600::get_offset_of_m_fCurGridScale_13(),
	CStripeManager_t2410268600::get_offset_of_m_fGridCam2ScaleXiShu_14(),
	CStripeManager_t2410268600::get_offset_of_m_fCurCamSize_15(),
	CStripeManager_t2410268600::get_offset_of_m_vecOriginPoints_16(),
	CStripeManager_t2410268600::get_offset_of_m_aryThreshold_17(),
	CStripeManager_t2410268600::get_offset_of_m_fLastCamSize_18(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (CTiaoZi_t1651486976), -1, sizeof(CTiaoZi_t1651486976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3517[63] = 
{
	CTiaoZi_t1651486976::get_offset_of__sortingGroup_2(),
	CTiaoZi_t1651486976_StaticFields::get_offset_of_vecTempScale_3(),
	CTiaoZi_t1651486976_StaticFields::get_offset_of_vecTempPos_4(),
	CTiaoZi_t1651486976_StaticFields::get_offset_of_vecTempColor_5(),
	CTiaoZi_t1651486976::get_offset_of__tmMain_6(),
	CTiaoZi_t1651486976::get_offset_of__canvasGroup_7(),
	CTiaoZi_t1651486976::get_offset_of__srBg_8(),
	CTiaoZi_t1651486976::get_offset_of__srTitle_9(),
	CTiaoZi_t1651486976::get_offset_of__txtContent_10(),
	CTiaoZi_t1651486976::get_offset_of__txtMeshContent_11(),
	CTiaoZi_t1651486976::get_offset_of__txtMeshContent_MiaoBian_12(),
	CTiaoZi_t1651486976::get_offset_of_m_eType_13(),
	CTiaoZi_t1651486976::get_offset_of_m_Ball_14(),
	CTiaoZi_t1651486976::get_offset_of_m_bActive_15(),
	CTiaoZi_t1651486976::get_offset_of_m_nStatus_16(),
	CTiaoZi_t1651486976::get_offset_of_m_vecParentV_17(),
	CTiaoZi_t1651486976::get_offset_of_m_fPaoWuXianVx0_18(),
	CTiaoZi_t1651486976::get_offset_of_m_fPaoWuXianVy0_19(),
	CTiaoZi_t1651486976::get_offset_of_m_fPaoWuXianT_20(),
	CTiaoZi_t1651486976::get_offset_of_m_fPaoWuXianA_21(),
	CTiaoZi_t1651486976::get_offset_of_m_fPaoWuXianAlphaA_22(),
	CTiaoZi_t1651486976::get_offset_of_m_fBianDaMaxScale_23(),
	CTiaoZi_t1651486976::get_offset_of_m_fBianDaA_24(),
	CTiaoZi_t1651486976::get_offset_of_m_fSuoXiaoA_25(),
	CTiaoZi_t1651486976::get_offset_of_m_fShangShengT_26(),
	CTiaoZi_t1651486976::get_offset_of_m_fShangShengBanJingXiShu_27(),
	CTiaoZi_t1651486976::get_offset_of_m_fShangShengV0_28(),
	CTiaoZi_t1651486976::get_offset_of_m_fShangShengA_29(),
	CTiaoZi_t1651486976::get_offset_of_m_fShangShengAlphaA_30(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_ScaleXiShu_31(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_BianDaA_32(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_BianDaT_33(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_ChiXuT_34(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_BianXiaoA_35(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_BianXiaoT_36(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_MaxScale_37(),
	CTiaoZi_t1651486976::get_offset_of_m_fType3_CurChiXuTime_38(),
	CTiaoZi_t1651486976::get_offset_of_m_fType4_CurChiXuTime_39(),
	CTiaoZi_t1651486976::get_offset_of_m_fType4_ChiXuTime_40(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_MaxScale_41(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_BianXiaoEndScale_42(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_ChiXuT_43(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_XiaJiangDistance_44(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_BianDaA_45(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_BianXiaoA_46(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_BianXiaoAlphaA_47(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_XiaJiangA_48(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_XiaJiangV0_49(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_XiaJiangV_50(),
	CTiaoZi_t1651486976::get_offset_of_m_fYanMie_CurChiXuTime_51(),
	CTiaoZi_t1651486976::get_offset_of_m_fTunShi_XiaJiang_V_52(),
	CTiaoZi_t1651486976::get_offset_of_m_fTunShi_Aplha_V_53(),
	CTiaoZi_t1651486976::get_offset_of_m_fTunShi_Scale_V_54(),
	CTiaoZi_t1651486976::get_offset_of_m_fTunShi_ChiXu_55(),
	CTiaoZi_t1651486976::get_offset_of_m_fTunShi_CurChiXuTime_56(),
	CTiaoZi_t1651486976::get_offset_of_m_vecStartPos_57(),
	CTiaoZi_t1651486976::get_offset_of_m_vecCurPos_58(),
	CTiaoZi_t1651486976::get_offset_of_m_fCurAlpha_59(),
	CTiaoZi_t1651486976::get_offset_of_m_fCurScale_60(),
	CTiaoZi_t1651486976::get_offset_of_m_fBgAlpha_61(),
	CTiaoZi_t1651486976::get_offset_of_m_eZiYuanType_62(),
	CTiaoZi_t1651486976::get_offset_of_m_fType1_CurChiXuTime_63(),
	CTiaoZi_t1651486976::get_offset_of_m_nIndex_64(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (CTiaoZi_JinBi_t3595309615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (CTiaoZi_YanMie_t3565687838), -1, sizeof(CTiaoZi_YanMie_t3565687838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3519[3] = 
{
	CTiaoZi_YanMie_t3565687838_StaticFields::get_offset_of_vecTempScale_65(),
	CTiaoZi_YanMie_t3565687838::get_offset_of__goMainContainer_66(),
	CTiaoZi_YanMie_t3565687838::get_offset_of__goTxt_67(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (CTiaoZiManager_t3709983989), -1, sizeof(CTiaoZiManager_t3709983989_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3520[64] = 
{
	CTiaoZiManager_t3709983989_StaticFields::get_offset_of_s_Instance_2(),
	CTiaoZiManager_t3709983989::get_offset_of_m_aryColors_3(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_TunShi_4(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_JinBi_5(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_ZhanCi_TiJi_6(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_ZhanCi_JinQian_7(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_TanKai_8(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_ShengJi_9(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_CiMianYi_10(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTiaoZiCamThreshold_YanMieMianYi_11(),
	CTiaoZiManager_t3709983989::get_offset_of_m_preTiaoZi_Test_12(),
	CTiaoZiManager_t3709983989::get_offset_of_m_preTiaoZi_13(),
	CTiaoZiManager_t3709983989::get_offset_of_m_preTiaoZi_YanMie_14(),
	CTiaoZiManager_t3709983989::get_offset_of_m_preTiaoZi_TunShi_15(),
	CTiaoZiManager_t3709983989::get_offset_of_m_preTiaoZi_JinBi_16(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fPaoWuXianVx0_17(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fPaoWuXianVy0_18(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fPaoWuXianT_19(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fPaoWuXianA_20(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fPaoWuXianBanJingXiShu_21(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fPaoWuXianSizeXiShu_22(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fBianDaT_23(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fShangShengT_24(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fBianDaBanJingXiShu_25(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fShangShengBanJingXiShu_26(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fType3_ScaleXiShu_27(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fType3_BianDaT_28(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fType3_BianXiaoT_29(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fType3_ChiXuT_30(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fType3_OffsetXiShuX_31(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fType3_OffsetXiShuY_32(),
	CTiaoZiManager_t3709983989::get_offset_of_ShengJi_ScaleXiShu_33(),
	CTiaoZiManager_t3709983989::get_offset_of_ShengJi_BianDaT_34(),
	CTiaoZiManager_t3709983989::get_offset_of_ShengJi_BianXiaoT_35(),
	CTiaoZiManager_t3709983989::get_offset_of_ShengJi_ChiXuT_36(),
	CTiaoZiManager_t3709983989::get_offset_of_ShengJi_OffsetXiShuX_37(),
	CTiaoZiManager_t3709983989::get_offset_of_ShengJi_OffsetXiShuY_38(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_ScaleXiShu_39(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_DaWeiYiXiShu_40(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_BianDaT_41(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_BianXiaoT_42(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_ChiXuT_43(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_XiaJiangXiShu_44(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMie_BianXiaoEndXiShu_45(),
	0,
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_ScaleXiShu_47(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_DistanceXiShu_48(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_XiaJiangXiShu_49(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_XiaJiangT_50(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_ChiXuT_51(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_DirX_52(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_DirY_53(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fTunShi_PushOffsetY_54(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fJinBi_ScaleXiShu_55(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fShengJi_ChiXuTime_56(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fShengJi_DirX_57(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fShengJi_DirY_58(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fCiMianYi_ChiXuTime_59(),
	CTiaoZiManager_t3709983989::get_offset_of_m_fYanMieMianYi_ChiXuTime_60(),
	CTiaoZiManager_t3709983989::get_offset_of_m_lstTiaoZi_61(),
	CTiaoZiManager_t3709983989::get_offset_of_m_lstTiaoZi_YanMie_62(),
	CTiaoZiManager_t3709983989::get_offset_of_m_lstTiaoZi_TunShi_63(),
	CTiaoZiManager_t3709983989::get_offset_of_m_lstTiaoZi_JinBi_64(),
	CTiaoZiManager_t3709983989::get_offset_of_m_lstQueue_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (eResourceType_t1087091141)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3521[3] = 
{
	eResourceType_t1087091141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (eTiaoZiType_t2622546907)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3522[9] = 
{
	eTiaoZiType_t2622546907::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (eZiYuanType_t1731312856)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3523[3] = 
{
	eZiYuanType_t1731312856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (sTiaoZiQueueItem_t3790450940)+ sizeof (RuntimeObject), sizeof(sTiaoZiQueueItem_t3790450940 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3524[3] = 
{
	sTiaoZiQueueItem_t3790450940::get_offset_of_pos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTiaoZiQueueItem_t3790450940::get_offset_of_fScale_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTiaoZiQueueItem_t3790450940::get_offset_of_Color_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (BallMergeSelfTrigger_t2821461748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[2] = 
{
	BallMergeSelfTrigger_t2821461748::get_offset_of__ball_2(),
	BallMergeSelfTrigger_t2821461748::get_offset_of__TriggerSelf_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (CTriggerForEatBean_t2846052271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3526[1] = 
{
	CTriggerForEatBean_t2846052271::get_offset_of__ball_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (CTriggerForEatSpore_t1580798230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[1] = 
{
	CTriggerForEatSpore_t1580798230::get_offset_of__ball_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (OtherBallTrigger_t3316840920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[1] = 
{
	OtherBallTrigger_t3316840920::get_offset_of__ball_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (CArrow_t1475493256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3529[3] = 
{
	CArrow_t1475493256::get_offset_of_m_bUI_2(),
	CArrow_t1475493256::get_offset_of__srMain_3(),
	CArrow_t1475493256::get_offset_of__imgMain_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (CCommonJinDuTiao_t2836400377), -1, sizeof(CCommonJinDuTiao_t2836400377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3530[12] = 
{
	CCommonJinDuTiao_t2836400377_StaticFields::get_offset_of_vecTempPos_2(),
	CCommonJinDuTiao_t2836400377_StaticFields::get_offset_of_vecTempScale_3(),
	CCommonJinDuTiao_t2836400377::get_offset_of__txtInfo_4(),
	CCommonJinDuTiao_t2836400377::get_offset_of__txtJinDu_5(),
	CCommonJinDuTiao_t2836400377::get_offset_of__imgJinDu_6(),
	CCommonJinDuTiao_t2836400377::get_offset_of__containerWave_7(),
	CCommonJinDuTiao_t2836400377::get_offset_of_m_fCurPercent_8(),
	CCommonJinDuTiao_t2836400377::get_offset_of_m_preBoWen_9(),
	CCommonJinDuTiao_t2836400377::get_offset_of_m_preBoWenGroup_10(),
	CCommonJinDuTiao_t2836400377::get_offset_of_m_fStartPosX_11(),
	CCommonJinDuTiao_t2836400377::get_offset_of_m_fIntervalX_12(),
	CCommonJinDuTiao_t2836400377::get_offset_of_m_fPosY_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (CCosmosButton_t2450447709), -1, sizeof(CCosmosButton_t2450447709_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3531[1] = 
{
	CCosmosButton_t2450447709_StaticFields::get_offset_of_vecTempScale_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (CCyberTreeList_t4040119676), -1, sizeof(CCyberTreeList_t4040119676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3532[31] = 
{
	CCyberTreeList_t4040119676::get_offset_of_m_bCurItemNum_2(),
	CCyberTreeList_t4040119676::get_offset_of_m_fItemSize_3(),
	CCyberTreeList_t4040119676::get_offset_of_m_goContainer_4(),
	CCyberTreeList_t4040119676::get_offset_of_m_lstItems_5(),
	CCyberTreeList_t4040119676_StaticFields::get_offset_of_vecTempPos_6(),
	CCyberTreeList_t4040119676_StaticFields::get_offset_of_vecTempPos2_7(),
	CCyberTreeList_t4040119676_StaticFields::get_offset_of_vecTempScale_8(),
	CCyberTreeList_t4040119676::get_offset_of_m_fMoveSpeed_9(),
	CCyberTreeList_t4040119676::get_offset_of_m_bVert_10(),
	CCyberTreeList_t4040119676::get_offset_of_m_fStartTop_11(),
	CCyberTreeList_t4040119676::get_offset_of_m_fMinTop_12(),
	CCyberTreeList_t4040119676::get_offset_of_m_fStartBottom_13(),
	CCyberTreeList_t4040119676::get_offset_of_m_fMaxBottom_14(),
	CCyberTreeList_t4040119676::get_offset_of_m_nDontAutoBackIfItemsNumLessThan_15(),
	CCyberTreeList_t4040119676::get_offset_of_m_fAutoBackSpeed_16(),
	CCyberTreeList_t4040119676::get_offset_of__graphicRaycaster_17(),
	CCyberTreeList_t4040119676::get_offset_of_eventSystem_18(),
	CCyberTreeList_t4040119676::get_offset_of_m_bDraging_19(),
	CCyberTreeList_t4040119676::get_offset_of_m_vecLastFrameMousePos_20(),
	CCyberTreeList_t4040119676::get_offset_of_m_vecLastMovement_21(),
	CCyberTreeList_t4040119676::get_offset_of_m_bSliding_22(),
	CCyberTreeList_t4040119676::get_offset_of_m_fV0_23(),
	CCyberTreeList_t4040119676::get_offset_of_m_fA_24(),
	CCyberTreeList_t4040119676::get_offset_of_m_nAutoBackDir_25(),
	CCyberTreeList_t4040119676::get_offset_of_m_fMaxExceedAmount_26(),
	CCyberTreeList_t4040119676::get_offset_of_m_bExceedLeftThreshold_27(),
	CCyberTreeList_t4040119676::get_offset_of_m_fExceedLeftAmount_28(),
	CCyberTreeList_t4040119676::get_offset_of_m_bExceedRightThreshold_29(),
	CCyberTreeList_t4040119676::get_offset_of_m_fExceedRightAmount_30(),
	CCyberTreeList_t4040119676::get_offset_of_m_bExceedTopThreshold_31(),
	CCyberTreeList_t4040119676::get_offset_of_m_bExceedBottomThreshold_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (CCyberTreeListItem_t2642449450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3533[1] = 
{
	CCyberTreeListItem_t2642449450::get_offset_of_m_nIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (CCyberTreePoPo_t261854377), -1, sizeof(CCyberTreePoPo_t261854377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3534[9] = 
{
	CCyberTreePoPo_t261854377_StaticFields::get_offset_of_vecTempPos_2(),
	CCyberTreePoPo_t261854377_StaticFields::get_offset_of_vecTempScale_3(),
	CCyberTreePoPo_t261854377::get_offset_of__imgLeft_4(),
	CCyberTreePoPo_t261854377::get_offset_of__imgCenter_5(),
	CCyberTreePoPo_t261854377::get_offset_of__imgRight_6(),
	CCyberTreePoPo_t261854377::get_offset_of__txtContent_7(),
	CCyberTreePoPo_t261854377::get_offset_of_m_fUnitWidth_8(),
	CCyberTreePoPo_t261854377::get_offset_of_m_fFontSize_9(),
	CCyberTreePoPo_t261854377::get_offset_of_m_bActive_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (CPaoMaDeng_t1722089677), -1, sizeof(CPaoMaDeng_t1722089677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3535[12] = 
{
	CPaoMaDeng_t1722089677_StaticFields::get_offset_of_VecTempPos_2(),
	CPaoMaDeng_t1722089677::get_offset_of_m_fSpeed_3(),
	CPaoMaDeng_t1722089677::get_offset_of_m_fStartPos_4(),
	CPaoMaDeng_t1722089677::get_offset_of_m_fDestPos_5(),
	CPaoMaDeng_t1722089677::get_offset_of_m_fInterval_6(),
	CPaoMaDeng_t1722089677::get_offset_of__txtContent_7(),
	CPaoMaDeng_t1722089677::get_offset_of_m_lstContents_8(),
	CPaoMaDeng_t1722089677::get_offset_of_m_aryContents_9(),
	CPaoMaDeng_t1722089677::get_offset_of_m_bWaiting_10(),
	CPaoMaDeng_t1722089677::get_offset_of_m_bRunnig_11(),
	CPaoMaDeng_t1722089677::get_offset_of_m_nIndex_12(),
	CPaoMaDeng_t1722089677::get_offset_of_m_fTimeElapse_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (CPlayerInfo_t53183828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3536[2] = 
{
	CPlayerInfo_t53183828::get_offset_of__txtPlayerName_2(),
	CPlayerInfo_t53183828::get_offset_of__txtPlayerLevel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (CWifi_t3890179523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3537[1] = 
{
	CWifi_t3890179523::get_offset_of_m_aryGrid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (CyberTreeScrollView_t257211719), -1, sizeof(CyberTreeScrollView_t257211719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3538[3] = 
{
	CyberTreeScrollView_t257211719_StaticFields::get_offset_of_vecTempScale_2(),
	CyberTreeScrollView_t257211719::get_offset_of_m_goContent_3(),
	CyberTreeScrollView_t257211719::get_offset_of_m_lstItems_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (CButtonClick_t2942357293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3539[6] = 
{
	CButtonClick_t2942357293::get_offset_of__effectClick_2(),
	CButtonClick_t2942357293::get_offset_of__imgMain_3(),
	CButtonClick_t2942357293::get_offset_of__txtTitle_4(),
	CButtonClick_t2942357293::get_offset_of__aryButtonStatus_5(),
	CButtonClick_t2942357293::get_offset_of__arySzButtonColors_6(),
	CButtonClick_t2942357293::get_offset_of__aryButtonColors_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (eButtonStatus_t48935231)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3540[3] = 
{
	eButtonStatus_t48935231::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (CClassEditor_t335744478), -1, sizeof(CClassEditor_t335744478_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3541[68] = 
{
	CClassEditor_t335744478::get_offset_of__MonsterEditor_2(),
	CClassEditor_t335744478_StaticFields::get_offset_of_vecTempScale_3(),
	CClassEditor_t335744478_StaticFields::get_offset_of_vecTempPos_4(),
	CClassEditor_t335744478_StaticFields::get_offset_of_vecTempPos1_5(),
	CClassEditor_t335744478_StaticFields::get_offset_of_vec2Temp_6(),
	CClassEditor_t335744478::get_offset_of__goClassCircle_7(),
	CClassEditor_t335744478::get_offset_of__cShengFuPanDingBall_8(),
	CClassEditor_t335744478::get_offset_of__goContainer_MannualMonster_9(),
	CClassEditor_t335744478::get_offset_of__inputTimeOfOneGame_10(),
	CClassEditor_t335744478::get_offset_of__inputAttenuateRate_11(),
	CClassEditor_t335744478::get_offset_of__inputClassCircleRadius_12(),
	CClassEditor_t335744478::get_offset_of__inputShengFuPanDingBallSize_13(),
	CClassEditor_t335744478::get_offset_of__dropArea_14(),
	CClassEditor_t335744478::get_offset_of__dropThornType_15(),
	CClassEditor_t335744478::get_offset_of__dropThornId_16(),
	CClassEditor_t335744478::get_offset_of__inputSizeThreshold_17(),
	CClassEditor_t335744478::get_offset_of__inputDensity_18(),
	CClassEditor_t335744478::get_offset_of__inputRebornTime_19(),
	CClassEditor_t335744478::get_offset_of__inputRebornTimeMannual_20(),
	CClassEditor_t335744478::get_offset_of__inputNormalAttenuate_21(),
	CClassEditor_t335744478::get_offset_of__inputInvasionAttenuate_22(),
	CClassEditor_t335744478::get_offset_of__inputInvasionSpeedSlowDown_23(),
	CClassEditor_t335744478::get_offset_of__inputWarFogRadius_24(),
	CClassEditor_t335744478::get_offset_of__btnAddOneMonster_25(),
	CClassEditor_t335744478::get_offset_of__btnDeleteOneMonster_26(),
	CClassEditor_t335744478::get_offset_of__txtMonsterNum_27(),
	CClassEditor_t335744478::get_offset_of_m_fTimeOfOneGame_28(),
	CClassEditor_t335744478::get_offset_of_m_fClassCircleRadius_29(),
	CClassEditor_t335744478::get_offset_of_m_fShengFuPanDingBallSize_30(),
	CClassEditor_t335744478::get_offset_of_m_fLowThreshold_31(),
	CClassEditor_t335744478::get_offset_of_m_fMiddleThreshold_32(),
	CClassEditor_t335744478::get_offset_of_m_fHighThreshold_33(),
	CClassEditor_t335744478::get_offset_of_m_fWarFogRadius_34(),
	CClassEditor_t335744478::get_offset_of_m_fAttenuateRate_35(),
	CClassEditor_t335744478::get_offset_of_m_dicClassConfig_36(),
	CClassEditor_t335744478::get_offset_of_m_CurConfig_37(),
	CClassEditor_t335744478::get_offset_of_m_CurThornConfig_38(),
	CClassEditor_t335744478::get_offset_of_m_CurMannualMonsterConfig_39(),
	CClassEditor_t335744478::get_offset_of_m_dicMannualMonsterConfig_40(),
	CClassEditor_t335744478::get_offset_of_tempMannualMonsterConfig_41(),
	CClassEditor_t335744478_StaticFields::get_offset_of_s_Instance_42(),
	CClassEditor_t335744478::get_offset_of_m_CurMovingMonster_43(),
	CClassEditor_t335744478::get_offset_of_m_CurSelectedMonster_44(),
	CClassEditor_t335744478::get_offset_of_m_bMouseDown_45(),
	CClassEditor_t335744478::get_offset_of_m_vecLastMouseWorldPos_46(),
	CClassEditor_t335744478::get_offset_of_tempThornConfig_47(),
	CClassEditor_t335744478_StaticFields::get_offset_of_s_nTotalBeanNum_48(),
	CClassEditor_t335744478::get_offset_of_m_dicAllSpores_49(),
	CClassEditor_t335744478::get_offset_of_m_dicAllThorns_50(),
	CClassEditor_t335744478::get_offset_of_m_lstMonstersX_51(),
	CClassEditor_t335744478::get_offset_of_m_lstMonstersY_52(),
	CClassEditor_t335744478::get_offset_of_m_lGuidCount_53(),
	0,
	CClassEditor_t335744478::get_offset_of_m_fCheckInCamViewTimeCount_55(),
	CClassEditor_t335744478::get_offset_of_dicTemp_56(),
	CClassEditor_t335744478::get_offset_of_m_lstActiveMonster_57(),
	CClassEditor_t335744478::get_offset_of_dicTempActiveMonsterOfThisRound_58(),
	CClassEditor_t335744478::get_offset_of_m_lstSyncPublicList_59(),
	CClassEditor_t335744478::get_offset_of_m_lstToBeReborn_60(),
	CClassEditor_t335744478::get_offset_of_m_lstCiChuXianThorn_61(),
	CClassEditor_t335744478::get_offset_of_m_dicThornColor_62(),
	CClassEditor_t335744478::get_offset_of_c_RebornInterval_63(),
	CClassEditor_t335744478::get_offset_of_m_fRebornCount_64(),
	CClassEditor_t335744478::get_offset_of_m_dicPushedThornList_65(),
	CClassEditor_t335744478_StaticFields::get_offset_of_s_LowCount_66(),
	0,
	CClassEditor_t335744478::get_offset_of_m_lSceneBallGuid_68(),
	CClassEditor_t335744478::get_offset_of_m_dicSceneBalls_69(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (sClassConfig_t764718558)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[6] = 
{
	sClassConfig_t764718558::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sClassConfig_t764718558::get_offset_of_fSizeThreshold_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sClassConfig_t764718558::get_offset_of_fNormalAttenuate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sClassConfig_t764718558::get_offset_of_fInvasionAttenuate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sClassConfig_t764718558::get_offset_of_fInvasionSpeedSlowdown_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sClassConfig_t764718558::get_offset_of_lstThorns_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (sThornOfThisClassConfig_t350464872)+ sizeof (RuntimeObject), sizeof(sThornOfThisClassConfig_t350464872_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3543[3] = 
{
	sThornOfThisClassConfig_t350464872::get_offset_of_szThornId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornOfThisClassConfig_t350464872::get_offset_of_fDensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornOfThisClassConfig_t350464872::get_offset_of_fRebornTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (sMannualMonsterConfig_t1844217021)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3544[3] = 
{
	sMannualMonsterConfig_t1844217021::get_offset_of_szMonsterId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sMannualMonsterConfig_t1844217021::get_offset_of_fRebornTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sMannualMonsterConfig_t1844217021::get_offset_of_lstPos_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (CDeadManager_t731087622), -1, sizeof(CDeadManager_t731087622_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3545[4] = 
{
	CDeadManager_t731087622_StaticFields::get_offset_of_s_Instance_2(),
	CDeadManager_t731087622::get_offset_of__panleMain_3(),
	CDeadManager_t731087622::get_offset_of__txtRebornWaitingTimeLeft_4(),
	CDeadManager_t731087622::get_offset_of_m_fRebornWaitingTimeLeft_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (CMonsterEditor_t2594843249), -1, sizeof(CMonsterEditor_t2594843249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3546[61] = 
{
	CMonsterEditor_t2594843249::get_offset_of_m_aryBeanPic_2(),
	CMonsterEditor_t2594843249::get_offset_of_m_arySprites_3(),
	CMonsterEditor_t2594843249::get_offset_of_m_arySprites_Thorn_4(),
	CMonsterEditor_t2594843249::get_offset_of__dropdownThorszId_5(),
	CMonsterEditor_t2594843249::get_offset_of__dropdownThornType_6(),
	CMonsterEditor_t2594843249::get_offset_of__dropdownSkill_7(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornDesc_8(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornFoodSize_9(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldSelfSize_10(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornColor_11(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExp_12(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornMoney_13(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornBuffId_14(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeChildNum_15(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeMotherLeftPercent_16(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeRunDistance_17(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeRunTime_18(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeStay_19(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeShellTime_20(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornExplodeFormatioszId_21(),
	CMonsterEditor_t2594843249::get_offset_of__inputfieldThornSkillName_22(),
	CMonsterEditor_t2594843249_StaticFields::get_offset_of_vecTempPos_23(),
	CMonsterEditor_t2594843249_StaticFields::get_offset_of_s_Instance_24(),
	CMonsterEditor_t2594843249::get_offset_of_m_dicMonsterConfig_25(),
	CMonsterEditor_t2594843249::get_offset_of_tempMonsterConfig_26(),
	CMonsterEditor_t2594843249::get_offset_of_m_CurThornConfig_27(),
	CMonsterEditor_t2594843249::get_offset_of_dicTemp_28(),
	0,
	0,
	0,
	0,
	0,
	CMonsterEditor_t2594843249::get_offset_of_m_goBeanContainer_34(),
	CMonsterEditor_t2594843249::get_offset_of_m_sprBeanPic_35(),
	CMonsterEditor_t2594843249::get_offset_of_m_sprBeanDeadPic_36(),
	CMonsterEditor_t2594843249::get_offset_of_m_sprBeanCollection_37(),
	CMonsterEditor_t2594843249::get_offset_of_m_preBeanCollection_38(),
	CMonsterEditor_t2594843249::get_offset_of_m_ColorBeanDead_39(),
	CMonsterEditor_t2594843249::get_offset_of_m_ColorBeanLive_40(),
	CMonsterEditor_t2594843249::get_offset_of_m_ColorAllEmpty_41(),
	CMonsterEditor_t2594843249::get_offset_of_m_lstBeanCollection_42(),
	CMonsterEditor_t2594843249::get_offset_of_m_nCollectionGuid_43(),
	CMonsterEditor_t2594843249::get_offset_of_m_lstLocalPos_44(),
	CMonsterEditor_t2594843249::get_offset_of_m_lstTexPos_45(),
	CMonsterEditor_t2594843249::get_offset_of_m_sprBeanCollectionEmpty_46(),
	CMonsterEditor_t2594843249::get_offset_of_m_dicBeanColor_47(),
	CMonsterEditor_t2594843249::get_offset_of_m_dicBeanCollections_48(),
	CMonsterEditor_t2594843249::get_offset_of_m_dicBeanCollectionsByGuid_49(),
	CMonsterEditor_t2594843249::get_offset_of_m_nCollectionIndex_50(),
	CMonsterEditor_t2594843249::get_offset_of_m_nQuKuai_51(),
	CMonsterEditor_t2594843249::get_offset_of_m_nQuKuaiIndex_i_52(),
	CMonsterEditor_t2594843249::get_offset_of_m_nQuKuaiIndex_j_53(),
	CMonsterEditor_t2594843249::get_offset_of_m_bInitQuKuaiCompleted_54(),
	CMonsterEditor_t2594843249::get_offset_of_m_nTotalNUm_55(),
	CMonsterEditor_t2594843249::get_offset_of_m_bInitQuKuaiStarted_56(),
	CMonsterEditor_t2594843249::get_offset_of_m_fBeanInteractBallTimeCount_57(),
	CMonsterEditor_t2594843249::get_offset_of_m_lstBeanRebornList_58(),
	CMonsterEditor_t2594843249::get_offset_of_m_fBeanRebornLoopTimeCount_59(),
	CMonsterEditor_t2594843249::get_offset_of_m_lstInteractingCollection_60(),
	0,
	CMonsterEditor_t2594843249::get_offset_of_m_lstGenerateBeanNodeList_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (eMonsterBuildingType_t3379255469)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3547[6] = 
{
	eMonsterBuildingType_t3379255469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (sThornConfig_t1242444451)+ sizeof (RuntimeObject), sizeof(sThornConfig_t1242444451_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3548[17] = 
{
	sThornConfig_t1242444451::get_offset_of_szId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_nType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fFoodSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_szColor_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_szDesc_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fSelfSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExp_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fMoney_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fBuffId_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeChildNum_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeMotherLeftPercent_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeRunDistance_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeRunTime_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeStayTime_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeShellTime_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_fExplodeFormationId_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sThornConfig_t1242444451::get_offset_of_nSkillId_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (sGenerateBeanNode_t810489850)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[4] = 
{
	sGenerateBeanNode_t810489850::get_offset_of_thorn_buildin_config_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGenerateBeanNode_t810489850::get_offset_of_thorn_in_class_config_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGenerateBeanNode_t810489850::get_offset_of_aryPos_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGenerateBeanNode_t810489850::get_offset_of_nCurNum_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (CMsgBox_t3123528476), -1, sizeof(CMsgBox_t3123528476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3550[5] = 
{
	CMsgBox_t3123528476_StaticFields::get_offset_of_s_Instance_2(),
	CMsgBox_t3123528476::get_offset_of_m_fTotalShowTime_3(),
	CMsgBox_t3123528476::get_offset_of_m_fCurShowTimeLapse_4(),
	CMsgBox_t3123528476::get_offset_of__goContainer_5(),
	CMsgBox_t3123528476::get_offset_of__txtContent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (CMsgList_t4144960221), -1, sizeof(CMsgList_t4144960221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3551[15] = 
{
	CMsgList_t4144960221::get_offset_of_m_preMsgListItem_2(),
	CMsgList_t4144960221::get_offset_of_m_preAssistAvatar_3(),
	CMsgList_t4144960221::get_offset_of_m_fVertSapce_4(),
	CMsgList_t4144960221::get_offset_of_m_fItemScale_5(),
	CMsgList_t4144960221::get_offset_of_m_fMoveInterval_6(),
	CMsgList_t4144960221::get_offset_of_m_fMoveTime_7(),
	CMsgList_t4144960221::get_offset_of_m_fMoveSpeed_8(),
	CMsgList_t4144960221::get_offset_of_m_lstItems_9(),
	CMsgList_t4144960221::get_offset_of_m_lstRecycledItems_10(),
	CMsgList_t4144960221_StaticFields::get_offset_of_vecTempPos_11(),
	CMsgList_t4144960221_StaticFields::get_offset_of_vecTempScale_12(),
	CMsgList_t4144960221_StaticFields::get_offset_of_s_Instance_13(),
	CMsgList_t4144960221::get_offset_of_m_bMoving_14(),
	CMsgList_t4144960221::get_offset_of_m_fMoveIntervalCount_15(),
	CMsgList_t4144960221::get_offset_of_m_fMoveTimeCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (CMsgListItem_t1139323174), -1, sizeof(CMsgListItem_t1139323174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3552[13] = 
{
	CMsgListItem_t1139323174::get_offset_of__txt1_2(),
	CMsgListItem_t1139323174::get_offset_of__txt2_3(),
	CMsgListItem_t1139323174::get_offset_of_img_4(),
	CMsgListItem_t1139323174::get_offset_of_imgHighLight_5(),
	CMsgListItem_t1139323174::get_offset_of__imgKiller_6(),
	CMsgListItem_t1139323174::get_offset_of__imgDeadMeat_7(),
	CMsgListItem_t1139323174::get_offset_of_m_fAlpha_8(),
	CMsgListItem_t1139323174::get_offset_of_m_bFading_9(),
	CMsgListItem_t1139323174_StaticFields::get_offset_of_cTempColor_10(),
	CMsgListItem_t1139323174_StaticFields::get_offset_of_vecTempPos_11(),
	CMsgListItem_t1139323174_StaticFields::get_offset_of_vecTempScale_12(),
	CMsgListItem_t1139323174_StaticFields::get_offset_of_tempRect_13(),
	CMsgListItem_t1139323174::get_offset_of_m_lstAssistAvatar_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (CProgressBar_t881982788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[2] = 
{
	CProgressBar_t881982788::get_offset_of__txtCaption_2(),
	CProgressBar_t881982788::get_offset_of__imgBar_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (CShuangYaoGan_t3597386049), -1, sizeof(CShuangYaoGan_t3597386049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3554[5] = 
{
	CShuangYaoGan_t3597386049_StaticFields::get_offset_of_vecTempPos_2(),
	CShuangYaoGan_t3597386049::get_offset_of__imgKnob_3(),
	CShuangYaoGan_t3597386049::get_offset_of_m_fRadius_4(),
	CShuangYaoGan_t3597386049::get_offset_of_m_bShowing_5(),
	CShuangYaoGan_t3597386049::get_offset_of_m_vecDir_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (CBiShuaTuoWei_t3092616324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3555[1] = 
{
	CBiShuaTuoWei_t3092616324::get_offset_of__imgMask_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (CGeRenJieSuanCounter_t645084407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3556[1] = 
{
	CGeRenJieSuanCounter_t645084407::get_offset_of__txtValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (CJieSuanCounter_t4266685435), -1, sizeof(CJieSuanCounter_t4266685435_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3557[21] = 
{
	CJieSuanCounter_t4266685435::get_offset_of__goZuiJiaKuang_2(),
	CJieSuanCounter_t4266685435::get_offset_of__skeleLike_3(),
	CJieSuanCounter_t4266685435::get_offset_of__imgLiked_Click_4(),
	CJieSuanCounter_t4266685435::get_offset_of__imgLiked_NotClick_5(),
	CJieSuanCounter_t4266685435::get_offset_of__effectAvatar_6(),
	CJieSuanCounter_t4266685435::get_offset_of__effectOuterBorder_7(),
	CJieSuanCounter_t4266685435::get_offset_of__effectQuanChangZuiJia_QianZou_8(),
	CJieSuanCounter_t4266685435::get_offset_of__effectQuanChangZuiJia_ChiXu_9(),
	CJieSuanCounter_t4266685435::get_offset_of__imgAvatar_10(),
	CJieSuanCounter_t4266685435::get_offset_of__txtName_11(),
	CJieSuanCounter_t4266685435::get_offset_of__txtVolume_12(),
	CJieSuanCounter_t4266685435::get_offset_of__CommonInfo_13(),
	CJieSuanCounter_t4266685435::get_offset_of__EatVolume_14(),
	CJieSuanCounter_t4266685435::get_offset_of__txtLikeNum_15(),
	CJieSuanCounter_t4266685435::get_offset_of_m_nZuiJiaKuangStatus_16(),
	CJieSuanCounter_t4266685435::get_offset_of_m_nLikeBtnStatus_17(),
	CJieSuanCounter_t4266685435::get_offset_of_m_fLikeStatusTimeLapse_18(),
	CJieSuanCounter_t4266685435_StaticFields::get_offset_of_vecTempScale_19(),
	CJieSuanCounter_t4266685435::get_offset_of_m_nPlayerId_20(),
	CJieSuanCounter_t4266685435::get_offset_of_m_nCounterId_21(),
	CJieSuanCounter_t4266685435::get_offset_of_m_nLikeNum_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (CJieSuanMain_t410608331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[3] = 
{
	CJieSuanMain_t410608331::get_offset_of__txtJieSuanInfo_2(),
	CJieSuanMain_t410608331::get_offset_of_m_fCount_3(),
	CJieSuanMain_t410608331::get_offset_of_m_bCounting_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (CJIeSuanSystem_t715337267), -1, sizeof(CJIeSuanSystem_t715337267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3559[2] = 
{
	CJIeSuanSystem_t715337267_StaticFields::get_offset_of_s_Instance_2(),
	CJIeSuanSystem_t715337267::get_offset_of_m_aryJieSuanPanels_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (eJieSuanType_t2754244357)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3560[2] = 
{
	eJieSuanType_t2754244357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (U3CLoadSceneU3Ec__Iterator0_t2187349195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[4] = 
{
	U3CLoadSceneU3Ec__Iterator0_t2187349195::get_offset_of_scene_name_0(),
	U3CLoadSceneU3Ec__Iterator0_t2187349195::get_offset_of_U24current_1(),
	U3CLoadSceneU3Ec__Iterator0_t2187349195::get_offset_of_U24disposing_2(),
	U3CLoadSceneU3Ec__Iterator0_t2187349195::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (CAvatar_t1252771393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3562[1] = 
{
	CAvatar_t1252771393::get_offset_of__imgAvatar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (UISkillCastButton_t1516657334), -1, sizeof(UISkillCastButton_t1516657334_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3563[24] = 
{
	UISkillCastButton_t1516657334::get_offset_of__effectColddownCompleted_2(),
	UISkillCastButton_t1516657334::get_offset_of__btnAddPoint_3(),
	UISkillCastButton_t1516657334::get_offset_of__txtCurLevel_4(),
	UISkillCastButton_t1516657334::get_offset_of__imgMain_5(),
	UISkillCastButton_t1516657334::get_offset_of__txtName_6(),
	UISkillCastButton_t1516657334::get_offset_of__imgLevelUp_7(),
	UISkillCastButton_t1516657334::get_offset_of__imgArrowAppear_8(),
	UISkillCastButton_t1516657334::get_offset_of__txtColdDown_9(),
	UISkillCastButton_t1516657334::get_offset_of__imgColdDown_10(),
	UISkillCastButton_t1516657334::get_offset_of__imgLevelRing_11(),
	UISkillCastButton_t1516657334::get_offset_of__effectCanLevelUp_12(),
	UISkillCastButton_t1516657334::get_offset_of__effectAddBtnFly_13(),
	UISkillCastButton_t1516657334::get_offset_of_m_nCurNum_14(),
	UISkillCastButton_t1516657334::get_offset_of_m_nMaxNum_15(),
	UISkillCastButton_t1516657334::get_offset_of_m_nSkillId_16(),
	UISkillCastButton_t1516657334_StaticFields::get_offset_of_tempColor_17(),
	UISkillCastButton_t1516657334::get_offset_of_m_bAddBtnVisible_18(),
	UISkillCastButton_t1516657334::get_offset_of_m_bFlying_19(),
	UISkillCastButton_t1516657334::get_offset_of_m_fArrowAppearEffectTimeCount_20(),
	UISkillCastButton_t1516657334::get_offset_of_m_nArrowAppearEffectSpriteIdx_21(),
	UISkillCastButton_t1516657334::get_offset_of_m_nBreatheSpriteIdx_22(),
	UISkillCastButton_t1516657334::get_offset_of_m_fBreatheTimeCount_23(),
	UISkillCastButton_t1516657334::get_offset_of_m_nLevelUpEffectSpriteIdx_24(),
	UISkillCastButton_t1516657334::get_offset_of_m_fLevelUpEffectTimeCount_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (UISkillConfig_t139965773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3564[11] = 
{
	0,
	0,
	UISkillConfig_t139965773::get_offset_of__aryValue0_4(),
	UISkillConfig_t139965773::get_offset_of__aryValue1_5(),
	UISkillConfig_t139965773::get_offset_of__aryValue2_6(),
	UISkillConfig_t139965773::get_offset_of__aryValue3_7(),
	UISkillConfig_t139965773::get_offset_of__aryValue4_8(),
	UISkillConfig_t139965773::get_offset_of_m_lstInputFieldValues_9(),
	UISkillConfig_t139965773::get_offset_of_m_lstValues_10(),
	UISkillConfig_t139965773::get_offset_of_m_bInited_11(),
	UISkillConfig_t139965773::get_offset_of_m_lstSkillParam_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (SystemMsg_t1823497383), -1, sizeof(SystemMsg_t1823497383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3565[7] = 
{
	SystemMsg_t1823497383_StaticFields::get_offset_of_cTempColor_2(),
	SystemMsg_t1823497383::get_offset_of__txtContent_3(),
	0,
	0,
	SystemMsg_t1823497383::get_offset_of_m_fCouting_6(),
	SystemMsg_t1823497383::get_offset_of_m_bShowing_7(),
	SystemMsg_t1823497383::get_offset_of_m_bFadinging_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (UIManager_t1042050227), -1, sizeof(UIManager_t1042050227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3566[19] = 
{
	UIManager_t1042050227::get_offset_of__uiMainCtrl_2(),
	0,
	0,
	0,
	0,
	UIManager_t1042050227_StaticFields::get_offset_of_vecTempPos_7(),
	UIManager_t1042050227_StaticFields::get_offset_of_vecTempPos2_8(),
	UIManager_t1042050227::get_offset_of_m_aryUIPanelGame_PC_9(),
	UIManager_t1042050227::get_offset_of_m_aryUIPanelGame_MOBIE_10(),
	UIManager_t1042050227_StaticFields::get_offset_of_s_Instance_11(),
	UIManager_t1042050227::get_offset_of__canvasScaler_12(),
	UIManager_t1042050227::get_offset_of__canvas_13(),
	UIManager_t1042050227::get_offset_of__imgYaoGanKnob_14(),
	UIManager_t1042050227::get_offset_of_m_bYaoGanKnobVisible_15(),
	UIManager_t1042050227::get_offset_of__etcJoyStick_16(),
	UIManager_t1042050227::get_offset_of__arrowYaoGan_17(),
	UIManager_t1042050227::get_offset_of__graphicRaycaster_18(),
	UIManager_t1042050227::get_offset_of_eventSystem_19(),
	UIManager_t1042050227::get_offset_of__uiAll_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (UIManager_New_t1948142528), -1, sizeof(UIManager_New_t1948142528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3567[22] = 
{
	UIManager_New_t1948142528_StaticFields::get_offset_of_s_Instance_2(),
	UIManager_New_t1948142528::get_offset_of__CanvasScaler_3(),
	UIManager_New_t1948142528::get_offset_of_vecTempPos_4(),
	UIManager_New_t1948142528::get_offset_of_vecTempPos2_5(),
	UIManager_New_t1948142528::get_offset_of_vecTempPos3_6(),
	UIManager_New_t1948142528::get_offset_of_vecTempPos4_7(),
	UIManager_New_t1948142528::get_offset_of__uiItemSystem_8(),
	UIManager_New_t1948142528::get_offset_of__uiChiQiuAndJiSha_9(),
	UIManager_New_t1948142528::get_offset_of__uiItemLevel_10(),
	UIManager_New_t1948142528::get_offset_of__uiMainCtrl_11(),
	UIManager_New_t1948142528::get_offset_of__uiExpAndLevel_12(),
	UIManager_New_t1948142528::get_offset_of__uiTime_13(),
	UIManager_New_t1948142528::get_offset_of__uiFightInfo_14(),
	UIManager_New_t1948142528::get_offset_of__uiPaiHangBang_15(),
	UIManager_New_t1948142528::get_offset_of__uiYaoGanKnob_16(),
	UIManager_New_t1948142528::get_offset_of__uiCheatBtn_17(),
	UIManager_New_t1948142528::get_offset_of__uiJoyStick_18(),
	UIManager_New_t1948142528::get_offset_of_m_vecItemSystem_CommonPos_19(),
	UIManager_New_t1948142528::get_offset_of_m_vecLevelSystem_CommonPos_20(),
	UIManager_New_t1948142528::get_offset_of_m_vecIPhoneXOffset_21(),
	UIManager_New_t1948142528::get_offset_of_m_vecIPhoneXOffset_MainCtrl_22(),
	UIManager_New_t1948142528::get_offset_of_m_vecIPhoneXOffset_ChiQiuAndJiSha_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (CWarFog_t4291097357), -1, sizeof(CWarFog_t4291097357_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3568[11] = 
{
	CWarFog_t4291097357_StaticFields::get_offset_of_vecTempPos_2(),
	CWarFog_t4291097357_StaticFields::get_offset_of_vecTempScale_3(),
	CWarFog_t4291097357_StaticFields::get_offset_of_s_Instance_4(),
	CWarFog_t4291097357::get_offset_of_m_goWarFog_5(),
	CWarFog_t4291097357::get_offset_of_m_matFog_6(),
	CWarFog_t4291097357::get_offset_of_m_fWorldSzie_7(),
	CWarFog_t4291097357::get_offset_of_m_fHalfWorldSizeX_8(),
	CWarFog_t4291097357::get_offset_of_m_fHalfWorldSizeY_9(),
	CWarFog_t4291097357::get_offset_of_m_lstBallRadius_10(),
	CWarFog_t4291097357::get_offset_of_m_lstBallPosX_11(),
	CWarFog_t4291097357::get_offset_of_m_lstBallPosY_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (eBallInfo_t935743328)+ sizeof (RuntimeObject), sizeof(eBallInfo_t935743328 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3569[3] = 
{
	eBallInfo_t935743328::get_offset_of_radius_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eBallInfo_t935743328::get_offset_of_posx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	eBallInfo_t935743328::get_offset_of_posy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (CMyWaveGrid_t3025636012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (CWave_t3010138791), -1, sizeof(CWave_t3010138791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3571[5] = 
{
	CWave_t3010138791_StaticFields::get_offset_of_vecTempPos_2(),
	CWave_t3010138791_StaticFields::get_offset_of_vecTempScale_3(),
	CWave_t3010138791_StaticFields::get_offset_of_colorTemp_4(),
	CWave_t3010138791::get_offset_of_m_nIndex_5(),
	CWave_t3010138791::get_offset_of__srMain_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (CWaveGroup_t2760058664), -1, sizeof(CWaveGroup_t2760058664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3572[3] = 
{
	CWaveGroup_t2760058664_StaticFields::get_offset_of_vecTempPos_2(),
	CWaveGroup_t2760058664_StaticFields::get_offset_of_vecTempScale_3(),
	CWaveGroup_t2760058664::get_offset_of_m_lst_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (CWaveManager_t224343356), -1, sizeof(CWaveManager_t224343356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3573[25] = 
{
	CWaveManager_t224343356_StaticFields::get_offset_of_s_Instance_2(),
	CWaveManager_t224343356_StaticFields::get_offset_of_vecTempPos_3(),
	CWaveManager_t224343356_StaticFields::get_offset_of_vecTempScale_4(),
	CWaveManager_t224343356::get_offset_of_m_preWave_5(),
	CWaveManager_t224343356::get_offset_of_m_preWaveGroup_6(),
	CWaveManager_t224343356::get_offset_of_m_preWaveGrid_7(),
	CWaveManager_t224343356::get_offset_of_m_fDeltaXInGroup_8(),
	CWaveManager_t224343356::get_offset_of_m_fDeltaYInGroup_9(),
	CWaveManager_t224343356::get_offset_of_m_fGroupInitDeltaX_10(),
	CWaveManager_t224343356::get_offset_of_m_fGroupInitDeltaY_11(),
	CWaveManager_t224343356::get_offset_of_m_fCurScale_12(),
	CWaveManager_t224343356::get_offset_of_m_fSizeXiShu_13(),
	CWaveManager_t224343356::get_offset_of_m_fIntervalXiShu_14(),
	CWaveManager_t224343356::get_offset_of_m_aryThreshold_15(),
	CWaveManager_t224343356::get_offset_of_m_aryThresholdValue_16(),
	CWaveManager_t224343356::get_offset_of_m_fLastCamSize_17(),
	CWaveManager_t224343356::get_offset_of_m_fGridWidth_18(),
	CWaveManager_t224343356::get_offset_of_m_fGridHeight_19(),
	CWaveManager_t224343356::get_offset_of_m_fGridOffsetX_20(),
	CWaveManager_t224343356::get_offset_of_m_dicGroups_21(),
	CWaveManager_t224343356::get_offset_of_m_lstRecycledWave_22(),
	CWaveManager_t224343356::get_offset_of_m_fCurAlpha_23(),
	CWaveManager_t224343356::get_offset_of_m_bChangingOut_24(),
	CWaveManager_t224343356::get_offset_of_m_bChangingIn_25(),
	CWaveManager_t224343356::get_offset_of_m_nDestLevel_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (Animation_t615783283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3574[3] = 
{
	Animation_t615783283::get_offset_of_timelines_0(),
	Animation_t615783283::get_offset_of_duration_1(),
	Animation_t615783283::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (MixPose_t983230599)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3576[4] = 
{
	MixPose_t983230599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (MixDirection_t3149175205)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3577[3] = 
{
	MixDirection_t3149175205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (TimelineType_t3142998273)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3578[16] = 
{
	TimelineType_t3142998273::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (CurveTimeline_t3673209699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[5] = 
{
	0,
	0,
	0,
	0,
	CurveTimeline_t3673209699::get_offset_of_curves_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (RotateTimeline_t860518009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[6] = 
{
	0,
	0,
	0,
	0,
	RotateTimeline_t860518009::get_offset_of_boneIndex_9(),
	RotateTimeline_t860518009::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (TranslateTimeline_t2733426737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3581[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TranslateTimeline_t2733426737::get_offset_of_boneIndex_11(),
	TranslateTimeline_t2733426737::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (ScaleTimeline_t1810411048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (ShearTimeline_t3812859450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (ColorTimeline_t3754116780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3584[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ColorTimeline_t3754116780::get_offset_of_slotIndex_15(),
	ColorTimeline_t3754116780::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (TwoColorTimeline_t1834727230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3585[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TwoColorTimeline_t1834727230::get_offset_of_frames_21(),
	TwoColorTimeline_t1834727230::get_offset_of_slotIndex_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (AttachmentTimeline_t2048728856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3586[3] = 
{
	AttachmentTimeline_t2048728856::get_offset_of_slotIndex_0(),
	AttachmentTimeline_t2048728856::get_offset_of_frames_1(),
	AttachmentTimeline_t2048728856::get_offset_of_attachmentNames_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (DeformTimeline_t3552075840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3587[4] = 
{
	DeformTimeline_t3552075840::get_offset_of_slotIndex_5(),
	DeformTimeline_t3552075840::get_offset_of_frames_6(),
	DeformTimeline_t3552075840::get_offset_of_frameVertices_7(),
	DeformTimeline_t3552075840::get_offset_of_attachment_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (EventTimeline_t2341881094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3588[2] = 
{
	EventTimeline_t2341881094::get_offset_of_frames_0(),
	EventTimeline_t2341881094::get_offset_of_events_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (DrawOrderTimeline_t1092982325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3589[2] = 
{
	DrawOrderTimeline_t1092982325::get_offset_of_frames_0(),
	DrawOrderTimeline_t1092982325::get_offset_of_drawOrders_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (IkConstraintTimeline_t1094182104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3590[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	IkConstraintTimeline_t1094182104::get_offset_of_ikConstraintIndex_11(),
	IkConstraintTimeline_t1094182104::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (TransformConstraintTimeline_t410042030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3591[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TransformConstraintTimeline_t410042030::get_offset_of_transformConstraintIndex_15(),
	TransformConstraintTimeline_t410042030::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (PathConstraintPositionTimeline_t1963796833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[6] = 
{
	0,
	0,
	0,
	0,
	PathConstraintPositionTimeline_t1963796833::get_offset_of_pathConstraintIndex_9(),
	PathConstraintPositionTimeline_t1963796833::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (PathConstraintSpacingTimeline_t1125597164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (PathConstraintMixTimeline_t534890125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3594[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	PathConstraintMixTimeline_t534890125::get_offset_of_pathConstraintIndex_11(),
	PathConstraintMixTimeline_t534890125::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (AnimationState_t3637309382), -1, sizeof(AnimationState_t3637309382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3595[20] = 
{
	AnimationState_t3637309382_StaticFields::get_offset_of_EmptyAnimation_0(),
	0,
	0,
	0,
	0,
	AnimationState_t3637309382::get_offset_of_data_5(),
	AnimationState_t3637309382::get_offset_of_trackEntryPool_6(),
	AnimationState_t3637309382::get_offset_of_tracks_7(),
	AnimationState_t3637309382::get_offset_of_events_8(),
	AnimationState_t3637309382::get_offset_of_queue_9(),
	AnimationState_t3637309382::get_offset_of_propertyIDs_10(),
	AnimationState_t3637309382::get_offset_of_mixingTo_11(),
	AnimationState_t3637309382::get_offset_of_animationsChanged_12(),
	AnimationState_t3637309382::get_offset_of_timeScale_13(),
	AnimationState_t3637309382::get_offset_of_Interrupt_14(),
	AnimationState_t3637309382::get_offset_of_End_15(),
	AnimationState_t3637309382::get_offset_of_Dispose_16(),
	AnimationState_t3637309382::get_offset_of_Complete_17(),
	AnimationState_t3637309382::get_offset_of_Start_18(),
	AnimationState_t3637309382::get_offset_of_Event_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (TrackEntryDelegate_t363257942), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (TrackEntryEventDelegate_t1653995044), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (TrackEntry_t1316488441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[32] = 
{
	TrackEntry_t1316488441::get_offset_of_animation_0(),
	TrackEntry_t1316488441::get_offset_of_next_1(),
	TrackEntry_t1316488441::get_offset_of_mixingFrom_2(),
	TrackEntry_t1316488441::get_offset_of_trackIndex_3(),
	TrackEntry_t1316488441::get_offset_of_loop_4(),
	TrackEntry_t1316488441::get_offset_of_eventThreshold_5(),
	TrackEntry_t1316488441::get_offset_of_attachmentThreshold_6(),
	TrackEntry_t1316488441::get_offset_of_drawOrderThreshold_7(),
	TrackEntry_t1316488441::get_offset_of_animationStart_8(),
	TrackEntry_t1316488441::get_offset_of_animationEnd_9(),
	TrackEntry_t1316488441::get_offset_of_animationLast_10(),
	TrackEntry_t1316488441::get_offset_of_nextAnimationLast_11(),
	TrackEntry_t1316488441::get_offset_of_delay_12(),
	TrackEntry_t1316488441::get_offset_of_trackTime_13(),
	TrackEntry_t1316488441::get_offset_of_trackLast_14(),
	TrackEntry_t1316488441::get_offset_of_nextTrackLast_15(),
	TrackEntry_t1316488441::get_offset_of_trackEnd_16(),
	TrackEntry_t1316488441::get_offset_of_timeScale_17(),
	TrackEntry_t1316488441::get_offset_of_alpha_18(),
	TrackEntry_t1316488441::get_offset_of_mixTime_19(),
	TrackEntry_t1316488441::get_offset_of_mixDuration_20(),
	TrackEntry_t1316488441::get_offset_of_interruptAlpha_21(),
	TrackEntry_t1316488441::get_offset_of_totalAlpha_22(),
	TrackEntry_t1316488441::get_offset_of_timelineData_23(),
	TrackEntry_t1316488441::get_offset_of_timelineDipMix_24(),
	TrackEntry_t1316488441::get_offset_of_timelinesRotation_25(),
	TrackEntry_t1316488441::get_offset_of_Interrupt_26(),
	TrackEntry_t1316488441::get_offset_of_End_27(),
	TrackEntry_t1316488441::get_offset_of_Dispose_28(),
	TrackEntry_t1316488441::get_offset_of_Complete_29(),
	TrackEntry_t1316488441::get_offset_of_Start_30(),
	TrackEntry_t1316488441::get_offset_of_Event_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (EventQueue_t808091267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3599[5] = 
{
	EventQueue_t808091267::get_offset_of_eventQueueEntries_0(),
	EventQueue_t808091267::get_offset_of_drainDisabled_1(),
	EventQueue_t808091267::get_offset_of_state_2(),
	EventQueue_t808091267::get_offset_of_trackEntryPool_3(),
	EventQueue_t808091267::get_offset_of_AnimationsChanged_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
