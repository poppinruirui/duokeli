﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3
struct U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// WebAuthAPI.Authenticator
struct Authenticator_t530988528;
// WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3/<simplePasswordLogin>c__AnonStoreyE
struct U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469;
// WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC
struct U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424;
// WebAuthAPI.SecureEphemeralJSON
struct SecureEphemeralJSON_t940315958;
// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF
struct U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005;
// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10
struct U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410;
// System.Action`2<System.Int32,System.String>
struct Action_2_t3073627706;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UniWebView
struct UniWebView_t941983939;
// UniWebView/ShouldCloseDelegate
struct ShouldCloseDelegate_t766319959;
// WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13
struct U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423;
// System.Action`2<System.Int32,System.Object>
struct Action_2_t11315885;
// WebAuthAPI.ChargeProductJSON[]
struct ChargeProductJSONU5BU5D_t3214290606;
// WebAuthAPI.ChargeProductJSON
struct ChargeProductJSON_t2737157911;
// System.Func`5<System.String,System.String,System.Object,System.Action`2<System.Int32,System.Object>,System.Collections.IEnumerator>
struct Func_5_t696329592;
// WebAuthAPI.SessionFileJSON
struct SessionFileJSON_t380207342;
// WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17
struct U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614;
// System.Action`3<System.Int32,System.String,System.String>
struct Action_3_t3371855306;
// UnityEngine.WWW
struct WWW_t3688466362;
// PolygonEditor
struct PolygonEditor_t2170978208;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.String[]
struct StringU5BU5D_t1281789340;
// Polygon
struct Polygon_t12692255;
// WebAuthAPI.AccountJSON
struct AccountJSON_t3293827731;
// AccountData/SkinJSON[]
struct SkinJSONU5BU5D_t732741759;
// AccountData
struct AccountData_t1002028515;
// AccountData/SkinListJSON
struct SkinListJSON_t1657696259;
// System.Collections.Generic.List`1<Resource>
struct List_1_t1603957611;
// AccountData/<loadLocaleResources>c__Iterator0/<loadLocaleResources>c__AnonStorey2
struct U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912;
// System.EventHandler`1<ProgressEventArgs>
struct EventHandler_1_t4063164883;
// AccountData/<loadLocaleResources>c__Iterator0
struct U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587;
// AccountData/FileSkinListJSON
struct FileSkinListJSON_t1219265033;
// AccountData/<loadRemoteResources>c__Iterator1/<loadRemoteResources>c__AnonStorey3
struct U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736;
// AccountData/<loadRemoteResources>c__Iterator1
struct U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// Ball
struct Ball_t2206666566;
// PhotonView
struct PhotonView_t2207721820;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Font[]
struct FontU5BU5D_t1399044585;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1812449865;
// ImprovedPerlin
struct ImprovedPerlin_t2236177176;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Shader
struct Shader_t4151988712;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3524055750;
// System.Collections.Generic.List`1<SpitBallTarget>
struct List_1_t1174412619;
// System.Collections.Generic.List`1<CMonster>
struct List_1_t3413545680;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.Single>>
struct Dictionary_2_t1758054847;
// System.Collections.Generic.List`1<CBuff>
struct List_1_t3012865878;
// System.Collections.Generic.List`1<CGrass>
struct List_1_t2206159218;
// System.Collections.Generic.List`1<CGunsight>
struct List_1_t3682063150;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// System.Collections.Generic.List`1<CMiaoHeEffect>
struct List_1_t2306155490;
// System.Collections.Generic.List`1<CItemBuyEffect>
struct List_1_t2305279210;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonAnimation>
struct List_1_t870293967;
// CFrameAnimationEffect
struct CFrameAnimationEffect_t443605508;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.UI.Text
struct Text_t1901882714;
// WebAuthAPI.Account
struct Account_t400769301;
// WebAuthAPI.Charge
struct Charge_t1653125646;
// WebAuthAPI.Market
struct Market_t538055513;
// WebAuthAPI.MarketBuymentJSON[]
struct MarketBuymentJSONU5BU5D_t1759546967;
// ResourceLoader
struct ResourceLoader_t3764463624;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.UI.Button
struct Button_t4055032469;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// CCosmosSkeleton
struct CCosmosSkeleton_t2697667825;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2767979955;
// CProgressBar
struct CProgressBar_t881982788;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t1172778254;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// CCyberTreeList
struct CCyberTreeList_t4040119676;
// CWifi
struct CWifi_t3890179523;
// CyberTreeScrollView
struct CyberTreeScrollView_t257211719;
// CCyberTreePoPo
struct CCyberTreePoPo_t261854377;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// PolygonPoint[]
struct PolygonPointU5BU5D_t150589628;
// PolygonPoint
struct PolygonPoint_t4211654561;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t57175488;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// ComoList
struct ComoList_t2152284863;
// MsgBox
struct MsgBox_t1665650521;
// CreateNewShit
struct CreateNewShit_t3983358377;
// System.Collections.Generic.Dictionary`2<System.String,PolygonLine>
struct Dictionary_2_t1680981515;
// System.Collections.Generic.List`1<PolygonLine>
struct List_1_t3367799958;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// System.Collections.Generic.List`1<PolygonPoint>
struct List_1_t1388762007;
// UnityEngine.EdgeCollider2D
struct EdgeCollider2D_t2781859275;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Font
struct Font_t1956802104;
// System.Collections.Generic.List`1<Ball>
struct List_1_t3678741308;
// System.Collections.Generic.Dictionary`2<System.Int32,Ball>
struct Dictionary_2_t1095379897;
// CSkillManager
struct CSkillManager_t2937013327;
// CPlayerIns
struct CPlayerIns_t3208190728;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t285980105;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// CGunsight[]
struct CGunsightU5BU5D_t395838697;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<Player/sSplitInfo>
struct List_1_t478107080;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t4032136720;
// System.Collections.Generic.List`1<CSpore>
struct List_1_t1482975291;
// System.Collections.Generic.Dictionary`2<System.Int32,CCosmosGunSight>
struct Dictionary_2_t2518726632;
// System.Collections.Generic.List`1<CBeanCollection/sBeanSyncInfo>
struct List_1_t2648729287;
// System.Collections.Generic.List`1<CExplodeNode>
struct List_1_t1569919409;
// System.Collections.Generic.List`1<CDust>
struct List_1_t198291272;
// System.Collections.Generic.Dictionary`2<System.Int32,CDust>
struct Dictionary_2_t1909897157;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TURNEXTENSIONS_T3150044944_H
#define TURNEXTENSIONS_T3150044944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnExtensions
struct  TurnExtensions_t3150044944  : public RuntimeObject
{
public:

public:
};

struct TurnExtensions_t3150044944_StaticFields
{
public:
	// System.String TurnExtensions::TurnPropKey
	String_t* ___TurnPropKey_0;
	// System.String TurnExtensions::TurnStartPropKey
	String_t* ___TurnStartPropKey_1;
	// System.String TurnExtensions::FinishedTurnPropKey
	String_t* ___FinishedTurnPropKey_2;

public:
	inline static int32_t get_offset_of_TurnPropKey_0() { return static_cast<int32_t>(offsetof(TurnExtensions_t3150044944_StaticFields, ___TurnPropKey_0)); }
	inline String_t* get_TurnPropKey_0() const { return ___TurnPropKey_0; }
	inline String_t** get_address_of_TurnPropKey_0() { return &___TurnPropKey_0; }
	inline void set_TurnPropKey_0(String_t* value)
	{
		___TurnPropKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___TurnPropKey_0), value);
	}

	inline static int32_t get_offset_of_TurnStartPropKey_1() { return static_cast<int32_t>(offsetof(TurnExtensions_t3150044944_StaticFields, ___TurnStartPropKey_1)); }
	inline String_t* get_TurnStartPropKey_1() const { return ___TurnStartPropKey_1; }
	inline String_t** get_address_of_TurnStartPropKey_1() { return &___TurnStartPropKey_1; }
	inline void set_TurnStartPropKey_1(String_t* value)
	{
		___TurnStartPropKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___TurnStartPropKey_1), value);
	}

	inline static int32_t get_offset_of_FinishedTurnPropKey_2() { return static_cast<int32_t>(offsetof(TurnExtensions_t3150044944_StaticFields, ___FinishedTurnPropKey_2)); }
	inline String_t* get_FinishedTurnPropKey_2() const { return ___FinishedTurnPropKey_2; }
	inline String_t** get_address_of_FinishedTurnPropKey_2() { return &___FinishedTurnPropKey_2; }
	inline void set_FinishedTurnPropKey_2(String_t* value)
	{
		___FinishedTurnPropKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedTurnPropKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNEXTENSIONS_T3150044944_H
#ifndef U3CSIMPLEPASSWORDLOGINU3EC__ANONSTOREYE_T2664070469_H
#define U3CSIMPLEPASSWORDLOGINU3EC__ANONSTOREYE_T2664070469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3/<simplePasswordLogin>c__AnonStoreyE
struct  U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469  : public RuntimeObject
{
public:
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3/<simplePasswordLogin>c__AnonStoreyE::callback
	Action_1_t3123413348 * ___callback_0;
	// WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3 WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3/<simplePasswordLogin>c__AnonStoreyE::<>f__ref$3
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469, ___callback_0)); }
	inline Action_1_t3123413348 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3123413348 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3123413348 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469, ___U3CU3Ef__refU243_1)); }
	inline U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSIMPLEPASSWORDLOGINU3EC__ANONSTOREYE_T2664070469_H
#ifndef U3CSIMPLEPASSWORDLOGINU3EC__ITERATOR3_T1174302285_H
#define U3CSIMPLEPASSWORDLOGINU3EC__ITERATOR3_T1174302285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3
struct  U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::password
	String_t* ___password_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::callback
	Action_1_t3123413348 * ___callback_4;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::$this
	Authenticator_t530988528 * ___U24this_5;
	// System.Object WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::$PC
	int32_t ___U24PC_8;
	// WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3/<simplePasswordLogin>c__AnonStoreyE WebAuthAPI.Authenticator/<simplePasswordLogin>c__Iterator3::$locvar1
	U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469 * ___U24locvar1_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___password_2)); }
	inline String_t* get_password_2() const { return ___password_2; }
	inline String_t** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(String_t* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier((&___password_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___callback_4)); }
	inline Action_1_t3123413348 * get_callback_4() const { return ___callback_4; }
	inline Action_1_t3123413348 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t3123413348 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U24this_5)); }
	inline Authenticator_t530988528 * get_U24this_5() const { return ___U24this_5; }
	inline Authenticator_t530988528 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(Authenticator_t530988528 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar1_9() { return static_cast<int32_t>(offsetof(U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285, ___U24locvar1_9)); }
	inline U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469 * get_U24locvar1_9() const { return ___U24locvar1_9; }
	inline U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469 ** get_address_of_U24locvar1_9() { return &___U24locvar1_9; }
	inline void set_U24locvar1_9(U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469 * value)
	{
		___U24locvar1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSIMPLEPASSWORDLOGINU3EC__ITERATOR3_T1174302285_H
#ifndef U3CSECUREREGISTERU3EC__ANONSTOREYD_T3780315107_H
#define U3CSECUREREGISTERU3EC__ANONSTOREYD_T3780315107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC/<secureRegister>c__AnonStoreyD
struct  U3CsecureRegisterU3Ec__AnonStoreyD_t3780315107  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC/<secureRegister>c__AnonStoreyD::salt
	String_t* ___salt_0;
	// WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC/<secureRegister>c__AnonStoreyD::<>f__ref$12
	U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424 * ___U3CU3Ef__refU2412_1;

public:
	inline static int32_t get_offset_of_salt_0() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyD_t3780315107, ___salt_0)); }
	inline String_t* get_salt_0() const { return ___salt_0; }
	inline String_t** get_address_of_salt_0() { return &___salt_0; }
	inline void set_salt_0(String_t* value)
	{
		___salt_0 = value;
		Il2CppCodeGenWriteBarrier((&___salt_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2412_1() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyD_t3780315107, ___U3CU3Ef__refU2412_1)); }
	inline U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424 * get_U3CU3Ef__refU2412_1() const { return ___U3CU3Ef__refU2412_1; }
	inline U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424 ** get_address_of_U3CU3Ef__refU2412_1() { return &___U3CU3Ef__refU2412_1; }
	inline void set_U3CU3Ef__refU2412_1(U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424 * value)
	{
		___U3CU3Ef__refU2412_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2412_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREREGISTERU3EC__ANONSTOREYD_T3780315107_H
#ifndef U3CSECUREREGISTERU3EC__ANONSTOREYC_T2323385424_H
#define U3CSECUREREGISTERU3EC__ANONSTOREYC_T2323385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC
struct  U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC::password
	String_t* ___password_1;
	// System.String WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC::smscode
	String_t* ___smscode_2;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC::callback
	Action_1_t3123413348 * ___callback_3;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<secureRegister>c__AnonStoreyC::$this
	Authenticator_t530988528 * ___U24this_4;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}

	inline static int32_t get_offset_of_smscode_2() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424, ___smscode_2)); }
	inline String_t* get_smscode_2() const { return ___smscode_2; }
	inline String_t** get_address_of_smscode_2() { return &___smscode_2; }
	inline void set_smscode_2(String_t* value)
	{
		___smscode_2 = value;
		Il2CppCodeGenWriteBarrier((&___smscode_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424, ___callback_3)); }
	inline Action_1_t3123413348 * get_callback_3() const { return ___callback_3; }
	inline Action_1_t3123413348 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_t3123413348 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424, ___U24this_4)); }
	inline Authenticator_t530988528 * get_U24this_4() const { return ___U24this_4; }
	inline Authenticator_t530988528 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Authenticator_t530988528 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREREGISTERU3EC__ANONSTOREYC_T2323385424_H
#ifndef U3CSIMPLEREGISTERU3EC__ITERATOR2_T975479272_H
#define U3CSIMPLEREGISTERU3EC__ITERATOR2_T975479272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2
struct  U3CsimpleRegisterU3Ec__Iterator2_t975479272  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::smscode
	String_t* ___smscode_2;
	// System.String WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::password
	String_t* ___password_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::callback
	Action_1_t3123413348 * ___callback_5;
	// System.Object WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Authenticator/<simpleRegister>c__Iterator2::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_smscode_2() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___smscode_2)); }
	inline String_t* get_smscode_2() const { return ___smscode_2; }
	inline String_t** get_address_of_smscode_2() { return &___smscode_2; }
	inline void set_smscode_2(String_t* value)
	{
		___smscode_2 = value;
		Il2CppCodeGenWriteBarrier((&___smscode_2), value);
	}

	inline static int32_t get_offset_of_password_3() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___password_3)); }
	inline String_t* get_password_3() const { return ___password_3; }
	inline String_t** get_address_of_password_3() { return &___password_3; }
	inline void set_password_3(String_t* value)
	{
		___password_3 = value;
		Il2CppCodeGenWriteBarrier((&___password_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___callback_5)); }
	inline Action_1_t3123413348 * get_callback_5() const { return ___callback_5; }
	inline Action_1_t3123413348 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_t3123413348 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsimpleRegisterU3Ec__Iterator2_t975479272, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSIMPLEREGISTERU3EC__ITERATOR2_T975479272_H
#ifndef U3CSECUREPASSWORDLOGINU3EC__ANONSTOREYF_T657179005_H
#define U3CSECUREPASSWORDLOGINU3EC__ANONSTOREYF_T657179005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF
struct  U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF::password
	String_t* ___password_1;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF::callback
	Action_1_t3123413348 * ___callback_2;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF::$this
	Authenticator_t530988528 * ___U24this_3;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005, ___callback_2)); }
	inline Action_1_t3123413348 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t3123413348 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t3123413348 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005, ___U24this_3)); }
	inline Authenticator_t530988528 * get_U24this_3() const { return ___U24this_3; }
	inline Authenticator_t530988528 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Authenticator_t530988528 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREPASSWORDLOGINU3EC__ANONSTOREYF_T657179005_H
#ifndef U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY10_T788032410_H
#define U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY10_T788032410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10
struct  U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410  : public RuntimeObject
{
public:
	// WebAuthAPI.SecureEphemeralJSON WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10::clientEphemeral
	SecureEphemeralJSON_t940315958 * ___clientEphemeral_0;
	// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10::<>f__ref$15
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * ___U3CU3Ef__refU2415_1;

public:
	inline static int32_t get_offset_of_clientEphemeral_0() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410, ___clientEphemeral_0)); }
	inline SecureEphemeralJSON_t940315958 * get_clientEphemeral_0() const { return ___clientEphemeral_0; }
	inline SecureEphemeralJSON_t940315958 ** get_address_of_clientEphemeral_0() { return &___clientEphemeral_0; }
	inline void set_clientEphemeral_0(SecureEphemeralJSON_t940315958 * value)
	{
		___clientEphemeral_0 = value;
		Il2CppCodeGenWriteBarrier((&___clientEphemeral_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2415_1() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410, ___U3CU3Ef__refU2415_1)); }
	inline U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * get_U3CU3Ef__refU2415_1() const { return ___U3CU3Ef__refU2415_1; }
	inline U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 ** get_address_of_U3CU3Ef__refU2415_1() { return &___U3CU3Ef__refU2415_1; }
	inline void set_U3CU3Ef__refU2415_1(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * value)
	{
		___U3CU3Ef__refU2415_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2415_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY10_T788032410_H
#ifndef U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY11_T1072147897_H
#define U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY11_T1072147897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11
struct  U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11::serverEphemeral
	String_t* ___serverEphemeral_0;
	// System.String WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11::salt
	String_t* ___salt_1;
	// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11::<>f__ref$15
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * ___U3CU3Ef__refU2415_2;
	// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10 WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11::<>f__ref$16
	U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 * ___U3CU3Ef__refU2416_3;

public:
	inline static int32_t get_offset_of_serverEphemeral_0() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897, ___serverEphemeral_0)); }
	inline String_t* get_serverEphemeral_0() const { return ___serverEphemeral_0; }
	inline String_t** get_address_of_serverEphemeral_0() { return &___serverEphemeral_0; }
	inline void set_serverEphemeral_0(String_t* value)
	{
		___serverEphemeral_0 = value;
		Il2CppCodeGenWriteBarrier((&___serverEphemeral_0), value);
	}

	inline static int32_t get_offset_of_salt_1() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897, ___salt_1)); }
	inline String_t* get_salt_1() const { return ___salt_1; }
	inline String_t** get_address_of_salt_1() { return &___salt_1; }
	inline void set_salt_1(String_t* value)
	{
		___salt_1 = value;
		Il2CppCodeGenWriteBarrier((&___salt_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2415_2() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897, ___U3CU3Ef__refU2415_2)); }
	inline U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * get_U3CU3Ef__refU2415_2() const { return ___U3CU3Ef__refU2415_2; }
	inline U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 ** get_address_of_U3CU3Ef__refU2415_2() { return &___U3CU3Ef__refU2415_2; }
	inline void set_U3CU3Ef__refU2415_2(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * value)
	{
		___U3CU3Ef__refU2415_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2415_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2416_3() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897, ___U3CU3Ef__refU2416_3)); }
	inline U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 * get_U3CU3Ef__refU2416_3() const { return ___U3CU3Ef__refU2416_3; }
	inline U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 ** get_address_of_U3CU3Ef__refU2416_3() { return &___U3CU3Ef__refU2416_3; }
	inline void set_U3CU3Ef__refU2416_3(U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 * value)
	{
		___U3CU3Ef__refU2416_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2416_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY11_T1072147897_H
#ifndef U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY12_T3603917365_H
#define U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY12_T3603917365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11/<securePasswordLogin>c__AnonStorey12
struct  U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11/<securePasswordLogin>c__AnonStorey12::sessionKey
	String_t* ___sessionKey_0;
	// System.String WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11/<securePasswordLogin>c__AnonStorey12::clientProof
	String_t* ___clientProof_1;
	// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11/<securePasswordLogin>c__AnonStorey12::<>f__ref$15
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * ___U3CU3Ef__refU2415_2;
	// WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10 WebAuthAPI.Authenticator/<securePasswordLogin>c__AnonStoreyF/<securePasswordLogin>c__AnonStorey10/<securePasswordLogin>c__AnonStorey11/<securePasswordLogin>c__AnonStorey12::<>f__ref$16
	U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 * ___U3CU3Ef__refU2416_3;

public:
	inline static int32_t get_offset_of_sessionKey_0() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365, ___sessionKey_0)); }
	inline String_t* get_sessionKey_0() const { return ___sessionKey_0; }
	inline String_t** get_address_of_sessionKey_0() { return &___sessionKey_0; }
	inline void set_sessionKey_0(String_t* value)
	{
		___sessionKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionKey_0), value);
	}

	inline static int32_t get_offset_of_clientProof_1() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365, ___clientProof_1)); }
	inline String_t* get_clientProof_1() const { return ___clientProof_1; }
	inline String_t** get_address_of_clientProof_1() { return &___clientProof_1; }
	inline void set_clientProof_1(String_t* value)
	{
		___clientProof_1 = value;
		Il2CppCodeGenWriteBarrier((&___clientProof_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2415_2() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365, ___U3CU3Ef__refU2415_2)); }
	inline U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * get_U3CU3Ef__refU2415_2() const { return ___U3CU3Ef__refU2415_2; }
	inline U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 ** get_address_of_U3CU3Ef__refU2415_2() { return &___U3CU3Ef__refU2415_2; }
	inline void set_U3CU3Ef__refU2415_2(U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005 * value)
	{
		___U3CU3Ef__refU2415_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2415_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2416_3() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365, ___U3CU3Ef__refU2416_3)); }
	inline U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 * get_U3CU3Ef__refU2416_3() const { return ___U3CU3Ef__refU2416_3; }
	inline U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 ** get_address_of_U3CU3Ef__refU2416_3() { return &___U3CU3Ef__refU2416_3; }
	inline void set_U3CU3Ef__refU2416_3(U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410 * value)
	{
		___U3CU3Ef__refU2416_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2416_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREPASSWORDLOGINU3EC__ANONSTOREY12_T3603917365_H
#ifndef U3CSIMPLERESETPASSWORDU3EC__ITERATOR4_T1182085331_H
#define U3CSIMPLERESETPASSWORDU3EC__ITERATOR4_T1182085331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4
struct  U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::smscode
	String_t* ___smscode_2;
	// System.String WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::password
	String_t* ___password_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::callback
	Action_1_t3123413348 * ___callback_5;
	// System.Object WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Authenticator/<simpleResetPassword>c__Iterator4::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_smscode_2() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___smscode_2)); }
	inline String_t* get_smscode_2() const { return ___smscode_2; }
	inline String_t** get_address_of_smscode_2() { return &___smscode_2; }
	inline void set_smscode_2(String_t* value)
	{
		___smscode_2 = value;
		Il2CppCodeGenWriteBarrier((&___smscode_2), value);
	}

	inline static int32_t get_offset_of_password_3() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___password_3)); }
	inline String_t* get_password_3() const { return ___password_3; }
	inline String_t** get_address_of_password_3() { return &___password_3; }
	inline void set_password_3(String_t* value)
	{
		___password_3 = value;
		Il2CppCodeGenWriteBarrier((&___password_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___callback_5)); }
	inline Action_1_t3123413348 * get_callback_5() const { return ___callback_5; }
	inline Action_1_t3123413348 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_t3123413348 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSIMPLERESETPASSWORDU3EC__ITERATOR4_T1182085331_H
#ifndef U3CSECURERESETPASSWORDU3EC__ANONSTOREY13_T3950057423_H
#define U3CSECURERESETPASSWORDU3EC__ANONSTOREY13_T3950057423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13
struct  U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13::password
	String_t* ___password_1;
	// System.String WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13::smscode
	String_t* ___smscode_2;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13::callback
	Action_1_t3123413348 * ___callback_3;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13::$this
	Authenticator_t530988528 * ___U24this_4;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}

	inline static int32_t get_offset_of_smscode_2() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423, ___smscode_2)); }
	inline String_t* get_smscode_2() const { return ___smscode_2; }
	inline String_t** get_address_of_smscode_2() { return &___smscode_2; }
	inline void set_smscode_2(String_t* value)
	{
		___smscode_2 = value;
		Il2CppCodeGenWriteBarrier((&___smscode_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423, ___callback_3)); }
	inline Action_1_t3123413348 * get_callback_3() const { return ___callback_3; }
	inline Action_1_t3123413348 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_t3123413348 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423, ___U24this_4)); }
	inline Authenticator_t530988528 * get_U24this_4() const { return ___U24this_4; }
	inline Authenticator_t530988528 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Authenticator_t530988528 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECURERESETPASSWORDU3EC__ANONSTOREY13_T3950057423_H
#ifndef U3CGETAUTHENTICATORTYPEU3EC__ITERATOR1_T284942605_H
#define U3CGETAUTHENTICATORTYPEU3EC__ITERATOR1_T284942605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<getAuthenticatorType>c__Iterator1
struct  U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<getAuthenticatorType>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_0;
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<getAuthenticatorType>c__Iterator1::callback
	Action_2_t3073627706 * ___callback_1;
	// System.Object WebAuthAPI.Authenticator/<getAuthenticatorType>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean WebAuthAPI.Authenticator/<getAuthenticatorType>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 WebAuthAPI.Authenticator/<getAuthenticatorType>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605, ___callback_1)); }
	inline Action_2_t3073627706 * get_callback_1() const { return ___callback_1; }
	inline Action_2_t3073627706 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_2_t3073627706 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETAUTHENTICATORTYPEU3EC__ITERATOR1_T284942605_H
#ifndef SECUREEPHEMERALJSON_T940315958_H
#define SECUREEPHEMERALJSON_T940315958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecureEphemeralJSON
struct  SecureEphemeralJSON_t940315958  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.SecureEphemeralJSON::ephemeral
	String_t* ___ephemeral_0;
	// System.String WebAuthAPI.SecureEphemeralJSON::secret
	String_t* ___secret_1;

public:
	inline static int32_t get_offset_of_ephemeral_0() { return static_cast<int32_t>(offsetof(SecureEphemeralJSON_t940315958, ___ephemeral_0)); }
	inline String_t* get_ephemeral_0() const { return ___ephemeral_0; }
	inline String_t** get_address_of_ephemeral_0() { return &___ephemeral_0; }
	inline void set_ephemeral_0(String_t* value)
	{
		___ephemeral_0 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeral_0), value);
	}

	inline static int32_t get_offset_of_secret_1() { return static_cast<int32_t>(offsetof(SecureEphemeralJSON_t940315958, ___secret_1)); }
	inline String_t* get_secret_1() const { return ___secret_1; }
	inline String_t** get_address_of_secret_1() { return &___secret_1; }
	inline void set_secret_1(String_t* value)
	{
		___secret_1 = value;
		Il2CppCodeGenWriteBarrier((&___secret_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECUREEPHEMERALJSON_T940315958_H
#ifndef SECURESESSIONJSON_T884481414_H
#define SECURESESSIONJSON_T884481414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecureSessionJSON
struct  SecureSessionJSON_t884481414  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.SecureSessionJSON::key
	String_t* ___key_0;
	// System.String WebAuthAPI.SecureSessionJSON::proof
	String_t* ___proof_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(SecureSessionJSON_t884481414, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_proof_1() { return static_cast<int32_t>(offsetof(SecureSessionJSON_t884481414, ___proof_1)); }
	inline String_t* get_proof_1() const { return ___proof_1; }
	inline String_t** get_address_of_proof_1() { return &___proof_1; }
	inline void set_proof_1(String_t* value)
	{
		___proof_1 = value;
		Il2CppCodeGenWriteBarrier((&___proof_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURESESSIONJSON_T884481414_H
#ifndef SECUREVERIFYSESSIONJSON_T2163129613_H
#define SECUREVERIFYSESSIONJSON_T2163129613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecureVerifySessionJSON
struct  SecureVerifySessionJSON_t2163129613  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.SecureVerifySessionJSON::error
	String_t* ___error_0;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(SecureVerifySessionJSON_t2163129613, ___error_0)); }
	inline String_t* get_error_0() const { return ___error_0; }
	inline String_t** get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(String_t* value)
	{
		___error_0 = value;
		Il2CppCodeGenWriteBarrier((&___error_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECUREVERIFYSESSIONJSON_T2163129613_H
#ifndef SIMPLERESETPASSWORDJSON_T489914809_H
#define SIMPLERESETPASSWORDJSON_T489914809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SimpleResetPasswordJSON
struct  SimpleResetPasswordJSON_t489914809  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SimpleResetPasswordJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SimpleResetPasswordJSON_t489914809, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLERESETPASSWORDJSON_T489914809_H
#ifndef SECURERESETPASSWORDJSON_T404722052_H
#define SECURERESETPASSWORDJSON_T404722052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecureResetPasswordJSON
struct  SecureResetPasswordJSON_t404722052  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SecureResetPasswordJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SecureResetPasswordJSON_t404722052, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURERESETPASSWORDJSON_T404722052_H
#ifndef SECUREGETOTPCOUNTERJSON_T3583328016_H
#define SECUREGETOTPCOUNTERJSON_T3583328016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecureGetOTPCounterJSON
struct  SecureGetOTPCounterJSON_t3583328016  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SecureGetOTPCounterJSON::code
	int32_t ___code_0;
	// System.String WebAuthAPI.SecureGetOTPCounterJSON::otpcounter
	String_t* ___otpcounter_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SecureGetOTPCounterJSON_t3583328016, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_otpcounter_1() { return static_cast<int32_t>(offsetof(SecureGetOTPCounterJSON_t3583328016, ___otpcounter_1)); }
	inline String_t* get_otpcounter_1() const { return ___otpcounter_1; }
	inline String_t** get_address_of_otpcounter_1() { return &___otpcounter_1; }
	inline void set_otpcounter_1(String_t* value)
	{
		___otpcounter_1 = value;
		Il2CppCodeGenWriteBarrier((&___otpcounter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECUREGETOTPCOUNTERJSON_T3583328016_H
#ifndef SESSIONVERIFYJSON_T2562930706_H
#define SESSIONVERIFYJSON_T2562930706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SessionVerifyJSON
struct  SessionVerifyJSON_t2562930706  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SessionVerifyJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SessionVerifyJSON_t2562930706, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONVERIFYJSON_T2562930706_H
#ifndef SESSIONFILEJSON_T380207342_H
#define SESSIONFILEJSON_T380207342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SessionFileJSON
struct  SessionFileJSON_t380207342  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.SessionFileJSON::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.SessionFileJSON::sessionKey
	String_t* ___sessionKey_1;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(SessionFileJSON_t380207342, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_sessionKey_1() { return static_cast<int32_t>(offsetof(SessionFileJSON_t380207342, ___sessionKey_1)); }
	inline String_t* get_sessionKey_1() const { return ___sessionKey_1; }
	inline String_t** get_address_of_sessionKey_1() { return &___sessionKey_1; }
	inline void set_sessionKey_1(String_t* value)
	{
		___sessionKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___sessionKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONFILEJSON_T380207342_H
#ifndef AUTHENTICATOR_T530988528_H
#define AUTHENTICATOR_T530988528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator
struct  Authenticator_t530988528  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour WebAuthAPI.Authenticator::context
	MonoBehaviour_t3962482529 * ___context_18;
	// UniWebView WebAuthAPI.Authenticator::webView
	UniWebView_t941983939 * ___webView_19;
	// System.String WebAuthAPI.Authenticator::authenticatorType
	String_t* ___authenticatorType_20;
	// System.Boolean WebAuthAPI.Authenticator::webViewPageLoaded
	bool ___webViewPageLoaded_21;

public:
	inline static int32_t get_offset_of_context_18() { return static_cast<int32_t>(offsetof(Authenticator_t530988528, ___context_18)); }
	inline MonoBehaviour_t3962482529 * get_context_18() const { return ___context_18; }
	inline MonoBehaviour_t3962482529 ** get_address_of_context_18() { return &___context_18; }
	inline void set_context_18(MonoBehaviour_t3962482529 * value)
	{
		___context_18 = value;
		Il2CppCodeGenWriteBarrier((&___context_18), value);
	}

	inline static int32_t get_offset_of_webView_19() { return static_cast<int32_t>(offsetof(Authenticator_t530988528, ___webView_19)); }
	inline UniWebView_t941983939 * get_webView_19() const { return ___webView_19; }
	inline UniWebView_t941983939 ** get_address_of_webView_19() { return &___webView_19; }
	inline void set_webView_19(UniWebView_t941983939 * value)
	{
		___webView_19 = value;
		Il2CppCodeGenWriteBarrier((&___webView_19), value);
	}

	inline static int32_t get_offset_of_authenticatorType_20() { return static_cast<int32_t>(offsetof(Authenticator_t530988528, ___authenticatorType_20)); }
	inline String_t* get_authenticatorType_20() const { return ___authenticatorType_20; }
	inline String_t** get_address_of_authenticatorType_20() { return &___authenticatorType_20; }
	inline void set_authenticatorType_20(String_t* value)
	{
		___authenticatorType_20 = value;
		Il2CppCodeGenWriteBarrier((&___authenticatorType_20), value);
	}

	inline static int32_t get_offset_of_webViewPageLoaded_21() { return static_cast<int32_t>(offsetof(Authenticator_t530988528, ___webViewPageLoaded_21)); }
	inline bool get_webViewPageLoaded_21() const { return ___webViewPageLoaded_21; }
	inline bool* get_address_of_webViewPageLoaded_21() { return &___webViewPageLoaded_21; }
	inline void set_webViewPageLoaded_21(bool value)
	{
		___webViewPageLoaded_21 = value;
	}
};

struct Authenticator_t530988528_StaticFields
{
public:
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator::<>f__am$cache0
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache0_22;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator::<>f__am$cache1
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache1_23;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator::<>f__am$cache2
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache2_24;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(Authenticator_t530988528_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_23() { return static_cast<int32_t>(offsetof(Authenticator_t530988528_StaticFields, ___U3CU3Ef__amU24cache1_23)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache1_23() const { return ___U3CU3Ef__amU24cache1_23; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache1_23() { return &___U3CU3Ef__amU24cache1_23; }
	inline void set_U3CU3Ef__amU24cache1_23(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_24() { return static_cast<int32_t>(offsetof(Authenticator_t530988528_StaticFields, ___U3CU3Ef__amU24cache2_24)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache2_24() const { return ___U3CU3Ef__amU24cache2_24; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache2_24() { return &___U3CU3Ef__amU24cache2_24; }
	inline void set_U3CU3Ef__amU24cache2_24(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache2_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATOR_T530988528_H
#ifndef U3CINITU3EC__ANONSTOREYB_T1325999526_H
#define U3CINITU3EC__ANONSTOREYB_T1325999526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<init>c__AnonStoreyB
struct  U3CinitU3Ec__AnonStoreyB_t1325999526  : public RuntimeObject
{
public:
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<init>c__AnonStoreyB::callback
	Action_1_t3123413348 * ___callback_0;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<init>c__AnonStoreyB::$this
	Authenticator_t530988528 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CinitU3Ec__AnonStoreyB_t1325999526, ___callback_0)); }
	inline Action_1_t3123413348 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3123413348 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3123413348 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CinitU3Ec__AnonStoreyB_t1325999526, ___U24this_1)); }
	inline Authenticator_t530988528 * get_U24this_1() const { return ___U24this_1; }
	inline Authenticator_t530988528 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Authenticator_t530988528 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

struct U3CinitU3Ec__AnonStoreyB_t1325999526_StaticFields
{
public:
	// UniWebView/ShouldCloseDelegate WebAuthAPI.Authenticator/<init>c__AnonStoreyB::<>f__am$cache0
	ShouldCloseDelegate_t766319959 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(U3CinitU3Ec__AnonStoreyB_t1325999526_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline ShouldCloseDelegate_t766319959 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline ShouldCloseDelegate_t766319959 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(ShouldCloseDelegate_t766319959 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITU3EC__ANONSTOREYB_T1325999526_H
#ifndef U3CGETSMSCODEU3EC__ITERATOR0_T1038232676_H
#define U3CGETSMSCODEU3EC__ITERATOR0_T1038232676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0
struct  U3CgetSMSCodeU3Ec__Iterator0_t1038232676  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::smstype
	String_t* ___smstype_1;
	// System.String WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::callback
	Action_1_t3123413348 * ___callback_4;
	// System.Object WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Authenticator/<getSMSCode>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_smstype_1() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___smstype_1)); }
	inline String_t* get_smstype_1() const { return ___smstype_1; }
	inline String_t** get_address_of_smstype_1() { return &___smstype_1; }
	inline void set_smstype_1(String_t* value)
	{
		___smstype_1 = value;
		Il2CppCodeGenWriteBarrier((&___smstype_1), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_2() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___U3CurlU3E__0_2)); }
	inline String_t* get_U3CurlU3E__0_2() const { return ___U3CurlU3E__0_2; }
	inline String_t** get_address_of_U3CurlU3E__0_2() { return &___U3CurlU3E__0_2; }
	inline void set_U3CurlU3E__0_2(String_t* value)
	{
		___U3CurlU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___callback_4)); }
	inline Action_1_t3123413348 * get_callback_4() const { return ___callback_4; }
	inline Action_1_t3123413348 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t3123413348 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CgetSMSCodeU3Ec__Iterator0_t1038232676, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSMSCODEU3EC__ITERATOR0_T1038232676_H
#ifndef U3CSECURERESETPASSWORDU3EC__ANONSTOREY14_T847051668_H
#define U3CSECURERESETPASSWORDU3EC__ANONSTOREY14_T847051668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13/<secureResetPassword>c__AnonStorey14
struct  U3CsecureResetPasswordU3Ec__AnonStorey14_t847051668  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13/<secureResetPassword>c__AnonStorey14::salt
	String_t* ___salt_0;
	// WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13 WebAuthAPI.Authenticator/<secureResetPassword>c__AnonStorey13/<secureResetPassword>c__AnonStorey14::<>f__ref$19
	U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423 * ___U3CU3Ef__refU2419_1;

public:
	inline static int32_t get_offset_of_salt_0() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey14_t847051668, ___salt_0)); }
	inline String_t* get_salt_0() const { return ___salt_0; }
	inline String_t** get_address_of_salt_0() { return &___salt_0; }
	inline void set_salt_0(String_t* value)
	{
		___salt_0 = value;
		Il2CppCodeGenWriteBarrier((&___salt_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2419_1() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordU3Ec__AnonStorey14_t847051668, ___U3CU3Ef__refU2419_1)); }
	inline U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423 * get_U3CU3Ef__refU2419_1() const { return ___U3CU3Ef__refU2419_1; }
	inline U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423 ** get_address_of_U3CU3Ef__refU2419_1() { return &___U3CU3Ef__refU2419_1; }
	inline void set_U3CU3Ef__refU2419_1(U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423 * value)
	{
		___U3CU3Ef__refU2419_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2419_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECURERESETPASSWORDU3EC__ANONSTOREY14_T847051668_H
#ifndef U3CSECUREVERIFYSESSIONU3EC__ANONSTOREY1D_T3772245982_H
#define U3CSECUREVERIFYSESSIONU3EC__ANONSTOREY1D_T3772245982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureVerifySession>c__AnonStorey1D
struct  U3CsecureVerifySessionU3Ec__AnonStorey1D_t3772245982  : public RuntimeObject
{
public:
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<secureVerifySession>c__AnonStorey1D::callback
	Action_1_t3123413348 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureVerifySessionU3Ec__AnonStorey1D_t3772245982, ___callback_0)); }
	inline Action_1_t3123413348 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3123413348 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3123413348 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREVERIFYSESSIONU3EC__ANONSTOREY1D_T3772245982_H
#ifndef U3CSECURERESETPASSWORDPOSTFORMU3EC__ITERATOR9_T3637492214_H
#define U3CSECURERESETPASSWORDPOSTFORMU3EC__ITERATOR9_T3637492214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9
struct  U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::smscode
	String_t* ___smscode_2;
	// System.String WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::salt
	String_t* ___salt_3;
	// System.String WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::verifier
	String_t* ___verifier_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::callback
	Action_1_t3123413348 * ___callback_6;
	// System.Object WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Authenticator/<secureResetPasswordPostForm>c__Iterator9::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_smscode_2() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___smscode_2)); }
	inline String_t* get_smscode_2() const { return ___smscode_2; }
	inline String_t** get_address_of_smscode_2() { return &___smscode_2; }
	inline void set_smscode_2(String_t* value)
	{
		___smscode_2 = value;
		Il2CppCodeGenWriteBarrier((&___smscode_2), value);
	}

	inline static int32_t get_offset_of_salt_3() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___salt_3)); }
	inline String_t* get_salt_3() const { return ___salt_3; }
	inline String_t** get_address_of_salt_3() { return &___salt_3; }
	inline void set_salt_3(String_t* value)
	{
		___salt_3 = value;
		Il2CppCodeGenWriteBarrier((&___salt_3), value);
	}

	inline static int32_t get_offset_of_verifier_4() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___verifier_4)); }
	inline String_t* get_verifier_4() const { return ___verifier_4; }
	inline String_t** get_address_of_verifier_4() { return &___verifier_4; }
	inline void set_verifier_4(String_t* value)
	{
		___verifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___verifier_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___callback_6)); }
	inline Action_1_t3123413348 * get_callback_6() const { return ___callback_6; }
	inline Action_1_t3123413348 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_1_t3123413348 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECURERESETPASSWORDPOSTFORMU3EC__ITERATOR9_T3637492214_H
#ifndef U3CSECUREGENERATEOTPTOKENU3EC__ANONSTOREY1E_T3576112639_H
#define U3CSECUREGENERATEOTPTOKENU3EC__ANONSTOREY1E_T3576112639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureGenerateOTPToken>c__AnonStorey1E
struct  U3CsecureGenerateOTPTokenU3Ec__AnonStorey1E_t3576112639  : public RuntimeObject
{
public:
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<secureGenerateOTPToken>c__AnonStorey1E::callback
	Action_2_t3073627706 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureGenerateOTPTokenU3Ec__AnonStorey1E_t3576112639, ___callback_0)); }
	inline Action_2_t3073627706 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3073627706 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3073627706 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREGENERATEOTPTOKENU3EC__ANONSTOREY1E_T3576112639_H
#ifndef U3CSESSIONVERIFYPOSTFORMU3EC__ITERATORA_T3925493049_H
#define U3CSESSIONVERIFYPOSTFORMU3EC__ITERATORA_T3925493049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA
struct  U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::idortoken
	String_t* ___idortoken_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::callback
	Action_2_t11315885 * ___callback_4;
	// System.Object WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Authenticator/<sessionVerifyPostForm>c__IteratorA::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___callback_4)); }
	inline Action_2_t11315885 * get_callback_4() const { return ___callback_4; }
	inline Action_2_t11315885 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_2_t11315885 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSESSIONVERIFYPOSTFORMU3EC__ITERATORA_T3925493049_H
#ifndef CHARGEPRODUCTJSON_T2737157911_H
#define CHARGEPRODUCTJSON_T2737157911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ChargeProductJSON
struct  ChargeProductJSON_t2737157911  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.ChargeProductJSON::productguid
	String_t* ___productguid_0;
	// System.String WebAuthAPI.ChargeProductJSON::productname
	String_t* ___productname_1;
	// System.String WebAuthAPI.ChargeProductJSON::productdesc
	String_t* ___productdesc_2;
	// System.UInt32 WebAuthAPI.ChargeProductJSON::earncoins
	uint32_t ___earncoins_3;
	// System.UInt32 WebAuthAPI.ChargeProductJSON::earndiamonds
	uint32_t ___earndiamonds_4;

public:
	inline static int32_t get_offset_of_productguid_0() { return static_cast<int32_t>(offsetof(ChargeProductJSON_t2737157911, ___productguid_0)); }
	inline String_t* get_productguid_0() const { return ___productguid_0; }
	inline String_t** get_address_of_productguid_0() { return &___productguid_0; }
	inline void set_productguid_0(String_t* value)
	{
		___productguid_0 = value;
		Il2CppCodeGenWriteBarrier((&___productguid_0), value);
	}

	inline static int32_t get_offset_of_productname_1() { return static_cast<int32_t>(offsetof(ChargeProductJSON_t2737157911, ___productname_1)); }
	inline String_t* get_productname_1() const { return ___productname_1; }
	inline String_t** get_address_of_productname_1() { return &___productname_1; }
	inline void set_productname_1(String_t* value)
	{
		___productname_1 = value;
		Il2CppCodeGenWriteBarrier((&___productname_1), value);
	}

	inline static int32_t get_offset_of_productdesc_2() { return static_cast<int32_t>(offsetof(ChargeProductJSON_t2737157911, ___productdesc_2)); }
	inline String_t* get_productdesc_2() const { return ___productdesc_2; }
	inline String_t** get_address_of_productdesc_2() { return &___productdesc_2; }
	inline void set_productdesc_2(String_t* value)
	{
		___productdesc_2 = value;
		Il2CppCodeGenWriteBarrier((&___productdesc_2), value);
	}

	inline static int32_t get_offset_of_earncoins_3() { return static_cast<int32_t>(offsetof(ChargeProductJSON_t2737157911, ___earncoins_3)); }
	inline uint32_t get_earncoins_3() const { return ___earncoins_3; }
	inline uint32_t* get_address_of_earncoins_3() { return &___earncoins_3; }
	inline void set_earncoins_3(uint32_t value)
	{
		___earncoins_3 = value;
	}

	inline static int32_t get_offset_of_earndiamonds_4() { return static_cast<int32_t>(offsetof(ChargeProductJSON_t2737157911, ___earndiamonds_4)); }
	inline uint32_t get_earndiamonds_4() const { return ___earndiamonds_4; }
	inline uint32_t* get_address_of_earndiamonds_4() { return &___earndiamonds_4; }
	inline void set_earndiamonds_4(uint32_t value)
	{
		___earndiamonds_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGEPRODUCTJSON_T2737157911_H
#ifndef GETCHARGELISTJSON_T3828529878_H
#define GETCHARGELISTJSON_T3828529878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.GetChargeListJSON
struct  GetChargeListJSON_t3828529878  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.GetChargeListJSON::code
	int32_t ___code_0;
	// WebAuthAPI.ChargeProductJSON[] WebAuthAPI.GetChargeListJSON::products
	ChargeProductJSONU5BU5D_t3214290606* ___products_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(GetChargeListJSON_t3828529878, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_products_1() { return static_cast<int32_t>(offsetof(GetChargeListJSON_t3828529878, ___products_1)); }
	inline ChargeProductJSONU5BU5D_t3214290606* get_products_1() const { return ___products_1; }
	inline ChargeProductJSONU5BU5D_t3214290606** get_address_of_products_1() { return &___products_1; }
	inline void set_products_1(ChargeProductJSONU5BU5D_t3214290606* value)
	{
		___products_1 = value;
		Il2CppCodeGenWriteBarrier((&___products_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCHARGELISTJSON_T3828529878_H
#ifndef CHARGEGETJSON_T1239220116_H
#define CHARGEGETJSON_T1239220116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ChargeGetJSON
struct  ChargeGetJSON_t1239220116  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.ChargeGetJSON::code
	int32_t ___code_0;
	// WebAuthAPI.ChargeProductJSON WebAuthAPI.ChargeGetJSON::product
	ChargeProductJSON_t2737157911 * ___product_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(ChargeGetJSON_t1239220116, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_product_1() { return static_cast<int32_t>(offsetof(ChargeGetJSON_t1239220116, ___product_1)); }
	inline ChargeProductJSON_t2737157911 * get_product_1() const { return ___product_1; }
	inline ChargeProductJSON_t2737157911 ** get_address_of_product_1() { return &___product_1; }
	inline void set_product_1(ChargeProductJSON_t2737157911 * value)
	{
		___product_1 = value;
		Il2CppCodeGenWriteBarrier((&___product_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGEGETJSON_T1239220116_H
#ifndef CHARGEADDJSON_T3390709337_H
#define CHARGEADDJSON_T3390709337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ChargeAddJSON
struct  ChargeAddJSON_t3390709337  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.ChargeAddJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(ChargeAddJSON_t3390709337, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGEADDJSON_T3390709337_H
#ifndef CHARGEDELJSON_T545259412_H
#define CHARGEDELJSON_T545259412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ChargeDelJSON
struct  ChargeDelJSON_t545259412  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.ChargeDelJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(ChargeDelJSON_t545259412, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGEDELJSON_T545259412_H
#ifndef CHARGEINFOJSON_T2136936477_H
#define CHARGEINFOJSON_T2136936477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ChargeInfoJSON
struct  ChargeInfoJSON_t2136936477  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.ChargeInfoJSON::productguid
	String_t* ___productguid_0;
	// System.UInt32 WebAuthAPI.ChargeInfoJSON::productnum
	uint32_t ___productnum_1;

public:
	inline static int32_t get_offset_of_productguid_0() { return static_cast<int32_t>(offsetof(ChargeInfoJSON_t2136936477, ___productguid_0)); }
	inline String_t* get_productguid_0() const { return ___productguid_0; }
	inline String_t** get_address_of_productguid_0() { return &___productguid_0; }
	inline void set_productguid_0(String_t* value)
	{
		___productguid_0 = value;
		Il2CppCodeGenWriteBarrier((&___productguid_0), value);
	}

	inline static int32_t get_offset_of_productnum_1() { return static_cast<int32_t>(offsetof(ChargeInfoJSON_t2136936477, ___productnum_1)); }
	inline uint32_t get_productnum_1() const { return ___productnum_1; }
	inline uint32_t* get_address_of_productnum_1() { return &___productnum_1; }
	inline void set_productnum_1(uint32_t value)
	{
		___productnum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGEINFOJSON_T2136936477_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSECUREPASSWORDLOGINVERIFYPOSTFORMU3EC__ITERATOR8_T2915460501_H
#define U3CSECUREPASSWORDLOGINVERIFYPOSTFORMU3EC__ITERATOR8_T2915460501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8
struct  U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::clientProof
	String_t* ___clientProof_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::callback
	Action_2_t3073627706 * ___callback_4;
	// System.Object WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Authenticator/<securePasswordLoginVerifyPostForm>c__Iterator8::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_clientProof_2() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___clientProof_2)); }
	inline String_t* get_clientProof_2() const { return ___clientProof_2; }
	inline String_t** get_address_of_clientProof_2() { return &___clientProof_2; }
	inline void set_clientProof_2(String_t* value)
	{
		___clientProof_2 = value;
		Il2CppCodeGenWriteBarrier((&___clientProof_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___callback_4)); }
	inline Action_2_t3073627706 * get_callback_4() const { return ___callback_4; }
	inline Action_2_t3073627706 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_2_t3073627706 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREPASSWORDLOGINVERIFYPOSTFORMU3EC__ITERATOR8_T2915460501_H
#ifndef U3CSIMPLESESSIONVERIFYU3EC__ANONSTOREY15_T2684843539_H
#define U3CSIMPLESESSIONVERIFYU3EC__ANONSTOREY15_T2684843539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<simpleSessionVerify>c__AnonStorey15
struct  U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539  : public RuntimeObject
{
public:
	// System.Func`5<System.String,System.String,System.Object,System.Action`2<System.Int32,System.Object>,System.Collections.IEnumerator> WebAuthAPI.Authenticator/<simpleSessionVerify>c__AnonStorey15::coroutine
	Func_5_t696329592 * ___coroutine_0;
	// System.Object WebAuthAPI.Authenticator/<simpleSessionVerify>c__AnonStorey15::passby
	RuntimeObject * ___passby_1;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Authenticator/<simpleSessionVerify>c__AnonStorey15::callback
	Action_2_t11315885 * ___callback_2;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<simpleSessionVerify>c__AnonStorey15::$this
	Authenticator_t530988528 * ___U24this_3;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539, ___coroutine_0)); }
	inline Func_5_t696329592 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_5_t696329592 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_5_t696329592 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_passby_1() { return static_cast<int32_t>(offsetof(U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539, ___passby_1)); }
	inline RuntimeObject * get_passby_1() const { return ___passby_1; }
	inline RuntimeObject ** get_address_of_passby_1() { return &___passby_1; }
	inline void set_passby_1(RuntimeObject * value)
	{
		___passby_1 = value;
		Il2CppCodeGenWriteBarrier((&___passby_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539, ___callback_2)); }
	inline Action_2_t11315885 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t11315885 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t11315885 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539, ___U24this_3)); }
	inline Authenticator_t530988528 * get_U24this_3() const { return ___U24this_3; }
	inline Authenticator_t530988528 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Authenticator_t530988528 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSIMPLESESSIONVERIFYU3EC__ANONSTOREY15_T2684843539_H
#ifndef U3CSECURESESSIONVERIFYU3EC__ANONSTOREY17_T2063931614_H
#define U3CSECURESESSIONVERIFYU3EC__ANONSTOREY17_T2063931614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17
struct  U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614  : public RuntimeObject
{
public:
	// System.Func`5<System.String,System.String,System.Object,System.Action`2<System.Int32,System.Object>,System.Collections.IEnumerator> WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17::coroutine
	Func_5_t696329592 * ___coroutine_0;
	// System.Object WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17::passby
	RuntimeObject * ___passby_1;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17::callback
	Action_2_t11315885 * ___callback_2;
	// WebAuthAPI.Authenticator WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17::$this
	Authenticator_t530988528 * ___U24this_3;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614, ___coroutine_0)); }
	inline Func_5_t696329592 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_5_t696329592 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_5_t696329592 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_passby_1() { return static_cast<int32_t>(offsetof(U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614, ___passby_1)); }
	inline RuntimeObject * get_passby_1() const { return ___passby_1; }
	inline RuntimeObject ** get_address_of_passby_1() { return &___passby_1; }
	inline void set_passby_1(RuntimeObject * value)
	{
		___passby_1 = value;
		Il2CppCodeGenWriteBarrier((&___passby_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614, ___callback_2)); }
	inline Action_2_t11315885 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t11315885 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t11315885 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614, ___U24this_3)); }
	inline Authenticator_t530988528 * get_U24this_3() const { return ___U24this_3; }
	inline Authenticator_t530988528 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Authenticator_t530988528 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECURESESSIONVERIFYU3EC__ANONSTOREY17_T2063931614_H
#ifndef U3CSECURESESSIONVERIFYU3EC__ANONSTOREY16_T378387632_H
#define U3CSECURESESSIONVERIFYU3EC__ANONSTOREY16_T378387632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17/<secureSessionVerify>c__AnonStorey16
struct  U3CsecureSessionVerifyU3Ec__AnonStorey16_t378387632  : public RuntimeObject
{
public:
	// WebAuthAPI.SessionFileJSON WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17/<secureSessionVerify>c__AnonStorey16::sessionData
	SessionFileJSON_t380207342 * ___sessionData_0;
	// WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17 WebAuthAPI.Authenticator/<secureSessionVerify>c__AnonStorey17/<secureSessionVerify>c__AnonStorey16::<>f__ref$23
	U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614 * ___U3CU3Ef__refU2423_1;

public:
	inline static int32_t get_offset_of_sessionData_0() { return static_cast<int32_t>(offsetof(U3CsecureSessionVerifyU3Ec__AnonStorey16_t378387632, ___sessionData_0)); }
	inline SessionFileJSON_t380207342 * get_sessionData_0() const { return ___sessionData_0; }
	inline SessionFileJSON_t380207342 ** get_address_of_sessionData_0() { return &___sessionData_0; }
	inline void set_sessionData_0(SessionFileJSON_t380207342 * value)
	{
		___sessionData_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionData_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2423_1() { return static_cast<int32_t>(offsetof(U3CsecureSessionVerifyU3Ec__AnonStorey16_t378387632, ___U3CU3Ef__refU2423_1)); }
	inline U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614 * get_U3CU3Ef__refU2423_1() const { return ___U3CU3Ef__refU2423_1; }
	inline U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614 ** get_address_of_U3CU3Ef__refU2423_1() { return &___U3CU3Ef__refU2423_1; }
	inline void set_U3CU3Ef__refU2423_1(U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614 * value)
	{
		___U3CU3Ef__refU2423_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2423_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECURESESSIONVERIFYU3EC__ANONSTOREY16_T378387632_H
#ifndef U3CSECUREGETOTPCOUNTERU3EC__ITERATOR5_T740756658_H
#define U3CSECUREGETOTPCOUNTERU3EC__ITERATOR5_T740756658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5
struct  U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_2;
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::callback
	Action_2_t3073627706 * ___callback_3;
	// System.Object WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebAuthAPI.Authenticator/<secureGetOTPCounter>c__Iterator5::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___callback_3)); }
	inline Action_2_t3073627706 * get_callback_3() const { return ___callback_3; }
	inline Action_2_t3073627706 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_2_t3073627706 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREGETOTPCOUNTERU3EC__ITERATOR5_T740756658_H
#ifndef U3CSECUREGENERATESALTU3EC__ANONSTOREY18_T2564459476_H
#define U3CSECUREGENERATESALTU3EC__ANONSTOREY18_T2564459476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureGenerateSalt>c__AnonStorey18
struct  U3CsecureGenerateSaltU3Ec__AnonStorey18_t2564459476  : public RuntimeObject
{
public:
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<secureGenerateSalt>c__AnonStorey18::callback
	Action_2_t3073627706 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureGenerateSaltU3Ec__AnonStorey18_t2564459476, ___callback_0)); }
	inline Action_2_t3073627706 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3073627706 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3073627706 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREGENERATESALTU3EC__ANONSTOREY18_T2564459476_H
#ifndef U3CSECUREDERIVEPRIVATEKEYU3EC__ANONSTOREY19_T1306885325_H
#define U3CSECUREDERIVEPRIVATEKEYU3EC__ANONSTOREY19_T1306885325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureDerivePrivateKey>c__AnonStorey19
struct  U3CsecureDerivePrivateKeyU3Ec__AnonStorey19_t1306885325  : public RuntimeObject
{
public:
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<secureDerivePrivateKey>c__AnonStorey19::callback
	Action_2_t3073627706 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureDerivePrivateKeyU3Ec__AnonStorey19_t1306885325, ___callback_0)); }
	inline Action_2_t3073627706 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3073627706 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3073627706 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREDERIVEPRIVATEKEYU3EC__ANONSTOREY19_T1306885325_H
#ifndef U3CSECUREDERIVEVERIFIERU3EC__ANONSTOREY1A_T670270685_H
#define U3CSECUREDERIVEVERIFIERU3EC__ANONSTOREY1A_T670270685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureDeriveVerifier>c__AnonStorey1A
struct  U3CsecureDeriveVerifierU3Ec__AnonStorey1A_t670270685  : public RuntimeObject
{
public:
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<secureDeriveVerifier>c__AnonStorey1A::callback
	Action_2_t3073627706 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureDeriveVerifierU3Ec__AnonStorey1A_t670270685, ___callback_0)); }
	inline Action_2_t3073627706 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3073627706 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3073627706 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREDERIVEVERIFIERU3EC__ANONSTOREY1A_T670270685_H
#ifndef U3CSECUREREGISTERPOSTFORMU3EC__ITERATOR6_T483746417_H
#define U3CSECUREREGISTERPOSTFORMU3EC__ITERATOR6_T483746417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6
struct  U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::smscode
	String_t* ___smscode_2;
	// System.String WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::salt
	String_t* ___salt_3;
	// System.String WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::verifier
	String_t* ___verifier_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`1<System.Int32> WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::callback
	Action_1_t3123413348 * ___callback_6;
	// System.Object WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Authenticator/<secureRegisterPostForm>c__Iterator6::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_smscode_2() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___smscode_2)); }
	inline String_t* get_smscode_2() const { return ___smscode_2; }
	inline String_t** get_address_of_smscode_2() { return &___smscode_2; }
	inline void set_smscode_2(String_t* value)
	{
		___smscode_2 = value;
		Il2CppCodeGenWriteBarrier((&___smscode_2), value);
	}

	inline static int32_t get_offset_of_salt_3() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___salt_3)); }
	inline String_t* get_salt_3() const { return ___salt_3; }
	inline String_t** get_address_of_salt_3() { return &___salt_3; }
	inline void set_salt_3(String_t* value)
	{
		___salt_3 = value;
		Il2CppCodeGenWriteBarrier((&___salt_3), value);
	}

	inline static int32_t get_offset_of_verifier_4() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___verifier_4)); }
	inline String_t* get_verifier_4() const { return ___verifier_4; }
	inline String_t** get_address_of_verifier_4() { return &___verifier_4; }
	inline void set_verifier_4(String_t* value)
	{
		___verifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___verifier_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___callback_6)); }
	inline Action_1_t3123413348 * get_callback_6() const { return ___callback_6; }
	inline Action_1_t3123413348 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_1_t3123413348 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREREGISTERPOSTFORMU3EC__ITERATOR6_T483746417_H
#ifndef U3CSECUREGENERATEEPHEMERALU3EC__ANONSTOREY1B_T3426005466_H
#define U3CSECUREGENERATEEPHEMERALU3EC__ANONSTOREY1B_T3426005466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureGenerateEphemeral>c__AnonStorey1B
struct  U3CsecureGenerateEphemeralU3Ec__AnonStorey1B_t3426005466  : public RuntimeObject
{
public:
	// System.Action`2<System.Int32,System.String> WebAuthAPI.Authenticator/<secureGenerateEphemeral>c__AnonStorey1B::callback
	Action_2_t3073627706 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureGenerateEphemeralU3Ec__AnonStorey1B_t3426005466, ___callback_0)); }
	inline Action_2_t3073627706 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3073627706 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3073627706 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREGENERATEEPHEMERALU3EC__ANONSTOREY1B_T3426005466_H
#ifndef U3CSECUREPASSWORDLOGINPOSTFORMU3EC__ITERATOR7_T2776907233_H
#define U3CSECUREPASSWORDLOGINPOSTFORMU3EC__ITERATOR7_T2776907233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7
struct  U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::ephemeral
	String_t* ___ephemeral_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`3<System.Int32,System.String,System.String> WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::callback
	Action_3_t3371855306 * ___callback_4;
	// System.Object WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Authenticator/<securePasswordLoginPostForm>c__Iterator7::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_ephemeral_2() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___ephemeral_2)); }
	inline String_t* get_ephemeral_2() const { return ___ephemeral_2; }
	inline String_t** get_address_of_ephemeral_2() { return &___ephemeral_2; }
	inline void set_ephemeral_2(String_t* value)
	{
		___ephemeral_2 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeral_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___callback_4)); }
	inline Action_3_t3371855306 * get_callback_4() const { return ___callback_4; }
	inline Action_3_t3371855306 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_3_t3371855306 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREPASSWORDLOGINPOSTFORMU3EC__ITERATOR7_T2776907233_H
#ifndef U3CSECUREDERIVESESSIONU3EC__ANONSTOREY1C_T1482553556_H
#define U3CSECUREDERIVESESSIONU3EC__ANONSTOREY1C_T1482553556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Authenticator/<secureDeriveSession>c__AnonStorey1C
struct  U3CsecureDeriveSessionU3Ec__AnonStorey1C_t1482553556  : public RuntimeObject
{
public:
	// System.Action`3<System.Int32,System.String,System.String> WebAuthAPI.Authenticator/<secureDeriveSession>c__AnonStorey1C::callback
	Action_3_t3371855306 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsecureDeriveSessionU3Ec__AnonStorey1C_t1482553556, ___callback_0)); }
	inline Action_3_t3371855306 * get_callback_0() const { return ___callback_0; }
	inline Action_3_t3371855306 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_3_t3371855306 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSECUREDERIVESESSIONU3EC__ANONSTOREY1C_T1482553556_H
#ifndef SECUREPASSWORDLOGINVERIFYJSON_T120611188_H
#define SECUREPASSWORDLOGINVERIFYJSON_T120611188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecurePasswordLoginVerifyJSON
struct  SecurePasswordLoginVerifyJSON_t120611188  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SecurePasswordLoginVerifyJSON::code
	int32_t ___code_0;
	// System.String WebAuthAPI.SecurePasswordLoginVerifyJSON::proof
	String_t* ___proof_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SecurePasswordLoginVerifyJSON_t120611188, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_proof_1() { return static_cast<int32_t>(offsetof(SecurePasswordLoginVerifyJSON_t120611188, ___proof_1)); }
	inline String_t* get_proof_1() const { return ___proof_1; }
	inline String_t** get_address_of_proof_1() { return &___proof_1; }
	inline void set_proof_1(String_t* value)
	{
		___proof_1 = value;
		Il2CppCodeGenWriteBarrier((&___proof_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECUREPASSWORDLOGINVERIFYJSON_T120611188_H
#ifndef SKINJSON_T2311182042_H
#define SKINJSON_T2311182042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/SkinJSON
struct  SkinJSON_t2311182042  : public RuntimeObject
{
public:
	// System.Int32 AccountData/SkinJSON::id
	int32_t ___id_0;
	// System.Int32 AccountData/SkinJSON::type
	int32_t ___type_1;
	// System.String AccountData/SkinJSON::name
	String_t* ___name_2;
	// System.String AccountData/SkinJSON::path
	String_t* ___path_3;
	// System.String AccountData/SkinJSON::thumbnail
	String_t* ___thumbnail_4;
	// System.Boolean AccountData/SkinJSON::forbidden
	bool ___forbidden_5;
	// System.String AccountData/SkinJSON::description
	String_t* ___description_6;
	// System.Int32 AccountData/SkinJSON::costcoins
	int32_t ___costcoins_7;
	// System.Int32 AccountData/SkinJSON::costdiamonds
	int32_t ___costdiamonds_8;
	// System.UInt32 AccountData/SkinJSON::ringcolor
	uint32_t ___ringcolor_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_thumbnail_4() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___thumbnail_4)); }
	inline String_t* get_thumbnail_4() const { return ___thumbnail_4; }
	inline String_t** get_address_of_thumbnail_4() { return &___thumbnail_4; }
	inline void set_thumbnail_4(String_t* value)
	{
		___thumbnail_4 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_4), value);
	}

	inline static int32_t get_offset_of_forbidden_5() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___forbidden_5)); }
	inline bool get_forbidden_5() const { return ___forbidden_5; }
	inline bool* get_address_of_forbidden_5() { return &___forbidden_5; }
	inline void set_forbidden_5(bool value)
	{
		___forbidden_5 = value;
	}

	inline static int32_t get_offset_of_description_6() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___description_6)); }
	inline String_t* get_description_6() const { return ___description_6; }
	inline String_t** get_address_of_description_6() { return &___description_6; }
	inline void set_description_6(String_t* value)
	{
		___description_6 = value;
		Il2CppCodeGenWriteBarrier((&___description_6), value);
	}

	inline static int32_t get_offset_of_costcoins_7() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___costcoins_7)); }
	inline int32_t get_costcoins_7() const { return ___costcoins_7; }
	inline int32_t* get_address_of_costcoins_7() { return &___costcoins_7; }
	inline void set_costcoins_7(int32_t value)
	{
		___costcoins_7 = value;
	}

	inline static int32_t get_offset_of_costdiamonds_8() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___costdiamonds_8)); }
	inline int32_t get_costdiamonds_8() const { return ___costdiamonds_8; }
	inline int32_t* get_address_of_costdiamonds_8() { return &___costdiamonds_8; }
	inline void set_costdiamonds_8(int32_t value)
	{
		___costdiamonds_8 = value;
	}

	inline static int32_t get_offset_of_ringcolor_9() { return static_cast<int32_t>(offsetof(SkinJSON_t2311182042, ___ringcolor_9)); }
	inline uint32_t get_ringcolor_9() const { return ___ringcolor_9; }
	inline uint32_t* get_address_of_ringcolor_9() { return &___ringcolor_9; }
	inline void set_ringcolor_9(uint32_t value)
	{
		___ringcolor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINJSON_T2311182042_H
#ifndef U3CLOADPOLYGONLISTU3EC__ITERATOR0_T892062156_H
#define U3CLOADPOLYGONLISTU3EC__ITERATOR0_T892062156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonEditor/<LoadPolygonList>c__Iterator0
struct  U3CLoadPolygonListU3Ec__Iterator0_t892062156  : public RuntimeObject
{
public:
	// System.String PolygonEditor/<LoadPolygonList>c__Iterator0::<szFileName>__0
	String_t* ___U3CszFileNameU3E__0_0;
	// UnityEngine.WWW PolygonEditor/<LoadPolygonList>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// PolygonEditor PolygonEditor/<LoadPolygonList>c__Iterator0::$this
	PolygonEditor_t2170978208 * ___U24this_2;
	// System.Object PolygonEditor/<LoadPolygonList>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PolygonEditor/<LoadPolygonList>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolygonEditor/<LoadPolygonList>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CszFileNameU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator0_t892062156, ___U3CszFileNameU3E__0_0)); }
	inline String_t* get_U3CszFileNameU3E__0_0() const { return ___U3CszFileNameU3E__0_0; }
	inline String_t** get_address_of_U3CszFileNameU3E__0_0() { return &___U3CszFileNameU3E__0_0; }
	inline void set_U3CszFileNameU3E__0_0(String_t* value)
	{
		___U3CszFileNameU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CszFileNameU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator0_t892062156, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator0_t892062156, ___U24this_2)); }
	inline PolygonEditor_t2170978208 * get_U24this_2() const { return ___U24this_2; }
	inline PolygonEditor_t2170978208 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PolygonEditor_t2170978208 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator0_t892062156, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator0_t892062156, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadPolygonListU3Ec__Iterator0_t892062156, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPOLYGONLISTU3EC__ITERATOR0_T892062156_H
#ifndef U3CUPDATEROLENAMEPOSTFORMU3EC__ITERATOR1_T1270243448_H
#define U3CUPDATEROLENAMEPOSTFORMU3EC__ITERATOR1_T1270243448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1
struct  U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Account/<updateRoleNamePostForm>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEROLENAMEPOSTFORMU3EC__ITERATOR1_T1270243448_H
#ifndef U3CGETACCOUNTINFOPOSTFORMU3EC__ITERATOR0_T4132270415_H
#define U3CGETACCOUNTINFOPOSTFORMU3EC__ITERATOR0_T4132270415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0
struct  U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::idortoken
	String_t* ___idortoken_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::callback
	Action_2_t11315885 * ___callback_4;
	// System.Object WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Account/<getAccountInfoPostForm>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___callback_4)); }
	inline Action_2_t11315885 * get_callback_4() const { return ___callback_4; }
	inline Action_2_t11315885 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_2_t11315885 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETACCOUNTINFOPOSTFORMU3EC__ITERATOR0_T4132270415_H
#ifndef ACCOUNT_T400769301_H
#define ACCOUNT_T400769301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Account
struct  Account_t400769301  : public RuntimeObject
{
public:
	// WebAuthAPI.Authenticator WebAuthAPI.Account::authenticator
	Authenticator_t530988528 * ___authenticator_0;

public:
	inline static int32_t get_offset_of_authenticator_0() { return static_cast<int32_t>(offsetof(Account_t400769301, ___authenticator_0)); }
	inline Authenticator_t530988528 * get_authenticator_0() const { return ___authenticator_0; }
	inline Authenticator_t530988528 ** get_address_of_authenticator_0() { return &___authenticator_0; }
	inline void set_authenticator_0(Authenticator_t530988528 * value)
	{
		___authenticator_0 = value;
		Il2CppCodeGenWriteBarrier((&___authenticator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNT_T400769301_H
#ifndef UPDATEROLENAMEJSON_T4080939306_H
#define UPDATEROLENAMEJSON_T4080939306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.UpdateRoleNameJSON
struct  UpdateRoleNameJSON_t4080939306  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.UpdateRoleNameJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(UpdateRoleNameJSON_t4080939306, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEROLENAMEJSON_T4080939306_H
#ifndef TIMEKEEPER_T3694205465_H
#define TIMEKEEPER_T3694205465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.DemoParticle.TimeKeeper
struct  TimeKeeper_t3694205465  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.DemoParticle.TimeKeeper::lastExecutionTime
	int32_t ___lastExecutionTime_0;
	// System.Boolean ExitGames.Client.DemoParticle.TimeKeeper::shouldExecute
	bool ___shouldExecute_1;
	// System.Int32 ExitGames.Client.DemoParticle.TimeKeeper::<Interval>k__BackingField
	int32_t ___U3CIntervalU3Ek__BackingField_2;
	// System.Boolean ExitGames.Client.DemoParticle.TimeKeeper::<IsEnabled>k__BackingField
	bool ___U3CIsEnabledU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_lastExecutionTime_0() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___lastExecutionTime_0)); }
	inline int32_t get_lastExecutionTime_0() const { return ___lastExecutionTime_0; }
	inline int32_t* get_address_of_lastExecutionTime_0() { return &___lastExecutionTime_0; }
	inline void set_lastExecutionTime_0(int32_t value)
	{
		___lastExecutionTime_0 = value;
	}

	inline static int32_t get_offset_of_shouldExecute_1() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___shouldExecute_1)); }
	inline bool get_shouldExecute_1() const { return ___shouldExecute_1; }
	inline bool* get_address_of_shouldExecute_1() { return &___shouldExecute_1; }
	inline void set_shouldExecute_1(bool value)
	{
		___shouldExecute_1 = value;
	}

	inline static int32_t get_offset_of_U3CIntervalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___U3CIntervalU3Ek__BackingField_2)); }
	inline int32_t get_U3CIntervalU3Ek__BackingField_2() const { return ___U3CIntervalU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CIntervalU3Ek__BackingField_2() { return &___U3CIntervalU3Ek__BackingField_2; }
	inline void set_U3CIntervalU3Ek__BackingField_2(int32_t value)
	{
		___U3CIntervalU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsEnabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___U3CIsEnabledU3Ek__BackingField_3)); }
	inline bool get_U3CIsEnabledU3Ek__BackingField_3() const { return ___U3CIsEnabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsEnabledU3Ek__BackingField_3() { return &___U3CIsEnabledU3Ek__BackingField_3; }
	inline void set_U3CIsEnabledU3Ek__BackingField_3(bool value)
	{
		___U3CIsEnabledU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEKEEPER_T3694205465_H
#ifndef U3CUPLOADPOLYGONLISTFILEU3EC__ITERATOR3_T2946356472_H
#define U3CUPLOADPOLYGONLISTFILEU3EC__ITERATOR3_T2946356472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonEditor/<UpLoadPolygonListFile>c__Iterator3
struct  U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PolygonEditor/<UpLoadPolygonListFile>c__Iterator3::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_0;
	// System.String PolygonEditor/<UpLoadPolygonListFile>c__Iterator3::szContent
	String_t* ___szContent_1;
	// UnityEngine.WWW PolygonEditor/<UpLoadPolygonListFile>c__Iterator3::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Object PolygonEditor/<UpLoadPolygonListFile>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PolygonEditor/<UpLoadPolygonListFile>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolygonEditor/<UpLoadPolygonListFile>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwwwFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472, ___U3CwwwFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_0() const { return ___U3CwwwFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_0() { return &___U3CwwwFormU3E__0_0; }
	inline void set_U3CwwwFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_szContent_1() { return static_cast<int32_t>(offsetof(U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472, ___szContent_1)); }
	inline String_t* get_szContent_1() const { return ___szContent_1; }
	inline String_t** get_address_of_szContent_1() { return &___szContent_1; }
	inline void set_szContent_1(String_t* value)
	{
		___szContent_1 = value;
		Il2CppCodeGenWriteBarrier((&___szContent_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPLOADPOLYGONLISTFILEU3EC__ITERATOR3_T2946356472_H
#ifndef U3CUPLOADPOLYGONXMLFILEU3EC__ITERATOR2_T908359867_H
#define U3CUPLOADPOLYGONXMLFILEU3EC__ITERATOR2_T908359867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonEditor/<UploadPolygonXmlFile>c__Iterator2
struct  U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_0;
	// System.String PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::szPolygonName
	String_t* ___szPolygonName_1;
	// System.String PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::szContent
	String_t* ___szContent_2;
	// UnityEngine.WWW PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_3;
	// System.Object PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 PolygonEditor/<UploadPolygonXmlFile>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwwwFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___U3CwwwFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_0() const { return ___U3CwwwFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_0() { return &___U3CwwwFormU3E__0_0; }
	inline void set_U3CwwwFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_szPolygonName_1() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___szPolygonName_1)); }
	inline String_t* get_szPolygonName_1() const { return ___szPolygonName_1; }
	inline String_t** get_address_of_szPolygonName_1() { return &___szPolygonName_1; }
	inline void set_szPolygonName_1(String_t* value)
	{
		___szPolygonName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szPolygonName_1), value);
	}

	inline static int32_t get_offset_of_szContent_2() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___szContent_2)); }
	inline String_t* get_szContent_2() const { return ___szContent_2; }
	inline String_t** get_address_of_szContent_2() { return &___szContent_2; }
	inline void set_szContent_2(String_t* value)
	{
		___szContent_2 = value;
		Il2CppCodeGenWriteBarrier((&___szContent_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___U3CwwwU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_3() const { return ___U3CwwwU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_3() { return &___U3CwwwU3E__0_3; }
	inline void set_U3CwwwU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPLOADPOLYGONXMLFILEU3EC__ITERATOR2_T908359867_H
#ifndef U3CLOADPOLYGONBYXMLU3EC__ITERATOR1_T1040811462_H
#define U3CLOADPOLYGONBYXMLU3EC__ITERATOR1_T1040811462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonEditor/<LoadPolygonByXml>c__Iterator1
struct  U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462  : public RuntimeObject
{
public:
	// System.String PolygonEditor/<LoadPolygonByXml>c__Iterator1::szPolygonName
	String_t* ___szPolygonName_0;
	// System.String PolygonEditor/<LoadPolygonByXml>c__Iterator1::<szFileName>__0
	String_t* ___U3CszFileNameU3E__0_1;
	// UnityEngine.WWW PolygonEditor/<LoadPolygonByXml>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Xml.XmlNode PolygonEditor/<LoadPolygonByXml>c__Iterator1::<root>__0
	XmlNode_t3767805227 * ___U3CrootU3E__0_3;
	// System.Xml.XmlDocument PolygonEditor/<LoadPolygonByXml>c__Iterator1::<myXmlDoc>__0
	XmlDocument_t2837193595 * ___U3CmyXmlDocU3E__0_4;
	// System.String[] PolygonEditor/<LoadPolygonByXml>c__Iterator1::<aryPointPos>__0
	StringU5BU5D_t1281789340* ___U3CaryPointPosU3E__0_5;
	// Polygon PolygonEditor/<LoadPolygonByXml>c__Iterator1::polygon
	Polygon_t12692255 * ___polygon_6;
	// System.Object PolygonEditor/<LoadPolygonByXml>c__Iterator1::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean PolygonEditor/<LoadPolygonByXml>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 PolygonEditor/<LoadPolygonByXml>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_szPolygonName_0() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___szPolygonName_0)); }
	inline String_t* get_szPolygonName_0() const { return ___szPolygonName_0; }
	inline String_t** get_address_of_szPolygonName_0() { return &___szPolygonName_0; }
	inline void set_szPolygonName_0(String_t* value)
	{
		___szPolygonName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szPolygonName_0), value);
	}

	inline static int32_t get_offset_of_U3CszFileNameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U3CszFileNameU3E__0_1)); }
	inline String_t* get_U3CszFileNameU3E__0_1() const { return ___U3CszFileNameU3E__0_1; }
	inline String_t** get_address_of_U3CszFileNameU3E__0_1() { return &___U3CszFileNameU3E__0_1; }
	inline void set_U3CszFileNameU3E__0_1(String_t* value)
	{
		___U3CszFileNameU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CszFileNameU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U3CrootU3E__0_3)); }
	inline XmlNode_t3767805227 * get_U3CrootU3E__0_3() const { return ___U3CrootU3E__0_3; }
	inline XmlNode_t3767805227 ** get_address_of_U3CrootU3E__0_3() { return &___U3CrootU3E__0_3; }
	inline void set_U3CrootU3E__0_3(XmlNode_t3767805227 * value)
	{
		___U3CrootU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CmyXmlDocU3E__0_4() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U3CmyXmlDocU3E__0_4)); }
	inline XmlDocument_t2837193595 * get_U3CmyXmlDocU3E__0_4() const { return ___U3CmyXmlDocU3E__0_4; }
	inline XmlDocument_t2837193595 ** get_address_of_U3CmyXmlDocU3E__0_4() { return &___U3CmyXmlDocU3E__0_4; }
	inline void set_U3CmyXmlDocU3E__0_4(XmlDocument_t2837193595 * value)
	{
		___U3CmyXmlDocU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmyXmlDocU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CaryPointPosU3E__0_5() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U3CaryPointPosU3E__0_5)); }
	inline StringU5BU5D_t1281789340* get_U3CaryPointPosU3E__0_5() const { return ___U3CaryPointPosU3E__0_5; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CaryPointPosU3E__0_5() { return &___U3CaryPointPosU3E__0_5; }
	inline void set_U3CaryPointPosU3E__0_5(StringU5BU5D_t1281789340* value)
	{
		___U3CaryPointPosU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaryPointPosU3E__0_5), value);
	}

	inline static int32_t get_offset_of_polygon_6() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___polygon_6)); }
	inline Polygon_t12692255 * get_polygon_6() const { return ___polygon_6; }
	inline Polygon_t12692255 ** get_address_of_polygon_6() { return &___polygon_6; }
	inline void set_polygon_6(Polygon_t12692255 * value)
	{
		___polygon_6 = value;
		Il2CppCodeGenWriteBarrier((&___polygon_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPOLYGONBYXMLU3EC__ITERATOR1_T1040811462_H
#ifndef ACCOUNTJSON_T3293827731_H
#define ACCOUNTJSON_T3293827731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.AccountJSON
struct  AccountJSON_t3293827731  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.AccountJSON::phonenum
	String_t* ___phonenum_0;
	// System.String WebAuthAPI.AccountJSON::rolename
	String_t* ___rolename_1;
	// System.UInt32 WebAuthAPI.AccountJSON::coins
	uint32_t ___coins_2;
	// System.UInt32 WebAuthAPI.AccountJSON::diamonds
	uint32_t ___diamonds_3;
	// System.Boolean WebAuthAPI.AccountJSON::forbidden
	bool ___forbidden_4;

public:
	inline static int32_t get_offset_of_phonenum_0() { return static_cast<int32_t>(offsetof(AccountJSON_t3293827731, ___phonenum_0)); }
	inline String_t* get_phonenum_0() const { return ___phonenum_0; }
	inline String_t** get_address_of_phonenum_0() { return &___phonenum_0; }
	inline void set_phonenum_0(String_t* value)
	{
		___phonenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_0), value);
	}

	inline static int32_t get_offset_of_rolename_1() { return static_cast<int32_t>(offsetof(AccountJSON_t3293827731, ___rolename_1)); }
	inline String_t* get_rolename_1() const { return ___rolename_1; }
	inline String_t** get_address_of_rolename_1() { return &___rolename_1; }
	inline void set_rolename_1(String_t* value)
	{
		___rolename_1 = value;
		Il2CppCodeGenWriteBarrier((&___rolename_1), value);
	}

	inline static int32_t get_offset_of_coins_2() { return static_cast<int32_t>(offsetof(AccountJSON_t3293827731, ___coins_2)); }
	inline uint32_t get_coins_2() const { return ___coins_2; }
	inline uint32_t* get_address_of_coins_2() { return &___coins_2; }
	inline void set_coins_2(uint32_t value)
	{
		___coins_2 = value;
	}

	inline static int32_t get_offset_of_diamonds_3() { return static_cast<int32_t>(offsetof(AccountJSON_t3293827731, ___diamonds_3)); }
	inline uint32_t get_diamonds_3() const { return ___diamonds_3; }
	inline uint32_t* get_address_of_diamonds_3() { return &___diamonds_3; }
	inline void set_diamonds_3(uint32_t value)
	{
		___diamonds_3 = value;
	}

	inline static int32_t get_offset_of_forbidden_4() { return static_cast<int32_t>(offsetof(AccountJSON_t3293827731, ___forbidden_4)); }
	inline bool get_forbidden_4() const { return ___forbidden_4; }
	inline bool* get_address_of_forbidden_4() { return &___forbidden_4; }
	inline void set_forbidden_4(bool value)
	{
		___forbidden_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNTJSON_T3293827731_H
#ifndef GETACCOUNTINFOJSON_T2268999360_H
#define GETACCOUNTINFOJSON_T2268999360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.GetAccountInfoJSON
struct  GetAccountInfoJSON_t2268999360  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.GetAccountInfoJSON::code
	int32_t ___code_0;
	// WebAuthAPI.AccountJSON WebAuthAPI.GetAccountInfoJSON::info
	AccountJSON_t3293827731 * ___info_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(GetAccountInfoJSON_t2268999360, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(GetAccountInfoJSON_t2268999360, ___info_1)); }
	inline AccountJSON_t3293827731 * get_info_1() const { return ___info_1; }
	inline AccountJSON_t3293827731 ** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(AccountJSON_t3293827731 * value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETACCOUNTINFOJSON_T2268999360_H
#ifndef SKINLISTJSON_T1657696259_H
#define SKINLISTJSON_T1657696259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/SkinListJSON
struct  SkinListJSON_t1657696259  : public RuntimeObject
{
public:
	// AccountData/SkinJSON[] AccountData/SkinListJSON::skins
	SkinJSONU5BU5D_t732741759* ___skins_0;

public:
	inline static int32_t get_offset_of_skins_0() { return static_cast<int32_t>(offsetof(SkinListJSON_t1657696259, ___skins_0)); }
	inline SkinJSONU5BU5D_t732741759* get_skins_0() const { return ___skins_0; }
	inline SkinJSONU5BU5D_t732741759** get_address_of_skins_0() { return &___skins_0; }
	inline void set_skins_0(SkinJSONU5BU5D_t732741759* value)
	{
		___skins_0 = value;
		Il2CppCodeGenWriteBarrier((&___skins_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINLISTJSON_T1657696259_H
#ifndef GETAUTHENTICATORTYPEJSON_T3656451505_H
#define GETAUTHENTICATORTYPEJSON_T3656451505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.GetAuthenticatorTypeJSON
struct  GetAuthenticatorTypeJSON_t3656451505  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.GetAuthenticatorTypeJSON::code
	int32_t ___code_0;
	// System.String WebAuthAPI.GetAuthenticatorTypeJSON::type
	String_t* ___type_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(GetAuthenticatorTypeJSON_t3656451505, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(GetAuthenticatorTypeJSON_t3656451505, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETAUTHENTICATORTYPEJSON_T3656451505_H
#ifndef GETSMSCODEJSON_T2897747432_H
#define GETSMSCODEJSON_T2897747432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.GetSMSCodeJSON
struct  GetSMSCodeJSON_t2897747432  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.GetSMSCodeJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(GetSMSCodeJSON_t2897747432, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSMSCODEJSON_T2897747432_H
#ifndef SIMPLEREGISTERJSON_T3209520220_H
#define SIMPLEREGISTERJSON_T3209520220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SimpleRegisterJSON
struct  SimpleRegisterJSON_t3209520220  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SimpleRegisterJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SimpleRegisterJSON_t3209520220, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEREGISTERJSON_T3209520220_H
#ifndef SECUREREGISTERJSON_T1894166460_H
#define SECUREREGISTERJSON_T1894166460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecureRegisterJSON
struct  SecureRegisterJSON_t1894166460  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SecureRegisterJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SecureRegisterJSON_t1894166460, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECUREREGISTERJSON_T1894166460_H
#ifndef SIMPLEPASSWORDLOGINJSON_T3050611672_H
#define SIMPLEPASSWORDLOGINJSON_T3050611672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SimplePasswordLoginJSON
struct  SimplePasswordLoginJSON_t3050611672  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SimplePasswordLoginJSON::code
	int32_t ___code_0;
	// System.String WebAuthAPI.SimplePasswordLoginJSON::sessionid
	String_t* ___sessionid_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SimplePasswordLoginJSON_t3050611672, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_sessionid_1() { return static_cast<int32_t>(offsetof(SimplePasswordLoginJSON_t3050611672, ___sessionid_1)); }
	inline String_t* get_sessionid_1() const { return ___sessionid_1; }
	inline String_t** get_address_of_sessionid_1() { return &___sessionid_1; }
	inline void set_sessionid_1(String_t* value)
	{
		___sessionid_1 = value;
		Il2CppCodeGenWriteBarrier((&___sessionid_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEPASSWORDLOGINJSON_T3050611672_H
#ifndef SECUREPASSWORDLOGINJSON_T1225826777_H
#define SECUREPASSWORDLOGINJSON_T1225826777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.SecurePasswordLoginJSON
struct  SecurePasswordLoginJSON_t1225826777  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.SecurePasswordLoginJSON::code
	int32_t ___code_0;
	// System.String WebAuthAPI.SecurePasswordLoginJSON::salt
	String_t* ___salt_1;
	// System.String WebAuthAPI.SecurePasswordLoginJSON::ephemeral
	String_t* ___ephemeral_2;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(SecurePasswordLoginJSON_t1225826777, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_salt_1() { return static_cast<int32_t>(offsetof(SecurePasswordLoginJSON_t1225826777, ___salt_1)); }
	inline String_t* get_salt_1() const { return ___salt_1; }
	inline String_t** get_address_of_salt_1() { return &___salt_1; }
	inline void set_salt_1(String_t* value)
	{
		___salt_1 = value;
		Il2CppCodeGenWriteBarrier((&___salt_1), value);
	}

	inline static int32_t get_offset_of_ephemeral_2() { return static_cast<int32_t>(offsetof(SecurePasswordLoginJSON_t1225826777, ___ephemeral_2)); }
	inline String_t* get_ephemeral_2() const { return ___ephemeral_2; }
	inline String_t** get_address_of_ephemeral_2() { return &___ephemeral_2; }
	inline void set_ephemeral_2(String_t* value)
	{
		___ephemeral_2 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeral_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECUREPASSWORDLOGINJSON_T1225826777_H
#ifndef U3CDOCHARGEU3EC__ANONSTOREY4_T1711606679_H
#define U3CDOCHARGEU3EC__ANONSTOREY4_T1711606679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/<DoCharge>c__AnonStorey4
struct  U3CDoChargeU3Ec__AnonStorey4_t1711606679  : public RuntimeObject
{
public:
	// System.Int32 AccountData/<DoCharge>c__AnonStorey4::code
	int32_t ___code_0;
	// AccountData AccountData/<DoCharge>c__AnonStorey4::$this
	AccountData_t1002028515 * ___U24this_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(U3CDoChargeU3Ec__AnonStorey4_t1711606679, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoChargeU3Ec__AnonStorey4_t1711606679, ___U24this_1)); }
	inline AccountData_t1002028515 * get_U24this_1() const { return ___U24this_1; }
	inline AccountData_t1002028515 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AccountData_t1002028515 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCHARGEU3EC__ANONSTOREY4_T1711606679_H
#ifndef FILESKINLISTJSON_T1219265033_H
#define FILESKINLISTJSON_T1219265033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/FileSkinListJSON
struct  FileSkinListJSON_t1219265033  : public RuntimeObject
{
public:
	// System.String AccountData/FileSkinListJSON::any_type
	String_t* ___any_type_0;
	// AccountData/SkinListJSON AccountData/FileSkinListJSON::any
	SkinListJSON_t1657696259 * ___any_1;

public:
	inline static int32_t get_offset_of_any_type_0() { return static_cast<int32_t>(offsetof(FileSkinListJSON_t1219265033, ___any_type_0)); }
	inline String_t* get_any_type_0() const { return ___any_type_0; }
	inline String_t** get_address_of_any_type_0() { return &___any_type_0; }
	inline void set_any_type_0(String_t* value)
	{
		___any_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___any_type_0), value);
	}

	inline static int32_t get_offset_of_any_1() { return static_cast<int32_t>(offsetof(FileSkinListJSON_t1219265033, ___any_1)); }
	inline SkinListJSON_t1657696259 * get_any_1() const { return ___any_1; }
	inline SkinListJSON_t1657696259 ** get_address_of_any_1() { return &___any_1; }
	inline void set_any_1(SkinListJSON_t1657696259 * value)
	{
		___any_1 = value;
		Il2CppCodeGenWriteBarrier((&___any_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESKINLISTJSON_T1219265033_H
#ifndef U3CLOADLOCALERESOURCESU3EC__ITERATOR0_T4084272587_H
#define U3CLOADLOCALERESOURCESU3EC__ITERATOR0_T4084272587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/<loadLocaleResources>c__Iterator0
struct  U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Resource> AccountData/<loadLocaleResources>c__Iterator0::<resources>__0
	List_1_t1603957611 * ___U3CresourcesU3E__0_0;
	// System.Action`1<System.Int32> AccountData/<loadLocaleResources>c__Iterator0::callback
	Action_1_t3123413348 * ___callback_1;
	// AccountData AccountData/<loadLocaleResources>c__Iterator0::$this
	AccountData_t1002028515 * ___U24this_2;
	// System.Object AccountData/<loadLocaleResources>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean AccountData/<loadLocaleResources>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 AccountData/<loadLocaleResources>c__Iterator0::$PC
	int32_t ___U24PC_5;
	// AccountData/<loadLocaleResources>c__Iterator0/<loadLocaleResources>c__AnonStorey2 AccountData/<loadLocaleResources>c__Iterator0::$locvar0
	U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912 * ___U24locvar0_6;

public:
	inline static int32_t get_offset_of_U3CresourcesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___U3CresourcesU3E__0_0)); }
	inline List_1_t1603957611 * get_U3CresourcesU3E__0_0() const { return ___U3CresourcesU3E__0_0; }
	inline List_1_t1603957611 ** get_address_of_U3CresourcesU3E__0_0() { return &___U3CresourcesU3E__0_0; }
	inline void set_U3CresourcesU3E__0_0(List_1_t1603957611 * value)
	{
		___U3CresourcesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourcesU3E__0_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___callback_1)); }
	inline Action_1_t3123413348 * get_callback_1() const { return ___callback_1; }
	inline Action_1_t3123413348 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_1_t3123413348 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___U24this_2)); }
	inline AccountData_t1002028515 * get_U24this_2() const { return ___U24this_2; }
	inline AccountData_t1002028515 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AccountData_t1002028515 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587, ___U24locvar0_6)); }
	inline U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912 * get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912 ** get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912 * value)
	{
		___U24locvar0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_6), value);
	}
};

struct U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587_StaticFields
{
public:
	// System.EventHandler`1<ProgressEventArgs> AccountData/<loadLocaleResources>c__Iterator0::<>f__am$cache0
	EventHandler_1_t4063164883 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline EventHandler_1_t4063164883 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline EventHandler_1_t4063164883 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(EventHandler_1_t4063164883 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLOCALERESOURCESU3EC__ITERATOR0_T4084272587_H
#ifndef U3CLOADLOCALERESOURCESU3EC__ANONSTOREY2_T1460044912_H
#define U3CLOADLOCALERESOURCESU3EC__ANONSTOREY2_T1460044912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/<loadLocaleResources>c__Iterator0/<loadLocaleResources>c__AnonStorey2
struct  U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912  : public RuntimeObject
{
public:
	// System.Action`1<System.Int32> AccountData/<loadLocaleResources>c__Iterator0/<loadLocaleResources>c__AnonStorey2::callback
	Action_1_t3123413348 * ___callback_0;
	// AccountData/<loadLocaleResources>c__Iterator0 AccountData/<loadLocaleResources>c__Iterator0/<loadLocaleResources>c__AnonStorey2::<>f__ref$0
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912, ___callback_0)); }
	inline Action_1_t3123413348 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3123413348 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3123413348 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912, ___U3CU3Ef__refU240_1)); }
	inline U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLOCALERESOURCESU3EC__ANONSTOREY2_T1460044912_H
#ifndef U3CLOADREMOTERESOURCESU3EC__ITERATOR1_T1103037512_H
#define U3CLOADREMOTERESOURCESU3EC__ITERATOR1_T1103037512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/<loadRemoteResources>c__Iterator1
struct  U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512  : public RuntimeObject
{
public:
	// System.String AccountData/<loadRemoteResources>c__Iterator1::<skinsJsonUrl>__0
	String_t* ___U3CskinsJsonUrlU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest AccountData/<loadRemoteResources>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_1;
	// System.Collections.Generic.List`1<Resource> AccountData/<loadRemoteResources>c__Iterator1::<resources>__1
	List_1_t1603957611 * ___U3CresourcesU3E__1_2;
	// AccountData/FileSkinListJSON AccountData/<loadRemoteResources>c__Iterator1::<json>__1
	FileSkinListJSON_t1219265033 * ___U3CjsonU3E__1_3;
	// System.Action`1<System.Int32> AccountData/<loadRemoteResources>c__Iterator1::callback
	Action_1_t3123413348 * ___callback_4;
	// AccountData AccountData/<loadRemoteResources>c__Iterator1::$this
	AccountData_t1002028515 * ___U24this_5;
	// System.Object AccountData/<loadRemoteResources>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean AccountData/<loadRemoteResources>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 AccountData/<loadRemoteResources>c__Iterator1::$PC
	int32_t ___U24PC_8;
	// AccountData/<loadRemoteResources>c__Iterator1/<loadRemoteResources>c__AnonStorey3 AccountData/<loadRemoteResources>c__Iterator1::$locvar0
	U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CskinsJsonUrlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U3CskinsJsonUrlU3E__0_0)); }
	inline String_t* get_U3CskinsJsonUrlU3E__0_0() const { return ___U3CskinsJsonUrlU3E__0_0; }
	inline String_t** get_address_of_U3CskinsJsonUrlU3E__0_0() { return &___U3CskinsJsonUrlU3E__0_0; }
	inline void set_U3CskinsJsonUrlU3E__0_0(String_t* value)
	{
		___U3CskinsJsonUrlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CskinsJsonUrlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CresourcesU3E__1_2() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U3CresourcesU3E__1_2)); }
	inline List_1_t1603957611 * get_U3CresourcesU3E__1_2() const { return ___U3CresourcesU3E__1_2; }
	inline List_1_t1603957611 ** get_address_of_U3CresourcesU3E__1_2() { return &___U3CresourcesU3E__1_2; }
	inline void set_U3CresourcesU3E__1_2(List_1_t1603957611 * value)
	{
		___U3CresourcesU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourcesU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_3() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U3CjsonU3E__1_3)); }
	inline FileSkinListJSON_t1219265033 * get_U3CjsonU3E__1_3() const { return ___U3CjsonU3E__1_3; }
	inline FileSkinListJSON_t1219265033 ** get_address_of_U3CjsonU3E__1_3() { return &___U3CjsonU3E__1_3; }
	inline void set_U3CjsonU3E__1_3(FileSkinListJSON_t1219265033 * value)
	{
		___U3CjsonU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__1_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___callback_4)); }
	inline Action_1_t3123413348 * get_callback_4() const { return ___callback_4; }
	inline Action_1_t3123413348 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t3123413348 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U24this_5)); }
	inline AccountData_t1002028515 * get_U24this_5() const { return ___U24this_5; }
	inline AccountData_t1002028515 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(AccountData_t1002028515 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512, ___U24locvar0_9)); }
	inline U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_9), value);
	}
};

struct U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512_StaticFields
{
public:
	// System.EventHandler`1<ProgressEventArgs> AccountData/<loadRemoteResources>c__Iterator1::<>f__am$cache0
	EventHandler_1_t4063164883 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline EventHandler_1_t4063164883 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline EventHandler_1_t4063164883 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(EventHandler_1_t4063164883 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADREMOTERESOURCESU3EC__ITERATOR1_T1103037512_H
#ifndef U3CLOADREMOTERESOURCESU3EC__ANONSTOREY3_T287817736_H
#define U3CLOADREMOTERESOURCESU3EC__ANONSTOREY3_T287817736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/<loadRemoteResources>c__Iterator1/<loadRemoteResources>c__AnonStorey3
struct  U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736  : public RuntimeObject
{
public:
	// System.Action`1<System.Int32> AccountData/<loadRemoteResources>c__Iterator1/<loadRemoteResources>c__AnonStorey3::callback
	Action_1_t3123413348 * ___callback_0;
	// AccountData/<loadRemoteResources>c__Iterator1 AccountData/<loadRemoteResources>c__Iterator1/<loadRemoteResources>c__AnonStorey3::<>f__ref$1
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736, ___callback_0)); }
	inline Action_1_t3123413348 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3123413348 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3123413348 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736, ___U3CU3Ef__refU241_1)); }
	inline U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADREMOTERESOURCESU3EC__ANONSTOREY3_T287817736_H
#ifndef SEXPLODEINFO_T354989019_H
#define SEXPLODEINFO_T354989019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/sExplodeInfo
struct  sExplodeInfo_t354989019 
{
public:
	union
	{
		struct
		{
		};
		uint8_t sExplodeInfo_t354989019__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEXPLODEINFO_T354989019_H
#ifndef SPLAYERBASEDATA_T1472229795_H
#define SPLAYERBASEDATA_T1472229795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/sPlayerBaseData
struct  sPlayerBaseData_t1472229795 
{
public:
	// System.Single Player/sPlayerBaseData::fBaseSpeed
	float ___fBaseSpeed_0;
	// System.Single Player/sPlayerBaseData::fBaseMp
	float ___fBaseMp_1;
	// System.Single Player/sPlayerBaseData::fBaseSize
	float ___fBaseSize_2;
	// System.Single Player/sPlayerBaseData::fBaseShellTime
	float ___fBaseShellTime_3;
	// System.Single Player/sPlayerBaseData::fBaseSpitDistance
	float ___fBaseSpitDistance_4;

public:
	inline static int32_t get_offset_of_fBaseSpeed_0() { return static_cast<int32_t>(offsetof(sPlayerBaseData_t1472229795, ___fBaseSpeed_0)); }
	inline float get_fBaseSpeed_0() const { return ___fBaseSpeed_0; }
	inline float* get_address_of_fBaseSpeed_0() { return &___fBaseSpeed_0; }
	inline void set_fBaseSpeed_0(float value)
	{
		___fBaseSpeed_0 = value;
	}

	inline static int32_t get_offset_of_fBaseMp_1() { return static_cast<int32_t>(offsetof(sPlayerBaseData_t1472229795, ___fBaseMp_1)); }
	inline float get_fBaseMp_1() const { return ___fBaseMp_1; }
	inline float* get_address_of_fBaseMp_1() { return &___fBaseMp_1; }
	inline void set_fBaseMp_1(float value)
	{
		___fBaseMp_1 = value;
	}

	inline static int32_t get_offset_of_fBaseSize_2() { return static_cast<int32_t>(offsetof(sPlayerBaseData_t1472229795, ___fBaseSize_2)); }
	inline float get_fBaseSize_2() const { return ___fBaseSize_2; }
	inline float* get_address_of_fBaseSize_2() { return &___fBaseSize_2; }
	inline void set_fBaseSize_2(float value)
	{
		___fBaseSize_2 = value;
	}

	inline static int32_t get_offset_of_fBaseShellTime_3() { return static_cast<int32_t>(offsetof(sPlayerBaseData_t1472229795, ___fBaseShellTime_3)); }
	inline float get_fBaseShellTime_3() const { return ___fBaseShellTime_3; }
	inline float* get_address_of_fBaseShellTime_3() { return &___fBaseShellTime_3; }
	inline void set_fBaseShellTime_3(float value)
	{
		___fBaseShellTime_3 = value;
	}

	inline static int32_t get_offset_of_fBaseSpitDistance_4() { return static_cast<int32_t>(offsetof(sPlayerBaseData_t1472229795, ___fBaseSpitDistance_4)); }
	inline float get_fBaseSpitDistance_4() const { return ___fBaseSpitDistance_4; }
	inline float* get_address_of_fBaseSpitDistance_4() { return &___fBaseSpitDistance_4; }
	inline void set_fBaseSpitDistance_4(float value)
	{
		___fBaseSpitDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLAYERBASEDATA_T1472229795_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef EPOLYGONEDITOPERATION_T330424546_H
#define EPOLYGONEDITOPERATION_T330424546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonEditor/ePolygonEditOperation
struct  ePolygonEditOperation_t330424546 
{
public:
	// System.Int32 PolygonEditor/ePolygonEditOperation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ePolygonEditOperation_t330424546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPOLYGONEDITOPERATION_T330424546_H
#ifndef SSPLITINFO_T3300999634_H
#define SSPLITINFO_T3300999634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/sSplitInfo
struct  sSplitInfo_t3300999634 
{
public:
	// UnityEngine.Vector3 Player/sSplitInfo::pos
	Vector3_t3722313464  ___pos_0;
	// UnityEngine.Vector2 Player/sSplitInfo::dir
	Vector2_t2156229523  ___dir_1;
	// System.Int32 Player/sSplitInfo::my_max_split_num
	int32_t ___my_max_split_num_2;
	// System.Int32 Player/sSplitInfo::my_real_split_num
	int32_t ___my_real_split_num_3;
	// System.Int32 Player/sSplitInfo::split_couting
	int32_t ___split_couting_4;
	// System.Single Player/sSplitInfo::real_split_size
	float ___real_split_size_5;
	// System.Single Player/sSplitInfo::seg_dis
	float ___seg_dis_6;
	// System.Boolean Player/sSplitInfo::dead_after_split
	bool ___dead_after_split_7;
	// Ball Player/sSplitInfo::_ball
	Ball_t2206666566 * ____ball_8;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___pos_0)); }
	inline Vector3_t3722313464  get_pos_0() const { return ___pos_0; }
	inline Vector3_t3722313464 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector3_t3722313464  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_dir_1() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___dir_1)); }
	inline Vector2_t2156229523  get_dir_1() const { return ___dir_1; }
	inline Vector2_t2156229523 * get_address_of_dir_1() { return &___dir_1; }
	inline void set_dir_1(Vector2_t2156229523  value)
	{
		___dir_1 = value;
	}

	inline static int32_t get_offset_of_my_max_split_num_2() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___my_max_split_num_2)); }
	inline int32_t get_my_max_split_num_2() const { return ___my_max_split_num_2; }
	inline int32_t* get_address_of_my_max_split_num_2() { return &___my_max_split_num_2; }
	inline void set_my_max_split_num_2(int32_t value)
	{
		___my_max_split_num_2 = value;
	}

	inline static int32_t get_offset_of_my_real_split_num_3() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___my_real_split_num_3)); }
	inline int32_t get_my_real_split_num_3() const { return ___my_real_split_num_3; }
	inline int32_t* get_address_of_my_real_split_num_3() { return &___my_real_split_num_3; }
	inline void set_my_real_split_num_3(int32_t value)
	{
		___my_real_split_num_3 = value;
	}

	inline static int32_t get_offset_of_split_couting_4() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___split_couting_4)); }
	inline int32_t get_split_couting_4() const { return ___split_couting_4; }
	inline int32_t* get_address_of_split_couting_4() { return &___split_couting_4; }
	inline void set_split_couting_4(int32_t value)
	{
		___split_couting_4 = value;
	}

	inline static int32_t get_offset_of_real_split_size_5() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___real_split_size_5)); }
	inline float get_real_split_size_5() const { return ___real_split_size_5; }
	inline float* get_address_of_real_split_size_5() { return &___real_split_size_5; }
	inline void set_real_split_size_5(float value)
	{
		___real_split_size_5 = value;
	}

	inline static int32_t get_offset_of_seg_dis_6() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___seg_dis_6)); }
	inline float get_seg_dis_6() const { return ___seg_dis_6; }
	inline float* get_address_of_seg_dis_6() { return &___seg_dis_6; }
	inline void set_seg_dis_6(float value)
	{
		___seg_dis_6 = value;
	}

	inline static int32_t get_offset_of_dead_after_split_7() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ___dead_after_split_7)); }
	inline bool get_dead_after_split_7() const { return ___dead_after_split_7; }
	inline bool* get_address_of_dead_after_split_7() { return &___dead_after_split_7; }
	inline void set_dead_after_split_7(bool value)
	{
		___dead_after_split_7 = value;
	}

	inline static int32_t get_offset_of__ball_8() { return static_cast<int32_t>(offsetof(sSplitInfo_t3300999634, ____ball_8)); }
	inline Ball_t2206666566 * get__ball_8() const { return ____ball_8; }
	inline Ball_t2206666566 ** get_address_of__ball_8() { return &____ball_8; }
	inline void set__ball_8(Ball_t2206666566 * value)
	{
		____ball_8 = value;
		Il2CppCodeGenWriteBarrier((&____ball_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Player/sSplitInfo
struct sSplitInfo_t3300999634_marshaled_pinvoke
{
	Vector3_t3722313464  ___pos_0;
	Vector2_t2156229523  ___dir_1;
	int32_t ___my_max_split_num_2;
	int32_t ___my_real_split_num_3;
	int32_t ___split_couting_4;
	float ___real_split_size_5;
	float ___seg_dis_6;
	int32_t ___dead_after_split_7;
	Ball_t2206666566 * ____ball_8;
};
// Native definition for COM marshalling of Player/sSplitInfo
struct sSplitInfo_t3300999634_marshaled_com
{
	Vector3_t3722313464  ___pos_0;
	Vector2_t2156229523  ___dir_1;
	int32_t ___my_max_split_num_2;
	int32_t ___my_real_split_num_3;
	int32_t ___split_couting_4;
	float ___real_split_size_5;
	float ___seg_dis_6;
	int32_t ___dead_after_split_7;
	Ball_t2206666566 * ____ball_8;
};
#endif // SSPLITINFO_T3300999634_H
#ifndef EMONEYTYPE_T3516216660_H
#define EMONEYTYPE_T3516216660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData/eMoneyType
struct  eMoneyType_t3516216660 
{
public:
	// System.Int32 AccountData/eMoneyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMoneyType_t3516216660, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONEYTYPE_T3516216660_H
#ifndef EMAPOBJTYPE_T1356972478_H
#define EMAPOBJTYPE_T1356972478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapObj/eMapObjType
struct  eMapObjType_t1356972478 
{
public:
	// System.Int32 MapObj/eMapObjType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMapObjType_t1356972478, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAPOBJTYPE_T1356972478_H
#ifndef ECTRLTYPE_T377641053_H
#define ECTRLTYPE_T377641053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAccountSystem/eCtrlType
struct  eCtrlType_t377641053 
{
public:
	// System.Int32 CAccountSystem/eCtrlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCtrlType_t377641053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECTRLTYPE_T377641053_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef RESOURCEMANAGER_T484397614_H
#define RESOURCEMANAGER_T484397614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager
struct  ResourceManager_t484397614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ResourceManager::m_preCiChuXian
	GameObject_t1113636619 * ___m_preCiChuXian_3;
	// UnityEngine.GameObject[] ResourceManager::m_aryCiChuXianDongHuaPrefabs
	GameObjectU5BU5D_t3328599146* ___m_aryCiChuXianDongHuaPrefabs_4;
	// UnityEngine.Font[] ResourceManager::m_font
	FontU5BU5D_t1399044585* ___m_font_5;
	// UnityEngine.GameObject ResourceManager::m_preBuff
	GameObject_t1113636619 * ___m_preBuff_6;
	// UnityEngine.GameObject ResourceManager::m_preGrass
	GameObject_t1113636619 * ___m_preGrass_7;
	// UnityEngine.GameObject ResourceManager::m_preSpray
	GameObject_t1113636619 * ___m_preSpray_8;
	// UnityEngine.GameObject ResourceManager::m_preSceneBall
	GameObject_t1113636619 * ___m_preSceneBall_9;
	// UnityEngine.GameObject ResourceManager::m_preGunsight
	GameObject_t1113636619 * ___m_preGunsight_10;
	// UnityEngine.GameObject ResourceManager::m_preCosmosSpore
	GameObject_t1113636619 * ___m_preCosmosSpore_11;
	// UnityEngine.GameObject ResourceManager::m_preThorn
	GameObject_t1113636619 * ___m_preThorn_12;
	// UnityEngine.GameObject ResourceManager::m_preExplodeEvent
	GameObject_t1113636619 * ___m_preExplodeEvent_13;
	// UnityEngine.GameObject ResourceManager::m_preRebornSpot
	GameObject_t1113636619 * ___m_preRebornSpot_14;
	// UnityEngine.Material ResourceManager::m_preBallSpineMaterial
	Material_t340375123 * ___m_preBallSpineMaterial_15;
	// UnityEngine.GameObject ResourceManager::m_preMiaoHeEffect
	GameObject_t1113636619 * ___m_preMiaoHeEffect_16;
	// UnityEngine.GameObject ResourceManager::m_preItemBuyEffect
	GameObject_t1113636619 * ___m_preItemBuyEffect_17;
	// System.String[] ResourceManager::m_arySzPlayerColor
	StringU5BU5D_t1281789340* ___m_arySzPlayerColor_18;
	// UnityEngine.Color[] ResourceManager::m_aryPlayerColors
	ColorU5BU5D_t941916413* ___m_aryPlayerColors_19;
	// UnityEngine.Material ResourceManager::m_matSpriteDefault
	Material_t340375123 * ___m_matSpriteDefault_20;
	// UnityEngine.Sprite[] ResourceManager::_arySkinTextures
	SpriteU5BU5D_t2581906349* ____arySkinTextures_21;
	// System.Collections.Generic.List`1<UnityEngine.Material> ResourceManager::m_arySkinMaterials
	List_1_t1812449865 * ___m_arySkinMaterials_24;
	// ImprovedPerlin ResourceManager::m_Perlin
	ImprovedPerlin_t2236177176 * ___m_Perlin_25;
	// UnityEngine.GameObject ResourceManager::m_preBean
	GameObject_t1113636619 * ___m_preBean_26;
	// UnityEngine.Material ResourceManager::m_matGray
	Material_t340375123 * ___m_matGray_27;
	// UnityEngine.Material ResourceManager::m_matThorn
	Material_t340375123 * ___m_matThorn_28;
	// UnityEngine.Sprite ResourceManager::m_sprThorn
	Sprite_t280657092 * ___m_sprThorn_29;
	// UnityEngine.Shader ResourceManager::m_shaderDefaultSkinShader
	Shader_t4151988712 * ___m_shaderDefaultSkinShader_32;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> ResourceManager::m_dicSkinId2Materials
	Dictionary_2_t3524055750 * ___m_dicSkinId2Materials_33;
	// UnityEngine.Sprite ResourceManager::m_sprDefault
	Sprite_t280657092 * ___m_sprDefault_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> ResourceManager::m_dicUnfoldRingMaterial
	Dictionary_2_t3524055750 * ___m_dicUnfoldRingMaterial_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.Single>> ResourceManager::m_dicSceneBeanRebornQueue
	Dictionary_2_t1758054847 * ___m_dicSceneBeanRebornQueue_40;
	// System.Collections.Generic.List`1<CGrass> ResourceManager::m_lstRecycledGrass
	List_1_t2206159218 * ___m_lstRecycledGrass_42;
	// System.Collections.Generic.List`1<CMonster> ResourceManager::m_lstRecycledSceneBall
	List_1_t3413545680 * ___m_lstRecycledSceneBall_43;
	// System.Collections.Generic.List`1<CGunsight> ResourceManager::m_lstRecycledGunsight
	List_1_t3682063150 * ___m_lstRecycledGunsight_44;
	// UnityEngine.Sprite ResourceManager::m_sprShit
	Sprite_t280657092 * ___m_sprShit_45;
	// UnityEngine.Sprite[] ResourceManager::m_aryBallSprites
	SpriteU5BU5D_t2581906349* ___m_aryBallSprites_46;
	// UnityEngine.Material[] ResourceManager::m_aryBallSkeletonMaterial
	MaterialU5BU5D_t561872642* ___m_aryBallSkeletonMaterial_48;
	// System.Collections.Generic.List`1<CMiaoHeEffect> ResourceManager::m_lstRecycledMiaoHeEffect
	List_1_t2306155490 * ___m_lstRecycledMiaoHeEffect_49;
	// System.Collections.Generic.List`1<CItemBuyEffect> ResourceManager::m_lstRecycledItemBuyEffect
	List_1_t2305279210 * ___m_lstRecycledItemBuyEffect_50;
	// System.Single ResourceManager::m_fCiChuXianDongHuaSpeed
	float ___m_fCiChuXianDongHuaSpeed_51;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonAnimation> ResourceManager::m_lstRecycledCiChuXianAni
	List_1_t870293967 * ___m_lstRecycledCiChuXianAni_52;

public:
	inline static int32_t get_offset_of_m_preCiChuXian_3() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preCiChuXian_3)); }
	inline GameObject_t1113636619 * get_m_preCiChuXian_3() const { return ___m_preCiChuXian_3; }
	inline GameObject_t1113636619 ** get_address_of_m_preCiChuXian_3() { return &___m_preCiChuXian_3; }
	inline void set_m_preCiChuXian_3(GameObject_t1113636619 * value)
	{
		___m_preCiChuXian_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCiChuXian_3), value);
	}

	inline static int32_t get_offset_of_m_aryCiChuXianDongHuaPrefabs_4() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_aryCiChuXianDongHuaPrefabs_4)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryCiChuXianDongHuaPrefabs_4() const { return ___m_aryCiChuXianDongHuaPrefabs_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryCiChuXianDongHuaPrefabs_4() { return &___m_aryCiChuXianDongHuaPrefabs_4; }
	inline void set_m_aryCiChuXianDongHuaPrefabs_4(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryCiChuXianDongHuaPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCiChuXianDongHuaPrefabs_4), value);
	}

	inline static int32_t get_offset_of_m_font_5() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_font_5)); }
	inline FontU5BU5D_t1399044585* get_m_font_5() const { return ___m_font_5; }
	inline FontU5BU5D_t1399044585** get_address_of_m_font_5() { return &___m_font_5; }
	inline void set_m_font_5(FontU5BU5D_t1399044585* value)
	{
		___m_font_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_font_5), value);
	}

	inline static int32_t get_offset_of_m_preBuff_6() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preBuff_6)); }
	inline GameObject_t1113636619 * get_m_preBuff_6() const { return ___m_preBuff_6; }
	inline GameObject_t1113636619 ** get_address_of_m_preBuff_6() { return &___m_preBuff_6; }
	inline void set_m_preBuff_6(GameObject_t1113636619 * value)
	{
		___m_preBuff_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBuff_6), value);
	}

	inline static int32_t get_offset_of_m_preGrass_7() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preGrass_7)); }
	inline GameObject_t1113636619 * get_m_preGrass_7() const { return ___m_preGrass_7; }
	inline GameObject_t1113636619 ** get_address_of_m_preGrass_7() { return &___m_preGrass_7; }
	inline void set_m_preGrass_7(GameObject_t1113636619 * value)
	{
		___m_preGrass_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGrass_7), value);
	}

	inline static int32_t get_offset_of_m_preSpray_8() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preSpray_8)); }
	inline GameObject_t1113636619 * get_m_preSpray_8() const { return ___m_preSpray_8; }
	inline GameObject_t1113636619 ** get_address_of_m_preSpray_8() { return &___m_preSpray_8; }
	inline void set_m_preSpray_8(GameObject_t1113636619 * value)
	{
		___m_preSpray_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSpray_8), value);
	}

	inline static int32_t get_offset_of_m_preSceneBall_9() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preSceneBall_9)); }
	inline GameObject_t1113636619 * get_m_preSceneBall_9() const { return ___m_preSceneBall_9; }
	inline GameObject_t1113636619 ** get_address_of_m_preSceneBall_9() { return &___m_preSceneBall_9; }
	inline void set_m_preSceneBall_9(GameObject_t1113636619 * value)
	{
		___m_preSceneBall_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSceneBall_9), value);
	}

	inline static int32_t get_offset_of_m_preGunsight_10() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preGunsight_10)); }
	inline GameObject_t1113636619 * get_m_preGunsight_10() const { return ___m_preGunsight_10; }
	inline GameObject_t1113636619 ** get_address_of_m_preGunsight_10() { return &___m_preGunsight_10; }
	inline void set_m_preGunsight_10(GameObject_t1113636619 * value)
	{
		___m_preGunsight_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGunsight_10), value);
	}

	inline static int32_t get_offset_of_m_preCosmosSpore_11() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preCosmosSpore_11)); }
	inline GameObject_t1113636619 * get_m_preCosmosSpore_11() const { return ___m_preCosmosSpore_11; }
	inline GameObject_t1113636619 ** get_address_of_m_preCosmosSpore_11() { return &___m_preCosmosSpore_11; }
	inline void set_m_preCosmosSpore_11(GameObject_t1113636619 * value)
	{
		___m_preCosmosSpore_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCosmosSpore_11), value);
	}

	inline static int32_t get_offset_of_m_preThorn_12() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preThorn_12)); }
	inline GameObject_t1113636619 * get_m_preThorn_12() const { return ___m_preThorn_12; }
	inline GameObject_t1113636619 ** get_address_of_m_preThorn_12() { return &___m_preThorn_12; }
	inline void set_m_preThorn_12(GameObject_t1113636619 * value)
	{
		___m_preThorn_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_preThorn_12), value);
	}

	inline static int32_t get_offset_of_m_preExplodeEvent_13() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preExplodeEvent_13)); }
	inline GameObject_t1113636619 * get_m_preExplodeEvent_13() const { return ___m_preExplodeEvent_13; }
	inline GameObject_t1113636619 ** get_address_of_m_preExplodeEvent_13() { return &___m_preExplodeEvent_13; }
	inline void set_m_preExplodeEvent_13(GameObject_t1113636619 * value)
	{
		___m_preExplodeEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_preExplodeEvent_13), value);
	}

	inline static int32_t get_offset_of_m_preRebornSpot_14() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preRebornSpot_14)); }
	inline GameObject_t1113636619 * get_m_preRebornSpot_14() const { return ___m_preRebornSpot_14; }
	inline GameObject_t1113636619 ** get_address_of_m_preRebornSpot_14() { return &___m_preRebornSpot_14; }
	inline void set_m_preRebornSpot_14(GameObject_t1113636619 * value)
	{
		___m_preRebornSpot_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_preRebornSpot_14), value);
	}

	inline static int32_t get_offset_of_m_preBallSpineMaterial_15() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preBallSpineMaterial_15)); }
	inline Material_t340375123 * get_m_preBallSpineMaterial_15() const { return ___m_preBallSpineMaterial_15; }
	inline Material_t340375123 ** get_address_of_m_preBallSpineMaterial_15() { return &___m_preBallSpineMaterial_15; }
	inline void set_m_preBallSpineMaterial_15(Material_t340375123 * value)
	{
		___m_preBallSpineMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBallSpineMaterial_15), value);
	}

	inline static int32_t get_offset_of_m_preMiaoHeEffect_16() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preMiaoHeEffect_16)); }
	inline GameObject_t1113636619 * get_m_preMiaoHeEffect_16() const { return ___m_preMiaoHeEffect_16; }
	inline GameObject_t1113636619 ** get_address_of_m_preMiaoHeEffect_16() { return &___m_preMiaoHeEffect_16; }
	inline void set_m_preMiaoHeEffect_16(GameObject_t1113636619 * value)
	{
		___m_preMiaoHeEffect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_preMiaoHeEffect_16), value);
	}

	inline static int32_t get_offset_of_m_preItemBuyEffect_17() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preItemBuyEffect_17)); }
	inline GameObject_t1113636619 * get_m_preItemBuyEffect_17() const { return ___m_preItemBuyEffect_17; }
	inline GameObject_t1113636619 ** get_address_of_m_preItemBuyEffect_17() { return &___m_preItemBuyEffect_17; }
	inline void set_m_preItemBuyEffect_17(GameObject_t1113636619 * value)
	{
		___m_preItemBuyEffect_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_preItemBuyEffect_17), value);
	}

	inline static int32_t get_offset_of_m_arySzPlayerColor_18() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_arySzPlayerColor_18)); }
	inline StringU5BU5D_t1281789340* get_m_arySzPlayerColor_18() const { return ___m_arySzPlayerColor_18; }
	inline StringU5BU5D_t1281789340** get_address_of_m_arySzPlayerColor_18() { return &___m_arySzPlayerColor_18; }
	inline void set_m_arySzPlayerColor_18(StringU5BU5D_t1281789340* value)
	{
		___m_arySzPlayerColor_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySzPlayerColor_18), value);
	}

	inline static int32_t get_offset_of_m_aryPlayerColors_19() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_aryPlayerColors_19)); }
	inline ColorU5BU5D_t941916413* get_m_aryPlayerColors_19() const { return ___m_aryPlayerColors_19; }
	inline ColorU5BU5D_t941916413** get_address_of_m_aryPlayerColors_19() { return &___m_aryPlayerColors_19; }
	inline void set_m_aryPlayerColors_19(ColorU5BU5D_t941916413* value)
	{
		___m_aryPlayerColors_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlayerColors_19), value);
	}

	inline static int32_t get_offset_of_m_matSpriteDefault_20() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_matSpriteDefault_20)); }
	inline Material_t340375123 * get_m_matSpriteDefault_20() const { return ___m_matSpriteDefault_20; }
	inline Material_t340375123 ** get_address_of_m_matSpriteDefault_20() { return &___m_matSpriteDefault_20; }
	inline void set_m_matSpriteDefault_20(Material_t340375123 * value)
	{
		___m_matSpriteDefault_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_matSpriteDefault_20), value);
	}

	inline static int32_t get_offset_of__arySkinTextures_21() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ____arySkinTextures_21)); }
	inline SpriteU5BU5D_t2581906349* get__arySkinTextures_21() const { return ____arySkinTextures_21; }
	inline SpriteU5BU5D_t2581906349** get_address_of__arySkinTextures_21() { return &____arySkinTextures_21; }
	inline void set__arySkinTextures_21(SpriteU5BU5D_t2581906349* value)
	{
		____arySkinTextures_21 = value;
		Il2CppCodeGenWriteBarrier((&____arySkinTextures_21), value);
	}

	inline static int32_t get_offset_of_m_arySkinMaterials_24() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_arySkinMaterials_24)); }
	inline List_1_t1812449865 * get_m_arySkinMaterials_24() const { return ___m_arySkinMaterials_24; }
	inline List_1_t1812449865 ** get_address_of_m_arySkinMaterials_24() { return &___m_arySkinMaterials_24; }
	inline void set_m_arySkinMaterials_24(List_1_t1812449865 * value)
	{
		___m_arySkinMaterials_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkinMaterials_24), value);
	}

	inline static int32_t get_offset_of_m_Perlin_25() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_Perlin_25)); }
	inline ImprovedPerlin_t2236177176 * get_m_Perlin_25() const { return ___m_Perlin_25; }
	inline ImprovedPerlin_t2236177176 ** get_address_of_m_Perlin_25() { return &___m_Perlin_25; }
	inline void set_m_Perlin_25(ImprovedPerlin_t2236177176 * value)
	{
		___m_Perlin_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Perlin_25), value);
	}

	inline static int32_t get_offset_of_m_preBean_26() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_preBean_26)); }
	inline GameObject_t1113636619 * get_m_preBean_26() const { return ___m_preBean_26; }
	inline GameObject_t1113636619 ** get_address_of_m_preBean_26() { return &___m_preBean_26; }
	inline void set_m_preBean_26(GameObject_t1113636619 * value)
	{
		___m_preBean_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBean_26), value);
	}

	inline static int32_t get_offset_of_m_matGray_27() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_matGray_27)); }
	inline Material_t340375123 * get_m_matGray_27() const { return ___m_matGray_27; }
	inline Material_t340375123 ** get_address_of_m_matGray_27() { return &___m_matGray_27; }
	inline void set_m_matGray_27(Material_t340375123 * value)
	{
		___m_matGray_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_matGray_27), value);
	}

	inline static int32_t get_offset_of_m_matThorn_28() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_matThorn_28)); }
	inline Material_t340375123 * get_m_matThorn_28() const { return ___m_matThorn_28; }
	inline Material_t340375123 ** get_address_of_m_matThorn_28() { return &___m_matThorn_28; }
	inline void set_m_matThorn_28(Material_t340375123 * value)
	{
		___m_matThorn_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_matThorn_28), value);
	}

	inline static int32_t get_offset_of_m_sprThorn_29() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_sprThorn_29)); }
	inline Sprite_t280657092 * get_m_sprThorn_29() const { return ___m_sprThorn_29; }
	inline Sprite_t280657092 ** get_address_of_m_sprThorn_29() { return &___m_sprThorn_29; }
	inline void set_m_sprThorn_29(Sprite_t280657092 * value)
	{
		___m_sprThorn_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprThorn_29), value);
	}

	inline static int32_t get_offset_of_m_shaderDefaultSkinShader_32() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_shaderDefaultSkinShader_32)); }
	inline Shader_t4151988712 * get_m_shaderDefaultSkinShader_32() const { return ___m_shaderDefaultSkinShader_32; }
	inline Shader_t4151988712 ** get_address_of_m_shaderDefaultSkinShader_32() { return &___m_shaderDefaultSkinShader_32; }
	inline void set_m_shaderDefaultSkinShader_32(Shader_t4151988712 * value)
	{
		___m_shaderDefaultSkinShader_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_shaderDefaultSkinShader_32), value);
	}

	inline static int32_t get_offset_of_m_dicSkinId2Materials_33() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_dicSkinId2Materials_33)); }
	inline Dictionary_2_t3524055750 * get_m_dicSkinId2Materials_33() const { return ___m_dicSkinId2Materials_33; }
	inline Dictionary_2_t3524055750 ** get_address_of_m_dicSkinId2Materials_33() { return &___m_dicSkinId2Materials_33; }
	inline void set_m_dicSkinId2Materials_33(Dictionary_2_t3524055750 * value)
	{
		___m_dicSkinId2Materials_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkinId2Materials_33), value);
	}

	inline static int32_t get_offset_of_m_sprDefault_34() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_sprDefault_34)); }
	inline Sprite_t280657092 * get_m_sprDefault_34() const { return ___m_sprDefault_34; }
	inline Sprite_t280657092 ** get_address_of_m_sprDefault_34() { return &___m_sprDefault_34; }
	inline void set_m_sprDefault_34(Sprite_t280657092 * value)
	{
		___m_sprDefault_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprDefault_34), value);
	}

	inline static int32_t get_offset_of_m_dicUnfoldRingMaterial_35() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_dicUnfoldRingMaterial_35)); }
	inline Dictionary_2_t3524055750 * get_m_dicUnfoldRingMaterial_35() const { return ___m_dicUnfoldRingMaterial_35; }
	inline Dictionary_2_t3524055750 ** get_address_of_m_dicUnfoldRingMaterial_35() { return &___m_dicUnfoldRingMaterial_35; }
	inline void set_m_dicUnfoldRingMaterial_35(Dictionary_2_t3524055750 * value)
	{
		___m_dicUnfoldRingMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicUnfoldRingMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_dicSceneBeanRebornQueue_40() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_dicSceneBeanRebornQueue_40)); }
	inline Dictionary_2_t1758054847 * get_m_dicSceneBeanRebornQueue_40() const { return ___m_dicSceneBeanRebornQueue_40; }
	inline Dictionary_2_t1758054847 ** get_address_of_m_dicSceneBeanRebornQueue_40() { return &___m_dicSceneBeanRebornQueue_40; }
	inline void set_m_dicSceneBeanRebornQueue_40(Dictionary_2_t1758054847 * value)
	{
		___m_dicSceneBeanRebornQueue_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSceneBeanRebornQueue_40), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledGrass_42() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_lstRecycledGrass_42)); }
	inline List_1_t2206159218 * get_m_lstRecycledGrass_42() const { return ___m_lstRecycledGrass_42; }
	inline List_1_t2206159218 ** get_address_of_m_lstRecycledGrass_42() { return &___m_lstRecycledGrass_42; }
	inline void set_m_lstRecycledGrass_42(List_1_t2206159218 * value)
	{
		___m_lstRecycledGrass_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledGrass_42), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledSceneBall_43() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_lstRecycledSceneBall_43)); }
	inline List_1_t3413545680 * get_m_lstRecycledSceneBall_43() const { return ___m_lstRecycledSceneBall_43; }
	inline List_1_t3413545680 ** get_address_of_m_lstRecycledSceneBall_43() { return &___m_lstRecycledSceneBall_43; }
	inline void set_m_lstRecycledSceneBall_43(List_1_t3413545680 * value)
	{
		___m_lstRecycledSceneBall_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledSceneBall_43), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledGunsight_44() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_lstRecycledGunsight_44)); }
	inline List_1_t3682063150 * get_m_lstRecycledGunsight_44() const { return ___m_lstRecycledGunsight_44; }
	inline List_1_t3682063150 ** get_address_of_m_lstRecycledGunsight_44() { return &___m_lstRecycledGunsight_44; }
	inline void set_m_lstRecycledGunsight_44(List_1_t3682063150 * value)
	{
		___m_lstRecycledGunsight_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledGunsight_44), value);
	}

	inline static int32_t get_offset_of_m_sprShit_45() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_sprShit_45)); }
	inline Sprite_t280657092 * get_m_sprShit_45() const { return ___m_sprShit_45; }
	inline Sprite_t280657092 ** get_address_of_m_sprShit_45() { return &___m_sprShit_45; }
	inline void set_m_sprShit_45(Sprite_t280657092 * value)
	{
		___m_sprShit_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprShit_45), value);
	}

	inline static int32_t get_offset_of_m_aryBallSprites_46() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_aryBallSprites_46)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryBallSprites_46() const { return ___m_aryBallSprites_46; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryBallSprites_46() { return &___m_aryBallSprites_46; }
	inline void set_m_aryBallSprites_46(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryBallSprites_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBallSprites_46), value);
	}

	inline static int32_t get_offset_of_m_aryBallSkeletonMaterial_48() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_aryBallSkeletonMaterial_48)); }
	inline MaterialU5BU5D_t561872642* get_m_aryBallSkeletonMaterial_48() const { return ___m_aryBallSkeletonMaterial_48; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_aryBallSkeletonMaterial_48() { return &___m_aryBallSkeletonMaterial_48; }
	inline void set_m_aryBallSkeletonMaterial_48(MaterialU5BU5D_t561872642* value)
	{
		___m_aryBallSkeletonMaterial_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBallSkeletonMaterial_48), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledMiaoHeEffect_49() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_lstRecycledMiaoHeEffect_49)); }
	inline List_1_t2306155490 * get_m_lstRecycledMiaoHeEffect_49() const { return ___m_lstRecycledMiaoHeEffect_49; }
	inline List_1_t2306155490 ** get_address_of_m_lstRecycledMiaoHeEffect_49() { return &___m_lstRecycledMiaoHeEffect_49; }
	inline void set_m_lstRecycledMiaoHeEffect_49(List_1_t2306155490 * value)
	{
		___m_lstRecycledMiaoHeEffect_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledMiaoHeEffect_49), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledItemBuyEffect_50() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_lstRecycledItemBuyEffect_50)); }
	inline List_1_t2305279210 * get_m_lstRecycledItemBuyEffect_50() const { return ___m_lstRecycledItemBuyEffect_50; }
	inline List_1_t2305279210 ** get_address_of_m_lstRecycledItemBuyEffect_50() { return &___m_lstRecycledItemBuyEffect_50; }
	inline void set_m_lstRecycledItemBuyEffect_50(List_1_t2305279210 * value)
	{
		___m_lstRecycledItemBuyEffect_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledItemBuyEffect_50), value);
	}

	inline static int32_t get_offset_of_m_fCiChuXianDongHuaSpeed_51() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_fCiChuXianDongHuaSpeed_51)); }
	inline float get_m_fCiChuXianDongHuaSpeed_51() const { return ___m_fCiChuXianDongHuaSpeed_51; }
	inline float* get_address_of_m_fCiChuXianDongHuaSpeed_51() { return &___m_fCiChuXianDongHuaSpeed_51; }
	inline void set_m_fCiChuXianDongHuaSpeed_51(float value)
	{
		___m_fCiChuXianDongHuaSpeed_51 = value;
	}

	inline static int32_t get_offset_of_m_lstRecycledCiChuXianAni_52() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614, ___m_lstRecycledCiChuXianAni_52)); }
	inline List_1_t870293967 * get_m_lstRecycledCiChuXianAni_52() const { return ___m_lstRecycledCiChuXianAni_52; }
	inline List_1_t870293967 ** get_address_of_m_lstRecycledCiChuXianAni_52() { return &___m_lstRecycledCiChuXianAni_52; }
	inline void set_m_lstRecycledCiChuXianAni_52(List_1_t870293967 * value)
	{
		___m_lstRecycledCiChuXianAni_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledCiChuXianAni_52), value);
	}
};

struct ResourceManager_t484397614_StaticFields
{
public:
	// ResourceManager ResourceManager::s_Instance
	ResourceManager_t484397614 * ___s_Instance_2;
	// System.String[] ResourceManager::m_arySZPlayerColor
	StringU5BU5D_t1281789340* ___m_arySZPlayerColor_30;
	// UnityEngine.Color[] ResourceManager::m_aryColorPlayerColor
	ColorU5BU5D_t941916413* ___m_aryColorPlayerColor_31;
	// System.Collections.Generic.List`1<SpitBallTarget> ResourceManager::m_lstRecycledTarget
	List_1_t1174412619 * ___m_lstRecycledTarget_36;
	// System.Collections.Generic.List`1<CMonster> ResourceManager::m_lstRecycledBean
	List_1_t3413545680 * ___m_lstRecycledBean_37;
	// System.Collections.Generic.List`1<CMonster> ResourceManager::m_lstRecycledThorn
	List_1_t3413545680 * ___m_lstRecycledThorn_38;
	// System.Int64 ResourceManager::m_nGuidCount
	int64_t ___m_nGuidCount_39;
	// System.Collections.Generic.List`1<CBuff> ResourceManager::m_lstRecycledBuff
	List_1_t3012865878 * ___m_lstRecycledBuff_41;
	// UnityEngine.Color ResourceManager::tempColor
	Color_t2555686324  ___tempColor_47;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___s_Instance_2)); }
	inline ResourceManager_t484397614 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline ResourceManager_t484397614 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(ResourceManager_t484397614 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_m_arySZPlayerColor_30() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_arySZPlayerColor_30)); }
	inline StringU5BU5D_t1281789340* get_m_arySZPlayerColor_30() const { return ___m_arySZPlayerColor_30; }
	inline StringU5BU5D_t1281789340** get_address_of_m_arySZPlayerColor_30() { return &___m_arySZPlayerColor_30; }
	inline void set_m_arySZPlayerColor_30(StringU5BU5D_t1281789340* value)
	{
		___m_arySZPlayerColor_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySZPlayerColor_30), value);
	}

	inline static int32_t get_offset_of_m_aryColorPlayerColor_31() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_aryColorPlayerColor_31)); }
	inline ColorU5BU5D_t941916413* get_m_aryColorPlayerColor_31() const { return ___m_aryColorPlayerColor_31; }
	inline ColorU5BU5D_t941916413** get_address_of_m_aryColorPlayerColor_31() { return &___m_aryColorPlayerColor_31; }
	inline void set_m_aryColorPlayerColor_31(ColorU5BU5D_t941916413* value)
	{
		___m_aryColorPlayerColor_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryColorPlayerColor_31), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledTarget_36() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_lstRecycledTarget_36)); }
	inline List_1_t1174412619 * get_m_lstRecycledTarget_36() const { return ___m_lstRecycledTarget_36; }
	inline List_1_t1174412619 ** get_address_of_m_lstRecycledTarget_36() { return &___m_lstRecycledTarget_36; }
	inline void set_m_lstRecycledTarget_36(List_1_t1174412619 * value)
	{
		___m_lstRecycledTarget_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledTarget_36), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledBean_37() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_lstRecycledBean_37)); }
	inline List_1_t3413545680 * get_m_lstRecycledBean_37() const { return ___m_lstRecycledBean_37; }
	inline List_1_t3413545680 ** get_address_of_m_lstRecycledBean_37() { return &___m_lstRecycledBean_37; }
	inline void set_m_lstRecycledBean_37(List_1_t3413545680 * value)
	{
		___m_lstRecycledBean_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledBean_37), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledThorn_38() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_lstRecycledThorn_38)); }
	inline List_1_t3413545680 * get_m_lstRecycledThorn_38() const { return ___m_lstRecycledThorn_38; }
	inline List_1_t3413545680 ** get_address_of_m_lstRecycledThorn_38() { return &___m_lstRecycledThorn_38; }
	inline void set_m_lstRecycledThorn_38(List_1_t3413545680 * value)
	{
		___m_lstRecycledThorn_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledThorn_38), value);
	}

	inline static int32_t get_offset_of_m_nGuidCount_39() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_nGuidCount_39)); }
	inline int64_t get_m_nGuidCount_39() const { return ___m_nGuidCount_39; }
	inline int64_t* get_address_of_m_nGuidCount_39() { return &___m_nGuidCount_39; }
	inline void set_m_nGuidCount_39(int64_t value)
	{
		___m_nGuidCount_39 = value;
	}

	inline static int32_t get_offset_of_m_lstRecycledBuff_41() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___m_lstRecycledBuff_41)); }
	inline List_1_t3012865878 * get_m_lstRecycledBuff_41() const { return ___m_lstRecycledBuff_41; }
	inline List_1_t3012865878 ** get_address_of_m_lstRecycledBuff_41() { return &___m_lstRecycledBuff_41; }
	inline void set_m_lstRecycledBuff_41(List_1_t3012865878 * value)
	{
		___m_lstRecycledBuff_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledBuff_41), value);
	}

	inline static int32_t get_offset_of_tempColor_47() { return static_cast<int32_t>(offsetof(ResourceManager_t484397614_StaticFields, ___tempColor_47)); }
	inline Color_t2555686324  get_tempColor_47() const { return ___tempColor_47; }
	inline Color_t2555686324 * get_address_of_tempColor_47() { return &___tempColor_47; }
	inline void set_tempColor_47(Color_t2555686324  value)
	{
		___tempColor_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEMANAGER_T484397614_H
#ifndef CZHANGYU_T3000622058_H
#define CZHANGYU_T3000622058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CZhangYu
struct  CZhangYu_t3000622058  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CZhangYu::m_fZhaYanJianGe
	float ___m_fZhaYanJianGe_2;
	// CFrameAnimationEffect CZhangYu::m_faZhaYan
	CFrameAnimationEffect_t443605508 * ___m_faZhaYan_3;
	// UnityEngine.SpriteRenderer CZhangYu::_srBody
	SpriteRenderer_t3235626157 * ____srBody_4;
	// System.Single CZhangYu::m_fZhaYanTimeElapse
	float ___m_fZhaYanTimeElapse_5;

public:
	inline static int32_t get_offset_of_m_fZhaYanJianGe_2() { return static_cast<int32_t>(offsetof(CZhangYu_t3000622058, ___m_fZhaYanJianGe_2)); }
	inline float get_m_fZhaYanJianGe_2() const { return ___m_fZhaYanJianGe_2; }
	inline float* get_address_of_m_fZhaYanJianGe_2() { return &___m_fZhaYanJianGe_2; }
	inline void set_m_fZhaYanJianGe_2(float value)
	{
		___m_fZhaYanJianGe_2 = value;
	}

	inline static int32_t get_offset_of_m_faZhaYan_3() { return static_cast<int32_t>(offsetof(CZhangYu_t3000622058, ___m_faZhaYan_3)); }
	inline CFrameAnimationEffect_t443605508 * get_m_faZhaYan_3() const { return ___m_faZhaYan_3; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of_m_faZhaYan_3() { return &___m_faZhaYan_3; }
	inline void set_m_faZhaYan_3(CFrameAnimationEffect_t443605508 * value)
	{
		___m_faZhaYan_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_faZhaYan_3), value);
	}

	inline static int32_t get_offset_of__srBody_4() { return static_cast<int32_t>(offsetof(CZhangYu_t3000622058, ____srBody_4)); }
	inline SpriteRenderer_t3235626157 * get__srBody_4() const { return ____srBody_4; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srBody_4() { return &____srBody_4; }
	inline void set__srBody_4(SpriteRenderer_t3235626157 * value)
	{
		____srBody_4 = value;
		Il2CppCodeGenWriteBarrier((&____srBody_4), value);
	}

	inline static int32_t get_offset_of_m_fZhaYanTimeElapse_5() { return static_cast<int32_t>(offsetof(CZhangYu_t3000622058, ___m_fZhaYanTimeElapse_5)); }
	inline float get_m_fZhaYanTimeElapse_5() const { return ___m_fZhaYanTimeElapse_5; }
	inline float* get_address_of_m_fZhaYanTimeElapse_5() { return &___m_fZhaYanTimeElapse_5; }
	inline void set_m_fZhaYanTimeElapse_5(float value)
	{
		___m_fZhaYanTimeElapse_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CZHANGYU_T3000622058_H
#ifndef CCHATMESSAGE_T3066647275_H
#define CCHATMESSAGE_T3066647275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CChatMessage
struct  CChatMessage_t3066647275  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CChatMessage::_txtContent
	Text_t1901882714 * ____txtContent_2;
	// System.Single CChatMessage::m_fHeight
	float ___m_fHeight_6;

public:
	inline static int32_t get_offset_of__txtContent_2() { return static_cast<int32_t>(offsetof(CChatMessage_t3066647275, ____txtContent_2)); }
	inline Text_t1901882714 * get__txtContent_2() const { return ____txtContent_2; }
	inline Text_t1901882714 ** get_address_of__txtContent_2() { return &____txtContent_2; }
	inline void set__txtContent_2(Text_t1901882714 * value)
	{
		____txtContent_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_2), value);
	}

	inline static int32_t get_offset_of_m_fHeight_6() { return static_cast<int32_t>(offsetof(CChatMessage_t3066647275, ___m_fHeight_6)); }
	inline float get_m_fHeight_6() const { return ___m_fHeight_6; }
	inline float* get_address_of_m_fHeight_6() { return &___m_fHeight_6; }
	inline void set_m_fHeight_6(float value)
	{
		___m_fHeight_6 = value;
	}
};

struct CChatMessage_t3066647275_StaticFields
{
public:
	// UnityEngine.Vector2 CChatMessage::vecTempSize
	Vector2_t2156229523  ___vecTempSize_3;
	// UnityEngine.Vector3 CChatMessage::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// UnityEngine.Vector3 CChatMessage::vecTempScale
	Vector3_t3722313464  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempSize_3() { return static_cast<int32_t>(offsetof(CChatMessage_t3066647275_StaticFields, ___vecTempSize_3)); }
	inline Vector2_t2156229523  get_vecTempSize_3() const { return ___vecTempSize_3; }
	inline Vector2_t2156229523 * get_address_of_vecTempSize_3() { return &___vecTempSize_3; }
	inline void set_vecTempSize_3(Vector2_t2156229523  value)
	{
		___vecTempSize_3 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(CChatMessage_t3066647275_StaticFields, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(CChatMessage_t3066647275_StaticFields, ___vecTempScale_5)); }
	inline Vector3_t3722313464  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_t3722313464  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCHATMESSAGE_T3066647275_H
#ifndef ACCOUNTDATA_T1002028515_H
#define ACCOUNTDATA_T1002028515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountData
struct  AccountData_t1002028515  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean AccountData::m_bLocalResLoaded
	bool ___m_bLocalResLoaded_3;
	// System.Boolean AccountData::m_bRemoteResLoaded
	bool ___m_bRemoteResLoaded_4;
	// UnityEngine.Sprite AccountData::m_sprNoSkin
	Sprite_t280657092 * ___m_sprNoSkin_5;
	// WebAuthAPI.Authenticator AccountData::authenticator
	Authenticator_t530988528 * ___authenticator_6;
	// WebAuthAPI.Account AccountData::account
	Account_t400769301 * ___account_7;
	// WebAuthAPI.Charge AccountData::charge
	Charge_t1653125646 * ___charge_8;
	// WebAuthAPI.Market AccountData::market
	Market_t538055513 * ___market_9;
	// WebAuthAPI.MarketBuymentJSON[] AccountData::buyments
	MarketBuymentJSONU5BU5D_t1759546967* ___buyments_10;
	// ResourceLoader AccountData::localeLoader
	ResourceLoader_t3764463624 * ___localeLoader_11;
	// ResourceLoader AccountData::remoteLoader
	ResourceLoader_t3764463624 * ___remoteLoader_12;
	// AccountData/SkinJSON[] AccountData::skins
	SkinJSONU5BU5D_t732741759* ___skins_13;
	// WebAuthAPI.ChargeProductJSON[] AccountData::products
	ChargeProductJSONU5BU5D_t3214290606* ___products_14;
	// System.UInt32[] AccountData::m_aryMoney
	UInt32U5BU5D_t2770800703* ___m_aryMoney_19;
	// UnityEngine.AsyncOperation AccountData::asyncLoad
	AsyncOperation_t1445031843 * ___asyncLoad_20;
	// UnityEngine.UI.Text AccountData::_txtCounting
	Text_t1901882714 * ____txtCounting_21;
	// UnityEngine.UI.Button AccountData::_btnCounting
	Button_t4055032469 * ____btnCounting_22;
	// System.Single AccountData::m_fSmsButtonCountingTime
	float ___m_fSmsButtonCountingTime_23;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> AccountData::m_dicBoughtItems
	Dictionary_2_t1839659084 * ___m_dicBoughtItems_24;
	// UnityEngine.UI.InputField AccountData::_inputTemp
	InputField_t3762917431 * ____inputTemp_25;

public:
	inline static int32_t get_offset_of_m_bLocalResLoaded_3() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___m_bLocalResLoaded_3)); }
	inline bool get_m_bLocalResLoaded_3() const { return ___m_bLocalResLoaded_3; }
	inline bool* get_address_of_m_bLocalResLoaded_3() { return &___m_bLocalResLoaded_3; }
	inline void set_m_bLocalResLoaded_3(bool value)
	{
		___m_bLocalResLoaded_3 = value;
	}

	inline static int32_t get_offset_of_m_bRemoteResLoaded_4() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___m_bRemoteResLoaded_4)); }
	inline bool get_m_bRemoteResLoaded_4() const { return ___m_bRemoteResLoaded_4; }
	inline bool* get_address_of_m_bRemoteResLoaded_4() { return &___m_bRemoteResLoaded_4; }
	inline void set_m_bRemoteResLoaded_4(bool value)
	{
		___m_bRemoteResLoaded_4 = value;
	}

	inline static int32_t get_offset_of_m_sprNoSkin_5() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___m_sprNoSkin_5)); }
	inline Sprite_t280657092 * get_m_sprNoSkin_5() const { return ___m_sprNoSkin_5; }
	inline Sprite_t280657092 ** get_address_of_m_sprNoSkin_5() { return &___m_sprNoSkin_5; }
	inline void set_m_sprNoSkin_5(Sprite_t280657092 * value)
	{
		___m_sprNoSkin_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprNoSkin_5), value);
	}

	inline static int32_t get_offset_of_authenticator_6() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___authenticator_6)); }
	inline Authenticator_t530988528 * get_authenticator_6() const { return ___authenticator_6; }
	inline Authenticator_t530988528 ** get_address_of_authenticator_6() { return &___authenticator_6; }
	inline void set_authenticator_6(Authenticator_t530988528 * value)
	{
		___authenticator_6 = value;
		Il2CppCodeGenWriteBarrier((&___authenticator_6), value);
	}

	inline static int32_t get_offset_of_account_7() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___account_7)); }
	inline Account_t400769301 * get_account_7() const { return ___account_7; }
	inline Account_t400769301 ** get_address_of_account_7() { return &___account_7; }
	inline void set_account_7(Account_t400769301 * value)
	{
		___account_7 = value;
		Il2CppCodeGenWriteBarrier((&___account_7), value);
	}

	inline static int32_t get_offset_of_charge_8() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___charge_8)); }
	inline Charge_t1653125646 * get_charge_8() const { return ___charge_8; }
	inline Charge_t1653125646 ** get_address_of_charge_8() { return &___charge_8; }
	inline void set_charge_8(Charge_t1653125646 * value)
	{
		___charge_8 = value;
		Il2CppCodeGenWriteBarrier((&___charge_8), value);
	}

	inline static int32_t get_offset_of_market_9() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___market_9)); }
	inline Market_t538055513 * get_market_9() const { return ___market_9; }
	inline Market_t538055513 ** get_address_of_market_9() { return &___market_9; }
	inline void set_market_9(Market_t538055513 * value)
	{
		___market_9 = value;
		Il2CppCodeGenWriteBarrier((&___market_9), value);
	}

	inline static int32_t get_offset_of_buyments_10() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___buyments_10)); }
	inline MarketBuymentJSONU5BU5D_t1759546967* get_buyments_10() const { return ___buyments_10; }
	inline MarketBuymentJSONU5BU5D_t1759546967** get_address_of_buyments_10() { return &___buyments_10; }
	inline void set_buyments_10(MarketBuymentJSONU5BU5D_t1759546967* value)
	{
		___buyments_10 = value;
		Il2CppCodeGenWriteBarrier((&___buyments_10), value);
	}

	inline static int32_t get_offset_of_localeLoader_11() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___localeLoader_11)); }
	inline ResourceLoader_t3764463624 * get_localeLoader_11() const { return ___localeLoader_11; }
	inline ResourceLoader_t3764463624 ** get_address_of_localeLoader_11() { return &___localeLoader_11; }
	inline void set_localeLoader_11(ResourceLoader_t3764463624 * value)
	{
		___localeLoader_11 = value;
		Il2CppCodeGenWriteBarrier((&___localeLoader_11), value);
	}

	inline static int32_t get_offset_of_remoteLoader_12() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___remoteLoader_12)); }
	inline ResourceLoader_t3764463624 * get_remoteLoader_12() const { return ___remoteLoader_12; }
	inline ResourceLoader_t3764463624 ** get_address_of_remoteLoader_12() { return &___remoteLoader_12; }
	inline void set_remoteLoader_12(ResourceLoader_t3764463624 * value)
	{
		___remoteLoader_12 = value;
		Il2CppCodeGenWriteBarrier((&___remoteLoader_12), value);
	}

	inline static int32_t get_offset_of_skins_13() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___skins_13)); }
	inline SkinJSONU5BU5D_t732741759* get_skins_13() const { return ___skins_13; }
	inline SkinJSONU5BU5D_t732741759** get_address_of_skins_13() { return &___skins_13; }
	inline void set_skins_13(SkinJSONU5BU5D_t732741759* value)
	{
		___skins_13 = value;
		Il2CppCodeGenWriteBarrier((&___skins_13), value);
	}

	inline static int32_t get_offset_of_products_14() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___products_14)); }
	inline ChargeProductJSONU5BU5D_t3214290606* get_products_14() const { return ___products_14; }
	inline ChargeProductJSONU5BU5D_t3214290606** get_address_of_products_14() { return &___products_14; }
	inline void set_products_14(ChargeProductJSONU5BU5D_t3214290606* value)
	{
		___products_14 = value;
		Il2CppCodeGenWriteBarrier((&___products_14), value);
	}

	inline static int32_t get_offset_of_m_aryMoney_19() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___m_aryMoney_19)); }
	inline UInt32U5BU5D_t2770800703* get_m_aryMoney_19() const { return ___m_aryMoney_19; }
	inline UInt32U5BU5D_t2770800703** get_address_of_m_aryMoney_19() { return &___m_aryMoney_19; }
	inline void set_m_aryMoney_19(UInt32U5BU5D_t2770800703* value)
	{
		___m_aryMoney_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoney_19), value);
	}

	inline static int32_t get_offset_of_asyncLoad_20() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___asyncLoad_20)); }
	inline AsyncOperation_t1445031843 * get_asyncLoad_20() const { return ___asyncLoad_20; }
	inline AsyncOperation_t1445031843 ** get_address_of_asyncLoad_20() { return &___asyncLoad_20; }
	inline void set_asyncLoad_20(AsyncOperation_t1445031843 * value)
	{
		___asyncLoad_20 = value;
		Il2CppCodeGenWriteBarrier((&___asyncLoad_20), value);
	}

	inline static int32_t get_offset_of__txtCounting_21() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ____txtCounting_21)); }
	inline Text_t1901882714 * get__txtCounting_21() const { return ____txtCounting_21; }
	inline Text_t1901882714 ** get_address_of__txtCounting_21() { return &____txtCounting_21; }
	inline void set__txtCounting_21(Text_t1901882714 * value)
	{
		____txtCounting_21 = value;
		Il2CppCodeGenWriteBarrier((&____txtCounting_21), value);
	}

	inline static int32_t get_offset_of__btnCounting_22() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ____btnCounting_22)); }
	inline Button_t4055032469 * get__btnCounting_22() const { return ____btnCounting_22; }
	inline Button_t4055032469 ** get_address_of__btnCounting_22() { return &____btnCounting_22; }
	inline void set__btnCounting_22(Button_t4055032469 * value)
	{
		____btnCounting_22 = value;
		Il2CppCodeGenWriteBarrier((&____btnCounting_22), value);
	}

	inline static int32_t get_offset_of_m_fSmsButtonCountingTime_23() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___m_fSmsButtonCountingTime_23)); }
	inline float get_m_fSmsButtonCountingTime_23() const { return ___m_fSmsButtonCountingTime_23; }
	inline float* get_address_of_m_fSmsButtonCountingTime_23() { return &___m_fSmsButtonCountingTime_23; }
	inline void set_m_fSmsButtonCountingTime_23(float value)
	{
		___m_fSmsButtonCountingTime_23 = value;
	}

	inline static int32_t get_offset_of_m_dicBoughtItems_24() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ___m_dicBoughtItems_24)); }
	inline Dictionary_2_t1839659084 * get_m_dicBoughtItems_24() const { return ___m_dicBoughtItems_24; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicBoughtItems_24() { return &___m_dicBoughtItems_24; }
	inline void set_m_dicBoughtItems_24(Dictionary_2_t1839659084 * value)
	{
		___m_dicBoughtItems_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBoughtItems_24), value);
	}

	inline static int32_t get_offset_of__inputTemp_25() { return static_cast<int32_t>(offsetof(AccountData_t1002028515, ____inputTemp_25)); }
	inline InputField_t3762917431 * get__inputTemp_25() const { return ____inputTemp_25; }
	inline InputField_t3762917431 ** get_address_of__inputTemp_25() { return &____inputTemp_25; }
	inline void set__inputTemp_25(InputField_t3762917431 * value)
	{
		____inputTemp_25 = value;
		Il2CppCodeGenWriteBarrier((&____inputTemp_25), value);
	}
};

struct AccountData_t1002028515_StaticFields
{
public:
	// AccountData AccountData::s_Instance
	AccountData_t1002028515 * ___s_Instance_2;
	// System.Func`5<System.String,System.String,System.Object,System.Action`2<System.Int32,System.Object>,System.Collections.IEnumerator> AccountData::<>f__mg$cache0
	Func_5_t696329592 * ___U3CU3Ef__mgU24cache0_26;
	// System.Action`1<System.Int32> AccountData::<>f__am$cache0
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache0_27;
	// System.Action`1<System.Int32> AccountData::<>f__am$cache1
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache1_28;
	// System.Action`1<System.Int32> AccountData::<>f__am$cache2
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache2_29;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(AccountData_t1002028515_StaticFields, ___s_Instance_2)); }
	inline AccountData_t1002028515 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline AccountData_t1002028515 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(AccountData_t1002028515 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_26() { return static_cast<int32_t>(offsetof(AccountData_t1002028515_StaticFields, ___U3CU3Ef__mgU24cache0_26)); }
	inline Func_5_t696329592 * get_U3CU3Ef__mgU24cache0_26() const { return ___U3CU3Ef__mgU24cache0_26; }
	inline Func_5_t696329592 ** get_address_of_U3CU3Ef__mgU24cache0_26() { return &___U3CU3Ef__mgU24cache0_26; }
	inline void set_U3CU3Ef__mgU24cache0_26(Func_5_t696329592 * value)
	{
		___U3CU3Ef__mgU24cache0_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_27() { return static_cast<int32_t>(offsetof(AccountData_t1002028515_StaticFields, ___U3CU3Ef__amU24cache0_27)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache0_27() const { return ___U3CU3Ef__amU24cache0_27; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache0_27() { return &___U3CU3Ef__amU24cache0_27; }
	inline void set_U3CU3Ef__amU24cache0_27(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache0_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_28() { return static_cast<int32_t>(offsetof(AccountData_t1002028515_StaticFields, ___U3CU3Ef__amU24cache1_28)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache1_28() const { return ___U3CU3Ef__amU24cache1_28; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache1_28() { return &___U3CU3Ef__amU24cache1_28; }
	inline void set_U3CU3Ef__amU24cache1_28(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache1_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_29() { return static_cast<int32_t>(offsetof(AccountData_t1002028515_StaticFields, ___U3CU3Ef__amU24cache2_29)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache2_29() const { return ___U3CU3Ef__amU24cache2_29; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache2_29() { return &___U3CU3Ef__amU24cache2_29; }
	inline void set_U3CU3Ef__amU24cache2_29(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache2_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNTDATA_T1002028515_H
#ifndef QUITONESCAPEORBACK_T3171497686_H
#define QUITONESCAPEORBACK_T3171497686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuitOnEscapeOrBack
struct  QuitOnEscapeOrBack_t3171497686  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUITONESCAPEORBACK_T3171497686_H
#ifndef MAPOBJ_T1733252447_H
#define MAPOBJ_T1733252447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapObj
struct  MapObj_t1733252447  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MapObj::m_fRotatoinZ
	float ___m_fRotatoinZ_2;
	// System.Single MapObj::m_fLiveTime
	float ___m_fLiveTime_3;
	// System.Boolean MapObj::m_bImortal
	bool ___m_bImortal_4;
	// UnityEngine.Vector3 MapObj::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Vector2 MapObj::_speed
	Vector2_t2156229523  ____speed_6;
	// System.Boolean MapObj::m_bDestroyed
	bool ___m_bDestroyed_7;
	// UnityEngine.Vector2 MapObj::m_Direction
	Vector2_t2156229523  ___m_Direction_8;
	// MapObj/eMapObjType MapObj::m_eObjType
	int32_t ___m_eObjType_9;
	// System.Single MapObj::m_fInitSpeed
	float ___m_fInitSpeed_10;
	// System.Single MapObj::m_fAccelerate
	float ___m_fAccelerate_11;
	// System.Single MapObj::m_fDirection
	float ___m_fDirection_12;
	// System.Boolean MapObj::m_bMoving
	bool ___m_bMoving_13;
	// System.Boolean MapObj::m_bEjecting
	bool ___m_bEjecting_14;
	// System.Single MapObj::m_fEjectDistance
	float ___m_fEjectDistance_15;
	// UnityEngine.Vector2 MapObj::m_vEjectStartPos
	Vector2_t2156229523  ___m_vEjectStartPos_16;

public:
	inline static int32_t get_offset_of_m_fRotatoinZ_2() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fRotatoinZ_2)); }
	inline float get_m_fRotatoinZ_2() const { return ___m_fRotatoinZ_2; }
	inline float* get_address_of_m_fRotatoinZ_2() { return &___m_fRotatoinZ_2; }
	inline void set_m_fRotatoinZ_2(float value)
	{
		___m_fRotatoinZ_2 = value;
	}

	inline static int32_t get_offset_of_m_fLiveTime_3() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fLiveTime_3)); }
	inline float get_m_fLiveTime_3() const { return ___m_fLiveTime_3; }
	inline float* get_address_of_m_fLiveTime_3() { return &___m_fLiveTime_3; }
	inline void set_m_fLiveTime_3(float value)
	{
		___m_fLiveTime_3 = value;
	}

	inline static int32_t get_offset_of_m_bImortal_4() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bImortal_4)); }
	inline bool get_m_bImortal_4() const { return ___m_bImortal_4; }
	inline bool* get_address_of_m_bImortal_4() { return &___m_bImortal_4; }
	inline void set_m_bImortal_4(bool value)
	{
		___m_bImortal_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ____speed_6)); }
	inline Vector2_t2156229523  get__speed_6() const { return ____speed_6; }
	inline Vector2_t2156229523 * get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(Vector2_t2156229523  value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of_m_bDestroyed_7() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bDestroyed_7)); }
	inline bool get_m_bDestroyed_7() const { return ___m_bDestroyed_7; }
	inline bool* get_address_of_m_bDestroyed_7() { return &___m_bDestroyed_7; }
	inline void set_m_bDestroyed_7(bool value)
	{
		___m_bDestroyed_7 = value;
	}

	inline static int32_t get_offset_of_m_Direction_8() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_Direction_8)); }
	inline Vector2_t2156229523  get_m_Direction_8() const { return ___m_Direction_8; }
	inline Vector2_t2156229523 * get_address_of_m_Direction_8() { return &___m_Direction_8; }
	inline void set_m_Direction_8(Vector2_t2156229523  value)
	{
		___m_Direction_8 = value;
	}

	inline static int32_t get_offset_of_m_eObjType_9() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_eObjType_9)); }
	inline int32_t get_m_eObjType_9() const { return ___m_eObjType_9; }
	inline int32_t* get_address_of_m_eObjType_9() { return &___m_eObjType_9; }
	inline void set_m_eObjType_9(int32_t value)
	{
		___m_eObjType_9 = value;
	}

	inline static int32_t get_offset_of_m_fInitSpeed_10() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fInitSpeed_10)); }
	inline float get_m_fInitSpeed_10() const { return ___m_fInitSpeed_10; }
	inline float* get_address_of_m_fInitSpeed_10() { return &___m_fInitSpeed_10; }
	inline void set_m_fInitSpeed_10(float value)
	{
		___m_fInitSpeed_10 = value;
	}

	inline static int32_t get_offset_of_m_fAccelerate_11() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fAccelerate_11)); }
	inline float get_m_fAccelerate_11() const { return ___m_fAccelerate_11; }
	inline float* get_address_of_m_fAccelerate_11() { return &___m_fAccelerate_11; }
	inline void set_m_fAccelerate_11(float value)
	{
		___m_fAccelerate_11 = value;
	}

	inline static int32_t get_offset_of_m_fDirection_12() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fDirection_12)); }
	inline float get_m_fDirection_12() const { return ___m_fDirection_12; }
	inline float* get_address_of_m_fDirection_12() { return &___m_fDirection_12; }
	inline void set_m_fDirection_12(float value)
	{
		___m_fDirection_12 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_13() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bMoving_13)); }
	inline bool get_m_bMoving_13() const { return ___m_bMoving_13; }
	inline bool* get_address_of_m_bMoving_13() { return &___m_bMoving_13; }
	inline void set_m_bMoving_13(bool value)
	{
		___m_bMoving_13 = value;
	}

	inline static int32_t get_offset_of_m_bEjecting_14() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bEjecting_14)); }
	inline bool get_m_bEjecting_14() const { return ___m_bEjecting_14; }
	inline bool* get_address_of_m_bEjecting_14() { return &___m_bEjecting_14; }
	inline void set_m_bEjecting_14(bool value)
	{
		___m_bEjecting_14 = value;
	}

	inline static int32_t get_offset_of_m_fEjectDistance_15() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fEjectDistance_15)); }
	inline float get_m_fEjectDistance_15() const { return ___m_fEjectDistance_15; }
	inline float* get_address_of_m_fEjectDistance_15() { return &___m_fEjectDistance_15; }
	inline void set_m_fEjectDistance_15(float value)
	{
		___m_fEjectDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_vEjectStartPos_16() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_vEjectStartPos_16)); }
	inline Vector2_t2156229523  get_m_vEjectStartPos_16() const { return ___m_vEjectStartPos_16; }
	inline Vector2_t2156229523 * get_address_of_m_vEjectStartPos_16() { return &___m_vEjectStartPos_16; }
	inline void set_m_vEjectStartPos_16(Vector2_t2156229523  value)
	{
		___m_vEjectStartPos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPOBJ_T1733252447_H
#ifndef SERVERTIME_T783080118_H
#define SERVERTIME_T783080118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerTime
struct  ServerTime_t783080118  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERTIME_T783080118_H
#ifndef CCENTERSERVER_T605262508_H
#define CCENTERSERVER_T605262508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCenterServer
struct  CCenterServer_t605262508  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCENTERSERVER_T605262508_H
#ifndef CACCOUNTSYSTEM_T3373977008_H
#define CACCOUNTSYSTEM_T3373977008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAccountSystem
struct  CAccountSystem_t3373977008  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite CAccountSystem::m_sprAddSkin
	Sprite_t280657092 * ___m_sprAddSkin_3;
	// CCosmosSkeleton CAccountSystem::_skeleonSelectedAvartar
	CCosmosSkeleton_t2697667825 * ____skeleonSelectedAvartar_4;
	// UnityEngine.UI.CanvasScaler CAccountSystem::_CanvasScaler
	CanvasScaler_t2767979955 * ____CanvasScaler_5;
	// UnityEngine.Color CAccountSystem::m_colorButtonEnabledColor
	Color_t2555686324  ___m_colorButtonEnabledColor_6;
	// UnityEngine.Color CAccountSystem::m_colorButtonDisabledColor
	Color_t2555686324  ___m_colorButtonDisabledColor_7;
	// UnityEngine.GameObject[] CAccountSystem::m_aryHomePageContainerAll
	GameObjectU5BU5D_t3328599146* ___m_aryHomePageContainerAll_8;
	// UnityEngine.GameObject[] CAccountSystem::m_arySelectSkilPanel
	GameObjectU5BU5D_t3328599146* ___m_arySelectSkilPanel_9;
	// UnityEngine.GameObject[] CAccountSystem::m_aryPanelSettings
	GameObjectU5BU5D_t3328599146* ___m_aryPanelSettings_10;
	// CProgressBar CAccountSystem::_progressLoadIng
	CProgressBar_t881982788 * ____progressLoadIng_11;
	// UnityEngine.GameObject CAccountSystem::_panelProgressLoadIng
	GameObject_t1113636619 * ____panelProgressLoadIng_12;
	// UnityEngine.UI.Scrollbar CAccountSystem::_scollLoadResourcePercent
	Scrollbar_t1494447233 * ____scollLoadResourcePercent_13;
	// UnityEngine.UI.Image CAccountSystem::_imgCurEquipedAvatar
	Image_t2670269651 * ____imgCurEquipedAvatar_14;
	// UnityEngine.UI.Image[] CAccountSystem::m_aryCurEquipedAvatar
	ImageU5BU5D_t2439009922* ___m_aryCurEquipedAvatar_15;
	// UnityEngine.UI.InputField CAccountSystem::_inputLogin_PhoneNum
	InputField_t3762917431 * ____inputLogin_PhoneNum_16;
	// UnityEngine.UI.InputField CAccountSystem::_inputLogin_Password
	InputField_t3762917431 * ____inputLogin_Password_17;
	// UnityEngine.UI.InputField CAccountSystem::_inputLogin_GoToRegidter
	InputField_t3762917431 * ____inputLogin_GoToRegidter_18;
	// UnityEngine.UI.InputField[] CAccountSystem::m_aryUpdateRoleName
	InputFieldU5BU5D_t1172778254* ___m_aryUpdateRoleName_19;
	// UnityEngine.UI.InputField CAccountSystem::_inputMainUI_RoleName
	InputField_t3762917431 * ____inputMainUI_RoleName_20;
	// UnityEngine.UI.Text CAccountSystem::_txtMainUI_RoleName
	Text_t1901882714 * ____txtMainUI_RoleName_21;
	// UnityEngine.UI.Text[] CAccountSystem::m_aryHomePageRoleName
	TextU5BU5D_t422084607* ___m_aryHomePageRoleName_22;
	// UnityEngine.UI.Text[] CAccountSystem::_aryMoney
	TextU5BU5D_t422084607* ____aryMoney_23;
	// CCyberTreeList CAccountSystem::m_cyberlistRankingList
	CCyberTreeList_t4040119676 * ___m_cyberlistRankingList_24;
	// UnityEngine.UI.InputField CAccountSystem::_inputRegister_PhoneNum
	InputField_t3762917431 * ____inputRegister_PhoneNum_25;
	// UnityEngine.UI.InputField CAccountSystem::_inputRegister_SmsCode
	InputField_t3762917431 * ____inputRegister_SmsCode_26;
	// UnityEngine.UI.InputField CAccountSystem::_inputRegister_Password
	InputField_t3762917431 * ____inputRegister_Password_27;
	// UnityEngine.UI.Button CAccountSystem::_btnGetSmsCode
	Button_t4055032469 * ____btnGetSmsCode_28;
	// UnityEngine.UI.Text CAccountSystem::_txtButtonSmsCodeCaption
	Text_t1901882714 * ____txtButtonSmsCodeCaption_29;
	// UnityEngine.UI.InputField CAccountSystem::_inputResetPassword_PhoneNum
	InputField_t3762917431 * ____inputResetPassword_PhoneNum_30;
	// UnityEngine.UI.InputField CAccountSystem::_inputResetPassword_SmsCode
	InputField_t3762917431 * ____inputResetPassword_SmsCode_31;
	// UnityEngine.UI.InputField CAccountSystem::_inputResetPassword_Password
	InputField_t3762917431 * ____inputResetPassword_Password_32;
	// UnityEngine.UI.Button CAccountSystem::_btnGetSmsCode_ResetPwd
	Button_t4055032469 * ____btnGetSmsCode_ResetPwd_33;
	// UnityEngine.UI.Text CAccountSystem::_txtButtonSmsCodeCaption_ResetPwd
	Text_t1901882714 * ____txtButtonSmsCodeCaption_ResetPwd_34;
	// UnityEngine.UI.InputField CAccountSystem::_inputTest
	InputField_t3762917431 * ____inputTest_35;
	// UnityEngine.GameObject CAccountSystem::_panelUpdateRoleName
	GameObject_t1113636619 * ____panelUpdateRoleName_36;
	// UnityEngine.GameObject[] CAccountSystem::m_aryUpdateRoleNamePanel
	GameObjectU5BU5D_t3328599146* ___m_aryUpdateRoleNamePanel_37;
	// UnityEngine.GameObject CAccountSystem::_panelAccountPasswordLogin
	GameObject_t1113636619 * ____panelAccountPasswordLogin_38;
	// UnityEngine.GameObject CAccountSystem::_panelMainUI
	GameObject_t1113636619 * ____panelMainUI_39;
	// UnityEngine.GameObject[] CAccountSystem::m_aryPanelMainUI
	GameObjectU5BU5D_t3328599146* ___m_aryPanelMainUI_40;
	// UnityEngine.GameObject CAccountSystem::_panelRegister
	GameObject_t1113636619 * ____panelRegister_41;
	// UnityEngine.GameObject CAccountSystem::_panelResetPassword
	GameObject_t1113636619 * ____panelResetPassword_42;
	// UnityEngine.UI.Text[] CAccountSystem::m_aryBottomButtons_Text
	TextU5BU5D_t422084607* ___m_aryBottomButtons_Text_43;
	// UnityEngine.UI.Image[] CAccountSystem::m_aryBottomButtons_Image
	ImageU5BU5D_t2439009922* ___m_aryBottomButtons_Image_44;
	// UnityEngine.Color CAccountSystem::m_colorBottomButton_Selected
	Color_t2555686324  ___m_colorBottomButton_Selected_45;
	// UnityEngine.Color CAccountSystem::m_colorBottomButton_NotSelected
	Color_t2555686324  ___m_colorBottomButton_NotSelected_46;
	// UnityEngine.Sprite CAccountSystem::m_sprBottomButton_Selected
	Sprite_t280657092 * ___m_sprBottomButton_Selected_47;
	// UnityEngine.Sprite CAccountSystem::m_sprBottomButton_NotSelected
	Sprite_t280657092 * ___m_sprBottomButton_NotSelected_48;
	// UnityEngine.GameObject CAccountSystem::m_preRankingListItem
	GameObject_t1113636619 * ___m_preRankingListItem_49;
	// UnityEngine.UI.Image CAccountSystem::_imgBattery
	Image_t2670269651 * ____imgBattery_50;
	// CWifi CAccountSystem::_wifiInfo
	CWifi_t3890179523 * ____wifiInfo_51;
	// CyberTreeScrollView CAccountSystem::_listPaiHangBang
	CyberTreeScrollView_t257211719 * ____listPaiHangBang_52;
	// System.Single CAccountSystem::m_fWifiTimeElapse
	float ___m_fWifiTimeElapse_54;
	// CCyberTreePoPo CAccountSystem::_popoStreetNews
	CCyberTreePoPo_t261854377 * ____popoStreetNews_55;
	// CCyberTreePoPo CAccountSystem::_popoHotEvents
	CCyberTreePoPo_t261854377 * ____popoHotEvents_56;
	// System.Single CAccountSystem::m_fPingTime
	float ___m_fPingTime_57;

public:
	inline static int32_t get_offset_of_m_sprAddSkin_3() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_sprAddSkin_3)); }
	inline Sprite_t280657092 * get_m_sprAddSkin_3() const { return ___m_sprAddSkin_3; }
	inline Sprite_t280657092 ** get_address_of_m_sprAddSkin_3() { return &___m_sprAddSkin_3; }
	inline void set_m_sprAddSkin_3(Sprite_t280657092 * value)
	{
		___m_sprAddSkin_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprAddSkin_3), value);
	}

	inline static int32_t get_offset_of__skeleonSelectedAvartar_4() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____skeleonSelectedAvartar_4)); }
	inline CCosmosSkeleton_t2697667825 * get__skeleonSelectedAvartar_4() const { return ____skeleonSelectedAvartar_4; }
	inline CCosmosSkeleton_t2697667825 ** get_address_of__skeleonSelectedAvartar_4() { return &____skeleonSelectedAvartar_4; }
	inline void set__skeleonSelectedAvartar_4(CCosmosSkeleton_t2697667825 * value)
	{
		____skeleonSelectedAvartar_4 = value;
		Il2CppCodeGenWriteBarrier((&____skeleonSelectedAvartar_4), value);
	}

	inline static int32_t get_offset_of__CanvasScaler_5() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____CanvasScaler_5)); }
	inline CanvasScaler_t2767979955 * get__CanvasScaler_5() const { return ____CanvasScaler_5; }
	inline CanvasScaler_t2767979955 ** get_address_of__CanvasScaler_5() { return &____CanvasScaler_5; }
	inline void set__CanvasScaler_5(CanvasScaler_t2767979955 * value)
	{
		____CanvasScaler_5 = value;
		Il2CppCodeGenWriteBarrier((&____CanvasScaler_5), value);
	}

	inline static int32_t get_offset_of_m_colorButtonEnabledColor_6() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_colorButtonEnabledColor_6)); }
	inline Color_t2555686324  get_m_colorButtonEnabledColor_6() const { return ___m_colorButtonEnabledColor_6; }
	inline Color_t2555686324 * get_address_of_m_colorButtonEnabledColor_6() { return &___m_colorButtonEnabledColor_6; }
	inline void set_m_colorButtonEnabledColor_6(Color_t2555686324  value)
	{
		___m_colorButtonEnabledColor_6 = value;
	}

	inline static int32_t get_offset_of_m_colorButtonDisabledColor_7() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_colorButtonDisabledColor_7)); }
	inline Color_t2555686324  get_m_colorButtonDisabledColor_7() const { return ___m_colorButtonDisabledColor_7; }
	inline Color_t2555686324 * get_address_of_m_colorButtonDisabledColor_7() { return &___m_colorButtonDisabledColor_7; }
	inline void set_m_colorButtonDisabledColor_7(Color_t2555686324  value)
	{
		___m_colorButtonDisabledColor_7 = value;
	}

	inline static int32_t get_offset_of_m_aryHomePageContainerAll_8() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryHomePageContainerAll_8)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryHomePageContainerAll_8() const { return ___m_aryHomePageContainerAll_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryHomePageContainerAll_8() { return &___m_aryHomePageContainerAll_8; }
	inline void set_m_aryHomePageContainerAll_8(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryHomePageContainerAll_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryHomePageContainerAll_8), value);
	}

	inline static int32_t get_offset_of_m_arySelectSkilPanel_9() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_arySelectSkilPanel_9)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySelectSkilPanel_9() const { return ___m_arySelectSkilPanel_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySelectSkilPanel_9() { return &___m_arySelectSkilPanel_9; }
	inline void set_m_arySelectSkilPanel_9(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySelectSkilPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySelectSkilPanel_9), value);
	}

	inline static int32_t get_offset_of_m_aryPanelSettings_10() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryPanelSettings_10)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryPanelSettings_10() const { return ___m_aryPanelSettings_10; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryPanelSettings_10() { return &___m_aryPanelSettings_10; }
	inline void set_m_aryPanelSettings_10(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryPanelSettings_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPanelSettings_10), value);
	}

	inline static int32_t get_offset_of__progressLoadIng_11() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____progressLoadIng_11)); }
	inline CProgressBar_t881982788 * get__progressLoadIng_11() const { return ____progressLoadIng_11; }
	inline CProgressBar_t881982788 ** get_address_of__progressLoadIng_11() { return &____progressLoadIng_11; }
	inline void set__progressLoadIng_11(CProgressBar_t881982788 * value)
	{
		____progressLoadIng_11 = value;
		Il2CppCodeGenWriteBarrier((&____progressLoadIng_11), value);
	}

	inline static int32_t get_offset_of__panelProgressLoadIng_12() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____panelProgressLoadIng_12)); }
	inline GameObject_t1113636619 * get__panelProgressLoadIng_12() const { return ____panelProgressLoadIng_12; }
	inline GameObject_t1113636619 ** get_address_of__panelProgressLoadIng_12() { return &____panelProgressLoadIng_12; }
	inline void set__panelProgressLoadIng_12(GameObject_t1113636619 * value)
	{
		____panelProgressLoadIng_12 = value;
		Il2CppCodeGenWriteBarrier((&____panelProgressLoadIng_12), value);
	}

	inline static int32_t get_offset_of__scollLoadResourcePercent_13() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____scollLoadResourcePercent_13)); }
	inline Scrollbar_t1494447233 * get__scollLoadResourcePercent_13() const { return ____scollLoadResourcePercent_13; }
	inline Scrollbar_t1494447233 ** get_address_of__scollLoadResourcePercent_13() { return &____scollLoadResourcePercent_13; }
	inline void set__scollLoadResourcePercent_13(Scrollbar_t1494447233 * value)
	{
		____scollLoadResourcePercent_13 = value;
		Il2CppCodeGenWriteBarrier((&____scollLoadResourcePercent_13), value);
	}

	inline static int32_t get_offset_of__imgCurEquipedAvatar_14() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____imgCurEquipedAvatar_14)); }
	inline Image_t2670269651 * get__imgCurEquipedAvatar_14() const { return ____imgCurEquipedAvatar_14; }
	inline Image_t2670269651 ** get_address_of__imgCurEquipedAvatar_14() { return &____imgCurEquipedAvatar_14; }
	inline void set__imgCurEquipedAvatar_14(Image_t2670269651 * value)
	{
		____imgCurEquipedAvatar_14 = value;
		Il2CppCodeGenWriteBarrier((&____imgCurEquipedAvatar_14), value);
	}

	inline static int32_t get_offset_of_m_aryCurEquipedAvatar_15() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryCurEquipedAvatar_15)); }
	inline ImageU5BU5D_t2439009922* get_m_aryCurEquipedAvatar_15() const { return ___m_aryCurEquipedAvatar_15; }
	inline ImageU5BU5D_t2439009922** get_address_of_m_aryCurEquipedAvatar_15() { return &___m_aryCurEquipedAvatar_15; }
	inline void set_m_aryCurEquipedAvatar_15(ImageU5BU5D_t2439009922* value)
	{
		___m_aryCurEquipedAvatar_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCurEquipedAvatar_15), value);
	}

	inline static int32_t get_offset_of__inputLogin_PhoneNum_16() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputLogin_PhoneNum_16)); }
	inline InputField_t3762917431 * get__inputLogin_PhoneNum_16() const { return ____inputLogin_PhoneNum_16; }
	inline InputField_t3762917431 ** get_address_of__inputLogin_PhoneNum_16() { return &____inputLogin_PhoneNum_16; }
	inline void set__inputLogin_PhoneNum_16(InputField_t3762917431 * value)
	{
		____inputLogin_PhoneNum_16 = value;
		Il2CppCodeGenWriteBarrier((&____inputLogin_PhoneNum_16), value);
	}

	inline static int32_t get_offset_of__inputLogin_Password_17() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputLogin_Password_17)); }
	inline InputField_t3762917431 * get__inputLogin_Password_17() const { return ____inputLogin_Password_17; }
	inline InputField_t3762917431 ** get_address_of__inputLogin_Password_17() { return &____inputLogin_Password_17; }
	inline void set__inputLogin_Password_17(InputField_t3762917431 * value)
	{
		____inputLogin_Password_17 = value;
		Il2CppCodeGenWriteBarrier((&____inputLogin_Password_17), value);
	}

	inline static int32_t get_offset_of__inputLogin_GoToRegidter_18() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputLogin_GoToRegidter_18)); }
	inline InputField_t3762917431 * get__inputLogin_GoToRegidter_18() const { return ____inputLogin_GoToRegidter_18; }
	inline InputField_t3762917431 ** get_address_of__inputLogin_GoToRegidter_18() { return &____inputLogin_GoToRegidter_18; }
	inline void set__inputLogin_GoToRegidter_18(InputField_t3762917431 * value)
	{
		____inputLogin_GoToRegidter_18 = value;
		Il2CppCodeGenWriteBarrier((&____inputLogin_GoToRegidter_18), value);
	}

	inline static int32_t get_offset_of_m_aryUpdateRoleName_19() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryUpdateRoleName_19)); }
	inline InputFieldU5BU5D_t1172778254* get_m_aryUpdateRoleName_19() const { return ___m_aryUpdateRoleName_19; }
	inline InputFieldU5BU5D_t1172778254** get_address_of_m_aryUpdateRoleName_19() { return &___m_aryUpdateRoleName_19; }
	inline void set_m_aryUpdateRoleName_19(InputFieldU5BU5D_t1172778254* value)
	{
		___m_aryUpdateRoleName_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUpdateRoleName_19), value);
	}

	inline static int32_t get_offset_of__inputMainUI_RoleName_20() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputMainUI_RoleName_20)); }
	inline InputField_t3762917431 * get__inputMainUI_RoleName_20() const { return ____inputMainUI_RoleName_20; }
	inline InputField_t3762917431 ** get_address_of__inputMainUI_RoleName_20() { return &____inputMainUI_RoleName_20; }
	inline void set__inputMainUI_RoleName_20(InputField_t3762917431 * value)
	{
		____inputMainUI_RoleName_20 = value;
		Il2CppCodeGenWriteBarrier((&____inputMainUI_RoleName_20), value);
	}

	inline static int32_t get_offset_of__txtMainUI_RoleName_21() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____txtMainUI_RoleName_21)); }
	inline Text_t1901882714 * get__txtMainUI_RoleName_21() const { return ____txtMainUI_RoleName_21; }
	inline Text_t1901882714 ** get_address_of__txtMainUI_RoleName_21() { return &____txtMainUI_RoleName_21; }
	inline void set__txtMainUI_RoleName_21(Text_t1901882714 * value)
	{
		____txtMainUI_RoleName_21 = value;
		Il2CppCodeGenWriteBarrier((&____txtMainUI_RoleName_21), value);
	}

	inline static int32_t get_offset_of_m_aryHomePageRoleName_22() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryHomePageRoleName_22)); }
	inline TextU5BU5D_t422084607* get_m_aryHomePageRoleName_22() const { return ___m_aryHomePageRoleName_22; }
	inline TextU5BU5D_t422084607** get_address_of_m_aryHomePageRoleName_22() { return &___m_aryHomePageRoleName_22; }
	inline void set_m_aryHomePageRoleName_22(TextU5BU5D_t422084607* value)
	{
		___m_aryHomePageRoleName_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryHomePageRoleName_22), value);
	}

	inline static int32_t get_offset_of__aryMoney_23() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____aryMoney_23)); }
	inline TextU5BU5D_t422084607* get__aryMoney_23() const { return ____aryMoney_23; }
	inline TextU5BU5D_t422084607** get_address_of__aryMoney_23() { return &____aryMoney_23; }
	inline void set__aryMoney_23(TextU5BU5D_t422084607* value)
	{
		____aryMoney_23 = value;
		Il2CppCodeGenWriteBarrier((&____aryMoney_23), value);
	}

	inline static int32_t get_offset_of_m_cyberlistRankingList_24() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_cyberlistRankingList_24)); }
	inline CCyberTreeList_t4040119676 * get_m_cyberlistRankingList_24() const { return ___m_cyberlistRankingList_24; }
	inline CCyberTreeList_t4040119676 ** get_address_of_m_cyberlistRankingList_24() { return &___m_cyberlistRankingList_24; }
	inline void set_m_cyberlistRankingList_24(CCyberTreeList_t4040119676 * value)
	{
		___m_cyberlistRankingList_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_cyberlistRankingList_24), value);
	}

	inline static int32_t get_offset_of__inputRegister_PhoneNum_25() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputRegister_PhoneNum_25)); }
	inline InputField_t3762917431 * get__inputRegister_PhoneNum_25() const { return ____inputRegister_PhoneNum_25; }
	inline InputField_t3762917431 ** get_address_of__inputRegister_PhoneNum_25() { return &____inputRegister_PhoneNum_25; }
	inline void set__inputRegister_PhoneNum_25(InputField_t3762917431 * value)
	{
		____inputRegister_PhoneNum_25 = value;
		Il2CppCodeGenWriteBarrier((&____inputRegister_PhoneNum_25), value);
	}

	inline static int32_t get_offset_of__inputRegister_SmsCode_26() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputRegister_SmsCode_26)); }
	inline InputField_t3762917431 * get__inputRegister_SmsCode_26() const { return ____inputRegister_SmsCode_26; }
	inline InputField_t3762917431 ** get_address_of__inputRegister_SmsCode_26() { return &____inputRegister_SmsCode_26; }
	inline void set__inputRegister_SmsCode_26(InputField_t3762917431 * value)
	{
		____inputRegister_SmsCode_26 = value;
		Il2CppCodeGenWriteBarrier((&____inputRegister_SmsCode_26), value);
	}

	inline static int32_t get_offset_of__inputRegister_Password_27() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputRegister_Password_27)); }
	inline InputField_t3762917431 * get__inputRegister_Password_27() const { return ____inputRegister_Password_27; }
	inline InputField_t3762917431 ** get_address_of__inputRegister_Password_27() { return &____inputRegister_Password_27; }
	inline void set__inputRegister_Password_27(InputField_t3762917431 * value)
	{
		____inputRegister_Password_27 = value;
		Il2CppCodeGenWriteBarrier((&____inputRegister_Password_27), value);
	}

	inline static int32_t get_offset_of__btnGetSmsCode_28() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____btnGetSmsCode_28)); }
	inline Button_t4055032469 * get__btnGetSmsCode_28() const { return ____btnGetSmsCode_28; }
	inline Button_t4055032469 ** get_address_of__btnGetSmsCode_28() { return &____btnGetSmsCode_28; }
	inline void set__btnGetSmsCode_28(Button_t4055032469 * value)
	{
		____btnGetSmsCode_28 = value;
		Il2CppCodeGenWriteBarrier((&____btnGetSmsCode_28), value);
	}

	inline static int32_t get_offset_of__txtButtonSmsCodeCaption_29() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____txtButtonSmsCodeCaption_29)); }
	inline Text_t1901882714 * get__txtButtonSmsCodeCaption_29() const { return ____txtButtonSmsCodeCaption_29; }
	inline Text_t1901882714 ** get_address_of__txtButtonSmsCodeCaption_29() { return &____txtButtonSmsCodeCaption_29; }
	inline void set__txtButtonSmsCodeCaption_29(Text_t1901882714 * value)
	{
		____txtButtonSmsCodeCaption_29 = value;
		Il2CppCodeGenWriteBarrier((&____txtButtonSmsCodeCaption_29), value);
	}

	inline static int32_t get_offset_of__inputResetPassword_PhoneNum_30() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputResetPassword_PhoneNum_30)); }
	inline InputField_t3762917431 * get__inputResetPassword_PhoneNum_30() const { return ____inputResetPassword_PhoneNum_30; }
	inline InputField_t3762917431 ** get_address_of__inputResetPassword_PhoneNum_30() { return &____inputResetPassword_PhoneNum_30; }
	inline void set__inputResetPassword_PhoneNum_30(InputField_t3762917431 * value)
	{
		____inputResetPassword_PhoneNum_30 = value;
		Il2CppCodeGenWriteBarrier((&____inputResetPassword_PhoneNum_30), value);
	}

	inline static int32_t get_offset_of__inputResetPassword_SmsCode_31() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputResetPassword_SmsCode_31)); }
	inline InputField_t3762917431 * get__inputResetPassword_SmsCode_31() const { return ____inputResetPassword_SmsCode_31; }
	inline InputField_t3762917431 ** get_address_of__inputResetPassword_SmsCode_31() { return &____inputResetPassword_SmsCode_31; }
	inline void set__inputResetPassword_SmsCode_31(InputField_t3762917431 * value)
	{
		____inputResetPassword_SmsCode_31 = value;
		Il2CppCodeGenWriteBarrier((&____inputResetPassword_SmsCode_31), value);
	}

	inline static int32_t get_offset_of__inputResetPassword_Password_32() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputResetPassword_Password_32)); }
	inline InputField_t3762917431 * get__inputResetPassword_Password_32() const { return ____inputResetPassword_Password_32; }
	inline InputField_t3762917431 ** get_address_of__inputResetPassword_Password_32() { return &____inputResetPassword_Password_32; }
	inline void set__inputResetPassword_Password_32(InputField_t3762917431 * value)
	{
		____inputResetPassword_Password_32 = value;
		Il2CppCodeGenWriteBarrier((&____inputResetPassword_Password_32), value);
	}

	inline static int32_t get_offset_of__btnGetSmsCode_ResetPwd_33() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____btnGetSmsCode_ResetPwd_33)); }
	inline Button_t4055032469 * get__btnGetSmsCode_ResetPwd_33() const { return ____btnGetSmsCode_ResetPwd_33; }
	inline Button_t4055032469 ** get_address_of__btnGetSmsCode_ResetPwd_33() { return &____btnGetSmsCode_ResetPwd_33; }
	inline void set__btnGetSmsCode_ResetPwd_33(Button_t4055032469 * value)
	{
		____btnGetSmsCode_ResetPwd_33 = value;
		Il2CppCodeGenWriteBarrier((&____btnGetSmsCode_ResetPwd_33), value);
	}

	inline static int32_t get_offset_of__txtButtonSmsCodeCaption_ResetPwd_34() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____txtButtonSmsCodeCaption_ResetPwd_34)); }
	inline Text_t1901882714 * get__txtButtonSmsCodeCaption_ResetPwd_34() const { return ____txtButtonSmsCodeCaption_ResetPwd_34; }
	inline Text_t1901882714 ** get_address_of__txtButtonSmsCodeCaption_ResetPwd_34() { return &____txtButtonSmsCodeCaption_ResetPwd_34; }
	inline void set__txtButtonSmsCodeCaption_ResetPwd_34(Text_t1901882714 * value)
	{
		____txtButtonSmsCodeCaption_ResetPwd_34 = value;
		Il2CppCodeGenWriteBarrier((&____txtButtonSmsCodeCaption_ResetPwd_34), value);
	}

	inline static int32_t get_offset_of__inputTest_35() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____inputTest_35)); }
	inline InputField_t3762917431 * get__inputTest_35() const { return ____inputTest_35; }
	inline InputField_t3762917431 ** get_address_of__inputTest_35() { return &____inputTest_35; }
	inline void set__inputTest_35(InputField_t3762917431 * value)
	{
		____inputTest_35 = value;
		Il2CppCodeGenWriteBarrier((&____inputTest_35), value);
	}

	inline static int32_t get_offset_of__panelUpdateRoleName_36() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____panelUpdateRoleName_36)); }
	inline GameObject_t1113636619 * get__panelUpdateRoleName_36() const { return ____panelUpdateRoleName_36; }
	inline GameObject_t1113636619 ** get_address_of__panelUpdateRoleName_36() { return &____panelUpdateRoleName_36; }
	inline void set__panelUpdateRoleName_36(GameObject_t1113636619 * value)
	{
		____panelUpdateRoleName_36 = value;
		Il2CppCodeGenWriteBarrier((&____panelUpdateRoleName_36), value);
	}

	inline static int32_t get_offset_of_m_aryUpdateRoleNamePanel_37() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryUpdateRoleNamePanel_37)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryUpdateRoleNamePanel_37() const { return ___m_aryUpdateRoleNamePanel_37; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryUpdateRoleNamePanel_37() { return &___m_aryUpdateRoleNamePanel_37; }
	inline void set_m_aryUpdateRoleNamePanel_37(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryUpdateRoleNamePanel_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUpdateRoleNamePanel_37), value);
	}

	inline static int32_t get_offset_of__panelAccountPasswordLogin_38() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____panelAccountPasswordLogin_38)); }
	inline GameObject_t1113636619 * get__panelAccountPasswordLogin_38() const { return ____panelAccountPasswordLogin_38; }
	inline GameObject_t1113636619 ** get_address_of__panelAccountPasswordLogin_38() { return &____panelAccountPasswordLogin_38; }
	inline void set__panelAccountPasswordLogin_38(GameObject_t1113636619 * value)
	{
		____panelAccountPasswordLogin_38 = value;
		Il2CppCodeGenWriteBarrier((&____panelAccountPasswordLogin_38), value);
	}

	inline static int32_t get_offset_of__panelMainUI_39() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____panelMainUI_39)); }
	inline GameObject_t1113636619 * get__panelMainUI_39() const { return ____panelMainUI_39; }
	inline GameObject_t1113636619 ** get_address_of__panelMainUI_39() { return &____panelMainUI_39; }
	inline void set__panelMainUI_39(GameObject_t1113636619 * value)
	{
		____panelMainUI_39 = value;
		Il2CppCodeGenWriteBarrier((&____panelMainUI_39), value);
	}

	inline static int32_t get_offset_of_m_aryPanelMainUI_40() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryPanelMainUI_40)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryPanelMainUI_40() const { return ___m_aryPanelMainUI_40; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryPanelMainUI_40() { return &___m_aryPanelMainUI_40; }
	inline void set_m_aryPanelMainUI_40(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryPanelMainUI_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPanelMainUI_40), value);
	}

	inline static int32_t get_offset_of__panelRegister_41() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____panelRegister_41)); }
	inline GameObject_t1113636619 * get__panelRegister_41() const { return ____panelRegister_41; }
	inline GameObject_t1113636619 ** get_address_of__panelRegister_41() { return &____panelRegister_41; }
	inline void set__panelRegister_41(GameObject_t1113636619 * value)
	{
		____panelRegister_41 = value;
		Il2CppCodeGenWriteBarrier((&____panelRegister_41), value);
	}

	inline static int32_t get_offset_of__panelResetPassword_42() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____panelResetPassword_42)); }
	inline GameObject_t1113636619 * get__panelResetPassword_42() const { return ____panelResetPassword_42; }
	inline GameObject_t1113636619 ** get_address_of__panelResetPassword_42() { return &____panelResetPassword_42; }
	inline void set__panelResetPassword_42(GameObject_t1113636619 * value)
	{
		____panelResetPassword_42 = value;
		Il2CppCodeGenWriteBarrier((&____panelResetPassword_42), value);
	}

	inline static int32_t get_offset_of_m_aryBottomButtons_Text_43() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryBottomButtons_Text_43)); }
	inline TextU5BU5D_t422084607* get_m_aryBottomButtons_Text_43() const { return ___m_aryBottomButtons_Text_43; }
	inline TextU5BU5D_t422084607** get_address_of_m_aryBottomButtons_Text_43() { return &___m_aryBottomButtons_Text_43; }
	inline void set_m_aryBottomButtons_Text_43(TextU5BU5D_t422084607* value)
	{
		___m_aryBottomButtons_Text_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBottomButtons_Text_43), value);
	}

	inline static int32_t get_offset_of_m_aryBottomButtons_Image_44() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_aryBottomButtons_Image_44)); }
	inline ImageU5BU5D_t2439009922* get_m_aryBottomButtons_Image_44() const { return ___m_aryBottomButtons_Image_44; }
	inline ImageU5BU5D_t2439009922** get_address_of_m_aryBottomButtons_Image_44() { return &___m_aryBottomButtons_Image_44; }
	inline void set_m_aryBottomButtons_Image_44(ImageU5BU5D_t2439009922* value)
	{
		___m_aryBottomButtons_Image_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBottomButtons_Image_44), value);
	}

	inline static int32_t get_offset_of_m_colorBottomButton_Selected_45() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_colorBottomButton_Selected_45)); }
	inline Color_t2555686324  get_m_colorBottomButton_Selected_45() const { return ___m_colorBottomButton_Selected_45; }
	inline Color_t2555686324 * get_address_of_m_colorBottomButton_Selected_45() { return &___m_colorBottomButton_Selected_45; }
	inline void set_m_colorBottomButton_Selected_45(Color_t2555686324  value)
	{
		___m_colorBottomButton_Selected_45 = value;
	}

	inline static int32_t get_offset_of_m_colorBottomButton_NotSelected_46() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_colorBottomButton_NotSelected_46)); }
	inline Color_t2555686324  get_m_colorBottomButton_NotSelected_46() const { return ___m_colorBottomButton_NotSelected_46; }
	inline Color_t2555686324 * get_address_of_m_colorBottomButton_NotSelected_46() { return &___m_colorBottomButton_NotSelected_46; }
	inline void set_m_colorBottomButton_NotSelected_46(Color_t2555686324  value)
	{
		___m_colorBottomButton_NotSelected_46 = value;
	}

	inline static int32_t get_offset_of_m_sprBottomButton_Selected_47() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_sprBottomButton_Selected_47)); }
	inline Sprite_t280657092 * get_m_sprBottomButton_Selected_47() const { return ___m_sprBottomButton_Selected_47; }
	inline Sprite_t280657092 ** get_address_of_m_sprBottomButton_Selected_47() { return &___m_sprBottomButton_Selected_47; }
	inline void set_m_sprBottomButton_Selected_47(Sprite_t280657092 * value)
	{
		___m_sprBottomButton_Selected_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBottomButton_Selected_47), value);
	}

	inline static int32_t get_offset_of_m_sprBottomButton_NotSelected_48() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_sprBottomButton_NotSelected_48)); }
	inline Sprite_t280657092 * get_m_sprBottomButton_NotSelected_48() const { return ___m_sprBottomButton_NotSelected_48; }
	inline Sprite_t280657092 ** get_address_of_m_sprBottomButton_NotSelected_48() { return &___m_sprBottomButton_NotSelected_48; }
	inline void set_m_sprBottomButton_NotSelected_48(Sprite_t280657092 * value)
	{
		___m_sprBottomButton_NotSelected_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBottomButton_NotSelected_48), value);
	}

	inline static int32_t get_offset_of_m_preRankingListItem_49() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_preRankingListItem_49)); }
	inline GameObject_t1113636619 * get_m_preRankingListItem_49() const { return ___m_preRankingListItem_49; }
	inline GameObject_t1113636619 ** get_address_of_m_preRankingListItem_49() { return &___m_preRankingListItem_49; }
	inline void set_m_preRankingListItem_49(GameObject_t1113636619 * value)
	{
		___m_preRankingListItem_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_preRankingListItem_49), value);
	}

	inline static int32_t get_offset_of__imgBattery_50() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____imgBattery_50)); }
	inline Image_t2670269651 * get__imgBattery_50() const { return ____imgBattery_50; }
	inline Image_t2670269651 ** get_address_of__imgBattery_50() { return &____imgBattery_50; }
	inline void set__imgBattery_50(Image_t2670269651 * value)
	{
		____imgBattery_50 = value;
		Il2CppCodeGenWriteBarrier((&____imgBattery_50), value);
	}

	inline static int32_t get_offset_of__wifiInfo_51() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____wifiInfo_51)); }
	inline CWifi_t3890179523 * get__wifiInfo_51() const { return ____wifiInfo_51; }
	inline CWifi_t3890179523 ** get_address_of__wifiInfo_51() { return &____wifiInfo_51; }
	inline void set__wifiInfo_51(CWifi_t3890179523 * value)
	{
		____wifiInfo_51 = value;
		Il2CppCodeGenWriteBarrier((&____wifiInfo_51), value);
	}

	inline static int32_t get_offset_of__listPaiHangBang_52() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____listPaiHangBang_52)); }
	inline CyberTreeScrollView_t257211719 * get__listPaiHangBang_52() const { return ____listPaiHangBang_52; }
	inline CyberTreeScrollView_t257211719 ** get_address_of__listPaiHangBang_52() { return &____listPaiHangBang_52; }
	inline void set__listPaiHangBang_52(CyberTreeScrollView_t257211719 * value)
	{
		____listPaiHangBang_52 = value;
		Il2CppCodeGenWriteBarrier((&____listPaiHangBang_52), value);
	}

	inline static int32_t get_offset_of_m_fWifiTimeElapse_54() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_fWifiTimeElapse_54)); }
	inline float get_m_fWifiTimeElapse_54() const { return ___m_fWifiTimeElapse_54; }
	inline float* get_address_of_m_fWifiTimeElapse_54() { return &___m_fWifiTimeElapse_54; }
	inline void set_m_fWifiTimeElapse_54(float value)
	{
		___m_fWifiTimeElapse_54 = value;
	}

	inline static int32_t get_offset_of__popoStreetNews_55() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____popoStreetNews_55)); }
	inline CCyberTreePoPo_t261854377 * get__popoStreetNews_55() const { return ____popoStreetNews_55; }
	inline CCyberTreePoPo_t261854377 ** get_address_of__popoStreetNews_55() { return &____popoStreetNews_55; }
	inline void set__popoStreetNews_55(CCyberTreePoPo_t261854377 * value)
	{
		____popoStreetNews_55 = value;
		Il2CppCodeGenWriteBarrier((&____popoStreetNews_55), value);
	}

	inline static int32_t get_offset_of__popoHotEvents_56() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ____popoHotEvents_56)); }
	inline CCyberTreePoPo_t261854377 * get__popoHotEvents_56() const { return ____popoHotEvents_56; }
	inline CCyberTreePoPo_t261854377 ** get_address_of__popoHotEvents_56() { return &____popoHotEvents_56; }
	inline void set__popoHotEvents_56(CCyberTreePoPo_t261854377 * value)
	{
		____popoHotEvents_56 = value;
		Il2CppCodeGenWriteBarrier((&____popoHotEvents_56), value);
	}

	inline static int32_t get_offset_of_m_fPingTime_57() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008, ___m_fPingTime_57)); }
	inline float get_m_fPingTime_57() const { return ___m_fPingTime_57; }
	inline float* get_address_of_m_fPingTime_57() { return &___m_fPingTime_57; }
	inline void set_m_fPingTime_57(float value)
	{
		___m_fPingTime_57 = value;
	}
};

struct CAccountSystem_t3373977008_StaticFields
{
public:
	// CAccountSystem CAccountSystem::s_Instance
	CAccountSystem_t3373977008 * ___s_Instance_2;
	// System.Boolean CAccountSystem::m_bNickNameSettled
	bool ___m_bNickNameSettled_53;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008_StaticFields, ___s_Instance_2)); }
	inline CAccountSystem_t3373977008 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CAccountSystem_t3373977008 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CAccountSystem_t3373977008 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_m_bNickNameSettled_53() { return static_cast<int32_t>(offsetof(CAccountSystem_t3373977008_StaticFields, ___m_bNickNameSettled_53)); }
	inline bool get_m_bNickNameSettled_53() const { return ___m_bNickNameSettled_53; }
	inline bool* get_address_of_m_bNickNameSettled_53() { return &___m_bNickNameSettled_53; }
	inline void set_m_bNickNameSettled_53(bool value)
	{
		___m_bNickNameSettled_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACCOUNTSYSTEM_T3373977008_H
#ifndef BUTTONINSIDESCROLLLIST_T657350886_H
#define BUTTONINSIDESCROLLLIST_T657350886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.UtilityScripts.ButtonInsideScrollList
struct  ButtonInsideScrollList_t657350886  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ScrollRect ExitGames.UtilityScripts.ButtonInsideScrollList::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_2;

public:
	inline static int32_t get_offset_of_scrollRect_2() { return static_cast<int32_t>(offsetof(ButtonInsideScrollList_t657350886, ___scrollRect_2)); }
	inline ScrollRect_t4137855814 * get_scrollRect_2() const { return ___scrollRect_2; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_2() { return &___scrollRect_2; }
	inline void set_scrollRect_2(ScrollRect_t4137855814 * value)
	{
		___scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINSIDESCROLLLIST_T657350886_H
#ifndef TEXTBUTTONTRANSITION_T3897064113_H
#define TEXTBUTTONTRANSITION_T3897064113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.UtilityScripts.TextButtonTransition
struct  TextButtonTransition_t3897064113  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ExitGames.UtilityScripts.TextButtonTransition::_text
	Text_t1901882714 * ____text_2;
	// UnityEngine.Color ExitGames.UtilityScripts.TextButtonTransition::NormalColor
	Color_t2555686324  ___NormalColor_3;
	// UnityEngine.Color ExitGames.UtilityScripts.TextButtonTransition::HoverColor
	Color_t2555686324  ___HoverColor_4;

public:
	inline static int32_t get_offset_of__text_2() { return static_cast<int32_t>(offsetof(TextButtonTransition_t3897064113, ____text_2)); }
	inline Text_t1901882714 * get__text_2() const { return ____text_2; }
	inline Text_t1901882714 ** get_address_of__text_2() { return &____text_2; }
	inline void set__text_2(Text_t1901882714 * value)
	{
		____text_2 = value;
		Il2CppCodeGenWriteBarrier((&____text_2), value);
	}

	inline static int32_t get_offset_of_NormalColor_3() { return static_cast<int32_t>(offsetof(TextButtonTransition_t3897064113, ___NormalColor_3)); }
	inline Color_t2555686324  get_NormalColor_3() const { return ___NormalColor_3; }
	inline Color_t2555686324 * get_address_of_NormalColor_3() { return &___NormalColor_3; }
	inline void set_NormalColor_3(Color_t2555686324  value)
	{
		___NormalColor_3 = value;
	}

	inline static int32_t get_offset_of_HoverColor_4() { return static_cast<int32_t>(offsetof(TextButtonTransition_t3897064113, ___HoverColor_4)); }
	inline Color_t2555686324  get_HoverColor_4() const { return ___HoverColor_4; }
	inline Color_t2555686324 * get_address_of_HoverColor_4() { return &___HoverColor_4; }
	inline void set_HoverColor_4(Color_t2555686324  value)
	{
		___HoverColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTBUTTONTRANSITION_T3897064113_H
#ifndef TEXTTOGGLEISONTRANSITION_T1534099090_H
#define TEXTTOGGLEISONTRANSITION_T1534099090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.UtilityScripts.TextToggleIsOnTransition
struct  TextToggleIsOnTransition_t1534099090  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle ExitGames.UtilityScripts.TextToggleIsOnTransition::toggle
	Toggle_t2735377061 * ___toggle_2;
	// UnityEngine.UI.Text ExitGames.UtilityScripts.TextToggleIsOnTransition::_text
	Text_t1901882714 * ____text_3;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::NormalOnColor
	Color_t2555686324  ___NormalOnColor_4;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::NormalOffColor
	Color_t2555686324  ___NormalOffColor_5;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::HoverOnColor
	Color_t2555686324  ___HoverOnColor_6;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::HoverOffColor
	Color_t2555686324  ___HoverOffColor_7;
	// System.Boolean ExitGames.UtilityScripts.TextToggleIsOnTransition::isHover
	bool ___isHover_8;

public:
	inline static int32_t get_offset_of_toggle_2() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___toggle_2)); }
	inline Toggle_t2735377061 * get_toggle_2() const { return ___toggle_2; }
	inline Toggle_t2735377061 ** get_address_of_toggle_2() { return &___toggle_2; }
	inline void set_toggle_2(Toggle_t2735377061 * value)
	{
		___toggle_2 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_2), value);
	}

	inline static int32_t get_offset_of__text_3() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ____text_3)); }
	inline Text_t1901882714 * get__text_3() const { return ____text_3; }
	inline Text_t1901882714 ** get_address_of__text_3() { return &____text_3; }
	inline void set__text_3(Text_t1901882714 * value)
	{
		____text_3 = value;
		Il2CppCodeGenWriteBarrier((&____text_3), value);
	}

	inline static int32_t get_offset_of_NormalOnColor_4() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___NormalOnColor_4)); }
	inline Color_t2555686324  get_NormalOnColor_4() const { return ___NormalOnColor_4; }
	inline Color_t2555686324 * get_address_of_NormalOnColor_4() { return &___NormalOnColor_4; }
	inline void set_NormalOnColor_4(Color_t2555686324  value)
	{
		___NormalOnColor_4 = value;
	}

	inline static int32_t get_offset_of_NormalOffColor_5() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___NormalOffColor_5)); }
	inline Color_t2555686324  get_NormalOffColor_5() const { return ___NormalOffColor_5; }
	inline Color_t2555686324 * get_address_of_NormalOffColor_5() { return &___NormalOffColor_5; }
	inline void set_NormalOffColor_5(Color_t2555686324  value)
	{
		___NormalOffColor_5 = value;
	}

	inline static int32_t get_offset_of_HoverOnColor_6() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___HoverOnColor_6)); }
	inline Color_t2555686324  get_HoverOnColor_6() const { return ___HoverOnColor_6; }
	inline Color_t2555686324 * get_address_of_HoverOnColor_6() { return &___HoverOnColor_6; }
	inline void set_HoverOnColor_6(Color_t2555686324  value)
	{
		___HoverOnColor_6 = value;
	}

	inline static int32_t get_offset_of_HoverOffColor_7() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___HoverOffColor_7)); }
	inline Color_t2555686324  get_HoverOffColor_7() const { return ___HoverOffColor_7; }
	inline Color_t2555686324 * get_address_of_HoverOffColor_7() { return &___HoverOffColor_7; }
	inline void set_HoverOffColor_7(Color_t2555686324  value)
	{
		___HoverOffColor_7 = value;
	}

	inline static int32_t get_offset_of_isHover_8() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___isHover_8)); }
	inline bool get_isHover_8() const { return ___isHover_8; }
	inline bool* get_address_of_isHover_8() { return &___isHover_8; }
	inline void set_isHover_8(bool value)
	{
		___isHover_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOGGLEISONTRANSITION_T1534099090_H
#ifndef POLYGONLINE_T1895725216_H
#define POLYGONLINE_T1895725216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonLine
struct  PolygonLine_t1895725216  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LineRenderer PolygonLine::_lrMain
	LineRenderer_t3154350270 * ____lrMain_2;
	// PolygonPoint[] PolygonLine::m_aryPoints
	PolygonPointU5BU5D_t150589628* ___m_aryPoints_3;
	// UnityEngine.Vector3 PolygonLine::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// System.String PolygonLine::m_sKey
	String_t* ___m_sKey_5;

public:
	inline static int32_t get_offset_of__lrMain_2() { return static_cast<int32_t>(offsetof(PolygonLine_t1895725216, ____lrMain_2)); }
	inline LineRenderer_t3154350270 * get__lrMain_2() const { return ____lrMain_2; }
	inline LineRenderer_t3154350270 ** get_address_of__lrMain_2() { return &____lrMain_2; }
	inline void set__lrMain_2(LineRenderer_t3154350270 * value)
	{
		____lrMain_2 = value;
		Il2CppCodeGenWriteBarrier((&____lrMain_2), value);
	}

	inline static int32_t get_offset_of_m_aryPoints_3() { return static_cast<int32_t>(offsetof(PolygonLine_t1895725216, ___m_aryPoints_3)); }
	inline PolygonPointU5BU5D_t150589628* get_m_aryPoints_3() const { return ___m_aryPoints_3; }
	inline PolygonPointU5BU5D_t150589628** get_address_of_m_aryPoints_3() { return &___m_aryPoints_3; }
	inline void set_m_aryPoints_3(PolygonPointU5BU5D_t150589628* value)
	{
		___m_aryPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPoints_3), value);
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(PolygonLine_t1895725216, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_m_sKey_5() { return static_cast<int32_t>(offsetof(PolygonLine_t1895725216, ___m_sKey_5)); }
	inline String_t* get_m_sKey_5() const { return ___m_sKey_5; }
	inline String_t** get_address_of_m_sKey_5() { return &___m_sKey_5; }
	inline void set_m_sKey_5(String_t* value)
	{
		___m_sKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONLINE_T1895725216_H
#ifndef POLYGONEDITOR_T2170978208_H
#define POLYGONEDITOR_T2170978208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonEditor
struct  PolygonEditor_t2170978208  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PolygonEditor::m_prePolygonPoint
	GameObject_t1113636619 * ___m_prePolygonPoint_2;
	// UnityEngine.GameObject PolygonEditor::m_prePolygonLine
	GameObject_t1113636619 * ___m_prePolygonLine_3;
	// UnityEngine.GameObject PolygonEditor::m_prePolygon
	GameObject_t1113636619 * ___m_prePolygon_4;
	// UnityEngine.Vector3 PolygonEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// PolygonPoint PolygonEditor::m_CurSelectedPoint
	PolygonPoint_t4211654561 * ___m_CurSelectedPoint_6;
	// UnityEngine.GameObject PolygonEditor::the_polygon
	GameObject_t1113636619 * ___the_polygon_7;
	// Polygon PolygonEditor::_polygon
	Polygon_t12692255 * ____polygon_8;
	// UnityEngine.PolygonCollider2D PolygonEditor::_polygonCollider
	PolygonCollider2D_t57175488 * ____polygonCollider_9;
	// UnityEngine.UI.Button PolygonEditor::_btnBeginOrEndDrawPath
	Button_t4055032469 * ____btnBeginOrEndDrawPath_10;
	// System.Collections.Generic.List`1<System.String> PolygonEditor::m_lstPolygons
	List_1_t3319525431 * ___m_lstPolygons_12;
	// ComoList PolygonEditor::_PolygonList
	ComoList_t2152284863 * ____PolygonList_13;
	// MsgBox PolygonEditor::m_MsgBox
	MsgBox_t1665650521 * ___m_MsgBox_14;
	// CreateNewShit PolygonEditor::_panelCreateNewPolygon
	CreateNewShit_t3983358377 * ____panelCreateNewPolygon_15;
	// System.Xml.XmlNode PolygonEditor::_root
	XmlNode_t3767805227 * ____root_16;
	// System.Xml.XmlDocument PolygonEditor::_XmlDoc
	XmlDocument_t2837193595 * ____XmlDoc_17;
	// System.String PolygonEditor::m_szCurSelectedPolygonName
	String_t* ___m_szCurSelectedPolygonName_18;
	// System.Collections.Generic.Dictionary`2<System.String,PolygonLine> PolygonEditor::m_dicLine
	Dictionary_2_t1680981515 * ___m_dicLine_20;
	// System.Collections.Generic.Dictionary`2<System.String,PolygonLine> PolygonEditor::m_dicLineTemp
	Dictionary_2_t1680981515 * ___m_dicLineTemp_21;
	// PolygonPoint PolygonEditor::m_CurMovingPoint
	PolygonPoint_t4211654561 * ___m_CurMovingPoint_22;
	// PolygonEditor/ePolygonEditOperation PolygonEditor::m_eOp
	int32_t ___m_eOp_23;

public:
	inline static int32_t get_offset_of_m_prePolygonPoint_2() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_prePolygonPoint_2)); }
	inline GameObject_t1113636619 * get_m_prePolygonPoint_2() const { return ___m_prePolygonPoint_2; }
	inline GameObject_t1113636619 ** get_address_of_m_prePolygonPoint_2() { return &___m_prePolygonPoint_2; }
	inline void set_m_prePolygonPoint_2(GameObject_t1113636619 * value)
	{
		___m_prePolygonPoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePolygonPoint_2), value);
	}

	inline static int32_t get_offset_of_m_prePolygonLine_3() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_prePolygonLine_3)); }
	inline GameObject_t1113636619 * get_m_prePolygonLine_3() const { return ___m_prePolygonLine_3; }
	inline GameObject_t1113636619 ** get_address_of_m_prePolygonLine_3() { return &___m_prePolygonLine_3; }
	inline void set_m_prePolygonLine_3(GameObject_t1113636619 * value)
	{
		___m_prePolygonLine_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePolygonLine_3), value);
	}

	inline static int32_t get_offset_of_m_prePolygon_4() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_prePolygon_4)); }
	inline GameObject_t1113636619 * get_m_prePolygon_4() const { return ___m_prePolygon_4; }
	inline GameObject_t1113636619 ** get_address_of_m_prePolygon_4() { return &___m_prePolygon_4; }
	inline void set_m_prePolygon_4(GameObject_t1113636619 * value)
	{
		___m_prePolygon_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePolygon_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_m_CurSelectedPoint_6() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_CurSelectedPoint_6)); }
	inline PolygonPoint_t4211654561 * get_m_CurSelectedPoint_6() const { return ___m_CurSelectedPoint_6; }
	inline PolygonPoint_t4211654561 ** get_address_of_m_CurSelectedPoint_6() { return &___m_CurSelectedPoint_6; }
	inline void set_m_CurSelectedPoint_6(PolygonPoint_t4211654561 * value)
	{
		___m_CurSelectedPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedPoint_6), value);
	}

	inline static int32_t get_offset_of_the_polygon_7() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___the_polygon_7)); }
	inline GameObject_t1113636619 * get_the_polygon_7() const { return ___the_polygon_7; }
	inline GameObject_t1113636619 ** get_address_of_the_polygon_7() { return &___the_polygon_7; }
	inline void set_the_polygon_7(GameObject_t1113636619 * value)
	{
		___the_polygon_7 = value;
		Il2CppCodeGenWriteBarrier((&___the_polygon_7), value);
	}

	inline static int32_t get_offset_of__polygon_8() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____polygon_8)); }
	inline Polygon_t12692255 * get__polygon_8() const { return ____polygon_8; }
	inline Polygon_t12692255 ** get_address_of__polygon_8() { return &____polygon_8; }
	inline void set__polygon_8(Polygon_t12692255 * value)
	{
		____polygon_8 = value;
		Il2CppCodeGenWriteBarrier((&____polygon_8), value);
	}

	inline static int32_t get_offset_of__polygonCollider_9() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____polygonCollider_9)); }
	inline PolygonCollider2D_t57175488 * get__polygonCollider_9() const { return ____polygonCollider_9; }
	inline PolygonCollider2D_t57175488 ** get_address_of__polygonCollider_9() { return &____polygonCollider_9; }
	inline void set__polygonCollider_9(PolygonCollider2D_t57175488 * value)
	{
		____polygonCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&____polygonCollider_9), value);
	}

	inline static int32_t get_offset_of__btnBeginOrEndDrawPath_10() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____btnBeginOrEndDrawPath_10)); }
	inline Button_t4055032469 * get__btnBeginOrEndDrawPath_10() const { return ____btnBeginOrEndDrawPath_10; }
	inline Button_t4055032469 ** get_address_of__btnBeginOrEndDrawPath_10() { return &____btnBeginOrEndDrawPath_10; }
	inline void set__btnBeginOrEndDrawPath_10(Button_t4055032469 * value)
	{
		____btnBeginOrEndDrawPath_10 = value;
		Il2CppCodeGenWriteBarrier((&____btnBeginOrEndDrawPath_10), value);
	}

	inline static int32_t get_offset_of_m_lstPolygons_12() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_lstPolygons_12)); }
	inline List_1_t3319525431 * get_m_lstPolygons_12() const { return ___m_lstPolygons_12; }
	inline List_1_t3319525431 ** get_address_of_m_lstPolygons_12() { return &___m_lstPolygons_12; }
	inline void set_m_lstPolygons_12(List_1_t3319525431 * value)
	{
		___m_lstPolygons_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPolygons_12), value);
	}

	inline static int32_t get_offset_of__PolygonList_13() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____PolygonList_13)); }
	inline ComoList_t2152284863 * get__PolygonList_13() const { return ____PolygonList_13; }
	inline ComoList_t2152284863 ** get_address_of__PolygonList_13() { return &____PolygonList_13; }
	inline void set__PolygonList_13(ComoList_t2152284863 * value)
	{
		____PolygonList_13 = value;
		Il2CppCodeGenWriteBarrier((&____PolygonList_13), value);
	}

	inline static int32_t get_offset_of_m_MsgBox_14() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_MsgBox_14)); }
	inline MsgBox_t1665650521 * get_m_MsgBox_14() const { return ___m_MsgBox_14; }
	inline MsgBox_t1665650521 ** get_address_of_m_MsgBox_14() { return &___m_MsgBox_14; }
	inline void set_m_MsgBox_14(MsgBox_t1665650521 * value)
	{
		___m_MsgBox_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBox_14), value);
	}

	inline static int32_t get_offset_of__panelCreateNewPolygon_15() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____panelCreateNewPolygon_15)); }
	inline CreateNewShit_t3983358377 * get__panelCreateNewPolygon_15() const { return ____panelCreateNewPolygon_15; }
	inline CreateNewShit_t3983358377 ** get_address_of__panelCreateNewPolygon_15() { return &____panelCreateNewPolygon_15; }
	inline void set__panelCreateNewPolygon_15(CreateNewShit_t3983358377 * value)
	{
		____panelCreateNewPolygon_15 = value;
		Il2CppCodeGenWriteBarrier((&____panelCreateNewPolygon_15), value);
	}

	inline static int32_t get_offset_of__root_16() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____root_16)); }
	inline XmlNode_t3767805227 * get__root_16() const { return ____root_16; }
	inline XmlNode_t3767805227 ** get_address_of__root_16() { return &____root_16; }
	inline void set__root_16(XmlNode_t3767805227 * value)
	{
		____root_16 = value;
		Il2CppCodeGenWriteBarrier((&____root_16), value);
	}

	inline static int32_t get_offset_of__XmlDoc_17() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ____XmlDoc_17)); }
	inline XmlDocument_t2837193595 * get__XmlDoc_17() const { return ____XmlDoc_17; }
	inline XmlDocument_t2837193595 ** get_address_of__XmlDoc_17() { return &____XmlDoc_17; }
	inline void set__XmlDoc_17(XmlDocument_t2837193595 * value)
	{
		____XmlDoc_17 = value;
		Il2CppCodeGenWriteBarrier((&____XmlDoc_17), value);
	}

	inline static int32_t get_offset_of_m_szCurSelectedPolygonName_18() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_szCurSelectedPolygonName_18)); }
	inline String_t* get_m_szCurSelectedPolygonName_18() const { return ___m_szCurSelectedPolygonName_18; }
	inline String_t** get_address_of_m_szCurSelectedPolygonName_18() { return &___m_szCurSelectedPolygonName_18; }
	inline void set_m_szCurSelectedPolygonName_18(String_t* value)
	{
		___m_szCurSelectedPolygonName_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCurSelectedPolygonName_18), value);
	}

	inline static int32_t get_offset_of_m_dicLine_20() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_dicLine_20)); }
	inline Dictionary_2_t1680981515 * get_m_dicLine_20() const { return ___m_dicLine_20; }
	inline Dictionary_2_t1680981515 ** get_address_of_m_dicLine_20() { return &___m_dicLine_20; }
	inline void set_m_dicLine_20(Dictionary_2_t1680981515 * value)
	{
		___m_dicLine_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLine_20), value);
	}

	inline static int32_t get_offset_of_m_dicLineTemp_21() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_dicLineTemp_21)); }
	inline Dictionary_2_t1680981515 * get_m_dicLineTemp_21() const { return ___m_dicLineTemp_21; }
	inline Dictionary_2_t1680981515 ** get_address_of_m_dicLineTemp_21() { return &___m_dicLineTemp_21; }
	inline void set_m_dicLineTemp_21(Dictionary_2_t1680981515 * value)
	{
		___m_dicLineTemp_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLineTemp_21), value);
	}

	inline static int32_t get_offset_of_m_CurMovingPoint_22() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_CurMovingPoint_22)); }
	inline PolygonPoint_t4211654561 * get_m_CurMovingPoint_22() const { return ___m_CurMovingPoint_22; }
	inline PolygonPoint_t4211654561 ** get_address_of_m_CurMovingPoint_22() { return &___m_CurMovingPoint_22; }
	inline void set_m_CurMovingPoint_22(PolygonPoint_t4211654561 * value)
	{
		___m_CurMovingPoint_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurMovingPoint_22), value);
	}

	inline static int32_t get_offset_of_m_eOp_23() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208, ___m_eOp_23)); }
	inline int32_t get_m_eOp_23() const { return ___m_eOp_23; }
	inline int32_t* get_address_of_m_eOp_23() { return &___m_eOp_23; }
	inline void set_m_eOp_23(int32_t value)
	{
		___m_eOp_23 = value;
	}
};

struct PolygonEditor_t2170978208_StaticFields
{
public:
	// System.Int32 PolygonEditor::s_nPointGUID
	int32_t ___s_nPointGUID_11;
	// PolygonEditor PolygonEditor::s_Instance
	PolygonEditor_t2170978208 * ___s_Instance_19;

public:
	inline static int32_t get_offset_of_s_nPointGUID_11() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208_StaticFields, ___s_nPointGUID_11)); }
	inline int32_t get_s_nPointGUID_11() const { return ___s_nPointGUID_11; }
	inline int32_t* get_address_of_s_nPointGUID_11() { return &___s_nPointGUID_11; }
	inline void set_s_nPointGUID_11(int32_t value)
	{
		___s_nPointGUID_11 = value;
	}

	inline static int32_t get_offset_of_s_Instance_19() { return static_cast<int32_t>(offsetof(PolygonEditor_t2170978208_StaticFields, ___s_Instance_19)); }
	inline PolygonEditor_t2170978208 * get_s_Instance_19() const { return ___s_Instance_19; }
	inline PolygonEditor_t2170978208 ** get_address_of_s_Instance_19() { return &___s_Instance_19; }
	inline void set_s_Instance_19(PolygonEditor_t2170978208 * value)
	{
		___s_Instance_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONEDITOR_T2170978208_H
#ifndef POLYGONPOINT_T4211654561_H
#define POLYGONPOINT_T4211654561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolygonPoint
struct  PolygonPoint_t4211654561  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer PolygonPoint::_srMain
	SpriteRenderer_t3235626157 * ____srMain_2;
	// System.Collections.Generic.List`1<PolygonLine> PolygonPoint::m_lstRelatedLine
	List_1_t3367799958 * ___m_lstRelatedLine_3;
	// System.Int32 PolygonPoint::m_nGUID
	int32_t ___m_nGUID_4;
	// System.Int32 PolygonPoint::m_nIndex
	int32_t ___m_nIndex_5;
	// UnityEngine.TextMesh PolygonPoint::_text
	TextMesh_t1536577757 * ____text_6;

public:
	inline static int32_t get_offset_of__srMain_2() { return static_cast<int32_t>(offsetof(PolygonPoint_t4211654561, ____srMain_2)); }
	inline SpriteRenderer_t3235626157 * get__srMain_2() const { return ____srMain_2; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_2() { return &____srMain_2; }
	inline void set__srMain_2(SpriteRenderer_t3235626157 * value)
	{
		____srMain_2 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_2), value);
	}

	inline static int32_t get_offset_of_m_lstRelatedLine_3() { return static_cast<int32_t>(offsetof(PolygonPoint_t4211654561, ___m_lstRelatedLine_3)); }
	inline List_1_t3367799958 * get_m_lstRelatedLine_3() const { return ___m_lstRelatedLine_3; }
	inline List_1_t3367799958 ** get_address_of_m_lstRelatedLine_3() { return &___m_lstRelatedLine_3; }
	inline void set_m_lstRelatedLine_3(List_1_t3367799958 * value)
	{
		___m_lstRelatedLine_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRelatedLine_3), value);
	}

	inline static int32_t get_offset_of_m_nGUID_4() { return static_cast<int32_t>(offsetof(PolygonPoint_t4211654561, ___m_nGUID_4)); }
	inline int32_t get_m_nGUID_4() const { return ___m_nGUID_4; }
	inline int32_t* get_address_of_m_nGUID_4() { return &___m_nGUID_4; }
	inline void set_m_nGUID_4(int32_t value)
	{
		___m_nGUID_4 = value;
	}

	inline static int32_t get_offset_of_m_nIndex_5() { return static_cast<int32_t>(offsetof(PolygonPoint_t4211654561, ___m_nIndex_5)); }
	inline int32_t get_m_nIndex_5() const { return ___m_nIndex_5; }
	inline int32_t* get_address_of_m_nIndex_5() { return &___m_nIndex_5; }
	inline void set_m_nIndex_5(int32_t value)
	{
		___m_nIndex_5 = value;
	}

	inline static int32_t get_offset_of__text_6() { return static_cast<int32_t>(offsetof(PolygonPoint_t4211654561, ____text_6)); }
	inline TextMesh_t1536577757 * get__text_6() const { return ____text_6; }
	inline TextMesh_t1536577757 ** get_address_of__text_6() { return &____text_6; }
	inline void set__text_6(TextMesh_t1536577757 * value)
	{
		____text_6 = value;
		Il2CppCodeGenWriteBarrier((&____text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONPOINT_T4211654561_H
#ifndef SUPPORTLOGGING_T3610999087_H
#define SUPPORTLOGGING_T3610999087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupportLogging
struct  SupportLogging_t3610999087  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SupportLogging::LogTrafficStats
	bool ___LogTrafficStats_2;

public:
	inline static int32_t get_offset_of_LogTrafficStats_2() { return static_cast<int32_t>(offsetof(SupportLogging_t3610999087, ___LogTrafficStats_2)); }
	inline bool get_LogTrafficStats_2() const { return ___LogTrafficStats_2; }
	inline bool* get_address_of_LogTrafficStats_2() { return &___LogTrafficStats_2; }
	inline void set_LogTrafficStats_2(bool value)
	{
		___LogTrafficStats_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTLOGGING_T3610999087_H
#ifndef SUPPORTLOGGER_T2840230211_H
#define SUPPORTLOGGER_T2840230211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupportLogger
struct  SupportLogger_t2840230211  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SupportLogger::LogTrafficStats
	bool ___LogTrafficStats_2;

public:
	inline static int32_t get_offset_of_LogTrafficStats_2() { return static_cast<int32_t>(offsetof(SupportLogger_t2840230211, ___LogTrafficStats_2)); }
	inline bool get_LogTrafficStats_2() const { return ___LogTrafficStats_2; }
	inline bool* get_address_of_LogTrafficStats_2() { return &___LogTrafficStats_2; }
	inline void set_LogTrafficStats_2(bool value)
	{
		___LogTrafficStats_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTLOGGER_T2840230211_H
#ifndef SHOWSTATUSWHENCONNECTING_T1063567576_H
#define SHOWSTATUSWHENCONNECTING_T1063567576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowStatusWhenConnecting
struct  ShowStatusWhenConnecting_t1063567576  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin ShowStatusWhenConnecting::Skin
	GUISkin_t1244372282 * ___Skin_2;

public:
	inline static int32_t get_offset_of_Skin_2() { return static_cast<int32_t>(offsetof(ShowStatusWhenConnecting_t1063567576, ___Skin_2)); }
	inline GUISkin_t1244372282 * get_Skin_2() const { return ___Skin_2; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_2() { return &___Skin_2; }
	inline void set_Skin_2(GUISkin_t1244372282 * value)
	{
		___Skin_2 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWSTATUSWHENCONNECTING_T1063567576_H
#ifndef POLYGON_T12692255_H
#define POLYGON_T12692255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Polygon
struct  Polygon_t12692255  : public MapObj_t1733252447
{
public:
	// System.Collections.Generic.List`1<PolygonPoint> Polygon::m_lstPoint
	List_1_t1388762007 * ___m_lstPoint_17;
	// System.Collections.Generic.Dictionary`2<System.String,PolygonLine> Polygon::m_dicLine
	Dictionary_2_t1680981515 * ___m_dicLine_18;
	// System.Collections.Generic.Dictionary`2<System.String,PolygonLine> Polygon::m_dicLineTemp
	Dictionary_2_t1680981515 * ___m_dicLineTemp_19;
	// UnityEngine.EdgeCollider2D Polygon::_collierEdge
	EdgeCollider2D_t2781859275 * ____collierEdge_20;
	// UnityEngine.PolygonCollider2D Polygon::_collierPolygon
	PolygonCollider2D_t57175488 * ____collierPolygon_21;
	// UnityEngine.CircleCollider2D Polygon::_colliderSeed
	CircleCollider2D_t662546754 * ____colliderSeed_22;
	// System.String Polygon::m_szFileName
	String_t* ___m_szFileName_23;
	// System.Boolean Polygon::m_bIsGrass
	bool ___m_bIsGrass_24;
	// System.Boolean Polygon::m_bIsGrassSeed
	bool ___m_bIsGrassSeed_25;
	// System.Boolean Polygon::m_bNowSeeding
	bool ___m_bNowSeeding_26;
	// UnityEngine.Mesh Polygon::m_meshGrass
	Mesh_t3648964284 * ___m_meshGrass_27;
	// UnityEngine.MeshRenderer Polygon::m_mrGrass
	MeshRenderer_t587009260 * ___m_mrGrass_28;
	// UnityEngine.Material Polygon::m_matMain
	Material_t340375123 * ___m_matMain_29;
	// UnityEngine.GameObject Polygon::m_goSeed
	GameObject_t1113636619 * ___m_goSeed_30;
	// UnityEngine.Vector3 Polygon::vecTempPos
	Vector3_t3722313464  ___vecTempPos_31;
	// UnityEngine.Vector3 Polygon::vecTempScale
	Vector3_t3722313464  ___vecTempScale_32;
	// System.Int32 Polygon::m_nOwnerPhotonId
	int32_t ___m_nOwnerPhotonId_33;
	// System.Double Polygon::m_dGrassActivatedTime
	double ___m_dGrassActivatedTime_34;
	// System.Int32 Polygon::m_nGUID
	int32_t ___m_nGUID_35;
	// System.Int32 Polygon::m_nHardObstacle
	int32_t ___m_nHardObstacle_36;
	// UnityEngine.Vector2 Polygon::m_vecCenter
	Vector2_t2156229523  ___m_vecCenter_38;
	// System.Single Polygon::m_fSeedGrassLifeTime
	float ___m_fSeedGrassLifeTime_39;
	// System.Single Polygon::m_fCurAlpha
	float ___m_fCurAlpha_40;
	// System.Int32 Polygon::m_nStatus
	int32_t ___m_nStatus_41;
	// System.Single Polygon::m_fAlphaCounting
	float ___m_fAlphaCounting_42;
	// System.Single Polygon::m_fSeedSize
	float ___m_fSeedSize_43;

public:
	inline static int32_t get_offset_of_m_lstPoint_17() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_lstPoint_17)); }
	inline List_1_t1388762007 * get_m_lstPoint_17() const { return ___m_lstPoint_17; }
	inline List_1_t1388762007 ** get_address_of_m_lstPoint_17() { return &___m_lstPoint_17; }
	inline void set_m_lstPoint_17(List_1_t1388762007 * value)
	{
		___m_lstPoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPoint_17), value);
	}

	inline static int32_t get_offset_of_m_dicLine_18() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_dicLine_18)); }
	inline Dictionary_2_t1680981515 * get_m_dicLine_18() const { return ___m_dicLine_18; }
	inline Dictionary_2_t1680981515 ** get_address_of_m_dicLine_18() { return &___m_dicLine_18; }
	inline void set_m_dicLine_18(Dictionary_2_t1680981515 * value)
	{
		___m_dicLine_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLine_18), value);
	}

	inline static int32_t get_offset_of_m_dicLineTemp_19() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_dicLineTemp_19)); }
	inline Dictionary_2_t1680981515 * get_m_dicLineTemp_19() const { return ___m_dicLineTemp_19; }
	inline Dictionary_2_t1680981515 ** get_address_of_m_dicLineTemp_19() { return &___m_dicLineTemp_19; }
	inline void set_m_dicLineTemp_19(Dictionary_2_t1680981515 * value)
	{
		___m_dicLineTemp_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLineTemp_19), value);
	}

	inline static int32_t get_offset_of__collierEdge_20() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ____collierEdge_20)); }
	inline EdgeCollider2D_t2781859275 * get__collierEdge_20() const { return ____collierEdge_20; }
	inline EdgeCollider2D_t2781859275 ** get_address_of__collierEdge_20() { return &____collierEdge_20; }
	inline void set__collierEdge_20(EdgeCollider2D_t2781859275 * value)
	{
		____collierEdge_20 = value;
		Il2CppCodeGenWriteBarrier((&____collierEdge_20), value);
	}

	inline static int32_t get_offset_of__collierPolygon_21() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ____collierPolygon_21)); }
	inline PolygonCollider2D_t57175488 * get__collierPolygon_21() const { return ____collierPolygon_21; }
	inline PolygonCollider2D_t57175488 ** get_address_of__collierPolygon_21() { return &____collierPolygon_21; }
	inline void set__collierPolygon_21(PolygonCollider2D_t57175488 * value)
	{
		____collierPolygon_21 = value;
		Il2CppCodeGenWriteBarrier((&____collierPolygon_21), value);
	}

	inline static int32_t get_offset_of__colliderSeed_22() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ____colliderSeed_22)); }
	inline CircleCollider2D_t662546754 * get__colliderSeed_22() const { return ____colliderSeed_22; }
	inline CircleCollider2D_t662546754 ** get_address_of__colliderSeed_22() { return &____colliderSeed_22; }
	inline void set__colliderSeed_22(CircleCollider2D_t662546754 * value)
	{
		____colliderSeed_22 = value;
		Il2CppCodeGenWriteBarrier((&____colliderSeed_22), value);
	}

	inline static int32_t get_offset_of_m_szFileName_23() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_szFileName_23)); }
	inline String_t* get_m_szFileName_23() const { return ___m_szFileName_23; }
	inline String_t** get_address_of_m_szFileName_23() { return &___m_szFileName_23; }
	inline void set_m_szFileName_23(String_t* value)
	{
		___m_szFileName_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_szFileName_23), value);
	}

	inline static int32_t get_offset_of_m_bIsGrass_24() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_bIsGrass_24)); }
	inline bool get_m_bIsGrass_24() const { return ___m_bIsGrass_24; }
	inline bool* get_address_of_m_bIsGrass_24() { return &___m_bIsGrass_24; }
	inline void set_m_bIsGrass_24(bool value)
	{
		___m_bIsGrass_24 = value;
	}

	inline static int32_t get_offset_of_m_bIsGrassSeed_25() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_bIsGrassSeed_25)); }
	inline bool get_m_bIsGrassSeed_25() const { return ___m_bIsGrassSeed_25; }
	inline bool* get_address_of_m_bIsGrassSeed_25() { return &___m_bIsGrassSeed_25; }
	inline void set_m_bIsGrassSeed_25(bool value)
	{
		___m_bIsGrassSeed_25 = value;
	}

	inline static int32_t get_offset_of_m_bNowSeeding_26() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_bNowSeeding_26)); }
	inline bool get_m_bNowSeeding_26() const { return ___m_bNowSeeding_26; }
	inline bool* get_address_of_m_bNowSeeding_26() { return &___m_bNowSeeding_26; }
	inline void set_m_bNowSeeding_26(bool value)
	{
		___m_bNowSeeding_26 = value;
	}

	inline static int32_t get_offset_of_m_meshGrass_27() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_meshGrass_27)); }
	inline Mesh_t3648964284 * get_m_meshGrass_27() const { return ___m_meshGrass_27; }
	inline Mesh_t3648964284 ** get_address_of_m_meshGrass_27() { return &___m_meshGrass_27; }
	inline void set_m_meshGrass_27(Mesh_t3648964284 * value)
	{
		___m_meshGrass_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshGrass_27), value);
	}

	inline static int32_t get_offset_of_m_mrGrass_28() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_mrGrass_28)); }
	inline MeshRenderer_t587009260 * get_m_mrGrass_28() const { return ___m_mrGrass_28; }
	inline MeshRenderer_t587009260 ** get_address_of_m_mrGrass_28() { return &___m_mrGrass_28; }
	inline void set_m_mrGrass_28(MeshRenderer_t587009260 * value)
	{
		___m_mrGrass_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_mrGrass_28), value);
	}

	inline static int32_t get_offset_of_m_matMain_29() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_matMain_29)); }
	inline Material_t340375123 * get_m_matMain_29() const { return ___m_matMain_29; }
	inline Material_t340375123 ** get_address_of_m_matMain_29() { return &___m_matMain_29; }
	inline void set_m_matMain_29(Material_t340375123 * value)
	{
		___m_matMain_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_matMain_29), value);
	}

	inline static int32_t get_offset_of_m_goSeed_30() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_goSeed_30)); }
	inline GameObject_t1113636619 * get_m_goSeed_30() const { return ___m_goSeed_30; }
	inline GameObject_t1113636619 ** get_address_of_m_goSeed_30() { return &___m_goSeed_30; }
	inline void set_m_goSeed_30(GameObject_t1113636619 * value)
	{
		___m_goSeed_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_goSeed_30), value);
	}

	inline static int32_t get_offset_of_vecTempPos_31() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___vecTempPos_31)); }
	inline Vector3_t3722313464  get_vecTempPos_31() const { return ___vecTempPos_31; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_31() { return &___vecTempPos_31; }
	inline void set_vecTempPos_31(Vector3_t3722313464  value)
	{
		___vecTempPos_31 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_32() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___vecTempScale_32)); }
	inline Vector3_t3722313464  get_vecTempScale_32() const { return ___vecTempScale_32; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_32() { return &___vecTempScale_32; }
	inline void set_vecTempScale_32(Vector3_t3722313464  value)
	{
		___vecTempScale_32 = value;
	}

	inline static int32_t get_offset_of_m_nOwnerPhotonId_33() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_nOwnerPhotonId_33)); }
	inline int32_t get_m_nOwnerPhotonId_33() const { return ___m_nOwnerPhotonId_33; }
	inline int32_t* get_address_of_m_nOwnerPhotonId_33() { return &___m_nOwnerPhotonId_33; }
	inline void set_m_nOwnerPhotonId_33(int32_t value)
	{
		___m_nOwnerPhotonId_33 = value;
	}

	inline static int32_t get_offset_of_m_dGrassActivatedTime_34() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_dGrassActivatedTime_34)); }
	inline double get_m_dGrassActivatedTime_34() const { return ___m_dGrassActivatedTime_34; }
	inline double* get_address_of_m_dGrassActivatedTime_34() { return &___m_dGrassActivatedTime_34; }
	inline void set_m_dGrassActivatedTime_34(double value)
	{
		___m_dGrassActivatedTime_34 = value;
	}

	inline static int32_t get_offset_of_m_nGUID_35() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_nGUID_35)); }
	inline int32_t get_m_nGUID_35() const { return ___m_nGUID_35; }
	inline int32_t* get_address_of_m_nGUID_35() { return &___m_nGUID_35; }
	inline void set_m_nGUID_35(int32_t value)
	{
		___m_nGUID_35 = value;
	}

	inline static int32_t get_offset_of_m_nHardObstacle_36() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_nHardObstacle_36)); }
	inline int32_t get_m_nHardObstacle_36() const { return ___m_nHardObstacle_36; }
	inline int32_t* get_address_of_m_nHardObstacle_36() { return &___m_nHardObstacle_36; }
	inline void set_m_nHardObstacle_36(int32_t value)
	{
		___m_nHardObstacle_36 = value;
	}

	inline static int32_t get_offset_of_m_vecCenter_38() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_vecCenter_38)); }
	inline Vector2_t2156229523  get_m_vecCenter_38() const { return ___m_vecCenter_38; }
	inline Vector2_t2156229523 * get_address_of_m_vecCenter_38() { return &___m_vecCenter_38; }
	inline void set_m_vecCenter_38(Vector2_t2156229523  value)
	{
		___m_vecCenter_38 = value;
	}

	inline static int32_t get_offset_of_m_fSeedGrassLifeTime_39() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_fSeedGrassLifeTime_39)); }
	inline float get_m_fSeedGrassLifeTime_39() const { return ___m_fSeedGrassLifeTime_39; }
	inline float* get_address_of_m_fSeedGrassLifeTime_39() { return &___m_fSeedGrassLifeTime_39; }
	inline void set_m_fSeedGrassLifeTime_39(float value)
	{
		___m_fSeedGrassLifeTime_39 = value;
	}

	inline static int32_t get_offset_of_m_fCurAlpha_40() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_fCurAlpha_40)); }
	inline float get_m_fCurAlpha_40() const { return ___m_fCurAlpha_40; }
	inline float* get_address_of_m_fCurAlpha_40() { return &___m_fCurAlpha_40; }
	inline void set_m_fCurAlpha_40(float value)
	{
		___m_fCurAlpha_40 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_41() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_nStatus_41)); }
	inline int32_t get_m_nStatus_41() const { return ___m_nStatus_41; }
	inline int32_t* get_address_of_m_nStatus_41() { return &___m_nStatus_41; }
	inline void set_m_nStatus_41(int32_t value)
	{
		___m_nStatus_41 = value;
	}

	inline static int32_t get_offset_of_m_fAlphaCounting_42() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_fAlphaCounting_42)); }
	inline float get_m_fAlphaCounting_42() const { return ___m_fAlphaCounting_42; }
	inline float* get_address_of_m_fAlphaCounting_42() { return &___m_fAlphaCounting_42; }
	inline void set_m_fAlphaCounting_42(float value)
	{
		___m_fAlphaCounting_42 = value;
	}

	inline static int32_t get_offset_of_m_fSeedSize_43() { return static_cast<int32_t>(offsetof(Polygon_t12692255, ___m_fSeedSize_43)); }
	inline float get_m_fSeedSize_43() const { return ___m_fSeedSize_43; }
	inline float* get_address_of_m_fSeedSize_43() { return &___m_fSeedSize_43; }
	inline void set_m_fSeedSize_43(float value)
	{
		___m_fSeedSize_43 = value;
	}
};

struct Polygon_t12692255_StaticFields
{
public:
	// UnityEngine.Vector2 Polygon::Vec2Temp
	Vector2_t2156229523  ___Vec2Temp_37;

public:
	inline static int32_t get_offset_of_Vec2Temp_37() { return static_cast<int32_t>(offsetof(Polygon_t12692255_StaticFields, ___Vec2Temp_37)); }
	inline Vector2_t2156229523  get_Vec2Temp_37() const { return ___Vec2Temp_37; }
	inline Vector2_t2156229523 * get_address_of_Vec2Temp_37() { return &___Vec2Temp_37; }
	inline void set_Vec2Temp_37(Vector2_t2156229523  value)
	{
		___Vec2Temp_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T12692255_H
#ifndef SHOWINFOOFPLAYER_T601303432_H
#define SHOWINFOOFPLAYER_T601303432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowInfoOfPlayer
struct  ShowInfoOfPlayer_t601303432  : public MonoBehaviour_t3225183318
{
public:
	// UnityEngine.GameObject ShowInfoOfPlayer::textGo
	GameObject_t1113636619 * ___textGo_3;
	// UnityEngine.TextMesh ShowInfoOfPlayer::tm
	TextMesh_t1536577757 * ___tm_4;
	// System.Single ShowInfoOfPlayer::CharacterSize
	float ___CharacterSize_5;
	// UnityEngine.Font ShowInfoOfPlayer::font
	Font_t1956802104 * ___font_6;
	// System.Boolean ShowInfoOfPlayer::DisableOnOwnObjects
	bool ___DisableOnOwnObjects_7;

public:
	inline static int32_t get_offset_of_textGo_3() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___textGo_3)); }
	inline GameObject_t1113636619 * get_textGo_3() const { return ___textGo_3; }
	inline GameObject_t1113636619 ** get_address_of_textGo_3() { return &___textGo_3; }
	inline void set_textGo_3(GameObject_t1113636619 * value)
	{
		___textGo_3 = value;
		Il2CppCodeGenWriteBarrier((&___textGo_3), value);
	}

	inline static int32_t get_offset_of_tm_4() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___tm_4)); }
	inline TextMesh_t1536577757 * get_tm_4() const { return ___tm_4; }
	inline TextMesh_t1536577757 ** get_address_of_tm_4() { return &___tm_4; }
	inline void set_tm_4(TextMesh_t1536577757 * value)
	{
		___tm_4 = value;
		Il2CppCodeGenWriteBarrier((&___tm_4), value);
	}

	inline static int32_t get_offset_of_CharacterSize_5() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___CharacterSize_5)); }
	inline float get_CharacterSize_5() const { return ___CharacterSize_5; }
	inline float* get_address_of_CharacterSize_5() { return &___CharacterSize_5; }
	inline void set_CharacterSize_5(float value)
	{
		___CharacterSize_5 = value;
	}

	inline static int32_t get_offset_of_font_6() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___font_6)); }
	inline Font_t1956802104 * get_font_6() const { return ___font_6; }
	inline Font_t1956802104 ** get_address_of_font_6() { return &___font_6; }
	inline void set_font_6(Font_t1956802104 * value)
	{
		___font_6 = value;
		Il2CppCodeGenWriteBarrier((&___font_6), value);
	}

	inline static int32_t get_offset_of_DisableOnOwnObjects_7() { return static_cast<int32_t>(offsetof(ShowInfoOfPlayer_t601303432, ___DisableOnOwnObjects_7)); }
	inline bool get_DisableOnOwnObjects_7() const { return ___DisableOnOwnObjects_7; }
	inline bool* get_address_of_DisableOnOwnObjects_7() { return &___DisableOnOwnObjects_7; }
	inline void set_DisableOnOwnObjects_7(bool value)
	{
		___DisableOnOwnObjects_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWINFOOFPLAYER_T601303432_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef SMOOTHSYNCMOVEMENT_T1809568931_H
#define SMOOTHSYNCMOVEMENT_T1809568931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothSyncMovement
struct  SmoothSyncMovement_t1809568931  : public MonoBehaviour_t3225183318
{
public:
	// System.Single SmoothSyncMovement::SmoothingDelay
	float ___SmoothingDelay_3;
	// UnityEngine.Vector3 SmoothSyncMovement::correctPlayerPos
	Vector3_t3722313464  ___correctPlayerPos_4;
	// UnityEngine.Quaternion SmoothSyncMovement::correctPlayerRot
	Quaternion_t2301928331  ___correctPlayerRot_5;

public:
	inline static int32_t get_offset_of_SmoothingDelay_3() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___SmoothingDelay_3)); }
	inline float get_SmoothingDelay_3() const { return ___SmoothingDelay_3; }
	inline float* get_address_of_SmoothingDelay_3() { return &___SmoothingDelay_3; }
	inline void set_SmoothingDelay_3(float value)
	{
		___SmoothingDelay_3 = value;
	}

	inline static int32_t get_offset_of_correctPlayerPos_4() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___correctPlayerPos_4)); }
	inline Vector3_t3722313464  get_correctPlayerPos_4() const { return ___correctPlayerPos_4; }
	inline Vector3_t3722313464 * get_address_of_correctPlayerPos_4() { return &___correctPlayerPos_4; }
	inline void set_correctPlayerPos_4(Vector3_t3722313464  value)
	{
		___correctPlayerPos_4 = value;
	}

	inline static int32_t get_offset_of_correctPlayerRot_5() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___correctPlayerRot_5)); }
	inline Quaternion_t2301928331  get_correctPlayerRot_5() const { return ___correctPlayerRot_5; }
	inline Quaternion_t2301928331 * get_address_of_correctPlayerRot_5() { return &___correctPlayerRot_5; }
	inline void set_correctPlayerRot_5(Quaternion_t2301928331  value)
	{
		___correctPlayerRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHSYNCMOVEMENT_T1809568931_H
#ifndef PLAYER_T3266647312_H
#define PLAYER_T3266647312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t3266647312  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.Color Player::m_colorMe
	Color_t2555686324  ___m_colorMe_3;
	// System.Int32 Player::m_nSkinId
	int32_t ___m_nSkinId_4;
	// System.Boolean Player::_isRebornProtected
	bool ____isRebornProtected_5;
	// UnityEngine.Color Player::_color_inner
	Color_t2555686324  ____color_inner_6;
	// UnityEngine.Color Player::_color_ring
	Color_t2555686324  ____color_ring_7;
	// UnityEngine.Color Player::_color_poison
	Color_t2555686324  ____color_poison_8;
	// System.Single Player::m_fProtectTime
	float ___m_fProtectTime_9;
	// System.Collections.Generic.List`1<Ball> Player::m_lstBalls
	List_1_t3678741308 * ___m_lstBalls_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,Ball> Player::m_dicActiveBalls
	Dictionary_2_t1095379897 * ___m_dicActiveBalls_11;
	// System.Int32 Player::m_nBallIndex
	int32_t ___m_nBallIndex_12;
	// UnityEngine.Vector2 Player::m_vecPos
	Vector2_t2156229523  ___m_vecPos_13;
	// CSkillManager Player::m_SkillManager
	CSkillManager_t2937013327 * ___m_SkillManager_19;
	// CPlayerIns Player::_PlayerIns
	CPlayerIns_t3208190728 * ____PlayerIns_20;
	// System.Single Player::m_fTotalEatArea
	float ___m_fTotalEatArea_21;
	// System.Single Player::m_fTotalEatFoodArea
	float ___m_fTotalEatFoodArea_22;
	// System.Int32 Player::m_nKillCount
	int32_t ___m_nKillCount_23;
	// System.Int32 Player::m_nBeKilled
	int32_t ___m_nBeKilled_24;
	// System.Int32 Player::m_IncAssistAttackCount
	int32_t ___m_IncAssistAttackCount_25;
	// System.Int32 Player::m_nEatThornNum
	int32_t ___m_nEatThornNum_26;
	// System.Int32 Player::m_nContinuousKillNum
	int32_t ___m_nContinuousKillNum_27;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Player::m_dicDuoShaoRecord
	Dictionary_2_t285980105 * ___m_dicDuoShaoRecord_28;
	// System.Int32 Player::m_nDuoSha
	int32_t ___m_nDuoSha_29;
	// System.Int32[] Player::m_aryItemNum
	Int32U5BU5D_t385246372* ___m_aryItemNum_30;
	// Player/sPlayerBaseData Player::m_BaseData
	sPlayerBaseData_t1472229795  ___m_BaseData_31;
	// System.Single Player::m_fBaseVolumeByLevel
	float ___m_fBaseVolumeByLevel_32;
	// System.Single Player::m_fProcessSelfCount
	float ___m_fProcessSelfCount_34;
	// System.Single Player::m_fLoop_1_Second_TimeElapse
	float ___m_fLoop_1_Second_TimeElapse_35;
	// System.Boolean Player::m_bQueryingFromMasterClientDataReceived
	bool ___m_bQueryingFromMasterClientDataReceived_37;
	// System.Single Player::m_fQueryingFromMasterClientTimeElapse
	float ___m_fQueryingFromMasterClientTimeElapse_38;
	// System.Boolean Player::m_bQueryingFromMainPlayerDataReceived
	bool ___m_bQueryingFromMainPlayerDataReceived_39;
	// System.Single Player::m_fQueryingFromMainPlayerTimeElapse
	float ___m_fQueryingFromMainPlayerTimeElapse_40;
	// System.Boolean Player::m_bOrphanBallsDataReceived
	bool ___m_bOrphanBallsDataReceived_41;
	// System.Boolean Player::m_bIniting
	bool ___m_bIniting_42;
	// System.Boolean Player::m_bInitCompleted
	bool ___m_bInitCompleted_43;
	// System.Boolean Player::m_bBallInstantiated
	bool ___m_bBallInstantiated_44;
	// System.Single Player::m_fInitingBallsTimeCount
	float ___m_fInitingBallsTimeCount_46;
	// System.Int32 Player::m_nKingExplodeIndex
	int32_t ___m_nKingExplodeIndex_47;
	// System.Boolean Player::m_bKingExploding
	bool ___m_bKingExploding_48;
	// System.String Player::m_szPlayerName
	String_t* ___m_szPlayerName_49;
	// System.String Player::m_szAccount
	String_t* ___m_szAccount_50;
	// System.Boolean Player::m_bPublicInfoInited
	bool ___m_bPublicInfoInited_52;
	// System.Int32 Player::m_nPlayerPlatform
	int32_t ___m_nPlayerPlatform_53;
	// System.Boolean Player::m_bObserve
	bool ___m_bObserve_55;
	// UnityEngine.Vector3 Player::m_vWorldCursorPos
	Vector3_t3722313464  ___m_vWorldCursorPos_56;
	// UnityEngine.Vector2 Player::m_vSpecialDirecton
	Vector2_t2156229523  ___m_vSpecialDirecton_57;
	// System.Single Player::m_fRealTimeSpeedWithoutSize
	float ___m_fRealTimeSpeedWithoutSize_58;
	// System.Single Player::m_fSyncMoveInfoCount
	float ___m_fSyncMoveInfoCount_59;
	// System.Single Player::m_fBaseSpeed
	float ___m_fBaseSpeed_60;
	// System.Single Player::m_fShellTimeBuffEffect
	float ___m_fShellTimeBuffEffect_61;
	// System.Boolean Player::m_bTestMove
	bool ___m_bTestMove_64;
	// System.Boolean Player::m_bMoving
	bool ___m_bMoving_65;
	// System.Single Player::m_fClearSpitTargetTotalTime
	float ___m_fClearSpitTargetTotalTime_66;
	// System.Int32 Player::m_nSpitIndex
	int32_t ___m_nSpitIndex_67;
	// System.Boolean Player::m_bSpitting
	bool ___m_bSpitting_68;
	// System.Single Player::m_fSpitBallHalfMpCost
	float ___m_fSpitBallHalfMpCost_69;
	// System.Single Player::m_fSpitBallHalfColddown
	float ___m_fSpitBallHalfColddown_70;
	// System.Single Player::m_fSpitBallHalfQianYao
	float ___m_fSpitBallHalfQianYao_71;
	// System.Single Player::m_fSpitBallHalfDistance
	float ___m_fSpitBallHalfDistance_72;
	// System.Single Player::m_fSpitBallHalfRunTime
	float ___m_fSpitBallHalfRunTime_73;
	// System.Single Player::m_fSpitBallHalfShellTime
	float ___m_fSpitBallHalfShellTime_74;
	// System.Int32 Player::m_nPreCastSkill_Status_SpitHalf
	int32_t ___m_nPreCastSkill_Status_SpitHalf_75;
	// System.Boolean Player::m_bPreCastSkill_SpitHalf
	bool ___m_bPreCastSkill_SpitHalf_76;
	// System.Single Player::m_fPreCastSkill_SpitHalf_TimeCount
	float ___m_fPreCastSkill_SpitHalf_TimeCount_77;
	// System.Int32 Player::m_nWSpitStatus
	int32_t ___m_nWSpitStatus_78;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Player::m_lstTempEjectEndPos
	List_1_t899420910 * ___m_lstTempEjectEndPos_80;
	// System.Single Player::m_fSpitBallCostMp
	float ___m_fSpitBallCostMp_81;
	// System.Single Player::m_fSpitBallDistance
	float ___m_fSpitBallDistance_82;
	// System.Single Player::m_fSpitBallRunTime
	float ___m_fSpitBallRunTime_83;
	// System.Single Player::m_fSpitBallShellTime
	float ___m_fSpitBallShellTime_84;
	// System.Single Player::m_fForceSpitTotalTime
	float ___m_fForceSpitTotalTime_85;
	// System.Boolean Player::m_fForcing
	bool ___m_fForcing_86;
	// System.Single Player::m_fForceSpitCurTime
	float ___m_fForceSpitCurTime_87;
	// System.Single Player::m_fSpitPercent
	float ___m_fSpitPercent_88;
	// CGunsight[] Player::m_aryGunsight
	CGunsightU5BU5D_t395838697* ___m_aryGunsight_89;
	// System.Single Player::m_fPreDrawCount
	float ___m_fPreDrawCount_90;
	// UnityEngine.Vector3 Player::m_vecBalslsCenter
	Vector3_t3722313464  ___m_vecBalslsCenter_91;
	// System.Single Player::m_fDistanceToSlowDown
	float ___m_fDistanceToSlowDown_92;
	// System.Collections.Generic.List`1<System.Int32> Player::m_lstToMergeAllBalls
	List_1_t128053199 * ___m_lstToMergeAllBalls_93;
	// System.Collections.Generic.List`1<CMiaoHeEffect> Player::m_lstMiaoHeEffect
	List_1_t2306155490 * ___m_lstMiaoHeEffect_95;
	// System.Collections.Generic.List`1<Player/sSplitInfo> Player::m_lstRecycledSpiltInfo
	List_1_t478107080 * ___m_lstRecycledSpiltInfo_96;
	// System.Boolean Player::m_bSplitting
	bool ___m_bSplitting_97;
	// System.Int32 Player::m_nCurSplitTimes
	int32_t ___m_nCurSplitTimes_98;
	// System.Int32 Player::m_nRealSplitNum
	int32_t ___m_nRealSplitNum_99;
	// System.Collections.Generic.List`1<Player/sSplitInfo> Player::m_lstSplitBalls
	List_1_t478107080 * ___m_lstSplitBalls_100;
	// System.Collections.Generic.List`1<System.Int32> Player::m_lstToExplodeBalls
	List_1_t128053199 * ___m_lstToExplodeBalls_102;
	// System.Single Player::m_fUpdateBuffCount
	float ___m_fUpdateBuffCount_103;
	// System.String Player::m_szSpitSporeMonsterId
	String_t* ___m_szSpitSporeMonsterId_105;
	// System.Single Player::m_fSpitSporeDis
	float ___m_fSpitSporeDis_106;
	// System.Single Player::m_fSpitSporeTimeInterval
	float ___m_fSpitSporeTimeInterval_107;
	// System.Single Player::m_fSpitSporeMpCost
	float ___m_fSpitSporeMpCost_108;
	// System.Boolean Player::m_bSpittingSpore
	bool ___m_bSpittingSpore_109;
	// System.Single Player::m_fSporeEjectSpeed
	float ___m_fSporeEjectSpeed_110;
	// System.Int32 Player::m_nCurSporeLevel
	int32_t ___m_nCurSporeLevel_111;
	// System.Collections.Generic.List`1<System.UInt32> Player::m_lstSporeToSpit
	List_1_t4032136720 * ___m_lstSporeToSpit_112;
	// System.Collections.Generic.List`1<System.UInt32> Player::lstSporeToSpitTemp
	List_1_t4032136720 * ___lstSporeToSpitTemp_113;
	// System.Single Player::m_fSpitSporeInterval
	float ___m_fSpitSporeInterval_114;
	// System.Single Player::m_fSpitSporeDistance
	float ___m_fSpitSporeDistance_115;
	// System.Single Player::m_fSpitSporeTimeCount
	float ___m_fSpitSporeTimeCount_116;
	// System.Single Player::m_fGenerateSporeIdTimeCount
	float ___m_fGenerateSporeIdTimeCount_117;
	// System.Int32 Player::m_nSporeNumPerSec
	int32_t ___m_nSporeNumPerSec_118;
	// System.UInt32 Player::m_uSporeGuid
	uint32_t ___m_uSporeGuid_119;
	// System.Collections.Generic.List`1<CSpore> Player::m_lstSporeToSync
	List_1_t1482975291 * ___m_lstSporeToSync_121;
	// System.UInt32 Player::m_uSporeIdCount
	uint32_t ___m_uSporeIdCount_122;
	// System.Single Player::m_fSpitSporeBlingCount
	float ___m_fSpitSporeBlingCount_123;
	// System.Byte[] Player::_bytesSpore
	ByteU5BU5D_t4116647657* ____bytesSpore_124;
	// System.Single Player::m_nAttenuateTime
	float ___m_nAttenuateTime_125;
	// System.Collections.Generic.List`1<System.Int32> Player::m_lstLiveBall
	List_1_t128053199 * ___m_lstLiveBall_126;
	// System.Int32 Player::m_nBallNum
	int32_t ___m_nBallNum_127;
	// System.Collections.Generic.List`1<System.Int32> Player::m_lstSomeAvailabelBallIndex
	List_1_t128053199 * ___m_lstSomeAvailabelBallIndex_128;
	// System.Collections.Generic.List`1<Ball> Player::m_lstRecycledBalls
	List_1_t3678741308 * ___m_lstRecycledBalls_129;
	// System.Boolean Player::m_bDead
	bool ___m_bDead_130;
	// System.Int32 Player::m_nEaterId
	int32_t ___m_nEaterId_131;
	// System.Boolean Player::m_bReborn
	bool ___m_bReborn_132;
	// System.Boolean Player::m_bAllBallsMovingToCenter
	bool ___m_bAllBallsMovingToCenter_133;
	// Ball Player::m_ballBiggest
	Ball_t2206666566 * ___m_ballBiggest_134;
	// System.Int32 Player::m_nTestExplodeStatus
	int32_t ___m_nTestExplodeStatus_135;
	// System.Boolean Player::m_bTestPlayer
	bool ___m_bTestPlayer_136;
	// UnityEngine.Vector2 Player::m_vecSpecailDirection
	Vector2_t2156229523  ___m_vecSpecailDirection_137;
	// System.Single Player::m_nAdjustMoveInfoFrameCount
	float ___m_nAdjustMoveInfoFrameCount_138;
	// System.Byte[] Player::_bytestRelatedInfo
	ByteU5BU5D_t4116647657* ____bytestRelatedInfo_142;
	// System.Single Player::m_nSyncMoveInfoTimeCount
	float ___m_nSyncMoveInfoTimeCount_144;
	// System.Collections.Generic.Dictionary`2<System.Int32,CCosmosGunSight> Player::m_dicGunSight
	Dictionary_2_t2518726632 * ___m_dicGunSight_146;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Player::m_dicNotPickedGunSight
	Dictionary_2_t1839659084 * ___m_dicNotPickedGunSight_147;
	// System.Boolean Player::m_bFirstReceive
	bool ___m_bFirstReceive_148;
	// System.Single Player::m_fReceiveTimeCount
	float ___m_fReceiveTimeCount_149;
	// System.Single Player::m_fReceiveTotalTime
	float ___m_fReceiveTotalTime_150;
	// System.Single Player::m_fLastTimeElapse
	float ___m_fLastTimeElapse_151;
	// System.Single Player::m_fDelay
	float ___m_fDelay_152;
	// System.Single Player::m_fTotalDelay
	float ___m_fTotalDelay_153;
	// System.Single Player::m_nDelayCount
	float ___m_nDelayCount_154;
	// System.Single Player::m_fLastReceiveTimeElapse
	float ___m_fLastReceiveTimeElapse_155;
	// System.Byte[] Player::_bytesDusts
	ByteU5BU5D_t4116647657* ____bytesDusts_156;
	// System.Single Player::m_fSyncDustTimeCount
	float ___m_fSyncDustTimeCount_158;
	// System.Single Player::m_fPaiHangBangSyncTimeCount
	float ___m_fPaiHangBangSyncTimeCount_160;
	// System.Collections.Generic.List`1<CBeanCollection/sBeanSyncInfo> Player::m_lstEatBeanSyncQueue
	List_1_t2648729287 * ___m_lstEatBeanSyncQueue_161;
	// System.Collections.Generic.List`1<CMonster> Player::m_lstEatThornSyncQueue
	List_1_t3413545680 * ___m_lstEatThornSyncQueue_162;
	// System.Collections.Generic.List`1<CMonster> Player::m_lstEatSporeSyncQueue
	List_1_t3413545680 * ___m_lstEatSporeSyncQueue_163;
	// System.Single Player::m_fEatThornSyncCount
	float ___m_fEatThornSyncCount_165;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Player::m_dicAssistAttack
	Dictionary_2_t285980105 * ___m_dicAssistAttack_166;
	// System.Collections.Generic.List`1<System.Int32> Player::m_lstValidAssitPlayerId
	List_1_t128053199 * ___m_lstValidAssitPlayerId_167;
	// System.Single Player::m_fExplodeChildNumBuffEffect
	float ___m_fExplodeChildNumBuffEffect_168;
	// System.Single Player::m_fExplodeMotherLeftBuffEffect
	float ___m_fExplodeMotherLeftBuffEffect_169;
	// System.Collections.Generic.List`1<CExplodeNode> Player::m_lstExplodingNode
	List_1_t1569919409 * ___m_lstExplodingNode_171;
	// System.Collections.Generic.List`1<CExplodeNode> Player::m_lstRecycledExplodeNode
	List_1_t1569919409 * ___m_lstRecycledExplodeNode_172;
	// System.Int32 Player::m_nCurSkinIndex
	int32_t ___m_nCurSkinIndex_173;
	// UnityEngine.Sprite Player::m_sprCurAvatar
	Sprite_t280657092 * ___m_sprCurAvatar_174;
	// System.Boolean Player::m_bMyDataLoaded
	bool ___m_bMyDataLoaded_175;
	// System.Single Player::m_SyncPosAfterEjectTotalTime
	float ___m_SyncPosAfterEjectTotalTime_176;
	// System.Single Player::m_fSkillMpCost_MagicShield
	float ___m_fSkillMpCost_MagicShield_177;
	// System.Single Player::m_fSkillDuration_MagicShield
	float ___m_fSkillDuration_MagicShield_178;
	// System.Boolean Player::m_bMagicShielding
	bool ___m_bMagicShielding_179;
	// System.Single Player::m_fSkillSpeedAffect_MagicShield
	float ___m_fSkillSpeedAffect_MagicShield_180;
	// System.Single Player::m_fSkillLoopCount_MagicShield
	float ___m_fSkillLoopCount_MagicShield_181;
	// System.Single Player::m_fSkillMpCost_Annihilate
	float ___m_fSkillMpCost_Annihilate_182;
	// System.Single Player::m_fSkillPercent_Annihilate
	float ___m_fSkillPercent_Annihilate_183;
	// System.Single Player::m_fSkillDuration_Annihilate
	float ___m_fSkillDuration_Annihilate_184;
	// System.Single Player::m_fSkillSpeedAffect_Annihilate
	float ___m_fSkillSpeedAffect_Annihilate_185;
	// System.Boolean Player::m_bAnnihilating
	bool ___m_bAnnihilating_186;
	// System.Single Player::m_fSkillLoopCount_Annihilate
	float ___m_fSkillLoopCount_Annihilate_187;
	// System.Single Player::m_fSkillMpCost_Gold
	float ___m_fSkillMpCost_Gold_188;
	// System.Single Player::m_fSkillGold_SpeedAddPercent
	float ___m_fSkillGold_SpeedAddPercent_189;
	// System.Single Player::m_fGold_Duration
	float ___m_fGold_Duration_190;
	// System.Boolean Player::m_bGold
	bool ___m_bGold_191;
	// System.Single Player::m_fGoldLoopCount
	float ___m_fGoldLoopCount_192;
	// System.Single Player::m_fSkillMpCost_Henzy
	float ___m_fSkillMpCost_Henzy_193;
	// System.Single Player::m_fHenzy_Duration
	float ___m_fHenzy_Duration_194;
	// System.Single Player::m_fHenzy_SpeedChangePercent
	float ___m_fHenzy_SpeedChangePercent_195;
	// System.Single Player::m_fHenzy_WColdDown
	float ___m_fHenzy_WColdDown_196;
	// System.Single Player::m_fHenzy_EColdDown
	float ___m_fHenzy_EColdDown_197;
	// System.Boolean Player::m_bHenzy
	bool ___m_bHenzy_198;
	// System.Single Player::m_fHenzyLoopCount
	float ___m_fHenzyLoopCount_199;
	// System.Single Player::m_fSkillMpCost_MergeAll
	float ___m_fSkillMpCost_MergeAll_200;
	// System.Single Player::m_fSkillQianYao_MergeAll
	float ___m_fSkillQianYao_MergeAll_201;
	// System.Boolean Player::m_bMiaoHeQianYao
	bool ___m_bMiaoHeQianYao_202;
	// System.Single Player::m_fSkillQianYaoCount_MergeAll
	float ___m_fSkillQianYaoCount_MergeAll_203;
	// System.Single Player::m_fMiaoHeQiaoYaoRotation
	float ___m_fMiaoHeQiaoYaoRotation_204;
	// System.Single Player::m_fSkillMpCost_Sneak
	float ___m_fSkillMpCost_Sneak_205;
	// System.Single Player::m_fSkillSpeedChangePercent_Sneak
	float ___m_fSkillSpeedChangePercent_Sneak_206;
	// System.Single Player::m_fSkillDuration_Sneak
	float ___m_fSkillDuration_Sneak_207;
	// System.Boolean Player::m_bSneaking
	bool ___m_bSneaking_208;
	// System.Single Player::m_fSkillDurationCount_Sneak
	float ___m_fSkillDurationCount_Sneak_209;
	// System.Single Player::m_fMpCost_BecomeThorn
	float ___m_fMpCost_BecomeThorn_210;
	// System.Single Player::m_fQianYao_BecomeThorn
	float ___m_fQianYao_BecomeThorn_211;
	// System.Single Player::m_fSpeedChangePercent_BecomeThorn
	float ___m_fSpeedChangePercent_BecomeThorn_212;
	// System.Single Player::m_fDuration_BecomeThorn
	float ___m_fDuration_BecomeThorn_213;
	// System.String Player::m_szExplodeConfigId_BecomeThorn
	String_t* ___m_szExplodeConfigId_BecomeThorn_214;
	// System.Single Player::m_fBecomeThornTimeCount
	float ___m_fBecomeThornTimeCount_215;
	// System.Int32 Player::m_nBecomeThornStatus
	int32_t ___m_nBecomeThornStatus_216;
	// System.Single Player::m_fUnfoldCostMp
	float ___m_fUnfoldCostMp_217;
	// System.Single Player::m_fPreUnfoldTime
	float ___m_fPreUnfoldTime_218;
	// System.Single Player::m_fPreUnfoldCurTimeLapse
	float ___m_fPreUnfoldCurTimeLapse_219;
	// System.Single Player::m_fUnfoldScale
	float ___m_fUnfoldScale_220;
	// System.Single Player::m_fUnfoldKeepTime
	float ___m_fUnfoldKeepTime_221;
	// System.Single Player::m_fUnfoldCurTimeLapse
	float ___m_fUnfoldCurTimeLapse_222;
	// System.Int32 Player::m_nSkillUnfoldStatus
	int32_t ___m_nSkillUnfoldStatus_223;
	// System.Single Player::m_fPreUnfoldTimeElpase
	float ___m_fPreUnfoldTimeElpase_224;
	// System.Single Player::m_fPreUnfoldTimeElapse
	float ___m_fPreUnfoldTimeElapse_226;
	// System.Single Player::m_fCurSkillPointAffect_Unfold
	float ___m_fCurSkillPointAffect_Unfold_227;
	// System.Single Player::m_fBaseArea
	float ___m_fBaseArea_228;
	// System.Single Player::m_fTotalArea
	float ___m_fTotalArea_229;
	// System.Int32 Player::m_nCurLiveBallNum
	int32_t ___m_nCurLiveBallNum_230;
	// System.Single Player::m_fCalcTotalAreaCount
	float ___m_fCalcTotalAreaCount_231;
	// System.Collections.Generic.List`1<CBuff> Player::m_lstBuff
	List_1_t3012865878 * ___m_lstBuff_232;
	// System.Int32 Player::m_nPreAddExp
	int32_t ___m_nPreAddExp_234;
	// System.Int32 Player::m_nLevel
	int32_t ___m_nLevel_235;
	// System.Boolean Player::m_bCastSkillWhickNeedStopMoving
	bool ___m_bCastSkillWhickNeedStopMoving_236;
	// System.Collections.Generic.List`1<CMonster> Player::m_lstBeingPushedThorn
	List_1_t3413545680 * ___m_lstBeingPushedThorn_237;
	// System.Collections.Generic.List`1<CDust> Player::m_lstSyncDust
	List_1_t198291272 * ___m_lstSyncDust_238;
	// System.Collections.Generic.Dictionary`2<System.Int32,CDust> Player::m_dicSyncDust
	Dictionary_2_t1909897157 * ___m_dicSyncDust_239;
	// System.Single Player::m_fAttenuateTimeLapse
	float ___m_fAttenuateTimeLapse_240;
	// System.Int32 Player::m_nAttenuateIndex
	int32_t ___m_nAttenuateIndex_242;
	// UnityEngine.Material[] Player::s_aryMat
	MaterialU5BU5D_t561872642* ___s_aryMat_243;
	// System.Int32 Player::m_nSelectedSkillLevel
	int32_t ___m_nSelectedSkillLevel_244;
	// System.Int32 Player::m_nSelectedSkillId
	int32_t ___m_nSelectedSkillId_245;
	// System.Collections.Generic.List`1<Ball> Player::m_lstSortedBallList
	List_1_t3678741308 * ___m_lstSortedBallList_246;
	// System.Collections.Generic.List`1<CSpore> Player::m_lstEatenSpore
	List_1_t1482975291 * ___m_lstEatenSpore_247;
	// System.Byte[] Player::_bytesGestures
	ByteU5BU5D_t4116647657* ____bytesGestures_248;
	// System.Collections.Generic.List`1<System.Int32> Player::m_lstCastingGestures
	List_1_t128053199 * ___m_lstCastingGestures_249;
	// UnityEngine.Vector2 Player::vecGestureDir
	Vector2_t2156229523  ___vecGestureDir_250;
	// System.Single Player::m_fSyncPlayerCommonInfoTimeElapse
	float ___m_fSyncPlayerCommonInfoTimeElapse_251;
	// System.Byte[] Player::_bytesCommonInfo
	ByteU5BU5D_t4116647657* ____bytesCommonInfo_252;
	// System.Single Player::m_fDelayTestTimeElapse
	float ___m_fDelayTestTimeElapse_254;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Player::m_dicDelayTest_PingTime
	Dictionary_2_t285980105 * ___m_dicDelayTest_PingTime_255;

public:
	inline static int32_t get_offset_of_m_colorMe_3() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_colorMe_3)); }
	inline Color_t2555686324  get_m_colorMe_3() const { return ___m_colorMe_3; }
	inline Color_t2555686324 * get_address_of_m_colorMe_3() { return &___m_colorMe_3; }
	inline void set_m_colorMe_3(Color_t2555686324  value)
	{
		___m_colorMe_3 = value;
	}

	inline static int32_t get_offset_of_m_nSkinId_4() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSkinId_4)); }
	inline int32_t get_m_nSkinId_4() const { return ___m_nSkinId_4; }
	inline int32_t* get_address_of_m_nSkinId_4() { return &___m_nSkinId_4; }
	inline void set_m_nSkinId_4(int32_t value)
	{
		___m_nSkinId_4 = value;
	}

	inline static int32_t get_offset_of__isRebornProtected_5() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____isRebornProtected_5)); }
	inline bool get__isRebornProtected_5() const { return ____isRebornProtected_5; }
	inline bool* get_address_of__isRebornProtected_5() { return &____isRebornProtected_5; }
	inline void set__isRebornProtected_5(bool value)
	{
		____isRebornProtected_5 = value;
	}

	inline static int32_t get_offset_of__color_inner_6() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____color_inner_6)); }
	inline Color_t2555686324  get__color_inner_6() const { return ____color_inner_6; }
	inline Color_t2555686324 * get_address_of__color_inner_6() { return &____color_inner_6; }
	inline void set__color_inner_6(Color_t2555686324  value)
	{
		____color_inner_6 = value;
	}

	inline static int32_t get_offset_of__color_ring_7() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____color_ring_7)); }
	inline Color_t2555686324  get__color_ring_7() const { return ____color_ring_7; }
	inline Color_t2555686324 * get_address_of__color_ring_7() { return &____color_ring_7; }
	inline void set__color_ring_7(Color_t2555686324  value)
	{
		____color_ring_7 = value;
	}

	inline static int32_t get_offset_of__color_poison_8() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____color_poison_8)); }
	inline Color_t2555686324  get__color_poison_8() const { return ____color_poison_8; }
	inline Color_t2555686324 * get_address_of__color_poison_8() { return &____color_poison_8; }
	inline void set__color_poison_8(Color_t2555686324  value)
	{
		____color_poison_8 = value;
	}

	inline static int32_t get_offset_of_m_fProtectTime_9() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fProtectTime_9)); }
	inline float get_m_fProtectTime_9() const { return ___m_fProtectTime_9; }
	inline float* get_address_of_m_fProtectTime_9() { return &___m_fProtectTime_9; }
	inline void set_m_fProtectTime_9(float value)
	{
		___m_fProtectTime_9 = value;
	}

	inline static int32_t get_offset_of_m_lstBalls_10() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstBalls_10)); }
	inline List_1_t3678741308 * get_m_lstBalls_10() const { return ___m_lstBalls_10; }
	inline List_1_t3678741308 ** get_address_of_m_lstBalls_10() { return &___m_lstBalls_10; }
	inline void set_m_lstBalls_10(List_1_t3678741308 * value)
	{
		___m_lstBalls_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBalls_10), value);
	}

	inline static int32_t get_offset_of_m_dicActiveBalls_11() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicActiveBalls_11)); }
	inline Dictionary_2_t1095379897 * get_m_dicActiveBalls_11() const { return ___m_dicActiveBalls_11; }
	inline Dictionary_2_t1095379897 ** get_address_of_m_dicActiveBalls_11() { return &___m_dicActiveBalls_11; }
	inline void set_m_dicActiveBalls_11(Dictionary_2_t1095379897 * value)
	{
		___m_dicActiveBalls_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicActiveBalls_11), value);
	}

	inline static int32_t get_offset_of_m_nBallIndex_12() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nBallIndex_12)); }
	inline int32_t get_m_nBallIndex_12() const { return ___m_nBallIndex_12; }
	inline int32_t* get_address_of_m_nBallIndex_12() { return &___m_nBallIndex_12; }
	inline void set_m_nBallIndex_12(int32_t value)
	{
		___m_nBallIndex_12 = value;
	}

	inline static int32_t get_offset_of_m_vecPos_13() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_vecPos_13)); }
	inline Vector2_t2156229523  get_m_vecPos_13() const { return ___m_vecPos_13; }
	inline Vector2_t2156229523 * get_address_of_m_vecPos_13() { return &___m_vecPos_13; }
	inline void set_m_vecPos_13(Vector2_t2156229523  value)
	{
		___m_vecPos_13 = value;
	}

	inline static int32_t get_offset_of_m_SkillManager_19() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_SkillManager_19)); }
	inline CSkillManager_t2937013327 * get_m_SkillManager_19() const { return ___m_SkillManager_19; }
	inline CSkillManager_t2937013327 ** get_address_of_m_SkillManager_19() { return &___m_SkillManager_19; }
	inline void set_m_SkillManager_19(CSkillManager_t2937013327 * value)
	{
		___m_SkillManager_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkillManager_19), value);
	}

	inline static int32_t get_offset_of__PlayerIns_20() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____PlayerIns_20)); }
	inline CPlayerIns_t3208190728 * get__PlayerIns_20() const { return ____PlayerIns_20; }
	inline CPlayerIns_t3208190728 ** get_address_of__PlayerIns_20() { return &____PlayerIns_20; }
	inline void set__PlayerIns_20(CPlayerIns_t3208190728 * value)
	{
		____PlayerIns_20 = value;
		Il2CppCodeGenWriteBarrier((&____PlayerIns_20), value);
	}

	inline static int32_t get_offset_of_m_fTotalEatArea_21() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fTotalEatArea_21)); }
	inline float get_m_fTotalEatArea_21() const { return ___m_fTotalEatArea_21; }
	inline float* get_address_of_m_fTotalEatArea_21() { return &___m_fTotalEatArea_21; }
	inline void set_m_fTotalEatArea_21(float value)
	{
		___m_fTotalEatArea_21 = value;
	}

	inline static int32_t get_offset_of_m_fTotalEatFoodArea_22() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fTotalEatFoodArea_22)); }
	inline float get_m_fTotalEatFoodArea_22() const { return ___m_fTotalEatFoodArea_22; }
	inline float* get_address_of_m_fTotalEatFoodArea_22() { return &___m_fTotalEatFoodArea_22; }
	inline void set_m_fTotalEatFoodArea_22(float value)
	{
		___m_fTotalEatFoodArea_22 = value;
	}

	inline static int32_t get_offset_of_m_nKillCount_23() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nKillCount_23)); }
	inline int32_t get_m_nKillCount_23() const { return ___m_nKillCount_23; }
	inline int32_t* get_address_of_m_nKillCount_23() { return &___m_nKillCount_23; }
	inline void set_m_nKillCount_23(int32_t value)
	{
		___m_nKillCount_23 = value;
	}

	inline static int32_t get_offset_of_m_nBeKilled_24() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nBeKilled_24)); }
	inline int32_t get_m_nBeKilled_24() const { return ___m_nBeKilled_24; }
	inline int32_t* get_address_of_m_nBeKilled_24() { return &___m_nBeKilled_24; }
	inline void set_m_nBeKilled_24(int32_t value)
	{
		___m_nBeKilled_24 = value;
	}

	inline static int32_t get_offset_of_m_IncAssistAttackCount_25() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_IncAssistAttackCount_25)); }
	inline int32_t get_m_IncAssistAttackCount_25() const { return ___m_IncAssistAttackCount_25; }
	inline int32_t* get_address_of_m_IncAssistAttackCount_25() { return &___m_IncAssistAttackCount_25; }
	inline void set_m_IncAssistAttackCount_25(int32_t value)
	{
		___m_IncAssistAttackCount_25 = value;
	}

	inline static int32_t get_offset_of_m_nEatThornNum_26() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nEatThornNum_26)); }
	inline int32_t get_m_nEatThornNum_26() const { return ___m_nEatThornNum_26; }
	inline int32_t* get_address_of_m_nEatThornNum_26() { return &___m_nEatThornNum_26; }
	inline void set_m_nEatThornNum_26(int32_t value)
	{
		___m_nEatThornNum_26 = value;
	}

	inline static int32_t get_offset_of_m_nContinuousKillNum_27() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nContinuousKillNum_27)); }
	inline int32_t get_m_nContinuousKillNum_27() const { return ___m_nContinuousKillNum_27; }
	inline int32_t* get_address_of_m_nContinuousKillNum_27() { return &___m_nContinuousKillNum_27; }
	inline void set_m_nContinuousKillNum_27(int32_t value)
	{
		___m_nContinuousKillNum_27 = value;
	}

	inline static int32_t get_offset_of_m_dicDuoShaoRecord_28() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicDuoShaoRecord_28)); }
	inline Dictionary_2_t285980105 * get_m_dicDuoShaoRecord_28() const { return ___m_dicDuoShaoRecord_28; }
	inline Dictionary_2_t285980105 ** get_address_of_m_dicDuoShaoRecord_28() { return &___m_dicDuoShaoRecord_28; }
	inline void set_m_dicDuoShaoRecord_28(Dictionary_2_t285980105 * value)
	{
		___m_dicDuoShaoRecord_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicDuoShaoRecord_28), value);
	}

	inline static int32_t get_offset_of_m_nDuoSha_29() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nDuoSha_29)); }
	inline int32_t get_m_nDuoSha_29() const { return ___m_nDuoSha_29; }
	inline int32_t* get_address_of_m_nDuoSha_29() { return &___m_nDuoSha_29; }
	inline void set_m_nDuoSha_29(int32_t value)
	{
		___m_nDuoSha_29 = value;
	}

	inline static int32_t get_offset_of_m_aryItemNum_30() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_aryItemNum_30)); }
	inline Int32U5BU5D_t385246372* get_m_aryItemNum_30() const { return ___m_aryItemNum_30; }
	inline Int32U5BU5D_t385246372** get_address_of_m_aryItemNum_30() { return &___m_aryItemNum_30; }
	inline void set_m_aryItemNum_30(Int32U5BU5D_t385246372* value)
	{
		___m_aryItemNum_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemNum_30), value);
	}

	inline static int32_t get_offset_of_m_BaseData_31() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_BaseData_31)); }
	inline sPlayerBaseData_t1472229795  get_m_BaseData_31() const { return ___m_BaseData_31; }
	inline sPlayerBaseData_t1472229795 * get_address_of_m_BaseData_31() { return &___m_BaseData_31; }
	inline void set_m_BaseData_31(sPlayerBaseData_t1472229795  value)
	{
		___m_BaseData_31 = value;
	}

	inline static int32_t get_offset_of_m_fBaseVolumeByLevel_32() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fBaseVolumeByLevel_32)); }
	inline float get_m_fBaseVolumeByLevel_32() const { return ___m_fBaseVolumeByLevel_32; }
	inline float* get_address_of_m_fBaseVolumeByLevel_32() { return &___m_fBaseVolumeByLevel_32; }
	inline void set_m_fBaseVolumeByLevel_32(float value)
	{
		___m_fBaseVolumeByLevel_32 = value;
	}

	inline static int32_t get_offset_of_m_fProcessSelfCount_34() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fProcessSelfCount_34)); }
	inline float get_m_fProcessSelfCount_34() const { return ___m_fProcessSelfCount_34; }
	inline float* get_address_of_m_fProcessSelfCount_34() { return &___m_fProcessSelfCount_34; }
	inline void set_m_fProcessSelfCount_34(float value)
	{
		___m_fProcessSelfCount_34 = value;
	}

	inline static int32_t get_offset_of_m_fLoop_1_Second_TimeElapse_35() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fLoop_1_Second_TimeElapse_35)); }
	inline float get_m_fLoop_1_Second_TimeElapse_35() const { return ___m_fLoop_1_Second_TimeElapse_35; }
	inline float* get_address_of_m_fLoop_1_Second_TimeElapse_35() { return &___m_fLoop_1_Second_TimeElapse_35; }
	inline void set_m_fLoop_1_Second_TimeElapse_35(float value)
	{
		___m_fLoop_1_Second_TimeElapse_35 = value;
	}

	inline static int32_t get_offset_of_m_bQueryingFromMasterClientDataReceived_37() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bQueryingFromMasterClientDataReceived_37)); }
	inline bool get_m_bQueryingFromMasterClientDataReceived_37() const { return ___m_bQueryingFromMasterClientDataReceived_37; }
	inline bool* get_address_of_m_bQueryingFromMasterClientDataReceived_37() { return &___m_bQueryingFromMasterClientDataReceived_37; }
	inline void set_m_bQueryingFromMasterClientDataReceived_37(bool value)
	{
		___m_bQueryingFromMasterClientDataReceived_37 = value;
	}

	inline static int32_t get_offset_of_m_fQueryingFromMasterClientTimeElapse_38() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fQueryingFromMasterClientTimeElapse_38)); }
	inline float get_m_fQueryingFromMasterClientTimeElapse_38() const { return ___m_fQueryingFromMasterClientTimeElapse_38; }
	inline float* get_address_of_m_fQueryingFromMasterClientTimeElapse_38() { return &___m_fQueryingFromMasterClientTimeElapse_38; }
	inline void set_m_fQueryingFromMasterClientTimeElapse_38(float value)
	{
		___m_fQueryingFromMasterClientTimeElapse_38 = value;
	}

	inline static int32_t get_offset_of_m_bQueryingFromMainPlayerDataReceived_39() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bQueryingFromMainPlayerDataReceived_39)); }
	inline bool get_m_bQueryingFromMainPlayerDataReceived_39() const { return ___m_bQueryingFromMainPlayerDataReceived_39; }
	inline bool* get_address_of_m_bQueryingFromMainPlayerDataReceived_39() { return &___m_bQueryingFromMainPlayerDataReceived_39; }
	inline void set_m_bQueryingFromMainPlayerDataReceived_39(bool value)
	{
		___m_bQueryingFromMainPlayerDataReceived_39 = value;
	}

	inline static int32_t get_offset_of_m_fQueryingFromMainPlayerTimeElapse_40() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fQueryingFromMainPlayerTimeElapse_40)); }
	inline float get_m_fQueryingFromMainPlayerTimeElapse_40() const { return ___m_fQueryingFromMainPlayerTimeElapse_40; }
	inline float* get_address_of_m_fQueryingFromMainPlayerTimeElapse_40() { return &___m_fQueryingFromMainPlayerTimeElapse_40; }
	inline void set_m_fQueryingFromMainPlayerTimeElapse_40(float value)
	{
		___m_fQueryingFromMainPlayerTimeElapse_40 = value;
	}

	inline static int32_t get_offset_of_m_bOrphanBallsDataReceived_41() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bOrphanBallsDataReceived_41)); }
	inline bool get_m_bOrphanBallsDataReceived_41() const { return ___m_bOrphanBallsDataReceived_41; }
	inline bool* get_address_of_m_bOrphanBallsDataReceived_41() { return &___m_bOrphanBallsDataReceived_41; }
	inline void set_m_bOrphanBallsDataReceived_41(bool value)
	{
		___m_bOrphanBallsDataReceived_41 = value;
	}

	inline static int32_t get_offset_of_m_bIniting_42() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bIniting_42)); }
	inline bool get_m_bIniting_42() const { return ___m_bIniting_42; }
	inline bool* get_address_of_m_bIniting_42() { return &___m_bIniting_42; }
	inline void set_m_bIniting_42(bool value)
	{
		___m_bIniting_42 = value;
	}

	inline static int32_t get_offset_of_m_bInitCompleted_43() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bInitCompleted_43)); }
	inline bool get_m_bInitCompleted_43() const { return ___m_bInitCompleted_43; }
	inline bool* get_address_of_m_bInitCompleted_43() { return &___m_bInitCompleted_43; }
	inline void set_m_bInitCompleted_43(bool value)
	{
		___m_bInitCompleted_43 = value;
	}

	inline static int32_t get_offset_of_m_bBallInstantiated_44() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bBallInstantiated_44)); }
	inline bool get_m_bBallInstantiated_44() const { return ___m_bBallInstantiated_44; }
	inline bool* get_address_of_m_bBallInstantiated_44() { return &___m_bBallInstantiated_44; }
	inline void set_m_bBallInstantiated_44(bool value)
	{
		___m_bBallInstantiated_44 = value;
	}

	inline static int32_t get_offset_of_m_fInitingBallsTimeCount_46() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fInitingBallsTimeCount_46)); }
	inline float get_m_fInitingBallsTimeCount_46() const { return ___m_fInitingBallsTimeCount_46; }
	inline float* get_address_of_m_fInitingBallsTimeCount_46() { return &___m_fInitingBallsTimeCount_46; }
	inline void set_m_fInitingBallsTimeCount_46(float value)
	{
		___m_fInitingBallsTimeCount_46 = value;
	}

	inline static int32_t get_offset_of_m_nKingExplodeIndex_47() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nKingExplodeIndex_47)); }
	inline int32_t get_m_nKingExplodeIndex_47() const { return ___m_nKingExplodeIndex_47; }
	inline int32_t* get_address_of_m_nKingExplodeIndex_47() { return &___m_nKingExplodeIndex_47; }
	inline void set_m_nKingExplodeIndex_47(int32_t value)
	{
		___m_nKingExplodeIndex_47 = value;
	}

	inline static int32_t get_offset_of_m_bKingExploding_48() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bKingExploding_48)); }
	inline bool get_m_bKingExploding_48() const { return ___m_bKingExploding_48; }
	inline bool* get_address_of_m_bKingExploding_48() { return &___m_bKingExploding_48; }
	inline void set_m_bKingExploding_48(bool value)
	{
		___m_bKingExploding_48 = value;
	}

	inline static int32_t get_offset_of_m_szPlayerName_49() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_szPlayerName_49)); }
	inline String_t* get_m_szPlayerName_49() const { return ___m_szPlayerName_49; }
	inline String_t** get_address_of_m_szPlayerName_49() { return &___m_szPlayerName_49; }
	inline void set_m_szPlayerName_49(String_t* value)
	{
		___m_szPlayerName_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_szPlayerName_49), value);
	}

	inline static int32_t get_offset_of_m_szAccount_50() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_szAccount_50)); }
	inline String_t* get_m_szAccount_50() const { return ___m_szAccount_50; }
	inline String_t** get_address_of_m_szAccount_50() { return &___m_szAccount_50; }
	inline void set_m_szAccount_50(String_t* value)
	{
		___m_szAccount_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_szAccount_50), value);
	}

	inline static int32_t get_offset_of_m_bPublicInfoInited_52() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bPublicInfoInited_52)); }
	inline bool get_m_bPublicInfoInited_52() const { return ___m_bPublicInfoInited_52; }
	inline bool* get_address_of_m_bPublicInfoInited_52() { return &___m_bPublicInfoInited_52; }
	inline void set_m_bPublicInfoInited_52(bool value)
	{
		___m_bPublicInfoInited_52 = value;
	}

	inline static int32_t get_offset_of_m_nPlayerPlatform_53() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nPlayerPlatform_53)); }
	inline int32_t get_m_nPlayerPlatform_53() const { return ___m_nPlayerPlatform_53; }
	inline int32_t* get_address_of_m_nPlayerPlatform_53() { return &___m_nPlayerPlatform_53; }
	inline void set_m_nPlayerPlatform_53(int32_t value)
	{
		___m_nPlayerPlatform_53 = value;
	}

	inline static int32_t get_offset_of_m_bObserve_55() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bObserve_55)); }
	inline bool get_m_bObserve_55() const { return ___m_bObserve_55; }
	inline bool* get_address_of_m_bObserve_55() { return &___m_bObserve_55; }
	inline void set_m_bObserve_55(bool value)
	{
		___m_bObserve_55 = value;
	}

	inline static int32_t get_offset_of_m_vWorldCursorPos_56() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_vWorldCursorPos_56)); }
	inline Vector3_t3722313464  get_m_vWorldCursorPos_56() const { return ___m_vWorldCursorPos_56; }
	inline Vector3_t3722313464 * get_address_of_m_vWorldCursorPos_56() { return &___m_vWorldCursorPos_56; }
	inline void set_m_vWorldCursorPos_56(Vector3_t3722313464  value)
	{
		___m_vWorldCursorPos_56 = value;
	}

	inline static int32_t get_offset_of_m_vSpecialDirecton_57() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_vSpecialDirecton_57)); }
	inline Vector2_t2156229523  get_m_vSpecialDirecton_57() const { return ___m_vSpecialDirecton_57; }
	inline Vector2_t2156229523 * get_address_of_m_vSpecialDirecton_57() { return &___m_vSpecialDirecton_57; }
	inline void set_m_vSpecialDirecton_57(Vector2_t2156229523  value)
	{
		___m_vSpecialDirecton_57 = value;
	}

	inline static int32_t get_offset_of_m_fRealTimeSpeedWithoutSize_58() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fRealTimeSpeedWithoutSize_58)); }
	inline float get_m_fRealTimeSpeedWithoutSize_58() const { return ___m_fRealTimeSpeedWithoutSize_58; }
	inline float* get_address_of_m_fRealTimeSpeedWithoutSize_58() { return &___m_fRealTimeSpeedWithoutSize_58; }
	inline void set_m_fRealTimeSpeedWithoutSize_58(float value)
	{
		___m_fRealTimeSpeedWithoutSize_58 = value;
	}

	inline static int32_t get_offset_of_m_fSyncMoveInfoCount_59() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSyncMoveInfoCount_59)); }
	inline float get_m_fSyncMoveInfoCount_59() const { return ___m_fSyncMoveInfoCount_59; }
	inline float* get_address_of_m_fSyncMoveInfoCount_59() { return &___m_fSyncMoveInfoCount_59; }
	inline void set_m_fSyncMoveInfoCount_59(float value)
	{
		___m_fSyncMoveInfoCount_59 = value;
	}

	inline static int32_t get_offset_of_m_fBaseSpeed_60() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fBaseSpeed_60)); }
	inline float get_m_fBaseSpeed_60() const { return ___m_fBaseSpeed_60; }
	inline float* get_address_of_m_fBaseSpeed_60() { return &___m_fBaseSpeed_60; }
	inline void set_m_fBaseSpeed_60(float value)
	{
		___m_fBaseSpeed_60 = value;
	}

	inline static int32_t get_offset_of_m_fShellTimeBuffEffect_61() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fShellTimeBuffEffect_61)); }
	inline float get_m_fShellTimeBuffEffect_61() const { return ___m_fShellTimeBuffEffect_61; }
	inline float* get_address_of_m_fShellTimeBuffEffect_61() { return &___m_fShellTimeBuffEffect_61; }
	inline void set_m_fShellTimeBuffEffect_61(float value)
	{
		___m_fShellTimeBuffEffect_61 = value;
	}

	inline static int32_t get_offset_of_m_bTestMove_64() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bTestMove_64)); }
	inline bool get_m_bTestMove_64() const { return ___m_bTestMove_64; }
	inline bool* get_address_of_m_bTestMove_64() { return &___m_bTestMove_64; }
	inline void set_m_bTestMove_64(bool value)
	{
		___m_bTestMove_64 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_65() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bMoving_65)); }
	inline bool get_m_bMoving_65() const { return ___m_bMoving_65; }
	inline bool* get_address_of_m_bMoving_65() { return &___m_bMoving_65; }
	inline void set_m_bMoving_65(bool value)
	{
		___m_bMoving_65 = value;
	}

	inline static int32_t get_offset_of_m_fClearSpitTargetTotalTime_66() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fClearSpitTargetTotalTime_66)); }
	inline float get_m_fClearSpitTargetTotalTime_66() const { return ___m_fClearSpitTargetTotalTime_66; }
	inline float* get_address_of_m_fClearSpitTargetTotalTime_66() { return &___m_fClearSpitTargetTotalTime_66; }
	inline void set_m_fClearSpitTargetTotalTime_66(float value)
	{
		___m_fClearSpitTargetTotalTime_66 = value;
	}

	inline static int32_t get_offset_of_m_nSpitIndex_67() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSpitIndex_67)); }
	inline int32_t get_m_nSpitIndex_67() const { return ___m_nSpitIndex_67; }
	inline int32_t* get_address_of_m_nSpitIndex_67() { return &___m_nSpitIndex_67; }
	inline void set_m_nSpitIndex_67(int32_t value)
	{
		___m_nSpitIndex_67 = value;
	}

	inline static int32_t get_offset_of_m_bSpitting_68() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bSpitting_68)); }
	inline bool get_m_bSpitting_68() const { return ___m_bSpitting_68; }
	inline bool* get_address_of_m_bSpitting_68() { return &___m_bSpitting_68; }
	inline void set_m_bSpitting_68(bool value)
	{
		___m_bSpitting_68 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfMpCost_69() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallHalfMpCost_69)); }
	inline float get_m_fSpitBallHalfMpCost_69() const { return ___m_fSpitBallHalfMpCost_69; }
	inline float* get_address_of_m_fSpitBallHalfMpCost_69() { return &___m_fSpitBallHalfMpCost_69; }
	inline void set_m_fSpitBallHalfMpCost_69(float value)
	{
		___m_fSpitBallHalfMpCost_69 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfColddown_70() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallHalfColddown_70)); }
	inline float get_m_fSpitBallHalfColddown_70() const { return ___m_fSpitBallHalfColddown_70; }
	inline float* get_address_of_m_fSpitBallHalfColddown_70() { return &___m_fSpitBallHalfColddown_70; }
	inline void set_m_fSpitBallHalfColddown_70(float value)
	{
		___m_fSpitBallHalfColddown_70 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfQianYao_71() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallHalfQianYao_71)); }
	inline float get_m_fSpitBallHalfQianYao_71() const { return ___m_fSpitBallHalfQianYao_71; }
	inline float* get_address_of_m_fSpitBallHalfQianYao_71() { return &___m_fSpitBallHalfQianYao_71; }
	inline void set_m_fSpitBallHalfQianYao_71(float value)
	{
		___m_fSpitBallHalfQianYao_71 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfDistance_72() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallHalfDistance_72)); }
	inline float get_m_fSpitBallHalfDistance_72() const { return ___m_fSpitBallHalfDistance_72; }
	inline float* get_address_of_m_fSpitBallHalfDistance_72() { return &___m_fSpitBallHalfDistance_72; }
	inline void set_m_fSpitBallHalfDistance_72(float value)
	{
		___m_fSpitBallHalfDistance_72 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfRunTime_73() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallHalfRunTime_73)); }
	inline float get_m_fSpitBallHalfRunTime_73() const { return ___m_fSpitBallHalfRunTime_73; }
	inline float* get_address_of_m_fSpitBallHalfRunTime_73() { return &___m_fSpitBallHalfRunTime_73; }
	inline void set_m_fSpitBallHalfRunTime_73(float value)
	{
		___m_fSpitBallHalfRunTime_73 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallHalfShellTime_74() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallHalfShellTime_74)); }
	inline float get_m_fSpitBallHalfShellTime_74() const { return ___m_fSpitBallHalfShellTime_74; }
	inline float* get_address_of_m_fSpitBallHalfShellTime_74() { return &___m_fSpitBallHalfShellTime_74; }
	inline void set_m_fSpitBallHalfShellTime_74(float value)
	{
		___m_fSpitBallHalfShellTime_74 = value;
	}

	inline static int32_t get_offset_of_m_nPreCastSkill_Status_SpitHalf_75() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nPreCastSkill_Status_SpitHalf_75)); }
	inline int32_t get_m_nPreCastSkill_Status_SpitHalf_75() const { return ___m_nPreCastSkill_Status_SpitHalf_75; }
	inline int32_t* get_address_of_m_nPreCastSkill_Status_SpitHalf_75() { return &___m_nPreCastSkill_Status_SpitHalf_75; }
	inline void set_m_nPreCastSkill_Status_SpitHalf_75(int32_t value)
	{
		___m_nPreCastSkill_Status_SpitHalf_75 = value;
	}

	inline static int32_t get_offset_of_m_bPreCastSkill_SpitHalf_76() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bPreCastSkill_SpitHalf_76)); }
	inline bool get_m_bPreCastSkill_SpitHalf_76() const { return ___m_bPreCastSkill_SpitHalf_76; }
	inline bool* get_address_of_m_bPreCastSkill_SpitHalf_76() { return &___m_bPreCastSkill_SpitHalf_76; }
	inline void set_m_bPreCastSkill_SpitHalf_76(bool value)
	{
		___m_bPreCastSkill_SpitHalf_76 = value;
	}

	inline static int32_t get_offset_of_m_fPreCastSkill_SpitHalf_TimeCount_77() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPreCastSkill_SpitHalf_TimeCount_77)); }
	inline float get_m_fPreCastSkill_SpitHalf_TimeCount_77() const { return ___m_fPreCastSkill_SpitHalf_TimeCount_77; }
	inline float* get_address_of_m_fPreCastSkill_SpitHalf_TimeCount_77() { return &___m_fPreCastSkill_SpitHalf_TimeCount_77; }
	inline void set_m_fPreCastSkill_SpitHalf_TimeCount_77(float value)
	{
		___m_fPreCastSkill_SpitHalf_TimeCount_77 = value;
	}

	inline static int32_t get_offset_of_m_nWSpitStatus_78() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nWSpitStatus_78)); }
	inline int32_t get_m_nWSpitStatus_78() const { return ___m_nWSpitStatus_78; }
	inline int32_t* get_address_of_m_nWSpitStatus_78() { return &___m_nWSpitStatus_78; }
	inline void set_m_nWSpitStatus_78(int32_t value)
	{
		___m_nWSpitStatus_78 = value;
	}

	inline static int32_t get_offset_of_m_lstTempEjectEndPos_80() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstTempEjectEndPos_80)); }
	inline List_1_t899420910 * get_m_lstTempEjectEndPos_80() const { return ___m_lstTempEjectEndPos_80; }
	inline List_1_t899420910 ** get_address_of_m_lstTempEjectEndPos_80() { return &___m_lstTempEjectEndPos_80; }
	inline void set_m_lstTempEjectEndPos_80(List_1_t899420910 * value)
	{
		___m_lstTempEjectEndPos_80 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTempEjectEndPos_80), value);
	}

	inline static int32_t get_offset_of_m_fSpitBallCostMp_81() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallCostMp_81)); }
	inline float get_m_fSpitBallCostMp_81() const { return ___m_fSpitBallCostMp_81; }
	inline float* get_address_of_m_fSpitBallCostMp_81() { return &___m_fSpitBallCostMp_81; }
	inline void set_m_fSpitBallCostMp_81(float value)
	{
		___m_fSpitBallCostMp_81 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallDistance_82() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallDistance_82)); }
	inline float get_m_fSpitBallDistance_82() const { return ___m_fSpitBallDistance_82; }
	inline float* get_address_of_m_fSpitBallDistance_82() { return &___m_fSpitBallDistance_82; }
	inline void set_m_fSpitBallDistance_82(float value)
	{
		___m_fSpitBallDistance_82 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallRunTime_83() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallRunTime_83)); }
	inline float get_m_fSpitBallRunTime_83() const { return ___m_fSpitBallRunTime_83; }
	inline float* get_address_of_m_fSpitBallRunTime_83() { return &___m_fSpitBallRunTime_83; }
	inline void set_m_fSpitBallRunTime_83(float value)
	{
		___m_fSpitBallRunTime_83 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallShellTime_84() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitBallShellTime_84)); }
	inline float get_m_fSpitBallShellTime_84() const { return ___m_fSpitBallShellTime_84; }
	inline float* get_address_of_m_fSpitBallShellTime_84() { return &___m_fSpitBallShellTime_84; }
	inline void set_m_fSpitBallShellTime_84(float value)
	{
		___m_fSpitBallShellTime_84 = value;
	}

	inline static int32_t get_offset_of_m_fForceSpitTotalTime_85() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fForceSpitTotalTime_85)); }
	inline float get_m_fForceSpitTotalTime_85() const { return ___m_fForceSpitTotalTime_85; }
	inline float* get_address_of_m_fForceSpitTotalTime_85() { return &___m_fForceSpitTotalTime_85; }
	inline void set_m_fForceSpitTotalTime_85(float value)
	{
		___m_fForceSpitTotalTime_85 = value;
	}

	inline static int32_t get_offset_of_m_fForcing_86() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fForcing_86)); }
	inline bool get_m_fForcing_86() const { return ___m_fForcing_86; }
	inline bool* get_address_of_m_fForcing_86() { return &___m_fForcing_86; }
	inline void set_m_fForcing_86(bool value)
	{
		___m_fForcing_86 = value;
	}

	inline static int32_t get_offset_of_m_fForceSpitCurTime_87() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fForceSpitCurTime_87)); }
	inline float get_m_fForceSpitCurTime_87() const { return ___m_fForceSpitCurTime_87; }
	inline float* get_address_of_m_fForceSpitCurTime_87() { return &___m_fForceSpitCurTime_87; }
	inline void set_m_fForceSpitCurTime_87(float value)
	{
		___m_fForceSpitCurTime_87 = value;
	}

	inline static int32_t get_offset_of_m_fSpitPercent_88() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitPercent_88)); }
	inline float get_m_fSpitPercent_88() const { return ___m_fSpitPercent_88; }
	inline float* get_address_of_m_fSpitPercent_88() { return &___m_fSpitPercent_88; }
	inline void set_m_fSpitPercent_88(float value)
	{
		___m_fSpitPercent_88 = value;
	}

	inline static int32_t get_offset_of_m_aryGunsight_89() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_aryGunsight_89)); }
	inline CGunsightU5BU5D_t395838697* get_m_aryGunsight_89() const { return ___m_aryGunsight_89; }
	inline CGunsightU5BU5D_t395838697** get_address_of_m_aryGunsight_89() { return &___m_aryGunsight_89; }
	inline void set_m_aryGunsight_89(CGunsightU5BU5D_t395838697* value)
	{
		___m_aryGunsight_89 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryGunsight_89), value);
	}

	inline static int32_t get_offset_of_m_fPreDrawCount_90() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPreDrawCount_90)); }
	inline float get_m_fPreDrawCount_90() const { return ___m_fPreDrawCount_90; }
	inline float* get_address_of_m_fPreDrawCount_90() { return &___m_fPreDrawCount_90; }
	inline void set_m_fPreDrawCount_90(float value)
	{
		___m_fPreDrawCount_90 = value;
	}

	inline static int32_t get_offset_of_m_vecBalslsCenter_91() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_vecBalslsCenter_91)); }
	inline Vector3_t3722313464  get_m_vecBalslsCenter_91() const { return ___m_vecBalslsCenter_91; }
	inline Vector3_t3722313464 * get_address_of_m_vecBalslsCenter_91() { return &___m_vecBalslsCenter_91; }
	inline void set_m_vecBalslsCenter_91(Vector3_t3722313464  value)
	{
		___m_vecBalslsCenter_91 = value;
	}

	inline static int32_t get_offset_of_m_fDistanceToSlowDown_92() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fDistanceToSlowDown_92)); }
	inline float get_m_fDistanceToSlowDown_92() const { return ___m_fDistanceToSlowDown_92; }
	inline float* get_address_of_m_fDistanceToSlowDown_92() { return &___m_fDistanceToSlowDown_92; }
	inline void set_m_fDistanceToSlowDown_92(float value)
	{
		___m_fDistanceToSlowDown_92 = value;
	}

	inline static int32_t get_offset_of_m_lstToMergeAllBalls_93() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstToMergeAllBalls_93)); }
	inline List_1_t128053199 * get_m_lstToMergeAllBalls_93() const { return ___m_lstToMergeAllBalls_93; }
	inline List_1_t128053199 ** get_address_of_m_lstToMergeAllBalls_93() { return &___m_lstToMergeAllBalls_93; }
	inline void set_m_lstToMergeAllBalls_93(List_1_t128053199 * value)
	{
		___m_lstToMergeAllBalls_93 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstToMergeAllBalls_93), value);
	}

	inline static int32_t get_offset_of_m_lstMiaoHeEffect_95() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstMiaoHeEffect_95)); }
	inline List_1_t2306155490 * get_m_lstMiaoHeEffect_95() const { return ___m_lstMiaoHeEffect_95; }
	inline List_1_t2306155490 ** get_address_of_m_lstMiaoHeEffect_95() { return &___m_lstMiaoHeEffect_95; }
	inline void set_m_lstMiaoHeEffect_95(List_1_t2306155490 * value)
	{
		___m_lstMiaoHeEffect_95 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMiaoHeEffect_95), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledSpiltInfo_96() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstRecycledSpiltInfo_96)); }
	inline List_1_t478107080 * get_m_lstRecycledSpiltInfo_96() const { return ___m_lstRecycledSpiltInfo_96; }
	inline List_1_t478107080 ** get_address_of_m_lstRecycledSpiltInfo_96() { return &___m_lstRecycledSpiltInfo_96; }
	inline void set_m_lstRecycledSpiltInfo_96(List_1_t478107080 * value)
	{
		___m_lstRecycledSpiltInfo_96 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledSpiltInfo_96), value);
	}

	inline static int32_t get_offset_of_m_bSplitting_97() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bSplitting_97)); }
	inline bool get_m_bSplitting_97() const { return ___m_bSplitting_97; }
	inline bool* get_address_of_m_bSplitting_97() { return &___m_bSplitting_97; }
	inline void set_m_bSplitting_97(bool value)
	{
		___m_bSplitting_97 = value;
	}

	inline static int32_t get_offset_of_m_nCurSplitTimes_98() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nCurSplitTimes_98)); }
	inline int32_t get_m_nCurSplitTimes_98() const { return ___m_nCurSplitTimes_98; }
	inline int32_t* get_address_of_m_nCurSplitTimes_98() { return &___m_nCurSplitTimes_98; }
	inline void set_m_nCurSplitTimes_98(int32_t value)
	{
		___m_nCurSplitTimes_98 = value;
	}

	inline static int32_t get_offset_of_m_nRealSplitNum_99() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nRealSplitNum_99)); }
	inline int32_t get_m_nRealSplitNum_99() const { return ___m_nRealSplitNum_99; }
	inline int32_t* get_address_of_m_nRealSplitNum_99() { return &___m_nRealSplitNum_99; }
	inline void set_m_nRealSplitNum_99(int32_t value)
	{
		___m_nRealSplitNum_99 = value;
	}

	inline static int32_t get_offset_of_m_lstSplitBalls_100() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstSplitBalls_100)); }
	inline List_1_t478107080 * get_m_lstSplitBalls_100() const { return ___m_lstSplitBalls_100; }
	inline List_1_t478107080 ** get_address_of_m_lstSplitBalls_100() { return &___m_lstSplitBalls_100; }
	inline void set_m_lstSplitBalls_100(List_1_t478107080 * value)
	{
		___m_lstSplitBalls_100 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSplitBalls_100), value);
	}

	inline static int32_t get_offset_of_m_lstToExplodeBalls_102() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstToExplodeBalls_102)); }
	inline List_1_t128053199 * get_m_lstToExplodeBalls_102() const { return ___m_lstToExplodeBalls_102; }
	inline List_1_t128053199 ** get_address_of_m_lstToExplodeBalls_102() { return &___m_lstToExplodeBalls_102; }
	inline void set_m_lstToExplodeBalls_102(List_1_t128053199 * value)
	{
		___m_lstToExplodeBalls_102 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstToExplodeBalls_102), value);
	}

	inline static int32_t get_offset_of_m_fUpdateBuffCount_103() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fUpdateBuffCount_103)); }
	inline float get_m_fUpdateBuffCount_103() const { return ___m_fUpdateBuffCount_103; }
	inline float* get_address_of_m_fUpdateBuffCount_103() { return &___m_fUpdateBuffCount_103; }
	inline void set_m_fUpdateBuffCount_103(float value)
	{
		___m_fUpdateBuffCount_103 = value;
	}

	inline static int32_t get_offset_of_m_szSpitSporeMonsterId_105() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_szSpitSporeMonsterId_105)); }
	inline String_t* get_m_szSpitSporeMonsterId_105() const { return ___m_szSpitSporeMonsterId_105; }
	inline String_t** get_address_of_m_szSpitSporeMonsterId_105() { return &___m_szSpitSporeMonsterId_105; }
	inline void set_m_szSpitSporeMonsterId_105(String_t* value)
	{
		___m_szSpitSporeMonsterId_105 = value;
		Il2CppCodeGenWriteBarrier((&___m_szSpitSporeMonsterId_105), value);
	}

	inline static int32_t get_offset_of_m_fSpitSporeDis_106() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeDis_106)); }
	inline float get_m_fSpitSporeDis_106() const { return ___m_fSpitSporeDis_106; }
	inline float* get_address_of_m_fSpitSporeDis_106() { return &___m_fSpitSporeDis_106; }
	inline void set_m_fSpitSporeDis_106(float value)
	{
		___m_fSpitSporeDis_106 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeTimeInterval_107() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeTimeInterval_107)); }
	inline float get_m_fSpitSporeTimeInterval_107() const { return ___m_fSpitSporeTimeInterval_107; }
	inline float* get_address_of_m_fSpitSporeTimeInterval_107() { return &___m_fSpitSporeTimeInterval_107; }
	inline void set_m_fSpitSporeTimeInterval_107(float value)
	{
		___m_fSpitSporeTimeInterval_107 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeMpCost_108() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeMpCost_108)); }
	inline float get_m_fSpitSporeMpCost_108() const { return ___m_fSpitSporeMpCost_108; }
	inline float* get_address_of_m_fSpitSporeMpCost_108() { return &___m_fSpitSporeMpCost_108; }
	inline void set_m_fSpitSporeMpCost_108(float value)
	{
		___m_fSpitSporeMpCost_108 = value;
	}

	inline static int32_t get_offset_of_m_bSpittingSpore_109() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bSpittingSpore_109)); }
	inline bool get_m_bSpittingSpore_109() const { return ___m_bSpittingSpore_109; }
	inline bool* get_address_of_m_bSpittingSpore_109() { return &___m_bSpittingSpore_109; }
	inline void set_m_bSpittingSpore_109(bool value)
	{
		___m_bSpittingSpore_109 = value;
	}

	inline static int32_t get_offset_of_m_fSporeEjectSpeed_110() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSporeEjectSpeed_110)); }
	inline float get_m_fSporeEjectSpeed_110() const { return ___m_fSporeEjectSpeed_110; }
	inline float* get_address_of_m_fSporeEjectSpeed_110() { return &___m_fSporeEjectSpeed_110; }
	inline void set_m_fSporeEjectSpeed_110(float value)
	{
		___m_fSporeEjectSpeed_110 = value;
	}

	inline static int32_t get_offset_of_m_nCurSporeLevel_111() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nCurSporeLevel_111)); }
	inline int32_t get_m_nCurSporeLevel_111() const { return ___m_nCurSporeLevel_111; }
	inline int32_t* get_address_of_m_nCurSporeLevel_111() { return &___m_nCurSporeLevel_111; }
	inline void set_m_nCurSporeLevel_111(int32_t value)
	{
		___m_nCurSporeLevel_111 = value;
	}

	inline static int32_t get_offset_of_m_lstSporeToSpit_112() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstSporeToSpit_112)); }
	inline List_1_t4032136720 * get_m_lstSporeToSpit_112() const { return ___m_lstSporeToSpit_112; }
	inline List_1_t4032136720 ** get_address_of_m_lstSporeToSpit_112() { return &___m_lstSporeToSpit_112; }
	inline void set_m_lstSporeToSpit_112(List_1_t4032136720 * value)
	{
		___m_lstSporeToSpit_112 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSporeToSpit_112), value);
	}

	inline static int32_t get_offset_of_lstSporeToSpitTemp_113() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___lstSporeToSpitTemp_113)); }
	inline List_1_t4032136720 * get_lstSporeToSpitTemp_113() const { return ___lstSporeToSpitTemp_113; }
	inline List_1_t4032136720 ** get_address_of_lstSporeToSpitTemp_113() { return &___lstSporeToSpitTemp_113; }
	inline void set_lstSporeToSpitTemp_113(List_1_t4032136720 * value)
	{
		___lstSporeToSpitTemp_113 = value;
		Il2CppCodeGenWriteBarrier((&___lstSporeToSpitTemp_113), value);
	}

	inline static int32_t get_offset_of_m_fSpitSporeInterval_114() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeInterval_114)); }
	inline float get_m_fSpitSporeInterval_114() const { return ___m_fSpitSporeInterval_114; }
	inline float* get_address_of_m_fSpitSporeInterval_114() { return &___m_fSpitSporeInterval_114; }
	inline void set_m_fSpitSporeInterval_114(float value)
	{
		___m_fSpitSporeInterval_114 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeDistance_115() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeDistance_115)); }
	inline float get_m_fSpitSporeDistance_115() const { return ___m_fSpitSporeDistance_115; }
	inline float* get_address_of_m_fSpitSporeDistance_115() { return &___m_fSpitSporeDistance_115; }
	inline void set_m_fSpitSporeDistance_115(float value)
	{
		___m_fSpitSporeDistance_115 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeTimeCount_116() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeTimeCount_116)); }
	inline float get_m_fSpitSporeTimeCount_116() const { return ___m_fSpitSporeTimeCount_116; }
	inline float* get_address_of_m_fSpitSporeTimeCount_116() { return &___m_fSpitSporeTimeCount_116; }
	inline void set_m_fSpitSporeTimeCount_116(float value)
	{
		___m_fSpitSporeTimeCount_116 = value;
	}

	inline static int32_t get_offset_of_m_fGenerateSporeIdTimeCount_117() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fGenerateSporeIdTimeCount_117)); }
	inline float get_m_fGenerateSporeIdTimeCount_117() const { return ___m_fGenerateSporeIdTimeCount_117; }
	inline float* get_address_of_m_fGenerateSporeIdTimeCount_117() { return &___m_fGenerateSporeIdTimeCount_117; }
	inline void set_m_fGenerateSporeIdTimeCount_117(float value)
	{
		___m_fGenerateSporeIdTimeCount_117 = value;
	}

	inline static int32_t get_offset_of_m_nSporeNumPerSec_118() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSporeNumPerSec_118)); }
	inline int32_t get_m_nSporeNumPerSec_118() const { return ___m_nSporeNumPerSec_118; }
	inline int32_t* get_address_of_m_nSporeNumPerSec_118() { return &___m_nSporeNumPerSec_118; }
	inline void set_m_nSporeNumPerSec_118(int32_t value)
	{
		___m_nSporeNumPerSec_118 = value;
	}

	inline static int32_t get_offset_of_m_uSporeGuid_119() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_uSporeGuid_119)); }
	inline uint32_t get_m_uSporeGuid_119() const { return ___m_uSporeGuid_119; }
	inline uint32_t* get_address_of_m_uSporeGuid_119() { return &___m_uSporeGuid_119; }
	inline void set_m_uSporeGuid_119(uint32_t value)
	{
		___m_uSporeGuid_119 = value;
	}

	inline static int32_t get_offset_of_m_lstSporeToSync_121() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstSporeToSync_121)); }
	inline List_1_t1482975291 * get_m_lstSporeToSync_121() const { return ___m_lstSporeToSync_121; }
	inline List_1_t1482975291 ** get_address_of_m_lstSporeToSync_121() { return &___m_lstSporeToSync_121; }
	inline void set_m_lstSporeToSync_121(List_1_t1482975291 * value)
	{
		___m_lstSporeToSync_121 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSporeToSync_121), value);
	}

	inline static int32_t get_offset_of_m_uSporeIdCount_122() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_uSporeIdCount_122)); }
	inline uint32_t get_m_uSporeIdCount_122() const { return ___m_uSporeIdCount_122; }
	inline uint32_t* get_address_of_m_uSporeIdCount_122() { return &___m_uSporeIdCount_122; }
	inline void set_m_uSporeIdCount_122(uint32_t value)
	{
		___m_uSporeIdCount_122 = value;
	}

	inline static int32_t get_offset_of_m_fSpitSporeBlingCount_123() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpitSporeBlingCount_123)); }
	inline float get_m_fSpitSporeBlingCount_123() const { return ___m_fSpitSporeBlingCount_123; }
	inline float* get_address_of_m_fSpitSporeBlingCount_123() { return &___m_fSpitSporeBlingCount_123; }
	inline void set_m_fSpitSporeBlingCount_123(float value)
	{
		___m_fSpitSporeBlingCount_123 = value;
	}

	inline static int32_t get_offset_of__bytesSpore_124() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____bytesSpore_124)); }
	inline ByteU5BU5D_t4116647657* get__bytesSpore_124() const { return ____bytesSpore_124; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesSpore_124() { return &____bytesSpore_124; }
	inline void set__bytesSpore_124(ByteU5BU5D_t4116647657* value)
	{
		____bytesSpore_124 = value;
		Il2CppCodeGenWriteBarrier((&____bytesSpore_124), value);
	}

	inline static int32_t get_offset_of_m_nAttenuateTime_125() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nAttenuateTime_125)); }
	inline float get_m_nAttenuateTime_125() const { return ___m_nAttenuateTime_125; }
	inline float* get_address_of_m_nAttenuateTime_125() { return &___m_nAttenuateTime_125; }
	inline void set_m_nAttenuateTime_125(float value)
	{
		___m_nAttenuateTime_125 = value;
	}

	inline static int32_t get_offset_of_m_lstLiveBall_126() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstLiveBall_126)); }
	inline List_1_t128053199 * get_m_lstLiveBall_126() const { return ___m_lstLiveBall_126; }
	inline List_1_t128053199 ** get_address_of_m_lstLiveBall_126() { return &___m_lstLiveBall_126; }
	inline void set_m_lstLiveBall_126(List_1_t128053199 * value)
	{
		___m_lstLiveBall_126 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstLiveBall_126), value);
	}

	inline static int32_t get_offset_of_m_nBallNum_127() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nBallNum_127)); }
	inline int32_t get_m_nBallNum_127() const { return ___m_nBallNum_127; }
	inline int32_t* get_address_of_m_nBallNum_127() { return &___m_nBallNum_127; }
	inline void set_m_nBallNum_127(int32_t value)
	{
		___m_nBallNum_127 = value;
	}

	inline static int32_t get_offset_of_m_lstSomeAvailabelBallIndex_128() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstSomeAvailabelBallIndex_128)); }
	inline List_1_t128053199 * get_m_lstSomeAvailabelBallIndex_128() const { return ___m_lstSomeAvailabelBallIndex_128; }
	inline List_1_t128053199 ** get_address_of_m_lstSomeAvailabelBallIndex_128() { return &___m_lstSomeAvailabelBallIndex_128; }
	inline void set_m_lstSomeAvailabelBallIndex_128(List_1_t128053199 * value)
	{
		___m_lstSomeAvailabelBallIndex_128 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSomeAvailabelBallIndex_128), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledBalls_129() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstRecycledBalls_129)); }
	inline List_1_t3678741308 * get_m_lstRecycledBalls_129() const { return ___m_lstRecycledBalls_129; }
	inline List_1_t3678741308 ** get_address_of_m_lstRecycledBalls_129() { return &___m_lstRecycledBalls_129; }
	inline void set_m_lstRecycledBalls_129(List_1_t3678741308 * value)
	{
		___m_lstRecycledBalls_129 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledBalls_129), value);
	}

	inline static int32_t get_offset_of_m_bDead_130() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bDead_130)); }
	inline bool get_m_bDead_130() const { return ___m_bDead_130; }
	inline bool* get_address_of_m_bDead_130() { return &___m_bDead_130; }
	inline void set_m_bDead_130(bool value)
	{
		___m_bDead_130 = value;
	}

	inline static int32_t get_offset_of_m_nEaterId_131() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nEaterId_131)); }
	inline int32_t get_m_nEaterId_131() const { return ___m_nEaterId_131; }
	inline int32_t* get_address_of_m_nEaterId_131() { return &___m_nEaterId_131; }
	inline void set_m_nEaterId_131(int32_t value)
	{
		___m_nEaterId_131 = value;
	}

	inline static int32_t get_offset_of_m_bReborn_132() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bReborn_132)); }
	inline bool get_m_bReborn_132() const { return ___m_bReborn_132; }
	inline bool* get_address_of_m_bReborn_132() { return &___m_bReborn_132; }
	inline void set_m_bReborn_132(bool value)
	{
		___m_bReborn_132 = value;
	}

	inline static int32_t get_offset_of_m_bAllBallsMovingToCenter_133() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bAllBallsMovingToCenter_133)); }
	inline bool get_m_bAllBallsMovingToCenter_133() const { return ___m_bAllBallsMovingToCenter_133; }
	inline bool* get_address_of_m_bAllBallsMovingToCenter_133() { return &___m_bAllBallsMovingToCenter_133; }
	inline void set_m_bAllBallsMovingToCenter_133(bool value)
	{
		___m_bAllBallsMovingToCenter_133 = value;
	}

	inline static int32_t get_offset_of_m_ballBiggest_134() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_ballBiggest_134)); }
	inline Ball_t2206666566 * get_m_ballBiggest_134() const { return ___m_ballBiggest_134; }
	inline Ball_t2206666566 ** get_address_of_m_ballBiggest_134() { return &___m_ballBiggest_134; }
	inline void set_m_ballBiggest_134(Ball_t2206666566 * value)
	{
		___m_ballBiggest_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_ballBiggest_134), value);
	}

	inline static int32_t get_offset_of_m_nTestExplodeStatus_135() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nTestExplodeStatus_135)); }
	inline int32_t get_m_nTestExplodeStatus_135() const { return ___m_nTestExplodeStatus_135; }
	inline int32_t* get_address_of_m_nTestExplodeStatus_135() { return &___m_nTestExplodeStatus_135; }
	inline void set_m_nTestExplodeStatus_135(int32_t value)
	{
		___m_nTestExplodeStatus_135 = value;
	}

	inline static int32_t get_offset_of_m_bTestPlayer_136() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bTestPlayer_136)); }
	inline bool get_m_bTestPlayer_136() const { return ___m_bTestPlayer_136; }
	inline bool* get_address_of_m_bTestPlayer_136() { return &___m_bTestPlayer_136; }
	inline void set_m_bTestPlayer_136(bool value)
	{
		___m_bTestPlayer_136 = value;
	}

	inline static int32_t get_offset_of_m_vecSpecailDirection_137() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_vecSpecailDirection_137)); }
	inline Vector2_t2156229523  get_m_vecSpecailDirection_137() const { return ___m_vecSpecailDirection_137; }
	inline Vector2_t2156229523 * get_address_of_m_vecSpecailDirection_137() { return &___m_vecSpecailDirection_137; }
	inline void set_m_vecSpecailDirection_137(Vector2_t2156229523  value)
	{
		___m_vecSpecailDirection_137 = value;
	}

	inline static int32_t get_offset_of_m_nAdjustMoveInfoFrameCount_138() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nAdjustMoveInfoFrameCount_138)); }
	inline float get_m_nAdjustMoveInfoFrameCount_138() const { return ___m_nAdjustMoveInfoFrameCount_138; }
	inline float* get_address_of_m_nAdjustMoveInfoFrameCount_138() { return &___m_nAdjustMoveInfoFrameCount_138; }
	inline void set_m_nAdjustMoveInfoFrameCount_138(float value)
	{
		___m_nAdjustMoveInfoFrameCount_138 = value;
	}

	inline static int32_t get_offset_of__bytestRelatedInfo_142() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____bytestRelatedInfo_142)); }
	inline ByteU5BU5D_t4116647657* get__bytestRelatedInfo_142() const { return ____bytestRelatedInfo_142; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytestRelatedInfo_142() { return &____bytestRelatedInfo_142; }
	inline void set__bytestRelatedInfo_142(ByteU5BU5D_t4116647657* value)
	{
		____bytestRelatedInfo_142 = value;
		Il2CppCodeGenWriteBarrier((&____bytestRelatedInfo_142), value);
	}

	inline static int32_t get_offset_of_m_nSyncMoveInfoTimeCount_144() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSyncMoveInfoTimeCount_144)); }
	inline float get_m_nSyncMoveInfoTimeCount_144() const { return ___m_nSyncMoveInfoTimeCount_144; }
	inline float* get_address_of_m_nSyncMoveInfoTimeCount_144() { return &___m_nSyncMoveInfoTimeCount_144; }
	inline void set_m_nSyncMoveInfoTimeCount_144(float value)
	{
		___m_nSyncMoveInfoTimeCount_144 = value;
	}

	inline static int32_t get_offset_of_m_dicGunSight_146() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicGunSight_146)); }
	inline Dictionary_2_t2518726632 * get_m_dicGunSight_146() const { return ___m_dicGunSight_146; }
	inline Dictionary_2_t2518726632 ** get_address_of_m_dicGunSight_146() { return &___m_dicGunSight_146; }
	inline void set_m_dicGunSight_146(Dictionary_2_t2518726632 * value)
	{
		___m_dicGunSight_146 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicGunSight_146), value);
	}

	inline static int32_t get_offset_of_m_dicNotPickedGunSight_147() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicNotPickedGunSight_147)); }
	inline Dictionary_2_t1839659084 * get_m_dicNotPickedGunSight_147() const { return ___m_dicNotPickedGunSight_147; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicNotPickedGunSight_147() { return &___m_dicNotPickedGunSight_147; }
	inline void set_m_dicNotPickedGunSight_147(Dictionary_2_t1839659084 * value)
	{
		___m_dicNotPickedGunSight_147 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicNotPickedGunSight_147), value);
	}

	inline static int32_t get_offset_of_m_bFirstReceive_148() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bFirstReceive_148)); }
	inline bool get_m_bFirstReceive_148() const { return ___m_bFirstReceive_148; }
	inline bool* get_address_of_m_bFirstReceive_148() { return &___m_bFirstReceive_148; }
	inline void set_m_bFirstReceive_148(bool value)
	{
		___m_bFirstReceive_148 = value;
	}

	inline static int32_t get_offset_of_m_fReceiveTimeCount_149() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fReceiveTimeCount_149)); }
	inline float get_m_fReceiveTimeCount_149() const { return ___m_fReceiveTimeCount_149; }
	inline float* get_address_of_m_fReceiveTimeCount_149() { return &___m_fReceiveTimeCount_149; }
	inline void set_m_fReceiveTimeCount_149(float value)
	{
		___m_fReceiveTimeCount_149 = value;
	}

	inline static int32_t get_offset_of_m_fReceiveTotalTime_150() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fReceiveTotalTime_150)); }
	inline float get_m_fReceiveTotalTime_150() const { return ___m_fReceiveTotalTime_150; }
	inline float* get_address_of_m_fReceiveTotalTime_150() { return &___m_fReceiveTotalTime_150; }
	inline void set_m_fReceiveTotalTime_150(float value)
	{
		___m_fReceiveTotalTime_150 = value;
	}

	inline static int32_t get_offset_of_m_fLastTimeElapse_151() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fLastTimeElapse_151)); }
	inline float get_m_fLastTimeElapse_151() const { return ___m_fLastTimeElapse_151; }
	inline float* get_address_of_m_fLastTimeElapse_151() { return &___m_fLastTimeElapse_151; }
	inline void set_m_fLastTimeElapse_151(float value)
	{
		___m_fLastTimeElapse_151 = value;
	}

	inline static int32_t get_offset_of_m_fDelay_152() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fDelay_152)); }
	inline float get_m_fDelay_152() const { return ___m_fDelay_152; }
	inline float* get_address_of_m_fDelay_152() { return &___m_fDelay_152; }
	inline void set_m_fDelay_152(float value)
	{
		___m_fDelay_152 = value;
	}

	inline static int32_t get_offset_of_m_fTotalDelay_153() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fTotalDelay_153)); }
	inline float get_m_fTotalDelay_153() const { return ___m_fTotalDelay_153; }
	inline float* get_address_of_m_fTotalDelay_153() { return &___m_fTotalDelay_153; }
	inline void set_m_fTotalDelay_153(float value)
	{
		___m_fTotalDelay_153 = value;
	}

	inline static int32_t get_offset_of_m_nDelayCount_154() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nDelayCount_154)); }
	inline float get_m_nDelayCount_154() const { return ___m_nDelayCount_154; }
	inline float* get_address_of_m_nDelayCount_154() { return &___m_nDelayCount_154; }
	inline void set_m_nDelayCount_154(float value)
	{
		___m_nDelayCount_154 = value;
	}

	inline static int32_t get_offset_of_m_fLastReceiveTimeElapse_155() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fLastReceiveTimeElapse_155)); }
	inline float get_m_fLastReceiveTimeElapse_155() const { return ___m_fLastReceiveTimeElapse_155; }
	inline float* get_address_of_m_fLastReceiveTimeElapse_155() { return &___m_fLastReceiveTimeElapse_155; }
	inline void set_m_fLastReceiveTimeElapse_155(float value)
	{
		___m_fLastReceiveTimeElapse_155 = value;
	}

	inline static int32_t get_offset_of__bytesDusts_156() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____bytesDusts_156)); }
	inline ByteU5BU5D_t4116647657* get__bytesDusts_156() const { return ____bytesDusts_156; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesDusts_156() { return &____bytesDusts_156; }
	inline void set__bytesDusts_156(ByteU5BU5D_t4116647657* value)
	{
		____bytesDusts_156 = value;
		Il2CppCodeGenWriteBarrier((&____bytesDusts_156), value);
	}

	inline static int32_t get_offset_of_m_fSyncDustTimeCount_158() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSyncDustTimeCount_158)); }
	inline float get_m_fSyncDustTimeCount_158() const { return ___m_fSyncDustTimeCount_158; }
	inline float* get_address_of_m_fSyncDustTimeCount_158() { return &___m_fSyncDustTimeCount_158; }
	inline void set_m_fSyncDustTimeCount_158(float value)
	{
		___m_fSyncDustTimeCount_158 = value;
	}

	inline static int32_t get_offset_of_m_fPaiHangBangSyncTimeCount_160() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPaiHangBangSyncTimeCount_160)); }
	inline float get_m_fPaiHangBangSyncTimeCount_160() const { return ___m_fPaiHangBangSyncTimeCount_160; }
	inline float* get_address_of_m_fPaiHangBangSyncTimeCount_160() { return &___m_fPaiHangBangSyncTimeCount_160; }
	inline void set_m_fPaiHangBangSyncTimeCount_160(float value)
	{
		___m_fPaiHangBangSyncTimeCount_160 = value;
	}

	inline static int32_t get_offset_of_m_lstEatBeanSyncQueue_161() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstEatBeanSyncQueue_161)); }
	inline List_1_t2648729287 * get_m_lstEatBeanSyncQueue_161() const { return ___m_lstEatBeanSyncQueue_161; }
	inline List_1_t2648729287 ** get_address_of_m_lstEatBeanSyncQueue_161() { return &___m_lstEatBeanSyncQueue_161; }
	inline void set_m_lstEatBeanSyncQueue_161(List_1_t2648729287 * value)
	{
		___m_lstEatBeanSyncQueue_161 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEatBeanSyncQueue_161), value);
	}

	inline static int32_t get_offset_of_m_lstEatThornSyncQueue_162() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstEatThornSyncQueue_162)); }
	inline List_1_t3413545680 * get_m_lstEatThornSyncQueue_162() const { return ___m_lstEatThornSyncQueue_162; }
	inline List_1_t3413545680 ** get_address_of_m_lstEatThornSyncQueue_162() { return &___m_lstEatThornSyncQueue_162; }
	inline void set_m_lstEatThornSyncQueue_162(List_1_t3413545680 * value)
	{
		___m_lstEatThornSyncQueue_162 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEatThornSyncQueue_162), value);
	}

	inline static int32_t get_offset_of_m_lstEatSporeSyncQueue_163() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstEatSporeSyncQueue_163)); }
	inline List_1_t3413545680 * get_m_lstEatSporeSyncQueue_163() const { return ___m_lstEatSporeSyncQueue_163; }
	inline List_1_t3413545680 ** get_address_of_m_lstEatSporeSyncQueue_163() { return &___m_lstEatSporeSyncQueue_163; }
	inline void set_m_lstEatSporeSyncQueue_163(List_1_t3413545680 * value)
	{
		___m_lstEatSporeSyncQueue_163 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEatSporeSyncQueue_163), value);
	}

	inline static int32_t get_offset_of_m_fEatThornSyncCount_165() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fEatThornSyncCount_165)); }
	inline float get_m_fEatThornSyncCount_165() const { return ___m_fEatThornSyncCount_165; }
	inline float* get_address_of_m_fEatThornSyncCount_165() { return &___m_fEatThornSyncCount_165; }
	inline void set_m_fEatThornSyncCount_165(float value)
	{
		___m_fEatThornSyncCount_165 = value;
	}

	inline static int32_t get_offset_of_m_dicAssistAttack_166() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicAssistAttack_166)); }
	inline Dictionary_2_t285980105 * get_m_dicAssistAttack_166() const { return ___m_dicAssistAttack_166; }
	inline Dictionary_2_t285980105 ** get_address_of_m_dicAssistAttack_166() { return &___m_dicAssistAttack_166; }
	inline void set_m_dicAssistAttack_166(Dictionary_2_t285980105 * value)
	{
		___m_dicAssistAttack_166 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAssistAttack_166), value);
	}

	inline static int32_t get_offset_of_m_lstValidAssitPlayerId_167() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstValidAssitPlayerId_167)); }
	inline List_1_t128053199 * get_m_lstValidAssitPlayerId_167() const { return ___m_lstValidAssitPlayerId_167; }
	inline List_1_t128053199 ** get_address_of_m_lstValidAssitPlayerId_167() { return &___m_lstValidAssitPlayerId_167; }
	inline void set_m_lstValidAssitPlayerId_167(List_1_t128053199 * value)
	{
		___m_lstValidAssitPlayerId_167 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstValidAssitPlayerId_167), value);
	}

	inline static int32_t get_offset_of_m_fExplodeChildNumBuffEffect_168() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fExplodeChildNumBuffEffect_168)); }
	inline float get_m_fExplodeChildNumBuffEffect_168() const { return ___m_fExplodeChildNumBuffEffect_168; }
	inline float* get_address_of_m_fExplodeChildNumBuffEffect_168() { return &___m_fExplodeChildNumBuffEffect_168; }
	inline void set_m_fExplodeChildNumBuffEffect_168(float value)
	{
		___m_fExplodeChildNumBuffEffect_168 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeMotherLeftBuffEffect_169() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fExplodeMotherLeftBuffEffect_169)); }
	inline float get_m_fExplodeMotherLeftBuffEffect_169() const { return ___m_fExplodeMotherLeftBuffEffect_169; }
	inline float* get_address_of_m_fExplodeMotherLeftBuffEffect_169() { return &___m_fExplodeMotherLeftBuffEffect_169; }
	inline void set_m_fExplodeMotherLeftBuffEffect_169(float value)
	{
		___m_fExplodeMotherLeftBuffEffect_169 = value;
	}

	inline static int32_t get_offset_of_m_lstExplodingNode_171() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstExplodingNode_171)); }
	inline List_1_t1569919409 * get_m_lstExplodingNode_171() const { return ___m_lstExplodingNode_171; }
	inline List_1_t1569919409 ** get_address_of_m_lstExplodingNode_171() { return &___m_lstExplodingNode_171; }
	inline void set_m_lstExplodingNode_171(List_1_t1569919409 * value)
	{
		___m_lstExplodingNode_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstExplodingNode_171), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledExplodeNode_172() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstRecycledExplodeNode_172)); }
	inline List_1_t1569919409 * get_m_lstRecycledExplodeNode_172() const { return ___m_lstRecycledExplodeNode_172; }
	inline List_1_t1569919409 ** get_address_of_m_lstRecycledExplodeNode_172() { return &___m_lstRecycledExplodeNode_172; }
	inline void set_m_lstRecycledExplodeNode_172(List_1_t1569919409 * value)
	{
		___m_lstRecycledExplodeNode_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledExplodeNode_172), value);
	}

	inline static int32_t get_offset_of_m_nCurSkinIndex_173() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nCurSkinIndex_173)); }
	inline int32_t get_m_nCurSkinIndex_173() const { return ___m_nCurSkinIndex_173; }
	inline int32_t* get_address_of_m_nCurSkinIndex_173() { return &___m_nCurSkinIndex_173; }
	inline void set_m_nCurSkinIndex_173(int32_t value)
	{
		___m_nCurSkinIndex_173 = value;
	}

	inline static int32_t get_offset_of_m_sprCurAvatar_174() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_sprCurAvatar_174)); }
	inline Sprite_t280657092 * get_m_sprCurAvatar_174() const { return ___m_sprCurAvatar_174; }
	inline Sprite_t280657092 ** get_address_of_m_sprCurAvatar_174() { return &___m_sprCurAvatar_174; }
	inline void set_m_sprCurAvatar_174(Sprite_t280657092 * value)
	{
		___m_sprCurAvatar_174 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprCurAvatar_174), value);
	}

	inline static int32_t get_offset_of_m_bMyDataLoaded_175() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bMyDataLoaded_175)); }
	inline bool get_m_bMyDataLoaded_175() const { return ___m_bMyDataLoaded_175; }
	inline bool* get_address_of_m_bMyDataLoaded_175() { return &___m_bMyDataLoaded_175; }
	inline void set_m_bMyDataLoaded_175(bool value)
	{
		___m_bMyDataLoaded_175 = value;
	}

	inline static int32_t get_offset_of_m_SyncPosAfterEjectTotalTime_176() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_SyncPosAfterEjectTotalTime_176)); }
	inline float get_m_SyncPosAfterEjectTotalTime_176() const { return ___m_SyncPosAfterEjectTotalTime_176; }
	inline float* get_address_of_m_SyncPosAfterEjectTotalTime_176() { return &___m_SyncPosAfterEjectTotalTime_176; }
	inline void set_m_SyncPosAfterEjectTotalTime_176(float value)
	{
		___m_SyncPosAfterEjectTotalTime_176 = value;
	}

	inline static int32_t get_offset_of_m_fSkillMpCost_MagicShield_177() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillMpCost_MagicShield_177)); }
	inline float get_m_fSkillMpCost_MagicShield_177() const { return ___m_fSkillMpCost_MagicShield_177; }
	inline float* get_address_of_m_fSkillMpCost_MagicShield_177() { return &___m_fSkillMpCost_MagicShield_177; }
	inline void set_m_fSkillMpCost_MagicShield_177(float value)
	{
		___m_fSkillMpCost_MagicShield_177 = value;
	}

	inline static int32_t get_offset_of_m_fSkillDuration_MagicShield_178() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillDuration_MagicShield_178)); }
	inline float get_m_fSkillDuration_MagicShield_178() const { return ___m_fSkillDuration_MagicShield_178; }
	inline float* get_address_of_m_fSkillDuration_MagicShield_178() { return &___m_fSkillDuration_MagicShield_178; }
	inline void set_m_fSkillDuration_MagicShield_178(float value)
	{
		___m_fSkillDuration_MagicShield_178 = value;
	}

	inline static int32_t get_offset_of_m_bMagicShielding_179() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bMagicShielding_179)); }
	inline bool get_m_bMagicShielding_179() const { return ___m_bMagicShielding_179; }
	inline bool* get_address_of_m_bMagicShielding_179() { return &___m_bMagicShielding_179; }
	inline void set_m_bMagicShielding_179(bool value)
	{
		___m_bMagicShielding_179 = value;
	}

	inline static int32_t get_offset_of_m_fSkillSpeedAffect_MagicShield_180() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillSpeedAffect_MagicShield_180)); }
	inline float get_m_fSkillSpeedAffect_MagicShield_180() const { return ___m_fSkillSpeedAffect_MagicShield_180; }
	inline float* get_address_of_m_fSkillSpeedAffect_MagicShield_180() { return &___m_fSkillSpeedAffect_MagicShield_180; }
	inline void set_m_fSkillSpeedAffect_MagicShield_180(float value)
	{
		___m_fSkillSpeedAffect_MagicShield_180 = value;
	}

	inline static int32_t get_offset_of_m_fSkillLoopCount_MagicShield_181() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillLoopCount_MagicShield_181)); }
	inline float get_m_fSkillLoopCount_MagicShield_181() const { return ___m_fSkillLoopCount_MagicShield_181; }
	inline float* get_address_of_m_fSkillLoopCount_MagicShield_181() { return &___m_fSkillLoopCount_MagicShield_181; }
	inline void set_m_fSkillLoopCount_MagicShield_181(float value)
	{
		___m_fSkillLoopCount_MagicShield_181 = value;
	}

	inline static int32_t get_offset_of_m_fSkillMpCost_Annihilate_182() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillMpCost_Annihilate_182)); }
	inline float get_m_fSkillMpCost_Annihilate_182() const { return ___m_fSkillMpCost_Annihilate_182; }
	inline float* get_address_of_m_fSkillMpCost_Annihilate_182() { return &___m_fSkillMpCost_Annihilate_182; }
	inline void set_m_fSkillMpCost_Annihilate_182(float value)
	{
		___m_fSkillMpCost_Annihilate_182 = value;
	}

	inline static int32_t get_offset_of_m_fSkillPercent_Annihilate_183() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillPercent_Annihilate_183)); }
	inline float get_m_fSkillPercent_Annihilate_183() const { return ___m_fSkillPercent_Annihilate_183; }
	inline float* get_address_of_m_fSkillPercent_Annihilate_183() { return &___m_fSkillPercent_Annihilate_183; }
	inline void set_m_fSkillPercent_Annihilate_183(float value)
	{
		___m_fSkillPercent_Annihilate_183 = value;
	}

	inline static int32_t get_offset_of_m_fSkillDuration_Annihilate_184() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillDuration_Annihilate_184)); }
	inline float get_m_fSkillDuration_Annihilate_184() const { return ___m_fSkillDuration_Annihilate_184; }
	inline float* get_address_of_m_fSkillDuration_Annihilate_184() { return &___m_fSkillDuration_Annihilate_184; }
	inline void set_m_fSkillDuration_Annihilate_184(float value)
	{
		___m_fSkillDuration_Annihilate_184 = value;
	}

	inline static int32_t get_offset_of_m_fSkillSpeedAffect_Annihilate_185() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillSpeedAffect_Annihilate_185)); }
	inline float get_m_fSkillSpeedAffect_Annihilate_185() const { return ___m_fSkillSpeedAffect_Annihilate_185; }
	inline float* get_address_of_m_fSkillSpeedAffect_Annihilate_185() { return &___m_fSkillSpeedAffect_Annihilate_185; }
	inline void set_m_fSkillSpeedAffect_Annihilate_185(float value)
	{
		___m_fSkillSpeedAffect_Annihilate_185 = value;
	}

	inline static int32_t get_offset_of_m_bAnnihilating_186() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bAnnihilating_186)); }
	inline bool get_m_bAnnihilating_186() const { return ___m_bAnnihilating_186; }
	inline bool* get_address_of_m_bAnnihilating_186() { return &___m_bAnnihilating_186; }
	inline void set_m_bAnnihilating_186(bool value)
	{
		___m_bAnnihilating_186 = value;
	}

	inline static int32_t get_offset_of_m_fSkillLoopCount_Annihilate_187() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillLoopCount_Annihilate_187)); }
	inline float get_m_fSkillLoopCount_Annihilate_187() const { return ___m_fSkillLoopCount_Annihilate_187; }
	inline float* get_address_of_m_fSkillLoopCount_Annihilate_187() { return &___m_fSkillLoopCount_Annihilate_187; }
	inline void set_m_fSkillLoopCount_Annihilate_187(float value)
	{
		___m_fSkillLoopCount_Annihilate_187 = value;
	}

	inline static int32_t get_offset_of_m_fSkillMpCost_Gold_188() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillMpCost_Gold_188)); }
	inline float get_m_fSkillMpCost_Gold_188() const { return ___m_fSkillMpCost_Gold_188; }
	inline float* get_address_of_m_fSkillMpCost_Gold_188() { return &___m_fSkillMpCost_Gold_188; }
	inline void set_m_fSkillMpCost_Gold_188(float value)
	{
		___m_fSkillMpCost_Gold_188 = value;
	}

	inline static int32_t get_offset_of_m_fSkillGold_SpeedAddPercent_189() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillGold_SpeedAddPercent_189)); }
	inline float get_m_fSkillGold_SpeedAddPercent_189() const { return ___m_fSkillGold_SpeedAddPercent_189; }
	inline float* get_address_of_m_fSkillGold_SpeedAddPercent_189() { return &___m_fSkillGold_SpeedAddPercent_189; }
	inline void set_m_fSkillGold_SpeedAddPercent_189(float value)
	{
		___m_fSkillGold_SpeedAddPercent_189 = value;
	}

	inline static int32_t get_offset_of_m_fGold_Duration_190() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fGold_Duration_190)); }
	inline float get_m_fGold_Duration_190() const { return ___m_fGold_Duration_190; }
	inline float* get_address_of_m_fGold_Duration_190() { return &___m_fGold_Duration_190; }
	inline void set_m_fGold_Duration_190(float value)
	{
		___m_fGold_Duration_190 = value;
	}

	inline static int32_t get_offset_of_m_bGold_191() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bGold_191)); }
	inline bool get_m_bGold_191() const { return ___m_bGold_191; }
	inline bool* get_address_of_m_bGold_191() { return &___m_bGold_191; }
	inline void set_m_bGold_191(bool value)
	{
		___m_bGold_191 = value;
	}

	inline static int32_t get_offset_of_m_fGoldLoopCount_192() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fGoldLoopCount_192)); }
	inline float get_m_fGoldLoopCount_192() const { return ___m_fGoldLoopCount_192; }
	inline float* get_address_of_m_fGoldLoopCount_192() { return &___m_fGoldLoopCount_192; }
	inline void set_m_fGoldLoopCount_192(float value)
	{
		___m_fGoldLoopCount_192 = value;
	}

	inline static int32_t get_offset_of_m_fSkillMpCost_Henzy_193() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillMpCost_Henzy_193)); }
	inline float get_m_fSkillMpCost_Henzy_193() const { return ___m_fSkillMpCost_Henzy_193; }
	inline float* get_address_of_m_fSkillMpCost_Henzy_193() { return &___m_fSkillMpCost_Henzy_193; }
	inline void set_m_fSkillMpCost_Henzy_193(float value)
	{
		___m_fSkillMpCost_Henzy_193 = value;
	}

	inline static int32_t get_offset_of_m_fHenzy_Duration_194() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fHenzy_Duration_194)); }
	inline float get_m_fHenzy_Duration_194() const { return ___m_fHenzy_Duration_194; }
	inline float* get_address_of_m_fHenzy_Duration_194() { return &___m_fHenzy_Duration_194; }
	inline void set_m_fHenzy_Duration_194(float value)
	{
		___m_fHenzy_Duration_194 = value;
	}

	inline static int32_t get_offset_of_m_fHenzy_SpeedChangePercent_195() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fHenzy_SpeedChangePercent_195)); }
	inline float get_m_fHenzy_SpeedChangePercent_195() const { return ___m_fHenzy_SpeedChangePercent_195; }
	inline float* get_address_of_m_fHenzy_SpeedChangePercent_195() { return &___m_fHenzy_SpeedChangePercent_195; }
	inline void set_m_fHenzy_SpeedChangePercent_195(float value)
	{
		___m_fHenzy_SpeedChangePercent_195 = value;
	}

	inline static int32_t get_offset_of_m_fHenzy_WColdDown_196() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fHenzy_WColdDown_196)); }
	inline float get_m_fHenzy_WColdDown_196() const { return ___m_fHenzy_WColdDown_196; }
	inline float* get_address_of_m_fHenzy_WColdDown_196() { return &___m_fHenzy_WColdDown_196; }
	inline void set_m_fHenzy_WColdDown_196(float value)
	{
		___m_fHenzy_WColdDown_196 = value;
	}

	inline static int32_t get_offset_of_m_fHenzy_EColdDown_197() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fHenzy_EColdDown_197)); }
	inline float get_m_fHenzy_EColdDown_197() const { return ___m_fHenzy_EColdDown_197; }
	inline float* get_address_of_m_fHenzy_EColdDown_197() { return &___m_fHenzy_EColdDown_197; }
	inline void set_m_fHenzy_EColdDown_197(float value)
	{
		___m_fHenzy_EColdDown_197 = value;
	}

	inline static int32_t get_offset_of_m_bHenzy_198() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bHenzy_198)); }
	inline bool get_m_bHenzy_198() const { return ___m_bHenzy_198; }
	inline bool* get_address_of_m_bHenzy_198() { return &___m_bHenzy_198; }
	inline void set_m_bHenzy_198(bool value)
	{
		___m_bHenzy_198 = value;
	}

	inline static int32_t get_offset_of_m_fHenzyLoopCount_199() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fHenzyLoopCount_199)); }
	inline float get_m_fHenzyLoopCount_199() const { return ___m_fHenzyLoopCount_199; }
	inline float* get_address_of_m_fHenzyLoopCount_199() { return &___m_fHenzyLoopCount_199; }
	inline void set_m_fHenzyLoopCount_199(float value)
	{
		___m_fHenzyLoopCount_199 = value;
	}

	inline static int32_t get_offset_of_m_fSkillMpCost_MergeAll_200() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillMpCost_MergeAll_200)); }
	inline float get_m_fSkillMpCost_MergeAll_200() const { return ___m_fSkillMpCost_MergeAll_200; }
	inline float* get_address_of_m_fSkillMpCost_MergeAll_200() { return &___m_fSkillMpCost_MergeAll_200; }
	inline void set_m_fSkillMpCost_MergeAll_200(float value)
	{
		___m_fSkillMpCost_MergeAll_200 = value;
	}

	inline static int32_t get_offset_of_m_fSkillQianYao_MergeAll_201() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillQianYao_MergeAll_201)); }
	inline float get_m_fSkillQianYao_MergeAll_201() const { return ___m_fSkillQianYao_MergeAll_201; }
	inline float* get_address_of_m_fSkillQianYao_MergeAll_201() { return &___m_fSkillQianYao_MergeAll_201; }
	inline void set_m_fSkillQianYao_MergeAll_201(float value)
	{
		___m_fSkillQianYao_MergeAll_201 = value;
	}

	inline static int32_t get_offset_of_m_bMiaoHeQianYao_202() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bMiaoHeQianYao_202)); }
	inline bool get_m_bMiaoHeQianYao_202() const { return ___m_bMiaoHeQianYao_202; }
	inline bool* get_address_of_m_bMiaoHeQianYao_202() { return &___m_bMiaoHeQianYao_202; }
	inline void set_m_bMiaoHeQianYao_202(bool value)
	{
		___m_bMiaoHeQianYao_202 = value;
	}

	inline static int32_t get_offset_of_m_fSkillQianYaoCount_MergeAll_203() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillQianYaoCount_MergeAll_203)); }
	inline float get_m_fSkillQianYaoCount_MergeAll_203() const { return ___m_fSkillQianYaoCount_MergeAll_203; }
	inline float* get_address_of_m_fSkillQianYaoCount_MergeAll_203() { return &___m_fSkillQianYaoCount_MergeAll_203; }
	inline void set_m_fSkillQianYaoCount_MergeAll_203(float value)
	{
		___m_fSkillQianYaoCount_MergeAll_203 = value;
	}

	inline static int32_t get_offset_of_m_fMiaoHeQiaoYaoRotation_204() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fMiaoHeQiaoYaoRotation_204)); }
	inline float get_m_fMiaoHeQiaoYaoRotation_204() const { return ___m_fMiaoHeQiaoYaoRotation_204; }
	inline float* get_address_of_m_fMiaoHeQiaoYaoRotation_204() { return &___m_fMiaoHeQiaoYaoRotation_204; }
	inline void set_m_fMiaoHeQiaoYaoRotation_204(float value)
	{
		___m_fMiaoHeQiaoYaoRotation_204 = value;
	}

	inline static int32_t get_offset_of_m_fSkillMpCost_Sneak_205() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillMpCost_Sneak_205)); }
	inline float get_m_fSkillMpCost_Sneak_205() const { return ___m_fSkillMpCost_Sneak_205; }
	inline float* get_address_of_m_fSkillMpCost_Sneak_205() { return &___m_fSkillMpCost_Sneak_205; }
	inline void set_m_fSkillMpCost_Sneak_205(float value)
	{
		___m_fSkillMpCost_Sneak_205 = value;
	}

	inline static int32_t get_offset_of_m_fSkillSpeedChangePercent_Sneak_206() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillSpeedChangePercent_Sneak_206)); }
	inline float get_m_fSkillSpeedChangePercent_Sneak_206() const { return ___m_fSkillSpeedChangePercent_Sneak_206; }
	inline float* get_address_of_m_fSkillSpeedChangePercent_Sneak_206() { return &___m_fSkillSpeedChangePercent_Sneak_206; }
	inline void set_m_fSkillSpeedChangePercent_Sneak_206(float value)
	{
		___m_fSkillSpeedChangePercent_Sneak_206 = value;
	}

	inline static int32_t get_offset_of_m_fSkillDuration_Sneak_207() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillDuration_Sneak_207)); }
	inline float get_m_fSkillDuration_Sneak_207() const { return ___m_fSkillDuration_Sneak_207; }
	inline float* get_address_of_m_fSkillDuration_Sneak_207() { return &___m_fSkillDuration_Sneak_207; }
	inline void set_m_fSkillDuration_Sneak_207(float value)
	{
		___m_fSkillDuration_Sneak_207 = value;
	}

	inline static int32_t get_offset_of_m_bSneaking_208() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bSneaking_208)); }
	inline bool get_m_bSneaking_208() const { return ___m_bSneaking_208; }
	inline bool* get_address_of_m_bSneaking_208() { return &___m_bSneaking_208; }
	inline void set_m_bSneaking_208(bool value)
	{
		___m_bSneaking_208 = value;
	}

	inline static int32_t get_offset_of_m_fSkillDurationCount_Sneak_209() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSkillDurationCount_Sneak_209)); }
	inline float get_m_fSkillDurationCount_Sneak_209() const { return ___m_fSkillDurationCount_Sneak_209; }
	inline float* get_address_of_m_fSkillDurationCount_Sneak_209() { return &___m_fSkillDurationCount_Sneak_209; }
	inline void set_m_fSkillDurationCount_Sneak_209(float value)
	{
		___m_fSkillDurationCount_Sneak_209 = value;
	}

	inline static int32_t get_offset_of_m_fMpCost_BecomeThorn_210() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fMpCost_BecomeThorn_210)); }
	inline float get_m_fMpCost_BecomeThorn_210() const { return ___m_fMpCost_BecomeThorn_210; }
	inline float* get_address_of_m_fMpCost_BecomeThorn_210() { return &___m_fMpCost_BecomeThorn_210; }
	inline void set_m_fMpCost_BecomeThorn_210(float value)
	{
		___m_fMpCost_BecomeThorn_210 = value;
	}

	inline static int32_t get_offset_of_m_fQianYao_BecomeThorn_211() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fQianYao_BecomeThorn_211)); }
	inline float get_m_fQianYao_BecomeThorn_211() const { return ___m_fQianYao_BecomeThorn_211; }
	inline float* get_address_of_m_fQianYao_BecomeThorn_211() { return &___m_fQianYao_BecomeThorn_211; }
	inline void set_m_fQianYao_BecomeThorn_211(float value)
	{
		___m_fQianYao_BecomeThorn_211 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedChangePercent_BecomeThorn_212() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSpeedChangePercent_BecomeThorn_212)); }
	inline float get_m_fSpeedChangePercent_BecomeThorn_212() const { return ___m_fSpeedChangePercent_BecomeThorn_212; }
	inline float* get_address_of_m_fSpeedChangePercent_BecomeThorn_212() { return &___m_fSpeedChangePercent_BecomeThorn_212; }
	inline void set_m_fSpeedChangePercent_BecomeThorn_212(float value)
	{
		___m_fSpeedChangePercent_BecomeThorn_212 = value;
	}

	inline static int32_t get_offset_of_m_fDuration_BecomeThorn_213() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fDuration_BecomeThorn_213)); }
	inline float get_m_fDuration_BecomeThorn_213() const { return ___m_fDuration_BecomeThorn_213; }
	inline float* get_address_of_m_fDuration_BecomeThorn_213() { return &___m_fDuration_BecomeThorn_213; }
	inline void set_m_fDuration_BecomeThorn_213(float value)
	{
		___m_fDuration_BecomeThorn_213 = value;
	}

	inline static int32_t get_offset_of_m_szExplodeConfigId_BecomeThorn_214() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_szExplodeConfigId_BecomeThorn_214)); }
	inline String_t* get_m_szExplodeConfigId_BecomeThorn_214() const { return ___m_szExplodeConfigId_BecomeThorn_214; }
	inline String_t** get_address_of_m_szExplodeConfigId_BecomeThorn_214() { return &___m_szExplodeConfigId_BecomeThorn_214; }
	inline void set_m_szExplodeConfigId_BecomeThorn_214(String_t* value)
	{
		___m_szExplodeConfigId_BecomeThorn_214 = value;
		Il2CppCodeGenWriteBarrier((&___m_szExplodeConfigId_BecomeThorn_214), value);
	}

	inline static int32_t get_offset_of_m_fBecomeThornTimeCount_215() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fBecomeThornTimeCount_215)); }
	inline float get_m_fBecomeThornTimeCount_215() const { return ___m_fBecomeThornTimeCount_215; }
	inline float* get_address_of_m_fBecomeThornTimeCount_215() { return &___m_fBecomeThornTimeCount_215; }
	inline void set_m_fBecomeThornTimeCount_215(float value)
	{
		___m_fBecomeThornTimeCount_215 = value;
	}

	inline static int32_t get_offset_of_m_nBecomeThornStatus_216() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nBecomeThornStatus_216)); }
	inline int32_t get_m_nBecomeThornStatus_216() const { return ___m_nBecomeThornStatus_216; }
	inline int32_t* get_address_of_m_nBecomeThornStatus_216() { return &___m_nBecomeThornStatus_216; }
	inline void set_m_nBecomeThornStatus_216(int32_t value)
	{
		___m_nBecomeThornStatus_216 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldCostMp_217() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fUnfoldCostMp_217)); }
	inline float get_m_fUnfoldCostMp_217() const { return ___m_fUnfoldCostMp_217; }
	inline float* get_address_of_m_fUnfoldCostMp_217() { return &___m_fUnfoldCostMp_217; }
	inline void set_m_fUnfoldCostMp_217(float value)
	{
		___m_fUnfoldCostMp_217 = value;
	}

	inline static int32_t get_offset_of_m_fPreUnfoldTime_218() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPreUnfoldTime_218)); }
	inline float get_m_fPreUnfoldTime_218() const { return ___m_fPreUnfoldTime_218; }
	inline float* get_address_of_m_fPreUnfoldTime_218() { return &___m_fPreUnfoldTime_218; }
	inline void set_m_fPreUnfoldTime_218(float value)
	{
		___m_fPreUnfoldTime_218 = value;
	}

	inline static int32_t get_offset_of_m_fPreUnfoldCurTimeLapse_219() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPreUnfoldCurTimeLapse_219)); }
	inline float get_m_fPreUnfoldCurTimeLapse_219() const { return ___m_fPreUnfoldCurTimeLapse_219; }
	inline float* get_address_of_m_fPreUnfoldCurTimeLapse_219() { return &___m_fPreUnfoldCurTimeLapse_219; }
	inline void set_m_fPreUnfoldCurTimeLapse_219(float value)
	{
		___m_fPreUnfoldCurTimeLapse_219 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldScale_220() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fUnfoldScale_220)); }
	inline float get_m_fUnfoldScale_220() const { return ___m_fUnfoldScale_220; }
	inline float* get_address_of_m_fUnfoldScale_220() { return &___m_fUnfoldScale_220; }
	inline void set_m_fUnfoldScale_220(float value)
	{
		___m_fUnfoldScale_220 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldKeepTime_221() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fUnfoldKeepTime_221)); }
	inline float get_m_fUnfoldKeepTime_221() const { return ___m_fUnfoldKeepTime_221; }
	inline float* get_address_of_m_fUnfoldKeepTime_221() { return &___m_fUnfoldKeepTime_221; }
	inline void set_m_fUnfoldKeepTime_221(float value)
	{
		___m_fUnfoldKeepTime_221 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldCurTimeLapse_222() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fUnfoldCurTimeLapse_222)); }
	inline float get_m_fUnfoldCurTimeLapse_222() const { return ___m_fUnfoldCurTimeLapse_222; }
	inline float* get_address_of_m_fUnfoldCurTimeLapse_222() { return &___m_fUnfoldCurTimeLapse_222; }
	inline void set_m_fUnfoldCurTimeLapse_222(float value)
	{
		___m_fUnfoldCurTimeLapse_222 = value;
	}

	inline static int32_t get_offset_of_m_nSkillUnfoldStatus_223() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSkillUnfoldStatus_223)); }
	inline int32_t get_m_nSkillUnfoldStatus_223() const { return ___m_nSkillUnfoldStatus_223; }
	inline int32_t* get_address_of_m_nSkillUnfoldStatus_223() { return &___m_nSkillUnfoldStatus_223; }
	inline void set_m_nSkillUnfoldStatus_223(int32_t value)
	{
		___m_nSkillUnfoldStatus_223 = value;
	}

	inline static int32_t get_offset_of_m_fPreUnfoldTimeElpase_224() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPreUnfoldTimeElpase_224)); }
	inline float get_m_fPreUnfoldTimeElpase_224() const { return ___m_fPreUnfoldTimeElpase_224; }
	inline float* get_address_of_m_fPreUnfoldTimeElpase_224() { return &___m_fPreUnfoldTimeElpase_224; }
	inline void set_m_fPreUnfoldTimeElpase_224(float value)
	{
		___m_fPreUnfoldTimeElpase_224 = value;
	}

	inline static int32_t get_offset_of_m_fPreUnfoldTimeElapse_226() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fPreUnfoldTimeElapse_226)); }
	inline float get_m_fPreUnfoldTimeElapse_226() const { return ___m_fPreUnfoldTimeElapse_226; }
	inline float* get_address_of_m_fPreUnfoldTimeElapse_226() { return &___m_fPreUnfoldTimeElapse_226; }
	inline void set_m_fPreUnfoldTimeElapse_226(float value)
	{
		___m_fPreUnfoldTimeElapse_226 = value;
	}

	inline static int32_t get_offset_of_m_fCurSkillPointAffect_Unfold_227() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fCurSkillPointAffect_Unfold_227)); }
	inline float get_m_fCurSkillPointAffect_Unfold_227() const { return ___m_fCurSkillPointAffect_Unfold_227; }
	inline float* get_address_of_m_fCurSkillPointAffect_Unfold_227() { return &___m_fCurSkillPointAffect_Unfold_227; }
	inline void set_m_fCurSkillPointAffect_Unfold_227(float value)
	{
		___m_fCurSkillPointAffect_Unfold_227 = value;
	}

	inline static int32_t get_offset_of_m_fBaseArea_228() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fBaseArea_228)); }
	inline float get_m_fBaseArea_228() const { return ___m_fBaseArea_228; }
	inline float* get_address_of_m_fBaseArea_228() { return &___m_fBaseArea_228; }
	inline void set_m_fBaseArea_228(float value)
	{
		___m_fBaseArea_228 = value;
	}

	inline static int32_t get_offset_of_m_fTotalArea_229() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fTotalArea_229)); }
	inline float get_m_fTotalArea_229() const { return ___m_fTotalArea_229; }
	inline float* get_address_of_m_fTotalArea_229() { return &___m_fTotalArea_229; }
	inline void set_m_fTotalArea_229(float value)
	{
		___m_fTotalArea_229 = value;
	}

	inline static int32_t get_offset_of_m_nCurLiveBallNum_230() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nCurLiveBallNum_230)); }
	inline int32_t get_m_nCurLiveBallNum_230() const { return ___m_nCurLiveBallNum_230; }
	inline int32_t* get_address_of_m_nCurLiveBallNum_230() { return &___m_nCurLiveBallNum_230; }
	inline void set_m_nCurLiveBallNum_230(int32_t value)
	{
		___m_nCurLiveBallNum_230 = value;
	}

	inline static int32_t get_offset_of_m_fCalcTotalAreaCount_231() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fCalcTotalAreaCount_231)); }
	inline float get_m_fCalcTotalAreaCount_231() const { return ___m_fCalcTotalAreaCount_231; }
	inline float* get_address_of_m_fCalcTotalAreaCount_231() { return &___m_fCalcTotalAreaCount_231; }
	inline void set_m_fCalcTotalAreaCount_231(float value)
	{
		___m_fCalcTotalAreaCount_231 = value;
	}

	inline static int32_t get_offset_of_m_lstBuff_232() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstBuff_232)); }
	inline List_1_t3012865878 * get_m_lstBuff_232() const { return ___m_lstBuff_232; }
	inline List_1_t3012865878 ** get_address_of_m_lstBuff_232() { return &___m_lstBuff_232; }
	inline void set_m_lstBuff_232(List_1_t3012865878 * value)
	{
		___m_lstBuff_232 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBuff_232), value);
	}

	inline static int32_t get_offset_of_m_nPreAddExp_234() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nPreAddExp_234)); }
	inline int32_t get_m_nPreAddExp_234() const { return ___m_nPreAddExp_234; }
	inline int32_t* get_address_of_m_nPreAddExp_234() { return &___m_nPreAddExp_234; }
	inline void set_m_nPreAddExp_234(int32_t value)
	{
		___m_nPreAddExp_234 = value;
	}

	inline static int32_t get_offset_of_m_nLevel_235() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nLevel_235)); }
	inline int32_t get_m_nLevel_235() const { return ___m_nLevel_235; }
	inline int32_t* get_address_of_m_nLevel_235() { return &___m_nLevel_235; }
	inline void set_m_nLevel_235(int32_t value)
	{
		___m_nLevel_235 = value;
	}

	inline static int32_t get_offset_of_m_bCastSkillWhickNeedStopMoving_236() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_bCastSkillWhickNeedStopMoving_236)); }
	inline bool get_m_bCastSkillWhickNeedStopMoving_236() const { return ___m_bCastSkillWhickNeedStopMoving_236; }
	inline bool* get_address_of_m_bCastSkillWhickNeedStopMoving_236() { return &___m_bCastSkillWhickNeedStopMoving_236; }
	inline void set_m_bCastSkillWhickNeedStopMoving_236(bool value)
	{
		___m_bCastSkillWhickNeedStopMoving_236 = value;
	}

	inline static int32_t get_offset_of_m_lstBeingPushedThorn_237() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstBeingPushedThorn_237)); }
	inline List_1_t3413545680 * get_m_lstBeingPushedThorn_237() const { return ___m_lstBeingPushedThorn_237; }
	inline List_1_t3413545680 ** get_address_of_m_lstBeingPushedThorn_237() { return &___m_lstBeingPushedThorn_237; }
	inline void set_m_lstBeingPushedThorn_237(List_1_t3413545680 * value)
	{
		___m_lstBeingPushedThorn_237 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeingPushedThorn_237), value);
	}

	inline static int32_t get_offset_of_m_lstSyncDust_238() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstSyncDust_238)); }
	inline List_1_t198291272 * get_m_lstSyncDust_238() const { return ___m_lstSyncDust_238; }
	inline List_1_t198291272 ** get_address_of_m_lstSyncDust_238() { return &___m_lstSyncDust_238; }
	inline void set_m_lstSyncDust_238(List_1_t198291272 * value)
	{
		___m_lstSyncDust_238 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSyncDust_238), value);
	}

	inline static int32_t get_offset_of_m_dicSyncDust_239() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicSyncDust_239)); }
	inline Dictionary_2_t1909897157 * get_m_dicSyncDust_239() const { return ___m_dicSyncDust_239; }
	inline Dictionary_2_t1909897157 ** get_address_of_m_dicSyncDust_239() { return &___m_dicSyncDust_239; }
	inline void set_m_dicSyncDust_239(Dictionary_2_t1909897157 * value)
	{
		___m_dicSyncDust_239 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSyncDust_239), value);
	}

	inline static int32_t get_offset_of_m_fAttenuateTimeLapse_240() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fAttenuateTimeLapse_240)); }
	inline float get_m_fAttenuateTimeLapse_240() const { return ___m_fAttenuateTimeLapse_240; }
	inline float* get_address_of_m_fAttenuateTimeLapse_240() { return &___m_fAttenuateTimeLapse_240; }
	inline void set_m_fAttenuateTimeLapse_240(float value)
	{
		___m_fAttenuateTimeLapse_240 = value;
	}

	inline static int32_t get_offset_of_m_nAttenuateIndex_242() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nAttenuateIndex_242)); }
	inline int32_t get_m_nAttenuateIndex_242() const { return ___m_nAttenuateIndex_242; }
	inline int32_t* get_address_of_m_nAttenuateIndex_242() { return &___m_nAttenuateIndex_242; }
	inline void set_m_nAttenuateIndex_242(int32_t value)
	{
		___m_nAttenuateIndex_242 = value;
	}

	inline static int32_t get_offset_of_s_aryMat_243() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___s_aryMat_243)); }
	inline MaterialU5BU5D_t561872642* get_s_aryMat_243() const { return ___s_aryMat_243; }
	inline MaterialU5BU5D_t561872642** get_address_of_s_aryMat_243() { return &___s_aryMat_243; }
	inline void set_s_aryMat_243(MaterialU5BU5D_t561872642* value)
	{
		___s_aryMat_243 = value;
		Il2CppCodeGenWriteBarrier((&___s_aryMat_243), value);
	}

	inline static int32_t get_offset_of_m_nSelectedSkillLevel_244() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSelectedSkillLevel_244)); }
	inline int32_t get_m_nSelectedSkillLevel_244() const { return ___m_nSelectedSkillLevel_244; }
	inline int32_t* get_address_of_m_nSelectedSkillLevel_244() { return &___m_nSelectedSkillLevel_244; }
	inline void set_m_nSelectedSkillLevel_244(int32_t value)
	{
		___m_nSelectedSkillLevel_244 = value;
	}

	inline static int32_t get_offset_of_m_nSelectedSkillId_245() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_nSelectedSkillId_245)); }
	inline int32_t get_m_nSelectedSkillId_245() const { return ___m_nSelectedSkillId_245; }
	inline int32_t* get_address_of_m_nSelectedSkillId_245() { return &___m_nSelectedSkillId_245; }
	inline void set_m_nSelectedSkillId_245(int32_t value)
	{
		___m_nSelectedSkillId_245 = value;
	}

	inline static int32_t get_offset_of_m_lstSortedBallList_246() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstSortedBallList_246)); }
	inline List_1_t3678741308 * get_m_lstSortedBallList_246() const { return ___m_lstSortedBallList_246; }
	inline List_1_t3678741308 ** get_address_of_m_lstSortedBallList_246() { return &___m_lstSortedBallList_246; }
	inline void set_m_lstSortedBallList_246(List_1_t3678741308 * value)
	{
		___m_lstSortedBallList_246 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSortedBallList_246), value);
	}

	inline static int32_t get_offset_of_m_lstEatenSpore_247() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstEatenSpore_247)); }
	inline List_1_t1482975291 * get_m_lstEatenSpore_247() const { return ___m_lstEatenSpore_247; }
	inline List_1_t1482975291 ** get_address_of_m_lstEatenSpore_247() { return &___m_lstEatenSpore_247; }
	inline void set_m_lstEatenSpore_247(List_1_t1482975291 * value)
	{
		___m_lstEatenSpore_247 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEatenSpore_247), value);
	}

	inline static int32_t get_offset_of__bytesGestures_248() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____bytesGestures_248)); }
	inline ByteU5BU5D_t4116647657* get__bytesGestures_248() const { return ____bytesGestures_248; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesGestures_248() { return &____bytesGestures_248; }
	inline void set__bytesGestures_248(ByteU5BU5D_t4116647657* value)
	{
		____bytesGestures_248 = value;
		Il2CppCodeGenWriteBarrier((&____bytesGestures_248), value);
	}

	inline static int32_t get_offset_of_m_lstCastingGestures_249() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_lstCastingGestures_249)); }
	inline List_1_t128053199 * get_m_lstCastingGestures_249() const { return ___m_lstCastingGestures_249; }
	inline List_1_t128053199 ** get_address_of_m_lstCastingGestures_249() { return &___m_lstCastingGestures_249; }
	inline void set_m_lstCastingGestures_249(List_1_t128053199 * value)
	{
		___m_lstCastingGestures_249 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstCastingGestures_249), value);
	}

	inline static int32_t get_offset_of_vecGestureDir_250() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___vecGestureDir_250)); }
	inline Vector2_t2156229523  get_vecGestureDir_250() const { return ___vecGestureDir_250; }
	inline Vector2_t2156229523 * get_address_of_vecGestureDir_250() { return &___vecGestureDir_250; }
	inline void set_vecGestureDir_250(Vector2_t2156229523  value)
	{
		___vecGestureDir_250 = value;
	}

	inline static int32_t get_offset_of_m_fSyncPlayerCommonInfoTimeElapse_251() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fSyncPlayerCommonInfoTimeElapse_251)); }
	inline float get_m_fSyncPlayerCommonInfoTimeElapse_251() const { return ___m_fSyncPlayerCommonInfoTimeElapse_251; }
	inline float* get_address_of_m_fSyncPlayerCommonInfoTimeElapse_251() { return &___m_fSyncPlayerCommonInfoTimeElapse_251; }
	inline void set_m_fSyncPlayerCommonInfoTimeElapse_251(float value)
	{
		___m_fSyncPlayerCommonInfoTimeElapse_251 = value;
	}

	inline static int32_t get_offset_of__bytesCommonInfo_252() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____bytesCommonInfo_252)); }
	inline ByteU5BU5D_t4116647657* get__bytesCommonInfo_252() const { return ____bytesCommonInfo_252; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesCommonInfo_252() { return &____bytesCommonInfo_252; }
	inline void set__bytesCommonInfo_252(ByteU5BU5D_t4116647657* value)
	{
		____bytesCommonInfo_252 = value;
		Il2CppCodeGenWriteBarrier((&____bytesCommonInfo_252), value);
	}

	inline static int32_t get_offset_of_m_fDelayTestTimeElapse_254() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_fDelayTestTimeElapse_254)); }
	inline float get_m_fDelayTestTimeElapse_254() const { return ___m_fDelayTestTimeElapse_254; }
	inline float* get_address_of_m_fDelayTestTimeElapse_254() { return &___m_fDelayTestTimeElapse_254; }
	inline void set_m_fDelayTestTimeElapse_254(float value)
	{
		___m_fDelayTestTimeElapse_254 = value;
	}

	inline static int32_t get_offset_of_m_dicDelayTest_PingTime_255() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___m_dicDelayTest_PingTime_255)); }
	inline Dictionary_2_t285980105 * get_m_dicDelayTest_PingTime_255() const { return ___m_dicDelayTest_PingTime_255; }
	inline Dictionary_2_t285980105 ** get_address_of_m_dicDelayTest_PingTime_255() { return &___m_dicDelayTest_PingTime_255; }
	inline void set_m_dicDelayTest_PingTime_255(Dictionary_2_t285980105 * value)
	{
		___m_dicDelayTest_PingTime_255 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicDelayTest_PingTime_255), value);
	}
};

struct Player_t3266647312_StaticFields
{
public:
	// UnityEngine.Vector3 Player::vecTempPos
	Vector3_t3722313464  ___vecTempPos_14;
	// UnityEngine.Vector3 Player::vecTempSize
	Vector3_t3722313464  ___vecTempSize_15;
	// UnityEngine.Vector3 Player::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_16;
	// UnityEngine.Vector2 Player::vec2Temp
	Vector2_t2156229523  ___vec2Temp_17;
	// UnityEngine.Color Player::colorTemp
	Color_t2555686324  ___colorTemp_18;
	// UnityEngine.Vector2 Player::vecTempDir
	Vector2_t2156229523  ___vecTempDir_36;
	// System.Byte[] Player::_bytesPublicInfo
	ByteU5BU5D_t4116647657* ____bytesPublicInfo_51;
	// System.Byte[] Player::_bytesFullInfo
	ByteU5BU5D_t4116647657* ____bytesFullInfo_54;
	// UnityEngine.Vector2 Player::vec2TempPos
	Vector2_t2156229523  ___vec2TempPos_62;
	// UnityEngine.Vector3 Player::vec3TempPos
	Vector3_t3722313464  ___vec3TempPos_63;
	// System.Byte[] Player::_bytesSpitBall
	ByteU5BU5D_t4116647657* ____bytesSpitBall_79;
	// System.Collections.Generic.List`1<CMiaoHeEffect> Player::s_lstMiaoHeEffect
	List_1_t2306155490 * ___s_lstMiaoHeEffect_94;
	// System.Byte[] Player::_bytesSplit
	ByteU5BU5D_t4116647657* ____bytesSplit_101;
	// System.Byte[] Player::_bytesSyncSporeId
	ByteU5BU5D_t4116647657* ____bytesSyncSporeId_120;
	// System.Byte[] Player::_bytesAdjustMoveInfo
	ByteU5BU5D_t4116647657* ____bytesAdjustMoveInfo_141;
	// System.Byte[] Player::_bytesSyncMoveInfo
	ByteU5BU5D_t4116647657* ____bytesSyncMoveInfo_145;
	// System.Byte[] Player::_bytesPaiHangBang
	ByteU5BU5D_t4116647657* ____bytesPaiHangBang_159;
	// System.Byte[] Player::_bytesEatThornSync
	ByteU5BU5D_t4116647657* ____bytesEatThornSync_164;
	// System.Byte[] Player::_bytesExplodeBall
	ByteU5BU5D_t4116647657* ____bytesExplodeBall_170;
	// System.Int64 Player::m_lBuffGUID
	int64_t ___m_lBuffGUID_233;
	// System.Int32 Player::s_nDelayTestId
	int32_t ___s_nDelayTestId_253;

public:
	inline static int32_t get_offset_of_vecTempPos_14() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vecTempPos_14)); }
	inline Vector3_t3722313464  get_vecTempPos_14() const { return ___vecTempPos_14; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_14() { return &___vecTempPos_14; }
	inline void set_vecTempPos_14(Vector3_t3722313464  value)
	{
		___vecTempPos_14 = value;
	}

	inline static int32_t get_offset_of_vecTempSize_15() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vecTempSize_15)); }
	inline Vector3_t3722313464  get_vecTempSize_15() const { return ___vecTempSize_15; }
	inline Vector3_t3722313464 * get_address_of_vecTempSize_15() { return &___vecTempSize_15; }
	inline void set_vecTempSize_15(Vector3_t3722313464  value)
	{
		___vecTempSize_15 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_16() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vecTempPos1_16)); }
	inline Vector3_t3722313464  get_vecTempPos1_16() const { return ___vecTempPos1_16; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_16() { return &___vecTempPos1_16; }
	inline void set_vecTempPos1_16(Vector3_t3722313464  value)
	{
		___vecTempPos1_16 = value;
	}

	inline static int32_t get_offset_of_vec2Temp_17() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vec2Temp_17)); }
	inline Vector2_t2156229523  get_vec2Temp_17() const { return ___vec2Temp_17; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_17() { return &___vec2Temp_17; }
	inline void set_vec2Temp_17(Vector2_t2156229523  value)
	{
		___vec2Temp_17 = value;
	}

	inline static int32_t get_offset_of_colorTemp_18() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___colorTemp_18)); }
	inline Color_t2555686324  get_colorTemp_18() const { return ___colorTemp_18; }
	inline Color_t2555686324 * get_address_of_colorTemp_18() { return &___colorTemp_18; }
	inline void set_colorTemp_18(Color_t2555686324  value)
	{
		___colorTemp_18 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_36() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vecTempDir_36)); }
	inline Vector2_t2156229523  get_vecTempDir_36() const { return ___vecTempDir_36; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_36() { return &___vecTempDir_36; }
	inline void set_vecTempDir_36(Vector2_t2156229523  value)
	{
		___vecTempDir_36 = value;
	}

	inline static int32_t get_offset_of__bytesPublicInfo_51() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesPublicInfo_51)); }
	inline ByteU5BU5D_t4116647657* get__bytesPublicInfo_51() const { return ____bytesPublicInfo_51; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesPublicInfo_51() { return &____bytesPublicInfo_51; }
	inline void set__bytesPublicInfo_51(ByteU5BU5D_t4116647657* value)
	{
		____bytesPublicInfo_51 = value;
		Il2CppCodeGenWriteBarrier((&____bytesPublicInfo_51), value);
	}

	inline static int32_t get_offset_of__bytesFullInfo_54() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesFullInfo_54)); }
	inline ByteU5BU5D_t4116647657* get__bytesFullInfo_54() const { return ____bytesFullInfo_54; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesFullInfo_54() { return &____bytesFullInfo_54; }
	inline void set__bytesFullInfo_54(ByteU5BU5D_t4116647657* value)
	{
		____bytesFullInfo_54 = value;
		Il2CppCodeGenWriteBarrier((&____bytesFullInfo_54), value);
	}

	inline static int32_t get_offset_of_vec2TempPos_62() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vec2TempPos_62)); }
	inline Vector2_t2156229523  get_vec2TempPos_62() const { return ___vec2TempPos_62; }
	inline Vector2_t2156229523 * get_address_of_vec2TempPos_62() { return &___vec2TempPos_62; }
	inline void set_vec2TempPos_62(Vector2_t2156229523  value)
	{
		___vec2TempPos_62 = value;
	}

	inline static int32_t get_offset_of_vec3TempPos_63() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___vec3TempPos_63)); }
	inline Vector3_t3722313464  get_vec3TempPos_63() const { return ___vec3TempPos_63; }
	inline Vector3_t3722313464 * get_address_of_vec3TempPos_63() { return &___vec3TempPos_63; }
	inline void set_vec3TempPos_63(Vector3_t3722313464  value)
	{
		___vec3TempPos_63 = value;
	}

	inline static int32_t get_offset_of__bytesSpitBall_79() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesSpitBall_79)); }
	inline ByteU5BU5D_t4116647657* get__bytesSpitBall_79() const { return ____bytesSpitBall_79; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesSpitBall_79() { return &____bytesSpitBall_79; }
	inline void set__bytesSpitBall_79(ByteU5BU5D_t4116647657* value)
	{
		____bytesSpitBall_79 = value;
		Il2CppCodeGenWriteBarrier((&____bytesSpitBall_79), value);
	}

	inline static int32_t get_offset_of_s_lstMiaoHeEffect_94() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___s_lstMiaoHeEffect_94)); }
	inline List_1_t2306155490 * get_s_lstMiaoHeEffect_94() const { return ___s_lstMiaoHeEffect_94; }
	inline List_1_t2306155490 ** get_address_of_s_lstMiaoHeEffect_94() { return &___s_lstMiaoHeEffect_94; }
	inline void set_s_lstMiaoHeEffect_94(List_1_t2306155490 * value)
	{
		___s_lstMiaoHeEffect_94 = value;
		Il2CppCodeGenWriteBarrier((&___s_lstMiaoHeEffect_94), value);
	}

	inline static int32_t get_offset_of__bytesSplit_101() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesSplit_101)); }
	inline ByteU5BU5D_t4116647657* get__bytesSplit_101() const { return ____bytesSplit_101; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesSplit_101() { return &____bytesSplit_101; }
	inline void set__bytesSplit_101(ByteU5BU5D_t4116647657* value)
	{
		____bytesSplit_101 = value;
		Il2CppCodeGenWriteBarrier((&____bytesSplit_101), value);
	}

	inline static int32_t get_offset_of__bytesSyncSporeId_120() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesSyncSporeId_120)); }
	inline ByteU5BU5D_t4116647657* get__bytesSyncSporeId_120() const { return ____bytesSyncSporeId_120; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesSyncSporeId_120() { return &____bytesSyncSporeId_120; }
	inline void set__bytesSyncSporeId_120(ByteU5BU5D_t4116647657* value)
	{
		____bytesSyncSporeId_120 = value;
		Il2CppCodeGenWriteBarrier((&____bytesSyncSporeId_120), value);
	}

	inline static int32_t get_offset_of__bytesAdjustMoveInfo_141() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesAdjustMoveInfo_141)); }
	inline ByteU5BU5D_t4116647657* get__bytesAdjustMoveInfo_141() const { return ____bytesAdjustMoveInfo_141; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesAdjustMoveInfo_141() { return &____bytesAdjustMoveInfo_141; }
	inline void set__bytesAdjustMoveInfo_141(ByteU5BU5D_t4116647657* value)
	{
		____bytesAdjustMoveInfo_141 = value;
		Il2CppCodeGenWriteBarrier((&____bytesAdjustMoveInfo_141), value);
	}

	inline static int32_t get_offset_of__bytesSyncMoveInfo_145() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesSyncMoveInfo_145)); }
	inline ByteU5BU5D_t4116647657* get__bytesSyncMoveInfo_145() const { return ____bytesSyncMoveInfo_145; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesSyncMoveInfo_145() { return &____bytesSyncMoveInfo_145; }
	inline void set__bytesSyncMoveInfo_145(ByteU5BU5D_t4116647657* value)
	{
		____bytesSyncMoveInfo_145 = value;
		Il2CppCodeGenWriteBarrier((&____bytesSyncMoveInfo_145), value);
	}

	inline static int32_t get_offset_of__bytesPaiHangBang_159() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesPaiHangBang_159)); }
	inline ByteU5BU5D_t4116647657* get__bytesPaiHangBang_159() const { return ____bytesPaiHangBang_159; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesPaiHangBang_159() { return &____bytesPaiHangBang_159; }
	inline void set__bytesPaiHangBang_159(ByteU5BU5D_t4116647657* value)
	{
		____bytesPaiHangBang_159 = value;
		Il2CppCodeGenWriteBarrier((&____bytesPaiHangBang_159), value);
	}

	inline static int32_t get_offset_of__bytesEatThornSync_164() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesEatThornSync_164)); }
	inline ByteU5BU5D_t4116647657* get__bytesEatThornSync_164() const { return ____bytesEatThornSync_164; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesEatThornSync_164() { return &____bytesEatThornSync_164; }
	inline void set__bytesEatThornSync_164(ByteU5BU5D_t4116647657* value)
	{
		____bytesEatThornSync_164 = value;
		Il2CppCodeGenWriteBarrier((&____bytesEatThornSync_164), value);
	}

	inline static int32_t get_offset_of__bytesExplodeBall_170() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____bytesExplodeBall_170)); }
	inline ByteU5BU5D_t4116647657* get__bytesExplodeBall_170() const { return ____bytesExplodeBall_170; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesExplodeBall_170() { return &____bytesExplodeBall_170; }
	inline void set__bytesExplodeBall_170(ByteU5BU5D_t4116647657* value)
	{
		____bytesExplodeBall_170 = value;
		Il2CppCodeGenWriteBarrier((&____bytesExplodeBall_170), value);
	}

	inline static int32_t get_offset_of_m_lBuffGUID_233() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___m_lBuffGUID_233)); }
	inline int64_t get_m_lBuffGUID_233() const { return ___m_lBuffGUID_233; }
	inline int64_t* get_address_of_m_lBuffGUID_233() { return &___m_lBuffGUID_233; }
	inline void set_m_lBuffGUID_233(int64_t value)
	{
		___m_lBuffGUID_233 = value;
	}

	inline static int32_t get_offset_of_s_nDelayTestId_253() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___s_nDelayTestId_253)); }
	inline int32_t get_s_nDelayTestId_253() const { return ___s_nDelayTestId_253; }
	inline int32_t* get_address_of_s_nDelayTestId_253() { return &___s_nDelayTestId_253; }
	inline void set_s_nDelayTestId_253(int32_t value)
	{
		___s_nDelayTestId_253 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T3266647312_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (TurnExtensions_t3150044944), -1, sizeof(TurnExtensions_t3150044944_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3100[3] = 
{
	TurnExtensions_t3150044944_StaticFields::get_offset_of_TurnPropKey_0(),
	TurnExtensions_t3150044944_StaticFields::get_offset_of_TurnStartPropKey_1(),
	TurnExtensions_t3150044944_StaticFields::get_offset_of_FinishedTurnPropKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (QuitOnEscapeOrBack_t3171497686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (ServerTime_t783080118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (ShowInfoOfPlayer_t601303432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[5] = 
{
	ShowInfoOfPlayer_t601303432::get_offset_of_textGo_3(),
	ShowInfoOfPlayer_t601303432::get_offset_of_tm_4(),
	ShowInfoOfPlayer_t601303432::get_offset_of_CharacterSize_5(),
	ShowInfoOfPlayer_t601303432::get_offset_of_font_6(),
	ShowInfoOfPlayer_t601303432::get_offset_of_DisableOnOwnObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (ShowStatusWhenConnecting_t1063567576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[1] = 
{
	ShowStatusWhenConnecting_t1063567576::get_offset_of_Skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (SmoothSyncMovement_t1809568931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[3] = 
{
	SmoothSyncMovement_t1809568931::get_offset_of_SmoothingDelay_3(),
	SmoothSyncMovement_t1809568931::get_offset_of_correctPlayerPos_4(),
	SmoothSyncMovement_t1809568931::get_offset_of_correctPlayerRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (SupportLogger_t2840230211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[1] = 
{
	SupportLogger_t2840230211::get_offset_of_LogTrafficStats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (SupportLogging_t3610999087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3107[1] = 
{
	SupportLogging_t3610999087::get_offset_of_LogTrafficStats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (TimeKeeper_t3694205465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[4] = 
{
	TimeKeeper_t3694205465::get_offset_of_lastExecutionTime_0(),
	TimeKeeper_t3694205465::get_offset_of_shouldExecute_1(),
	TimeKeeper_t3694205465::get_offset_of_U3CIntervalU3Ek__BackingField_2(),
	TimeKeeper_t3694205465::get_offset_of_U3CIsEnabledU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (ButtonInsideScrollList_t657350886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[1] = 
{
	ButtonInsideScrollList_t657350886::get_offset_of_scrollRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (TextButtonTransition_t3897064113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[3] = 
{
	TextButtonTransition_t3897064113::get_offset_of__text_2(),
	TextButtonTransition_t3897064113::get_offset_of_NormalColor_3(),
	TextButtonTransition_t3897064113::get_offset_of_HoverColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (TextToggleIsOnTransition_t1534099090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[7] = 
{
	TextToggleIsOnTransition_t1534099090::get_offset_of_toggle_2(),
	TextToggleIsOnTransition_t1534099090::get_offset_of__text_3(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_NormalOnColor_4(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_NormalOffColor_5(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_HoverOnColor_6(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_HoverOffColor_7(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_isHover_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (Player_t3266647312), -1, sizeof(Player_t3266647312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3112[253] = 
{
	Player_t3266647312::get_offset_of_m_colorMe_3(),
	Player_t3266647312::get_offset_of_m_nSkinId_4(),
	Player_t3266647312::get_offset_of__isRebornProtected_5(),
	Player_t3266647312::get_offset_of__color_inner_6(),
	Player_t3266647312::get_offset_of__color_ring_7(),
	Player_t3266647312::get_offset_of__color_poison_8(),
	Player_t3266647312::get_offset_of_m_fProtectTime_9(),
	Player_t3266647312::get_offset_of_m_lstBalls_10(),
	Player_t3266647312::get_offset_of_m_dicActiveBalls_11(),
	Player_t3266647312::get_offset_of_m_nBallIndex_12(),
	Player_t3266647312::get_offset_of_m_vecPos_13(),
	Player_t3266647312_StaticFields::get_offset_of_vecTempPos_14(),
	Player_t3266647312_StaticFields::get_offset_of_vecTempSize_15(),
	Player_t3266647312_StaticFields::get_offset_of_vecTempPos1_16(),
	Player_t3266647312_StaticFields::get_offset_of_vec2Temp_17(),
	Player_t3266647312_StaticFields::get_offset_of_colorTemp_18(),
	Player_t3266647312::get_offset_of_m_SkillManager_19(),
	Player_t3266647312::get_offset_of__PlayerIns_20(),
	Player_t3266647312::get_offset_of_m_fTotalEatArea_21(),
	Player_t3266647312::get_offset_of_m_fTotalEatFoodArea_22(),
	Player_t3266647312::get_offset_of_m_nKillCount_23(),
	Player_t3266647312::get_offset_of_m_nBeKilled_24(),
	Player_t3266647312::get_offset_of_m_IncAssistAttackCount_25(),
	Player_t3266647312::get_offset_of_m_nEatThornNum_26(),
	Player_t3266647312::get_offset_of_m_nContinuousKillNum_27(),
	Player_t3266647312::get_offset_of_m_dicDuoShaoRecord_28(),
	Player_t3266647312::get_offset_of_m_nDuoSha_29(),
	Player_t3266647312::get_offset_of_m_aryItemNum_30(),
	Player_t3266647312::get_offset_of_m_BaseData_31(),
	Player_t3266647312::get_offset_of_m_fBaseVolumeByLevel_32(),
	0,
	Player_t3266647312::get_offset_of_m_fProcessSelfCount_34(),
	Player_t3266647312::get_offset_of_m_fLoop_1_Second_TimeElapse_35(),
	Player_t3266647312_StaticFields::get_offset_of_vecTempDir_36(),
	Player_t3266647312::get_offset_of_m_bQueryingFromMasterClientDataReceived_37(),
	Player_t3266647312::get_offset_of_m_fQueryingFromMasterClientTimeElapse_38(),
	Player_t3266647312::get_offset_of_m_bQueryingFromMainPlayerDataReceived_39(),
	Player_t3266647312::get_offset_of_m_fQueryingFromMainPlayerTimeElapse_40(),
	Player_t3266647312::get_offset_of_m_bOrphanBallsDataReceived_41(),
	Player_t3266647312::get_offset_of_m_bIniting_42(),
	Player_t3266647312::get_offset_of_m_bInitCompleted_43(),
	Player_t3266647312::get_offset_of_m_bBallInstantiated_44(),
	0,
	Player_t3266647312::get_offset_of_m_fInitingBallsTimeCount_46(),
	Player_t3266647312::get_offset_of_m_nKingExplodeIndex_47(),
	Player_t3266647312::get_offset_of_m_bKingExploding_48(),
	Player_t3266647312::get_offset_of_m_szPlayerName_49(),
	Player_t3266647312::get_offset_of_m_szAccount_50(),
	Player_t3266647312_StaticFields::get_offset_of__bytesPublicInfo_51(),
	Player_t3266647312::get_offset_of_m_bPublicInfoInited_52(),
	Player_t3266647312::get_offset_of_m_nPlayerPlatform_53(),
	Player_t3266647312_StaticFields::get_offset_of__bytesFullInfo_54(),
	Player_t3266647312::get_offset_of_m_bObserve_55(),
	Player_t3266647312::get_offset_of_m_vWorldCursorPos_56(),
	Player_t3266647312::get_offset_of_m_vSpecialDirecton_57(),
	Player_t3266647312::get_offset_of_m_fRealTimeSpeedWithoutSize_58(),
	Player_t3266647312::get_offset_of_m_fSyncMoveInfoCount_59(),
	Player_t3266647312::get_offset_of_m_fBaseSpeed_60(),
	Player_t3266647312::get_offset_of_m_fShellTimeBuffEffect_61(),
	Player_t3266647312_StaticFields::get_offset_of_vec2TempPos_62(),
	Player_t3266647312_StaticFields::get_offset_of_vec3TempPos_63(),
	Player_t3266647312::get_offset_of_m_bTestMove_64(),
	Player_t3266647312::get_offset_of_m_bMoving_65(),
	Player_t3266647312::get_offset_of_m_fClearSpitTargetTotalTime_66(),
	Player_t3266647312::get_offset_of_m_nSpitIndex_67(),
	Player_t3266647312::get_offset_of_m_bSpitting_68(),
	Player_t3266647312::get_offset_of_m_fSpitBallHalfMpCost_69(),
	Player_t3266647312::get_offset_of_m_fSpitBallHalfColddown_70(),
	Player_t3266647312::get_offset_of_m_fSpitBallHalfQianYao_71(),
	Player_t3266647312::get_offset_of_m_fSpitBallHalfDistance_72(),
	Player_t3266647312::get_offset_of_m_fSpitBallHalfRunTime_73(),
	Player_t3266647312::get_offset_of_m_fSpitBallHalfShellTime_74(),
	Player_t3266647312::get_offset_of_m_nPreCastSkill_Status_SpitHalf_75(),
	Player_t3266647312::get_offset_of_m_bPreCastSkill_SpitHalf_76(),
	Player_t3266647312::get_offset_of_m_fPreCastSkill_SpitHalf_TimeCount_77(),
	Player_t3266647312::get_offset_of_m_nWSpitStatus_78(),
	Player_t3266647312_StaticFields::get_offset_of__bytesSpitBall_79(),
	Player_t3266647312::get_offset_of_m_lstTempEjectEndPos_80(),
	Player_t3266647312::get_offset_of_m_fSpitBallCostMp_81(),
	Player_t3266647312::get_offset_of_m_fSpitBallDistance_82(),
	Player_t3266647312::get_offset_of_m_fSpitBallRunTime_83(),
	Player_t3266647312::get_offset_of_m_fSpitBallShellTime_84(),
	Player_t3266647312::get_offset_of_m_fForceSpitTotalTime_85(),
	Player_t3266647312::get_offset_of_m_fForcing_86(),
	Player_t3266647312::get_offset_of_m_fForceSpitCurTime_87(),
	Player_t3266647312::get_offset_of_m_fSpitPercent_88(),
	Player_t3266647312::get_offset_of_m_aryGunsight_89(),
	Player_t3266647312::get_offset_of_m_fPreDrawCount_90(),
	Player_t3266647312::get_offset_of_m_vecBalslsCenter_91(),
	Player_t3266647312::get_offset_of_m_fDistanceToSlowDown_92(),
	Player_t3266647312::get_offset_of_m_lstToMergeAllBalls_93(),
	Player_t3266647312_StaticFields::get_offset_of_s_lstMiaoHeEffect_94(),
	Player_t3266647312::get_offset_of_m_lstMiaoHeEffect_95(),
	Player_t3266647312::get_offset_of_m_lstRecycledSpiltInfo_96(),
	Player_t3266647312::get_offset_of_m_bSplitting_97(),
	Player_t3266647312::get_offset_of_m_nCurSplitTimes_98(),
	Player_t3266647312::get_offset_of_m_nRealSplitNum_99(),
	Player_t3266647312::get_offset_of_m_lstSplitBalls_100(),
	Player_t3266647312_StaticFields::get_offset_of__bytesSplit_101(),
	Player_t3266647312::get_offset_of_m_lstToExplodeBalls_102(),
	Player_t3266647312::get_offset_of_m_fUpdateBuffCount_103(),
	0,
	Player_t3266647312::get_offset_of_m_szSpitSporeMonsterId_105(),
	Player_t3266647312::get_offset_of_m_fSpitSporeDis_106(),
	Player_t3266647312::get_offset_of_m_fSpitSporeTimeInterval_107(),
	Player_t3266647312::get_offset_of_m_fSpitSporeMpCost_108(),
	Player_t3266647312::get_offset_of_m_bSpittingSpore_109(),
	Player_t3266647312::get_offset_of_m_fSporeEjectSpeed_110(),
	Player_t3266647312::get_offset_of_m_nCurSporeLevel_111(),
	Player_t3266647312::get_offset_of_m_lstSporeToSpit_112(),
	Player_t3266647312::get_offset_of_lstSporeToSpitTemp_113(),
	Player_t3266647312::get_offset_of_m_fSpitSporeInterval_114(),
	Player_t3266647312::get_offset_of_m_fSpitSporeDistance_115(),
	Player_t3266647312::get_offset_of_m_fSpitSporeTimeCount_116(),
	Player_t3266647312::get_offset_of_m_fGenerateSporeIdTimeCount_117(),
	Player_t3266647312::get_offset_of_m_nSporeNumPerSec_118(),
	Player_t3266647312::get_offset_of_m_uSporeGuid_119(),
	Player_t3266647312_StaticFields::get_offset_of__bytesSyncSporeId_120(),
	Player_t3266647312::get_offset_of_m_lstSporeToSync_121(),
	Player_t3266647312::get_offset_of_m_uSporeIdCount_122(),
	Player_t3266647312::get_offset_of_m_fSpitSporeBlingCount_123(),
	Player_t3266647312::get_offset_of__bytesSpore_124(),
	Player_t3266647312::get_offset_of_m_nAttenuateTime_125(),
	Player_t3266647312::get_offset_of_m_lstLiveBall_126(),
	Player_t3266647312::get_offset_of_m_nBallNum_127(),
	Player_t3266647312::get_offset_of_m_lstSomeAvailabelBallIndex_128(),
	Player_t3266647312::get_offset_of_m_lstRecycledBalls_129(),
	Player_t3266647312::get_offset_of_m_bDead_130(),
	Player_t3266647312::get_offset_of_m_nEaterId_131(),
	Player_t3266647312::get_offset_of_m_bReborn_132(),
	Player_t3266647312::get_offset_of_m_bAllBallsMovingToCenter_133(),
	Player_t3266647312::get_offset_of_m_ballBiggest_134(),
	Player_t3266647312::get_offset_of_m_nTestExplodeStatus_135(),
	Player_t3266647312::get_offset_of_m_bTestPlayer_136(),
	Player_t3266647312::get_offset_of_m_vecSpecailDirection_137(),
	Player_t3266647312::get_offset_of_m_nAdjustMoveInfoFrameCount_138(),
	0,
	0,
	Player_t3266647312_StaticFields::get_offset_of__bytesAdjustMoveInfo_141(),
	Player_t3266647312::get_offset_of__bytestRelatedInfo_142(),
	0,
	Player_t3266647312::get_offset_of_m_nSyncMoveInfoTimeCount_144(),
	Player_t3266647312_StaticFields::get_offset_of__bytesSyncMoveInfo_145(),
	Player_t3266647312::get_offset_of_m_dicGunSight_146(),
	Player_t3266647312::get_offset_of_m_dicNotPickedGunSight_147(),
	Player_t3266647312::get_offset_of_m_bFirstReceive_148(),
	Player_t3266647312::get_offset_of_m_fReceiveTimeCount_149(),
	Player_t3266647312::get_offset_of_m_fReceiveTotalTime_150(),
	Player_t3266647312::get_offset_of_m_fLastTimeElapse_151(),
	Player_t3266647312::get_offset_of_m_fDelay_152(),
	Player_t3266647312::get_offset_of_m_fTotalDelay_153(),
	Player_t3266647312::get_offset_of_m_nDelayCount_154(),
	Player_t3266647312::get_offset_of_m_fLastReceiveTimeElapse_155(),
	Player_t3266647312::get_offset_of__bytesDusts_156(),
	0,
	Player_t3266647312::get_offset_of_m_fSyncDustTimeCount_158(),
	Player_t3266647312_StaticFields::get_offset_of__bytesPaiHangBang_159(),
	Player_t3266647312::get_offset_of_m_fPaiHangBangSyncTimeCount_160(),
	Player_t3266647312::get_offset_of_m_lstEatBeanSyncQueue_161(),
	Player_t3266647312::get_offset_of_m_lstEatThornSyncQueue_162(),
	Player_t3266647312::get_offset_of_m_lstEatSporeSyncQueue_163(),
	Player_t3266647312_StaticFields::get_offset_of__bytesEatThornSync_164(),
	Player_t3266647312::get_offset_of_m_fEatThornSyncCount_165(),
	Player_t3266647312::get_offset_of_m_dicAssistAttack_166(),
	Player_t3266647312::get_offset_of_m_lstValidAssitPlayerId_167(),
	Player_t3266647312::get_offset_of_m_fExplodeChildNumBuffEffect_168(),
	Player_t3266647312::get_offset_of_m_fExplodeMotherLeftBuffEffect_169(),
	Player_t3266647312_StaticFields::get_offset_of__bytesExplodeBall_170(),
	Player_t3266647312::get_offset_of_m_lstExplodingNode_171(),
	Player_t3266647312::get_offset_of_m_lstRecycledExplodeNode_172(),
	Player_t3266647312::get_offset_of_m_nCurSkinIndex_173(),
	Player_t3266647312::get_offset_of_m_sprCurAvatar_174(),
	Player_t3266647312::get_offset_of_m_bMyDataLoaded_175(),
	Player_t3266647312::get_offset_of_m_SyncPosAfterEjectTotalTime_176(),
	Player_t3266647312::get_offset_of_m_fSkillMpCost_MagicShield_177(),
	Player_t3266647312::get_offset_of_m_fSkillDuration_MagicShield_178(),
	Player_t3266647312::get_offset_of_m_bMagicShielding_179(),
	Player_t3266647312::get_offset_of_m_fSkillSpeedAffect_MagicShield_180(),
	Player_t3266647312::get_offset_of_m_fSkillLoopCount_MagicShield_181(),
	Player_t3266647312::get_offset_of_m_fSkillMpCost_Annihilate_182(),
	Player_t3266647312::get_offset_of_m_fSkillPercent_Annihilate_183(),
	Player_t3266647312::get_offset_of_m_fSkillDuration_Annihilate_184(),
	Player_t3266647312::get_offset_of_m_fSkillSpeedAffect_Annihilate_185(),
	Player_t3266647312::get_offset_of_m_bAnnihilating_186(),
	Player_t3266647312::get_offset_of_m_fSkillLoopCount_Annihilate_187(),
	Player_t3266647312::get_offset_of_m_fSkillMpCost_Gold_188(),
	Player_t3266647312::get_offset_of_m_fSkillGold_SpeedAddPercent_189(),
	Player_t3266647312::get_offset_of_m_fGold_Duration_190(),
	Player_t3266647312::get_offset_of_m_bGold_191(),
	Player_t3266647312::get_offset_of_m_fGoldLoopCount_192(),
	Player_t3266647312::get_offset_of_m_fSkillMpCost_Henzy_193(),
	Player_t3266647312::get_offset_of_m_fHenzy_Duration_194(),
	Player_t3266647312::get_offset_of_m_fHenzy_SpeedChangePercent_195(),
	Player_t3266647312::get_offset_of_m_fHenzy_WColdDown_196(),
	Player_t3266647312::get_offset_of_m_fHenzy_EColdDown_197(),
	Player_t3266647312::get_offset_of_m_bHenzy_198(),
	Player_t3266647312::get_offset_of_m_fHenzyLoopCount_199(),
	Player_t3266647312::get_offset_of_m_fSkillMpCost_MergeAll_200(),
	Player_t3266647312::get_offset_of_m_fSkillQianYao_MergeAll_201(),
	Player_t3266647312::get_offset_of_m_bMiaoHeQianYao_202(),
	Player_t3266647312::get_offset_of_m_fSkillQianYaoCount_MergeAll_203(),
	Player_t3266647312::get_offset_of_m_fMiaoHeQiaoYaoRotation_204(),
	Player_t3266647312::get_offset_of_m_fSkillMpCost_Sneak_205(),
	Player_t3266647312::get_offset_of_m_fSkillSpeedChangePercent_Sneak_206(),
	Player_t3266647312::get_offset_of_m_fSkillDuration_Sneak_207(),
	Player_t3266647312::get_offset_of_m_bSneaking_208(),
	Player_t3266647312::get_offset_of_m_fSkillDurationCount_Sneak_209(),
	Player_t3266647312::get_offset_of_m_fMpCost_BecomeThorn_210(),
	Player_t3266647312::get_offset_of_m_fQianYao_BecomeThorn_211(),
	Player_t3266647312::get_offset_of_m_fSpeedChangePercent_BecomeThorn_212(),
	Player_t3266647312::get_offset_of_m_fDuration_BecomeThorn_213(),
	Player_t3266647312::get_offset_of_m_szExplodeConfigId_BecomeThorn_214(),
	Player_t3266647312::get_offset_of_m_fBecomeThornTimeCount_215(),
	Player_t3266647312::get_offset_of_m_nBecomeThornStatus_216(),
	Player_t3266647312::get_offset_of_m_fUnfoldCostMp_217(),
	Player_t3266647312::get_offset_of_m_fPreUnfoldTime_218(),
	Player_t3266647312::get_offset_of_m_fPreUnfoldCurTimeLapse_219(),
	Player_t3266647312::get_offset_of_m_fUnfoldScale_220(),
	Player_t3266647312::get_offset_of_m_fUnfoldKeepTime_221(),
	Player_t3266647312::get_offset_of_m_fUnfoldCurTimeLapse_222(),
	Player_t3266647312::get_offset_of_m_nSkillUnfoldStatus_223(),
	Player_t3266647312::get_offset_of_m_fPreUnfoldTimeElpase_224(),
	0,
	Player_t3266647312::get_offset_of_m_fPreUnfoldTimeElapse_226(),
	Player_t3266647312::get_offset_of_m_fCurSkillPointAffect_Unfold_227(),
	Player_t3266647312::get_offset_of_m_fBaseArea_228(),
	Player_t3266647312::get_offset_of_m_fTotalArea_229(),
	Player_t3266647312::get_offset_of_m_nCurLiveBallNum_230(),
	Player_t3266647312::get_offset_of_m_fCalcTotalAreaCount_231(),
	Player_t3266647312::get_offset_of_m_lstBuff_232(),
	Player_t3266647312_StaticFields::get_offset_of_m_lBuffGUID_233(),
	Player_t3266647312::get_offset_of_m_nPreAddExp_234(),
	Player_t3266647312::get_offset_of_m_nLevel_235(),
	Player_t3266647312::get_offset_of_m_bCastSkillWhickNeedStopMoving_236(),
	Player_t3266647312::get_offset_of_m_lstBeingPushedThorn_237(),
	Player_t3266647312::get_offset_of_m_lstSyncDust_238(),
	Player_t3266647312::get_offset_of_m_dicSyncDust_239(),
	Player_t3266647312::get_offset_of_m_fAttenuateTimeLapse_240(),
	0,
	Player_t3266647312::get_offset_of_m_nAttenuateIndex_242(),
	Player_t3266647312::get_offset_of_s_aryMat_243(),
	Player_t3266647312::get_offset_of_m_nSelectedSkillLevel_244(),
	Player_t3266647312::get_offset_of_m_nSelectedSkillId_245(),
	Player_t3266647312::get_offset_of_m_lstSortedBallList_246(),
	Player_t3266647312::get_offset_of_m_lstEatenSpore_247(),
	Player_t3266647312::get_offset_of__bytesGestures_248(),
	Player_t3266647312::get_offset_of_m_lstCastingGestures_249(),
	Player_t3266647312::get_offset_of_vecGestureDir_250(),
	Player_t3266647312::get_offset_of_m_fSyncPlayerCommonInfoTimeElapse_251(),
	Player_t3266647312::get_offset_of__bytesCommonInfo_252(),
	Player_t3266647312_StaticFields::get_offset_of_s_nDelayTestId_253(),
	Player_t3266647312::get_offset_of_m_fDelayTestTimeElapse_254(),
	Player_t3266647312::get_offset_of_m_dicDelayTest_PingTime_255(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (sPlayerBaseData_t1472229795)+ sizeof (RuntimeObject), sizeof(sPlayerBaseData_t1472229795 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3113[5] = 
{
	sPlayerBaseData_t1472229795::get_offset_of_fBaseSpeed_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerBaseData_t1472229795::get_offset_of_fBaseMp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerBaseData_t1472229795::get_offset_of_fBaseSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerBaseData_t1472229795::get_offset_of_fBaseShellTime_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerBaseData_t1472229795::get_offset_of_fBaseSpitDistance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (sSplitInfo_t3300999634)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[9] = 
{
	sSplitInfo_t3300999634::get_offset_of_pos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_dir_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_my_max_split_num_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_my_real_split_num_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_split_couting_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_real_split_size_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_seg_dis_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of_dead_after_split_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSplitInfo_t3300999634::get_offset_of__ball_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (sExplodeInfo_t354989019)+ sizeof (RuntimeObject), sizeof(sExplodeInfo_t354989019 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (Polygon_t12692255), -1, sizeof(Polygon_t12692255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3116[27] = 
{
	Polygon_t12692255::get_offset_of_m_lstPoint_17(),
	Polygon_t12692255::get_offset_of_m_dicLine_18(),
	Polygon_t12692255::get_offset_of_m_dicLineTemp_19(),
	Polygon_t12692255::get_offset_of__collierEdge_20(),
	Polygon_t12692255::get_offset_of__collierPolygon_21(),
	Polygon_t12692255::get_offset_of__colliderSeed_22(),
	Polygon_t12692255::get_offset_of_m_szFileName_23(),
	Polygon_t12692255::get_offset_of_m_bIsGrass_24(),
	Polygon_t12692255::get_offset_of_m_bIsGrassSeed_25(),
	Polygon_t12692255::get_offset_of_m_bNowSeeding_26(),
	Polygon_t12692255::get_offset_of_m_meshGrass_27(),
	Polygon_t12692255::get_offset_of_m_mrGrass_28(),
	Polygon_t12692255::get_offset_of_m_matMain_29(),
	Polygon_t12692255::get_offset_of_m_goSeed_30(),
	Polygon_t12692255::get_offset_of_vecTempPos_31(),
	Polygon_t12692255::get_offset_of_vecTempScale_32(),
	Polygon_t12692255::get_offset_of_m_nOwnerPhotonId_33(),
	Polygon_t12692255::get_offset_of_m_dGrassActivatedTime_34(),
	Polygon_t12692255::get_offset_of_m_nGUID_35(),
	Polygon_t12692255::get_offset_of_m_nHardObstacle_36(),
	Polygon_t12692255_StaticFields::get_offset_of_Vec2Temp_37(),
	Polygon_t12692255::get_offset_of_m_vecCenter_38(),
	Polygon_t12692255::get_offset_of_m_fSeedGrassLifeTime_39(),
	Polygon_t12692255::get_offset_of_m_fCurAlpha_40(),
	Polygon_t12692255::get_offset_of_m_nStatus_41(),
	Polygon_t12692255::get_offset_of_m_fAlphaCounting_42(),
	Polygon_t12692255::get_offset_of_m_fSeedSize_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (PolygonEditor_t2170978208), -1, sizeof(PolygonEditor_t2170978208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3117[22] = 
{
	PolygonEditor_t2170978208::get_offset_of_m_prePolygonPoint_2(),
	PolygonEditor_t2170978208::get_offset_of_m_prePolygonLine_3(),
	PolygonEditor_t2170978208::get_offset_of_m_prePolygon_4(),
	PolygonEditor_t2170978208::get_offset_of_vecTempPos_5(),
	PolygonEditor_t2170978208::get_offset_of_m_CurSelectedPoint_6(),
	PolygonEditor_t2170978208::get_offset_of_the_polygon_7(),
	PolygonEditor_t2170978208::get_offset_of__polygon_8(),
	PolygonEditor_t2170978208::get_offset_of__polygonCollider_9(),
	PolygonEditor_t2170978208::get_offset_of__btnBeginOrEndDrawPath_10(),
	PolygonEditor_t2170978208_StaticFields::get_offset_of_s_nPointGUID_11(),
	PolygonEditor_t2170978208::get_offset_of_m_lstPolygons_12(),
	PolygonEditor_t2170978208::get_offset_of__PolygonList_13(),
	PolygonEditor_t2170978208::get_offset_of_m_MsgBox_14(),
	PolygonEditor_t2170978208::get_offset_of__panelCreateNewPolygon_15(),
	PolygonEditor_t2170978208::get_offset_of__root_16(),
	PolygonEditor_t2170978208::get_offset_of__XmlDoc_17(),
	PolygonEditor_t2170978208::get_offset_of_m_szCurSelectedPolygonName_18(),
	PolygonEditor_t2170978208_StaticFields::get_offset_of_s_Instance_19(),
	PolygonEditor_t2170978208::get_offset_of_m_dicLine_20(),
	PolygonEditor_t2170978208::get_offset_of_m_dicLineTemp_21(),
	PolygonEditor_t2170978208::get_offset_of_m_CurMovingPoint_22(),
	PolygonEditor_t2170978208::get_offset_of_m_eOp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (ePolygonEditOperation_t330424546)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3118[5] = 
{
	ePolygonEditOperation_t330424546::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (U3CLoadPolygonListU3Ec__Iterator0_t892062156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[6] = 
{
	U3CLoadPolygonListU3Ec__Iterator0_t892062156::get_offset_of_U3CszFileNameU3E__0_0(),
	U3CLoadPolygonListU3Ec__Iterator0_t892062156::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadPolygonListU3Ec__Iterator0_t892062156::get_offset_of_U24this_2(),
	U3CLoadPolygonListU3Ec__Iterator0_t892062156::get_offset_of_U24current_3(),
	U3CLoadPolygonListU3Ec__Iterator0_t892062156::get_offset_of_U24disposing_4(),
	U3CLoadPolygonListU3Ec__Iterator0_t892062156::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[10] = 
{
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_szPolygonName_0(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U3CszFileNameU3E__0_1(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U3CwwwU3E__0_2(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U3CrootU3E__0_3(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U3CmyXmlDocU3E__0_4(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U3CaryPointPosU3E__0_5(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_polygon_6(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U24current_7(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U24disposing_8(),
	U3CLoadPolygonByXmlU3Ec__Iterator1_t1040811462::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[7] = 
{
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_U3CwwwFormU3E__0_0(),
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_szPolygonName_1(),
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_szContent_2(),
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_U3CwwwU3E__0_3(),
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_U24current_4(),
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_U24disposing_5(),
	U3CUploadPolygonXmlFileU3Ec__Iterator2_t908359867::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[6] = 
{
	U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472::get_offset_of_U3CwwwFormU3E__0_0(),
	U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472::get_offset_of_szContent_1(),
	U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472::get_offset_of_U3CwwwU3E__0_2(),
	U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472::get_offset_of_U24current_3(),
	U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472::get_offset_of_U24disposing_4(),
	U3CUpLoadPolygonListFileU3Ec__Iterator3_t2946356472::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (PolygonLine_t1895725216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[4] = 
{
	PolygonLine_t1895725216::get_offset_of__lrMain_2(),
	PolygonLine_t1895725216::get_offset_of_m_aryPoints_3(),
	PolygonLine_t1895725216::get_offset_of_vecTempPos_4(),
	PolygonLine_t1895725216::get_offset_of_m_sKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (PolygonPoint_t4211654561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[5] = 
{
	PolygonPoint_t4211654561::get_offset_of__srMain_2(),
	PolygonPoint_t4211654561::get_offset_of_m_lstRelatedLine_3(),
	PolygonPoint_t4211654561::get_offset_of_m_nGUID_4(),
	PolygonPoint_t4211654561::get_offset_of_m_nIndex_5(),
	PolygonPoint_t4211654561::get_offset_of__text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (ResourceManager_t484397614), -1, sizeof(ResourceManager_t484397614_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3125[51] = 
{
	ResourceManager_t484397614_StaticFields::get_offset_of_s_Instance_2(),
	ResourceManager_t484397614::get_offset_of_m_preCiChuXian_3(),
	ResourceManager_t484397614::get_offset_of_m_aryCiChuXianDongHuaPrefabs_4(),
	ResourceManager_t484397614::get_offset_of_m_font_5(),
	ResourceManager_t484397614::get_offset_of_m_preBuff_6(),
	ResourceManager_t484397614::get_offset_of_m_preGrass_7(),
	ResourceManager_t484397614::get_offset_of_m_preSpray_8(),
	ResourceManager_t484397614::get_offset_of_m_preSceneBall_9(),
	ResourceManager_t484397614::get_offset_of_m_preGunsight_10(),
	ResourceManager_t484397614::get_offset_of_m_preCosmosSpore_11(),
	ResourceManager_t484397614::get_offset_of_m_preThorn_12(),
	ResourceManager_t484397614::get_offset_of_m_preExplodeEvent_13(),
	ResourceManager_t484397614::get_offset_of_m_preRebornSpot_14(),
	ResourceManager_t484397614::get_offset_of_m_preBallSpineMaterial_15(),
	ResourceManager_t484397614::get_offset_of_m_preMiaoHeEffect_16(),
	ResourceManager_t484397614::get_offset_of_m_preItemBuyEffect_17(),
	ResourceManager_t484397614::get_offset_of_m_arySzPlayerColor_18(),
	ResourceManager_t484397614::get_offset_of_m_aryPlayerColors_19(),
	ResourceManager_t484397614::get_offset_of_m_matSpriteDefault_20(),
	ResourceManager_t484397614::get_offset_of__arySkinTextures_21(),
	0,
	0,
	ResourceManager_t484397614::get_offset_of_m_arySkinMaterials_24(),
	ResourceManager_t484397614::get_offset_of_m_Perlin_25(),
	ResourceManager_t484397614::get_offset_of_m_preBean_26(),
	ResourceManager_t484397614::get_offset_of_m_matGray_27(),
	ResourceManager_t484397614::get_offset_of_m_matThorn_28(),
	ResourceManager_t484397614::get_offset_of_m_sprThorn_29(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_arySZPlayerColor_30(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_aryColorPlayerColor_31(),
	ResourceManager_t484397614::get_offset_of_m_shaderDefaultSkinShader_32(),
	ResourceManager_t484397614::get_offset_of_m_dicSkinId2Materials_33(),
	ResourceManager_t484397614::get_offset_of_m_sprDefault_34(),
	ResourceManager_t484397614::get_offset_of_m_dicUnfoldRingMaterial_35(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_lstRecycledTarget_36(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_lstRecycledBean_37(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_lstRecycledThorn_38(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_nGuidCount_39(),
	ResourceManager_t484397614::get_offset_of_m_dicSceneBeanRebornQueue_40(),
	ResourceManager_t484397614_StaticFields::get_offset_of_m_lstRecycledBuff_41(),
	ResourceManager_t484397614::get_offset_of_m_lstRecycledGrass_42(),
	ResourceManager_t484397614::get_offset_of_m_lstRecycledSceneBall_43(),
	ResourceManager_t484397614::get_offset_of_m_lstRecycledGunsight_44(),
	ResourceManager_t484397614::get_offset_of_m_sprShit_45(),
	ResourceManager_t484397614::get_offset_of_m_aryBallSprites_46(),
	ResourceManager_t484397614_StaticFields::get_offset_of_tempColor_47(),
	ResourceManager_t484397614::get_offset_of_m_aryBallSkeletonMaterial_48(),
	ResourceManager_t484397614::get_offset_of_m_lstRecycledMiaoHeEffect_49(),
	ResourceManager_t484397614::get_offset_of_m_lstRecycledItemBuyEffect_50(),
	ResourceManager_t484397614::get_offset_of_m_fCiChuXianDongHuaSpeed_51(),
	ResourceManager_t484397614::get_offset_of_m_lstRecycledCiChuXianAni_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (CZhangYu_t3000622058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[4] = 
{
	CZhangYu_t3000622058::get_offset_of_m_fZhaYanJianGe_2(),
	CZhangYu_t3000622058::get_offset_of_m_faZhaYan_3(),
	CZhangYu_t3000622058::get_offset_of__srBody_4(),
	CZhangYu_t3000622058::get_offset_of_m_fZhaYanTimeElapse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (CChatMessage_t3066647275), -1, sizeof(CChatMessage_t3066647275_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3127[5] = 
{
	CChatMessage_t3066647275::get_offset_of__txtContent_2(),
	CChatMessage_t3066647275_StaticFields::get_offset_of_vecTempSize_3(),
	CChatMessage_t3066647275_StaticFields::get_offset_of_vecTempPos_4(),
	CChatMessage_t3066647275_StaticFields::get_offset_of_vecTempScale_5(),
	CChatMessage_t3066647275::get_offset_of_m_fHeight_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (AccountJSON_t3293827731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[5] = 
{
	AccountJSON_t3293827731::get_offset_of_phonenum_0(),
	AccountJSON_t3293827731::get_offset_of_rolename_1(),
	AccountJSON_t3293827731::get_offset_of_coins_2(),
	AccountJSON_t3293827731::get_offset_of_diamonds_3(),
	AccountJSON_t3293827731::get_offset_of_forbidden_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (GetAccountInfoJSON_t2268999360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[2] = 
{
	GetAccountInfoJSON_t2268999360::get_offset_of_code_0(),
	GetAccountInfoJSON_t2268999360::get_offset_of_info_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (UpdateRoleNameJSON_t4080939306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[1] = 
{
	UpdateRoleNameJSON_t4080939306::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (Account_t400769301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[3] = 
{
	Account_t400769301::get_offset_of_authenticator_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[8] = 
{
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_U3CformU3E__0_0(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_phonenum_1(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_idortoken_2(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_U3CrequestU3E__0_3(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_callback_4(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_U24current_5(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_U24disposing_6(),
	U3CgetAccountInfoPostFormU3Ec__Iterator0_t4132270415::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[9] = 
{
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_U3CformU3E__0_0(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_phonenum_1(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_idortoken_2(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_passby_3(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_U3CrequestU3E__0_4(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_callback_5(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_U24current_6(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_U24disposing_7(),
	U3CupdateRoleNamePostFormU3Ec__Iterator1_t1270243448::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (AccountData_t1002028515), -1, sizeof(AccountData_t1002028515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3134[28] = 
{
	AccountData_t1002028515_StaticFields::get_offset_of_s_Instance_2(),
	AccountData_t1002028515::get_offset_of_m_bLocalResLoaded_3(),
	AccountData_t1002028515::get_offset_of_m_bRemoteResLoaded_4(),
	AccountData_t1002028515::get_offset_of_m_sprNoSkin_5(),
	AccountData_t1002028515::get_offset_of_authenticator_6(),
	AccountData_t1002028515::get_offset_of_account_7(),
	AccountData_t1002028515::get_offset_of_charge_8(),
	AccountData_t1002028515::get_offset_of_market_9(),
	AccountData_t1002028515::get_offset_of_buyments_10(),
	AccountData_t1002028515::get_offset_of_localeLoader_11(),
	AccountData_t1002028515::get_offset_of_remoteLoader_12(),
	AccountData_t1002028515::get_offset_of_skins_13(),
	AccountData_t1002028515::get_offset_of_products_14(),
	0,
	0,
	0,
	0,
	AccountData_t1002028515::get_offset_of_m_aryMoney_19(),
	AccountData_t1002028515::get_offset_of_asyncLoad_20(),
	AccountData_t1002028515::get_offset_of__txtCounting_21(),
	AccountData_t1002028515::get_offset_of__btnCounting_22(),
	AccountData_t1002028515::get_offset_of_m_fSmsButtonCountingTime_23(),
	AccountData_t1002028515::get_offset_of_m_dicBoughtItems_24(),
	AccountData_t1002028515::get_offset_of__inputTemp_25(),
	AccountData_t1002028515_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_26(),
	AccountData_t1002028515_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_27(),
	AccountData_t1002028515_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_28(),
	AccountData_t1002028515_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (SkinJSON_t2311182042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[10] = 
{
	SkinJSON_t2311182042::get_offset_of_id_0(),
	SkinJSON_t2311182042::get_offset_of_type_1(),
	SkinJSON_t2311182042::get_offset_of_name_2(),
	SkinJSON_t2311182042::get_offset_of_path_3(),
	SkinJSON_t2311182042::get_offset_of_thumbnail_4(),
	SkinJSON_t2311182042::get_offset_of_forbidden_5(),
	SkinJSON_t2311182042::get_offset_of_description_6(),
	SkinJSON_t2311182042::get_offset_of_costcoins_7(),
	SkinJSON_t2311182042::get_offset_of_costdiamonds_8(),
	SkinJSON_t2311182042::get_offset_of_ringcolor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (SkinListJSON_t1657696259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[1] = 
{
	SkinListJSON_t1657696259::get_offset_of_skins_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (FileSkinListJSON_t1219265033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[2] = 
{
	FileSkinListJSON_t1219265033::get_offset_of_any_type_0(),
	FileSkinListJSON_t1219265033::get_offset_of_any_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (eMoneyType_t3516216660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3138[3] = 
{
	eMoneyType_t3516216660::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587), -1, sizeof(U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3139[8] = 
{
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_U3CresourcesU3E__0_0(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_callback_1(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_U24this_2(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_U24current_3(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_U24disposing_4(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_U24PC_5(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587::get_offset_of_U24locvar0_6(),
	U3CloadLocaleResourcesU3Ec__Iterator0_t4084272587_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[2] = 
{
	U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912::get_offset_of_callback_0(),
	U3CloadLocaleResourcesU3Ec__AnonStorey2_t1460044912::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512), -1, sizeof(U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3141[11] = 
{
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U3CskinsJsonUrlU3E__0_0(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U3CrequestU3E__0_1(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U3CresourcesU3E__1_2(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U3CjsonU3E__1_3(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_callback_4(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U24this_5(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U24current_6(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U24disposing_7(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U24PC_8(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512::get_offset_of_U24locvar0_9(),
	U3CloadRemoteResourcesU3Ec__Iterator1_t1103037512_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[2] = 
{
	U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736::get_offset_of_callback_0(),
	U3CloadRemoteResourcesU3Ec__AnonStorey3_t287817736::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (U3CDoChargeU3Ec__AnonStorey4_t1711606679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[2] = 
{
	U3CDoChargeU3Ec__AnonStorey4_t1711606679::get_offset_of_code_0(),
	U3CDoChargeU3Ec__AnonStorey4_t1711606679::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (GetAuthenticatorTypeJSON_t3656451505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[2] = 
{
	GetAuthenticatorTypeJSON_t3656451505::get_offset_of_code_0(),
	GetAuthenticatorTypeJSON_t3656451505::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (GetSMSCodeJSON_t2897747432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[1] = 
{
	GetSMSCodeJSON_t2897747432::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (SimpleRegisterJSON_t3209520220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[1] = 
{
	SimpleRegisterJSON_t3209520220::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (SecureRegisterJSON_t1894166460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[1] = 
{
	SecureRegisterJSON_t1894166460::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (SimplePasswordLoginJSON_t3050611672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[2] = 
{
	SimplePasswordLoginJSON_t3050611672::get_offset_of_code_0(),
	SimplePasswordLoginJSON_t3050611672::get_offset_of_sessionid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (SecurePasswordLoginJSON_t1225826777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[3] = 
{
	SecurePasswordLoginJSON_t1225826777::get_offset_of_code_0(),
	SecurePasswordLoginJSON_t1225826777::get_offset_of_salt_1(),
	SecurePasswordLoginJSON_t1225826777::get_offset_of_ephemeral_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (SecurePasswordLoginVerifyJSON_t120611188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[2] = 
{
	SecurePasswordLoginVerifyJSON_t120611188::get_offset_of_code_0(),
	SecurePasswordLoginVerifyJSON_t120611188::get_offset_of_proof_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (SecureEphemeralJSON_t940315958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[2] = 
{
	SecureEphemeralJSON_t940315958::get_offset_of_ephemeral_0(),
	SecureEphemeralJSON_t940315958::get_offset_of_secret_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (SecureSessionJSON_t884481414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[2] = 
{
	SecureSessionJSON_t884481414::get_offset_of_key_0(),
	SecureSessionJSON_t884481414::get_offset_of_proof_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (SecureVerifySessionJSON_t2163129613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[1] = 
{
	SecureVerifySessionJSON_t2163129613::get_offset_of_error_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (SimpleResetPasswordJSON_t489914809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[1] = 
{
	SimpleResetPasswordJSON_t489914809::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (SecureResetPasswordJSON_t404722052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[1] = 
{
	SecureResetPasswordJSON_t404722052::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (SecureGetOTPCounterJSON_t3583328016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[2] = 
{
	SecureGetOTPCounterJSON_t3583328016::get_offset_of_code_0(),
	SecureGetOTPCounterJSON_t3583328016::get_offset_of_otpcounter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (SessionVerifyJSON_t2562930706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[1] = 
{
	SessionVerifyJSON_t2562930706::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (SessionFileJSON_t380207342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[2] = 
{
	SessionFileJSON_t380207342::get_offset_of_phonenum_0(),
	SessionFileJSON_t380207342::get_offset_of_sessionKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (Authenticator_t530988528), -1, sizeof(Authenticator_t530988528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3159[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Authenticator_t530988528::get_offset_of_context_18(),
	Authenticator_t530988528::get_offset_of_webView_19(),
	Authenticator_t530988528::get_offset_of_authenticatorType_20(),
	Authenticator_t530988528::get_offset_of_webViewPageLoaded_21(),
	Authenticator_t530988528_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
	Authenticator_t530988528_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_23(),
	Authenticator_t530988528_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (U3CinitU3Ec__AnonStoreyB_t1325999526), -1, sizeof(U3CinitU3Ec__AnonStoreyB_t1325999526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3160[3] = 
{
	U3CinitU3Ec__AnonStoreyB_t1325999526::get_offset_of_callback_0(),
	U3CinitU3Ec__AnonStoreyB_t1325999526::get_offset_of_U24this_1(),
	U3CinitU3Ec__AnonStoreyB_t1325999526_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (U3CgetSMSCodeU3Ec__Iterator0_t1038232676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[8] = 
{
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_phonenum_0(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_smstype_1(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_U3CurlU3E__0_2(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_U3CrequestU3E__0_3(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_callback_4(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_U24current_5(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_U24disposing_6(),
	U3CgetSMSCodeU3Ec__Iterator0_t1038232676::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[5] = 
{
	U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605::get_offset_of_U3CrequestU3E__0_0(),
	U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605::get_offset_of_callback_1(),
	U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605::get_offset_of_U24current_2(),
	U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605::get_offset_of_U24disposing_3(),
	U3CgetAuthenticatorTypeU3Ec__Iterator1_t284942605::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (U3CsimpleRegisterU3Ec__Iterator2_t975479272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[9] = 
{
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_U3CformU3E__0_0(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_phonenum_1(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_smscode_2(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_password_3(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_U3CrequestU3E__0_4(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_callback_5(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_U24current_6(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_U24disposing_7(),
	U3CsimpleRegisterU3Ec__Iterator2_t975479272::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[5] = 
{
	U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424::get_offset_of_phonenum_0(),
	U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424::get_offset_of_password_1(),
	U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424::get_offset_of_smscode_2(),
	U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424::get_offset_of_callback_3(),
	U3CsecureRegisterU3Ec__AnonStoreyC_t2323385424::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (U3CsecureRegisterU3Ec__AnonStoreyD_t3780315107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[2] = 
{
	U3CsecureRegisterU3Ec__AnonStoreyD_t3780315107::get_offset_of_salt_0(),
	U3CsecureRegisterU3Ec__AnonStoreyD_t3780315107::get_offset_of_U3CU3Ef__refU2412_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[10] = 
{
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U3CformU3E__0_0(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_phonenum_1(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_password_2(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U3CrequestU3E__0_3(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_callback_4(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U24this_5(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U24current_6(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U24disposing_7(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U24PC_8(),
	U3CsimplePasswordLoginU3Ec__Iterator3_t1174302285::get_offset_of_U24locvar1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[2] = 
{
	U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469::get_offset_of_callback_0(),
	U3CsimplePasswordLoginU3Ec__AnonStoreyE_t2664070469::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[4] = 
{
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005::get_offset_of_phonenum_0(),
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005::get_offset_of_password_1(),
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005::get_offset_of_callback_2(),
	U3CsecurePasswordLoginU3Ec__AnonStoreyF_t657179005::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[2] = 
{
	U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410::get_offset_of_clientEphemeral_0(),
	U3CsecurePasswordLoginU3Ec__AnonStorey10_t788032410::get_offset_of_U3CU3Ef__refU2415_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[4] = 
{
	U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897::get_offset_of_serverEphemeral_0(),
	U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897::get_offset_of_salt_1(),
	U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897::get_offset_of_U3CU3Ef__refU2415_2(),
	U3CsecurePasswordLoginU3Ec__AnonStorey11_t1072147897::get_offset_of_U3CU3Ef__refU2416_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[4] = 
{
	U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365::get_offset_of_sessionKey_0(),
	U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365::get_offset_of_clientProof_1(),
	U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365::get_offset_of_U3CU3Ef__refU2415_2(),
	U3CsecurePasswordLoginU3Ec__AnonStorey12_t3603917365::get_offset_of_U3CU3Ef__refU2416_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[9] = 
{
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_U3CformU3E__0_0(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_phonenum_1(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_smscode_2(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_password_3(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_U3CrequestU3E__0_4(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_callback_5(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_U24current_6(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_U24disposing_7(),
	U3CsimpleResetPasswordU3Ec__Iterator4_t1182085331::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[5] = 
{
	U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423::get_offset_of_phonenum_0(),
	U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423::get_offset_of_password_1(),
	U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423::get_offset_of_smscode_2(),
	U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423::get_offset_of_callback_3(),
	U3CsecureResetPasswordU3Ec__AnonStorey13_t3950057423::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (U3CsecureResetPasswordU3Ec__AnonStorey14_t847051668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[2] = 
{
	U3CsecureResetPasswordU3Ec__AnonStorey14_t847051668::get_offset_of_salt_0(),
	U3CsecureResetPasswordU3Ec__AnonStorey14_t847051668::get_offset_of_U3CU3Ef__refU2419_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[4] = 
{
	U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539::get_offset_of_coroutine_0(),
	U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539::get_offset_of_passby_1(),
	U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539::get_offset_of_callback_2(),
	U3CsimpleSessionVerifyU3Ec__AnonStorey15_t2684843539::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[4] = 
{
	U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614::get_offset_of_coroutine_0(),
	U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614::get_offset_of_passby_1(),
	U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614::get_offset_of_callback_2(),
	U3CsecureSessionVerifyU3Ec__AnonStorey17_t2063931614::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (U3CsecureSessionVerifyU3Ec__AnonStorey16_t378387632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[2] = 
{
	U3CsecureSessionVerifyU3Ec__AnonStorey16_t378387632::get_offset_of_sessionData_0(),
	U3CsecureSessionVerifyU3Ec__AnonStorey16_t378387632::get_offset_of_U3CU3Ef__refU2423_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[7] = 
{
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_phonenum_0(),
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_U3CurlU3E__0_1(),
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_U3CrequestU3E__0_2(),
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_callback_3(),
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_U24current_4(),
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_U24disposing_5(),
	U3CsecureGetOTPCounterU3Ec__Iterator5_t740756658::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (U3CsecureGenerateSaltU3Ec__AnonStorey18_t2564459476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[1] = 
{
	U3CsecureGenerateSaltU3Ec__AnonStorey18_t2564459476::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (U3CsecureDerivePrivateKeyU3Ec__AnonStorey19_t1306885325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[1] = 
{
	U3CsecureDerivePrivateKeyU3Ec__AnonStorey19_t1306885325::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (U3CsecureDeriveVerifierU3Ec__AnonStorey1A_t670270685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[1] = 
{
	U3CsecureDeriveVerifierU3Ec__AnonStorey1A_t670270685::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[10] = 
{
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_U3CformU3E__0_0(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_phonenum_1(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_smscode_2(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_salt_3(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_verifier_4(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_U3CrequestU3E__0_5(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_callback_6(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_U24current_7(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_U24disposing_8(),
	U3CsecureRegisterPostFormU3Ec__Iterator6_t483746417::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (U3CsecureGenerateEphemeralU3Ec__AnonStorey1B_t3426005466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[1] = 
{
	U3CsecureGenerateEphemeralU3Ec__AnonStorey1B_t3426005466::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[8] = 
{
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_U3CformU3E__0_0(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_phonenum_1(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_ephemeral_2(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_U3CrequestU3E__0_3(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_callback_4(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_U24current_5(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_U24disposing_6(),
	U3CsecurePasswordLoginPostFormU3Ec__Iterator7_t2776907233::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (U3CsecureDeriveSessionU3Ec__AnonStorey1C_t1482553556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[1] = 
{
	U3CsecureDeriveSessionU3Ec__AnonStorey1C_t1482553556::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[8] = 
{
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_U3CformU3E__0_0(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_phonenum_1(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_clientProof_2(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_U3CrequestU3E__0_3(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_callback_4(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_U24current_5(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_U24disposing_6(),
	U3CsecurePasswordLoginVerifyPostFormU3Ec__Iterator8_t2915460501::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (U3CsecureVerifySessionU3Ec__AnonStorey1D_t3772245982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3187[1] = 
{
	U3CsecureVerifySessionU3Ec__AnonStorey1D_t3772245982::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[10] = 
{
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_U3CformU3E__0_0(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_phonenum_1(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_smscode_2(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_salt_3(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_verifier_4(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_U3CrequestU3E__0_5(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_callback_6(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_U24current_7(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_U24disposing_8(),
	U3CsecureResetPasswordPostFormU3Ec__Iterator9_t3637492214::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (U3CsecureGenerateOTPTokenU3Ec__AnonStorey1E_t3576112639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[1] = 
{
	U3CsecureGenerateOTPTokenU3Ec__AnonStorey1E_t3576112639::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[8] = 
{
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_U3CformU3E__0_0(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_phonenum_1(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_idortoken_2(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_U3CrequestU3E__0_3(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_callback_4(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_U24current_5(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_U24disposing_6(),
	U3CsessionVerifyPostFormU3Ec__IteratorA_t3925493049::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (CAccountSystem_t3373977008), -1, sizeof(CAccountSystem_t3373977008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3191[56] = 
{
	CAccountSystem_t3373977008_StaticFields::get_offset_of_s_Instance_2(),
	CAccountSystem_t3373977008::get_offset_of_m_sprAddSkin_3(),
	CAccountSystem_t3373977008::get_offset_of__skeleonSelectedAvartar_4(),
	CAccountSystem_t3373977008::get_offset_of__CanvasScaler_5(),
	CAccountSystem_t3373977008::get_offset_of_m_colorButtonEnabledColor_6(),
	CAccountSystem_t3373977008::get_offset_of_m_colorButtonDisabledColor_7(),
	CAccountSystem_t3373977008::get_offset_of_m_aryHomePageContainerAll_8(),
	CAccountSystem_t3373977008::get_offset_of_m_arySelectSkilPanel_9(),
	CAccountSystem_t3373977008::get_offset_of_m_aryPanelSettings_10(),
	CAccountSystem_t3373977008::get_offset_of__progressLoadIng_11(),
	CAccountSystem_t3373977008::get_offset_of__panelProgressLoadIng_12(),
	CAccountSystem_t3373977008::get_offset_of__scollLoadResourcePercent_13(),
	CAccountSystem_t3373977008::get_offset_of__imgCurEquipedAvatar_14(),
	CAccountSystem_t3373977008::get_offset_of_m_aryCurEquipedAvatar_15(),
	CAccountSystem_t3373977008::get_offset_of__inputLogin_PhoneNum_16(),
	CAccountSystem_t3373977008::get_offset_of__inputLogin_Password_17(),
	CAccountSystem_t3373977008::get_offset_of__inputLogin_GoToRegidter_18(),
	CAccountSystem_t3373977008::get_offset_of_m_aryUpdateRoleName_19(),
	CAccountSystem_t3373977008::get_offset_of__inputMainUI_RoleName_20(),
	CAccountSystem_t3373977008::get_offset_of__txtMainUI_RoleName_21(),
	CAccountSystem_t3373977008::get_offset_of_m_aryHomePageRoleName_22(),
	CAccountSystem_t3373977008::get_offset_of__aryMoney_23(),
	CAccountSystem_t3373977008::get_offset_of_m_cyberlistRankingList_24(),
	CAccountSystem_t3373977008::get_offset_of__inputRegister_PhoneNum_25(),
	CAccountSystem_t3373977008::get_offset_of__inputRegister_SmsCode_26(),
	CAccountSystem_t3373977008::get_offset_of__inputRegister_Password_27(),
	CAccountSystem_t3373977008::get_offset_of__btnGetSmsCode_28(),
	CAccountSystem_t3373977008::get_offset_of__txtButtonSmsCodeCaption_29(),
	CAccountSystem_t3373977008::get_offset_of__inputResetPassword_PhoneNum_30(),
	CAccountSystem_t3373977008::get_offset_of__inputResetPassword_SmsCode_31(),
	CAccountSystem_t3373977008::get_offset_of__inputResetPassword_Password_32(),
	CAccountSystem_t3373977008::get_offset_of__btnGetSmsCode_ResetPwd_33(),
	CAccountSystem_t3373977008::get_offset_of__txtButtonSmsCodeCaption_ResetPwd_34(),
	CAccountSystem_t3373977008::get_offset_of__inputTest_35(),
	CAccountSystem_t3373977008::get_offset_of__panelUpdateRoleName_36(),
	CAccountSystem_t3373977008::get_offset_of_m_aryUpdateRoleNamePanel_37(),
	CAccountSystem_t3373977008::get_offset_of__panelAccountPasswordLogin_38(),
	CAccountSystem_t3373977008::get_offset_of__panelMainUI_39(),
	CAccountSystem_t3373977008::get_offset_of_m_aryPanelMainUI_40(),
	CAccountSystem_t3373977008::get_offset_of__panelRegister_41(),
	CAccountSystem_t3373977008::get_offset_of__panelResetPassword_42(),
	CAccountSystem_t3373977008::get_offset_of_m_aryBottomButtons_Text_43(),
	CAccountSystem_t3373977008::get_offset_of_m_aryBottomButtons_Image_44(),
	CAccountSystem_t3373977008::get_offset_of_m_colorBottomButton_Selected_45(),
	CAccountSystem_t3373977008::get_offset_of_m_colorBottomButton_NotSelected_46(),
	CAccountSystem_t3373977008::get_offset_of_m_sprBottomButton_Selected_47(),
	CAccountSystem_t3373977008::get_offset_of_m_sprBottomButton_NotSelected_48(),
	CAccountSystem_t3373977008::get_offset_of_m_preRankingListItem_49(),
	CAccountSystem_t3373977008::get_offset_of__imgBattery_50(),
	CAccountSystem_t3373977008::get_offset_of__wifiInfo_51(),
	CAccountSystem_t3373977008::get_offset_of__listPaiHangBang_52(),
	CAccountSystem_t3373977008_StaticFields::get_offset_of_m_bNickNameSettled_53(),
	CAccountSystem_t3373977008::get_offset_of_m_fWifiTimeElapse_54(),
	CAccountSystem_t3373977008::get_offset_of__popoStreetNews_55(),
	CAccountSystem_t3373977008::get_offset_of__popoHotEvents_56(),
	CAccountSystem_t3373977008::get_offset_of_m_fPingTime_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (eCtrlType_t377641053)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3192[9] = 
{
	eCtrlType_t377641053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (CCenterServer_t605262508), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (ChargeProductJSON_t2737157911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[5] = 
{
	ChargeProductJSON_t2737157911::get_offset_of_productguid_0(),
	ChargeProductJSON_t2737157911::get_offset_of_productname_1(),
	ChargeProductJSON_t2737157911::get_offset_of_productdesc_2(),
	ChargeProductJSON_t2737157911::get_offset_of_earncoins_3(),
	ChargeProductJSON_t2737157911::get_offset_of_earndiamonds_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (GetChargeListJSON_t3828529878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[2] = 
{
	GetChargeListJSON_t3828529878::get_offset_of_code_0(),
	GetChargeListJSON_t3828529878::get_offset_of_products_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (ChargeGetJSON_t1239220116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[2] = 
{
	ChargeGetJSON_t1239220116::get_offset_of_code_0(),
	ChargeGetJSON_t1239220116::get_offset_of_product_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (ChargeAddJSON_t3390709337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3197[1] = 
{
	ChargeAddJSON_t3390709337::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (ChargeDelJSON_t545259412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[1] = 
{
	ChargeDelJSON_t545259412::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (ChargeInfoJSON_t2136936477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[2] = 
{
	ChargeInfoJSON_t2136936477::get_offset_of_productguid_0(),
	ChargeInfoJSON_t2136936477::get_offset_of_productnum_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
