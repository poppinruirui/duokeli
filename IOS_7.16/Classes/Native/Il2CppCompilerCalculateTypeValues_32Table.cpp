﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Action`2<System.Int32,System.Object>
struct Action_2_t11315885;
// WebAuthAPI.MarketProductJSON
struct MarketProductJSON_t12495859;
// WebAuthAPI.Authenticator
struct Authenticator_t530988528;
// WebAuthAPI.MarketBuymentJSON[]
struct MarketBuymentJSONU5BU5D_t1759546967;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.Dictionary`2<System.String,Resource>
struct Dictionary_2_t4212106464;
// System.EventHandler`1<ProgressEventArgs>
struct EventHandler_1_t4063164883;
// System.EventHandler`1<LoadedEventArgs>
struct EventHandler_1_t1554359864;
// WebAuthAPI.BuyInfoJSON
struct BuyInfoJSON_t2640594623;
// WebAuthAPI.ConsumeInfoJSON
struct ConsumeInfoJSON_t2550589647;
// WebAuthAPI.ChargeInfoJSON
struct ChargeInfoJSON_t2136936477;
// WebAuthAPI.MarketProductJSON[]
struct MarketProductJSONU5BU5D_t3409632994;
// WebAuthAPI.ChargeProductJSON
struct ChargeProductJSON_t2737157911;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Resource
struct Resource_t131882869;
// ResourceLoader
struct ResourceLoader_t3764463624;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t3119663542;
// ResourceBundleList
struct ResourceBundleList_t987847975;
// UnityEngine.AssetBundleManifest
struct AssetBundleManifest_t2634949939;
// ResourceBundle
struct ResourceBundle_t1032369214;
// LocaleResourceLoader
struct LocaleResourceLoader_t808386688;
// Ball
struct Ball_t2206666566;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// RemoteResourceLoader
struct RemoteResourceLoader_t1429902794;
// UnityEngine.AssetBundle
struct AssetBundle_t1153907252;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Material
struct Material_t340375123;
// PhotonView
struct PhotonView_t2207721820;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Button
struct Button_t4055032469;
// WebAuthAPI.Account
struct Account_t400769301;
// WebAuthAPI.Charge
struct Charge_t1653125646;
// WebAuthAPI.Market
struct Market_t538055513;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Func`5<System.String,System.String,System.Object,System.Action`2<System.Int32,System.Object>,System.Collections.IEnumerator>
struct Func_5_t696329592;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// CFrameAnimationEffect[]
struct CFrameAnimationEffectU5BU5D_t897573229;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// Player
struct Player_t3266647312;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t3693186521;
// System.Collections.Generic.Dictionary`2<CBloodStainManager/eBloodStainType,System.Collections.Generic.List`1<CBloodStain>>
struct Dictionary_2_t2042087675;
// System.Collections.Generic.Dictionary`2<System.UInt32,CBloodStain>
struct Dictionary_2_t722626127;
// System.Collections.Generic.List`1<CBloodStain>
struct List_1_t920433207;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t1172778254;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// System.Collections.Generic.Dictionary`2<System.Int32,CBuffEditor/sBuffConfig>
struct Dictionary_2_t3953472576;
// CPaoMaDeng
struct CPaoMaDeng_t1722089677;
// CPaoMaDeng[]
struct CPaoMaDengU5BU5D_t1843095904;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<Ball>
struct List_1_t3678741308;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Rendering.SortingGroup
struct SortingGroup_t3239126932;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t4028559421;
// System.Collections.Generic.Dictionary`2<System.Int32,Ball>
struct Dictionary_2_t1095379897;
// CCosmosEffect[]
struct CCosmosEffectU5BU5D_t3177071136;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.Generic.List`1<CBean>
struct List_1_t2533685502;
// System.Collections.Generic.Dictionary`2<System.Int16,CBean>
struct Dictionary_2_t3251917897;
// System.Collections.Generic.List`1<CBeanManager/sInitBeanNode>
struct List_1_t3233134082;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Collections.Generic.List`1<CBean>>
struct Dictionary_2_t3807953164;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t4032136720;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t4027761066;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// ImprovedPerlin
struct ImprovedPerlin_t2236177176;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityStandardAssets.ImageEffects.BlurOptimized
struct BlurOptimized_t3334654964;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// System.Collections.Generic.List`1<CChiQiuAndJiShaItem>
struct List_1_t3459246459;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// ETCJoystick
struct ETCJoystick_t3250784002;
// CRebornSpot
struct CRebornSpot_t259671536;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Color>
struct Dictionary_2_t2340942623;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMARKETDELPOSTFORMU3EC__ITERATOR3_T3690002772_H
#define U3CMARKETDELPOSTFORMU3EC__ITERATOR3_T3690002772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<marketDelPostForm>c__Iterator3
struct  U3CmarketDelPostFormU3Ec__Iterator3_t3690002772  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Market/<marketDelPostForm>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CmarketDelPostFormU3Ec__Iterator3_t3690002772, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARKETDELPOSTFORMU3EC__ITERATOR3_T3690002772_H
#ifndef U3CMARKETADDPOSTFORMU3EC__ITERATOR2_T927080642_H
#define U3CMARKETADDPOSTFORMU3EC__ITERATOR2_T927080642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<marketAddPostForm>c__Iterator2
struct  U3CmarketAddPostFormU3Ec__Iterator2_t927080642  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::passby
	RuntimeObject * ___passby_3;
	// WebAuthAPI.MarketProductJSON WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::<product>__0
	MarketProductJSON_t12495859 * ___U3CproductU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::callback
	Action_2_t11315885 * ___callback_6;
	// System.Object WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Market/<marketAddPostForm>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CproductU3E__0_4() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___U3CproductU3E__0_4)); }
	inline MarketProductJSON_t12495859 * get_U3CproductU3E__0_4() const { return ___U3CproductU3E__0_4; }
	inline MarketProductJSON_t12495859 ** get_address_of_U3CproductU3E__0_4() { return &___U3CproductU3E__0_4; }
	inline void set_U3CproductU3E__0_4(MarketProductJSON_t12495859 * value)
	{
		___U3CproductU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___callback_6)); }
	inline Action_2_t11315885 * get_callback_6() const { return ___callback_6; }
	inline Action_2_t11315885 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_2_t11315885 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CmarketAddPostFormU3Ec__Iterator2_t927080642, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARKETADDPOSTFORMU3EC__ITERATOR2_T927080642_H
#ifndef U3CMARKETGETPOSTFORMU3EC__ITERATOR1_T3287888726_H
#define U3CMARKETGETPOSTFORMU3EC__ITERATOR1_T3287888726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<marketGetPostForm>c__Iterator1
struct  U3CmarketGetPostFormU3Ec__Iterator1_t3287888726  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Market/<marketGetPostForm>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CmarketGetPostFormU3Ec__Iterator1_t3287888726, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARKETGETPOSTFORMU3EC__ITERATOR1_T3287888726_H
#ifndef U3CGETPRODUCTLISTPOSTFORMU3EC__ITERATOR0_T2123906673_H
#define U3CGETPRODUCTLISTPOSTFORMU3EC__ITERATOR0_T2123906673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<getProductListPostForm>c__Iterator0
struct  U3CgetProductListPostFormU3Ec__Iterator0_t2123906673  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::idortoken
	String_t* ___idortoken_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::callback
	Action_2_t11315885 * ___callback_4;
	// System.Object WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Market/<getProductListPostForm>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___callback_4)); }
	inline Action_2_t11315885 * get_callback_4() const { return ___callback_4; }
	inline Action_2_t11315885 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_2_t11315885 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CgetProductListPostFormU3Ec__Iterator0_t2123906673, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPRODUCTLISTPOSTFORMU3EC__ITERATOR0_T2123906673_H
#ifndef CHARGECHARGEJSON_T4154268863_H
#define CHARGECHARGEJSON_T4154268863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ChargeChargeJSON
struct  ChargeChargeJSON_t4154268863  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.ChargeChargeJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(ChargeChargeJSON_t4154268863, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGECHARGEJSON_T4154268863_H
#ifndef MARKET_T538055513_H
#define MARKET_T538055513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market
struct  Market_t538055513  : public RuntimeObject
{
public:
	// WebAuthAPI.Authenticator WebAuthAPI.Market::authenticator
	Authenticator_t530988528 * ___authenticator_0;

public:
	inline static int32_t get_offset_of_authenticator_0() { return static_cast<int32_t>(offsetof(Market_t538055513, ___authenticator_0)); }
	inline Authenticator_t530988528 * get_authenticator_0() const { return ___authenticator_0; }
	inline Authenticator_t530988528 ** get_address_of_authenticator_0() { return &___authenticator_0; }
	inline void set_authenticator_0(Authenticator_t530988528 * value)
	{
		___authenticator_0 = value;
		Il2CppCodeGenWriteBarrier((&___authenticator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKET_T538055513_H
#ifndef GETBUYMENTLISTJSON_T3848378166_H
#define GETBUYMENTLISTJSON_T3848378166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.GetBuymentListJSON
struct  GetBuymentListJSON_t3848378166  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.GetBuymentListJSON::code
	int32_t ___code_0;
	// WebAuthAPI.MarketBuymentJSON[] WebAuthAPI.GetBuymentListJSON::buyments
	MarketBuymentJSONU5BU5D_t1759546967* ___buyments_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(GetBuymentListJSON_t3848378166, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_buyments_1() { return static_cast<int32_t>(offsetof(GetBuymentListJSON_t3848378166, ___buyments_1)); }
	inline MarketBuymentJSONU5BU5D_t1759546967* get_buyments_1() const { return ___buyments_1; }
	inline MarketBuymentJSONU5BU5D_t1759546967** get_address_of_buyments_1() { return &___buyments_1; }
	inline void set_buyments_1(MarketBuymentJSONU5BU5D_t1759546967* value)
	{
		___buyments_1 = value;
		Il2CppCodeGenWriteBarrier((&___buyments_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETBUYMENTLISTJSON_T3848378166_H
#ifndef MARKETBUYMENTJSON_T3043372002_H
#define MARKETBUYMENTJSON_T3043372002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketBuymentJSON
struct  MarketBuymentJSON_t3043372002  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.MarketBuymentJSON::productguid
	String_t* ___productguid_0;
	// System.UInt32 WebAuthAPI.MarketBuymentJSON::quantity
	uint32_t ___quantity_1;

public:
	inline static int32_t get_offset_of_productguid_0() { return static_cast<int32_t>(offsetof(MarketBuymentJSON_t3043372002, ___productguid_0)); }
	inline String_t* get_productguid_0() const { return ___productguid_0; }
	inline String_t** get_address_of_productguid_0() { return &___productguid_0; }
	inline void set_productguid_0(String_t* value)
	{
		___productguid_0 = value;
		Il2CppCodeGenWriteBarrier((&___productguid_0), value);
	}

	inline static int32_t get_offset_of_quantity_1() { return static_cast<int32_t>(offsetof(MarketBuymentJSON_t3043372002, ___quantity_1)); }
	inline uint32_t get_quantity_1() const { return ___quantity_1; }
	inline uint32_t* get_address_of_quantity_1() { return &___quantity_1; }
	inline void set_quantity_1(uint32_t value)
	{
		___quantity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETBUYMENTJSON_T3043372002_H
#ifndef MARKETBUYJSON_T505675942_H
#define MARKETBUYJSON_T505675942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketBuyJSON
struct  MarketBuyJSON_t505675942  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.MarketBuyJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(MarketBuyJSON_t505675942, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETBUYJSON_T505675942_H
#ifndef U3CMARKETSETPOSTFORMU3EC__ITERATOR4_T993933197_H
#define U3CMARKETSETPOSTFORMU3EC__ITERATOR4_T993933197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<marketSetPostForm>c__Iterator4
struct  U3CmarketSetPostFormU3Ec__Iterator4_t993933197  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::passby
	RuntimeObject * ___passby_3;
	// WebAuthAPI.MarketProductJSON WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::<product>__0
	MarketProductJSON_t12495859 * ___U3CproductU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::callback
	Action_2_t11315885 * ___callback_6;
	// System.Object WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Market/<marketSetPostForm>c__Iterator4::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CproductU3E__0_4() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___U3CproductU3E__0_4)); }
	inline MarketProductJSON_t12495859 * get_U3CproductU3E__0_4() const { return ___U3CproductU3E__0_4; }
	inline MarketProductJSON_t12495859 ** get_address_of_U3CproductU3E__0_4() { return &___U3CproductU3E__0_4; }
	inline void set_U3CproductU3E__0_4(MarketProductJSON_t12495859 * value)
	{
		___U3CproductU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___callback_6)); }
	inline Action_2_t11315885 * get_callback_6() const { return ___callback_6; }
	inline Action_2_t11315885 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_2_t11315885 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CmarketSetPostFormU3Ec__Iterator4_t993933197, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARKETSETPOSTFORMU3EC__ITERATOR4_T993933197_H
#ifndef IMPROVEDPERLIN_T2236177176_H
#define IMPROVEDPERLIN_T2236177176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImprovedPerlin
struct  ImprovedPerlin_t2236177176  : public RuntimeObject
{
public:
	// System.Int32 ImprovedPerlin::X
	int32_t ___X_0;
	// System.Int32 ImprovedPerlin::Y
	int32_t ___Y_1;
	// System.Int32 ImprovedPerlin::Z
	int32_t ___Z_2;
	// System.Single ImprovedPerlin::u
	float ___u_3;
	// System.Single ImprovedPerlin::v
	float ___v_4;
	// System.Single ImprovedPerlin::w
	float ___w_5;
	// System.Int32 ImprovedPerlin::A
	int32_t ___A_6;
	// System.Int32 ImprovedPerlin::AA
	int32_t ___AA_7;
	// System.Int32 ImprovedPerlin::AB
	int32_t ___AB_8;
	// System.Int32 ImprovedPerlin::B
	int32_t ___B_9;
	// System.Int32 ImprovedPerlin::BA
	int32_t ___BA_10;
	// System.Int32 ImprovedPerlin::BB
	int32_t ___BB_11;
	// System.Single ImprovedPerlin::floorx
	float ___floorx_12;
	// System.Single ImprovedPerlin::floory
	float ___floory_13;
	// System.Single ImprovedPerlin::floorz
	float ___floorz_14;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___X_0)); }
	inline int32_t get_X_0() const { return ___X_0; }
	inline int32_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int32_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___Y_1)); }
	inline int32_t get_Y_1() const { return ___Y_1; }
	inline int32_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int32_t value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_Z_2() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___Z_2)); }
	inline int32_t get_Z_2() const { return ___Z_2; }
	inline int32_t* get_address_of_Z_2() { return &___Z_2; }
	inline void set_Z_2(int32_t value)
	{
		___Z_2 = value;
	}

	inline static int32_t get_offset_of_u_3() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___u_3)); }
	inline float get_u_3() const { return ___u_3; }
	inline float* get_address_of_u_3() { return &___u_3; }
	inline void set_u_3(float value)
	{
		___u_3 = value;
	}

	inline static int32_t get_offset_of_v_4() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___v_4)); }
	inline float get_v_4() const { return ___v_4; }
	inline float* get_address_of_v_4() { return &___v_4; }
	inline void set_v_4(float value)
	{
		___v_4 = value;
	}

	inline static int32_t get_offset_of_w_5() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___w_5)); }
	inline float get_w_5() const { return ___w_5; }
	inline float* get_address_of_w_5() { return &___w_5; }
	inline void set_w_5(float value)
	{
		___w_5 = value;
	}

	inline static int32_t get_offset_of_A_6() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___A_6)); }
	inline int32_t get_A_6() const { return ___A_6; }
	inline int32_t* get_address_of_A_6() { return &___A_6; }
	inline void set_A_6(int32_t value)
	{
		___A_6 = value;
	}

	inline static int32_t get_offset_of_AA_7() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___AA_7)); }
	inline int32_t get_AA_7() const { return ___AA_7; }
	inline int32_t* get_address_of_AA_7() { return &___AA_7; }
	inline void set_AA_7(int32_t value)
	{
		___AA_7 = value;
	}

	inline static int32_t get_offset_of_AB_8() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___AB_8)); }
	inline int32_t get_AB_8() const { return ___AB_8; }
	inline int32_t* get_address_of_AB_8() { return &___AB_8; }
	inline void set_AB_8(int32_t value)
	{
		___AB_8 = value;
	}

	inline static int32_t get_offset_of_B_9() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___B_9)); }
	inline int32_t get_B_9() const { return ___B_9; }
	inline int32_t* get_address_of_B_9() { return &___B_9; }
	inline void set_B_9(int32_t value)
	{
		___B_9 = value;
	}

	inline static int32_t get_offset_of_BA_10() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___BA_10)); }
	inline int32_t get_BA_10() const { return ___BA_10; }
	inline int32_t* get_address_of_BA_10() { return &___BA_10; }
	inline void set_BA_10(int32_t value)
	{
		___BA_10 = value;
	}

	inline static int32_t get_offset_of_BB_11() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___BB_11)); }
	inline int32_t get_BB_11() const { return ___BB_11; }
	inline int32_t* get_address_of_BB_11() { return &___BB_11; }
	inline void set_BB_11(int32_t value)
	{
		___BB_11 = value;
	}

	inline static int32_t get_offset_of_floorx_12() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___floorx_12)); }
	inline float get_floorx_12() const { return ___floorx_12; }
	inline float* get_address_of_floorx_12() { return &___floorx_12; }
	inline void set_floorx_12(float value)
	{
		___floorx_12 = value;
	}

	inline static int32_t get_offset_of_floory_13() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___floory_13)); }
	inline float get_floory_13() const { return ___floory_13; }
	inline float* get_address_of_floory_13() { return &___floory_13; }
	inline void set_floory_13(float value)
	{
		___floory_13 = value;
	}

	inline static int32_t get_offset_of_floorz_14() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176, ___floorz_14)); }
	inline float get_floorz_14() const { return ___floorz_14; }
	inline float* get_address_of_floorz_14() { return &___floorz_14; }
	inline void set_floorz_14(float value)
	{
		___floorz_14 = value;
	}
};

struct ImprovedPerlin_t2236177176_StaticFields
{
public:
	// System.Int32[] ImprovedPerlin::p
	Int32U5BU5D_t385246372* ___p_15;
	// System.Int32[] ImprovedPerlin::permutation
	Int32U5BU5D_t385246372* ___permutation_16;

public:
	inline static int32_t get_offset_of_p_15() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176_StaticFields, ___p_15)); }
	inline Int32U5BU5D_t385246372* get_p_15() const { return ___p_15; }
	inline Int32U5BU5D_t385246372** get_address_of_p_15() { return &___p_15; }
	inline void set_p_15(Int32U5BU5D_t385246372* value)
	{
		___p_15 = value;
		Il2CppCodeGenWriteBarrier((&___p_15), value);
	}

	inline static int32_t get_offset_of_permutation_16() { return static_cast<int32_t>(offsetof(ImprovedPerlin_t2236177176_StaticFields, ___permutation_16)); }
	inline Int32U5BU5D_t385246372* get_permutation_16() const { return ___permutation_16; }
	inline Int32U5BU5D_t385246372** get_address_of_permutation_16() { return &___permutation_16; }
	inline void set_permutation_16(Int32U5BU5D_t385246372* value)
	{
		___permutation_16 = value;
		Il2CppCodeGenWriteBarrier((&___permutation_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPROVEDPERLIN_T2236177176_H
#ifndef UTILS_T1444179947_H
#define UTILS_T1444179947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils
struct  Utils_t1444179947  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T1444179947_H
#ifndef SERVERR_T270116950_H
#define SERVERR_T270116950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServErr
struct  ServErr_t270116950  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERR_T270116950_H
#ifndef COLORPALETTE_T2614655767_H
#define COLORPALETTE_T2614655767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPalette
struct  ColorPalette_t2614655767  : public RuntimeObject
{
public:

public:
};

struct ColorPalette_t2614655767_StaticFields
{
public:
	// System.String[] ColorPalette::_color_strs_bg
	StringU5BU5D_t1281789340* ____color_strs_bg_0;
	// UnityEngine.Color[] ColorPalette::_colors_bg
	ColorU5BU5D_t941916413* ____colors_bg_1;
	// System.Single[] ColorPalette::_ranges_bg_light
	SingleU5BU5D_t1444911251* ____ranges_bg_light_2;
	// System.String[] ColorPalette::_color_strs_ring
	StringU5BU5D_t1281789340* ____color_strs_ring_3;
	// UnityEngine.Color[] ColorPalette::_colors_ring
	ColorU5BU5D_t941916413* ____colors_ring_4;
	// System.String[] ColorPalette::_color_strs_inner
	StringU5BU5D_t1281789340* ____color_strs_inner_5;
	// UnityEngine.Color[] ColorPalette::_colors_inner
	ColorU5BU5D_t941916413* ____colors_inner_6;
	// System.String[] ColorPalette::_color_strs_poison
	StringU5BU5D_t1281789340* ____color_strs_poison_7;
	// UnityEngine.Color[] ColorPalette::_colors_poison
	ColorU5BU5D_t941916413* ____colors_poison_8;
	// System.String[] ColorPalette::_color_strs_food
	StringU5BU5D_t1281789340* ____color_strs_food_9;
	// UnityEngine.Color[] ColorPalette::_colors_food
	ColorU5BU5D_t941916413* ____colors_food_10;
	// System.String[] ColorPalette::_color_strs_hedge
	StringU5BU5D_t1281789340* ____color_strs_hedge_11;
	// UnityEngine.Color[] ColorPalette::_colors_hedge
	ColorU5BU5D_t941916413* ____colors_hedge_12;

public:
	inline static int32_t get_offset_of__color_strs_bg_0() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____color_strs_bg_0)); }
	inline StringU5BU5D_t1281789340* get__color_strs_bg_0() const { return ____color_strs_bg_0; }
	inline StringU5BU5D_t1281789340** get_address_of__color_strs_bg_0() { return &____color_strs_bg_0; }
	inline void set__color_strs_bg_0(StringU5BU5D_t1281789340* value)
	{
		____color_strs_bg_0 = value;
		Il2CppCodeGenWriteBarrier((&____color_strs_bg_0), value);
	}

	inline static int32_t get_offset_of__colors_bg_1() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____colors_bg_1)); }
	inline ColorU5BU5D_t941916413* get__colors_bg_1() const { return ____colors_bg_1; }
	inline ColorU5BU5D_t941916413** get_address_of__colors_bg_1() { return &____colors_bg_1; }
	inline void set__colors_bg_1(ColorU5BU5D_t941916413* value)
	{
		____colors_bg_1 = value;
		Il2CppCodeGenWriteBarrier((&____colors_bg_1), value);
	}

	inline static int32_t get_offset_of__ranges_bg_light_2() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____ranges_bg_light_2)); }
	inline SingleU5BU5D_t1444911251* get__ranges_bg_light_2() const { return ____ranges_bg_light_2; }
	inline SingleU5BU5D_t1444911251** get_address_of__ranges_bg_light_2() { return &____ranges_bg_light_2; }
	inline void set__ranges_bg_light_2(SingleU5BU5D_t1444911251* value)
	{
		____ranges_bg_light_2 = value;
		Il2CppCodeGenWriteBarrier((&____ranges_bg_light_2), value);
	}

	inline static int32_t get_offset_of__color_strs_ring_3() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____color_strs_ring_3)); }
	inline StringU5BU5D_t1281789340* get__color_strs_ring_3() const { return ____color_strs_ring_3; }
	inline StringU5BU5D_t1281789340** get_address_of__color_strs_ring_3() { return &____color_strs_ring_3; }
	inline void set__color_strs_ring_3(StringU5BU5D_t1281789340* value)
	{
		____color_strs_ring_3 = value;
		Il2CppCodeGenWriteBarrier((&____color_strs_ring_3), value);
	}

	inline static int32_t get_offset_of__colors_ring_4() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____colors_ring_4)); }
	inline ColorU5BU5D_t941916413* get__colors_ring_4() const { return ____colors_ring_4; }
	inline ColorU5BU5D_t941916413** get_address_of__colors_ring_4() { return &____colors_ring_4; }
	inline void set__colors_ring_4(ColorU5BU5D_t941916413* value)
	{
		____colors_ring_4 = value;
		Il2CppCodeGenWriteBarrier((&____colors_ring_4), value);
	}

	inline static int32_t get_offset_of__color_strs_inner_5() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____color_strs_inner_5)); }
	inline StringU5BU5D_t1281789340* get__color_strs_inner_5() const { return ____color_strs_inner_5; }
	inline StringU5BU5D_t1281789340** get_address_of__color_strs_inner_5() { return &____color_strs_inner_5; }
	inline void set__color_strs_inner_5(StringU5BU5D_t1281789340* value)
	{
		____color_strs_inner_5 = value;
		Il2CppCodeGenWriteBarrier((&____color_strs_inner_5), value);
	}

	inline static int32_t get_offset_of__colors_inner_6() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____colors_inner_6)); }
	inline ColorU5BU5D_t941916413* get__colors_inner_6() const { return ____colors_inner_6; }
	inline ColorU5BU5D_t941916413** get_address_of__colors_inner_6() { return &____colors_inner_6; }
	inline void set__colors_inner_6(ColorU5BU5D_t941916413* value)
	{
		____colors_inner_6 = value;
		Il2CppCodeGenWriteBarrier((&____colors_inner_6), value);
	}

	inline static int32_t get_offset_of__color_strs_poison_7() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____color_strs_poison_7)); }
	inline StringU5BU5D_t1281789340* get__color_strs_poison_7() const { return ____color_strs_poison_7; }
	inline StringU5BU5D_t1281789340** get_address_of__color_strs_poison_7() { return &____color_strs_poison_7; }
	inline void set__color_strs_poison_7(StringU5BU5D_t1281789340* value)
	{
		____color_strs_poison_7 = value;
		Il2CppCodeGenWriteBarrier((&____color_strs_poison_7), value);
	}

	inline static int32_t get_offset_of__colors_poison_8() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____colors_poison_8)); }
	inline ColorU5BU5D_t941916413* get__colors_poison_8() const { return ____colors_poison_8; }
	inline ColorU5BU5D_t941916413** get_address_of__colors_poison_8() { return &____colors_poison_8; }
	inline void set__colors_poison_8(ColorU5BU5D_t941916413* value)
	{
		____colors_poison_8 = value;
		Il2CppCodeGenWriteBarrier((&____colors_poison_8), value);
	}

	inline static int32_t get_offset_of__color_strs_food_9() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____color_strs_food_9)); }
	inline StringU5BU5D_t1281789340* get__color_strs_food_9() const { return ____color_strs_food_9; }
	inline StringU5BU5D_t1281789340** get_address_of__color_strs_food_9() { return &____color_strs_food_9; }
	inline void set__color_strs_food_9(StringU5BU5D_t1281789340* value)
	{
		____color_strs_food_9 = value;
		Il2CppCodeGenWriteBarrier((&____color_strs_food_9), value);
	}

	inline static int32_t get_offset_of__colors_food_10() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____colors_food_10)); }
	inline ColorU5BU5D_t941916413* get__colors_food_10() const { return ____colors_food_10; }
	inline ColorU5BU5D_t941916413** get_address_of__colors_food_10() { return &____colors_food_10; }
	inline void set__colors_food_10(ColorU5BU5D_t941916413* value)
	{
		____colors_food_10 = value;
		Il2CppCodeGenWriteBarrier((&____colors_food_10), value);
	}

	inline static int32_t get_offset_of__color_strs_hedge_11() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____color_strs_hedge_11)); }
	inline StringU5BU5D_t1281789340* get__color_strs_hedge_11() const { return ____color_strs_hedge_11; }
	inline StringU5BU5D_t1281789340** get_address_of__color_strs_hedge_11() { return &____color_strs_hedge_11; }
	inline void set__color_strs_hedge_11(StringU5BU5D_t1281789340* value)
	{
		____color_strs_hedge_11 = value;
		Il2CppCodeGenWriteBarrier((&____color_strs_hedge_11), value);
	}

	inline static int32_t get_offset_of__colors_hedge_12() { return static_cast<int32_t>(offsetof(ColorPalette_t2614655767_StaticFields, ____colors_hedge_12)); }
	inline ColorU5BU5D_t941916413* get__colors_hedge_12() const { return ____colors_hedge_12; }
	inline ColorU5BU5D_t941916413** get_address_of__colors_hedge_12() { return &____colors_hedge_12; }
	inline void set__colors_hedge_12(ColorU5BU5D_t941916413* value)
	{
		____colors_hedge_12 = value;
		Il2CppCodeGenWriteBarrier((&____colors_hedge_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPALETTE_T2614655767_H
#ifndef RESOURCELOADER_T3764463624_H
#define RESOURCELOADER_T3764463624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceLoader
struct  ResourceLoader_t3764463624  : public RuntimeObject
{
public:
	// System.String ResourceLoader::url
	String_t* ___url_0;
	// System.Collections.Generic.Dictionary`2<System.String,Resource> ResourceLoader::resources
	Dictionary_2_t4212106464 * ___resources_1;
	// System.Int32 ResourceLoader::progress
	int32_t ___progress_2;
	// System.EventHandler`1<ProgressEventArgs> ResourceLoader::raiseProgressEvent
	EventHandler_1_t4063164883 * ___raiseProgressEvent_3;
	// System.EventHandler`1<LoadedEventArgs> ResourceLoader::raiseLoadedEvent
	EventHandler_1_t1554359864 * ___raiseLoadedEvent_4;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(ResourceLoader_t3764463624, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_resources_1() { return static_cast<int32_t>(offsetof(ResourceLoader_t3764463624, ___resources_1)); }
	inline Dictionary_2_t4212106464 * get_resources_1() const { return ___resources_1; }
	inline Dictionary_2_t4212106464 ** get_address_of_resources_1() { return &___resources_1; }
	inline void set_resources_1(Dictionary_2_t4212106464 * value)
	{
		___resources_1 = value;
		Il2CppCodeGenWriteBarrier((&___resources_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(ResourceLoader_t3764463624, ___progress_2)); }
	inline int32_t get_progress_2() const { return ___progress_2; }
	inline int32_t* get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(int32_t value)
	{
		___progress_2 = value;
	}

	inline static int32_t get_offset_of_raiseProgressEvent_3() { return static_cast<int32_t>(offsetof(ResourceLoader_t3764463624, ___raiseProgressEvent_3)); }
	inline EventHandler_1_t4063164883 * get_raiseProgressEvent_3() const { return ___raiseProgressEvent_3; }
	inline EventHandler_1_t4063164883 ** get_address_of_raiseProgressEvent_3() { return &___raiseProgressEvent_3; }
	inline void set_raiseProgressEvent_3(EventHandler_1_t4063164883 * value)
	{
		___raiseProgressEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___raiseProgressEvent_3), value);
	}

	inline static int32_t get_offset_of_raiseLoadedEvent_4() { return static_cast<int32_t>(offsetof(ResourceLoader_t3764463624, ___raiseLoadedEvent_4)); }
	inline EventHandler_1_t1554359864 * get_raiseLoadedEvent_4() const { return ___raiseLoadedEvent_4; }
	inline EventHandler_1_t1554359864 ** get_address_of_raiseLoadedEvent_4() { return &___raiseLoadedEvent_4; }
	inline void set_raiseLoadedEvent_4(EventHandler_1_t1554359864 * value)
	{
		___raiseLoadedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___raiseLoadedEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCELOADER_T3764463624_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef U3CGETBUYMENTLISTPOSTFORMU3EC__ITERATOR7_T4024730788_H
#define U3CGETBUYMENTLISTPOSTFORMU3EC__ITERATOR7_T4024730788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7
struct  U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Market/<getBuymentListPostForm>c__Iterator7::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBUYMENTLISTPOSTFORMU3EC__ITERATOR7_T4024730788_H
#ifndef U3CMARKETBUYPOSTFORMU3EC__ITERATOR6_T2827113427_H
#define U3CMARKETBUYPOSTFORMU3EC__ITERATOR6_T2827113427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6
struct  U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::passby
	RuntimeObject * ___passby_3;
	// WebAuthAPI.BuyInfoJSON WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::<info>__0
	BuyInfoJSON_t2640594623 * ___U3CinfoU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::callback
	Action_2_t11315885 * ___callback_6;
	// System.Object WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Market/<marketBuyPostForm>c__Iterator6::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CinfoU3E__0_4() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___U3CinfoU3E__0_4)); }
	inline BuyInfoJSON_t2640594623 * get_U3CinfoU3E__0_4() const { return ___U3CinfoU3E__0_4; }
	inline BuyInfoJSON_t2640594623 ** get_address_of_U3CinfoU3E__0_4() { return &___U3CinfoU3E__0_4; }
	inline void set_U3CinfoU3E__0_4(BuyInfoJSON_t2640594623 * value)
	{
		___U3CinfoU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinfoU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___callback_6)); }
	inline Action_2_t11315885 * get_callback_6() const { return ___callback_6; }
	inline Action_2_t11315885 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_2_t11315885 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARKETBUYPOSTFORMU3EC__ITERATOR6_T2827113427_H
#ifndef U3CMARKETCONSUMEPOSTFORMU3EC__ITERATOR5_T4079474279_H
#define U3CMARKETCONSUMEPOSTFORMU3EC__ITERATOR5_T4079474279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5
struct  U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::passby
	RuntimeObject * ___passby_3;
	// WebAuthAPI.ConsumeInfoJSON WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::<info>__0
	ConsumeInfoJSON_t2550589647 * ___U3CinfoU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::callback
	Action_2_t11315885 * ___callback_6;
	// System.Object WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Market/<marketConsumePostForm>c__Iterator5::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CinfoU3E__0_4() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___U3CinfoU3E__0_4)); }
	inline ConsumeInfoJSON_t2550589647 * get_U3CinfoU3E__0_4() const { return ___U3CinfoU3E__0_4; }
	inline ConsumeInfoJSON_t2550589647 ** get_address_of_U3CinfoU3E__0_4() { return &___U3CinfoU3E__0_4; }
	inline void set_U3CinfoU3E__0_4(ConsumeInfoJSON_t2550589647 * value)
	{
		___U3CinfoU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinfoU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___callback_6)); }
	inline Action_2_t11315885 * get_callback_6() const { return ___callback_6; }
	inline Action_2_t11315885 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_2_t11315885 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMARKETCONSUMEPOSTFORMU3EC__ITERATOR5_T4079474279_H
#ifndef BUYINFOJSON_T2640594623_H
#define BUYINFOJSON_T2640594623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.BuyInfoJSON
struct  BuyInfoJSON_t2640594623  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.BuyInfoJSON::productguid
	String_t* ___productguid_0;
	// System.UInt32 WebAuthAPI.BuyInfoJSON::productnum
	uint32_t ___productnum_1;

public:
	inline static int32_t get_offset_of_productguid_0() { return static_cast<int32_t>(offsetof(BuyInfoJSON_t2640594623, ___productguid_0)); }
	inline String_t* get_productguid_0() const { return ___productguid_0; }
	inline String_t** get_address_of_productguid_0() { return &___productguid_0; }
	inline void set_productguid_0(String_t* value)
	{
		___productguid_0 = value;
		Il2CppCodeGenWriteBarrier((&___productguid_0), value);
	}

	inline static int32_t get_offset_of_productnum_1() { return static_cast<int32_t>(offsetof(BuyInfoJSON_t2640594623, ___productnum_1)); }
	inline uint32_t get_productnum_1() const { return ___productnum_1; }
	inline uint32_t* get_address_of_productnum_1() { return &___productnum_1; }
	inline void set_productnum_1(uint32_t value)
	{
		___productnum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYINFOJSON_T2640594623_H
#ifndef U3CCHARGEDELPOSTFORMU3EC__ITERATOR3_T612667134_H
#define U3CCHARGEDELPOSTFORMU3EC__ITERATOR3_T612667134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3
struct  U3CchargeDelPostFormU3Ec__Iterator3_t612667134  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Charge/<chargeDelPostForm>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CchargeDelPostFormU3Ec__Iterator3_t612667134, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHARGEDELPOSTFORMU3EC__ITERATOR3_T612667134_H
#ifndef CONSTANTS_T701097383_H
#define CONSTANTS_T701097383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Constants
struct  Constants_t701097383  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T701097383_H
#ifndef U3CRECEIPTVALIDATEPOSTFORMU3EC__ITERATOR5_T1483565208_H
#define U3CRECEIPTVALIDATEPOSTFORMU3EC__ITERATOR5_T1483565208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5
struct  U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Charge/<receiptValidatePostForm>c__Iterator5::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECEIPTVALIDATEPOSTFORMU3EC__ITERATOR5_T1483565208_H
#ifndef RECEIPTVALIDATEJSON_T2198994320_H
#define RECEIPTVALIDATEJSON_T2198994320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ReceiptValidateJSON
struct  ReceiptValidateJSON_t2198994320  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.ReceiptValidateJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(ReceiptValidateJSON_t2198994320, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIPTVALIDATEJSON_T2198994320_H
#ifndef CHARGE_T1653125646_H
#define CHARGE_T1653125646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge
struct  Charge_t1653125646  : public RuntimeObject
{
public:
	// WebAuthAPI.Authenticator WebAuthAPI.Charge::authenticator
	Authenticator_t530988528 * ___authenticator_0;

public:
	inline static int32_t get_offset_of_authenticator_0() { return static_cast<int32_t>(offsetof(Charge_t1653125646, ___authenticator_0)); }
	inline Authenticator_t530988528 * get_authenticator_0() const { return ___authenticator_0; }
	inline Authenticator_t530988528 ** get_address_of_authenticator_0() { return &___authenticator_0; }
	inline void set_authenticator_0(Authenticator_t530988528 * value)
	{
		___authenticator_0 = value;
		Il2CppCodeGenWriteBarrier((&___authenticator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGE_T1653125646_H
#ifndef U3CGETCHARGELISTPOSTFORMU3EC__ITERATOR0_T185136653_H
#define U3CGETCHARGELISTPOSTFORMU3EC__ITERATOR0_T185136653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0
struct  U3CgetChargeListPostFormU3Ec__Iterator0_t185136653  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::idortoken
	String_t* ___idortoken_2;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_3;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::callback
	Action_2_t11315885 * ___callback_4;
	// System.Object WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebAuthAPI.Charge/<getChargeListPostForm>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___callback_4)); }
	inline Action_2_t11315885 * get_callback_4() const { return ___callback_4; }
	inline Action_2_t11315885 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_2_t11315885 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CgetChargeListPostFormU3Ec__Iterator0_t185136653, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCHARGELISTPOSTFORMU3EC__ITERATOR0_T185136653_H
#ifndef U3CCHARGECHARGEPOSTFORMU3EC__ITERATOR4_T563494620_H
#define U3CCHARGECHARGEPOSTFORMU3EC__ITERATOR4_T563494620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4
struct  U3CchargeChargePostFormU3Ec__Iterator4_t563494620  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::passby
	RuntimeObject * ___passby_3;
	// WebAuthAPI.ChargeInfoJSON WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::<info>__0
	ChargeInfoJSON_t2136936477 * ___U3CinfoU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::callback
	Action_2_t11315885 * ___callback_6;
	// System.Object WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Charge/<chargeChargePostForm>c__Iterator4::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CinfoU3E__0_4() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___U3CinfoU3E__0_4)); }
	inline ChargeInfoJSON_t2136936477 * get_U3CinfoU3E__0_4() const { return ___U3CinfoU3E__0_4; }
	inline ChargeInfoJSON_t2136936477 ** get_address_of_U3CinfoU3E__0_4() { return &___U3CinfoU3E__0_4; }
	inline void set_U3CinfoU3E__0_4(ChargeInfoJSON_t2136936477 * value)
	{
		___U3CinfoU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinfoU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___callback_6)); }
	inline Action_2_t11315885 * get_callback_6() const { return ___callback_6; }
	inline Action_2_t11315885 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_2_t11315885 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CchargeChargePostFormU3Ec__Iterator4_t563494620, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHARGECHARGEPOSTFORMU3EC__ITERATOR4_T563494620_H
#ifndef U3CCHARGEGETPOSTFORMU3EC__ITERATOR1_T1522386906_H
#define U3CCHARGEGETPOSTFORMU3EC__ITERATOR1_T1522386906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1
struct  U3CchargeGetPostFormU3Ec__Iterator1_t1522386906  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::passby
	RuntimeObject * ___passby_3;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_4;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::callback
	Action_2_t11315885 * ___callback_5;
	// System.Object WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 WebAuthAPI.Charge/<chargeGetPostForm>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_4() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___U3CrequestU3E__0_4)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_4() const { return ___U3CrequestU3E__0_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_4() { return &___U3CrequestU3E__0_4; }
	inline void set_U3CrequestU3E__0_4(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___callback_5)); }
	inline Action_2_t11315885 * get_callback_5() const { return ___callback_5; }
	inline Action_2_t11315885 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_2_t11315885 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CchargeGetPostFormU3Ec__Iterator1_t1522386906, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHARGEGETPOSTFORMU3EC__ITERATOR1_T1522386906_H
#ifndef U3CLOADSCENEU3EC__ITERATOR0_T2627343872_H
#define U3CLOADSCENEU3EC__ITERATOR0_T2627343872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CReconnectManager/<LoadScene>c__Iterator0
struct  U3CLoadSceneU3Ec__Iterator0_t2627343872  : public RuntimeObject
{
public:
	// System.String CReconnectManager/<LoadScene>c__Iterator0::scene_name
	String_t* ___scene_name_0;
	// System.Object CReconnectManager/<LoadScene>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CReconnectManager/<LoadScene>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CReconnectManager/<LoadScene>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_scene_name_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2627343872, ___scene_name_0)); }
	inline String_t* get_scene_name_0() const { return ___scene_name_0; }
	inline String_t** get_address_of_scene_name_0() { return &___scene_name_0; }
	inline void set_scene_name_0(String_t* value)
	{
		___scene_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_name_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2627343872, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2627343872, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2627343872, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3EC__ITERATOR0_T2627343872_H
#ifndef MARKETCONSUMEJSON_T3254223683_H
#define MARKETCONSUMEJSON_T3254223683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketConsumeJSON
struct  MarketConsumeJSON_t3254223683  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.MarketConsumeJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(MarketConsumeJSON_t3254223683, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETCONSUMEJSON_T3254223683_H
#ifndef CONSUMEINFOJSON_T2550589647_H
#define CONSUMEINFOJSON_T2550589647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.ConsumeInfoJSON
struct  ConsumeInfoJSON_t2550589647  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.ConsumeInfoJSON::productguid
	String_t* ___productguid_0;
	// System.UInt32 WebAuthAPI.ConsumeInfoJSON::productnum
	uint32_t ___productnum_1;

public:
	inline static int32_t get_offset_of_productguid_0() { return static_cast<int32_t>(offsetof(ConsumeInfoJSON_t2550589647, ___productguid_0)); }
	inline String_t* get_productguid_0() const { return ___productguid_0; }
	inline String_t** get_address_of_productguid_0() { return &___productguid_0; }
	inline void set_productguid_0(String_t* value)
	{
		___productguid_0 = value;
		Il2CppCodeGenWriteBarrier((&___productguid_0), value);
	}

	inline static int32_t get_offset_of_productnum_1() { return static_cast<int32_t>(offsetof(ConsumeInfoJSON_t2550589647, ___productnum_1)); }
	inline uint32_t get_productnum_1() const { return ___productnum_1; }
	inline uint32_t* get_address_of_productnum_1() { return &___productnum_1; }
	inline void set_productnum_1(uint32_t value)
	{
		___productnum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMEINFOJSON_T2550589647_H
#ifndef MARKETSETJSON_T666009686_H
#define MARKETSETJSON_T666009686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketSetJSON
struct  MarketSetJSON_t666009686  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.MarketSetJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(MarketSetJSON_t666009686, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETSETJSON_T666009686_H
#ifndef MARKETDELJSON_T1725136982_H
#define MARKETDELJSON_T1725136982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketDelJSON
struct  MarketDelJSON_t1725136982  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.MarketDelJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(MarketDelJSON_t1725136982, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETDELJSON_T1725136982_H
#ifndef MARKETADDJSON_T2861654417_H
#define MARKETADDJSON_T2861654417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketAddJSON
struct  MarketAddJSON_t2861654417  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.MarketAddJSON::code
	int32_t ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(MarketAddJSON_t2861654417, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETADDJSON_T2861654417_H
#ifndef MARKETGETJSON_T711360598_H
#define MARKETGETJSON_T711360598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketGetJSON
struct  MarketGetJSON_t711360598  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.MarketGetJSON::code
	int32_t ___code_0;
	// WebAuthAPI.MarketProductJSON WebAuthAPI.MarketGetJSON::product
	MarketProductJSON_t12495859 * ___product_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(MarketGetJSON_t711360598, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_product_1() { return static_cast<int32_t>(offsetof(MarketGetJSON_t711360598, ___product_1)); }
	inline MarketProductJSON_t12495859 * get_product_1() const { return ___product_1; }
	inline MarketProductJSON_t12495859 ** get_address_of_product_1() { return &___product_1; }
	inline void set_product_1(MarketProductJSON_t12495859 * value)
	{
		___product_1 = value;
		Il2CppCodeGenWriteBarrier((&___product_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETGETJSON_T711360598_H
#ifndef GETPRODUCTLISTJSON_T196412724_H
#define GETPRODUCTLISTJSON_T196412724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.GetProductListJSON
struct  GetProductListJSON_t196412724  : public RuntimeObject
{
public:
	// System.Int32 WebAuthAPI.GetProductListJSON::code
	int32_t ___code_0;
	// WebAuthAPI.MarketProductJSON[] WebAuthAPI.GetProductListJSON::products
	MarketProductJSONU5BU5D_t3409632994* ___products_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(GetProductListJSON_t196412724, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_products_1() { return static_cast<int32_t>(offsetof(GetProductListJSON_t196412724, ___products_1)); }
	inline MarketProductJSONU5BU5D_t3409632994* get_products_1() const { return ___products_1; }
	inline MarketProductJSONU5BU5D_t3409632994** get_address_of_products_1() { return &___products_1; }
	inline void set_products_1(MarketProductJSONU5BU5D_t3409632994* value)
	{
		___products_1 = value;
		Il2CppCodeGenWriteBarrier((&___products_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPRODUCTLISTJSON_T196412724_H
#ifndef MARKETPRODUCTJSON_T12495859_H
#define MARKETPRODUCTJSON_T12495859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.MarketProductJSON
struct  MarketProductJSON_t12495859  : public RuntimeObject
{
public:
	// System.String WebAuthAPI.MarketProductJSON::productguid
	String_t* ___productguid_0;
	// System.String WebAuthAPI.MarketProductJSON::productname
	String_t* ___productname_1;
	// System.String WebAuthAPI.MarketProductJSON::producttype
	String_t* ___producttype_2;
	// System.String WebAuthAPI.MarketProductJSON::forbidden
	String_t* ___forbidden_3;
	// System.String WebAuthAPI.MarketProductJSON::productdesc
	String_t* ___productdesc_4;
	// System.String WebAuthAPI.MarketProductJSON::productimage
	String_t* ___productimage_5;
	// System.UInt32 WebAuthAPI.MarketProductJSON::costcoins
	uint32_t ___costcoins_6;
	// System.UInt32 WebAuthAPI.MarketProductJSON::costdiamonds
	uint32_t ___costdiamonds_7;
	// System.UInt32 WebAuthAPI.MarketProductJSON::productnumattr1
	uint32_t ___productnumattr1_8;
	// System.UInt32 WebAuthAPI.MarketProductJSON::productnumattr2
	uint32_t ___productnumattr2_9;
	// System.String WebAuthAPI.MarketProductJSON::producttxtattr1
	String_t* ___producttxtattr1_10;
	// System.String WebAuthAPI.MarketProductJSON::producttxtattr2
	String_t* ___producttxtattr2_11;

public:
	inline static int32_t get_offset_of_productguid_0() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___productguid_0)); }
	inline String_t* get_productguid_0() const { return ___productguid_0; }
	inline String_t** get_address_of_productguid_0() { return &___productguid_0; }
	inline void set_productguid_0(String_t* value)
	{
		___productguid_0 = value;
		Il2CppCodeGenWriteBarrier((&___productguid_0), value);
	}

	inline static int32_t get_offset_of_productname_1() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___productname_1)); }
	inline String_t* get_productname_1() const { return ___productname_1; }
	inline String_t** get_address_of_productname_1() { return &___productname_1; }
	inline void set_productname_1(String_t* value)
	{
		___productname_1 = value;
		Il2CppCodeGenWriteBarrier((&___productname_1), value);
	}

	inline static int32_t get_offset_of_producttype_2() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___producttype_2)); }
	inline String_t* get_producttype_2() const { return ___producttype_2; }
	inline String_t** get_address_of_producttype_2() { return &___producttype_2; }
	inline void set_producttype_2(String_t* value)
	{
		___producttype_2 = value;
		Il2CppCodeGenWriteBarrier((&___producttype_2), value);
	}

	inline static int32_t get_offset_of_forbidden_3() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___forbidden_3)); }
	inline String_t* get_forbidden_3() const { return ___forbidden_3; }
	inline String_t** get_address_of_forbidden_3() { return &___forbidden_3; }
	inline void set_forbidden_3(String_t* value)
	{
		___forbidden_3 = value;
		Il2CppCodeGenWriteBarrier((&___forbidden_3), value);
	}

	inline static int32_t get_offset_of_productdesc_4() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___productdesc_4)); }
	inline String_t* get_productdesc_4() const { return ___productdesc_4; }
	inline String_t** get_address_of_productdesc_4() { return &___productdesc_4; }
	inline void set_productdesc_4(String_t* value)
	{
		___productdesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___productdesc_4), value);
	}

	inline static int32_t get_offset_of_productimage_5() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___productimage_5)); }
	inline String_t* get_productimage_5() const { return ___productimage_5; }
	inline String_t** get_address_of_productimage_5() { return &___productimage_5; }
	inline void set_productimage_5(String_t* value)
	{
		___productimage_5 = value;
		Il2CppCodeGenWriteBarrier((&___productimage_5), value);
	}

	inline static int32_t get_offset_of_costcoins_6() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___costcoins_6)); }
	inline uint32_t get_costcoins_6() const { return ___costcoins_6; }
	inline uint32_t* get_address_of_costcoins_6() { return &___costcoins_6; }
	inline void set_costcoins_6(uint32_t value)
	{
		___costcoins_6 = value;
	}

	inline static int32_t get_offset_of_costdiamonds_7() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___costdiamonds_7)); }
	inline uint32_t get_costdiamonds_7() const { return ___costdiamonds_7; }
	inline uint32_t* get_address_of_costdiamonds_7() { return &___costdiamonds_7; }
	inline void set_costdiamonds_7(uint32_t value)
	{
		___costdiamonds_7 = value;
	}

	inline static int32_t get_offset_of_productnumattr1_8() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___productnumattr1_8)); }
	inline uint32_t get_productnumattr1_8() const { return ___productnumattr1_8; }
	inline uint32_t* get_address_of_productnumattr1_8() { return &___productnumattr1_8; }
	inline void set_productnumattr1_8(uint32_t value)
	{
		___productnumattr1_8 = value;
	}

	inline static int32_t get_offset_of_productnumattr2_9() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___productnumattr2_9)); }
	inline uint32_t get_productnumattr2_9() const { return ___productnumattr2_9; }
	inline uint32_t* get_address_of_productnumattr2_9() { return &___productnumattr2_9; }
	inline void set_productnumattr2_9(uint32_t value)
	{
		___productnumattr2_9 = value;
	}

	inline static int32_t get_offset_of_producttxtattr1_10() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___producttxtattr1_10)); }
	inline String_t* get_producttxtattr1_10() const { return ___producttxtattr1_10; }
	inline String_t** get_address_of_producttxtattr1_10() { return &___producttxtattr1_10; }
	inline void set_producttxtattr1_10(String_t* value)
	{
		___producttxtattr1_10 = value;
		Il2CppCodeGenWriteBarrier((&___producttxtattr1_10), value);
	}

	inline static int32_t get_offset_of_producttxtattr2_11() { return static_cast<int32_t>(offsetof(MarketProductJSON_t12495859, ___producttxtattr2_11)); }
	inline String_t* get_producttxtattr2_11() const { return ___producttxtattr2_11; }
	inline String_t** get_address_of_producttxtattr2_11() { return &___producttxtattr2_11; }
	inline void set_producttxtattr2_11(String_t* value)
	{
		___producttxtattr2_11 = value;
		Il2CppCodeGenWriteBarrier((&___producttxtattr2_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKETPRODUCTJSON_T12495859_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CCHARGEADDPOSTFORMU3EC__ITERATOR2_T2596936324_H
#define U3CCHARGEADDPOSTFORMU3EC__ITERATOR2_T2596936324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2
struct  U3CchargeAddPostFormU3Ec__Iterator2_t2596936324  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::phonenum
	String_t* ___phonenum_1;
	// System.String WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::idortoken
	String_t* ___idortoken_2;
	// System.Object WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::passby
	RuntimeObject * ___passby_3;
	// WebAuthAPI.ChargeProductJSON WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::<product>__0
	ChargeProductJSON_t2737157911 * ___U3CproductU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::<request>__0
	UnityWebRequest_t463507806 * ___U3CrequestU3E__0_5;
	// System.Action`2<System.Int32,System.Object> WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::callback
	Action_2_t11315885 * ___callback_6;
	// System.Object WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 WebAuthAPI.Charge/<chargeAddPostForm>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_phonenum_1() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___phonenum_1)); }
	inline String_t* get_phonenum_1() const { return ___phonenum_1; }
	inline String_t** get_address_of_phonenum_1() { return &___phonenum_1; }
	inline void set_phonenum_1(String_t* value)
	{
		___phonenum_1 = value;
		Il2CppCodeGenWriteBarrier((&___phonenum_1), value);
	}

	inline static int32_t get_offset_of_idortoken_2() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___idortoken_2)); }
	inline String_t* get_idortoken_2() const { return ___idortoken_2; }
	inline String_t** get_address_of_idortoken_2() { return &___idortoken_2; }
	inline void set_idortoken_2(String_t* value)
	{
		___idortoken_2 = value;
		Il2CppCodeGenWriteBarrier((&___idortoken_2), value);
	}

	inline static int32_t get_offset_of_passby_3() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___passby_3)); }
	inline RuntimeObject * get_passby_3() const { return ___passby_3; }
	inline RuntimeObject ** get_address_of_passby_3() { return &___passby_3; }
	inline void set_passby_3(RuntimeObject * value)
	{
		___passby_3 = value;
		Il2CppCodeGenWriteBarrier((&___passby_3), value);
	}

	inline static int32_t get_offset_of_U3CproductU3E__0_4() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___U3CproductU3E__0_4)); }
	inline ChargeProductJSON_t2737157911 * get_U3CproductU3E__0_4() const { return ___U3CproductU3E__0_4; }
	inline ChargeProductJSON_t2737157911 ** get_address_of_U3CproductU3E__0_4() { return &___U3CproductU3E__0_4; }
	inline void set_U3CproductU3E__0_4(ChargeProductJSON_t2737157911 * value)
	{
		___U3CproductU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_5() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___U3CrequestU3E__0_5)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__0_5() const { return ___U3CrequestU3E__0_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__0_5() { return &___U3CrequestU3E__0_5; }
	inline void set_U3CrequestU3E__0_5(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___callback_6)); }
	inline Action_2_t11315885 * get_callback_6() const { return ___callback_6; }
	inline Action_2_t11315885 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_2_t11315885 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CchargeAddPostFormU3Ec__Iterator2_t2596936324, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHARGEADDPOSTFORMU3EC__ITERATOR2_T2596936324_H
#ifndef SBUFFCONFIG_T769791949_H
#define SBUFFCONFIG_T769791949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuffEditor/sBuffConfig
struct  sBuffConfig_t769791949 
{
public:
	// System.Int32 CBuffEditor/sBuffConfig::id
	int32_t ___id_0;
	// System.Int32 CBuffEditor/sBuffConfig::func
	int32_t ___func_1;
	// System.Single CBuffEditor/sBuffConfig::time
	float ___time_2;
	// System.String[] CBuffEditor/sBuffConfig::aryDesc
	StringU5BU5D_t1281789340* ___aryDesc_3;
	// System.Single[] CBuffEditor/sBuffConfig::aryValues
	SingleU5BU5D_t1444911251* ___aryValues_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(sBuffConfig_t769791949, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_func_1() { return static_cast<int32_t>(offsetof(sBuffConfig_t769791949, ___func_1)); }
	inline int32_t get_func_1() const { return ___func_1; }
	inline int32_t* get_address_of_func_1() { return &___func_1; }
	inline void set_func_1(int32_t value)
	{
		___func_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(sBuffConfig_t769791949, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_aryDesc_3() { return static_cast<int32_t>(offsetof(sBuffConfig_t769791949, ___aryDesc_3)); }
	inline StringU5BU5D_t1281789340* get_aryDesc_3() const { return ___aryDesc_3; }
	inline StringU5BU5D_t1281789340** get_address_of_aryDesc_3() { return &___aryDesc_3; }
	inline void set_aryDesc_3(StringU5BU5D_t1281789340* value)
	{
		___aryDesc_3 = value;
		Il2CppCodeGenWriteBarrier((&___aryDesc_3), value);
	}

	inline static int32_t get_offset_of_aryValues_4() { return static_cast<int32_t>(offsetof(sBuffConfig_t769791949, ___aryValues_4)); }
	inline SingleU5BU5D_t1444911251* get_aryValues_4() const { return ___aryValues_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_aryValues_4() { return &___aryValues_4; }
	inline void set_aryValues_4(SingleU5BU5D_t1444911251* value)
	{
		___aryValues_4 = value;
		Il2CppCodeGenWriteBarrier((&___aryValues_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CBuffEditor/sBuffConfig
struct sBuffConfig_t769791949_marshaled_pinvoke
{
	int32_t ___id_0;
	int32_t ___func_1;
	float ___time_2;
	char** ___aryDesc_3;
	float* ___aryValues_4;
};
// Native definition for COM marshalling of CBuffEditor/sBuffConfig
struct sBuffConfig_t769791949_marshaled_com
{
	int32_t ___id_0;
	int32_t ___func_1;
	float ___time_2;
	Il2CppChar** ___aryDesc_3;
	float* ___aryValues_4;
};
#endif // SBUFFCONFIG_T769791949_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef STHORNOFTHISCLASSCONFIG_T350464872_H
#define STHORNOFTHISCLASSCONFIG_T350464872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CClassEditor/sThornOfThisClassConfig
struct  sThornOfThisClassConfig_t350464872 
{
public:
	// System.String CClassEditor/sThornOfThisClassConfig::szThornId
	String_t* ___szThornId_0;
	// System.Single CClassEditor/sThornOfThisClassConfig::fDensity
	float ___fDensity_1;
	// System.Single CClassEditor/sThornOfThisClassConfig::fRebornTime
	float ___fRebornTime_2;

public:
	inline static int32_t get_offset_of_szThornId_0() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___szThornId_0)); }
	inline String_t* get_szThornId_0() const { return ___szThornId_0; }
	inline String_t** get_address_of_szThornId_0() { return &___szThornId_0; }
	inline void set_szThornId_0(String_t* value)
	{
		___szThornId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szThornId_0), value);
	}

	inline static int32_t get_offset_of_fDensity_1() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___fDensity_1)); }
	inline float get_fDensity_1() const { return ___fDensity_1; }
	inline float* get_address_of_fDensity_1() { return &___fDensity_1; }
	inline void set_fDensity_1(float value)
	{
		___fDensity_1 = value;
	}

	inline static int32_t get_offset_of_fRebornTime_2() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___fRebornTime_2)); }
	inline float get_fRebornTime_2() const { return ___fRebornTime_2; }
	inline float* get_address_of_fRebornTime_2() { return &___fRebornTime_2; }
	inline void set_fRebornTime_2(float value)
	{
		___fRebornTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CClassEditor/sThornOfThisClassConfig
struct sThornOfThisClassConfig_t350464872_marshaled_pinvoke
{
	char* ___szThornId_0;
	float ___fDensity_1;
	float ___fRebornTime_2;
};
// Native definition for COM marshalling of CClassEditor/sThornOfThisClassConfig
struct sThornOfThisClassConfig_t350464872_marshaled_com
{
	Il2CppChar* ___szThornId_0;
	float ___fDensity_1;
	float ___fRebornTime_2;
};
#endif // STHORNOFTHISCLASSCONFIG_T350464872_H
#ifndef STHORNCONFIG_T1242444451_H
#define STHORNCONFIG_T1242444451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/sThornConfig
struct  sThornConfig_t1242444451 
{
public:
	// System.String CMonsterEditor/sThornConfig::szId
	String_t* ___szId_0;
	// System.Int32 CMonsterEditor/sThornConfig::nType
	int32_t ___nType_1;
	// System.Single CMonsterEditor/sThornConfig::fFoodSize
	float ___fFoodSize_2;
	// System.String CMonsterEditor/sThornConfig::szColor
	String_t* ___szColor_3;
	// System.String CMonsterEditor/sThornConfig::szDesc
	String_t* ___szDesc_4;
	// System.Single CMonsterEditor/sThornConfig::fSelfSize
	float ___fSelfSize_5;
	// System.Single CMonsterEditor/sThornConfig::fExp
	float ___fExp_6;
	// System.Single CMonsterEditor/sThornConfig::fMoney
	float ___fMoney_7;
	// System.Single CMonsterEditor/sThornConfig::fBuffId
	float ___fBuffId_8;
	// System.Single CMonsterEditor/sThornConfig::fExplodeChildNum
	float ___fExplodeChildNum_9;
	// System.Single CMonsterEditor/sThornConfig::fExplodeMotherLeftPercent
	float ___fExplodeMotherLeftPercent_10;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunDistance
	float ___fExplodeRunDistance_11;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunTime
	float ___fExplodeRunTime_12;
	// System.Single CMonsterEditor/sThornConfig::fExplodeStayTime
	float ___fExplodeStayTime_13;
	// System.Single CMonsterEditor/sThornConfig::fExplodeShellTime
	float ___fExplodeShellTime_14;
	// System.Single CMonsterEditor/sThornConfig::fExplodeFormationId
	float ___fExplodeFormationId_15;
	// System.Int32 CMonsterEditor/sThornConfig::nSkillId
	int32_t ___nSkillId_16;

public:
	inline static int32_t get_offset_of_szId_0() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szId_0)); }
	inline String_t* get_szId_0() const { return ___szId_0; }
	inline String_t** get_address_of_szId_0() { return &___szId_0; }
	inline void set_szId_0(String_t* value)
	{
		___szId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szId_0), value);
	}

	inline static int32_t get_offset_of_nType_1() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nType_1)); }
	inline int32_t get_nType_1() const { return ___nType_1; }
	inline int32_t* get_address_of_nType_1() { return &___nType_1; }
	inline void set_nType_1(int32_t value)
	{
		___nType_1 = value;
	}

	inline static int32_t get_offset_of_fFoodSize_2() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fFoodSize_2)); }
	inline float get_fFoodSize_2() const { return ___fFoodSize_2; }
	inline float* get_address_of_fFoodSize_2() { return &___fFoodSize_2; }
	inline void set_fFoodSize_2(float value)
	{
		___fFoodSize_2 = value;
	}

	inline static int32_t get_offset_of_szColor_3() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szColor_3)); }
	inline String_t* get_szColor_3() const { return ___szColor_3; }
	inline String_t** get_address_of_szColor_3() { return &___szColor_3; }
	inline void set_szColor_3(String_t* value)
	{
		___szColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___szColor_3), value);
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_fSelfSize_5() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fSelfSize_5)); }
	inline float get_fSelfSize_5() const { return ___fSelfSize_5; }
	inline float* get_address_of_fSelfSize_5() { return &___fSelfSize_5; }
	inline void set_fSelfSize_5(float value)
	{
		___fSelfSize_5 = value;
	}

	inline static int32_t get_offset_of_fExp_6() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExp_6)); }
	inline float get_fExp_6() const { return ___fExp_6; }
	inline float* get_address_of_fExp_6() { return &___fExp_6; }
	inline void set_fExp_6(float value)
	{
		___fExp_6 = value;
	}

	inline static int32_t get_offset_of_fMoney_7() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fMoney_7)); }
	inline float get_fMoney_7() const { return ___fMoney_7; }
	inline float* get_address_of_fMoney_7() { return &___fMoney_7; }
	inline void set_fMoney_7(float value)
	{
		___fMoney_7 = value;
	}

	inline static int32_t get_offset_of_fBuffId_8() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fBuffId_8)); }
	inline float get_fBuffId_8() const { return ___fBuffId_8; }
	inline float* get_address_of_fBuffId_8() { return &___fBuffId_8; }
	inline void set_fBuffId_8(float value)
	{
		___fBuffId_8 = value;
	}

	inline static int32_t get_offset_of_fExplodeChildNum_9() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeChildNum_9)); }
	inline float get_fExplodeChildNum_9() const { return ___fExplodeChildNum_9; }
	inline float* get_address_of_fExplodeChildNum_9() { return &___fExplodeChildNum_9; }
	inline void set_fExplodeChildNum_9(float value)
	{
		___fExplodeChildNum_9 = value;
	}

	inline static int32_t get_offset_of_fExplodeMotherLeftPercent_10() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeMotherLeftPercent_10)); }
	inline float get_fExplodeMotherLeftPercent_10() const { return ___fExplodeMotherLeftPercent_10; }
	inline float* get_address_of_fExplodeMotherLeftPercent_10() { return &___fExplodeMotherLeftPercent_10; }
	inline void set_fExplodeMotherLeftPercent_10(float value)
	{
		___fExplodeMotherLeftPercent_10 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunDistance_11() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunDistance_11)); }
	inline float get_fExplodeRunDistance_11() const { return ___fExplodeRunDistance_11; }
	inline float* get_address_of_fExplodeRunDistance_11() { return &___fExplodeRunDistance_11; }
	inline void set_fExplodeRunDistance_11(float value)
	{
		___fExplodeRunDistance_11 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunTime_12() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunTime_12)); }
	inline float get_fExplodeRunTime_12() const { return ___fExplodeRunTime_12; }
	inline float* get_address_of_fExplodeRunTime_12() { return &___fExplodeRunTime_12; }
	inline void set_fExplodeRunTime_12(float value)
	{
		___fExplodeRunTime_12 = value;
	}

	inline static int32_t get_offset_of_fExplodeStayTime_13() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeStayTime_13)); }
	inline float get_fExplodeStayTime_13() const { return ___fExplodeStayTime_13; }
	inline float* get_address_of_fExplodeStayTime_13() { return &___fExplodeStayTime_13; }
	inline void set_fExplodeStayTime_13(float value)
	{
		___fExplodeStayTime_13 = value;
	}

	inline static int32_t get_offset_of_fExplodeShellTime_14() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeShellTime_14)); }
	inline float get_fExplodeShellTime_14() const { return ___fExplodeShellTime_14; }
	inline float* get_address_of_fExplodeShellTime_14() { return &___fExplodeShellTime_14; }
	inline void set_fExplodeShellTime_14(float value)
	{
		___fExplodeShellTime_14 = value;
	}

	inline static int32_t get_offset_of_fExplodeFormationId_15() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeFormationId_15)); }
	inline float get_fExplodeFormationId_15() const { return ___fExplodeFormationId_15; }
	inline float* get_address_of_fExplodeFormationId_15() { return &___fExplodeFormationId_15; }
	inline void set_fExplodeFormationId_15(float value)
	{
		___fExplodeFormationId_15 = value;
	}

	inline static int32_t get_offset_of_nSkillId_16() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nSkillId_16)); }
	inline int32_t get_nSkillId_16() const { return ___nSkillId_16; }
	inline int32_t* get_address_of_nSkillId_16() { return &___nSkillId_16; }
	inline void set_nSkillId_16(int32_t value)
	{
		___nSkillId_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_pinvoke
{
	char* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	char* ___szColor_3;
	char* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
// Native definition for COM marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_com
{
	Il2CppChar* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	Il2CppChar* ___szColor_3;
	Il2CppChar* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
#endif // STHORNCONFIG_T1242444451_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef KEYVALUEPAIR_2_T2314811335_H
#define KEYVALUEPAIR_2_T2314811335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Resource>
struct  KeyValuePair_2_t2314811335 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Resource_t131882869 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2314811335, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2314811335, ___value_1)); }
	inline Resource_t131882869 * get_value_1() const { return ___value_1; }
	inline Resource_t131882869 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Resource_t131882869 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2314811335_H
#ifndef REMOTERESOURCELOADER_T1429902794_H
#define REMOTERESOURCELOADER_T1429902794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteResourceLoader
struct  RemoteResourceLoader_t1429902794  : public ResourceLoader_t3764463624
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTERESOURCELOADER_T1429902794_H
#ifndef LOCALERESOURCELOADER_T808386688_H
#define LOCALERESOURCELOADER_T808386688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocaleResourceLoader
struct  LocaleResourceLoader_t808386688  : public ResourceLoader_t3764463624
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALERESOURCELOADER_T808386688_H
#ifndef PROGRESSEVENTARGS_T1844038154_H
#define PROGRESSEVENTARGS_T1844038154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressEventArgs
struct  ProgressEventArgs_t1844038154  : public EventArgs_t3591816995
{
public:
	// ResourceLoader ProgressEventArgs::loader
	ResourceLoader_t3764463624 * ___loader_1;
	// Resource ProgressEventArgs::resource
	Resource_t131882869 * ___resource_2;

public:
	inline static int32_t get_offset_of_loader_1() { return static_cast<int32_t>(offsetof(ProgressEventArgs_t1844038154, ___loader_1)); }
	inline ResourceLoader_t3764463624 * get_loader_1() const { return ___loader_1; }
	inline ResourceLoader_t3764463624 ** get_address_of_loader_1() { return &___loader_1; }
	inline void set_loader_1(ResourceLoader_t3764463624 * value)
	{
		___loader_1 = value;
		Il2CppCodeGenWriteBarrier((&___loader_1), value);
	}

	inline static int32_t get_offset_of_resource_2() { return static_cast<int32_t>(offsetof(ProgressEventArgs_t1844038154, ___resource_2)); }
	inline Resource_t131882869 * get_resource_2() const { return ___resource_2; }
	inline Resource_t131882869 ** get_address_of_resource_2() { return &___resource_2; }
	inline void set_resource_2(Resource_t131882869 * value)
	{
		___resource_2 = value;
		Il2CppCodeGenWriteBarrier((&___resource_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSEVENTARGS_T1844038154_H
#ifndef ECAMERMOVESTATUS_T487596215_H
#define ECAMERMOVESTATUS_T487596215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CtrlMode/eCamerMoveStatus
struct  eCamerMoveStatus_t487596215 
{
public:
	// System.Int32 CtrlMode/eCamerMoveStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCamerMoveStatus_t487596215, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECAMERMOVESTATUS_T487596215_H
#ifndef EBUFFTYPE_T2377588228_H
#define EBUFFTYPE_T2377588228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuff/eBuffType
struct  eBuffType_t2377588228 
{
public:
	// System.Int32 CBuff/eBuffType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBuffType_t2377588228, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBUFFTYPE_T2377588228_H
#ifndef SINITBEANNODE_T1761059340_H
#define SINITBEANNODE_T1761059340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBeanManager/sInitBeanNode
struct  sInitBeanNode_t1761059340 
{
public:
	// CMonsterEditor/sThornConfig CBeanManager/sInitBeanNode::config
	sThornConfig_t1242444451  ___config_0;
	// System.String[] CBeanManager/sInitBeanNode::aryPos
	StringU5BU5D_t1281789340* ___aryPos_1;
	// CClassEditor/sThornOfThisClassConfig CBeanManager/sInitBeanNode::class_config
	sThornOfThisClassConfig_t350464872  ___class_config_2;
	// System.Int32 CBeanManager/sInitBeanNode::nClassId
	int32_t ___nClassId_3;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(sInitBeanNode_t1761059340, ___config_0)); }
	inline sThornConfig_t1242444451  get_config_0() const { return ___config_0; }
	inline sThornConfig_t1242444451 * get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(sThornConfig_t1242444451  value)
	{
		___config_0 = value;
	}

	inline static int32_t get_offset_of_aryPos_1() { return static_cast<int32_t>(offsetof(sInitBeanNode_t1761059340, ___aryPos_1)); }
	inline StringU5BU5D_t1281789340* get_aryPos_1() const { return ___aryPos_1; }
	inline StringU5BU5D_t1281789340** get_address_of_aryPos_1() { return &___aryPos_1; }
	inline void set_aryPos_1(StringU5BU5D_t1281789340* value)
	{
		___aryPos_1 = value;
		Il2CppCodeGenWriteBarrier((&___aryPos_1), value);
	}

	inline static int32_t get_offset_of_class_config_2() { return static_cast<int32_t>(offsetof(sInitBeanNode_t1761059340, ___class_config_2)); }
	inline sThornOfThisClassConfig_t350464872  get_class_config_2() const { return ___class_config_2; }
	inline sThornOfThisClassConfig_t350464872 * get_address_of_class_config_2() { return &___class_config_2; }
	inline void set_class_config_2(sThornOfThisClassConfig_t350464872  value)
	{
		___class_config_2 = value;
	}

	inline static int32_t get_offset_of_nClassId_3() { return static_cast<int32_t>(offsetof(sInitBeanNode_t1761059340, ___nClassId_3)); }
	inline int32_t get_nClassId_3() const { return ___nClassId_3; }
	inline int32_t* get_address_of_nClassId_3() { return &___nClassId_3; }
	inline void set_nClassId_3(int32_t value)
	{
		___nClassId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CBeanManager/sInitBeanNode
struct sInitBeanNode_t1761059340_marshaled_pinvoke
{
	sThornConfig_t1242444451_marshaled_pinvoke ___config_0;
	char** ___aryPos_1;
	sThornOfThisClassConfig_t350464872_marshaled_pinvoke ___class_config_2;
	int32_t ___nClassId_3;
};
// Native definition for COM marshalling of CBeanManager/sInitBeanNode
struct sInitBeanNode_t1761059340_marshaled_com
{
	sThornConfig_t1242444451_marshaled_com ___config_0;
	Il2CppChar** ___aryPos_1;
	sThornOfThisClassConfig_t350464872_marshaled_com ___class_config_2;
	int32_t ___nClassId_3;
};
#endif // SINITBEANNODE_T1761059340_H
#ifndef ESKILLID_T2951149716_H
#define ESKILLID_T2951149716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/eSkillId
struct  eSkillId_t2951149716 
{
public:
	// System.Int32 CSkillSystem/eSkillId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillId_t2951149716, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLID_T2951149716_H
#ifndef EFADEMODE_T2947864873_H
#define EFADEMODE_T2947864873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBloodStainManager/eFadeMode
struct  eFadeMode_t2947864873 
{
public:
	// System.Int32 CBloodStainManager/eFadeMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eFadeMode_t2947864873, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFADEMODE_T2947864873_H
#ifndef EBUFFFUNC_T3332395474_H
#define EBUFFFUNC_T3332395474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuffEditor/eBuffFunc
struct  eBuffFunc_t3332395474 
{
public:
	// System.Int32 CBuffEditor/eBuffFunc::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBuffFunc_t3332395474, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBUFFFUNC_T3332395474_H
#ifndef NULLABLE_1_T4278248406_H
#define NULLABLE_1_T4278248406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_t4278248406 
{
public:
	// T System.Nullable`1::value
	Color_t2555686324  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4278248406, ___value_0)); }
	inline Color_t2555686324  get_value_0() const { return ___value_0; }
	inline Color_t2555686324 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t2555686324  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4278248406, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4278248406_H
#ifndef EDEVICETYPE_T2574412910_H
#define EDEVICETYPE_T2574412910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAdaptiveManager/eDeviceTYpe
struct  eDeviceTYpe_t2574412910 
{
public:
	// System.Int32 CAdaptiveManager/eDeviceTYpe::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eDeviceTYpe_t2574412910, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDEVICETYPE_T2574412910_H
#ifndef LOADRESULT_T1889652257_H
#define LOADRESULT_T1889652257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadResult
struct  LoadResult_t1889652257 
{
public:
	// System.SByte LoadResult::value__
	int8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadResult_t1889652257, ___value___1)); }
	inline int8_t get_value___1() const { return ___value___1; }
	inline int8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADRESULT_T1889652257_H
#ifndef ENUMERATOR_T1871321943_H
#define ENUMERATOR_T1871321943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Resource>
struct  Enumerator_t1871321943 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t4212106464 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2314811335  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1871321943, ___dictionary_0)); }
	inline Dictionary_2_t4212106464 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t4212106464 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t4212106464 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1871321943, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1871321943, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1871321943, ___current_3)); }
	inline KeyValuePair_2_t2314811335  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2314811335 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2314811335  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1871321943_H
#ifndef ECAMERSIZEPROTECTSTATUS_T2037340949_H
#define ECAMERSIZEPROTECTSTATUS_T2037340949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CtrlMode/eCamerSizeProtectStatus
struct  eCamerSizeProtectStatus_t2037340949 
{
public:
	// System.Int32 CtrlMode/eCamerSizeProtectStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCamerSizeProtectStatus_t2037340949, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECAMERSIZEPROTECTSTATUS_T2037340949_H
#ifndef EDYNAMICEFFECTTYPE_T713621222_H
#define EDYNAMICEFFECTTYPE_T713621222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBloodStainManager/eDynamicEffectType
struct  eDynamicEffectType_t713621222 
{
public:
	// System.Int32 CBloodStainManager/eDynamicEffectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eDynamicEffectType_t713621222, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDYNAMICEFFECTTYPE_T713621222_H
#ifndef ELAYERID_T1442111487_H
#define ELAYERID_T1442111487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLayerManager/eLayerId
struct  eLayerId_t1442111487 
{
public:
	// System.Int32 CLayerManager/eLayerId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eLayerId_t1442111487, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELAYERID_T1442111487_H
#ifndef EMAINBG_T3769614300_H
#define EMAINBG_T3769614300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAudioManager/eMainBg
struct  eMainBg_t3769614300 
{
public:
	// System.Int32 CAudioManager/eMainBg::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMainBg_t3769614300, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAINBG_T3769614300_H
#ifndef ESE_T1337385684_H
#define ESE_T1337385684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAudioManager/eSE
struct  eSE_t1337385684 
{
public:
	// System.Int32 CAudioManager/eSE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSE_t1337385684, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESE_T1337385684_H
#ifndef EVOICE_T2630767850_H
#define EVOICE_T2630767850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAudioManager/eVoice
struct  eVoice_t2630767850 
{
public:
	// System.Int32 CAudioManager/eVoice::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eVoice_t2630767850, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVOICE_T2630767850_H
#ifndef EAUDIOID_T13996765_H
#define EAUDIOID_T13996765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAudioManager/eAudioId
struct  eAudioId_t13996765 
{
public:
	// System.Int32 CAudioManager/eAudioId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eAudioId_t13996765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAUDIOID_T13996765_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef EMONSTERTYPE_T4095993688_H
#define EMONSTERTYPE_T4095993688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonster/eMonsterType
struct  eMonsterType_t4095993688 
{
public:
	// System.Int32 CMonster/eMonsterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMonsterType_t4095993688, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONSTERTYPE_T4095993688_H
#ifndef RESOURCETYPE_T3261224423_H
#define RESOURCETYPE_T3261224423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceType
struct  ResourceType_t3261224423 
{
public:
	// System.SByte ResourceType::value__
	int8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResourceType_t3261224423, ___value___1)); }
	inline int8_t get_value___1() const { return ___value___1; }
	inline int8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCETYPE_T3261224423_H
#ifndef EBLOCKPOSTYPE_T149092435_H
#define EBLOCKPOSTYPE_T149092435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBlockDivide/eBlockPosType
struct  eBlockPosType_t149092435 
{
public:
	// System.Int32 CBlockDivide/eBlockPosType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBlockPosType_t149092435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBLOCKPOSTYPE_T149092435_H
#ifndef EBLOODSTAINTYPE_T333536540_H
#define EBLOODSTAINTYPE_T333536540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBloodStainManager/eBloodStainType
struct  eBloodStainType_t333536540 
{
public:
	// System.Int32 CBloodStainManager/eBloodStainType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBloodStainType_t333536540, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBLOODSTAINTYPE_T333536540_H
#ifndef EMONSTERBUILDINGTYPE_T3379255469_H
#define EMONSTERBUILDINGTYPE_T3379255469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/eMonsterBuildingType
struct  eMonsterBuildingType_t3379255469 
{
public:
	// System.Int32 CMonsterEditor/eMonsterBuildingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMonsterBuildingType_t3379255469, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONSTERBUILDINGTYPE_T3379255469_H
#ifndef RESOURCE_T131882869_H
#define RESOURCE_T131882869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Resource
struct  Resource_t131882869  : public RuntimeObject
{
public:
	// System.String Resource::name
	String_t* ___name_0;
	// System.String Resource::url
	String_t* ___url_1;
	// ResourceType Resource::type
	int8_t ___type_2;
	// System.String Resource::text
	String_t* ___text_3;
	// System.Byte[] Resource::data
	ByteU5BU5D_t4116647657* ___data_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Resource_t131882869, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(Resource_t131882869, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Resource_t131882869, ___type_2)); }
	inline int8_t get_type_2() const { return ___type_2; }
	inline int8_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int8_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(Resource_t131882869, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(Resource_t131882869, ___data_4)); }
	inline ByteU5BU5D_t4116647657* get_data_4() const { return ___data_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ByteU5BU5D_t4116647657* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCE_T131882869_H
#ifndef U3CLOADU3EC__ITERATOR0_T1446289385_H
#define U3CLOADU3EC__ITERATOR0_T1446289385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocaleResourceLoader/<load>c__Iterator0
struct  U3CloadU3Ec__Iterator0_t1446289385  : public RuntimeObject
{
public:
	// UnityEngine.AsyncOperation LocaleResourceLoader/<load>c__Iterator0::<asyncOperation>__0
	AsyncOperation_t1445031843 * ___U3CasyncOperationU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest LocaleResourceLoader/<load>c__Iterator0::<webRequest>__0
	UnityWebRequest_t463507806 * ___U3CwebRequestU3E__0_1;
	// UnityEngine.AssetBundleCreateRequest LocaleResourceLoader/<load>c__Iterator0::<fileRequest>__0
	AssetBundleCreateRequest_t3119663542 * ___U3CfileRequestU3E__0_2;
	// ResourceBundleList LocaleResourceLoader/<load>c__Iterator0::<resourceBundleList>__0
	ResourceBundleList_t987847975 * ___U3CresourceBundleListU3E__0_3;
	// System.String LocaleResourceLoader/<load>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_4;
	// UnityEngine.AssetBundleManifest LocaleResourceLoader/<load>c__Iterator0::<manifest>__1
	AssetBundleManifest_t2634949939 * ___U3CmanifestU3E__1_5;
	// System.String[] LocaleResourceLoader/<load>c__Iterator0::<bundleNames>__1
	StringU5BU5D_t1281789340* ___U3CbundleNamesU3E__1_6;
	// System.Int32 LocaleResourceLoader/<load>c__Iterator0::<counter>__1
	int32_t ___U3CcounterU3E__1_7;
	// ResourceBundle LocaleResourceLoader/<load>c__Iterator0::<resourceBundle>__1
	ResourceBundle_t1032369214 * ___U3CresourceBundleU3E__1_8;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Resource> LocaleResourceLoader/<load>c__Iterator0::$locvar0
	Enumerator_t1871321943  ___U24locvar0_9;
	// System.Collections.Generic.KeyValuePair`2<System.String,Resource> LocaleResourceLoader/<load>c__Iterator0::<pair>__2
	KeyValuePair_2_t2314811335  ___U3CpairU3E__2_10;
	// Resource LocaleResourceLoader/<load>c__Iterator0::<resource>__3
	Resource_t131882869 * ___U3CresourceU3E__3_11;
	// System.String LocaleResourceLoader/<load>c__Iterator0::<hashSavePath>__4
	String_t* ___U3ChashSavePathU3E__4_12;
	// System.String LocaleResourceLoader/<load>c__Iterator0::<fileSavePath>__4
	String_t* ___U3CfileSavePathU3E__4_13;
	// LocaleResourceLoader LocaleResourceLoader/<load>c__Iterator0::$this
	LocaleResourceLoader_t808386688 * ___U24this_14;
	// System.Object LocaleResourceLoader/<load>c__Iterator0::$current
	RuntimeObject * ___U24current_15;
	// System.Boolean LocaleResourceLoader/<load>c__Iterator0::$disposing
	bool ___U24disposing_16;
	// System.Int32 LocaleResourceLoader/<load>c__Iterator0::$PC
	int32_t ___U24PC_17;

public:
	inline static int32_t get_offset_of_U3CasyncOperationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CasyncOperationU3E__0_0)); }
	inline AsyncOperation_t1445031843 * get_U3CasyncOperationU3E__0_0() const { return ___U3CasyncOperationU3E__0_0; }
	inline AsyncOperation_t1445031843 ** get_address_of_U3CasyncOperationU3E__0_0() { return &___U3CasyncOperationU3E__0_0; }
	inline void set_U3CasyncOperationU3E__0_0(AsyncOperation_t1445031843 * value)
	{
		___U3CasyncOperationU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncOperationU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwebRequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CwebRequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwebRequestU3E__0_1() const { return ___U3CwebRequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwebRequestU3E__0_1() { return &___U3CwebRequestU3E__0_1; }
	inline void set_U3CwebRequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwebRequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebRequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CfileRequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CfileRequestU3E__0_2)); }
	inline AssetBundleCreateRequest_t3119663542 * get_U3CfileRequestU3E__0_2() const { return ___U3CfileRequestU3E__0_2; }
	inline AssetBundleCreateRequest_t3119663542 ** get_address_of_U3CfileRequestU3E__0_2() { return &___U3CfileRequestU3E__0_2; }
	inline void set_U3CfileRequestU3E__0_2(AssetBundleCreateRequest_t3119663542 * value)
	{
		___U3CfileRequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileRequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CresourceBundleListU3E__0_3() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CresourceBundleListU3E__0_3)); }
	inline ResourceBundleList_t987847975 * get_U3CresourceBundleListU3E__0_3() const { return ___U3CresourceBundleListU3E__0_3; }
	inline ResourceBundleList_t987847975 ** get_address_of_U3CresourceBundleListU3E__0_3() { return &___U3CresourceBundleListU3E__0_3; }
	inline void set_U3CresourceBundleListU3E__0_3(ResourceBundleList_t987847975 * value)
	{
		___U3CresourceBundleListU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourceBundleListU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_4() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CurlU3E__0_4)); }
	inline String_t* get_U3CurlU3E__0_4() const { return ___U3CurlU3E__0_4; }
	inline String_t** get_address_of_U3CurlU3E__0_4() { return &___U3CurlU3E__0_4; }
	inline void set_U3CurlU3E__0_4(String_t* value)
	{
		___U3CurlU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CmanifestU3E__1_5() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CmanifestU3E__1_5)); }
	inline AssetBundleManifest_t2634949939 * get_U3CmanifestU3E__1_5() const { return ___U3CmanifestU3E__1_5; }
	inline AssetBundleManifest_t2634949939 ** get_address_of_U3CmanifestU3E__1_5() { return &___U3CmanifestU3E__1_5; }
	inline void set_U3CmanifestU3E__1_5(AssetBundleManifest_t2634949939 * value)
	{
		___U3CmanifestU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanifestU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CbundleNamesU3E__1_6() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CbundleNamesU3E__1_6)); }
	inline StringU5BU5D_t1281789340* get_U3CbundleNamesU3E__1_6() const { return ___U3CbundleNamesU3E__1_6; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CbundleNamesU3E__1_6() { return &___U3CbundleNamesU3E__1_6; }
	inline void set_U3CbundleNamesU3E__1_6(StringU5BU5D_t1281789340* value)
	{
		___U3CbundleNamesU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbundleNamesU3E__1_6), value);
	}

	inline static int32_t get_offset_of_U3CcounterU3E__1_7() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CcounterU3E__1_7)); }
	inline int32_t get_U3CcounterU3E__1_7() const { return ___U3CcounterU3E__1_7; }
	inline int32_t* get_address_of_U3CcounterU3E__1_7() { return &___U3CcounterU3E__1_7; }
	inline void set_U3CcounterU3E__1_7(int32_t value)
	{
		___U3CcounterU3E__1_7 = value;
	}

	inline static int32_t get_offset_of_U3CresourceBundleU3E__1_8() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CresourceBundleU3E__1_8)); }
	inline ResourceBundle_t1032369214 * get_U3CresourceBundleU3E__1_8() const { return ___U3CresourceBundleU3E__1_8; }
	inline ResourceBundle_t1032369214 ** get_address_of_U3CresourceBundleU3E__1_8() { return &___U3CresourceBundleU3E__1_8; }
	inline void set_U3CresourceBundleU3E__1_8(ResourceBundle_t1032369214 * value)
	{
		___U3CresourceBundleU3E__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourceBundleU3E__1_8), value);
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U24locvar0_9)); }
	inline Enumerator_t1871321943  get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline Enumerator_t1871321943 * get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(Enumerator_t1871321943  value)
	{
		___U24locvar0_9 = value;
	}

	inline static int32_t get_offset_of_U3CpairU3E__2_10() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CpairU3E__2_10)); }
	inline KeyValuePair_2_t2314811335  get_U3CpairU3E__2_10() const { return ___U3CpairU3E__2_10; }
	inline KeyValuePair_2_t2314811335 * get_address_of_U3CpairU3E__2_10() { return &___U3CpairU3E__2_10; }
	inline void set_U3CpairU3E__2_10(KeyValuePair_2_t2314811335  value)
	{
		___U3CpairU3E__2_10 = value;
	}

	inline static int32_t get_offset_of_U3CresourceU3E__3_11() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CresourceU3E__3_11)); }
	inline Resource_t131882869 * get_U3CresourceU3E__3_11() const { return ___U3CresourceU3E__3_11; }
	inline Resource_t131882869 ** get_address_of_U3CresourceU3E__3_11() { return &___U3CresourceU3E__3_11; }
	inline void set_U3CresourceU3E__3_11(Resource_t131882869 * value)
	{
		___U3CresourceU3E__3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourceU3E__3_11), value);
	}

	inline static int32_t get_offset_of_U3ChashSavePathU3E__4_12() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3ChashSavePathU3E__4_12)); }
	inline String_t* get_U3ChashSavePathU3E__4_12() const { return ___U3ChashSavePathU3E__4_12; }
	inline String_t** get_address_of_U3ChashSavePathU3E__4_12() { return &___U3ChashSavePathU3E__4_12; }
	inline void set_U3ChashSavePathU3E__4_12(String_t* value)
	{
		___U3ChashSavePathU3E__4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChashSavePathU3E__4_12), value);
	}

	inline static int32_t get_offset_of_U3CfileSavePathU3E__4_13() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U3CfileSavePathU3E__4_13)); }
	inline String_t* get_U3CfileSavePathU3E__4_13() const { return ___U3CfileSavePathU3E__4_13; }
	inline String_t** get_address_of_U3CfileSavePathU3E__4_13() { return &___U3CfileSavePathU3E__4_13; }
	inline void set_U3CfileSavePathU3E__4_13(String_t* value)
	{
		___U3CfileSavePathU3E__4_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileSavePathU3E__4_13), value);
	}

	inline static int32_t get_offset_of_U24this_14() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U24this_14)); }
	inline LocaleResourceLoader_t808386688 * get_U24this_14() const { return ___U24this_14; }
	inline LocaleResourceLoader_t808386688 ** get_address_of_U24this_14() { return &___U24this_14; }
	inline void set_U24this_14(LocaleResourceLoader_t808386688 * value)
	{
		___U24this_14 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_14), value);
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U24current_15)); }
	inline RuntimeObject * get_U24current_15() const { return ___U24current_15; }
	inline RuntimeObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(RuntimeObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_15), value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1446289385, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3EC__ITERATOR0_T1446289385_H
#ifndef LOADEDEVENTARGS_T3630200431_H
#define LOADEDEVENTARGS_T3630200431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadedEventArgs
struct  LoadedEventArgs_t3630200431  : public EventArgs_t3591816995
{
public:
	// ResourceLoader LoadedEventArgs::loader
	ResourceLoader_t3764463624 * ___loader_1;
	// LoadResult LoadedEventArgs::result
	int8_t ___result_2;

public:
	inline static int32_t get_offset_of_loader_1() { return static_cast<int32_t>(offsetof(LoadedEventArgs_t3630200431, ___loader_1)); }
	inline ResourceLoader_t3764463624 * get_loader_1() const { return ___loader_1; }
	inline ResourceLoader_t3764463624 ** get_address_of_loader_1() { return &___loader_1; }
	inline void set_loader_1(ResourceLoader_t3764463624 * value)
	{
		___loader_1 = value;
		Il2CppCodeGenWriteBarrier((&___loader_1), value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(LoadedEventArgs_t3630200431, ___result_2)); }
	inline int8_t get_result_2() const { return ___result_2; }
	inline int8_t* get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(int8_t value)
	{
		___result_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADEDEVENTARGS_T3630200431_H
#ifndef CTRLMODE_T3902354508_H
#define CTRLMODE_T3902354508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CtrlMode
struct  CtrlMode_t3902354508  : public RuntimeObject
{
public:
	// System.Boolean CtrlMode::m_bFirstFuck
	bool ___m_bFirstFuck_20;

public:
	inline static int32_t get_offset_of_m_bFirstFuck_20() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508, ___m_bFirstFuck_20)); }
	inline bool get_m_bFirstFuck_20() const { return ___m_bFirstFuck_20; }
	inline bool* get_address_of_m_bFirstFuck_20() { return &___m_bFirstFuck_20; }
	inline void set_m_bFirstFuck_20(bool value)
	{
		___m_bFirstFuck_20 = value;
	}
};

struct CtrlMode_t3902354508_StaticFields
{
public:
	// System.Int32 CtrlMode::_ctrl_mode
	int32_t ____ctrl_mode_7;
	// System.Int32 CtrlMode::_dir_indicator_type
	int32_t ____dir_indicator_type_8;
	// System.Single CtrlMode::_ball_scale_zoom_factor
	float ____ball_scale_zoom_factor_11;
	// System.Single CtrlMode::_world_port_h
	float ____world_port_h_12;
	// System.Single CtrlMode::_world_port_v
	float ____world_port_v_13;
	// System.Single CtrlMode::_zoom_velocity
	float ____zoom_velocity_15;
	// System.Single CtrlMode::_zoom_time
	float ____zoom_time_16;
	// UnityEngine.Vector3 CtrlMode::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_17;
	// UnityEngine.Vector3 CtrlMode::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_18;
	// CtrlMode/eCamerMoveStatus CtrlMode::s_eCamMoveStatus
	int32_t ___s_eCamMoveStatus_21;
	// System.Single CtrlMode::m_fAcctackRadiusMultiple
	float ___m_fAcctackRadiusMultiple_22;
	// System.Single CtrlMode::m_fSpitBallSpeed
	float ___m_fSpitBallSpeed_23;
	// Ball CtrlMode::ballLeft
	Ball_t2206666566 * ___ballLeft_24;
	// Ball CtrlMode::ballRight
	Ball_t2206666566 * ___ballRight_25;
	// Ball CtrlMode::ballTop
	Ball_t2206666566 * ___ballTop_26;
	// Ball CtrlMode::ballBottom
	Ball_t2206666566 * ___ballBottom_27;
	// System.Single CtrlMode::fLeftBorderPos
	float ___fLeftBorderPos_28;
	// System.Single CtrlMode::fRightBorderPos
	float ___fRightBorderPos_29;
	// System.Single CtrlMode::fTopBorderPos
	float ___fTopBorderPos_30;
	// System.Single CtrlMode::fBottomBorderPos
	float ___fBottomBorderPos_31;
	// UnityEngine.Vector3 CtrlMode::s_balls_min_position
	Vector3_t3722313464  ___s_balls_min_position_32;
	// UnityEngine.Vector3 CtrlMode::s_balls_max_position
	Vector3_t3722313464  ___s_balls_max_position_33;
	// System.Single CtrlMode::s_fLastDisRaise
	float ___s_fLastDisRaise_34;
	// System.Single CtrlMode::s_fLastDisDown
	float ___s_fLastDisDown_35;
	// System.Single CtrlMode::s_fMaxCamSize
	float ___s_fMaxCamSize_36;
	// System.Single CtrlMode::s_fCameraProtectTime
	float ___s_fCameraProtectTime_37;
	// System.Single CtrlMode::s_fCameraStayBeforeDownTime
	float ___s_fCameraStayBeforeDownTime_38;
	// CtrlMode/eCamerSizeProtectStatus CtrlMode::s_eProtectStatus
	int32_t ___s_eProtectStatus_39;
	// System.Single CtrlMode::s_fShangFuDestCamSize
	float ___s_fShangFuDestCamSize_40;
	// System.Single CtrlMode::s_fRangeBeforeProtect
	float ___s_fRangeBeforeProtect_41;
	// System.Single CtrlMode::s_fRangeBeforeProtect2
	float ___s_fRangeBeforeProtect2_42;
	// System.Single CtrlMode::s_fShangFuXiShu
	float ___s_fShangFuXiShu_43;
	// System.Single CtrlMode::s_fForecastCameraSizeChangeAmount
	float ___s_fForecastCameraSizeChangeAmount_44;
	// System.Single CtrlMode::m_fProtectTime
	float ___m_fProtectTime_46;
	// System.Collections.Generic.List`1<System.Single> CtrlMode::s_lstTemp
	List_1_t2869341516 * ___s_lstTemp_47;
	// System.Single CtrlMode::s_fCameraSizeChangeSpeed
	float ___s_fCameraSizeChangeSpeed_48;
	// System.Single CtrlMode::s_fCameraProtectRaiseSpeed
	float ___s_fCameraProtectRaiseSpeed_49;
	// System.Single CtrlMode::s_fCameraProtectDownSpeed
	float ___s_fCameraProtectDownSpeed_50;

public:
	inline static int32_t get_offset_of__ctrl_mode_7() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____ctrl_mode_7)); }
	inline int32_t get__ctrl_mode_7() const { return ____ctrl_mode_7; }
	inline int32_t* get_address_of__ctrl_mode_7() { return &____ctrl_mode_7; }
	inline void set__ctrl_mode_7(int32_t value)
	{
		____ctrl_mode_7 = value;
	}

	inline static int32_t get_offset_of__dir_indicator_type_8() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____dir_indicator_type_8)); }
	inline int32_t get__dir_indicator_type_8() const { return ____dir_indicator_type_8; }
	inline int32_t* get_address_of__dir_indicator_type_8() { return &____dir_indicator_type_8; }
	inline void set__dir_indicator_type_8(int32_t value)
	{
		____dir_indicator_type_8 = value;
	}

	inline static int32_t get_offset_of__ball_scale_zoom_factor_11() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____ball_scale_zoom_factor_11)); }
	inline float get__ball_scale_zoom_factor_11() const { return ____ball_scale_zoom_factor_11; }
	inline float* get_address_of__ball_scale_zoom_factor_11() { return &____ball_scale_zoom_factor_11; }
	inline void set__ball_scale_zoom_factor_11(float value)
	{
		____ball_scale_zoom_factor_11 = value;
	}

	inline static int32_t get_offset_of__world_port_h_12() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____world_port_h_12)); }
	inline float get__world_port_h_12() const { return ____world_port_h_12; }
	inline float* get_address_of__world_port_h_12() { return &____world_port_h_12; }
	inline void set__world_port_h_12(float value)
	{
		____world_port_h_12 = value;
	}

	inline static int32_t get_offset_of__world_port_v_13() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____world_port_v_13)); }
	inline float get__world_port_v_13() const { return ____world_port_v_13; }
	inline float* get_address_of__world_port_v_13() { return &____world_port_v_13; }
	inline void set__world_port_v_13(float value)
	{
		____world_port_v_13 = value;
	}

	inline static int32_t get_offset_of__zoom_velocity_15() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____zoom_velocity_15)); }
	inline float get__zoom_velocity_15() const { return ____zoom_velocity_15; }
	inline float* get_address_of__zoom_velocity_15() { return &____zoom_velocity_15; }
	inline void set__zoom_velocity_15(float value)
	{
		____zoom_velocity_15 = value;
	}

	inline static int32_t get_offset_of__zoom_time_16() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ____zoom_time_16)); }
	inline float get__zoom_time_16() const { return ____zoom_time_16; }
	inline float* get_address_of__zoom_time_16() { return &____zoom_time_16; }
	inline void set__zoom_time_16(float value)
	{
		____zoom_time_16 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_17() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___vecTempPos1_17)); }
	inline Vector3_t3722313464  get_vecTempPos1_17() const { return ___vecTempPos1_17; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_17() { return &___vecTempPos1_17; }
	inline void set_vecTempPos1_17(Vector3_t3722313464  value)
	{
		___vecTempPos1_17 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_18() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___vecTempPos2_18)); }
	inline Vector3_t3722313464  get_vecTempPos2_18() const { return ___vecTempPos2_18; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_18() { return &___vecTempPos2_18; }
	inline void set_vecTempPos2_18(Vector3_t3722313464  value)
	{
		___vecTempPos2_18 = value;
	}

	inline static int32_t get_offset_of_s_eCamMoveStatus_21() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_eCamMoveStatus_21)); }
	inline int32_t get_s_eCamMoveStatus_21() const { return ___s_eCamMoveStatus_21; }
	inline int32_t* get_address_of_s_eCamMoveStatus_21() { return &___s_eCamMoveStatus_21; }
	inline void set_s_eCamMoveStatus_21(int32_t value)
	{
		___s_eCamMoveStatus_21 = value;
	}

	inline static int32_t get_offset_of_m_fAcctackRadiusMultiple_22() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___m_fAcctackRadiusMultiple_22)); }
	inline float get_m_fAcctackRadiusMultiple_22() const { return ___m_fAcctackRadiusMultiple_22; }
	inline float* get_address_of_m_fAcctackRadiusMultiple_22() { return &___m_fAcctackRadiusMultiple_22; }
	inline void set_m_fAcctackRadiusMultiple_22(float value)
	{
		___m_fAcctackRadiusMultiple_22 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallSpeed_23() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___m_fSpitBallSpeed_23)); }
	inline float get_m_fSpitBallSpeed_23() const { return ___m_fSpitBallSpeed_23; }
	inline float* get_address_of_m_fSpitBallSpeed_23() { return &___m_fSpitBallSpeed_23; }
	inline void set_m_fSpitBallSpeed_23(float value)
	{
		___m_fSpitBallSpeed_23 = value;
	}

	inline static int32_t get_offset_of_ballLeft_24() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___ballLeft_24)); }
	inline Ball_t2206666566 * get_ballLeft_24() const { return ___ballLeft_24; }
	inline Ball_t2206666566 ** get_address_of_ballLeft_24() { return &___ballLeft_24; }
	inline void set_ballLeft_24(Ball_t2206666566 * value)
	{
		___ballLeft_24 = value;
		Il2CppCodeGenWriteBarrier((&___ballLeft_24), value);
	}

	inline static int32_t get_offset_of_ballRight_25() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___ballRight_25)); }
	inline Ball_t2206666566 * get_ballRight_25() const { return ___ballRight_25; }
	inline Ball_t2206666566 ** get_address_of_ballRight_25() { return &___ballRight_25; }
	inline void set_ballRight_25(Ball_t2206666566 * value)
	{
		___ballRight_25 = value;
		Il2CppCodeGenWriteBarrier((&___ballRight_25), value);
	}

	inline static int32_t get_offset_of_ballTop_26() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___ballTop_26)); }
	inline Ball_t2206666566 * get_ballTop_26() const { return ___ballTop_26; }
	inline Ball_t2206666566 ** get_address_of_ballTop_26() { return &___ballTop_26; }
	inline void set_ballTop_26(Ball_t2206666566 * value)
	{
		___ballTop_26 = value;
		Il2CppCodeGenWriteBarrier((&___ballTop_26), value);
	}

	inline static int32_t get_offset_of_ballBottom_27() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___ballBottom_27)); }
	inline Ball_t2206666566 * get_ballBottom_27() const { return ___ballBottom_27; }
	inline Ball_t2206666566 ** get_address_of_ballBottom_27() { return &___ballBottom_27; }
	inline void set_ballBottom_27(Ball_t2206666566 * value)
	{
		___ballBottom_27 = value;
		Il2CppCodeGenWriteBarrier((&___ballBottom_27), value);
	}

	inline static int32_t get_offset_of_fLeftBorderPos_28() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___fLeftBorderPos_28)); }
	inline float get_fLeftBorderPos_28() const { return ___fLeftBorderPos_28; }
	inline float* get_address_of_fLeftBorderPos_28() { return &___fLeftBorderPos_28; }
	inline void set_fLeftBorderPos_28(float value)
	{
		___fLeftBorderPos_28 = value;
	}

	inline static int32_t get_offset_of_fRightBorderPos_29() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___fRightBorderPos_29)); }
	inline float get_fRightBorderPos_29() const { return ___fRightBorderPos_29; }
	inline float* get_address_of_fRightBorderPos_29() { return &___fRightBorderPos_29; }
	inline void set_fRightBorderPos_29(float value)
	{
		___fRightBorderPos_29 = value;
	}

	inline static int32_t get_offset_of_fTopBorderPos_30() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___fTopBorderPos_30)); }
	inline float get_fTopBorderPos_30() const { return ___fTopBorderPos_30; }
	inline float* get_address_of_fTopBorderPos_30() { return &___fTopBorderPos_30; }
	inline void set_fTopBorderPos_30(float value)
	{
		___fTopBorderPos_30 = value;
	}

	inline static int32_t get_offset_of_fBottomBorderPos_31() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___fBottomBorderPos_31)); }
	inline float get_fBottomBorderPos_31() const { return ___fBottomBorderPos_31; }
	inline float* get_address_of_fBottomBorderPos_31() { return &___fBottomBorderPos_31; }
	inline void set_fBottomBorderPos_31(float value)
	{
		___fBottomBorderPos_31 = value;
	}

	inline static int32_t get_offset_of_s_balls_min_position_32() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_balls_min_position_32)); }
	inline Vector3_t3722313464  get_s_balls_min_position_32() const { return ___s_balls_min_position_32; }
	inline Vector3_t3722313464 * get_address_of_s_balls_min_position_32() { return &___s_balls_min_position_32; }
	inline void set_s_balls_min_position_32(Vector3_t3722313464  value)
	{
		___s_balls_min_position_32 = value;
	}

	inline static int32_t get_offset_of_s_balls_max_position_33() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_balls_max_position_33)); }
	inline Vector3_t3722313464  get_s_balls_max_position_33() const { return ___s_balls_max_position_33; }
	inline Vector3_t3722313464 * get_address_of_s_balls_max_position_33() { return &___s_balls_max_position_33; }
	inline void set_s_balls_max_position_33(Vector3_t3722313464  value)
	{
		___s_balls_max_position_33 = value;
	}

	inline static int32_t get_offset_of_s_fLastDisRaise_34() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fLastDisRaise_34)); }
	inline float get_s_fLastDisRaise_34() const { return ___s_fLastDisRaise_34; }
	inline float* get_address_of_s_fLastDisRaise_34() { return &___s_fLastDisRaise_34; }
	inline void set_s_fLastDisRaise_34(float value)
	{
		___s_fLastDisRaise_34 = value;
	}

	inline static int32_t get_offset_of_s_fLastDisDown_35() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fLastDisDown_35)); }
	inline float get_s_fLastDisDown_35() const { return ___s_fLastDisDown_35; }
	inline float* get_address_of_s_fLastDisDown_35() { return &___s_fLastDisDown_35; }
	inline void set_s_fLastDisDown_35(float value)
	{
		___s_fLastDisDown_35 = value;
	}

	inline static int32_t get_offset_of_s_fMaxCamSize_36() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fMaxCamSize_36)); }
	inline float get_s_fMaxCamSize_36() const { return ___s_fMaxCamSize_36; }
	inline float* get_address_of_s_fMaxCamSize_36() { return &___s_fMaxCamSize_36; }
	inline void set_s_fMaxCamSize_36(float value)
	{
		___s_fMaxCamSize_36 = value;
	}

	inline static int32_t get_offset_of_s_fCameraProtectTime_37() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fCameraProtectTime_37)); }
	inline float get_s_fCameraProtectTime_37() const { return ___s_fCameraProtectTime_37; }
	inline float* get_address_of_s_fCameraProtectTime_37() { return &___s_fCameraProtectTime_37; }
	inline void set_s_fCameraProtectTime_37(float value)
	{
		___s_fCameraProtectTime_37 = value;
	}

	inline static int32_t get_offset_of_s_fCameraStayBeforeDownTime_38() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fCameraStayBeforeDownTime_38)); }
	inline float get_s_fCameraStayBeforeDownTime_38() const { return ___s_fCameraStayBeforeDownTime_38; }
	inline float* get_address_of_s_fCameraStayBeforeDownTime_38() { return &___s_fCameraStayBeforeDownTime_38; }
	inline void set_s_fCameraStayBeforeDownTime_38(float value)
	{
		___s_fCameraStayBeforeDownTime_38 = value;
	}

	inline static int32_t get_offset_of_s_eProtectStatus_39() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_eProtectStatus_39)); }
	inline int32_t get_s_eProtectStatus_39() const { return ___s_eProtectStatus_39; }
	inline int32_t* get_address_of_s_eProtectStatus_39() { return &___s_eProtectStatus_39; }
	inline void set_s_eProtectStatus_39(int32_t value)
	{
		___s_eProtectStatus_39 = value;
	}

	inline static int32_t get_offset_of_s_fShangFuDestCamSize_40() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fShangFuDestCamSize_40)); }
	inline float get_s_fShangFuDestCamSize_40() const { return ___s_fShangFuDestCamSize_40; }
	inline float* get_address_of_s_fShangFuDestCamSize_40() { return &___s_fShangFuDestCamSize_40; }
	inline void set_s_fShangFuDestCamSize_40(float value)
	{
		___s_fShangFuDestCamSize_40 = value;
	}

	inline static int32_t get_offset_of_s_fRangeBeforeProtect_41() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fRangeBeforeProtect_41)); }
	inline float get_s_fRangeBeforeProtect_41() const { return ___s_fRangeBeforeProtect_41; }
	inline float* get_address_of_s_fRangeBeforeProtect_41() { return &___s_fRangeBeforeProtect_41; }
	inline void set_s_fRangeBeforeProtect_41(float value)
	{
		___s_fRangeBeforeProtect_41 = value;
	}

	inline static int32_t get_offset_of_s_fRangeBeforeProtect2_42() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fRangeBeforeProtect2_42)); }
	inline float get_s_fRangeBeforeProtect2_42() const { return ___s_fRangeBeforeProtect2_42; }
	inline float* get_address_of_s_fRangeBeforeProtect2_42() { return &___s_fRangeBeforeProtect2_42; }
	inline void set_s_fRangeBeforeProtect2_42(float value)
	{
		___s_fRangeBeforeProtect2_42 = value;
	}

	inline static int32_t get_offset_of_s_fShangFuXiShu_43() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fShangFuXiShu_43)); }
	inline float get_s_fShangFuXiShu_43() const { return ___s_fShangFuXiShu_43; }
	inline float* get_address_of_s_fShangFuXiShu_43() { return &___s_fShangFuXiShu_43; }
	inline void set_s_fShangFuXiShu_43(float value)
	{
		___s_fShangFuXiShu_43 = value;
	}

	inline static int32_t get_offset_of_s_fForecastCameraSizeChangeAmount_44() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fForecastCameraSizeChangeAmount_44)); }
	inline float get_s_fForecastCameraSizeChangeAmount_44() const { return ___s_fForecastCameraSizeChangeAmount_44; }
	inline float* get_address_of_s_fForecastCameraSizeChangeAmount_44() { return &___s_fForecastCameraSizeChangeAmount_44; }
	inline void set_s_fForecastCameraSizeChangeAmount_44(float value)
	{
		___s_fForecastCameraSizeChangeAmount_44 = value;
	}

	inline static int32_t get_offset_of_m_fProtectTime_46() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___m_fProtectTime_46)); }
	inline float get_m_fProtectTime_46() const { return ___m_fProtectTime_46; }
	inline float* get_address_of_m_fProtectTime_46() { return &___m_fProtectTime_46; }
	inline void set_m_fProtectTime_46(float value)
	{
		___m_fProtectTime_46 = value;
	}

	inline static int32_t get_offset_of_s_lstTemp_47() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_lstTemp_47)); }
	inline List_1_t2869341516 * get_s_lstTemp_47() const { return ___s_lstTemp_47; }
	inline List_1_t2869341516 ** get_address_of_s_lstTemp_47() { return &___s_lstTemp_47; }
	inline void set_s_lstTemp_47(List_1_t2869341516 * value)
	{
		___s_lstTemp_47 = value;
		Il2CppCodeGenWriteBarrier((&___s_lstTemp_47), value);
	}

	inline static int32_t get_offset_of_s_fCameraSizeChangeSpeed_48() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fCameraSizeChangeSpeed_48)); }
	inline float get_s_fCameraSizeChangeSpeed_48() const { return ___s_fCameraSizeChangeSpeed_48; }
	inline float* get_address_of_s_fCameraSizeChangeSpeed_48() { return &___s_fCameraSizeChangeSpeed_48; }
	inline void set_s_fCameraSizeChangeSpeed_48(float value)
	{
		___s_fCameraSizeChangeSpeed_48 = value;
	}

	inline static int32_t get_offset_of_s_fCameraProtectRaiseSpeed_49() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fCameraProtectRaiseSpeed_49)); }
	inline float get_s_fCameraProtectRaiseSpeed_49() const { return ___s_fCameraProtectRaiseSpeed_49; }
	inline float* get_address_of_s_fCameraProtectRaiseSpeed_49() { return &___s_fCameraProtectRaiseSpeed_49; }
	inline void set_s_fCameraProtectRaiseSpeed_49(float value)
	{
		___s_fCameraProtectRaiseSpeed_49 = value;
	}

	inline static int32_t get_offset_of_s_fCameraProtectDownSpeed_50() { return static_cast<int32_t>(offsetof(CtrlMode_t3902354508_StaticFields, ___s_fCameraProtectDownSpeed_50)); }
	inline float get_s_fCameraProtectDownSpeed_50() const { return ___s_fCameraProtectDownSpeed_50; }
	inline float* get_address_of_s_fCameraProtectDownSpeed_50() { return &___s_fCameraProtectDownSpeed_50; }
	inline void set_s_fCameraProtectDownSpeed_50(float value)
	{
		___s_fCameraProtectDownSpeed_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTRLMODE_T3902354508_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef U3CLOADU3EC__ITERATOR0_T2710249343_H
#define U3CLOADU3EC__ITERATOR0_T2710249343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteResourceLoader/<load>c__Iterator0
struct  U3CloadU3Ec__Iterator0_t2710249343  : public RuntimeObject
{
public:
	// System.Int32 RemoteResourceLoader/<load>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_0;
	// ResourceBundleList RemoteResourceLoader/<load>c__Iterator0::<resourceBundleList>__0
	ResourceBundleList_t987847975 * ___U3CresourceBundleListU3E__0_1;
	// ResourceBundle RemoteResourceLoader/<load>c__Iterator0::<resourceBundle>__0
	ResourceBundle_t1032369214 * ___U3CresourceBundleU3E__0_2;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Resource> RemoteResourceLoader/<load>c__Iterator0::$locvar0
	Enumerator_t1871321943  ___U24locvar0_3;
	// System.Collections.Generic.KeyValuePair`2<System.String,Resource> RemoteResourceLoader/<load>c__Iterator0::<pair>__1
	KeyValuePair_2_t2314811335  ___U3CpairU3E__1_4;
	// Resource RemoteResourceLoader/<load>c__Iterator0::<resource>__2
	Resource_t131882869 * ___U3CresourceU3E__2_5;
	// System.String RemoteResourceLoader/<load>c__Iterator0::<url>__2
	String_t* ___U3CurlU3E__2_6;
	// UnityEngine.Networking.UnityWebRequest RemoteResourceLoader/<load>c__Iterator0::<request>__2
	UnityWebRequest_t463507806 * ___U3CrequestU3E__2_7;
	// UnityEngine.AssetBundleManifest RemoteResourceLoader/<load>c__Iterator0::<manifest>__3
	AssetBundleManifest_t2634949939 * ___U3CmanifestU3E__3_8;
	// System.String RemoteResourceLoader/<load>c__Iterator0::<hashSavePath>__3
	String_t* ___U3ChashSavePathU3E__3_9;
	// System.String RemoteResourceLoader/<load>c__Iterator0::<fileSavePath>__3
	String_t* ___U3CfileSavePathU3E__3_10;
	// RemoteResourceLoader RemoteResourceLoader/<load>c__Iterator0::$this
	RemoteResourceLoader_t1429902794 * ___U24this_11;
	// System.Object RemoteResourceLoader/<load>c__Iterator0::$current
	RuntimeObject * ___U24current_12;
	// System.Boolean RemoteResourceLoader/<load>c__Iterator0::$disposing
	bool ___U24disposing_13;
	// System.Int32 RemoteResourceLoader/<load>c__Iterator0::$PC
	int32_t ___U24PC_14;

public:
	inline static int32_t get_offset_of_U3CcounterU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CcounterU3E__0_0)); }
	inline int32_t get_U3CcounterU3E__0_0() const { return ___U3CcounterU3E__0_0; }
	inline int32_t* get_address_of_U3CcounterU3E__0_0() { return &___U3CcounterU3E__0_0; }
	inline void set_U3CcounterU3E__0_0(int32_t value)
	{
		___U3CcounterU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CresourceBundleListU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CresourceBundleListU3E__0_1)); }
	inline ResourceBundleList_t987847975 * get_U3CresourceBundleListU3E__0_1() const { return ___U3CresourceBundleListU3E__0_1; }
	inline ResourceBundleList_t987847975 ** get_address_of_U3CresourceBundleListU3E__0_1() { return &___U3CresourceBundleListU3E__0_1; }
	inline void set_U3CresourceBundleListU3E__0_1(ResourceBundleList_t987847975 * value)
	{
		___U3CresourceBundleListU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourceBundleListU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CresourceBundleU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CresourceBundleU3E__0_2)); }
	inline ResourceBundle_t1032369214 * get_U3CresourceBundleU3E__0_2() const { return ___U3CresourceBundleU3E__0_2; }
	inline ResourceBundle_t1032369214 ** get_address_of_U3CresourceBundleU3E__0_2() { return &___U3CresourceBundleU3E__0_2; }
	inline void set_U3CresourceBundleU3E__0_2(ResourceBundle_t1032369214 * value)
	{
		___U3CresourceBundleU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourceBundleU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U24locvar0_3)); }
	inline Enumerator_t1871321943  get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline Enumerator_t1871321943 * get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(Enumerator_t1871321943  value)
	{
		___U24locvar0_3 = value;
	}

	inline static int32_t get_offset_of_U3CpairU3E__1_4() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CpairU3E__1_4)); }
	inline KeyValuePair_2_t2314811335  get_U3CpairU3E__1_4() const { return ___U3CpairU3E__1_4; }
	inline KeyValuePair_2_t2314811335 * get_address_of_U3CpairU3E__1_4() { return &___U3CpairU3E__1_4; }
	inline void set_U3CpairU3E__1_4(KeyValuePair_2_t2314811335  value)
	{
		___U3CpairU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CresourceU3E__2_5() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CresourceU3E__2_5)); }
	inline Resource_t131882869 * get_U3CresourceU3E__2_5() const { return ___U3CresourceU3E__2_5; }
	inline Resource_t131882869 ** get_address_of_U3CresourceU3E__2_5() { return &___U3CresourceU3E__2_5; }
	inline void set_U3CresourceU3E__2_5(Resource_t131882869 * value)
	{
		___U3CresourceU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourceU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__2_6() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CurlU3E__2_6)); }
	inline String_t* get_U3CurlU3E__2_6() const { return ___U3CurlU3E__2_6; }
	inline String_t** get_address_of_U3CurlU3E__2_6() { return &___U3CurlU3E__2_6; }
	inline void set_U3CurlU3E__2_6(String_t* value)
	{
		___U3CurlU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__2_7() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CrequestU3E__2_7)); }
	inline UnityWebRequest_t463507806 * get_U3CrequestU3E__2_7() const { return ___U3CrequestU3E__2_7; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CrequestU3E__2_7() { return &___U3CrequestU3E__2_7; }
	inline void set_U3CrequestU3E__2_7(UnityWebRequest_t463507806 * value)
	{
		___U3CrequestU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmanifestU3E__3_8() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CmanifestU3E__3_8)); }
	inline AssetBundleManifest_t2634949939 * get_U3CmanifestU3E__3_8() const { return ___U3CmanifestU3E__3_8; }
	inline AssetBundleManifest_t2634949939 ** get_address_of_U3CmanifestU3E__3_8() { return &___U3CmanifestU3E__3_8; }
	inline void set_U3CmanifestU3E__3_8(AssetBundleManifest_t2634949939 * value)
	{
		___U3CmanifestU3E__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanifestU3E__3_8), value);
	}

	inline static int32_t get_offset_of_U3ChashSavePathU3E__3_9() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3ChashSavePathU3E__3_9)); }
	inline String_t* get_U3ChashSavePathU3E__3_9() const { return ___U3ChashSavePathU3E__3_9; }
	inline String_t** get_address_of_U3ChashSavePathU3E__3_9() { return &___U3ChashSavePathU3E__3_9; }
	inline void set_U3ChashSavePathU3E__3_9(String_t* value)
	{
		___U3ChashSavePathU3E__3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChashSavePathU3E__3_9), value);
	}

	inline static int32_t get_offset_of_U3CfileSavePathU3E__3_10() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U3CfileSavePathU3E__3_10)); }
	inline String_t* get_U3CfileSavePathU3E__3_10() const { return ___U3CfileSavePathU3E__3_10; }
	inline String_t** get_address_of_U3CfileSavePathU3E__3_10() { return &___U3CfileSavePathU3E__3_10; }
	inline void set_U3CfileSavePathU3E__3_10(String_t* value)
	{
		___U3CfileSavePathU3E__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileSavePathU3E__3_10), value);
	}

	inline static int32_t get_offset_of_U24this_11() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U24this_11)); }
	inline RemoteResourceLoader_t1429902794 * get_U24this_11() const { return ___U24this_11; }
	inline RemoteResourceLoader_t1429902794 ** get_address_of_U24this_11() { return &___U24this_11; }
	inline void set_U24this_11(RemoteResourceLoader_t1429902794 * value)
	{
		___U24this_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_11), value);
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U24current_12)); }
	inline RuntimeObject * get_U24current_12() const { return ___U24current_12; }
	inline RuntimeObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(RuntimeObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_12), value);
	}

	inline static int32_t get_offset_of_U24disposing_13() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U24disposing_13)); }
	inline bool get_U24disposing_13() const { return ___U24disposing_13; }
	inline bool* get_address_of_U24disposing_13() { return &___U24disposing_13; }
	inline void set_U24disposing_13(bool value)
	{
		___U24disposing_13 = value;
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t2710249343, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3EC__ITERATOR0_T2710249343_H
#ifndef RESOURCEBUNDLE_T1032369214_H
#define RESOURCEBUNDLE_T1032369214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceBundle
struct  ResourceBundle_t1032369214  : public Resource_t131882869
{
public:
	// System.String ResourceBundle::hash
	String_t* ___hash_5;
	// UnityEngine.AssetBundle ResourceBundle::bundle
	AssetBundle_t1153907252 * ___bundle_6;
	// UnityEngine.Shader ResourceBundle::shader
	Shader_t4151988712 * ___shader_7;
	// UnityEngine.Sprite ResourceBundle::sprite
	Sprite_t280657092 * ___sprite_8;
	// UnityEngine.Material ResourceBundle::material
	Material_t340375123 * ___material_9;

public:
	inline static int32_t get_offset_of_hash_5() { return static_cast<int32_t>(offsetof(ResourceBundle_t1032369214, ___hash_5)); }
	inline String_t* get_hash_5() const { return ___hash_5; }
	inline String_t** get_address_of_hash_5() { return &___hash_5; }
	inline void set_hash_5(String_t* value)
	{
		___hash_5 = value;
		Il2CppCodeGenWriteBarrier((&___hash_5), value);
	}

	inline static int32_t get_offset_of_bundle_6() { return static_cast<int32_t>(offsetof(ResourceBundle_t1032369214, ___bundle_6)); }
	inline AssetBundle_t1153907252 * get_bundle_6() const { return ___bundle_6; }
	inline AssetBundle_t1153907252 ** get_address_of_bundle_6() { return &___bundle_6; }
	inline void set_bundle_6(AssetBundle_t1153907252 * value)
	{
		___bundle_6 = value;
		Il2CppCodeGenWriteBarrier((&___bundle_6), value);
	}

	inline static int32_t get_offset_of_shader_7() { return static_cast<int32_t>(offsetof(ResourceBundle_t1032369214, ___shader_7)); }
	inline Shader_t4151988712 * get_shader_7() const { return ___shader_7; }
	inline Shader_t4151988712 ** get_address_of_shader_7() { return &___shader_7; }
	inline void set_shader_7(Shader_t4151988712 * value)
	{
		___shader_7 = value;
		Il2CppCodeGenWriteBarrier((&___shader_7), value);
	}

	inline static int32_t get_offset_of_sprite_8() { return static_cast<int32_t>(offsetof(ResourceBundle_t1032369214, ___sprite_8)); }
	inline Sprite_t280657092 * get_sprite_8() const { return ___sprite_8; }
	inline Sprite_t280657092 ** get_address_of_sprite_8() { return &___sprite_8; }
	inline void set_sprite_8(Sprite_t280657092 * value)
	{
		___sprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_8), value);
	}

	inline static int32_t get_offset_of_material_9() { return static_cast<int32_t>(offsetof(ResourceBundle_t1032369214, ___material_9)); }
	inline Material_t340375123 * get_material_9() const { return ___material_9; }
	inline Material_t340375123 ** get_address_of_material_9() { return &___material_9; }
	inline void set_material_9(Material_t340375123 * value)
	{
		___material_9 = value;
		Il2CppCodeGenWriteBarrier((&___material_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEBUNDLE_T1032369214_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RESOURCEBUNDLELIST_T987847975_H
#define RESOURCEBUNDLELIST_T987847975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceBundleList
struct  ResourceBundleList_t987847975  : public Resource_t131882869
{
public:
	// UnityEngine.AssetBundle ResourceBundleList::bundle
	AssetBundle_t1153907252 * ___bundle_5;
	// UnityEngine.AssetBundleManifest ResourceBundleList::manifest
	AssetBundleManifest_t2634949939 * ___manifest_6;

public:
	inline static int32_t get_offset_of_bundle_5() { return static_cast<int32_t>(offsetof(ResourceBundleList_t987847975, ___bundle_5)); }
	inline AssetBundle_t1153907252 * get_bundle_5() const { return ___bundle_5; }
	inline AssetBundle_t1153907252 ** get_address_of_bundle_5() { return &___bundle_5; }
	inline void set_bundle_5(AssetBundle_t1153907252 * value)
	{
		___bundle_5 = value;
		Il2CppCodeGenWriteBarrier((&___bundle_5), value);
	}

	inline static int32_t get_offset_of_manifest_6() { return static_cast<int32_t>(offsetof(ResourceBundleList_t987847975, ___manifest_6)); }
	inline AssetBundleManifest_t2634949939 * get_manifest_6() const { return ___manifest_6; }
	inline AssetBundleManifest_t2634949939 ** get_address_of_manifest_6() { return &___manifest_6; }
	inline void set_manifest_6(AssetBundleManifest_t2634949939 * value)
	{
		___manifest_6 = value;
		Il2CppCodeGenWriteBarrier((&___manifest_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEBUNDLELIST_T987847975_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef MAIN_T2227614042_H
#define MAIN_T2227614042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// main
struct  main_t2227614042  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField main::registerPanelInputFieldPhoneNumber
	InputField_t3762917431 * ___registerPanelInputFieldPhoneNumber_2;
	// UnityEngine.UI.InputField main::registerPanelInputFieldSMSCode
	InputField_t3762917431 * ___registerPanelInputFieldSMSCode_3;
	// UnityEngine.UI.InputField main::registerPanelInputFieldPassword
	InputField_t3762917431 * ___registerPanelInputFieldPassword_4;
	// UnityEngine.UI.Button main::registerPanelButtonRegister
	Button_t4055032469 * ___registerPanelButtonRegister_5;
	// UnityEngine.UI.Button main::registerPanelButtonGetSMSCode
	Button_t4055032469 * ___registerPanelButtonGetSMSCode_6;
	// UnityEngine.UI.InputField main::passwordLoginPanelInputFieldPhoneNumber
	InputField_t3762917431 * ___passwordLoginPanelInputFieldPhoneNumber_7;
	// UnityEngine.UI.InputField main::passwordLoginPanelInputFieldPassword
	InputField_t3762917431 * ___passwordLoginPanelInputFieldPassword_8;
	// UnityEngine.UI.Button main::passwordLoginPanelButtonLogin
	Button_t4055032469 * ___passwordLoginPanelButtonLogin_9;
	// UnityEngine.UI.InputField main::resetPanelInputFieldPhoneNumber
	InputField_t3762917431 * ___resetPanelInputFieldPhoneNumber_10;
	// UnityEngine.UI.InputField main::resetPanelInputFieldSMSCode
	InputField_t3762917431 * ___resetPanelInputFieldSMSCode_11;
	// UnityEngine.UI.InputField main::resetPanelInputFieldPassword
	InputField_t3762917431 * ___resetPanelInputFieldPassword_12;
	// UnityEngine.UI.Button main::resetPanelButtonReset
	Button_t4055032469 * ___resetPanelButtonReset_13;
	// UnityEngine.UI.Button main::resetPanelButtonGetSMSCode
	Button_t4055032469 * ___resetPanelButtonGetSMSCode_14;
	// UnityEngine.UI.Button main::sessionVerifyPanelButtonVerify
	Button_t4055032469 * ___sessionVerifyPanelButtonVerify_15;
	// UnityEngine.UI.Button main::sessionVerifyPanelButtonExecute
	Button_t4055032469 * ___sessionVerifyPanelButtonExecute_16;
	// UnityEngine.UI.Button main::sessionVerifyPanelButtonLogout
	Button_t4055032469 * ___sessionVerifyPanelButtonLogout_17;
	// WebAuthAPI.Authenticator main::authenticator
	Authenticator_t530988528 * ___authenticator_18;
	// WebAuthAPI.Account main::account
	Account_t400769301 * ___account_19;
	// WebAuthAPI.Charge main::charge
	Charge_t1653125646 * ___charge_20;
	// WebAuthAPI.Market main::market
	Market_t538055513 * ___market_21;

public:
	inline static int32_t get_offset_of_registerPanelInputFieldPhoneNumber_2() { return static_cast<int32_t>(offsetof(main_t2227614042, ___registerPanelInputFieldPhoneNumber_2)); }
	inline InputField_t3762917431 * get_registerPanelInputFieldPhoneNumber_2() const { return ___registerPanelInputFieldPhoneNumber_2; }
	inline InputField_t3762917431 ** get_address_of_registerPanelInputFieldPhoneNumber_2() { return &___registerPanelInputFieldPhoneNumber_2; }
	inline void set_registerPanelInputFieldPhoneNumber_2(InputField_t3762917431 * value)
	{
		___registerPanelInputFieldPhoneNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___registerPanelInputFieldPhoneNumber_2), value);
	}

	inline static int32_t get_offset_of_registerPanelInputFieldSMSCode_3() { return static_cast<int32_t>(offsetof(main_t2227614042, ___registerPanelInputFieldSMSCode_3)); }
	inline InputField_t3762917431 * get_registerPanelInputFieldSMSCode_3() const { return ___registerPanelInputFieldSMSCode_3; }
	inline InputField_t3762917431 ** get_address_of_registerPanelInputFieldSMSCode_3() { return &___registerPanelInputFieldSMSCode_3; }
	inline void set_registerPanelInputFieldSMSCode_3(InputField_t3762917431 * value)
	{
		___registerPanelInputFieldSMSCode_3 = value;
		Il2CppCodeGenWriteBarrier((&___registerPanelInputFieldSMSCode_3), value);
	}

	inline static int32_t get_offset_of_registerPanelInputFieldPassword_4() { return static_cast<int32_t>(offsetof(main_t2227614042, ___registerPanelInputFieldPassword_4)); }
	inline InputField_t3762917431 * get_registerPanelInputFieldPassword_4() const { return ___registerPanelInputFieldPassword_4; }
	inline InputField_t3762917431 ** get_address_of_registerPanelInputFieldPassword_4() { return &___registerPanelInputFieldPassword_4; }
	inline void set_registerPanelInputFieldPassword_4(InputField_t3762917431 * value)
	{
		___registerPanelInputFieldPassword_4 = value;
		Il2CppCodeGenWriteBarrier((&___registerPanelInputFieldPassword_4), value);
	}

	inline static int32_t get_offset_of_registerPanelButtonRegister_5() { return static_cast<int32_t>(offsetof(main_t2227614042, ___registerPanelButtonRegister_5)); }
	inline Button_t4055032469 * get_registerPanelButtonRegister_5() const { return ___registerPanelButtonRegister_5; }
	inline Button_t4055032469 ** get_address_of_registerPanelButtonRegister_5() { return &___registerPanelButtonRegister_5; }
	inline void set_registerPanelButtonRegister_5(Button_t4055032469 * value)
	{
		___registerPanelButtonRegister_5 = value;
		Il2CppCodeGenWriteBarrier((&___registerPanelButtonRegister_5), value);
	}

	inline static int32_t get_offset_of_registerPanelButtonGetSMSCode_6() { return static_cast<int32_t>(offsetof(main_t2227614042, ___registerPanelButtonGetSMSCode_6)); }
	inline Button_t4055032469 * get_registerPanelButtonGetSMSCode_6() const { return ___registerPanelButtonGetSMSCode_6; }
	inline Button_t4055032469 ** get_address_of_registerPanelButtonGetSMSCode_6() { return &___registerPanelButtonGetSMSCode_6; }
	inline void set_registerPanelButtonGetSMSCode_6(Button_t4055032469 * value)
	{
		___registerPanelButtonGetSMSCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___registerPanelButtonGetSMSCode_6), value);
	}

	inline static int32_t get_offset_of_passwordLoginPanelInputFieldPhoneNumber_7() { return static_cast<int32_t>(offsetof(main_t2227614042, ___passwordLoginPanelInputFieldPhoneNumber_7)); }
	inline InputField_t3762917431 * get_passwordLoginPanelInputFieldPhoneNumber_7() const { return ___passwordLoginPanelInputFieldPhoneNumber_7; }
	inline InputField_t3762917431 ** get_address_of_passwordLoginPanelInputFieldPhoneNumber_7() { return &___passwordLoginPanelInputFieldPhoneNumber_7; }
	inline void set_passwordLoginPanelInputFieldPhoneNumber_7(InputField_t3762917431 * value)
	{
		___passwordLoginPanelInputFieldPhoneNumber_7 = value;
		Il2CppCodeGenWriteBarrier((&___passwordLoginPanelInputFieldPhoneNumber_7), value);
	}

	inline static int32_t get_offset_of_passwordLoginPanelInputFieldPassword_8() { return static_cast<int32_t>(offsetof(main_t2227614042, ___passwordLoginPanelInputFieldPassword_8)); }
	inline InputField_t3762917431 * get_passwordLoginPanelInputFieldPassword_8() const { return ___passwordLoginPanelInputFieldPassword_8; }
	inline InputField_t3762917431 ** get_address_of_passwordLoginPanelInputFieldPassword_8() { return &___passwordLoginPanelInputFieldPassword_8; }
	inline void set_passwordLoginPanelInputFieldPassword_8(InputField_t3762917431 * value)
	{
		___passwordLoginPanelInputFieldPassword_8 = value;
		Il2CppCodeGenWriteBarrier((&___passwordLoginPanelInputFieldPassword_8), value);
	}

	inline static int32_t get_offset_of_passwordLoginPanelButtonLogin_9() { return static_cast<int32_t>(offsetof(main_t2227614042, ___passwordLoginPanelButtonLogin_9)); }
	inline Button_t4055032469 * get_passwordLoginPanelButtonLogin_9() const { return ___passwordLoginPanelButtonLogin_9; }
	inline Button_t4055032469 ** get_address_of_passwordLoginPanelButtonLogin_9() { return &___passwordLoginPanelButtonLogin_9; }
	inline void set_passwordLoginPanelButtonLogin_9(Button_t4055032469 * value)
	{
		___passwordLoginPanelButtonLogin_9 = value;
		Il2CppCodeGenWriteBarrier((&___passwordLoginPanelButtonLogin_9), value);
	}

	inline static int32_t get_offset_of_resetPanelInputFieldPhoneNumber_10() { return static_cast<int32_t>(offsetof(main_t2227614042, ___resetPanelInputFieldPhoneNumber_10)); }
	inline InputField_t3762917431 * get_resetPanelInputFieldPhoneNumber_10() const { return ___resetPanelInputFieldPhoneNumber_10; }
	inline InputField_t3762917431 ** get_address_of_resetPanelInputFieldPhoneNumber_10() { return &___resetPanelInputFieldPhoneNumber_10; }
	inline void set_resetPanelInputFieldPhoneNumber_10(InputField_t3762917431 * value)
	{
		___resetPanelInputFieldPhoneNumber_10 = value;
		Il2CppCodeGenWriteBarrier((&___resetPanelInputFieldPhoneNumber_10), value);
	}

	inline static int32_t get_offset_of_resetPanelInputFieldSMSCode_11() { return static_cast<int32_t>(offsetof(main_t2227614042, ___resetPanelInputFieldSMSCode_11)); }
	inline InputField_t3762917431 * get_resetPanelInputFieldSMSCode_11() const { return ___resetPanelInputFieldSMSCode_11; }
	inline InputField_t3762917431 ** get_address_of_resetPanelInputFieldSMSCode_11() { return &___resetPanelInputFieldSMSCode_11; }
	inline void set_resetPanelInputFieldSMSCode_11(InputField_t3762917431 * value)
	{
		___resetPanelInputFieldSMSCode_11 = value;
		Il2CppCodeGenWriteBarrier((&___resetPanelInputFieldSMSCode_11), value);
	}

	inline static int32_t get_offset_of_resetPanelInputFieldPassword_12() { return static_cast<int32_t>(offsetof(main_t2227614042, ___resetPanelInputFieldPassword_12)); }
	inline InputField_t3762917431 * get_resetPanelInputFieldPassword_12() const { return ___resetPanelInputFieldPassword_12; }
	inline InputField_t3762917431 ** get_address_of_resetPanelInputFieldPassword_12() { return &___resetPanelInputFieldPassword_12; }
	inline void set_resetPanelInputFieldPassword_12(InputField_t3762917431 * value)
	{
		___resetPanelInputFieldPassword_12 = value;
		Il2CppCodeGenWriteBarrier((&___resetPanelInputFieldPassword_12), value);
	}

	inline static int32_t get_offset_of_resetPanelButtonReset_13() { return static_cast<int32_t>(offsetof(main_t2227614042, ___resetPanelButtonReset_13)); }
	inline Button_t4055032469 * get_resetPanelButtonReset_13() const { return ___resetPanelButtonReset_13; }
	inline Button_t4055032469 ** get_address_of_resetPanelButtonReset_13() { return &___resetPanelButtonReset_13; }
	inline void set_resetPanelButtonReset_13(Button_t4055032469 * value)
	{
		___resetPanelButtonReset_13 = value;
		Il2CppCodeGenWriteBarrier((&___resetPanelButtonReset_13), value);
	}

	inline static int32_t get_offset_of_resetPanelButtonGetSMSCode_14() { return static_cast<int32_t>(offsetof(main_t2227614042, ___resetPanelButtonGetSMSCode_14)); }
	inline Button_t4055032469 * get_resetPanelButtonGetSMSCode_14() const { return ___resetPanelButtonGetSMSCode_14; }
	inline Button_t4055032469 ** get_address_of_resetPanelButtonGetSMSCode_14() { return &___resetPanelButtonGetSMSCode_14; }
	inline void set_resetPanelButtonGetSMSCode_14(Button_t4055032469 * value)
	{
		___resetPanelButtonGetSMSCode_14 = value;
		Il2CppCodeGenWriteBarrier((&___resetPanelButtonGetSMSCode_14), value);
	}

	inline static int32_t get_offset_of_sessionVerifyPanelButtonVerify_15() { return static_cast<int32_t>(offsetof(main_t2227614042, ___sessionVerifyPanelButtonVerify_15)); }
	inline Button_t4055032469 * get_sessionVerifyPanelButtonVerify_15() const { return ___sessionVerifyPanelButtonVerify_15; }
	inline Button_t4055032469 ** get_address_of_sessionVerifyPanelButtonVerify_15() { return &___sessionVerifyPanelButtonVerify_15; }
	inline void set_sessionVerifyPanelButtonVerify_15(Button_t4055032469 * value)
	{
		___sessionVerifyPanelButtonVerify_15 = value;
		Il2CppCodeGenWriteBarrier((&___sessionVerifyPanelButtonVerify_15), value);
	}

	inline static int32_t get_offset_of_sessionVerifyPanelButtonExecute_16() { return static_cast<int32_t>(offsetof(main_t2227614042, ___sessionVerifyPanelButtonExecute_16)); }
	inline Button_t4055032469 * get_sessionVerifyPanelButtonExecute_16() const { return ___sessionVerifyPanelButtonExecute_16; }
	inline Button_t4055032469 ** get_address_of_sessionVerifyPanelButtonExecute_16() { return &___sessionVerifyPanelButtonExecute_16; }
	inline void set_sessionVerifyPanelButtonExecute_16(Button_t4055032469 * value)
	{
		___sessionVerifyPanelButtonExecute_16 = value;
		Il2CppCodeGenWriteBarrier((&___sessionVerifyPanelButtonExecute_16), value);
	}

	inline static int32_t get_offset_of_sessionVerifyPanelButtonLogout_17() { return static_cast<int32_t>(offsetof(main_t2227614042, ___sessionVerifyPanelButtonLogout_17)); }
	inline Button_t4055032469 * get_sessionVerifyPanelButtonLogout_17() const { return ___sessionVerifyPanelButtonLogout_17; }
	inline Button_t4055032469 ** get_address_of_sessionVerifyPanelButtonLogout_17() { return &___sessionVerifyPanelButtonLogout_17; }
	inline void set_sessionVerifyPanelButtonLogout_17(Button_t4055032469 * value)
	{
		___sessionVerifyPanelButtonLogout_17 = value;
		Il2CppCodeGenWriteBarrier((&___sessionVerifyPanelButtonLogout_17), value);
	}

	inline static int32_t get_offset_of_authenticator_18() { return static_cast<int32_t>(offsetof(main_t2227614042, ___authenticator_18)); }
	inline Authenticator_t530988528 * get_authenticator_18() const { return ___authenticator_18; }
	inline Authenticator_t530988528 ** get_address_of_authenticator_18() { return &___authenticator_18; }
	inline void set_authenticator_18(Authenticator_t530988528 * value)
	{
		___authenticator_18 = value;
		Il2CppCodeGenWriteBarrier((&___authenticator_18), value);
	}

	inline static int32_t get_offset_of_account_19() { return static_cast<int32_t>(offsetof(main_t2227614042, ___account_19)); }
	inline Account_t400769301 * get_account_19() const { return ___account_19; }
	inline Account_t400769301 ** get_address_of_account_19() { return &___account_19; }
	inline void set_account_19(Account_t400769301 * value)
	{
		___account_19 = value;
		Il2CppCodeGenWriteBarrier((&___account_19), value);
	}

	inline static int32_t get_offset_of_charge_20() { return static_cast<int32_t>(offsetof(main_t2227614042, ___charge_20)); }
	inline Charge_t1653125646 * get_charge_20() const { return ___charge_20; }
	inline Charge_t1653125646 ** get_address_of_charge_20() { return &___charge_20; }
	inline void set_charge_20(Charge_t1653125646 * value)
	{
		___charge_20 = value;
		Il2CppCodeGenWriteBarrier((&___charge_20), value);
	}

	inline static int32_t get_offset_of_market_21() { return static_cast<int32_t>(offsetof(main_t2227614042, ___market_21)); }
	inline Market_t538055513 * get_market_21() const { return ___market_21; }
	inline Market_t538055513 ** get_address_of_market_21() { return &___market_21; }
	inline void set_market_21(Market_t538055513 * value)
	{
		___market_21 = value;
		Il2CppCodeGenWriteBarrier((&___market_21), value);
	}
};

struct main_t2227614042_StaticFields
{
public:
	// System.Action`1<System.Int32> main::<>f__am$cache0
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache0_22;
	// System.Action`1<System.Int32> main::<>f__am$cache1
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache1_23;
	// System.Action`1<System.Int32> main::<>f__am$cache2
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache2_24;
	// System.Action`1<System.Int32> main::<>f__am$cache3
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache3_25;
	// System.Action`1<System.Int32> main::<>f__am$cache4
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache4_26;
	// System.Action`1<System.Int32> main::<>f__am$cache5
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache5_27;
	// System.Func`5<System.String,System.String,System.Object,System.Action`2<System.Int32,System.Object>,System.Collections.IEnumerator> main::<>f__mg$cache0
	Func_5_t696329592 * ___U3CU3Ef__mgU24cache0_28;
	// System.Action`2<System.Int32,System.Object> main::<>f__am$cache6
	Action_2_t11315885 * ___U3CU3Ef__amU24cache6_29;
	// System.Action`1<System.Int32> main::<>f__am$cache7
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache7_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_23() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache1_23)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache1_23() const { return ___U3CU3Ef__amU24cache1_23; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache1_23() { return &___U3CU3Ef__amU24cache1_23; }
	inline void set_U3CU3Ef__amU24cache1_23(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_24() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache2_24)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache2_24() const { return ___U3CU3Ef__amU24cache2_24; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache2_24() { return &___U3CU3Ef__amU24cache2_24; }
	inline void set_U3CU3Ef__amU24cache2_24(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache2_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_25() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache3_25)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache3_25() const { return ___U3CU3Ef__amU24cache3_25; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache3_25() { return &___U3CU3Ef__amU24cache3_25; }
	inline void set_U3CU3Ef__amU24cache3_25(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache3_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_26() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache4_26)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache4_26() const { return ___U3CU3Ef__amU24cache4_26; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache4_26() { return &___U3CU3Ef__amU24cache4_26; }
	inline void set_U3CU3Ef__amU24cache4_26(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache4_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_27() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache5_27)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache5_27() const { return ___U3CU3Ef__amU24cache5_27; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache5_27() { return &___U3CU3Ef__amU24cache5_27; }
	inline void set_U3CU3Ef__amU24cache5_27(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache5_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_28() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__mgU24cache0_28)); }
	inline Func_5_t696329592 * get_U3CU3Ef__mgU24cache0_28() const { return ___U3CU3Ef__mgU24cache0_28; }
	inline Func_5_t696329592 ** get_address_of_U3CU3Ef__mgU24cache0_28() { return &___U3CU3Ef__mgU24cache0_28; }
	inline void set_U3CU3Ef__mgU24cache0_28(Func_5_t696329592 * value)
	{
		___U3CU3Ef__mgU24cache0_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_29() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache6_29)); }
	inline Action_2_t11315885 * get_U3CU3Ef__amU24cache6_29() const { return ___U3CU3Ef__amU24cache6_29; }
	inline Action_2_t11315885 ** get_address_of_U3CU3Ef__amU24cache6_29() { return &___U3CU3Ef__amU24cache6_29; }
	inline void set_U3CU3Ef__amU24cache6_29(Action_2_t11315885 * value)
	{
		___U3CU3Ef__amU24cache6_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_30() { return static_cast<int32_t>(offsetof(main_t2227614042_StaticFields, ___U3CU3Ef__amU24cache7_30)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache7_30() const { return ___U3CU3Ef__amU24cache7_30; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache7_30() { return &___U3CU3Ef__amU24cache7_30; }
	inline void set_U3CU3Ef__amU24cache7_30(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache7_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T2227614042_H
#ifndef CCOSMOSEFFECT_T495978253_H
#define CCOSMOSEFFECT_T495978253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCosmosEffect
struct  CCosmosEffect_t495978253  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup CCosmosEffect::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_4;
	// UnityEngine.UI.Image CCosmosEffect::_imgMain
	Image_t2670269651 * ____imgMain_5;
	// UnityEngine.SpriteRenderer CCosmosEffect::_sprMain
	SpriteRenderer_t3235626157 * ____sprMain_6;
	// System.Boolean CCosmosEffect::m_bUI
	bool ___m_bUI_7;
	// UnityEngine.SpriteRenderer CCosmosEffect::_srRotationPic
	SpriteRenderer_t3235626157 * ____srRotationPic_8;
	// UnityEngine.UI.Image CCosmosEffect::_imgRotationPic
	Image_t2670269651 * ____imgRotationPic_9;
	// System.Boolean CCosmosEffect::m_bRotating
	bool ___m_bRotating_10;
	// System.Single CCosmosEffect::m_fRotatingSpeed
	float ___m_fRotatingSpeed_11;
	// System.Single CCosmosEffect::m_fRotatingAngle
	float ___m_fRotatingAngle_12;
	// System.Boolean CCosmosEffect::m_bAlphaChange
	bool ___m_bAlphaChange_13;
	// System.Single CCosmosEffect::m_fMaxAlpha
	float ___m_fMaxAlpha_14;
	// System.Single CCosmosEffect::m_fMinAlpha
	float ___m_fMinAlpha_15;
	// System.Single CCosmosEffect::m_fAlphaChangeSpeed
	float ___m_fAlphaChangeSpeed_16;

public:
	inline static int32_t get_offset_of__canvasGroup_4() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____canvasGroup_4)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_4() const { return ____canvasGroup_4; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_4() { return &____canvasGroup_4; }
	inline void set__canvasGroup_4(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_4), value);
	}

	inline static int32_t get_offset_of__imgMain_5() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____imgMain_5)); }
	inline Image_t2670269651 * get__imgMain_5() const { return ____imgMain_5; }
	inline Image_t2670269651 ** get_address_of__imgMain_5() { return &____imgMain_5; }
	inline void set__imgMain_5(Image_t2670269651 * value)
	{
		____imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_5), value);
	}

	inline static int32_t get_offset_of__sprMain_6() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____sprMain_6)); }
	inline SpriteRenderer_t3235626157 * get__sprMain_6() const { return ____sprMain_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprMain_6() { return &____sprMain_6; }
	inline void set__sprMain_6(SpriteRenderer_t3235626157 * value)
	{
		____sprMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprMain_6), value);
	}

	inline static int32_t get_offset_of_m_bUI_7() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bUI_7)); }
	inline bool get_m_bUI_7() const { return ___m_bUI_7; }
	inline bool* get_address_of_m_bUI_7() { return &___m_bUI_7; }
	inline void set_m_bUI_7(bool value)
	{
		___m_bUI_7 = value;
	}

	inline static int32_t get_offset_of__srRotationPic_8() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____srRotationPic_8)); }
	inline SpriteRenderer_t3235626157 * get__srRotationPic_8() const { return ____srRotationPic_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srRotationPic_8() { return &____srRotationPic_8; }
	inline void set__srRotationPic_8(SpriteRenderer_t3235626157 * value)
	{
		____srRotationPic_8 = value;
		Il2CppCodeGenWriteBarrier((&____srRotationPic_8), value);
	}

	inline static int32_t get_offset_of__imgRotationPic_9() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____imgRotationPic_9)); }
	inline Image_t2670269651 * get__imgRotationPic_9() const { return ____imgRotationPic_9; }
	inline Image_t2670269651 ** get_address_of__imgRotationPic_9() { return &____imgRotationPic_9; }
	inline void set__imgRotationPic_9(Image_t2670269651 * value)
	{
		____imgRotationPic_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgRotationPic_9), value);
	}

	inline static int32_t get_offset_of_m_bRotating_10() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bRotating_10)); }
	inline bool get_m_bRotating_10() const { return ___m_bRotating_10; }
	inline bool* get_address_of_m_bRotating_10() { return &___m_bRotating_10; }
	inline void set_m_bRotating_10(bool value)
	{
		___m_bRotating_10 = value;
	}

	inline static int32_t get_offset_of_m_fRotatingSpeed_11() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fRotatingSpeed_11)); }
	inline float get_m_fRotatingSpeed_11() const { return ___m_fRotatingSpeed_11; }
	inline float* get_address_of_m_fRotatingSpeed_11() { return &___m_fRotatingSpeed_11; }
	inline void set_m_fRotatingSpeed_11(float value)
	{
		___m_fRotatingSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_fRotatingAngle_12() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fRotatingAngle_12)); }
	inline float get_m_fRotatingAngle_12() const { return ___m_fRotatingAngle_12; }
	inline float* get_address_of_m_fRotatingAngle_12() { return &___m_fRotatingAngle_12; }
	inline void set_m_fRotatingAngle_12(float value)
	{
		___m_fRotatingAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_bAlphaChange_13() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bAlphaChange_13)); }
	inline bool get_m_bAlphaChange_13() const { return ___m_bAlphaChange_13; }
	inline bool* get_address_of_m_bAlphaChange_13() { return &___m_bAlphaChange_13; }
	inline void set_m_bAlphaChange_13(bool value)
	{
		___m_bAlphaChange_13 = value;
	}

	inline static int32_t get_offset_of_m_fMaxAlpha_14() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fMaxAlpha_14)); }
	inline float get_m_fMaxAlpha_14() const { return ___m_fMaxAlpha_14; }
	inline float* get_address_of_m_fMaxAlpha_14() { return &___m_fMaxAlpha_14; }
	inline void set_m_fMaxAlpha_14(float value)
	{
		___m_fMaxAlpha_14 = value;
	}

	inline static int32_t get_offset_of_m_fMinAlpha_15() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fMinAlpha_15)); }
	inline float get_m_fMinAlpha_15() const { return ___m_fMinAlpha_15; }
	inline float* get_address_of_m_fMinAlpha_15() { return &___m_fMinAlpha_15; }
	inline void set_m_fMinAlpha_15(float value)
	{
		___m_fMinAlpha_15 = value;
	}

	inline static int32_t get_offset_of_m_fAlphaChangeSpeed_16() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fAlphaChangeSpeed_16)); }
	inline float get_m_fAlphaChangeSpeed_16() const { return ___m_fAlphaChangeSpeed_16; }
	inline float* get_address_of_m_fAlphaChangeSpeed_16() { return &___m_fAlphaChangeSpeed_16; }
	inline void set_m_fAlphaChangeSpeed_16(float value)
	{
		___m_fAlphaChangeSpeed_16 = value;
	}
};

struct CCosmosEffect_t495978253_StaticFields
{
public:
	// UnityEngine.Vector3 CCosmosEffect::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCosmosEffect::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOSMOSEFFECT_T495978253_H
#ifndef CBALLEFFECT_T2512637482_H
#define CBALLEFFECT_T2512637482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBallEffect
struct  CBallEffect_t2512637482  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CBallEffect::m_aryContainer
	GameObjectU5BU5D_t3328599146* ___m_aryContainer_2;
	// CFrameAnimationEffect[] CBallEffect::m_aryQianYao
	CFrameAnimationEffectU5BU5D_t897573229* ___m_aryQianYao_3;
	// CFrameAnimationEffect[] CBallEffect::m_aryChiXu
	CFrameAnimationEffectU5BU5D_t897573229* ___m_aryChiXu_4;

public:
	inline static int32_t get_offset_of_m_aryContainer_2() { return static_cast<int32_t>(offsetof(CBallEffect_t2512637482, ___m_aryContainer_2)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryContainer_2() const { return ___m_aryContainer_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryContainer_2() { return &___m_aryContainer_2; }
	inline void set_m_aryContainer_2(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryContainer_2), value);
	}

	inline static int32_t get_offset_of_m_aryQianYao_3() { return static_cast<int32_t>(offsetof(CBallEffect_t2512637482, ___m_aryQianYao_3)); }
	inline CFrameAnimationEffectU5BU5D_t897573229* get_m_aryQianYao_3() const { return ___m_aryQianYao_3; }
	inline CFrameAnimationEffectU5BU5D_t897573229** get_address_of_m_aryQianYao_3() { return &___m_aryQianYao_3; }
	inline void set_m_aryQianYao_3(CFrameAnimationEffectU5BU5D_t897573229* value)
	{
		___m_aryQianYao_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryQianYao_3), value);
	}

	inline static int32_t get_offset_of_m_aryChiXu_4() { return static_cast<int32_t>(offsetof(CBallEffect_t2512637482, ___m_aryChiXu_4)); }
	inline CFrameAnimationEffectU5BU5D_t897573229* get_m_aryChiXu_4() const { return ___m_aryChiXu_4; }
	inline CFrameAnimationEffectU5BU5D_t897573229** get_address_of_m_aryChiXu_4() { return &___m_aryChiXu_4; }
	inline void set_m_aryChiXu_4(CFrameAnimationEffectU5BU5D_t897573229* value)
	{
		___m_aryChiXu_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryChiXu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBALLEFFECT_T2512637482_H
#ifndef CBLACKANDWHITESHADER_T2223015440_H
#define CBLACKANDWHITESHADER_T2223015440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBlackAndWhiteShader
struct  CBlackAndWhiteShader_t2223015440  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader CBlackAndWhiteShader::curShader
	Shader_t4151988712 * ___curShader_3;
	// System.Single CBlackAndWhiteShader::grayScaleAmount
	float ___grayScaleAmount_4;
	// UnityEngine.Material CBlackAndWhiteShader::curMaterial
	Material_t340375123 * ___curMaterial_5;
	// System.Single CBlackAndWhiteShader::m_fFadeTotalTime
	float ___m_fFadeTotalTime_6;
	// System.Boolean CBlackAndWhiteShader::m_bFading
	bool ___m_bFading_7;

public:
	inline static int32_t get_offset_of_curShader_3() { return static_cast<int32_t>(offsetof(CBlackAndWhiteShader_t2223015440, ___curShader_3)); }
	inline Shader_t4151988712 * get_curShader_3() const { return ___curShader_3; }
	inline Shader_t4151988712 ** get_address_of_curShader_3() { return &___curShader_3; }
	inline void set_curShader_3(Shader_t4151988712 * value)
	{
		___curShader_3 = value;
		Il2CppCodeGenWriteBarrier((&___curShader_3), value);
	}

	inline static int32_t get_offset_of_grayScaleAmount_4() { return static_cast<int32_t>(offsetof(CBlackAndWhiteShader_t2223015440, ___grayScaleAmount_4)); }
	inline float get_grayScaleAmount_4() const { return ___grayScaleAmount_4; }
	inline float* get_address_of_grayScaleAmount_4() { return &___grayScaleAmount_4; }
	inline void set_grayScaleAmount_4(float value)
	{
		___grayScaleAmount_4 = value;
	}

	inline static int32_t get_offset_of_curMaterial_5() { return static_cast<int32_t>(offsetof(CBlackAndWhiteShader_t2223015440, ___curMaterial_5)); }
	inline Material_t340375123 * get_curMaterial_5() const { return ___curMaterial_5; }
	inline Material_t340375123 ** get_address_of_curMaterial_5() { return &___curMaterial_5; }
	inline void set_curMaterial_5(Material_t340375123 * value)
	{
		___curMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___curMaterial_5), value);
	}

	inline static int32_t get_offset_of_m_fFadeTotalTime_6() { return static_cast<int32_t>(offsetof(CBlackAndWhiteShader_t2223015440, ___m_fFadeTotalTime_6)); }
	inline float get_m_fFadeTotalTime_6() const { return ___m_fFadeTotalTime_6; }
	inline float* get_address_of_m_fFadeTotalTime_6() { return &___m_fFadeTotalTime_6; }
	inline void set_m_fFadeTotalTime_6(float value)
	{
		___m_fFadeTotalTime_6 = value;
	}

	inline static int32_t get_offset_of_m_bFading_7() { return static_cast<int32_t>(offsetof(CBlackAndWhiteShader_t2223015440, ___m_bFading_7)); }
	inline bool get_m_bFading_7() const { return ___m_bFading_7; }
	inline bool* get_address_of_m_bFading_7() { return &___m_bFading_7; }
	inline void set_m_bFading_7(bool value)
	{
		___m_bFading_7 = value;
	}
};

struct CBlackAndWhiteShader_t2223015440_StaticFields
{
public:
	// CBlackAndWhiteShader CBlackAndWhiteShader::s_Instance
	CBlackAndWhiteShader_t2223015440 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CBlackAndWhiteShader_t2223015440_StaticFields, ___s_Instance_2)); }
	inline CBlackAndWhiteShader_t2223015440 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CBlackAndWhiteShader_t2223015440 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CBlackAndWhiteShader_t2223015440 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBLACKANDWHITESHADER_T2223015440_H
#ifndef CMONSTER_T1941470938_H
#define CMONSTER_T1941470938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonster
struct  CMonster_t1941470938  : public MonoBehaviour_t3962482529
{
public:
	// CMonsterEditor/sThornConfig CMonster::m_Config
	sThornConfig_t1242444451  ___m_Config_2;
	// System.Boolean CMonster::m_bIsJueSheng
	bool ___m_bIsJueSheng_3;
	// UnityEngine.TextMesh CMonster::_tmText
	TextMesh_t1536577757 * ____tmText_8;
	// UnityEngine.Rigidbody2D CMonster::_rigid
	Rigidbody2D_t939494601 * ____rigid_9;
	// System.Int64 CMonster::m_nGUID
	int64_t ___m_nGUID_10;
	// System.String CMonster::m_szConfigId
	String_t* ___m_szConfigId_11;
	// UnityEngine.CircleCollider2D CMonster::_Trigger
	CircleCollider2D_t662546754 * ____Trigger_12;
	// UnityEngine.SpriteRenderer CMonster::_srMain
	SpriteRenderer_t3235626157 * ____srMain_13;
	// System.Single CMonster::m_fSize
	float ___m_fSize_14;
	// System.Single CMonster::m_fRadius
	float ___m_fRadius_15;
	// CMonster/eMonsterType CMonster::m_eMonsterType
	int32_t ___m_eMonsterType_16;
	// CMonsterEditor/eMonsterBuildingType CMonster::m_eMonsterBuildingType
	int32_t ___m_eMonsterBuildingType_17;
	// System.Int32[] CMonster::m_aryIntParam
	Int32U5BU5D_t385246372* ___m_aryIntParam_18;
	// System.Single[] CMonster::m_aryFloatParam
	SingleU5BU5D_t1444911251* ___m_aryFloatParam_19;
	// System.Boolean CMonster::m_bDead
	bool ___m_bDead_20;
	// System.Single CMonster::m_fDeadOccurTime
	float ___m_fDeadOccurTime_21;
	// System.Single CMonster::m_fRebornTime
	float ___m_fRebornTime_22;
	// UnityEngine.Vector2 CMonster::m_Dir
	Vector2_t2156229523  ___m_Dir_23;
	// UnityEngine.Vector3 CMonster::m_Pos
	Vector3_t3722313464  ___m_Pos_24;
	// UnityEngine.Vector2 CMonster::m_vEjectSpeed
	Vector2_t2156229523  ___m_vEjectSpeed_25;
	// System.Single CMonster::m_fjectTotalDis
	float ___m_fjectTotalDis_26;
	// System.Boolean CMonster::m_bEjecting
	bool ___m_bEjecting_27;
	// UnityEngine.Vector3 CMonster::m_vEjectStartPos
	Vector3_t3722313464  ___m_vEjectStartPos_28;
	// Player CMonster::m_Player
	Player_t3266647312 * ___m_Player_29;
	// Ball CMonster::m_Ball
	Ball_t2206666566 * ___m_Ball_30;
	// UnityEngine.GameObject CMonster::m_goContainer
	GameObject_t1113636619 * ___m_goContainer_31;
	// System.Boolean CMonster::m_bAutoReborn
	bool ___m_bAutoReborn_32;
	// System.Single CMonster::m_fRotation
	float ___m_fRotation_33;
	// System.Single CMonster::m_fRotationSpeed
	float ___m_fRotationSpeed_34;
	// System.Boolean CMonster::m_bNeedSync
	bool ___m_bNeedSync_35;
	// UnityEngine.Vector3 CMonster::m_vecInitPos
	Vector3_t3722313464  ___m_vecInitPos_36;
	// Spine.Unity.SkeletonAnimation CMonster::m_aniCiChuXian
	SkeletonAnimation_t3693186521 * ___m_aniCiChuXian_37;
	// System.Single CMonster::m_fCiChuXianTimeLeft
	float ___m_fCiChuXianTimeLeft_38;

public:
	inline static int32_t get_offset_of_m_Config_2() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_Config_2)); }
	inline sThornConfig_t1242444451  get_m_Config_2() const { return ___m_Config_2; }
	inline sThornConfig_t1242444451 * get_address_of_m_Config_2() { return &___m_Config_2; }
	inline void set_m_Config_2(sThornConfig_t1242444451  value)
	{
		___m_Config_2 = value;
	}

	inline static int32_t get_offset_of_m_bIsJueSheng_3() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_bIsJueSheng_3)); }
	inline bool get_m_bIsJueSheng_3() const { return ___m_bIsJueSheng_3; }
	inline bool* get_address_of_m_bIsJueSheng_3() { return &___m_bIsJueSheng_3; }
	inline void set_m_bIsJueSheng_3(bool value)
	{
		___m_bIsJueSheng_3 = value;
	}

	inline static int32_t get_offset_of__tmText_8() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ____tmText_8)); }
	inline TextMesh_t1536577757 * get__tmText_8() const { return ____tmText_8; }
	inline TextMesh_t1536577757 ** get_address_of__tmText_8() { return &____tmText_8; }
	inline void set__tmText_8(TextMesh_t1536577757 * value)
	{
		____tmText_8 = value;
		Il2CppCodeGenWriteBarrier((&____tmText_8), value);
	}

	inline static int32_t get_offset_of__rigid_9() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ____rigid_9)); }
	inline Rigidbody2D_t939494601 * get__rigid_9() const { return ____rigid_9; }
	inline Rigidbody2D_t939494601 ** get_address_of__rigid_9() { return &____rigid_9; }
	inline void set__rigid_9(Rigidbody2D_t939494601 * value)
	{
		____rigid_9 = value;
		Il2CppCodeGenWriteBarrier((&____rigid_9), value);
	}

	inline static int32_t get_offset_of_m_nGUID_10() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_nGUID_10)); }
	inline int64_t get_m_nGUID_10() const { return ___m_nGUID_10; }
	inline int64_t* get_address_of_m_nGUID_10() { return &___m_nGUID_10; }
	inline void set_m_nGUID_10(int64_t value)
	{
		___m_nGUID_10 = value;
	}

	inline static int32_t get_offset_of_m_szConfigId_11() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_szConfigId_11)); }
	inline String_t* get_m_szConfigId_11() const { return ___m_szConfigId_11; }
	inline String_t** get_address_of_m_szConfigId_11() { return &___m_szConfigId_11; }
	inline void set_m_szConfigId_11(String_t* value)
	{
		___m_szConfigId_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_szConfigId_11), value);
	}

	inline static int32_t get_offset_of__Trigger_12() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ____Trigger_12)); }
	inline CircleCollider2D_t662546754 * get__Trigger_12() const { return ____Trigger_12; }
	inline CircleCollider2D_t662546754 ** get_address_of__Trigger_12() { return &____Trigger_12; }
	inline void set__Trigger_12(CircleCollider2D_t662546754 * value)
	{
		____Trigger_12 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_12), value);
	}

	inline static int32_t get_offset_of__srMain_13() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ____srMain_13)); }
	inline SpriteRenderer_t3235626157 * get__srMain_13() const { return ____srMain_13; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_13() { return &____srMain_13; }
	inline void set__srMain_13(SpriteRenderer_t3235626157 * value)
	{
		____srMain_13 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_13), value);
	}

	inline static int32_t get_offset_of_m_fSize_14() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fSize_14)); }
	inline float get_m_fSize_14() const { return ___m_fSize_14; }
	inline float* get_address_of_m_fSize_14() { return &___m_fSize_14; }
	inline void set_m_fSize_14(float value)
	{
		___m_fSize_14 = value;
	}

	inline static int32_t get_offset_of_m_fRadius_15() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fRadius_15)); }
	inline float get_m_fRadius_15() const { return ___m_fRadius_15; }
	inline float* get_address_of_m_fRadius_15() { return &___m_fRadius_15; }
	inline void set_m_fRadius_15(float value)
	{
		___m_fRadius_15 = value;
	}

	inline static int32_t get_offset_of_m_eMonsterType_16() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_eMonsterType_16)); }
	inline int32_t get_m_eMonsterType_16() const { return ___m_eMonsterType_16; }
	inline int32_t* get_address_of_m_eMonsterType_16() { return &___m_eMonsterType_16; }
	inline void set_m_eMonsterType_16(int32_t value)
	{
		___m_eMonsterType_16 = value;
	}

	inline static int32_t get_offset_of_m_eMonsterBuildingType_17() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_eMonsterBuildingType_17)); }
	inline int32_t get_m_eMonsterBuildingType_17() const { return ___m_eMonsterBuildingType_17; }
	inline int32_t* get_address_of_m_eMonsterBuildingType_17() { return &___m_eMonsterBuildingType_17; }
	inline void set_m_eMonsterBuildingType_17(int32_t value)
	{
		___m_eMonsterBuildingType_17 = value;
	}

	inline static int32_t get_offset_of_m_aryIntParam_18() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_aryIntParam_18)); }
	inline Int32U5BU5D_t385246372* get_m_aryIntParam_18() const { return ___m_aryIntParam_18; }
	inline Int32U5BU5D_t385246372** get_address_of_m_aryIntParam_18() { return &___m_aryIntParam_18; }
	inline void set_m_aryIntParam_18(Int32U5BU5D_t385246372* value)
	{
		___m_aryIntParam_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryIntParam_18), value);
	}

	inline static int32_t get_offset_of_m_aryFloatParam_19() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_aryFloatParam_19)); }
	inline SingleU5BU5D_t1444911251* get_m_aryFloatParam_19() const { return ___m_aryFloatParam_19; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryFloatParam_19() { return &___m_aryFloatParam_19; }
	inline void set_m_aryFloatParam_19(SingleU5BU5D_t1444911251* value)
	{
		___m_aryFloatParam_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFloatParam_19), value);
	}

	inline static int32_t get_offset_of_m_bDead_20() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_bDead_20)); }
	inline bool get_m_bDead_20() const { return ___m_bDead_20; }
	inline bool* get_address_of_m_bDead_20() { return &___m_bDead_20; }
	inline void set_m_bDead_20(bool value)
	{
		___m_bDead_20 = value;
	}

	inline static int32_t get_offset_of_m_fDeadOccurTime_21() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fDeadOccurTime_21)); }
	inline float get_m_fDeadOccurTime_21() const { return ___m_fDeadOccurTime_21; }
	inline float* get_address_of_m_fDeadOccurTime_21() { return &___m_fDeadOccurTime_21; }
	inline void set_m_fDeadOccurTime_21(float value)
	{
		___m_fDeadOccurTime_21 = value;
	}

	inline static int32_t get_offset_of_m_fRebornTime_22() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fRebornTime_22)); }
	inline float get_m_fRebornTime_22() const { return ___m_fRebornTime_22; }
	inline float* get_address_of_m_fRebornTime_22() { return &___m_fRebornTime_22; }
	inline void set_m_fRebornTime_22(float value)
	{
		___m_fRebornTime_22 = value;
	}

	inline static int32_t get_offset_of_m_Dir_23() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_Dir_23)); }
	inline Vector2_t2156229523  get_m_Dir_23() const { return ___m_Dir_23; }
	inline Vector2_t2156229523 * get_address_of_m_Dir_23() { return &___m_Dir_23; }
	inline void set_m_Dir_23(Vector2_t2156229523  value)
	{
		___m_Dir_23 = value;
	}

	inline static int32_t get_offset_of_m_Pos_24() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_Pos_24)); }
	inline Vector3_t3722313464  get_m_Pos_24() const { return ___m_Pos_24; }
	inline Vector3_t3722313464 * get_address_of_m_Pos_24() { return &___m_Pos_24; }
	inline void set_m_Pos_24(Vector3_t3722313464  value)
	{
		___m_Pos_24 = value;
	}

	inline static int32_t get_offset_of_m_vEjectSpeed_25() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_vEjectSpeed_25)); }
	inline Vector2_t2156229523  get_m_vEjectSpeed_25() const { return ___m_vEjectSpeed_25; }
	inline Vector2_t2156229523 * get_address_of_m_vEjectSpeed_25() { return &___m_vEjectSpeed_25; }
	inline void set_m_vEjectSpeed_25(Vector2_t2156229523  value)
	{
		___m_vEjectSpeed_25 = value;
	}

	inline static int32_t get_offset_of_m_fjectTotalDis_26() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fjectTotalDis_26)); }
	inline float get_m_fjectTotalDis_26() const { return ___m_fjectTotalDis_26; }
	inline float* get_address_of_m_fjectTotalDis_26() { return &___m_fjectTotalDis_26; }
	inline void set_m_fjectTotalDis_26(float value)
	{
		___m_fjectTotalDis_26 = value;
	}

	inline static int32_t get_offset_of_m_bEjecting_27() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_bEjecting_27)); }
	inline bool get_m_bEjecting_27() const { return ___m_bEjecting_27; }
	inline bool* get_address_of_m_bEjecting_27() { return &___m_bEjecting_27; }
	inline void set_m_bEjecting_27(bool value)
	{
		___m_bEjecting_27 = value;
	}

	inline static int32_t get_offset_of_m_vEjectStartPos_28() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_vEjectStartPos_28)); }
	inline Vector3_t3722313464  get_m_vEjectStartPos_28() const { return ___m_vEjectStartPos_28; }
	inline Vector3_t3722313464 * get_address_of_m_vEjectStartPos_28() { return &___m_vEjectStartPos_28; }
	inline void set_m_vEjectStartPos_28(Vector3_t3722313464  value)
	{
		___m_vEjectStartPos_28 = value;
	}

	inline static int32_t get_offset_of_m_Player_29() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_Player_29)); }
	inline Player_t3266647312 * get_m_Player_29() const { return ___m_Player_29; }
	inline Player_t3266647312 ** get_address_of_m_Player_29() { return &___m_Player_29; }
	inline void set_m_Player_29(Player_t3266647312 * value)
	{
		___m_Player_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Player_29), value);
	}

	inline static int32_t get_offset_of_m_Ball_30() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_Ball_30)); }
	inline Ball_t2206666566 * get_m_Ball_30() const { return ___m_Ball_30; }
	inline Ball_t2206666566 ** get_address_of_m_Ball_30() { return &___m_Ball_30; }
	inline void set_m_Ball_30(Ball_t2206666566 * value)
	{
		___m_Ball_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Ball_30), value);
	}

	inline static int32_t get_offset_of_m_goContainer_31() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_goContainer_31)); }
	inline GameObject_t1113636619 * get_m_goContainer_31() const { return ___m_goContainer_31; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainer_31() { return &___m_goContainer_31; }
	inline void set_m_goContainer_31(GameObject_t1113636619 * value)
	{
		___m_goContainer_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainer_31), value);
	}

	inline static int32_t get_offset_of_m_bAutoReborn_32() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_bAutoReborn_32)); }
	inline bool get_m_bAutoReborn_32() const { return ___m_bAutoReborn_32; }
	inline bool* get_address_of_m_bAutoReborn_32() { return &___m_bAutoReborn_32; }
	inline void set_m_bAutoReborn_32(bool value)
	{
		___m_bAutoReborn_32 = value;
	}

	inline static int32_t get_offset_of_m_fRotation_33() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fRotation_33)); }
	inline float get_m_fRotation_33() const { return ___m_fRotation_33; }
	inline float* get_address_of_m_fRotation_33() { return &___m_fRotation_33; }
	inline void set_m_fRotation_33(float value)
	{
		___m_fRotation_33 = value;
	}

	inline static int32_t get_offset_of_m_fRotationSpeed_34() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fRotationSpeed_34)); }
	inline float get_m_fRotationSpeed_34() const { return ___m_fRotationSpeed_34; }
	inline float* get_address_of_m_fRotationSpeed_34() { return &___m_fRotationSpeed_34; }
	inline void set_m_fRotationSpeed_34(float value)
	{
		___m_fRotationSpeed_34 = value;
	}

	inline static int32_t get_offset_of_m_bNeedSync_35() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_bNeedSync_35)); }
	inline bool get_m_bNeedSync_35() const { return ___m_bNeedSync_35; }
	inline bool* get_address_of_m_bNeedSync_35() { return &___m_bNeedSync_35; }
	inline void set_m_bNeedSync_35(bool value)
	{
		___m_bNeedSync_35 = value;
	}

	inline static int32_t get_offset_of_m_vecInitPos_36() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_vecInitPos_36)); }
	inline Vector3_t3722313464  get_m_vecInitPos_36() const { return ___m_vecInitPos_36; }
	inline Vector3_t3722313464 * get_address_of_m_vecInitPos_36() { return &___m_vecInitPos_36; }
	inline void set_m_vecInitPos_36(Vector3_t3722313464  value)
	{
		___m_vecInitPos_36 = value;
	}

	inline static int32_t get_offset_of_m_aniCiChuXian_37() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_aniCiChuXian_37)); }
	inline SkeletonAnimation_t3693186521 * get_m_aniCiChuXian_37() const { return ___m_aniCiChuXian_37; }
	inline SkeletonAnimation_t3693186521 ** get_address_of_m_aniCiChuXian_37() { return &___m_aniCiChuXian_37; }
	inline void set_m_aniCiChuXian_37(SkeletonAnimation_t3693186521 * value)
	{
		___m_aniCiChuXian_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_aniCiChuXian_37), value);
	}

	inline static int32_t get_offset_of_m_fCiChuXianTimeLeft_38() { return static_cast<int32_t>(offsetof(CMonster_t1941470938, ___m_fCiChuXianTimeLeft_38)); }
	inline float get_m_fCiChuXianTimeLeft_38() const { return ___m_fCiChuXianTimeLeft_38; }
	inline float* get_address_of_m_fCiChuXianTimeLeft_38() { return &___m_fCiChuXianTimeLeft_38; }
	inline void set_m_fCiChuXianTimeLeft_38(float value)
	{
		___m_fCiChuXianTimeLeft_38 = value;
	}
};

struct CMonster_t1941470938_StaticFields
{
public:
	// UnityEngine.Vector2 CMonster::vec2Temp
	Vector2_t2156229523  ___vec2Temp_4;
	// UnityEngine.Vector3 CMonster::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Vector3 CMonster::vecTempScale
	Vector3_t3722313464  ___vecTempScale_6;
	// UnityEngine.Color CMonster::cTempColor
	Color_t2555686324  ___cTempColor_7;

public:
	inline static int32_t get_offset_of_vec2Temp_4() { return static_cast<int32_t>(offsetof(CMonster_t1941470938_StaticFields, ___vec2Temp_4)); }
	inline Vector2_t2156229523  get_vec2Temp_4() const { return ___vec2Temp_4; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_4() { return &___vec2Temp_4; }
	inline void set_vec2Temp_4(Vector2_t2156229523  value)
	{
		___vec2Temp_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CMonster_t1941470938_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_6() { return static_cast<int32_t>(offsetof(CMonster_t1941470938_StaticFields, ___vecTempScale_6)); }
	inline Vector3_t3722313464  get_vecTempScale_6() const { return ___vecTempScale_6; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_6() { return &___vecTempScale_6; }
	inline void set_vecTempScale_6(Vector3_t3722313464  value)
	{
		___vecTempScale_6 = value;
	}

	inline static int32_t get_offset_of_cTempColor_7() { return static_cast<int32_t>(offsetof(CMonster_t1941470938_StaticFields, ___cTempColor_7)); }
	inline Color_t2555686324  get_cTempColor_7() const { return ___cTempColor_7; }
	inline Color_t2555686324 * get_address_of_cTempColor_7() { return &___cTempColor_7; }
	inline void set_cTempColor_7(Color_t2555686324  value)
	{
		___cTempColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMONSTER_T1941470938_H
#ifndef CBLOODSTAINMANAGER_T2257855270_H
#define CBLOODSTAINMANAGER_T2257855270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBloodStainManager
struct  CBloodStainManager_t2257855270  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CBloodStainManager::m_preFrameAni_XiangQianPenJian
	GameObject_t1113636619 * ___m_preFrameAni_XiangQianPenJian_5;
	// UnityEngine.GameObject[] CBloodStainManager::m_aryBloodStainPrefab
	GameObjectU5BU5D_t3328599146* ___m_aryBloodStainPrefab_6;
	// System.Single[] CBloodStainManager::m_aryBloodSize2BallSize
	SingleU5BU5D_t1444911251* ___m_aryBloodSize2BallSize_7;
	// System.Single[] CBloodStainManager::m_aryLastTime
	SingleU5BU5D_t1444911251* ___m_aryLastTime_8;
	// System.Single[] CBloodStainManager::m_aryFadeTime
	SingleU5BU5D_t1444911251* ___m_aryFadeTime_9;
	// System.Single CBloodStainManager::m_fDynamicScaleTime
	float ___m_fDynamicScaleTime_10;
	// UnityEngine.GameObject CBloodStainManager::m_preBloodTile
	GameObject_t1113636619 * ___m_preBloodTile_11;
	// System.Single CBloodStainManager::m_fBloodTileInterval
	float ___m_fBloodTileInterval_12;
	// UnityEngine.GameObject CBloodStainManager::m_preStain
	GameObject_t1113636619 * ___m_preStain_13;
	// UnityEngine.GameObject CBloodStainManager::m_goArea0
	GameObject_t1113636619 * ___m_goArea0_14;
	// System.Single CBloodStainManager::m_fXiangQianPenJianScale
	float ___m_fXiangQianPenJianScale_15;
	// System.Single CBloodStainManager::m_fXiangQianPenJianPosDirX
	float ___m_fXiangQianPenJianPosDirX_16;
	// System.Single CBloodStainManager::m_fXiangQianPenJianPosDirY
	float ___m_fXiangQianPenJianPosDirY_17;
	// System.Collections.Generic.Dictionary`2<CBloodStainManager/eBloodStainType,System.Collections.Generic.List`1<CBloodStain>> CBloodStainManager::m_dicRecycledBloodStain
	Dictionary_2_t2042087675 * ___m_dicRecycledBloodStain_18;
	// System.Collections.Generic.Dictionary`2<System.UInt32,CBloodStain> CBloodStainManager::m_dicBloodStains
	Dictionary_2_t722626127 * ___m_dicBloodStains_19;
	// System.Collections.Generic.List`1<CBloodStain> CBloodStainManager::m_lstRecycledBloodStains
	List_1_t920433207 * ___m_lstRecycledBloodStains_20;
	// System.Collections.Generic.List`1<CBloodStain> CBloodStainManager::m_lstFading
	List_1_t920433207 * ___m_lstFading_21;
	// System.Single CBloodStainManager::m_fFadeTime
	float ___m_fFadeTime_22;

public:
	inline static int32_t get_offset_of_m_preFrameAni_XiangQianPenJian_5() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_preFrameAni_XiangQianPenJian_5)); }
	inline GameObject_t1113636619 * get_m_preFrameAni_XiangQianPenJian_5() const { return ___m_preFrameAni_XiangQianPenJian_5; }
	inline GameObject_t1113636619 ** get_address_of_m_preFrameAni_XiangQianPenJian_5() { return &___m_preFrameAni_XiangQianPenJian_5; }
	inline void set_m_preFrameAni_XiangQianPenJian_5(GameObject_t1113636619 * value)
	{
		___m_preFrameAni_XiangQianPenJian_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_preFrameAni_XiangQianPenJian_5), value);
	}

	inline static int32_t get_offset_of_m_aryBloodStainPrefab_6() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_aryBloodStainPrefab_6)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryBloodStainPrefab_6() const { return ___m_aryBloodStainPrefab_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryBloodStainPrefab_6() { return &___m_aryBloodStainPrefab_6; }
	inline void set_m_aryBloodStainPrefab_6(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryBloodStainPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBloodStainPrefab_6), value);
	}

	inline static int32_t get_offset_of_m_aryBloodSize2BallSize_7() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_aryBloodSize2BallSize_7)); }
	inline SingleU5BU5D_t1444911251* get_m_aryBloodSize2BallSize_7() const { return ___m_aryBloodSize2BallSize_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryBloodSize2BallSize_7() { return &___m_aryBloodSize2BallSize_7; }
	inline void set_m_aryBloodSize2BallSize_7(SingleU5BU5D_t1444911251* value)
	{
		___m_aryBloodSize2BallSize_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBloodSize2BallSize_7), value);
	}

	inline static int32_t get_offset_of_m_aryLastTime_8() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_aryLastTime_8)); }
	inline SingleU5BU5D_t1444911251* get_m_aryLastTime_8() const { return ___m_aryLastTime_8; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryLastTime_8() { return &___m_aryLastTime_8; }
	inline void set_m_aryLastTime_8(SingleU5BU5D_t1444911251* value)
	{
		___m_aryLastTime_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLastTime_8), value);
	}

	inline static int32_t get_offset_of_m_aryFadeTime_9() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_aryFadeTime_9)); }
	inline SingleU5BU5D_t1444911251* get_m_aryFadeTime_9() const { return ___m_aryFadeTime_9; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryFadeTime_9() { return &___m_aryFadeTime_9; }
	inline void set_m_aryFadeTime_9(SingleU5BU5D_t1444911251* value)
	{
		___m_aryFadeTime_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFadeTime_9), value);
	}

	inline static int32_t get_offset_of_m_fDynamicScaleTime_10() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_fDynamicScaleTime_10)); }
	inline float get_m_fDynamicScaleTime_10() const { return ___m_fDynamicScaleTime_10; }
	inline float* get_address_of_m_fDynamicScaleTime_10() { return &___m_fDynamicScaleTime_10; }
	inline void set_m_fDynamicScaleTime_10(float value)
	{
		___m_fDynamicScaleTime_10 = value;
	}

	inline static int32_t get_offset_of_m_preBloodTile_11() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_preBloodTile_11)); }
	inline GameObject_t1113636619 * get_m_preBloodTile_11() const { return ___m_preBloodTile_11; }
	inline GameObject_t1113636619 ** get_address_of_m_preBloodTile_11() { return &___m_preBloodTile_11; }
	inline void set_m_preBloodTile_11(GameObject_t1113636619 * value)
	{
		___m_preBloodTile_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBloodTile_11), value);
	}

	inline static int32_t get_offset_of_m_fBloodTileInterval_12() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_fBloodTileInterval_12)); }
	inline float get_m_fBloodTileInterval_12() const { return ___m_fBloodTileInterval_12; }
	inline float* get_address_of_m_fBloodTileInterval_12() { return &___m_fBloodTileInterval_12; }
	inline void set_m_fBloodTileInterval_12(float value)
	{
		___m_fBloodTileInterval_12 = value;
	}

	inline static int32_t get_offset_of_m_preStain_13() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_preStain_13)); }
	inline GameObject_t1113636619 * get_m_preStain_13() const { return ___m_preStain_13; }
	inline GameObject_t1113636619 ** get_address_of_m_preStain_13() { return &___m_preStain_13; }
	inline void set_m_preStain_13(GameObject_t1113636619 * value)
	{
		___m_preStain_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_preStain_13), value);
	}

	inline static int32_t get_offset_of_m_goArea0_14() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_goArea0_14)); }
	inline GameObject_t1113636619 * get_m_goArea0_14() const { return ___m_goArea0_14; }
	inline GameObject_t1113636619 ** get_address_of_m_goArea0_14() { return &___m_goArea0_14; }
	inline void set_m_goArea0_14(GameObject_t1113636619 * value)
	{
		___m_goArea0_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_goArea0_14), value);
	}

	inline static int32_t get_offset_of_m_fXiangQianPenJianScale_15() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_fXiangQianPenJianScale_15)); }
	inline float get_m_fXiangQianPenJianScale_15() const { return ___m_fXiangQianPenJianScale_15; }
	inline float* get_address_of_m_fXiangQianPenJianScale_15() { return &___m_fXiangQianPenJianScale_15; }
	inline void set_m_fXiangQianPenJianScale_15(float value)
	{
		___m_fXiangQianPenJianScale_15 = value;
	}

	inline static int32_t get_offset_of_m_fXiangQianPenJianPosDirX_16() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_fXiangQianPenJianPosDirX_16)); }
	inline float get_m_fXiangQianPenJianPosDirX_16() const { return ___m_fXiangQianPenJianPosDirX_16; }
	inline float* get_address_of_m_fXiangQianPenJianPosDirX_16() { return &___m_fXiangQianPenJianPosDirX_16; }
	inline void set_m_fXiangQianPenJianPosDirX_16(float value)
	{
		___m_fXiangQianPenJianPosDirX_16 = value;
	}

	inline static int32_t get_offset_of_m_fXiangQianPenJianPosDirY_17() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_fXiangQianPenJianPosDirY_17)); }
	inline float get_m_fXiangQianPenJianPosDirY_17() const { return ___m_fXiangQianPenJianPosDirY_17; }
	inline float* get_address_of_m_fXiangQianPenJianPosDirY_17() { return &___m_fXiangQianPenJianPosDirY_17; }
	inline void set_m_fXiangQianPenJianPosDirY_17(float value)
	{
		___m_fXiangQianPenJianPosDirY_17 = value;
	}

	inline static int32_t get_offset_of_m_dicRecycledBloodStain_18() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_dicRecycledBloodStain_18)); }
	inline Dictionary_2_t2042087675 * get_m_dicRecycledBloodStain_18() const { return ___m_dicRecycledBloodStain_18; }
	inline Dictionary_2_t2042087675 ** get_address_of_m_dicRecycledBloodStain_18() { return &___m_dicRecycledBloodStain_18; }
	inline void set_m_dicRecycledBloodStain_18(Dictionary_2_t2042087675 * value)
	{
		___m_dicRecycledBloodStain_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledBloodStain_18), value);
	}

	inline static int32_t get_offset_of_m_dicBloodStains_19() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_dicBloodStains_19)); }
	inline Dictionary_2_t722626127 * get_m_dicBloodStains_19() const { return ___m_dicBloodStains_19; }
	inline Dictionary_2_t722626127 ** get_address_of_m_dicBloodStains_19() { return &___m_dicBloodStains_19; }
	inline void set_m_dicBloodStains_19(Dictionary_2_t722626127 * value)
	{
		___m_dicBloodStains_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBloodStains_19), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledBloodStains_20() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_lstRecycledBloodStains_20)); }
	inline List_1_t920433207 * get_m_lstRecycledBloodStains_20() const { return ___m_lstRecycledBloodStains_20; }
	inline List_1_t920433207 ** get_address_of_m_lstRecycledBloodStains_20() { return &___m_lstRecycledBloodStains_20; }
	inline void set_m_lstRecycledBloodStains_20(List_1_t920433207 * value)
	{
		___m_lstRecycledBloodStains_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledBloodStains_20), value);
	}

	inline static int32_t get_offset_of_m_lstFading_21() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_lstFading_21)); }
	inline List_1_t920433207 * get_m_lstFading_21() const { return ___m_lstFading_21; }
	inline List_1_t920433207 ** get_address_of_m_lstFading_21() { return &___m_lstFading_21; }
	inline void set_m_lstFading_21(List_1_t920433207 * value)
	{
		___m_lstFading_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstFading_21), value);
	}

	inline static int32_t get_offset_of_m_fFadeTime_22() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270, ___m_fFadeTime_22)); }
	inline float get_m_fFadeTime_22() const { return ___m_fFadeTime_22; }
	inline float* get_address_of_m_fFadeTime_22() { return &___m_fFadeTime_22; }
	inline void set_m_fFadeTime_22(float value)
	{
		___m_fFadeTime_22 = value;
	}
};

struct CBloodStainManager_t2257855270_StaticFields
{
public:
	// CBloodStainManager CBloodStainManager::s_Instance
	CBloodStainManager_t2257855270 * ___s_Instance_2;
	// UnityEngine.Vector3 CBloodStainManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector2 CBloodStainManager::vecTempDir
	Vector2_t2156229523  ___vecTempDir_4;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270_StaticFields, ___s_Instance_2)); }
	inline CBloodStainManager_t2257855270 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CBloodStainManager_t2257855270 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CBloodStainManager_t2257855270 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_4() { return static_cast<int32_t>(offsetof(CBloodStainManager_t2257855270_StaticFields, ___vecTempDir_4)); }
	inline Vector2_t2156229523  get_vecTempDir_4() const { return ___vecTempDir_4; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_4() { return &___vecTempDir_4; }
	inline void set_vecTempDir_4(Vector2_t2156229523  value)
	{
		___vecTempDir_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBLOODSTAINMANAGER_T2257855270_H
#ifndef CBLOODTILE_T2617155586_H
#define CBLOODTILE_T2617155586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBloodTile
struct  CBloodTile_t2617155586  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer CBloodTile::m_sprMain
	SpriteRenderer_t3235626157 * ___m_sprMain_2;
	// UnityEngine.GameObject CBloodTile::m_goMainContainer
	GameObject_t1113636619 * ___m_goMainContainer_3;

public:
	inline static int32_t get_offset_of_m_sprMain_2() { return static_cast<int32_t>(offsetof(CBloodTile_t2617155586, ___m_sprMain_2)); }
	inline SpriteRenderer_t3235626157 * get_m_sprMain_2() const { return ___m_sprMain_2; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_sprMain_2() { return &___m_sprMain_2; }
	inline void set_m_sprMain_2(SpriteRenderer_t3235626157 * value)
	{
		___m_sprMain_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMain_2), value);
	}

	inline static int32_t get_offset_of_m_goMainContainer_3() { return static_cast<int32_t>(offsetof(CBloodTile_t2617155586, ___m_goMainContainer_3)); }
	inline GameObject_t1113636619 * get_m_goMainContainer_3() const { return ___m_goMainContainer_3; }
	inline GameObject_t1113636619 ** get_address_of_m_goMainContainer_3() { return &___m_goMainContainer_3; }
	inline void set_m_goMainContainer_3(GameObject_t1113636619 * value)
	{
		___m_goMainContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_goMainContainer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBLOODTILE_T2617155586_H
#ifndef CBUFF_T1540791136_H
#define CBUFF_T1540791136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuff
struct  CBuff_t1540791136  : public MonoBehaviour_t3962482529
{
public:
	// CBuffEditor/sBuffConfig CBuff::m_Config
	sBuffConfig_t769791949  ___m_Config_2;
	// System.Single CBuff::m_fTime
	float ___m_fTime_3;
	// System.Int64 CBuff::m_lGUID
	int64_t ___m_lGUID_4;

public:
	inline static int32_t get_offset_of_m_Config_2() { return static_cast<int32_t>(offsetof(CBuff_t1540791136, ___m_Config_2)); }
	inline sBuffConfig_t769791949  get_m_Config_2() const { return ___m_Config_2; }
	inline sBuffConfig_t769791949 * get_address_of_m_Config_2() { return &___m_Config_2; }
	inline void set_m_Config_2(sBuffConfig_t769791949  value)
	{
		___m_Config_2 = value;
	}

	inline static int32_t get_offset_of_m_fTime_3() { return static_cast<int32_t>(offsetof(CBuff_t1540791136, ___m_fTime_3)); }
	inline float get_m_fTime_3() const { return ___m_fTime_3; }
	inline float* get_address_of_m_fTime_3() { return &___m_fTime_3; }
	inline void set_m_fTime_3(float value)
	{
		___m_fTime_3 = value;
	}

	inline static int32_t get_offset_of_m_lGUID_4() { return static_cast<int32_t>(offsetof(CBuff_t1540791136, ___m_lGUID_4)); }
	inline int64_t get_m_lGUID_4() const { return ___m_lGUID_4; }
	inline int64_t* get_address_of_m_lGUID_4() { return &___m_lGUID_4; }
	inline void set_m_lGUID_4(int64_t value)
	{
		___m_lGUID_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBUFF_T1540791136_H
#ifndef CBUFFEDITOR_T818684383_H
#define CBUFFEDITOR_T818684383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuffEditor
struct  CBuffEditor_t818684383  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField[] CBuffEditor::_aryDesc
	InputFieldU5BU5D_t1172778254* ____aryDesc_3;
	// UnityEngine.UI.InputField[] CBuffEditor::_aryValues
	InputFieldU5BU5D_t1172778254* ____aryValues_4;
	// UnityEngine.UI.InputField CBuffEditor::_inputTime
	InputField_t3762917431 * ____inputTime_5;
	// UnityEngine.UI.Dropdown CBuffEditor::_dropdownBuffId
	Dropdown_t2274391225 * ____dropdownBuffId_6;
	// UnityEngine.UI.Dropdown CBuffEditor::_dropdownBuffFunc
	Dropdown_t2274391225 * ____dropdownBuffFunc_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,CBuffEditor/sBuffConfig> CBuffEditor::m_dicBuffConfig
	Dictionary_2_t3953472576 * ___m_dicBuffConfig_9;
	// CBuffEditor/sBuffConfig CBuffEditor::m_CurEditConfig
	sBuffConfig_t769791949  ___m_CurEditConfig_10;
	// CBuffEditor/sBuffConfig CBuffEditor::tempConfig
	sBuffConfig_t769791949  ___tempConfig_11;

public:
	inline static int32_t get_offset_of__aryDesc_3() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ____aryDesc_3)); }
	inline InputFieldU5BU5D_t1172778254* get__aryDesc_3() const { return ____aryDesc_3; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryDesc_3() { return &____aryDesc_3; }
	inline void set__aryDesc_3(InputFieldU5BU5D_t1172778254* value)
	{
		____aryDesc_3 = value;
		Il2CppCodeGenWriteBarrier((&____aryDesc_3), value);
	}

	inline static int32_t get_offset_of__aryValues_4() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ____aryValues_4)); }
	inline InputFieldU5BU5D_t1172778254* get__aryValues_4() const { return ____aryValues_4; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryValues_4() { return &____aryValues_4; }
	inline void set__aryValues_4(InputFieldU5BU5D_t1172778254* value)
	{
		____aryValues_4 = value;
		Il2CppCodeGenWriteBarrier((&____aryValues_4), value);
	}

	inline static int32_t get_offset_of__inputTime_5() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ____inputTime_5)); }
	inline InputField_t3762917431 * get__inputTime_5() const { return ____inputTime_5; }
	inline InputField_t3762917431 ** get_address_of__inputTime_5() { return &____inputTime_5; }
	inline void set__inputTime_5(InputField_t3762917431 * value)
	{
		____inputTime_5 = value;
		Il2CppCodeGenWriteBarrier((&____inputTime_5), value);
	}

	inline static int32_t get_offset_of__dropdownBuffId_6() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ____dropdownBuffId_6)); }
	inline Dropdown_t2274391225 * get__dropdownBuffId_6() const { return ____dropdownBuffId_6; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownBuffId_6() { return &____dropdownBuffId_6; }
	inline void set__dropdownBuffId_6(Dropdown_t2274391225 * value)
	{
		____dropdownBuffId_6 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownBuffId_6), value);
	}

	inline static int32_t get_offset_of__dropdownBuffFunc_7() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ____dropdownBuffFunc_7)); }
	inline Dropdown_t2274391225 * get__dropdownBuffFunc_7() const { return ____dropdownBuffFunc_7; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownBuffFunc_7() { return &____dropdownBuffFunc_7; }
	inline void set__dropdownBuffFunc_7(Dropdown_t2274391225 * value)
	{
		____dropdownBuffFunc_7 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownBuffFunc_7), value);
	}

	inline static int32_t get_offset_of_m_dicBuffConfig_9() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ___m_dicBuffConfig_9)); }
	inline Dictionary_2_t3953472576 * get_m_dicBuffConfig_9() const { return ___m_dicBuffConfig_9; }
	inline Dictionary_2_t3953472576 ** get_address_of_m_dicBuffConfig_9() { return &___m_dicBuffConfig_9; }
	inline void set_m_dicBuffConfig_9(Dictionary_2_t3953472576 * value)
	{
		___m_dicBuffConfig_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBuffConfig_9), value);
	}

	inline static int32_t get_offset_of_m_CurEditConfig_10() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ___m_CurEditConfig_10)); }
	inline sBuffConfig_t769791949  get_m_CurEditConfig_10() const { return ___m_CurEditConfig_10; }
	inline sBuffConfig_t769791949 * get_address_of_m_CurEditConfig_10() { return &___m_CurEditConfig_10; }
	inline void set_m_CurEditConfig_10(sBuffConfig_t769791949  value)
	{
		___m_CurEditConfig_10 = value;
	}

	inline static int32_t get_offset_of_tempConfig_11() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383, ___tempConfig_11)); }
	inline sBuffConfig_t769791949  get_tempConfig_11() const { return ___tempConfig_11; }
	inline sBuffConfig_t769791949 * get_address_of_tempConfig_11() { return &___tempConfig_11; }
	inline void set_tempConfig_11(sBuffConfig_t769791949  value)
	{
		___tempConfig_11 = value;
	}
};

struct CBuffEditor_t818684383_StaticFields
{
public:
	// CBuffEditor CBuffEditor::s_Instance
	CBuffEditor_t818684383 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CBuffEditor_t818684383_StaticFields, ___s_Instance_2)); }
	inline CBuffEditor_t818684383 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CBuffEditor_t818684383 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CBuffEditor_t818684383 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBUFFEDITOR_T818684383_H
#ifndef CADAPTIVEMANAGER_T2673862881_H
#define CADAPTIVEMANAGER_T2673862881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAdaptiveManager
struct  CAdaptiveManager_t2673862881  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CAdaptiveManager::m_bCheckDEvice
	bool ___m_bCheckDEvice_3;
	// CPaoMaDeng CAdaptiveManager::_paomadeng
	CPaoMaDeng_t1722089677 * ____paomadeng_4;
	// CPaoMaDeng[] CAdaptiveManager::m_aryPaoMaDeng
	CPaoMaDengU5BU5D_t1843095904* ___m_aryPaoMaDeng_5;
	// CAdaptiveManager/eDeviceTYpe CAdaptiveManager::m_eDeviceType
	int32_t ___m_eDeviceType_6;
	// UnityEngine.Vector2[] CAdaptiveManager::m_Resolution
	Vector2U5BU5D_t1457185986* ___m_Resolution_7;

public:
	inline static int32_t get_offset_of_m_bCheckDEvice_3() { return static_cast<int32_t>(offsetof(CAdaptiveManager_t2673862881, ___m_bCheckDEvice_3)); }
	inline bool get_m_bCheckDEvice_3() const { return ___m_bCheckDEvice_3; }
	inline bool* get_address_of_m_bCheckDEvice_3() { return &___m_bCheckDEvice_3; }
	inline void set_m_bCheckDEvice_3(bool value)
	{
		___m_bCheckDEvice_3 = value;
	}

	inline static int32_t get_offset_of__paomadeng_4() { return static_cast<int32_t>(offsetof(CAdaptiveManager_t2673862881, ____paomadeng_4)); }
	inline CPaoMaDeng_t1722089677 * get__paomadeng_4() const { return ____paomadeng_4; }
	inline CPaoMaDeng_t1722089677 ** get_address_of__paomadeng_4() { return &____paomadeng_4; }
	inline void set__paomadeng_4(CPaoMaDeng_t1722089677 * value)
	{
		____paomadeng_4 = value;
		Il2CppCodeGenWriteBarrier((&____paomadeng_4), value);
	}

	inline static int32_t get_offset_of_m_aryPaoMaDeng_5() { return static_cast<int32_t>(offsetof(CAdaptiveManager_t2673862881, ___m_aryPaoMaDeng_5)); }
	inline CPaoMaDengU5BU5D_t1843095904* get_m_aryPaoMaDeng_5() const { return ___m_aryPaoMaDeng_5; }
	inline CPaoMaDengU5BU5D_t1843095904** get_address_of_m_aryPaoMaDeng_5() { return &___m_aryPaoMaDeng_5; }
	inline void set_m_aryPaoMaDeng_5(CPaoMaDengU5BU5D_t1843095904* value)
	{
		___m_aryPaoMaDeng_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPaoMaDeng_5), value);
	}

	inline static int32_t get_offset_of_m_eDeviceType_6() { return static_cast<int32_t>(offsetof(CAdaptiveManager_t2673862881, ___m_eDeviceType_6)); }
	inline int32_t get_m_eDeviceType_6() const { return ___m_eDeviceType_6; }
	inline int32_t* get_address_of_m_eDeviceType_6() { return &___m_eDeviceType_6; }
	inline void set_m_eDeviceType_6(int32_t value)
	{
		___m_eDeviceType_6 = value;
	}

	inline static int32_t get_offset_of_m_Resolution_7() { return static_cast<int32_t>(offsetof(CAdaptiveManager_t2673862881, ___m_Resolution_7)); }
	inline Vector2U5BU5D_t1457185986* get_m_Resolution_7() const { return ___m_Resolution_7; }
	inline Vector2U5BU5D_t1457185986** get_address_of_m_Resolution_7() { return &___m_Resolution_7; }
	inline void set_m_Resolution_7(Vector2U5BU5D_t1457185986* value)
	{
		___m_Resolution_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resolution_7), value);
	}
};

struct CAdaptiveManager_t2673862881_StaticFields
{
public:
	// CAdaptiveManager CAdaptiveManager::s_Instance
	CAdaptiveManager_t2673862881 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CAdaptiveManager_t2673862881_StaticFields, ___s_Instance_2)); }
	inline CAdaptiveManager_t2673862881 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CAdaptiveManager_t2673862881 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CAdaptiveManager_t2673862881 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CADAPTIVEMANAGER_T2673862881_H
#ifndef CCHEAT_T3466918192_H
#define CCHEAT_T3466918192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCheat
struct  CCheat_t3466918192  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CCheat::m_bActive
	bool ___m_bActive_5;
	// UnityEngine.GameObject CCheat::_goPanel
	GameObject_t1113636619 * ____goPanel_6;
	// UnityEngine.UI.Toggle CCheat::_toggleFixedRebornPos
	Toggle_t2735377061 * ____toggleFixedRebornPos_7;
	// UnityEngine.GameObject CCheat::_goMoveToCenter
	GameObject_t1113636619 * ____goMoveToCenter_8;
	// UnityEngine.UI.InputField CCheat::_inputJiShaTestScript
	InputField_t3762917431 * ____inputJiShaTestScript_9;
	// UnityEngine.UI.Toggle CCheat::_toggleXingNengCeShi
	Toggle_t2735377061 * ____toggleXingNengCeShi_10;
	// System.Single CCheat::m_fShitCount
	float ___m_fShitCount_11;
	// System.Collections.Generic.List`1<System.Int32> CCheat::lstAssist
	List_1_t128053199 * ___lstAssist_12;
	// UnityEngine.UI.InputField CCheat::_inputBallsNum
	InputField_t3762917431 * ____inputBallsNum_13;
	// UnityEngine.UI.InputField CCheat::_inputBallMoveTimeInterval
	InputField_t3762917431 * ____inputBallMoveTimeInterval_14;
	// System.Single CCheat::m_fBallMoveTimeInterval
	float ___m_fBallMoveTimeInterval_15;
	// System.Int32 CCheat::m_nShitCount
	int32_t ___m_nShitCount_16;
	// System.Int32 CCheat::m_nShitSkinCount
	int32_t ___m_nShitSkinCount_17;
	// System.Single CCheat::m_fShitTimeCount
	float ___m_fShitTimeCount_18;
	// System.Int32 CCheat::m_nShitSkinIndex
	int32_t ___m_nShitSkinIndex_19;
	// System.Collections.Generic.List`1<Ball> CCheat::m_lstTestBalls
	List_1_t3678741308 * ___m_lstTestBalls_20;
	// System.Boolean CCheat::m_bTestBallsMoving
	bool ___m_bTestBallsMoving_21;
	// System.Single CCheat::m_fAutoMoveTimeElapse
	float ___m_fAutoMoveTimeElapse_22;
	// UnityEngine.UI.Toggle CCheat::_toggleRigidMove
	Toggle_t2735377061 * ____toggleRigidMove_23;
	// UnityEngine.UI.InputField CCheat::_inputTRiangleNum
	InputField_t3762917431 * ____inputTRiangleNum_24;
	// System.Int32 CCheat::m_nAutoExplode
	int32_t ___m_nAutoExplode_25;
	// System.Single CCheat::m_fAutoEXplodeTimeCount
	float ___m_fAutoEXplodeTimeCount_26;
	// System.Int32 CCheat::m_nFrameCount
	int32_t ___m_nFrameCount_27;
	// System.Single CCheat::m_fFPSCalcCount
	float ___m_fFPSCalcCount_28;
	// UnityEngine.UI.InputField CCheat::_inputMannualControlViewSize
	InputField_t3762917431 * ____inputMannualControlViewSize_30;
	// UnityEngine.UI.Toggle CCheat::_toggleMannualControlViewSize
	Toggle_t2735377061 * ____toggleMannualControlViewSize_31;
	// System.Single CCheat::m_fMannualControlViewSize
	float ___m_fMannualControlViewSize_32;
	// UnityEngine.UI.InputField CCheat::_inputClickToStopMove
	InputField_t3762917431 * ____inputClickToStopMove_33;
	// System.Single CCheat::_fClickToStopMoveInterval
	float ____fClickToStopMoveInterval_34;
	// UnityEngine.UI.InputField CCheat::_inputMaxCamSize
	InputField_t3762917431 * ____inputMaxCamSize_35;
	// UnityEngine.UI.Toggle CCheat::_tggleMaxCamSizeLimit
	Toggle_t2735377061 * ____tggleMaxCamSizeLimit_36;
	// UnityEngine.GameObject CCheat::_panelHomePageCheatPanel
	GameObject_t1113636619 * ____panelHomePageCheatPanel_37;
	// System.Boolean CCheat::m_bHomePageCheatPanelShowing
	bool ___m_bHomePageCheatPanelShowing_38;
	// UnityEngine.UI.InputField CCheat::_inputStreetNews
	InputField_t3762917431 * ____inputStreetNews_39;
	// UnityEngine.UI.InputField CCheat::_inputHotEvents
	InputField_t3762917431 * ____inputHotEvents_40;
	// UnityEngine.UI.Text CCheat::_txtDebugInfo
	Text_t1901882714 * ____txtDebugInfo_41;
	// UnityEngine.UI.InputField CCheat::_inputBallIndex
	InputField_t3762917431 * ____inputBallIndex_42;
	// UnityEngine.UI.InputField CCheat::_inputFakeAccount
	InputField_t3762917431 * ____inputFakeAccount_43;
	// UnityEngine.UI.InputField CCheat::_inputSize
	InputField_t3762917431 * ____inputSize_44;
	// UnityEngine.UI.InputField CCheat::_inputInterval
	InputField_t3762917431 * ____inputInterval_45;
	// UnityEngine.UI.InputField CCheat::_inputStarine
	InputField_t3762917431 * ____inputStarine_46;

public:
	inline static int32_t get_offset_of_m_bActive_5() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_bActive_5)); }
	inline bool get_m_bActive_5() const { return ___m_bActive_5; }
	inline bool* get_address_of_m_bActive_5() { return &___m_bActive_5; }
	inline void set_m_bActive_5(bool value)
	{
		___m_bActive_5 = value;
	}

	inline static int32_t get_offset_of__goPanel_6() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____goPanel_6)); }
	inline GameObject_t1113636619 * get__goPanel_6() const { return ____goPanel_6; }
	inline GameObject_t1113636619 ** get_address_of__goPanel_6() { return &____goPanel_6; }
	inline void set__goPanel_6(GameObject_t1113636619 * value)
	{
		____goPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&____goPanel_6), value);
	}

	inline static int32_t get_offset_of__toggleFixedRebornPos_7() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____toggleFixedRebornPos_7)); }
	inline Toggle_t2735377061 * get__toggleFixedRebornPos_7() const { return ____toggleFixedRebornPos_7; }
	inline Toggle_t2735377061 ** get_address_of__toggleFixedRebornPos_7() { return &____toggleFixedRebornPos_7; }
	inline void set__toggleFixedRebornPos_7(Toggle_t2735377061 * value)
	{
		____toggleFixedRebornPos_7 = value;
		Il2CppCodeGenWriteBarrier((&____toggleFixedRebornPos_7), value);
	}

	inline static int32_t get_offset_of__goMoveToCenter_8() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____goMoveToCenter_8)); }
	inline GameObject_t1113636619 * get__goMoveToCenter_8() const { return ____goMoveToCenter_8; }
	inline GameObject_t1113636619 ** get_address_of__goMoveToCenter_8() { return &____goMoveToCenter_8; }
	inline void set__goMoveToCenter_8(GameObject_t1113636619 * value)
	{
		____goMoveToCenter_8 = value;
		Il2CppCodeGenWriteBarrier((&____goMoveToCenter_8), value);
	}

	inline static int32_t get_offset_of__inputJiShaTestScript_9() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputJiShaTestScript_9)); }
	inline InputField_t3762917431 * get__inputJiShaTestScript_9() const { return ____inputJiShaTestScript_9; }
	inline InputField_t3762917431 ** get_address_of__inputJiShaTestScript_9() { return &____inputJiShaTestScript_9; }
	inline void set__inputJiShaTestScript_9(InputField_t3762917431 * value)
	{
		____inputJiShaTestScript_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputJiShaTestScript_9), value);
	}

	inline static int32_t get_offset_of__toggleXingNengCeShi_10() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____toggleXingNengCeShi_10)); }
	inline Toggle_t2735377061 * get__toggleXingNengCeShi_10() const { return ____toggleXingNengCeShi_10; }
	inline Toggle_t2735377061 ** get_address_of__toggleXingNengCeShi_10() { return &____toggleXingNengCeShi_10; }
	inline void set__toggleXingNengCeShi_10(Toggle_t2735377061 * value)
	{
		____toggleXingNengCeShi_10 = value;
		Il2CppCodeGenWriteBarrier((&____toggleXingNengCeShi_10), value);
	}

	inline static int32_t get_offset_of_m_fShitCount_11() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fShitCount_11)); }
	inline float get_m_fShitCount_11() const { return ___m_fShitCount_11; }
	inline float* get_address_of_m_fShitCount_11() { return &___m_fShitCount_11; }
	inline void set_m_fShitCount_11(float value)
	{
		___m_fShitCount_11 = value;
	}

	inline static int32_t get_offset_of_lstAssist_12() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___lstAssist_12)); }
	inline List_1_t128053199 * get_lstAssist_12() const { return ___lstAssist_12; }
	inline List_1_t128053199 ** get_address_of_lstAssist_12() { return &___lstAssist_12; }
	inline void set_lstAssist_12(List_1_t128053199 * value)
	{
		___lstAssist_12 = value;
		Il2CppCodeGenWriteBarrier((&___lstAssist_12), value);
	}

	inline static int32_t get_offset_of__inputBallsNum_13() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputBallsNum_13)); }
	inline InputField_t3762917431 * get__inputBallsNum_13() const { return ____inputBallsNum_13; }
	inline InputField_t3762917431 ** get_address_of__inputBallsNum_13() { return &____inputBallsNum_13; }
	inline void set__inputBallsNum_13(InputField_t3762917431 * value)
	{
		____inputBallsNum_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputBallsNum_13), value);
	}

	inline static int32_t get_offset_of__inputBallMoveTimeInterval_14() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputBallMoveTimeInterval_14)); }
	inline InputField_t3762917431 * get__inputBallMoveTimeInterval_14() const { return ____inputBallMoveTimeInterval_14; }
	inline InputField_t3762917431 ** get_address_of__inputBallMoveTimeInterval_14() { return &____inputBallMoveTimeInterval_14; }
	inline void set__inputBallMoveTimeInterval_14(InputField_t3762917431 * value)
	{
		____inputBallMoveTimeInterval_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputBallMoveTimeInterval_14), value);
	}

	inline static int32_t get_offset_of_m_fBallMoveTimeInterval_15() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fBallMoveTimeInterval_15)); }
	inline float get_m_fBallMoveTimeInterval_15() const { return ___m_fBallMoveTimeInterval_15; }
	inline float* get_address_of_m_fBallMoveTimeInterval_15() { return &___m_fBallMoveTimeInterval_15; }
	inline void set_m_fBallMoveTimeInterval_15(float value)
	{
		___m_fBallMoveTimeInterval_15 = value;
	}

	inline static int32_t get_offset_of_m_nShitCount_16() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_nShitCount_16)); }
	inline int32_t get_m_nShitCount_16() const { return ___m_nShitCount_16; }
	inline int32_t* get_address_of_m_nShitCount_16() { return &___m_nShitCount_16; }
	inline void set_m_nShitCount_16(int32_t value)
	{
		___m_nShitCount_16 = value;
	}

	inline static int32_t get_offset_of_m_nShitSkinCount_17() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_nShitSkinCount_17)); }
	inline int32_t get_m_nShitSkinCount_17() const { return ___m_nShitSkinCount_17; }
	inline int32_t* get_address_of_m_nShitSkinCount_17() { return &___m_nShitSkinCount_17; }
	inline void set_m_nShitSkinCount_17(int32_t value)
	{
		___m_nShitSkinCount_17 = value;
	}

	inline static int32_t get_offset_of_m_fShitTimeCount_18() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fShitTimeCount_18)); }
	inline float get_m_fShitTimeCount_18() const { return ___m_fShitTimeCount_18; }
	inline float* get_address_of_m_fShitTimeCount_18() { return &___m_fShitTimeCount_18; }
	inline void set_m_fShitTimeCount_18(float value)
	{
		___m_fShitTimeCount_18 = value;
	}

	inline static int32_t get_offset_of_m_nShitSkinIndex_19() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_nShitSkinIndex_19)); }
	inline int32_t get_m_nShitSkinIndex_19() const { return ___m_nShitSkinIndex_19; }
	inline int32_t* get_address_of_m_nShitSkinIndex_19() { return &___m_nShitSkinIndex_19; }
	inline void set_m_nShitSkinIndex_19(int32_t value)
	{
		___m_nShitSkinIndex_19 = value;
	}

	inline static int32_t get_offset_of_m_lstTestBalls_20() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_lstTestBalls_20)); }
	inline List_1_t3678741308 * get_m_lstTestBalls_20() const { return ___m_lstTestBalls_20; }
	inline List_1_t3678741308 ** get_address_of_m_lstTestBalls_20() { return &___m_lstTestBalls_20; }
	inline void set_m_lstTestBalls_20(List_1_t3678741308 * value)
	{
		___m_lstTestBalls_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTestBalls_20), value);
	}

	inline static int32_t get_offset_of_m_bTestBallsMoving_21() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_bTestBallsMoving_21)); }
	inline bool get_m_bTestBallsMoving_21() const { return ___m_bTestBallsMoving_21; }
	inline bool* get_address_of_m_bTestBallsMoving_21() { return &___m_bTestBallsMoving_21; }
	inline void set_m_bTestBallsMoving_21(bool value)
	{
		___m_bTestBallsMoving_21 = value;
	}

	inline static int32_t get_offset_of_m_fAutoMoveTimeElapse_22() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fAutoMoveTimeElapse_22)); }
	inline float get_m_fAutoMoveTimeElapse_22() const { return ___m_fAutoMoveTimeElapse_22; }
	inline float* get_address_of_m_fAutoMoveTimeElapse_22() { return &___m_fAutoMoveTimeElapse_22; }
	inline void set_m_fAutoMoveTimeElapse_22(float value)
	{
		___m_fAutoMoveTimeElapse_22 = value;
	}

	inline static int32_t get_offset_of__toggleRigidMove_23() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____toggleRigidMove_23)); }
	inline Toggle_t2735377061 * get__toggleRigidMove_23() const { return ____toggleRigidMove_23; }
	inline Toggle_t2735377061 ** get_address_of__toggleRigidMove_23() { return &____toggleRigidMove_23; }
	inline void set__toggleRigidMove_23(Toggle_t2735377061 * value)
	{
		____toggleRigidMove_23 = value;
		Il2CppCodeGenWriteBarrier((&____toggleRigidMove_23), value);
	}

	inline static int32_t get_offset_of__inputTRiangleNum_24() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputTRiangleNum_24)); }
	inline InputField_t3762917431 * get__inputTRiangleNum_24() const { return ____inputTRiangleNum_24; }
	inline InputField_t3762917431 ** get_address_of__inputTRiangleNum_24() { return &____inputTRiangleNum_24; }
	inline void set__inputTRiangleNum_24(InputField_t3762917431 * value)
	{
		____inputTRiangleNum_24 = value;
		Il2CppCodeGenWriteBarrier((&____inputTRiangleNum_24), value);
	}

	inline static int32_t get_offset_of_m_nAutoExplode_25() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_nAutoExplode_25)); }
	inline int32_t get_m_nAutoExplode_25() const { return ___m_nAutoExplode_25; }
	inline int32_t* get_address_of_m_nAutoExplode_25() { return &___m_nAutoExplode_25; }
	inline void set_m_nAutoExplode_25(int32_t value)
	{
		___m_nAutoExplode_25 = value;
	}

	inline static int32_t get_offset_of_m_fAutoEXplodeTimeCount_26() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fAutoEXplodeTimeCount_26)); }
	inline float get_m_fAutoEXplodeTimeCount_26() const { return ___m_fAutoEXplodeTimeCount_26; }
	inline float* get_address_of_m_fAutoEXplodeTimeCount_26() { return &___m_fAutoEXplodeTimeCount_26; }
	inline void set_m_fAutoEXplodeTimeCount_26(float value)
	{
		___m_fAutoEXplodeTimeCount_26 = value;
	}

	inline static int32_t get_offset_of_m_nFrameCount_27() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_nFrameCount_27)); }
	inline int32_t get_m_nFrameCount_27() const { return ___m_nFrameCount_27; }
	inline int32_t* get_address_of_m_nFrameCount_27() { return &___m_nFrameCount_27; }
	inline void set_m_nFrameCount_27(int32_t value)
	{
		___m_nFrameCount_27 = value;
	}

	inline static int32_t get_offset_of_m_fFPSCalcCount_28() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fFPSCalcCount_28)); }
	inline float get_m_fFPSCalcCount_28() const { return ___m_fFPSCalcCount_28; }
	inline float* get_address_of_m_fFPSCalcCount_28() { return &___m_fFPSCalcCount_28; }
	inline void set_m_fFPSCalcCount_28(float value)
	{
		___m_fFPSCalcCount_28 = value;
	}

	inline static int32_t get_offset_of__inputMannualControlViewSize_30() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputMannualControlViewSize_30)); }
	inline InputField_t3762917431 * get__inputMannualControlViewSize_30() const { return ____inputMannualControlViewSize_30; }
	inline InputField_t3762917431 ** get_address_of__inputMannualControlViewSize_30() { return &____inputMannualControlViewSize_30; }
	inline void set__inputMannualControlViewSize_30(InputField_t3762917431 * value)
	{
		____inputMannualControlViewSize_30 = value;
		Il2CppCodeGenWriteBarrier((&____inputMannualControlViewSize_30), value);
	}

	inline static int32_t get_offset_of__toggleMannualControlViewSize_31() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____toggleMannualControlViewSize_31)); }
	inline Toggle_t2735377061 * get__toggleMannualControlViewSize_31() const { return ____toggleMannualControlViewSize_31; }
	inline Toggle_t2735377061 ** get_address_of__toggleMannualControlViewSize_31() { return &____toggleMannualControlViewSize_31; }
	inline void set__toggleMannualControlViewSize_31(Toggle_t2735377061 * value)
	{
		____toggleMannualControlViewSize_31 = value;
		Il2CppCodeGenWriteBarrier((&____toggleMannualControlViewSize_31), value);
	}

	inline static int32_t get_offset_of_m_fMannualControlViewSize_32() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_fMannualControlViewSize_32)); }
	inline float get_m_fMannualControlViewSize_32() const { return ___m_fMannualControlViewSize_32; }
	inline float* get_address_of_m_fMannualControlViewSize_32() { return &___m_fMannualControlViewSize_32; }
	inline void set_m_fMannualControlViewSize_32(float value)
	{
		___m_fMannualControlViewSize_32 = value;
	}

	inline static int32_t get_offset_of__inputClickToStopMove_33() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputClickToStopMove_33)); }
	inline InputField_t3762917431 * get__inputClickToStopMove_33() const { return ____inputClickToStopMove_33; }
	inline InputField_t3762917431 ** get_address_of__inputClickToStopMove_33() { return &____inputClickToStopMove_33; }
	inline void set__inputClickToStopMove_33(InputField_t3762917431 * value)
	{
		____inputClickToStopMove_33 = value;
		Il2CppCodeGenWriteBarrier((&____inputClickToStopMove_33), value);
	}

	inline static int32_t get_offset_of__fClickToStopMoveInterval_34() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____fClickToStopMoveInterval_34)); }
	inline float get__fClickToStopMoveInterval_34() const { return ____fClickToStopMoveInterval_34; }
	inline float* get_address_of__fClickToStopMoveInterval_34() { return &____fClickToStopMoveInterval_34; }
	inline void set__fClickToStopMoveInterval_34(float value)
	{
		____fClickToStopMoveInterval_34 = value;
	}

	inline static int32_t get_offset_of__inputMaxCamSize_35() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputMaxCamSize_35)); }
	inline InputField_t3762917431 * get__inputMaxCamSize_35() const { return ____inputMaxCamSize_35; }
	inline InputField_t3762917431 ** get_address_of__inputMaxCamSize_35() { return &____inputMaxCamSize_35; }
	inline void set__inputMaxCamSize_35(InputField_t3762917431 * value)
	{
		____inputMaxCamSize_35 = value;
		Il2CppCodeGenWriteBarrier((&____inputMaxCamSize_35), value);
	}

	inline static int32_t get_offset_of__tggleMaxCamSizeLimit_36() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____tggleMaxCamSizeLimit_36)); }
	inline Toggle_t2735377061 * get__tggleMaxCamSizeLimit_36() const { return ____tggleMaxCamSizeLimit_36; }
	inline Toggle_t2735377061 ** get_address_of__tggleMaxCamSizeLimit_36() { return &____tggleMaxCamSizeLimit_36; }
	inline void set__tggleMaxCamSizeLimit_36(Toggle_t2735377061 * value)
	{
		____tggleMaxCamSizeLimit_36 = value;
		Il2CppCodeGenWriteBarrier((&____tggleMaxCamSizeLimit_36), value);
	}

	inline static int32_t get_offset_of__panelHomePageCheatPanel_37() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____panelHomePageCheatPanel_37)); }
	inline GameObject_t1113636619 * get__panelHomePageCheatPanel_37() const { return ____panelHomePageCheatPanel_37; }
	inline GameObject_t1113636619 ** get_address_of__panelHomePageCheatPanel_37() { return &____panelHomePageCheatPanel_37; }
	inline void set__panelHomePageCheatPanel_37(GameObject_t1113636619 * value)
	{
		____panelHomePageCheatPanel_37 = value;
		Il2CppCodeGenWriteBarrier((&____panelHomePageCheatPanel_37), value);
	}

	inline static int32_t get_offset_of_m_bHomePageCheatPanelShowing_38() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ___m_bHomePageCheatPanelShowing_38)); }
	inline bool get_m_bHomePageCheatPanelShowing_38() const { return ___m_bHomePageCheatPanelShowing_38; }
	inline bool* get_address_of_m_bHomePageCheatPanelShowing_38() { return &___m_bHomePageCheatPanelShowing_38; }
	inline void set_m_bHomePageCheatPanelShowing_38(bool value)
	{
		___m_bHomePageCheatPanelShowing_38 = value;
	}

	inline static int32_t get_offset_of__inputStreetNews_39() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputStreetNews_39)); }
	inline InputField_t3762917431 * get__inputStreetNews_39() const { return ____inputStreetNews_39; }
	inline InputField_t3762917431 ** get_address_of__inputStreetNews_39() { return &____inputStreetNews_39; }
	inline void set__inputStreetNews_39(InputField_t3762917431 * value)
	{
		____inputStreetNews_39 = value;
		Il2CppCodeGenWriteBarrier((&____inputStreetNews_39), value);
	}

	inline static int32_t get_offset_of__inputHotEvents_40() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputHotEvents_40)); }
	inline InputField_t3762917431 * get__inputHotEvents_40() const { return ____inputHotEvents_40; }
	inline InputField_t3762917431 ** get_address_of__inputHotEvents_40() { return &____inputHotEvents_40; }
	inline void set__inputHotEvents_40(InputField_t3762917431 * value)
	{
		____inputHotEvents_40 = value;
		Il2CppCodeGenWriteBarrier((&____inputHotEvents_40), value);
	}

	inline static int32_t get_offset_of__txtDebugInfo_41() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____txtDebugInfo_41)); }
	inline Text_t1901882714 * get__txtDebugInfo_41() const { return ____txtDebugInfo_41; }
	inline Text_t1901882714 ** get_address_of__txtDebugInfo_41() { return &____txtDebugInfo_41; }
	inline void set__txtDebugInfo_41(Text_t1901882714 * value)
	{
		____txtDebugInfo_41 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfo_41), value);
	}

	inline static int32_t get_offset_of__inputBallIndex_42() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputBallIndex_42)); }
	inline InputField_t3762917431 * get__inputBallIndex_42() const { return ____inputBallIndex_42; }
	inline InputField_t3762917431 ** get_address_of__inputBallIndex_42() { return &____inputBallIndex_42; }
	inline void set__inputBallIndex_42(InputField_t3762917431 * value)
	{
		____inputBallIndex_42 = value;
		Il2CppCodeGenWriteBarrier((&____inputBallIndex_42), value);
	}

	inline static int32_t get_offset_of__inputFakeAccount_43() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputFakeAccount_43)); }
	inline InputField_t3762917431 * get__inputFakeAccount_43() const { return ____inputFakeAccount_43; }
	inline InputField_t3762917431 ** get_address_of__inputFakeAccount_43() { return &____inputFakeAccount_43; }
	inline void set__inputFakeAccount_43(InputField_t3762917431 * value)
	{
		____inputFakeAccount_43 = value;
		Il2CppCodeGenWriteBarrier((&____inputFakeAccount_43), value);
	}

	inline static int32_t get_offset_of__inputSize_44() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputSize_44)); }
	inline InputField_t3762917431 * get__inputSize_44() const { return ____inputSize_44; }
	inline InputField_t3762917431 ** get_address_of__inputSize_44() { return &____inputSize_44; }
	inline void set__inputSize_44(InputField_t3762917431 * value)
	{
		____inputSize_44 = value;
		Il2CppCodeGenWriteBarrier((&____inputSize_44), value);
	}

	inline static int32_t get_offset_of__inputInterval_45() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputInterval_45)); }
	inline InputField_t3762917431 * get__inputInterval_45() const { return ____inputInterval_45; }
	inline InputField_t3762917431 ** get_address_of__inputInterval_45() { return &____inputInterval_45; }
	inline void set__inputInterval_45(InputField_t3762917431 * value)
	{
		____inputInterval_45 = value;
		Il2CppCodeGenWriteBarrier((&____inputInterval_45), value);
	}

	inline static int32_t get_offset_of__inputStarine_46() { return static_cast<int32_t>(offsetof(CCheat_t3466918192, ____inputStarine_46)); }
	inline InputField_t3762917431 * get__inputStarine_46() const { return ____inputStarine_46; }
	inline InputField_t3762917431 ** get_address_of__inputStarine_46() { return &____inputStarine_46; }
	inline void set__inputStarine_46(InputField_t3762917431 * value)
	{
		____inputStarine_46 = value;
		Il2CppCodeGenWriteBarrier((&____inputStarine_46), value);
	}
};

struct CCheat_t3466918192_StaticFields
{
public:
	// UnityEngine.Vector3 CCheat::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCheat::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// CCheat CCheat::s_Instance
	CCheat_t3466918192 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCheat_t3466918192_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCheat_t3466918192_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CCheat_t3466918192_StaticFields, ___s_Instance_4)); }
	inline CCheat_t3466918192 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CCheat_t3466918192 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CCheat_t3466918192 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCHEAT_T3466918192_H
#ifndef CBLOODSTAIN_T3743325761_H
#define CBLOODSTAIN_T3743325761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBloodStain
struct  CBloodStain_t3743325761  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CBloodStain::m_goMainContainer
	GameObject_t1113636619 * ___m_goMainContainer_2;
	// CBloodStainManager/eBloodStainType CBloodStain::m_eType
	int32_t ___m_eType_6;
	// CBloodStainManager/eFadeMode CBloodStain::m_eFadeMode
	int32_t ___m_eFadeMode_7;
	// UnityEngine.SpriteRenderer CBloodStain::m_srMain
	SpriteRenderer_t3235626157 * ___m_srMain_8;
	// System.Single CBloodStain::m_fLastTime
	float ___m_fLastTime_9;
	// System.Single CBloodStain::m_fFadeTime
	float ___m_fFadeTime_10;
	// System.Single CBloodStain::m_fFadeTimeElapse
	float ___m_fFadeTimeElapse_11;
	// System.Boolean CBloodStain::m_bLasting
	bool ___m_bLasting_12;
	// System.Boolean CBloodStain::m_bFading
	bool ___m_bFading_13;
	// System.Single CBloodStain::m_fDynamicScaleTime
	float ___m_fDynamicScaleTime_14;
	// System.Single CBloodStain::m_fScale
	float ___m_fScale_15;
	// System.Boolean CBloodStain::m_bDynamicScale
	bool ___m_bDynamicScale_16;
	// System.Boolean CBloodStain::m_bFilling
	bool ___m_bFilling_17;
	// System.Single CBloodStain::m_fFillTotalTime
	float ___m_fFillTotalTime_18;
	// System.Single CBloodStain::m_fFillAmount
	float ___m_fFillAmount_19;
	// System.UInt32 CBloodStain::m_uKey
	uint32_t ___m_uKey_20;
	// System.UInt32 CBloodStain::m_uParentKey
	uint32_t ___m_uParentKey_21;
	// CBlockDivide/eBlockPosType CBloodStain::m_ePosType
	int32_t ___m_ePosType_22;
	// CBlockDivide/eBlockPosType CBloodStain::m_eParentPosType
	int32_t ___m_eParentPosType_23;
	// System.Int32 CBloodStain::m_nHierarchy
	int32_t ___m_nHierarchy_24;
	// System.Int32 CBloodStain::m_nColorId
	int32_t ___m_nColorId_25;
	// UnityEngine.SpriteRenderer CBloodStain::m_sprMain
	SpriteRenderer_t3235626157 * ___m_sprMain_26;
	// UnityEngine.Rendering.SortingGroup CBloodStain::_sortingGroup
	SortingGroup_t3239126932 * ____sortingGroup_27;

public:
	inline static int32_t get_offset_of_m_goMainContainer_2() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_goMainContainer_2)); }
	inline GameObject_t1113636619 * get_m_goMainContainer_2() const { return ___m_goMainContainer_2; }
	inline GameObject_t1113636619 ** get_address_of_m_goMainContainer_2() { return &___m_goMainContainer_2; }
	inline void set_m_goMainContainer_2(GameObject_t1113636619 * value)
	{
		___m_goMainContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_goMainContainer_2), value);
	}

	inline static int32_t get_offset_of_m_eType_6() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_eType_6)); }
	inline int32_t get_m_eType_6() const { return ___m_eType_6; }
	inline int32_t* get_address_of_m_eType_6() { return &___m_eType_6; }
	inline void set_m_eType_6(int32_t value)
	{
		___m_eType_6 = value;
	}

	inline static int32_t get_offset_of_m_eFadeMode_7() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_eFadeMode_7)); }
	inline int32_t get_m_eFadeMode_7() const { return ___m_eFadeMode_7; }
	inline int32_t* get_address_of_m_eFadeMode_7() { return &___m_eFadeMode_7; }
	inline void set_m_eFadeMode_7(int32_t value)
	{
		___m_eFadeMode_7 = value;
	}

	inline static int32_t get_offset_of_m_srMain_8() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_srMain_8)); }
	inline SpriteRenderer_t3235626157 * get_m_srMain_8() const { return ___m_srMain_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_srMain_8() { return &___m_srMain_8; }
	inline void set_m_srMain_8(SpriteRenderer_t3235626157 * value)
	{
		___m_srMain_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_srMain_8), value);
	}

	inline static int32_t get_offset_of_m_fLastTime_9() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fLastTime_9)); }
	inline float get_m_fLastTime_9() const { return ___m_fLastTime_9; }
	inline float* get_address_of_m_fLastTime_9() { return &___m_fLastTime_9; }
	inline void set_m_fLastTime_9(float value)
	{
		___m_fLastTime_9 = value;
	}

	inline static int32_t get_offset_of_m_fFadeTime_10() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fFadeTime_10)); }
	inline float get_m_fFadeTime_10() const { return ___m_fFadeTime_10; }
	inline float* get_address_of_m_fFadeTime_10() { return &___m_fFadeTime_10; }
	inline void set_m_fFadeTime_10(float value)
	{
		___m_fFadeTime_10 = value;
	}

	inline static int32_t get_offset_of_m_fFadeTimeElapse_11() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fFadeTimeElapse_11)); }
	inline float get_m_fFadeTimeElapse_11() const { return ___m_fFadeTimeElapse_11; }
	inline float* get_address_of_m_fFadeTimeElapse_11() { return &___m_fFadeTimeElapse_11; }
	inline void set_m_fFadeTimeElapse_11(float value)
	{
		___m_fFadeTimeElapse_11 = value;
	}

	inline static int32_t get_offset_of_m_bLasting_12() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_bLasting_12)); }
	inline bool get_m_bLasting_12() const { return ___m_bLasting_12; }
	inline bool* get_address_of_m_bLasting_12() { return &___m_bLasting_12; }
	inline void set_m_bLasting_12(bool value)
	{
		___m_bLasting_12 = value;
	}

	inline static int32_t get_offset_of_m_bFading_13() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_bFading_13)); }
	inline bool get_m_bFading_13() const { return ___m_bFading_13; }
	inline bool* get_address_of_m_bFading_13() { return &___m_bFading_13; }
	inline void set_m_bFading_13(bool value)
	{
		___m_bFading_13 = value;
	}

	inline static int32_t get_offset_of_m_fDynamicScaleTime_14() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fDynamicScaleTime_14)); }
	inline float get_m_fDynamicScaleTime_14() const { return ___m_fDynamicScaleTime_14; }
	inline float* get_address_of_m_fDynamicScaleTime_14() { return &___m_fDynamicScaleTime_14; }
	inline void set_m_fDynamicScaleTime_14(float value)
	{
		___m_fDynamicScaleTime_14 = value;
	}

	inline static int32_t get_offset_of_m_fScale_15() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fScale_15)); }
	inline float get_m_fScale_15() const { return ___m_fScale_15; }
	inline float* get_address_of_m_fScale_15() { return &___m_fScale_15; }
	inline void set_m_fScale_15(float value)
	{
		___m_fScale_15 = value;
	}

	inline static int32_t get_offset_of_m_bDynamicScale_16() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_bDynamicScale_16)); }
	inline bool get_m_bDynamicScale_16() const { return ___m_bDynamicScale_16; }
	inline bool* get_address_of_m_bDynamicScale_16() { return &___m_bDynamicScale_16; }
	inline void set_m_bDynamicScale_16(bool value)
	{
		___m_bDynamicScale_16 = value;
	}

	inline static int32_t get_offset_of_m_bFilling_17() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_bFilling_17)); }
	inline bool get_m_bFilling_17() const { return ___m_bFilling_17; }
	inline bool* get_address_of_m_bFilling_17() { return &___m_bFilling_17; }
	inline void set_m_bFilling_17(bool value)
	{
		___m_bFilling_17 = value;
	}

	inline static int32_t get_offset_of_m_fFillTotalTime_18() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fFillTotalTime_18)); }
	inline float get_m_fFillTotalTime_18() const { return ___m_fFillTotalTime_18; }
	inline float* get_address_of_m_fFillTotalTime_18() { return &___m_fFillTotalTime_18; }
	inline void set_m_fFillTotalTime_18(float value)
	{
		___m_fFillTotalTime_18 = value;
	}

	inline static int32_t get_offset_of_m_fFillAmount_19() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_fFillAmount_19)); }
	inline float get_m_fFillAmount_19() const { return ___m_fFillAmount_19; }
	inline float* get_address_of_m_fFillAmount_19() { return &___m_fFillAmount_19; }
	inline void set_m_fFillAmount_19(float value)
	{
		___m_fFillAmount_19 = value;
	}

	inline static int32_t get_offset_of_m_uKey_20() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_uKey_20)); }
	inline uint32_t get_m_uKey_20() const { return ___m_uKey_20; }
	inline uint32_t* get_address_of_m_uKey_20() { return &___m_uKey_20; }
	inline void set_m_uKey_20(uint32_t value)
	{
		___m_uKey_20 = value;
	}

	inline static int32_t get_offset_of_m_uParentKey_21() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_uParentKey_21)); }
	inline uint32_t get_m_uParentKey_21() const { return ___m_uParentKey_21; }
	inline uint32_t* get_address_of_m_uParentKey_21() { return &___m_uParentKey_21; }
	inline void set_m_uParentKey_21(uint32_t value)
	{
		___m_uParentKey_21 = value;
	}

	inline static int32_t get_offset_of_m_ePosType_22() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_ePosType_22)); }
	inline int32_t get_m_ePosType_22() const { return ___m_ePosType_22; }
	inline int32_t* get_address_of_m_ePosType_22() { return &___m_ePosType_22; }
	inline void set_m_ePosType_22(int32_t value)
	{
		___m_ePosType_22 = value;
	}

	inline static int32_t get_offset_of_m_eParentPosType_23() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_eParentPosType_23)); }
	inline int32_t get_m_eParentPosType_23() const { return ___m_eParentPosType_23; }
	inline int32_t* get_address_of_m_eParentPosType_23() { return &___m_eParentPosType_23; }
	inline void set_m_eParentPosType_23(int32_t value)
	{
		___m_eParentPosType_23 = value;
	}

	inline static int32_t get_offset_of_m_nHierarchy_24() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_nHierarchy_24)); }
	inline int32_t get_m_nHierarchy_24() const { return ___m_nHierarchy_24; }
	inline int32_t* get_address_of_m_nHierarchy_24() { return &___m_nHierarchy_24; }
	inline void set_m_nHierarchy_24(int32_t value)
	{
		___m_nHierarchy_24 = value;
	}

	inline static int32_t get_offset_of_m_nColorId_25() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_nColorId_25)); }
	inline int32_t get_m_nColorId_25() const { return ___m_nColorId_25; }
	inline int32_t* get_address_of_m_nColorId_25() { return &___m_nColorId_25; }
	inline void set_m_nColorId_25(int32_t value)
	{
		___m_nColorId_25 = value;
	}

	inline static int32_t get_offset_of_m_sprMain_26() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ___m_sprMain_26)); }
	inline SpriteRenderer_t3235626157 * get_m_sprMain_26() const { return ___m_sprMain_26; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_sprMain_26() { return &___m_sprMain_26; }
	inline void set_m_sprMain_26(SpriteRenderer_t3235626157 * value)
	{
		___m_sprMain_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMain_26), value);
	}

	inline static int32_t get_offset_of__sortingGroup_27() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761, ____sortingGroup_27)); }
	inline SortingGroup_t3239126932 * get__sortingGroup_27() const { return ____sortingGroup_27; }
	inline SortingGroup_t3239126932 ** get_address_of__sortingGroup_27() { return &____sortingGroup_27; }
	inline void set__sortingGroup_27(SortingGroup_t3239126932 * value)
	{
		____sortingGroup_27 = value;
		Il2CppCodeGenWriteBarrier((&____sortingGroup_27), value);
	}
};

struct CBloodStain_t3743325761_StaticFields
{
public:
	// UnityEngine.Vector3 CBloodStain::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CBloodStain::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;
	// UnityEngine.Color CBloodStain::colorTemp
	Color_t2555686324  ___colorTemp_5;

public:
	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}

	inline static int32_t get_offset_of_colorTemp_5() { return static_cast<int32_t>(offsetof(CBloodStain_t3743325761_StaticFields, ___colorTemp_5)); }
	inline Color_t2555686324  get_colorTemp_5() const { return ___colorTemp_5; }
	inline Color_t2555686324 * get_address_of_colorTemp_5() { return &___colorTemp_5; }
	inline void set_colorTemp_5(Color_t2555686324  value)
	{
		___colorTemp_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBLOODSTAIN_T3743325761_H
#ifndef CAUDIOMANAGER_T2887365613_H
#define CAUDIOMANAGER_T2887365613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAudioManager
struct  CAudioManager_t2887365613  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource CAudioManager::audio_main_bg
	AudioSource_t3935305588 * ___audio_main_bg_3;
	// UnityEngine.AudioSource CAudioManager::audio_login_bgm
	AudioSource_t3935305588 * ___audio_login_bgm_4;
	// UnityEngine.AudioSource[] CAudioManager::aryAudio
	AudioSourceU5BU5D_t4028559421* ___aryAudio_5;
	// UnityEngine.AudioSource[] CAudioManager::m_aryVoice
	AudioSourceU5BU5D_t4028559421* ___m_aryVoice_6;
	// CAudioManager/eMainBg CAudioManager::m_eCurMainBg
	int32_t ___m_eCurMainBg_7;
	// UnityEngine.AudioSource[] CAudioManager::m_aryMainBg
	AudioSourceU5BU5D_t4028559421* ___m_aryMainBg_8;

public:
	inline static int32_t get_offset_of_audio_main_bg_3() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613, ___audio_main_bg_3)); }
	inline AudioSource_t3935305588 * get_audio_main_bg_3() const { return ___audio_main_bg_3; }
	inline AudioSource_t3935305588 ** get_address_of_audio_main_bg_3() { return &___audio_main_bg_3; }
	inline void set_audio_main_bg_3(AudioSource_t3935305588 * value)
	{
		___audio_main_bg_3 = value;
		Il2CppCodeGenWriteBarrier((&___audio_main_bg_3), value);
	}

	inline static int32_t get_offset_of_audio_login_bgm_4() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613, ___audio_login_bgm_4)); }
	inline AudioSource_t3935305588 * get_audio_login_bgm_4() const { return ___audio_login_bgm_4; }
	inline AudioSource_t3935305588 ** get_address_of_audio_login_bgm_4() { return &___audio_login_bgm_4; }
	inline void set_audio_login_bgm_4(AudioSource_t3935305588 * value)
	{
		___audio_login_bgm_4 = value;
		Il2CppCodeGenWriteBarrier((&___audio_login_bgm_4), value);
	}

	inline static int32_t get_offset_of_aryAudio_5() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613, ___aryAudio_5)); }
	inline AudioSourceU5BU5D_t4028559421* get_aryAudio_5() const { return ___aryAudio_5; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_aryAudio_5() { return &___aryAudio_5; }
	inline void set_aryAudio_5(AudioSourceU5BU5D_t4028559421* value)
	{
		___aryAudio_5 = value;
		Il2CppCodeGenWriteBarrier((&___aryAudio_5), value);
	}

	inline static int32_t get_offset_of_m_aryVoice_6() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613, ___m_aryVoice_6)); }
	inline AudioSourceU5BU5D_t4028559421* get_m_aryVoice_6() const { return ___m_aryVoice_6; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_m_aryVoice_6() { return &___m_aryVoice_6; }
	inline void set_m_aryVoice_6(AudioSourceU5BU5D_t4028559421* value)
	{
		___m_aryVoice_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVoice_6), value);
	}

	inline static int32_t get_offset_of_m_eCurMainBg_7() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613, ___m_eCurMainBg_7)); }
	inline int32_t get_m_eCurMainBg_7() const { return ___m_eCurMainBg_7; }
	inline int32_t* get_address_of_m_eCurMainBg_7() { return &___m_eCurMainBg_7; }
	inline void set_m_eCurMainBg_7(int32_t value)
	{
		___m_eCurMainBg_7 = value;
	}

	inline static int32_t get_offset_of_m_aryMainBg_8() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613, ___m_aryMainBg_8)); }
	inline AudioSourceU5BU5D_t4028559421* get_m_aryMainBg_8() const { return ___m_aryMainBg_8; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_m_aryMainBg_8() { return &___m_aryMainBg_8; }
	inline void set_m_aryMainBg_8(AudioSourceU5BU5D_t4028559421* value)
	{
		___m_aryMainBg_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainBg_8), value);
	}
};

struct CAudioManager_t2887365613_StaticFields
{
public:
	// CAudioManager CAudioManager::s_Instance
	CAudioManager_t2887365613 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CAudioManager_t2887365613_StaticFields, ___s_Instance_2)); }
	inline CAudioManager_t2887365613 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CAudioManager_t2887365613 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CAudioManager_t2887365613 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAUDIOMANAGER_T2887365613_H
#ifndef CBALLMANAGER_T3583688634_H
#define CBALLMANAGER_T3583688634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBallManager
struct  CBallManager_t3583688634  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Ball> CBallManager::m_dicRecycledBalls_MainPlayer
	Dictionary_2_t1095379897 * ___m_dicRecycledBalls_MainPlayer_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,Ball> CBallManager::m_dicRecycledBalls_OtherClient
	Dictionary_2_t1095379897 * ___m_dicRecycledBalls_OtherClient_4;
	// UnityEngine.GameObject CBallManager::m_preArrow
	GameObject_t1113636619 * ___m_preArrow_5;
	// System.Single CBallManager::m_fArrowLocalScale
	float ___m_fArrowLocalScale_6;
	// System.Int32 CBallManager::s_nBallGuid
	int32_t ___s_nBallGuid_7;

public:
	inline static int32_t get_offset_of_m_dicRecycledBalls_MainPlayer_3() { return static_cast<int32_t>(offsetof(CBallManager_t3583688634, ___m_dicRecycledBalls_MainPlayer_3)); }
	inline Dictionary_2_t1095379897 * get_m_dicRecycledBalls_MainPlayer_3() const { return ___m_dicRecycledBalls_MainPlayer_3; }
	inline Dictionary_2_t1095379897 ** get_address_of_m_dicRecycledBalls_MainPlayer_3() { return &___m_dicRecycledBalls_MainPlayer_3; }
	inline void set_m_dicRecycledBalls_MainPlayer_3(Dictionary_2_t1095379897 * value)
	{
		___m_dicRecycledBalls_MainPlayer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledBalls_MainPlayer_3), value);
	}

	inline static int32_t get_offset_of_m_dicRecycledBalls_OtherClient_4() { return static_cast<int32_t>(offsetof(CBallManager_t3583688634, ___m_dicRecycledBalls_OtherClient_4)); }
	inline Dictionary_2_t1095379897 * get_m_dicRecycledBalls_OtherClient_4() const { return ___m_dicRecycledBalls_OtherClient_4; }
	inline Dictionary_2_t1095379897 ** get_address_of_m_dicRecycledBalls_OtherClient_4() { return &___m_dicRecycledBalls_OtherClient_4; }
	inline void set_m_dicRecycledBalls_OtherClient_4(Dictionary_2_t1095379897 * value)
	{
		___m_dicRecycledBalls_OtherClient_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledBalls_OtherClient_4), value);
	}

	inline static int32_t get_offset_of_m_preArrow_5() { return static_cast<int32_t>(offsetof(CBallManager_t3583688634, ___m_preArrow_5)); }
	inline GameObject_t1113636619 * get_m_preArrow_5() const { return ___m_preArrow_5; }
	inline GameObject_t1113636619 ** get_address_of_m_preArrow_5() { return &___m_preArrow_5; }
	inline void set_m_preArrow_5(GameObject_t1113636619 * value)
	{
		___m_preArrow_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_preArrow_5), value);
	}

	inline static int32_t get_offset_of_m_fArrowLocalScale_6() { return static_cast<int32_t>(offsetof(CBallManager_t3583688634, ___m_fArrowLocalScale_6)); }
	inline float get_m_fArrowLocalScale_6() const { return ___m_fArrowLocalScale_6; }
	inline float* get_address_of_m_fArrowLocalScale_6() { return &___m_fArrowLocalScale_6; }
	inline void set_m_fArrowLocalScale_6(float value)
	{
		___m_fArrowLocalScale_6 = value;
	}

	inline static int32_t get_offset_of_s_nBallGuid_7() { return static_cast<int32_t>(offsetof(CBallManager_t3583688634, ___s_nBallGuid_7)); }
	inline int32_t get_s_nBallGuid_7() const { return ___s_nBallGuid_7; }
	inline int32_t* get_address_of_s_nBallGuid_7() { return &___s_nBallGuid_7; }
	inline void set_s_nBallGuid_7(int32_t value)
	{
		___s_nBallGuid_7 = value;
	}
};

struct CBallManager_t3583688634_StaticFields
{
public:
	// CBallManager CBallManager::s_Instance
	CBallManager_t3583688634 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CBallManager_t3583688634_StaticFields, ___s_Instance_2)); }
	inline CBallManager_t3583688634 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CBallManager_t3583688634 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CBallManager_t3583688634 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBALLMANAGER_T3583688634_H
#ifndef CSKILLMODULEFORBALL_T1995272125_H
#define CSKILLMODULEFORBALL_T1995272125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillModuleForBall
struct  CSkillModuleForBall_t1995272125  : public MonoBehaviour_t3962482529
{
public:
	// Ball CSkillModuleForBall::_ball
	Ball_t2206666566 * ____ball_3;
	// UnityEngine.GameObject CSkillModuleForBall::_goEffectContainer
	GameObject_t1113636619 * ____goEffectContainer_4;
	// CCosmosEffect[] CSkillModuleForBall::m_aryActiveEffect_QianYao
	CCosmosEffectU5BU5D_t3177071136* ___m_aryActiveEffect_QianYao_5;
	// CCosmosEffect[] CSkillModuleForBall::m_aryActiveEffect_ChiXu
	CCosmosEffectU5BU5D_t3177071136* ___m_aryActiveEffect_ChiXu_6;
	// System.Int16 CSkillModuleForBall::m_nSkillStatus
	int16_t ___m_nSkillStatus_7;
	// CSkillSystem/eSkillId CSkillModuleForBall::m_eSkillId
	int32_t ___m_eSkillId_8;

public:
	inline static int32_t get_offset_of__ball_3() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125, ____ball_3)); }
	inline Ball_t2206666566 * get__ball_3() const { return ____ball_3; }
	inline Ball_t2206666566 ** get_address_of__ball_3() { return &____ball_3; }
	inline void set__ball_3(Ball_t2206666566 * value)
	{
		____ball_3 = value;
		Il2CppCodeGenWriteBarrier((&____ball_3), value);
	}

	inline static int32_t get_offset_of__goEffectContainer_4() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125, ____goEffectContainer_4)); }
	inline GameObject_t1113636619 * get__goEffectContainer_4() const { return ____goEffectContainer_4; }
	inline GameObject_t1113636619 ** get_address_of__goEffectContainer_4() { return &____goEffectContainer_4; }
	inline void set__goEffectContainer_4(GameObject_t1113636619 * value)
	{
		____goEffectContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____goEffectContainer_4), value);
	}

	inline static int32_t get_offset_of_m_aryActiveEffect_QianYao_5() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125, ___m_aryActiveEffect_QianYao_5)); }
	inline CCosmosEffectU5BU5D_t3177071136* get_m_aryActiveEffect_QianYao_5() const { return ___m_aryActiveEffect_QianYao_5; }
	inline CCosmosEffectU5BU5D_t3177071136** get_address_of_m_aryActiveEffect_QianYao_5() { return &___m_aryActiveEffect_QianYao_5; }
	inline void set_m_aryActiveEffect_QianYao_5(CCosmosEffectU5BU5D_t3177071136* value)
	{
		___m_aryActiveEffect_QianYao_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryActiveEffect_QianYao_5), value);
	}

	inline static int32_t get_offset_of_m_aryActiveEffect_ChiXu_6() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125, ___m_aryActiveEffect_ChiXu_6)); }
	inline CCosmosEffectU5BU5D_t3177071136* get_m_aryActiveEffect_ChiXu_6() const { return ___m_aryActiveEffect_ChiXu_6; }
	inline CCosmosEffectU5BU5D_t3177071136** get_address_of_m_aryActiveEffect_ChiXu_6() { return &___m_aryActiveEffect_ChiXu_6; }
	inline void set_m_aryActiveEffect_ChiXu_6(CCosmosEffectU5BU5D_t3177071136* value)
	{
		___m_aryActiveEffect_ChiXu_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryActiveEffect_ChiXu_6), value);
	}

	inline static int32_t get_offset_of_m_nSkillStatus_7() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125, ___m_nSkillStatus_7)); }
	inline int16_t get_m_nSkillStatus_7() const { return ___m_nSkillStatus_7; }
	inline int16_t* get_address_of_m_nSkillStatus_7() { return &___m_nSkillStatus_7; }
	inline void set_m_nSkillStatus_7(int16_t value)
	{
		___m_nSkillStatus_7 = value;
	}

	inline static int32_t get_offset_of_m_eSkillId_8() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125, ___m_eSkillId_8)); }
	inline int32_t get_m_eSkillId_8() const { return ___m_eSkillId_8; }
	inline int32_t* get_address_of_m_eSkillId_8() { return &___m_eSkillId_8; }
	inline void set_m_eSkillId_8(int32_t value)
	{
		___m_eSkillId_8 = value;
	}
};

struct CSkillModuleForBall_t1995272125_StaticFields
{
public:
	// UnityEngine.Vector3 CSkillModuleForBall::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CSkillModuleForBall_t1995272125_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSKILLMODULEFORBALL_T1995272125_H
#ifndef BALLACTION_T576009403_H
#define BALLACTION_T576009403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallAction
struct  BallAction_t576009403  : public MonoBehaviour_t3962482529
{
public:
	// System.Single BallAction::_total_scale_min
	float ____total_scale_min_3;
	// System.Single BallAction::_total_scale_max
	float ____total_scale_max_4;
	// System.Single BallAction::_total_scale_ratio
	float ____total_scale_ratio_5;
	// System.Single BallAction::_total_scale_inpoisonable_ratio
	float ____total_scale_inpoisonable_ratio_6;
	// System.Single BallAction::_poison_scale_min
	float ____poison_scale_min_7;
	// System.Single BallAction::_poison_scale_max
	float ____poison_scale_max_8;
	// System.Single BallAction::_poison_scale_inc
	float ____poison_scale_inc_9;
	// System.Single BallAction::_poison_scale_inc_delta
	float ____poison_scale_inc_delta_10;
	// UnityEngine.Vector2 BallAction::_direction
	Vector2_t2156229523  ____direction_11;
	// System.Single BallAction::_ball_realtime_velocity
	float ____ball_realtime_velocity_12;
	// System.Int32 BallAction::_color_index
	int32_t ____color_index_13;
	// System.Single BallAction::_velocity
	float ____velocity_14;
	// UnityEngine.GameObject BallAction::_dir_indicator
	GameObject_t1113636619 * ____dir_indicator_16;
	// UnityEngine.LineRenderer BallAction::_dir_line
	LineRenderer_t3154350270 * ____dir_line_17;
	// Ball BallAction::_ball
	Ball_t2206666566 * ____ball_18;
	// UnityEngine.Vector3 BallAction::vecTempPos1
	Vector3_t3722313464  ___vecTempPos1_19;
	// UnityEngine.Vector3 BallAction::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_20;
	// System.Single BallAction::m_fRiNiGeGuiSpeed
	float ___m_fRiNiGeGuiSpeed_21;
	// System.Single BallAction::_fLastFrameDeltaX
	float ____fLastFrameDeltaX_24;
	// System.Single BallAction::_fLastFrameDeltaY
	float ____fLastFrameDeltaY_25;
	// System.Single BallAction::_velocity_without_size
	float ____velocity_without_size_26;
	// System.Boolean BallAction::m_bAttackDisUpdated
	bool ___m_bAttackDisUpdated_27;
	// System.Boolean BallAction::m_bArrowUpdated
	bool ___m_bArrowUpdated_28;
	// System.Boolean BallAction::m_bRealTimeSpeedUpdated
	bool ___m_bRealTimeSpeedUpdated_29;
	// System.Single BallAction::m_fAttackDis
	float ___m_fAttackDis_30;
	// System.Single BallAction::m_flastSpeedWithoutSize
	float ___m_flastSpeedWithoutSize_31;
	// UnityEngine.Vector2 BallAction::m_vecRealTimeSpeed
	Vector2_t2156229523  ___m_vecRealTimeSpeed_32;

public:
	inline static int32_t get_offset_of__total_scale_min_3() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____total_scale_min_3)); }
	inline float get__total_scale_min_3() const { return ____total_scale_min_3; }
	inline float* get_address_of__total_scale_min_3() { return &____total_scale_min_3; }
	inline void set__total_scale_min_3(float value)
	{
		____total_scale_min_3 = value;
	}

	inline static int32_t get_offset_of__total_scale_max_4() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____total_scale_max_4)); }
	inline float get__total_scale_max_4() const { return ____total_scale_max_4; }
	inline float* get_address_of__total_scale_max_4() { return &____total_scale_max_4; }
	inline void set__total_scale_max_4(float value)
	{
		____total_scale_max_4 = value;
	}

	inline static int32_t get_offset_of__total_scale_ratio_5() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____total_scale_ratio_5)); }
	inline float get__total_scale_ratio_5() const { return ____total_scale_ratio_5; }
	inline float* get_address_of__total_scale_ratio_5() { return &____total_scale_ratio_5; }
	inline void set__total_scale_ratio_5(float value)
	{
		____total_scale_ratio_5 = value;
	}

	inline static int32_t get_offset_of__total_scale_inpoisonable_ratio_6() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____total_scale_inpoisonable_ratio_6)); }
	inline float get__total_scale_inpoisonable_ratio_6() const { return ____total_scale_inpoisonable_ratio_6; }
	inline float* get_address_of__total_scale_inpoisonable_ratio_6() { return &____total_scale_inpoisonable_ratio_6; }
	inline void set__total_scale_inpoisonable_ratio_6(float value)
	{
		____total_scale_inpoisonable_ratio_6 = value;
	}

	inline static int32_t get_offset_of__poison_scale_min_7() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____poison_scale_min_7)); }
	inline float get__poison_scale_min_7() const { return ____poison_scale_min_7; }
	inline float* get_address_of__poison_scale_min_7() { return &____poison_scale_min_7; }
	inline void set__poison_scale_min_7(float value)
	{
		____poison_scale_min_7 = value;
	}

	inline static int32_t get_offset_of__poison_scale_max_8() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____poison_scale_max_8)); }
	inline float get__poison_scale_max_8() const { return ____poison_scale_max_8; }
	inline float* get_address_of__poison_scale_max_8() { return &____poison_scale_max_8; }
	inline void set__poison_scale_max_8(float value)
	{
		____poison_scale_max_8 = value;
	}

	inline static int32_t get_offset_of__poison_scale_inc_9() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____poison_scale_inc_9)); }
	inline float get__poison_scale_inc_9() const { return ____poison_scale_inc_9; }
	inline float* get_address_of__poison_scale_inc_9() { return &____poison_scale_inc_9; }
	inline void set__poison_scale_inc_9(float value)
	{
		____poison_scale_inc_9 = value;
	}

	inline static int32_t get_offset_of__poison_scale_inc_delta_10() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____poison_scale_inc_delta_10)); }
	inline float get__poison_scale_inc_delta_10() const { return ____poison_scale_inc_delta_10; }
	inline float* get_address_of__poison_scale_inc_delta_10() { return &____poison_scale_inc_delta_10; }
	inline void set__poison_scale_inc_delta_10(float value)
	{
		____poison_scale_inc_delta_10 = value;
	}

	inline static int32_t get_offset_of__direction_11() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____direction_11)); }
	inline Vector2_t2156229523  get__direction_11() const { return ____direction_11; }
	inline Vector2_t2156229523 * get_address_of__direction_11() { return &____direction_11; }
	inline void set__direction_11(Vector2_t2156229523  value)
	{
		____direction_11 = value;
	}

	inline static int32_t get_offset_of__ball_realtime_velocity_12() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____ball_realtime_velocity_12)); }
	inline float get__ball_realtime_velocity_12() const { return ____ball_realtime_velocity_12; }
	inline float* get_address_of__ball_realtime_velocity_12() { return &____ball_realtime_velocity_12; }
	inline void set__ball_realtime_velocity_12(float value)
	{
		____ball_realtime_velocity_12 = value;
	}

	inline static int32_t get_offset_of__color_index_13() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____color_index_13)); }
	inline int32_t get__color_index_13() const { return ____color_index_13; }
	inline int32_t* get_address_of__color_index_13() { return &____color_index_13; }
	inline void set__color_index_13(int32_t value)
	{
		____color_index_13 = value;
	}

	inline static int32_t get_offset_of__velocity_14() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____velocity_14)); }
	inline float get__velocity_14() const { return ____velocity_14; }
	inline float* get_address_of__velocity_14() { return &____velocity_14; }
	inline void set__velocity_14(float value)
	{
		____velocity_14 = value;
	}

	inline static int32_t get_offset_of__dir_indicator_16() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____dir_indicator_16)); }
	inline GameObject_t1113636619 * get__dir_indicator_16() const { return ____dir_indicator_16; }
	inline GameObject_t1113636619 ** get_address_of__dir_indicator_16() { return &____dir_indicator_16; }
	inline void set__dir_indicator_16(GameObject_t1113636619 * value)
	{
		____dir_indicator_16 = value;
		Il2CppCodeGenWriteBarrier((&____dir_indicator_16), value);
	}

	inline static int32_t get_offset_of__dir_line_17() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____dir_line_17)); }
	inline LineRenderer_t3154350270 * get__dir_line_17() const { return ____dir_line_17; }
	inline LineRenderer_t3154350270 ** get_address_of__dir_line_17() { return &____dir_line_17; }
	inline void set__dir_line_17(LineRenderer_t3154350270 * value)
	{
		____dir_line_17 = value;
		Il2CppCodeGenWriteBarrier((&____dir_line_17), value);
	}

	inline static int32_t get_offset_of__ball_18() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____ball_18)); }
	inline Ball_t2206666566 * get__ball_18() const { return ____ball_18; }
	inline Ball_t2206666566 ** get_address_of__ball_18() { return &____ball_18; }
	inline void set__ball_18(Ball_t2206666566 * value)
	{
		____ball_18 = value;
		Il2CppCodeGenWriteBarrier((&____ball_18), value);
	}

	inline static int32_t get_offset_of_vecTempPos1_19() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___vecTempPos1_19)); }
	inline Vector3_t3722313464  get_vecTempPos1_19() const { return ___vecTempPos1_19; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos1_19() { return &___vecTempPos1_19; }
	inline void set_vecTempPos1_19(Vector3_t3722313464  value)
	{
		___vecTempPos1_19 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_20() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___vecTempPos2_20)); }
	inline Vector3_t3722313464  get_vecTempPos2_20() const { return ___vecTempPos2_20; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_20() { return &___vecTempPos2_20; }
	inline void set_vecTempPos2_20(Vector3_t3722313464  value)
	{
		___vecTempPos2_20 = value;
	}

	inline static int32_t get_offset_of_m_fRiNiGeGuiSpeed_21() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_fRiNiGeGuiSpeed_21)); }
	inline float get_m_fRiNiGeGuiSpeed_21() const { return ___m_fRiNiGeGuiSpeed_21; }
	inline float* get_address_of_m_fRiNiGeGuiSpeed_21() { return &___m_fRiNiGeGuiSpeed_21; }
	inline void set_m_fRiNiGeGuiSpeed_21(float value)
	{
		___m_fRiNiGeGuiSpeed_21 = value;
	}

	inline static int32_t get_offset_of__fLastFrameDeltaX_24() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____fLastFrameDeltaX_24)); }
	inline float get__fLastFrameDeltaX_24() const { return ____fLastFrameDeltaX_24; }
	inline float* get_address_of__fLastFrameDeltaX_24() { return &____fLastFrameDeltaX_24; }
	inline void set__fLastFrameDeltaX_24(float value)
	{
		____fLastFrameDeltaX_24 = value;
	}

	inline static int32_t get_offset_of__fLastFrameDeltaY_25() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____fLastFrameDeltaY_25)); }
	inline float get__fLastFrameDeltaY_25() const { return ____fLastFrameDeltaY_25; }
	inline float* get_address_of__fLastFrameDeltaY_25() { return &____fLastFrameDeltaY_25; }
	inline void set__fLastFrameDeltaY_25(float value)
	{
		____fLastFrameDeltaY_25 = value;
	}

	inline static int32_t get_offset_of__velocity_without_size_26() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ____velocity_without_size_26)); }
	inline float get__velocity_without_size_26() const { return ____velocity_without_size_26; }
	inline float* get_address_of__velocity_without_size_26() { return &____velocity_without_size_26; }
	inline void set__velocity_without_size_26(float value)
	{
		____velocity_without_size_26 = value;
	}

	inline static int32_t get_offset_of_m_bAttackDisUpdated_27() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_bAttackDisUpdated_27)); }
	inline bool get_m_bAttackDisUpdated_27() const { return ___m_bAttackDisUpdated_27; }
	inline bool* get_address_of_m_bAttackDisUpdated_27() { return &___m_bAttackDisUpdated_27; }
	inline void set_m_bAttackDisUpdated_27(bool value)
	{
		___m_bAttackDisUpdated_27 = value;
	}

	inline static int32_t get_offset_of_m_bArrowUpdated_28() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_bArrowUpdated_28)); }
	inline bool get_m_bArrowUpdated_28() const { return ___m_bArrowUpdated_28; }
	inline bool* get_address_of_m_bArrowUpdated_28() { return &___m_bArrowUpdated_28; }
	inline void set_m_bArrowUpdated_28(bool value)
	{
		___m_bArrowUpdated_28 = value;
	}

	inline static int32_t get_offset_of_m_bRealTimeSpeedUpdated_29() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_bRealTimeSpeedUpdated_29)); }
	inline bool get_m_bRealTimeSpeedUpdated_29() const { return ___m_bRealTimeSpeedUpdated_29; }
	inline bool* get_address_of_m_bRealTimeSpeedUpdated_29() { return &___m_bRealTimeSpeedUpdated_29; }
	inline void set_m_bRealTimeSpeedUpdated_29(bool value)
	{
		___m_bRealTimeSpeedUpdated_29 = value;
	}

	inline static int32_t get_offset_of_m_fAttackDis_30() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_fAttackDis_30)); }
	inline float get_m_fAttackDis_30() const { return ___m_fAttackDis_30; }
	inline float* get_address_of_m_fAttackDis_30() { return &___m_fAttackDis_30; }
	inline void set_m_fAttackDis_30(float value)
	{
		___m_fAttackDis_30 = value;
	}

	inline static int32_t get_offset_of_m_flastSpeedWithoutSize_31() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_flastSpeedWithoutSize_31)); }
	inline float get_m_flastSpeedWithoutSize_31() const { return ___m_flastSpeedWithoutSize_31; }
	inline float* get_address_of_m_flastSpeedWithoutSize_31() { return &___m_flastSpeedWithoutSize_31; }
	inline void set_m_flastSpeedWithoutSize_31(float value)
	{
		___m_flastSpeedWithoutSize_31 = value;
	}

	inline static int32_t get_offset_of_m_vecRealTimeSpeed_32() { return static_cast<int32_t>(offsetof(BallAction_t576009403, ___m_vecRealTimeSpeed_32)); }
	inline Vector2_t2156229523  get_m_vecRealTimeSpeed_32() const { return ___m_vecRealTimeSpeed_32; }
	inline Vector2_t2156229523 * get_address_of_m_vecRealTimeSpeed_32() { return &___m_vecRealTimeSpeed_32; }
	inline void set_m_vecRealTimeSpeed_32(Vector2_t2156229523  value)
	{
		___m_vecRealTimeSpeed_32 = value;
	}
};

struct BallAction_t576009403_StaticFields
{
public:
	// UnityEngine.Vector2 BallAction::s_pos1
	Vector2_t2156229523  ___s_pos1_22;
	// UnityEngine.Vector2 BallAction::s_pos2
	Vector2_t2156229523  ___s_pos2_23;

public:
	inline static int32_t get_offset_of_s_pos1_22() { return static_cast<int32_t>(offsetof(BallAction_t576009403_StaticFields, ___s_pos1_22)); }
	inline Vector2_t2156229523  get_s_pos1_22() const { return ___s_pos1_22; }
	inline Vector2_t2156229523 * get_address_of_s_pos1_22() { return &___s_pos1_22; }
	inline void set_s_pos1_22(Vector2_t2156229523  value)
	{
		___s_pos1_22 = value;
	}

	inline static int32_t get_offset_of_s_pos2_23() { return static_cast<int32_t>(offsetof(BallAction_t576009403_StaticFields, ___s_pos2_23)); }
	inline Vector2_t2156229523  get_s_pos2_23() const { return ___s_pos2_23; }
	inline Vector2_t2156229523 * get_address_of_s_pos2_23() { return &___s_pos2_23; }
	inline void set_s_pos2_23(Vector2_t2156229523  value)
	{
		___s_pos2_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLACTION_T576009403_H
#ifndef CBEAN_T1061610760_H
#define CBEAN_T1061610760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBean
struct  CBean_t1061610760  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer CBean::_sr
	SpriteRenderer_t3235626157 * ____sr_2;
	// System.Int16 CBean::m_nGuid
	int16_t ___m_nGuid_3;
	// System.String CBean::m_szConfigId
	String_t* ___m_szConfigId_4;
	// System.Single CBean::m_fDeadTime
	float ___m_fDeadTime_5;
	// CMonsterEditor/sThornConfig CBean::m_Config
	sThornConfig_t1242444451  ___m_Config_6;
	// CClassEditor/sThornOfThisClassConfig CBean::m_ClassConfig
	sThornOfThisClassConfig_t350464872  ___m_ClassConfig_7;
	// System.Boolean CBean::m_bActive
	bool ___m_bActive_8;
	// UnityEngine.GameObject CBean::m_goContainer
	GameObject_t1113636619 * ___m_goContainer_9;
	// UnityEngine.Collider2D CBean::_Trigger
	Collider2D_t2806799626 * ____Trigger_10;

public:
	inline static int32_t get_offset_of__sr_2() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ____sr_2)); }
	inline SpriteRenderer_t3235626157 * get__sr_2() const { return ____sr_2; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sr_2() { return &____sr_2; }
	inline void set__sr_2(SpriteRenderer_t3235626157 * value)
	{
		____sr_2 = value;
		Il2CppCodeGenWriteBarrier((&____sr_2), value);
	}

	inline static int32_t get_offset_of_m_nGuid_3() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_nGuid_3)); }
	inline int16_t get_m_nGuid_3() const { return ___m_nGuid_3; }
	inline int16_t* get_address_of_m_nGuid_3() { return &___m_nGuid_3; }
	inline void set_m_nGuid_3(int16_t value)
	{
		___m_nGuid_3 = value;
	}

	inline static int32_t get_offset_of_m_szConfigId_4() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_szConfigId_4)); }
	inline String_t* get_m_szConfigId_4() const { return ___m_szConfigId_4; }
	inline String_t** get_address_of_m_szConfigId_4() { return &___m_szConfigId_4; }
	inline void set_m_szConfigId_4(String_t* value)
	{
		___m_szConfigId_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_szConfigId_4), value);
	}

	inline static int32_t get_offset_of_m_fDeadTime_5() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_fDeadTime_5)); }
	inline float get_m_fDeadTime_5() const { return ___m_fDeadTime_5; }
	inline float* get_address_of_m_fDeadTime_5() { return &___m_fDeadTime_5; }
	inline void set_m_fDeadTime_5(float value)
	{
		___m_fDeadTime_5 = value;
	}

	inline static int32_t get_offset_of_m_Config_6() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_Config_6)); }
	inline sThornConfig_t1242444451  get_m_Config_6() const { return ___m_Config_6; }
	inline sThornConfig_t1242444451 * get_address_of_m_Config_6() { return &___m_Config_6; }
	inline void set_m_Config_6(sThornConfig_t1242444451  value)
	{
		___m_Config_6 = value;
	}

	inline static int32_t get_offset_of_m_ClassConfig_7() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_ClassConfig_7)); }
	inline sThornOfThisClassConfig_t350464872  get_m_ClassConfig_7() const { return ___m_ClassConfig_7; }
	inline sThornOfThisClassConfig_t350464872 * get_address_of_m_ClassConfig_7() { return &___m_ClassConfig_7; }
	inline void set_m_ClassConfig_7(sThornOfThisClassConfig_t350464872  value)
	{
		___m_ClassConfig_7 = value;
	}

	inline static int32_t get_offset_of_m_bActive_8() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_bActive_8)); }
	inline bool get_m_bActive_8() const { return ___m_bActive_8; }
	inline bool* get_address_of_m_bActive_8() { return &___m_bActive_8; }
	inline void set_m_bActive_8(bool value)
	{
		___m_bActive_8 = value;
	}

	inline static int32_t get_offset_of_m_goContainer_9() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ___m_goContainer_9)); }
	inline GameObject_t1113636619 * get_m_goContainer_9() const { return ___m_goContainer_9; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainer_9() { return &___m_goContainer_9; }
	inline void set_m_goContainer_9(GameObject_t1113636619 * value)
	{
		___m_goContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainer_9), value);
	}

	inline static int32_t get_offset_of__Trigger_10() { return static_cast<int32_t>(offsetof(CBean_t1061610760, ____Trigger_10)); }
	inline Collider2D_t2806799626 * get__Trigger_10() const { return ____Trigger_10; }
	inline Collider2D_t2806799626 ** get_address_of__Trigger_10() { return &____Trigger_10; }
	inline void set__Trigger_10(Collider2D_t2806799626 * value)
	{
		____Trigger_10 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBEAN_T1061610760_H
#ifndef CBEANMANAGER_T1120502076_H
#define CBEANMANAGER_T1120502076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBeanManager
struct  CBeanManager_t1120502076  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CBeanManager::m_goBeanContainer
	GameObject_t1113636619 * ___m_goBeanContainer_3;
	// System.Int16 CBeanManager::m_nBeanGuid
	int16_t ___m_nBeanGuid_5;
	// System.Collections.Generic.List`1<CBean> CBeanManager::m_lstEatBean
	List_1_t2533685502 * ___m_lstEatBean_6;
	// System.Single CBeanManager::m_fEatBeanCount
	float ___m_fEatBeanCount_8;
	// System.Byte[] CBeanManager::_bytesEatBeanData
	ByteU5BU5D_t4116647657* ____bytesEatBeanData_9;
	// System.Collections.Generic.Dictionary`2<System.Int16,CBean> CBeanManager::m_dicBeans
	Dictionary_2_t3251917897 * ___m_dicBeans_10;
	// System.Single CBeanManager::m_fBeanRebornCount
	float ___m_fBeanRebornCount_12;
	// System.Int32 CBeanManager::m_nTestParam
	int32_t ___m_nTestParam_13;
	// System.Collections.Generic.List`1<CBeanManager/sInitBeanNode> CBeanManager::m_lstBeanToInit
	List_1_t3233134082 * ___m_lstBeanToInit_14;
	// System.Int32 CBeanManager::m_nInitBeanIndex
	int32_t ___m_nInitBeanIndex_15;
	// System.Int32 CBeanManager::m_nInitBeanSubIndex
	int32_t ___m_nInitBeanSubIndex_16;
	// System.Boolean CBeanManager::m_bBeanInitCompleted
	bool ___m_bBeanInitCompleted_17;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Collections.Generic.List`1<CBean>> CBeanManager::m_dicBeansToBeReborn
	Dictionary_2_t3807953164 * ___m_dicBeansToBeReborn_18;
	// System.Collections.Generic.List`1<System.UInt32> CBeanManager::m_lstTempReborn
	List_1_t4032136720 * ___m_lstTempReborn_19;
	// System.Boolean CBeanManager::m_bTesting
	bool ___m_bTesting_20;

public:
	inline static int32_t get_offset_of_m_goBeanContainer_3() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_goBeanContainer_3)); }
	inline GameObject_t1113636619 * get_m_goBeanContainer_3() const { return ___m_goBeanContainer_3; }
	inline GameObject_t1113636619 ** get_address_of_m_goBeanContainer_3() { return &___m_goBeanContainer_3; }
	inline void set_m_goBeanContainer_3(GameObject_t1113636619 * value)
	{
		___m_goBeanContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBeanContainer_3), value);
	}

	inline static int32_t get_offset_of_m_nBeanGuid_5() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_nBeanGuid_5)); }
	inline int16_t get_m_nBeanGuid_5() const { return ___m_nBeanGuid_5; }
	inline int16_t* get_address_of_m_nBeanGuid_5() { return &___m_nBeanGuid_5; }
	inline void set_m_nBeanGuid_5(int16_t value)
	{
		___m_nBeanGuid_5 = value;
	}

	inline static int32_t get_offset_of_m_lstEatBean_6() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_lstEatBean_6)); }
	inline List_1_t2533685502 * get_m_lstEatBean_6() const { return ___m_lstEatBean_6; }
	inline List_1_t2533685502 ** get_address_of_m_lstEatBean_6() { return &___m_lstEatBean_6; }
	inline void set_m_lstEatBean_6(List_1_t2533685502 * value)
	{
		___m_lstEatBean_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEatBean_6), value);
	}

	inline static int32_t get_offset_of_m_fEatBeanCount_8() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_fEatBeanCount_8)); }
	inline float get_m_fEatBeanCount_8() const { return ___m_fEatBeanCount_8; }
	inline float* get_address_of_m_fEatBeanCount_8() { return &___m_fEatBeanCount_8; }
	inline void set_m_fEatBeanCount_8(float value)
	{
		___m_fEatBeanCount_8 = value;
	}

	inline static int32_t get_offset_of__bytesEatBeanData_9() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ____bytesEatBeanData_9)); }
	inline ByteU5BU5D_t4116647657* get__bytesEatBeanData_9() const { return ____bytesEatBeanData_9; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesEatBeanData_9() { return &____bytesEatBeanData_9; }
	inline void set__bytesEatBeanData_9(ByteU5BU5D_t4116647657* value)
	{
		____bytesEatBeanData_9 = value;
		Il2CppCodeGenWriteBarrier((&____bytesEatBeanData_9), value);
	}

	inline static int32_t get_offset_of_m_dicBeans_10() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_dicBeans_10)); }
	inline Dictionary_2_t3251917897 * get_m_dicBeans_10() const { return ___m_dicBeans_10; }
	inline Dictionary_2_t3251917897 ** get_address_of_m_dicBeans_10() { return &___m_dicBeans_10; }
	inline void set_m_dicBeans_10(Dictionary_2_t3251917897 * value)
	{
		___m_dicBeans_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBeans_10), value);
	}

	inline static int32_t get_offset_of_m_fBeanRebornCount_12() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_fBeanRebornCount_12)); }
	inline float get_m_fBeanRebornCount_12() const { return ___m_fBeanRebornCount_12; }
	inline float* get_address_of_m_fBeanRebornCount_12() { return &___m_fBeanRebornCount_12; }
	inline void set_m_fBeanRebornCount_12(float value)
	{
		___m_fBeanRebornCount_12 = value;
	}

	inline static int32_t get_offset_of_m_nTestParam_13() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_nTestParam_13)); }
	inline int32_t get_m_nTestParam_13() const { return ___m_nTestParam_13; }
	inline int32_t* get_address_of_m_nTestParam_13() { return &___m_nTestParam_13; }
	inline void set_m_nTestParam_13(int32_t value)
	{
		___m_nTestParam_13 = value;
	}

	inline static int32_t get_offset_of_m_lstBeanToInit_14() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_lstBeanToInit_14)); }
	inline List_1_t3233134082 * get_m_lstBeanToInit_14() const { return ___m_lstBeanToInit_14; }
	inline List_1_t3233134082 ** get_address_of_m_lstBeanToInit_14() { return &___m_lstBeanToInit_14; }
	inline void set_m_lstBeanToInit_14(List_1_t3233134082 * value)
	{
		___m_lstBeanToInit_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeanToInit_14), value);
	}

	inline static int32_t get_offset_of_m_nInitBeanIndex_15() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_nInitBeanIndex_15)); }
	inline int32_t get_m_nInitBeanIndex_15() const { return ___m_nInitBeanIndex_15; }
	inline int32_t* get_address_of_m_nInitBeanIndex_15() { return &___m_nInitBeanIndex_15; }
	inline void set_m_nInitBeanIndex_15(int32_t value)
	{
		___m_nInitBeanIndex_15 = value;
	}

	inline static int32_t get_offset_of_m_nInitBeanSubIndex_16() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_nInitBeanSubIndex_16)); }
	inline int32_t get_m_nInitBeanSubIndex_16() const { return ___m_nInitBeanSubIndex_16; }
	inline int32_t* get_address_of_m_nInitBeanSubIndex_16() { return &___m_nInitBeanSubIndex_16; }
	inline void set_m_nInitBeanSubIndex_16(int32_t value)
	{
		___m_nInitBeanSubIndex_16 = value;
	}

	inline static int32_t get_offset_of_m_bBeanInitCompleted_17() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_bBeanInitCompleted_17)); }
	inline bool get_m_bBeanInitCompleted_17() const { return ___m_bBeanInitCompleted_17; }
	inline bool* get_address_of_m_bBeanInitCompleted_17() { return &___m_bBeanInitCompleted_17; }
	inline void set_m_bBeanInitCompleted_17(bool value)
	{
		___m_bBeanInitCompleted_17 = value;
	}

	inline static int32_t get_offset_of_m_dicBeansToBeReborn_18() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_dicBeansToBeReborn_18)); }
	inline Dictionary_2_t3807953164 * get_m_dicBeansToBeReborn_18() const { return ___m_dicBeansToBeReborn_18; }
	inline Dictionary_2_t3807953164 ** get_address_of_m_dicBeansToBeReborn_18() { return &___m_dicBeansToBeReborn_18; }
	inline void set_m_dicBeansToBeReborn_18(Dictionary_2_t3807953164 * value)
	{
		___m_dicBeansToBeReborn_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBeansToBeReborn_18), value);
	}

	inline static int32_t get_offset_of_m_lstTempReborn_19() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_lstTempReborn_19)); }
	inline List_1_t4032136720 * get_m_lstTempReborn_19() const { return ___m_lstTempReborn_19; }
	inline List_1_t4032136720 ** get_address_of_m_lstTempReborn_19() { return &___m_lstTempReborn_19; }
	inline void set_m_lstTempReborn_19(List_1_t4032136720 * value)
	{
		___m_lstTempReborn_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTempReborn_19), value);
	}

	inline static int32_t get_offset_of_m_bTesting_20() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076, ___m_bTesting_20)); }
	inline bool get_m_bTesting_20() const { return ___m_bTesting_20; }
	inline bool* get_address_of_m_bTesting_20() { return &___m_bTesting_20; }
	inline void set_m_bTesting_20(bool value)
	{
		___m_bTesting_20 = value;
	}
};

struct CBeanManager_t1120502076_StaticFields
{
public:
	// UnityEngine.Vector3 CBeanManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// CBeanManager CBeanManager::s_Instance
	CBeanManager_t1120502076 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CBeanManager_t1120502076_StaticFields, ___s_Instance_4)); }
	inline CBeanManager_t1120502076 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CBeanManager_t1120502076 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CBeanManager_t1120502076 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBEANMANAGER_T1120502076_H
#ifndef MESHSHAPE_T3358450374_H
#define MESHSHAPE_T3358450374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshShape
struct  MeshShape_t3358450374  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MeshShape::springForce
	float ___springForce_3;
	// System.Single MeshShape::damping
	float ___damping_4;
	// System.Single MeshShape::uniformScale
	float ___uniformScale_5;
	// UnityEngine.Mesh MeshShape::mesh
	Mesh_t3648964284 * ___mesh_6;
	// UnityEngine.MeshRenderer MeshShape::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_7;
	// UnityEngine.MeshFilter MeshShape::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> MeshShape::vertices
	List_1_t899420910 * ___vertices_9;
	// System.Collections.Generic.List`1<System.Int32> MeshShape::triangles
	List_1_t128053199 * ___triangles_10;
	// System.Collections.Generic.List`1<UnityEngine.Color> MeshShape::colors
	List_1_t4027761066 * ___colors_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> MeshShape::uvs
	List_1_t3628304265 * ___uvs_12;
	// System.Boolean MeshShape::isFillPolygon
	bool ___isFillPolygon_13;
	// System.Boolean MeshShape::isFillColor
	bool ___isFillColor_14;
	// System.Single MeshShape::animationAngle
	float ___animationAngle_15;
	// UnityEngine.Vector3[] MeshShape::animationVertices
	Vector3U5BU5D_t1718750761* ___animationVertices_16;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> MeshShape::orignalVertices
	List_1_t899420910 * ___orignalVertices_17;
	// UnityEngine.Vector3[] MeshShape::distortVertices
	Vector3U5BU5D_t1718750761* ___distortVertices_18;
	// UnityEngine.Vector3[] MeshShape::distortVerticesVelocities
	Vector3U5BU5D_t1718750761* ___distortVerticesVelocities_19;
	// UnityEngine.Vector2[] MeshShape::animationUVs
	Vector2U5BU5D_t1457185986* ___animationUVs_20;
	// System.Single MeshShape::animationOuterRadius
	float ___animationOuterRadius_21;
	// System.Single MeshShape::animationInnerRadius
	float ___animationInnerRadius_22;
	// System.Single MeshShape::animationStarrines
	float ___animationStarrines_23;
	// System.Single MeshShape::animationNoiseOffset
	float ___animationNoiseOffset_24;
	// System.Single MeshShape::animationNoiseSpeed
	float ___animationNoiseSpeed_25;
	// System.Single MeshShape::animationNoiseFrequency
	float ___animationNoiseFrequency_26;
	// System.Single MeshShape::animationNoiseAmplitude
	float ___animationNoiseAmplitude_27;
	// ImprovedPerlin MeshShape::m_Perlin
	ImprovedPerlin_t2236177176 * ___m_Perlin_28;
	// System.Boolean MeshShape::m_bDahsed
	bool ___m_bDahsed_29;
	// System.Nullable`1<UnityEngine.Color> MeshShape::m_Color
	Nullable_1_t4278248406  ___m_Color_30;
	// System.Int32 MeshShape::m_numOfSegments
	int32_t ___m_numOfSegments_31;
	// System.Single MeshShape::m_outterRadius
	float ___m_outterRadius_32;
	// System.Single MeshShape::m_innerRadius
	float ___m_innerRadius_33;
	// System.Single MeshShape::m_angle1
	float ___m_angle1_34;
	// System.Single MeshShape::m_angle2
	float ___m_angle2_35;
	// System.Single MeshShape::m_starrines
	float ___m_starrines_36;
	// System.Single MeshShape::m_rotation
	float ___m_rotation_37;

public:
	inline static int32_t get_offset_of_springForce_3() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___springForce_3)); }
	inline float get_springForce_3() const { return ___springForce_3; }
	inline float* get_address_of_springForce_3() { return &___springForce_3; }
	inline void set_springForce_3(float value)
	{
		___springForce_3 = value;
	}

	inline static int32_t get_offset_of_damping_4() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___damping_4)); }
	inline float get_damping_4() const { return ___damping_4; }
	inline float* get_address_of_damping_4() { return &___damping_4; }
	inline void set_damping_4(float value)
	{
		___damping_4 = value;
	}

	inline static int32_t get_offset_of_uniformScale_5() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___uniformScale_5)); }
	inline float get_uniformScale_5() const { return ___uniformScale_5; }
	inline float* get_address_of_uniformScale_5() { return &___uniformScale_5; }
	inline void set_uniformScale_5(float value)
	{
		___uniformScale_5 = value;
	}

	inline static int32_t get_offset_of_mesh_6() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___mesh_6)); }
	inline Mesh_t3648964284 * get_mesh_6() const { return ___mesh_6; }
	inline Mesh_t3648964284 ** get_address_of_mesh_6() { return &___mesh_6; }
	inline void set_mesh_6(Mesh_t3648964284 * value)
	{
		___mesh_6 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_6), value);
	}

	inline static int32_t get_offset_of_meshRenderer_7() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___meshRenderer_7)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_7() const { return ___meshRenderer_7; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_7() { return &___meshRenderer_7; }
	inline void set_meshRenderer_7(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_7), value);
	}

	inline static int32_t get_offset_of_meshFilter_8() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___meshFilter_8)); }
	inline MeshFilter_t3523625662 * get_meshFilter_8() const { return ___meshFilter_8; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_8() { return &___meshFilter_8; }
	inline void set_meshFilter_8(MeshFilter_t3523625662 * value)
	{
		___meshFilter_8 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_8), value);
	}

	inline static int32_t get_offset_of_vertices_9() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___vertices_9)); }
	inline List_1_t899420910 * get_vertices_9() const { return ___vertices_9; }
	inline List_1_t899420910 ** get_address_of_vertices_9() { return &___vertices_9; }
	inline void set_vertices_9(List_1_t899420910 * value)
	{
		___vertices_9 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_9), value);
	}

	inline static int32_t get_offset_of_triangles_10() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___triangles_10)); }
	inline List_1_t128053199 * get_triangles_10() const { return ___triangles_10; }
	inline List_1_t128053199 ** get_address_of_triangles_10() { return &___triangles_10; }
	inline void set_triangles_10(List_1_t128053199 * value)
	{
		___triangles_10 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_10), value);
	}

	inline static int32_t get_offset_of_colors_11() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___colors_11)); }
	inline List_1_t4027761066 * get_colors_11() const { return ___colors_11; }
	inline List_1_t4027761066 ** get_address_of_colors_11() { return &___colors_11; }
	inline void set_colors_11(List_1_t4027761066 * value)
	{
		___colors_11 = value;
		Il2CppCodeGenWriteBarrier((&___colors_11), value);
	}

	inline static int32_t get_offset_of_uvs_12() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___uvs_12)); }
	inline List_1_t3628304265 * get_uvs_12() const { return ___uvs_12; }
	inline List_1_t3628304265 ** get_address_of_uvs_12() { return &___uvs_12; }
	inline void set_uvs_12(List_1_t3628304265 * value)
	{
		___uvs_12 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_12), value);
	}

	inline static int32_t get_offset_of_isFillPolygon_13() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___isFillPolygon_13)); }
	inline bool get_isFillPolygon_13() const { return ___isFillPolygon_13; }
	inline bool* get_address_of_isFillPolygon_13() { return &___isFillPolygon_13; }
	inline void set_isFillPolygon_13(bool value)
	{
		___isFillPolygon_13 = value;
	}

	inline static int32_t get_offset_of_isFillColor_14() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___isFillColor_14)); }
	inline bool get_isFillColor_14() const { return ___isFillColor_14; }
	inline bool* get_address_of_isFillColor_14() { return &___isFillColor_14; }
	inline void set_isFillColor_14(bool value)
	{
		___isFillColor_14 = value;
	}

	inline static int32_t get_offset_of_animationAngle_15() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationAngle_15)); }
	inline float get_animationAngle_15() const { return ___animationAngle_15; }
	inline float* get_address_of_animationAngle_15() { return &___animationAngle_15; }
	inline void set_animationAngle_15(float value)
	{
		___animationAngle_15 = value;
	}

	inline static int32_t get_offset_of_animationVertices_16() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationVertices_16)); }
	inline Vector3U5BU5D_t1718750761* get_animationVertices_16() const { return ___animationVertices_16; }
	inline Vector3U5BU5D_t1718750761** get_address_of_animationVertices_16() { return &___animationVertices_16; }
	inline void set_animationVertices_16(Vector3U5BU5D_t1718750761* value)
	{
		___animationVertices_16 = value;
		Il2CppCodeGenWriteBarrier((&___animationVertices_16), value);
	}

	inline static int32_t get_offset_of_orignalVertices_17() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___orignalVertices_17)); }
	inline List_1_t899420910 * get_orignalVertices_17() const { return ___orignalVertices_17; }
	inline List_1_t899420910 ** get_address_of_orignalVertices_17() { return &___orignalVertices_17; }
	inline void set_orignalVertices_17(List_1_t899420910 * value)
	{
		___orignalVertices_17 = value;
		Il2CppCodeGenWriteBarrier((&___orignalVertices_17), value);
	}

	inline static int32_t get_offset_of_distortVertices_18() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___distortVertices_18)); }
	inline Vector3U5BU5D_t1718750761* get_distortVertices_18() const { return ___distortVertices_18; }
	inline Vector3U5BU5D_t1718750761** get_address_of_distortVertices_18() { return &___distortVertices_18; }
	inline void set_distortVertices_18(Vector3U5BU5D_t1718750761* value)
	{
		___distortVertices_18 = value;
		Il2CppCodeGenWriteBarrier((&___distortVertices_18), value);
	}

	inline static int32_t get_offset_of_distortVerticesVelocities_19() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___distortVerticesVelocities_19)); }
	inline Vector3U5BU5D_t1718750761* get_distortVerticesVelocities_19() const { return ___distortVerticesVelocities_19; }
	inline Vector3U5BU5D_t1718750761** get_address_of_distortVerticesVelocities_19() { return &___distortVerticesVelocities_19; }
	inline void set_distortVerticesVelocities_19(Vector3U5BU5D_t1718750761* value)
	{
		___distortVerticesVelocities_19 = value;
		Il2CppCodeGenWriteBarrier((&___distortVerticesVelocities_19), value);
	}

	inline static int32_t get_offset_of_animationUVs_20() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationUVs_20)); }
	inline Vector2U5BU5D_t1457185986* get_animationUVs_20() const { return ___animationUVs_20; }
	inline Vector2U5BU5D_t1457185986** get_address_of_animationUVs_20() { return &___animationUVs_20; }
	inline void set_animationUVs_20(Vector2U5BU5D_t1457185986* value)
	{
		___animationUVs_20 = value;
		Il2CppCodeGenWriteBarrier((&___animationUVs_20), value);
	}

	inline static int32_t get_offset_of_animationOuterRadius_21() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationOuterRadius_21)); }
	inline float get_animationOuterRadius_21() const { return ___animationOuterRadius_21; }
	inline float* get_address_of_animationOuterRadius_21() { return &___animationOuterRadius_21; }
	inline void set_animationOuterRadius_21(float value)
	{
		___animationOuterRadius_21 = value;
	}

	inline static int32_t get_offset_of_animationInnerRadius_22() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationInnerRadius_22)); }
	inline float get_animationInnerRadius_22() const { return ___animationInnerRadius_22; }
	inline float* get_address_of_animationInnerRadius_22() { return &___animationInnerRadius_22; }
	inline void set_animationInnerRadius_22(float value)
	{
		___animationInnerRadius_22 = value;
	}

	inline static int32_t get_offset_of_animationStarrines_23() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationStarrines_23)); }
	inline float get_animationStarrines_23() const { return ___animationStarrines_23; }
	inline float* get_address_of_animationStarrines_23() { return &___animationStarrines_23; }
	inline void set_animationStarrines_23(float value)
	{
		___animationStarrines_23 = value;
	}

	inline static int32_t get_offset_of_animationNoiseOffset_24() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationNoiseOffset_24)); }
	inline float get_animationNoiseOffset_24() const { return ___animationNoiseOffset_24; }
	inline float* get_address_of_animationNoiseOffset_24() { return &___animationNoiseOffset_24; }
	inline void set_animationNoiseOffset_24(float value)
	{
		___animationNoiseOffset_24 = value;
	}

	inline static int32_t get_offset_of_animationNoiseSpeed_25() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationNoiseSpeed_25)); }
	inline float get_animationNoiseSpeed_25() const { return ___animationNoiseSpeed_25; }
	inline float* get_address_of_animationNoiseSpeed_25() { return &___animationNoiseSpeed_25; }
	inline void set_animationNoiseSpeed_25(float value)
	{
		___animationNoiseSpeed_25 = value;
	}

	inline static int32_t get_offset_of_animationNoiseFrequency_26() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationNoiseFrequency_26)); }
	inline float get_animationNoiseFrequency_26() const { return ___animationNoiseFrequency_26; }
	inline float* get_address_of_animationNoiseFrequency_26() { return &___animationNoiseFrequency_26; }
	inline void set_animationNoiseFrequency_26(float value)
	{
		___animationNoiseFrequency_26 = value;
	}

	inline static int32_t get_offset_of_animationNoiseAmplitude_27() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___animationNoiseAmplitude_27)); }
	inline float get_animationNoiseAmplitude_27() const { return ___animationNoiseAmplitude_27; }
	inline float* get_address_of_animationNoiseAmplitude_27() { return &___animationNoiseAmplitude_27; }
	inline void set_animationNoiseAmplitude_27(float value)
	{
		___animationNoiseAmplitude_27 = value;
	}

	inline static int32_t get_offset_of_m_Perlin_28() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_Perlin_28)); }
	inline ImprovedPerlin_t2236177176 * get_m_Perlin_28() const { return ___m_Perlin_28; }
	inline ImprovedPerlin_t2236177176 ** get_address_of_m_Perlin_28() { return &___m_Perlin_28; }
	inline void set_m_Perlin_28(ImprovedPerlin_t2236177176 * value)
	{
		___m_Perlin_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Perlin_28), value);
	}

	inline static int32_t get_offset_of_m_bDahsed_29() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_bDahsed_29)); }
	inline bool get_m_bDahsed_29() const { return ___m_bDahsed_29; }
	inline bool* get_address_of_m_bDahsed_29() { return &___m_bDahsed_29; }
	inline void set_m_bDahsed_29(bool value)
	{
		___m_bDahsed_29 = value;
	}

	inline static int32_t get_offset_of_m_Color_30() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_Color_30)); }
	inline Nullable_1_t4278248406  get_m_Color_30() const { return ___m_Color_30; }
	inline Nullable_1_t4278248406 * get_address_of_m_Color_30() { return &___m_Color_30; }
	inline void set_m_Color_30(Nullable_1_t4278248406  value)
	{
		___m_Color_30 = value;
	}

	inline static int32_t get_offset_of_m_numOfSegments_31() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_numOfSegments_31)); }
	inline int32_t get_m_numOfSegments_31() const { return ___m_numOfSegments_31; }
	inline int32_t* get_address_of_m_numOfSegments_31() { return &___m_numOfSegments_31; }
	inline void set_m_numOfSegments_31(int32_t value)
	{
		___m_numOfSegments_31 = value;
	}

	inline static int32_t get_offset_of_m_outterRadius_32() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_outterRadius_32)); }
	inline float get_m_outterRadius_32() const { return ___m_outterRadius_32; }
	inline float* get_address_of_m_outterRadius_32() { return &___m_outterRadius_32; }
	inline void set_m_outterRadius_32(float value)
	{
		___m_outterRadius_32 = value;
	}

	inline static int32_t get_offset_of_m_innerRadius_33() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_innerRadius_33)); }
	inline float get_m_innerRadius_33() const { return ___m_innerRadius_33; }
	inline float* get_address_of_m_innerRadius_33() { return &___m_innerRadius_33; }
	inline void set_m_innerRadius_33(float value)
	{
		___m_innerRadius_33 = value;
	}

	inline static int32_t get_offset_of_m_angle1_34() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_angle1_34)); }
	inline float get_m_angle1_34() const { return ___m_angle1_34; }
	inline float* get_address_of_m_angle1_34() { return &___m_angle1_34; }
	inline void set_m_angle1_34(float value)
	{
		___m_angle1_34 = value;
	}

	inline static int32_t get_offset_of_m_angle2_35() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_angle2_35)); }
	inline float get_m_angle2_35() const { return ___m_angle2_35; }
	inline float* get_address_of_m_angle2_35() { return &___m_angle2_35; }
	inline void set_m_angle2_35(float value)
	{
		___m_angle2_35 = value;
	}

	inline static int32_t get_offset_of_m_starrines_36() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_starrines_36)); }
	inline float get_m_starrines_36() const { return ___m_starrines_36; }
	inline float* get_address_of_m_starrines_36() { return &___m_starrines_36; }
	inline void set_m_starrines_36(float value)
	{
		___m_starrines_36 = value;
	}

	inline static int32_t get_offset_of_m_rotation_37() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374, ___m_rotation_37)); }
	inline float get_m_rotation_37() const { return ___m_rotation_37; }
	inline float* get_address_of_m_rotation_37() { return &___m_rotation_37; }
	inline void set_m_rotation_37(float value)
	{
		___m_rotation_37 = value;
	}
};

struct MeshShape_t3358450374_StaticFields
{
public:
	// UnityEngine.Matrix4x4 MeshShape::matrix
	Matrix4x4_t1817901843  ___matrix_2;

public:
	inline static int32_t get_offset_of_matrix_2() { return static_cast<int32_t>(offsetof(MeshShape_t3358450374_StaticFields, ___matrix_2)); }
	inline Matrix4x4_t1817901843  get_matrix_2() const { return ___matrix_2; }
	inline Matrix4x4_t1817901843 * get_address_of_matrix_2() { return &___matrix_2; }
	inline void set_matrix_2(Matrix4x4_t1817901843  value)
	{
		___matrix_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHSHAPE_T3358450374_H
#ifndef CCONTROL_T2293828845_H
#define CCONTROL_T2293828845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CControl
struct  CControl_t2293828845  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField CControl::_inputBallTeamRadiusMultiple
	InputField_t3762917431 * ____inputBallTeamRadiusMultiple_3;

public:
	inline static int32_t get_offset_of__inputBallTeamRadiusMultiple_3() { return static_cast<int32_t>(offsetof(CControl_t2293828845, ____inputBallTeamRadiusMultiple_3)); }
	inline InputField_t3762917431 * get__inputBallTeamRadiusMultiple_3() const { return ____inputBallTeamRadiusMultiple_3; }
	inline InputField_t3762917431 ** get_address_of__inputBallTeamRadiusMultiple_3() { return &____inputBallTeamRadiusMultiple_3; }
	inline void set__inputBallTeamRadiusMultiple_3(InputField_t3762917431 * value)
	{
		____inputBallTeamRadiusMultiple_3 = value;
		Il2CppCodeGenWriteBarrier((&____inputBallTeamRadiusMultiple_3), value);
	}
};

struct CControl_t2293828845_StaticFields
{
public:
	// CControl CControl::s_Instance
	CControl_t2293828845 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CControl_t2293828845_StaticFields, ___s_Instance_2)); }
	inline CControl_t2293828845 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CControl_t2293828845 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CControl_t2293828845 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCONTROL_T2293828845_H
#ifndef CSCREENSHOT_T989390525_H
#define CSCREENSHOT_T989390525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CScreenShot
struct  CScreenShot_t989390525  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera CScreenShot::mainCam
	Camera_t4157153871 * ___mainCam_2;
	// UnityEngine.RenderTexture CScreenShot::rt
	RenderTexture_t2108887433 * ___rt_3;
	// UnityEngine.Texture2D CScreenShot::t2d
	Texture2D_t3840446185 * ___t2d_4;
	// System.Int32 CScreenShot::num
	int32_t ___num_5;

public:
	inline static int32_t get_offset_of_mainCam_2() { return static_cast<int32_t>(offsetof(CScreenShot_t989390525, ___mainCam_2)); }
	inline Camera_t4157153871 * get_mainCam_2() const { return ___mainCam_2; }
	inline Camera_t4157153871 ** get_address_of_mainCam_2() { return &___mainCam_2; }
	inline void set_mainCam_2(Camera_t4157153871 * value)
	{
		___mainCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCam_2), value);
	}

	inline static int32_t get_offset_of_rt_3() { return static_cast<int32_t>(offsetof(CScreenShot_t989390525, ___rt_3)); }
	inline RenderTexture_t2108887433 * get_rt_3() const { return ___rt_3; }
	inline RenderTexture_t2108887433 ** get_address_of_rt_3() { return &___rt_3; }
	inline void set_rt_3(RenderTexture_t2108887433 * value)
	{
		___rt_3 = value;
		Il2CppCodeGenWriteBarrier((&___rt_3), value);
	}

	inline static int32_t get_offset_of_t2d_4() { return static_cast<int32_t>(offsetof(CScreenShot_t989390525, ___t2d_4)); }
	inline Texture2D_t3840446185 * get_t2d_4() const { return ___t2d_4; }
	inline Texture2D_t3840446185 ** get_address_of_t2d_4() { return &___t2d_4; }
	inline void set_t2d_4(Texture2D_t3840446185 * value)
	{
		___t2d_4 = value;
		Il2CppCodeGenWriteBarrier((&___t2d_4), value);
	}

	inline static int32_t get_offset_of_num_5() { return static_cast<int32_t>(offsetof(CScreenShot_t989390525, ___num_5)); }
	inline int32_t get_num_5() const { return ___num_5; }
	inline int32_t* get_address_of_num_5() { return &___num_5; }
	inline void set_num_5(int32_t value)
	{
		___num_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSCREENSHOT_T989390525_H
#ifndef CVISIONMANAER_T3289296692_H
#define CVISIONMANAER_T3289296692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CVisionManaer
struct  CVisionManaer_t3289296692  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.ImageEffects.BlurOptimized CVisionManaer::_scriptBlur
	BlurOptimized_t3334654964 * ____scriptBlur_3;

public:
	inline static int32_t get_offset_of__scriptBlur_3() { return static_cast<int32_t>(offsetof(CVisionManaer_t3289296692, ____scriptBlur_3)); }
	inline BlurOptimized_t3334654964 * get__scriptBlur_3() const { return ____scriptBlur_3; }
	inline BlurOptimized_t3334654964 ** get_address_of__scriptBlur_3() { return &____scriptBlur_3; }
	inline void set__scriptBlur_3(BlurOptimized_t3334654964 * value)
	{
		____scriptBlur_3 = value;
		Il2CppCodeGenWriteBarrier((&____scriptBlur_3), value);
	}
};

struct CVisionManaer_t3289296692_StaticFields
{
public:
	// CVisionManaer CVisionManaer::s_Instance
	CVisionManaer_t3289296692 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CVisionManaer_t3289296692_StaticFields, ___s_Instance_2)); }
	inline CVisionManaer_t3289296692 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CVisionManaer_t3289296692 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CVisionManaer_t3289296692 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CVISIONMANAER_T3289296692_H
#ifndef CAUTOTEST_T3240667618_H
#define CAUTOTEST_T3240667618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CAutoTest
struct  CAutoTest_t3240667618  : public MonoBehaviour_t3962482529
{
public:
	// Player CAutoTest::m_Player
	Player_t3266647312 * ___m_Player_3;
	// System.Boolean CAutoTest::m_bAutoTesing
	bool ___m_bAutoTesing_4;
	// UnityEngine.Vector3 CAutoTest::m_vecWorldCursorPos
	Vector3_t3722313464  ___m_vecWorldCursorPos_7;
	// System.Single CAutoTest::m_fChangeMoveDirInterval
	float ___m_fChangeMoveDirInterval_8;
	// System.Single CAutoTest::m_fAddVolumeInterval
	float ___m_fAddVolumeInterval_9;
	// System.Single CAutoTest::m_fSpitBallInterval
	float ___m_fSpitBallInterval_10;
	// System.Boolean CAutoTest::m_bForceSpiting
	bool ___m_bForceSpiting_11;
	// System.Single CAutoTest::m_fForceSpitingTimeElapse
	float ___m_fForceSpitingTimeElapse_12;

public:
	inline static int32_t get_offset_of_m_Player_3() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_Player_3)); }
	inline Player_t3266647312 * get_m_Player_3() const { return ___m_Player_3; }
	inline Player_t3266647312 ** get_address_of_m_Player_3() { return &___m_Player_3; }
	inline void set_m_Player_3(Player_t3266647312 * value)
	{
		___m_Player_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Player_3), value);
	}

	inline static int32_t get_offset_of_m_bAutoTesing_4() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_bAutoTesing_4)); }
	inline bool get_m_bAutoTesing_4() const { return ___m_bAutoTesing_4; }
	inline bool* get_address_of_m_bAutoTesing_4() { return &___m_bAutoTesing_4; }
	inline void set_m_bAutoTesing_4(bool value)
	{
		___m_bAutoTesing_4 = value;
	}

	inline static int32_t get_offset_of_m_vecWorldCursorPos_7() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_vecWorldCursorPos_7)); }
	inline Vector3_t3722313464  get_m_vecWorldCursorPos_7() const { return ___m_vecWorldCursorPos_7; }
	inline Vector3_t3722313464 * get_address_of_m_vecWorldCursorPos_7() { return &___m_vecWorldCursorPos_7; }
	inline void set_m_vecWorldCursorPos_7(Vector3_t3722313464  value)
	{
		___m_vecWorldCursorPos_7 = value;
	}

	inline static int32_t get_offset_of_m_fChangeMoveDirInterval_8() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_fChangeMoveDirInterval_8)); }
	inline float get_m_fChangeMoveDirInterval_8() const { return ___m_fChangeMoveDirInterval_8; }
	inline float* get_address_of_m_fChangeMoveDirInterval_8() { return &___m_fChangeMoveDirInterval_8; }
	inline void set_m_fChangeMoveDirInterval_8(float value)
	{
		___m_fChangeMoveDirInterval_8 = value;
	}

	inline static int32_t get_offset_of_m_fAddVolumeInterval_9() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_fAddVolumeInterval_9)); }
	inline float get_m_fAddVolumeInterval_9() const { return ___m_fAddVolumeInterval_9; }
	inline float* get_address_of_m_fAddVolumeInterval_9() { return &___m_fAddVolumeInterval_9; }
	inline void set_m_fAddVolumeInterval_9(float value)
	{
		___m_fAddVolumeInterval_9 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallInterval_10() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_fSpitBallInterval_10)); }
	inline float get_m_fSpitBallInterval_10() const { return ___m_fSpitBallInterval_10; }
	inline float* get_address_of_m_fSpitBallInterval_10() { return &___m_fSpitBallInterval_10; }
	inline void set_m_fSpitBallInterval_10(float value)
	{
		___m_fSpitBallInterval_10 = value;
	}

	inline static int32_t get_offset_of_m_bForceSpiting_11() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_bForceSpiting_11)); }
	inline bool get_m_bForceSpiting_11() const { return ___m_bForceSpiting_11; }
	inline bool* get_address_of_m_bForceSpiting_11() { return &___m_bForceSpiting_11; }
	inline void set_m_bForceSpiting_11(bool value)
	{
		___m_bForceSpiting_11 = value;
	}

	inline static int32_t get_offset_of_m_fForceSpitingTimeElapse_12() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618, ___m_fForceSpitingTimeElapse_12)); }
	inline float get_m_fForceSpitingTimeElapse_12() const { return ___m_fForceSpitingTimeElapse_12; }
	inline float* get_address_of_m_fForceSpitingTimeElapse_12() { return &___m_fForceSpitingTimeElapse_12; }
	inline void set_m_fForceSpitingTimeElapse_12(float value)
	{
		___m_fForceSpitingTimeElapse_12 = value;
	}
};

struct CAutoTest_t3240667618_StaticFields
{
public:
	// CAutoTest CAutoTest::s_Instance
	CAutoTest_t3240667618 * ___s_Instance_2;
	// UnityEngine.Vector3 CAutoTest::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Vector2 CAutoTest::vecTempDir
	Vector2_t2156229523  ___vecTempDir_6;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618_StaticFields, ___s_Instance_2)); }
	inline CAutoTest_t3240667618 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CAutoTest_t3240667618 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CAutoTest_t3240667618 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_6() { return static_cast<int32_t>(offsetof(CAutoTest_t3240667618_StaticFields, ___vecTempDir_6)); }
	inline Vector2_t2156229523  get_vecTempDir_6() const { return ___vecTempDir_6; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_6() { return &___vecTempDir_6; }
	inline void set_vecTempDir_6(Vector2_t2156229523  value)
	{
		___vecTempDir_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAUTOTEST_T3240667618_H
#ifndef CCHIQIUANDJISHAITEM_T1987171717_H
#define CCHIQIUANDJISHAITEM_T1987171717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CChiQiuAndJiShaItem
struct  CChiQiuAndJiShaItem_t1987171717  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CChiQiuAndJiShaItem::_txtEaterName
	Text_t1901882714 * ____txtEaterName_3;
	// UnityEngine.UI.Text CChiQiuAndJiShaItem::_txtDeadMeatName
	Text_t1901882714 * ____txtDeadMeatName_4;
	// UnityEngine.UI.Image CChiQiuAndJiShaItem::_imgActionIcon
	Image_t2670269651 * ____imgActionIcon_5;
	// UnityEngine.UI.Image CChiQiuAndJiShaItem::_imgBgBar
	Image_t2670269651 * ____imgBgBar_6;
	// UnityEngine.CanvasGroup CChiQiuAndJiShaItem::_CanvasGroup
	CanvasGroup_t4083511760 * ____CanvasGroup_7;
	// System.Int32 CChiQiuAndJiShaItem::m_nStatus
	int32_t ___m_nStatus_8;
	// System.Single CChiQiuAndJiShaItem::m_fTimeElapse
	float ___m_fTimeElapse_9;
	// System.Single CChiQiuAndJiShaItem::m_fSlideV0
	float ___m_fSlideV0_10;
	// System.Single CChiQiuAndJiShaItem::m_fJumpV0
	float ___m_fJumpV0_11;

public:
	inline static int32_t get_offset_of__txtEaterName_3() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ____txtEaterName_3)); }
	inline Text_t1901882714 * get__txtEaterName_3() const { return ____txtEaterName_3; }
	inline Text_t1901882714 ** get_address_of__txtEaterName_3() { return &____txtEaterName_3; }
	inline void set__txtEaterName_3(Text_t1901882714 * value)
	{
		____txtEaterName_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtEaterName_3), value);
	}

	inline static int32_t get_offset_of__txtDeadMeatName_4() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ____txtDeadMeatName_4)); }
	inline Text_t1901882714 * get__txtDeadMeatName_4() const { return ____txtDeadMeatName_4; }
	inline Text_t1901882714 ** get_address_of__txtDeadMeatName_4() { return &____txtDeadMeatName_4; }
	inline void set__txtDeadMeatName_4(Text_t1901882714 * value)
	{
		____txtDeadMeatName_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtDeadMeatName_4), value);
	}

	inline static int32_t get_offset_of__imgActionIcon_5() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ____imgActionIcon_5)); }
	inline Image_t2670269651 * get__imgActionIcon_5() const { return ____imgActionIcon_5; }
	inline Image_t2670269651 ** get_address_of__imgActionIcon_5() { return &____imgActionIcon_5; }
	inline void set__imgActionIcon_5(Image_t2670269651 * value)
	{
		____imgActionIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgActionIcon_5), value);
	}

	inline static int32_t get_offset_of__imgBgBar_6() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ____imgBgBar_6)); }
	inline Image_t2670269651 * get__imgBgBar_6() const { return ____imgBgBar_6; }
	inline Image_t2670269651 ** get_address_of__imgBgBar_6() { return &____imgBgBar_6; }
	inline void set__imgBgBar_6(Image_t2670269651 * value)
	{
		____imgBgBar_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgBgBar_6), value);
	}

	inline static int32_t get_offset_of__CanvasGroup_7() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ____CanvasGroup_7)); }
	inline CanvasGroup_t4083511760 * get__CanvasGroup_7() const { return ____CanvasGroup_7; }
	inline CanvasGroup_t4083511760 ** get_address_of__CanvasGroup_7() { return &____CanvasGroup_7; }
	inline void set__CanvasGroup_7(CanvasGroup_t4083511760 * value)
	{
		____CanvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&____CanvasGroup_7), value);
	}

	inline static int32_t get_offset_of_m_nStatus_8() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ___m_nStatus_8)); }
	inline int32_t get_m_nStatus_8() const { return ___m_nStatus_8; }
	inline int32_t* get_address_of_m_nStatus_8() { return &___m_nStatus_8; }
	inline void set_m_nStatus_8(int32_t value)
	{
		___m_nStatus_8 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_9() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ___m_fTimeElapse_9)); }
	inline float get_m_fTimeElapse_9() const { return ___m_fTimeElapse_9; }
	inline float* get_address_of_m_fTimeElapse_9() { return &___m_fTimeElapse_9; }
	inline void set_m_fTimeElapse_9(float value)
	{
		___m_fTimeElapse_9 = value;
	}

	inline static int32_t get_offset_of_m_fSlideV0_10() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ___m_fSlideV0_10)); }
	inline float get_m_fSlideV0_10() const { return ___m_fSlideV0_10; }
	inline float* get_address_of_m_fSlideV0_10() { return &___m_fSlideV0_10; }
	inline void set_m_fSlideV0_10(float value)
	{
		___m_fSlideV0_10 = value;
	}

	inline static int32_t get_offset_of_m_fJumpV0_11() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717, ___m_fJumpV0_11)); }
	inline float get_m_fJumpV0_11() const { return ___m_fJumpV0_11; }
	inline float* get_address_of_m_fJumpV0_11() { return &___m_fJumpV0_11; }
	inline void set_m_fJumpV0_11(float value)
	{
		___m_fJumpV0_11 = value;
	}
};

struct CChiQiuAndJiShaItem_t1987171717_StaticFields
{
public:
	// UnityEngine.Vector3 CChiQiuAndJiShaItem::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaItem_t1987171717_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCHIQIUANDJISHAITEM_T1987171717_H
#ifndef CCHIQIUANDJISHAMANAGER_T3507423475_H
#define CCHIQIUANDJISHAMANAGER_T3507423475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CChiQiuAndJiShaManager
struct  CChiQiuAndJiShaManager_t3507423475  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color CChiQiuAndJiShaManager::m_colorMe
	Color_t2555686324  ___m_colorMe_5;
	// UnityEngine.Color CChiQiuAndJiShaManager::m_colorOthers
	Color_t2555686324  ___m_colorOthers_6;
	// UnityEngine.GameObject CChiQiuAndJiShaManager::m_preChiQiuAndJiShaItem
	GameObject_t1113636619 * ___m_preChiQiuAndJiShaItem_7;
	// UnityEngine.Sprite[] CChiQiuAndJiShaManager::m_aryActionIcon
	SpriteU5BU5D_t2581906349* ___m_aryActionIcon_8;
	// UnityEngine.GameObject CChiQiuAndJiShaManager::m_goContainer
	GameObject_t1113636619 * ___m_goContainer_9;
	// System.Single CChiQiuAndJiShaManager::m_fItemGap
	float ___m_fItemGap_10;
	// System.Int32 CChiQiuAndJiShaManager::m_nMaxItemNum
	int32_t ___m_nMaxItemNum_11;
	// System.Single CChiQiuAndJiShaManager::m_fItemShowTime
	float ___m_fItemShowTime_12;
	// System.Single CChiQiuAndJiShaManager::m_fCalculateChiQiuInterval
	float ___m_fCalculateChiQiuInterval_13;
	// System.Single CChiQiuAndJiShaManager::m_fMoveItemsSpeed
	float ___m_fMoveItemsSpeed_14;
	// System.Single CChiQiuAndJiShaManager::m_fSlideTime
	float ___m_fSlideTime_15;
	// System.Single CChiQiuAndJiShaManager::m_fSlideDistance
	float ___m_fSlideDistance_16;
	// System.Single CChiQiuAndJiShaManager::m_fSlideV0
	float ___m_fSlideV0_17;
	// System.Single CChiQiuAndJiShaManager::m_fSlideA
	float ___m_fSlideA_18;
	// System.Single CChiQiuAndJiShaManager::m_fJumpDistance
	float ___m_fJumpDistance_19;
	// System.Single CChiQiuAndJiShaManager::m_fJumpTime
	float ___m_fJumpTime_20;
	// System.Single CChiQiuAndJiShaManager::m_fJumpV0
	float ___m_fJumpV0_21;
	// System.Single CChiQiuAndJiShaManager::m_fJumpA
	float ___m_fJumpA_22;
	// System.Single CChiQiuAndJiShaManager::m_fJumpFadeSpeed
	float ___m_fJumpFadeSpeed_23;
	// System.Single CChiQiuAndJiShaManager::m_fFadeSpeed
	float ___m_fFadeSpeed_24;
	// System.Collections.Generic.List`1<CChiQiuAndJiShaItem> CChiQiuAndJiShaManager::m_lstItems
	List_1_t3459246459 * ___m_lstItems_25;
	// System.Collections.Generic.List`1<CChiQiuAndJiShaItem> CChiQiuAndJiShaManager::m_lstRecycledItems
	List_1_t3459246459 * ___m_lstRecycledItems_26;
	// System.Single CChiQiuAndJiShaManager::m_fTotalMoveDistance
	float ___m_fTotalMoveDistance_27;

public:
	inline static int32_t get_offset_of_m_colorMe_5() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_colorMe_5)); }
	inline Color_t2555686324  get_m_colorMe_5() const { return ___m_colorMe_5; }
	inline Color_t2555686324 * get_address_of_m_colorMe_5() { return &___m_colorMe_5; }
	inline void set_m_colorMe_5(Color_t2555686324  value)
	{
		___m_colorMe_5 = value;
	}

	inline static int32_t get_offset_of_m_colorOthers_6() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_colorOthers_6)); }
	inline Color_t2555686324  get_m_colorOthers_6() const { return ___m_colorOthers_6; }
	inline Color_t2555686324 * get_address_of_m_colorOthers_6() { return &___m_colorOthers_6; }
	inline void set_m_colorOthers_6(Color_t2555686324  value)
	{
		___m_colorOthers_6 = value;
	}

	inline static int32_t get_offset_of_m_preChiQiuAndJiShaItem_7() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_preChiQiuAndJiShaItem_7)); }
	inline GameObject_t1113636619 * get_m_preChiQiuAndJiShaItem_7() const { return ___m_preChiQiuAndJiShaItem_7; }
	inline GameObject_t1113636619 ** get_address_of_m_preChiQiuAndJiShaItem_7() { return &___m_preChiQiuAndJiShaItem_7; }
	inline void set_m_preChiQiuAndJiShaItem_7(GameObject_t1113636619 * value)
	{
		___m_preChiQiuAndJiShaItem_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_preChiQiuAndJiShaItem_7), value);
	}

	inline static int32_t get_offset_of_m_aryActionIcon_8() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_aryActionIcon_8)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryActionIcon_8() const { return ___m_aryActionIcon_8; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryActionIcon_8() { return &___m_aryActionIcon_8; }
	inline void set_m_aryActionIcon_8(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryActionIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryActionIcon_8), value);
	}

	inline static int32_t get_offset_of_m_goContainer_9() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_goContainer_9)); }
	inline GameObject_t1113636619 * get_m_goContainer_9() const { return ___m_goContainer_9; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainer_9() { return &___m_goContainer_9; }
	inline void set_m_goContainer_9(GameObject_t1113636619 * value)
	{
		___m_goContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainer_9), value);
	}

	inline static int32_t get_offset_of_m_fItemGap_10() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fItemGap_10)); }
	inline float get_m_fItemGap_10() const { return ___m_fItemGap_10; }
	inline float* get_address_of_m_fItemGap_10() { return &___m_fItemGap_10; }
	inline void set_m_fItemGap_10(float value)
	{
		___m_fItemGap_10 = value;
	}

	inline static int32_t get_offset_of_m_nMaxItemNum_11() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_nMaxItemNum_11)); }
	inline int32_t get_m_nMaxItemNum_11() const { return ___m_nMaxItemNum_11; }
	inline int32_t* get_address_of_m_nMaxItemNum_11() { return &___m_nMaxItemNum_11; }
	inline void set_m_nMaxItemNum_11(int32_t value)
	{
		___m_nMaxItemNum_11 = value;
	}

	inline static int32_t get_offset_of_m_fItemShowTime_12() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fItemShowTime_12)); }
	inline float get_m_fItemShowTime_12() const { return ___m_fItemShowTime_12; }
	inline float* get_address_of_m_fItemShowTime_12() { return &___m_fItemShowTime_12; }
	inline void set_m_fItemShowTime_12(float value)
	{
		___m_fItemShowTime_12 = value;
	}

	inline static int32_t get_offset_of_m_fCalculateChiQiuInterval_13() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fCalculateChiQiuInterval_13)); }
	inline float get_m_fCalculateChiQiuInterval_13() const { return ___m_fCalculateChiQiuInterval_13; }
	inline float* get_address_of_m_fCalculateChiQiuInterval_13() { return &___m_fCalculateChiQiuInterval_13; }
	inline void set_m_fCalculateChiQiuInterval_13(float value)
	{
		___m_fCalculateChiQiuInterval_13 = value;
	}

	inline static int32_t get_offset_of_m_fMoveItemsSpeed_14() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fMoveItemsSpeed_14)); }
	inline float get_m_fMoveItemsSpeed_14() const { return ___m_fMoveItemsSpeed_14; }
	inline float* get_address_of_m_fMoveItemsSpeed_14() { return &___m_fMoveItemsSpeed_14; }
	inline void set_m_fMoveItemsSpeed_14(float value)
	{
		___m_fMoveItemsSpeed_14 = value;
	}

	inline static int32_t get_offset_of_m_fSlideTime_15() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fSlideTime_15)); }
	inline float get_m_fSlideTime_15() const { return ___m_fSlideTime_15; }
	inline float* get_address_of_m_fSlideTime_15() { return &___m_fSlideTime_15; }
	inline void set_m_fSlideTime_15(float value)
	{
		___m_fSlideTime_15 = value;
	}

	inline static int32_t get_offset_of_m_fSlideDistance_16() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fSlideDistance_16)); }
	inline float get_m_fSlideDistance_16() const { return ___m_fSlideDistance_16; }
	inline float* get_address_of_m_fSlideDistance_16() { return &___m_fSlideDistance_16; }
	inline void set_m_fSlideDistance_16(float value)
	{
		___m_fSlideDistance_16 = value;
	}

	inline static int32_t get_offset_of_m_fSlideV0_17() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fSlideV0_17)); }
	inline float get_m_fSlideV0_17() const { return ___m_fSlideV0_17; }
	inline float* get_address_of_m_fSlideV0_17() { return &___m_fSlideV0_17; }
	inline void set_m_fSlideV0_17(float value)
	{
		___m_fSlideV0_17 = value;
	}

	inline static int32_t get_offset_of_m_fSlideA_18() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fSlideA_18)); }
	inline float get_m_fSlideA_18() const { return ___m_fSlideA_18; }
	inline float* get_address_of_m_fSlideA_18() { return &___m_fSlideA_18; }
	inline void set_m_fSlideA_18(float value)
	{
		___m_fSlideA_18 = value;
	}

	inline static int32_t get_offset_of_m_fJumpDistance_19() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fJumpDistance_19)); }
	inline float get_m_fJumpDistance_19() const { return ___m_fJumpDistance_19; }
	inline float* get_address_of_m_fJumpDistance_19() { return &___m_fJumpDistance_19; }
	inline void set_m_fJumpDistance_19(float value)
	{
		___m_fJumpDistance_19 = value;
	}

	inline static int32_t get_offset_of_m_fJumpTime_20() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fJumpTime_20)); }
	inline float get_m_fJumpTime_20() const { return ___m_fJumpTime_20; }
	inline float* get_address_of_m_fJumpTime_20() { return &___m_fJumpTime_20; }
	inline void set_m_fJumpTime_20(float value)
	{
		___m_fJumpTime_20 = value;
	}

	inline static int32_t get_offset_of_m_fJumpV0_21() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fJumpV0_21)); }
	inline float get_m_fJumpV0_21() const { return ___m_fJumpV0_21; }
	inline float* get_address_of_m_fJumpV0_21() { return &___m_fJumpV0_21; }
	inline void set_m_fJumpV0_21(float value)
	{
		___m_fJumpV0_21 = value;
	}

	inline static int32_t get_offset_of_m_fJumpA_22() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fJumpA_22)); }
	inline float get_m_fJumpA_22() const { return ___m_fJumpA_22; }
	inline float* get_address_of_m_fJumpA_22() { return &___m_fJumpA_22; }
	inline void set_m_fJumpA_22(float value)
	{
		___m_fJumpA_22 = value;
	}

	inline static int32_t get_offset_of_m_fJumpFadeSpeed_23() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fJumpFadeSpeed_23)); }
	inline float get_m_fJumpFadeSpeed_23() const { return ___m_fJumpFadeSpeed_23; }
	inline float* get_address_of_m_fJumpFadeSpeed_23() { return &___m_fJumpFadeSpeed_23; }
	inline void set_m_fJumpFadeSpeed_23(float value)
	{
		___m_fJumpFadeSpeed_23 = value;
	}

	inline static int32_t get_offset_of_m_fFadeSpeed_24() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fFadeSpeed_24)); }
	inline float get_m_fFadeSpeed_24() const { return ___m_fFadeSpeed_24; }
	inline float* get_address_of_m_fFadeSpeed_24() { return &___m_fFadeSpeed_24; }
	inline void set_m_fFadeSpeed_24(float value)
	{
		___m_fFadeSpeed_24 = value;
	}

	inline static int32_t get_offset_of_m_lstItems_25() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_lstItems_25)); }
	inline List_1_t3459246459 * get_m_lstItems_25() const { return ___m_lstItems_25; }
	inline List_1_t3459246459 ** get_address_of_m_lstItems_25() { return &___m_lstItems_25; }
	inline void set_m_lstItems_25(List_1_t3459246459 * value)
	{
		___m_lstItems_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_25), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledItems_26() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_lstRecycledItems_26)); }
	inline List_1_t3459246459 * get_m_lstRecycledItems_26() const { return ___m_lstRecycledItems_26; }
	inline List_1_t3459246459 ** get_address_of_m_lstRecycledItems_26() { return &___m_lstRecycledItems_26; }
	inline void set_m_lstRecycledItems_26(List_1_t3459246459 * value)
	{
		___m_lstRecycledItems_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledItems_26), value);
	}

	inline static int32_t get_offset_of_m_fTotalMoveDistance_27() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475, ___m_fTotalMoveDistance_27)); }
	inline float get_m_fTotalMoveDistance_27() const { return ___m_fTotalMoveDistance_27; }
	inline float* get_address_of_m_fTotalMoveDistance_27() { return &___m_fTotalMoveDistance_27; }
	inline void set_m_fTotalMoveDistance_27(float value)
	{
		___m_fTotalMoveDistance_27 = value;
	}
};

struct CChiQiuAndJiShaManager_t3507423475_StaticFields
{
public:
	// CChiQiuAndJiShaManager CChiQiuAndJiShaManager::s_Instance
	CChiQiuAndJiShaManager_t3507423475 * ___s_Instance_2;
	// UnityEngine.Vector3 CChiQiuAndJiShaManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CChiQiuAndJiShaManager::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475_StaticFields, ___s_Instance_2)); }
	inline CChiQiuAndJiShaManager_t3507423475 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CChiQiuAndJiShaManager_t3507423475 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CChiQiuAndJiShaManager_t3507423475 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CChiQiuAndJiShaManager_t3507423475_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCHIQIUANDJISHAMANAGER_T3507423475_H
#ifndef CLAYERMANAGER_T3570232401_H
#define CLAYERMANAGER_T3570232401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLayerManager
struct  CLayerManager_t3570232401  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAYERMANAGER_T3570232401_H
#ifndef CMOVIE_T1832462693_H
#define CMOVIE_T1832462693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMovie
struct  CMovie_t1832462693  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Video.VideoPlayer CMovie::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_2;
	// UnityEngine.UI.RawImage CMovie::rawImage
	RawImage_t3182918964 * ___rawImage_3;

public:
	inline static int32_t get_offset_of_videoPlayer_2() { return static_cast<int32_t>(offsetof(CMovie_t1832462693, ___videoPlayer_2)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_2() const { return ___videoPlayer_2; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_2() { return &___videoPlayer_2; }
	inline void set_videoPlayer_2(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_2), value);
	}

	inline static int32_t get_offset_of_rawImage_3() { return static_cast<int32_t>(offsetof(CMovie_t1832462693, ___rawImage_3)); }
	inline RawImage_t3182918964 * get_rawImage_3() const { return ___rawImage_3; }
	inline RawImage_t3182918964 ** get_address_of_rawImage_3() { return &___rawImage_3; }
	inline void set_rawImage_3(RawImage_t3182918964 * value)
	{
		___rawImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___rawImage_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMOVIE_T1832462693_H
#ifndef CGYROSCOPE_T3470865232_H
#define CGYROSCOPE_T3470865232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGyroscope
struct  CGyroscope_t3470865232  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CGyroscope::_txtDebugInfo
	Text_t1901882714 * ____txtDebugInfo_3;
	// System.Single CGyroscope::m_fMaxGyroAmount
	float ___m_fMaxGyroAmount_4;
	// System.Single CGyroscope::m_fMaxImagePosDelta
	float ___m_fMaxImagePosDelta_5;
	// UnityEngine.GameObject CGyroscope::m_goBgImage
	GameObject_t1113636619 * ___m_goBgImage_6;

public:
	inline static int32_t get_offset_of__txtDebugInfo_3() { return static_cast<int32_t>(offsetof(CGyroscope_t3470865232, ____txtDebugInfo_3)); }
	inline Text_t1901882714 * get__txtDebugInfo_3() const { return ____txtDebugInfo_3; }
	inline Text_t1901882714 ** get_address_of__txtDebugInfo_3() { return &____txtDebugInfo_3; }
	inline void set__txtDebugInfo_3(Text_t1901882714 * value)
	{
		____txtDebugInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfo_3), value);
	}

	inline static int32_t get_offset_of_m_fMaxGyroAmount_4() { return static_cast<int32_t>(offsetof(CGyroscope_t3470865232, ___m_fMaxGyroAmount_4)); }
	inline float get_m_fMaxGyroAmount_4() const { return ___m_fMaxGyroAmount_4; }
	inline float* get_address_of_m_fMaxGyroAmount_4() { return &___m_fMaxGyroAmount_4; }
	inline void set_m_fMaxGyroAmount_4(float value)
	{
		___m_fMaxGyroAmount_4 = value;
	}

	inline static int32_t get_offset_of_m_fMaxImagePosDelta_5() { return static_cast<int32_t>(offsetof(CGyroscope_t3470865232, ___m_fMaxImagePosDelta_5)); }
	inline float get_m_fMaxImagePosDelta_5() const { return ___m_fMaxImagePosDelta_5; }
	inline float* get_address_of_m_fMaxImagePosDelta_5() { return &___m_fMaxImagePosDelta_5; }
	inline void set_m_fMaxImagePosDelta_5(float value)
	{
		___m_fMaxImagePosDelta_5 = value;
	}

	inline static int32_t get_offset_of_m_goBgImage_6() { return static_cast<int32_t>(offsetof(CGyroscope_t3470865232, ___m_goBgImage_6)); }
	inline GameObject_t1113636619 * get_m_goBgImage_6() const { return ___m_goBgImage_6; }
	inline GameObject_t1113636619 ** get_address_of_m_goBgImage_6() { return &___m_goBgImage_6; }
	inline void set_m_goBgImage_6(GameObject_t1113636619 * value)
	{
		___m_goBgImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBgImage_6), value);
	}
};

struct CGyroscope_t3470865232_StaticFields
{
public:
	// UnityEngine.Vector3 CGyroscope::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CGyroscope_t3470865232_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGYROSCOPE_T3470865232_H
#ifndef CDEF_T3381995704_H
#define CDEF_T3381995704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CDef
struct  CDef_t3381995704  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CDEF_T3381995704_H
#ifndef CDICK_T2288086465_H
#define CDICK_T2288086465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CDick
struct  CDick_t2288086465  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CDick::_goCyliner
	GameObject_t1113636619 * ____goCyliner_2;
	// UnityEngine.SpriteRenderer CDick::_srCyliner
	SpriteRenderer_t3235626157 * ____srCyliner_3;
	// UnityEngine.GameObject CDick::_goCircle
	GameObject_t1113636619 * ____goCircle_4;
	// UnityEngine.SpriteRenderer CDick::_srCircle
	SpriteRenderer_t3235626157 * ____srCircle_5;

public:
	inline static int32_t get_offset_of__goCyliner_2() { return static_cast<int32_t>(offsetof(CDick_t2288086465, ____goCyliner_2)); }
	inline GameObject_t1113636619 * get__goCyliner_2() const { return ____goCyliner_2; }
	inline GameObject_t1113636619 ** get_address_of__goCyliner_2() { return &____goCyliner_2; }
	inline void set__goCyliner_2(GameObject_t1113636619 * value)
	{
		____goCyliner_2 = value;
		Il2CppCodeGenWriteBarrier((&____goCyliner_2), value);
	}

	inline static int32_t get_offset_of__srCyliner_3() { return static_cast<int32_t>(offsetof(CDick_t2288086465, ____srCyliner_3)); }
	inline SpriteRenderer_t3235626157 * get__srCyliner_3() const { return ____srCyliner_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srCyliner_3() { return &____srCyliner_3; }
	inline void set__srCyliner_3(SpriteRenderer_t3235626157 * value)
	{
		____srCyliner_3 = value;
		Il2CppCodeGenWriteBarrier((&____srCyliner_3), value);
	}

	inline static int32_t get_offset_of__goCircle_4() { return static_cast<int32_t>(offsetof(CDick_t2288086465, ____goCircle_4)); }
	inline GameObject_t1113636619 * get__goCircle_4() const { return ____goCircle_4; }
	inline GameObject_t1113636619 ** get_address_of__goCircle_4() { return &____goCircle_4; }
	inline void set__goCircle_4(GameObject_t1113636619 * value)
	{
		____goCircle_4 = value;
		Il2CppCodeGenWriteBarrier((&____goCircle_4), value);
	}

	inline static int32_t get_offset_of__srCircle_5() { return static_cast<int32_t>(offsetof(CDick_t2288086465, ____srCircle_5)); }
	inline SpriteRenderer_t3235626157 * get__srCircle_5() const { return ____srCircle_5; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srCircle_5() { return &____srCircle_5; }
	inline void set__srCircle_5(SpriteRenderer_t3235626157 * value)
	{
		____srCircle_5 = value;
		Il2CppCodeGenWriteBarrier((&____srCircle_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CDICK_T2288086465_H
#ifndef CEXPLODENODE_T97844667_H
#define CEXPLODENODE_T97844667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CExplodeNode
struct  CExplodeNode_t97844667  : public MonoBehaviour_t3962482529
{
public:
	// Player CExplodeNode::_Player
	Player_t3266647312 * ____Player_2;
	// System.Boolean CExplodeNode::m_bExploding
	bool ___m_bExploding_3;
	// System.Boolean CExplodeNode::m_bWaiting
	bool ___m_bWaiting_4;
	// System.Boolean CExplodeNode::m_bAutoMoveAfterExplodeCompleted
	bool ___m_bAutoMoveAfterExplodeCompleted_5;
	// Ball CExplodeNode::ballMother
	Ball_t2206666566 * ___ballMother_6;
	// CMonsterEditor/sThornConfig CExplodeNode::explodeConfig
	sThornConfig_t1242444451  ___explodeConfig_7;
	// System.Boolean CExplodeNode::m_bMotherMoving
	bool ___m_bMotherMoving_8;
	// System.Int32 CExplodeNode::nChildNum
	int32_t ___nChildNum_9;
	// System.Int32 CExplodeNode::nConfigChildNum
	int32_t ___nConfigChildNum_10;
	// System.Single CExplodeNode::fExpectAngle
	float ___fExpectAngle_11;
	// System.Single CExplodeNode::fChildVolume
	float ___fChildVolume_12;
	// System.Single CExplodeNode::fMotherLeftVolume
	float ___fMotherLeftVolume_13;
	// System.Collections.Generic.List`1<System.Int32> CExplodeNode::lstChildBallIndex
	List_1_t128053199 * ___lstChildBallIndex_14;
	// UnityEngine.Vector3 CExplodeNode::m_vecMotherPos
	Vector3_t3722313464  ___m_vecMotherPos_15;
	// System.Single CExplodeNode::m_fMotherRadius
	float ___m_fMotherRadius_16;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> CExplodeNode::m_lstDir
	List_1_t3628304265 * ___m_lstDir_17;
	// System.Single CExplodeNode::m_fSpeed
	float ___m_fSpeed_18;
	// System.Single CExplodeNode::m_fRunDistance
	float ___m_fRunDistance_19;
	// System.Int32 CExplodeNode::m_nShiZi_BallNum_PerDir_Moving
	int32_t ___m_nShiZi_BallNum_PerDir_Moving_20;
	// System.Int32 CExplodeNode::m_nShiZi_BallNum_PerDir_NotMoving
	int32_t ___m_nShiZi_BallNum_PerDir_NotMoving_21;
	// UnityEngine.Vector2 CExplodeNode::m_vecMotherDir
	Vector2_t2156229523  ___m_vecMotherDir_22;
	// System.Single CExplodeNode::m_fMotherDirAngle
	float ___m_fMotherDirAngle_23;
	// System.Single CExplodeNode::m_fShiZi_ZuoCe_Angle
	float ___m_fShiZi_ZuoCe_Angle_24;
	// System.Single CExplodeNode::m_fShiZi_YouCe_Angle
	float ___m_fShiZi_YouCe_Angle_25;
	// System.Single CExplodeNode::m_fShiZi_HouFang_Angle
	float ___m_fShiZi_HouFang_Angle_26;
	// System.Single CExplodeNode::m_fSegDis_Moving
	float ___m_fSegDis_Moving_27;
	// UnityEngine.Vector2 CExplodeNode::m_vecSHiZi_ZuoCe_Dir
	Vector2_t2156229523  ___m_vecSHiZi_ZuoCe_Dir_28;
	// UnityEngine.Vector2 CExplodeNode::m_vecSHiZi_YouCe_Dir
	Vector2_t2156229523  ___m_vecSHiZi_YouCe_Dir_29;
	// UnityEngine.Vector2 CExplodeNode::m_vecSHiZi_HouFang_Dir
	Vector2_t2156229523  ___m_vecSHiZi_HouFang_Dir_30;
	// System.Int32 CExplodeNode::m_nShiZiStatus
	int32_t ___m_nShiZiStatus_31;
	// System.Int32 CExplodeNode::m_nShiZiStatusNumCount
	int32_t ___m_nShiZiStatusNumCount_32;
	// System.Int32 CExplodeNode::m_nZuoNumCount
	int32_t ___m_nZuoNumCount_33;
	// System.Int32 CExplodeNode::m_nYouNumCount
	int32_t ___m_nYouNumCount_34;
	// UnityEngine.Vector2 CExplodeNode::m_vecSanJiao_ZuoCe_Dir
	Vector2_t2156229523  ___m_vecSanJiao_ZuoCe_Dir_38;
	// UnityEngine.Vector2 CExplodeNode::m_vecSanJiao_YouCe_Dir
	Vector2_t2156229523  ___m_vecSanJiao_YouCe_Dir_39;
	// System.Int32 CExplodeNode::m_nSanJiaoStatus
	int32_t ___m_nSanJiaoStatus_40;
	// System.Int32 CExplodeNode::m_nSanJiaoStatusNumCount
	int32_t ___m_nSanJiaoStatusNumCount_41;
	// System.Int32 CExplodeNode::m_nSanJiaoZuoNumCount
	int32_t ___m_nSanJiaoZuoNumCount_42;
	// System.Int32 CExplodeNode::m_nSanJiaoYouNumCount
	int32_t ___m_nSanJiaoYouNumCount_43;
	// System.Int32 CExplodeNode::m_nSanJiao_BallNum_PerDir_Moving
	int32_t ___m_nSanJiao_BallNum_PerDir_Moving_46;
	// System.Single CExplodeNode::m_fSanJiao_SegDis_Moving
	float ___m_fSanJiao_SegDis_Moving_47;
	// System.Int32 CExplodeNode::m_nCurChildNum
	int32_t ___m_nCurChildNum_48;
	// System.Int32 CExplodeNode::m_nExplodeRound
	int32_t ___m_nExplodeRound_49;
	// System.Single CExplodeNode::m_fSegDis
	float ___m_fSegDis_50;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CExplodeNode::m_lstTempEjectEndPos
	List_1_t899420910 * ___m_lstTempEjectEndPos_55;
	// System.Single CExplodeNode::m_fWaitingTimeElapse
	float ___m_fWaitingTimeElapse_56;

public:
	inline static int32_t get_offset_of__Player_2() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ____Player_2)); }
	inline Player_t3266647312 * get__Player_2() const { return ____Player_2; }
	inline Player_t3266647312 ** get_address_of__Player_2() { return &____Player_2; }
	inline void set__Player_2(Player_t3266647312 * value)
	{
		____Player_2 = value;
		Il2CppCodeGenWriteBarrier((&____Player_2), value);
	}

	inline static int32_t get_offset_of_m_bExploding_3() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_bExploding_3)); }
	inline bool get_m_bExploding_3() const { return ___m_bExploding_3; }
	inline bool* get_address_of_m_bExploding_3() { return &___m_bExploding_3; }
	inline void set_m_bExploding_3(bool value)
	{
		___m_bExploding_3 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting_4() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_bWaiting_4)); }
	inline bool get_m_bWaiting_4() const { return ___m_bWaiting_4; }
	inline bool* get_address_of_m_bWaiting_4() { return &___m_bWaiting_4; }
	inline void set_m_bWaiting_4(bool value)
	{
		___m_bWaiting_4 = value;
	}

	inline static int32_t get_offset_of_m_bAutoMoveAfterExplodeCompleted_5() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_bAutoMoveAfterExplodeCompleted_5)); }
	inline bool get_m_bAutoMoveAfterExplodeCompleted_5() const { return ___m_bAutoMoveAfterExplodeCompleted_5; }
	inline bool* get_address_of_m_bAutoMoveAfterExplodeCompleted_5() { return &___m_bAutoMoveAfterExplodeCompleted_5; }
	inline void set_m_bAutoMoveAfterExplodeCompleted_5(bool value)
	{
		___m_bAutoMoveAfterExplodeCompleted_5 = value;
	}

	inline static int32_t get_offset_of_ballMother_6() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___ballMother_6)); }
	inline Ball_t2206666566 * get_ballMother_6() const { return ___ballMother_6; }
	inline Ball_t2206666566 ** get_address_of_ballMother_6() { return &___ballMother_6; }
	inline void set_ballMother_6(Ball_t2206666566 * value)
	{
		___ballMother_6 = value;
		Il2CppCodeGenWriteBarrier((&___ballMother_6), value);
	}

	inline static int32_t get_offset_of_explodeConfig_7() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___explodeConfig_7)); }
	inline sThornConfig_t1242444451  get_explodeConfig_7() const { return ___explodeConfig_7; }
	inline sThornConfig_t1242444451 * get_address_of_explodeConfig_7() { return &___explodeConfig_7; }
	inline void set_explodeConfig_7(sThornConfig_t1242444451  value)
	{
		___explodeConfig_7 = value;
	}

	inline static int32_t get_offset_of_m_bMotherMoving_8() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_bMotherMoving_8)); }
	inline bool get_m_bMotherMoving_8() const { return ___m_bMotherMoving_8; }
	inline bool* get_address_of_m_bMotherMoving_8() { return &___m_bMotherMoving_8; }
	inline void set_m_bMotherMoving_8(bool value)
	{
		___m_bMotherMoving_8 = value;
	}

	inline static int32_t get_offset_of_nChildNum_9() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___nChildNum_9)); }
	inline int32_t get_nChildNum_9() const { return ___nChildNum_9; }
	inline int32_t* get_address_of_nChildNum_9() { return &___nChildNum_9; }
	inline void set_nChildNum_9(int32_t value)
	{
		___nChildNum_9 = value;
	}

	inline static int32_t get_offset_of_nConfigChildNum_10() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___nConfigChildNum_10)); }
	inline int32_t get_nConfigChildNum_10() const { return ___nConfigChildNum_10; }
	inline int32_t* get_address_of_nConfigChildNum_10() { return &___nConfigChildNum_10; }
	inline void set_nConfigChildNum_10(int32_t value)
	{
		___nConfigChildNum_10 = value;
	}

	inline static int32_t get_offset_of_fExpectAngle_11() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___fExpectAngle_11)); }
	inline float get_fExpectAngle_11() const { return ___fExpectAngle_11; }
	inline float* get_address_of_fExpectAngle_11() { return &___fExpectAngle_11; }
	inline void set_fExpectAngle_11(float value)
	{
		___fExpectAngle_11 = value;
	}

	inline static int32_t get_offset_of_fChildVolume_12() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___fChildVolume_12)); }
	inline float get_fChildVolume_12() const { return ___fChildVolume_12; }
	inline float* get_address_of_fChildVolume_12() { return &___fChildVolume_12; }
	inline void set_fChildVolume_12(float value)
	{
		___fChildVolume_12 = value;
	}

	inline static int32_t get_offset_of_fMotherLeftVolume_13() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___fMotherLeftVolume_13)); }
	inline float get_fMotherLeftVolume_13() const { return ___fMotherLeftVolume_13; }
	inline float* get_address_of_fMotherLeftVolume_13() { return &___fMotherLeftVolume_13; }
	inline void set_fMotherLeftVolume_13(float value)
	{
		___fMotherLeftVolume_13 = value;
	}

	inline static int32_t get_offset_of_lstChildBallIndex_14() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___lstChildBallIndex_14)); }
	inline List_1_t128053199 * get_lstChildBallIndex_14() const { return ___lstChildBallIndex_14; }
	inline List_1_t128053199 ** get_address_of_lstChildBallIndex_14() { return &___lstChildBallIndex_14; }
	inline void set_lstChildBallIndex_14(List_1_t128053199 * value)
	{
		___lstChildBallIndex_14 = value;
		Il2CppCodeGenWriteBarrier((&___lstChildBallIndex_14), value);
	}

	inline static int32_t get_offset_of_m_vecMotherPos_15() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecMotherPos_15)); }
	inline Vector3_t3722313464  get_m_vecMotherPos_15() const { return ___m_vecMotherPos_15; }
	inline Vector3_t3722313464 * get_address_of_m_vecMotherPos_15() { return &___m_vecMotherPos_15; }
	inline void set_m_vecMotherPos_15(Vector3_t3722313464  value)
	{
		___m_vecMotherPos_15 = value;
	}

	inline static int32_t get_offset_of_m_fMotherRadius_16() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fMotherRadius_16)); }
	inline float get_m_fMotherRadius_16() const { return ___m_fMotherRadius_16; }
	inline float* get_address_of_m_fMotherRadius_16() { return &___m_fMotherRadius_16; }
	inline void set_m_fMotherRadius_16(float value)
	{
		___m_fMotherRadius_16 = value;
	}

	inline static int32_t get_offset_of_m_lstDir_17() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_lstDir_17)); }
	inline List_1_t3628304265 * get_m_lstDir_17() const { return ___m_lstDir_17; }
	inline List_1_t3628304265 ** get_address_of_m_lstDir_17() { return &___m_lstDir_17; }
	inline void set_m_lstDir_17(List_1_t3628304265 * value)
	{
		___m_lstDir_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstDir_17), value);
	}

	inline static int32_t get_offset_of_m_fSpeed_18() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fSpeed_18)); }
	inline float get_m_fSpeed_18() const { return ___m_fSpeed_18; }
	inline float* get_address_of_m_fSpeed_18() { return &___m_fSpeed_18; }
	inline void set_m_fSpeed_18(float value)
	{
		___m_fSpeed_18 = value;
	}

	inline static int32_t get_offset_of_m_fRunDistance_19() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fRunDistance_19)); }
	inline float get_m_fRunDistance_19() const { return ___m_fRunDistance_19; }
	inline float* get_address_of_m_fRunDistance_19() { return &___m_fRunDistance_19; }
	inline void set_m_fRunDistance_19(float value)
	{
		___m_fRunDistance_19 = value;
	}

	inline static int32_t get_offset_of_m_nShiZi_BallNum_PerDir_Moving_20() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nShiZi_BallNum_PerDir_Moving_20)); }
	inline int32_t get_m_nShiZi_BallNum_PerDir_Moving_20() const { return ___m_nShiZi_BallNum_PerDir_Moving_20; }
	inline int32_t* get_address_of_m_nShiZi_BallNum_PerDir_Moving_20() { return &___m_nShiZi_BallNum_PerDir_Moving_20; }
	inline void set_m_nShiZi_BallNum_PerDir_Moving_20(int32_t value)
	{
		___m_nShiZi_BallNum_PerDir_Moving_20 = value;
	}

	inline static int32_t get_offset_of_m_nShiZi_BallNum_PerDir_NotMoving_21() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nShiZi_BallNum_PerDir_NotMoving_21)); }
	inline int32_t get_m_nShiZi_BallNum_PerDir_NotMoving_21() const { return ___m_nShiZi_BallNum_PerDir_NotMoving_21; }
	inline int32_t* get_address_of_m_nShiZi_BallNum_PerDir_NotMoving_21() { return &___m_nShiZi_BallNum_PerDir_NotMoving_21; }
	inline void set_m_nShiZi_BallNum_PerDir_NotMoving_21(int32_t value)
	{
		___m_nShiZi_BallNum_PerDir_NotMoving_21 = value;
	}

	inline static int32_t get_offset_of_m_vecMotherDir_22() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecMotherDir_22)); }
	inline Vector2_t2156229523  get_m_vecMotherDir_22() const { return ___m_vecMotherDir_22; }
	inline Vector2_t2156229523 * get_address_of_m_vecMotherDir_22() { return &___m_vecMotherDir_22; }
	inline void set_m_vecMotherDir_22(Vector2_t2156229523  value)
	{
		___m_vecMotherDir_22 = value;
	}

	inline static int32_t get_offset_of_m_fMotherDirAngle_23() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fMotherDirAngle_23)); }
	inline float get_m_fMotherDirAngle_23() const { return ___m_fMotherDirAngle_23; }
	inline float* get_address_of_m_fMotherDirAngle_23() { return &___m_fMotherDirAngle_23; }
	inline void set_m_fMotherDirAngle_23(float value)
	{
		___m_fMotherDirAngle_23 = value;
	}

	inline static int32_t get_offset_of_m_fShiZi_ZuoCe_Angle_24() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fShiZi_ZuoCe_Angle_24)); }
	inline float get_m_fShiZi_ZuoCe_Angle_24() const { return ___m_fShiZi_ZuoCe_Angle_24; }
	inline float* get_address_of_m_fShiZi_ZuoCe_Angle_24() { return &___m_fShiZi_ZuoCe_Angle_24; }
	inline void set_m_fShiZi_ZuoCe_Angle_24(float value)
	{
		___m_fShiZi_ZuoCe_Angle_24 = value;
	}

	inline static int32_t get_offset_of_m_fShiZi_YouCe_Angle_25() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fShiZi_YouCe_Angle_25)); }
	inline float get_m_fShiZi_YouCe_Angle_25() const { return ___m_fShiZi_YouCe_Angle_25; }
	inline float* get_address_of_m_fShiZi_YouCe_Angle_25() { return &___m_fShiZi_YouCe_Angle_25; }
	inline void set_m_fShiZi_YouCe_Angle_25(float value)
	{
		___m_fShiZi_YouCe_Angle_25 = value;
	}

	inline static int32_t get_offset_of_m_fShiZi_HouFang_Angle_26() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fShiZi_HouFang_Angle_26)); }
	inline float get_m_fShiZi_HouFang_Angle_26() const { return ___m_fShiZi_HouFang_Angle_26; }
	inline float* get_address_of_m_fShiZi_HouFang_Angle_26() { return &___m_fShiZi_HouFang_Angle_26; }
	inline void set_m_fShiZi_HouFang_Angle_26(float value)
	{
		___m_fShiZi_HouFang_Angle_26 = value;
	}

	inline static int32_t get_offset_of_m_fSegDis_Moving_27() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fSegDis_Moving_27)); }
	inline float get_m_fSegDis_Moving_27() const { return ___m_fSegDis_Moving_27; }
	inline float* get_address_of_m_fSegDis_Moving_27() { return &___m_fSegDis_Moving_27; }
	inline void set_m_fSegDis_Moving_27(float value)
	{
		___m_fSegDis_Moving_27 = value;
	}

	inline static int32_t get_offset_of_m_vecSHiZi_ZuoCe_Dir_28() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecSHiZi_ZuoCe_Dir_28)); }
	inline Vector2_t2156229523  get_m_vecSHiZi_ZuoCe_Dir_28() const { return ___m_vecSHiZi_ZuoCe_Dir_28; }
	inline Vector2_t2156229523 * get_address_of_m_vecSHiZi_ZuoCe_Dir_28() { return &___m_vecSHiZi_ZuoCe_Dir_28; }
	inline void set_m_vecSHiZi_ZuoCe_Dir_28(Vector2_t2156229523  value)
	{
		___m_vecSHiZi_ZuoCe_Dir_28 = value;
	}

	inline static int32_t get_offset_of_m_vecSHiZi_YouCe_Dir_29() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecSHiZi_YouCe_Dir_29)); }
	inline Vector2_t2156229523  get_m_vecSHiZi_YouCe_Dir_29() const { return ___m_vecSHiZi_YouCe_Dir_29; }
	inline Vector2_t2156229523 * get_address_of_m_vecSHiZi_YouCe_Dir_29() { return &___m_vecSHiZi_YouCe_Dir_29; }
	inline void set_m_vecSHiZi_YouCe_Dir_29(Vector2_t2156229523  value)
	{
		___m_vecSHiZi_YouCe_Dir_29 = value;
	}

	inline static int32_t get_offset_of_m_vecSHiZi_HouFang_Dir_30() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecSHiZi_HouFang_Dir_30)); }
	inline Vector2_t2156229523  get_m_vecSHiZi_HouFang_Dir_30() const { return ___m_vecSHiZi_HouFang_Dir_30; }
	inline Vector2_t2156229523 * get_address_of_m_vecSHiZi_HouFang_Dir_30() { return &___m_vecSHiZi_HouFang_Dir_30; }
	inline void set_m_vecSHiZi_HouFang_Dir_30(Vector2_t2156229523  value)
	{
		___m_vecSHiZi_HouFang_Dir_30 = value;
	}

	inline static int32_t get_offset_of_m_nShiZiStatus_31() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nShiZiStatus_31)); }
	inline int32_t get_m_nShiZiStatus_31() const { return ___m_nShiZiStatus_31; }
	inline int32_t* get_address_of_m_nShiZiStatus_31() { return &___m_nShiZiStatus_31; }
	inline void set_m_nShiZiStatus_31(int32_t value)
	{
		___m_nShiZiStatus_31 = value;
	}

	inline static int32_t get_offset_of_m_nShiZiStatusNumCount_32() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nShiZiStatusNumCount_32)); }
	inline int32_t get_m_nShiZiStatusNumCount_32() const { return ___m_nShiZiStatusNumCount_32; }
	inline int32_t* get_address_of_m_nShiZiStatusNumCount_32() { return &___m_nShiZiStatusNumCount_32; }
	inline void set_m_nShiZiStatusNumCount_32(int32_t value)
	{
		___m_nShiZiStatusNumCount_32 = value;
	}

	inline static int32_t get_offset_of_m_nZuoNumCount_33() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nZuoNumCount_33)); }
	inline int32_t get_m_nZuoNumCount_33() const { return ___m_nZuoNumCount_33; }
	inline int32_t* get_address_of_m_nZuoNumCount_33() { return &___m_nZuoNumCount_33; }
	inline void set_m_nZuoNumCount_33(int32_t value)
	{
		___m_nZuoNumCount_33 = value;
	}

	inline static int32_t get_offset_of_m_nYouNumCount_34() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nYouNumCount_34)); }
	inline int32_t get_m_nYouNumCount_34() const { return ___m_nYouNumCount_34; }
	inline int32_t* get_address_of_m_nYouNumCount_34() { return &___m_nYouNumCount_34; }
	inline void set_m_nYouNumCount_34(int32_t value)
	{
		___m_nYouNumCount_34 = value;
	}

	inline static int32_t get_offset_of_m_vecSanJiao_ZuoCe_Dir_38() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecSanJiao_ZuoCe_Dir_38)); }
	inline Vector2_t2156229523  get_m_vecSanJiao_ZuoCe_Dir_38() const { return ___m_vecSanJiao_ZuoCe_Dir_38; }
	inline Vector2_t2156229523 * get_address_of_m_vecSanJiao_ZuoCe_Dir_38() { return &___m_vecSanJiao_ZuoCe_Dir_38; }
	inline void set_m_vecSanJiao_ZuoCe_Dir_38(Vector2_t2156229523  value)
	{
		___m_vecSanJiao_ZuoCe_Dir_38 = value;
	}

	inline static int32_t get_offset_of_m_vecSanJiao_YouCe_Dir_39() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_vecSanJiao_YouCe_Dir_39)); }
	inline Vector2_t2156229523  get_m_vecSanJiao_YouCe_Dir_39() const { return ___m_vecSanJiao_YouCe_Dir_39; }
	inline Vector2_t2156229523 * get_address_of_m_vecSanJiao_YouCe_Dir_39() { return &___m_vecSanJiao_YouCe_Dir_39; }
	inline void set_m_vecSanJiao_YouCe_Dir_39(Vector2_t2156229523  value)
	{
		___m_vecSanJiao_YouCe_Dir_39 = value;
	}

	inline static int32_t get_offset_of_m_nSanJiaoStatus_40() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nSanJiaoStatus_40)); }
	inline int32_t get_m_nSanJiaoStatus_40() const { return ___m_nSanJiaoStatus_40; }
	inline int32_t* get_address_of_m_nSanJiaoStatus_40() { return &___m_nSanJiaoStatus_40; }
	inline void set_m_nSanJiaoStatus_40(int32_t value)
	{
		___m_nSanJiaoStatus_40 = value;
	}

	inline static int32_t get_offset_of_m_nSanJiaoStatusNumCount_41() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nSanJiaoStatusNumCount_41)); }
	inline int32_t get_m_nSanJiaoStatusNumCount_41() const { return ___m_nSanJiaoStatusNumCount_41; }
	inline int32_t* get_address_of_m_nSanJiaoStatusNumCount_41() { return &___m_nSanJiaoStatusNumCount_41; }
	inline void set_m_nSanJiaoStatusNumCount_41(int32_t value)
	{
		___m_nSanJiaoStatusNumCount_41 = value;
	}

	inline static int32_t get_offset_of_m_nSanJiaoZuoNumCount_42() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nSanJiaoZuoNumCount_42)); }
	inline int32_t get_m_nSanJiaoZuoNumCount_42() const { return ___m_nSanJiaoZuoNumCount_42; }
	inline int32_t* get_address_of_m_nSanJiaoZuoNumCount_42() { return &___m_nSanJiaoZuoNumCount_42; }
	inline void set_m_nSanJiaoZuoNumCount_42(int32_t value)
	{
		___m_nSanJiaoZuoNumCount_42 = value;
	}

	inline static int32_t get_offset_of_m_nSanJiaoYouNumCount_43() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nSanJiaoYouNumCount_43)); }
	inline int32_t get_m_nSanJiaoYouNumCount_43() const { return ___m_nSanJiaoYouNumCount_43; }
	inline int32_t* get_address_of_m_nSanJiaoYouNumCount_43() { return &___m_nSanJiaoYouNumCount_43; }
	inline void set_m_nSanJiaoYouNumCount_43(int32_t value)
	{
		___m_nSanJiaoYouNumCount_43 = value;
	}

	inline static int32_t get_offset_of_m_nSanJiao_BallNum_PerDir_Moving_46() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nSanJiao_BallNum_PerDir_Moving_46)); }
	inline int32_t get_m_nSanJiao_BallNum_PerDir_Moving_46() const { return ___m_nSanJiao_BallNum_PerDir_Moving_46; }
	inline int32_t* get_address_of_m_nSanJiao_BallNum_PerDir_Moving_46() { return &___m_nSanJiao_BallNum_PerDir_Moving_46; }
	inline void set_m_nSanJiao_BallNum_PerDir_Moving_46(int32_t value)
	{
		___m_nSanJiao_BallNum_PerDir_Moving_46 = value;
	}

	inline static int32_t get_offset_of_m_fSanJiao_SegDis_Moving_47() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fSanJiao_SegDis_Moving_47)); }
	inline float get_m_fSanJiao_SegDis_Moving_47() const { return ___m_fSanJiao_SegDis_Moving_47; }
	inline float* get_address_of_m_fSanJiao_SegDis_Moving_47() { return &___m_fSanJiao_SegDis_Moving_47; }
	inline void set_m_fSanJiao_SegDis_Moving_47(float value)
	{
		___m_fSanJiao_SegDis_Moving_47 = value;
	}

	inline static int32_t get_offset_of_m_nCurChildNum_48() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nCurChildNum_48)); }
	inline int32_t get_m_nCurChildNum_48() const { return ___m_nCurChildNum_48; }
	inline int32_t* get_address_of_m_nCurChildNum_48() { return &___m_nCurChildNum_48; }
	inline void set_m_nCurChildNum_48(int32_t value)
	{
		___m_nCurChildNum_48 = value;
	}

	inline static int32_t get_offset_of_m_nExplodeRound_49() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_nExplodeRound_49)); }
	inline int32_t get_m_nExplodeRound_49() const { return ___m_nExplodeRound_49; }
	inline int32_t* get_address_of_m_nExplodeRound_49() { return &___m_nExplodeRound_49; }
	inline void set_m_nExplodeRound_49(int32_t value)
	{
		___m_nExplodeRound_49 = value;
	}

	inline static int32_t get_offset_of_m_fSegDis_50() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fSegDis_50)); }
	inline float get_m_fSegDis_50() const { return ___m_fSegDis_50; }
	inline float* get_address_of_m_fSegDis_50() { return &___m_fSegDis_50; }
	inline void set_m_fSegDis_50(float value)
	{
		___m_fSegDis_50 = value;
	}

	inline static int32_t get_offset_of_m_lstTempEjectEndPos_55() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_lstTempEjectEndPos_55)); }
	inline List_1_t899420910 * get_m_lstTempEjectEndPos_55() const { return ___m_lstTempEjectEndPos_55; }
	inline List_1_t899420910 ** get_address_of_m_lstTempEjectEndPos_55() { return &___m_lstTempEjectEndPos_55; }
	inline void set_m_lstTempEjectEndPos_55(List_1_t899420910 * value)
	{
		___m_lstTempEjectEndPos_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTempEjectEndPos_55), value);
	}

	inline static int32_t get_offset_of_m_fWaitingTimeElapse_56() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667, ___m_fWaitingTimeElapse_56)); }
	inline float get_m_fWaitingTimeElapse_56() const { return ___m_fWaitingTimeElapse_56; }
	inline float* get_address_of_m_fWaitingTimeElapse_56() { return &___m_fWaitingTimeElapse_56; }
	inline void set_m_fWaitingTimeElapse_56(float value)
	{
		___m_fWaitingTimeElapse_56 = value;
	}
};

struct CExplodeNode_t97844667_StaticFields
{
public:
	// UnityEngine.Vector2 CExplodeNode::vec2Temp
	Vector2_t2156229523  ___vec2Temp_52;
	// UnityEngine.Vector3 CExplodeNode::vecTempPos
	Vector3_t3722313464  ___vecTempPos_53;
	// UnityEngine.Vector3 CExplodeNode::vecTempDir
	Vector3_t3722313464  ___vecTempDir_54;

public:
	inline static int32_t get_offset_of_vec2Temp_52() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667_StaticFields, ___vec2Temp_52)); }
	inline Vector2_t2156229523  get_vec2Temp_52() const { return ___vec2Temp_52; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_52() { return &___vec2Temp_52; }
	inline void set_vec2Temp_52(Vector2_t2156229523  value)
	{
		___vec2Temp_52 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_53() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667_StaticFields, ___vecTempPos_53)); }
	inline Vector3_t3722313464  get_vecTempPos_53() const { return ___vecTempPos_53; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_53() { return &___vecTempPos_53; }
	inline void set_vecTempPos_53(Vector3_t3722313464  value)
	{
		___vecTempPos_53 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_54() { return static_cast<int32_t>(offsetof(CExplodeNode_t97844667_StaticFields, ___vecTempDir_54)); }
	inline Vector3_t3722313464  get_vecTempDir_54() const { return ___vecTempDir_54; }
	inline Vector3_t3722313464 * get_address_of_vecTempDir_54() { return &___vecTempDir_54; }
	inline void set_vecTempDir_54(Vector3_t3722313464  value)
	{
		___vecTempDir_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CEXPLODENODE_T97844667_H
#ifndef CGUNSIGHT_T2209988408_H
#define CGUNSIGHT_T2209988408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGunsight
struct  CGunsight_t2209988408  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CGunsight::_dickJoint
	GameObject_t1113636619 * ____dickJoint_5;
	// UnityEngine.GameObject CGunsight::_dickStick
	GameObject_t1113636619 * ____dickStick_6;
	// UnityEngine.GameObject CGunsight::_circle
	GameObject_t1113636619 * ____circle_7;

public:
	inline static int32_t get_offset_of__dickJoint_5() { return static_cast<int32_t>(offsetof(CGunsight_t2209988408, ____dickJoint_5)); }
	inline GameObject_t1113636619 * get__dickJoint_5() const { return ____dickJoint_5; }
	inline GameObject_t1113636619 ** get_address_of__dickJoint_5() { return &____dickJoint_5; }
	inline void set__dickJoint_5(GameObject_t1113636619 * value)
	{
		____dickJoint_5 = value;
		Il2CppCodeGenWriteBarrier((&____dickJoint_5), value);
	}

	inline static int32_t get_offset_of__dickStick_6() { return static_cast<int32_t>(offsetof(CGunsight_t2209988408, ____dickStick_6)); }
	inline GameObject_t1113636619 * get__dickStick_6() const { return ____dickStick_6; }
	inline GameObject_t1113636619 ** get_address_of__dickStick_6() { return &____dickStick_6; }
	inline void set__dickStick_6(GameObject_t1113636619 * value)
	{
		____dickStick_6 = value;
		Il2CppCodeGenWriteBarrier((&____dickStick_6), value);
	}

	inline static int32_t get_offset_of__circle_7() { return static_cast<int32_t>(offsetof(CGunsight_t2209988408, ____circle_7)); }
	inline GameObject_t1113636619 * get__circle_7() const { return ____circle_7; }
	inline GameObject_t1113636619 ** get_address_of__circle_7() { return &____circle_7; }
	inline void set__circle_7(GameObject_t1113636619 * value)
	{
		____circle_7 = value;
		Il2CppCodeGenWriteBarrier((&____circle_7), value);
	}
};

struct CGunsight_t2209988408_StaticFields
{
public:
	// UnityEngine.Vector3 CGunsight::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;
	// UnityEngine.Vector3 CGunsight::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector2 CGunsight::vecTempDir
	Vector2_t2156229523  ___vecTempDir_4;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CGunsight_t2209988408_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CGunsight_t2209988408_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_4() { return static_cast<int32_t>(offsetof(CGunsight_t2209988408_StaticFields, ___vecTempDir_4)); }
	inline Vector2_t2156229523  get_vecTempDir_4() const { return ___vecTempDir_4; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_4() { return &___vecTempDir_4; }
	inline void set_vecTempDir_4(Vector2_t2156229523  value)
	{
		___vecTempDir_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGUNSIGHT_T2209988408_H
#ifndef CCAMERAMANAGER_T1888214120_H
#define CCAMERAMANAGER_T1888214120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCameraManager
struct  CCameraManager_t1888214120  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera CCameraManager::m_camWave
	Camera_t4157153871 * ___m_camWave_3;
	// UnityEngine.UI.InputField CCameraManager::_inputTimeBeforeUp
	InputField_t3762917431 * ____inputTimeBeforeUp_4;
	// UnityEngine.UI.InputField CCameraManager::_inputUpTime
	InputField_t3762917431 * ____inputUpTime_5;
	// UnityEngine.UI.InputField CCameraManager::_inputObserverTime
	InputField_t3762917431 * ____inputObserverTime_6;
	// UnityEngine.UI.InputField CCameraManager::_inputDownTime
	InputField_t3762917431 * ____inputDownTime_7;
	// UnityEngine.UI.InputField CCameraManager::_inputRebornSpotScale
	InputField_t3762917431 * ____inputRebornSpotScale_8;
	// UnityEngine.UI.InputField CCameraManager::_inputRebornSpotBlingSpeed
	InputField_t3762917431 * ____inputRebornSpotBlingSpeed_9;
	// System.Single CCameraManager::m_fRebornCamerSize
	float ___m_fRebornCamerSize_15;
	// System.Single CCameraManager::m_fUpTime
	float ___m_fUpTime_16;
	// System.Single CCameraManager::m_fDownTime
	float ___m_fDownTime_17;
	// System.Single CCameraManager::m_fDeadWatchTime
	float ___m_fDeadWatchTime_18;
	// System.Single CCameraManager::m_fTimeBeforeUp
	float ___m_fTimeBeforeUp_19;
	// System.Single CCameraManager::m_fRebornSpotScale
	float ___m_fRebornSpotScale_20;
	// System.Single CCameraManager::m_fRebornSpotBlingSpeed
	float ___m_fRebornSpotBlingSpeed_21;
	// System.Single CCameraManager::m_fUpSizeSpeed
	float ___m_fUpSizeSpeed_22;
	// System.Single CCameraManager::m_fDownSizeSpeed
	float ___m_fDownSizeSpeed_23;
	// UnityEngine.Vector3 CCameraManager::m_vecUpPosSpeed
	Vector3_t3722313464  ___m_vecUpPosSpeed_24;
	// UnityEngine.Vector3 CCameraManager::m_vecDownPosSpeed
	Vector3_t3722313464  ___m_vecDownPosSpeed_25;
	// System.Int32 CCameraManager::m_nStatus
	int32_t ___m_nStatus_26;
	// ETCJoystick CCameraManager::_etc
	ETCJoystick_t3250784002 * ____etc_27;
	// CRebornSpot CCameraManager::_RebornSpot
	CRebornSpot_t259671536 * ____RebornSpot_28;
	// System.Single CCameraManager::m_fScale
	float ___m_fScale_29;
	// UnityEngine.GameObject CCameraManager::_uiAll
	GameObject_t1113636619 * ____uiAll_30;
	// System.Single CCameraManager::m_fHidePlayerNameThreshold
	float ___m_fHidePlayerNameThreshold_31;
	// System.Single CCameraManager::m_fMaxCamSize
	float ___m_fMaxCamSize_32;
	// System.Boolean CCameraManager::m_bMaxCamLimit
	bool ___m_bMaxCamLimit_33;
	// System.Single CCameraManager::m_fPlayerNameVisibleXiShu
	float ___m_fPlayerNameVisibleXiShu_34;
	// UnityEngine.Vector3 CCameraManager::m_vecDownDetPos
	Vector3_t3722313464  ___m_vecDownDetPos_35;
	// System.Single CCameraManager::m_fDownAccelerate
	float ___m_fDownAccelerate_36;
	// System.Single CCameraManager::m_fDownV0
	float ___m_fDownV0_37;
	// System.Single CCameraManager::m_fShitWaitTimeCount
	float ___m_fShitWaitTimeCount_38;
	// UnityEngine.Vector3 CCameraManager::m_vecWorldCoorViewLeftBottom
	Vector3_t3722313464  ___m_vecWorldCoorViewLeftBottom_39;
	// UnityEngine.Vector3 CCameraManager::m_vecWorldCoorViewRightTop
	Vector3_t3722313464  ___m_vecWorldCoorViewRightTop_40;
	// UnityEngine.LineRenderer CCameraManager::_lrShitBar
	LineRenderer_t3154350270 * ____lrShitBar_41;
	// System.Single CCameraManager::m_fViewBorderDistance
	float ___m_fViewBorderDistance_42;
	// System.Single CCameraManager::m_fLastCamSize
	float ___m_fLastCamSize_43;
	// System.Single CCameraManager::m_fWaveCamSpeed
	float ___m_fWaveCamSpeed_44;

public:
	inline static int32_t get_offset_of_m_camWave_3() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_camWave_3)); }
	inline Camera_t4157153871 * get_m_camWave_3() const { return ___m_camWave_3; }
	inline Camera_t4157153871 ** get_address_of_m_camWave_3() { return &___m_camWave_3; }
	inline void set_m_camWave_3(Camera_t4157153871 * value)
	{
		___m_camWave_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_camWave_3), value);
	}

	inline static int32_t get_offset_of__inputTimeBeforeUp_4() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____inputTimeBeforeUp_4)); }
	inline InputField_t3762917431 * get__inputTimeBeforeUp_4() const { return ____inputTimeBeforeUp_4; }
	inline InputField_t3762917431 ** get_address_of__inputTimeBeforeUp_4() { return &____inputTimeBeforeUp_4; }
	inline void set__inputTimeBeforeUp_4(InputField_t3762917431 * value)
	{
		____inputTimeBeforeUp_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputTimeBeforeUp_4), value);
	}

	inline static int32_t get_offset_of__inputUpTime_5() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____inputUpTime_5)); }
	inline InputField_t3762917431 * get__inputUpTime_5() const { return ____inputUpTime_5; }
	inline InputField_t3762917431 ** get_address_of__inputUpTime_5() { return &____inputUpTime_5; }
	inline void set__inputUpTime_5(InputField_t3762917431 * value)
	{
		____inputUpTime_5 = value;
		Il2CppCodeGenWriteBarrier((&____inputUpTime_5), value);
	}

	inline static int32_t get_offset_of__inputObserverTime_6() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____inputObserverTime_6)); }
	inline InputField_t3762917431 * get__inputObserverTime_6() const { return ____inputObserverTime_6; }
	inline InputField_t3762917431 ** get_address_of__inputObserverTime_6() { return &____inputObserverTime_6; }
	inline void set__inputObserverTime_6(InputField_t3762917431 * value)
	{
		____inputObserverTime_6 = value;
		Il2CppCodeGenWriteBarrier((&____inputObserverTime_6), value);
	}

	inline static int32_t get_offset_of__inputDownTime_7() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____inputDownTime_7)); }
	inline InputField_t3762917431 * get__inputDownTime_7() const { return ____inputDownTime_7; }
	inline InputField_t3762917431 ** get_address_of__inputDownTime_7() { return &____inputDownTime_7; }
	inline void set__inputDownTime_7(InputField_t3762917431 * value)
	{
		____inputDownTime_7 = value;
		Il2CppCodeGenWriteBarrier((&____inputDownTime_7), value);
	}

	inline static int32_t get_offset_of__inputRebornSpotScale_8() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____inputRebornSpotScale_8)); }
	inline InputField_t3762917431 * get__inputRebornSpotScale_8() const { return ____inputRebornSpotScale_8; }
	inline InputField_t3762917431 ** get_address_of__inputRebornSpotScale_8() { return &____inputRebornSpotScale_8; }
	inline void set__inputRebornSpotScale_8(InputField_t3762917431 * value)
	{
		____inputRebornSpotScale_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputRebornSpotScale_8), value);
	}

	inline static int32_t get_offset_of__inputRebornSpotBlingSpeed_9() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____inputRebornSpotBlingSpeed_9)); }
	inline InputField_t3762917431 * get__inputRebornSpotBlingSpeed_9() const { return ____inputRebornSpotBlingSpeed_9; }
	inline InputField_t3762917431 ** get_address_of__inputRebornSpotBlingSpeed_9() { return &____inputRebornSpotBlingSpeed_9; }
	inline void set__inputRebornSpotBlingSpeed_9(InputField_t3762917431 * value)
	{
		____inputRebornSpotBlingSpeed_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputRebornSpotBlingSpeed_9), value);
	}

	inline static int32_t get_offset_of_m_fRebornCamerSize_15() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fRebornCamerSize_15)); }
	inline float get_m_fRebornCamerSize_15() const { return ___m_fRebornCamerSize_15; }
	inline float* get_address_of_m_fRebornCamerSize_15() { return &___m_fRebornCamerSize_15; }
	inline void set_m_fRebornCamerSize_15(float value)
	{
		___m_fRebornCamerSize_15 = value;
	}

	inline static int32_t get_offset_of_m_fUpTime_16() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fUpTime_16)); }
	inline float get_m_fUpTime_16() const { return ___m_fUpTime_16; }
	inline float* get_address_of_m_fUpTime_16() { return &___m_fUpTime_16; }
	inline void set_m_fUpTime_16(float value)
	{
		___m_fUpTime_16 = value;
	}

	inline static int32_t get_offset_of_m_fDownTime_17() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fDownTime_17)); }
	inline float get_m_fDownTime_17() const { return ___m_fDownTime_17; }
	inline float* get_address_of_m_fDownTime_17() { return &___m_fDownTime_17; }
	inline void set_m_fDownTime_17(float value)
	{
		___m_fDownTime_17 = value;
	}

	inline static int32_t get_offset_of_m_fDeadWatchTime_18() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fDeadWatchTime_18)); }
	inline float get_m_fDeadWatchTime_18() const { return ___m_fDeadWatchTime_18; }
	inline float* get_address_of_m_fDeadWatchTime_18() { return &___m_fDeadWatchTime_18; }
	inline void set_m_fDeadWatchTime_18(float value)
	{
		___m_fDeadWatchTime_18 = value;
	}

	inline static int32_t get_offset_of_m_fTimeBeforeUp_19() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fTimeBeforeUp_19)); }
	inline float get_m_fTimeBeforeUp_19() const { return ___m_fTimeBeforeUp_19; }
	inline float* get_address_of_m_fTimeBeforeUp_19() { return &___m_fTimeBeforeUp_19; }
	inline void set_m_fTimeBeforeUp_19(float value)
	{
		___m_fTimeBeforeUp_19 = value;
	}

	inline static int32_t get_offset_of_m_fRebornSpotScale_20() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fRebornSpotScale_20)); }
	inline float get_m_fRebornSpotScale_20() const { return ___m_fRebornSpotScale_20; }
	inline float* get_address_of_m_fRebornSpotScale_20() { return &___m_fRebornSpotScale_20; }
	inline void set_m_fRebornSpotScale_20(float value)
	{
		___m_fRebornSpotScale_20 = value;
	}

	inline static int32_t get_offset_of_m_fRebornSpotBlingSpeed_21() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fRebornSpotBlingSpeed_21)); }
	inline float get_m_fRebornSpotBlingSpeed_21() const { return ___m_fRebornSpotBlingSpeed_21; }
	inline float* get_address_of_m_fRebornSpotBlingSpeed_21() { return &___m_fRebornSpotBlingSpeed_21; }
	inline void set_m_fRebornSpotBlingSpeed_21(float value)
	{
		___m_fRebornSpotBlingSpeed_21 = value;
	}

	inline static int32_t get_offset_of_m_fUpSizeSpeed_22() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fUpSizeSpeed_22)); }
	inline float get_m_fUpSizeSpeed_22() const { return ___m_fUpSizeSpeed_22; }
	inline float* get_address_of_m_fUpSizeSpeed_22() { return &___m_fUpSizeSpeed_22; }
	inline void set_m_fUpSizeSpeed_22(float value)
	{
		___m_fUpSizeSpeed_22 = value;
	}

	inline static int32_t get_offset_of_m_fDownSizeSpeed_23() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fDownSizeSpeed_23)); }
	inline float get_m_fDownSizeSpeed_23() const { return ___m_fDownSizeSpeed_23; }
	inline float* get_address_of_m_fDownSizeSpeed_23() { return &___m_fDownSizeSpeed_23; }
	inline void set_m_fDownSizeSpeed_23(float value)
	{
		___m_fDownSizeSpeed_23 = value;
	}

	inline static int32_t get_offset_of_m_vecUpPosSpeed_24() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_vecUpPosSpeed_24)); }
	inline Vector3_t3722313464  get_m_vecUpPosSpeed_24() const { return ___m_vecUpPosSpeed_24; }
	inline Vector3_t3722313464 * get_address_of_m_vecUpPosSpeed_24() { return &___m_vecUpPosSpeed_24; }
	inline void set_m_vecUpPosSpeed_24(Vector3_t3722313464  value)
	{
		___m_vecUpPosSpeed_24 = value;
	}

	inline static int32_t get_offset_of_m_vecDownPosSpeed_25() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_vecDownPosSpeed_25)); }
	inline Vector3_t3722313464  get_m_vecDownPosSpeed_25() const { return ___m_vecDownPosSpeed_25; }
	inline Vector3_t3722313464 * get_address_of_m_vecDownPosSpeed_25() { return &___m_vecDownPosSpeed_25; }
	inline void set_m_vecDownPosSpeed_25(Vector3_t3722313464  value)
	{
		___m_vecDownPosSpeed_25 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_26() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_nStatus_26)); }
	inline int32_t get_m_nStatus_26() const { return ___m_nStatus_26; }
	inline int32_t* get_address_of_m_nStatus_26() { return &___m_nStatus_26; }
	inline void set_m_nStatus_26(int32_t value)
	{
		___m_nStatus_26 = value;
	}

	inline static int32_t get_offset_of__etc_27() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____etc_27)); }
	inline ETCJoystick_t3250784002 * get__etc_27() const { return ____etc_27; }
	inline ETCJoystick_t3250784002 ** get_address_of__etc_27() { return &____etc_27; }
	inline void set__etc_27(ETCJoystick_t3250784002 * value)
	{
		____etc_27 = value;
		Il2CppCodeGenWriteBarrier((&____etc_27), value);
	}

	inline static int32_t get_offset_of__RebornSpot_28() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____RebornSpot_28)); }
	inline CRebornSpot_t259671536 * get__RebornSpot_28() const { return ____RebornSpot_28; }
	inline CRebornSpot_t259671536 ** get_address_of__RebornSpot_28() { return &____RebornSpot_28; }
	inline void set__RebornSpot_28(CRebornSpot_t259671536 * value)
	{
		____RebornSpot_28 = value;
		Il2CppCodeGenWriteBarrier((&____RebornSpot_28), value);
	}

	inline static int32_t get_offset_of_m_fScale_29() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fScale_29)); }
	inline float get_m_fScale_29() const { return ___m_fScale_29; }
	inline float* get_address_of_m_fScale_29() { return &___m_fScale_29; }
	inline void set_m_fScale_29(float value)
	{
		___m_fScale_29 = value;
	}

	inline static int32_t get_offset_of__uiAll_30() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____uiAll_30)); }
	inline GameObject_t1113636619 * get__uiAll_30() const { return ____uiAll_30; }
	inline GameObject_t1113636619 ** get_address_of__uiAll_30() { return &____uiAll_30; }
	inline void set__uiAll_30(GameObject_t1113636619 * value)
	{
		____uiAll_30 = value;
		Il2CppCodeGenWriteBarrier((&____uiAll_30), value);
	}

	inline static int32_t get_offset_of_m_fHidePlayerNameThreshold_31() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fHidePlayerNameThreshold_31)); }
	inline float get_m_fHidePlayerNameThreshold_31() const { return ___m_fHidePlayerNameThreshold_31; }
	inline float* get_address_of_m_fHidePlayerNameThreshold_31() { return &___m_fHidePlayerNameThreshold_31; }
	inline void set_m_fHidePlayerNameThreshold_31(float value)
	{
		___m_fHidePlayerNameThreshold_31 = value;
	}

	inline static int32_t get_offset_of_m_fMaxCamSize_32() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fMaxCamSize_32)); }
	inline float get_m_fMaxCamSize_32() const { return ___m_fMaxCamSize_32; }
	inline float* get_address_of_m_fMaxCamSize_32() { return &___m_fMaxCamSize_32; }
	inline void set_m_fMaxCamSize_32(float value)
	{
		___m_fMaxCamSize_32 = value;
	}

	inline static int32_t get_offset_of_m_bMaxCamLimit_33() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_bMaxCamLimit_33)); }
	inline bool get_m_bMaxCamLimit_33() const { return ___m_bMaxCamLimit_33; }
	inline bool* get_address_of_m_bMaxCamLimit_33() { return &___m_bMaxCamLimit_33; }
	inline void set_m_bMaxCamLimit_33(bool value)
	{
		___m_bMaxCamLimit_33 = value;
	}

	inline static int32_t get_offset_of_m_fPlayerNameVisibleXiShu_34() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fPlayerNameVisibleXiShu_34)); }
	inline float get_m_fPlayerNameVisibleXiShu_34() const { return ___m_fPlayerNameVisibleXiShu_34; }
	inline float* get_address_of_m_fPlayerNameVisibleXiShu_34() { return &___m_fPlayerNameVisibleXiShu_34; }
	inline void set_m_fPlayerNameVisibleXiShu_34(float value)
	{
		___m_fPlayerNameVisibleXiShu_34 = value;
	}

	inline static int32_t get_offset_of_m_vecDownDetPos_35() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_vecDownDetPos_35)); }
	inline Vector3_t3722313464  get_m_vecDownDetPos_35() const { return ___m_vecDownDetPos_35; }
	inline Vector3_t3722313464 * get_address_of_m_vecDownDetPos_35() { return &___m_vecDownDetPos_35; }
	inline void set_m_vecDownDetPos_35(Vector3_t3722313464  value)
	{
		___m_vecDownDetPos_35 = value;
	}

	inline static int32_t get_offset_of_m_fDownAccelerate_36() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fDownAccelerate_36)); }
	inline float get_m_fDownAccelerate_36() const { return ___m_fDownAccelerate_36; }
	inline float* get_address_of_m_fDownAccelerate_36() { return &___m_fDownAccelerate_36; }
	inline void set_m_fDownAccelerate_36(float value)
	{
		___m_fDownAccelerate_36 = value;
	}

	inline static int32_t get_offset_of_m_fDownV0_37() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fDownV0_37)); }
	inline float get_m_fDownV0_37() const { return ___m_fDownV0_37; }
	inline float* get_address_of_m_fDownV0_37() { return &___m_fDownV0_37; }
	inline void set_m_fDownV0_37(float value)
	{
		___m_fDownV0_37 = value;
	}

	inline static int32_t get_offset_of_m_fShitWaitTimeCount_38() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fShitWaitTimeCount_38)); }
	inline float get_m_fShitWaitTimeCount_38() const { return ___m_fShitWaitTimeCount_38; }
	inline float* get_address_of_m_fShitWaitTimeCount_38() { return &___m_fShitWaitTimeCount_38; }
	inline void set_m_fShitWaitTimeCount_38(float value)
	{
		___m_fShitWaitTimeCount_38 = value;
	}

	inline static int32_t get_offset_of_m_vecWorldCoorViewLeftBottom_39() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_vecWorldCoorViewLeftBottom_39)); }
	inline Vector3_t3722313464  get_m_vecWorldCoorViewLeftBottom_39() const { return ___m_vecWorldCoorViewLeftBottom_39; }
	inline Vector3_t3722313464 * get_address_of_m_vecWorldCoorViewLeftBottom_39() { return &___m_vecWorldCoorViewLeftBottom_39; }
	inline void set_m_vecWorldCoorViewLeftBottom_39(Vector3_t3722313464  value)
	{
		___m_vecWorldCoorViewLeftBottom_39 = value;
	}

	inline static int32_t get_offset_of_m_vecWorldCoorViewRightTop_40() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_vecWorldCoorViewRightTop_40)); }
	inline Vector3_t3722313464  get_m_vecWorldCoorViewRightTop_40() const { return ___m_vecWorldCoorViewRightTop_40; }
	inline Vector3_t3722313464 * get_address_of_m_vecWorldCoorViewRightTop_40() { return &___m_vecWorldCoorViewRightTop_40; }
	inline void set_m_vecWorldCoorViewRightTop_40(Vector3_t3722313464  value)
	{
		___m_vecWorldCoorViewRightTop_40 = value;
	}

	inline static int32_t get_offset_of__lrShitBar_41() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ____lrShitBar_41)); }
	inline LineRenderer_t3154350270 * get__lrShitBar_41() const { return ____lrShitBar_41; }
	inline LineRenderer_t3154350270 ** get_address_of__lrShitBar_41() { return &____lrShitBar_41; }
	inline void set__lrShitBar_41(LineRenderer_t3154350270 * value)
	{
		____lrShitBar_41 = value;
		Il2CppCodeGenWriteBarrier((&____lrShitBar_41), value);
	}

	inline static int32_t get_offset_of_m_fViewBorderDistance_42() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fViewBorderDistance_42)); }
	inline float get_m_fViewBorderDistance_42() const { return ___m_fViewBorderDistance_42; }
	inline float* get_address_of_m_fViewBorderDistance_42() { return &___m_fViewBorderDistance_42; }
	inline void set_m_fViewBorderDistance_42(float value)
	{
		___m_fViewBorderDistance_42 = value;
	}

	inline static int32_t get_offset_of_m_fLastCamSize_43() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fLastCamSize_43)); }
	inline float get_m_fLastCamSize_43() const { return ___m_fLastCamSize_43; }
	inline float* get_address_of_m_fLastCamSize_43() { return &___m_fLastCamSize_43; }
	inline void set_m_fLastCamSize_43(float value)
	{
		___m_fLastCamSize_43 = value;
	}

	inline static int32_t get_offset_of_m_fWaveCamSpeed_44() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120, ___m_fWaveCamSpeed_44)); }
	inline float get_m_fWaveCamSpeed_44() const { return ___m_fWaveCamSpeed_44; }
	inline float* get_address_of_m_fWaveCamSpeed_44() { return &___m_fWaveCamSpeed_44; }
	inline void set_m_fWaveCamSpeed_44(float value)
	{
		___m_fWaveCamSpeed_44 = value;
	}
};

struct CCameraManager_t1888214120_StaticFields
{
public:
	// CCameraManager CCameraManager::s_Instance
	CCameraManager_t1888214120 * ___s_Instance_2;
	// UnityEngine.Vector3 CCameraManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_10;
	// UnityEngine.Vector3 CCameraManager::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_11;
	// UnityEngine.Vector3 CCameraManager::vecCamerCurPos
	Vector3_t3722313464  ___vecCamerCurPos_12;
	// UnityEngine.Vector3 CCameraManager::vecArenaLeftBottom_ScreenCorSys
	Vector3_t3722313464  ___vecArenaLeftBottom_ScreenCorSys_13;
	// UnityEngine.Vector3 CCameraManager::vecArenaRightTop_ScreenCorSys
	Vector3_t3722313464  ___vecArenaRightTop_ScreenCorSys_14;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120_StaticFields, ___s_Instance_2)); }
	inline CCameraManager_t1888214120 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CCameraManager_t1888214120 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CCameraManager_t1888214120 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_10() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120_StaticFields, ___vecTempPos_10)); }
	inline Vector3_t3722313464  get_vecTempPos_10() const { return ___vecTempPos_10; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_10() { return &___vecTempPos_10; }
	inline void set_vecTempPos_10(Vector3_t3722313464  value)
	{
		___vecTempPos_10 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_11() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120_StaticFields, ___vecTempPos2_11)); }
	inline Vector3_t3722313464  get_vecTempPos2_11() const { return ___vecTempPos2_11; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_11() { return &___vecTempPos2_11; }
	inline void set_vecTempPos2_11(Vector3_t3722313464  value)
	{
		___vecTempPos2_11 = value;
	}

	inline static int32_t get_offset_of_vecCamerCurPos_12() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120_StaticFields, ___vecCamerCurPos_12)); }
	inline Vector3_t3722313464  get_vecCamerCurPos_12() const { return ___vecCamerCurPos_12; }
	inline Vector3_t3722313464 * get_address_of_vecCamerCurPos_12() { return &___vecCamerCurPos_12; }
	inline void set_vecCamerCurPos_12(Vector3_t3722313464  value)
	{
		___vecCamerCurPos_12 = value;
	}

	inline static int32_t get_offset_of_vecArenaLeftBottom_ScreenCorSys_13() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120_StaticFields, ___vecArenaLeftBottom_ScreenCorSys_13)); }
	inline Vector3_t3722313464  get_vecArenaLeftBottom_ScreenCorSys_13() const { return ___vecArenaLeftBottom_ScreenCorSys_13; }
	inline Vector3_t3722313464 * get_address_of_vecArenaLeftBottom_ScreenCorSys_13() { return &___vecArenaLeftBottom_ScreenCorSys_13; }
	inline void set_vecArenaLeftBottom_ScreenCorSys_13(Vector3_t3722313464  value)
	{
		___vecArenaLeftBottom_ScreenCorSys_13 = value;
	}

	inline static int32_t get_offset_of_vecArenaRightTop_ScreenCorSys_14() { return static_cast<int32_t>(offsetof(CCameraManager_t1888214120_StaticFields, ___vecArenaRightTop_ScreenCorSys_14)); }
	inline Vector3_t3722313464  get_vecArenaRightTop_ScreenCorSys_14() const { return ___vecArenaRightTop_ScreenCorSys_14; }
	inline Vector3_t3722313464 * get_address_of_vecArenaRightTop_ScreenCorSys_14() { return &___vecArenaRightTop_ScreenCorSys_14; }
	inline void set_vecArenaRightTop_ScreenCorSys_14(Vector3_t3722313464  value)
	{
		___vecArenaRightTop_ScreenCorSys_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCAMERAMANAGER_T1888214120_H
#ifndef CCOLORMANAGER_T615473758_H
#define CCOLORMANAGER_T615473758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CColorManager
struct  CColorManager_t615473758  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Color> CColorManager::m_dicColors
	Dictionary_2_t2340942623 * ___m_dicColors_3;

public:
	inline static int32_t get_offset_of_m_dicColors_3() { return static_cast<int32_t>(offsetof(CColorManager_t615473758, ___m_dicColors_3)); }
	inline Dictionary_2_t2340942623 * get_m_dicColors_3() const { return ___m_dicColors_3; }
	inline Dictionary_2_t2340942623 ** get_address_of_m_dicColors_3() { return &___m_dicColors_3; }
	inline void set_m_dicColors_3(Dictionary_2_t2340942623 * value)
	{
		___m_dicColors_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicColors_3), value);
	}
};

struct CColorManager_t615473758_StaticFields
{
public:
	// CColorManager CColorManager::s_Instance
	CColorManager_t615473758 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CColorManager_t615473758_StaticFields, ___s_Instance_2)); }
	inline CColorManager_t615473758 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CColorManager_t615473758 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CColorManager_t615473758 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOLORMANAGER_T615473758_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef CRECONNECTMANAGER_T1285157373_H
#define CRECONNECTMANAGER_T1285157373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CReconnectManager
struct  CReconnectManager_t1285157373  : public PunBehaviour_t987309092
{
public:

public:
};

struct CReconnectManager_t1285157373_StaticFields
{
public:
	// CReconnectManager CReconnectManager::s_Instance
	CReconnectManager_t1285157373 * ___s_Instance_3;

public:
	inline static int32_t get_offset_of_s_Instance_3() { return static_cast<int32_t>(offsetof(CReconnectManager_t1285157373_StaticFields, ___s_Instance_3)); }
	inline CReconnectManager_t1285157373 * get_s_Instance_3() const { return ___s_Instance_3; }
	inline CReconnectManager_t1285157373 ** get_address_of_s_Instance_3() { return &___s_Instance_3; }
	inline void set_s_Instance_3(CReconnectManager_t1285157373 * value)
	{
		___s_Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRECONNECTMANAGER_T1285157373_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (ChargeChargeJSON_t4154268863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[1] = 
{
	ChargeChargeJSON_t4154268863::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (ReceiptValidateJSON_t2198994320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[1] = 
{
	ReceiptValidateJSON_t2198994320::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (Charge_t1653125646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3202[7] = 
{
	Charge_t1653125646::get_offset_of_authenticator_0(),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (U3CgetChargeListPostFormU3Ec__Iterator0_t185136653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[8] = 
{
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_U3CformU3E__0_0(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_phonenum_1(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_idortoken_2(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_U3CrequestU3E__0_3(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_callback_4(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_U24current_5(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_U24disposing_6(),
	U3CgetChargeListPostFormU3Ec__Iterator0_t185136653::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (U3CchargeGetPostFormU3Ec__Iterator1_t1522386906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[9] = 
{
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_U3CformU3E__0_0(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_phonenum_1(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_idortoken_2(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_passby_3(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_U3CrequestU3E__0_4(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_callback_5(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_U24current_6(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_U24disposing_7(),
	U3CchargeGetPostFormU3Ec__Iterator1_t1522386906::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (U3CchargeAddPostFormU3Ec__Iterator2_t2596936324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3205[10] = 
{
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_U3CformU3E__0_0(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_phonenum_1(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_idortoken_2(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_passby_3(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_U3CproductU3E__0_4(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_U3CrequestU3E__0_5(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_callback_6(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_U24current_7(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_U24disposing_8(),
	U3CchargeAddPostFormU3Ec__Iterator2_t2596936324::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (U3CchargeDelPostFormU3Ec__Iterator3_t612667134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3206[9] = 
{
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_U3CformU3E__0_0(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_phonenum_1(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_idortoken_2(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_passby_3(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_U3CrequestU3E__0_4(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_callback_5(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_U24current_6(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_U24disposing_7(),
	U3CchargeDelPostFormU3Ec__Iterator3_t612667134::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (U3CchargeChargePostFormU3Ec__Iterator4_t563494620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[10] = 
{
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_U3CformU3E__0_0(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_phonenum_1(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_idortoken_2(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_passby_3(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_U3CinfoU3E__0_4(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_U3CrequestU3E__0_5(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_callback_6(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_U24current_7(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_U24disposing_8(),
	U3CchargeChargePostFormU3Ec__Iterator4_t563494620::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3208[9] = 
{
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_U3CformU3E__0_0(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_phonenum_1(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_idortoken_2(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_passby_3(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_U3CrequestU3E__0_4(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_callback_5(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_U24current_6(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_U24disposing_7(),
	U3CreceiptValidatePostFormU3Ec__Iterator5_t1483565208::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (Constants_t701097383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (CReconnectManager_t1285157373), -1, sizeof(CReconnectManager_t1285157373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3210[1] = 
{
	CReconnectManager_t1285157373_StaticFields::get_offset_of_s_Instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (U3CLoadSceneU3Ec__Iterator0_t2627343872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3211[4] = 
{
	U3CLoadSceneU3Ec__Iterator0_t2627343872::get_offset_of_scene_name_0(),
	U3CLoadSceneU3Ec__Iterator0_t2627343872::get_offset_of_U24current_1(),
	U3CLoadSceneU3Ec__Iterator0_t2627343872::get_offset_of_U24disposing_2(),
	U3CLoadSceneU3Ec__Iterator0_t2627343872::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (main_t2227614042), -1, sizeof(main_t2227614042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3212[29] = 
{
	main_t2227614042::get_offset_of_registerPanelInputFieldPhoneNumber_2(),
	main_t2227614042::get_offset_of_registerPanelInputFieldSMSCode_3(),
	main_t2227614042::get_offset_of_registerPanelInputFieldPassword_4(),
	main_t2227614042::get_offset_of_registerPanelButtonRegister_5(),
	main_t2227614042::get_offset_of_registerPanelButtonGetSMSCode_6(),
	main_t2227614042::get_offset_of_passwordLoginPanelInputFieldPhoneNumber_7(),
	main_t2227614042::get_offset_of_passwordLoginPanelInputFieldPassword_8(),
	main_t2227614042::get_offset_of_passwordLoginPanelButtonLogin_9(),
	main_t2227614042::get_offset_of_resetPanelInputFieldPhoneNumber_10(),
	main_t2227614042::get_offset_of_resetPanelInputFieldSMSCode_11(),
	main_t2227614042::get_offset_of_resetPanelInputFieldPassword_12(),
	main_t2227614042::get_offset_of_resetPanelButtonReset_13(),
	main_t2227614042::get_offset_of_resetPanelButtonGetSMSCode_14(),
	main_t2227614042::get_offset_of_sessionVerifyPanelButtonVerify_15(),
	main_t2227614042::get_offset_of_sessionVerifyPanelButtonExecute_16(),
	main_t2227614042::get_offset_of_sessionVerifyPanelButtonLogout_17(),
	main_t2227614042::get_offset_of_authenticator_18(),
	main_t2227614042::get_offset_of_account_19(),
	main_t2227614042::get_offset_of_charge_20(),
	main_t2227614042::get_offset_of_market_21(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_23(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_24(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_25(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_26(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_27(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_28(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_29(),
	main_t2227614042_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (MarketProductJSON_t12495859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3213[12] = 
{
	MarketProductJSON_t12495859::get_offset_of_productguid_0(),
	MarketProductJSON_t12495859::get_offset_of_productname_1(),
	MarketProductJSON_t12495859::get_offset_of_producttype_2(),
	MarketProductJSON_t12495859::get_offset_of_forbidden_3(),
	MarketProductJSON_t12495859::get_offset_of_productdesc_4(),
	MarketProductJSON_t12495859::get_offset_of_productimage_5(),
	MarketProductJSON_t12495859::get_offset_of_costcoins_6(),
	MarketProductJSON_t12495859::get_offset_of_costdiamonds_7(),
	MarketProductJSON_t12495859::get_offset_of_productnumattr1_8(),
	MarketProductJSON_t12495859::get_offset_of_productnumattr2_9(),
	MarketProductJSON_t12495859::get_offset_of_producttxtattr1_10(),
	MarketProductJSON_t12495859::get_offset_of_producttxtattr2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (GetProductListJSON_t196412724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[2] = 
{
	GetProductListJSON_t196412724::get_offset_of_code_0(),
	GetProductListJSON_t196412724::get_offset_of_products_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (MarketGetJSON_t711360598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3215[2] = 
{
	MarketGetJSON_t711360598::get_offset_of_code_0(),
	MarketGetJSON_t711360598::get_offset_of_product_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (MarketAddJSON_t2861654417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[1] = 
{
	MarketAddJSON_t2861654417::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (MarketDelJSON_t1725136982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3217[1] = 
{
	MarketDelJSON_t1725136982::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (MarketSetJSON_t666009686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[1] = 
{
	MarketSetJSON_t666009686::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (ConsumeInfoJSON_t2550589647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3219[2] = 
{
	ConsumeInfoJSON_t2550589647::get_offset_of_productguid_0(),
	ConsumeInfoJSON_t2550589647::get_offset_of_productnum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (MarketConsumeJSON_t3254223683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3220[1] = 
{
	MarketConsumeJSON_t3254223683::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (BuyInfoJSON_t2640594623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3221[2] = 
{
	BuyInfoJSON_t2640594623::get_offset_of_productguid_0(),
	BuyInfoJSON_t2640594623::get_offset_of_productnum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (MarketBuyJSON_t505675942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3222[1] = 
{
	MarketBuyJSON_t505675942::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (MarketBuymentJSON_t3043372002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3223[2] = 
{
	MarketBuymentJSON_t3043372002::get_offset_of_productguid_0(),
	MarketBuymentJSON_t3043372002::get_offset_of_quantity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (GetBuymentListJSON_t3848378166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3224[2] = 
{
	GetBuymentListJSON_t3848378166::get_offset_of_code_0(),
	GetBuymentListJSON_t3848378166::get_offset_of_buyments_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (Market_t538055513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[9] = 
{
	Market_t538055513::get_offset_of_authenticator_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (U3CgetProductListPostFormU3Ec__Iterator0_t2123906673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[8] = 
{
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_U3CformU3E__0_0(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_phonenum_1(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_idortoken_2(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_U3CrequestU3E__0_3(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_callback_4(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_U24current_5(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_U24disposing_6(),
	U3CgetProductListPostFormU3Ec__Iterator0_t2123906673::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (U3CmarketGetPostFormU3Ec__Iterator1_t3287888726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[9] = 
{
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_U3CformU3E__0_0(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_phonenum_1(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_idortoken_2(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_passby_3(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_U3CrequestU3E__0_4(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_callback_5(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_U24current_6(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_U24disposing_7(),
	U3CmarketGetPostFormU3Ec__Iterator1_t3287888726::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (U3CmarketAddPostFormU3Ec__Iterator2_t927080642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[10] = 
{
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_U3CformU3E__0_0(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_phonenum_1(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_idortoken_2(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_passby_3(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_U3CproductU3E__0_4(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_U3CrequestU3E__0_5(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_callback_6(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_U24current_7(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_U24disposing_8(),
	U3CmarketAddPostFormU3Ec__Iterator2_t927080642::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (U3CmarketDelPostFormU3Ec__Iterator3_t3690002772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[9] = 
{
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_U3CformU3E__0_0(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_phonenum_1(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_idortoken_2(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_passby_3(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_U3CrequestU3E__0_4(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_callback_5(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_U24current_6(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_U24disposing_7(),
	U3CmarketDelPostFormU3Ec__Iterator3_t3690002772::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (U3CmarketSetPostFormU3Ec__Iterator4_t993933197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[10] = 
{
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_U3CformU3E__0_0(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_phonenum_1(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_idortoken_2(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_passby_3(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_U3CproductU3E__0_4(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_U3CrequestU3E__0_5(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_callback_6(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_U24current_7(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_U24disposing_8(),
	U3CmarketSetPostFormU3Ec__Iterator4_t993933197::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[10] = 
{
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_U3CformU3E__0_0(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_phonenum_1(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_idortoken_2(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_passby_3(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_U3CinfoU3E__0_4(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_U3CrequestU3E__0_5(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_callback_6(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_U24current_7(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_U24disposing_8(),
	U3CmarketConsumePostFormU3Ec__Iterator5_t4079474279::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[10] = 
{
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_U3CformU3E__0_0(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_phonenum_1(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_idortoken_2(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_passby_3(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_U3CinfoU3E__0_4(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_U3CrequestU3E__0_5(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_callback_6(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_U24current_7(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_U24disposing_8(),
	U3CmarketBuyPostFormU3Ec__Iterator6_t2827113427::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3233[9] = 
{
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_U3CformU3E__0_0(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_phonenum_1(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_idortoken_2(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_passby_3(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_U3CrequestU3E__0_4(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_callback_5(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_U24current_6(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_U24disposing_7(),
	U3CgetBuymentListPostFormU3Ec__Iterator7_t4024730788::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (ResourceType_t3261224423)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3234[5] = 
{
	ResourceType_t3261224423::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (Resource_t131882869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3235[5] = 
{
	Resource_t131882869::get_offset_of_name_0(),
	Resource_t131882869::get_offset_of_url_1(),
	Resource_t131882869::get_offset_of_type_2(),
	Resource_t131882869::get_offset_of_text_3(),
	Resource_t131882869::get_offset_of_data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (ResourceBundleList_t987847975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3236[2] = 
{
	ResourceBundleList_t987847975::get_offset_of_bundle_5(),
	ResourceBundleList_t987847975::get_offset_of_manifest_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (ResourceBundle_t1032369214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[5] = 
{
	ResourceBundle_t1032369214::get_offset_of_hash_5(),
	ResourceBundle_t1032369214::get_offset_of_bundle_6(),
	ResourceBundle_t1032369214::get_offset_of_shader_7(),
	ResourceBundle_t1032369214::get_offset_of_sprite_8(),
	ResourceBundle_t1032369214::get_offset_of_material_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (ProgressEventArgs_t1844038154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3238[2] = 
{
	ProgressEventArgs_t1844038154::get_offset_of_loader_1(),
	ProgressEventArgs_t1844038154::get_offset_of_resource_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (LoadResult_t1889652257)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3239[3] = 
{
	LoadResult_t1889652257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (LoadedEventArgs_t3630200431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[2] = 
{
	LoadedEventArgs_t3630200431::get_offset_of_loader_1(),
	LoadedEventArgs_t3630200431::get_offset_of_result_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (ResourceLoader_t3764463624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[5] = 
{
	ResourceLoader_t3764463624::get_offset_of_url_0(),
	ResourceLoader_t3764463624::get_offset_of_resources_1(),
	ResourceLoader_t3764463624::get_offset_of_progress_2(),
	ResourceLoader_t3764463624::get_offset_of_raiseProgressEvent_3(),
	ResourceLoader_t3764463624::get_offset_of_raiseLoadedEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (LocaleResourceLoader_t808386688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (U3CloadU3Ec__Iterator0_t1446289385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[18] = 
{
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CasyncOperationU3E__0_0(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CwebRequestU3E__0_1(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CfileRequestU3E__0_2(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CresourceBundleListU3E__0_3(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CurlU3E__0_4(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CmanifestU3E__1_5(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CbundleNamesU3E__1_6(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CcounterU3E__1_7(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CresourceBundleU3E__1_8(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U24locvar0_9(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CpairU3E__2_10(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CresourceU3E__3_11(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3ChashSavePathU3E__4_12(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U3CfileSavePathU3E__4_13(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U24this_14(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U24current_15(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U24disposing_16(),
	U3CloadU3Ec__Iterator0_t1446289385::get_offset_of_U24PC_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (RemoteResourceLoader_t1429902794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (U3CloadU3Ec__Iterator0_t2710249343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[15] = 
{
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CcounterU3E__0_0(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CresourceBundleListU3E__0_1(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CresourceBundleU3E__0_2(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U24locvar0_3(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CpairU3E__1_4(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CresourceU3E__2_5(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CurlU3E__2_6(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CrequestU3E__2_7(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CmanifestU3E__3_8(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3ChashSavePathU3E__3_9(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U3CfileSavePathU3E__3_10(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U24this_11(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U24current_12(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U24disposing_13(),
	U3CloadU3Ec__Iterator0_t2710249343::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (ServErr_t270116950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3246[102] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (Utils_t1444179947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (ImprovedPerlin_t2236177176), -1, sizeof(ImprovedPerlin_t2236177176_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3248[17] = 
{
	ImprovedPerlin_t2236177176::get_offset_of_X_0(),
	ImprovedPerlin_t2236177176::get_offset_of_Y_1(),
	ImprovedPerlin_t2236177176::get_offset_of_Z_2(),
	ImprovedPerlin_t2236177176::get_offset_of_u_3(),
	ImprovedPerlin_t2236177176::get_offset_of_v_4(),
	ImprovedPerlin_t2236177176::get_offset_of_w_5(),
	ImprovedPerlin_t2236177176::get_offset_of_A_6(),
	ImprovedPerlin_t2236177176::get_offset_of_AA_7(),
	ImprovedPerlin_t2236177176::get_offset_of_AB_8(),
	ImprovedPerlin_t2236177176::get_offset_of_B_9(),
	ImprovedPerlin_t2236177176::get_offset_of_BA_10(),
	ImprovedPerlin_t2236177176::get_offset_of_BB_11(),
	ImprovedPerlin_t2236177176::get_offset_of_floorx_12(),
	ImprovedPerlin_t2236177176::get_offset_of_floory_13(),
	ImprovedPerlin_t2236177176::get_offset_of_floorz_14(),
	ImprovedPerlin_t2236177176_StaticFields::get_offset_of_p_15(),
	ImprovedPerlin_t2236177176_StaticFields::get_offset_of_permutation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (MeshShape_t3358450374), -1, sizeof(MeshShape_t3358450374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3249[36] = 
{
	MeshShape_t3358450374_StaticFields::get_offset_of_matrix_2(),
	MeshShape_t3358450374::get_offset_of_springForce_3(),
	MeshShape_t3358450374::get_offset_of_damping_4(),
	MeshShape_t3358450374::get_offset_of_uniformScale_5(),
	MeshShape_t3358450374::get_offset_of_mesh_6(),
	MeshShape_t3358450374::get_offset_of_meshRenderer_7(),
	MeshShape_t3358450374::get_offset_of_meshFilter_8(),
	MeshShape_t3358450374::get_offset_of_vertices_9(),
	MeshShape_t3358450374::get_offset_of_triangles_10(),
	MeshShape_t3358450374::get_offset_of_colors_11(),
	MeshShape_t3358450374::get_offset_of_uvs_12(),
	MeshShape_t3358450374::get_offset_of_isFillPolygon_13(),
	MeshShape_t3358450374::get_offset_of_isFillColor_14(),
	MeshShape_t3358450374::get_offset_of_animationAngle_15(),
	MeshShape_t3358450374::get_offset_of_animationVertices_16(),
	MeshShape_t3358450374::get_offset_of_orignalVertices_17(),
	MeshShape_t3358450374::get_offset_of_distortVertices_18(),
	MeshShape_t3358450374::get_offset_of_distortVerticesVelocities_19(),
	MeshShape_t3358450374::get_offset_of_animationUVs_20(),
	MeshShape_t3358450374::get_offset_of_animationOuterRadius_21(),
	MeshShape_t3358450374::get_offset_of_animationInnerRadius_22(),
	MeshShape_t3358450374::get_offset_of_animationStarrines_23(),
	MeshShape_t3358450374::get_offset_of_animationNoiseOffset_24(),
	MeshShape_t3358450374::get_offset_of_animationNoiseSpeed_25(),
	MeshShape_t3358450374::get_offset_of_animationNoiseFrequency_26(),
	MeshShape_t3358450374::get_offset_of_animationNoiseAmplitude_27(),
	MeshShape_t3358450374::get_offset_of_m_Perlin_28(),
	MeshShape_t3358450374::get_offset_of_m_bDahsed_29(),
	MeshShape_t3358450374::get_offset_of_m_Color_30(),
	MeshShape_t3358450374::get_offset_of_m_numOfSegments_31(),
	MeshShape_t3358450374::get_offset_of_m_outterRadius_32(),
	MeshShape_t3358450374::get_offset_of_m_innerRadius_33(),
	MeshShape_t3358450374::get_offset_of_m_angle1_34(),
	MeshShape_t3358450374::get_offset_of_m_angle2_35(),
	MeshShape_t3358450374::get_offset_of_m_starrines_36(),
	MeshShape_t3358450374::get_offset_of_m_rotation_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (CAutoTest_t3240667618), -1, sizeof(CAutoTest_t3240667618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3250[11] = 
{
	CAutoTest_t3240667618_StaticFields::get_offset_of_s_Instance_2(),
	CAutoTest_t3240667618::get_offset_of_m_Player_3(),
	CAutoTest_t3240667618::get_offset_of_m_bAutoTesing_4(),
	CAutoTest_t3240667618_StaticFields::get_offset_of_vecTempPos_5(),
	CAutoTest_t3240667618_StaticFields::get_offset_of_vecTempDir_6(),
	CAutoTest_t3240667618::get_offset_of_m_vecWorldCursorPos_7(),
	CAutoTest_t3240667618::get_offset_of_m_fChangeMoveDirInterval_8(),
	CAutoTest_t3240667618::get_offset_of_m_fAddVolumeInterval_9(),
	CAutoTest_t3240667618::get_offset_of_m_fSpitBallInterval_10(),
	CAutoTest_t3240667618::get_offset_of_m_bForceSpiting_11(),
	CAutoTest_t3240667618::get_offset_of_m_fForceSpitingTimeElapse_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (CAudioManager_t2887365613), -1, sizeof(CAudioManager_t2887365613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3251[7] = 
{
	CAudioManager_t2887365613_StaticFields::get_offset_of_s_Instance_2(),
	CAudioManager_t2887365613::get_offset_of_audio_main_bg_3(),
	CAudioManager_t2887365613::get_offset_of_audio_login_bgm_4(),
	CAudioManager_t2887365613::get_offset_of_aryAudio_5(),
	CAudioManager_t2887365613::get_offset_of_m_aryVoice_6(),
	CAudioManager_t2887365613::get_offset_of_m_eCurMainBg_7(),
	CAudioManager_t2887365613::get_offset_of_m_aryMainBg_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (eAudioId_t13996765)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3252[13] = 
{
	eAudioId_t13996765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (eVoice_t2630767850)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3253[2] = 
{
	eVoice_t2630767850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (eSE_t1337385684)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3254[14] = 
{
	eSE_t1337385684::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (eMainBg_t3769614300)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3255[5] = 
{
	eMainBg_t3769614300::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (CBallManager_t3583688634), -1, sizeof(CBallManager_t3583688634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3256[6] = 
{
	CBallManager_t3583688634_StaticFields::get_offset_of_s_Instance_2(),
	CBallManager_t3583688634::get_offset_of_m_dicRecycledBalls_MainPlayer_3(),
	CBallManager_t3583688634::get_offset_of_m_dicRecycledBalls_OtherClient_4(),
	CBallManager_t3583688634::get_offset_of_m_preArrow_5(),
	CBallManager_t3583688634::get_offset_of_m_fArrowLocalScale_6(),
	CBallManager_t3583688634::get_offset_of_s_nBallGuid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (CSkillModuleForBall_t1995272125), -1, sizeof(CSkillModuleForBall_t1995272125_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3257[7] = 
{
	CSkillModuleForBall_t1995272125_StaticFields::get_offset_of_vecTempPos_2(),
	CSkillModuleForBall_t1995272125::get_offset_of__ball_3(),
	CSkillModuleForBall_t1995272125::get_offset_of__goEffectContainer_4(),
	CSkillModuleForBall_t1995272125::get_offset_of_m_aryActiveEffect_QianYao_5(),
	CSkillModuleForBall_t1995272125::get_offset_of_m_aryActiveEffect_ChiXu_6(),
	CSkillModuleForBall_t1995272125::get_offset_of_m_nSkillStatus_7(),
	CSkillModuleForBall_t1995272125::get_offset_of_m_eSkillId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (BallAction_t576009403), -1, sizeof(BallAction_t576009403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3258[31] = 
{
	0,
	BallAction_t576009403::get_offset_of__total_scale_min_3(),
	BallAction_t576009403::get_offset_of__total_scale_max_4(),
	BallAction_t576009403::get_offset_of__total_scale_ratio_5(),
	BallAction_t576009403::get_offset_of__total_scale_inpoisonable_ratio_6(),
	BallAction_t576009403::get_offset_of__poison_scale_min_7(),
	BallAction_t576009403::get_offset_of__poison_scale_max_8(),
	BallAction_t576009403::get_offset_of__poison_scale_inc_9(),
	BallAction_t576009403::get_offset_of__poison_scale_inc_delta_10(),
	BallAction_t576009403::get_offset_of__direction_11(),
	BallAction_t576009403::get_offset_of__ball_realtime_velocity_12(),
	BallAction_t576009403::get_offset_of__color_index_13(),
	BallAction_t576009403::get_offset_of__velocity_14(),
	0,
	BallAction_t576009403::get_offset_of__dir_indicator_16(),
	BallAction_t576009403::get_offset_of__dir_line_17(),
	BallAction_t576009403::get_offset_of__ball_18(),
	BallAction_t576009403::get_offset_of_vecTempPos1_19(),
	BallAction_t576009403::get_offset_of_vecTempPos2_20(),
	BallAction_t576009403::get_offset_of_m_fRiNiGeGuiSpeed_21(),
	BallAction_t576009403_StaticFields::get_offset_of_s_pos1_22(),
	BallAction_t576009403_StaticFields::get_offset_of_s_pos2_23(),
	BallAction_t576009403::get_offset_of__fLastFrameDeltaX_24(),
	BallAction_t576009403::get_offset_of__fLastFrameDeltaY_25(),
	BallAction_t576009403::get_offset_of__velocity_without_size_26(),
	BallAction_t576009403::get_offset_of_m_bAttackDisUpdated_27(),
	BallAction_t576009403::get_offset_of_m_bArrowUpdated_28(),
	BallAction_t576009403::get_offset_of_m_bRealTimeSpeedUpdated_29(),
	BallAction_t576009403::get_offset_of_m_fAttackDis_30(),
	BallAction_t576009403::get_offset_of_m_flastSpeedWithoutSize_31(),
	BallAction_t576009403::get_offset_of_m_vecRealTimeSpeed_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (CBean_t1061610760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[9] = 
{
	CBean_t1061610760::get_offset_of__sr_2(),
	CBean_t1061610760::get_offset_of_m_nGuid_3(),
	CBean_t1061610760::get_offset_of_m_szConfigId_4(),
	CBean_t1061610760::get_offset_of_m_fDeadTime_5(),
	CBean_t1061610760::get_offset_of_m_Config_6(),
	CBean_t1061610760::get_offset_of_m_ClassConfig_7(),
	CBean_t1061610760::get_offset_of_m_bActive_8(),
	CBean_t1061610760::get_offset_of_m_goContainer_9(),
	CBean_t1061610760::get_offset_of__Trigger_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (CBeanManager_t1120502076), -1, sizeof(CBeanManager_t1120502076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3260[19] = 
{
	CBeanManager_t1120502076_StaticFields::get_offset_of_vecTempPos_2(),
	CBeanManager_t1120502076::get_offset_of_m_goBeanContainer_3(),
	CBeanManager_t1120502076_StaticFields::get_offset_of_s_Instance_4(),
	CBeanManager_t1120502076::get_offset_of_m_nBeanGuid_5(),
	CBeanManager_t1120502076::get_offset_of_m_lstEatBean_6(),
	0,
	CBeanManager_t1120502076::get_offset_of_m_fEatBeanCount_8(),
	CBeanManager_t1120502076::get_offset_of__bytesEatBeanData_9(),
	CBeanManager_t1120502076::get_offset_of_m_dicBeans_10(),
	0,
	CBeanManager_t1120502076::get_offset_of_m_fBeanRebornCount_12(),
	CBeanManager_t1120502076::get_offset_of_m_nTestParam_13(),
	CBeanManager_t1120502076::get_offset_of_m_lstBeanToInit_14(),
	CBeanManager_t1120502076::get_offset_of_m_nInitBeanIndex_15(),
	CBeanManager_t1120502076::get_offset_of_m_nInitBeanSubIndex_16(),
	CBeanManager_t1120502076::get_offset_of_m_bBeanInitCompleted_17(),
	CBeanManager_t1120502076::get_offset_of_m_dicBeansToBeReborn_18(),
	CBeanManager_t1120502076::get_offset_of_m_lstTempReborn_19(),
	CBeanManager_t1120502076::get_offset_of_m_bTesting_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (sInitBeanNode_t1761059340)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3261[4] = 
{
	sInitBeanNode_t1761059340::get_offset_of_config_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sInitBeanNode_t1761059340::get_offset_of_aryPos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sInitBeanNode_t1761059340::get_offset_of_class_config_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sInitBeanNode_t1761059340::get_offset_of_nClassId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (CBloodStain_t3743325761), -1, sizeof(CBloodStain_t3743325761_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3262[26] = 
{
	CBloodStain_t3743325761::get_offset_of_m_goMainContainer_2(),
	CBloodStain_t3743325761_StaticFields::get_offset_of_vecTempPos_3(),
	CBloodStain_t3743325761_StaticFields::get_offset_of_vecTempScale_4(),
	CBloodStain_t3743325761_StaticFields::get_offset_of_colorTemp_5(),
	CBloodStain_t3743325761::get_offset_of_m_eType_6(),
	CBloodStain_t3743325761::get_offset_of_m_eFadeMode_7(),
	CBloodStain_t3743325761::get_offset_of_m_srMain_8(),
	CBloodStain_t3743325761::get_offset_of_m_fLastTime_9(),
	CBloodStain_t3743325761::get_offset_of_m_fFadeTime_10(),
	CBloodStain_t3743325761::get_offset_of_m_fFadeTimeElapse_11(),
	CBloodStain_t3743325761::get_offset_of_m_bLasting_12(),
	CBloodStain_t3743325761::get_offset_of_m_bFading_13(),
	CBloodStain_t3743325761::get_offset_of_m_fDynamicScaleTime_14(),
	CBloodStain_t3743325761::get_offset_of_m_fScale_15(),
	CBloodStain_t3743325761::get_offset_of_m_bDynamicScale_16(),
	CBloodStain_t3743325761::get_offset_of_m_bFilling_17(),
	CBloodStain_t3743325761::get_offset_of_m_fFillTotalTime_18(),
	CBloodStain_t3743325761::get_offset_of_m_fFillAmount_19(),
	CBloodStain_t3743325761::get_offset_of_m_uKey_20(),
	CBloodStain_t3743325761::get_offset_of_m_uParentKey_21(),
	CBloodStain_t3743325761::get_offset_of_m_ePosType_22(),
	CBloodStain_t3743325761::get_offset_of_m_eParentPosType_23(),
	CBloodStain_t3743325761::get_offset_of_m_nHierarchy_24(),
	CBloodStain_t3743325761::get_offset_of_m_nColorId_25(),
	CBloodStain_t3743325761::get_offset_of_m_sprMain_26(),
	CBloodStain_t3743325761::get_offset_of__sortingGroup_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (CBloodStainManager_t2257855270), -1, sizeof(CBloodStainManager_t2257855270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3263[21] = 
{
	CBloodStainManager_t2257855270_StaticFields::get_offset_of_s_Instance_2(),
	CBloodStainManager_t2257855270_StaticFields::get_offset_of_vecTempPos_3(),
	CBloodStainManager_t2257855270_StaticFields::get_offset_of_vecTempDir_4(),
	CBloodStainManager_t2257855270::get_offset_of_m_preFrameAni_XiangQianPenJian_5(),
	CBloodStainManager_t2257855270::get_offset_of_m_aryBloodStainPrefab_6(),
	CBloodStainManager_t2257855270::get_offset_of_m_aryBloodSize2BallSize_7(),
	CBloodStainManager_t2257855270::get_offset_of_m_aryLastTime_8(),
	CBloodStainManager_t2257855270::get_offset_of_m_aryFadeTime_9(),
	CBloodStainManager_t2257855270::get_offset_of_m_fDynamicScaleTime_10(),
	CBloodStainManager_t2257855270::get_offset_of_m_preBloodTile_11(),
	CBloodStainManager_t2257855270::get_offset_of_m_fBloodTileInterval_12(),
	CBloodStainManager_t2257855270::get_offset_of_m_preStain_13(),
	CBloodStainManager_t2257855270::get_offset_of_m_goArea0_14(),
	CBloodStainManager_t2257855270::get_offset_of_m_fXiangQianPenJianScale_15(),
	CBloodStainManager_t2257855270::get_offset_of_m_fXiangQianPenJianPosDirX_16(),
	CBloodStainManager_t2257855270::get_offset_of_m_fXiangQianPenJianPosDirY_17(),
	CBloodStainManager_t2257855270::get_offset_of_m_dicRecycledBloodStain_18(),
	CBloodStainManager_t2257855270::get_offset_of_m_dicBloodStains_19(),
	CBloodStainManager_t2257855270::get_offset_of_m_lstRecycledBloodStains_20(),
	CBloodStainManager_t2257855270::get_offset_of_m_lstFading_21(),
	CBloodStainManager_t2257855270::get_offset_of_m_fFadeTime_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (eBloodStainType_t333536540)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3264[5] = 
{
	eBloodStainType_t333536540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (eDynamicEffectType_t713621222)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3265[4] = 
{
	eDynamicEffectType_t713621222::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (eFadeMode_t2947864873)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3266[3] = 
{
	eFadeMode_t2947864873::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (CBloodTile_t2617155586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[2] = 
{
	CBloodTile_t2617155586::get_offset_of_m_sprMain_2(),
	CBloodTile_t2617155586::get_offset_of_m_goMainContainer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (CBuff_t1540791136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[3] = 
{
	CBuff_t1540791136::get_offset_of_m_Config_2(),
	CBuff_t1540791136::get_offset_of_m_fTime_3(),
	CBuff_t1540791136::get_offset_of_m_lGUID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (eBuffType_t2377588228)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3269[1] = 
{
	eBuffType_t2377588228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (CBuffEditor_t818684383), -1, sizeof(CBuffEditor_t818684383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3270[10] = 
{
	CBuffEditor_t818684383_StaticFields::get_offset_of_s_Instance_2(),
	CBuffEditor_t818684383::get_offset_of__aryDesc_3(),
	CBuffEditor_t818684383::get_offset_of__aryValues_4(),
	CBuffEditor_t818684383::get_offset_of__inputTime_5(),
	CBuffEditor_t818684383::get_offset_of__dropdownBuffId_6(),
	CBuffEditor_t818684383::get_offset_of__dropdownBuffFunc_7(),
	0,
	CBuffEditor_t818684383::get_offset_of_m_dicBuffConfig_9(),
	CBuffEditor_t818684383::get_offset_of_m_CurEditConfig_10(),
	CBuffEditor_t818684383::get_offset_of_tempConfig_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (eBuffFunc_t3332395474)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3271[7] = 
{
	eBuffFunc_t3332395474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (sBuffConfig_t769791949)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[5] = 
{
	sBuffConfig_t769791949::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBuffConfig_t769791949::get_offset_of_func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBuffConfig_t769791949::get_offset_of_time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBuffConfig_t769791949::get_offset_of_aryDesc_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBuffConfig_t769791949::get_offset_of_aryValues_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (CAdaptiveManager_t2673862881), -1, sizeof(CAdaptiveManager_t2673862881_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3273[6] = 
{
	CAdaptiveManager_t2673862881_StaticFields::get_offset_of_s_Instance_2(),
	CAdaptiveManager_t2673862881::get_offset_of_m_bCheckDEvice_3(),
	CAdaptiveManager_t2673862881::get_offset_of__paomadeng_4(),
	CAdaptiveManager_t2673862881::get_offset_of_m_aryPaoMaDeng_5(),
	CAdaptiveManager_t2673862881::get_offset_of_m_eDeviceType_6(),
	CAdaptiveManager_t2673862881::get_offset_of_m_Resolution_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (eDeviceTYpe_t2574412910)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3274[3] = 
{
	eDeviceTYpe_t2574412910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (CCheat_t3466918192), -1, sizeof(CCheat_t3466918192_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3275[45] = 
{
	CCheat_t3466918192_StaticFields::get_offset_of_vecTempPos_2(),
	CCheat_t3466918192_StaticFields::get_offset_of_vecTempScale_3(),
	CCheat_t3466918192_StaticFields::get_offset_of_s_Instance_4(),
	CCheat_t3466918192::get_offset_of_m_bActive_5(),
	CCheat_t3466918192::get_offset_of__goPanel_6(),
	CCheat_t3466918192::get_offset_of__toggleFixedRebornPos_7(),
	CCheat_t3466918192::get_offset_of__goMoveToCenter_8(),
	CCheat_t3466918192::get_offset_of__inputJiShaTestScript_9(),
	CCheat_t3466918192::get_offset_of__toggleXingNengCeShi_10(),
	CCheat_t3466918192::get_offset_of_m_fShitCount_11(),
	CCheat_t3466918192::get_offset_of_lstAssist_12(),
	CCheat_t3466918192::get_offset_of__inputBallsNum_13(),
	CCheat_t3466918192::get_offset_of__inputBallMoveTimeInterval_14(),
	CCheat_t3466918192::get_offset_of_m_fBallMoveTimeInterval_15(),
	CCheat_t3466918192::get_offset_of_m_nShitCount_16(),
	CCheat_t3466918192::get_offset_of_m_nShitSkinCount_17(),
	CCheat_t3466918192::get_offset_of_m_fShitTimeCount_18(),
	CCheat_t3466918192::get_offset_of_m_nShitSkinIndex_19(),
	CCheat_t3466918192::get_offset_of_m_lstTestBalls_20(),
	CCheat_t3466918192::get_offset_of_m_bTestBallsMoving_21(),
	CCheat_t3466918192::get_offset_of_m_fAutoMoveTimeElapse_22(),
	CCheat_t3466918192::get_offset_of__toggleRigidMove_23(),
	CCheat_t3466918192::get_offset_of__inputTRiangleNum_24(),
	CCheat_t3466918192::get_offset_of_m_nAutoExplode_25(),
	CCheat_t3466918192::get_offset_of_m_fAutoEXplodeTimeCount_26(),
	CCheat_t3466918192::get_offset_of_m_nFrameCount_27(),
	CCheat_t3466918192::get_offset_of_m_fFPSCalcCount_28(),
	0,
	CCheat_t3466918192::get_offset_of__inputMannualControlViewSize_30(),
	CCheat_t3466918192::get_offset_of__toggleMannualControlViewSize_31(),
	CCheat_t3466918192::get_offset_of_m_fMannualControlViewSize_32(),
	CCheat_t3466918192::get_offset_of__inputClickToStopMove_33(),
	CCheat_t3466918192::get_offset_of__fClickToStopMoveInterval_34(),
	CCheat_t3466918192::get_offset_of__inputMaxCamSize_35(),
	CCheat_t3466918192::get_offset_of__tggleMaxCamSizeLimit_36(),
	CCheat_t3466918192::get_offset_of__panelHomePageCheatPanel_37(),
	CCheat_t3466918192::get_offset_of_m_bHomePageCheatPanelShowing_38(),
	CCheat_t3466918192::get_offset_of__inputStreetNews_39(),
	CCheat_t3466918192::get_offset_of__inputHotEvents_40(),
	CCheat_t3466918192::get_offset_of__txtDebugInfo_41(),
	CCheat_t3466918192::get_offset_of__inputBallIndex_42(),
	CCheat_t3466918192::get_offset_of__inputFakeAccount_43(),
	CCheat_t3466918192::get_offset_of__inputSize_44(),
	CCheat_t3466918192::get_offset_of__inputInterval_45(),
	CCheat_t3466918192::get_offset_of__inputStarine_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (CControl_t2293828845), -1, sizeof(CControl_t2293828845_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3276[2] = 
{
	CControl_t2293828845_StaticFields::get_offset_of_s_Instance_2(),
	CControl_t2293828845::get_offset_of__inputBallTeamRadiusMultiple_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (CGyroscope_t3470865232), -1, sizeof(CGyroscope_t3470865232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3277[5] = 
{
	CGyroscope_t3470865232_StaticFields::get_offset_of_vecTempPos_2(),
	CGyroscope_t3470865232::get_offset_of__txtDebugInfo_3(),
	CGyroscope_t3470865232::get_offset_of_m_fMaxGyroAmount_4(),
	CGyroscope_t3470865232::get_offset_of_m_fMaxImagePosDelta_5(),
	CGyroscope_t3470865232::get_offset_of_m_goBgImage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (CDef_t3381995704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (CDick_t2288086465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3279[4] = 
{
	CDick_t2288086465::get_offset_of__goCyliner_2(),
	CDick_t2288086465::get_offset_of__srCyliner_3(),
	CDick_t2288086465::get_offset_of__goCircle_4(),
	CDick_t2288086465::get_offset_of__srCircle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (CExplodeNode_t97844667), -1, sizeof(CExplodeNode_t97844667_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[55] = 
{
	CExplodeNode_t97844667::get_offset_of__Player_2(),
	CExplodeNode_t97844667::get_offset_of_m_bExploding_3(),
	CExplodeNode_t97844667::get_offset_of_m_bWaiting_4(),
	CExplodeNode_t97844667::get_offset_of_m_bAutoMoveAfterExplodeCompleted_5(),
	CExplodeNode_t97844667::get_offset_of_ballMother_6(),
	CExplodeNode_t97844667::get_offset_of_explodeConfig_7(),
	CExplodeNode_t97844667::get_offset_of_m_bMotherMoving_8(),
	CExplodeNode_t97844667::get_offset_of_nChildNum_9(),
	CExplodeNode_t97844667::get_offset_of_nConfigChildNum_10(),
	CExplodeNode_t97844667::get_offset_of_fExpectAngle_11(),
	CExplodeNode_t97844667::get_offset_of_fChildVolume_12(),
	CExplodeNode_t97844667::get_offset_of_fMotherLeftVolume_13(),
	CExplodeNode_t97844667::get_offset_of_lstChildBallIndex_14(),
	CExplodeNode_t97844667::get_offset_of_m_vecMotherPos_15(),
	CExplodeNode_t97844667::get_offset_of_m_fMotherRadius_16(),
	CExplodeNode_t97844667::get_offset_of_m_lstDir_17(),
	CExplodeNode_t97844667::get_offset_of_m_fSpeed_18(),
	CExplodeNode_t97844667::get_offset_of_m_fRunDistance_19(),
	CExplodeNode_t97844667::get_offset_of_m_nShiZi_BallNum_PerDir_Moving_20(),
	CExplodeNode_t97844667::get_offset_of_m_nShiZi_BallNum_PerDir_NotMoving_21(),
	CExplodeNode_t97844667::get_offset_of_m_vecMotherDir_22(),
	CExplodeNode_t97844667::get_offset_of_m_fMotherDirAngle_23(),
	CExplodeNode_t97844667::get_offset_of_m_fShiZi_ZuoCe_Angle_24(),
	CExplodeNode_t97844667::get_offset_of_m_fShiZi_YouCe_Angle_25(),
	CExplodeNode_t97844667::get_offset_of_m_fShiZi_HouFang_Angle_26(),
	CExplodeNode_t97844667::get_offset_of_m_fSegDis_Moving_27(),
	CExplodeNode_t97844667::get_offset_of_m_vecSHiZi_ZuoCe_Dir_28(),
	CExplodeNode_t97844667::get_offset_of_m_vecSHiZi_YouCe_Dir_29(),
	CExplodeNode_t97844667::get_offset_of_m_vecSHiZi_HouFang_Dir_30(),
	CExplodeNode_t97844667::get_offset_of_m_nShiZiStatus_31(),
	CExplodeNode_t97844667::get_offset_of_m_nShiZiStatusNumCount_32(),
	CExplodeNode_t97844667::get_offset_of_m_nZuoNumCount_33(),
	CExplodeNode_t97844667::get_offset_of_m_nYouNumCount_34(),
	0,
	0,
	0,
	CExplodeNode_t97844667::get_offset_of_m_vecSanJiao_ZuoCe_Dir_38(),
	CExplodeNode_t97844667::get_offset_of_m_vecSanJiao_YouCe_Dir_39(),
	CExplodeNode_t97844667::get_offset_of_m_nSanJiaoStatus_40(),
	CExplodeNode_t97844667::get_offset_of_m_nSanJiaoStatusNumCount_41(),
	CExplodeNode_t97844667::get_offset_of_m_nSanJiaoZuoNumCount_42(),
	CExplodeNode_t97844667::get_offset_of_m_nSanJiaoYouNumCount_43(),
	0,
	0,
	CExplodeNode_t97844667::get_offset_of_m_nSanJiao_BallNum_PerDir_Moving_46(),
	CExplodeNode_t97844667::get_offset_of_m_fSanJiao_SegDis_Moving_47(),
	CExplodeNode_t97844667::get_offset_of_m_nCurChildNum_48(),
	CExplodeNode_t97844667::get_offset_of_m_nExplodeRound_49(),
	CExplodeNode_t97844667::get_offset_of_m_fSegDis_50(),
	0,
	CExplodeNode_t97844667_StaticFields::get_offset_of_vec2Temp_52(),
	CExplodeNode_t97844667_StaticFields::get_offset_of_vecTempPos_53(),
	CExplodeNode_t97844667_StaticFields::get_offset_of_vecTempDir_54(),
	CExplodeNode_t97844667::get_offset_of_m_lstTempEjectEndPos_55(),
	CExplodeNode_t97844667::get_offset_of_m_fWaitingTimeElapse_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (CCameraManager_t1888214120), -1, sizeof(CCameraManager_t1888214120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3281[43] = 
{
	CCameraManager_t1888214120_StaticFields::get_offset_of_s_Instance_2(),
	CCameraManager_t1888214120::get_offset_of_m_camWave_3(),
	CCameraManager_t1888214120::get_offset_of__inputTimeBeforeUp_4(),
	CCameraManager_t1888214120::get_offset_of__inputUpTime_5(),
	CCameraManager_t1888214120::get_offset_of__inputObserverTime_6(),
	CCameraManager_t1888214120::get_offset_of__inputDownTime_7(),
	CCameraManager_t1888214120::get_offset_of__inputRebornSpotScale_8(),
	CCameraManager_t1888214120::get_offset_of__inputRebornSpotBlingSpeed_9(),
	CCameraManager_t1888214120_StaticFields::get_offset_of_vecTempPos_10(),
	CCameraManager_t1888214120_StaticFields::get_offset_of_vecTempPos2_11(),
	CCameraManager_t1888214120_StaticFields::get_offset_of_vecCamerCurPos_12(),
	CCameraManager_t1888214120_StaticFields::get_offset_of_vecArenaLeftBottom_ScreenCorSys_13(),
	CCameraManager_t1888214120_StaticFields::get_offset_of_vecArenaRightTop_ScreenCorSys_14(),
	CCameraManager_t1888214120::get_offset_of_m_fRebornCamerSize_15(),
	CCameraManager_t1888214120::get_offset_of_m_fUpTime_16(),
	CCameraManager_t1888214120::get_offset_of_m_fDownTime_17(),
	CCameraManager_t1888214120::get_offset_of_m_fDeadWatchTime_18(),
	CCameraManager_t1888214120::get_offset_of_m_fTimeBeforeUp_19(),
	CCameraManager_t1888214120::get_offset_of_m_fRebornSpotScale_20(),
	CCameraManager_t1888214120::get_offset_of_m_fRebornSpotBlingSpeed_21(),
	CCameraManager_t1888214120::get_offset_of_m_fUpSizeSpeed_22(),
	CCameraManager_t1888214120::get_offset_of_m_fDownSizeSpeed_23(),
	CCameraManager_t1888214120::get_offset_of_m_vecUpPosSpeed_24(),
	CCameraManager_t1888214120::get_offset_of_m_vecDownPosSpeed_25(),
	CCameraManager_t1888214120::get_offset_of_m_nStatus_26(),
	CCameraManager_t1888214120::get_offset_of__etc_27(),
	CCameraManager_t1888214120::get_offset_of__RebornSpot_28(),
	CCameraManager_t1888214120::get_offset_of_m_fScale_29(),
	CCameraManager_t1888214120::get_offset_of__uiAll_30(),
	CCameraManager_t1888214120::get_offset_of_m_fHidePlayerNameThreshold_31(),
	CCameraManager_t1888214120::get_offset_of_m_fMaxCamSize_32(),
	CCameraManager_t1888214120::get_offset_of_m_bMaxCamLimit_33(),
	CCameraManager_t1888214120::get_offset_of_m_fPlayerNameVisibleXiShu_34(),
	CCameraManager_t1888214120::get_offset_of_m_vecDownDetPos_35(),
	CCameraManager_t1888214120::get_offset_of_m_fDownAccelerate_36(),
	CCameraManager_t1888214120::get_offset_of_m_fDownV0_37(),
	CCameraManager_t1888214120::get_offset_of_m_fShitWaitTimeCount_38(),
	CCameraManager_t1888214120::get_offset_of_m_vecWorldCoorViewLeftBottom_39(),
	CCameraManager_t1888214120::get_offset_of_m_vecWorldCoorViewRightTop_40(),
	CCameraManager_t1888214120::get_offset_of__lrShitBar_41(),
	CCameraManager_t1888214120::get_offset_of_m_fViewBorderDistance_42(),
	CCameraManager_t1888214120::get_offset_of_m_fLastCamSize_43(),
	CCameraManager_t1888214120::get_offset_of_m_fWaveCamSpeed_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (CColorManager_t615473758), -1, sizeof(CColorManager_t615473758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3282[2] = 
{
	CColorManager_t615473758_StaticFields::get_offset_of_s_Instance_2(),
	CColorManager_t615473758::get_offset_of_m_dicColors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (CMovie_t1832462693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[2] = 
{
	CMovie_t1832462693::get_offset_of_videoPlayer_2(),
	CMovie_t1832462693::get_offset_of_rawImage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (CScreenShot_t989390525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[4] = 
{
	CScreenShot_t989390525::get_offset_of_mainCam_2(),
	CScreenShot_t989390525::get_offset_of_rt_3(),
	CScreenShot_t989390525::get_offset_of_t2d_4(),
	CScreenShot_t989390525::get_offset_of_num_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (CVisionManaer_t3289296692), -1, sizeof(CVisionManaer_t3289296692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3285[2] = 
{
	CVisionManaer_t3289296692_StaticFields::get_offset_of_s_Instance_2(),
	CVisionManaer_t3289296692::get_offset_of__scriptBlur_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (CGunsight_t2209988408), -1, sizeof(CGunsight_t2209988408_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3286[6] = 
{
	CGunsight_t2209988408_StaticFields::get_offset_of_vecTempScale_2(),
	CGunsight_t2209988408_StaticFields::get_offset_of_vecTempPos_3(),
	CGunsight_t2209988408_StaticFields::get_offset_of_vecTempDir_4(),
	CGunsight_t2209988408::get_offset_of__dickJoint_5(),
	CGunsight_t2209988408::get_offset_of__dickStick_6(),
	CGunsight_t2209988408::get_offset_of__circle_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (CChiQiuAndJiShaItem_t1987171717), -1, sizeof(CChiQiuAndJiShaItem_t1987171717_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3287[10] = 
{
	CChiQiuAndJiShaItem_t1987171717_StaticFields::get_offset_of_vecTempPos_2(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of__txtEaterName_3(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of__txtDeadMeatName_4(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of__imgActionIcon_5(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of__imgBgBar_6(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of__CanvasGroup_7(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of_m_nStatus_8(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of_m_fTimeElapse_9(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of_m_fSlideV0_10(),
	CChiQiuAndJiShaItem_t1987171717::get_offset_of_m_fJumpV0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (CChiQiuAndJiShaManager_t3507423475), -1, sizeof(CChiQiuAndJiShaManager_t3507423475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3288[26] = 
{
	CChiQiuAndJiShaManager_t3507423475_StaticFields::get_offset_of_s_Instance_2(),
	CChiQiuAndJiShaManager_t3507423475_StaticFields::get_offset_of_vecTempPos_3(),
	CChiQiuAndJiShaManager_t3507423475_StaticFields::get_offset_of_vecTempScale_4(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_colorMe_5(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_colorOthers_6(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_preChiQiuAndJiShaItem_7(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_aryActionIcon_8(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_goContainer_9(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fItemGap_10(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_nMaxItemNum_11(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fItemShowTime_12(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fCalculateChiQiuInterval_13(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fMoveItemsSpeed_14(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fSlideTime_15(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fSlideDistance_16(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fSlideV0_17(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fSlideA_18(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fJumpDistance_19(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fJumpTime_20(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fJumpV0_21(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fJumpA_22(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fJumpFadeSpeed_23(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fFadeSpeed_24(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_lstItems_25(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_lstRecycledItems_26(),
	CChiQiuAndJiShaManager_t3507423475::get_offset_of_m_fTotalMoveDistance_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (CLayerManager_t3570232401), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (eLayerId_t1442111487)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3290[5] = 
{
	eLayerId_t1442111487::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (CMonster_t1941470938), -1, sizeof(CMonster_t1941470938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3291[37] = 
{
	CMonster_t1941470938::get_offset_of_m_Config_2(),
	CMonster_t1941470938::get_offset_of_m_bIsJueSheng_3(),
	CMonster_t1941470938_StaticFields::get_offset_of_vec2Temp_4(),
	CMonster_t1941470938_StaticFields::get_offset_of_vecTempPos_5(),
	CMonster_t1941470938_StaticFields::get_offset_of_vecTempScale_6(),
	CMonster_t1941470938_StaticFields::get_offset_of_cTempColor_7(),
	CMonster_t1941470938::get_offset_of__tmText_8(),
	CMonster_t1941470938::get_offset_of__rigid_9(),
	CMonster_t1941470938::get_offset_of_m_nGUID_10(),
	CMonster_t1941470938::get_offset_of_m_szConfigId_11(),
	CMonster_t1941470938::get_offset_of__Trigger_12(),
	CMonster_t1941470938::get_offset_of__srMain_13(),
	CMonster_t1941470938::get_offset_of_m_fSize_14(),
	CMonster_t1941470938::get_offset_of_m_fRadius_15(),
	CMonster_t1941470938::get_offset_of_m_eMonsterType_16(),
	CMonster_t1941470938::get_offset_of_m_eMonsterBuildingType_17(),
	CMonster_t1941470938::get_offset_of_m_aryIntParam_18(),
	CMonster_t1941470938::get_offset_of_m_aryFloatParam_19(),
	CMonster_t1941470938::get_offset_of_m_bDead_20(),
	CMonster_t1941470938::get_offset_of_m_fDeadOccurTime_21(),
	CMonster_t1941470938::get_offset_of_m_fRebornTime_22(),
	CMonster_t1941470938::get_offset_of_m_Dir_23(),
	CMonster_t1941470938::get_offset_of_m_Pos_24(),
	CMonster_t1941470938::get_offset_of_m_vEjectSpeed_25(),
	CMonster_t1941470938::get_offset_of_m_fjectTotalDis_26(),
	CMonster_t1941470938::get_offset_of_m_bEjecting_27(),
	CMonster_t1941470938::get_offset_of_m_vEjectStartPos_28(),
	CMonster_t1941470938::get_offset_of_m_Player_29(),
	CMonster_t1941470938::get_offset_of_m_Ball_30(),
	CMonster_t1941470938::get_offset_of_m_goContainer_31(),
	CMonster_t1941470938::get_offset_of_m_bAutoReborn_32(),
	CMonster_t1941470938::get_offset_of_m_fRotation_33(),
	CMonster_t1941470938::get_offset_of_m_fRotationSpeed_34(),
	CMonster_t1941470938::get_offset_of_m_bNeedSync_35(),
	CMonster_t1941470938::get_offset_of_m_vecInitPos_36(),
	CMonster_t1941470938::get_offset_of_m_aniCiChuXian_37(),
	CMonster_t1941470938::get_offset_of_m_fCiChuXianTimeLeft_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (eMonsterType_t4095993688)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3292[5] = 
{
	eMonsterType_t4095993688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (ColorPalette_t2614655767), -1, sizeof(ColorPalette_t2614655767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3293[13] = 
{
	ColorPalette_t2614655767_StaticFields::get_offset_of__color_strs_bg_0(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__colors_bg_1(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__ranges_bg_light_2(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__color_strs_ring_3(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__colors_ring_4(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__color_strs_inner_5(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__colors_inner_6(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__color_strs_poison_7(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__colors_poison_8(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__color_strs_food_9(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__colors_food_10(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__color_strs_hedge_11(),
	ColorPalette_t2614655767_StaticFields::get_offset_of__colors_hedge_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (CtrlMode_t3902354508), -1, sizeof(CtrlMode_t3902354508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3294[51] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	CtrlMode_t3902354508_StaticFields::get_offset_of__ctrl_mode_7(),
	CtrlMode_t3902354508_StaticFields::get_offset_of__dir_indicator_type_8(),
	0,
	0,
	CtrlMode_t3902354508_StaticFields::get_offset_of__ball_scale_zoom_factor_11(),
	CtrlMode_t3902354508_StaticFields::get_offset_of__world_port_h_12(),
	CtrlMode_t3902354508_StaticFields::get_offset_of__world_port_v_13(),
	0,
	CtrlMode_t3902354508_StaticFields::get_offset_of__zoom_velocity_15(),
	CtrlMode_t3902354508_StaticFields::get_offset_of__zoom_time_16(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_vecTempPos1_17(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_vecTempPos2_18(),
	0,
	CtrlMode_t3902354508::get_offset_of_m_bFirstFuck_20(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_eCamMoveStatus_21(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_m_fAcctackRadiusMultiple_22(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_m_fSpitBallSpeed_23(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_ballLeft_24(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_ballRight_25(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_ballTop_26(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_ballBottom_27(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_fLeftBorderPos_28(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_fRightBorderPos_29(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_fTopBorderPos_30(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_fBottomBorderPos_31(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_balls_min_position_32(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_balls_max_position_33(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fLastDisRaise_34(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fLastDisDown_35(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fMaxCamSize_36(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fCameraProtectTime_37(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fCameraStayBeforeDownTime_38(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_eProtectStatus_39(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fShangFuDestCamSize_40(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fRangeBeforeProtect_41(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fRangeBeforeProtect2_42(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fShangFuXiShu_43(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fForecastCameraSizeChangeAmount_44(),
	0,
	CtrlMode_t3902354508_StaticFields::get_offset_of_m_fProtectTime_46(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_lstTemp_47(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fCameraSizeChangeSpeed_48(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fCameraProtectRaiseSpeed_49(),
	CtrlMode_t3902354508_StaticFields::get_offset_of_s_fCameraProtectDownSpeed_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (eCamerMoveStatus_t487596215)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3295[6] = 
{
	eCamerMoveStatus_t487596215::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (eCamerSizeProtectStatus_t2037340949)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3296[6] = 
{
	eCamerSizeProtectStatus_t2037340949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (CBallEffect_t2512637482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3297[3] = 
{
	CBallEffect_t2512637482::get_offset_of_m_aryContainer_2(),
	CBallEffect_t2512637482::get_offset_of_m_aryQianYao_3(),
	CBallEffect_t2512637482::get_offset_of_m_aryChiXu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (CBlackAndWhiteShader_t2223015440), -1, sizeof(CBlackAndWhiteShader_t2223015440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3298[6] = 
{
	CBlackAndWhiteShader_t2223015440_StaticFields::get_offset_of_s_Instance_2(),
	CBlackAndWhiteShader_t2223015440::get_offset_of_curShader_3(),
	CBlackAndWhiteShader_t2223015440::get_offset_of_grayScaleAmount_4(),
	CBlackAndWhiteShader_t2223015440::get_offset_of_curMaterial_5(),
	CBlackAndWhiteShader_t2223015440::get_offset_of_m_fFadeTotalTime_6(),
	CBlackAndWhiteShader_t2223015440::get_offset_of_m_bFading_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (CCosmosEffect_t495978253), -1, sizeof(CCosmosEffect_t495978253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3299[15] = 
{
	CCosmosEffect_t495978253_StaticFields::get_offset_of_vecTempPos_2(),
	CCosmosEffect_t495978253_StaticFields::get_offset_of_vecTempScale_3(),
	CCosmosEffect_t495978253::get_offset_of__canvasGroup_4(),
	CCosmosEffect_t495978253::get_offset_of__imgMain_5(),
	CCosmosEffect_t495978253::get_offset_of__sprMain_6(),
	CCosmosEffect_t495978253::get_offset_of_m_bUI_7(),
	CCosmosEffect_t495978253::get_offset_of__srRotationPic_8(),
	CCosmosEffect_t495978253::get_offset_of__imgRotationPic_9(),
	CCosmosEffect_t495978253::get_offset_of_m_bRotating_10(),
	CCosmosEffect_t495978253::get_offset_of_m_fRotatingSpeed_11(),
	CCosmosEffect_t495978253::get_offset_of_m_fRotatingAngle_12(),
	CCosmosEffect_t495978253::get_offset_of_m_bAlphaChange_13(),
	CCosmosEffect_t495978253::get_offset_of_m_fMaxAlpha_14(),
	CCosmosEffect_t495978253::get_offset_of_m_fMinAlpha_15(),
	CCosmosEffect_t495978253::get_offset_of_m_fAlphaChangeSpeed_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
