﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m4002292959(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m3688808850(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UniWebViewInterface_SendMessage_m2375818150(intptr_t ___namePtr0, intptr_t ___methodPtr1, intptr_t ___parameterPtr2);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[3] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m4002292959),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m3688808850),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UniWebViewInterface_SendMessage_m2375818150),
};
