﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Spine.EventData
struct EventData_t724759987;
// System.String
struct String_t;
// Spine.AnimationState
struct AnimationState_t3637309382;
// Spine.Unity.Modules.SkeletonRagdoll2D
struct SkeletonRagdoll2D_t2972821364;
// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[]
struct MixModeU5BU5D_t2594259929;
// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation>
struct Dictionary_2_t3799463910;
// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32>
struct Dictionary_2_t3091039290;
// System.Collections.Generic.List`1<Spine.Animation>
struct List_1_t2087858025;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t333824601;
// UnityEngine.Animator
struct Animator_t434523843;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip>
struct IEqualityComparer_1_t130870709;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// UniWebViewInterface/UnitySendMessageDelegate
struct UnitySendMessageDelegate_t3447265919;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Material
struct Material_t340375123;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t651787775;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t2098681813;
// UniWebView
struct UniWebView_t941983939;
// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material>
struct Dictionary_2_t556337403;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// CShuangYaoGan
struct CShuangYaoGan_t3597386049;
// Main
struct Main_t2227614074;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair>
struct List_1_t972525202;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t1744877482;
// Spine.Bone
struct Bone_t1086356328;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t1354683548;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// Spine.Unity.MeshRendererBuffers
struct MeshRendererBuffers_t756429994;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer>
struct List_1_t2139201959;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct SkeletonRendererDelegate_t3507789975;
// Spine.Unity.MeshGeneratorDelegate
struct MeshGeneratorDelegate_t1654156803;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t3748144825;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<Spine.Slot>
struct List_1_t692048146;
// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct InstructionDelegate_t2225421195;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t3700682020;
// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material>
struct Dictionary_2_t3424054551;
// Spine.Skeleton
struct Skeleton_t3686076450;
// Ball
struct Ball_t2206666566;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t3005360988;
// System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>
struct Dictionary_2_t930519433;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// Spine.Unity.SkeletonUtilityBone
struct SkeletonUtilityBone_t2225520134;
// Spine.Unity.SkeletonUtility
struct SkeletonUtility_t2980767925;
// UniWebView/PageStartedDelegate
struct PageStartedDelegate_t2830724344;
// UniWebView/PageFinishedDelegate
struct PageFinishedDelegate_t2717015276;
// UniWebView/PageErrorReceivedDelegate
struct PageErrorReceivedDelegate_t1724664023;
// UniWebView/MessageReceivedDelegate
struct MessageReceivedDelegate_t2288957136;
// UniWebView/ShouldCloseDelegate
struct ShouldCloseDelegate_t766319959;
// UniWebView/KeyCodeReceivedDelegate
struct KeyCodeReceivedDelegate_t3839084677;
// UniWebView/OreintationChangedDelegate
struct OreintationChangedDelegate_t1877368362;
// UniWebViewNativeListener
struct UniWebViewNativeListener_t1145263134;
// System.Collections.Generic.Dictionary`2<System.String,System.Action>
struct Dictionary_2_t1049633776;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>
struct Dictionary_2_t4030463511;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct SkeletonUtilityDelegate_t487527757;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_t3931555305;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone>
struct List_1_t3697594876;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint>
struct List_1_t2486457248;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// Spine.Unity.UpdateBonesDelegate
struct UpdateBonesDelegate_t735903178;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// System.Collections.Generic.List`1<Thorn>
struct List_1_t4012719951;
// System.Collections.Generic.List`1<Ball>
struct List_1_t3678741308;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct MecanimTranslator_t2363469064;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Texture
struct Texture_t3661962703;
// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh>
struct DoubleBuffered_1_t2489386064;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef WAITFORSPINEANIMATIONCOMPLETE_T3713264939_H
#define WAITFORSPINEANIMATIONCOMPLETE_T3713264939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineAnimationComplete
struct  WaitForSpineAnimationComplete_t3713264939  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineAnimationComplete::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineAnimationComplete_t3713264939, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEANIMATIONCOMPLETE_T3713264939_H
#ifndef WAITFORSPINEEVENT_T3999047839_H
#define WAITFORSPINEEVENT_T3999047839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineEvent
struct  WaitForSpineEvent_t3999047839  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Unity.WaitForSpineEvent::m_TargetEvent
	EventData_t724759987 * ___m_TargetEvent_0;
	// System.String Spine.Unity.WaitForSpineEvent::m_EventName
	String_t* ___m_EventName_1;
	// Spine.AnimationState Spine.Unity.WaitForSpineEvent::m_AnimationState
	AnimationState_t3637309382 * ___m_AnimationState_2;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_WasFired
	bool ___m_WasFired_3;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_unsubscribeAfterFiring
	bool ___m_unsubscribeAfterFiring_4;

public:
	inline static int32_t get_offset_of_m_TargetEvent_0() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_TargetEvent_0)); }
	inline EventData_t724759987 * get_m_TargetEvent_0() const { return ___m_TargetEvent_0; }
	inline EventData_t724759987 ** get_address_of_m_TargetEvent_0() { return &___m_TargetEvent_0; }
	inline void set_m_TargetEvent_0(EventData_t724759987 * value)
	{
		___m_TargetEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetEvent_0), value);
	}

	inline static int32_t get_offset_of_m_EventName_1() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_EventName_1)); }
	inline String_t* get_m_EventName_1() const { return ___m_EventName_1; }
	inline String_t** get_address_of_m_EventName_1() { return &___m_EventName_1; }
	inline void set_m_EventName_1(String_t* value)
	{
		___m_EventName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_1), value);
	}

	inline static int32_t get_offset_of_m_AnimationState_2() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_AnimationState_2)); }
	inline AnimationState_t3637309382 * get_m_AnimationState_2() const { return ___m_AnimationState_2; }
	inline AnimationState_t3637309382 ** get_address_of_m_AnimationState_2() { return &___m_AnimationState_2; }
	inline void set_m_AnimationState_2(AnimationState_t3637309382 * value)
	{
		___m_AnimationState_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationState_2), value);
	}

	inline static int32_t get_offset_of_m_WasFired_3() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_WasFired_3)); }
	inline bool get_m_WasFired_3() const { return ___m_WasFired_3; }
	inline bool* get_address_of_m_WasFired_3() { return &___m_WasFired_3; }
	inline void set_m_WasFired_3(bool value)
	{
		___m_WasFired_3 = value;
	}

	inline static int32_t get_offset_of_m_unsubscribeAfterFiring_4() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_unsubscribeAfterFiring_4)); }
	inline bool get_m_unsubscribeAfterFiring_4() const { return ___m_unsubscribeAfterFiring_4; }
	inline bool* get_address_of_m_unsubscribeAfterFiring_4() { return &___m_unsubscribeAfterFiring_4; }
	inline void set_m_unsubscribeAfterFiring_4(bool value)
	{
		___m_unsubscribeAfterFiring_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEEVENT_T3999047839_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3329131839_H
#define U3CSTARTU3EC__ITERATOR0_T3329131839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3329131839  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$this
	SkeletonRagdoll2D_t2972821364 * ___U24this_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24this_0)); }
	inline SkeletonRagdoll2D_t2972821364 * get_U24this_0() const { return ___U24this_0; }
	inline SkeletonRagdoll2D_t2972821364 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkeletonRagdoll2D_t2972821364 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3329131839_H
#ifndef MECANIMTRANSLATOR_T2363469064_H
#define MECANIMTRANSLATOR_T2363469064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct  MecanimTranslator_t2363469064  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonAnimator/MecanimTranslator::autoReset
	bool ___autoReset_0;
	// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[] Spine.Unity.SkeletonAnimator/MecanimTranslator::layerMixModes
	MixModeU5BU5D_t2594259929* ___layerMixModes_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::animationTable
	Dictionary_2_t3799463910 * ___animationTable_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipNameHashCodeTable
	Dictionary_2_t3091039290 * ___clipNameHashCodeTable_3;
	// System.Collections.Generic.List`1<Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::previousAnimations
	List_1_t2087858025 * ___previousAnimations_4;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipInfoCache
	List_1_t333824601 * ___clipInfoCache_5;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator::nextClipInfoCache
	List_1_t333824601 * ___nextClipInfoCache_6;
	// UnityEngine.Animator Spine.Unity.SkeletonAnimator/MecanimTranslator::animator
	Animator_t434523843 * ___animator_7;

public:
	inline static int32_t get_offset_of_autoReset_0() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___autoReset_0)); }
	inline bool get_autoReset_0() const { return ___autoReset_0; }
	inline bool* get_address_of_autoReset_0() { return &___autoReset_0; }
	inline void set_autoReset_0(bool value)
	{
		___autoReset_0 = value;
	}

	inline static int32_t get_offset_of_layerMixModes_1() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___layerMixModes_1)); }
	inline MixModeU5BU5D_t2594259929* get_layerMixModes_1() const { return ___layerMixModes_1; }
	inline MixModeU5BU5D_t2594259929** get_address_of_layerMixModes_1() { return &___layerMixModes_1; }
	inline void set_layerMixModes_1(MixModeU5BU5D_t2594259929* value)
	{
		___layerMixModes_1 = value;
		Il2CppCodeGenWriteBarrier((&___layerMixModes_1), value);
	}

	inline static int32_t get_offset_of_animationTable_2() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___animationTable_2)); }
	inline Dictionary_2_t3799463910 * get_animationTable_2() const { return ___animationTable_2; }
	inline Dictionary_2_t3799463910 ** get_address_of_animationTable_2() { return &___animationTable_2; }
	inline void set_animationTable_2(Dictionary_2_t3799463910 * value)
	{
		___animationTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___animationTable_2), value);
	}

	inline static int32_t get_offset_of_clipNameHashCodeTable_3() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___clipNameHashCodeTable_3)); }
	inline Dictionary_2_t3091039290 * get_clipNameHashCodeTable_3() const { return ___clipNameHashCodeTable_3; }
	inline Dictionary_2_t3091039290 ** get_address_of_clipNameHashCodeTable_3() { return &___clipNameHashCodeTable_3; }
	inline void set_clipNameHashCodeTable_3(Dictionary_2_t3091039290 * value)
	{
		___clipNameHashCodeTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___clipNameHashCodeTable_3), value);
	}

	inline static int32_t get_offset_of_previousAnimations_4() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___previousAnimations_4)); }
	inline List_1_t2087858025 * get_previousAnimations_4() const { return ___previousAnimations_4; }
	inline List_1_t2087858025 ** get_address_of_previousAnimations_4() { return &___previousAnimations_4; }
	inline void set_previousAnimations_4(List_1_t2087858025 * value)
	{
		___previousAnimations_4 = value;
		Il2CppCodeGenWriteBarrier((&___previousAnimations_4), value);
	}

	inline static int32_t get_offset_of_clipInfoCache_5() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___clipInfoCache_5)); }
	inline List_1_t333824601 * get_clipInfoCache_5() const { return ___clipInfoCache_5; }
	inline List_1_t333824601 ** get_address_of_clipInfoCache_5() { return &___clipInfoCache_5; }
	inline void set_clipInfoCache_5(List_1_t333824601 * value)
	{
		___clipInfoCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___clipInfoCache_5), value);
	}

	inline static int32_t get_offset_of_nextClipInfoCache_6() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___nextClipInfoCache_6)); }
	inline List_1_t333824601 * get_nextClipInfoCache_6() const { return ___nextClipInfoCache_6; }
	inline List_1_t333824601 ** get_address_of_nextClipInfoCache_6() { return &___nextClipInfoCache_6; }
	inline void set_nextClipInfoCache_6(List_1_t333824601 * value)
	{
		___nextClipInfoCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextClipInfoCache_6), value);
	}

	inline static int32_t get_offset_of_animator_7() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___animator_7)); }
	inline Animator_t434523843 * get_animator_7() const { return ___animator_7; }
	inline Animator_t434523843 ** get_address_of_animator_7() { return &___animator_7; }
	inline void set_animator_7(Animator_t434523843 * value)
	{
		___animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___animator_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MECANIMTRANSLATOR_T2363469064_H
#ifndef ANIMATIONCLIPEQUALITYCOMPARER_T3042476764_H
#define ANIMATIONCLIPEQUALITYCOMPARER_T3042476764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer
struct  AnimationClipEqualityComparer_t3042476764  : public RuntimeObject
{
public:

public:
};

struct AnimationClipEqualityComparer_t3042476764_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip> Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationClipEqualityComparer_t3042476764_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPEQUALITYCOMPARER_T3042476764_H
#ifndef INTEQUALITYCOMPARER_T2049031273_H
#define INTEQUALITYCOMPARER_T2049031273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer
struct  IntEqualityComparer_t2049031273  : public RuntimeObject
{
public:

public:
};

struct IntEqualityComparer_t2049031273_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(IntEqualityComparer_t2049031273_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEQUALITYCOMPARER_T2049031273_H
#ifndef SKELETONEXTENSIONS_T1502749191_H
#define SKELETONEXTENSIONS_T1502749191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonExtensions
struct  SkeletonExtensions_t1502749191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T1502749191_H
#ifndef SKELETONEXTENSIONS_T671502775_H
#define SKELETONEXTENSIONS_T671502775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonExtensions
struct  SkeletonExtensions_t671502775  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T671502775_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNIWEBVIEWHELPER_T1459904593_H
#define UNIWEBVIEWHELPER_T1459904593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewHelper
struct  UniWebViewHelper_t1459904593  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWHELPER_T1459904593_H
#ifndef U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T3141170647_H
#define U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T3141170647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1
struct  U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::<startMix>__0
	float ___U3CstartMixU3E__0_1;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::target
	float ___target_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::duration
	float ___duration_3;
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$this
	SkeletonRagdoll2D_t2972821364 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U3CstartMixU3E__0_1)); }
	inline float get_U3CstartMixU3E__0_1() const { return ___U3CstartMixU3E__0_1; }
	inline float* get_address_of_U3CstartMixU3E__0_1() { return &___U3CstartMixU3E__0_1; }
	inline void set_U3CstartMixU3E__0_1(float value)
	{
		___U3CstartMixU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24this_4)); }
	inline SkeletonRagdoll2D_t2972821364 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonRagdoll2D_t2972821364 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonRagdoll2D_t2972821364 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T3141170647_H
#ifndef UNIWEBVIEWINTERFACE_T2356025224_H
#define UNIWEBVIEWINTERFACE_T2356025224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewInterface
struct  UniWebViewInterface_t2356025224  : public RuntimeObject
{
public:

public:
};

struct UniWebViewInterface_t2356025224_StaticFields
{
public:
	// System.Boolean UniWebViewInterface::correctPlatform
	bool ___correctPlatform_1;
	// UniWebViewInterface/UnitySendMessageDelegate UniWebViewInterface::<>f__mg$cache0
	UnitySendMessageDelegate_t3447265919 * ___U3CU3Ef__mgU24cache0_2;

public:
	inline static int32_t get_offset_of_correctPlatform_1() { return static_cast<int32_t>(offsetof(UniWebViewInterface_t2356025224_StaticFields, ___correctPlatform_1)); }
	inline bool get_correctPlatform_1() const { return ___correctPlatform_1; }
	inline bool* get_address_of_correctPlatform_1() { return &___correctPlatform_1; }
	inline void set_correctPlatform_1(bool value)
	{
		___correctPlatform_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(UniWebViewInterface_t2356025224_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline UnitySendMessageDelegate_t3447265919 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline UnitySendMessageDelegate_t3447265919 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(UnitySendMessageDelegate_t3447265919 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWINTERFACE_T2356025224_H
#ifndef U3CGETHTMLCONTENTU3EC__ANONSTOREY0_T803715261_H
#define U3CGETHTMLCONTENTU3EC__ANONSTOREY0_T803715261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/<GetHTMLContent>c__AnonStorey0
struct  U3CGetHTMLContentU3Ec__AnonStorey0_t803715261  : public RuntimeObject
{
public:
	// System.Action`1<System.String> UniWebView/<GetHTMLContent>c__AnonStorey0::handler
	Action_1_t2019918284 * ___handler_0;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CGetHTMLContentU3Ec__AnonStorey0_t803715261, ___handler_0)); }
	inline Action_1_t2019918284 * get_handler_0() const { return ___handler_0; }
	inline Action_1_t2019918284 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(Action_1_t2019918284 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETHTMLCONTENTU3EC__ANONSTOREY0_T803715261_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef WAITFORSPINETRACKENTRYEND_T2617682820_H
#define WAITFORSPINETRACKENTRYEND_T2617682820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineTrackEntryEnd
struct  WaitForSpineTrackEntryEnd_t2617682820  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineTrackEntryEnd::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineTrackEntryEnd_t2617682820, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINETRACKENTRYEND_T2617682820_H
#ifndef UNIWEBVIEWNATIVERESULTPAYLOAD_T4072739617_H
#define UNIWEBVIEWNATIVERESULTPAYLOAD_T4072739617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeResultPayload
struct  UniWebViewNativeResultPayload_t4072739617  : public RuntimeObject
{
public:
	// System.String UniWebViewNativeResultPayload::identifier
	String_t* ___identifier_0;
	// System.String UniWebViewNativeResultPayload::resultCode
	String_t* ___resultCode_1;
	// System.String UniWebViewNativeResultPayload::data
	String_t* ___data_2;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t4072739617, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_resultCode_1() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t4072739617, ___resultCode_1)); }
	inline String_t* get_resultCode_1() const { return ___resultCode_1; }
	inline String_t** get_address_of_resultCode_1() { return &___resultCode_1; }
	inline void set_resultCode_1(String_t* value)
	{
		___resultCode_1 = value;
		Il2CppCodeGenWriteBarrier((&___resultCode_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t4072739617, ___data_2)); }
	inline String_t* get_data_2() const { return ___data_2; }
	inline String_t** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(String_t* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWNATIVERESULTPAYLOAD_T4072739617_H
#ifndef UNIWEBVIEWMESSAGE_T2441068380_H
#define UNIWEBVIEWMESSAGE_T2441068380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewMessage
struct  UniWebViewMessage_t2441068380 
{
public:
	union
	{
		struct
		{
			// System.String UniWebViewMessage::<RawMessage>k__BackingField
			String_t* ___U3CRawMessageU3Ek__BackingField_0;
			// System.String UniWebViewMessage::<Scheme>k__BackingField
			String_t* ___U3CSchemeU3Ek__BackingField_1;
			// System.String UniWebViewMessage::<Path>k__BackingField
			String_t* ___U3CPathU3Ek__BackingField_2;
			// System.Collections.Generic.Dictionary`2<System.String,System.String> UniWebViewMessage::<Args>k__BackingField
			Dictionary_2_t1632706988 * ___U3CArgsU3Ek__BackingField_3;
		};
		uint8_t UniWebViewMessage_t2441068380__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CRawMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CRawMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CRawMessageU3Ek__BackingField_0() const { return ___U3CRawMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CRawMessageU3Ek__BackingField_0() { return &___U3CRawMessageU3Ek__BackingField_0; }
	inline void set_U3CRawMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CRawMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSchemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CSchemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CSchemeU3Ek__BackingField_1() const { return ___U3CSchemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSchemeU3Ek__BackingField_1() { return &___U3CSchemeU3Ek__BackingField_1; }
	inline void set_U3CSchemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CSchemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSchemeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CPathU3Ek__BackingField_2)); }
	inline String_t* get_U3CPathU3Ek__BackingField_2() const { return ___U3CPathU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_2() { return &___U3CPathU3Ek__BackingField_2; }
	inline void set_U3CPathU3Ek__BackingField_2(String_t* value)
	{
		___U3CPathU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CArgsU3Ek__BackingField_3)); }
	inline Dictionary_2_t1632706988 * get_U3CArgsU3Ek__BackingField_3() const { return ___U3CArgsU3Ek__BackingField_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CArgsU3Ek__BackingField_3() { return &___U3CArgsU3Ek__BackingField_3; }
	inline void set_U3CArgsU3Ek__BackingField_3(Dictionary_2_t1632706988 * value)
	{
		___U3CArgsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniWebViewMessage
struct UniWebViewMessage_t2441068380_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CRawMessageU3Ek__BackingField_0;
			char* ___U3CSchemeU3Ek__BackingField_1;
			char* ___U3CPathU3Ek__BackingField_2;
			Dictionary_2_t1632706988 * ___U3CArgsU3Ek__BackingField_3;
		};
		uint8_t UniWebViewMessage_t2441068380__padding[1];
	};
};
// Native definition for COM marshalling of UniWebViewMessage
struct UniWebViewMessage_t2441068380_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CRawMessageU3Ek__BackingField_0;
			Il2CppChar* ___U3CSchemeU3Ek__BackingField_1;
			Il2CppChar* ___U3CPathU3Ek__BackingField_2;
			Dictionary_2_t1632706988 * ___U3CArgsU3Ek__BackingField_3;
		};
		uint8_t UniWebViewMessage_t2441068380__padding[1];
	};
};
#endif // UNIWEBVIEWMESSAGE_T2441068380_H
#ifndef U24ARRAYTYPEU3D36_T120960362_H
#define U24ARRAYTYPEU3D36_T120960362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=36
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D36_t120960362 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D36_t120960362__padding[36];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D36_T120960362_H
#ifndef HIERARCHY_T2161623951_H
#define HIERARCHY_T2161623951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment/Hierarchy
struct  Hierarchy_t2161623951 
{
public:
	// System.String Spine.Unity.SpineAttachment/Hierarchy::skin
	String_t* ___skin_0;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::slot
	String_t* ___slot_1;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_skin_0() { return static_cast<int32_t>(offsetof(Hierarchy_t2161623951, ___skin_0)); }
	inline String_t* get_skin_0() const { return ___skin_0; }
	inline String_t** get_address_of_skin_0() { return &___skin_0; }
	inline void set_skin_0(String_t* value)
	{
		___skin_0 = value;
		Il2CppCodeGenWriteBarrier((&___skin_0), value);
	}

	inline static int32_t get_offset_of_slot_1() { return static_cast<int32_t>(offsetof(Hierarchy_t2161623951, ___slot_1)); }
	inline String_t* get_slot_1() const { return ___slot_1; }
	inline String_t** get_address_of_slot_1() { return &___slot_1; }
	inline void set_slot_1(String_t* value)
	{
		___slot_1 = value;
		Il2CppCodeGenWriteBarrier((&___slot_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Hierarchy_t2161623951, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t2161623951_marshaled_pinvoke
{
	char* ___skin_0;
	char* ___slot_1;
	char* ___name_2;
};
// Native definition for COM marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t2161623951_marshaled_com
{
	Il2CppChar* ___skin_0;
	Il2CppChar* ___slot_1;
	Il2CppChar* ___name_2;
};
#endif // HIERARCHY_T2161623951_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef U24ARRAYTYPEU3D20_T1702832645_H
#define U24ARRAYTYPEU3D20_T1702832645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832645 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832645__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832645_H
#ifndef U24ARRAYTYPEU3D76_T2446559190_H
#define U24ARRAYTYPEU3D76_T2446559190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=76
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D76_t2446559190 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D76_t2446559190__padding[76];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D76_T2446559190_H
#ifndef COLORTHORNPARAM_T1112536431_H
#define COLORTHORNPARAM_T1112536431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Thorn/ColorThornParam
struct  ColorThornParam_t1112536431 
{
public:
	// System.Single Thorn/ColorThornParam::thorn_size
	float ___thorn_size_0;
	// System.Single Thorn/ColorThornParam::ball_size
	float ___ball_size_1;
	// System.Single Thorn/ColorThornParam::reborn_time
	float ___reborn_time_2;

public:
	inline static int32_t get_offset_of_thorn_size_0() { return static_cast<int32_t>(offsetof(ColorThornParam_t1112536431, ___thorn_size_0)); }
	inline float get_thorn_size_0() const { return ___thorn_size_0; }
	inline float* get_address_of_thorn_size_0() { return &___thorn_size_0; }
	inline void set_thorn_size_0(float value)
	{
		___thorn_size_0 = value;
	}

	inline static int32_t get_offset_of_ball_size_1() { return static_cast<int32_t>(offsetof(ColorThornParam_t1112536431, ___ball_size_1)); }
	inline float get_ball_size_1() const { return ___ball_size_1; }
	inline float* get_address_of_ball_size_1() { return &___ball_size_1; }
	inline void set_ball_size_1(float value)
	{
		___ball_size_1 = value;
	}

	inline static int32_t get_offset_of_reborn_time_2() { return static_cast<int32_t>(offsetof(ColorThornParam_t1112536431, ___reborn_time_2)); }
	inline float get_reborn_time_2() const { return ___reborn_time_2; }
	inline float* get_address_of_reborn_time_2() { return &___reborn_time_2; }
	inline void set_reborn_time_2(float value)
	{
		___reborn_time_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTHORNPARAM_T1112536431_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D48_T1336283963_H
#define U24ARRAYTYPEU3D48_T1336283963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D48_t1336283963 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D48_t1336283963__padding[48];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D48_T1336283963_H
#ifndef U24ARRAYTYPEU3D1024_T3853988145_H
#define U24ARRAYTYPEU3D1024_T3853988145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=1024
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D1024_t3853988145 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D1024_t3853988145__padding[1024];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D1024_T3853988145_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef TRANSFORMPAIR_T3795417756_H
#define TRANSFORMPAIR_T3795417756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct  TransformPair_t3795417756 
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::dest
	Transform_t3600365921 * ___dest_0;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::src
	Transform_t3600365921 * ___src_1;

public:
	inline static int32_t get_offset_of_dest_0() { return static_cast<int32_t>(offsetof(TransformPair_t3795417756, ___dest_0)); }
	inline Transform_t3600365921 * get_dest_0() const { return ___dest_0; }
	inline Transform_t3600365921 ** get_address_of_dest_0() { return &___dest_0; }
	inline void set_dest_0(Transform_t3600365921 * value)
	{
		___dest_0 = value;
		Il2CppCodeGenWriteBarrier((&___dest_0), value);
	}

	inline static int32_t get_offset_of_src_1() { return static_cast<int32_t>(offsetof(TransformPair_t3795417756, ___src_1)); }
	inline Transform_t3600365921 * get_src_1() const { return ___src_1; }
	inline Transform_t3600365921 ** get_address_of_src_1() { return &___src_1; }
	inline void set_src_1(Transform_t3600365921 * value)
	{
		___src_1 = value;
		Il2CppCodeGenWriteBarrier((&___src_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t3795417756_marshaled_pinvoke
{
	Transform_t3600365921 * ___dest_0;
	Transform_t3600365921 * ___src_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t3795417756_marshaled_com
{
	Transform_t3600365921 * ___dest_0;
	Transform_t3600365921 * ___src_1;
};
#endif // TRANSFORMPAIR_T3795417756_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef MATERIALTEXTUREPAIR_T1637207048_H
#define MATERIALTEXTUREPAIR_T1637207048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct  MaterialTexturePair_t1637207048 
{
public:
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::texture2D
	Texture2D_t3840446185 * ___texture2D_0;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::material
	Material_t340375123 * ___material_1;

public:
	inline static int32_t get_offset_of_texture2D_0() { return static_cast<int32_t>(offsetof(MaterialTexturePair_t1637207048, ___texture2D_0)); }
	inline Texture2D_t3840446185 * get_texture2D_0() const { return ___texture2D_0; }
	inline Texture2D_t3840446185 ** get_address_of_texture2D_0() { return &___texture2D_0; }
	inline void set_texture2D_0(Texture2D_t3840446185 * value)
	{
		___texture2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture2D_0), value);
	}

	inline static int32_t get_offset_of_material_1() { return static_cast<int32_t>(offsetof(MaterialTexturePair_t1637207048, ___material_1)); }
	inline Material_t340375123 * get_material_1() const { return ___material_1; }
	inline Material_t340375123 ** get_address_of_material_1() { return &___material_1; }
	inline void set_material_1(Material_t340375123 * value)
	{
		___material_1 = value;
		Il2CppCodeGenWriteBarrier((&___material_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_t1637207048_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___texture2D_0;
	Material_t340375123 * ___material_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_t1637207048_marshaled_com
{
	Texture2D_t3840446185 * ___texture2D_0;
	Material_t340375123 * ___material_1;
};
#endif // MATERIALTEXTUREPAIR_T1637207048_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef EMAPOBJTYPE_T1356972478_H
#define EMAPOBJTYPE_T1356972478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapObj/eMapObjType
struct  eMapObjType_t1356972478 
{
public:
	// System.Int32 MapObj/eMapObjType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMapObjType_t1356972478, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAPOBJTYPE_T1356972478_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef LEVEL_T4211844589_H
#define LEVEL_T4211844589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewLogger/Level
struct  Level_t4211844589 
{
public:
	// System.Int32 UniWebViewLogger/Level::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Level_t4211844589, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T4211844589_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef UNIWEBVIEWTOOLBARPOSITION_T2230581447_H
#define UNIWEBVIEWTOOLBARPOSITION_T2230581447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewToolbarPosition
struct  UniWebViewToolbarPosition_t2230581447 
{
public:
	// System.Int32 UniWebViewToolbarPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UniWebViewToolbarPosition_t2230581447, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTOOLBARPOSITION_T2230581447_H
#ifndef UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#define UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewTransitionEdge
struct  UniWebViewTransitionEdge_t2354894166 
{
public:
	// System.Int32 UniWebViewTransitionEdge::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UniWebViewTransitionEdge_t2354894166, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=36 <PrivateImplementationDetails>::$field-39343524298E63D94C076EE07AB61EFA7807E0B0
	U24ArrayTypeU3D36_t120960362  ___U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-8658990BAD6546E619D8A5C4F90BCF3F089E0953
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-739C505E9F0985CE1E08892BC46BE5E839FF061A
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-35FDBB6669F521B572D4AD71DD77E77F43C1B71B
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3;
	// <PrivateImplementationDetails>/$ArrayType=1024 <PrivateImplementationDetails>::$field-C660A6C2191F7639613A25F2A723ED819CE3C613
	U24ArrayTypeU3D1024_t3853988145  ___U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4;
	// <PrivateImplementationDetails>/$ArrayType=76 <PrivateImplementationDetails>::$field-82DD799C96AA69293BA404D79A459D6F9750AE19
	U24ArrayTypeU3D76_t2446559190  ___U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5;
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-B6E6EA57C32297E83203480EE50A22C3581AA09C
	U24ArrayTypeU3D20_t1702832645  ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7;

public:
	inline static int32_t get_offset_of_U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0)); }
	inline U24ArrayTypeU3D36_t120960362  get_U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0() const { return ___U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0; }
	inline U24ArrayTypeU3D36_t120960362 * get_address_of_U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0() { return &___U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0; }
	inline void set_U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0(U24ArrayTypeU3D36_t120960362  value)
	{
		___U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() const { return ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() { return &___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1; }
	inline void set_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() const { return ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() { return &___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2; }
	inline void set_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() const { return ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() { return &___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3; }
	inline void set_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4)); }
	inline U24ArrayTypeU3D1024_t3853988145  get_U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4() const { return ___U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4; }
	inline U24ArrayTypeU3D1024_t3853988145 * get_address_of_U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4() { return &___U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4; }
	inline void set_U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4(U24ArrayTypeU3D1024_t3853988145  value)
	{
		___U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5)); }
	inline U24ArrayTypeU3D76_t2446559190  get_U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5() const { return ___U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5; }
	inline U24ArrayTypeU3D76_t2446559190 * get_address_of_U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5() { return &___U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5; }
	inline void set_U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5(U24ArrayTypeU3D76_t2446559190  value)
	{
		___U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6)); }
	inline U24ArrayTypeU3D20_t1702832645  get_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6() const { return ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6; }
	inline U24ArrayTypeU3D20_t1702832645 * get_address_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6() { return &___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6; }
	inline void set_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6(U24ArrayTypeU3D20_t1702832645  value)
	{
		___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7() const { return ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7() { return &___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7; }
	inline void set_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef MIXMODE_T3058145032_H
#define MIXMODE_T3058145032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode
struct  MixMode_t3058145032 
{
public:
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixMode_t3058145032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXMODE_T3058145032_H
#ifndef ETHORNTYPE_T2871459091_H
#define ETHORNTYPE_T2871459091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Thorn/eThornType
struct  eThornType_t2871459091 
{
public:
	// System.Int32 Thorn/eThornType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eThornType_t2871459091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETHORNTYPE_T2871459091_H
#ifndef SPINEATLASREGION_T3654419954_H
#define SPINEATLASREGION_T3654419954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAtlasRegion
struct  SpineAtlasRegion_t3654419954  : public PropertyAttribute_t3677895545
{
public:
	// System.String Spine.Unity.SpineAtlasRegion::atlasAssetField
	String_t* ___atlasAssetField_0;

public:
	inline static int32_t get_offset_of_atlasAssetField_0() { return static_cast<int32_t>(offsetof(SpineAtlasRegion_t3654419954, ___atlasAssetField_0)); }
	inline String_t* get_atlasAssetField_0() const { return ___atlasAssetField_0; }
	inline String_t** get_address_of_atlasAssetField_0() { return &___atlasAssetField_0; }
	inline void set_atlasAssetField_0(String_t* value)
	{
		___atlasAssetField_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssetField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATLASREGION_T3654419954_H
#ifndef MODE_T3700196520_H
#define MODE_T3700196520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone/Mode
struct  Mode_t3700196520 
{
public:
	// System.Int32 Spine.Unity.SkeletonUtilityBone/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3700196520, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3700196520_H
#ifndef SPINEATTRIBUTEBASE_T340149836_H
#define SPINEATTRIBUTEBASE_T340149836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttributeBase
struct  SpineAttributeBase_t340149836  : public PropertyAttribute_t3677895545
{
public:
	// System.String Spine.Unity.SpineAttributeBase::dataField
	String_t* ___dataField_0;
	// System.String Spine.Unity.SpineAttributeBase::startsWith
	String_t* ___startsWith_1;
	// System.Boolean Spine.Unity.SpineAttributeBase::includeNone
	bool ___includeNone_2;
	// System.Boolean Spine.Unity.SpineAttributeBase::fallbackToTextField
	bool ___fallbackToTextField_3;

public:
	inline static int32_t get_offset_of_dataField_0() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___dataField_0)); }
	inline String_t* get_dataField_0() const { return ___dataField_0; }
	inline String_t** get_address_of_dataField_0() { return &___dataField_0; }
	inline void set_dataField_0(String_t* value)
	{
		___dataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataField_0), value);
	}

	inline static int32_t get_offset_of_startsWith_1() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___startsWith_1)); }
	inline String_t* get_startsWith_1() const { return ___startsWith_1; }
	inline String_t** get_address_of_startsWith_1() { return &___startsWith_1; }
	inline void set_startsWith_1(String_t* value)
	{
		___startsWith_1 = value;
		Il2CppCodeGenWriteBarrier((&___startsWith_1), value);
	}

	inline static int32_t get_offset_of_includeNone_2() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___includeNone_2)); }
	inline bool get_includeNone_2() const { return ___includeNone_2; }
	inline bool* get_address_of_includeNone_2() { return &___includeNone_2; }
	inline void set_includeNone_2(bool value)
	{
		___includeNone_2 = value;
	}

	inline static int32_t get_offset_of_fallbackToTextField_3() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___fallbackToTextField_3)); }
	inline bool get_fallbackToTextField_3() const { return ___fallbackToTextField_3; }
	inline bool* get_address_of_fallbackToTextField_3() { return &___fallbackToTextField_3; }
	inline void set_fallbackToTextField_3(bool value)
	{
		___fallbackToTextField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTRIBUTEBASE_T340149836_H
#ifndef SPINEPATHCONSTRAINT_T3765886426_H
#define SPINEPATHCONSTRAINT_T3765886426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpinePathConstraint
struct  SpinePathConstraint_t3765886426  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEPATHCONSTRAINT_T3765886426_H
#ifndef SPINEEVENT_T2537401913_H
#define SPINEEVENT_T2537401913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineEvent
struct  SpineEvent_t2537401913  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENT_T2537401913_H
#ifndef UNIWEBVIEWLOGGER_T952558916_H
#define UNIWEBVIEWLOGGER_T952558916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewLogger
struct  UniWebViewLogger_t952558916  : public RuntimeObject
{
public:
	// UniWebViewLogger/Level UniWebViewLogger::level
	int32_t ___level_1;

public:
	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(UniWebViewLogger_t952558916, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}
};

struct UniWebViewLogger_t952558916_StaticFields
{
public:
	// UniWebViewLogger UniWebViewLogger::instance
	UniWebViewLogger_t952558916 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(UniWebViewLogger_t952558916_StaticFields, ___instance_0)); }
	inline UniWebViewLogger_t952558916 * get_instance_0() const { return ___instance_0; }
	inline UniWebViewLogger_t952558916 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(UniWebViewLogger_t952558916 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWLOGGER_T952558916_H
#ifndef SPINETRANSFORMCONSTRAINT_T1324873370_H
#define SPINETRANSFORMCONSTRAINT_T1324873370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineTransformConstraint
struct  SpineTransformConstraint_t1324873370  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINETRANSFORMCONSTRAINT_T1324873370_H
#ifndef SPINESKIN_T4048709426_H
#define SPINESKIN_T4048709426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSkin
struct  SpineSkin_t4048709426  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKIN_T4048709426_H
#ifndef SPINEANIMATION_T42895223_H
#define SPINEANIMATION_T42895223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAnimation
struct  SpineAnimation_t42895223  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATION_T42895223_H
#ifndef SPINEATTACHMENT_T743638603_H
#define SPINEATTACHMENT_T743638603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment
struct  SpineAttachment_t743638603  : public SpineAttributeBase_t340149836
{
public:
	// System.Boolean Spine.Unity.SpineAttachment::returnAttachmentPath
	bool ___returnAttachmentPath_4;
	// System.Boolean Spine.Unity.SpineAttachment::currentSkinOnly
	bool ___currentSkinOnly_5;
	// System.Boolean Spine.Unity.SpineAttachment::placeholdersOnly
	bool ___placeholdersOnly_6;
	// System.String Spine.Unity.SpineAttachment::skinField
	String_t* ___skinField_7;
	// System.String Spine.Unity.SpineAttachment::slotField
	String_t* ___slotField_8;

public:
	inline static int32_t get_offset_of_returnAttachmentPath_4() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___returnAttachmentPath_4)); }
	inline bool get_returnAttachmentPath_4() const { return ___returnAttachmentPath_4; }
	inline bool* get_address_of_returnAttachmentPath_4() { return &___returnAttachmentPath_4; }
	inline void set_returnAttachmentPath_4(bool value)
	{
		___returnAttachmentPath_4 = value;
	}

	inline static int32_t get_offset_of_currentSkinOnly_5() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___currentSkinOnly_5)); }
	inline bool get_currentSkinOnly_5() const { return ___currentSkinOnly_5; }
	inline bool* get_address_of_currentSkinOnly_5() { return &___currentSkinOnly_5; }
	inline void set_currentSkinOnly_5(bool value)
	{
		___currentSkinOnly_5 = value;
	}

	inline static int32_t get_offset_of_placeholdersOnly_6() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___placeholdersOnly_6)); }
	inline bool get_placeholdersOnly_6() const { return ___placeholdersOnly_6; }
	inline bool* get_address_of_placeholdersOnly_6() { return &___placeholdersOnly_6; }
	inline void set_placeholdersOnly_6(bool value)
	{
		___placeholdersOnly_6 = value;
	}

	inline static int32_t get_offset_of_skinField_7() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___skinField_7)); }
	inline String_t* get_skinField_7() const { return ___skinField_7; }
	inline String_t** get_address_of_skinField_7() { return &___skinField_7; }
	inline void set_skinField_7(String_t* value)
	{
		___skinField_7 = value;
		Il2CppCodeGenWriteBarrier((&___skinField_7), value);
	}

	inline static int32_t get_offset_of_slotField_8() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___slotField_8)); }
	inline String_t* get_slotField_8() const { return ___slotField_8; }
	inline String_t** get_address_of_slotField_8() { return &___slotField_8; }
	inline void set_slotField_8(String_t* value)
	{
		___slotField_8 = value;
		Il2CppCodeGenWriteBarrier((&___slotField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTACHMENT_T743638603_H
#ifndef SPINEBONE_T3329529041_H
#define SPINEBONE_T3329529041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineBone
struct  SpineBone_t3329529041  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEBONE_T3329529041_H
#ifndef SPINEIKCONSTRAINT_T1587650654_H
#define SPINEIKCONSTRAINT_T1587650654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineIkConstraint
struct  SpineIkConstraint_t1587650654  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEIKCONSTRAINT_T1587650654_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SPINESLOT_T2906857509_H
#define SPINESLOT_T2906857509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSlot
struct  SpineSlot_t2906857509  : public SpineAttributeBase_t340149836
{
public:
	// System.Boolean Spine.Unity.SpineSlot::containsBoundingBoxes
	bool ___containsBoundingBoxes_4;

public:
	inline static int32_t get_offset_of_containsBoundingBoxes_4() { return static_cast<int32_t>(offsetof(SpineSlot_t2906857509, ___containsBoundingBoxes_4)); }
	inline bool get_containsBoundingBoxes_4() const { return ___containsBoundingBoxes_4; }
	inline bool* get_address_of_containsBoundingBoxes_4() { return &___containsBoundingBoxes_4; }
	inline void set_containsBoundingBoxes_4(bool value)
	{
		___containsBoundingBoxes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESLOT_T2906857509_H
#ifndef INSTRUCTIONDELEGATE_T2225421195_H
#define INSTRUCTIONDELEGATE_T2225421195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct  InstructionDelegate_t2225421195  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTIONDELEGATE_T2225421195_H
#ifndef SKELETONUTILITYDELEGATE_T487527757_H
#define SKELETONUTILITYDELEGATE_T487527757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct  SkeletonUtilityDelegate_t487527757  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYDELEGATE_T487527757_H
#ifndef SKELETONRENDERERDELEGATE_T3507789975_H
#define SKELETONRENDERERDELEGATE_T3507789975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct  SkeletonRendererDelegate_t3507789975  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERDELEGATE_T3507789975_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UNITYSENDMESSAGEDELEGATE_T3447265919_H
#define UNITYSENDMESSAGEDELEGATE_T3447265919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewInterface/UnitySendMessageDelegate
struct  UnitySendMessageDelegate_t3447265919  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSENDMESSAGEDELEGATE_T3447265919_H
#ifndef OREINTATIONCHANGEDDELEGATE_T1877368362_H
#define OREINTATIONCHANGEDDELEGATE_T1877368362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/OreintationChangedDelegate
struct  OreintationChangedDelegate_t1877368362  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OREINTATIONCHANGEDDELEGATE_T1877368362_H
#ifndef KEYCODERECEIVEDDELEGATE_T3839084677_H
#define KEYCODERECEIVEDDELEGATE_T3839084677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/KeyCodeReceivedDelegate
struct  KeyCodeReceivedDelegate_t3839084677  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODERECEIVEDDELEGATE_T3839084677_H
#ifndef MESSAGERECEIVEDDELEGATE_T2288957136_H
#define MESSAGERECEIVEDDELEGATE_T2288957136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/MessageReceivedDelegate
struct  MessageReceivedDelegate_t2288957136  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDDELEGATE_T2288957136_H
#ifndef PAGEFINISHEDDELEGATE_T2717015276_H
#define PAGEFINISHEDDELEGATE_T2717015276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/PageFinishedDelegate
struct  PageFinishedDelegate_t2717015276  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEFINISHEDDELEGATE_T2717015276_H
#ifndef SHOULDCLOSEDELEGATE_T766319959_H
#define SHOULDCLOSEDELEGATE_T766319959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/ShouldCloseDelegate
struct  ShouldCloseDelegate_t766319959  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOULDCLOSEDELEGATE_T766319959_H
#ifndef PAGESTARTEDDELEGATE_T2830724344_H
#define PAGESTARTEDDELEGATE_T2830724344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/PageStartedDelegate
struct  PageStartedDelegate_t2830724344  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGESTARTEDDELEGATE_T2830724344_H
#ifndef PAGEERRORRECEIVEDDELEGATE_T1724664023_H
#define PAGEERRORRECEIVEDDELEGATE_T1724664023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/PageErrorReceivedDelegate
struct  PageErrorReceivedDelegate_t1724664023  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEERRORRECEIVEDDELEGATE_T1724664023_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SLOTBLENDMODES_T1277474191_H
#define SLOTBLENDMODES_T1277474191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes
struct  SlotBlendModes_t1277474191  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::multiplyMaterialSource
	Material_t340375123 * ___multiplyMaterialSource_3;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::screenMaterialSource
	Material_t340375123 * ___screenMaterialSource_4;
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes::texture
	Texture2D_t3840446185 * ___texture_5;
	// System.Boolean Spine.Unity.Modules.SlotBlendModes::<Applied>k__BackingField
	bool ___U3CAppliedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_multiplyMaterialSource_3() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___multiplyMaterialSource_3)); }
	inline Material_t340375123 * get_multiplyMaterialSource_3() const { return ___multiplyMaterialSource_3; }
	inline Material_t340375123 ** get_address_of_multiplyMaterialSource_3() { return &___multiplyMaterialSource_3; }
	inline void set_multiplyMaterialSource_3(Material_t340375123 * value)
	{
		___multiplyMaterialSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyMaterialSource_3), value);
	}

	inline static int32_t get_offset_of_screenMaterialSource_4() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___screenMaterialSource_4)); }
	inline Material_t340375123 * get_screenMaterialSource_4() const { return ___screenMaterialSource_4; }
	inline Material_t340375123 ** get_address_of_screenMaterialSource_4() { return &___screenMaterialSource_4; }
	inline void set_screenMaterialSource_4(Material_t340375123 * value)
	{
		___screenMaterialSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenMaterialSource_4), value);
	}

	inline static int32_t get_offset_of_texture_5() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___texture_5)); }
	inline Texture2D_t3840446185 * get_texture_5() const { return ___texture_5; }
	inline Texture2D_t3840446185 ** get_address_of_texture_5() { return &___texture_5; }
	inline void set_texture_5(Texture2D_t3840446185 * value)
	{
		___texture_5 = value;
		Il2CppCodeGenWriteBarrier((&___texture_5), value);
	}

	inline static int32_t get_offset_of_U3CAppliedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___U3CAppliedU3Ek__BackingField_6)); }
	inline bool get_U3CAppliedU3Ek__BackingField_6() const { return ___U3CAppliedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CAppliedU3Ek__BackingField_6() { return &___U3CAppliedU3Ek__BackingField_6; }
	inline void set_U3CAppliedU3Ek__BackingField_6(bool value)
	{
		___U3CAppliedU3Ek__BackingField_6 = value;
	}
};

struct SlotBlendModes_t1277474191_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material> Spine.Unity.Modules.SlotBlendModes::materialTable
	Dictionary_2_t556337403 * ___materialTable_2;

public:
	inline static int32_t get_offset_of_materialTable_2() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191_StaticFields, ___materialTable_2)); }
	inline Dictionary_2_t556337403 * get_materialTable_2() const { return ___materialTable_2; }
	inline Dictionary_2_t556337403 ** get_address_of_materialTable_2() { return &___materialTable_2; }
	inline void set_materialTable_2(Dictionary_2_t556337403 * value)
	{
		___materialTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTBLENDMODES_T1277474191_H
#ifndef WORLDBORDER_T1275663748_H
#define WORLDBORDER_T1275663748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldBorder
struct  WorldBorder_t1275663748  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.BoxCollider2D WorldBorder::_colliderEdge
	BoxCollider2D_t3581341831 * ____colliderEdge_2;
	// UnityEngine.LineRenderer WorldBorder::_lr
	LineRenderer_t3154350270 * ____lr_3;
	// UnityEngine.Vector3 WorldBorder::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// UnityEngine.Vector2 WorldBorder::vecTemp2
	Vector2_t2156229523  ___vecTemp2_5;
	// UnityEngine.Vector2[] WorldBorder::m_aryColliderPoints
	Vector2U5BU5D_t1457185986* ___m_aryColliderPoints_6;

public:
	inline static int32_t get_offset_of__colliderEdge_2() { return static_cast<int32_t>(offsetof(WorldBorder_t1275663748, ____colliderEdge_2)); }
	inline BoxCollider2D_t3581341831 * get__colliderEdge_2() const { return ____colliderEdge_2; }
	inline BoxCollider2D_t3581341831 ** get_address_of__colliderEdge_2() { return &____colliderEdge_2; }
	inline void set__colliderEdge_2(BoxCollider2D_t3581341831 * value)
	{
		____colliderEdge_2 = value;
		Il2CppCodeGenWriteBarrier((&____colliderEdge_2), value);
	}

	inline static int32_t get_offset_of__lr_3() { return static_cast<int32_t>(offsetof(WorldBorder_t1275663748, ____lr_3)); }
	inline LineRenderer_t3154350270 * get__lr_3() const { return ____lr_3; }
	inline LineRenderer_t3154350270 ** get_address_of__lr_3() { return &____lr_3; }
	inline void set__lr_3(LineRenderer_t3154350270 * value)
	{
		____lr_3 = value;
		Il2CppCodeGenWriteBarrier((&____lr_3), value);
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(WorldBorder_t1275663748, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTemp2_5() { return static_cast<int32_t>(offsetof(WorldBorder_t1275663748, ___vecTemp2_5)); }
	inline Vector2_t2156229523  get_vecTemp2_5() const { return ___vecTemp2_5; }
	inline Vector2_t2156229523 * get_address_of_vecTemp2_5() { return &___vecTemp2_5; }
	inline void set_vecTemp2_5(Vector2_t2156229523  value)
	{
		___vecTemp2_5 = value;
	}

	inline static int32_t get_offset_of_m_aryColliderPoints_6() { return static_cast<int32_t>(offsetof(WorldBorder_t1275663748, ___m_aryColliderPoints_6)); }
	inline Vector2U5BU5D_t1457185986* get_m_aryColliderPoints_6() const { return ___m_aryColliderPoints_6; }
	inline Vector2U5BU5D_t1457185986** get_address_of_m_aryColliderPoints_6() { return &___m_aryColliderPoints_6; }
	inline void set_m_aryColliderPoints_6(Vector2U5BU5D_t1457185986* value)
	{
		___m_aryColliderPoints_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryColliderPoints_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDBORDER_T1275663748_H
#ifndef UNIWEBVIEWANDROIDSTATICLISTENER_T2820583295_H
#define UNIWEBVIEWANDROIDSTATICLISTENER_T2820583295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewAndroidStaticListener
struct  UniWebViewAndroidStaticListener_t2820583295  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWANDROIDSTATICLISTENER_T2820583295_H
#ifndef UI_MAINSKILL_UNFOLD_T57022693_H
#define UI_MAINSKILL_UNFOLD_T57022693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_MainSkill_Unfold
struct  UI_MainSkill_Unfold_t57022693  : public MonoBehaviour_t3962482529
{
public:
	// CShuangYaoGan UI_MainSkill_Unfold::_ShuangYaoGan
	CShuangYaoGan_t3597386049 * ____ShuangYaoGan_2;
	// Main UI_MainSkill_Unfold::m_Main
	Main_t2227614074 * ___m_Main_3;
	// System.Boolean UI_MainSkill_Unfold::m_bSpitting
	bool ___m_bSpitting_5;
	// UnityEngine.Touch UI_MainSkill_Unfold::_touch
	Touch_t1921856868  ____touch_6;

public:
	inline static int32_t get_offset_of__ShuangYaoGan_2() { return static_cast<int32_t>(offsetof(UI_MainSkill_Unfold_t57022693, ____ShuangYaoGan_2)); }
	inline CShuangYaoGan_t3597386049 * get__ShuangYaoGan_2() const { return ____ShuangYaoGan_2; }
	inline CShuangYaoGan_t3597386049 ** get_address_of__ShuangYaoGan_2() { return &____ShuangYaoGan_2; }
	inline void set__ShuangYaoGan_2(CShuangYaoGan_t3597386049 * value)
	{
		____ShuangYaoGan_2 = value;
		Il2CppCodeGenWriteBarrier((&____ShuangYaoGan_2), value);
	}

	inline static int32_t get_offset_of_m_Main_3() { return static_cast<int32_t>(offsetof(UI_MainSkill_Unfold_t57022693, ___m_Main_3)); }
	inline Main_t2227614074 * get_m_Main_3() const { return ___m_Main_3; }
	inline Main_t2227614074 ** get_address_of_m_Main_3() { return &___m_Main_3; }
	inline void set_m_Main_3(Main_t2227614074 * value)
	{
		___m_Main_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Main_3), value);
	}

	inline static int32_t get_offset_of_m_bSpitting_5() { return static_cast<int32_t>(offsetof(UI_MainSkill_Unfold_t57022693, ___m_bSpitting_5)); }
	inline bool get_m_bSpitting_5() const { return ___m_bSpitting_5; }
	inline bool* get_address_of_m_bSpitting_5() { return &___m_bSpitting_5; }
	inline void set_m_bSpitting_5(bool value)
	{
		___m_bSpitting_5 = value;
	}

	inline static int32_t get_offset_of__touch_6() { return static_cast<int32_t>(offsetof(UI_MainSkill_Unfold_t57022693, ____touch_6)); }
	inline Touch_t1921856868  get__touch_6() const { return ____touch_6; }
	inline Touch_t1921856868 * get_address_of__touch_6() { return &____touch_6; }
	inline void set__touch_6(Touch_t1921856868  value)
	{
		____touch_6 = value;
	}
};

struct UI_MainSkill_Unfold_t57022693_StaticFields
{
public:
	// System.Boolean UI_MainSkill_Unfold::s_bUsingUi
	bool ___s_bUsingUi_4;

public:
	inline static int32_t get_offset_of_s_bUsingUi_4() { return static_cast<int32_t>(offsetof(UI_MainSkill_Unfold_t57022693_StaticFields, ___s_bUsingUi_4)); }
	inline bool get_s_bUsingUi_4() const { return ___s_bUsingUi_4; }
	inline bool* get_address_of_s_bUsingUi_4() { return &___s_bUsingUi_4; }
	inline void set_s_bUsingUi_4(bool value)
	{
		___s_bUsingUi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_MAINSKILL_UNFOLD_T57022693_H
#ifndef MAPOBJ_T1733252447_H
#define MAPOBJ_T1733252447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapObj
struct  MapObj_t1733252447  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MapObj::m_fRotatoinZ
	float ___m_fRotatoinZ_2;
	// System.Single MapObj::m_fLiveTime
	float ___m_fLiveTime_3;
	// System.Boolean MapObj::m_bImortal
	bool ___m_bImortal_4;
	// UnityEngine.Vector3 MapObj::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Vector2 MapObj::_speed
	Vector2_t2156229523  ____speed_6;
	// System.Boolean MapObj::m_bDestroyed
	bool ___m_bDestroyed_7;
	// UnityEngine.Vector2 MapObj::m_Direction
	Vector2_t2156229523  ___m_Direction_8;
	// MapObj/eMapObjType MapObj::m_eObjType
	int32_t ___m_eObjType_9;
	// System.Single MapObj::m_fInitSpeed
	float ___m_fInitSpeed_10;
	// System.Single MapObj::m_fAccelerate
	float ___m_fAccelerate_11;
	// System.Single MapObj::m_fDirection
	float ___m_fDirection_12;
	// System.Boolean MapObj::m_bMoving
	bool ___m_bMoving_13;
	// System.Boolean MapObj::m_bEjecting
	bool ___m_bEjecting_14;
	// System.Single MapObj::m_fEjectDistance
	float ___m_fEjectDistance_15;
	// UnityEngine.Vector2 MapObj::m_vEjectStartPos
	Vector2_t2156229523  ___m_vEjectStartPos_16;

public:
	inline static int32_t get_offset_of_m_fRotatoinZ_2() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fRotatoinZ_2)); }
	inline float get_m_fRotatoinZ_2() const { return ___m_fRotatoinZ_2; }
	inline float* get_address_of_m_fRotatoinZ_2() { return &___m_fRotatoinZ_2; }
	inline void set_m_fRotatoinZ_2(float value)
	{
		___m_fRotatoinZ_2 = value;
	}

	inline static int32_t get_offset_of_m_fLiveTime_3() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fLiveTime_3)); }
	inline float get_m_fLiveTime_3() const { return ___m_fLiveTime_3; }
	inline float* get_address_of_m_fLiveTime_3() { return &___m_fLiveTime_3; }
	inline void set_m_fLiveTime_3(float value)
	{
		___m_fLiveTime_3 = value;
	}

	inline static int32_t get_offset_of_m_bImortal_4() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bImortal_4)); }
	inline bool get_m_bImortal_4() const { return ___m_bImortal_4; }
	inline bool* get_address_of_m_bImortal_4() { return &___m_bImortal_4; }
	inline void set_m_bImortal_4(bool value)
	{
		___m_bImortal_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ____speed_6)); }
	inline Vector2_t2156229523  get__speed_6() const { return ____speed_6; }
	inline Vector2_t2156229523 * get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(Vector2_t2156229523  value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of_m_bDestroyed_7() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bDestroyed_7)); }
	inline bool get_m_bDestroyed_7() const { return ___m_bDestroyed_7; }
	inline bool* get_address_of_m_bDestroyed_7() { return &___m_bDestroyed_7; }
	inline void set_m_bDestroyed_7(bool value)
	{
		___m_bDestroyed_7 = value;
	}

	inline static int32_t get_offset_of_m_Direction_8() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_Direction_8)); }
	inline Vector2_t2156229523  get_m_Direction_8() const { return ___m_Direction_8; }
	inline Vector2_t2156229523 * get_address_of_m_Direction_8() { return &___m_Direction_8; }
	inline void set_m_Direction_8(Vector2_t2156229523  value)
	{
		___m_Direction_8 = value;
	}

	inline static int32_t get_offset_of_m_eObjType_9() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_eObjType_9)); }
	inline int32_t get_m_eObjType_9() const { return ___m_eObjType_9; }
	inline int32_t* get_address_of_m_eObjType_9() { return &___m_eObjType_9; }
	inline void set_m_eObjType_9(int32_t value)
	{
		___m_eObjType_9 = value;
	}

	inline static int32_t get_offset_of_m_fInitSpeed_10() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fInitSpeed_10)); }
	inline float get_m_fInitSpeed_10() const { return ___m_fInitSpeed_10; }
	inline float* get_address_of_m_fInitSpeed_10() { return &___m_fInitSpeed_10; }
	inline void set_m_fInitSpeed_10(float value)
	{
		___m_fInitSpeed_10 = value;
	}

	inline static int32_t get_offset_of_m_fAccelerate_11() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fAccelerate_11)); }
	inline float get_m_fAccelerate_11() const { return ___m_fAccelerate_11; }
	inline float* get_address_of_m_fAccelerate_11() { return &___m_fAccelerate_11; }
	inline void set_m_fAccelerate_11(float value)
	{
		___m_fAccelerate_11 = value;
	}

	inline static int32_t get_offset_of_m_fDirection_12() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fDirection_12)); }
	inline float get_m_fDirection_12() const { return ___m_fDirection_12; }
	inline float* get_address_of_m_fDirection_12() { return &___m_fDirection_12; }
	inline void set_m_fDirection_12(float value)
	{
		___m_fDirection_12 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_13() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bMoving_13)); }
	inline bool get_m_bMoving_13() const { return ___m_bMoving_13; }
	inline bool* get_address_of_m_bMoving_13() { return &___m_bMoving_13; }
	inline void set_m_bMoving_13(bool value)
	{
		___m_bMoving_13 = value;
	}

	inline static int32_t get_offset_of_m_bEjecting_14() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_bEjecting_14)); }
	inline bool get_m_bEjecting_14() const { return ___m_bEjecting_14; }
	inline bool* get_address_of_m_bEjecting_14() { return &___m_bEjecting_14; }
	inline void set_m_bEjecting_14(bool value)
	{
		___m_bEjecting_14 = value;
	}

	inline static int32_t get_offset_of_m_fEjectDistance_15() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_fEjectDistance_15)); }
	inline float get_m_fEjectDistance_15() const { return ___m_fEjectDistance_15; }
	inline float* get_address_of_m_fEjectDistance_15() { return &___m_fEjectDistance_15; }
	inline void set_m_fEjectDistance_15(float value)
	{
		___m_fEjectDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_vEjectStartPos_16() { return static_cast<int32_t>(offsetof(MapObj_t1733252447, ___m_vEjectStartPos_16)); }
	inline Vector2_t2156229523  get_m_vEjectStartPos_16() const { return ___m_vEjectStartPos_16; }
	inline Vector2_t2156229523 * get_address_of_m_vEjectStartPos_16() { return &___m_vEjectStartPos_16; }
	inline void set_m_vEjectStartPos_16(Vector2_t2156229523  value)
	{
		___m_vEjectStartPos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPOBJ_T1733252447_H
#ifndef SKELETONUTILITYKINEMATICSHADOW_T296913764_H
#define SKELETONUTILITYKINEMATICSHADOW_T296913764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow
struct  SkeletonUtilityKinematicShadow_t296913764  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::detachedShadow
	bool ___detachedShadow_2;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow::parent
	Transform_t3600365921 * ___parent_3;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::hideShadow
	bool ___hideShadow_4;
	// UnityEngine.GameObject Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowRoot
	GameObject_t1113636619 * ___shadowRoot_5;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair> Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowTable
	List_1_t972525202 * ___shadowTable_6;

public:
	inline static int32_t get_offset_of_detachedShadow_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___detachedShadow_2)); }
	inline bool get_detachedShadow_2() const { return ___detachedShadow_2; }
	inline bool* get_address_of_detachedShadow_2() { return &___detachedShadow_2; }
	inline void set_detachedShadow_2(bool value)
	{
		___detachedShadow_2 = value;
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___parent_3)); }
	inline Transform_t3600365921 * get_parent_3() const { return ___parent_3; }
	inline Transform_t3600365921 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Transform_t3600365921 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_hideShadow_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___hideShadow_4)); }
	inline bool get_hideShadow_4() const { return ___hideShadow_4; }
	inline bool* get_address_of_hideShadow_4() { return &___hideShadow_4; }
	inline void set_hideShadow_4(bool value)
	{
		___hideShadow_4 = value;
	}

	inline static int32_t get_offset_of_shadowRoot_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___shadowRoot_5)); }
	inline GameObject_t1113636619 * get_shadowRoot_5() const { return ___shadowRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_shadowRoot_5() { return &___shadowRoot_5; }
	inline void set_shadowRoot_5(GameObject_t1113636619 * value)
	{
		___shadowRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___shadowRoot_5), value);
	}

	inline static int32_t get_offset_of_shadowTable_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___shadowTable_6)); }
	inline List_1_t972525202 * get_shadowTable_6() const { return ___shadowTable_6; }
	inline List_1_t972525202 ** get_address_of_shadowTable_6() { return &___shadowTable_6; }
	inline void set_shadowTable_6(List_1_t972525202 * value)
	{
		___shadowTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___shadowTable_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYKINEMATICSHADOW_T296913764_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef UI_SPITBEAN_T2554404907_H
#define UI_SPITBEAN_T2554404907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_SpitBean
struct  UI_SpitBean_t2554404907  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UI_SpitBean::m_bSplitting
	bool ___m_bSplitting_3;

public:
	inline static int32_t get_offset_of_m_bSplitting_3() { return static_cast<int32_t>(offsetof(UI_SpitBean_t2554404907, ___m_bSplitting_3)); }
	inline bool get_m_bSplitting_3() const { return ___m_bSplitting_3; }
	inline bool* get_address_of_m_bSplitting_3() { return &___m_bSplitting_3; }
	inline void set_m_bSplitting_3(bool value)
	{
		___m_bSplitting_3 = value;
	}
};

struct UI_SpitBean_t2554404907_StaticFields
{
public:
	// System.Boolean UI_SpitBean::s_bUsingUi
	bool ___s_bUsingUi_2;

public:
	inline static int32_t get_offset_of_s_bUsingUi_2() { return static_cast<int32_t>(offsetof(UI_SpitBean_t2554404907_StaticFields, ___s_bUsingUi_2)); }
	inline bool get_s_bUsingUi_2() const { return ___s_bUsingUi_2; }
	inline bool* get_address_of_s_bUsingUi_2() { return &___s_bUsingUi_2; }
	inline void set_s_bUsingUi_2(bool value)
	{
		___s_bUsingUi_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_SPITBEAN_T2554404907_H
#ifndef BONEFOLLOWERGRAPHIC_T46362405_H
#define BONEFOLLOWERGRAPHIC_T46362405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollowerGraphic
struct  BoneFollowerGraphic_t46362405  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonGraphic Spine.Unity.BoneFollowerGraphic::skeletonGraphic
	SkeletonGraphic_t1744877482 * ___skeletonGraphic_2;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::initializeOnAwake
	bool ___initializeOnAwake_3;
	// System.String Spine.Unity.BoneFollowerGraphic::boneName
	String_t* ___boneName_4;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followBoneRotation
	bool ___followBoneRotation_5;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followLocalScale
	bool ___followLocalScale_7;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followZPosition
	bool ___followZPosition_8;
	// Spine.Bone Spine.Unity.BoneFollowerGraphic::bone
	Bone_t1086356328 * ___bone_9;
	// UnityEngine.Transform Spine.Unity.BoneFollowerGraphic::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_10;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_11;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::valid
	bool ___valid_12;

public:
	inline static int32_t get_offset_of_skeletonGraphic_2() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___skeletonGraphic_2)); }
	inline SkeletonGraphic_t1744877482 * get_skeletonGraphic_2() const { return ___skeletonGraphic_2; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_skeletonGraphic_2() { return &___skeletonGraphic_2; }
	inline void set_skeletonGraphic_2(SkeletonGraphic_t1744877482 * value)
	{
		___skeletonGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonGraphic_2), value);
	}

	inline static int32_t get_offset_of_initializeOnAwake_3() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___initializeOnAwake_3)); }
	inline bool get_initializeOnAwake_3() const { return ___initializeOnAwake_3; }
	inline bool* get_address_of_initializeOnAwake_3() { return &___initializeOnAwake_3; }
	inline void set_initializeOnAwake_3(bool value)
	{
		___initializeOnAwake_3 = value;
	}

	inline static int32_t get_offset_of_boneName_4() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___boneName_4)); }
	inline String_t* get_boneName_4() const { return ___boneName_4; }
	inline String_t** get_address_of_boneName_4() { return &___boneName_4; }
	inline void set_boneName_4(String_t* value)
	{
		___boneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_4), value);
	}

	inline static int32_t get_offset_of_followBoneRotation_5() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followBoneRotation_5)); }
	inline bool get_followBoneRotation_5() const { return ___followBoneRotation_5; }
	inline bool* get_address_of_followBoneRotation_5() { return &___followBoneRotation_5; }
	inline void set_followBoneRotation_5(bool value)
	{
		___followBoneRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_7() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followLocalScale_7)); }
	inline bool get_followLocalScale_7() const { return ___followLocalScale_7; }
	inline bool* get_address_of_followLocalScale_7() { return &___followLocalScale_7; }
	inline void set_followLocalScale_7(bool value)
	{
		___followLocalScale_7 = value;
	}

	inline static int32_t get_offset_of_followZPosition_8() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followZPosition_8)); }
	inline bool get_followZPosition_8() const { return ___followZPosition_8; }
	inline bool* get_address_of_followZPosition_8() { return &___followZPosition_8; }
	inline void set_followZPosition_8(bool value)
	{
		___followZPosition_8 = value;
	}

	inline static int32_t get_offset_of_bone_9() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___bone_9)); }
	inline Bone_t1086356328 * get_bone_9() const { return ___bone_9; }
	inline Bone_t1086356328 ** get_address_of_bone_9() { return &___bone_9; }
	inline void set_bone_9(Bone_t1086356328 * value)
	{
		___bone_9 = value;
		Il2CppCodeGenWriteBarrier((&___bone_9), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_10() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___skeletonTransform_10)); }
	inline Transform_t3600365921 * get_skeletonTransform_10() const { return ___skeletonTransform_10; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_10() { return &___skeletonTransform_10; }
	inline void set_skeletonTransform_10(Transform_t3600365921 * value)
	{
		___skeletonTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_11() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___skeletonTransformIsParent_11)); }
	inline bool get_skeletonTransformIsParent_11() const { return ___skeletonTransformIsParent_11; }
	inline bool* get_address_of_skeletonTransformIsParent_11() { return &___skeletonTransformIsParent_11; }
	inline void set_skeletonTransformIsParent_11(bool value)
	{
		___skeletonTransformIsParent_11 = value;
	}

	inline static int32_t get_offset_of_valid_12() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___valid_12)); }
	inline bool get_valid_12() const { return ___valid_12; }
	inline bool* get_address_of_valid_12() { return &___valid_12; }
	inline void set_valid_12(bool value)
	{
		___valid_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWERGRAPHIC_T46362405_H
#ifndef SKELETONPARTSRENDERER_T667127217_H
#define SKELETONPARTSRENDERER_T667127217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonPartsRenderer
struct  SkeletonPartsRenderer_t667127217  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.MeshGenerator Spine.Unity.Modules.SkeletonPartsRenderer::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_2;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonPartsRenderer::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_3;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonPartsRenderer::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_4;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.Modules.SkeletonPartsRenderer::buffers
	MeshRendererBuffers_t756429994 * ___buffers_5;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.Modules.SkeletonPartsRenderer::currentInstructions
	SkeletonRendererInstruction_t651787775 * ___currentInstructions_6;

public:
	inline static int32_t get_offset_of_meshGenerator_2() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___meshGenerator_2)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_2() const { return ___meshGenerator_2; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_2() { return &___meshGenerator_2; }
	inline void set_meshGenerator_2(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_2), value);
	}

	inline static int32_t get_offset_of_meshRenderer_3() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___meshRenderer_3)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_3() const { return ___meshRenderer_3; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_3() { return &___meshRenderer_3; }
	inline void set_meshRenderer_3(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_3), value);
	}

	inline static int32_t get_offset_of_meshFilter_4() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___meshFilter_4)); }
	inline MeshFilter_t3523625662 * get_meshFilter_4() const { return ___meshFilter_4; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_4() { return &___meshFilter_4; }
	inline void set_meshFilter_4(MeshFilter_t3523625662 * value)
	{
		___meshFilter_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_4), value);
	}

	inline static int32_t get_offset_of_buffers_5() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___buffers_5)); }
	inline MeshRendererBuffers_t756429994 * get_buffers_5() const { return ___buffers_5; }
	inline MeshRendererBuffers_t756429994 ** get_address_of_buffers_5() { return &___buffers_5; }
	inline void set_buffers_5(MeshRendererBuffers_t756429994 * value)
	{
		___buffers_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_5), value);
	}

	inline static int32_t get_offset_of_currentInstructions_6() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___currentInstructions_6)); }
	inline SkeletonRendererInstruction_t651787775 * get_currentInstructions_6() const { return ___currentInstructions_6; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_currentInstructions_6() { return &___currentInstructions_6; }
	inline void set_currentInstructions_6(SkeletonRendererInstruction_t651787775 * value)
	{
		___currentInstructions_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONPARTSRENDERER_T667127217_H
#ifndef SKELETONRENDERSEPARATOR_T2026602841_H
#define SKELETONRENDERSEPARATOR_T2026602841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRenderSeparator
struct  SkeletonRenderSeparator_t2026602841  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRenderSeparator::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_3;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonRenderSeparator::mainMeshRenderer
	MeshRenderer_t587009260 * ___mainMeshRenderer_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyPropertyBlock
	bool ___copyPropertyBlock_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyMeshRendererFlags
	bool ___copyMeshRendererFlags_6;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer> Spine.Unity.Modules.SkeletonRenderSeparator::partsRenderers
	List_1_t2139201959 * ___partsRenderers_7;
	// UnityEngine.MaterialPropertyBlock Spine.Unity.Modules.SkeletonRenderSeparator::copiedBlock
	MaterialPropertyBlock_t3213117958 * ___copiedBlock_8;

public:
	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}

	inline static int32_t get_offset_of_mainMeshRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___mainMeshRenderer_4)); }
	inline MeshRenderer_t587009260 * get_mainMeshRenderer_4() const { return ___mainMeshRenderer_4; }
	inline MeshRenderer_t587009260 ** get_address_of_mainMeshRenderer_4() { return &___mainMeshRenderer_4; }
	inline void set_mainMeshRenderer_4(MeshRenderer_t587009260 * value)
	{
		___mainMeshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainMeshRenderer_4), value);
	}

	inline static int32_t get_offset_of_copyPropertyBlock_5() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___copyPropertyBlock_5)); }
	inline bool get_copyPropertyBlock_5() const { return ___copyPropertyBlock_5; }
	inline bool* get_address_of_copyPropertyBlock_5() { return &___copyPropertyBlock_5; }
	inline void set_copyPropertyBlock_5(bool value)
	{
		___copyPropertyBlock_5 = value;
	}

	inline static int32_t get_offset_of_copyMeshRendererFlags_6() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___copyMeshRendererFlags_6)); }
	inline bool get_copyMeshRendererFlags_6() const { return ___copyMeshRendererFlags_6; }
	inline bool* get_address_of_copyMeshRendererFlags_6() { return &___copyMeshRendererFlags_6; }
	inline void set_copyMeshRendererFlags_6(bool value)
	{
		___copyMeshRendererFlags_6 = value;
	}

	inline static int32_t get_offset_of_partsRenderers_7() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___partsRenderers_7)); }
	inline List_1_t2139201959 * get_partsRenderers_7() const { return ___partsRenderers_7; }
	inline List_1_t2139201959 ** get_address_of_partsRenderers_7() { return &___partsRenderers_7; }
	inline void set_partsRenderers_7(List_1_t2139201959 * value)
	{
		___partsRenderers_7 = value;
		Il2CppCodeGenWriteBarrier((&___partsRenderers_7), value);
	}

	inline static int32_t get_offset_of_copiedBlock_8() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___copiedBlock_8)); }
	inline MaterialPropertyBlock_t3213117958 * get_copiedBlock_8() const { return ___copiedBlock_8; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_copiedBlock_8() { return &___copiedBlock_8; }
	inline void set_copiedBlock_8(MaterialPropertyBlock_t3213117958 * value)
	{
		___copiedBlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___copiedBlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERSEPARATOR_T2026602841_H
#ifndef UI_SPLIT_T2785288677_H
#define UI_SPLIT_T2785288677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Split
struct  UI_Split_t2785288677  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UI_Split::m_bSplitting
	bool ___m_bSplitting_3;

public:
	inline static int32_t get_offset_of_m_bSplitting_3() { return static_cast<int32_t>(offsetof(UI_Split_t2785288677, ___m_bSplitting_3)); }
	inline bool get_m_bSplitting_3() const { return ___m_bSplitting_3; }
	inline bool* get_address_of_m_bSplitting_3() { return &___m_bSplitting_3; }
	inline void set_m_bSplitting_3(bool value)
	{
		___m_bSplitting_3 = value;
	}
};

struct UI_Split_t2785288677_StaticFields
{
public:
	// System.Boolean UI_Split::s_bUsingUi
	bool ___s_bUsingUi_2;

public:
	inline static int32_t get_offset_of_s_bUsingUi_2() { return static_cast<int32_t>(offsetof(UI_Split_t2785288677_StaticFields, ___s_bUsingUi_2)); }
	inline bool get_s_bUsingUi_2() const { return ___s_bUsingUi_2; }
	inline bool* get_address_of_s_bUsingUi_2() { return &___s_bUsingUi_2; }
	inline void set_s_bUsingUi_2(bool value)
	{
		___s_bUsingUi_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_SPLIT_T2785288677_H
#ifndef SKELETONRENDERER_T2098681813_H
#define SKELETONRENDERER_T2098681813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer
struct  SkeletonRenderer_t2098681813  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate Spine.Unity.SkeletonRenderer::OnRebuild
	SkeletonRendererDelegate_t3507789975 * ___OnRebuild_2;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonRenderer::OnPostProcessVertices
	MeshGeneratorDelegate_t1654156803 * ___OnPostProcessVertices_3;
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonRenderer::skeletonDataAsset
	SkeletonDataAsset_t3748144825 * ___skeletonDataAsset_4;
	// System.String Spine.Unity.SkeletonRenderer::initialSkinName
	String_t* ___initialSkinName_5;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipX
	bool ___initialFlipX_6;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipY
	bool ___initialFlipY_7;
	// System.String[] Spine.Unity.SkeletonRenderer::separatorSlotNames
	StringU5BU5D_t1281789340* ___separatorSlotNames_8;
	// System.Collections.Generic.List`1<Spine.Slot> Spine.Unity.SkeletonRenderer::separatorSlots
	List_1_t692048146 * ___separatorSlots_9;
	// System.Single Spine.Unity.SkeletonRenderer::zSpacing
	float ___zSpacing_10;
	// System.Boolean Spine.Unity.SkeletonRenderer::useClipping
	bool ___useClipping_11;
	// System.Boolean Spine.Unity.SkeletonRenderer::immutableTriangles
	bool ___immutableTriangles_12;
	// System.Boolean Spine.Unity.SkeletonRenderer::pmaVertexColors
	bool ___pmaVertexColors_13;
	// System.Boolean Spine.Unity.SkeletonRenderer::clearStateOnDisable
	bool ___clearStateOnDisable_14;
	// System.Boolean Spine.Unity.SkeletonRenderer::tintBlack
	bool ___tintBlack_15;
	// System.Boolean Spine.Unity.SkeletonRenderer::singleSubmesh
	bool ___singleSubmesh_16;
	// System.Boolean Spine.Unity.SkeletonRenderer::addNormals
	bool ___addNormals_17;
	// System.Boolean Spine.Unity.SkeletonRenderer::calculateTangents
	bool ___calculateTangents_18;
	// System.Boolean Spine.Unity.SkeletonRenderer::logErrors
	bool ___logErrors_19;
	// System.Boolean Spine.Unity.SkeletonRenderer::disableRenderingOnOverride
	bool ___disableRenderingOnOverride_20;
	// Spine.Unity.SkeletonRenderer/InstructionDelegate Spine.Unity.SkeletonRenderer::generateMeshOverride
	InstructionDelegate_t2225421195 * ___generateMeshOverride_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customMaterialOverride
	Dictionary_2_t3700682020 * ___customMaterialOverride_22;
	// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customSlotMaterials
	Dictionary_2_t3424054551 * ___customSlotMaterials_23;
	// UnityEngine.MeshRenderer Spine.Unity.SkeletonRenderer::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_24;
	// UnityEngine.MeshFilter Spine.Unity.SkeletonRenderer::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_25;
	// System.Boolean Spine.Unity.SkeletonRenderer::valid
	bool ___valid_26;
	// Spine.Skeleton Spine.Unity.SkeletonRenderer::skeleton
	Skeleton_t3686076450 * ___skeleton_27;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonRenderer::currentInstructions
	SkeletonRendererInstruction_t651787775 * ___currentInstructions_28;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonRenderer::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_29;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.SkeletonRenderer::rendererBuffers
	MeshRendererBuffers_t756429994 * ___rendererBuffers_30;

public:
	inline static int32_t get_offset_of_OnRebuild_2() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___OnRebuild_2)); }
	inline SkeletonRendererDelegate_t3507789975 * get_OnRebuild_2() const { return ___OnRebuild_2; }
	inline SkeletonRendererDelegate_t3507789975 ** get_address_of_OnRebuild_2() { return &___OnRebuild_2; }
	inline void set_OnRebuild_2(SkeletonRendererDelegate_t3507789975 * value)
	{
		___OnRebuild_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRebuild_2), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_3() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___OnPostProcessVertices_3)); }
	inline MeshGeneratorDelegate_t1654156803 * get_OnPostProcessVertices_3() const { return ___OnPostProcessVertices_3; }
	inline MeshGeneratorDelegate_t1654156803 ** get_address_of_OnPostProcessVertices_3() { return &___OnPostProcessVertices_3; }
	inline void set_OnPostProcessVertices_3(MeshGeneratorDelegate_t1654156803 * value)
	{
		___OnPostProcessVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_3), value);
	}

	inline static int32_t get_offset_of_skeletonDataAsset_4() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___skeletonDataAsset_4)); }
	inline SkeletonDataAsset_t3748144825 * get_skeletonDataAsset_4() const { return ___skeletonDataAsset_4; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_skeletonDataAsset_4() { return &___skeletonDataAsset_4; }
	inline void set_skeletonDataAsset_4(SkeletonDataAsset_t3748144825 * value)
	{
		___skeletonDataAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_4), value);
	}

	inline static int32_t get_offset_of_initialSkinName_5() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___initialSkinName_5)); }
	inline String_t* get_initialSkinName_5() const { return ___initialSkinName_5; }
	inline String_t** get_address_of_initialSkinName_5() { return &___initialSkinName_5; }
	inline void set_initialSkinName_5(String_t* value)
	{
		___initialSkinName_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_5), value);
	}

	inline static int32_t get_offset_of_initialFlipX_6() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___initialFlipX_6)); }
	inline bool get_initialFlipX_6() const { return ___initialFlipX_6; }
	inline bool* get_address_of_initialFlipX_6() { return &___initialFlipX_6; }
	inline void set_initialFlipX_6(bool value)
	{
		___initialFlipX_6 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_7() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___initialFlipY_7)); }
	inline bool get_initialFlipY_7() const { return ___initialFlipY_7; }
	inline bool* get_address_of_initialFlipY_7() { return &___initialFlipY_7; }
	inline void set_initialFlipY_7(bool value)
	{
		___initialFlipY_7 = value;
	}

	inline static int32_t get_offset_of_separatorSlotNames_8() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___separatorSlotNames_8)); }
	inline StringU5BU5D_t1281789340* get_separatorSlotNames_8() const { return ___separatorSlotNames_8; }
	inline StringU5BU5D_t1281789340** get_address_of_separatorSlotNames_8() { return &___separatorSlotNames_8; }
	inline void set_separatorSlotNames_8(StringU5BU5D_t1281789340* value)
	{
		___separatorSlotNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlotNames_8), value);
	}

	inline static int32_t get_offset_of_separatorSlots_9() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___separatorSlots_9)); }
	inline List_1_t692048146 * get_separatorSlots_9() const { return ___separatorSlots_9; }
	inline List_1_t692048146 ** get_address_of_separatorSlots_9() { return &___separatorSlots_9; }
	inline void set_separatorSlots_9(List_1_t692048146 * value)
	{
		___separatorSlots_9 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlots_9), value);
	}

	inline static int32_t get_offset_of_zSpacing_10() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___zSpacing_10)); }
	inline float get_zSpacing_10() const { return ___zSpacing_10; }
	inline float* get_address_of_zSpacing_10() { return &___zSpacing_10; }
	inline void set_zSpacing_10(float value)
	{
		___zSpacing_10 = value;
	}

	inline static int32_t get_offset_of_useClipping_11() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___useClipping_11)); }
	inline bool get_useClipping_11() const { return ___useClipping_11; }
	inline bool* get_address_of_useClipping_11() { return &___useClipping_11; }
	inline void set_useClipping_11(bool value)
	{
		___useClipping_11 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_12() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___immutableTriangles_12)); }
	inline bool get_immutableTriangles_12() const { return ___immutableTriangles_12; }
	inline bool* get_address_of_immutableTriangles_12() { return &___immutableTriangles_12; }
	inline void set_immutableTriangles_12(bool value)
	{
		___immutableTriangles_12 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_13() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___pmaVertexColors_13)); }
	inline bool get_pmaVertexColors_13() const { return ___pmaVertexColors_13; }
	inline bool* get_address_of_pmaVertexColors_13() { return &___pmaVertexColors_13; }
	inline void set_pmaVertexColors_13(bool value)
	{
		___pmaVertexColors_13 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_14() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___clearStateOnDisable_14)); }
	inline bool get_clearStateOnDisable_14() const { return ___clearStateOnDisable_14; }
	inline bool* get_address_of_clearStateOnDisable_14() { return &___clearStateOnDisable_14; }
	inline void set_clearStateOnDisable_14(bool value)
	{
		___clearStateOnDisable_14 = value;
	}

	inline static int32_t get_offset_of_tintBlack_15() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___tintBlack_15)); }
	inline bool get_tintBlack_15() const { return ___tintBlack_15; }
	inline bool* get_address_of_tintBlack_15() { return &___tintBlack_15; }
	inline void set_tintBlack_15(bool value)
	{
		___tintBlack_15 = value;
	}

	inline static int32_t get_offset_of_singleSubmesh_16() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___singleSubmesh_16)); }
	inline bool get_singleSubmesh_16() const { return ___singleSubmesh_16; }
	inline bool* get_address_of_singleSubmesh_16() { return &___singleSubmesh_16; }
	inline void set_singleSubmesh_16(bool value)
	{
		___singleSubmesh_16 = value;
	}

	inline static int32_t get_offset_of_addNormals_17() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___addNormals_17)); }
	inline bool get_addNormals_17() const { return ___addNormals_17; }
	inline bool* get_address_of_addNormals_17() { return &___addNormals_17; }
	inline void set_addNormals_17(bool value)
	{
		___addNormals_17 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_18() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___calculateTangents_18)); }
	inline bool get_calculateTangents_18() const { return ___calculateTangents_18; }
	inline bool* get_address_of_calculateTangents_18() { return &___calculateTangents_18; }
	inline void set_calculateTangents_18(bool value)
	{
		___calculateTangents_18 = value;
	}

	inline static int32_t get_offset_of_logErrors_19() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___logErrors_19)); }
	inline bool get_logErrors_19() const { return ___logErrors_19; }
	inline bool* get_address_of_logErrors_19() { return &___logErrors_19; }
	inline void set_logErrors_19(bool value)
	{
		___logErrors_19 = value;
	}

	inline static int32_t get_offset_of_disableRenderingOnOverride_20() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___disableRenderingOnOverride_20)); }
	inline bool get_disableRenderingOnOverride_20() const { return ___disableRenderingOnOverride_20; }
	inline bool* get_address_of_disableRenderingOnOverride_20() { return &___disableRenderingOnOverride_20; }
	inline void set_disableRenderingOnOverride_20(bool value)
	{
		___disableRenderingOnOverride_20 = value;
	}

	inline static int32_t get_offset_of_generateMeshOverride_21() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___generateMeshOverride_21)); }
	inline InstructionDelegate_t2225421195 * get_generateMeshOverride_21() const { return ___generateMeshOverride_21; }
	inline InstructionDelegate_t2225421195 ** get_address_of_generateMeshOverride_21() { return &___generateMeshOverride_21; }
	inline void set_generateMeshOverride_21(InstructionDelegate_t2225421195 * value)
	{
		___generateMeshOverride_21 = value;
		Il2CppCodeGenWriteBarrier((&___generateMeshOverride_21), value);
	}

	inline static int32_t get_offset_of_customMaterialOverride_22() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___customMaterialOverride_22)); }
	inline Dictionary_2_t3700682020 * get_customMaterialOverride_22() const { return ___customMaterialOverride_22; }
	inline Dictionary_2_t3700682020 ** get_address_of_customMaterialOverride_22() { return &___customMaterialOverride_22; }
	inline void set_customMaterialOverride_22(Dictionary_2_t3700682020 * value)
	{
		___customMaterialOverride_22 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverride_22), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_23() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___customSlotMaterials_23)); }
	inline Dictionary_2_t3424054551 * get_customSlotMaterials_23() const { return ___customSlotMaterials_23; }
	inline Dictionary_2_t3424054551 ** get_address_of_customSlotMaterials_23() { return &___customSlotMaterials_23; }
	inline void set_customSlotMaterials_23(Dictionary_2_t3424054551 * value)
	{
		___customSlotMaterials_23 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_23), value);
	}

	inline static int32_t get_offset_of_meshRenderer_24() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___meshRenderer_24)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_24() const { return ___meshRenderer_24; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_24() { return &___meshRenderer_24; }
	inline void set_meshRenderer_24(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_24 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_24), value);
	}

	inline static int32_t get_offset_of_meshFilter_25() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___meshFilter_25)); }
	inline MeshFilter_t3523625662 * get_meshFilter_25() const { return ___meshFilter_25; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_25() { return &___meshFilter_25; }
	inline void set_meshFilter_25(MeshFilter_t3523625662 * value)
	{
		___meshFilter_25 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_25), value);
	}

	inline static int32_t get_offset_of_valid_26() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___valid_26)); }
	inline bool get_valid_26() const { return ___valid_26; }
	inline bool* get_address_of_valid_26() { return &___valid_26; }
	inline void set_valid_26(bool value)
	{
		___valid_26 = value;
	}

	inline static int32_t get_offset_of_skeleton_27() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___skeleton_27)); }
	inline Skeleton_t3686076450 * get_skeleton_27() const { return ___skeleton_27; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_27() { return &___skeleton_27; }
	inline void set_skeleton_27(Skeleton_t3686076450 * value)
	{
		___skeleton_27 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_27), value);
	}

	inline static int32_t get_offset_of_currentInstructions_28() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___currentInstructions_28)); }
	inline SkeletonRendererInstruction_t651787775 * get_currentInstructions_28() const { return ___currentInstructions_28; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_currentInstructions_28() { return &___currentInstructions_28; }
	inline void set_currentInstructions_28(SkeletonRendererInstruction_t651787775 * value)
	{
		___currentInstructions_28 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_28), value);
	}

	inline static int32_t get_offset_of_meshGenerator_29() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___meshGenerator_29)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_29() const { return ___meshGenerator_29; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_29() { return &___meshGenerator_29; }
	inline void set_meshGenerator_29(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_29 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_29), value);
	}

	inline static int32_t get_offset_of_rendererBuffers_30() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___rendererBuffers_30)); }
	inline MeshRendererBuffers_t756429994 * get_rendererBuffers_30() const { return ___rendererBuffers_30; }
	inline MeshRendererBuffers_t756429994 ** get_address_of_rendererBuffers_30() { return &___rendererBuffers_30; }
	inline void set_rendererBuffers_30(MeshRendererBuffers_t756429994 * value)
	{
		___rendererBuffers_30 = value;
		Il2CppCodeGenWriteBarrier((&___rendererBuffers_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERER_T2098681813_H
#ifndef SPITBALLTARGET_T3997305173_H
#define SPITBALLTARGET_T3997305173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpitBallTarget
struct  SpitBallTarget_t3997305173  : public MonoBehaviour_t3962482529
{
public:
	// Ball SpitBallTarget::m_ball
	Ball_t2206666566 * ___m_ball_2;
	// UnityEngine.SpriteRenderer SpitBallTarget::m_sr
	SpriteRenderer_t3235626157 * ___m_sr_3;
	// System.Single SpitBallTarget::m_fTotalTime
	float ___m_fTotalTime_4;
	// System.Boolean SpitBallTarget::m_bCouting
	bool ___m_bCouting_5;
	// UnityEngine.Vector3 SpitBallTarget::m_vecPos
	Vector3_t3722313464  ___m_vecPos_8;
	// System.Single SpitBallTarget::m_fSize
	float ___m_fSize_9;

public:
	inline static int32_t get_offset_of_m_ball_2() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173, ___m_ball_2)); }
	inline Ball_t2206666566 * get_m_ball_2() const { return ___m_ball_2; }
	inline Ball_t2206666566 ** get_address_of_m_ball_2() { return &___m_ball_2; }
	inline void set_m_ball_2(Ball_t2206666566 * value)
	{
		___m_ball_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ball_2), value);
	}

	inline static int32_t get_offset_of_m_sr_3() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173, ___m_sr_3)); }
	inline SpriteRenderer_t3235626157 * get_m_sr_3() const { return ___m_sr_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_sr_3() { return &___m_sr_3; }
	inline void set_m_sr_3(SpriteRenderer_t3235626157 * value)
	{
		___m_sr_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_sr_3), value);
	}

	inline static int32_t get_offset_of_m_fTotalTime_4() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173, ___m_fTotalTime_4)); }
	inline float get_m_fTotalTime_4() const { return ___m_fTotalTime_4; }
	inline float* get_address_of_m_fTotalTime_4() { return &___m_fTotalTime_4; }
	inline void set_m_fTotalTime_4(float value)
	{
		___m_fTotalTime_4 = value;
	}

	inline static int32_t get_offset_of_m_bCouting_5() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173, ___m_bCouting_5)); }
	inline bool get_m_bCouting_5() const { return ___m_bCouting_5; }
	inline bool* get_address_of_m_bCouting_5() { return &___m_bCouting_5; }
	inline void set_m_bCouting_5(bool value)
	{
		___m_bCouting_5 = value;
	}

	inline static int32_t get_offset_of_m_vecPos_8() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173, ___m_vecPos_8)); }
	inline Vector3_t3722313464  get_m_vecPos_8() const { return ___m_vecPos_8; }
	inline Vector3_t3722313464 * get_address_of_m_vecPos_8() { return &___m_vecPos_8; }
	inline void set_m_vecPos_8(Vector3_t3722313464  value)
	{
		___m_vecPos_8 = value;
	}

	inline static int32_t get_offset_of_m_fSize_9() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173, ___m_fSize_9)); }
	inline float get_m_fSize_9() const { return ___m_fSize_9; }
	inline float* get_address_of_m_fSize_9() { return &___m_fSize_9; }
	inline void set_m_fSize_9(float value)
	{
		___m_fSize_9 = value;
	}
};

struct SpitBallTarget_t3997305173_StaticFields
{
public:
	// UnityEngine.Vector3 SpitBallTarget::vecTempPos
	Vector3_t3722313464  ___vecTempPos_6;
	// UnityEngine.Vector3 SpitBallTarget::vecTempScale
	Vector3_t3722313464  ___vecTempScale_7;
	// System.Byte[] SpitBallTarget::m_Blob8
	ByteU5BU5D_t4116647657* ___m_Blob8_10;
	// System.Byte[] SpitBallTarget::m_Blob16
	ByteU5BU5D_t4116647657* ___m_Blob16_11;
	// System.Byte[] SpitBallTarget::m_Blob24
	ByteU5BU5D_t4116647657* ___m_Blob24_12;
	// System.Byte[] SpitBallTarget::m_Blob32
	ByteU5BU5D_t4116647657* ___m_Blob32_13;
	// System.Byte[] SpitBallTarget::m_Blob64
	ByteU5BU5D_t4116647657* ___m_Blob64_14;
	// System.Byte[] SpitBallTarget::m_Blob128
	ByteU5BU5D_t4116647657* ___m_Blob128_15;
	// System.Byte[] SpitBallTarget::m_Blob256
	ByteU5BU5D_t4116647657* ___m_Blob256_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> SpitBallTarget::m_dicBlobs
	Dictionary_2_t3005360988 * ___m_dicBlobs_17;

public:
	inline static int32_t get_offset_of_vecTempPos_6() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___vecTempPos_6)); }
	inline Vector3_t3722313464  get_vecTempPos_6() const { return ___vecTempPos_6; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_6() { return &___vecTempPos_6; }
	inline void set_vecTempPos_6(Vector3_t3722313464  value)
	{
		___vecTempPos_6 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_7() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___vecTempScale_7)); }
	inline Vector3_t3722313464  get_vecTempScale_7() const { return ___vecTempScale_7; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_7() { return &___vecTempScale_7; }
	inline void set_vecTempScale_7(Vector3_t3722313464  value)
	{
		___vecTempScale_7 = value;
	}

	inline static int32_t get_offset_of_m_Blob8_10() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob8_10)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob8_10() const { return ___m_Blob8_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob8_10() { return &___m_Blob8_10; }
	inline void set_m_Blob8_10(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob8_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob8_10), value);
	}

	inline static int32_t get_offset_of_m_Blob16_11() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob16_11)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob16_11() const { return ___m_Blob16_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob16_11() { return &___m_Blob16_11; }
	inline void set_m_Blob16_11(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob16_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob16_11), value);
	}

	inline static int32_t get_offset_of_m_Blob24_12() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob24_12)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob24_12() const { return ___m_Blob24_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob24_12() { return &___m_Blob24_12; }
	inline void set_m_Blob24_12(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob24_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob24_12), value);
	}

	inline static int32_t get_offset_of_m_Blob32_13() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob32_13)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob32_13() const { return ___m_Blob32_13; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob32_13() { return &___m_Blob32_13; }
	inline void set_m_Blob32_13(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob32_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob32_13), value);
	}

	inline static int32_t get_offset_of_m_Blob64_14() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob64_14)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob64_14() const { return ___m_Blob64_14; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob64_14() { return &___m_Blob64_14; }
	inline void set_m_Blob64_14(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob64_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob64_14), value);
	}

	inline static int32_t get_offset_of_m_Blob128_15() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob128_15)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob128_15() const { return ___m_Blob128_15; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob128_15() { return &___m_Blob128_15; }
	inline void set_m_Blob128_15(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob128_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob128_15), value);
	}

	inline static int32_t get_offset_of_m_Blob256_16() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_Blob256_16)); }
	inline ByteU5BU5D_t4116647657* get_m_Blob256_16() const { return ___m_Blob256_16; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Blob256_16() { return &___m_Blob256_16; }
	inline void set_m_Blob256_16(ByteU5BU5D_t4116647657* value)
	{
		___m_Blob256_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blob256_16), value);
	}

	inline static int32_t get_offset_of_m_dicBlobs_17() { return static_cast<int32_t>(offsetof(SpitBallTarget_t3997305173_StaticFields, ___m_dicBlobs_17)); }
	inline Dictionary_2_t3005360988 * get_m_dicBlobs_17() const { return ___m_dicBlobs_17; }
	inline Dictionary_2_t3005360988 ** get_address_of_m_dicBlobs_17() { return &___m_dicBlobs_17; }
	inline void set_m_dicBlobs_17(Dictionary_2_t3005360988 * value)
	{
		___m_dicBlobs_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBlobs_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPITBALLTARGET_T3997305173_H
#ifndef UNIWEBVIEWNATIVELISTENER_T1145263134_H
#define UNIWEBVIEWNATIVELISTENER_T1145263134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeListener
struct  UniWebViewNativeListener_t1145263134  : public MonoBehaviour_t3962482529
{
public:
	// UniWebView UniWebViewNativeListener::webView
	UniWebView_t941983939 * ___webView_3;

public:
	inline static int32_t get_offset_of_webView_3() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_t1145263134, ___webView_3)); }
	inline UniWebView_t941983939 * get_webView_3() const { return ___webView_3; }
	inline UniWebView_t941983939 ** get_address_of_webView_3() { return &___webView_3; }
	inline void set_webView_3(UniWebView_t941983939 * value)
	{
		___webView_3 = value;
		Il2CppCodeGenWriteBarrier((&___webView_3), value);
	}
};

struct UniWebViewNativeListener_t1145263134_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener> UniWebViewNativeListener::listeners
	Dictionary_2_t930519433 * ___listeners_2;

public:
	inline static int32_t get_offset_of_listeners_2() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_t1145263134_StaticFields, ___listeners_2)); }
	inline Dictionary_2_t930519433 * get_listeners_2() const { return ___listeners_2; }
	inline Dictionary_2_t930519433 ** get_address_of_listeners_2() { return &___listeners_2; }
	inline void set_listeners_2(Dictionary_2_t930519433 * value)
	{
		___listeners_2 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWNATIVELISTENER_T1145263134_H
#ifndef STRINGMANAGER_T2047563430_H
#define STRINGMANAGER_T2047563430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StringManager
struct  StringManager_t2047563430  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct StringManager_t2047563430_StaticFields
{
public:
	// System.Byte[] StringManager::_bytes
	ByteU5BU5D_t4116647657* ____bytes_2;
	// System.Int32 StringManager::_pointer
	int32_t ____pointer_3;
	// System.Collections.Generic.List`1<System.Byte[]> StringManager::m_lstBlobs
	List_1_t1293755103 * ___m_lstBlobs_4;

public:
	inline static int32_t get_offset_of__bytes_2() { return static_cast<int32_t>(offsetof(StringManager_t2047563430_StaticFields, ____bytes_2)); }
	inline ByteU5BU5D_t4116647657* get__bytes_2() const { return ____bytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytes_2() { return &____bytes_2; }
	inline void set__bytes_2(ByteU5BU5D_t4116647657* value)
	{
		____bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____bytes_2), value);
	}

	inline static int32_t get_offset_of__pointer_3() { return static_cast<int32_t>(offsetof(StringManager_t2047563430_StaticFields, ____pointer_3)); }
	inline int32_t get__pointer_3() const { return ____pointer_3; }
	inline int32_t* get_address_of__pointer_3() { return &____pointer_3; }
	inline void set__pointer_3(int32_t value)
	{
		____pointer_3 = value;
	}

	inline static int32_t get_offset_of_m_lstBlobs_4() { return static_cast<int32_t>(offsetof(StringManager_t2047563430_StaticFields, ___m_lstBlobs_4)); }
	inline List_1_t1293755103 * get_m_lstBlobs_4() const { return ___m_lstBlobs_4; }
	inline List_1_t1293755103 ** get_address_of_m_lstBlobs_4() { return &___m_lstBlobs_4; }
	inline void set_m_lstBlobs_4(List_1_t1293755103 * value)
	{
		___m_lstBlobs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBlobs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGMANAGER_T2047563430_H
#ifndef SKELETONUTILITYCONSTRAINT_T1014382506_H
#define SKELETONUTILITYCONSTRAINT_T1014382506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityConstraint
struct  SkeletonUtilityConstraint_t1014382506  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonUtilityBone Spine.Unity.SkeletonUtilityConstraint::utilBone
	SkeletonUtilityBone_t2225520134 * ___utilBone_2;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityConstraint::skeletonUtility
	SkeletonUtility_t2980767925 * ___skeletonUtility_3;

public:
	inline static int32_t get_offset_of_utilBone_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_t1014382506, ___utilBone_2)); }
	inline SkeletonUtilityBone_t2225520134 * get_utilBone_2() const { return ___utilBone_2; }
	inline SkeletonUtilityBone_t2225520134 ** get_address_of_utilBone_2() { return &___utilBone_2; }
	inline void set_utilBone_2(SkeletonUtilityBone_t2225520134 * value)
	{
		___utilBone_2 = value;
		Il2CppCodeGenWriteBarrier((&___utilBone_2), value);
	}

	inline static int32_t get_offset_of_skeletonUtility_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_t1014382506, ___skeletonUtility_3)); }
	inline SkeletonUtility_t2980767925 * get_skeletonUtility_3() const { return ___skeletonUtility_3; }
	inline SkeletonUtility_t2980767925 ** get_address_of_skeletonUtility_3() { return &___skeletonUtility_3; }
	inline void set_skeletonUtility_3(SkeletonUtility_t2980767925 * value)
	{
		___skeletonUtility_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYCONSTRAINT_T1014382506_H
#ifndef SKELETONUTILITYBONE_T2225520134_H
#define SKELETONUTILITYBONE_T2225520134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone
struct  SkeletonUtilityBone_t2225520134  : public MonoBehaviour_t3962482529
{
public:
	// System.String Spine.Unity.SkeletonUtilityBone::boneName
	String_t* ___boneName_2;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::parentReference
	Transform_t3600365921 * ___parentReference_3;
	// Spine.Unity.SkeletonUtilityBone/Mode Spine.Unity.SkeletonUtilityBone::mode
	int32_t ___mode_4;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::position
	bool ___position_5;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::rotation
	bool ___rotation_6;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::scale
	bool ___scale_7;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::zPosition
	bool ___zPosition_8;
	// System.Single Spine.Unity.SkeletonUtilityBone::overrideAlpha
	float ___overrideAlpha_9;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityBone::skeletonUtility
	SkeletonUtility_t2980767925 * ___skeletonUtility_10;
	// Spine.Bone Spine.Unity.SkeletonUtilityBone::bone
	Bone_t1086356328 * ___bone_11;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::transformLerpComplete
	bool ___transformLerpComplete_12;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::valid
	bool ___valid_13;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::cachedTransform
	Transform_t3600365921 * ___cachedTransform_14;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_15;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::incompatibleTransformMode
	bool ___incompatibleTransformMode_16;

public:
	inline static int32_t get_offset_of_boneName_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___boneName_2)); }
	inline String_t* get_boneName_2() const { return ___boneName_2; }
	inline String_t** get_address_of_boneName_2() { return &___boneName_2; }
	inline void set_boneName_2(String_t* value)
	{
		___boneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_2), value);
	}

	inline static int32_t get_offset_of_parentReference_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___parentReference_3)); }
	inline Transform_t3600365921 * get_parentReference_3() const { return ___parentReference_3; }
	inline Transform_t3600365921 ** get_address_of_parentReference_3() { return &___parentReference_3; }
	inline void set_parentReference_3(Transform_t3600365921 * value)
	{
		___parentReference_3 = value;
		Il2CppCodeGenWriteBarrier((&___parentReference_3), value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___position_5)); }
	inline bool get_position_5() const { return ___position_5; }
	inline bool* get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(bool value)
	{
		___position_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___rotation_6)); }
	inline bool get_rotation_6() const { return ___rotation_6; }
	inline bool* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(bool value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___scale_7)); }
	inline bool get_scale_7() const { return ___scale_7; }
	inline bool* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(bool value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_zPosition_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___zPosition_8)); }
	inline bool get_zPosition_8() const { return ___zPosition_8; }
	inline bool* get_address_of_zPosition_8() { return &___zPosition_8; }
	inline void set_zPosition_8(bool value)
	{
		___zPosition_8 = value;
	}

	inline static int32_t get_offset_of_overrideAlpha_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___overrideAlpha_9)); }
	inline float get_overrideAlpha_9() const { return ___overrideAlpha_9; }
	inline float* get_address_of_overrideAlpha_9() { return &___overrideAlpha_9; }
	inline void set_overrideAlpha_9(float value)
	{
		___overrideAlpha_9 = value;
	}

	inline static int32_t get_offset_of_skeletonUtility_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___skeletonUtility_10)); }
	inline SkeletonUtility_t2980767925 * get_skeletonUtility_10() const { return ___skeletonUtility_10; }
	inline SkeletonUtility_t2980767925 ** get_address_of_skeletonUtility_10() { return &___skeletonUtility_10; }
	inline void set_skeletonUtility_10(SkeletonUtility_t2980767925 * value)
	{
		___skeletonUtility_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_10), value);
	}

	inline static int32_t get_offset_of_bone_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___bone_11)); }
	inline Bone_t1086356328 * get_bone_11() const { return ___bone_11; }
	inline Bone_t1086356328 ** get_address_of_bone_11() { return &___bone_11; }
	inline void set_bone_11(Bone_t1086356328 * value)
	{
		___bone_11 = value;
		Il2CppCodeGenWriteBarrier((&___bone_11), value);
	}

	inline static int32_t get_offset_of_transformLerpComplete_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___transformLerpComplete_12)); }
	inline bool get_transformLerpComplete_12() const { return ___transformLerpComplete_12; }
	inline bool* get_address_of_transformLerpComplete_12() { return &___transformLerpComplete_12; }
	inline void set_transformLerpComplete_12(bool value)
	{
		___transformLerpComplete_12 = value;
	}

	inline static int32_t get_offset_of_valid_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___valid_13)); }
	inline bool get_valid_13() const { return ___valid_13; }
	inline bool* get_address_of_valid_13() { return &___valid_13; }
	inline void set_valid_13(bool value)
	{
		___valid_13 = value;
	}

	inline static int32_t get_offset_of_cachedTransform_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___cachedTransform_14)); }
	inline Transform_t3600365921 * get_cachedTransform_14() const { return ___cachedTransform_14; }
	inline Transform_t3600365921 ** get_address_of_cachedTransform_14() { return &___cachedTransform_14; }
	inline void set_cachedTransform_14(Transform_t3600365921 * value)
	{
		___cachedTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_14), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___skeletonTransform_15)); }
	inline Transform_t3600365921 * get_skeletonTransform_15() const { return ___skeletonTransform_15; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_15() { return &___skeletonTransform_15; }
	inline void set_skeletonTransform_15(Transform_t3600365921 * value)
	{
		___skeletonTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_15), value);
	}

	inline static int32_t get_offset_of_incompatibleTransformMode_16() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___incompatibleTransformMode_16)); }
	inline bool get_incompatibleTransformMode_16() const { return ___incompatibleTransformMode_16; }
	inline bool* get_address_of_incompatibleTransformMode_16() { return &___incompatibleTransformMode_16; }
	inline void set_incompatibleTransformMode_16(bool value)
	{
		___incompatibleTransformMode_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYBONE_T2225520134_H
#ifndef UI_MAINSKILL_SELECT_T3468825974_H
#define UI_MAINSKILL_SELECT_T3468825974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_MainSkill_Select
struct  UI_MainSkill_Select_t3468825974  : public MonoBehaviour_t3962482529
{
public:
	// CShuangYaoGan UI_MainSkill_Select::_ShuangYaoGan
	CShuangYaoGan_t3597386049 * ____ShuangYaoGan_2;
	// Main UI_MainSkill_Select::m_Main
	Main_t2227614074 * ___m_Main_3;
	// System.Boolean UI_MainSkill_Select::m_bSpitting
	bool ___m_bSpitting_5;
	// UnityEngine.Touch UI_MainSkill_Select::_touch
	Touch_t1921856868  ____touch_6;

public:
	inline static int32_t get_offset_of__ShuangYaoGan_2() { return static_cast<int32_t>(offsetof(UI_MainSkill_Select_t3468825974, ____ShuangYaoGan_2)); }
	inline CShuangYaoGan_t3597386049 * get__ShuangYaoGan_2() const { return ____ShuangYaoGan_2; }
	inline CShuangYaoGan_t3597386049 ** get_address_of__ShuangYaoGan_2() { return &____ShuangYaoGan_2; }
	inline void set__ShuangYaoGan_2(CShuangYaoGan_t3597386049 * value)
	{
		____ShuangYaoGan_2 = value;
		Il2CppCodeGenWriteBarrier((&____ShuangYaoGan_2), value);
	}

	inline static int32_t get_offset_of_m_Main_3() { return static_cast<int32_t>(offsetof(UI_MainSkill_Select_t3468825974, ___m_Main_3)); }
	inline Main_t2227614074 * get_m_Main_3() const { return ___m_Main_3; }
	inline Main_t2227614074 ** get_address_of_m_Main_3() { return &___m_Main_3; }
	inline void set_m_Main_3(Main_t2227614074 * value)
	{
		___m_Main_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Main_3), value);
	}

	inline static int32_t get_offset_of_m_bSpitting_5() { return static_cast<int32_t>(offsetof(UI_MainSkill_Select_t3468825974, ___m_bSpitting_5)); }
	inline bool get_m_bSpitting_5() const { return ___m_bSpitting_5; }
	inline bool* get_address_of_m_bSpitting_5() { return &___m_bSpitting_5; }
	inline void set_m_bSpitting_5(bool value)
	{
		___m_bSpitting_5 = value;
	}

	inline static int32_t get_offset_of__touch_6() { return static_cast<int32_t>(offsetof(UI_MainSkill_Select_t3468825974, ____touch_6)); }
	inline Touch_t1921856868  get__touch_6() const { return ____touch_6; }
	inline Touch_t1921856868 * get_address_of__touch_6() { return &____touch_6; }
	inline void set__touch_6(Touch_t1921856868  value)
	{
		____touch_6 = value;
	}
};

struct UI_MainSkill_Select_t3468825974_StaticFields
{
public:
	// System.Boolean UI_MainSkill_Select::s_bUsingUi
	bool ___s_bUsingUi_4;

public:
	inline static int32_t get_offset_of_s_bUsingUi_4() { return static_cast<int32_t>(offsetof(UI_MainSkill_Select_t3468825974_StaticFields, ___s_bUsingUi_4)); }
	inline bool get_s_bUsingUi_4() const { return ___s_bUsingUi_4; }
	inline bool* get_address_of_s_bUsingUi_4() { return &___s_bUsingUi_4; }
	inline void set_s_bUsingUi_4(bool value)
	{
		___s_bUsingUi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_MAINSKILL_SELECT_T3468825974_H
#ifndef UNIWEBVIEW_T941983939_H
#define UNIWEBVIEW_T941983939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView
struct  UniWebView_t941983939  : public MonoBehaviour_t3962482529
{
public:
	// UniWebView/PageStartedDelegate UniWebView::OnPageStarted
	PageStartedDelegate_t2830724344 * ___OnPageStarted_2;
	// UniWebView/PageFinishedDelegate UniWebView::OnPageFinished
	PageFinishedDelegate_t2717015276 * ___OnPageFinished_3;
	// UniWebView/PageErrorReceivedDelegate UniWebView::OnPageErrorReceived
	PageErrorReceivedDelegate_t1724664023 * ___OnPageErrorReceived_4;
	// UniWebView/MessageReceivedDelegate UniWebView::OnMessageReceived
	MessageReceivedDelegate_t2288957136 * ___OnMessageReceived_5;
	// UniWebView/ShouldCloseDelegate UniWebView::OnShouldClose
	ShouldCloseDelegate_t766319959 * ___OnShouldClose_6;
	// UniWebView/KeyCodeReceivedDelegate UniWebView::OnKeyCodeReceived
	KeyCodeReceivedDelegate_t3839084677 * ___OnKeyCodeReceived_7;
	// UniWebView/OreintationChangedDelegate UniWebView::OnOreintationChanged
	OreintationChangedDelegate_t1877368362 * ___OnOreintationChanged_8;
	// System.String UniWebView::id
	String_t* ___id_9;
	// UniWebViewNativeListener UniWebView::listener
	UniWebViewNativeListener_t1145263134 * ___listener_10;
	// System.Boolean UniWebView::isPortrait
	bool ___isPortrait_11;
	// System.String UniWebView::urlOnStart
	String_t* ___urlOnStart_12;
	// System.Boolean UniWebView::showOnStart
	bool ___showOnStart_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action> UniWebView::actions
	Dictionary_2_t1049633776 * ___actions_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<UniWebViewNativeResultPayload>> UniWebView::payloadActions
	Dictionary_2_t4030463511 * ___payloadActions_15;
	// System.Boolean UniWebView::fullScreen
	bool ___fullScreen_16;
	// UnityEngine.Rect UniWebView::frame
	Rect_t2360479859  ___frame_17;
	// UnityEngine.RectTransform UniWebView::referenceRectTransform
	RectTransform_t3704657025 * ___referenceRectTransform_18;
	// System.Boolean UniWebView::useToolbar
	bool ___useToolbar_19;
	// UniWebViewToolbarPosition UniWebView::toolbarPosition
	int32_t ___toolbarPosition_20;
	// System.Boolean UniWebView::started
	bool ___started_21;
	// UnityEngine.Color UniWebView::backgroundColor
	Color_t2555686324  ___backgroundColor_22;

public:
	inline static int32_t get_offset_of_OnPageStarted_2() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnPageStarted_2)); }
	inline PageStartedDelegate_t2830724344 * get_OnPageStarted_2() const { return ___OnPageStarted_2; }
	inline PageStartedDelegate_t2830724344 ** get_address_of_OnPageStarted_2() { return &___OnPageStarted_2; }
	inline void set_OnPageStarted_2(PageStartedDelegate_t2830724344 * value)
	{
		___OnPageStarted_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnPageStarted_2), value);
	}

	inline static int32_t get_offset_of_OnPageFinished_3() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnPageFinished_3)); }
	inline PageFinishedDelegate_t2717015276 * get_OnPageFinished_3() const { return ___OnPageFinished_3; }
	inline PageFinishedDelegate_t2717015276 ** get_address_of_OnPageFinished_3() { return &___OnPageFinished_3; }
	inline void set_OnPageFinished_3(PageFinishedDelegate_t2717015276 * value)
	{
		___OnPageFinished_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPageFinished_3), value);
	}

	inline static int32_t get_offset_of_OnPageErrorReceived_4() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnPageErrorReceived_4)); }
	inline PageErrorReceivedDelegate_t1724664023 * get_OnPageErrorReceived_4() const { return ___OnPageErrorReceived_4; }
	inline PageErrorReceivedDelegate_t1724664023 ** get_address_of_OnPageErrorReceived_4() { return &___OnPageErrorReceived_4; }
	inline void set_OnPageErrorReceived_4(PageErrorReceivedDelegate_t1724664023 * value)
	{
		___OnPageErrorReceived_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPageErrorReceived_4), value);
	}

	inline static int32_t get_offset_of_OnMessageReceived_5() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnMessageReceived_5)); }
	inline MessageReceivedDelegate_t2288957136 * get_OnMessageReceived_5() const { return ___OnMessageReceived_5; }
	inline MessageReceivedDelegate_t2288957136 ** get_address_of_OnMessageReceived_5() { return &___OnMessageReceived_5; }
	inline void set_OnMessageReceived_5(MessageReceivedDelegate_t2288957136 * value)
	{
		___OnMessageReceived_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessageReceived_5), value);
	}

	inline static int32_t get_offset_of_OnShouldClose_6() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnShouldClose_6)); }
	inline ShouldCloseDelegate_t766319959 * get_OnShouldClose_6() const { return ___OnShouldClose_6; }
	inline ShouldCloseDelegate_t766319959 ** get_address_of_OnShouldClose_6() { return &___OnShouldClose_6; }
	inline void set_OnShouldClose_6(ShouldCloseDelegate_t766319959 * value)
	{
		___OnShouldClose_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnShouldClose_6), value);
	}

	inline static int32_t get_offset_of_OnKeyCodeReceived_7() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnKeyCodeReceived_7)); }
	inline KeyCodeReceivedDelegate_t3839084677 * get_OnKeyCodeReceived_7() const { return ___OnKeyCodeReceived_7; }
	inline KeyCodeReceivedDelegate_t3839084677 ** get_address_of_OnKeyCodeReceived_7() { return &___OnKeyCodeReceived_7; }
	inline void set_OnKeyCodeReceived_7(KeyCodeReceivedDelegate_t3839084677 * value)
	{
		___OnKeyCodeReceived_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnKeyCodeReceived_7), value);
	}

	inline static int32_t get_offset_of_OnOreintationChanged_8() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___OnOreintationChanged_8)); }
	inline OreintationChangedDelegate_t1877368362 * get_OnOreintationChanged_8() const { return ___OnOreintationChanged_8; }
	inline OreintationChangedDelegate_t1877368362 ** get_address_of_OnOreintationChanged_8() { return &___OnOreintationChanged_8; }
	inline void set_OnOreintationChanged_8(OreintationChangedDelegate_t1877368362 * value)
	{
		___OnOreintationChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnOreintationChanged_8), value);
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___id_9)); }
	inline String_t* get_id_9() const { return ___id_9; }
	inline String_t** get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(String_t* value)
	{
		___id_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_9), value);
	}

	inline static int32_t get_offset_of_listener_10() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___listener_10)); }
	inline UniWebViewNativeListener_t1145263134 * get_listener_10() const { return ___listener_10; }
	inline UniWebViewNativeListener_t1145263134 ** get_address_of_listener_10() { return &___listener_10; }
	inline void set_listener_10(UniWebViewNativeListener_t1145263134 * value)
	{
		___listener_10 = value;
		Il2CppCodeGenWriteBarrier((&___listener_10), value);
	}

	inline static int32_t get_offset_of_isPortrait_11() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___isPortrait_11)); }
	inline bool get_isPortrait_11() const { return ___isPortrait_11; }
	inline bool* get_address_of_isPortrait_11() { return &___isPortrait_11; }
	inline void set_isPortrait_11(bool value)
	{
		___isPortrait_11 = value;
	}

	inline static int32_t get_offset_of_urlOnStart_12() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___urlOnStart_12)); }
	inline String_t* get_urlOnStart_12() const { return ___urlOnStart_12; }
	inline String_t** get_address_of_urlOnStart_12() { return &___urlOnStart_12; }
	inline void set_urlOnStart_12(String_t* value)
	{
		___urlOnStart_12 = value;
		Il2CppCodeGenWriteBarrier((&___urlOnStart_12), value);
	}

	inline static int32_t get_offset_of_showOnStart_13() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___showOnStart_13)); }
	inline bool get_showOnStart_13() const { return ___showOnStart_13; }
	inline bool* get_address_of_showOnStart_13() { return &___showOnStart_13; }
	inline void set_showOnStart_13(bool value)
	{
		___showOnStart_13 = value;
	}

	inline static int32_t get_offset_of_actions_14() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___actions_14)); }
	inline Dictionary_2_t1049633776 * get_actions_14() const { return ___actions_14; }
	inline Dictionary_2_t1049633776 ** get_address_of_actions_14() { return &___actions_14; }
	inline void set_actions_14(Dictionary_2_t1049633776 * value)
	{
		___actions_14 = value;
		Il2CppCodeGenWriteBarrier((&___actions_14), value);
	}

	inline static int32_t get_offset_of_payloadActions_15() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___payloadActions_15)); }
	inline Dictionary_2_t4030463511 * get_payloadActions_15() const { return ___payloadActions_15; }
	inline Dictionary_2_t4030463511 ** get_address_of_payloadActions_15() { return &___payloadActions_15; }
	inline void set_payloadActions_15(Dictionary_2_t4030463511 * value)
	{
		___payloadActions_15 = value;
		Il2CppCodeGenWriteBarrier((&___payloadActions_15), value);
	}

	inline static int32_t get_offset_of_fullScreen_16() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___fullScreen_16)); }
	inline bool get_fullScreen_16() const { return ___fullScreen_16; }
	inline bool* get_address_of_fullScreen_16() { return &___fullScreen_16; }
	inline void set_fullScreen_16(bool value)
	{
		___fullScreen_16 = value;
	}

	inline static int32_t get_offset_of_frame_17() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___frame_17)); }
	inline Rect_t2360479859  get_frame_17() const { return ___frame_17; }
	inline Rect_t2360479859 * get_address_of_frame_17() { return &___frame_17; }
	inline void set_frame_17(Rect_t2360479859  value)
	{
		___frame_17 = value;
	}

	inline static int32_t get_offset_of_referenceRectTransform_18() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___referenceRectTransform_18)); }
	inline RectTransform_t3704657025 * get_referenceRectTransform_18() const { return ___referenceRectTransform_18; }
	inline RectTransform_t3704657025 ** get_address_of_referenceRectTransform_18() { return &___referenceRectTransform_18; }
	inline void set_referenceRectTransform_18(RectTransform_t3704657025 * value)
	{
		___referenceRectTransform_18 = value;
		Il2CppCodeGenWriteBarrier((&___referenceRectTransform_18), value);
	}

	inline static int32_t get_offset_of_useToolbar_19() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___useToolbar_19)); }
	inline bool get_useToolbar_19() const { return ___useToolbar_19; }
	inline bool* get_address_of_useToolbar_19() { return &___useToolbar_19; }
	inline void set_useToolbar_19(bool value)
	{
		___useToolbar_19 = value;
	}

	inline static int32_t get_offset_of_toolbarPosition_20() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___toolbarPosition_20)); }
	inline int32_t get_toolbarPosition_20() const { return ___toolbarPosition_20; }
	inline int32_t* get_address_of_toolbarPosition_20() { return &___toolbarPosition_20; }
	inline void set_toolbarPosition_20(int32_t value)
	{
		___toolbarPosition_20 = value;
	}

	inline static int32_t get_offset_of_started_21() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___started_21)); }
	inline bool get_started_21() const { return ___started_21; }
	inline bool* get_address_of_started_21() { return &___started_21; }
	inline void set_started_21(bool value)
	{
		___started_21 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_22() { return static_cast<int32_t>(offsetof(UniWebView_t941983939, ___backgroundColor_22)); }
	inline Color_t2555686324  get_backgroundColor_22() const { return ___backgroundColor_22; }
	inline Color_t2555686324 * get_address_of_backgroundColor_22() { return &___backgroundColor_22; }
	inline void set_backgroundColor_22(Color_t2555686324  value)
	{
		___backgroundColor_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEW_T941983939_H
#ifndef SKELETONUTILITY_T2980767925_H
#define SKELETONUTILITY_T2980767925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility
struct  SkeletonUtility_t2980767925  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate Spine.Unity.SkeletonUtility::OnReset
	SkeletonUtilityDelegate_t487527757 * ___OnReset_2;
	// UnityEngine.Transform Spine.Unity.SkeletonUtility::boneRoot
	Transform_t3600365921 * ___boneRoot_3;
	// Spine.Unity.SkeletonRenderer Spine.Unity.SkeletonUtility::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_4;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.SkeletonUtility::skeletonAnimation
	RuntimeObject* ___skeletonAnimation_5;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone> Spine.Unity.SkeletonUtility::utilityBones
	List_1_t3697594876 * ___utilityBones_6;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint> Spine.Unity.SkeletonUtility::utilityConstraints
	List_1_t2486457248 * ___utilityConstraints_7;
	// System.Boolean Spine.Unity.SkeletonUtility::hasTransformBones
	bool ___hasTransformBones_8;
	// System.Boolean Spine.Unity.SkeletonUtility::hasUtilityConstraints
	bool ___hasUtilityConstraints_9;
	// System.Boolean Spine.Unity.SkeletonUtility::needToReprocessBones
	bool ___needToReprocessBones_10;

public:
	inline static int32_t get_offset_of_OnReset_2() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___OnReset_2)); }
	inline SkeletonUtilityDelegate_t487527757 * get_OnReset_2() const { return ___OnReset_2; }
	inline SkeletonUtilityDelegate_t487527757 ** get_address_of_OnReset_2() { return &___OnReset_2; }
	inline void set_OnReset_2(SkeletonUtilityDelegate_t487527757 * value)
	{
		___OnReset_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnReset_2), value);
	}

	inline static int32_t get_offset_of_boneRoot_3() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___boneRoot_3)); }
	inline Transform_t3600365921 * get_boneRoot_3() const { return ___boneRoot_3; }
	inline Transform_t3600365921 ** get_address_of_boneRoot_3() { return &___boneRoot_3; }
	inline void set_boneRoot_3(Transform_t3600365921 * value)
	{
		___boneRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___boneRoot_3), value);
	}

	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_skeletonAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___skeletonAnimation_5)); }
	inline RuntimeObject* get_skeletonAnimation_5() const { return ___skeletonAnimation_5; }
	inline RuntimeObject** get_address_of_skeletonAnimation_5() { return &___skeletonAnimation_5; }
	inline void set_skeletonAnimation_5(RuntimeObject* value)
	{
		___skeletonAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_5), value);
	}

	inline static int32_t get_offset_of_utilityBones_6() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___utilityBones_6)); }
	inline List_1_t3697594876 * get_utilityBones_6() const { return ___utilityBones_6; }
	inline List_1_t3697594876 ** get_address_of_utilityBones_6() { return &___utilityBones_6; }
	inline void set_utilityBones_6(List_1_t3697594876 * value)
	{
		___utilityBones_6 = value;
		Il2CppCodeGenWriteBarrier((&___utilityBones_6), value);
	}

	inline static int32_t get_offset_of_utilityConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___utilityConstraints_7)); }
	inline List_1_t2486457248 * get_utilityConstraints_7() const { return ___utilityConstraints_7; }
	inline List_1_t2486457248 ** get_address_of_utilityConstraints_7() { return &___utilityConstraints_7; }
	inline void set_utilityConstraints_7(List_1_t2486457248 * value)
	{
		___utilityConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___utilityConstraints_7), value);
	}

	inline static int32_t get_offset_of_hasTransformBones_8() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___hasTransformBones_8)); }
	inline bool get_hasTransformBones_8() const { return ___hasTransformBones_8; }
	inline bool* get_address_of_hasTransformBones_8() { return &___hasTransformBones_8; }
	inline void set_hasTransformBones_8(bool value)
	{
		___hasTransformBones_8 = value;
	}

	inline static int32_t get_offset_of_hasUtilityConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___hasUtilityConstraints_9)); }
	inline bool get_hasUtilityConstraints_9() const { return ___hasUtilityConstraints_9; }
	inline bool* get_address_of_hasUtilityConstraints_9() { return &___hasUtilityConstraints_9; }
	inline void set_hasUtilityConstraints_9(bool value)
	{
		___hasUtilityConstraints_9 = value;
	}

	inline static int32_t get_offset_of_needToReprocessBones_10() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___needToReprocessBones_10)); }
	inline bool get_needToReprocessBones_10() const { return ___needToReprocessBones_10; }
	inline bool* get_address_of_needToReprocessBones_10() { return &___needToReprocessBones_10; }
	inline void set_needToReprocessBones_10(bool value)
	{
		___needToReprocessBones_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITY_T2980767925_H
#ifndef UI_SPIT_T1775598180_H
#define UI_SPIT_T1775598180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Spit
struct  UI_Spit_t1775598180  : public MonoBehaviour_t3962482529
{
public:
	// CShuangYaoGan UI_Spit::_ShuangYaoGan
	CShuangYaoGan_t3597386049 * ____ShuangYaoGan_2;
	// Main UI_Spit::m_Main
	Main_t2227614074 * ___m_Main_3;
	// System.Boolean UI_Spit::m_bSpitting
	bool ___m_bSpitting_5;
	// UnityEngine.Touch UI_Spit::_touch
	Touch_t1921856868  ____touch_6;

public:
	inline static int32_t get_offset_of__ShuangYaoGan_2() { return static_cast<int32_t>(offsetof(UI_Spit_t1775598180, ____ShuangYaoGan_2)); }
	inline CShuangYaoGan_t3597386049 * get__ShuangYaoGan_2() const { return ____ShuangYaoGan_2; }
	inline CShuangYaoGan_t3597386049 ** get_address_of__ShuangYaoGan_2() { return &____ShuangYaoGan_2; }
	inline void set__ShuangYaoGan_2(CShuangYaoGan_t3597386049 * value)
	{
		____ShuangYaoGan_2 = value;
		Il2CppCodeGenWriteBarrier((&____ShuangYaoGan_2), value);
	}

	inline static int32_t get_offset_of_m_Main_3() { return static_cast<int32_t>(offsetof(UI_Spit_t1775598180, ___m_Main_3)); }
	inline Main_t2227614074 * get_m_Main_3() const { return ___m_Main_3; }
	inline Main_t2227614074 ** get_address_of_m_Main_3() { return &___m_Main_3; }
	inline void set_m_Main_3(Main_t2227614074 * value)
	{
		___m_Main_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Main_3), value);
	}

	inline static int32_t get_offset_of_m_bSpitting_5() { return static_cast<int32_t>(offsetof(UI_Spit_t1775598180, ___m_bSpitting_5)); }
	inline bool get_m_bSpitting_5() const { return ___m_bSpitting_5; }
	inline bool* get_address_of_m_bSpitting_5() { return &___m_bSpitting_5; }
	inline void set_m_bSpitting_5(bool value)
	{
		___m_bSpitting_5 = value;
	}

	inline static int32_t get_offset_of__touch_6() { return static_cast<int32_t>(offsetof(UI_Spit_t1775598180, ____touch_6)); }
	inline Touch_t1921856868  get__touch_6() const { return ____touch_6; }
	inline Touch_t1921856868 * get_address_of__touch_6() { return &____touch_6; }
	inline void set__touch_6(Touch_t1921856868  value)
	{
		____touch_6 = value;
	}
};

struct UI_Spit_t1775598180_StaticFields
{
public:
	// System.Boolean UI_Spit::s_bUsingUi
	bool ___s_bUsingUi_4;

public:
	inline static int32_t get_offset_of_s_bUsingUi_4() { return static_cast<int32_t>(offsetof(UI_Spit_t1775598180_StaticFields, ___s_bUsingUi_4)); }
	inline bool get_s_bUsingUi_4() const { return ___s_bUsingUi_4; }
	inline bool* get_address_of_s_bUsingUi_4() { return &___s_bUsingUi_4; }
	inline void set_s_bUsingUi_4(bool value)
	{
		___s_bUsingUi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_SPIT_T1775598180_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SKELETONANIMATION_T3693186521_H
#define SKELETONANIMATION_T3693186521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimation
struct  SkeletonAnimation_t3693186521  : public SkeletonRenderer_t2098681813
{
public:
	// Spine.AnimationState Spine.Unity.SkeletonAnimation::state
	AnimationState_t3637309382 * ___state_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateLocal
	UpdateBonesDelegate_t735903178 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateWorld
	UpdateBonesDelegate_t735903178 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateComplete
	UpdateBonesDelegate_t735903178 * ____UpdateComplete_34;
	// System.String Spine.Unity.SkeletonAnimation::_animationName
	String_t* ____animationName_35;
	// System.Boolean Spine.Unity.SkeletonAnimation::loop
	bool ___loop_36;
	// System.Single Spine.Unity.SkeletonAnimation::timeScale
	float ___timeScale_37;

public:
	inline static int32_t get_offset_of_state_31() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ___state_31)); }
	inline AnimationState_t3637309382 * get_state_31() const { return ___state_31; }
	inline AnimationState_t3637309382 ** get_address_of_state_31() { return &___state_31; }
	inline void set_state_31(AnimationState_t3637309382 * value)
	{
		___state_31 = value;
		Il2CppCodeGenWriteBarrier((&___state_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}

	inline static int32_t get_offset_of__animationName_35() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____animationName_35)); }
	inline String_t* get__animationName_35() const { return ____animationName_35; }
	inline String_t** get_address_of__animationName_35() { return &____animationName_35; }
	inline void set__animationName_35(String_t* value)
	{
		____animationName_35 = value;
		Il2CppCodeGenWriteBarrier((&____animationName_35), value);
	}

	inline static int32_t get_offset_of_loop_36() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ___loop_36)); }
	inline bool get_loop_36() const { return ___loop_36; }
	inline bool* get_address_of_loop_36() { return &___loop_36; }
	inline void set_loop_36(bool value)
	{
		___loop_36 = value;
	}

	inline static int32_t get_offset_of_timeScale_37() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ___timeScale_37)); }
	inline float get_timeScale_37() const { return ___timeScale_37; }
	inline float* get_address_of_timeScale_37() { return &___timeScale_37; }
	inline void set_timeScale_37(float value)
	{
		___timeScale_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATION_T3693186521_H
#ifndef SPRAY_T722157414_H
#define SPRAY_T722157414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spray
struct  Spray_t722157414  : public MapObj_t1733252447
{
public:
	// UnityEngine.TextMesh Spray::_txtSpraySize
	TextMesh_t1536577757 * ____txtSpraySize_17;
	// UnityEngine.TextMesh Spray::_txtCurMeatBeanLeftTime
	TextMesh_t1536577757 * ____txtCurMeatBeanLeftTime_18;
	// System.Single Spray::m_fRotationZ
	float ___m_fRotationZ_19;
	// System.Single Spray::m_fSprayInterval
	float ___m_fSprayInterval_20;
	// System.Single Spray::m_fPreSprayTime
	float ___m_fPreSprayTime_21;
	// System.Single Spray::m_fDensity
	float ___m_fDensity_22;
	// System.Single Spray::m_fMeatDensity
	float ___m_fMeatDensity_23;
	// System.Single Spray::m_fMaxDis
	float ___m_fMaxDis_24;
	// System.Single Spray::m_fMinDis
	float ___m_fMinDis_25;
	// System.Single Spray::m_fBeanLifeTime
	float ___m_fBeanLifeTime_26;
	// System.Single Spray::m_fMeat2BeanSprayTime
	float ___m_fMeat2BeanSprayTime_27;
	// System.Single Spray::m_fSpraySize
	float ___m_fSpraySize_28;
	// System.Single Spray::m_fDrawSpeed
	float ___m_fDrawSpeed_29;
	// System.Single Spray::m_fDrawArea
	float ___m_fDrawArea_30;
	// System.Single Spray::m_fPreDrawTime
	float ___m_fPreDrawTime_31;
	// System.Single Spray::m_fDrawTime
	float ___m_fDrawTime_32;
	// System.Single Spray::m_fDrawInterval
	float ___m_fDrawInterval_33;
	// UnityEngine.GameObject Spray::_goMain
	GameObject_t1113636619 * ____goMain_34;
	// UnityEngine.GameObject Spray::_goSprayedBeansContainer
	GameObject_t1113636619 * ____goSprayedBeansContainer_35;
	// System.Int32 Spray::m_nBeanCountForthornChance
	int32_t ___m_nBeanCountForthornChance_39;
	// System.Int32 Spray::m_nThornChance
	int32_t ___m_nThornChance_40;
	// UnityEngine.CircleCollider2D Spray::_CollierMain
	CircleCollider2D_t662546754 * ____CollierMain_41;
	// UnityEngine.CircleCollider2D Spray::_CollierObsCircle
	CircleCollider2D_t662546754 * ____CollierObsCircle_42;
	// System.Int32 Spray::m_nGUID
	int32_t ___m_nGUID_43;
	// System.Single Spray::m_fCurMeatBeanLeftTime
	float ___m_fCurMeatBeanLeftTime_44;
	// System.Single Spray::m_fSprayTimeCount
	float ___m_fSprayTimeCount_45;
	// System.Collections.Generic.List`1<Thorn> Spray::m_lstThorns
	List_1_t4012719951 * ___m_lstThorns_47;
	// System.Single Spray::m_fMeatBeanSprayCount
	float ___m_fMeatBeanSprayCount_48;
	// System.Boolean Spray::m_bPreSpraying
	bool ___m_bPreSpraying_49;
	// System.Single Spray::m_fMeatBeanSprayInterval
	float ___m_fMeatBeanSprayInterval_50;
	// System.Single Spray::m_fRadius
	float ___m_fRadius_51;
	// System.Single Spray::m_fBeginPreDrawTime
	float ___m_fBeginPreDrawTime_52;
	// System.Int32 Spray::m_nDrawStatus
	int32_t ___m_nDrawStatus_53;
	// System.Single Spray::m_fBeginDrawTime
	float ___m_fBeginDrawTime_54;
	// System.Collections.Generic.List`1<Ball> Spray::m_lstCapturedBalls
	List_1_t3678741308 * ___m_lstCapturedBalls_55;
	// System.Single Spray::m_fMeatBeanLeftTimeShowInterval
	float ___m_fMeatBeanLeftTimeShowInterval_56;

public:
	inline static int32_t get_offset_of__txtSpraySize_17() { return static_cast<int32_t>(offsetof(Spray_t722157414, ____txtSpraySize_17)); }
	inline TextMesh_t1536577757 * get__txtSpraySize_17() const { return ____txtSpraySize_17; }
	inline TextMesh_t1536577757 ** get_address_of__txtSpraySize_17() { return &____txtSpraySize_17; }
	inline void set__txtSpraySize_17(TextMesh_t1536577757 * value)
	{
		____txtSpraySize_17 = value;
		Il2CppCodeGenWriteBarrier((&____txtSpraySize_17), value);
	}

	inline static int32_t get_offset_of__txtCurMeatBeanLeftTime_18() { return static_cast<int32_t>(offsetof(Spray_t722157414, ____txtCurMeatBeanLeftTime_18)); }
	inline TextMesh_t1536577757 * get__txtCurMeatBeanLeftTime_18() const { return ____txtCurMeatBeanLeftTime_18; }
	inline TextMesh_t1536577757 ** get_address_of__txtCurMeatBeanLeftTime_18() { return &____txtCurMeatBeanLeftTime_18; }
	inline void set__txtCurMeatBeanLeftTime_18(TextMesh_t1536577757 * value)
	{
		____txtCurMeatBeanLeftTime_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurMeatBeanLeftTime_18), value);
	}

	inline static int32_t get_offset_of_m_fRotationZ_19() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fRotationZ_19)); }
	inline float get_m_fRotationZ_19() const { return ___m_fRotationZ_19; }
	inline float* get_address_of_m_fRotationZ_19() { return &___m_fRotationZ_19; }
	inline void set_m_fRotationZ_19(float value)
	{
		___m_fRotationZ_19 = value;
	}

	inline static int32_t get_offset_of_m_fSprayInterval_20() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fSprayInterval_20)); }
	inline float get_m_fSprayInterval_20() const { return ___m_fSprayInterval_20; }
	inline float* get_address_of_m_fSprayInterval_20() { return &___m_fSprayInterval_20; }
	inline void set_m_fSprayInterval_20(float value)
	{
		___m_fSprayInterval_20 = value;
	}

	inline static int32_t get_offset_of_m_fPreSprayTime_21() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fPreSprayTime_21)); }
	inline float get_m_fPreSprayTime_21() const { return ___m_fPreSprayTime_21; }
	inline float* get_address_of_m_fPreSprayTime_21() { return &___m_fPreSprayTime_21; }
	inline void set_m_fPreSprayTime_21(float value)
	{
		___m_fPreSprayTime_21 = value;
	}

	inline static int32_t get_offset_of_m_fDensity_22() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fDensity_22)); }
	inline float get_m_fDensity_22() const { return ___m_fDensity_22; }
	inline float* get_address_of_m_fDensity_22() { return &___m_fDensity_22; }
	inline void set_m_fDensity_22(float value)
	{
		___m_fDensity_22 = value;
	}

	inline static int32_t get_offset_of_m_fMeatDensity_23() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMeatDensity_23)); }
	inline float get_m_fMeatDensity_23() const { return ___m_fMeatDensity_23; }
	inline float* get_address_of_m_fMeatDensity_23() { return &___m_fMeatDensity_23; }
	inline void set_m_fMeatDensity_23(float value)
	{
		___m_fMeatDensity_23 = value;
	}

	inline static int32_t get_offset_of_m_fMaxDis_24() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMaxDis_24)); }
	inline float get_m_fMaxDis_24() const { return ___m_fMaxDis_24; }
	inline float* get_address_of_m_fMaxDis_24() { return &___m_fMaxDis_24; }
	inline void set_m_fMaxDis_24(float value)
	{
		___m_fMaxDis_24 = value;
	}

	inline static int32_t get_offset_of_m_fMinDis_25() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMinDis_25)); }
	inline float get_m_fMinDis_25() const { return ___m_fMinDis_25; }
	inline float* get_address_of_m_fMinDis_25() { return &___m_fMinDis_25; }
	inline void set_m_fMinDis_25(float value)
	{
		___m_fMinDis_25 = value;
	}

	inline static int32_t get_offset_of_m_fBeanLifeTime_26() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fBeanLifeTime_26)); }
	inline float get_m_fBeanLifeTime_26() const { return ___m_fBeanLifeTime_26; }
	inline float* get_address_of_m_fBeanLifeTime_26() { return &___m_fBeanLifeTime_26; }
	inline void set_m_fBeanLifeTime_26(float value)
	{
		___m_fBeanLifeTime_26 = value;
	}

	inline static int32_t get_offset_of_m_fMeat2BeanSprayTime_27() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMeat2BeanSprayTime_27)); }
	inline float get_m_fMeat2BeanSprayTime_27() const { return ___m_fMeat2BeanSprayTime_27; }
	inline float* get_address_of_m_fMeat2BeanSprayTime_27() { return &___m_fMeat2BeanSprayTime_27; }
	inline void set_m_fMeat2BeanSprayTime_27(float value)
	{
		___m_fMeat2BeanSprayTime_27 = value;
	}

	inline static int32_t get_offset_of_m_fSpraySize_28() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fSpraySize_28)); }
	inline float get_m_fSpraySize_28() const { return ___m_fSpraySize_28; }
	inline float* get_address_of_m_fSpraySize_28() { return &___m_fSpraySize_28; }
	inline void set_m_fSpraySize_28(float value)
	{
		___m_fSpraySize_28 = value;
	}

	inline static int32_t get_offset_of_m_fDrawSpeed_29() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fDrawSpeed_29)); }
	inline float get_m_fDrawSpeed_29() const { return ___m_fDrawSpeed_29; }
	inline float* get_address_of_m_fDrawSpeed_29() { return &___m_fDrawSpeed_29; }
	inline void set_m_fDrawSpeed_29(float value)
	{
		___m_fDrawSpeed_29 = value;
	}

	inline static int32_t get_offset_of_m_fDrawArea_30() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fDrawArea_30)); }
	inline float get_m_fDrawArea_30() const { return ___m_fDrawArea_30; }
	inline float* get_address_of_m_fDrawArea_30() { return &___m_fDrawArea_30; }
	inline void set_m_fDrawArea_30(float value)
	{
		___m_fDrawArea_30 = value;
	}

	inline static int32_t get_offset_of_m_fPreDrawTime_31() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fPreDrawTime_31)); }
	inline float get_m_fPreDrawTime_31() const { return ___m_fPreDrawTime_31; }
	inline float* get_address_of_m_fPreDrawTime_31() { return &___m_fPreDrawTime_31; }
	inline void set_m_fPreDrawTime_31(float value)
	{
		___m_fPreDrawTime_31 = value;
	}

	inline static int32_t get_offset_of_m_fDrawTime_32() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fDrawTime_32)); }
	inline float get_m_fDrawTime_32() const { return ___m_fDrawTime_32; }
	inline float* get_address_of_m_fDrawTime_32() { return &___m_fDrawTime_32; }
	inline void set_m_fDrawTime_32(float value)
	{
		___m_fDrawTime_32 = value;
	}

	inline static int32_t get_offset_of_m_fDrawInterval_33() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fDrawInterval_33)); }
	inline float get_m_fDrawInterval_33() const { return ___m_fDrawInterval_33; }
	inline float* get_address_of_m_fDrawInterval_33() { return &___m_fDrawInterval_33; }
	inline void set_m_fDrawInterval_33(float value)
	{
		___m_fDrawInterval_33 = value;
	}

	inline static int32_t get_offset_of__goMain_34() { return static_cast<int32_t>(offsetof(Spray_t722157414, ____goMain_34)); }
	inline GameObject_t1113636619 * get__goMain_34() const { return ____goMain_34; }
	inline GameObject_t1113636619 ** get_address_of__goMain_34() { return &____goMain_34; }
	inline void set__goMain_34(GameObject_t1113636619 * value)
	{
		____goMain_34 = value;
		Il2CppCodeGenWriteBarrier((&____goMain_34), value);
	}

	inline static int32_t get_offset_of__goSprayedBeansContainer_35() { return static_cast<int32_t>(offsetof(Spray_t722157414, ____goSprayedBeansContainer_35)); }
	inline GameObject_t1113636619 * get__goSprayedBeansContainer_35() const { return ____goSprayedBeansContainer_35; }
	inline GameObject_t1113636619 ** get_address_of__goSprayedBeansContainer_35() { return &____goSprayedBeansContainer_35; }
	inline void set__goSprayedBeansContainer_35(GameObject_t1113636619 * value)
	{
		____goSprayedBeansContainer_35 = value;
		Il2CppCodeGenWriteBarrier((&____goSprayedBeansContainer_35), value);
	}

	inline static int32_t get_offset_of_m_nBeanCountForthornChance_39() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_nBeanCountForthornChance_39)); }
	inline int32_t get_m_nBeanCountForthornChance_39() const { return ___m_nBeanCountForthornChance_39; }
	inline int32_t* get_address_of_m_nBeanCountForthornChance_39() { return &___m_nBeanCountForthornChance_39; }
	inline void set_m_nBeanCountForthornChance_39(int32_t value)
	{
		___m_nBeanCountForthornChance_39 = value;
	}

	inline static int32_t get_offset_of_m_nThornChance_40() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_nThornChance_40)); }
	inline int32_t get_m_nThornChance_40() const { return ___m_nThornChance_40; }
	inline int32_t* get_address_of_m_nThornChance_40() { return &___m_nThornChance_40; }
	inline void set_m_nThornChance_40(int32_t value)
	{
		___m_nThornChance_40 = value;
	}

	inline static int32_t get_offset_of__CollierMain_41() { return static_cast<int32_t>(offsetof(Spray_t722157414, ____CollierMain_41)); }
	inline CircleCollider2D_t662546754 * get__CollierMain_41() const { return ____CollierMain_41; }
	inline CircleCollider2D_t662546754 ** get_address_of__CollierMain_41() { return &____CollierMain_41; }
	inline void set__CollierMain_41(CircleCollider2D_t662546754 * value)
	{
		____CollierMain_41 = value;
		Il2CppCodeGenWriteBarrier((&____CollierMain_41), value);
	}

	inline static int32_t get_offset_of__CollierObsCircle_42() { return static_cast<int32_t>(offsetof(Spray_t722157414, ____CollierObsCircle_42)); }
	inline CircleCollider2D_t662546754 * get__CollierObsCircle_42() const { return ____CollierObsCircle_42; }
	inline CircleCollider2D_t662546754 ** get_address_of__CollierObsCircle_42() { return &____CollierObsCircle_42; }
	inline void set__CollierObsCircle_42(CircleCollider2D_t662546754 * value)
	{
		____CollierObsCircle_42 = value;
		Il2CppCodeGenWriteBarrier((&____CollierObsCircle_42), value);
	}

	inline static int32_t get_offset_of_m_nGUID_43() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_nGUID_43)); }
	inline int32_t get_m_nGUID_43() const { return ___m_nGUID_43; }
	inline int32_t* get_address_of_m_nGUID_43() { return &___m_nGUID_43; }
	inline void set_m_nGUID_43(int32_t value)
	{
		___m_nGUID_43 = value;
	}

	inline static int32_t get_offset_of_m_fCurMeatBeanLeftTime_44() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fCurMeatBeanLeftTime_44)); }
	inline float get_m_fCurMeatBeanLeftTime_44() const { return ___m_fCurMeatBeanLeftTime_44; }
	inline float* get_address_of_m_fCurMeatBeanLeftTime_44() { return &___m_fCurMeatBeanLeftTime_44; }
	inline void set_m_fCurMeatBeanLeftTime_44(float value)
	{
		___m_fCurMeatBeanLeftTime_44 = value;
	}

	inline static int32_t get_offset_of_m_fSprayTimeCount_45() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fSprayTimeCount_45)); }
	inline float get_m_fSprayTimeCount_45() const { return ___m_fSprayTimeCount_45; }
	inline float* get_address_of_m_fSprayTimeCount_45() { return &___m_fSprayTimeCount_45; }
	inline void set_m_fSprayTimeCount_45(float value)
	{
		___m_fSprayTimeCount_45 = value;
	}

	inline static int32_t get_offset_of_m_lstThorns_47() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_lstThorns_47)); }
	inline List_1_t4012719951 * get_m_lstThorns_47() const { return ___m_lstThorns_47; }
	inline List_1_t4012719951 ** get_address_of_m_lstThorns_47() { return &___m_lstThorns_47; }
	inline void set_m_lstThorns_47(List_1_t4012719951 * value)
	{
		___m_lstThorns_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstThorns_47), value);
	}

	inline static int32_t get_offset_of_m_fMeatBeanSprayCount_48() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMeatBeanSprayCount_48)); }
	inline float get_m_fMeatBeanSprayCount_48() const { return ___m_fMeatBeanSprayCount_48; }
	inline float* get_address_of_m_fMeatBeanSprayCount_48() { return &___m_fMeatBeanSprayCount_48; }
	inline void set_m_fMeatBeanSprayCount_48(float value)
	{
		___m_fMeatBeanSprayCount_48 = value;
	}

	inline static int32_t get_offset_of_m_bPreSpraying_49() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_bPreSpraying_49)); }
	inline bool get_m_bPreSpraying_49() const { return ___m_bPreSpraying_49; }
	inline bool* get_address_of_m_bPreSpraying_49() { return &___m_bPreSpraying_49; }
	inline void set_m_bPreSpraying_49(bool value)
	{
		___m_bPreSpraying_49 = value;
	}

	inline static int32_t get_offset_of_m_fMeatBeanSprayInterval_50() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMeatBeanSprayInterval_50)); }
	inline float get_m_fMeatBeanSprayInterval_50() const { return ___m_fMeatBeanSprayInterval_50; }
	inline float* get_address_of_m_fMeatBeanSprayInterval_50() { return &___m_fMeatBeanSprayInterval_50; }
	inline void set_m_fMeatBeanSprayInterval_50(float value)
	{
		___m_fMeatBeanSprayInterval_50 = value;
	}

	inline static int32_t get_offset_of_m_fRadius_51() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fRadius_51)); }
	inline float get_m_fRadius_51() const { return ___m_fRadius_51; }
	inline float* get_address_of_m_fRadius_51() { return &___m_fRadius_51; }
	inline void set_m_fRadius_51(float value)
	{
		___m_fRadius_51 = value;
	}

	inline static int32_t get_offset_of_m_fBeginPreDrawTime_52() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fBeginPreDrawTime_52)); }
	inline float get_m_fBeginPreDrawTime_52() const { return ___m_fBeginPreDrawTime_52; }
	inline float* get_address_of_m_fBeginPreDrawTime_52() { return &___m_fBeginPreDrawTime_52; }
	inline void set_m_fBeginPreDrawTime_52(float value)
	{
		___m_fBeginPreDrawTime_52 = value;
	}

	inline static int32_t get_offset_of_m_nDrawStatus_53() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_nDrawStatus_53)); }
	inline int32_t get_m_nDrawStatus_53() const { return ___m_nDrawStatus_53; }
	inline int32_t* get_address_of_m_nDrawStatus_53() { return &___m_nDrawStatus_53; }
	inline void set_m_nDrawStatus_53(int32_t value)
	{
		___m_nDrawStatus_53 = value;
	}

	inline static int32_t get_offset_of_m_fBeginDrawTime_54() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fBeginDrawTime_54)); }
	inline float get_m_fBeginDrawTime_54() const { return ___m_fBeginDrawTime_54; }
	inline float* get_address_of_m_fBeginDrawTime_54() { return &___m_fBeginDrawTime_54; }
	inline void set_m_fBeginDrawTime_54(float value)
	{
		___m_fBeginDrawTime_54 = value;
	}

	inline static int32_t get_offset_of_m_lstCapturedBalls_55() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_lstCapturedBalls_55)); }
	inline List_1_t3678741308 * get_m_lstCapturedBalls_55() const { return ___m_lstCapturedBalls_55; }
	inline List_1_t3678741308 ** get_address_of_m_lstCapturedBalls_55() { return &___m_lstCapturedBalls_55; }
	inline void set_m_lstCapturedBalls_55(List_1_t3678741308 * value)
	{
		___m_lstCapturedBalls_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstCapturedBalls_55), value);
	}

	inline static int32_t get_offset_of_m_fMeatBeanLeftTimeShowInterval_56() { return static_cast<int32_t>(offsetof(Spray_t722157414, ___m_fMeatBeanLeftTimeShowInterval_56)); }
	inline float get_m_fMeatBeanLeftTimeShowInterval_56() const { return ___m_fMeatBeanLeftTimeShowInterval_56; }
	inline float* get_address_of_m_fMeatBeanLeftTimeShowInterval_56() { return &___m_fMeatBeanLeftTimeShowInterval_56; }
	inline void set_m_fMeatBeanLeftTimeShowInterval_56(float value)
	{
		___m_fMeatBeanLeftTimeShowInterval_56 = value;
	}
};

struct Spray_t722157414_StaticFields
{
public:
	// UnityEngine.Vector3 Spray::vecTempScale
	Vector3_t3722313464  ___vecTempScale_36;
	// UnityEngine.Vector3 Spray::vecTempRotation
	Vector3_t3722313464  ___vecTempRotation_37;
	// UnityEngine.Vector3 Spray::vecTempPos
	Vector3_t3722313464  ___vecTempPos_38;
	// System.Int32 Spray::s_Shit
	int32_t ___s_Shit_46;

public:
	inline static int32_t get_offset_of_vecTempScale_36() { return static_cast<int32_t>(offsetof(Spray_t722157414_StaticFields, ___vecTempScale_36)); }
	inline Vector3_t3722313464  get_vecTempScale_36() const { return ___vecTempScale_36; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_36() { return &___vecTempScale_36; }
	inline void set_vecTempScale_36(Vector3_t3722313464  value)
	{
		___vecTempScale_36 = value;
	}

	inline static int32_t get_offset_of_vecTempRotation_37() { return static_cast<int32_t>(offsetof(Spray_t722157414_StaticFields, ___vecTempRotation_37)); }
	inline Vector3_t3722313464  get_vecTempRotation_37() const { return ___vecTempRotation_37; }
	inline Vector3_t3722313464 * get_address_of_vecTempRotation_37() { return &___vecTempRotation_37; }
	inline void set_vecTempRotation_37(Vector3_t3722313464  value)
	{
		___vecTempRotation_37 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_38() { return static_cast<int32_t>(offsetof(Spray_t722157414_StaticFields, ___vecTempPos_38)); }
	inline Vector3_t3722313464  get_vecTempPos_38() const { return ___vecTempPos_38; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_38() { return &___vecTempPos_38; }
	inline void set_vecTempPos_38(Vector3_t3722313464  value)
	{
		___vecTempPos_38 = value;
	}

	inline static int32_t get_offset_of_s_Shit_46() { return static_cast<int32_t>(offsetof(Spray_t722157414_StaticFields, ___s_Shit_46)); }
	inline int32_t get_s_Shit_46() const { return ___s_Shit_46; }
	inline int32_t* get_address_of_s_Shit_46() { return &___s_Shit_46; }
	inline void set_s_Shit_46(int32_t value)
	{
		___s_Shit_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRAY_T722157414_H
#ifndef SKELETONUTILITYEYECONSTRAINT_T2225743321_H
#define SKELETONUTILITYEYECONSTRAINT_T2225743321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityEyeConstraint
struct  SkeletonUtilityEyeConstraint_t2225743321  : public SkeletonUtilityConstraint_t1014382506
{
public:
	// UnityEngine.Transform[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::eyes
	TransformU5BU5D_t807237628* ___eyes_4;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::radius
	float ___radius_5;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityEyeConstraint::target
	Transform_t3600365921 * ___target_6;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::targetPosition
	Vector3_t3722313464  ___targetPosition_7;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::speed
	float ___speed_8;
	// UnityEngine.Vector3[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::origins
	Vector3U5BU5D_t1718750761* ___origins_9;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::centerPoint
	Vector3_t3722313464  ___centerPoint_10;

public:
	inline static int32_t get_offset_of_eyes_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___eyes_4)); }
	inline TransformU5BU5D_t807237628* get_eyes_4() const { return ___eyes_4; }
	inline TransformU5BU5D_t807237628** get_address_of_eyes_4() { return &___eyes_4; }
	inline void set_eyes_4(TransformU5BU5D_t807237628* value)
	{
		___eyes_4 = value;
		Il2CppCodeGenWriteBarrier((&___eyes_4), value);
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_targetPosition_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___targetPosition_7)); }
	inline Vector3_t3722313464  get_targetPosition_7() const { return ___targetPosition_7; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_7() { return &___targetPosition_7; }
	inline void set_targetPosition_7(Vector3_t3722313464  value)
	{
		___targetPosition_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_origins_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___origins_9)); }
	inline Vector3U5BU5D_t1718750761* get_origins_9() const { return ___origins_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of_origins_9() { return &___origins_9; }
	inline void set_origins_9(Vector3U5BU5D_t1718750761* value)
	{
		___origins_9 = value;
		Il2CppCodeGenWriteBarrier((&___origins_9), value);
	}

	inline static int32_t get_offset_of_centerPoint_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___centerPoint_10)); }
	inline Vector3_t3722313464  get_centerPoint_10() const { return ___centerPoint_10; }
	inline Vector3_t3722313464 * get_address_of_centerPoint_10() { return &___centerPoint_10; }
	inline void set_centerPoint_10(Vector3_t3722313464  value)
	{
		___centerPoint_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYEYECONSTRAINT_T2225743321_H
#ifndef SKELETONUTILITYGROUNDCONSTRAINT_T1570100391_H
#define SKELETONUTILITYGROUNDCONSTRAINT_T1570100391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityGroundConstraint
struct  SkeletonUtilityGroundConstraint_t1570100391  : public SkeletonUtilityConstraint_t1014382506
{
public:
	// UnityEngine.LayerMask Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundMask
	LayerMask_t3493934918  ___groundMask_4;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::use2D
	bool ___use2D_5;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::useRadius
	bool ___useRadius_6;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castRadius
	float ___castRadius_7;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castDistance
	float ___castDistance_8;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castOffset
	float ___castOffset_9;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundOffset
	float ___groundOffset_10;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::adjustSpeed
	float ___adjustSpeed_11;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayOrigin
	Vector3_t3722313464  ___rayOrigin_12;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayDir
	Vector3_t3722313464  ___rayDir_13;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::hitY
	float ___hitY_14;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::lastHitY
	float ___lastHitY_15;

public:
	inline static int32_t get_offset_of_groundMask_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___groundMask_4)); }
	inline LayerMask_t3493934918  get_groundMask_4() const { return ___groundMask_4; }
	inline LayerMask_t3493934918 * get_address_of_groundMask_4() { return &___groundMask_4; }
	inline void set_groundMask_4(LayerMask_t3493934918  value)
	{
		___groundMask_4 = value;
	}

	inline static int32_t get_offset_of_use2D_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___use2D_5)); }
	inline bool get_use2D_5() const { return ___use2D_5; }
	inline bool* get_address_of_use2D_5() { return &___use2D_5; }
	inline void set_use2D_5(bool value)
	{
		___use2D_5 = value;
	}

	inline static int32_t get_offset_of_useRadius_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___useRadius_6)); }
	inline bool get_useRadius_6() const { return ___useRadius_6; }
	inline bool* get_address_of_useRadius_6() { return &___useRadius_6; }
	inline void set_useRadius_6(bool value)
	{
		___useRadius_6 = value;
	}

	inline static int32_t get_offset_of_castRadius_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___castRadius_7)); }
	inline float get_castRadius_7() const { return ___castRadius_7; }
	inline float* get_address_of_castRadius_7() { return &___castRadius_7; }
	inline void set_castRadius_7(float value)
	{
		___castRadius_7 = value;
	}

	inline static int32_t get_offset_of_castDistance_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___castDistance_8)); }
	inline float get_castDistance_8() const { return ___castDistance_8; }
	inline float* get_address_of_castDistance_8() { return &___castDistance_8; }
	inline void set_castDistance_8(float value)
	{
		___castDistance_8 = value;
	}

	inline static int32_t get_offset_of_castOffset_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___castOffset_9)); }
	inline float get_castOffset_9() const { return ___castOffset_9; }
	inline float* get_address_of_castOffset_9() { return &___castOffset_9; }
	inline void set_castOffset_9(float value)
	{
		___castOffset_9 = value;
	}

	inline static int32_t get_offset_of_groundOffset_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___groundOffset_10)); }
	inline float get_groundOffset_10() const { return ___groundOffset_10; }
	inline float* get_address_of_groundOffset_10() { return &___groundOffset_10; }
	inline void set_groundOffset_10(float value)
	{
		___groundOffset_10 = value;
	}

	inline static int32_t get_offset_of_adjustSpeed_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___adjustSpeed_11)); }
	inline float get_adjustSpeed_11() const { return ___adjustSpeed_11; }
	inline float* get_address_of_adjustSpeed_11() { return &___adjustSpeed_11; }
	inline void set_adjustSpeed_11(float value)
	{
		___adjustSpeed_11 = value;
	}

	inline static int32_t get_offset_of_rayOrigin_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___rayOrigin_12)); }
	inline Vector3_t3722313464  get_rayOrigin_12() const { return ___rayOrigin_12; }
	inline Vector3_t3722313464 * get_address_of_rayOrigin_12() { return &___rayOrigin_12; }
	inline void set_rayOrigin_12(Vector3_t3722313464  value)
	{
		___rayOrigin_12 = value;
	}

	inline static int32_t get_offset_of_rayDir_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___rayDir_13)); }
	inline Vector3_t3722313464  get_rayDir_13() const { return ___rayDir_13; }
	inline Vector3_t3722313464 * get_address_of_rayDir_13() { return &___rayDir_13; }
	inline void set_rayDir_13(Vector3_t3722313464  value)
	{
		___rayDir_13 = value;
	}

	inline static int32_t get_offset_of_hitY_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___hitY_14)); }
	inline float get_hitY_14() const { return ___hitY_14; }
	inline float* get_address_of_hitY_14() { return &___hitY_14; }
	inline void set_hitY_14(float value)
	{
		___hitY_14 = value;
	}

	inline static int32_t get_offset_of_lastHitY_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___lastHitY_15)); }
	inline float get_lastHitY_15() const { return ___lastHitY_15; }
	inline float* get_address_of_lastHitY_15() { return &___lastHitY_15; }
	inline void set_lastHitY_15(float value)
	{
		___lastHitY_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYGROUNDCONSTRAINT_T1570100391_H
#ifndef THORN_T2540645209_H
#define THORN_T2540645209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Thorn
struct  Thorn_t2540645209  : public MapObj_t1733252447
{
public:
	// Thorn/eThornType Thorn::m_eThornType
	int32_t ___m_eThornType_17;
	// UnityEngine.GameObject Thorn::_main
	GameObject_t1113636619 * ____main_19;
	// UnityEngine.CircleCollider2D Thorn::_Trigger
	CircleCollider2D_t662546754 * ____Trigger_20;
	// UnityEngine.SpriteRenderer Thorn::_srMain
	SpriteRenderer_t3235626157 * ____srMain_21;
	// System.Int32 Thorn::m_nIndex
	int32_t ___m_nIndex_22;
	// System.Int32 Thorn::m_nOwnerId
	int32_t ___m_nOwnerId_23;
	// System.Int32 Thorn::m_nColorIdx
	int32_t ___m_nColorIdx_25;
	// System.Boolean Thorn::m_bIsColorThorn
	bool ___m_bIsColorThorn_27;
	// System.Single Thorn::m_fRebornTime
	float ___m_fRebornTime_29;
	// System.Boolean Thorn::m_bDead
	bool ___m_bDead_30;

public:
	inline static int32_t get_offset_of_m_eThornType_17() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_eThornType_17)); }
	inline int32_t get_m_eThornType_17() const { return ___m_eThornType_17; }
	inline int32_t* get_address_of_m_eThornType_17() { return &___m_eThornType_17; }
	inline void set_m_eThornType_17(int32_t value)
	{
		___m_eThornType_17 = value;
	}

	inline static int32_t get_offset_of__main_19() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ____main_19)); }
	inline GameObject_t1113636619 * get__main_19() const { return ____main_19; }
	inline GameObject_t1113636619 ** get_address_of__main_19() { return &____main_19; }
	inline void set__main_19(GameObject_t1113636619 * value)
	{
		____main_19 = value;
		Il2CppCodeGenWriteBarrier((&____main_19), value);
	}

	inline static int32_t get_offset_of__Trigger_20() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ____Trigger_20)); }
	inline CircleCollider2D_t662546754 * get__Trigger_20() const { return ____Trigger_20; }
	inline CircleCollider2D_t662546754 ** get_address_of__Trigger_20() { return &____Trigger_20; }
	inline void set__Trigger_20(CircleCollider2D_t662546754 * value)
	{
		____Trigger_20 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_20), value);
	}

	inline static int32_t get_offset_of__srMain_21() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ____srMain_21)); }
	inline SpriteRenderer_t3235626157 * get__srMain_21() const { return ____srMain_21; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_21() { return &____srMain_21; }
	inline void set__srMain_21(SpriteRenderer_t3235626157 * value)
	{
		____srMain_21 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_21), value);
	}

	inline static int32_t get_offset_of_m_nIndex_22() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_nIndex_22)); }
	inline int32_t get_m_nIndex_22() const { return ___m_nIndex_22; }
	inline int32_t* get_address_of_m_nIndex_22() { return &___m_nIndex_22; }
	inline void set_m_nIndex_22(int32_t value)
	{
		___m_nIndex_22 = value;
	}

	inline static int32_t get_offset_of_m_nOwnerId_23() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_nOwnerId_23)); }
	inline int32_t get_m_nOwnerId_23() const { return ___m_nOwnerId_23; }
	inline int32_t* get_address_of_m_nOwnerId_23() { return &___m_nOwnerId_23; }
	inline void set_m_nOwnerId_23(int32_t value)
	{
		___m_nOwnerId_23 = value;
	}

	inline static int32_t get_offset_of_m_nColorIdx_25() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_nColorIdx_25)); }
	inline int32_t get_m_nColorIdx_25() const { return ___m_nColorIdx_25; }
	inline int32_t* get_address_of_m_nColorIdx_25() { return &___m_nColorIdx_25; }
	inline void set_m_nColorIdx_25(int32_t value)
	{
		___m_nColorIdx_25 = value;
	}

	inline static int32_t get_offset_of_m_bIsColorThorn_27() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_bIsColorThorn_27)); }
	inline bool get_m_bIsColorThorn_27() const { return ___m_bIsColorThorn_27; }
	inline bool* get_address_of_m_bIsColorThorn_27() { return &___m_bIsColorThorn_27; }
	inline void set_m_bIsColorThorn_27(bool value)
	{
		___m_bIsColorThorn_27 = value;
	}

	inline static int32_t get_offset_of_m_fRebornTime_29() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_fRebornTime_29)); }
	inline float get_m_fRebornTime_29() const { return ___m_fRebornTime_29; }
	inline float* get_address_of_m_fRebornTime_29() { return &___m_fRebornTime_29; }
	inline void set_m_fRebornTime_29(float value)
	{
		___m_fRebornTime_29 = value;
	}

	inline static int32_t get_offset_of_m_bDead_30() { return static_cast<int32_t>(offsetof(Thorn_t2540645209, ___m_bDead_30)); }
	inline bool get_m_bDead_30() const { return ___m_bDead_30; }
	inline bool* get_address_of_m_bDead_30() { return &___m_bDead_30; }
	inline void set_m_bDead_30(bool value)
	{
		___m_bDead_30 = value;
	}
};

struct Thorn_t2540645209_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Single> Thorn::m_lstNewThornApplication
	List_1_t2869341516 * ___m_lstNewThornApplication_18;
	// System.String[] Thorn::c_aryThornColor
	StringU5BU5D_t1281789340* ___c_aryThornColor_24;
	// System.Int32 Thorn::m_nSceneThornRebornIndex
	int32_t ___m_nSceneThornRebornIndex_26;
	// System.Collections.Generic.List`1<Thorn> Thorn::m_lstColorThornRebornList
	List_1_t4012719951 * ___m_lstColorThornRebornList_28;

public:
	inline static int32_t get_offset_of_m_lstNewThornApplication_18() { return static_cast<int32_t>(offsetof(Thorn_t2540645209_StaticFields, ___m_lstNewThornApplication_18)); }
	inline List_1_t2869341516 * get_m_lstNewThornApplication_18() const { return ___m_lstNewThornApplication_18; }
	inline List_1_t2869341516 ** get_address_of_m_lstNewThornApplication_18() { return &___m_lstNewThornApplication_18; }
	inline void set_m_lstNewThornApplication_18(List_1_t2869341516 * value)
	{
		___m_lstNewThornApplication_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstNewThornApplication_18), value);
	}

	inline static int32_t get_offset_of_c_aryThornColor_24() { return static_cast<int32_t>(offsetof(Thorn_t2540645209_StaticFields, ___c_aryThornColor_24)); }
	inline StringU5BU5D_t1281789340* get_c_aryThornColor_24() const { return ___c_aryThornColor_24; }
	inline StringU5BU5D_t1281789340** get_address_of_c_aryThornColor_24() { return &___c_aryThornColor_24; }
	inline void set_c_aryThornColor_24(StringU5BU5D_t1281789340* value)
	{
		___c_aryThornColor_24 = value;
		Il2CppCodeGenWriteBarrier((&___c_aryThornColor_24), value);
	}

	inline static int32_t get_offset_of_m_nSceneThornRebornIndex_26() { return static_cast<int32_t>(offsetof(Thorn_t2540645209_StaticFields, ___m_nSceneThornRebornIndex_26)); }
	inline int32_t get_m_nSceneThornRebornIndex_26() const { return ___m_nSceneThornRebornIndex_26; }
	inline int32_t* get_address_of_m_nSceneThornRebornIndex_26() { return &___m_nSceneThornRebornIndex_26; }
	inline void set_m_nSceneThornRebornIndex_26(int32_t value)
	{
		___m_nSceneThornRebornIndex_26 = value;
	}

	inline static int32_t get_offset_of_m_lstColorThornRebornList_28() { return static_cast<int32_t>(offsetof(Thorn_t2540645209_StaticFields, ___m_lstColorThornRebornList_28)); }
	inline List_1_t4012719951 * get_m_lstColorThornRebornList_28() const { return ___m_lstColorThornRebornList_28; }
	inline List_1_t4012719951 ** get_address_of_m_lstColorThornRebornList_28() { return &___m_lstColorThornRebornList_28; }
	inline void set_m_lstColorThornRebornList_28(List_1_t4012719951 * value)
	{
		___m_lstColorThornRebornList_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstColorThornRebornList_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THORN_T2540645209_H
#ifndef SKELETONANIMATOR_T1073737811_H
#define SKELETONANIMATOR_T1073737811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator
struct  SkeletonAnimator_t1073737811  : public SkeletonRenderer_t2098681813
{
public:
	// Spine.Unity.SkeletonAnimator/MecanimTranslator Spine.Unity.SkeletonAnimator::translator
	MecanimTranslator_t2363469064 * ___translator_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateLocal
	UpdateBonesDelegate_t735903178 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateWorld
	UpdateBonesDelegate_t735903178 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateComplete
	UpdateBonesDelegate_t735903178 * ____UpdateComplete_34;

public:
	inline static int32_t get_offset_of_translator_31() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ___translator_31)); }
	inline MecanimTranslator_t2363469064 * get_translator_31() const { return ___translator_31; }
	inline MecanimTranslator_t2363469064 ** get_address_of_translator_31() { return &___translator_31; }
	inline void set_translator_31(MecanimTranslator_t2363469064 * value)
	{
		___translator_31 = value;
		Il2CppCodeGenWriteBarrier((&___translator_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATOR_T1073737811_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef SKELETONGRAPHIC_T1744877482_H
#define SKELETONGRAPHIC_T1744877482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonGraphic
struct  SkeletonGraphic_t1744877482  : public MaskableGraphic_t3839221559
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonGraphic::skeletonDataAsset
	SkeletonDataAsset_t3748144825 * ___skeletonDataAsset_28;
	// System.String Spine.Unity.SkeletonGraphic::initialSkinName
	String_t* ___initialSkinName_29;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipX
	bool ___initialFlipX_30;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipY
	bool ___initialFlipY_31;
	// System.String Spine.Unity.SkeletonGraphic::startingAnimation
	String_t* ___startingAnimation_32;
	// System.Boolean Spine.Unity.SkeletonGraphic::startingLoop
	bool ___startingLoop_33;
	// System.Single Spine.Unity.SkeletonGraphic::timeScale
	float ___timeScale_34;
	// System.Boolean Spine.Unity.SkeletonGraphic::freeze
	bool ___freeze_35;
	// System.Boolean Spine.Unity.SkeletonGraphic::unscaledTime
	bool ___unscaledTime_36;
	// UnityEngine.Texture Spine.Unity.SkeletonGraphic::overrideTexture
	Texture_t3661962703 * ___overrideTexture_37;
	// Spine.Skeleton Spine.Unity.SkeletonGraphic::skeleton
	Skeleton_t3686076450 * ___skeleton_38;
	// Spine.AnimationState Spine.Unity.SkeletonGraphic::state
	AnimationState_t3637309382 * ___state_39;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonGraphic::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_40;
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.SkeletonGraphic::meshBuffers
	DoubleBuffered_1_t2489386064 * ___meshBuffers_41;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonGraphic::currentInstructions
	SkeletonRendererInstruction_t651787775 * ___currentInstructions_42;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateLocal
	UpdateBonesDelegate_t735903178 * ___UpdateLocal_43;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateWorld
	UpdateBonesDelegate_t735903178 * ___UpdateWorld_44;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateComplete
	UpdateBonesDelegate_t735903178 * ___UpdateComplete_45;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonGraphic::OnPostProcessVertices
	MeshGeneratorDelegate_t1654156803 * ___OnPostProcessVertices_46;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_28() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___skeletonDataAsset_28)); }
	inline SkeletonDataAsset_t3748144825 * get_skeletonDataAsset_28() const { return ___skeletonDataAsset_28; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_skeletonDataAsset_28() { return &___skeletonDataAsset_28; }
	inline void set_skeletonDataAsset_28(SkeletonDataAsset_t3748144825 * value)
	{
		___skeletonDataAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_28), value);
	}

	inline static int32_t get_offset_of_initialSkinName_29() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___initialSkinName_29)); }
	inline String_t* get_initialSkinName_29() const { return ___initialSkinName_29; }
	inline String_t** get_address_of_initialSkinName_29() { return &___initialSkinName_29; }
	inline void set_initialSkinName_29(String_t* value)
	{
		___initialSkinName_29 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_29), value);
	}

	inline static int32_t get_offset_of_initialFlipX_30() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___initialFlipX_30)); }
	inline bool get_initialFlipX_30() const { return ___initialFlipX_30; }
	inline bool* get_address_of_initialFlipX_30() { return &___initialFlipX_30; }
	inline void set_initialFlipX_30(bool value)
	{
		___initialFlipX_30 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_31() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___initialFlipY_31)); }
	inline bool get_initialFlipY_31() const { return ___initialFlipY_31; }
	inline bool* get_address_of_initialFlipY_31() { return &___initialFlipY_31; }
	inline void set_initialFlipY_31(bool value)
	{
		___initialFlipY_31 = value;
	}

	inline static int32_t get_offset_of_startingAnimation_32() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___startingAnimation_32)); }
	inline String_t* get_startingAnimation_32() const { return ___startingAnimation_32; }
	inline String_t** get_address_of_startingAnimation_32() { return &___startingAnimation_32; }
	inline void set_startingAnimation_32(String_t* value)
	{
		___startingAnimation_32 = value;
		Il2CppCodeGenWriteBarrier((&___startingAnimation_32), value);
	}

	inline static int32_t get_offset_of_startingLoop_33() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___startingLoop_33)); }
	inline bool get_startingLoop_33() const { return ___startingLoop_33; }
	inline bool* get_address_of_startingLoop_33() { return &___startingLoop_33; }
	inline void set_startingLoop_33(bool value)
	{
		___startingLoop_33 = value;
	}

	inline static int32_t get_offset_of_timeScale_34() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___timeScale_34)); }
	inline float get_timeScale_34() const { return ___timeScale_34; }
	inline float* get_address_of_timeScale_34() { return &___timeScale_34; }
	inline void set_timeScale_34(float value)
	{
		___timeScale_34 = value;
	}

	inline static int32_t get_offset_of_freeze_35() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___freeze_35)); }
	inline bool get_freeze_35() const { return ___freeze_35; }
	inline bool* get_address_of_freeze_35() { return &___freeze_35; }
	inline void set_freeze_35(bool value)
	{
		___freeze_35 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_36() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___unscaledTime_36)); }
	inline bool get_unscaledTime_36() const { return ___unscaledTime_36; }
	inline bool* get_address_of_unscaledTime_36() { return &___unscaledTime_36; }
	inline void set_unscaledTime_36(bool value)
	{
		___unscaledTime_36 = value;
	}

	inline static int32_t get_offset_of_overrideTexture_37() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___overrideTexture_37)); }
	inline Texture_t3661962703 * get_overrideTexture_37() const { return ___overrideTexture_37; }
	inline Texture_t3661962703 ** get_address_of_overrideTexture_37() { return &___overrideTexture_37; }
	inline void set_overrideTexture_37(Texture_t3661962703 * value)
	{
		___overrideTexture_37 = value;
		Il2CppCodeGenWriteBarrier((&___overrideTexture_37), value);
	}

	inline static int32_t get_offset_of_skeleton_38() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___skeleton_38)); }
	inline Skeleton_t3686076450 * get_skeleton_38() const { return ___skeleton_38; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_38() { return &___skeleton_38; }
	inline void set_skeleton_38(Skeleton_t3686076450 * value)
	{
		___skeleton_38 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_38), value);
	}

	inline static int32_t get_offset_of_state_39() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___state_39)); }
	inline AnimationState_t3637309382 * get_state_39() const { return ___state_39; }
	inline AnimationState_t3637309382 ** get_address_of_state_39() { return &___state_39; }
	inline void set_state_39(AnimationState_t3637309382 * value)
	{
		___state_39 = value;
		Il2CppCodeGenWriteBarrier((&___state_39), value);
	}

	inline static int32_t get_offset_of_meshGenerator_40() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___meshGenerator_40)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_40() const { return ___meshGenerator_40; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_40() { return &___meshGenerator_40; }
	inline void set_meshGenerator_40(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_40 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_40), value);
	}

	inline static int32_t get_offset_of_meshBuffers_41() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___meshBuffers_41)); }
	inline DoubleBuffered_1_t2489386064 * get_meshBuffers_41() const { return ___meshBuffers_41; }
	inline DoubleBuffered_1_t2489386064 ** get_address_of_meshBuffers_41() { return &___meshBuffers_41; }
	inline void set_meshBuffers_41(DoubleBuffered_1_t2489386064 * value)
	{
		___meshBuffers_41 = value;
		Il2CppCodeGenWriteBarrier((&___meshBuffers_41), value);
	}

	inline static int32_t get_offset_of_currentInstructions_42() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___currentInstructions_42)); }
	inline SkeletonRendererInstruction_t651787775 * get_currentInstructions_42() const { return ___currentInstructions_42; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_currentInstructions_42() { return &___currentInstructions_42; }
	inline void set_currentInstructions_42(SkeletonRendererInstruction_t651787775 * value)
	{
		___currentInstructions_42 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_42), value);
	}

	inline static int32_t get_offset_of_UpdateLocal_43() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___UpdateLocal_43)); }
	inline UpdateBonesDelegate_t735903178 * get_UpdateLocal_43() const { return ___UpdateLocal_43; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of_UpdateLocal_43() { return &___UpdateLocal_43; }
	inline void set_UpdateLocal_43(UpdateBonesDelegate_t735903178 * value)
	{
		___UpdateLocal_43 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateLocal_43), value);
	}

	inline static int32_t get_offset_of_UpdateWorld_44() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___UpdateWorld_44)); }
	inline UpdateBonesDelegate_t735903178 * get_UpdateWorld_44() const { return ___UpdateWorld_44; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of_UpdateWorld_44() { return &___UpdateWorld_44; }
	inline void set_UpdateWorld_44(UpdateBonesDelegate_t735903178 * value)
	{
		___UpdateWorld_44 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateWorld_44), value);
	}

	inline static int32_t get_offset_of_UpdateComplete_45() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___UpdateComplete_45)); }
	inline UpdateBonesDelegate_t735903178 * get_UpdateComplete_45() const { return ___UpdateComplete_45; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of_UpdateComplete_45() { return &___UpdateComplete_45; }
	inline void set_UpdateComplete_45(UpdateBonesDelegate_t735903178 * value)
	{
		___UpdateComplete_45 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateComplete_45), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_46() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___OnPostProcessVertices_46)); }
	inline MeshGeneratorDelegate_t1654156803 * get_OnPostProcessVertices_46() const { return ___OnPostProcessVertices_46; }
	inline MeshGeneratorDelegate_t1654156803 ** get_address_of_OnPostProcessVertices_46() { return &___OnPostProcessVertices_46; }
	inline void set_OnPostProcessVertices_46(MeshGeneratorDelegate_t1654156803 * value)
	{
		___OnPostProcessVertices_46 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGRAPHIC_T1744877482_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (U3CStartU3Ec__Iterator0_t3329131839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[4] = 
{
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[8] = 
{
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U3CstartMixU3E__0_1(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_target_2(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_duration_3(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24this_4(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24current_5(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24disposing_6(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (BoneFollowerGraphic_t46362405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[11] = 
{
	BoneFollowerGraphic_t46362405::get_offset_of_skeletonGraphic_2(),
	BoneFollowerGraphic_t46362405::get_offset_of_initializeOnAwake_3(),
	BoneFollowerGraphic_t46362405::get_offset_of_boneName_4(),
	BoneFollowerGraphic_t46362405::get_offset_of_followBoneRotation_5(),
	BoneFollowerGraphic_t46362405::get_offset_of_followSkeletonFlip_6(),
	BoneFollowerGraphic_t46362405::get_offset_of_followLocalScale_7(),
	BoneFollowerGraphic_t46362405::get_offset_of_followZPosition_8(),
	BoneFollowerGraphic_t46362405::get_offset_of_bone_9(),
	BoneFollowerGraphic_t46362405::get_offset_of_skeletonTransform_10(),
	BoneFollowerGraphic_t46362405::get_offset_of_skeletonTransformIsParent_11(),
	BoneFollowerGraphic_t46362405::get_offset_of_valid_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (SkeletonGraphic_t1744877482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[19] = 
{
	SkeletonGraphic_t1744877482::get_offset_of_skeletonDataAsset_28(),
	SkeletonGraphic_t1744877482::get_offset_of_initialSkinName_29(),
	SkeletonGraphic_t1744877482::get_offset_of_initialFlipX_30(),
	SkeletonGraphic_t1744877482::get_offset_of_initialFlipY_31(),
	SkeletonGraphic_t1744877482::get_offset_of_startingAnimation_32(),
	SkeletonGraphic_t1744877482::get_offset_of_startingLoop_33(),
	SkeletonGraphic_t1744877482::get_offset_of_timeScale_34(),
	SkeletonGraphic_t1744877482::get_offset_of_freeze_35(),
	SkeletonGraphic_t1744877482::get_offset_of_unscaledTime_36(),
	SkeletonGraphic_t1744877482::get_offset_of_overrideTexture_37(),
	SkeletonGraphic_t1744877482::get_offset_of_skeleton_38(),
	SkeletonGraphic_t1744877482::get_offset_of_state_39(),
	SkeletonGraphic_t1744877482::get_offset_of_meshGenerator_40(),
	SkeletonGraphic_t1744877482::get_offset_of_meshBuffers_41(),
	SkeletonGraphic_t1744877482::get_offset_of_currentInstructions_42(),
	SkeletonGraphic_t1744877482::get_offset_of_UpdateLocal_43(),
	SkeletonGraphic_t1744877482::get_offset_of_UpdateWorld_44(),
	SkeletonGraphic_t1744877482::get_offset_of_UpdateComplete_45(),
	SkeletonGraphic_t1744877482::get_offset_of_OnPostProcessVertices_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (SkeletonPartsRenderer_t667127217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[5] = 
{
	SkeletonPartsRenderer_t667127217::get_offset_of_meshGenerator_2(),
	SkeletonPartsRenderer_t667127217::get_offset_of_meshRenderer_3(),
	SkeletonPartsRenderer_t667127217::get_offset_of_meshFilter_4(),
	SkeletonPartsRenderer_t667127217::get_offset_of_buffers_5(),
	SkeletonPartsRenderer_t667127217::get_offset_of_currentInstructions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (SkeletonRenderSeparator_t2026602841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[7] = 
{
	0,
	SkeletonRenderSeparator_t2026602841::get_offset_of_skeletonRenderer_3(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_mainMeshRenderer_4(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_copyPropertyBlock_5(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_copyMeshRendererFlags_6(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_partsRenderers_7(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_copiedBlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (SkeletonUtilityEyeConstraint_t2225743321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3706[7] = 
{
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_eyes_4(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_radius_5(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_target_6(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_targetPosition_7(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_speed_8(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_origins_9(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_centerPoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (SkeletonUtilityGroundConstraint_t1570100391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[12] = 
{
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_groundMask_4(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_use2D_5(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_useRadius_6(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_castRadius_7(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_castDistance_8(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_castOffset_9(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_groundOffset_10(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_adjustSpeed_11(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_rayOrigin_12(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_rayDir_13(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_hitY_14(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_lastHitY_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (SkeletonUtilityKinematicShadow_t296913764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3708[5] = 
{
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_detachedShadow_2(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_parent_3(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_hideShadow_4(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_shadowRoot_5(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_shadowTable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (TransformPair_t3795417756)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[2] = 
{
	TransformPair_t3795417756::get_offset_of_dest_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPair_t3795417756::get_offset_of_src_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (SlotBlendModes_t1277474191), -1, sizeof(SlotBlendModes_t1277474191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3710[5] = 
{
	SlotBlendModes_t1277474191_StaticFields::get_offset_of_materialTable_2(),
	SlotBlendModes_t1277474191::get_offset_of_multiplyMaterialSource_3(),
	SlotBlendModes_t1277474191::get_offset_of_screenMaterialSource_4(),
	SlotBlendModes_t1277474191::get_offset_of_texture_5(),
	SlotBlendModes_t1277474191::get_offset_of_U3CAppliedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (MaterialTexturePair_t1637207048)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3711[2] = 
{
	MaterialTexturePair_t1637207048::get_offset_of_texture2D_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTexturePair_t1637207048::get_offset_of_material_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (WaitForSpineAnimationComplete_t3713264939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3712[1] = 
{
	WaitForSpineAnimationComplete_t3713264939::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (WaitForSpineEvent_t3999047839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3713[5] = 
{
	WaitForSpineEvent_t3999047839::get_offset_of_m_TargetEvent_0(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_EventName_1(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_AnimationState_2(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_WasFired_3(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_unsubscribeAfterFiring_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (WaitForSpineTrackEntryEnd_t2617682820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[1] = 
{
	WaitForSpineTrackEntryEnd_t2617682820::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (SkeletonAnimation_t3693186521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[7] = 
{
	SkeletonAnimation_t3693186521::get_offset_of_state_31(),
	SkeletonAnimation_t3693186521::get_offset_of__UpdateLocal_32(),
	SkeletonAnimation_t3693186521::get_offset_of__UpdateWorld_33(),
	SkeletonAnimation_t3693186521::get_offset_of__UpdateComplete_34(),
	SkeletonAnimation_t3693186521::get_offset_of__animationName_35(),
	SkeletonAnimation_t3693186521::get_offset_of_loop_36(),
	SkeletonAnimation_t3693186521::get_offset_of_timeScale_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (SkeletonAnimator_t1073737811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[4] = 
{
	SkeletonAnimator_t1073737811::get_offset_of_translator_31(),
	SkeletonAnimator_t1073737811::get_offset_of__UpdateLocal_32(),
	SkeletonAnimator_t1073737811::get_offset_of__UpdateWorld_33(),
	SkeletonAnimator_t1073737811::get_offset_of__UpdateComplete_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (MecanimTranslator_t2363469064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[8] = 
{
	MecanimTranslator_t2363469064::get_offset_of_autoReset_0(),
	MecanimTranslator_t2363469064::get_offset_of_layerMixModes_1(),
	MecanimTranslator_t2363469064::get_offset_of_animationTable_2(),
	MecanimTranslator_t2363469064::get_offset_of_clipNameHashCodeTable_3(),
	MecanimTranslator_t2363469064::get_offset_of_previousAnimations_4(),
	MecanimTranslator_t2363469064::get_offset_of_clipInfoCache_5(),
	MecanimTranslator_t2363469064::get_offset_of_nextClipInfoCache_6(),
	MecanimTranslator_t2363469064::get_offset_of_animator_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (MixMode_t3058145032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3718[4] = 
{
	MixMode_t3058145032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (AnimationClipEqualityComparer_t3042476764), -1, sizeof(AnimationClipEqualityComparer_t3042476764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3719[1] = 
{
	AnimationClipEqualityComparer_t3042476764_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (IntEqualityComparer_t2049031273), -1, sizeof(IntEqualityComparer_t2049031273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3720[1] = 
{
	IntEqualityComparer_t2049031273_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (SkeletonExtensions_t1502749191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3721[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (SkeletonExtensions_t671502775), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (SkeletonRenderer_t2098681813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[29] = 
{
	SkeletonRenderer_t2098681813::get_offset_of_OnRebuild_2(),
	SkeletonRenderer_t2098681813::get_offset_of_OnPostProcessVertices_3(),
	SkeletonRenderer_t2098681813::get_offset_of_skeletonDataAsset_4(),
	SkeletonRenderer_t2098681813::get_offset_of_initialSkinName_5(),
	SkeletonRenderer_t2098681813::get_offset_of_initialFlipX_6(),
	SkeletonRenderer_t2098681813::get_offset_of_initialFlipY_7(),
	SkeletonRenderer_t2098681813::get_offset_of_separatorSlotNames_8(),
	SkeletonRenderer_t2098681813::get_offset_of_separatorSlots_9(),
	SkeletonRenderer_t2098681813::get_offset_of_zSpacing_10(),
	SkeletonRenderer_t2098681813::get_offset_of_useClipping_11(),
	SkeletonRenderer_t2098681813::get_offset_of_immutableTriangles_12(),
	SkeletonRenderer_t2098681813::get_offset_of_pmaVertexColors_13(),
	SkeletonRenderer_t2098681813::get_offset_of_clearStateOnDisable_14(),
	SkeletonRenderer_t2098681813::get_offset_of_tintBlack_15(),
	SkeletonRenderer_t2098681813::get_offset_of_singleSubmesh_16(),
	SkeletonRenderer_t2098681813::get_offset_of_addNormals_17(),
	SkeletonRenderer_t2098681813::get_offset_of_calculateTangents_18(),
	SkeletonRenderer_t2098681813::get_offset_of_logErrors_19(),
	SkeletonRenderer_t2098681813::get_offset_of_disableRenderingOnOverride_20(),
	SkeletonRenderer_t2098681813::get_offset_of_generateMeshOverride_21(),
	SkeletonRenderer_t2098681813::get_offset_of_customMaterialOverride_22(),
	SkeletonRenderer_t2098681813::get_offset_of_customSlotMaterials_23(),
	SkeletonRenderer_t2098681813::get_offset_of_meshRenderer_24(),
	SkeletonRenderer_t2098681813::get_offset_of_meshFilter_25(),
	SkeletonRenderer_t2098681813::get_offset_of_valid_26(),
	SkeletonRenderer_t2098681813::get_offset_of_skeleton_27(),
	SkeletonRenderer_t2098681813::get_offset_of_currentInstructions_28(),
	SkeletonRenderer_t2098681813::get_offset_of_meshGenerator_29(),
	SkeletonRenderer_t2098681813::get_offset_of_rendererBuffers_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (SkeletonRendererDelegate_t3507789975), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (InstructionDelegate_t2225421195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (SkeletonUtility_t2980767925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[9] = 
{
	SkeletonUtility_t2980767925::get_offset_of_OnReset_2(),
	SkeletonUtility_t2980767925::get_offset_of_boneRoot_3(),
	SkeletonUtility_t2980767925::get_offset_of_skeletonRenderer_4(),
	SkeletonUtility_t2980767925::get_offset_of_skeletonAnimation_5(),
	SkeletonUtility_t2980767925::get_offset_of_utilityBones_6(),
	SkeletonUtility_t2980767925::get_offset_of_utilityConstraints_7(),
	SkeletonUtility_t2980767925::get_offset_of_hasTransformBones_8(),
	SkeletonUtility_t2980767925::get_offset_of_hasUtilityConstraints_9(),
	SkeletonUtility_t2980767925::get_offset_of_needToReprocessBones_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (SkeletonUtilityDelegate_t487527757), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (SkeletonUtilityBone_t2225520134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[15] = 
{
	SkeletonUtilityBone_t2225520134::get_offset_of_boneName_2(),
	SkeletonUtilityBone_t2225520134::get_offset_of_parentReference_3(),
	SkeletonUtilityBone_t2225520134::get_offset_of_mode_4(),
	SkeletonUtilityBone_t2225520134::get_offset_of_position_5(),
	SkeletonUtilityBone_t2225520134::get_offset_of_rotation_6(),
	SkeletonUtilityBone_t2225520134::get_offset_of_scale_7(),
	SkeletonUtilityBone_t2225520134::get_offset_of_zPosition_8(),
	SkeletonUtilityBone_t2225520134::get_offset_of_overrideAlpha_9(),
	SkeletonUtilityBone_t2225520134::get_offset_of_skeletonUtility_10(),
	SkeletonUtilityBone_t2225520134::get_offset_of_bone_11(),
	SkeletonUtilityBone_t2225520134::get_offset_of_transformLerpComplete_12(),
	SkeletonUtilityBone_t2225520134::get_offset_of_valid_13(),
	SkeletonUtilityBone_t2225520134::get_offset_of_cachedTransform_14(),
	SkeletonUtilityBone_t2225520134::get_offset_of_skeletonTransform_15(),
	SkeletonUtilityBone_t2225520134::get_offset_of_incompatibleTransformMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (Mode_t3700196520)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3729[3] = 
{
	Mode_t3700196520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (SkeletonUtilityConstraint_t1014382506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[2] = 
{
	SkeletonUtilityConstraint_t1014382506::get_offset_of_utilBone_2(),
	SkeletonUtilityConstraint_t1014382506::get_offset_of_skeletonUtility_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (SpineAttributeBase_t340149836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3731[4] = 
{
	SpineAttributeBase_t340149836::get_offset_of_dataField_0(),
	SpineAttributeBase_t340149836::get_offset_of_startsWith_1(),
	SpineAttributeBase_t340149836::get_offset_of_includeNone_2(),
	SpineAttributeBase_t340149836::get_offset_of_fallbackToTextField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (SpineSlot_t2906857509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[1] = 
{
	SpineSlot_t2906857509::get_offset_of_containsBoundingBoxes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (SpineEvent_t2537401913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (SpineIkConstraint_t1587650654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (SpinePathConstraint_t3765886426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (SpineTransformConstraint_t1324873370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (SpineSkin_t4048709426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (SpineAnimation_t42895223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (SpineAttachment_t743638603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[5] = 
{
	SpineAttachment_t743638603::get_offset_of_returnAttachmentPath_4(),
	SpineAttachment_t743638603::get_offset_of_currentSkinOnly_5(),
	SpineAttachment_t743638603::get_offset_of_placeholdersOnly_6(),
	SpineAttachment_t743638603::get_offset_of_skinField_7(),
	SpineAttachment_t743638603::get_offset_of_slotField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (Hierarchy_t2161623951)+ sizeof (RuntimeObject), sizeof(Hierarchy_t2161623951_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3740[3] = 
{
	Hierarchy_t2161623951::get_offset_of_skin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t2161623951::get_offset_of_slot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t2161623951::get_offset_of_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (SpineBone_t3329529041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (SpineAtlasRegion_t3654419954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3742[1] = 
{
	SpineAtlasRegion_t3654419954::get_offset_of_atlasAssetField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (SpitBallTarget_t3997305173), -1, sizeof(SpitBallTarget_t3997305173_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3743[16] = 
{
	SpitBallTarget_t3997305173::get_offset_of_m_ball_2(),
	SpitBallTarget_t3997305173::get_offset_of_m_sr_3(),
	SpitBallTarget_t3997305173::get_offset_of_m_fTotalTime_4(),
	SpitBallTarget_t3997305173::get_offset_of_m_bCouting_5(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_vecTempPos_6(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_vecTempScale_7(),
	SpitBallTarget_t3997305173::get_offset_of_m_vecPos_8(),
	SpitBallTarget_t3997305173::get_offset_of_m_fSize_9(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob8_10(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob16_11(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob24_12(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob32_13(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob64_14(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob128_15(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_Blob256_16(),
	SpitBallTarget_t3997305173_StaticFields::get_offset_of_m_dicBlobs_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (Spray_t722157414), -1, sizeof(Spray_t722157414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3744[40] = 
{
	Spray_t722157414::get_offset_of__txtSpraySize_17(),
	Spray_t722157414::get_offset_of__txtCurMeatBeanLeftTime_18(),
	Spray_t722157414::get_offset_of_m_fRotationZ_19(),
	Spray_t722157414::get_offset_of_m_fSprayInterval_20(),
	Spray_t722157414::get_offset_of_m_fPreSprayTime_21(),
	Spray_t722157414::get_offset_of_m_fDensity_22(),
	Spray_t722157414::get_offset_of_m_fMeatDensity_23(),
	Spray_t722157414::get_offset_of_m_fMaxDis_24(),
	Spray_t722157414::get_offset_of_m_fMinDis_25(),
	Spray_t722157414::get_offset_of_m_fBeanLifeTime_26(),
	Spray_t722157414::get_offset_of_m_fMeat2BeanSprayTime_27(),
	Spray_t722157414::get_offset_of_m_fSpraySize_28(),
	Spray_t722157414::get_offset_of_m_fDrawSpeed_29(),
	Spray_t722157414::get_offset_of_m_fDrawArea_30(),
	Spray_t722157414::get_offset_of_m_fPreDrawTime_31(),
	Spray_t722157414::get_offset_of_m_fDrawTime_32(),
	Spray_t722157414::get_offset_of_m_fDrawInterval_33(),
	Spray_t722157414::get_offset_of__goMain_34(),
	Spray_t722157414::get_offset_of__goSprayedBeansContainer_35(),
	Spray_t722157414_StaticFields::get_offset_of_vecTempScale_36(),
	Spray_t722157414_StaticFields::get_offset_of_vecTempRotation_37(),
	Spray_t722157414_StaticFields::get_offset_of_vecTempPos_38(),
	Spray_t722157414::get_offset_of_m_nBeanCountForthornChance_39(),
	Spray_t722157414::get_offset_of_m_nThornChance_40(),
	Spray_t722157414::get_offset_of__CollierMain_41(),
	Spray_t722157414::get_offset_of__CollierObsCircle_42(),
	Spray_t722157414::get_offset_of_m_nGUID_43(),
	Spray_t722157414::get_offset_of_m_fCurMeatBeanLeftTime_44(),
	Spray_t722157414::get_offset_of_m_fSprayTimeCount_45(),
	Spray_t722157414_StaticFields::get_offset_of_s_Shit_46(),
	Spray_t722157414::get_offset_of_m_lstThorns_47(),
	Spray_t722157414::get_offset_of_m_fMeatBeanSprayCount_48(),
	Spray_t722157414::get_offset_of_m_bPreSpraying_49(),
	Spray_t722157414::get_offset_of_m_fMeatBeanSprayInterval_50(),
	Spray_t722157414::get_offset_of_m_fRadius_51(),
	Spray_t722157414::get_offset_of_m_fBeginPreDrawTime_52(),
	Spray_t722157414::get_offset_of_m_nDrawStatus_53(),
	Spray_t722157414::get_offset_of_m_fBeginDrawTime_54(),
	Spray_t722157414::get_offset_of_m_lstCapturedBalls_55(),
	Spray_t722157414::get_offset_of_m_fMeatBeanLeftTimeShowInterval_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (StringManager_t2047563430), -1, sizeof(StringManager_t2047563430_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3745[3] = 
{
	StringManager_t2047563430_StaticFields::get_offset_of__bytes_2(),
	StringManager_t2047563430_StaticFields::get_offset_of__pointer_3(),
	StringManager_t2047563430_StaticFields::get_offset_of_m_lstBlobs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (Thorn_t2540645209), -1, sizeof(Thorn_t2540645209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3746[14] = 
{
	Thorn_t2540645209::get_offset_of_m_eThornType_17(),
	Thorn_t2540645209_StaticFields::get_offset_of_m_lstNewThornApplication_18(),
	Thorn_t2540645209::get_offset_of__main_19(),
	Thorn_t2540645209::get_offset_of__Trigger_20(),
	Thorn_t2540645209::get_offset_of__srMain_21(),
	Thorn_t2540645209::get_offset_of_m_nIndex_22(),
	Thorn_t2540645209::get_offset_of_m_nOwnerId_23(),
	Thorn_t2540645209_StaticFields::get_offset_of_c_aryThornColor_24(),
	Thorn_t2540645209::get_offset_of_m_nColorIdx_25(),
	Thorn_t2540645209_StaticFields::get_offset_of_m_nSceneThornRebornIndex_26(),
	Thorn_t2540645209::get_offset_of_m_bIsColorThorn_27(),
	Thorn_t2540645209_StaticFields::get_offset_of_m_lstColorThornRebornList_28(),
	Thorn_t2540645209::get_offset_of_m_fRebornTime_29(),
	Thorn_t2540645209::get_offset_of_m_bDead_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (eThornType_t2871459091)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3747[3] = 
{
	eThornType_t2871459091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (ColorThornParam_t1112536431)+ sizeof (RuntimeObject), sizeof(ColorThornParam_t1112536431 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3748[3] = 
{
	ColorThornParam_t1112536431::get_offset_of_thorn_size_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorThornParam_t1112536431::get_offset_of_ball_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorThornParam_t1112536431::get_offset_of_reborn_time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (UI_MainSkill_Select_t3468825974), -1, sizeof(UI_MainSkill_Select_t3468825974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3749[5] = 
{
	UI_MainSkill_Select_t3468825974::get_offset_of__ShuangYaoGan_2(),
	UI_MainSkill_Select_t3468825974::get_offset_of_m_Main_3(),
	UI_MainSkill_Select_t3468825974_StaticFields::get_offset_of_s_bUsingUi_4(),
	UI_MainSkill_Select_t3468825974::get_offset_of_m_bSpitting_5(),
	UI_MainSkill_Select_t3468825974::get_offset_of__touch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (UI_MainSkill_Unfold_t57022693), -1, sizeof(UI_MainSkill_Unfold_t57022693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3750[5] = 
{
	UI_MainSkill_Unfold_t57022693::get_offset_of__ShuangYaoGan_2(),
	UI_MainSkill_Unfold_t57022693::get_offset_of_m_Main_3(),
	UI_MainSkill_Unfold_t57022693_StaticFields::get_offset_of_s_bUsingUi_4(),
	UI_MainSkill_Unfold_t57022693::get_offset_of_m_bSpitting_5(),
	UI_MainSkill_Unfold_t57022693::get_offset_of__touch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (UI_Spit_t1775598180), -1, sizeof(UI_Spit_t1775598180_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3751[5] = 
{
	UI_Spit_t1775598180::get_offset_of__ShuangYaoGan_2(),
	UI_Spit_t1775598180::get_offset_of_m_Main_3(),
	UI_Spit_t1775598180_StaticFields::get_offset_of_s_bUsingUi_4(),
	UI_Spit_t1775598180::get_offset_of_m_bSpitting_5(),
	UI_Spit_t1775598180::get_offset_of__touch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (UI_SpitBean_t2554404907), -1, sizeof(UI_SpitBean_t2554404907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3752[2] = 
{
	UI_SpitBean_t2554404907_StaticFields::get_offset_of_s_bUsingUi_2(),
	UI_SpitBean_t2554404907::get_offset_of_m_bSplitting_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (UI_Split_t2785288677), -1, sizeof(UI_Split_t2785288677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3753[2] = 
{
	UI_Split_t2785288677_StaticFields::get_offset_of_s_bUsingUi_2(),
	UI_Split_t2785288677::get_offset_of_m_bSplitting_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (UniWebViewAndroidStaticListener_t2820583295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (UniWebViewInterface_t2356025224), -1, sizeof(UniWebViewInterface_t2356025224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3755[3] = 
{
	0,
	UniWebViewInterface_t2356025224_StaticFields::get_offset_of_correctPlatform_1(),
	UniWebViewInterface_t2356025224_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (UnitySendMessageDelegate_t3447265919), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (UniWebView_t941983939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[21] = 
{
	UniWebView_t941983939::get_offset_of_OnPageStarted_2(),
	UniWebView_t941983939::get_offset_of_OnPageFinished_3(),
	UniWebView_t941983939::get_offset_of_OnPageErrorReceived_4(),
	UniWebView_t941983939::get_offset_of_OnMessageReceived_5(),
	UniWebView_t941983939::get_offset_of_OnShouldClose_6(),
	UniWebView_t941983939::get_offset_of_OnKeyCodeReceived_7(),
	UniWebView_t941983939::get_offset_of_OnOreintationChanged_8(),
	UniWebView_t941983939::get_offset_of_id_9(),
	UniWebView_t941983939::get_offset_of_listener_10(),
	UniWebView_t941983939::get_offset_of_isPortrait_11(),
	UniWebView_t941983939::get_offset_of_urlOnStart_12(),
	UniWebView_t941983939::get_offset_of_showOnStart_13(),
	UniWebView_t941983939::get_offset_of_actions_14(),
	UniWebView_t941983939::get_offset_of_payloadActions_15(),
	UniWebView_t941983939::get_offset_of_fullScreen_16(),
	UniWebView_t941983939::get_offset_of_frame_17(),
	UniWebView_t941983939::get_offset_of_referenceRectTransform_18(),
	UniWebView_t941983939::get_offset_of_useToolbar_19(),
	UniWebView_t941983939::get_offset_of_toolbarPosition_20(),
	UniWebView_t941983939::get_offset_of_started_21(),
	UniWebView_t941983939::get_offset_of_backgroundColor_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (PageStartedDelegate_t2830724344), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (PageFinishedDelegate_t2717015276), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (PageErrorReceivedDelegate_t1724664023), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (MessageReceivedDelegate_t2288957136), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (ShouldCloseDelegate_t766319959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (KeyCodeReceivedDelegate_t3839084677), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (OreintationChangedDelegate_t1877368362), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (U3CGetHTMLContentU3Ec__AnonStorey0_t803715261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[1] = 
{
	U3CGetHTMLContentU3Ec__AnonStorey0_t803715261::get_offset_of_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (UniWebViewHelper_t1459904593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (UniWebViewLogger_t952558916), -1, sizeof(UniWebViewLogger_t952558916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3767[2] = 
{
	UniWebViewLogger_t952558916_StaticFields::get_offset_of_instance_0(),
	UniWebViewLogger_t952558916::get_offset_of_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (Level_t4211844589)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3768[6] = 
{
	Level_t4211844589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (UniWebViewMessage_t2441068380)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[4] = 
{
	UniWebViewMessage_t2441068380::get_offset_of_U3CRawMessageU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CSchemeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CPathU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CArgsU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (UniWebViewNativeListener_t1145263134), -1, sizeof(UniWebViewNativeListener_t1145263134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3770[2] = 
{
	UniWebViewNativeListener_t1145263134_StaticFields::get_offset_of_listeners_2(),
	UniWebViewNativeListener_t1145263134::get_offset_of_webView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (UniWebViewNativeResultPayload_t4072739617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3771[3] = 
{
	UniWebViewNativeResultPayload_t4072739617::get_offset_of_identifier_0(),
	UniWebViewNativeResultPayload_t4072739617::get_offset_of_resultCode_1(),
	UniWebViewNativeResultPayload_t4072739617::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (UniWebViewToolbarPosition_t2230581447)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3772[3] = 
{
	UniWebViewToolbarPosition_t2230581447::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (UniWebViewTransitionEdge_t2354894166)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3773[6] = 
{
	UniWebViewTransitionEdge_t2354894166::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (WorldBorder_t1275663748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3774[5] = 
{
	WorldBorder_t1275663748::get_offset_of__colliderEdge_2(),
	WorldBorder_t1275663748::get_offset_of__lr_3(),
	WorldBorder_t1275663748::get_offset_of_vecTempPos_4(),
	WorldBorder_t1275663748::get_offset_of_vecTemp2_5(),
	WorldBorder_t1275663748::get_offset_of_m_aryColliderPoints_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3775[8] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D39343524298E63D94C076EE07AB61EFA7807E0B0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DC660A6C2191F7639613A25F2A723ED819CE3C613_4(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D82DD799C96AA69293BA404D79A459D6F9750AE19_5(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_6(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (U24ArrayTypeU3D36_t120960362)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D36_t120960362 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (U24ArrayTypeU3D48_t1336283963)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D48_t1336283963 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (U24ArrayTypeU3D1024_t3853988145)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D1024_t3853988145 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (U24ArrayTypeU3D76_t2446559190)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D76_t2446559190 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (U24ArrayTypeU3D20_t1702832645)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t1702832645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
