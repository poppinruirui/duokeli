﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Mesh[]
struct MeshU5BU5D_t3972987605;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,CItemSystem/sValueByLevel>
struct Dictionary_2_t3994391469;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;
// CBiShuaTuoWei[]
struct CBiShuaTuoWeiU5BU5D_t1377408237;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UIItem[]
struct UIItemU5BU5D_t2543761648;
// System.Collections.Generic.Dictionary`2<System.Int32,CItemSystem/sItemConfig>
struct Dictionary_2_t1859915667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.List`1<CItemBuyEffect>
struct List_1_t2305279210;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// CFrameAnimationEffect
struct CFrameAnimationEffect_t443605508;
// CJieSuanCounter[]
struct CJieSuanCounterU5BU5D_t794168762;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.List`1<CCosmosGunSight>
struct List_1_t807120747;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Collections.Generic.List`1<CGesture>
struct List_1_t3653243616;
// System.Collections.Generic.List`1<CGesturePopo>
struct List_1_t3832709940;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// CGesturePopo
struct CGesturePopo_t2360635198;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// Ball
struct Ball_t2206666566;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// CWifi
struct CWifi_t3890179523;
// System.Collections.Generic.Dictionary`2<System.Int32,CGrowSystem/sLevelConfig>
struct Dictionary_2_t3180303622;
// Player
struct Player_t3266647312;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1812449865;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Collections.Generic.Dictionary`2<CSkillSystem/eSkillId,System.Collections.Generic.List`1<CCosmosEffect>>
struct Dictionary_2_t2961645935;
// CCosmosEffect[]
struct CCosmosEffectU5BU5D_t3177071136;
// System.Collections.Generic.Dictionary`2<CEffectManager/eMainFightEffectType,System.Collections.Generic.List`1<CCosmosEffect>>
struct Dictionary_2_t3341708860;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Texture3D
struct Texture3D_t1884131049;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t2302988098;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef IMAGEEFFECTS_T1214077586_H
#define IMAGEEFFECTS_T1214077586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffects
struct  ImageEffects_t1214077586  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTS_T1214077586_H
#ifndef QUADS_T1152577304_H
#define QUADS_T1152577304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Quads
struct  Quads_t1152577304  : public RuntimeObject
{
public:

public:
};

struct Quads_t1152577304_StaticFields
{
public:
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::meshes
	MeshU5BU5D_t3972987605* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Quads::currentQuads
	int32_t ___currentQuads_1;

public:
	inline static int32_t get_offset_of_meshes_0() { return static_cast<int32_t>(offsetof(Quads_t1152577304_StaticFields, ___meshes_0)); }
	inline MeshU5BU5D_t3972987605* get_meshes_0() const { return ___meshes_0; }
	inline MeshU5BU5D_t3972987605** get_address_of_meshes_0() { return &___meshes_0; }
	inline void set_meshes_0(MeshU5BU5D_t3972987605* value)
	{
		___meshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_0), value);
	}

	inline static int32_t get_offset_of_currentQuads_1() { return static_cast<int32_t>(offsetof(Quads_t1152577304_StaticFields, ___currentQuads_1)); }
	inline int32_t get_currentQuads_1() const { return ___currentQuads_1; }
	inline int32_t* get_address_of_currentQuads_1() { return &___currentQuads_1; }
	inline void set_currentQuads_1(int32_t value)
	{
		___currentQuads_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADS_T1152577304_H
#ifndef U3CLOADSCENEU3EC__ITERATOR0_T2118664053_H
#define U3CLOADSCENEU3EC__ITERATOR0_T2118664053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGameOverManager/<LoadScene>c__Iterator0
struct  U3CLoadSceneU3Ec__Iterator0_t2118664053  : public RuntimeObject
{
public:
	// System.String CGameOverManager/<LoadScene>c__Iterator0::scene_name
	String_t* ___scene_name_0;
	// System.Object CGameOverManager/<LoadScene>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CGameOverManager/<LoadScene>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CGameOverManager/<LoadScene>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_scene_name_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2118664053, ___scene_name_0)); }
	inline String_t* get_scene_name_0() const { return ___scene_name_0; }
	inline String_t** get_address_of_scene_name_0() { return &___scene_name_0; }
	inline void set_scene_name_0(String_t* value)
	{
		___scene_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_name_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2118664053, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2118664053, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t2118664053, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3EC__ITERATOR0_T2118664053_H
#ifndef TRIANGLES_T2090031681_H
#define TRIANGLES_T2090031681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Triangles
struct  Triangles_t2090031681  : public RuntimeObject
{
public:

public:
};

struct Triangles_t2090031681_StaticFields
{
public:
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::meshes
	MeshU5BU5D_t3972987605* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Triangles::currentTris
	int32_t ___currentTris_1;

public:
	inline static int32_t get_offset_of_meshes_0() { return static_cast<int32_t>(offsetof(Triangles_t2090031681_StaticFields, ___meshes_0)); }
	inline MeshU5BU5D_t3972987605* get_meshes_0() const { return ___meshes_0; }
	inline MeshU5BU5D_t3972987605** get_address_of_meshes_0() { return &___meshes_0; }
	inline void set_meshes_0(MeshU5BU5D_t3972987605* value)
	{
		___meshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_0), value);
	}

	inline static int32_t get_offset_of_currentTris_1() { return static_cast<int32_t>(offsetof(Triangles_t2090031681_StaticFields, ___currentTris_1)); }
	inline int32_t get_currentTris_1() const { return ___currentTris_1; }
	inline int32_t* get_address_of_currentTris_1() { return &___currentTris_1; }
	inline void set_currentTris_1(int32_t value)
	{
		___currentTris_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLES_T2090031681_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SVALUEBYLEVEL_T810710842_H
#define SVALUEBYLEVEL_T810710842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CItemSystem/sValueByLevel
struct  sValueByLevel_t810710842 
{
public:
	// System.Int32 CItemSystem/sValueByLevel::nPrice
	int32_t ___nPrice_0;
	// System.Single CItemSystem/sValueByLevel::fValue
	float ___fValue_1;

public:
	inline static int32_t get_offset_of_nPrice_0() { return static_cast<int32_t>(offsetof(sValueByLevel_t810710842, ___nPrice_0)); }
	inline int32_t get_nPrice_0() const { return ___nPrice_0; }
	inline int32_t* get_address_of_nPrice_0() { return &___nPrice_0; }
	inline void set_nPrice_0(int32_t value)
	{
		___nPrice_0 = value;
	}

	inline static int32_t get_offset_of_fValue_1() { return static_cast<int32_t>(offsetof(sValueByLevel_t810710842, ___fValue_1)); }
	inline float get_fValue_1() const { return ___fValue_1; }
	inline float* get_address_of_fValue_1() { return &___fValue_1; }
	inline void set_fValue_1(float value)
	{
		___fValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVALUEBYLEVEL_T810710842_H
#ifndef SITEMCONFIG_T2971202336_H
#define SITEMCONFIG_T2971202336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CItemSystem/sItemConfig
struct  sItemConfig_t2971202336 
{
public:
	// System.Int32 CItemSystem/sItemConfig::nConfigId
	int32_t ___nConfigId_0;
	// System.Int32 CItemSystem/sItemConfig::nShortcutKey
	int32_t ___nShortcutKey_1;
	// System.String CItemSystem/sItemConfig::szName
	String_t* ___szName_2;
	// System.String CItemSystem/sItemConfig::szDesc
	String_t* ___szDesc_3;
	// System.Int32 CItemSystem/sItemConfig::nFunc
	int32_t ___nFunc_4;
	// System.Int32 CItemSystem/sItemConfig::nSpriteId
	int32_t ___nSpriteId_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,CItemSystem/sValueByLevel> CItemSystem/sItemConfig::dicValueByLevel
	Dictionary_2_t3994391469 * ___dicValueByLevel_6;

public:
	inline static int32_t get_offset_of_nConfigId_0() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___nConfigId_0)); }
	inline int32_t get_nConfigId_0() const { return ___nConfigId_0; }
	inline int32_t* get_address_of_nConfigId_0() { return &___nConfigId_0; }
	inline void set_nConfigId_0(int32_t value)
	{
		___nConfigId_0 = value;
	}

	inline static int32_t get_offset_of_nShortcutKey_1() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___nShortcutKey_1)); }
	inline int32_t get_nShortcutKey_1() const { return ___nShortcutKey_1; }
	inline int32_t* get_address_of_nShortcutKey_1() { return &___nShortcutKey_1; }
	inline void set_nShortcutKey_1(int32_t value)
	{
		___nShortcutKey_1 = value;
	}

	inline static int32_t get_offset_of_szName_2() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___szName_2)); }
	inline String_t* get_szName_2() const { return ___szName_2; }
	inline String_t** get_address_of_szName_2() { return &___szName_2; }
	inline void set_szName_2(String_t* value)
	{
		___szName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szName_2), value);
	}

	inline static int32_t get_offset_of_szDesc_3() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___szDesc_3)); }
	inline String_t* get_szDesc_3() const { return ___szDesc_3; }
	inline String_t** get_address_of_szDesc_3() { return &___szDesc_3; }
	inline void set_szDesc_3(String_t* value)
	{
		___szDesc_3 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_3), value);
	}

	inline static int32_t get_offset_of_nFunc_4() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___nFunc_4)); }
	inline int32_t get_nFunc_4() const { return ___nFunc_4; }
	inline int32_t* get_address_of_nFunc_4() { return &___nFunc_4; }
	inline void set_nFunc_4(int32_t value)
	{
		___nFunc_4 = value;
	}

	inline static int32_t get_offset_of_nSpriteId_5() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___nSpriteId_5)); }
	inline int32_t get_nSpriteId_5() const { return ___nSpriteId_5; }
	inline int32_t* get_address_of_nSpriteId_5() { return &___nSpriteId_5; }
	inline void set_nSpriteId_5(int32_t value)
	{
		___nSpriteId_5 = value;
	}

	inline static int32_t get_offset_of_dicValueByLevel_6() { return static_cast<int32_t>(offsetof(sItemConfig_t2971202336, ___dicValueByLevel_6)); }
	inline Dictionary_2_t3994391469 * get_dicValueByLevel_6() const { return ___dicValueByLevel_6; }
	inline Dictionary_2_t3994391469 ** get_address_of_dicValueByLevel_6() { return &___dicValueByLevel_6; }
	inline void set_dicValueByLevel_6(Dictionary_2_t3994391469 * value)
	{
		___dicValueByLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&___dicValueByLevel_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CItemSystem/sItemConfig
struct sItemConfig_t2971202336_marshaled_pinvoke
{
	int32_t ___nConfigId_0;
	int32_t ___nShortcutKey_1;
	char* ___szName_2;
	char* ___szDesc_3;
	int32_t ___nFunc_4;
	int32_t ___nSpriteId_5;
	Dictionary_2_t3994391469 * ___dicValueByLevel_6;
};
// Native definition for COM marshalling of CItemSystem/sItemConfig
struct sItemConfig_t2971202336_marshaled_com
{
	int32_t ___nConfigId_0;
	int32_t ___nShortcutKey_1;
	Il2CppChar* ___szName_2;
	Il2CppChar* ___szDesc_3;
	int32_t ___nFunc_4;
	int32_t ___nSpriteId_5;
	Dictionary_2_t3994391469 * ___dicValueByLevel_6;
};
#endif // SITEMCONFIG_T2971202336_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef SLEVELCONFIG_T4291590291_H
#define SLEVELCONFIG_T4291590291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrowSystem/sLevelConfig
struct  sLevelConfig_t4291590291 
{
public:
	// System.Int32 CGrowSystem/sLevelConfig::nId
	int32_t ___nId_0;
	// System.Int32 CGrowSystem/sLevelConfig::fNeedExp
	int32_t ___fNeedExp_1;
	// System.Int32 CGrowSystem/sLevelConfig::fMP
	int32_t ___fMP_2;
	// System.Single CGrowSystem/sLevelConfig::fMPRecoverSpeed
	float ___fMPRecoverSpeed_3;
	// System.Int32 CGrowSystem/sLevelConfig::fKillGainExp
	int32_t ___fKillGainExp_4;
	// System.Int32 CGrowSystem/sLevelConfig::fKillGainMoney
	int32_t ___fKillGainMoney_5;
	// System.Single CGrowSystem/sLevelConfig::fBaseVolume
	float ___fBaseVolume_6;
	// System.Int32 CGrowSystem/sLevelConfig::fLevelMoney
	int32_t ___fLevelMoney_7;
	// System.Int32 CGrowSystem/sLevelConfig::nDeadWaitingTime
	int32_t ___nDeadWaitingTime_8;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_fNeedExp_1() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fNeedExp_1)); }
	inline int32_t get_fNeedExp_1() const { return ___fNeedExp_1; }
	inline int32_t* get_address_of_fNeedExp_1() { return &___fNeedExp_1; }
	inline void set_fNeedExp_1(int32_t value)
	{
		___fNeedExp_1 = value;
	}

	inline static int32_t get_offset_of_fMP_2() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fMP_2)); }
	inline int32_t get_fMP_2() const { return ___fMP_2; }
	inline int32_t* get_address_of_fMP_2() { return &___fMP_2; }
	inline void set_fMP_2(int32_t value)
	{
		___fMP_2 = value;
	}

	inline static int32_t get_offset_of_fMPRecoverSpeed_3() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fMPRecoverSpeed_3)); }
	inline float get_fMPRecoverSpeed_3() const { return ___fMPRecoverSpeed_3; }
	inline float* get_address_of_fMPRecoverSpeed_3() { return &___fMPRecoverSpeed_3; }
	inline void set_fMPRecoverSpeed_3(float value)
	{
		___fMPRecoverSpeed_3 = value;
	}

	inline static int32_t get_offset_of_fKillGainExp_4() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fKillGainExp_4)); }
	inline int32_t get_fKillGainExp_4() const { return ___fKillGainExp_4; }
	inline int32_t* get_address_of_fKillGainExp_4() { return &___fKillGainExp_4; }
	inline void set_fKillGainExp_4(int32_t value)
	{
		___fKillGainExp_4 = value;
	}

	inline static int32_t get_offset_of_fKillGainMoney_5() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fKillGainMoney_5)); }
	inline int32_t get_fKillGainMoney_5() const { return ___fKillGainMoney_5; }
	inline int32_t* get_address_of_fKillGainMoney_5() { return &___fKillGainMoney_5; }
	inline void set_fKillGainMoney_5(int32_t value)
	{
		___fKillGainMoney_5 = value;
	}

	inline static int32_t get_offset_of_fBaseVolume_6() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fBaseVolume_6)); }
	inline float get_fBaseVolume_6() const { return ___fBaseVolume_6; }
	inline float* get_address_of_fBaseVolume_6() { return &___fBaseVolume_6; }
	inline void set_fBaseVolume_6(float value)
	{
		___fBaseVolume_6 = value;
	}

	inline static int32_t get_offset_of_fLevelMoney_7() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___fLevelMoney_7)); }
	inline int32_t get_fLevelMoney_7() const { return ___fLevelMoney_7; }
	inline int32_t* get_address_of_fLevelMoney_7() { return &___fLevelMoney_7; }
	inline void set_fLevelMoney_7(int32_t value)
	{
		___fLevelMoney_7 = value;
	}

	inline static int32_t get_offset_of_nDeadWaitingTime_8() { return static_cast<int32_t>(offsetof(sLevelConfig_t4291590291, ___nDeadWaitingTime_8)); }
	inline int32_t get_nDeadWaitingTime_8() const { return ___nDeadWaitingTime_8; }
	inline int32_t* get_address_of_nDeadWaitingTime_8() { return &___nDeadWaitingTime_8; }
	inline void set_nDeadWaitingTime_8(int32_t value)
	{
		___nDeadWaitingTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLEVELCONFIG_T4291590291_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef TONEMAPPERTYPE_T52991894_H
#define TONEMAPPERTYPE_T52991894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
struct  TonemapperType_t52991894 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TonemapperType_t52991894, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPERTYPE_T52991894_H
#ifndef DOF34QUALITYSETTING_T3636551379_H
#define DOF34QUALITYSETTING_T3636551379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
struct  Dof34QualitySetting_t3636551379 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Dof34QualitySetting_t3636551379, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOF34QUALITYSETTING_T3636551379_H
#ifndef OVERLAYBLENDMODE_T429753458_H
#define OVERLAYBLENDMODE_T429753458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
struct  OverlayBlendMode_t429753458 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OverlayBlendMode_t429753458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLAYBLENDMODE_T429753458_H
#ifndef BLURSAMPLECOUNT_T3210294001_H
#define BLURSAMPLECOUNT_T3210294001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
struct  BlurSampleCount_t3210294001 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurSampleCount_t3210294001, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURSAMPLECOUNT_T3210294001_H
#ifndef SHAFTSSCREENBLENDMODE_T4165054462_H
#define SHAFTSSCREENBLENDMODE_T4165054462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
struct  ShaftsScreenBlendMode_t4165054462 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShaftsScreenBlendMode_t4165054462, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAFTSSCREENBLENDMODE_T4165054462_H
#ifndef BLURTYPE_T3871645803_H
#define BLURTYPE_T3871645803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfField/BlurType
struct  BlurType_t3871645803 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfField/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t3871645803, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T3871645803_H
#ifndef SSAOSAMPLES_T2619211009_H
#define SSAOSAMPLES_T2619211009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
struct  SSAOSamples_t2619211009 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSAOSamples_t2619211009, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSAOSAMPLES_T2619211009_H
#ifndef EGESTUREPOPODIR_T4294713869_H
#define EGESTUREPOPODIR_T4294713869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGestureManager/eGesturePopoDir
struct  eGesturePopoDir_t4294713869 
{
public:
	// System.Int32 CGestureManager/eGesturePopoDir::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eGesturePopoDir_t4294713869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGESTUREPOPODIR_T4294713869_H
#ifndef TILTSHIFTMODE_T1375727185_H
#define TILTSHIFTMODE_T1375727185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
struct  TiltShiftMode_t1375727185 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TiltShiftMode_t1375727185, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTSHIFTMODE_T1375727185_H
#ifndef TILTSHIFTQUALITY_T4173331534_H
#define TILTSHIFTQUALITY_T4173331534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
struct  TiltShiftQuality_t4173331534 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TiltShiftQuality_t4173331534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTSHIFTQUALITY_T4173331534_H
#ifndef EDGEDETECTMODE_T1984240676_H
#define EDGEDETECTMODE_T1984240676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
struct  EdgeDetectMode_t1984240676 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EdgeDetectMode_t1984240676, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEDETECTMODE_T1984240676_H
#ifndef EGAMEOVERSUBPANELTYPE_T2225654706_H
#define EGAMEOVERSUBPANELTYPE_T2225654706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGameOverManager/eGameOverSubPanelType
struct  eGameOverSubPanelType_t2225654706 
{
public:
	// System.Int32 CGameOverManager/eGameOverSubPanelType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eGameOverSubPanelType_t2225654706, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGAMEOVERSUBPANELTYPE_T2225654706_H
#ifndef BOKEHDESTINATION_T1233703462_H
#define BOKEHDESTINATION_T1233703462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
struct  BokehDestination_t1233703462 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BokehDestination_t1233703462, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOKEHDESTINATION_T1233703462_H
#ifndef DOFBLURRINESS_T473098480_H
#define DOFBLURRINESS_T473098480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
struct  DofBlurriness_t473098480 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DofBlurriness_t473098480, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOFBLURRINESS_T473098480_H
#ifndef EITEMFUNC_T2560496039_H
#define EITEMFUNC_T2560496039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CItemSystem/eItemFunc
struct  eItemFunc_t2560496039 
{
public:
	// System.Int32 CItemSystem/eItemFunc::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eItemFunc_t2560496039, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EITEMFUNC_T2560496039_H
#ifndef ADAPTIVETEXSIZE_T1062941056_H
#define ADAPTIVETEXSIZE_T1062941056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
struct  AdaptiveTexSize_t1062941056 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdaptiveTexSize_t1062941056, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVETEXSIZE_T1062941056_H
#ifndef DOFRESOLUTION_T1566655669_H
#define DOFRESOLUTION_T1566655669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
struct  DofResolution_t1566655669 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DofResolution_t1566655669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOFRESOLUTION_T1566655669_H
#ifndef ABERRATIONMODE_T218549536_H
#define ABERRATIONMODE_T218549536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode
struct  AberrationMode_t218549536 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AberrationMode_t218549536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABERRATIONMODE_T218549536_H
#ifndef LENSFLARESTYLE34_T4260782719_H
#define LENSFLARESTYLE34_T4260782719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.LensflareStyle34
struct  LensflareStyle34_t4260782719 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.LensflareStyle34::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LensflareStyle34_t4260782719, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSFLARESTYLE34_T4260782719_H
#ifndef BLOOMQUALITY_T3369172721_H
#define BLOOMQUALITY_T3369172721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
struct  BloomQuality_t3369172721 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/BloomQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BloomQuality_t3369172721, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMQUALITY_T3369172721_H
#ifndef BLOOMSCREENBLENDMODE_T2012607685_H
#define BLOOMSCREENBLENDMODE_T2012607685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
struct  BloomScreenBlendMode_t2012607685 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BloomScreenBlendMode_t2012607685, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMSCREENBLENDMODE_T2012607685_H
#ifndef HDRBLOOMMODE_T3774419504_H
#define HDRBLOOMMODE_T3774419504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
struct  HDRBloomMode_t3774419504 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HDRBloomMode_t3774419504, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDRBLOOMMODE_T3774419504_H
#ifndef TWEAKMODE_T747557136_H
#define TWEAKMODE_T747557136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/TweakMode
struct  TweakMode_t747557136 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/TweakMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweakMode_t747557136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKMODE_T747557136_H
#ifndef LENSFLARESTYLE_T630413071_H
#define LENSFLARESTYLE_T630413071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
struct  LensFlareStyle_t630413071 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LensFlareStyle_t630413071, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSFLARESTYLE_T630413071_H
#ifndef AAMODE_T1871701680_H
#define AAMODE_T1871701680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.AAMode
struct  AAMode_t1871701680 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.AAMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AAMode_t1871701680, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AAMODE_T1871701680_H
#ifndef ELIGHTTYPE_T683687508_H
#define ELIGHTTYPE_T683687508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLightSystem/eLightType
struct  eLightType_t683687508 
{
public:
	// System.Int32 CLightSystem/eLightType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eLightType_t683687508, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELIGHTTYPE_T683687508_H
#ifndef EMAINFIGHTEFFECTTYPE_T3875050507_H
#define EMAINFIGHTEFFECTTYPE_T3875050507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEffectManager/eMainFightEffectType
struct  eMainFightEffectType_t3875050507 
{
public:
	// System.Int32 CEffectManager/eMainFightEffectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMainFightEffectType_t3875050507, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAINFIGHTEFFECTTYPE_T3875050507_H
#ifndef ESKILLEFFECTTYPE_T1495465422_H
#define ESKILLEFFECTTYPE_T1495465422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEffectManager/eSkillEffectType
struct  eSkillEffectType_t1495465422 
{
public:
	// System.Int32 CEffectManager/eSkillEffectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillEffectType_t1495465422, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLEFFECTTYPE_T1495465422_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TWEAKMODE34_T984135990_H
#define TWEAKMODE34_T984135990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TweakMode34
struct  TweakMode34_t984135990 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.TweakMode34::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweakMode34_t984135990, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKMODE34_T984135990_H
#ifndef SUNSHAFTSRESOLUTION_T3826757637_H
#define SUNSHAFTSRESOLUTION_T3826757637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
struct  SunShaftsResolution_t3826757637 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SunShaftsResolution_t3826757637, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUNSHAFTSRESOLUTION_T3826757637_H
#ifndef COLORCORRECTIONMODE_T1416458051_H
#define COLORCORRECTIONMODE_T1416458051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
struct  ColorCorrectionMode_t1416458051 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorCorrectionMode_t1416458051, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONMODE_T1416458051_H
#ifndef MOTIONBLURFILTER_T520253047_H
#define MOTIONBLURFILTER_T520253047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
struct  MotionBlurFilter_t520253047 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionBlurFilter_t520253047, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURFILTER_T520253047_H
#ifndef BLURTYPE_T1046251128_H
#define BLURTYPE_T1046251128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
struct  BlurType_t1046251128 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BlurOptimized/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t1046251128, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T1046251128_H
#ifndef BLURTYPE_T2416258039_H
#define BLURTYPE_T2416258039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
struct  BlurType_t2416258039 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized/BlurType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlurType_t2416258039, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLURTYPE_T2416258039_H
#ifndef BLOOMSCREENBLENDMODE_T19712272_H
#define BLOOMSCREENBLENDMODE_T19712272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomScreenBlendMode
struct  BloomScreenBlendMode_t19712272 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomScreenBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BloomScreenBlendMode_t19712272, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMSCREENBLENDMODE_T19712272_H
#ifndef HDRBLOOMMODE_T4271191419_H
#define HDRBLOOMMODE_T4271191419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.HDRBloomMode
struct  HDRBloomMode_t4271191419 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.HDRBloomMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HDRBloomMode_t4271191419, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDRBLOOMMODE_T4271191419_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef FILTERMODE_T3761284007_H
#define FILTERMODE_T3761284007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t3761284007 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t3761284007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T3761284007_H
#ifndef RESOLUTION_T1804605042_H
#define RESOLUTION_T1804605042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
struct  Resolution_t1804605042 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized/Resolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Resolution_t1804605042, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T1804605042_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CGESTURE_T2181168874_H
#define CGESTURE_T2181168874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGesture
struct  CGesture_t2181168874  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CGesture::m_bUI
	bool ___m_bUI_3;
	// UnityEngine.SpriteRenderer CGesture::m_sprMain
	SpriteRenderer_t3235626157 * ___m_sprMain_4;
	// UnityEngine.UI.Image CGesture::m_imgMain
	Image_t2670269651 * ___m_imgMain_5;
	// UnityEngine.UI.Button CGesture::m_btnMain
	Button_t4055032469 * ___m_btnMain_6;
	// System.Int32 CGesture::m_nId
	int32_t ___m_nId_7;

public:
	inline static int32_t get_offset_of_m_bUI_3() { return static_cast<int32_t>(offsetof(CGesture_t2181168874, ___m_bUI_3)); }
	inline bool get_m_bUI_3() const { return ___m_bUI_3; }
	inline bool* get_address_of_m_bUI_3() { return &___m_bUI_3; }
	inline void set_m_bUI_3(bool value)
	{
		___m_bUI_3 = value;
	}

	inline static int32_t get_offset_of_m_sprMain_4() { return static_cast<int32_t>(offsetof(CGesture_t2181168874, ___m_sprMain_4)); }
	inline SpriteRenderer_t3235626157 * get_m_sprMain_4() const { return ___m_sprMain_4; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_sprMain_4() { return &___m_sprMain_4; }
	inline void set_m_sprMain_4(SpriteRenderer_t3235626157 * value)
	{
		___m_sprMain_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMain_4), value);
	}

	inline static int32_t get_offset_of_m_imgMain_5() { return static_cast<int32_t>(offsetof(CGesture_t2181168874, ___m_imgMain_5)); }
	inline Image_t2670269651 * get_m_imgMain_5() const { return ___m_imgMain_5; }
	inline Image_t2670269651 ** get_address_of_m_imgMain_5() { return &___m_imgMain_5; }
	inline void set_m_imgMain_5(Image_t2670269651 * value)
	{
		___m_imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgMain_5), value);
	}

	inline static int32_t get_offset_of_m_btnMain_6() { return static_cast<int32_t>(offsetof(CGesture_t2181168874, ___m_btnMain_6)); }
	inline Button_t4055032469 * get_m_btnMain_6() const { return ___m_btnMain_6; }
	inline Button_t4055032469 ** get_address_of_m_btnMain_6() { return &___m_btnMain_6; }
	inline void set_m_btnMain_6(Button_t4055032469 * value)
	{
		___m_btnMain_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_btnMain_6), value);
	}

	inline static int32_t get_offset_of_m_nId_7() { return static_cast<int32_t>(offsetof(CGesture_t2181168874, ___m_nId_7)); }
	inline int32_t get_m_nId_7() const { return ___m_nId_7; }
	inline int32_t* get_address_of_m_nId_7() { return &___m_nId_7; }
	inline void set_m_nId_7(int32_t value)
	{
		___m_nId_7 = value;
	}
};

struct CGesture_t2181168874_StaticFields
{
public:
	// UnityEngine.Vector3 CGesture::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CGesture_t2181168874_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGESTURE_T2181168874_H
#ifndef CGAMEOVERMANAGER_T3022133880_H
#define CGAMEOVERMANAGER_T3022133880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGameOverManager
struct  CGameOverManager_t3022133880  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CGameOverManager::_goPanel
	GameObject_t1113636619 * ____goPanel_5;
	// System.Single CGameOverManager::m_fCountingBorderMinFill
	float ___m_fCountingBorderMinFill_6;
	// System.Single CGameOverManager::m_fCountingBorderMaxFill
	float ___m_fCountingBorderMaxFill_7;
	// System.Single CGameOverManager::m_fFillSpeed
	float ___m_fFillSpeed_8;
	// System.Single CGameOverManager::m_fRankTextMaxScale
	float ___m_fRankTextMaxScale_9;
	// System.Single CGameOverManager::m_fRankTextMinScale
	float ___m_fRankTextMinScale_10;
	// System.Single CGameOverManager::m_fRankTextAnimationTime
	float ___m_fRankTextAnimationTime_11;
	// System.Boolean CGameOverManager::m_bRankTextAnimation
	bool ___m_bRankTextAnimation_12;
	// System.Single CGameOverManager::m_fRankTextAnimationScaleV
	float ___m_fRankTextAnimationScaleV_13;
	// UnityEngine.GameObject CGameOverManager::_goTextRank
	GameObject_t1113636619 * ____goTextRank_14;
	// UnityEngine.GameObject CGameOverManager::_goTextRank_CanYing
	GameObject_t1113636619 * ____goTextRank_CanYing_15;
	// UnityEngine.GameObject CGameOverManager::_goTextTitle
	GameObject_t1113636619 * ____goTextTitle_16;
	// UnityEngine.GameObject CGameOverManager::_goTextTitle_CanYing
	GameObject_t1113636619 * ____goTextTitle_CanYing_17;
	// System.Single CGameOverManager::m_fOneTuoWeiTotalTime
	float ___m_fOneTuoWeiTotalTime_18;
	// System.Single CGameOverManager::m_fOneTuoWeiTotalTimeMiddle
	float ___m_fOneTuoWeiTotalTimeMiddle_19;
	// UnityEngine.UI.Text CGameOverManager::_txtRank
	Text_t1901882714 * ____txtRank_20;
	// UnityEngine.UI.Text CGameOverManager::_txtRank_CanYing
	Text_t1901882714 * ____txtRank_CanYing_21;
	// UnityEngine.GameObject CGameOverManager::_containerRank
	GameObject_t1113636619 * ____containerRank_22;
	// System.Single CGameOverManager::m_fTimeElpase
	float ___m_fTimeElpase_25;
	// CBiShuaTuoWei[] CGameOverManager::m_aryTuoWei
	CBiShuaTuoWeiU5BU5D_t1377408237* ___m_aryTuoWei_26;
	// UnityEngine.GameObject CGameOverManager::_panelCounting
	GameObject_t1113636619 * ____panelCounting_27;
	// UnityEngine.UI.Text CGameOverManager::_txtCounting
	Text_t1901882714 * ____txtCounting_28;
	// UnityEngine.UI.Image CGameOverManager::_imgCountingBorder
	Image_t2670269651 * ____imgCountingBorder_29;
	// UnityEngine.GameObject[] CGameOverManager::m_arySubPanels
	GameObjectU5BU5D_t3328599146* ___m_arySubPanels_30;
	// System.Int32 CGameOverManager::m_nTuoWeiStatus
	int32_t ___m_nTuoWeiStatus_31;
	// System.Single CGameOverManager::m_fTuoWeiTimeElapse
	float ___m_fTuoWeiTimeElapse_32;
	// System.Boolean CGameOverManager::m_bRankFirstFrame
	bool ___m_bRankFirstFrame_33;
	// System.Boolean CGameOverManager::m_bTitleFirstFrame
	bool ___m_bTitleFirstFrame_34;
	// System.Int32 CGameOverManager::m_nFrameCount
	int32_t ___m_nFrameCount_35;
	// System.Boolean CGameOverManager::m_bRankAnimationStarted
	bool ___m_bRankAnimationStarted_36;
	// System.Boolean CGameOverManager::m_bTitleAnimationStarted
	bool ___m_bTitleAnimationStarted_37;
	// System.Single CGameOverManager::m_fCurFillAmount
	float ___m_fCurFillAmount_38;
	// System.Int32 CGameOverManager::m_nCountingBorderStatus
	int32_t ___m_nCountingBorderStatus_39;

public:
	inline static int32_t get_offset_of__goPanel_5() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____goPanel_5)); }
	inline GameObject_t1113636619 * get__goPanel_5() const { return ____goPanel_5; }
	inline GameObject_t1113636619 ** get_address_of__goPanel_5() { return &____goPanel_5; }
	inline void set__goPanel_5(GameObject_t1113636619 * value)
	{
		____goPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&____goPanel_5), value);
	}

	inline static int32_t get_offset_of_m_fCountingBorderMinFill_6() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fCountingBorderMinFill_6)); }
	inline float get_m_fCountingBorderMinFill_6() const { return ___m_fCountingBorderMinFill_6; }
	inline float* get_address_of_m_fCountingBorderMinFill_6() { return &___m_fCountingBorderMinFill_6; }
	inline void set_m_fCountingBorderMinFill_6(float value)
	{
		___m_fCountingBorderMinFill_6 = value;
	}

	inline static int32_t get_offset_of_m_fCountingBorderMaxFill_7() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fCountingBorderMaxFill_7)); }
	inline float get_m_fCountingBorderMaxFill_7() const { return ___m_fCountingBorderMaxFill_7; }
	inline float* get_address_of_m_fCountingBorderMaxFill_7() { return &___m_fCountingBorderMaxFill_7; }
	inline void set_m_fCountingBorderMaxFill_7(float value)
	{
		___m_fCountingBorderMaxFill_7 = value;
	}

	inline static int32_t get_offset_of_m_fFillSpeed_8() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fFillSpeed_8)); }
	inline float get_m_fFillSpeed_8() const { return ___m_fFillSpeed_8; }
	inline float* get_address_of_m_fFillSpeed_8() { return &___m_fFillSpeed_8; }
	inline void set_m_fFillSpeed_8(float value)
	{
		___m_fFillSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_fRankTextMaxScale_9() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fRankTextMaxScale_9)); }
	inline float get_m_fRankTextMaxScale_9() const { return ___m_fRankTextMaxScale_9; }
	inline float* get_address_of_m_fRankTextMaxScale_9() { return &___m_fRankTextMaxScale_9; }
	inline void set_m_fRankTextMaxScale_9(float value)
	{
		___m_fRankTextMaxScale_9 = value;
	}

	inline static int32_t get_offset_of_m_fRankTextMinScale_10() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fRankTextMinScale_10)); }
	inline float get_m_fRankTextMinScale_10() const { return ___m_fRankTextMinScale_10; }
	inline float* get_address_of_m_fRankTextMinScale_10() { return &___m_fRankTextMinScale_10; }
	inline void set_m_fRankTextMinScale_10(float value)
	{
		___m_fRankTextMinScale_10 = value;
	}

	inline static int32_t get_offset_of_m_fRankTextAnimationTime_11() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fRankTextAnimationTime_11)); }
	inline float get_m_fRankTextAnimationTime_11() const { return ___m_fRankTextAnimationTime_11; }
	inline float* get_address_of_m_fRankTextAnimationTime_11() { return &___m_fRankTextAnimationTime_11; }
	inline void set_m_fRankTextAnimationTime_11(float value)
	{
		___m_fRankTextAnimationTime_11 = value;
	}

	inline static int32_t get_offset_of_m_bRankTextAnimation_12() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_bRankTextAnimation_12)); }
	inline bool get_m_bRankTextAnimation_12() const { return ___m_bRankTextAnimation_12; }
	inline bool* get_address_of_m_bRankTextAnimation_12() { return &___m_bRankTextAnimation_12; }
	inline void set_m_bRankTextAnimation_12(bool value)
	{
		___m_bRankTextAnimation_12 = value;
	}

	inline static int32_t get_offset_of_m_fRankTextAnimationScaleV_13() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fRankTextAnimationScaleV_13)); }
	inline float get_m_fRankTextAnimationScaleV_13() const { return ___m_fRankTextAnimationScaleV_13; }
	inline float* get_address_of_m_fRankTextAnimationScaleV_13() { return &___m_fRankTextAnimationScaleV_13; }
	inline void set_m_fRankTextAnimationScaleV_13(float value)
	{
		___m_fRankTextAnimationScaleV_13 = value;
	}

	inline static int32_t get_offset_of__goTextRank_14() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____goTextRank_14)); }
	inline GameObject_t1113636619 * get__goTextRank_14() const { return ____goTextRank_14; }
	inline GameObject_t1113636619 ** get_address_of__goTextRank_14() { return &____goTextRank_14; }
	inline void set__goTextRank_14(GameObject_t1113636619 * value)
	{
		____goTextRank_14 = value;
		Il2CppCodeGenWriteBarrier((&____goTextRank_14), value);
	}

	inline static int32_t get_offset_of__goTextRank_CanYing_15() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____goTextRank_CanYing_15)); }
	inline GameObject_t1113636619 * get__goTextRank_CanYing_15() const { return ____goTextRank_CanYing_15; }
	inline GameObject_t1113636619 ** get_address_of__goTextRank_CanYing_15() { return &____goTextRank_CanYing_15; }
	inline void set__goTextRank_CanYing_15(GameObject_t1113636619 * value)
	{
		____goTextRank_CanYing_15 = value;
		Il2CppCodeGenWriteBarrier((&____goTextRank_CanYing_15), value);
	}

	inline static int32_t get_offset_of__goTextTitle_16() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____goTextTitle_16)); }
	inline GameObject_t1113636619 * get__goTextTitle_16() const { return ____goTextTitle_16; }
	inline GameObject_t1113636619 ** get_address_of__goTextTitle_16() { return &____goTextTitle_16; }
	inline void set__goTextTitle_16(GameObject_t1113636619 * value)
	{
		____goTextTitle_16 = value;
		Il2CppCodeGenWriteBarrier((&____goTextTitle_16), value);
	}

	inline static int32_t get_offset_of__goTextTitle_CanYing_17() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____goTextTitle_CanYing_17)); }
	inline GameObject_t1113636619 * get__goTextTitle_CanYing_17() const { return ____goTextTitle_CanYing_17; }
	inline GameObject_t1113636619 ** get_address_of__goTextTitle_CanYing_17() { return &____goTextTitle_CanYing_17; }
	inline void set__goTextTitle_CanYing_17(GameObject_t1113636619 * value)
	{
		____goTextTitle_CanYing_17 = value;
		Il2CppCodeGenWriteBarrier((&____goTextTitle_CanYing_17), value);
	}

	inline static int32_t get_offset_of_m_fOneTuoWeiTotalTime_18() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fOneTuoWeiTotalTime_18)); }
	inline float get_m_fOneTuoWeiTotalTime_18() const { return ___m_fOneTuoWeiTotalTime_18; }
	inline float* get_address_of_m_fOneTuoWeiTotalTime_18() { return &___m_fOneTuoWeiTotalTime_18; }
	inline void set_m_fOneTuoWeiTotalTime_18(float value)
	{
		___m_fOneTuoWeiTotalTime_18 = value;
	}

	inline static int32_t get_offset_of_m_fOneTuoWeiTotalTimeMiddle_19() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fOneTuoWeiTotalTimeMiddle_19)); }
	inline float get_m_fOneTuoWeiTotalTimeMiddle_19() const { return ___m_fOneTuoWeiTotalTimeMiddle_19; }
	inline float* get_address_of_m_fOneTuoWeiTotalTimeMiddle_19() { return &___m_fOneTuoWeiTotalTimeMiddle_19; }
	inline void set_m_fOneTuoWeiTotalTimeMiddle_19(float value)
	{
		___m_fOneTuoWeiTotalTimeMiddle_19 = value;
	}

	inline static int32_t get_offset_of__txtRank_20() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____txtRank_20)); }
	inline Text_t1901882714 * get__txtRank_20() const { return ____txtRank_20; }
	inline Text_t1901882714 ** get_address_of__txtRank_20() { return &____txtRank_20; }
	inline void set__txtRank_20(Text_t1901882714 * value)
	{
		____txtRank_20 = value;
		Il2CppCodeGenWriteBarrier((&____txtRank_20), value);
	}

	inline static int32_t get_offset_of__txtRank_CanYing_21() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____txtRank_CanYing_21)); }
	inline Text_t1901882714 * get__txtRank_CanYing_21() const { return ____txtRank_CanYing_21; }
	inline Text_t1901882714 ** get_address_of__txtRank_CanYing_21() { return &____txtRank_CanYing_21; }
	inline void set__txtRank_CanYing_21(Text_t1901882714 * value)
	{
		____txtRank_CanYing_21 = value;
		Il2CppCodeGenWriteBarrier((&____txtRank_CanYing_21), value);
	}

	inline static int32_t get_offset_of__containerRank_22() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____containerRank_22)); }
	inline GameObject_t1113636619 * get__containerRank_22() const { return ____containerRank_22; }
	inline GameObject_t1113636619 ** get_address_of__containerRank_22() { return &____containerRank_22; }
	inline void set__containerRank_22(GameObject_t1113636619 * value)
	{
		____containerRank_22 = value;
		Il2CppCodeGenWriteBarrier((&____containerRank_22), value);
	}

	inline static int32_t get_offset_of_m_fTimeElpase_25() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fTimeElpase_25)); }
	inline float get_m_fTimeElpase_25() const { return ___m_fTimeElpase_25; }
	inline float* get_address_of_m_fTimeElpase_25() { return &___m_fTimeElpase_25; }
	inline void set_m_fTimeElpase_25(float value)
	{
		___m_fTimeElpase_25 = value;
	}

	inline static int32_t get_offset_of_m_aryTuoWei_26() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_aryTuoWei_26)); }
	inline CBiShuaTuoWeiU5BU5D_t1377408237* get_m_aryTuoWei_26() const { return ___m_aryTuoWei_26; }
	inline CBiShuaTuoWeiU5BU5D_t1377408237** get_address_of_m_aryTuoWei_26() { return &___m_aryTuoWei_26; }
	inline void set_m_aryTuoWei_26(CBiShuaTuoWeiU5BU5D_t1377408237* value)
	{
		___m_aryTuoWei_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTuoWei_26), value);
	}

	inline static int32_t get_offset_of__panelCounting_27() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____panelCounting_27)); }
	inline GameObject_t1113636619 * get__panelCounting_27() const { return ____panelCounting_27; }
	inline GameObject_t1113636619 ** get_address_of__panelCounting_27() { return &____panelCounting_27; }
	inline void set__panelCounting_27(GameObject_t1113636619 * value)
	{
		____panelCounting_27 = value;
		Il2CppCodeGenWriteBarrier((&____panelCounting_27), value);
	}

	inline static int32_t get_offset_of__txtCounting_28() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____txtCounting_28)); }
	inline Text_t1901882714 * get__txtCounting_28() const { return ____txtCounting_28; }
	inline Text_t1901882714 ** get_address_of__txtCounting_28() { return &____txtCounting_28; }
	inline void set__txtCounting_28(Text_t1901882714 * value)
	{
		____txtCounting_28 = value;
		Il2CppCodeGenWriteBarrier((&____txtCounting_28), value);
	}

	inline static int32_t get_offset_of__imgCountingBorder_29() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ____imgCountingBorder_29)); }
	inline Image_t2670269651 * get__imgCountingBorder_29() const { return ____imgCountingBorder_29; }
	inline Image_t2670269651 ** get_address_of__imgCountingBorder_29() { return &____imgCountingBorder_29; }
	inline void set__imgCountingBorder_29(Image_t2670269651 * value)
	{
		____imgCountingBorder_29 = value;
		Il2CppCodeGenWriteBarrier((&____imgCountingBorder_29), value);
	}

	inline static int32_t get_offset_of_m_arySubPanels_30() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_arySubPanels_30)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySubPanels_30() const { return ___m_arySubPanels_30; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySubPanels_30() { return &___m_arySubPanels_30; }
	inline void set_m_arySubPanels_30(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySubPanels_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySubPanels_30), value);
	}

	inline static int32_t get_offset_of_m_nTuoWeiStatus_31() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_nTuoWeiStatus_31)); }
	inline int32_t get_m_nTuoWeiStatus_31() const { return ___m_nTuoWeiStatus_31; }
	inline int32_t* get_address_of_m_nTuoWeiStatus_31() { return &___m_nTuoWeiStatus_31; }
	inline void set_m_nTuoWeiStatus_31(int32_t value)
	{
		___m_nTuoWeiStatus_31 = value;
	}

	inline static int32_t get_offset_of_m_fTuoWeiTimeElapse_32() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fTuoWeiTimeElapse_32)); }
	inline float get_m_fTuoWeiTimeElapse_32() const { return ___m_fTuoWeiTimeElapse_32; }
	inline float* get_address_of_m_fTuoWeiTimeElapse_32() { return &___m_fTuoWeiTimeElapse_32; }
	inline void set_m_fTuoWeiTimeElapse_32(float value)
	{
		___m_fTuoWeiTimeElapse_32 = value;
	}

	inline static int32_t get_offset_of_m_bRankFirstFrame_33() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_bRankFirstFrame_33)); }
	inline bool get_m_bRankFirstFrame_33() const { return ___m_bRankFirstFrame_33; }
	inline bool* get_address_of_m_bRankFirstFrame_33() { return &___m_bRankFirstFrame_33; }
	inline void set_m_bRankFirstFrame_33(bool value)
	{
		___m_bRankFirstFrame_33 = value;
	}

	inline static int32_t get_offset_of_m_bTitleFirstFrame_34() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_bTitleFirstFrame_34)); }
	inline bool get_m_bTitleFirstFrame_34() const { return ___m_bTitleFirstFrame_34; }
	inline bool* get_address_of_m_bTitleFirstFrame_34() { return &___m_bTitleFirstFrame_34; }
	inline void set_m_bTitleFirstFrame_34(bool value)
	{
		___m_bTitleFirstFrame_34 = value;
	}

	inline static int32_t get_offset_of_m_nFrameCount_35() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_nFrameCount_35)); }
	inline int32_t get_m_nFrameCount_35() const { return ___m_nFrameCount_35; }
	inline int32_t* get_address_of_m_nFrameCount_35() { return &___m_nFrameCount_35; }
	inline void set_m_nFrameCount_35(int32_t value)
	{
		___m_nFrameCount_35 = value;
	}

	inline static int32_t get_offset_of_m_bRankAnimationStarted_36() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_bRankAnimationStarted_36)); }
	inline bool get_m_bRankAnimationStarted_36() const { return ___m_bRankAnimationStarted_36; }
	inline bool* get_address_of_m_bRankAnimationStarted_36() { return &___m_bRankAnimationStarted_36; }
	inline void set_m_bRankAnimationStarted_36(bool value)
	{
		___m_bRankAnimationStarted_36 = value;
	}

	inline static int32_t get_offset_of_m_bTitleAnimationStarted_37() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_bTitleAnimationStarted_37)); }
	inline bool get_m_bTitleAnimationStarted_37() const { return ___m_bTitleAnimationStarted_37; }
	inline bool* get_address_of_m_bTitleAnimationStarted_37() { return &___m_bTitleAnimationStarted_37; }
	inline void set_m_bTitleAnimationStarted_37(bool value)
	{
		___m_bTitleAnimationStarted_37 = value;
	}

	inline static int32_t get_offset_of_m_fCurFillAmount_38() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_fCurFillAmount_38)); }
	inline float get_m_fCurFillAmount_38() const { return ___m_fCurFillAmount_38; }
	inline float* get_address_of_m_fCurFillAmount_38() { return &___m_fCurFillAmount_38; }
	inline void set_m_fCurFillAmount_38(float value)
	{
		___m_fCurFillAmount_38 = value;
	}

	inline static int32_t get_offset_of_m_nCountingBorderStatus_39() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880, ___m_nCountingBorderStatus_39)); }
	inline int32_t get_m_nCountingBorderStatus_39() const { return ___m_nCountingBorderStatus_39; }
	inline int32_t* get_address_of_m_nCountingBorderStatus_39() { return &___m_nCountingBorderStatus_39; }
	inline void set_m_nCountingBorderStatus_39(int32_t value)
	{
		___m_nCountingBorderStatus_39 = value;
	}
};

struct CGameOverManager_t3022133880_StaticFields
{
public:
	// UnityEngine.Vector3 CGameOverManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CGameOverManager::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// CGameOverManager CGameOverManager::s_Instance
	CGameOverManager_t3022133880 * ___s_Instance_4;
	// System.Boolean CGameOverManager::s_bNowGameOvering
	bool ___s_bNowGameOvering_23;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880_StaticFields, ___s_Instance_4)); }
	inline CGameOverManager_t3022133880 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CGameOverManager_t3022133880 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CGameOverManager_t3022133880 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_s_bNowGameOvering_23() { return static_cast<int32_t>(offsetof(CGameOverManager_t3022133880_StaticFields, ___s_bNowGameOvering_23)); }
	inline bool get_s_bNowGameOvering_23() const { return ___s_bNowGameOvering_23; }
	inline bool* get_address_of_s_bNowGameOvering_23() { return &___s_bNowGameOvering_23; }
	inline void set_s_bNowGameOvering_23(bool value)
	{
		___s_bNowGameOvering_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGAMEOVERMANAGER_T3022133880_H
#ifndef CEXPLODEEVENT_T2581699128_H
#define CEXPLODEEVENT_T2581699128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CExplodeEvent
struct  CExplodeEvent_t2581699128  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CEXPLODEEVENT_T2581699128_H
#ifndef CITEM_T1170024128_H
#define CITEM_T1170024128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CItem
struct  CItem_t1170024128  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITEM_T1170024128_H
#ifndef CITEMSYSTEM_T1283876235_H
#define CITEMSYSTEM_T1283876235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CItemSystem
struct  CItemSystem_t1283876235  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Dropdown CItemSystem::_dropdownFunc
	Dropdown_t2274391225 * ____dropdownFunc_6;
	// UnityEngine.UI.Dropdown CItemSystem::_dropdownLevel
	Dropdown_t2274391225 * ____dropdownLevel_7;
	// UnityEngine.UI.InputField CItemSystem::_inputName
	InputField_t3762917431 * ____inputName_8;
	// UnityEngine.UI.InputField CItemSystem::_inputDesc
	InputField_t3762917431 * ____inputDesc_9;
	// UnityEngine.UI.InputField CItemSystem::_inputSpriteId
	InputField_t3762917431 * ____inputSpriteId_10;
	// UnityEngine.UI.InputField CItemSystem::_inputShortKey
	InputField_t3762917431 * ____inputShortKey_11;
	// UnityEngine.UI.InputField CItemSystem::_inputPrice
	InputField_t3762917431 * ____inputPrice_12;
	// UnityEngine.UI.InputField CItemSystem::_inputValue
	InputField_t3762917431 * ____inputValue_13;
	// UnityEngine.Sprite[] CItemSystem::m_aryItemSprite
	SpriteU5BU5D_t2581906349* ___m_aryItemSprite_14;
	// UIItem[] CItemSystem::m_aryUiItem
	UIItemU5BU5D_t2543761648* ___m_aryUiItem_15;
	// UIItem[] CItemSystem::m_aryUiItem_PC
	UIItemU5BU5D_t2543761648* ___m_aryUiItem_PC_16;
	// UIItem[] CItemSystem::m_aryUiItem_Mobile
	UIItemU5BU5D_t2543761648* ___m_aryUiItem_Mobile_17;
	// CItemSystem/sItemConfig CItemSystem::m_CurItemConfig
	sItemConfig_t2971202336  ___m_CurItemConfig_18;
	// CItemSystem/sValueByLevel CItemSystem::tempValueByLevel
	sValueByLevel_t810710842  ___tempValueByLevel_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,CItemSystem/sItemConfig> CItemSystem::m_dicItemConfig
	Dictionary_2_t1859915667 * ___m_dicItemConfig_20;
	// CItemSystem/sItemConfig CItemSystem::tempItemConfig
	sItemConfig_t2971202336  ___tempItemConfig_21;
	// System.Int32 CItemSystem::m_nCurConfigId
	int32_t ___m_nCurConfigId_22;
	// System.Int32 CItemSystem::m_nCurLevel
	int32_t ___m_nCurLevel_23;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CItemSystem::m_dicItemNum
	Dictionary_2_t1839659084 * ___m_dicItemNum_24;
	// System.Boolean CItemSystem::m_bUiInited
	bool ___m_bUiInited_25;
	// System.Collections.Generic.List`1<CItemBuyEffect> CItemSystem::m_lstItemBuyEffect
	List_1_t2305279210 * ___m_lstItemBuyEffect_26;
	// UnityEngine.Vector2[] CItemSystem::m_aryCounterPos
	Vector2U5BU5D_t1457185986* ___m_aryCounterPos_27;
	// System.Boolean[] CItemSystem::m_aryCounterTaken
	BooleanU5BU5D_t2897418192* ___m_aryCounterTaken_28;

public:
	inline static int32_t get_offset_of__dropdownFunc_6() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____dropdownFunc_6)); }
	inline Dropdown_t2274391225 * get__dropdownFunc_6() const { return ____dropdownFunc_6; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownFunc_6() { return &____dropdownFunc_6; }
	inline void set__dropdownFunc_6(Dropdown_t2274391225 * value)
	{
		____dropdownFunc_6 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownFunc_6), value);
	}

	inline static int32_t get_offset_of__dropdownLevel_7() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____dropdownLevel_7)); }
	inline Dropdown_t2274391225 * get__dropdownLevel_7() const { return ____dropdownLevel_7; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownLevel_7() { return &____dropdownLevel_7; }
	inline void set__dropdownLevel_7(Dropdown_t2274391225 * value)
	{
		____dropdownLevel_7 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownLevel_7), value);
	}

	inline static int32_t get_offset_of__inputName_8() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____inputName_8)); }
	inline InputField_t3762917431 * get__inputName_8() const { return ____inputName_8; }
	inline InputField_t3762917431 ** get_address_of__inputName_8() { return &____inputName_8; }
	inline void set__inputName_8(InputField_t3762917431 * value)
	{
		____inputName_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputName_8), value);
	}

	inline static int32_t get_offset_of__inputDesc_9() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____inputDesc_9)); }
	inline InputField_t3762917431 * get__inputDesc_9() const { return ____inputDesc_9; }
	inline InputField_t3762917431 ** get_address_of__inputDesc_9() { return &____inputDesc_9; }
	inline void set__inputDesc_9(InputField_t3762917431 * value)
	{
		____inputDesc_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputDesc_9), value);
	}

	inline static int32_t get_offset_of__inputSpriteId_10() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____inputSpriteId_10)); }
	inline InputField_t3762917431 * get__inputSpriteId_10() const { return ____inputSpriteId_10; }
	inline InputField_t3762917431 ** get_address_of__inputSpriteId_10() { return &____inputSpriteId_10; }
	inline void set__inputSpriteId_10(InputField_t3762917431 * value)
	{
		____inputSpriteId_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputSpriteId_10), value);
	}

	inline static int32_t get_offset_of__inputShortKey_11() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____inputShortKey_11)); }
	inline InputField_t3762917431 * get__inputShortKey_11() const { return ____inputShortKey_11; }
	inline InputField_t3762917431 ** get_address_of__inputShortKey_11() { return &____inputShortKey_11; }
	inline void set__inputShortKey_11(InputField_t3762917431 * value)
	{
		____inputShortKey_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputShortKey_11), value);
	}

	inline static int32_t get_offset_of__inputPrice_12() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____inputPrice_12)); }
	inline InputField_t3762917431 * get__inputPrice_12() const { return ____inputPrice_12; }
	inline InputField_t3762917431 ** get_address_of__inputPrice_12() { return &____inputPrice_12; }
	inline void set__inputPrice_12(InputField_t3762917431 * value)
	{
		____inputPrice_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputPrice_12), value);
	}

	inline static int32_t get_offset_of__inputValue_13() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ____inputValue_13)); }
	inline InputField_t3762917431 * get__inputValue_13() const { return ____inputValue_13; }
	inline InputField_t3762917431 ** get_address_of__inputValue_13() { return &____inputValue_13; }
	inline void set__inputValue_13(InputField_t3762917431 * value)
	{
		____inputValue_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputValue_13), value);
	}

	inline static int32_t get_offset_of_m_aryItemSprite_14() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_aryItemSprite_14)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryItemSprite_14() const { return ___m_aryItemSprite_14; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryItemSprite_14() { return &___m_aryItemSprite_14; }
	inline void set_m_aryItemSprite_14(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryItemSprite_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemSprite_14), value);
	}

	inline static int32_t get_offset_of_m_aryUiItem_15() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_aryUiItem_15)); }
	inline UIItemU5BU5D_t2543761648* get_m_aryUiItem_15() const { return ___m_aryUiItem_15; }
	inline UIItemU5BU5D_t2543761648** get_address_of_m_aryUiItem_15() { return &___m_aryUiItem_15; }
	inline void set_m_aryUiItem_15(UIItemU5BU5D_t2543761648* value)
	{
		___m_aryUiItem_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUiItem_15), value);
	}

	inline static int32_t get_offset_of_m_aryUiItem_PC_16() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_aryUiItem_PC_16)); }
	inline UIItemU5BU5D_t2543761648* get_m_aryUiItem_PC_16() const { return ___m_aryUiItem_PC_16; }
	inline UIItemU5BU5D_t2543761648** get_address_of_m_aryUiItem_PC_16() { return &___m_aryUiItem_PC_16; }
	inline void set_m_aryUiItem_PC_16(UIItemU5BU5D_t2543761648* value)
	{
		___m_aryUiItem_PC_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUiItem_PC_16), value);
	}

	inline static int32_t get_offset_of_m_aryUiItem_Mobile_17() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_aryUiItem_Mobile_17)); }
	inline UIItemU5BU5D_t2543761648* get_m_aryUiItem_Mobile_17() const { return ___m_aryUiItem_Mobile_17; }
	inline UIItemU5BU5D_t2543761648** get_address_of_m_aryUiItem_Mobile_17() { return &___m_aryUiItem_Mobile_17; }
	inline void set_m_aryUiItem_Mobile_17(UIItemU5BU5D_t2543761648* value)
	{
		___m_aryUiItem_Mobile_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUiItem_Mobile_17), value);
	}

	inline static int32_t get_offset_of_m_CurItemConfig_18() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_CurItemConfig_18)); }
	inline sItemConfig_t2971202336  get_m_CurItemConfig_18() const { return ___m_CurItemConfig_18; }
	inline sItemConfig_t2971202336 * get_address_of_m_CurItemConfig_18() { return &___m_CurItemConfig_18; }
	inline void set_m_CurItemConfig_18(sItemConfig_t2971202336  value)
	{
		___m_CurItemConfig_18 = value;
	}

	inline static int32_t get_offset_of_tempValueByLevel_19() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___tempValueByLevel_19)); }
	inline sValueByLevel_t810710842  get_tempValueByLevel_19() const { return ___tempValueByLevel_19; }
	inline sValueByLevel_t810710842 * get_address_of_tempValueByLevel_19() { return &___tempValueByLevel_19; }
	inline void set_tempValueByLevel_19(sValueByLevel_t810710842  value)
	{
		___tempValueByLevel_19 = value;
	}

	inline static int32_t get_offset_of_m_dicItemConfig_20() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_dicItemConfig_20)); }
	inline Dictionary_2_t1859915667 * get_m_dicItemConfig_20() const { return ___m_dicItemConfig_20; }
	inline Dictionary_2_t1859915667 ** get_address_of_m_dicItemConfig_20() { return &___m_dicItemConfig_20; }
	inline void set_m_dicItemConfig_20(Dictionary_2_t1859915667 * value)
	{
		___m_dicItemConfig_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicItemConfig_20), value);
	}

	inline static int32_t get_offset_of_tempItemConfig_21() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___tempItemConfig_21)); }
	inline sItemConfig_t2971202336  get_tempItemConfig_21() const { return ___tempItemConfig_21; }
	inline sItemConfig_t2971202336 * get_address_of_tempItemConfig_21() { return &___tempItemConfig_21; }
	inline void set_tempItemConfig_21(sItemConfig_t2971202336  value)
	{
		___tempItemConfig_21 = value;
	}

	inline static int32_t get_offset_of_m_nCurConfigId_22() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_nCurConfigId_22)); }
	inline int32_t get_m_nCurConfigId_22() const { return ___m_nCurConfigId_22; }
	inline int32_t* get_address_of_m_nCurConfigId_22() { return &___m_nCurConfigId_22; }
	inline void set_m_nCurConfigId_22(int32_t value)
	{
		___m_nCurConfigId_22 = value;
	}

	inline static int32_t get_offset_of_m_nCurLevel_23() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_nCurLevel_23)); }
	inline int32_t get_m_nCurLevel_23() const { return ___m_nCurLevel_23; }
	inline int32_t* get_address_of_m_nCurLevel_23() { return &___m_nCurLevel_23; }
	inline void set_m_nCurLevel_23(int32_t value)
	{
		___m_nCurLevel_23 = value;
	}

	inline static int32_t get_offset_of_m_dicItemNum_24() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_dicItemNum_24)); }
	inline Dictionary_2_t1839659084 * get_m_dicItemNum_24() const { return ___m_dicItemNum_24; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicItemNum_24() { return &___m_dicItemNum_24; }
	inline void set_m_dicItemNum_24(Dictionary_2_t1839659084 * value)
	{
		___m_dicItemNum_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicItemNum_24), value);
	}

	inline static int32_t get_offset_of_m_bUiInited_25() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_bUiInited_25)); }
	inline bool get_m_bUiInited_25() const { return ___m_bUiInited_25; }
	inline bool* get_address_of_m_bUiInited_25() { return &___m_bUiInited_25; }
	inline void set_m_bUiInited_25(bool value)
	{
		___m_bUiInited_25 = value;
	}

	inline static int32_t get_offset_of_m_lstItemBuyEffect_26() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_lstItemBuyEffect_26)); }
	inline List_1_t2305279210 * get_m_lstItemBuyEffect_26() const { return ___m_lstItemBuyEffect_26; }
	inline List_1_t2305279210 ** get_address_of_m_lstItemBuyEffect_26() { return &___m_lstItemBuyEffect_26; }
	inline void set_m_lstItemBuyEffect_26(List_1_t2305279210 * value)
	{
		___m_lstItemBuyEffect_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItemBuyEffect_26), value);
	}

	inline static int32_t get_offset_of_m_aryCounterPos_27() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_aryCounterPos_27)); }
	inline Vector2U5BU5D_t1457185986* get_m_aryCounterPos_27() const { return ___m_aryCounterPos_27; }
	inline Vector2U5BU5D_t1457185986** get_address_of_m_aryCounterPos_27() { return &___m_aryCounterPos_27; }
	inline void set_m_aryCounterPos_27(Vector2U5BU5D_t1457185986* value)
	{
		___m_aryCounterPos_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCounterPos_27), value);
	}

	inline static int32_t get_offset_of_m_aryCounterTaken_28() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235, ___m_aryCounterTaken_28)); }
	inline BooleanU5BU5D_t2897418192* get_m_aryCounterTaken_28() const { return ___m_aryCounterTaken_28; }
	inline BooleanU5BU5D_t2897418192** get_address_of_m_aryCounterTaken_28() { return &___m_aryCounterTaken_28; }
	inline void set_m_aryCounterTaken_28(BooleanU5BU5D_t2897418192* value)
	{
		___m_aryCounterTaken_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCounterTaken_28), value);
	}
};

struct CItemSystem_t1283876235_StaticFields
{
public:
	// UnityEngine.Vector3 CItemSystem::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// CItemSystem CItemSystem::s_Instance
	CItemSystem_t1283876235 * ___s_Instance_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_s_Instance_3() { return static_cast<int32_t>(offsetof(CItemSystem_t1283876235_StaticFields, ___s_Instance_3)); }
	inline CItemSystem_t1283876235 * get_s_Instance_3() const { return ___s_Instance_3; }
	inline CItemSystem_t1283876235 ** get_address_of_s_Instance_3() { return &___s_Instance_3; }
	inline void set_s_Instance_3(CItemSystem_t1283876235 * value)
	{
		___s_Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITEMSYSTEM_T1283876235_H
#ifndef UIITEM_T1605098621_H
#define UIITEM_T1605098621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItem
struct  UIItem_t1605098621  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup UIItem::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_3;
	// UnityEngine.UI.Button UIItem::_btnAddPoint
	Button_t4055032469 * ____btnAddPoint_4;
	// UnityEngine.UI.Text UIItem::_txtNum
	Text_t1901882714 * ____txtNum_5;
	// UnityEngine.UI.Text UIItem::_txtName
	Text_t1901882714 * ____txtName_6;
	// UnityEngine.UI.Text UIItem::_txtDescValue
	Text_t1901882714 * ____txtDescValue_7;
	// UnityEngine.UI.Text UIItem::_txtDesc
	Text_t1901882714 * ____txtDesc_8;
	// UnityEngine.UI.Text UIItem::_txtPrice
	Text_t1901882714 * ____txtPrice_9;
	// UnityEngine.UI.Text UIItem::_txtNextLevelDesc
	Text_t1901882714 * ____txtNextLevelDesc_10;
	// UnityEngine.UI.Image UIItem::_imgMain
	Image_t2670269651 * ____imgMain_11;
	// UnityEngine.GameObject UIItem::_tips
	GameObject_t1113636619 * ____tips_12;
	// UnityEngine.UI.Image UIItem::_imgLevel
	Image_t2670269651 * ____imgLevel_13;
	// UnityEngine.UI.Button UIItem::_btnQiPao
	Button_t4055032469 * ____btnQiPao_14;
	// UnityEngine.UI.Button UIItem::_btnMain
	Button_t4055032469 * ____btnMain_15;
	// System.Int32 UIItem::m_nShortcuteKey
	int32_t ___m_nShortcuteKey_16;
	// CItemSystem/sItemConfig UIItem::m_Config
	sItemConfig_t2971202336  ___m_Config_17;
	// CFrameAnimationEffect UIItem::_effectCanLevelUp
	CFrameAnimationEffect_t443605508 * ____effectCanLevelUp_18;
	// System.Int32 UIItem::m_nCurLevel
	int32_t ___m_nCurLevel_19;
	// System.Boolean UIItem::m_bVisible
	bool ___m_bVisible_20;
	// System.Int32 UIItem::m_nCurPosIndex
	int32_t ___m_nCurPosIndex_22;
	// System.Int32 UIItem::m_nTaleFadeStatus
	int32_t ___m_nTaleFadeStatus_24;
	// System.Single UIItem::m_fTaleFadeAlpha
	float ___m_fTaleFadeAlpha_25;
	// System.Single UIItem::m_fTaleFadeTimeElapse
	float ___m_fTaleFadeTimeElapse_26;
	// System.Boolean UIItem::m_bFaded
	bool ___m_bFaded_27;

public:
	inline static int32_t get_offset_of__canvasGroup_3() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____canvasGroup_3)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_3() const { return ____canvasGroup_3; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_3() { return &____canvasGroup_3; }
	inline void set__canvasGroup_3(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_3), value);
	}

	inline static int32_t get_offset_of__btnAddPoint_4() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____btnAddPoint_4)); }
	inline Button_t4055032469 * get__btnAddPoint_4() const { return ____btnAddPoint_4; }
	inline Button_t4055032469 ** get_address_of__btnAddPoint_4() { return &____btnAddPoint_4; }
	inline void set__btnAddPoint_4(Button_t4055032469 * value)
	{
		____btnAddPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&____btnAddPoint_4), value);
	}

	inline static int32_t get_offset_of__txtNum_5() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____txtNum_5)); }
	inline Text_t1901882714 * get__txtNum_5() const { return ____txtNum_5; }
	inline Text_t1901882714 ** get_address_of__txtNum_5() { return &____txtNum_5; }
	inline void set__txtNum_5(Text_t1901882714 * value)
	{
		____txtNum_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_5), value);
	}

	inline static int32_t get_offset_of__txtName_6() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____txtName_6)); }
	inline Text_t1901882714 * get__txtName_6() const { return ____txtName_6; }
	inline Text_t1901882714 ** get_address_of__txtName_6() { return &____txtName_6; }
	inline void set__txtName_6(Text_t1901882714 * value)
	{
		____txtName_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_6), value);
	}

	inline static int32_t get_offset_of__txtDescValue_7() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____txtDescValue_7)); }
	inline Text_t1901882714 * get__txtDescValue_7() const { return ____txtDescValue_7; }
	inline Text_t1901882714 ** get_address_of__txtDescValue_7() { return &____txtDescValue_7; }
	inline void set__txtDescValue_7(Text_t1901882714 * value)
	{
		____txtDescValue_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtDescValue_7), value);
	}

	inline static int32_t get_offset_of__txtDesc_8() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____txtDesc_8)); }
	inline Text_t1901882714 * get__txtDesc_8() const { return ____txtDesc_8; }
	inline Text_t1901882714 ** get_address_of__txtDesc_8() { return &____txtDesc_8; }
	inline void set__txtDesc_8(Text_t1901882714 * value)
	{
		____txtDesc_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtDesc_8), value);
	}

	inline static int32_t get_offset_of__txtPrice_9() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____txtPrice_9)); }
	inline Text_t1901882714 * get__txtPrice_9() const { return ____txtPrice_9; }
	inline Text_t1901882714 ** get_address_of__txtPrice_9() { return &____txtPrice_9; }
	inline void set__txtPrice_9(Text_t1901882714 * value)
	{
		____txtPrice_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice_9), value);
	}

	inline static int32_t get_offset_of__txtNextLevelDesc_10() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____txtNextLevelDesc_10)); }
	inline Text_t1901882714 * get__txtNextLevelDesc_10() const { return ____txtNextLevelDesc_10; }
	inline Text_t1901882714 ** get_address_of__txtNextLevelDesc_10() { return &____txtNextLevelDesc_10; }
	inline void set__txtNextLevelDesc_10(Text_t1901882714 * value)
	{
		____txtNextLevelDesc_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtNextLevelDesc_10), value);
	}

	inline static int32_t get_offset_of__imgMain_11() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____imgMain_11)); }
	inline Image_t2670269651 * get__imgMain_11() const { return ____imgMain_11; }
	inline Image_t2670269651 ** get_address_of__imgMain_11() { return &____imgMain_11; }
	inline void set__imgMain_11(Image_t2670269651 * value)
	{
		____imgMain_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_11), value);
	}

	inline static int32_t get_offset_of__tips_12() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____tips_12)); }
	inline GameObject_t1113636619 * get__tips_12() const { return ____tips_12; }
	inline GameObject_t1113636619 ** get_address_of__tips_12() { return &____tips_12; }
	inline void set__tips_12(GameObject_t1113636619 * value)
	{
		____tips_12 = value;
		Il2CppCodeGenWriteBarrier((&____tips_12), value);
	}

	inline static int32_t get_offset_of__imgLevel_13() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____imgLevel_13)); }
	inline Image_t2670269651 * get__imgLevel_13() const { return ____imgLevel_13; }
	inline Image_t2670269651 ** get_address_of__imgLevel_13() { return &____imgLevel_13; }
	inline void set__imgLevel_13(Image_t2670269651 * value)
	{
		____imgLevel_13 = value;
		Il2CppCodeGenWriteBarrier((&____imgLevel_13), value);
	}

	inline static int32_t get_offset_of__btnQiPao_14() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____btnQiPao_14)); }
	inline Button_t4055032469 * get__btnQiPao_14() const { return ____btnQiPao_14; }
	inline Button_t4055032469 ** get_address_of__btnQiPao_14() { return &____btnQiPao_14; }
	inline void set__btnQiPao_14(Button_t4055032469 * value)
	{
		____btnQiPao_14 = value;
		Il2CppCodeGenWriteBarrier((&____btnQiPao_14), value);
	}

	inline static int32_t get_offset_of__btnMain_15() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____btnMain_15)); }
	inline Button_t4055032469 * get__btnMain_15() const { return ____btnMain_15; }
	inline Button_t4055032469 ** get_address_of__btnMain_15() { return &____btnMain_15; }
	inline void set__btnMain_15(Button_t4055032469 * value)
	{
		____btnMain_15 = value;
		Il2CppCodeGenWriteBarrier((&____btnMain_15), value);
	}

	inline static int32_t get_offset_of_m_nShortcuteKey_16() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_nShortcuteKey_16)); }
	inline int32_t get_m_nShortcuteKey_16() const { return ___m_nShortcuteKey_16; }
	inline int32_t* get_address_of_m_nShortcuteKey_16() { return &___m_nShortcuteKey_16; }
	inline void set_m_nShortcuteKey_16(int32_t value)
	{
		___m_nShortcuteKey_16 = value;
	}

	inline static int32_t get_offset_of_m_Config_17() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_Config_17)); }
	inline sItemConfig_t2971202336  get_m_Config_17() const { return ___m_Config_17; }
	inline sItemConfig_t2971202336 * get_address_of_m_Config_17() { return &___m_Config_17; }
	inline void set_m_Config_17(sItemConfig_t2971202336  value)
	{
		___m_Config_17 = value;
	}

	inline static int32_t get_offset_of__effectCanLevelUp_18() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ____effectCanLevelUp_18)); }
	inline CFrameAnimationEffect_t443605508 * get__effectCanLevelUp_18() const { return ____effectCanLevelUp_18; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectCanLevelUp_18() { return &____effectCanLevelUp_18; }
	inline void set__effectCanLevelUp_18(CFrameAnimationEffect_t443605508 * value)
	{
		____effectCanLevelUp_18 = value;
		Il2CppCodeGenWriteBarrier((&____effectCanLevelUp_18), value);
	}

	inline static int32_t get_offset_of_m_nCurLevel_19() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_nCurLevel_19)); }
	inline int32_t get_m_nCurLevel_19() const { return ___m_nCurLevel_19; }
	inline int32_t* get_address_of_m_nCurLevel_19() { return &___m_nCurLevel_19; }
	inline void set_m_nCurLevel_19(int32_t value)
	{
		___m_nCurLevel_19 = value;
	}

	inline static int32_t get_offset_of_m_bVisible_20() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_bVisible_20)); }
	inline bool get_m_bVisible_20() const { return ___m_bVisible_20; }
	inline bool* get_address_of_m_bVisible_20() { return &___m_bVisible_20; }
	inline void set_m_bVisible_20(bool value)
	{
		___m_bVisible_20 = value;
	}

	inline static int32_t get_offset_of_m_nCurPosIndex_22() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_nCurPosIndex_22)); }
	inline int32_t get_m_nCurPosIndex_22() const { return ___m_nCurPosIndex_22; }
	inline int32_t* get_address_of_m_nCurPosIndex_22() { return &___m_nCurPosIndex_22; }
	inline void set_m_nCurPosIndex_22(int32_t value)
	{
		___m_nCurPosIndex_22 = value;
	}

	inline static int32_t get_offset_of_m_nTaleFadeStatus_24() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_nTaleFadeStatus_24)); }
	inline int32_t get_m_nTaleFadeStatus_24() const { return ___m_nTaleFadeStatus_24; }
	inline int32_t* get_address_of_m_nTaleFadeStatus_24() { return &___m_nTaleFadeStatus_24; }
	inline void set_m_nTaleFadeStatus_24(int32_t value)
	{
		___m_nTaleFadeStatus_24 = value;
	}

	inline static int32_t get_offset_of_m_fTaleFadeAlpha_25() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_fTaleFadeAlpha_25)); }
	inline float get_m_fTaleFadeAlpha_25() const { return ___m_fTaleFadeAlpha_25; }
	inline float* get_address_of_m_fTaleFadeAlpha_25() { return &___m_fTaleFadeAlpha_25; }
	inline void set_m_fTaleFadeAlpha_25(float value)
	{
		___m_fTaleFadeAlpha_25 = value;
	}

	inline static int32_t get_offset_of_m_fTaleFadeTimeElapse_26() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_fTaleFadeTimeElapse_26)); }
	inline float get_m_fTaleFadeTimeElapse_26() const { return ___m_fTaleFadeTimeElapse_26; }
	inline float* get_address_of_m_fTaleFadeTimeElapse_26() { return &___m_fTaleFadeTimeElapse_26; }
	inline void set_m_fTaleFadeTimeElapse_26(float value)
	{
		___m_fTaleFadeTimeElapse_26 = value;
	}

	inline static int32_t get_offset_of_m_bFaded_27() { return static_cast<int32_t>(offsetof(UIItem_t1605098621, ___m_bFaded_27)); }
	inline bool get_m_bFaded_27() const { return ___m_bFaded_27; }
	inline bool* get_address_of_m_bFaded_27() { return &___m_bFaded_27; }
	inline void set_m_bFaded_27(bool value)
	{
		___m_bFaded_27 = value;
	}
};

struct UIItem_t1605098621_StaticFields
{
public:
	// UIItem UIItem::s_Instance
	UIItem_t1605098621 * ___s_Instance_2;
	// UnityEngine.Vector2 UIItem::vec2TempPos
	Vector2_t2156229523  ___vec2TempPos_23;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(UIItem_t1605098621_StaticFields, ___s_Instance_2)); }
	inline UIItem_t1605098621 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline UIItem_t1605098621 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(UIItem_t1605098621 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vec2TempPos_23() { return static_cast<int32_t>(offsetof(UIItem_t1605098621_StaticFields, ___vec2TempPos_23)); }
	inline Vector2_t2156229523  get_vec2TempPos_23() const { return ___vec2TempPos_23; }
	inline Vector2_t2156229523 * get_address_of_vec2TempPos_23() { return &___vec2TempPos_23; }
	inline void set_vec2TempPos_23(Vector2_t2156229523  value)
	{
		___vec2TempPos_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEM_T1605098621_H
#ifndef CJIESUANMANAGER_T2490531887_H
#define CJIESUANMANAGER_T2490531887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJieSuanManager
struct  CJieSuanManager_t2490531887  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CJieSuanManager::_panelJieSuan
	GameObject_t1113636619 * ____panelJieSuan_3;
	// UnityEngine.GameObject CJieSuanManager::_panelGeRenJieSuan
	GameObject_t1113636619 * ____panelGeRenJieSuan_4;
	// UnityEngine.Vector2[] CJieSuanManager::m_aryCounterPos
	Vector2U5BU5D_t1457185986* ___m_aryCounterPos_5;
	// System.Int32 CJieSuanManager::m_nMoveStatus
	int32_t ___m_nMoveStatus_6;
	// System.Int32 CJieSuanManager::m_nQuanChangZuiJiaStatus
	int32_t ___m_nQuanChangZuiJiaStatus_7;
	// System.Single CJieSuanManager::m_fCounterMoveSpeed
	float ___m_fCounterMoveSpeed_8;
	// System.Single CJieSuanManager::m_fZuiJiaKuangScaleSpeed
	float ___m_fZuiJiaKuangScaleSpeed_9;
	// CJieSuanCounter[] CJieSuanManager::m_aryJiesuanCounter
	CJieSuanCounterU5BU5D_t794168762* ___m_aryJiesuanCounter_10;
	// UnityEngine.UI.Image CJieSuanManager::_imgZuiJiaKuang
	Image_t2670269651 * ____imgZuiJiaKuang_11;
	// UnityEngine.Vector3 CJieSuanManager::m_vecDestPos
	Vector3_t3722313464  ___m_vecDestPos_12;
	// UnityEngine.GameObject CJieSuanManager::_goCounterContainer
	GameObject_t1113636619 * ____goCounterContainer_18;
	// CFrameAnimationEffect CJieSuanManager::_effectQuanChangZuiJia_QianZou
	CFrameAnimationEffect_t443605508 * ____effectQuanChangZuiJia_QianZou_19;
	// CFrameAnimationEffect CJieSuanManager::_effectQuanChangZuiJia_ChiXu
	CFrameAnimationEffect_t443605508 * ____effectQuanChangZuiJia_ChiXu_20;
	// System.Single CJieSuanManager::m_fZhengTiJieSuanShowTime
	float ___m_fZhengTiJieSuanShowTime_21;
	// System.Single CJieSuanManager::m_fZhengTiJieSuanTimeElapse
	float ___m_fZhengTiJieSuanTimeElapse_22;
	// System.Single[] CJieSuanManager::m_aryScalingSpeed
	SingleU5BU5D_t1444911251* ___m_aryScalingSpeed_23;
	// System.Single[] CJieSuanManager::m_aryDestValue
	SingleU5BU5D_t1444911251* ___m_aryDestValue_24;
	// System.Boolean CJieSuanManager::m_bGenRenJieSuan
	bool ___m_bGenRenJieSuan_25;
	// System.Boolean CJieSuanManager::m_bZhengTiJieSuan
	bool ___m_bZhengTiJieSuan_26;
	// System.Boolean CJieSuanManager::m_bShowingZhengTiJieSuan
	bool ___m_bShowingZhengTiJieSuan_27;

public:
	inline static int32_t get_offset_of__panelJieSuan_3() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ____panelJieSuan_3)); }
	inline GameObject_t1113636619 * get__panelJieSuan_3() const { return ____panelJieSuan_3; }
	inline GameObject_t1113636619 ** get_address_of__panelJieSuan_3() { return &____panelJieSuan_3; }
	inline void set__panelJieSuan_3(GameObject_t1113636619 * value)
	{
		____panelJieSuan_3 = value;
		Il2CppCodeGenWriteBarrier((&____panelJieSuan_3), value);
	}

	inline static int32_t get_offset_of__panelGeRenJieSuan_4() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ____panelGeRenJieSuan_4)); }
	inline GameObject_t1113636619 * get__panelGeRenJieSuan_4() const { return ____panelGeRenJieSuan_4; }
	inline GameObject_t1113636619 ** get_address_of__panelGeRenJieSuan_4() { return &____panelGeRenJieSuan_4; }
	inline void set__panelGeRenJieSuan_4(GameObject_t1113636619 * value)
	{
		____panelGeRenJieSuan_4 = value;
		Il2CppCodeGenWriteBarrier((&____panelGeRenJieSuan_4), value);
	}

	inline static int32_t get_offset_of_m_aryCounterPos_5() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_aryCounterPos_5)); }
	inline Vector2U5BU5D_t1457185986* get_m_aryCounterPos_5() const { return ___m_aryCounterPos_5; }
	inline Vector2U5BU5D_t1457185986** get_address_of_m_aryCounterPos_5() { return &___m_aryCounterPos_5; }
	inline void set_m_aryCounterPos_5(Vector2U5BU5D_t1457185986* value)
	{
		___m_aryCounterPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCounterPos_5), value);
	}

	inline static int32_t get_offset_of_m_nMoveStatus_6() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_nMoveStatus_6)); }
	inline int32_t get_m_nMoveStatus_6() const { return ___m_nMoveStatus_6; }
	inline int32_t* get_address_of_m_nMoveStatus_6() { return &___m_nMoveStatus_6; }
	inline void set_m_nMoveStatus_6(int32_t value)
	{
		___m_nMoveStatus_6 = value;
	}

	inline static int32_t get_offset_of_m_nQuanChangZuiJiaStatus_7() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_nQuanChangZuiJiaStatus_7)); }
	inline int32_t get_m_nQuanChangZuiJiaStatus_7() const { return ___m_nQuanChangZuiJiaStatus_7; }
	inline int32_t* get_address_of_m_nQuanChangZuiJiaStatus_7() { return &___m_nQuanChangZuiJiaStatus_7; }
	inline void set_m_nQuanChangZuiJiaStatus_7(int32_t value)
	{
		___m_nQuanChangZuiJiaStatus_7 = value;
	}

	inline static int32_t get_offset_of_m_fCounterMoveSpeed_8() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_fCounterMoveSpeed_8)); }
	inline float get_m_fCounterMoveSpeed_8() const { return ___m_fCounterMoveSpeed_8; }
	inline float* get_address_of_m_fCounterMoveSpeed_8() { return &___m_fCounterMoveSpeed_8; }
	inline void set_m_fCounterMoveSpeed_8(float value)
	{
		___m_fCounterMoveSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_fZuiJiaKuangScaleSpeed_9() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_fZuiJiaKuangScaleSpeed_9)); }
	inline float get_m_fZuiJiaKuangScaleSpeed_9() const { return ___m_fZuiJiaKuangScaleSpeed_9; }
	inline float* get_address_of_m_fZuiJiaKuangScaleSpeed_9() { return &___m_fZuiJiaKuangScaleSpeed_9; }
	inline void set_m_fZuiJiaKuangScaleSpeed_9(float value)
	{
		___m_fZuiJiaKuangScaleSpeed_9 = value;
	}

	inline static int32_t get_offset_of_m_aryJiesuanCounter_10() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_aryJiesuanCounter_10)); }
	inline CJieSuanCounterU5BU5D_t794168762* get_m_aryJiesuanCounter_10() const { return ___m_aryJiesuanCounter_10; }
	inline CJieSuanCounterU5BU5D_t794168762** get_address_of_m_aryJiesuanCounter_10() { return &___m_aryJiesuanCounter_10; }
	inline void set_m_aryJiesuanCounter_10(CJieSuanCounterU5BU5D_t794168762* value)
	{
		___m_aryJiesuanCounter_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryJiesuanCounter_10), value);
	}

	inline static int32_t get_offset_of__imgZuiJiaKuang_11() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ____imgZuiJiaKuang_11)); }
	inline Image_t2670269651 * get__imgZuiJiaKuang_11() const { return ____imgZuiJiaKuang_11; }
	inline Image_t2670269651 ** get_address_of__imgZuiJiaKuang_11() { return &____imgZuiJiaKuang_11; }
	inline void set__imgZuiJiaKuang_11(Image_t2670269651 * value)
	{
		____imgZuiJiaKuang_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgZuiJiaKuang_11), value);
	}

	inline static int32_t get_offset_of_m_vecDestPos_12() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_vecDestPos_12)); }
	inline Vector3_t3722313464  get_m_vecDestPos_12() const { return ___m_vecDestPos_12; }
	inline Vector3_t3722313464 * get_address_of_m_vecDestPos_12() { return &___m_vecDestPos_12; }
	inline void set_m_vecDestPos_12(Vector3_t3722313464  value)
	{
		___m_vecDestPos_12 = value;
	}

	inline static int32_t get_offset_of__goCounterContainer_18() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ____goCounterContainer_18)); }
	inline GameObject_t1113636619 * get__goCounterContainer_18() const { return ____goCounterContainer_18; }
	inline GameObject_t1113636619 ** get_address_of__goCounterContainer_18() { return &____goCounterContainer_18; }
	inline void set__goCounterContainer_18(GameObject_t1113636619 * value)
	{
		____goCounterContainer_18 = value;
		Il2CppCodeGenWriteBarrier((&____goCounterContainer_18), value);
	}

	inline static int32_t get_offset_of__effectQuanChangZuiJia_QianZou_19() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ____effectQuanChangZuiJia_QianZou_19)); }
	inline CFrameAnimationEffect_t443605508 * get__effectQuanChangZuiJia_QianZou_19() const { return ____effectQuanChangZuiJia_QianZou_19; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectQuanChangZuiJia_QianZou_19() { return &____effectQuanChangZuiJia_QianZou_19; }
	inline void set__effectQuanChangZuiJia_QianZou_19(CFrameAnimationEffect_t443605508 * value)
	{
		____effectQuanChangZuiJia_QianZou_19 = value;
		Il2CppCodeGenWriteBarrier((&____effectQuanChangZuiJia_QianZou_19), value);
	}

	inline static int32_t get_offset_of__effectQuanChangZuiJia_ChiXu_20() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ____effectQuanChangZuiJia_ChiXu_20)); }
	inline CFrameAnimationEffect_t443605508 * get__effectQuanChangZuiJia_ChiXu_20() const { return ____effectQuanChangZuiJia_ChiXu_20; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectQuanChangZuiJia_ChiXu_20() { return &____effectQuanChangZuiJia_ChiXu_20; }
	inline void set__effectQuanChangZuiJia_ChiXu_20(CFrameAnimationEffect_t443605508 * value)
	{
		____effectQuanChangZuiJia_ChiXu_20 = value;
		Il2CppCodeGenWriteBarrier((&____effectQuanChangZuiJia_ChiXu_20), value);
	}

	inline static int32_t get_offset_of_m_fZhengTiJieSuanShowTime_21() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_fZhengTiJieSuanShowTime_21)); }
	inline float get_m_fZhengTiJieSuanShowTime_21() const { return ___m_fZhengTiJieSuanShowTime_21; }
	inline float* get_address_of_m_fZhengTiJieSuanShowTime_21() { return &___m_fZhengTiJieSuanShowTime_21; }
	inline void set_m_fZhengTiJieSuanShowTime_21(float value)
	{
		___m_fZhengTiJieSuanShowTime_21 = value;
	}

	inline static int32_t get_offset_of_m_fZhengTiJieSuanTimeElapse_22() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_fZhengTiJieSuanTimeElapse_22)); }
	inline float get_m_fZhengTiJieSuanTimeElapse_22() const { return ___m_fZhengTiJieSuanTimeElapse_22; }
	inline float* get_address_of_m_fZhengTiJieSuanTimeElapse_22() { return &___m_fZhengTiJieSuanTimeElapse_22; }
	inline void set_m_fZhengTiJieSuanTimeElapse_22(float value)
	{
		___m_fZhengTiJieSuanTimeElapse_22 = value;
	}

	inline static int32_t get_offset_of_m_aryScalingSpeed_23() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_aryScalingSpeed_23)); }
	inline SingleU5BU5D_t1444911251* get_m_aryScalingSpeed_23() const { return ___m_aryScalingSpeed_23; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryScalingSpeed_23() { return &___m_aryScalingSpeed_23; }
	inline void set_m_aryScalingSpeed_23(SingleU5BU5D_t1444911251* value)
	{
		___m_aryScalingSpeed_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryScalingSpeed_23), value);
	}

	inline static int32_t get_offset_of_m_aryDestValue_24() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_aryDestValue_24)); }
	inline SingleU5BU5D_t1444911251* get_m_aryDestValue_24() const { return ___m_aryDestValue_24; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryDestValue_24() { return &___m_aryDestValue_24; }
	inline void set_m_aryDestValue_24(SingleU5BU5D_t1444911251* value)
	{
		___m_aryDestValue_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDestValue_24), value);
	}

	inline static int32_t get_offset_of_m_bGenRenJieSuan_25() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_bGenRenJieSuan_25)); }
	inline bool get_m_bGenRenJieSuan_25() const { return ___m_bGenRenJieSuan_25; }
	inline bool* get_address_of_m_bGenRenJieSuan_25() { return &___m_bGenRenJieSuan_25; }
	inline void set_m_bGenRenJieSuan_25(bool value)
	{
		___m_bGenRenJieSuan_25 = value;
	}

	inline static int32_t get_offset_of_m_bZhengTiJieSuan_26() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_bZhengTiJieSuan_26)); }
	inline bool get_m_bZhengTiJieSuan_26() const { return ___m_bZhengTiJieSuan_26; }
	inline bool* get_address_of_m_bZhengTiJieSuan_26() { return &___m_bZhengTiJieSuan_26; }
	inline void set_m_bZhengTiJieSuan_26(bool value)
	{
		___m_bZhengTiJieSuan_26 = value;
	}

	inline static int32_t get_offset_of_m_bShowingZhengTiJieSuan_27() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887, ___m_bShowingZhengTiJieSuan_27)); }
	inline bool get_m_bShowingZhengTiJieSuan_27() const { return ___m_bShowingZhengTiJieSuan_27; }
	inline bool* get_address_of_m_bShowingZhengTiJieSuan_27() { return &___m_bShowingZhengTiJieSuan_27; }
	inline void set_m_bShowingZhengTiJieSuan_27(bool value)
	{
		___m_bShowingZhengTiJieSuan_27 = value;
	}
};

struct CJieSuanManager_t2490531887_StaticFields
{
public:
	// CJieSuanManager CJieSuanManager::s_Instance
	CJieSuanManager_t2490531887 * ___s_Instance_2;
	// UnityEngine.Vector2 CJieSuanManager::vecTempDir
	Vector2_t2156229523  ___vecTempDir_13;
	// UnityEngine.Vector3 CJieSuanManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_14;
	// UnityEngine.Vector3 CJieSuanManager::vecTempScale
	Vector3_t3722313464  ___vecTempScale_15;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887_StaticFields, ___s_Instance_2)); }
	inline CJieSuanManager_t2490531887 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CJieSuanManager_t2490531887 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CJieSuanManager_t2490531887 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempDir_13() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887_StaticFields, ___vecTempDir_13)); }
	inline Vector2_t2156229523  get_vecTempDir_13() const { return ___vecTempDir_13; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_13() { return &___vecTempDir_13; }
	inline void set_vecTempDir_13(Vector2_t2156229523  value)
	{
		___vecTempDir_13 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_14() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887_StaticFields, ___vecTempPos_14)); }
	inline Vector3_t3722313464  get_vecTempPos_14() const { return ___vecTempPos_14; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_14() { return &___vecTempPos_14; }
	inline void set_vecTempPos_14(Vector3_t3722313464  value)
	{
		___vecTempPos_14 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_15() { return static_cast<int32_t>(offsetof(CJieSuanManager_t2490531887_StaticFields, ___vecTempScale_15)); }
	inline Vector3_t3722313464  get_vecTempScale_15() const { return ___vecTempScale_15; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_15() { return &___vecTempScale_15; }
	inline void set_vecTempScale_15(Vector3_t3722313464  value)
	{
		___vecTempScale_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJIESUANMANAGER_T2490531887_H
#ifndef CLIGHT_T1535733186_H
#define CLIGHT_T1535733186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLight
struct  CLight_t1535733186  : public MonoBehaviour_t3962482529
{
public:
	// CLightSystem/eLightType CLight::m_eType
	int32_t ___m_eType_2;

public:
	inline static int32_t get_offset_of_m_eType_2() { return static_cast<int32_t>(offsetof(CLight_t1535733186, ___m_eType_2)); }
	inline int32_t get_m_eType_2() const { return ___m_eType_2; }
	inline int32_t* get_address_of_m_eType_2() { return &___m_eType_2; }
	inline void set_m_eType_2(int32_t value)
	{
		___m_eType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIGHT_T1535733186_H
#ifndef CCOSMOSEFFECT_T495978253_H
#define CCOSMOSEFFECT_T495978253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCosmosEffect
struct  CCosmosEffect_t495978253  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup CCosmosEffect::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_4;
	// UnityEngine.UI.Image CCosmosEffect::_imgMain
	Image_t2670269651 * ____imgMain_5;
	// UnityEngine.SpriteRenderer CCosmosEffect::_sprMain
	SpriteRenderer_t3235626157 * ____sprMain_6;
	// System.Boolean CCosmosEffect::m_bUI
	bool ___m_bUI_7;
	// UnityEngine.SpriteRenderer CCosmosEffect::_srRotationPic
	SpriteRenderer_t3235626157 * ____srRotationPic_8;
	// UnityEngine.UI.Image CCosmosEffect::_imgRotationPic
	Image_t2670269651 * ____imgRotationPic_9;
	// System.Boolean CCosmosEffect::m_bRotating
	bool ___m_bRotating_10;
	// System.Single CCosmosEffect::m_fRotatingSpeed
	float ___m_fRotatingSpeed_11;
	// System.Single CCosmosEffect::m_fRotatingAngle
	float ___m_fRotatingAngle_12;
	// System.Boolean CCosmosEffect::m_bAlphaChange
	bool ___m_bAlphaChange_13;
	// System.Single CCosmosEffect::m_fMaxAlpha
	float ___m_fMaxAlpha_14;
	// System.Single CCosmosEffect::m_fMinAlpha
	float ___m_fMinAlpha_15;
	// System.Single CCosmosEffect::m_fAlphaChangeSpeed
	float ___m_fAlphaChangeSpeed_16;

public:
	inline static int32_t get_offset_of__canvasGroup_4() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____canvasGroup_4)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_4() const { return ____canvasGroup_4; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_4() { return &____canvasGroup_4; }
	inline void set__canvasGroup_4(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_4), value);
	}

	inline static int32_t get_offset_of__imgMain_5() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____imgMain_5)); }
	inline Image_t2670269651 * get__imgMain_5() const { return ____imgMain_5; }
	inline Image_t2670269651 ** get_address_of__imgMain_5() { return &____imgMain_5; }
	inline void set__imgMain_5(Image_t2670269651 * value)
	{
		____imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_5), value);
	}

	inline static int32_t get_offset_of__sprMain_6() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____sprMain_6)); }
	inline SpriteRenderer_t3235626157 * get__sprMain_6() const { return ____sprMain_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprMain_6() { return &____sprMain_6; }
	inline void set__sprMain_6(SpriteRenderer_t3235626157 * value)
	{
		____sprMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprMain_6), value);
	}

	inline static int32_t get_offset_of_m_bUI_7() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bUI_7)); }
	inline bool get_m_bUI_7() const { return ___m_bUI_7; }
	inline bool* get_address_of_m_bUI_7() { return &___m_bUI_7; }
	inline void set_m_bUI_7(bool value)
	{
		___m_bUI_7 = value;
	}

	inline static int32_t get_offset_of__srRotationPic_8() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____srRotationPic_8)); }
	inline SpriteRenderer_t3235626157 * get__srRotationPic_8() const { return ____srRotationPic_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srRotationPic_8() { return &____srRotationPic_8; }
	inline void set__srRotationPic_8(SpriteRenderer_t3235626157 * value)
	{
		____srRotationPic_8 = value;
		Il2CppCodeGenWriteBarrier((&____srRotationPic_8), value);
	}

	inline static int32_t get_offset_of__imgRotationPic_9() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____imgRotationPic_9)); }
	inline Image_t2670269651 * get__imgRotationPic_9() const { return ____imgRotationPic_9; }
	inline Image_t2670269651 ** get_address_of__imgRotationPic_9() { return &____imgRotationPic_9; }
	inline void set__imgRotationPic_9(Image_t2670269651 * value)
	{
		____imgRotationPic_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgRotationPic_9), value);
	}

	inline static int32_t get_offset_of_m_bRotating_10() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bRotating_10)); }
	inline bool get_m_bRotating_10() const { return ___m_bRotating_10; }
	inline bool* get_address_of_m_bRotating_10() { return &___m_bRotating_10; }
	inline void set_m_bRotating_10(bool value)
	{
		___m_bRotating_10 = value;
	}

	inline static int32_t get_offset_of_m_fRotatingSpeed_11() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fRotatingSpeed_11)); }
	inline float get_m_fRotatingSpeed_11() const { return ___m_fRotatingSpeed_11; }
	inline float* get_address_of_m_fRotatingSpeed_11() { return &___m_fRotatingSpeed_11; }
	inline void set_m_fRotatingSpeed_11(float value)
	{
		___m_fRotatingSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_fRotatingAngle_12() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fRotatingAngle_12)); }
	inline float get_m_fRotatingAngle_12() const { return ___m_fRotatingAngle_12; }
	inline float* get_address_of_m_fRotatingAngle_12() { return &___m_fRotatingAngle_12; }
	inline void set_m_fRotatingAngle_12(float value)
	{
		___m_fRotatingAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_bAlphaChange_13() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bAlphaChange_13)); }
	inline bool get_m_bAlphaChange_13() const { return ___m_bAlphaChange_13; }
	inline bool* get_address_of_m_bAlphaChange_13() { return &___m_bAlphaChange_13; }
	inline void set_m_bAlphaChange_13(bool value)
	{
		___m_bAlphaChange_13 = value;
	}

	inline static int32_t get_offset_of_m_fMaxAlpha_14() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fMaxAlpha_14)); }
	inline float get_m_fMaxAlpha_14() const { return ___m_fMaxAlpha_14; }
	inline float* get_address_of_m_fMaxAlpha_14() { return &___m_fMaxAlpha_14; }
	inline void set_m_fMaxAlpha_14(float value)
	{
		___m_fMaxAlpha_14 = value;
	}

	inline static int32_t get_offset_of_m_fMinAlpha_15() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fMinAlpha_15)); }
	inline float get_m_fMinAlpha_15() const { return ___m_fMinAlpha_15; }
	inline float* get_address_of_m_fMinAlpha_15() { return &___m_fMinAlpha_15; }
	inline void set_m_fMinAlpha_15(float value)
	{
		___m_fMinAlpha_15 = value;
	}

	inline static int32_t get_offset_of_m_fAlphaChangeSpeed_16() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fAlphaChangeSpeed_16)); }
	inline float get_m_fAlphaChangeSpeed_16() const { return ___m_fAlphaChangeSpeed_16; }
	inline float* get_address_of_m_fAlphaChangeSpeed_16() { return &___m_fAlphaChangeSpeed_16; }
	inline void set_m_fAlphaChangeSpeed_16(float value)
	{
		___m_fAlphaChangeSpeed_16 = value;
	}
};

struct CCosmosEffect_t495978253_StaticFields
{
public:
	// UnityEngine.Vector3 CCosmosEffect::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCosmosEffect::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOSMOSEFFECT_T495978253_H
#ifndef CGUNSIGHTMANAGER_T3371991777_H
#define CGUNSIGHTMANAGER_T3371991777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGunSightManager
struct  CGunSightManager_t3371991777  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CGunSightManager::m_preGunSight
	GameObject_t1113636619 * ___m_preGunSight_3;
	// System.Collections.Generic.List`1<CCosmosGunSight> CGunSightManager::m_lstRecycledGunSight
	List_1_t807120747 * ___m_lstRecycledGunSight_4;

public:
	inline static int32_t get_offset_of_m_preGunSight_3() { return static_cast<int32_t>(offsetof(CGunSightManager_t3371991777, ___m_preGunSight_3)); }
	inline GameObject_t1113636619 * get_m_preGunSight_3() const { return ___m_preGunSight_3; }
	inline GameObject_t1113636619 ** get_address_of_m_preGunSight_3() { return &___m_preGunSight_3; }
	inline void set_m_preGunSight_3(GameObject_t1113636619 * value)
	{
		___m_preGunSight_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGunSight_3), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledGunSight_4() { return static_cast<int32_t>(offsetof(CGunSightManager_t3371991777, ___m_lstRecycledGunSight_4)); }
	inline List_1_t807120747 * get_m_lstRecycledGunSight_4() const { return ___m_lstRecycledGunSight_4; }
	inline List_1_t807120747 ** get_address_of_m_lstRecycledGunSight_4() { return &___m_lstRecycledGunSight_4; }
	inline void set_m_lstRecycledGunSight_4(List_1_t807120747 * value)
	{
		___m_lstRecycledGunSight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledGunSight_4), value);
	}
};

struct CGunSightManager_t3371991777_StaticFields
{
public:
	// CGunSightManager CGunSightManager::s_Instance
	CGunSightManager_t3371991777 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CGunSightManager_t3371991777_StaticFields, ___s_Instance_2)); }
	inline CGunSightManager_t3371991777 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CGunSightManager_t3371991777 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CGunSightManager_t3371991777 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGUNSIGHTMANAGER_T3371991777_H
#ifndef CGESTUREMANAGER_T1993322746_H
#define CGESTUREMANAGER_T1993322746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGestureManager
struct  CGestureManager_t1993322746  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CGestureManager::m_nAvailableNum
	int32_t ___m_nAvailableNum_5;
	// UnityEngine.Sprite CGestureManager::m_sprToggleButtongImg_NotOpen
	Sprite_t280657092 * ___m_sprToggleButtongImg_NotOpen_6;
	// UnityEngine.Sprite CGestureManager::m_sprToggleButtongImg_Open
	Sprite_t280657092 * ___m_sprToggleButtongImg_Open_7;
	// UnityEngine.Sprite[] CGestureManager::m_aryGestures
	SpriteU5BU5D_t2581906349* ___m_aryGestures_8;
	// UnityEngine.Sprite CGestureManager::m_sprTerminalWithTail
	Sprite_t280657092 * ___m_sprTerminalWithTail_9;
	// UnityEngine.Sprite CGestureManager::m_sprTerminalWithoutTail
	Sprite_t280657092 * ___m_sprTerminalWithoutTail_10;
	// UnityEngine.UI.Button CGestureManager::_btnToggle
	Button_t4055032469 * ____btnToggle_11;
	// UnityEngine.UI.InputField CGestureManager::_inputGestureNumOneTime
	InputField_t3762917431 * ____inputGestureNumOneTime_12;
	// UnityEngine.UI.InputField CGestureManager::_inputColdDown
	InputField_t3762917431 * ____inputColdDown_13;
	// UnityEngine.UI.InputField CGestureManager::_inputLastTime
	InputField_t3762917431 * ____inputLastTime_14;
	// UnityEngine.UI.InputField CGestureManager::_inputMaxEquippedNum
	InputField_t3762917431 * ____inputMaxEquippedNum_15;
	// UnityEngine.UI.InputField CGestureManager::_inputSmallColdDown
	InputField_t3762917431 * ____inputSmallColdDown_16;
	// UnityEngine.UI.Image CGestureManager::_imgColdDown
	Image_t2670269651 * ____imgColdDown_17;
	// UnityEngine.UI.Image CGestureManager::_imgColdDownBg
	Image_t2670269651 * ____imgColdDownBg_18;
	// UnityEngine.GameObject CGestureManager::m_panelGesturesSystem
	GameObject_t1113636619 * ___m_panelGesturesSystem_19;
	// UnityEngine.GameObject CGestureManager::m_goContainerBoughtGestures
	GameObject_t1113636619 * ___m_goContainerBoughtGestures_20;
	// UnityEngine.GameObject CGestureManager::m_goContainerSelectedGestures
	GameObject_t1113636619 * ___m_goContainerSelectedGestures_21;
	// UnityEngine.GameObject CGestureManager::m_goContainerPopo
	GameObject_t1113636619 * ___m_goContainerPopo_22;
	// UnityEngine.GameObject CGestureManager::m_preGesture
	GameObject_t1113636619 * ___m_preGesture_23;
	// UnityEngine.GameObject CGestureManager::m_preGestureNotUI
	GameObject_t1113636619 * ___m_preGestureNotUI_24;
	// UnityEngine.GameObject CGestureManager::m_preGesturePopo
	GameObject_t1113636619 * ___m_preGesturePopo_25;
	// UnityEngine.GameObject CGestureManager::m_preGesturePopoCenter
	GameObject_t1113636619 * ___m_preGesturePopoCenter_26;
	// System.Single CGestureManager::m_fBoughtGesturesInterval
	float ___m_fBoughtGesturesInterval_27;
	// System.Single CGestureManager::m_fSelectedGesturesInterval
	float ___m_fSelectedGesturesInterval_28;
	// System.Single CGestureManager::m_fCastedGesturesInterval
	float ___m_fCastedGesturesInterval_29;
	// System.Single CGestureManager::m_fBoughtGesturesScale
	float ___m_fBoughtGesturesScale_30;
	// System.Single CGestureManager::m_fSelectedGesturesScale
	float ___m_fSelectedGesturesScale_31;
	// System.Single CGestureManager::m_fCastedGesturesScale
	float ___m_fCastedGesturesScale_32;
	// System.Single CGestureManager::m_fUnitWidth
	float ___m_fUnitWidth_33;
	// System.Single CGestureManager::m_fBgOffsetY
	float ___m_fBgOffsetY_34;
	// System.Single CGestureManager::m_fWidthPerCharaqcter
	float ___m_fWidthPerCharaqcter_35;
	// System.Single CGestureManager::m_fGenstureOriginalWidth
	float ___m_fGenstureOriginalWidth_36;
	// System.Single CGestureManager::m_fLeftAndRightOffset
	float ___m_fLeftAndRightOffset_37;
	// System.Int32 CGestureManager::m_nMaxSelectedNum
	int32_t ___m_nMaxSelectedNum_38;
	// System.Single CGestureManager::m_fColdDown
	float ___m_fColdDown_39;
	// System.Single CGestureManager::m_fSmallColdDown
	float ___m_fSmallColdDown_40;
	// System.Single CGestureManager::m_fPopoLastTime
	float ___m_fPopoLastTime_41;
	// System.Single CGestureManager::m_fLastCastGestureTime
	float ___m_fLastCastGestureTime_42;
	// System.Single CGestureManager::m_fOffestX
	float ___m_fOffestX_43;
	// System.Single CGestureManager::m_fOffestY
	float ___m_fOffestY_44;
	// System.Collections.Generic.List`1<CGesture> CGestureManager::m_lstSelcted
	List_1_t3653243616 * ___m_lstSelcted_45;
	// CGestureManager/eGesturePopoDir CGestureManager::m_eDir
	int32_t ___m_eDir_46;
	// System.Collections.Generic.List`1<CGesture> CGestureManager::m_lstRecycledGestures
	List_1_t3653243616 * ___m_lstRecycledGestures_47;
	// System.Collections.Generic.List`1<CGesturePopo> CGestureManager::m_lstRecycledGesturePopos
	List_1_t3832709940 * ___m_lstRecycledGesturePopos_48;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CGestureManager::m_lstRecycledPopoCenter
	List_1_t2585711361 * ___m_lstRecycledPopoCenter_49;
	// System.Boolean CGestureManager::m_bShowing
	bool ___m_bShowing_50;
	// System.Single CGestureManager::m_fSmallColdDownTimeLeft
	float ___m_fSmallColdDownTimeLeft_51;
	// System.Single CGestureManager::m_fBigColdDownTimeLeft
	float ___m_fBigColdDownTimeLeft_52;
	// System.Collections.Generic.List`1<System.Int32> CGestureManager::s_lstCastingGestureId
	List_1_t128053199 * ___s_lstCastingGestureId_53;
	// CGesturePopo CGestureManager::_testPopo
	CGesturePopo_t2360635198 * ____testPopo_54;
	// UnityEngine.GameObject CGestureManager::m_goBoughtListLeft
	GameObject_t1113636619 * ___m_goBoughtListLeft_55;
	// UnityEngine.GameObject CGestureManager::m_goBoughtListMain
	GameObject_t1113636619 * ___m_goBoughtListMain_56;
	// System.Single CGestureManager::m_fBoughtUnitWidht
	float ___m_fBoughtUnitWidht_57;

public:
	inline static int32_t get_offset_of_m_nAvailableNum_5() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_nAvailableNum_5)); }
	inline int32_t get_m_nAvailableNum_5() const { return ___m_nAvailableNum_5; }
	inline int32_t* get_address_of_m_nAvailableNum_5() { return &___m_nAvailableNum_5; }
	inline void set_m_nAvailableNum_5(int32_t value)
	{
		___m_nAvailableNum_5 = value;
	}

	inline static int32_t get_offset_of_m_sprToggleButtongImg_NotOpen_6() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_sprToggleButtongImg_NotOpen_6)); }
	inline Sprite_t280657092 * get_m_sprToggleButtongImg_NotOpen_6() const { return ___m_sprToggleButtongImg_NotOpen_6; }
	inline Sprite_t280657092 ** get_address_of_m_sprToggleButtongImg_NotOpen_6() { return &___m_sprToggleButtongImg_NotOpen_6; }
	inline void set_m_sprToggleButtongImg_NotOpen_6(Sprite_t280657092 * value)
	{
		___m_sprToggleButtongImg_NotOpen_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprToggleButtongImg_NotOpen_6), value);
	}

	inline static int32_t get_offset_of_m_sprToggleButtongImg_Open_7() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_sprToggleButtongImg_Open_7)); }
	inline Sprite_t280657092 * get_m_sprToggleButtongImg_Open_7() const { return ___m_sprToggleButtongImg_Open_7; }
	inline Sprite_t280657092 ** get_address_of_m_sprToggleButtongImg_Open_7() { return &___m_sprToggleButtongImg_Open_7; }
	inline void set_m_sprToggleButtongImg_Open_7(Sprite_t280657092 * value)
	{
		___m_sprToggleButtongImg_Open_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprToggleButtongImg_Open_7), value);
	}

	inline static int32_t get_offset_of_m_aryGestures_8() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_aryGestures_8)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryGestures_8() const { return ___m_aryGestures_8; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryGestures_8() { return &___m_aryGestures_8; }
	inline void set_m_aryGestures_8(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryGestures_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryGestures_8), value);
	}

	inline static int32_t get_offset_of_m_sprTerminalWithTail_9() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_sprTerminalWithTail_9)); }
	inline Sprite_t280657092 * get_m_sprTerminalWithTail_9() const { return ___m_sprTerminalWithTail_9; }
	inline Sprite_t280657092 ** get_address_of_m_sprTerminalWithTail_9() { return &___m_sprTerminalWithTail_9; }
	inline void set_m_sprTerminalWithTail_9(Sprite_t280657092 * value)
	{
		___m_sprTerminalWithTail_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprTerminalWithTail_9), value);
	}

	inline static int32_t get_offset_of_m_sprTerminalWithoutTail_10() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_sprTerminalWithoutTail_10)); }
	inline Sprite_t280657092 * get_m_sprTerminalWithoutTail_10() const { return ___m_sprTerminalWithoutTail_10; }
	inline Sprite_t280657092 ** get_address_of_m_sprTerminalWithoutTail_10() { return &___m_sprTerminalWithoutTail_10; }
	inline void set_m_sprTerminalWithoutTail_10(Sprite_t280657092 * value)
	{
		___m_sprTerminalWithoutTail_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprTerminalWithoutTail_10), value);
	}

	inline static int32_t get_offset_of__btnToggle_11() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____btnToggle_11)); }
	inline Button_t4055032469 * get__btnToggle_11() const { return ____btnToggle_11; }
	inline Button_t4055032469 ** get_address_of__btnToggle_11() { return &____btnToggle_11; }
	inline void set__btnToggle_11(Button_t4055032469 * value)
	{
		____btnToggle_11 = value;
		Il2CppCodeGenWriteBarrier((&____btnToggle_11), value);
	}

	inline static int32_t get_offset_of__inputGestureNumOneTime_12() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____inputGestureNumOneTime_12)); }
	inline InputField_t3762917431 * get__inputGestureNumOneTime_12() const { return ____inputGestureNumOneTime_12; }
	inline InputField_t3762917431 ** get_address_of__inputGestureNumOneTime_12() { return &____inputGestureNumOneTime_12; }
	inline void set__inputGestureNumOneTime_12(InputField_t3762917431 * value)
	{
		____inputGestureNumOneTime_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputGestureNumOneTime_12), value);
	}

	inline static int32_t get_offset_of__inputColdDown_13() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____inputColdDown_13)); }
	inline InputField_t3762917431 * get__inputColdDown_13() const { return ____inputColdDown_13; }
	inline InputField_t3762917431 ** get_address_of__inputColdDown_13() { return &____inputColdDown_13; }
	inline void set__inputColdDown_13(InputField_t3762917431 * value)
	{
		____inputColdDown_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputColdDown_13), value);
	}

	inline static int32_t get_offset_of__inputLastTime_14() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____inputLastTime_14)); }
	inline InputField_t3762917431 * get__inputLastTime_14() const { return ____inputLastTime_14; }
	inline InputField_t3762917431 ** get_address_of__inputLastTime_14() { return &____inputLastTime_14; }
	inline void set__inputLastTime_14(InputField_t3762917431 * value)
	{
		____inputLastTime_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputLastTime_14), value);
	}

	inline static int32_t get_offset_of__inputMaxEquippedNum_15() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____inputMaxEquippedNum_15)); }
	inline InputField_t3762917431 * get__inputMaxEquippedNum_15() const { return ____inputMaxEquippedNum_15; }
	inline InputField_t3762917431 ** get_address_of__inputMaxEquippedNum_15() { return &____inputMaxEquippedNum_15; }
	inline void set__inputMaxEquippedNum_15(InputField_t3762917431 * value)
	{
		____inputMaxEquippedNum_15 = value;
		Il2CppCodeGenWriteBarrier((&____inputMaxEquippedNum_15), value);
	}

	inline static int32_t get_offset_of__inputSmallColdDown_16() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____inputSmallColdDown_16)); }
	inline InputField_t3762917431 * get__inputSmallColdDown_16() const { return ____inputSmallColdDown_16; }
	inline InputField_t3762917431 ** get_address_of__inputSmallColdDown_16() { return &____inputSmallColdDown_16; }
	inline void set__inputSmallColdDown_16(InputField_t3762917431 * value)
	{
		____inputSmallColdDown_16 = value;
		Il2CppCodeGenWriteBarrier((&____inputSmallColdDown_16), value);
	}

	inline static int32_t get_offset_of__imgColdDown_17() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____imgColdDown_17)); }
	inline Image_t2670269651 * get__imgColdDown_17() const { return ____imgColdDown_17; }
	inline Image_t2670269651 ** get_address_of__imgColdDown_17() { return &____imgColdDown_17; }
	inline void set__imgColdDown_17(Image_t2670269651 * value)
	{
		____imgColdDown_17 = value;
		Il2CppCodeGenWriteBarrier((&____imgColdDown_17), value);
	}

	inline static int32_t get_offset_of__imgColdDownBg_18() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____imgColdDownBg_18)); }
	inline Image_t2670269651 * get__imgColdDownBg_18() const { return ____imgColdDownBg_18; }
	inline Image_t2670269651 ** get_address_of__imgColdDownBg_18() { return &____imgColdDownBg_18; }
	inline void set__imgColdDownBg_18(Image_t2670269651 * value)
	{
		____imgColdDownBg_18 = value;
		Il2CppCodeGenWriteBarrier((&____imgColdDownBg_18), value);
	}

	inline static int32_t get_offset_of_m_panelGesturesSystem_19() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_panelGesturesSystem_19)); }
	inline GameObject_t1113636619 * get_m_panelGesturesSystem_19() const { return ___m_panelGesturesSystem_19; }
	inline GameObject_t1113636619 ** get_address_of_m_panelGesturesSystem_19() { return &___m_panelGesturesSystem_19; }
	inline void set_m_panelGesturesSystem_19(GameObject_t1113636619 * value)
	{
		___m_panelGesturesSystem_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_panelGesturesSystem_19), value);
	}

	inline static int32_t get_offset_of_m_goContainerBoughtGestures_20() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_goContainerBoughtGestures_20)); }
	inline GameObject_t1113636619 * get_m_goContainerBoughtGestures_20() const { return ___m_goContainerBoughtGestures_20; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainerBoughtGestures_20() { return &___m_goContainerBoughtGestures_20; }
	inline void set_m_goContainerBoughtGestures_20(GameObject_t1113636619 * value)
	{
		___m_goContainerBoughtGestures_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerBoughtGestures_20), value);
	}

	inline static int32_t get_offset_of_m_goContainerSelectedGestures_21() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_goContainerSelectedGestures_21)); }
	inline GameObject_t1113636619 * get_m_goContainerSelectedGestures_21() const { return ___m_goContainerSelectedGestures_21; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainerSelectedGestures_21() { return &___m_goContainerSelectedGestures_21; }
	inline void set_m_goContainerSelectedGestures_21(GameObject_t1113636619 * value)
	{
		___m_goContainerSelectedGestures_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerSelectedGestures_21), value);
	}

	inline static int32_t get_offset_of_m_goContainerPopo_22() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_goContainerPopo_22)); }
	inline GameObject_t1113636619 * get_m_goContainerPopo_22() const { return ___m_goContainerPopo_22; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainerPopo_22() { return &___m_goContainerPopo_22; }
	inline void set_m_goContainerPopo_22(GameObject_t1113636619 * value)
	{
		___m_goContainerPopo_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerPopo_22), value);
	}

	inline static int32_t get_offset_of_m_preGesture_23() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_preGesture_23)); }
	inline GameObject_t1113636619 * get_m_preGesture_23() const { return ___m_preGesture_23; }
	inline GameObject_t1113636619 ** get_address_of_m_preGesture_23() { return &___m_preGesture_23; }
	inline void set_m_preGesture_23(GameObject_t1113636619 * value)
	{
		___m_preGesture_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGesture_23), value);
	}

	inline static int32_t get_offset_of_m_preGestureNotUI_24() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_preGestureNotUI_24)); }
	inline GameObject_t1113636619 * get_m_preGestureNotUI_24() const { return ___m_preGestureNotUI_24; }
	inline GameObject_t1113636619 ** get_address_of_m_preGestureNotUI_24() { return &___m_preGestureNotUI_24; }
	inline void set_m_preGestureNotUI_24(GameObject_t1113636619 * value)
	{
		___m_preGestureNotUI_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGestureNotUI_24), value);
	}

	inline static int32_t get_offset_of_m_preGesturePopo_25() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_preGesturePopo_25)); }
	inline GameObject_t1113636619 * get_m_preGesturePopo_25() const { return ___m_preGesturePopo_25; }
	inline GameObject_t1113636619 ** get_address_of_m_preGesturePopo_25() { return &___m_preGesturePopo_25; }
	inline void set_m_preGesturePopo_25(GameObject_t1113636619 * value)
	{
		___m_preGesturePopo_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGesturePopo_25), value);
	}

	inline static int32_t get_offset_of_m_preGesturePopoCenter_26() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_preGesturePopoCenter_26)); }
	inline GameObject_t1113636619 * get_m_preGesturePopoCenter_26() const { return ___m_preGesturePopoCenter_26; }
	inline GameObject_t1113636619 ** get_address_of_m_preGesturePopoCenter_26() { return &___m_preGesturePopoCenter_26; }
	inline void set_m_preGesturePopoCenter_26(GameObject_t1113636619 * value)
	{
		___m_preGesturePopoCenter_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_preGesturePopoCenter_26), value);
	}

	inline static int32_t get_offset_of_m_fBoughtGesturesInterval_27() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fBoughtGesturesInterval_27)); }
	inline float get_m_fBoughtGesturesInterval_27() const { return ___m_fBoughtGesturesInterval_27; }
	inline float* get_address_of_m_fBoughtGesturesInterval_27() { return &___m_fBoughtGesturesInterval_27; }
	inline void set_m_fBoughtGesturesInterval_27(float value)
	{
		___m_fBoughtGesturesInterval_27 = value;
	}

	inline static int32_t get_offset_of_m_fSelectedGesturesInterval_28() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fSelectedGesturesInterval_28)); }
	inline float get_m_fSelectedGesturesInterval_28() const { return ___m_fSelectedGesturesInterval_28; }
	inline float* get_address_of_m_fSelectedGesturesInterval_28() { return &___m_fSelectedGesturesInterval_28; }
	inline void set_m_fSelectedGesturesInterval_28(float value)
	{
		___m_fSelectedGesturesInterval_28 = value;
	}

	inline static int32_t get_offset_of_m_fCastedGesturesInterval_29() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fCastedGesturesInterval_29)); }
	inline float get_m_fCastedGesturesInterval_29() const { return ___m_fCastedGesturesInterval_29; }
	inline float* get_address_of_m_fCastedGesturesInterval_29() { return &___m_fCastedGesturesInterval_29; }
	inline void set_m_fCastedGesturesInterval_29(float value)
	{
		___m_fCastedGesturesInterval_29 = value;
	}

	inline static int32_t get_offset_of_m_fBoughtGesturesScale_30() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fBoughtGesturesScale_30)); }
	inline float get_m_fBoughtGesturesScale_30() const { return ___m_fBoughtGesturesScale_30; }
	inline float* get_address_of_m_fBoughtGesturesScale_30() { return &___m_fBoughtGesturesScale_30; }
	inline void set_m_fBoughtGesturesScale_30(float value)
	{
		___m_fBoughtGesturesScale_30 = value;
	}

	inline static int32_t get_offset_of_m_fSelectedGesturesScale_31() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fSelectedGesturesScale_31)); }
	inline float get_m_fSelectedGesturesScale_31() const { return ___m_fSelectedGesturesScale_31; }
	inline float* get_address_of_m_fSelectedGesturesScale_31() { return &___m_fSelectedGesturesScale_31; }
	inline void set_m_fSelectedGesturesScale_31(float value)
	{
		___m_fSelectedGesturesScale_31 = value;
	}

	inline static int32_t get_offset_of_m_fCastedGesturesScale_32() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fCastedGesturesScale_32)); }
	inline float get_m_fCastedGesturesScale_32() const { return ___m_fCastedGesturesScale_32; }
	inline float* get_address_of_m_fCastedGesturesScale_32() { return &___m_fCastedGesturesScale_32; }
	inline void set_m_fCastedGesturesScale_32(float value)
	{
		___m_fCastedGesturesScale_32 = value;
	}

	inline static int32_t get_offset_of_m_fUnitWidth_33() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fUnitWidth_33)); }
	inline float get_m_fUnitWidth_33() const { return ___m_fUnitWidth_33; }
	inline float* get_address_of_m_fUnitWidth_33() { return &___m_fUnitWidth_33; }
	inline void set_m_fUnitWidth_33(float value)
	{
		___m_fUnitWidth_33 = value;
	}

	inline static int32_t get_offset_of_m_fBgOffsetY_34() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fBgOffsetY_34)); }
	inline float get_m_fBgOffsetY_34() const { return ___m_fBgOffsetY_34; }
	inline float* get_address_of_m_fBgOffsetY_34() { return &___m_fBgOffsetY_34; }
	inline void set_m_fBgOffsetY_34(float value)
	{
		___m_fBgOffsetY_34 = value;
	}

	inline static int32_t get_offset_of_m_fWidthPerCharaqcter_35() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fWidthPerCharaqcter_35)); }
	inline float get_m_fWidthPerCharaqcter_35() const { return ___m_fWidthPerCharaqcter_35; }
	inline float* get_address_of_m_fWidthPerCharaqcter_35() { return &___m_fWidthPerCharaqcter_35; }
	inline void set_m_fWidthPerCharaqcter_35(float value)
	{
		___m_fWidthPerCharaqcter_35 = value;
	}

	inline static int32_t get_offset_of_m_fGenstureOriginalWidth_36() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fGenstureOriginalWidth_36)); }
	inline float get_m_fGenstureOriginalWidth_36() const { return ___m_fGenstureOriginalWidth_36; }
	inline float* get_address_of_m_fGenstureOriginalWidth_36() { return &___m_fGenstureOriginalWidth_36; }
	inline void set_m_fGenstureOriginalWidth_36(float value)
	{
		___m_fGenstureOriginalWidth_36 = value;
	}

	inline static int32_t get_offset_of_m_fLeftAndRightOffset_37() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fLeftAndRightOffset_37)); }
	inline float get_m_fLeftAndRightOffset_37() const { return ___m_fLeftAndRightOffset_37; }
	inline float* get_address_of_m_fLeftAndRightOffset_37() { return &___m_fLeftAndRightOffset_37; }
	inline void set_m_fLeftAndRightOffset_37(float value)
	{
		___m_fLeftAndRightOffset_37 = value;
	}

	inline static int32_t get_offset_of_m_nMaxSelectedNum_38() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_nMaxSelectedNum_38)); }
	inline int32_t get_m_nMaxSelectedNum_38() const { return ___m_nMaxSelectedNum_38; }
	inline int32_t* get_address_of_m_nMaxSelectedNum_38() { return &___m_nMaxSelectedNum_38; }
	inline void set_m_nMaxSelectedNum_38(int32_t value)
	{
		___m_nMaxSelectedNum_38 = value;
	}

	inline static int32_t get_offset_of_m_fColdDown_39() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fColdDown_39)); }
	inline float get_m_fColdDown_39() const { return ___m_fColdDown_39; }
	inline float* get_address_of_m_fColdDown_39() { return &___m_fColdDown_39; }
	inline void set_m_fColdDown_39(float value)
	{
		___m_fColdDown_39 = value;
	}

	inline static int32_t get_offset_of_m_fSmallColdDown_40() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fSmallColdDown_40)); }
	inline float get_m_fSmallColdDown_40() const { return ___m_fSmallColdDown_40; }
	inline float* get_address_of_m_fSmallColdDown_40() { return &___m_fSmallColdDown_40; }
	inline void set_m_fSmallColdDown_40(float value)
	{
		___m_fSmallColdDown_40 = value;
	}

	inline static int32_t get_offset_of_m_fPopoLastTime_41() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fPopoLastTime_41)); }
	inline float get_m_fPopoLastTime_41() const { return ___m_fPopoLastTime_41; }
	inline float* get_address_of_m_fPopoLastTime_41() { return &___m_fPopoLastTime_41; }
	inline void set_m_fPopoLastTime_41(float value)
	{
		___m_fPopoLastTime_41 = value;
	}

	inline static int32_t get_offset_of_m_fLastCastGestureTime_42() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fLastCastGestureTime_42)); }
	inline float get_m_fLastCastGestureTime_42() const { return ___m_fLastCastGestureTime_42; }
	inline float* get_address_of_m_fLastCastGestureTime_42() { return &___m_fLastCastGestureTime_42; }
	inline void set_m_fLastCastGestureTime_42(float value)
	{
		___m_fLastCastGestureTime_42 = value;
	}

	inline static int32_t get_offset_of_m_fOffestX_43() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fOffestX_43)); }
	inline float get_m_fOffestX_43() const { return ___m_fOffestX_43; }
	inline float* get_address_of_m_fOffestX_43() { return &___m_fOffestX_43; }
	inline void set_m_fOffestX_43(float value)
	{
		___m_fOffestX_43 = value;
	}

	inline static int32_t get_offset_of_m_fOffestY_44() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fOffestY_44)); }
	inline float get_m_fOffestY_44() const { return ___m_fOffestY_44; }
	inline float* get_address_of_m_fOffestY_44() { return &___m_fOffestY_44; }
	inline void set_m_fOffestY_44(float value)
	{
		___m_fOffestY_44 = value;
	}

	inline static int32_t get_offset_of_m_lstSelcted_45() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_lstSelcted_45)); }
	inline List_1_t3653243616 * get_m_lstSelcted_45() const { return ___m_lstSelcted_45; }
	inline List_1_t3653243616 ** get_address_of_m_lstSelcted_45() { return &___m_lstSelcted_45; }
	inline void set_m_lstSelcted_45(List_1_t3653243616 * value)
	{
		___m_lstSelcted_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSelcted_45), value);
	}

	inline static int32_t get_offset_of_m_eDir_46() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_eDir_46)); }
	inline int32_t get_m_eDir_46() const { return ___m_eDir_46; }
	inline int32_t* get_address_of_m_eDir_46() { return &___m_eDir_46; }
	inline void set_m_eDir_46(int32_t value)
	{
		___m_eDir_46 = value;
	}

	inline static int32_t get_offset_of_m_lstRecycledGestures_47() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_lstRecycledGestures_47)); }
	inline List_1_t3653243616 * get_m_lstRecycledGestures_47() const { return ___m_lstRecycledGestures_47; }
	inline List_1_t3653243616 ** get_address_of_m_lstRecycledGestures_47() { return &___m_lstRecycledGestures_47; }
	inline void set_m_lstRecycledGestures_47(List_1_t3653243616 * value)
	{
		___m_lstRecycledGestures_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledGestures_47), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledGesturePopos_48() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_lstRecycledGesturePopos_48)); }
	inline List_1_t3832709940 * get_m_lstRecycledGesturePopos_48() const { return ___m_lstRecycledGesturePopos_48; }
	inline List_1_t3832709940 ** get_address_of_m_lstRecycledGesturePopos_48() { return &___m_lstRecycledGesturePopos_48; }
	inline void set_m_lstRecycledGesturePopos_48(List_1_t3832709940 * value)
	{
		___m_lstRecycledGesturePopos_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledGesturePopos_48), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledPopoCenter_49() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_lstRecycledPopoCenter_49)); }
	inline List_1_t2585711361 * get_m_lstRecycledPopoCenter_49() const { return ___m_lstRecycledPopoCenter_49; }
	inline List_1_t2585711361 ** get_address_of_m_lstRecycledPopoCenter_49() { return &___m_lstRecycledPopoCenter_49; }
	inline void set_m_lstRecycledPopoCenter_49(List_1_t2585711361 * value)
	{
		___m_lstRecycledPopoCenter_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledPopoCenter_49), value);
	}

	inline static int32_t get_offset_of_m_bShowing_50() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_bShowing_50)); }
	inline bool get_m_bShowing_50() const { return ___m_bShowing_50; }
	inline bool* get_address_of_m_bShowing_50() { return &___m_bShowing_50; }
	inline void set_m_bShowing_50(bool value)
	{
		___m_bShowing_50 = value;
	}

	inline static int32_t get_offset_of_m_fSmallColdDownTimeLeft_51() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fSmallColdDownTimeLeft_51)); }
	inline float get_m_fSmallColdDownTimeLeft_51() const { return ___m_fSmallColdDownTimeLeft_51; }
	inline float* get_address_of_m_fSmallColdDownTimeLeft_51() { return &___m_fSmallColdDownTimeLeft_51; }
	inline void set_m_fSmallColdDownTimeLeft_51(float value)
	{
		___m_fSmallColdDownTimeLeft_51 = value;
	}

	inline static int32_t get_offset_of_m_fBigColdDownTimeLeft_52() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fBigColdDownTimeLeft_52)); }
	inline float get_m_fBigColdDownTimeLeft_52() const { return ___m_fBigColdDownTimeLeft_52; }
	inline float* get_address_of_m_fBigColdDownTimeLeft_52() { return &___m_fBigColdDownTimeLeft_52; }
	inline void set_m_fBigColdDownTimeLeft_52(float value)
	{
		___m_fBigColdDownTimeLeft_52 = value;
	}

	inline static int32_t get_offset_of_s_lstCastingGestureId_53() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___s_lstCastingGestureId_53)); }
	inline List_1_t128053199 * get_s_lstCastingGestureId_53() const { return ___s_lstCastingGestureId_53; }
	inline List_1_t128053199 ** get_address_of_s_lstCastingGestureId_53() { return &___s_lstCastingGestureId_53; }
	inline void set_s_lstCastingGestureId_53(List_1_t128053199 * value)
	{
		___s_lstCastingGestureId_53 = value;
		Il2CppCodeGenWriteBarrier((&___s_lstCastingGestureId_53), value);
	}

	inline static int32_t get_offset_of__testPopo_54() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ____testPopo_54)); }
	inline CGesturePopo_t2360635198 * get__testPopo_54() const { return ____testPopo_54; }
	inline CGesturePopo_t2360635198 ** get_address_of__testPopo_54() { return &____testPopo_54; }
	inline void set__testPopo_54(CGesturePopo_t2360635198 * value)
	{
		____testPopo_54 = value;
		Il2CppCodeGenWriteBarrier((&____testPopo_54), value);
	}

	inline static int32_t get_offset_of_m_goBoughtListLeft_55() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_goBoughtListLeft_55)); }
	inline GameObject_t1113636619 * get_m_goBoughtListLeft_55() const { return ___m_goBoughtListLeft_55; }
	inline GameObject_t1113636619 ** get_address_of_m_goBoughtListLeft_55() { return &___m_goBoughtListLeft_55; }
	inline void set_m_goBoughtListLeft_55(GameObject_t1113636619 * value)
	{
		___m_goBoughtListLeft_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBoughtListLeft_55), value);
	}

	inline static int32_t get_offset_of_m_goBoughtListMain_56() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_goBoughtListMain_56)); }
	inline GameObject_t1113636619 * get_m_goBoughtListMain_56() const { return ___m_goBoughtListMain_56; }
	inline GameObject_t1113636619 ** get_address_of_m_goBoughtListMain_56() { return &___m_goBoughtListMain_56; }
	inline void set_m_goBoughtListMain_56(GameObject_t1113636619 * value)
	{
		___m_goBoughtListMain_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBoughtListMain_56), value);
	}

	inline static int32_t get_offset_of_m_fBoughtUnitWidht_57() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746, ___m_fBoughtUnitWidht_57)); }
	inline float get_m_fBoughtUnitWidht_57() const { return ___m_fBoughtUnitWidht_57; }
	inline float* get_address_of_m_fBoughtUnitWidht_57() { return &___m_fBoughtUnitWidht_57; }
	inline void set_m_fBoughtUnitWidht_57(float value)
	{
		___m_fBoughtUnitWidht_57 = value;
	}
};

struct CGestureManager_t1993322746_StaticFields
{
public:
	// CGestureManager CGestureManager::s_Instance
	CGestureManager_t1993322746 * ___s_Instance_2;
	// UnityEngine.Vector3 CGestureManager::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CGestureManager::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746_StaticFields, ___s_Instance_2)); }
	inline CGestureManager_t1993322746 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CGestureManager_t1993322746 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CGestureManager_t1993322746 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CGestureManager_t1993322746_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGESTUREMANAGER_T1993322746_H
#ifndef CGESTUREPOPO_T2360635198_H
#define CGESTUREPOPO_T2360635198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGesturePopo
struct  CGesturePopo_t2360635198  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CGesturePopo::m_nDir
	int32_t ___m_nDir_5;
	// UnityEngine.SpriteRenderer CGesturePopo::_sprLeft
	SpriteRenderer_t3235626157 * ____sprLeft_6;
	// UnityEngine.SpriteRenderer CGesturePopo::_sprRight
	SpriteRenderer_t3235626157 * ____sprRight_7;
	// UnityEngine.GameObject CGesturePopo::m_goLeft
	GameObject_t1113636619 * ___m_goLeft_8;
	// UnityEngine.GameObject CGesturePopo::m_goRighjt
	GameObject_t1113636619 * ___m_goRighjt_9;
	// UnityEngine.GameObject CGesturePopo::m_goCenter
	GameObject_t1113636619 * ___m_goCenter_10;
	// UnityEngine.GameObject CGesturePopo::m_goGesturesContianer
	GameObject_t1113636619 * ___m_goGesturesContianer_11;
	// UnityEngine.GameObject CGesturePopo::m_goBgContainer
	GameObject_t1113636619 * ___m_goBgContainer_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CGesturePopo::m_lstCenterSeg
	List_1_t2585711361 * ___m_lstCenterSeg_13;
	// System.Collections.Generic.List`1<CGesture> CGesturePopo::m_lstGestures
	List_1_t3653243616 * ___m_lstGestures_14;
	// System.Single CGesturePopo::m_fTimeElapse
	float ___m_fTimeElapse_15;
	// System.Boolean CGesturePopo::m_bShowing
	bool ___m_bShowing_16;
	// UnityEngine.TextMesh CGesturePopo::_txtPlayerName
	TextMesh_t1536577757 * ____txtPlayerName_17;
	// UnityEngine.Vector2 CGesturePopo::m_vecDir
	Vector2_t2156229523  ___m_vecDir_18;
	// CGestureManager/eGesturePopoDir CGesturePopo::m_eDir
	int32_t ___m_eDir_19;
	// System.Single CGesturePopo::m_fTotalWidth
	float ___m_fTotalWidth_20;
	// System.Single CGesturePopo::m_fOffestX
	float ___m_fOffestX_22;
	// System.Single CGesturePopo::m_fOffestY
	float ___m_fOffestY_23;
	// System.Single CGesturePopo::m_fCurScale
	float ___m_fCurScale_24;
	// Ball CGesturePopo::m_Ball
	Ball_t2206666566 * ___m_Ball_25;

public:
	inline static int32_t get_offset_of_m_nDir_5() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_nDir_5)); }
	inline int32_t get_m_nDir_5() const { return ___m_nDir_5; }
	inline int32_t* get_address_of_m_nDir_5() { return &___m_nDir_5; }
	inline void set_m_nDir_5(int32_t value)
	{
		___m_nDir_5 = value;
	}

	inline static int32_t get_offset_of__sprLeft_6() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ____sprLeft_6)); }
	inline SpriteRenderer_t3235626157 * get__sprLeft_6() const { return ____sprLeft_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprLeft_6() { return &____sprLeft_6; }
	inline void set__sprLeft_6(SpriteRenderer_t3235626157 * value)
	{
		____sprLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprLeft_6), value);
	}

	inline static int32_t get_offset_of__sprRight_7() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ____sprRight_7)); }
	inline SpriteRenderer_t3235626157 * get__sprRight_7() const { return ____sprRight_7; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprRight_7() { return &____sprRight_7; }
	inline void set__sprRight_7(SpriteRenderer_t3235626157 * value)
	{
		____sprRight_7 = value;
		Il2CppCodeGenWriteBarrier((&____sprRight_7), value);
	}

	inline static int32_t get_offset_of_m_goLeft_8() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_goLeft_8)); }
	inline GameObject_t1113636619 * get_m_goLeft_8() const { return ___m_goLeft_8; }
	inline GameObject_t1113636619 ** get_address_of_m_goLeft_8() { return &___m_goLeft_8; }
	inline void set_m_goLeft_8(GameObject_t1113636619 * value)
	{
		___m_goLeft_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_goLeft_8), value);
	}

	inline static int32_t get_offset_of_m_goRighjt_9() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_goRighjt_9)); }
	inline GameObject_t1113636619 * get_m_goRighjt_9() const { return ___m_goRighjt_9; }
	inline GameObject_t1113636619 ** get_address_of_m_goRighjt_9() { return &___m_goRighjt_9; }
	inline void set_m_goRighjt_9(GameObject_t1113636619 * value)
	{
		___m_goRighjt_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRighjt_9), value);
	}

	inline static int32_t get_offset_of_m_goCenter_10() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_goCenter_10)); }
	inline GameObject_t1113636619 * get_m_goCenter_10() const { return ___m_goCenter_10; }
	inline GameObject_t1113636619 ** get_address_of_m_goCenter_10() { return &___m_goCenter_10; }
	inline void set_m_goCenter_10(GameObject_t1113636619 * value)
	{
		___m_goCenter_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_goCenter_10), value);
	}

	inline static int32_t get_offset_of_m_goGesturesContianer_11() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_goGesturesContianer_11)); }
	inline GameObject_t1113636619 * get_m_goGesturesContianer_11() const { return ___m_goGesturesContianer_11; }
	inline GameObject_t1113636619 ** get_address_of_m_goGesturesContianer_11() { return &___m_goGesturesContianer_11; }
	inline void set_m_goGesturesContianer_11(GameObject_t1113636619 * value)
	{
		___m_goGesturesContianer_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_goGesturesContianer_11), value);
	}

	inline static int32_t get_offset_of_m_goBgContainer_12() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_goBgContainer_12)); }
	inline GameObject_t1113636619 * get_m_goBgContainer_12() const { return ___m_goBgContainer_12; }
	inline GameObject_t1113636619 ** get_address_of_m_goBgContainer_12() { return &___m_goBgContainer_12; }
	inline void set_m_goBgContainer_12(GameObject_t1113636619 * value)
	{
		___m_goBgContainer_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBgContainer_12), value);
	}

	inline static int32_t get_offset_of_m_lstCenterSeg_13() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_lstCenterSeg_13)); }
	inline List_1_t2585711361 * get_m_lstCenterSeg_13() const { return ___m_lstCenterSeg_13; }
	inline List_1_t2585711361 ** get_address_of_m_lstCenterSeg_13() { return &___m_lstCenterSeg_13; }
	inline void set_m_lstCenterSeg_13(List_1_t2585711361 * value)
	{
		___m_lstCenterSeg_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstCenterSeg_13), value);
	}

	inline static int32_t get_offset_of_m_lstGestures_14() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_lstGestures_14)); }
	inline List_1_t3653243616 * get_m_lstGestures_14() const { return ___m_lstGestures_14; }
	inline List_1_t3653243616 ** get_address_of_m_lstGestures_14() { return &___m_lstGestures_14; }
	inline void set_m_lstGestures_14(List_1_t3653243616 * value)
	{
		___m_lstGestures_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstGestures_14), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_15() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_fTimeElapse_15)); }
	inline float get_m_fTimeElapse_15() const { return ___m_fTimeElapse_15; }
	inline float* get_address_of_m_fTimeElapse_15() { return &___m_fTimeElapse_15; }
	inline void set_m_fTimeElapse_15(float value)
	{
		___m_fTimeElapse_15 = value;
	}

	inline static int32_t get_offset_of_m_bShowing_16() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_bShowing_16)); }
	inline bool get_m_bShowing_16() const { return ___m_bShowing_16; }
	inline bool* get_address_of_m_bShowing_16() { return &___m_bShowing_16; }
	inline void set_m_bShowing_16(bool value)
	{
		___m_bShowing_16 = value;
	}

	inline static int32_t get_offset_of__txtPlayerName_17() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ____txtPlayerName_17)); }
	inline TextMesh_t1536577757 * get__txtPlayerName_17() const { return ____txtPlayerName_17; }
	inline TextMesh_t1536577757 ** get_address_of__txtPlayerName_17() { return &____txtPlayerName_17; }
	inline void set__txtPlayerName_17(TextMesh_t1536577757 * value)
	{
		____txtPlayerName_17 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_17), value);
	}

	inline static int32_t get_offset_of_m_vecDir_18() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_vecDir_18)); }
	inline Vector2_t2156229523  get_m_vecDir_18() const { return ___m_vecDir_18; }
	inline Vector2_t2156229523 * get_address_of_m_vecDir_18() { return &___m_vecDir_18; }
	inline void set_m_vecDir_18(Vector2_t2156229523  value)
	{
		___m_vecDir_18 = value;
	}

	inline static int32_t get_offset_of_m_eDir_19() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_eDir_19)); }
	inline int32_t get_m_eDir_19() const { return ___m_eDir_19; }
	inline int32_t* get_address_of_m_eDir_19() { return &___m_eDir_19; }
	inline void set_m_eDir_19(int32_t value)
	{
		___m_eDir_19 = value;
	}

	inline static int32_t get_offset_of_m_fTotalWidth_20() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_fTotalWidth_20)); }
	inline float get_m_fTotalWidth_20() const { return ___m_fTotalWidth_20; }
	inline float* get_address_of_m_fTotalWidth_20() { return &___m_fTotalWidth_20; }
	inline void set_m_fTotalWidth_20(float value)
	{
		___m_fTotalWidth_20 = value;
	}

	inline static int32_t get_offset_of_m_fOffestX_22() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_fOffestX_22)); }
	inline float get_m_fOffestX_22() const { return ___m_fOffestX_22; }
	inline float* get_address_of_m_fOffestX_22() { return &___m_fOffestX_22; }
	inline void set_m_fOffestX_22(float value)
	{
		___m_fOffestX_22 = value;
	}

	inline static int32_t get_offset_of_m_fOffestY_23() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_fOffestY_23)); }
	inline float get_m_fOffestY_23() const { return ___m_fOffestY_23; }
	inline float* get_address_of_m_fOffestY_23() { return &___m_fOffestY_23; }
	inline void set_m_fOffestY_23(float value)
	{
		___m_fOffestY_23 = value;
	}

	inline static int32_t get_offset_of_m_fCurScale_24() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_fCurScale_24)); }
	inline float get_m_fCurScale_24() const { return ___m_fCurScale_24; }
	inline float* get_address_of_m_fCurScale_24() { return &___m_fCurScale_24; }
	inline void set_m_fCurScale_24(float value)
	{
		___m_fCurScale_24 = value;
	}

	inline static int32_t get_offset_of_m_Ball_25() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198, ___m_Ball_25)); }
	inline Ball_t2206666566 * get_m_Ball_25() const { return ___m_Ball_25; }
	inline Ball_t2206666566 ** get_address_of_m_Ball_25() { return &___m_Ball_25; }
	inline void set_m_Ball_25(Ball_t2206666566 * value)
	{
		___m_Ball_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Ball_25), value);
	}
};

struct CGesturePopo_t2360635198_StaticFields
{
public:
	// UnityEngine.Vector3 CGesturePopo::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CGesturePopo::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// UnityEngine.Vector2 CGesturePopo::vecTempDir
	Vector2_t2156229523  ___vecTempDir_4;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_4() { return static_cast<int32_t>(offsetof(CGesturePopo_t2360635198_StaticFields, ___vecTempDir_4)); }
	inline Vector2_t2156229523  get_vecTempDir_4() const { return ___vecTempDir_4; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_4() { return &___vecTempDir_4; }
	inline void set_vecTempDir_4(Vector2_t2156229523  value)
	{
		___vecTempDir_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGESTUREPOPO_T2360635198_H
#ifndef CGMSYSTEM_T2710125162_H
#define CGMSYSTEM_T2710125162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGMSystem
struct  CGMSystem_t2710125162  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CGMSystem_t2710125162_StaticFields
{
public:
	// CGMSystem CGMSystem::s_Instance
	CGMSystem_t2710125162 * ___s_Instance_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CGMSystem::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CGMSystem_t2710125162_StaticFields, ___s_Instance_2)); }
	inline CGMSystem_t2710125162 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CGMSystem_t2710125162 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CGMSystem_t2710125162 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(CGMSystem_t2710125162_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGMSYSTEM_T2710125162_H
#ifndef CGROWSYSTEM_T4101788339_H
#define CGROWSYSTEM_T4101788339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrowSystem
struct  CGrowSystem_t4101788339  : public MonoBehaviour_t3962482529
{
public:
	// CFrameAnimationEffect CGrowSystem::_effectLevelUpNumber
	CFrameAnimationEffect_t443605508 * ____effectLevelUpNumber_2;
	// UnityEngine.UI.Dropdown CGrowSystem::_dropDownLevel
	Dropdown_t2274391225 * ____dropDownLevel_6;
	// UnityEngine.UI.InputField CGrowSystem::_inputNeedExp
	InputField_t3762917431 * ____inputNeedExp_7;
	// UnityEngine.UI.InputField CGrowSystem::_inputMP
	InputField_t3762917431 * ____inputMP_8;
	// UnityEngine.UI.InputField CGrowSystem::_inputMPRecoverSpeed
	InputField_t3762917431 * ____inputMPRecoverSpeed_9;
	// UnityEngine.UI.InputField CGrowSystem::_inputKillGainExp
	InputField_t3762917431 * ____inputKillGainExp_10;
	// UnityEngine.UI.InputField CGrowSystem::_inputKillGainMoney
	InputField_t3762917431 * ____inputKillGainMoney_11;
	// UnityEngine.UI.InputField CGrowSystem::_inputBaseVolume
	InputField_t3762917431 * ____inputBaseVolume_12;
	// UnityEngine.UI.InputField CGrowSystem::_inputLevelMoney
	InputField_t3762917431 * ____inputLevelMoney_13;
	// UnityEngine.UI.InputField CGrowSystem::_inputDeadWaitingTime
	InputField_t3762917431 * ____inputDeadWaitingTime_14;
	// UnityEngine.UI.Image CGrowSystem::_imgMp
	Image_t2670269651 * ____imgMp_15;
	// UnityEngine.UI.Image CGrowSystem::_imgMp_PC
	Image_t2670269651 * ____imgMp_PC_16;
	// UnityEngine.UI.Image CGrowSystem::_imgMp_MOBILE
	Image_t2670269651 * ____imgMp_MOBILE_17;
	// UnityEngine.UI.Text CGrowSystem::_txtMp
	Text_t1901882714 * ____txtMp_18;
	// UnityEngine.UI.Text CGrowSystem::_txtMp_PC
	Text_t1901882714 * ____txtMp_PC_19;
	// UnityEngine.UI.Text CGrowSystem::_txtMp_MOBILE
	Text_t1901882714 * ____txtMp_MOBILE_20;
	// UnityEngine.UI.Text CGrowSystem::_txtLevel
	Text_t1901882714 * ____txtLevel_21;
	// UnityEngine.UI.Text CGrowSystem::_txtExp
	Text_t1901882714 * ____txtExp_22;
	// UnityEngine.UI.Text CGrowSystem::_txtNextLevelExp
	Text_t1901882714 * ____txtNextLevelExp_23;
	// UnityEngine.UI.Image CGrowSystem::_imgExpPercent
	Image_t2670269651 * ____imgExpPercent_24;
	// UnityEngine.UI.Text CGrowSystem::_txtMyRank
	Text_t1901882714 * ____txtMyRank_25;
	// UnityEngine.UI.Text CGrowSystem::_txtTotalPlayerNum
	Text_t1901882714 * ____txtTotalPlayerNum_26;
	// UnityEngine.UI.Text CGrowSystem::_txtMyRank_PC
	Text_t1901882714 * ____txtMyRank_PC_27;
	// UnityEngine.UI.Text CGrowSystem::_txtMyRank_MOBILE
	Text_t1901882714 * ____txtMyRank_MOBILE_28;
	// UnityEngine.UI.Text CGrowSystem::_txtTotalPlayerNum_PC
	Text_t1901882714 * ____txtTotalPlayerNum_PC_29;
	// UnityEngine.UI.Text CGrowSystem::_txtTotalPlayerNum_MOBILE
	Text_t1901882714 * ____txtTotalPlayerNum_MOBILE_30;
	// UnityEngine.UI.Text CGrowSystem::_txtCurTotalVolume
	Text_t1901882714 * ____txtCurTotalVolume_31;
	// UnityEngine.UI.Text CGrowSystem::_txtEatThornNum
	Text_t1901882714 * ____txtEatThornNum_32;
	// UnityEngine.UI.Text CGrowSystem::_txtPlayerSpeed
	Text_t1901882714 * ____txtPlayerSpeed_33;
	// UnityEngine.UI.Text CGrowSystem::_txtLiveBallNum
	Text_t1901882714 * ____txtLiveBallNum_34;
	// UnityEngine.UI.Text CGrowSystem::_txtJiShaInfo
	Text_t1901882714 * ____txtJiShaInfo_35;
	// UnityEngine.UI.Text CGrowSystem::_txtAttenuate
	Text_t1901882714 * ____txtAttenuate_36;
	// UnityEngine.UI.Text CGrowSystem::_txtKill
	Text_t1901882714 * ____txtKill_37;
	// UnityEngine.UI.Text CGrowSystem::_txtBeKilled
	Text_t1901882714 * ____txtBeKilled_38;
	// UnityEngine.UI.Text CGrowSystem::_txtAssist
	Text_t1901882714 * ____txtAssist_39;
	// UnityEngine.UI.Text CGrowSystem::_txtCurTotalVolume_PC
	Text_t1901882714 * ____txtCurTotalVolume_PC_40;
	// UnityEngine.UI.Text CGrowSystem::_txtEatThornNum_PC
	Text_t1901882714 * ____txtEatThornNum_PC_41;
	// UnityEngine.UI.Text CGrowSystem::_txtPlayerSpeed_PC
	Text_t1901882714 * ____txtPlayerSpeed_PC_42;
	// UnityEngine.UI.Text CGrowSystem::_txtKill_PC
	Text_t1901882714 * ____txtKill_PC_43;
	// UnityEngine.UI.Text CGrowSystem::_txtBeKilled_PC
	Text_t1901882714 * ____txtBeKilled_PC_44;
	// UnityEngine.UI.Text CGrowSystem::_txtAssist_PC
	Text_t1901882714 * ____txtAssist_PC_45;
	// UnityEngine.UI.Text CGrowSystem::_txtCurTotalVolume_MOBILE
	Text_t1901882714 * ____txtCurTotalVolume_MOBILE_46;
	// UnityEngine.UI.Text CGrowSystem::_txtEatThornNum_MOBILE
	Text_t1901882714 * ____txtEatThornNum_MOBILE_47;
	// UnityEngine.UI.Text CGrowSystem::_txtPlayerSpeed_MOBILE
	Text_t1901882714 * ____txtPlayerSpeed_MOBILE_48;
	// UnityEngine.UI.Text CGrowSystem::_txtKill_MOBILE
	Text_t1901882714 * ____txtKill_MOBILE_49;
	// UnityEngine.UI.Text CGrowSystem::_txtBeKilled_MOBILE
	Text_t1901882714 * ____txtBeKilled_MOBILE_50;
	// UnityEngine.UI.Text CGrowSystem::_txtAssist_MOBILE
	Text_t1901882714 * ____txtAssist_MOBILE_51;
	// UnityEngine.UI.Image CGrowSystem::_imgBattery
	Image_t2670269651 * ____imgBattery_52;
	// CWifi CGrowSystem::_wifiInfo
	CWifi_t3890179523 * ____wifiInfo_53;
	// CGrowSystem/sLevelConfig CGrowSystem::m_CurLevelConfig
	sLevelConfig_t4291590291  ___m_CurLevelConfig_54;
	// System.Collections.Generic.Dictionary`2<System.Int32,CGrowSystem/sLevelConfig> CGrowSystem::m_dicLevelConfig
	Dictionary_2_t3180303622 * ___m_dicLevelConfig_55;
	// System.Int32 CGrowSystem::m_nCurLevel
	int32_t ___m_nCurLevel_56;
	// System.Int32 CGrowSystem::m_nCurExp
	int32_t ___m_nCurExp_57;
	// System.Int32 CGrowSystem::m_nNextLevelExp
	int32_t ___m_nNextLevelExp_58;
	// System.Single CGrowSystem::m_fCurMP
	float ___m_fCurMP_59;
	// System.Single CGrowSystem::m_fCurMaxMP
	float ___m_fCurMaxMP_60;
	// System.Int32 CGrowSystem::m_nKillCount
	int32_t ___m_nKillCount_61;
	// System.Int32 CGrowSystem::m_nBeingKilledCount
	int32_t ___m_nBeingKilledCount_62;
	// System.Int32 CGrowSystem::m_nAssistAttackCount
	int32_t ___m_nAssistAttackCount_63;
	// System.Single CGrowSystem::m_fMpTextStartPos
	float ___m_fMpTextStartPos_64;
	// System.Single CGrowSystem::m_fMpTextEndPos
	float ___m_fMpTextEndPos_65;
	// System.Single CGrowSystem::m_fMpTextDistance
	float ___m_fMpTextDistance_66;
	// System.Single CGrowSystem::m_fMpEffectStartPos
	float ___m_fMpEffectStartPos_67;
	// System.Single CGrowSystem::m_fMpEffectEndPos
	float ___m_fMpEffectEndPos_68;
	// System.Single CGrowSystem::m_fMpEffectDistance
	float ___m_fMpEffectDistance_69;
	// CFrameAnimationEffect CGrowSystem::_effectMp
	CFrameAnimationEffect_t443605508 * ____effectMp_70;
	// CFrameAnimationEffect CGrowSystem::_effectMpStart
	CFrameAnimationEffect_t443605508 * ____effectMpStart_71;
	// UnityEngine.UI.Text CGrowSystem::_txtRecoverSpeed
	Text_t1901882714 * ____txtRecoverSpeed_72;
	// UnityEngine.GameObject CGrowSystem::_goMpPanel_PC
	GameObject_t1113636619 * ____goMpPanel_PC_73;
	// UnityEngine.GameObject CGrowSystem::_goMpPanel_Mobile
	GameObject_t1113636619 * ____goMpPanel_Mobile_74;
	// Player CGrowSystem::_Player
	Player_t3266647312 * ____Player_75;
	// System.Boolean CGrowSystem::m_bUiInited
	bool ___m_bUiInited_76;
	// CGrowSystem/sLevelConfig CGrowSystem::tempLevelConfig
	sLevelConfig_t4291590291  ___tempLevelConfig_77;
	// System.Boolean CGrowSystem::m_bExpChanged
	bool ___m_bExpChanged_78;
	// System.Int32 CGrowSystem::m_nExplodeThornNum
	int32_t ___m_nExplodeThornNum_79;

public:
	inline static int32_t get_offset_of__effectLevelUpNumber_2() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____effectLevelUpNumber_2)); }
	inline CFrameAnimationEffect_t443605508 * get__effectLevelUpNumber_2() const { return ____effectLevelUpNumber_2; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectLevelUpNumber_2() { return &____effectLevelUpNumber_2; }
	inline void set__effectLevelUpNumber_2(CFrameAnimationEffect_t443605508 * value)
	{
		____effectLevelUpNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&____effectLevelUpNumber_2), value);
	}

	inline static int32_t get_offset_of__dropDownLevel_6() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____dropDownLevel_6)); }
	inline Dropdown_t2274391225 * get__dropDownLevel_6() const { return ____dropDownLevel_6; }
	inline Dropdown_t2274391225 ** get_address_of__dropDownLevel_6() { return &____dropDownLevel_6; }
	inline void set__dropDownLevel_6(Dropdown_t2274391225 * value)
	{
		____dropDownLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&____dropDownLevel_6), value);
	}

	inline static int32_t get_offset_of__inputNeedExp_7() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputNeedExp_7)); }
	inline InputField_t3762917431 * get__inputNeedExp_7() const { return ____inputNeedExp_7; }
	inline InputField_t3762917431 ** get_address_of__inputNeedExp_7() { return &____inputNeedExp_7; }
	inline void set__inputNeedExp_7(InputField_t3762917431 * value)
	{
		____inputNeedExp_7 = value;
		Il2CppCodeGenWriteBarrier((&____inputNeedExp_7), value);
	}

	inline static int32_t get_offset_of__inputMP_8() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputMP_8)); }
	inline InputField_t3762917431 * get__inputMP_8() const { return ____inputMP_8; }
	inline InputField_t3762917431 ** get_address_of__inputMP_8() { return &____inputMP_8; }
	inline void set__inputMP_8(InputField_t3762917431 * value)
	{
		____inputMP_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputMP_8), value);
	}

	inline static int32_t get_offset_of__inputMPRecoverSpeed_9() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputMPRecoverSpeed_9)); }
	inline InputField_t3762917431 * get__inputMPRecoverSpeed_9() const { return ____inputMPRecoverSpeed_9; }
	inline InputField_t3762917431 ** get_address_of__inputMPRecoverSpeed_9() { return &____inputMPRecoverSpeed_9; }
	inline void set__inputMPRecoverSpeed_9(InputField_t3762917431 * value)
	{
		____inputMPRecoverSpeed_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputMPRecoverSpeed_9), value);
	}

	inline static int32_t get_offset_of__inputKillGainExp_10() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputKillGainExp_10)); }
	inline InputField_t3762917431 * get__inputKillGainExp_10() const { return ____inputKillGainExp_10; }
	inline InputField_t3762917431 ** get_address_of__inputKillGainExp_10() { return &____inputKillGainExp_10; }
	inline void set__inputKillGainExp_10(InputField_t3762917431 * value)
	{
		____inputKillGainExp_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputKillGainExp_10), value);
	}

	inline static int32_t get_offset_of__inputKillGainMoney_11() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputKillGainMoney_11)); }
	inline InputField_t3762917431 * get__inputKillGainMoney_11() const { return ____inputKillGainMoney_11; }
	inline InputField_t3762917431 ** get_address_of__inputKillGainMoney_11() { return &____inputKillGainMoney_11; }
	inline void set__inputKillGainMoney_11(InputField_t3762917431 * value)
	{
		____inputKillGainMoney_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputKillGainMoney_11), value);
	}

	inline static int32_t get_offset_of__inputBaseVolume_12() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputBaseVolume_12)); }
	inline InputField_t3762917431 * get__inputBaseVolume_12() const { return ____inputBaseVolume_12; }
	inline InputField_t3762917431 ** get_address_of__inputBaseVolume_12() { return &____inputBaseVolume_12; }
	inline void set__inputBaseVolume_12(InputField_t3762917431 * value)
	{
		____inputBaseVolume_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputBaseVolume_12), value);
	}

	inline static int32_t get_offset_of__inputLevelMoney_13() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputLevelMoney_13)); }
	inline InputField_t3762917431 * get__inputLevelMoney_13() const { return ____inputLevelMoney_13; }
	inline InputField_t3762917431 ** get_address_of__inputLevelMoney_13() { return &____inputLevelMoney_13; }
	inline void set__inputLevelMoney_13(InputField_t3762917431 * value)
	{
		____inputLevelMoney_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputLevelMoney_13), value);
	}

	inline static int32_t get_offset_of__inputDeadWaitingTime_14() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____inputDeadWaitingTime_14)); }
	inline InputField_t3762917431 * get__inputDeadWaitingTime_14() const { return ____inputDeadWaitingTime_14; }
	inline InputField_t3762917431 ** get_address_of__inputDeadWaitingTime_14() { return &____inputDeadWaitingTime_14; }
	inline void set__inputDeadWaitingTime_14(InputField_t3762917431 * value)
	{
		____inputDeadWaitingTime_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputDeadWaitingTime_14), value);
	}

	inline static int32_t get_offset_of__imgMp_15() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____imgMp_15)); }
	inline Image_t2670269651 * get__imgMp_15() const { return ____imgMp_15; }
	inline Image_t2670269651 ** get_address_of__imgMp_15() { return &____imgMp_15; }
	inline void set__imgMp_15(Image_t2670269651 * value)
	{
		____imgMp_15 = value;
		Il2CppCodeGenWriteBarrier((&____imgMp_15), value);
	}

	inline static int32_t get_offset_of__imgMp_PC_16() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____imgMp_PC_16)); }
	inline Image_t2670269651 * get__imgMp_PC_16() const { return ____imgMp_PC_16; }
	inline Image_t2670269651 ** get_address_of__imgMp_PC_16() { return &____imgMp_PC_16; }
	inline void set__imgMp_PC_16(Image_t2670269651 * value)
	{
		____imgMp_PC_16 = value;
		Il2CppCodeGenWriteBarrier((&____imgMp_PC_16), value);
	}

	inline static int32_t get_offset_of__imgMp_MOBILE_17() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____imgMp_MOBILE_17)); }
	inline Image_t2670269651 * get__imgMp_MOBILE_17() const { return ____imgMp_MOBILE_17; }
	inline Image_t2670269651 ** get_address_of__imgMp_MOBILE_17() { return &____imgMp_MOBILE_17; }
	inline void set__imgMp_MOBILE_17(Image_t2670269651 * value)
	{
		____imgMp_MOBILE_17 = value;
		Il2CppCodeGenWriteBarrier((&____imgMp_MOBILE_17), value);
	}

	inline static int32_t get_offset_of__txtMp_18() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtMp_18)); }
	inline Text_t1901882714 * get__txtMp_18() const { return ____txtMp_18; }
	inline Text_t1901882714 ** get_address_of__txtMp_18() { return &____txtMp_18; }
	inline void set__txtMp_18(Text_t1901882714 * value)
	{
		____txtMp_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtMp_18), value);
	}

	inline static int32_t get_offset_of__txtMp_PC_19() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtMp_PC_19)); }
	inline Text_t1901882714 * get__txtMp_PC_19() const { return ____txtMp_PC_19; }
	inline Text_t1901882714 ** get_address_of__txtMp_PC_19() { return &____txtMp_PC_19; }
	inline void set__txtMp_PC_19(Text_t1901882714 * value)
	{
		____txtMp_PC_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtMp_PC_19), value);
	}

	inline static int32_t get_offset_of__txtMp_MOBILE_20() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtMp_MOBILE_20)); }
	inline Text_t1901882714 * get__txtMp_MOBILE_20() const { return ____txtMp_MOBILE_20; }
	inline Text_t1901882714 ** get_address_of__txtMp_MOBILE_20() { return &____txtMp_MOBILE_20; }
	inline void set__txtMp_MOBILE_20(Text_t1901882714 * value)
	{
		____txtMp_MOBILE_20 = value;
		Il2CppCodeGenWriteBarrier((&____txtMp_MOBILE_20), value);
	}

	inline static int32_t get_offset_of__txtLevel_21() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtLevel_21)); }
	inline Text_t1901882714 * get__txtLevel_21() const { return ____txtLevel_21; }
	inline Text_t1901882714 ** get_address_of__txtLevel_21() { return &____txtLevel_21; }
	inline void set__txtLevel_21(Text_t1901882714 * value)
	{
		____txtLevel_21 = value;
		Il2CppCodeGenWriteBarrier((&____txtLevel_21), value);
	}

	inline static int32_t get_offset_of__txtExp_22() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtExp_22)); }
	inline Text_t1901882714 * get__txtExp_22() const { return ____txtExp_22; }
	inline Text_t1901882714 ** get_address_of__txtExp_22() { return &____txtExp_22; }
	inline void set__txtExp_22(Text_t1901882714 * value)
	{
		____txtExp_22 = value;
		Il2CppCodeGenWriteBarrier((&____txtExp_22), value);
	}

	inline static int32_t get_offset_of__txtNextLevelExp_23() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtNextLevelExp_23)); }
	inline Text_t1901882714 * get__txtNextLevelExp_23() const { return ____txtNextLevelExp_23; }
	inline Text_t1901882714 ** get_address_of__txtNextLevelExp_23() { return &____txtNextLevelExp_23; }
	inline void set__txtNextLevelExp_23(Text_t1901882714 * value)
	{
		____txtNextLevelExp_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtNextLevelExp_23), value);
	}

	inline static int32_t get_offset_of__imgExpPercent_24() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____imgExpPercent_24)); }
	inline Image_t2670269651 * get__imgExpPercent_24() const { return ____imgExpPercent_24; }
	inline Image_t2670269651 ** get_address_of__imgExpPercent_24() { return &____imgExpPercent_24; }
	inline void set__imgExpPercent_24(Image_t2670269651 * value)
	{
		____imgExpPercent_24 = value;
		Il2CppCodeGenWriteBarrier((&____imgExpPercent_24), value);
	}

	inline static int32_t get_offset_of__txtMyRank_25() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtMyRank_25)); }
	inline Text_t1901882714 * get__txtMyRank_25() const { return ____txtMyRank_25; }
	inline Text_t1901882714 ** get_address_of__txtMyRank_25() { return &____txtMyRank_25; }
	inline void set__txtMyRank_25(Text_t1901882714 * value)
	{
		____txtMyRank_25 = value;
		Il2CppCodeGenWriteBarrier((&____txtMyRank_25), value);
	}

	inline static int32_t get_offset_of__txtTotalPlayerNum_26() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtTotalPlayerNum_26)); }
	inline Text_t1901882714 * get__txtTotalPlayerNum_26() const { return ____txtTotalPlayerNum_26; }
	inline Text_t1901882714 ** get_address_of__txtTotalPlayerNum_26() { return &____txtTotalPlayerNum_26; }
	inline void set__txtTotalPlayerNum_26(Text_t1901882714 * value)
	{
		____txtTotalPlayerNum_26 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalPlayerNum_26), value);
	}

	inline static int32_t get_offset_of__txtMyRank_PC_27() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtMyRank_PC_27)); }
	inline Text_t1901882714 * get__txtMyRank_PC_27() const { return ____txtMyRank_PC_27; }
	inline Text_t1901882714 ** get_address_of__txtMyRank_PC_27() { return &____txtMyRank_PC_27; }
	inline void set__txtMyRank_PC_27(Text_t1901882714 * value)
	{
		____txtMyRank_PC_27 = value;
		Il2CppCodeGenWriteBarrier((&____txtMyRank_PC_27), value);
	}

	inline static int32_t get_offset_of__txtMyRank_MOBILE_28() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtMyRank_MOBILE_28)); }
	inline Text_t1901882714 * get__txtMyRank_MOBILE_28() const { return ____txtMyRank_MOBILE_28; }
	inline Text_t1901882714 ** get_address_of__txtMyRank_MOBILE_28() { return &____txtMyRank_MOBILE_28; }
	inline void set__txtMyRank_MOBILE_28(Text_t1901882714 * value)
	{
		____txtMyRank_MOBILE_28 = value;
		Il2CppCodeGenWriteBarrier((&____txtMyRank_MOBILE_28), value);
	}

	inline static int32_t get_offset_of__txtTotalPlayerNum_PC_29() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtTotalPlayerNum_PC_29)); }
	inline Text_t1901882714 * get__txtTotalPlayerNum_PC_29() const { return ____txtTotalPlayerNum_PC_29; }
	inline Text_t1901882714 ** get_address_of__txtTotalPlayerNum_PC_29() { return &____txtTotalPlayerNum_PC_29; }
	inline void set__txtTotalPlayerNum_PC_29(Text_t1901882714 * value)
	{
		____txtTotalPlayerNum_PC_29 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalPlayerNum_PC_29), value);
	}

	inline static int32_t get_offset_of__txtTotalPlayerNum_MOBILE_30() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtTotalPlayerNum_MOBILE_30)); }
	inline Text_t1901882714 * get__txtTotalPlayerNum_MOBILE_30() const { return ____txtTotalPlayerNum_MOBILE_30; }
	inline Text_t1901882714 ** get_address_of__txtTotalPlayerNum_MOBILE_30() { return &____txtTotalPlayerNum_MOBILE_30; }
	inline void set__txtTotalPlayerNum_MOBILE_30(Text_t1901882714 * value)
	{
		____txtTotalPlayerNum_MOBILE_30 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalPlayerNum_MOBILE_30), value);
	}

	inline static int32_t get_offset_of__txtCurTotalVolume_31() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtCurTotalVolume_31)); }
	inline Text_t1901882714 * get__txtCurTotalVolume_31() const { return ____txtCurTotalVolume_31; }
	inline Text_t1901882714 ** get_address_of__txtCurTotalVolume_31() { return &____txtCurTotalVolume_31; }
	inline void set__txtCurTotalVolume_31(Text_t1901882714 * value)
	{
		____txtCurTotalVolume_31 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurTotalVolume_31), value);
	}

	inline static int32_t get_offset_of__txtEatThornNum_32() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtEatThornNum_32)); }
	inline Text_t1901882714 * get__txtEatThornNum_32() const { return ____txtEatThornNum_32; }
	inline Text_t1901882714 ** get_address_of__txtEatThornNum_32() { return &____txtEatThornNum_32; }
	inline void set__txtEatThornNum_32(Text_t1901882714 * value)
	{
		____txtEatThornNum_32 = value;
		Il2CppCodeGenWriteBarrier((&____txtEatThornNum_32), value);
	}

	inline static int32_t get_offset_of__txtPlayerSpeed_33() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtPlayerSpeed_33)); }
	inline Text_t1901882714 * get__txtPlayerSpeed_33() const { return ____txtPlayerSpeed_33; }
	inline Text_t1901882714 ** get_address_of__txtPlayerSpeed_33() { return &____txtPlayerSpeed_33; }
	inline void set__txtPlayerSpeed_33(Text_t1901882714 * value)
	{
		____txtPlayerSpeed_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerSpeed_33), value);
	}

	inline static int32_t get_offset_of__txtLiveBallNum_34() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtLiveBallNum_34)); }
	inline Text_t1901882714 * get__txtLiveBallNum_34() const { return ____txtLiveBallNum_34; }
	inline Text_t1901882714 ** get_address_of__txtLiveBallNum_34() { return &____txtLiveBallNum_34; }
	inline void set__txtLiveBallNum_34(Text_t1901882714 * value)
	{
		____txtLiveBallNum_34 = value;
		Il2CppCodeGenWriteBarrier((&____txtLiveBallNum_34), value);
	}

	inline static int32_t get_offset_of__txtJiShaInfo_35() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtJiShaInfo_35)); }
	inline Text_t1901882714 * get__txtJiShaInfo_35() const { return ____txtJiShaInfo_35; }
	inline Text_t1901882714 ** get_address_of__txtJiShaInfo_35() { return &____txtJiShaInfo_35; }
	inline void set__txtJiShaInfo_35(Text_t1901882714 * value)
	{
		____txtJiShaInfo_35 = value;
		Il2CppCodeGenWriteBarrier((&____txtJiShaInfo_35), value);
	}

	inline static int32_t get_offset_of__txtAttenuate_36() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtAttenuate_36)); }
	inline Text_t1901882714 * get__txtAttenuate_36() const { return ____txtAttenuate_36; }
	inline Text_t1901882714 ** get_address_of__txtAttenuate_36() { return &____txtAttenuate_36; }
	inline void set__txtAttenuate_36(Text_t1901882714 * value)
	{
		____txtAttenuate_36 = value;
		Il2CppCodeGenWriteBarrier((&____txtAttenuate_36), value);
	}

	inline static int32_t get_offset_of__txtKill_37() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtKill_37)); }
	inline Text_t1901882714 * get__txtKill_37() const { return ____txtKill_37; }
	inline Text_t1901882714 ** get_address_of__txtKill_37() { return &____txtKill_37; }
	inline void set__txtKill_37(Text_t1901882714 * value)
	{
		____txtKill_37 = value;
		Il2CppCodeGenWriteBarrier((&____txtKill_37), value);
	}

	inline static int32_t get_offset_of__txtBeKilled_38() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtBeKilled_38)); }
	inline Text_t1901882714 * get__txtBeKilled_38() const { return ____txtBeKilled_38; }
	inline Text_t1901882714 ** get_address_of__txtBeKilled_38() { return &____txtBeKilled_38; }
	inline void set__txtBeKilled_38(Text_t1901882714 * value)
	{
		____txtBeKilled_38 = value;
		Il2CppCodeGenWriteBarrier((&____txtBeKilled_38), value);
	}

	inline static int32_t get_offset_of__txtAssist_39() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtAssist_39)); }
	inline Text_t1901882714 * get__txtAssist_39() const { return ____txtAssist_39; }
	inline Text_t1901882714 ** get_address_of__txtAssist_39() { return &____txtAssist_39; }
	inline void set__txtAssist_39(Text_t1901882714 * value)
	{
		____txtAssist_39 = value;
		Il2CppCodeGenWriteBarrier((&____txtAssist_39), value);
	}

	inline static int32_t get_offset_of__txtCurTotalVolume_PC_40() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtCurTotalVolume_PC_40)); }
	inline Text_t1901882714 * get__txtCurTotalVolume_PC_40() const { return ____txtCurTotalVolume_PC_40; }
	inline Text_t1901882714 ** get_address_of__txtCurTotalVolume_PC_40() { return &____txtCurTotalVolume_PC_40; }
	inline void set__txtCurTotalVolume_PC_40(Text_t1901882714 * value)
	{
		____txtCurTotalVolume_PC_40 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurTotalVolume_PC_40), value);
	}

	inline static int32_t get_offset_of__txtEatThornNum_PC_41() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtEatThornNum_PC_41)); }
	inline Text_t1901882714 * get__txtEatThornNum_PC_41() const { return ____txtEatThornNum_PC_41; }
	inline Text_t1901882714 ** get_address_of__txtEatThornNum_PC_41() { return &____txtEatThornNum_PC_41; }
	inline void set__txtEatThornNum_PC_41(Text_t1901882714 * value)
	{
		____txtEatThornNum_PC_41 = value;
		Il2CppCodeGenWriteBarrier((&____txtEatThornNum_PC_41), value);
	}

	inline static int32_t get_offset_of__txtPlayerSpeed_PC_42() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtPlayerSpeed_PC_42)); }
	inline Text_t1901882714 * get__txtPlayerSpeed_PC_42() const { return ____txtPlayerSpeed_PC_42; }
	inline Text_t1901882714 ** get_address_of__txtPlayerSpeed_PC_42() { return &____txtPlayerSpeed_PC_42; }
	inline void set__txtPlayerSpeed_PC_42(Text_t1901882714 * value)
	{
		____txtPlayerSpeed_PC_42 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerSpeed_PC_42), value);
	}

	inline static int32_t get_offset_of__txtKill_PC_43() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtKill_PC_43)); }
	inline Text_t1901882714 * get__txtKill_PC_43() const { return ____txtKill_PC_43; }
	inline Text_t1901882714 ** get_address_of__txtKill_PC_43() { return &____txtKill_PC_43; }
	inline void set__txtKill_PC_43(Text_t1901882714 * value)
	{
		____txtKill_PC_43 = value;
		Il2CppCodeGenWriteBarrier((&____txtKill_PC_43), value);
	}

	inline static int32_t get_offset_of__txtBeKilled_PC_44() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtBeKilled_PC_44)); }
	inline Text_t1901882714 * get__txtBeKilled_PC_44() const { return ____txtBeKilled_PC_44; }
	inline Text_t1901882714 ** get_address_of__txtBeKilled_PC_44() { return &____txtBeKilled_PC_44; }
	inline void set__txtBeKilled_PC_44(Text_t1901882714 * value)
	{
		____txtBeKilled_PC_44 = value;
		Il2CppCodeGenWriteBarrier((&____txtBeKilled_PC_44), value);
	}

	inline static int32_t get_offset_of__txtAssist_PC_45() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtAssist_PC_45)); }
	inline Text_t1901882714 * get__txtAssist_PC_45() const { return ____txtAssist_PC_45; }
	inline Text_t1901882714 ** get_address_of__txtAssist_PC_45() { return &____txtAssist_PC_45; }
	inline void set__txtAssist_PC_45(Text_t1901882714 * value)
	{
		____txtAssist_PC_45 = value;
		Il2CppCodeGenWriteBarrier((&____txtAssist_PC_45), value);
	}

	inline static int32_t get_offset_of__txtCurTotalVolume_MOBILE_46() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtCurTotalVolume_MOBILE_46)); }
	inline Text_t1901882714 * get__txtCurTotalVolume_MOBILE_46() const { return ____txtCurTotalVolume_MOBILE_46; }
	inline Text_t1901882714 ** get_address_of__txtCurTotalVolume_MOBILE_46() { return &____txtCurTotalVolume_MOBILE_46; }
	inline void set__txtCurTotalVolume_MOBILE_46(Text_t1901882714 * value)
	{
		____txtCurTotalVolume_MOBILE_46 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurTotalVolume_MOBILE_46), value);
	}

	inline static int32_t get_offset_of__txtEatThornNum_MOBILE_47() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtEatThornNum_MOBILE_47)); }
	inline Text_t1901882714 * get__txtEatThornNum_MOBILE_47() const { return ____txtEatThornNum_MOBILE_47; }
	inline Text_t1901882714 ** get_address_of__txtEatThornNum_MOBILE_47() { return &____txtEatThornNum_MOBILE_47; }
	inline void set__txtEatThornNum_MOBILE_47(Text_t1901882714 * value)
	{
		____txtEatThornNum_MOBILE_47 = value;
		Il2CppCodeGenWriteBarrier((&____txtEatThornNum_MOBILE_47), value);
	}

	inline static int32_t get_offset_of__txtPlayerSpeed_MOBILE_48() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtPlayerSpeed_MOBILE_48)); }
	inline Text_t1901882714 * get__txtPlayerSpeed_MOBILE_48() const { return ____txtPlayerSpeed_MOBILE_48; }
	inline Text_t1901882714 ** get_address_of__txtPlayerSpeed_MOBILE_48() { return &____txtPlayerSpeed_MOBILE_48; }
	inline void set__txtPlayerSpeed_MOBILE_48(Text_t1901882714 * value)
	{
		____txtPlayerSpeed_MOBILE_48 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerSpeed_MOBILE_48), value);
	}

	inline static int32_t get_offset_of__txtKill_MOBILE_49() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtKill_MOBILE_49)); }
	inline Text_t1901882714 * get__txtKill_MOBILE_49() const { return ____txtKill_MOBILE_49; }
	inline Text_t1901882714 ** get_address_of__txtKill_MOBILE_49() { return &____txtKill_MOBILE_49; }
	inline void set__txtKill_MOBILE_49(Text_t1901882714 * value)
	{
		____txtKill_MOBILE_49 = value;
		Il2CppCodeGenWriteBarrier((&____txtKill_MOBILE_49), value);
	}

	inline static int32_t get_offset_of__txtBeKilled_MOBILE_50() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtBeKilled_MOBILE_50)); }
	inline Text_t1901882714 * get__txtBeKilled_MOBILE_50() const { return ____txtBeKilled_MOBILE_50; }
	inline Text_t1901882714 ** get_address_of__txtBeKilled_MOBILE_50() { return &____txtBeKilled_MOBILE_50; }
	inline void set__txtBeKilled_MOBILE_50(Text_t1901882714 * value)
	{
		____txtBeKilled_MOBILE_50 = value;
		Il2CppCodeGenWriteBarrier((&____txtBeKilled_MOBILE_50), value);
	}

	inline static int32_t get_offset_of__txtAssist_MOBILE_51() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtAssist_MOBILE_51)); }
	inline Text_t1901882714 * get__txtAssist_MOBILE_51() const { return ____txtAssist_MOBILE_51; }
	inline Text_t1901882714 ** get_address_of__txtAssist_MOBILE_51() { return &____txtAssist_MOBILE_51; }
	inline void set__txtAssist_MOBILE_51(Text_t1901882714 * value)
	{
		____txtAssist_MOBILE_51 = value;
		Il2CppCodeGenWriteBarrier((&____txtAssist_MOBILE_51), value);
	}

	inline static int32_t get_offset_of__imgBattery_52() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____imgBattery_52)); }
	inline Image_t2670269651 * get__imgBattery_52() const { return ____imgBattery_52; }
	inline Image_t2670269651 ** get_address_of__imgBattery_52() { return &____imgBattery_52; }
	inline void set__imgBattery_52(Image_t2670269651 * value)
	{
		____imgBattery_52 = value;
		Il2CppCodeGenWriteBarrier((&____imgBattery_52), value);
	}

	inline static int32_t get_offset_of__wifiInfo_53() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____wifiInfo_53)); }
	inline CWifi_t3890179523 * get__wifiInfo_53() const { return ____wifiInfo_53; }
	inline CWifi_t3890179523 ** get_address_of__wifiInfo_53() { return &____wifiInfo_53; }
	inline void set__wifiInfo_53(CWifi_t3890179523 * value)
	{
		____wifiInfo_53 = value;
		Il2CppCodeGenWriteBarrier((&____wifiInfo_53), value);
	}

	inline static int32_t get_offset_of_m_CurLevelConfig_54() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_CurLevelConfig_54)); }
	inline sLevelConfig_t4291590291  get_m_CurLevelConfig_54() const { return ___m_CurLevelConfig_54; }
	inline sLevelConfig_t4291590291 * get_address_of_m_CurLevelConfig_54() { return &___m_CurLevelConfig_54; }
	inline void set_m_CurLevelConfig_54(sLevelConfig_t4291590291  value)
	{
		___m_CurLevelConfig_54 = value;
	}

	inline static int32_t get_offset_of_m_dicLevelConfig_55() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_dicLevelConfig_55)); }
	inline Dictionary_2_t3180303622 * get_m_dicLevelConfig_55() const { return ___m_dicLevelConfig_55; }
	inline Dictionary_2_t3180303622 ** get_address_of_m_dicLevelConfig_55() { return &___m_dicLevelConfig_55; }
	inline void set_m_dicLevelConfig_55(Dictionary_2_t3180303622 * value)
	{
		___m_dicLevelConfig_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLevelConfig_55), value);
	}

	inline static int32_t get_offset_of_m_nCurLevel_56() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nCurLevel_56)); }
	inline int32_t get_m_nCurLevel_56() const { return ___m_nCurLevel_56; }
	inline int32_t* get_address_of_m_nCurLevel_56() { return &___m_nCurLevel_56; }
	inline void set_m_nCurLevel_56(int32_t value)
	{
		___m_nCurLevel_56 = value;
	}

	inline static int32_t get_offset_of_m_nCurExp_57() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nCurExp_57)); }
	inline int32_t get_m_nCurExp_57() const { return ___m_nCurExp_57; }
	inline int32_t* get_address_of_m_nCurExp_57() { return &___m_nCurExp_57; }
	inline void set_m_nCurExp_57(int32_t value)
	{
		___m_nCurExp_57 = value;
	}

	inline static int32_t get_offset_of_m_nNextLevelExp_58() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nNextLevelExp_58)); }
	inline int32_t get_m_nNextLevelExp_58() const { return ___m_nNextLevelExp_58; }
	inline int32_t* get_address_of_m_nNextLevelExp_58() { return &___m_nNextLevelExp_58; }
	inline void set_m_nNextLevelExp_58(int32_t value)
	{
		___m_nNextLevelExp_58 = value;
	}

	inline static int32_t get_offset_of_m_fCurMP_59() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fCurMP_59)); }
	inline float get_m_fCurMP_59() const { return ___m_fCurMP_59; }
	inline float* get_address_of_m_fCurMP_59() { return &___m_fCurMP_59; }
	inline void set_m_fCurMP_59(float value)
	{
		___m_fCurMP_59 = value;
	}

	inline static int32_t get_offset_of_m_fCurMaxMP_60() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fCurMaxMP_60)); }
	inline float get_m_fCurMaxMP_60() const { return ___m_fCurMaxMP_60; }
	inline float* get_address_of_m_fCurMaxMP_60() { return &___m_fCurMaxMP_60; }
	inline void set_m_fCurMaxMP_60(float value)
	{
		___m_fCurMaxMP_60 = value;
	}

	inline static int32_t get_offset_of_m_nKillCount_61() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nKillCount_61)); }
	inline int32_t get_m_nKillCount_61() const { return ___m_nKillCount_61; }
	inline int32_t* get_address_of_m_nKillCount_61() { return &___m_nKillCount_61; }
	inline void set_m_nKillCount_61(int32_t value)
	{
		___m_nKillCount_61 = value;
	}

	inline static int32_t get_offset_of_m_nBeingKilledCount_62() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nBeingKilledCount_62)); }
	inline int32_t get_m_nBeingKilledCount_62() const { return ___m_nBeingKilledCount_62; }
	inline int32_t* get_address_of_m_nBeingKilledCount_62() { return &___m_nBeingKilledCount_62; }
	inline void set_m_nBeingKilledCount_62(int32_t value)
	{
		___m_nBeingKilledCount_62 = value;
	}

	inline static int32_t get_offset_of_m_nAssistAttackCount_63() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nAssistAttackCount_63)); }
	inline int32_t get_m_nAssistAttackCount_63() const { return ___m_nAssistAttackCount_63; }
	inline int32_t* get_address_of_m_nAssistAttackCount_63() { return &___m_nAssistAttackCount_63; }
	inline void set_m_nAssistAttackCount_63(int32_t value)
	{
		___m_nAssistAttackCount_63 = value;
	}

	inline static int32_t get_offset_of_m_fMpTextStartPos_64() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fMpTextStartPos_64)); }
	inline float get_m_fMpTextStartPos_64() const { return ___m_fMpTextStartPos_64; }
	inline float* get_address_of_m_fMpTextStartPos_64() { return &___m_fMpTextStartPos_64; }
	inline void set_m_fMpTextStartPos_64(float value)
	{
		___m_fMpTextStartPos_64 = value;
	}

	inline static int32_t get_offset_of_m_fMpTextEndPos_65() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fMpTextEndPos_65)); }
	inline float get_m_fMpTextEndPos_65() const { return ___m_fMpTextEndPos_65; }
	inline float* get_address_of_m_fMpTextEndPos_65() { return &___m_fMpTextEndPos_65; }
	inline void set_m_fMpTextEndPos_65(float value)
	{
		___m_fMpTextEndPos_65 = value;
	}

	inline static int32_t get_offset_of_m_fMpTextDistance_66() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fMpTextDistance_66)); }
	inline float get_m_fMpTextDistance_66() const { return ___m_fMpTextDistance_66; }
	inline float* get_address_of_m_fMpTextDistance_66() { return &___m_fMpTextDistance_66; }
	inline void set_m_fMpTextDistance_66(float value)
	{
		___m_fMpTextDistance_66 = value;
	}

	inline static int32_t get_offset_of_m_fMpEffectStartPos_67() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fMpEffectStartPos_67)); }
	inline float get_m_fMpEffectStartPos_67() const { return ___m_fMpEffectStartPos_67; }
	inline float* get_address_of_m_fMpEffectStartPos_67() { return &___m_fMpEffectStartPos_67; }
	inline void set_m_fMpEffectStartPos_67(float value)
	{
		___m_fMpEffectStartPos_67 = value;
	}

	inline static int32_t get_offset_of_m_fMpEffectEndPos_68() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fMpEffectEndPos_68)); }
	inline float get_m_fMpEffectEndPos_68() const { return ___m_fMpEffectEndPos_68; }
	inline float* get_address_of_m_fMpEffectEndPos_68() { return &___m_fMpEffectEndPos_68; }
	inline void set_m_fMpEffectEndPos_68(float value)
	{
		___m_fMpEffectEndPos_68 = value;
	}

	inline static int32_t get_offset_of_m_fMpEffectDistance_69() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_fMpEffectDistance_69)); }
	inline float get_m_fMpEffectDistance_69() const { return ___m_fMpEffectDistance_69; }
	inline float* get_address_of_m_fMpEffectDistance_69() { return &___m_fMpEffectDistance_69; }
	inline void set_m_fMpEffectDistance_69(float value)
	{
		___m_fMpEffectDistance_69 = value;
	}

	inline static int32_t get_offset_of__effectMp_70() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____effectMp_70)); }
	inline CFrameAnimationEffect_t443605508 * get__effectMp_70() const { return ____effectMp_70; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectMp_70() { return &____effectMp_70; }
	inline void set__effectMp_70(CFrameAnimationEffect_t443605508 * value)
	{
		____effectMp_70 = value;
		Il2CppCodeGenWriteBarrier((&____effectMp_70), value);
	}

	inline static int32_t get_offset_of__effectMpStart_71() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____effectMpStart_71)); }
	inline CFrameAnimationEffect_t443605508 * get__effectMpStart_71() const { return ____effectMpStart_71; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectMpStart_71() { return &____effectMpStart_71; }
	inline void set__effectMpStart_71(CFrameAnimationEffect_t443605508 * value)
	{
		____effectMpStart_71 = value;
		Il2CppCodeGenWriteBarrier((&____effectMpStart_71), value);
	}

	inline static int32_t get_offset_of__txtRecoverSpeed_72() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____txtRecoverSpeed_72)); }
	inline Text_t1901882714 * get__txtRecoverSpeed_72() const { return ____txtRecoverSpeed_72; }
	inline Text_t1901882714 ** get_address_of__txtRecoverSpeed_72() { return &____txtRecoverSpeed_72; }
	inline void set__txtRecoverSpeed_72(Text_t1901882714 * value)
	{
		____txtRecoverSpeed_72 = value;
		Il2CppCodeGenWriteBarrier((&____txtRecoverSpeed_72), value);
	}

	inline static int32_t get_offset_of__goMpPanel_PC_73() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____goMpPanel_PC_73)); }
	inline GameObject_t1113636619 * get__goMpPanel_PC_73() const { return ____goMpPanel_PC_73; }
	inline GameObject_t1113636619 ** get_address_of__goMpPanel_PC_73() { return &____goMpPanel_PC_73; }
	inline void set__goMpPanel_PC_73(GameObject_t1113636619 * value)
	{
		____goMpPanel_PC_73 = value;
		Il2CppCodeGenWriteBarrier((&____goMpPanel_PC_73), value);
	}

	inline static int32_t get_offset_of__goMpPanel_Mobile_74() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____goMpPanel_Mobile_74)); }
	inline GameObject_t1113636619 * get__goMpPanel_Mobile_74() const { return ____goMpPanel_Mobile_74; }
	inline GameObject_t1113636619 ** get_address_of__goMpPanel_Mobile_74() { return &____goMpPanel_Mobile_74; }
	inline void set__goMpPanel_Mobile_74(GameObject_t1113636619 * value)
	{
		____goMpPanel_Mobile_74 = value;
		Il2CppCodeGenWriteBarrier((&____goMpPanel_Mobile_74), value);
	}

	inline static int32_t get_offset_of__Player_75() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ____Player_75)); }
	inline Player_t3266647312 * get__Player_75() const { return ____Player_75; }
	inline Player_t3266647312 ** get_address_of__Player_75() { return &____Player_75; }
	inline void set__Player_75(Player_t3266647312 * value)
	{
		____Player_75 = value;
		Il2CppCodeGenWriteBarrier((&____Player_75), value);
	}

	inline static int32_t get_offset_of_m_bUiInited_76() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_bUiInited_76)); }
	inline bool get_m_bUiInited_76() const { return ___m_bUiInited_76; }
	inline bool* get_address_of_m_bUiInited_76() { return &___m_bUiInited_76; }
	inline void set_m_bUiInited_76(bool value)
	{
		___m_bUiInited_76 = value;
	}

	inline static int32_t get_offset_of_tempLevelConfig_77() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___tempLevelConfig_77)); }
	inline sLevelConfig_t4291590291  get_tempLevelConfig_77() const { return ___tempLevelConfig_77; }
	inline sLevelConfig_t4291590291 * get_address_of_tempLevelConfig_77() { return &___tempLevelConfig_77; }
	inline void set_tempLevelConfig_77(sLevelConfig_t4291590291  value)
	{
		___tempLevelConfig_77 = value;
	}

	inline static int32_t get_offset_of_m_bExpChanged_78() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_bExpChanged_78)); }
	inline bool get_m_bExpChanged_78() const { return ___m_bExpChanged_78; }
	inline bool* get_address_of_m_bExpChanged_78() { return &___m_bExpChanged_78; }
	inline void set_m_bExpChanged_78(bool value)
	{
		___m_bExpChanged_78 = value;
	}

	inline static int32_t get_offset_of_m_nExplodeThornNum_79() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339, ___m_nExplodeThornNum_79)); }
	inline int32_t get_m_nExplodeThornNum_79() const { return ___m_nExplodeThornNum_79; }
	inline int32_t* get_address_of_m_nExplodeThornNum_79() { return &___m_nExplodeThornNum_79; }
	inline void set_m_nExplodeThornNum_79(int32_t value)
	{
		___m_nExplodeThornNum_79 = value;
	}
};

struct CGrowSystem_t4101788339_StaticFields
{
public:
	// UnityEngine.Vector3 CGrowSystem::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// CGrowSystem CGrowSystem::s_Instance
	CGrowSystem_t4101788339 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CGrowSystem_t4101788339_StaticFields, ___s_Instance_4)); }
	inline CGrowSystem_t4101788339 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CGrowSystem_t4101788339 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CGrowSystem_t4101788339 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGROWSYSTEM_T4101788339_H
#ifndef GUIACTION_T288362040_H
#define GUIACTION_T288362040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIAction
struct  GUIAction_t288362040  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas GUIAction::_canvas
	Canvas_t3310196443 * ____canvas_3;
	// UnityEngine.GameObject GUIAction::_product_title
	GameObject_t1113636619 * ____product_title_4;
	// UnityEngine.UI.Text GUIAction::_bgm_title
	Text_t1901882714 * ____bgm_title_5;
	// UnityEngine.UI.Button GUIAction::_menu_button
	Button_t4055032469 * ____menu_button_6;
	// UnityEngine.UI.Button GUIAction::_play_button
	Button_t4055032469 * ____play_button_7;
	// UnityEngine.UI.Button GUIAction::_next_button
	Button_t4055032469 * ____next_button_8;
	// UnityEngine.UI.Button GUIAction::_prev_button
	Button_t4055032469 * ____prev_button_9;
	// UnityEngine.UI.Button GUIAction::_home_button
	Button_t4055032469 * ____home_button_10;
	// UnityEngine.UI.Button GUIAction::_shoot_button
	Button_t4055032469 * ____shoot_button_11;
	// UnityEngine.UI.Button GUIAction::_split_button
	Button_t4055032469 * ____split_button_12;
	// UnityEngine.UI.Dropdown GUIAction::_ctrl_mode_dropdown
	Dropdown_t2274391225 * ____ctrl_mode_dropdown_13;
	// UnityEngine.UI.Dropdown GUIAction::_dir_indicator_type_dropdown
	Dropdown_t2274391225 * ____dir_indicator_type_dropdown_14;
	// UnityEngine.UI.Button GUIAction::_spit_spore
	Button_t4055032469 * ____spit_spore_15;
	// UnityEngine.UI.Button GUIAction::_spit_ball
	Button_t4055032469 * ____spit_ball_16;
	// UnityEngine.UI.Button GUIAction::_obscene_unfold
	Button_t4055032469 * ____obscene_unfold_17;
	// UnityEngine.UI.Button GUIAction::_spit_thorn
	Button_t4055032469 * ____spit_thorn_18;
	// UnityEngine.UI.Button GUIAction::_lababa
	Button_t4055032469 * ____lababa_19;
	// UnityEngine.UI.Button GUIAction::_change_skin
	Button_t4055032469 * ____change_skin_20;
	// UnityEngine.UI.Button GUIAction::_show_skill_point_ui
	Button_t4055032469 * ____show_skill_point_ui_21;
	// UnityEngine.GameObject GUIAction::_MainControlPanel
	GameObject_t1113636619 * ____MainControlPanel_22;
	// UnityEngine.GameObject GUIAction::_goSpitBall
	GameObject_t1113636619 * ____goSpitBall_23;
	// UnityEngine.GameObject GUIAction::_goSpitBall_AddPoint
	GameObject_t1113636619 * ____goSpitBall_AddPoint_24;
	// System.Single GUIAction::_rhythm_rotate_factor
	float ____rhythm_rotate_factor_25;
	// System.Single GUIAction::_rhythm_scale_factor
	float ____rhythm_scale_factor_26;

public:
	inline static int32_t get_offset_of__canvas_3() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____canvas_3)); }
	inline Canvas_t3310196443 * get__canvas_3() const { return ____canvas_3; }
	inline Canvas_t3310196443 ** get_address_of__canvas_3() { return &____canvas_3; }
	inline void set__canvas_3(Canvas_t3310196443 * value)
	{
		____canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_3), value);
	}

	inline static int32_t get_offset_of__product_title_4() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____product_title_4)); }
	inline GameObject_t1113636619 * get__product_title_4() const { return ____product_title_4; }
	inline GameObject_t1113636619 ** get_address_of__product_title_4() { return &____product_title_4; }
	inline void set__product_title_4(GameObject_t1113636619 * value)
	{
		____product_title_4 = value;
		Il2CppCodeGenWriteBarrier((&____product_title_4), value);
	}

	inline static int32_t get_offset_of__bgm_title_5() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____bgm_title_5)); }
	inline Text_t1901882714 * get__bgm_title_5() const { return ____bgm_title_5; }
	inline Text_t1901882714 ** get_address_of__bgm_title_5() { return &____bgm_title_5; }
	inline void set__bgm_title_5(Text_t1901882714 * value)
	{
		____bgm_title_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgm_title_5), value);
	}

	inline static int32_t get_offset_of__menu_button_6() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____menu_button_6)); }
	inline Button_t4055032469 * get__menu_button_6() const { return ____menu_button_6; }
	inline Button_t4055032469 ** get_address_of__menu_button_6() { return &____menu_button_6; }
	inline void set__menu_button_6(Button_t4055032469 * value)
	{
		____menu_button_6 = value;
		Il2CppCodeGenWriteBarrier((&____menu_button_6), value);
	}

	inline static int32_t get_offset_of__play_button_7() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____play_button_7)); }
	inline Button_t4055032469 * get__play_button_7() const { return ____play_button_7; }
	inline Button_t4055032469 ** get_address_of__play_button_7() { return &____play_button_7; }
	inline void set__play_button_7(Button_t4055032469 * value)
	{
		____play_button_7 = value;
		Il2CppCodeGenWriteBarrier((&____play_button_7), value);
	}

	inline static int32_t get_offset_of__next_button_8() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____next_button_8)); }
	inline Button_t4055032469 * get__next_button_8() const { return ____next_button_8; }
	inline Button_t4055032469 ** get_address_of__next_button_8() { return &____next_button_8; }
	inline void set__next_button_8(Button_t4055032469 * value)
	{
		____next_button_8 = value;
		Il2CppCodeGenWriteBarrier((&____next_button_8), value);
	}

	inline static int32_t get_offset_of__prev_button_9() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____prev_button_9)); }
	inline Button_t4055032469 * get__prev_button_9() const { return ____prev_button_9; }
	inline Button_t4055032469 ** get_address_of__prev_button_9() { return &____prev_button_9; }
	inline void set__prev_button_9(Button_t4055032469 * value)
	{
		____prev_button_9 = value;
		Il2CppCodeGenWriteBarrier((&____prev_button_9), value);
	}

	inline static int32_t get_offset_of__home_button_10() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____home_button_10)); }
	inline Button_t4055032469 * get__home_button_10() const { return ____home_button_10; }
	inline Button_t4055032469 ** get_address_of__home_button_10() { return &____home_button_10; }
	inline void set__home_button_10(Button_t4055032469 * value)
	{
		____home_button_10 = value;
		Il2CppCodeGenWriteBarrier((&____home_button_10), value);
	}

	inline static int32_t get_offset_of__shoot_button_11() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____shoot_button_11)); }
	inline Button_t4055032469 * get__shoot_button_11() const { return ____shoot_button_11; }
	inline Button_t4055032469 ** get_address_of__shoot_button_11() { return &____shoot_button_11; }
	inline void set__shoot_button_11(Button_t4055032469 * value)
	{
		____shoot_button_11 = value;
		Il2CppCodeGenWriteBarrier((&____shoot_button_11), value);
	}

	inline static int32_t get_offset_of__split_button_12() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____split_button_12)); }
	inline Button_t4055032469 * get__split_button_12() const { return ____split_button_12; }
	inline Button_t4055032469 ** get_address_of__split_button_12() { return &____split_button_12; }
	inline void set__split_button_12(Button_t4055032469 * value)
	{
		____split_button_12 = value;
		Il2CppCodeGenWriteBarrier((&____split_button_12), value);
	}

	inline static int32_t get_offset_of__ctrl_mode_dropdown_13() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____ctrl_mode_dropdown_13)); }
	inline Dropdown_t2274391225 * get__ctrl_mode_dropdown_13() const { return ____ctrl_mode_dropdown_13; }
	inline Dropdown_t2274391225 ** get_address_of__ctrl_mode_dropdown_13() { return &____ctrl_mode_dropdown_13; }
	inline void set__ctrl_mode_dropdown_13(Dropdown_t2274391225 * value)
	{
		____ctrl_mode_dropdown_13 = value;
		Il2CppCodeGenWriteBarrier((&____ctrl_mode_dropdown_13), value);
	}

	inline static int32_t get_offset_of__dir_indicator_type_dropdown_14() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____dir_indicator_type_dropdown_14)); }
	inline Dropdown_t2274391225 * get__dir_indicator_type_dropdown_14() const { return ____dir_indicator_type_dropdown_14; }
	inline Dropdown_t2274391225 ** get_address_of__dir_indicator_type_dropdown_14() { return &____dir_indicator_type_dropdown_14; }
	inline void set__dir_indicator_type_dropdown_14(Dropdown_t2274391225 * value)
	{
		____dir_indicator_type_dropdown_14 = value;
		Il2CppCodeGenWriteBarrier((&____dir_indicator_type_dropdown_14), value);
	}

	inline static int32_t get_offset_of__spit_spore_15() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____spit_spore_15)); }
	inline Button_t4055032469 * get__spit_spore_15() const { return ____spit_spore_15; }
	inline Button_t4055032469 ** get_address_of__spit_spore_15() { return &____spit_spore_15; }
	inline void set__spit_spore_15(Button_t4055032469 * value)
	{
		____spit_spore_15 = value;
		Il2CppCodeGenWriteBarrier((&____spit_spore_15), value);
	}

	inline static int32_t get_offset_of__spit_ball_16() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____spit_ball_16)); }
	inline Button_t4055032469 * get__spit_ball_16() const { return ____spit_ball_16; }
	inline Button_t4055032469 ** get_address_of__spit_ball_16() { return &____spit_ball_16; }
	inline void set__spit_ball_16(Button_t4055032469 * value)
	{
		____spit_ball_16 = value;
		Il2CppCodeGenWriteBarrier((&____spit_ball_16), value);
	}

	inline static int32_t get_offset_of__obscene_unfold_17() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____obscene_unfold_17)); }
	inline Button_t4055032469 * get__obscene_unfold_17() const { return ____obscene_unfold_17; }
	inline Button_t4055032469 ** get_address_of__obscene_unfold_17() { return &____obscene_unfold_17; }
	inline void set__obscene_unfold_17(Button_t4055032469 * value)
	{
		____obscene_unfold_17 = value;
		Il2CppCodeGenWriteBarrier((&____obscene_unfold_17), value);
	}

	inline static int32_t get_offset_of__spit_thorn_18() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____spit_thorn_18)); }
	inline Button_t4055032469 * get__spit_thorn_18() const { return ____spit_thorn_18; }
	inline Button_t4055032469 ** get_address_of__spit_thorn_18() { return &____spit_thorn_18; }
	inline void set__spit_thorn_18(Button_t4055032469 * value)
	{
		____spit_thorn_18 = value;
		Il2CppCodeGenWriteBarrier((&____spit_thorn_18), value);
	}

	inline static int32_t get_offset_of__lababa_19() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____lababa_19)); }
	inline Button_t4055032469 * get__lababa_19() const { return ____lababa_19; }
	inline Button_t4055032469 ** get_address_of__lababa_19() { return &____lababa_19; }
	inline void set__lababa_19(Button_t4055032469 * value)
	{
		____lababa_19 = value;
		Il2CppCodeGenWriteBarrier((&____lababa_19), value);
	}

	inline static int32_t get_offset_of__change_skin_20() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____change_skin_20)); }
	inline Button_t4055032469 * get__change_skin_20() const { return ____change_skin_20; }
	inline Button_t4055032469 ** get_address_of__change_skin_20() { return &____change_skin_20; }
	inline void set__change_skin_20(Button_t4055032469 * value)
	{
		____change_skin_20 = value;
		Il2CppCodeGenWriteBarrier((&____change_skin_20), value);
	}

	inline static int32_t get_offset_of__show_skill_point_ui_21() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____show_skill_point_ui_21)); }
	inline Button_t4055032469 * get__show_skill_point_ui_21() const { return ____show_skill_point_ui_21; }
	inline Button_t4055032469 ** get_address_of__show_skill_point_ui_21() { return &____show_skill_point_ui_21; }
	inline void set__show_skill_point_ui_21(Button_t4055032469 * value)
	{
		____show_skill_point_ui_21 = value;
		Il2CppCodeGenWriteBarrier((&____show_skill_point_ui_21), value);
	}

	inline static int32_t get_offset_of__MainControlPanel_22() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____MainControlPanel_22)); }
	inline GameObject_t1113636619 * get__MainControlPanel_22() const { return ____MainControlPanel_22; }
	inline GameObject_t1113636619 ** get_address_of__MainControlPanel_22() { return &____MainControlPanel_22; }
	inline void set__MainControlPanel_22(GameObject_t1113636619 * value)
	{
		____MainControlPanel_22 = value;
		Il2CppCodeGenWriteBarrier((&____MainControlPanel_22), value);
	}

	inline static int32_t get_offset_of__goSpitBall_23() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____goSpitBall_23)); }
	inline GameObject_t1113636619 * get__goSpitBall_23() const { return ____goSpitBall_23; }
	inline GameObject_t1113636619 ** get_address_of__goSpitBall_23() { return &____goSpitBall_23; }
	inline void set__goSpitBall_23(GameObject_t1113636619 * value)
	{
		____goSpitBall_23 = value;
		Il2CppCodeGenWriteBarrier((&____goSpitBall_23), value);
	}

	inline static int32_t get_offset_of__goSpitBall_AddPoint_24() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____goSpitBall_AddPoint_24)); }
	inline GameObject_t1113636619 * get__goSpitBall_AddPoint_24() const { return ____goSpitBall_AddPoint_24; }
	inline GameObject_t1113636619 ** get_address_of__goSpitBall_AddPoint_24() { return &____goSpitBall_AddPoint_24; }
	inline void set__goSpitBall_AddPoint_24(GameObject_t1113636619 * value)
	{
		____goSpitBall_AddPoint_24 = value;
		Il2CppCodeGenWriteBarrier((&____goSpitBall_AddPoint_24), value);
	}

	inline static int32_t get_offset_of__rhythm_rotate_factor_25() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____rhythm_rotate_factor_25)); }
	inline float get__rhythm_rotate_factor_25() const { return ____rhythm_rotate_factor_25; }
	inline float* get_address_of__rhythm_rotate_factor_25() { return &____rhythm_rotate_factor_25; }
	inline void set__rhythm_rotate_factor_25(float value)
	{
		____rhythm_rotate_factor_25 = value;
	}

	inline static int32_t get_offset_of__rhythm_scale_factor_26() { return static_cast<int32_t>(offsetof(GUIAction_t288362040, ____rhythm_scale_factor_26)); }
	inline float get__rhythm_scale_factor_26() const { return ____rhythm_scale_factor_26; }
	inline float* get_address_of__rhythm_scale_factor_26() { return &____rhythm_scale_factor_26; }
	inline void set__rhythm_scale_factor_26(float value)
	{
		____rhythm_scale_factor_26 = value;
	}
};

struct GUIAction_t288362040_StaticFields
{
public:
	// GUIAction GUIAction::s_Instance
	GUIAction_t288362040 * ___s_Instance_27;

public:
	inline static int32_t get_offset_of_s_Instance_27() { return static_cast<int32_t>(offsetof(GUIAction_t288362040_StaticFields, ___s_Instance_27)); }
	inline GUIAction_t288362040 * get_s_Instance_27() const { return ___s_Instance_27; }
	inline GUIAction_t288362040 ** get_address_of_s_Instance_27() { return &___s_Instance_27; }
	inline void set_s_Instance_27(GUIAction_t288362040 * value)
	{
		___s_Instance_27 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIACTION_T288362040_H
#ifndef CCOSMOSGUNSIGHT_T3630013301_H
#define CCOSMOSGUNSIGHT_T3630013301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCosmosGunSight
struct  CCosmosGunSight_t3630013301  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CCosmosGunSight::m_goHead
	GameObject_t1113636619 * ___m_goHead_4;
	// UnityEngine.GameObject CCosmosGunSight::m_goJoint
	GameObject_t1113636619 * ___m_goJoint_5;
	// UnityEngine.GameObject CCosmosGunSight::m_goStick
	GameObject_t1113636619 * ___m_goStick_6;
	// System.Single CCosmosGunSight::m_fAngle
	float ___m_fAngle_7;
	// System.Single CCosmosGunSight::m_fChildSize
	float ___m_fChildSize_8;
	// System.Single CCosmosGunSight::m_fDistanceForStick
	float ___m_fDistanceForStick_9;
	// System.Single CCosmosGunSight::m_fTotalDistance
	float ___m_fTotalDistance_10;
	// System.Boolean CCosmosGunSight::m_bPicked
	bool ___m_bPicked_11;

public:
	inline static int32_t get_offset_of_m_goHead_4() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_goHead_4)); }
	inline GameObject_t1113636619 * get_m_goHead_4() const { return ___m_goHead_4; }
	inline GameObject_t1113636619 ** get_address_of_m_goHead_4() { return &___m_goHead_4; }
	inline void set_m_goHead_4(GameObject_t1113636619 * value)
	{
		___m_goHead_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_goHead_4), value);
	}

	inline static int32_t get_offset_of_m_goJoint_5() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_goJoint_5)); }
	inline GameObject_t1113636619 * get_m_goJoint_5() const { return ___m_goJoint_5; }
	inline GameObject_t1113636619 ** get_address_of_m_goJoint_5() { return &___m_goJoint_5; }
	inline void set_m_goJoint_5(GameObject_t1113636619 * value)
	{
		___m_goJoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_goJoint_5), value);
	}

	inline static int32_t get_offset_of_m_goStick_6() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_goStick_6)); }
	inline GameObject_t1113636619 * get_m_goStick_6() const { return ___m_goStick_6; }
	inline GameObject_t1113636619 ** get_address_of_m_goStick_6() { return &___m_goStick_6; }
	inline void set_m_goStick_6(GameObject_t1113636619 * value)
	{
		___m_goStick_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_goStick_6), value);
	}

	inline static int32_t get_offset_of_m_fAngle_7() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_fAngle_7)); }
	inline float get_m_fAngle_7() const { return ___m_fAngle_7; }
	inline float* get_address_of_m_fAngle_7() { return &___m_fAngle_7; }
	inline void set_m_fAngle_7(float value)
	{
		___m_fAngle_7 = value;
	}

	inline static int32_t get_offset_of_m_fChildSize_8() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_fChildSize_8)); }
	inline float get_m_fChildSize_8() const { return ___m_fChildSize_8; }
	inline float* get_address_of_m_fChildSize_8() { return &___m_fChildSize_8; }
	inline void set_m_fChildSize_8(float value)
	{
		___m_fChildSize_8 = value;
	}

	inline static int32_t get_offset_of_m_fDistanceForStick_9() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_fDistanceForStick_9)); }
	inline float get_m_fDistanceForStick_9() const { return ___m_fDistanceForStick_9; }
	inline float* get_address_of_m_fDistanceForStick_9() { return &___m_fDistanceForStick_9; }
	inline void set_m_fDistanceForStick_9(float value)
	{
		___m_fDistanceForStick_9 = value;
	}

	inline static int32_t get_offset_of_m_fTotalDistance_10() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_fTotalDistance_10)); }
	inline float get_m_fTotalDistance_10() const { return ___m_fTotalDistance_10; }
	inline float* get_address_of_m_fTotalDistance_10() { return &___m_fTotalDistance_10; }
	inline void set_m_fTotalDistance_10(float value)
	{
		___m_fTotalDistance_10 = value;
	}

	inline static int32_t get_offset_of_m_bPicked_11() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301, ___m_bPicked_11)); }
	inline bool get_m_bPicked_11() const { return ___m_bPicked_11; }
	inline bool* get_address_of_m_bPicked_11() { return &___m_bPicked_11; }
	inline void set_m_bPicked_11(bool value)
	{
		___m_bPicked_11 = value;
	}
};

struct CCosmosGunSight_t3630013301_StaticFields
{
public:
	// UnityEngine.Vector3 CCosmosGunSight::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCosmosGunSight::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCosmosGunSight_t3630013301_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOSMOSGUNSIGHT_T3630013301_H
#ifndef CSHAKECAMERA_T3305427617_H
#define CSHAKECAMERA_T3305427617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CShakeCamera
struct  CShakeCamera_t3305427617  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CShakeCamera::isshakeCamera
	bool ___isshakeCamera_3;
	// System.Single CShakeCamera::shakeLevel
	float ___shakeLevel_4;
	// System.Single CShakeCamera::setShakeTime
	float ___setShakeTime_5;
	// System.Single CShakeCamera::shakeFps
	float ___shakeFps_6;
	// System.Single CShakeCamera::fps
	float ___fps_7;
	// System.Single CShakeCamera::shakeTime
	float ___shakeTime_8;
	// System.Single CShakeCamera::frameTime
	float ___frameTime_9;
	// System.Single CShakeCamera::shakeDelta
	float ___shakeDelta_10;
	// UnityEngine.Camera CShakeCamera::selfCamera
	Camera_t4157153871 * ___selfCamera_11;
	// UnityEngine.Rect CShakeCamera::changeRect
	Rect_t2360479859  ___changeRect_12;

public:
	inline static int32_t get_offset_of_isshakeCamera_3() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___isshakeCamera_3)); }
	inline bool get_isshakeCamera_3() const { return ___isshakeCamera_3; }
	inline bool* get_address_of_isshakeCamera_3() { return &___isshakeCamera_3; }
	inline void set_isshakeCamera_3(bool value)
	{
		___isshakeCamera_3 = value;
	}

	inline static int32_t get_offset_of_shakeLevel_4() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___shakeLevel_4)); }
	inline float get_shakeLevel_4() const { return ___shakeLevel_4; }
	inline float* get_address_of_shakeLevel_4() { return &___shakeLevel_4; }
	inline void set_shakeLevel_4(float value)
	{
		___shakeLevel_4 = value;
	}

	inline static int32_t get_offset_of_setShakeTime_5() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___setShakeTime_5)); }
	inline float get_setShakeTime_5() const { return ___setShakeTime_5; }
	inline float* get_address_of_setShakeTime_5() { return &___setShakeTime_5; }
	inline void set_setShakeTime_5(float value)
	{
		___setShakeTime_5 = value;
	}

	inline static int32_t get_offset_of_shakeFps_6() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___shakeFps_6)); }
	inline float get_shakeFps_6() const { return ___shakeFps_6; }
	inline float* get_address_of_shakeFps_6() { return &___shakeFps_6; }
	inline void set_shakeFps_6(float value)
	{
		___shakeFps_6 = value;
	}

	inline static int32_t get_offset_of_fps_7() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___fps_7)); }
	inline float get_fps_7() const { return ___fps_7; }
	inline float* get_address_of_fps_7() { return &___fps_7; }
	inline void set_fps_7(float value)
	{
		___fps_7 = value;
	}

	inline static int32_t get_offset_of_shakeTime_8() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___shakeTime_8)); }
	inline float get_shakeTime_8() const { return ___shakeTime_8; }
	inline float* get_address_of_shakeTime_8() { return &___shakeTime_8; }
	inline void set_shakeTime_8(float value)
	{
		___shakeTime_8 = value;
	}

	inline static int32_t get_offset_of_frameTime_9() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___frameTime_9)); }
	inline float get_frameTime_9() const { return ___frameTime_9; }
	inline float* get_address_of_frameTime_9() { return &___frameTime_9; }
	inline void set_frameTime_9(float value)
	{
		___frameTime_9 = value;
	}

	inline static int32_t get_offset_of_shakeDelta_10() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___shakeDelta_10)); }
	inline float get_shakeDelta_10() const { return ___shakeDelta_10; }
	inline float* get_address_of_shakeDelta_10() { return &___shakeDelta_10; }
	inline void set_shakeDelta_10(float value)
	{
		___shakeDelta_10 = value;
	}

	inline static int32_t get_offset_of_selfCamera_11() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___selfCamera_11)); }
	inline Camera_t4157153871 * get_selfCamera_11() const { return ___selfCamera_11; }
	inline Camera_t4157153871 ** get_address_of_selfCamera_11() { return &___selfCamera_11; }
	inline void set_selfCamera_11(Camera_t4157153871 * value)
	{
		___selfCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___selfCamera_11), value);
	}

	inline static int32_t get_offset_of_changeRect_12() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617, ___changeRect_12)); }
	inline Rect_t2360479859  get_changeRect_12() const { return ___changeRect_12; }
	inline Rect_t2360479859 * get_address_of_changeRect_12() { return &___changeRect_12; }
	inline void set_changeRect_12(Rect_t2360479859  value)
	{
		___changeRect_12 = value;
	}
};

struct CShakeCamera_t3305427617_StaticFields
{
public:
	// CShakeCamera CShakeCamera::s_Instance
	CShakeCamera_t3305427617 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CShakeCamera_t3305427617_StaticFields, ___s_Instance_2)); }
	inline CShakeCamera_t3305427617 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CShakeCamera_t3305427617 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CShakeCamera_t3305427617 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHAKECAMERA_T3305427617_H
#ifndef CEFFECT_T3687947224_H
#define CEFFECT_T3687947224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEffect
struct  CEffect_t3687947224  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CEffect::m_fRotateSpeed
	float ___m_fRotateSpeed_2;
	// System.Single CEffect::m_fRotationZ
	float ___m_fRotationZ_3;
	// UnityEngine.SpriteRenderer CEffect::_sprMain
	SpriteRenderer_t3235626157 * ____sprMain_6;
	// UnityEngine.UI.Image CEffect::_imgMain
	Image_t2670269651 * ____imgMain_7;
	// UnityEngine.SpriteRenderer CEffect::_sprWhite
	SpriteRenderer_t3235626157 * ____sprWhite_8;

public:
	inline static int32_t get_offset_of_m_fRotateSpeed_2() { return static_cast<int32_t>(offsetof(CEffect_t3687947224, ___m_fRotateSpeed_2)); }
	inline float get_m_fRotateSpeed_2() const { return ___m_fRotateSpeed_2; }
	inline float* get_address_of_m_fRotateSpeed_2() { return &___m_fRotateSpeed_2; }
	inline void set_m_fRotateSpeed_2(float value)
	{
		___m_fRotateSpeed_2 = value;
	}

	inline static int32_t get_offset_of_m_fRotationZ_3() { return static_cast<int32_t>(offsetof(CEffect_t3687947224, ___m_fRotationZ_3)); }
	inline float get_m_fRotationZ_3() const { return ___m_fRotationZ_3; }
	inline float* get_address_of_m_fRotationZ_3() { return &___m_fRotationZ_3; }
	inline void set_m_fRotationZ_3(float value)
	{
		___m_fRotationZ_3 = value;
	}

	inline static int32_t get_offset_of__sprMain_6() { return static_cast<int32_t>(offsetof(CEffect_t3687947224, ____sprMain_6)); }
	inline SpriteRenderer_t3235626157 * get__sprMain_6() const { return ____sprMain_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprMain_6() { return &____sprMain_6; }
	inline void set__sprMain_6(SpriteRenderer_t3235626157 * value)
	{
		____sprMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprMain_6), value);
	}

	inline static int32_t get_offset_of__imgMain_7() { return static_cast<int32_t>(offsetof(CEffect_t3687947224, ____imgMain_7)); }
	inline Image_t2670269651 * get__imgMain_7() const { return ____imgMain_7; }
	inline Image_t2670269651 ** get_address_of__imgMain_7() { return &____imgMain_7; }
	inline void set__imgMain_7(Image_t2670269651 * value)
	{
		____imgMain_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_7), value);
	}

	inline static int32_t get_offset_of__sprWhite_8() { return static_cast<int32_t>(offsetof(CEffect_t3687947224, ____sprWhite_8)); }
	inline SpriteRenderer_t3235626157 * get__sprWhite_8() const { return ____sprWhite_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprWhite_8() { return &____sprWhite_8; }
	inline void set__sprWhite_8(SpriteRenderer_t3235626157 * value)
	{
		____sprWhite_8 = value;
		Il2CppCodeGenWriteBarrier((&____sprWhite_8), value);
	}
};

struct CEffect_t3687947224_StaticFields
{
public:
	// UnityEngine.Vector3 CEffect::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;
	// UnityEngine.Vector3 CEffect::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;

public:
	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CEffect_t3687947224_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CEffect_t3687947224_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CEFFECT_T3687947224_H
#ifndef BLUR_T1038294851_H
#define BLUR_T1038294851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Blur
struct  Blur_t1038294851  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Blur::iterations
	int32_t ___iterations_2;
	// System.Single UnityStandardAssets.ImageEffects.Blur::blurSpread
	float ___blurSpread_3;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Blur::blurShader
	Shader_t4151988712 * ___blurShader_4;

public:
	inline static int32_t get_offset_of_iterations_2() { return static_cast<int32_t>(offsetof(Blur_t1038294851, ___iterations_2)); }
	inline int32_t get_iterations_2() const { return ___iterations_2; }
	inline int32_t* get_address_of_iterations_2() { return &___iterations_2; }
	inline void set_iterations_2(int32_t value)
	{
		___iterations_2 = value;
	}

	inline static int32_t get_offset_of_blurSpread_3() { return static_cast<int32_t>(offsetof(Blur_t1038294851, ___blurSpread_3)); }
	inline float get_blurSpread_3() const { return ___blurSpread_3; }
	inline float* get_address_of_blurSpread_3() { return &___blurSpread_3; }
	inline void set_blurSpread_3(float value)
	{
		___blurSpread_3 = value;
	}

	inline static int32_t get_offset_of_blurShader_4() { return static_cast<int32_t>(offsetof(Blur_t1038294851, ___blurShader_4)); }
	inline Shader_t4151988712 * get_blurShader_4() const { return ___blurShader_4; }
	inline Shader_t4151988712 ** get_address_of_blurShader_4() { return &___blurShader_4; }
	inline void set_blurShader_4(Shader_t4151988712 * value)
	{
		___blurShader_4 = value;
		Il2CppCodeGenWriteBarrier((&___blurShader_4), value);
	}
};

struct Blur_t1038294851_StaticFields
{
public:
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::m_Material
	Material_t340375123 * ___m_Material_5;

public:
	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(Blur_t1038294851_StaticFields, ___m_Material_5)); }
	inline Material_t340375123 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t340375123 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t340375123 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUR_T1038294851_H
#ifndef POSTEFFECTSHELPER_T675066462_H
#define POSTEFFECTSHELPER_T675066462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsHelper
struct  PostEffectsHelper_t675066462  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSHELPER_T675066462_H
#ifndef POSTEFFECTSBASE_T2404315739_H
#define POSTEFFECTSBASE_T2404315739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_t2404315739  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_2;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_3;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_4;
	// System.Collections.Generic.List`1<UnityEngine.Material> UnityStandardAssets.ImageEffects.PostEffectsBase::createdMaterials
	List_1_t1812449865 * ___createdMaterials_5;

public:
	inline static int32_t get_offset_of_supportHDRTextures_2() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___supportHDRTextures_2)); }
	inline bool get_supportHDRTextures_2() const { return ___supportHDRTextures_2; }
	inline bool* get_address_of_supportHDRTextures_2() { return &___supportHDRTextures_2; }
	inline void set_supportHDRTextures_2(bool value)
	{
		___supportHDRTextures_2 = value;
	}

	inline static int32_t get_offset_of_supportDX11_3() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___supportDX11_3)); }
	inline bool get_supportDX11_3() const { return ___supportDX11_3; }
	inline bool* get_address_of_supportDX11_3() { return &___supportDX11_3; }
	inline void set_supportDX11_3(bool value)
	{
		___supportDX11_3 = value;
	}

	inline static int32_t get_offset_of_isSupported_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___isSupported_4)); }
	inline bool get_isSupported_4() const { return ___isSupported_4; }
	inline bool* get_address_of_isSupported_4() { return &___isSupported_4; }
	inline void set_isSupported_4(bool value)
	{
		___isSupported_4 = value;
	}

	inline static int32_t get_offset_of_createdMaterials_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___createdMaterials_5)); }
	inline List_1_t1812449865 * get_createdMaterials_5() const { return ___createdMaterials_5; }
	inline List_1_t1812449865 ** get_address_of_createdMaterials_5() { return &___createdMaterials_5; }
	inline void set_createdMaterials_5(List_1_t1812449865 * value)
	{
		___createdMaterials_5 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_T2404315739_H
#ifndef NOISEANDSCRATCHES_T1457296845_H
#define NOISEANDSCRATCHES_T1457296845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.NoiseAndScratches
struct  NoiseAndScratches_t1457296845  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndScratches::monochrome
	bool ___monochrome_2;
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndScratches::rgbFallback
	bool ___rgbFallback_3;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::grainIntensityMin
	float ___grainIntensityMin_4;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::grainIntensityMax
	float ___grainIntensityMax_5;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::grainSize
	float ___grainSize_6;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchIntensityMin
	float ___scratchIntensityMin_7;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchIntensityMax
	float ___scratchIntensityMax_8;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchFPS
	float ___scratchFPS_9;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchJitter
	float ___scratchJitter_10;
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.NoiseAndScratches::grainTexture
	Texture_t3661962703 * ___grainTexture_11;
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchTexture
	Texture_t3661962703 * ___scratchTexture_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndScratches::shaderRGB
	Shader_t4151988712 * ___shaderRGB_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndScratches::shaderYUV
	Shader_t4151988712 * ___shaderYUV_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::m_MaterialRGB
	Material_t340375123 * ___m_MaterialRGB_15;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::m_MaterialYUV
	Material_t340375123 * ___m_MaterialYUV_16;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchTimeLeft
	float ___scratchTimeLeft_17;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchX
	float ___scratchX_18;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndScratches::scratchY
	float ___scratchY_19;

public:
	inline static int32_t get_offset_of_monochrome_2() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___monochrome_2)); }
	inline bool get_monochrome_2() const { return ___monochrome_2; }
	inline bool* get_address_of_monochrome_2() { return &___monochrome_2; }
	inline void set_monochrome_2(bool value)
	{
		___monochrome_2 = value;
	}

	inline static int32_t get_offset_of_rgbFallback_3() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___rgbFallback_3)); }
	inline bool get_rgbFallback_3() const { return ___rgbFallback_3; }
	inline bool* get_address_of_rgbFallback_3() { return &___rgbFallback_3; }
	inline void set_rgbFallback_3(bool value)
	{
		___rgbFallback_3 = value;
	}

	inline static int32_t get_offset_of_grainIntensityMin_4() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___grainIntensityMin_4)); }
	inline float get_grainIntensityMin_4() const { return ___grainIntensityMin_4; }
	inline float* get_address_of_grainIntensityMin_4() { return &___grainIntensityMin_4; }
	inline void set_grainIntensityMin_4(float value)
	{
		___grainIntensityMin_4 = value;
	}

	inline static int32_t get_offset_of_grainIntensityMax_5() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___grainIntensityMax_5)); }
	inline float get_grainIntensityMax_5() const { return ___grainIntensityMax_5; }
	inline float* get_address_of_grainIntensityMax_5() { return &___grainIntensityMax_5; }
	inline void set_grainIntensityMax_5(float value)
	{
		___grainIntensityMax_5 = value;
	}

	inline static int32_t get_offset_of_grainSize_6() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___grainSize_6)); }
	inline float get_grainSize_6() const { return ___grainSize_6; }
	inline float* get_address_of_grainSize_6() { return &___grainSize_6; }
	inline void set_grainSize_6(float value)
	{
		___grainSize_6 = value;
	}

	inline static int32_t get_offset_of_scratchIntensityMin_7() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchIntensityMin_7)); }
	inline float get_scratchIntensityMin_7() const { return ___scratchIntensityMin_7; }
	inline float* get_address_of_scratchIntensityMin_7() { return &___scratchIntensityMin_7; }
	inline void set_scratchIntensityMin_7(float value)
	{
		___scratchIntensityMin_7 = value;
	}

	inline static int32_t get_offset_of_scratchIntensityMax_8() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchIntensityMax_8)); }
	inline float get_scratchIntensityMax_8() const { return ___scratchIntensityMax_8; }
	inline float* get_address_of_scratchIntensityMax_8() { return &___scratchIntensityMax_8; }
	inline void set_scratchIntensityMax_8(float value)
	{
		___scratchIntensityMax_8 = value;
	}

	inline static int32_t get_offset_of_scratchFPS_9() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchFPS_9)); }
	inline float get_scratchFPS_9() const { return ___scratchFPS_9; }
	inline float* get_address_of_scratchFPS_9() { return &___scratchFPS_9; }
	inline void set_scratchFPS_9(float value)
	{
		___scratchFPS_9 = value;
	}

	inline static int32_t get_offset_of_scratchJitter_10() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchJitter_10)); }
	inline float get_scratchJitter_10() const { return ___scratchJitter_10; }
	inline float* get_address_of_scratchJitter_10() { return &___scratchJitter_10; }
	inline void set_scratchJitter_10(float value)
	{
		___scratchJitter_10 = value;
	}

	inline static int32_t get_offset_of_grainTexture_11() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___grainTexture_11)); }
	inline Texture_t3661962703 * get_grainTexture_11() const { return ___grainTexture_11; }
	inline Texture_t3661962703 ** get_address_of_grainTexture_11() { return &___grainTexture_11; }
	inline void set_grainTexture_11(Texture_t3661962703 * value)
	{
		___grainTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___grainTexture_11), value);
	}

	inline static int32_t get_offset_of_scratchTexture_12() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchTexture_12)); }
	inline Texture_t3661962703 * get_scratchTexture_12() const { return ___scratchTexture_12; }
	inline Texture_t3661962703 ** get_address_of_scratchTexture_12() { return &___scratchTexture_12; }
	inline void set_scratchTexture_12(Texture_t3661962703 * value)
	{
		___scratchTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___scratchTexture_12), value);
	}

	inline static int32_t get_offset_of_shaderRGB_13() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___shaderRGB_13)); }
	inline Shader_t4151988712 * get_shaderRGB_13() const { return ___shaderRGB_13; }
	inline Shader_t4151988712 ** get_address_of_shaderRGB_13() { return &___shaderRGB_13; }
	inline void set_shaderRGB_13(Shader_t4151988712 * value)
	{
		___shaderRGB_13 = value;
		Il2CppCodeGenWriteBarrier((&___shaderRGB_13), value);
	}

	inline static int32_t get_offset_of_shaderYUV_14() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___shaderYUV_14)); }
	inline Shader_t4151988712 * get_shaderYUV_14() const { return ___shaderYUV_14; }
	inline Shader_t4151988712 ** get_address_of_shaderYUV_14() { return &___shaderYUV_14; }
	inline void set_shaderYUV_14(Shader_t4151988712 * value)
	{
		___shaderYUV_14 = value;
		Il2CppCodeGenWriteBarrier((&___shaderYUV_14), value);
	}

	inline static int32_t get_offset_of_m_MaterialRGB_15() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___m_MaterialRGB_15)); }
	inline Material_t340375123 * get_m_MaterialRGB_15() const { return ___m_MaterialRGB_15; }
	inline Material_t340375123 ** get_address_of_m_MaterialRGB_15() { return &___m_MaterialRGB_15; }
	inline void set_m_MaterialRGB_15(Material_t340375123 * value)
	{
		___m_MaterialRGB_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialRGB_15), value);
	}

	inline static int32_t get_offset_of_m_MaterialYUV_16() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___m_MaterialYUV_16)); }
	inline Material_t340375123 * get_m_MaterialYUV_16() const { return ___m_MaterialYUV_16; }
	inline Material_t340375123 ** get_address_of_m_MaterialYUV_16() { return &___m_MaterialYUV_16; }
	inline void set_m_MaterialYUV_16(Material_t340375123 * value)
	{
		___m_MaterialYUV_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialYUV_16), value);
	}

	inline static int32_t get_offset_of_scratchTimeLeft_17() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchTimeLeft_17)); }
	inline float get_scratchTimeLeft_17() const { return ___scratchTimeLeft_17; }
	inline float* get_address_of_scratchTimeLeft_17() { return &___scratchTimeLeft_17; }
	inline void set_scratchTimeLeft_17(float value)
	{
		___scratchTimeLeft_17 = value;
	}

	inline static int32_t get_offset_of_scratchX_18() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchX_18)); }
	inline float get_scratchX_18() const { return ___scratchX_18; }
	inline float* get_address_of_scratchX_18() { return &___scratchX_18; }
	inline void set_scratchX_18(float value)
	{
		___scratchX_18 = value;
	}

	inline static int32_t get_offset_of_scratchY_19() { return static_cast<int32_t>(offsetof(NoiseAndScratches_t1457296845, ___scratchY_19)); }
	inline float get_scratchY_19() const { return ___scratchY_19; }
	inline float* get_address_of_scratchY_19() { return &___scratchY_19; }
	inline void set_scratchY_19(float value)
	{
		___scratchY_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISEANDSCRATCHES_T1457296845_H
#ifndef IMAGEEFFECTBASE_T2026006575_H
#define IMAGEEFFECTBASE_T2026006575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffectBase
struct  ImageEffectBase_t2026006575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ImageEffectBase::shader
	Shader_t4151988712 * ___shader_2;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::m_Material
	Material_t340375123 * ___m_Material_3;

public:
	inline static int32_t get_offset_of_shader_2() { return static_cast<int32_t>(offsetof(ImageEffectBase_t2026006575, ___shader_2)); }
	inline Shader_t4151988712 * get_shader_2() const { return ___shader_2; }
	inline Shader_t4151988712 ** get_address_of_shader_2() { return &___shader_2; }
	inline void set_shader_2(Shader_t4151988712 * value)
	{
		___shader_2 = value;
		Il2CppCodeGenWriteBarrier((&___shader_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ImageEffectBase_t2026006575, ___m_Material_3)); }
	inline Material_t340375123 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t340375123 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t340375123 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTBASE_T2026006575_H
#ifndef CONTRASTSTRETCH_T3424449263_H
#define CONTRASTSTRETCH_T3424449263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ContrastStretch
struct  ContrastStretch_t3424449263  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ContrastStretch::adaptationSpeed
	float ___adaptationSpeed_2;
	// System.Single UnityStandardAssets.ImageEffects.ContrastStretch::limitMinimum
	float ___limitMinimum_3;
	// System.Single UnityStandardAssets.ImageEffects.ContrastStretch::limitMaximum
	float ___limitMaximum_4;
	// UnityEngine.RenderTexture[] UnityStandardAssets.ImageEffects.ContrastStretch::adaptRenderTex
	RenderTextureU5BU5D_t4111643188* ___adaptRenderTex_5;
	// System.Int32 UnityStandardAssets.ImageEffects.ContrastStretch::curAdaptIndex
	int32_t ___curAdaptIndex_6;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderLum
	Shader_t4151988712 * ___shaderLum_7;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialLum
	Material_t340375123 * ___m_materialLum_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderReduce
	Shader_t4151988712 * ___shaderReduce_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialReduce
	Material_t340375123 * ___m_materialReduce_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderAdapt
	Shader_t4151988712 * ___shaderAdapt_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialAdapt
	Material_t340375123 * ___m_materialAdapt_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastStretch::shaderApply
	Shader_t4151988712 * ___shaderApply_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::m_materialApply
	Material_t340375123 * ___m_materialApply_14;

public:
	inline static int32_t get_offset_of_adaptationSpeed_2() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___adaptationSpeed_2)); }
	inline float get_adaptationSpeed_2() const { return ___adaptationSpeed_2; }
	inline float* get_address_of_adaptationSpeed_2() { return &___adaptationSpeed_2; }
	inline void set_adaptationSpeed_2(float value)
	{
		___adaptationSpeed_2 = value;
	}

	inline static int32_t get_offset_of_limitMinimum_3() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___limitMinimum_3)); }
	inline float get_limitMinimum_3() const { return ___limitMinimum_3; }
	inline float* get_address_of_limitMinimum_3() { return &___limitMinimum_3; }
	inline void set_limitMinimum_3(float value)
	{
		___limitMinimum_3 = value;
	}

	inline static int32_t get_offset_of_limitMaximum_4() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___limitMaximum_4)); }
	inline float get_limitMaximum_4() const { return ___limitMaximum_4; }
	inline float* get_address_of_limitMaximum_4() { return &___limitMaximum_4; }
	inline void set_limitMaximum_4(float value)
	{
		___limitMaximum_4 = value;
	}

	inline static int32_t get_offset_of_adaptRenderTex_5() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___adaptRenderTex_5)); }
	inline RenderTextureU5BU5D_t4111643188* get_adaptRenderTex_5() const { return ___adaptRenderTex_5; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_adaptRenderTex_5() { return &___adaptRenderTex_5; }
	inline void set_adaptRenderTex_5(RenderTextureU5BU5D_t4111643188* value)
	{
		___adaptRenderTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___adaptRenderTex_5), value);
	}

	inline static int32_t get_offset_of_curAdaptIndex_6() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___curAdaptIndex_6)); }
	inline int32_t get_curAdaptIndex_6() const { return ___curAdaptIndex_6; }
	inline int32_t* get_address_of_curAdaptIndex_6() { return &___curAdaptIndex_6; }
	inline void set_curAdaptIndex_6(int32_t value)
	{
		___curAdaptIndex_6 = value;
	}

	inline static int32_t get_offset_of_shaderLum_7() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___shaderLum_7)); }
	inline Shader_t4151988712 * get_shaderLum_7() const { return ___shaderLum_7; }
	inline Shader_t4151988712 ** get_address_of_shaderLum_7() { return &___shaderLum_7; }
	inline void set_shaderLum_7(Shader_t4151988712 * value)
	{
		___shaderLum_7 = value;
		Il2CppCodeGenWriteBarrier((&___shaderLum_7), value);
	}

	inline static int32_t get_offset_of_m_materialLum_8() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___m_materialLum_8)); }
	inline Material_t340375123 * get_m_materialLum_8() const { return ___m_materialLum_8; }
	inline Material_t340375123 ** get_address_of_m_materialLum_8() { return &___m_materialLum_8; }
	inline void set_m_materialLum_8(Material_t340375123 * value)
	{
		___m_materialLum_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialLum_8), value);
	}

	inline static int32_t get_offset_of_shaderReduce_9() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___shaderReduce_9)); }
	inline Shader_t4151988712 * get_shaderReduce_9() const { return ___shaderReduce_9; }
	inline Shader_t4151988712 ** get_address_of_shaderReduce_9() { return &___shaderReduce_9; }
	inline void set_shaderReduce_9(Shader_t4151988712 * value)
	{
		___shaderReduce_9 = value;
		Il2CppCodeGenWriteBarrier((&___shaderReduce_9), value);
	}

	inline static int32_t get_offset_of_m_materialReduce_10() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___m_materialReduce_10)); }
	inline Material_t340375123 * get_m_materialReduce_10() const { return ___m_materialReduce_10; }
	inline Material_t340375123 ** get_address_of_m_materialReduce_10() { return &___m_materialReduce_10; }
	inline void set_m_materialReduce_10(Material_t340375123 * value)
	{
		___m_materialReduce_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReduce_10), value);
	}

	inline static int32_t get_offset_of_shaderAdapt_11() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___shaderAdapt_11)); }
	inline Shader_t4151988712 * get_shaderAdapt_11() const { return ___shaderAdapt_11; }
	inline Shader_t4151988712 ** get_address_of_shaderAdapt_11() { return &___shaderAdapt_11; }
	inline void set_shaderAdapt_11(Shader_t4151988712 * value)
	{
		___shaderAdapt_11 = value;
		Il2CppCodeGenWriteBarrier((&___shaderAdapt_11), value);
	}

	inline static int32_t get_offset_of_m_materialAdapt_12() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___m_materialAdapt_12)); }
	inline Material_t340375123 * get_m_materialAdapt_12() const { return ___m_materialAdapt_12; }
	inline Material_t340375123 ** get_address_of_m_materialAdapt_12() { return &___m_materialAdapt_12; }
	inline void set_m_materialAdapt_12(Material_t340375123 * value)
	{
		___m_materialAdapt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialAdapt_12), value);
	}

	inline static int32_t get_offset_of_shaderApply_13() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___shaderApply_13)); }
	inline Shader_t4151988712 * get_shaderApply_13() const { return ___shaderApply_13; }
	inline Shader_t4151988712 ** get_address_of_shaderApply_13() { return &___shaderApply_13; }
	inline void set_shaderApply_13(Shader_t4151988712 * value)
	{
		___shaderApply_13 = value;
		Il2CppCodeGenWriteBarrier((&___shaderApply_13), value);
	}

	inline static int32_t get_offset_of_m_materialApply_14() { return static_cast<int32_t>(offsetof(ContrastStretch_t3424449263, ___m_materialApply_14)); }
	inline Material_t340375123 * get_m_materialApply_14() const { return ___m_materialApply_14; }
	inline Material_t340375123 ** get_address_of_m_materialApply_14() { return &___m_materialApply_14; }
	inline void set_m_materialApply_14(Material_t340375123 * value)
	{
		___m_materialApply_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialApply_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTSTRETCH_T3424449263_H
#ifndef SCREENSPACEAMBIENTOCCLUSION_T1675618705_H
#define SCREENSPACEAMBIENTOCCLUSION_T1675618705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
struct  ScreenSpaceAmbientOcclusion_t1675618705  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Radius
	float ___m_Radius_2;
	// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SampleCount
	int32_t ___m_SampleCount_3;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_OcclusionIntensity
	float ___m_OcclusionIntensity_4;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Blur
	int32_t ___m_Blur_5;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Downsampling
	int32_t ___m_Downsampling_6;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_OcclusionAttenuation
	float ___m_OcclusionAttenuation_7;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_MinZ
	float ___m_MinZ_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SSAOShader
	Shader_t4151988712 * ___m_SSAOShader_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SSAOMaterial
	Material_t340375123 * ___m_SSAOMaterial_10;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_RandomTexture
	Texture2D_t3840446185 * ___m_RandomTexture_11;
	// System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Supported
	bool ___m_Supported_12;

public:
	inline static int32_t get_offset_of_m_Radius_2() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Radius_2)); }
	inline float get_m_Radius_2() const { return ___m_Radius_2; }
	inline float* get_address_of_m_Radius_2() { return &___m_Radius_2; }
	inline void set_m_Radius_2(float value)
	{
		___m_Radius_2 = value;
	}

	inline static int32_t get_offset_of_m_SampleCount_3() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_SampleCount_3)); }
	inline int32_t get_m_SampleCount_3() const { return ___m_SampleCount_3; }
	inline int32_t* get_address_of_m_SampleCount_3() { return &___m_SampleCount_3; }
	inline void set_m_SampleCount_3(int32_t value)
	{
		___m_SampleCount_3 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionIntensity_4() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_OcclusionIntensity_4)); }
	inline float get_m_OcclusionIntensity_4() const { return ___m_OcclusionIntensity_4; }
	inline float* get_address_of_m_OcclusionIntensity_4() { return &___m_OcclusionIntensity_4; }
	inline void set_m_OcclusionIntensity_4(float value)
	{
		___m_OcclusionIntensity_4 = value;
	}

	inline static int32_t get_offset_of_m_Blur_5() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Blur_5)); }
	inline int32_t get_m_Blur_5() const { return ___m_Blur_5; }
	inline int32_t* get_address_of_m_Blur_5() { return &___m_Blur_5; }
	inline void set_m_Blur_5(int32_t value)
	{
		___m_Blur_5 = value;
	}

	inline static int32_t get_offset_of_m_Downsampling_6() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Downsampling_6)); }
	inline int32_t get_m_Downsampling_6() const { return ___m_Downsampling_6; }
	inline int32_t* get_address_of_m_Downsampling_6() { return &___m_Downsampling_6; }
	inline void set_m_Downsampling_6(int32_t value)
	{
		___m_Downsampling_6 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionAttenuation_7() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_OcclusionAttenuation_7)); }
	inline float get_m_OcclusionAttenuation_7() const { return ___m_OcclusionAttenuation_7; }
	inline float* get_address_of_m_OcclusionAttenuation_7() { return &___m_OcclusionAttenuation_7; }
	inline void set_m_OcclusionAttenuation_7(float value)
	{
		___m_OcclusionAttenuation_7 = value;
	}

	inline static int32_t get_offset_of_m_MinZ_8() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_MinZ_8)); }
	inline float get_m_MinZ_8() const { return ___m_MinZ_8; }
	inline float* get_address_of_m_MinZ_8() { return &___m_MinZ_8; }
	inline void set_m_MinZ_8(float value)
	{
		___m_MinZ_8 = value;
	}

	inline static int32_t get_offset_of_m_SSAOShader_9() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_SSAOShader_9)); }
	inline Shader_t4151988712 * get_m_SSAOShader_9() const { return ___m_SSAOShader_9; }
	inline Shader_t4151988712 ** get_address_of_m_SSAOShader_9() { return &___m_SSAOShader_9; }
	inline void set_m_SSAOShader_9(Shader_t4151988712 * value)
	{
		___m_SSAOShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSAOShader_9), value);
	}

	inline static int32_t get_offset_of_m_SSAOMaterial_10() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_SSAOMaterial_10)); }
	inline Material_t340375123 * get_m_SSAOMaterial_10() const { return ___m_SSAOMaterial_10; }
	inline Material_t340375123 ** get_address_of_m_SSAOMaterial_10() { return &___m_SSAOMaterial_10; }
	inline void set_m_SSAOMaterial_10(Material_t340375123 * value)
	{
		___m_SSAOMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSAOMaterial_10), value);
	}

	inline static int32_t get_offset_of_m_RandomTexture_11() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_RandomTexture_11)); }
	inline Texture2D_t3840446185 * get_m_RandomTexture_11() const { return ___m_RandomTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of_m_RandomTexture_11() { return &___m_RandomTexture_11; }
	inline void set_m_RandomTexture_11(Texture2D_t3840446185 * value)
	{
		___m_RandomTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RandomTexture_11), value);
	}

	inline static int32_t get_offset_of_m_Supported_12() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Supported_12)); }
	inline bool get_m_Supported_12() const { return ___m_Supported_12; }
	inline bool* get_address_of_m_Supported_12() { return &___m_Supported_12; }
	inline void set_m_Supported_12(bool value)
	{
		___m_Supported_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEAMBIENTOCCLUSION_T1675618705_H
#ifndef CEFFECTMANAGER_T184376453_H
#define CEFFECTMANAGER_T184376453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEffectManager
struct  CEffectManager_t184376453  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CEffectManager::m_arySkillEffect_QianYao
	GameObjectU5BU5D_t3328599146* ___m_arySkillEffect_QianYao_3;
	// UnityEngine.GameObject[] CEffectManager::m_arySkillEffect_ChiXu
	GameObjectU5BU5D_t3328599146* ___m_arySkillEffect_ChiXu_4;
	// System.Single[] CEffectManager::m_arySkillEffectScale_QianYao
	SingleU5BU5D_t1444911251* ___m_arySkillEffectScale_QianYao_5;
	// System.Single[] CEffectManager::m_arySkillEffectScale_ChiXu
	SingleU5BU5D_t1444911251* ___m_arySkillEffectScale_ChiXu_6;
	// System.Collections.Generic.Dictionary`2<CSkillSystem/eSkillId,System.Collections.Generic.List`1<CCosmosEffect>> CEffectManager::m_dicRecycledSkillEffect_QianYao
	Dictionary_2_t2961645935 * ___m_dicRecycledSkillEffect_QianYao_7;
	// System.Collections.Generic.Dictionary`2<CSkillSystem/eSkillId,System.Collections.Generic.List`1<CCosmosEffect>> CEffectManager::m_dicRecycledSkillEffect_ChiXu
	Dictionary_2_t2961645935 * ___m_dicRecycledSkillEffect_ChiXu_8;
	// CCosmosEffect[] CEffectManager::m_aryMainFightEffects
	CCosmosEffectU5BU5D_t3177071136* ___m_aryMainFightEffects_9;
	// System.Single[] CEffectManager::m_aryMainFightEffects_FrameInterval
	SingleU5BU5D_t1444911251* ___m_aryMainFightEffects_FrameInterval_10;
	// System.Single[] CEffectManager::m_aryMainFightEffects_Scale
	SingleU5BU5D_t1444911251* ___m_aryMainFightEffects_Scale_11;
	// System.Int32 CEffectManager::m_nNewTimes
	int32_t ___m_nNewTimes_12;
	// System.Int32 CEffectManager::m_nGuid
	int32_t ___m_nGuid_13;
	// System.Int32 CEffectManager::m_nDelTimes
	int32_t ___m_nDelTimes_14;
	// System.Collections.Generic.Dictionary`2<CEffectManager/eMainFightEffectType,System.Collections.Generic.List`1<CCosmosEffect>> CEffectManager::m_dicMainFightEffefcts_Recycled
	Dictionary_2_t3341708860 * ___m_dicMainFightEffefcts_Recycled_15;

public:
	inline static int32_t get_offset_of_m_arySkillEffect_QianYao_3() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_arySkillEffect_QianYao_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySkillEffect_QianYao_3() const { return ___m_arySkillEffect_QianYao_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySkillEffect_QianYao_3() { return &___m_arySkillEffect_QianYao_3; }
	inline void set_m_arySkillEffect_QianYao_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySkillEffect_QianYao_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillEffect_QianYao_3), value);
	}

	inline static int32_t get_offset_of_m_arySkillEffect_ChiXu_4() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_arySkillEffect_ChiXu_4)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySkillEffect_ChiXu_4() const { return ___m_arySkillEffect_ChiXu_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySkillEffect_ChiXu_4() { return &___m_arySkillEffect_ChiXu_4; }
	inline void set_m_arySkillEffect_ChiXu_4(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySkillEffect_ChiXu_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillEffect_ChiXu_4), value);
	}

	inline static int32_t get_offset_of_m_arySkillEffectScale_QianYao_5() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_arySkillEffectScale_QianYao_5)); }
	inline SingleU5BU5D_t1444911251* get_m_arySkillEffectScale_QianYao_5() const { return ___m_arySkillEffectScale_QianYao_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_arySkillEffectScale_QianYao_5() { return &___m_arySkillEffectScale_QianYao_5; }
	inline void set_m_arySkillEffectScale_QianYao_5(SingleU5BU5D_t1444911251* value)
	{
		___m_arySkillEffectScale_QianYao_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillEffectScale_QianYao_5), value);
	}

	inline static int32_t get_offset_of_m_arySkillEffectScale_ChiXu_6() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_arySkillEffectScale_ChiXu_6)); }
	inline SingleU5BU5D_t1444911251* get_m_arySkillEffectScale_ChiXu_6() const { return ___m_arySkillEffectScale_ChiXu_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_arySkillEffectScale_ChiXu_6() { return &___m_arySkillEffectScale_ChiXu_6; }
	inline void set_m_arySkillEffectScale_ChiXu_6(SingleU5BU5D_t1444911251* value)
	{
		___m_arySkillEffectScale_ChiXu_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillEffectScale_ChiXu_6), value);
	}

	inline static int32_t get_offset_of_m_dicRecycledSkillEffect_QianYao_7() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_dicRecycledSkillEffect_QianYao_7)); }
	inline Dictionary_2_t2961645935 * get_m_dicRecycledSkillEffect_QianYao_7() const { return ___m_dicRecycledSkillEffect_QianYao_7; }
	inline Dictionary_2_t2961645935 ** get_address_of_m_dicRecycledSkillEffect_QianYao_7() { return &___m_dicRecycledSkillEffect_QianYao_7; }
	inline void set_m_dicRecycledSkillEffect_QianYao_7(Dictionary_2_t2961645935 * value)
	{
		___m_dicRecycledSkillEffect_QianYao_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledSkillEffect_QianYao_7), value);
	}

	inline static int32_t get_offset_of_m_dicRecycledSkillEffect_ChiXu_8() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_dicRecycledSkillEffect_ChiXu_8)); }
	inline Dictionary_2_t2961645935 * get_m_dicRecycledSkillEffect_ChiXu_8() const { return ___m_dicRecycledSkillEffect_ChiXu_8; }
	inline Dictionary_2_t2961645935 ** get_address_of_m_dicRecycledSkillEffect_ChiXu_8() { return &___m_dicRecycledSkillEffect_ChiXu_8; }
	inline void set_m_dicRecycledSkillEffect_ChiXu_8(Dictionary_2_t2961645935 * value)
	{
		___m_dicRecycledSkillEffect_ChiXu_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledSkillEffect_ChiXu_8), value);
	}

	inline static int32_t get_offset_of_m_aryMainFightEffects_9() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_aryMainFightEffects_9)); }
	inline CCosmosEffectU5BU5D_t3177071136* get_m_aryMainFightEffects_9() const { return ___m_aryMainFightEffects_9; }
	inline CCosmosEffectU5BU5D_t3177071136** get_address_of_m_aryMainFightEffects_9() { return &___m_aryMainFightEffects_9; }
	inline void set_m_aryMainFightEffects_9(CCosmosEffectU5BU5D_t3177071136* value)
	{
		___m_aryMainFightEffects_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainFightEffects_9), value);
	}

	inline static int32_t get_offset_of_m_aryMainFightEffects_FrameInterval_10() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_aryMainFightEffects_FrameInterval_10)); }
	inline SingleU5BU5D_t1444911251* get_m_aryMainFightEffects_FrameInterval_10() const { return ___m_aryMainFightEffects_FrameInterval_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryMainFightEffects_FrameInterval_10() { return &___m_aryMainFightEffects_FrameInterval_10; }
	inline void set_m_aryMainFightEffects_FrameInterval_10(SingleU5BU5D_t1444911251* value)
	{
		___m_aryMainFightEffects_FrameInterval_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainFightEffects_FrameInterval_10), value);
	}

	inline static int32_t get_offset_of_m_aryMainFightEffects_Scale_11() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_aryMainFightEffects_Scale_11)); }
	inline SingleU5BU5D_t1444911251* get_m_aryMainFightEffects_Scale_11() const { return ___m_aryMainFightEffects_Scale_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryMainFightEffects_Scale_11() { return &___m_aryMainFightEffects_Scale_11; }
	inline void set_m_aryMainFightEffects_Scale_11(SingleU5BU5D_t1444911251* value)
	{
		___m_aryMainFightEffects_Scale_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainFightEffects_Scale_11), value);
	}

	inline static int32_t get_offset_of_m_nNewTimes_12() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_nNewTimes_12)); }
	inline int32_t get_m_nNewTimes_12() const { return ___m_nNewTimes_12; }
	inline int32_t* get_address_of_m_nNewTimes_12() { return &___m_nNewTimes_12; }
	inline void set_m_nNewTimes_12(int32_t value)
	{
		___m_nNewTimes_12 = value;
	}

	inline static int32_t get_offset_of_m_nGuid_13() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_nGuid_13)); }
	inline int32_t get_m_nGuid_13() const { return ___m_nGuid_13; }
	inline int32_t* get_address_of_m_nGuid_13() { return &___m_nGuid_13; }
	inline void set_m_nGuid_13(int32_t value)
	{
		___m_nGuid_13 = value;
	}

	inline static int32_t get_offset_of_m_nDelTimes_14() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_nDelTimes_14)); }
	inline int32_t get_m_nDelTimes_14() const { return ___m_nDelTimes_14; }
	inline int32_t* get_address_of_m_nDelTimes_14() { return &___m_nDelTimes_14; }
	inline void set_m_nDelTimes_14(int32_t value)
	{
		___m_nDelTimes_14 = value;
	}

	inline static int32_t get_offset_of_m_dicMainFightEffefcts_Recycled_15() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453, ___m_dicMainFightEffefcts_Recycled_15)); }
	inline Dictionary_2_t3341708860 * get_m_dicMainFightEffefcts_Recycled_15() const { return ___m_dicMainFightEffefcts_Recycled_15; }
	inline Dictionary_2_t3341708860 ** get_address_of_m_dicMainFightEffefcts_Recycled_15() { return &___m_dicMainFightEffefcts_Recycled_15; }
	inline void set_m_dicMainFightEffefcts_Recycled_15(Dictionary_2_t3341708860 * value)
	{
		___m_dicMainFightEffefcts_Recycled_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicMainFightEffefcts_Recycled_15), value);
	}
};

struct CEffectManager_t184376453_StaticFields
{
public:
	// CEffectManager CEffectManager::s_Instance
	CEffectManager_t184376453 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CEffectManager_t184376453_StaticFields, ___s_Instance_2)); }
	inline CEffectManager_t184376453 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CEffectManager_t184376453 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CEffectManager_t184376453 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CEFFECTMANAGER_T184376453_H
#ifndef CITEMBUYEFFECT_T833204468_H
#define CITEMBUYEFFECT_T833204468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CItemBuyEffect
struct  CItemBuyEffect_t833204468  : public MonoBehaviour_t3962482529
{
public:
	// CFrameAnimationEffect CItemBuyEffect::_effectMain
	CFrameAnimationEffect_t443605508 * ____effectMain_2;
	// System.Single CItemBuyEffect::m_fSpeed
	float ___m_fSpeed_7;
	// UnityEngine.Vector2 CItemBuyEffect::m_Dir
	Vector2_t2156229523  ___m_Dir_8;
	// UnityEngine.Vector3 CItemBuyEffect::m_vecDest
	Vector3_t3722313464  ___m_vecDest_9;
	// System.Single CItemBuyEffect::m_fStartTime
	float ___m_fStartTime_10;
	// Ball CItemBuyEffect::_ballDest
	Ball_t2206666566 * ____ballDest_11;

public:
	inline static int32_t get_offset_of__effectMain_2() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468, ____effectMain_2)); }
	inline CFrameAnimationEffect_t443605508 * get__effectMain_2() const { return ____effectMain_2; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectMain_2() { return &____effectMain_2; }
	inline void set__effectMain_2(CFrameAnimationEffect_t443605508 * value)
	{
		____effectMain_2 = value;
		Il2CppCodeGenWriteBarrier((&____effectMain_2), value);
	}

	inline static int32_t get_offset_of_m_fSpeed_7() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468, ___m_fSpeed_7)); }
	inline float get_m_fSpeed_7() const { return ___m_fSpeed_7; }
	inline float* get_address_of_m_fSpeed_7() { return &___m_fSpeed_7; }
	inline void set_m_fSpeed_7(float value)
	{
		___m_fSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_Dir_8() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468, ___m_Dir_8)); }
	inline Vector2_t2156229523  get_m_Dir_8() const { return ___m_Dir_8; }
	inline Vector2_t2156229523 * get_address_of_m_Dir_8() { return &___m_Dir_8; }
	inline void set_m_Dir_8(Vector2_t2156229523  value)
	{
		___m_Dir_8 = value;
	}

	inline static int32_t get_offset_of_m_vecDest_9() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468, ___m_vecDest_9)); }
	inline Vector3_t3722313464  get_m_vecDest_9() const { return ___m_vecDest_9; }
	inline Vector3_t3722313464 * get_address_of_m_vecDest_9() { return &___m_vecDest_9; }
	inline void set_m_vecDest_9(Vector3_t3722313464  value)
	{
		___m_vecDest_9 = value;
	}

	inline static int32_t get_offset_of_m_fStartTime_10() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468, ___m_fStartTime_10)); }
	inline float get_m_fStartTime_10() const { return ___m_fStartTime_10; }
	inline float* get_address_of_m_fStartTime_10() { return &___m_fStartTime_10; }
	inline void set_m_fStartTime_10(float value)
	{
		___m_fStartTime_10 = value;
	}

	inline static int32_t get_offset_of__ballDest_11() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468, ____ballDest_11)); }
	inline Ball_t2206666566 * get__ballDest_11() const { return ____ballDest_11; }
	inline Ball_t2206666566 ** get_address_of__ballDest_11() { return &____ballDest_11; }
	inline void set__ballDest_11(Ball_t2206666566 * value)
	{
		____ballDest_11 = value;
		Il2CppCodeGenWriteBarrier((&____ballDest_11), value);
	}
};

struct CItemBuyEffect_t833204468_StaticFields
{
public:
	// UnityEngine.Vector3 CItemBuyEffect::vecTempScale
	Vector3_t3722313464  ___vecTempScale_5;
	// UnityEngine.Vector3 CItemBuyEffect::vecTempPos
	Vector3_t3722313464  ___vecTempPos_6;

public:
	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468_StaticFields, ___vecTempScale_5)); }
	inline Vector3_t3722313464  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_t3722313464  value)
	{
		___vecTempScale_5 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_6() { return static_cast<int32_t>(offsetof(CItemBuyEffect_t833204468_StaticFields, ___vecTempPos_6)); }
	inline Vector3_t3722313464  get_vecTempPos_6() const { return ___vecTempPos_6; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_6() { return &___vecTempPos_6; }
	inline void set_vecTempPos_6(Vector3_t3722313464  value)
	{
		___vecTempPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CITEMBUYEFFECT_T833204468_H
#ifndef CMIAOHEEFFECT_T834080748_H
#define CMIAOHEEFFECT_T834080748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMiaoHeEffect
struct  CMiaoHeEffect_t834080748  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CMiaoHeEffect::m_fStartTime
	float ___m_fStartTime_7;
	// UnityEngine.Vector2 CMiaoHeEffect::m_vecMoveSpeed
	Vector2_t2156229523  ___m_vecMoveSpeed_8;
	// UnityEngine.Vector2 CMiaoHeEffect::m_Dir
	Vector2_t2156229523  ___m_Dir_9;
	// UnityEngine.SpriteRenderer CMiaoHeEffect::_srMain
	SpriteRenderer_t3235626157 * ____srMain_10;
	// UnityEngine.SpriteRenderer CMiaoHeEffect::_srWhite
	SpriteRenderer_t3235626157 * ____srWhite_11;
	// System.Boolean CMiaoHeEffect::m_bStart
	bool ___m_bStart_12;
	// System.Int32 CMiaoHeEffect::m_nStatus
	int32_t ___m_nStatus_13;
	// System.Single CMiaoHeEffect::m_fTimeLapse
	float ___m_fTimeLapse_14;
	// System.Single CMiaoHeEffect::m_fRandomDelayTime
	float ___m_fRandomDelayTime_15;
	// UnityEngine.Vector3 CMiaoHeEffect::m_vecDestPos
	Vector3_t3722313464  ___m_vecDestPos_16;
	// Ball CMiaoHeEffect::_ballDest
	Ball_t2206666566 * ____ballDest_17;

public:
	inline static int32_t get_offset_of_m_fStartTime_7() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_fStartTime_7)); }
	inline float get_m_fStartTime_7() const { return ___m_fStartTime_7; }
	inline float* get_address_of_m_fStartTime_7() { return &___m_fStartTime_7; }
	inline void set_m_fStartTime_7(float value)
	{
		___m_fStartTime_7 = value;
	}

	inline static int32_t get_offset_of_m_vecMoveSpeed_8() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_vecMoveSpeed_8)); }
	inline Vector2_t2156229523  get_m_vecMoveSpeed_8() const { return ___m_vecMoveSpeed_8; }
	inline Vector2_t2156229523 * get_address_of_m_vecMoveSpeed_8() { return &___m_vecMoveSpeed_8; }
	inline void set_m_vecMoveSpeed_8(Vector2_t2156229523  value)
	{
		___m_vecMoveSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_Dir_9() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_Dir_9)); }
	inline Vector2_t2156229523  get_m_Dir_9() const { return ___m_Dir_9; }
	inline Vector2_t2156229523 * get_address_of_m_Dir_9() { return &___m_Dir_9; }
	inline void set_m_Dir_9(Vector2_t2156229523  value)
	{
		___m_Dir_9 = value;
	}

	inline static int32_t get_offset_of__srMain_10() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ____srMain_10)); }
	inline SpriteRenderer_t3235626157 * get__srMain_10() const { return ____srMain_10; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_10() { return &____srMain_10; }
	inline void set__srMain_10(SpriteRenderer_t3235626157 * value)
	{
		____srMain_10 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_10), value);
	}

	inline static int32_t get_offset_of__srWhite_11() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ____srWhite_11)); }
	inline SpriteRenderer_t3235626157 * get__srWhite_11() const { return ____srWhite_11; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srWhite_11() { return &____srWhite_11; }
	inline void set__srWhite_11(SpriteRenderer_t3235626157 * value)
	{
		____srWhite_11 = value;
		Il2CppCodeGenWriteBarrier((&____srWhite_11), value);
	}

	inline static int32_t get_offset_of_m_bStart_12() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_bStart_12)); }
	inline bool get_m_bStart_12() const { return ___m_bStart_12; }
	inline bool* get_address_of_m_bStart_12() { return &___m_bStart_12; }
	inline void set_m_bStart_12(bool value)
	{
		___m_bStart_12 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_13() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_nStatus_13)); }
	inline int32_t get_m_nStatus_13() const { return ___m_nStatus_13; }
	inline int32_t* get_address_of_m_nStatus_13() { return &___m_nStatus_13; }
	inline void set_m_nStatus_13(int32_t value)
	{
		___m_nStatus_13 = value;
	}

	inline static int32_t get_offset_of_m_fTimeLapse_14() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_fTimeLapse_14)); }
	inline float get_m_fTimeLapse_14() const { return ___m_fTimeLapse_14; }
	inline float* get_address_of_m_fTimeLapse_14() { return &___m_fTimeLapse_14; }
	inline void set_m_fTimeLapse_14(float value)
	{
		___m_fTimeLapse_14 = value;
	}

	inline static int32_t get_offset_of_m_fRandomDelayTime_15() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_fRandomDelayTime_15)); }
	inline float get_m_fRandomDelayTime_15() const { return ___m_fRandomDelayTime_15; }
	inline float* get_address_of_m_fRandomDelayTime_15() { return &___m_fRandomDelayTime_15; }
	inline void set_m_fRandomDelayTime_15(float value)
	{
		___m_fRandomDelayTime_15 = value;
	}

	inline static int32_t get_offset_of_m_vecDestPos_16() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ___m_vecDestPos_16)); }
	inline Vector3_t3722313464  get_m_vecDestPos_16() const { return ___m_vecDestPos_16; }
	inline Vector3_t3722313464 * get_address_of_m_vecDestPos_16() { return &___m_vecDestPos_16; }
	inline void set_m_vecDestPos_16(Vector3_t3722313464  value)
	{
		___m_vecDestPos_16 = value;
	}

	inline static int32_t get_offset_of__ballDest_17() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748, ____ballDest_17)); }
	inline Ball_t2206666566 * get__ballDest_17() const { return ____ballDest_17; }
	inline Ball_t2206666566 ** get_address_of__ballDest_17() { return &____ballDest_17; }
	inline void set__ballDest_17(Ball_t2206666566 * value)
	{
		____ballDest_17 = value;
		Il2CppCodeGenWriteBarrier((&____ballDest_17), value);
	}
};

struct CMiaoHeEffect_t834080748_StaticFields
{
public:
	// UnityEngine.Vector3 CMiaoHeEffect::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;
	// UnityEngine.Vector3 CMiaoHeEffect::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;
	// UnityEngine.Color CMiaoHeEffect::colorTemp
	Color_t2555686324  ___colorTemp_6;

public:
	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_colorTemp_6() { return static_cast<int32_t>(offsetof(CMiaoHeEffect_t834080748_StaticFields, ___colorTemp_6)); }
	inline Color_t2555686324  get_colorTemp_6() const { return ___colorTemp_6; }
	inline Color_t2555686324 * get_address_of_colorTemp_6() { return &___colorTemp_6; }
	inline void set_colorTemp_6(Color_t2555686324  value)
	{
		___colorTemp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMIAOHEEFFECT_T834080748_H
#ifndef COLORCORRECTIONCURVES_T3742166504_H
#define COLORCORRECTIONCURVES_T3742166504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
struct  ColorCorrectionCurves_t3742166504  : public PostEffectsBase_t2404315739
{
public:
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::redChannel
	AnimationCurve_t3046754366 * ___redChannel_6;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::greenChannel
	AnimationCurve_t3046754366 * ___greenChannel_7;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::blueChannel
	AnimationCurve_t3046754366 * ___blueChannel_8;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::useDepthCorrection
	bool ___useDepthCorrection_9;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::zCurve
	AnimationCurve_t3046754366 * ___zCurve_10;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::depthRedChannel
	AnimationCurve_t3046754366 * ___depthRedChannel_11;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::depthGreenChannel
	AnimationCurve_t3046754366 * ___depthGreenChannel_12;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.ColorCorrectionCurves::depthBlueChannel
	AnimationCurve_t3046754366 * ___depthBlueChannel_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionCurves::ccMaterial
	Material_t340375123 * ___ccMaterial_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionCurves::ccDepthMaterial
	Material_t340375123 * ___ccDepthMaterial_15;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveCcMaterial
	Material_t340375123 * ___selectiveCcMaterial_16;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ColorCorrectionCurves::rgbChannelTex
	Texture2D_t3840446185 * ___rgbChannelTex_17;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ColorCorrectionCurves::rgbDepthChannelTex
	Texture2D_t3840446185 * ___rgbDepthChannelTex_18;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ColorCorrectionCurves::zCurveTex
	Texture2D_t3840446185 * ___zCurveTex_19;
	// System.Single UnityStandardAssets.ImageEffects.ColorCorrectionCurves::saturation
	float ___saturation_20;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveCc
	bool ___selectiveCc_21;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveFromColor
	Color_t2555686324  ___selectiveFromColor_22;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.ColorCorrectionCurves::selectiveToColor
	Color_t2555686324  ___selectiveToColor_23;
	// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode UnityStandardAssets.ImageEffects.ColorCorrectionCurves::mode
	int32_t ___mode_24;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::updateTextures
	bool ___updateTextures_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionCurves::colorCorrectionCurvesShader
	Shader_t4151988712 * ___colorCorrectionCurvesShader_26;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionCurves::simpleColorCorrectionCurvesShader
	Shader_t4151988712 * ___simpleColorCorrectionCurvesShader_27;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionCurves::colorCorrectionSelectiveShader
	Shader_t4151988712 * ___colorCorrectionSelectiveShader_28;
	// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::updateTexturesOnStartup
	bool ___updateTexturesOnStartup_29;

public:
	inline static int32_t get_offset_of_redChannel_6() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___redChannel_6)); }
	inline AnimationCurve_t3046754366 * get_redChannel_6() const { return ___redChannel_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_redChannel_6() { return &___redChannel_6; }
	inline void set_redChannel_6(AnimationCurve_t3046754366 * value)
	{
		___redChannel_6 = value;
		Il2CppCodeGenWriteBarrier((&___redChannel_6), value);
	}

	inline static int32_t get_offset_of_greenChannel_7() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___greenChannel_7)); }
	inline AnimationCurve_t3046754366 * get_greenChannel_7() const { return ___greenChannel_7; }
	inline AnimationCurve_t3046754366 ** get_address_of_greenChannel_7() { return &___greenChannel_7; }
	inline void set_greenChannel_7(AnimationCurve_t3046754366 * value)
	{
		___greenChannel_7 = value;
		Il2CppCodeGenWriteBarrier((&___greenChannel_7), value);
	}

	inline static int32_t get_offset_of_blueChannel_8() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___blueChannel_8)); }
	inline AnimationCurve_t3046754366 * get_blueChannel_8() const { return ___blueChannel_8; }
	inline AnimationCurve_t3046754366 ** get_address_of_blueChannel_8() { return &___blueChannel_8; }
	inline void set_blueChannel_8(AnimationCurve_t3046754366 * value)
	{
		___blueChannel_8 = value;
		Il2CppCodeGenWriteBarrier((&___blueChannel_8), value);
	}

	inline static int32_t get_offset_of_useDepthCorrection_9() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___useDepthCorrection_9)); }
	inline bool get_useDepthCorrection_9() const { return ___useDepthCorrection_9; }
	inline bool* get_address_of_useDepthCorrection_9() { return &___useDepthCorrection_9; }
	inline void set_useDepthCorrection_9(bool value)
	{
		___useDepthCorrection_9 = value;
	}

	inline static int32_t get_offset_of_zCurve_10() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___zCurve_10)); }
	inline AnimationCurve_t3046754366 * get_zCurve_10() const { return ___zCurve_10; }
	inline AnimationCurve_t3046754366 ** get_address_of_zCurve_10() { return &___zCurve_10; }
	inline void set_zCurve_10(AnimationCurve_t3046754366 * value)
	{
		___zCurve_10 = value;
		Il2CppCodeGenWriteBarrier((&___zCurve_10), value);
	}

	inline static int32_t get_offset_of_depthRedChannel_11() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___depthRedChannel_11)); }
	inline AnimationCurve_t3046754366 * get_depthRedChannel_11() const { return ___depthRedChannel_11; }
	inline AnimationCurve_t3046754366 ** get_address_of_depthRedChannel_11() { return &___depthRedChannel_11; }
	inline void set_depthRedChannel_11(AnimationCurve_t3046754366 * value)
	{
		___depthRedChannel_11 = value;
		Il2CppCodeGenWriteBarrier((&___depthRedChannel_11), value);
	}

	inline static int32_t get_offset_of_depthGreenChannel_12() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___depthGreenChannel_12)); }
	inline AnimationCurve_t3046754366 * get_depthGreenChannel_12() const { return ___depthGreenChannel_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_depthGreenChannel_12() { return &___depthGreenChannel_12; }
	inline void set_depthGreenChannel_12(AnimationCurve_t3046754366 * value)
	{
		___depthGreenChannel_12 = value;
		Il2CppCodeGenWriteBarrier((&___depthGreenChannel_12), value);
	}

	inline static int32_t get_offset_of_depthBlueChannel_13() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___depthBlueChannel_13)); }
	inline AnimationCurve_t3046754366 * get_depthBlueChannel_13() const { return ___depthBlueChannel_13; }
	inline AnimationCurve_t3046754366 ** get_address_of_depthBlueChannel_13() { return &___depthBlueChannel_13; }
	inline void set_depthBlueChannel_13(AnimationCurve_t3046754366 * value)
	{
		___depthBlueChannel_13 = value;
		Il2CppCodeGenWriteBarrier((&___depthBlueChannel_13), value);
	}

	inline static int32_t get_offset_of_ccMaterial_14() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___ccMaterial_14)); }
	inline Material_t340375123 * get_ccMaterial_14() const { return ___ccMaterial_14; }
	inline Material_t340375123 ** get_address_of_ccMaterial_14() { return &___ccMaterial_14; }
	inline void set_ccMaterial_14(Material_t340375123 * value)
	{
		___ccMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___ccMaterial_14), value);
	}

	inline static int32_t get_offset_of_ccDepthMaterial_15() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___ccDepthMaterial_15)); }
	inline Material_t340375123 * get_ccDepthMaterial_15() const { return ___ccDepthMaterial_15; }
	inline Material_t340375123 ** get_address_of_ccDepthMaterial_15() { return &___ccDepthMaterial_15; }
	inline void set_ccDepthMaterial_15(Material_t340375123 * value)
	{
		___ccDepthMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___ccDepthMaterial_15), value);
	}

	inline static int32_t get_offset_of_selectiveCcMaterial_16() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___selectiveCcMaterial_16)); }
	inline Material_t340375123 * get_selectiveCcMaterial_16() const { return ___selectiveCcMaterial_16; }
	inline Material_t340375123 ** get_address_of_selectiveCcMaterial_16() { return &___selectiveCcMaterial_16; }
	inline void set_selectiveCcMaterial_16(Material_t340375123 * value)
	{
		___selectiveCcMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___selectiveCcMaterial_16), value);
	}

	inline static int32_t get_offset_of_rgbChannelTex_17() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___rgbChannelTex_17)); }
	inline Texture2D_t3840446185 * get_rgbChannelTex_17() const { return ___rgbChannelTex_17; }
	inline Texture2D_t3840446185 ** get_address_of_rgbChannelTex_17() { return &___rgbChannelTex_17; }
	inline void set_rgbChannelTex_17(Texture2D_t3840446185 * value)
	{
		___rgbChannelTex_17 = value;
		Il2CppCodeGenWriteBarrier((&___rgbChannelTex_17), value);
	}

	inline static int32_t get_offset_of_rgbDepthChannelTex_18() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___rgbDepthChannelTex_18)); }
	inline Texture2D_t3840446185 * get_rgbDepthChannelTex_18() const { return ___rgbDepthChannelTex_18; }
	inline Texture2D_t3840446185 ** get_address_of_rgbDepthChannelTex_18() { return &___rgbDepthChannelTex_18; }
	inline void set_rgbDepthChannelTex_18(Texture2D_t3840446185 * value)
	{
		___rgbDepthChannelTex_18 = value;
		Il2CppCodeGenWriteBarrier((&___rgbDepthChannelTex_18), value);
	}

	inline static int32_t get_offset_of_zCurveTex_19() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___zCurveTex_19)); }
	inline Texture2D_t3840446185 * get_zCurveTex_19() const { return ___zCurveTex_19; }
	inline Texture2D_t3840446185 ** get_address_of_zCurveTex_19() { return &___zCurveTex_19; }
	inline void set_zCurveTex_19(Texture2D_t3840446185 * value)
	{
		___zCurveTex_19 = value;
		Il2CppCodeGenWriteBarrier((&___zCurveTex_19), value);
	}

	inline static int32_t get_offset_of_saturation_20() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___saturation_20)); }
	inline float get_saturation_20() const { return ___saturation_20; }
	inline float* get_address_of_saturation_20() { return &___saturation_20; }
	inline void set_saturation_20(float value)
	{
		___saturation_20 = value;
	}

	inline static int32_t get_offset_of_selectiveCc_21() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___selectiveCc_21)); }
	inline bool get_selectiveCc_21() const { return ___selectiveCc_21; }
	inline bool* get_address_of_selectiveCc_21() { return &___selectiveCc_21; }
	inline void set_selectiveCc_21(bool value)
	{
		___selectiveCc_21 = value;
	}

	inline static int32_t get_offset_of_selectiveFromColor_22() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___selectiveFromColor_22)); }
	inline Color_t2555686324  get_selectiveFromColor_22() const { return ___selectiveFromColor_22; }
	inline Color_t2555686324 * get_address_of_selectiveFromColor_22() { return &___selectiveFromColor_22; }
	inline void set_selectiveFromColor_22(Color_t2555686324  value)
	{
		___selectiveFromColor_22 = value;
	}

	inline static int32_t get_offset_of_selectiveToColor_23() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___selectiveToColor_23)); }
	inline Color_t2555686324  get_selectiveToColor_23() const { return ___selectiveToColor_23; }
	inline Color_t2555686324 * get_address_of_selectiveToColor_23() { return &___selectiveToColor_23; }
	inline void set_selectiveToColor_23(Color_t2555686324  value)
	{
		___selectiveToColor_23 = value;
	}

	inline static int32_t get_offset_of_mode_24() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___mode_24)); }
	inline int32_t get_mode_24() const { return ___mode_24; }
	inline int32_t* get_address_of_mode_24() { return &___mode_24; }
	inline void set_mode_24(int32_t value)
	{
		___mode_24 = value;
	}

	inline static int32_t get_offset_of_updateTextures_25() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___updateTextures_25)); }
	inline bool get_updateTextures_25() const { return ___updateTextures_25; }
	inline bool* get_address_of_updateTextures_25() { return &___updateTextures_25; }
	inline void set_updateTextures_25(bool value)
	{
		___updateTextures_25 = value;
	}

	inline static int32_t get_offset_of_colorCorrectionCurvesShader_26() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___colorCorrectionCurvesShader_26)); }
	inline Shader_t4151988712 * get_colorCorrectionCurvesShader_26() const { return ___colorCorrectionCurvesShader_26; }
	inline Shader_t4151988712 ** get_address_of_colorCorrectionCurvesShader_26() { return &___colorCorrectionCurvesShader_26; }
	inline void set_colorCorrectionCurvesShader_26(Shader_t4151988712 * value)
	{
		___colorCorrectionCurvesShader_26 = value;
		Il2CppCodeGenWriteBarrier((&___colorCorrectionCurvesShader_26), value);
	}

	inline static int32_t get_offset_of_simpleColorCorrectionCurvesShader_27() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___simpleColorCorrectionCurvesShader_27)); }
	inline Shader_t4151988712 * get_simpleColorCorrectionCurvesShader_27() const { return ___simpleColorCorrectionCurvesShader_27; }
	inline Shader_t4151988712 ** get_address_of_simpleColorCorrectionCurvesShader_27() { return &___simpleColorCorrectionCurvesShader_27; }
	inline void set_simpleColorCorrectionCurvesShader_27(Shader_t4151988712 * value)
	{
		___simpleColorCorrectionCurvesShader_27 = value;
		Il2CppCodeGenWriteBarrier((&___simpleColorCorrectionCurvesShader_27), value);
	}

	inline static int32_t get_offset_of_colorCorrectionSelectiveShader_28() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___colorCorrectionSelectiveShader_28)); }
	inline Shader_t4151988712 * get_colorCorrectionSelectiveShader_28() const { return ___colorCorrectionSelectiveShader_28; }
	inline Shader_t4151988712 ** get_address_of_colorCorrectionSelectiveShader_28() { return &___colorCorrectionSelectiveShader_28; }
	inline void set_colorCorrectionSelectiveShader_28(Shader_t4151988712 * value)
	{
		___colorCorrectionSelectiveShader_28 = value;
		Il2CppCodeGenWriteBarrier((&___colorCorrectionSelectiveShader_28), value);
	}

	inline static int32_t get_offset_of_updateTexturesOnStartup_29() { return static_cast<int32_t>(offsetof(ColorCorrectionCurves_t3742166504, ___updateTexturesOnStartup_29)); }
	inline bool get_updateTexturesOnStartup_29() const { return ___updateTexturesOnStartup_29; }
	inline bool* get_address_of_updateTexturesOnStartup_29() { return &___updateTexturesOnStartup_29; }
	inline void set_updateTexturesOnStartup_29(bool value)
	{
		___updateTexturesOnStartup_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONCURVES_T3742166504_H
#ifndef COLORCORRECTIONLOOKUP_T1159177774_H
#define COLORCORRECTIONLOOKUP_T1159177774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
struct  ColorCorrectionLookup_t1159177774  : public PostEffectsBase_t2404315739
{
public:
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ColorCorrectionLookup::shader
	Shader_t4151988712 * ___shader_6;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ColorCorrectionLookup::material
	Material_t340375123 * ___material_7;
	// UnityEngine.Texture3D UnityStandardAssets.ImageEffects.ColorCorrectionLookup::converted3DLut
	Texture3D_t1884131049 * ___converted3DLut_8;
	// System.String UnityStandardAssets.ImageEffects.ColorCorrectionLookup::basedOnTempTex
	String_t* ___basedOnTempTex_9;

public:
	inline static int32_t get_offset_of_shader_6() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t1159177774, ___shader_6)); }
	inline Shader_t4151988712 * get_shader_6() const { return ___shader_6; }
	inline Shader_t4151988712 ** get_address_of_shader_6() { return &___shader_6; }
	inline void set_shader_6(Shader_t4151988712 * value)
	{
		___shader_6 = value;
		Il2CppCodeGenWriteBarrier((&___shader_6), value);
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t1159177774, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_converted3DLut_8() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t1159177774, ___converted3DLut_8)); }
	inline Texture3D_t1884131049 * get_converted3DLut_8() const { return ___converted3DLut_8; }
	inline Texture3D_t1884131049 ** get_address_of_converted3DLut_8() { return &___converted3DLut_8; }
	inline void set_converted3DLut_8(Texture3D_t1884131049 * value)
	{
		___converted3DLut_8 = value;
		Il2CppCodeGenWriteBarrier((&___converted3DLut_8), value);
	}

	inline static int32_t get_offset_of_basedOnTempTex_9() { return static_cast<int32_t>(offsetof(ColorCorrectionLookup_t1159177774, ___basedOnTempTex_9)); }
	inline String_t* get_basedOnTempTex_9() const { return ___basedOnTempTex_9; }
	inline String_t** get_address_of_basedOnTempTex_9() { return &___basedOnTempTex_9; }
	inline void set_basedOnTempTex_9(String_t* value)
	{
		___basedOnTempTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___basedOnTempTex_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONLOOKUP_T1159177774_H
#ifndef COLORCORRECTIONRAMP_T3562116199_H
#define COLORCORRECTIONRAMP_T3562116199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
struct  ColorCorrectionRamp_t3562116199  : public ImageEffectBase_t2026006575
{
public:
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.ColorCorrectionRamp::textureRamp
	Texture_t3661962703 * ___textureRamp_4;

public:
	inline static int32_t get_offset_of_textureRamp_4() { return static_cast<int32_t>(offsetof(ColorCorrectionRamp_t3562116199, ___textureRamp_4)); }
	inline Texture_t3661962703 * get_textureRamp_4() const { return ___textureRamp_4; }
	inline Texture_t3661962703 ** get_address_of_textureRamp_4() { return &___textureRamp_4; }
	inline void set_textureRamp_4(Texture_t3661962703 * value)
	{
		___textureRamp_4 = value;
		Il2CppCodeGenWriteBarrier((&___textureRamp_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCORRECTIONRAMP_T3562116199_H
#ifndef CONTRASTENHANCE_T640919481_H
#define CONTRASTENHANCE_T640919481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ContrastEnhance
struct  ContrastEnhance_t640919481  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ContrastEnhance::intensity
	float ___intensity_6;
	// System.Single UnityStandardAssets.ImageEffects.ContrastEnhance::threshold
	float ___threshold_7;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastEnhance::separableBlurMaterial
	Material_t340375123 * ___separableBlurMaterial_8;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastEnhance::contrastCompositeMaterial
	Material_t340375123 * ___contrastCompositeMaterial_9;
	// System.Single UnityStandardAssets.ImageEffects.ContrastEnhance::blurSpread
	float ___blurSpread_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastEnhance::separableBlurShader
	Shader_t4151988712 * ___separableBlurShader_11;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ContrastEnhance::contrastCompositeShader
	Shader_t4151988712 * ___contrastCompositeShader_12;

public:
	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_threshold_7() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___threshold_7)); }
	inline float get_threshold_7() const { return ___threshold_7; }
	inline float* get_address_of_threshold_7() { return &___threshold_7; }
	inline void set_threshold_7(float value)
	{
		___threshold_7 = value;
	}

	inline static int32_t get_offset_of_separableBlurMaterial_8() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___separableBlurMaterial_8)); }
	inline Material_t340375123 * get_separableBlurMaterial_8() const { return ___separableBlurMaterial_8; }
	inline Material_t340375123 ** get_address_of_separableBlurMaterial_8() { return &___separableBlurMaterial_8; }
	inline void set_separableBlurMaterial_8(Material_t340375123 * value)
	{
		___separableBlurMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurMaterial_8), value);
	}

	inline static int32_t get_offset_of_contrastCompositeMaterial_9() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___contrastCompositeMaterial_9)); }
	inline Material_t340375123 * get_contrastCompositeMaterial_9() const { return ___contrastCompositeMaterial_9; }
	inline Material_t340375123 ** get_address_of_contrastCompositeMaterial_9() { return &___contrastCompositeMaterial_9; }
	inline void set_contrastCompositeMaterial_9(Material_t340375123 * value)
	{
		___contrastCompositeMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___contrastCompositeMaterial_9), value);
	}

	inline static int32_t get_offset_of_blurSpread_10() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___blurSpread_10)); }
	inline float get_blurSpread_10() const { return ___blurSpread_10; }
	inline float* get_address_of_blurSpread_10() { return &___blurSpread_10; }
	inline void set_blurSpread_10(float value)
	{
		___blurSpread_10 = value;
	}

	inline static int32_t get_offset_of_separableBlurShader_11() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___separableBlurShader_11)); }
	inline Shader_t4151988712 * get_separableBlurShader_11() const { return ___separableBlurShader_11; }
	inline Shader_t4151988712 ** get_address_of_separableBlurShader_11() { return &___separableBlurShader_11; }
	inline void set_separableBlurShader_11(Shader_t4151988712 * value)
	{
		___separableBlurShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurShader_11), value);
	}

	inline static int32_t get_offset_of_contrastCompositeShader_12() { return static_cast<int32_t>(offsetof(ContrastEnhance_t640919481, ___contrastCompositeShader_12)); }
	inline Shader_t4151988712 * get_contrastCompositeShader_12() const { return ___contrastCompositeShader_12; }
	inline Shader_t4151988712 ** get_address_of_contrastCompositeShader_12() { return &___contrastCompositeShader_12; }
	inline void set_contrastCompositeShader_12(Shader_t4151988712 * value)
	{
		___contrastCompositeShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___contrastCompositeShader_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTENHANCE_T640919481_H
#ifndef CFRAMEANIMATIONEFFECT_T443605508_H
#define CFRAMEANIMATIONEFFECT_T443605508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFrameAnimationEffect
struct  CFrameAnimationEffect_t443605508  : public CCosmosEffect_t495978253
{
public:
	// UnityEngine.Sprite[] CFrameAnimationEffect::m_aryFrameSprites
	SpriteU5BU5D_t2581906349* ___m_aryFrameSprites_17;
	// System.Single CFrameAnimationEffect::m_fFrameInterval
	float ___m_fFrameInterval_18;
	// System.Single CFrameAnimationEffect::m_fTimeCount
	float ___m_fTimeCount_19;
	// System.Boolean CFrameAnimationEffect::m_bPlaying
	bool ___m_bPlaying_20;
	// System.Int32 CFrameAnimationEffect::m_nFrameIndex
	int32_t ___m_nFrameIndex_21;
	// System.Boolean CFrameAnimationEffect::m_bLoop
	bool ___m_bLoop_22;
	// System.Boolean CFrameAnimationEffect::m_bReverse
	bool ___m_bReverse_23;
	// System.Boolean CFrameAnimationEffect::m_bCurDir
	bool ___m_bCurDir_24;
	// System.Single CFrameAnimationEffect::m_fLoopInterval
	float ___m_fLoopInterval_25;
	// System.Boolean CFrameAnimationEffect::m_bWaiting
	bool ___m_bWaiting_26;
	// System.Single CFrameAnimationEffect::m_fWaitingTimeCount
	float ___m_fWaitingTimeCount_27;
	// System.Boolean CFrameAnimationEffect::m_bPlayAuto
	bool ___m_bPlayAuto_28;
	// System.Int32 CFrameAnimationEffect::m_nLoopTimes
	int32_t ___m_nLoopTimes_29;
	// System.Boolean CFrameAnimationEffect::m_bHideWhenEnd
	bool ___m_bHideWhenEnd_30;
	// System.Boolean CFrameAnimationEffect::m_bDestroyWhenEnd
	bool ___m_bDestroyWhenEnd_31;
	// System.Int32 CFrameAnimationEffect::m_nId
	int32_t ___m_nId_32;

public:
	inline static int32_t get_offset_of_m_aryFrameSprites_17() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_aryFrameSprites_17)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryFrameSprites_17() const { return ___m_aryFrameSprites_17; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryFrameSprites_17() { return &___m_aryFrameSprites_17; }
	inline void set_m_aryFrameSprites_17(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryFrameSprites_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFrameSprites_17), value);
	}

	inline static int32_t get_offset_of_m_fFrameInterval_18() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_fFrameInterval_18)); }
	inline float get_m_fFrameInterval_18() const { return ___m_fFrameInterval_18; }
	inline float* get_address_of_m_fFrameInterval_18() { return &___m_fFrameInterval_18; }
	inline void set_m_fFrameInterval_18(float value)
	{
		___m_fFrameInterval_18 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCount_19() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_fTimeCount_19)); }
	inline float get_m_fTimeCount_19() const { return ___m_fTimeCount_19; }
	inline float* get_address_of_m_fTimeCount_19() { return &___m_fTimeCount_19; }
	inline void set_m_fTimeCount_19(float value)
	{
		___m_fTimeCount_19 = value;
	}

	inline static int32_t get_offset_of_m_bPlaying_20() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bPlaying_20)); }
	inline bool get_m_bPlaying_20() const { return ___m_bPlaying_20; }
	inline bool* get_address_of_m_bPlaying_20() { return &___m_bPlaying_20; }
	inline void set_m_bPlaying_20(bool value)
	{
		___m_bPlaying_20 = value;
	}

	inline static int32_t get_offset_of_m_nFrameIndex_21() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_nFrameIndex_21)); }
	inline int32_t get_m_nFrameIndex_21() const { return ___m_nFrameIndex_21; }
	inline int32_t* get_address_of_m_nFrameIndex_21() { return &___m_nFrameIndex_21; }
	inline void set_m_nFrameIndex_21(int32_t value)
	{
		___m_nFrameIndex_21 = value;
	}

	inline static int32_t get_offset_of_m_bLoop_22() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bLoop_22)); }
	inline bool get_m_bLoop_22() const { return ___m_bLoop_22; }
	inline bool* get_address_of_m_bLoop_22() { return &___m_bLoop_22; }
	inline void set_m_bLoop_22(bool value)
	{
		___m_bLoop_22 = value;
	}

	inline static int32_t get_offset_of_m_bReverse_23() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bReverse_23)); }
	inline bool get_m_bReverse_23() const { return ___m_bReverse_23; }
	inline bool* get_address_of_m_bReverse_23() { return &___m_bReverse_23; }
	inline void set_m_bReverse_23(bool value)
	{
		___m_bReverse_23 = value;
	}

	inline static int32_t get_offset_of_m_bCurDir_24() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bCurDir_24)); }
	inline bool get_m_bCurDir_24() const { return ___m_bCurDir_24; }
	inline bool* get_address_of_m_bCurDir_24() { return &___m_bCurDir_24; }
	inline void set_m_bCurDir_24(bool value)
	{
		___m_bCurDir_24 = value;
	}

	inline static int32_t get_offset_of_m_fLoopInterval_25() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_fLoopInterval_25)); }
	inline float get_m_fLoopInterval_25() const { return ___m_fLoopInterval_25; }
	inline float* get_address_of_m_fLoopInterval_25() { return &___m_fLoopInterval_25; }
	inline void set_m_fLoopInterval_25(float value)
	{
		___m_fLoopInterval_25 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting_26() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bWaiting_26)); }
	inline bool get_m_bWaiting_26() const { return ___m_bWaiting_26; }
	inline bool* get_address_of_m_bWaiting_26() { return &___m_bWaiting_26; }
	inline void set_m_bWaiting_26(bool value)
	{
		___m_bWaiting_26 = value;
	}

	inline static int32_t get_offset_of_m_fWaitingTimeCount_27() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_fWaitingTimeCount_27)); }
	inline float get_m_fWaitingTimeCount_27() const { return ___m_fWaitingTimeCount_27; }
	inline float* get_address_of_m_fWaitingTimeCount_27() { return &___m_fWaitingTimeCount_27; }
	inline void set_m_fWaitingTimeCount_27(float value)
	{
		___m_fWaitingTimeCount_27 = value;
	}

	inline static int32_t get_offset_of_m_bPlayAuto_28() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bPlayAuto_28)); }
	inline bool get_m_bPlayAuto_28() const { return ___m_bPlayAuto_28; }
	inline bool* get_address_of_m_bPlayAuto_28() { return &___m_bPlayAuto_28; }
	inline void set_m_bPlayAuto_28(bool value)
	{
		___m_bPlayAuto_28 = value;
	}

	inline static int32_t get_offset_of_m_nLoopTimes_29() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_nLoopTimes_29)); }
	inline int32_t get_m_nLoopTimes_29() const { return ___m_nLoopTimes_29; }
	inline int32_t* get_address_of_m_nLoopTimes_29() { return &___m_nLoopTimes_29; }
	inline void set_m_nLoopTimes_29(int32_t value)
	{
		___m_nLoopTimes_29 = value;
	}

	inline static int32_t get_offset_of_m_bHideWhenEnd_30() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bHideWhenEnd_30)); }
	inline bool get_m_bHideWhenEnd_30() const { return ___m_bHideWhenEnd_30; }
	inline bool* get_address_of_m_bHideWhenEnd_30() { return &___m_bHideWhenEnd_30; }
	inline void set_m_bHideWhenEnd_30(bool value)
	{
		___m_bHideWhenEnd_30 = value;
	}

	inline static int32_t get_offset_of_m_bDestroyWhenEnd_31() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_bDestroyWhenEnd_31)); }
	inline bool get_m_bDestroyWhenEnd_31() const { return ___m_bDestroyWhenEnd_31; }
	inline bool* get_address_of_m_bDestroyWhenEnd_31() { return &___m_bDestroyWhenEnd_31; }
	inline void set_m_bDestroyWhenEnd_31(bool value)
	{
		___m_bDestroyWhenEnd_31 = value;
	}

	inline static int32_t get_offset_of_m_nId_32() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_t443605508, ___m_nId_32)); }
	inline int32_t get_m_nId_32() const { return ___m_nId_32; }
	inline int32_t* get_address_of_m_nId_32() { return &___m_nId_32; }
	inline void set_m_nId_32(int32_t value)
	{
		___m_nId_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRAMEANIMATIONEFFECT_T443605508_H
#ifndef BLOOMOPTIMIZED_T2685819829_H
#define BLOOMOPTIMIZED_T2685819829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomOptimized
struct  BloomOptimized_t2685819829  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::threshold
	float ___threshold_6;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::intensity
	float ___intensity_7;
	// System.Single UnityStandardAssets.ImageEffects.BloomOptimized::blurSize
	float ___blurSize_8;
	// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution UnityStandardAssets.ImageEffects.BloomOptimized::resolution
	int32_t ___resolution_9;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomOptimized::blurIterations
	int32_t ___blurIterations_10;
	// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType UnityStandardAssets.ImageEffects.BloomOptimized::blurType
	int32_t ___blurType_11;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomShader
	Shader_t4151988712 * ___fastBloomShader_12;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomOptimized::fastBloomMaterial
	Material_t340375123 * ___fastBloomMaterial_13;

public:
	inline static int32_t get_offset_of_threshold_6() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___threshold_6)); }
	inline float get_threshold_6() const { return ___threshold_6; }
	inline float* get_address_of_threshold_6() { return &___threshold_6; }
	inline void set_threshold_6(float value)
	{
		___threshold_6 = value;
	}

	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_blurSize_8() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___blurSize_8)); }
	inline float get_blurSize_8() const { return ___blurSize_8; }
	inline float* get_address_of_blurSize_8() { return &___blurSize_8; }
	inline void set_blurSize_8(float value)
	{
		___blurSize_8 = value;
	}

	inline static int32_t get_offset_of_resolution_9() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___resolution_9)); }
	inline int32_t get_resolution_9() const { return ___resolution_9; }
	inline int32_t* get_address_of_resolution_9() { return &___resolution_9; }
	inline void set_resolution_9(int32_t value)
	{
		___resolution_9 = value;
	}

	inline static int32_t get_offset_of_blurIterations_10() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___blurIterations_10)); }
	inline int32_t get_blurIterations_10() const { return ___blurIterations_10; }
	inline int32_t* get_address_of_blurIterations_10() { return &___blurIterations_10; }
	inline void set_blurIterations_10(int32_t value)
	{
		___blurIterations_10 = value;
	}

	inline static int32_t get_offset_of_blurType_11() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___blurType_11)); }
	inline int32_t get_blurType_11() const { return ___blurType_11; }
	inline int32_t* get_address_of_blurType_11() { return &___blurType_11; }
	inline void set_blurType_11(int32_t value)
	{
		___blurType_11 = value;
	}

	inline static int32_t get_offset_of_fastBloomShader_12() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___fastBloomShader_12)); }
	inline Shader_t4151988712 * get_fastBloomShader_12() const { return ___fastBloomShader_12; }
	inline Shader_t4151988712 ** get_address_of_fastBloomShader_12() { return &___fastBloomShader_12; }
	inline void set_fastBloomShader_12(Shader_t4151988712 * value)
	{
		___fastBloomShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomShader_12), value);
	}

	inline static int32_t get_offset_of_fastBloomMaterial_13() { return static_cast<int32_t>(offsetof(BloomOptimized_t2685819829, ___fastBloomMaterial_13)); }
	inline Material_t340375123 * get_fastBloomMaterial_13() const { return ___fastBloomMaterial_13; }
	inline Material_t340375123 ** get_address_of_fastBloomMaterial_13() { return &___fastBloomMaterial_13; }
	inline void set_fastBloomMaterial_13(Material_t340375123 * value)
	{
		___fastBloomMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___fastBloomMaterial_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMOPTIMIZED_T2685819829_H
#ifndef BLOOMANDFLARES_T2848767628_H
#define BLOOMANDFLARES_T2848767628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BloomAndFlares
struct  BloomAndFlares_t2848767628  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.TweakMode34 UnityStandardAssets.ImageEffects.BloomAndFlares::tweakMode
	int32_t ___tweakMode_6;
	// UnityStandardAssets.ImageEffects.BloomScreenBlendMode UnityStandardAssets.ImageEffects.BloomAndFlares::screenBlendMode
	int32_t ___screenBlendMode_7;
	// UnityStandardAssets.ImageEffects.HDRBloomMode UnityStandardAssets.ImageEffects.BloomAndFlares::hdr
	int32_t ___hdr_8;
	// System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::doHdr
	bool ___doHdr_9;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::sepBlurSpread
	float ___sepBlurSpread_10;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::useSrcAlphaAsMask
	float ___useSrcAlphaAsMask_11;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::bloomIntensity
	float ___bloomIntensity_12;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::bloomThreshold
	float ___bloomThreshold_13;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomAndFlares::bloomBlurIterations
	int32_t ___bloomBlurIterations_14;
	// System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::lensflares
	bool ___lensflares_15;
	// System.Int32 UnityStandardAssets.ImageEffects.BloomAndFlares::hollywoodFlareBlurIterations
	int32_t ___hollywoodFlareBlurIterations_16;
	// UnityStandardAssets.ImageEffects.LensflareStyle34 UnityStandardAssets.ImageEffects.BloomAndFlares::lensflareMode
	int32_t ___lensflareMode_17;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::hollyStretchWidth
	float ___hollyStretchWidth_18;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::lensflareIntensity
	float ___lensflareIntensity_19;
	// System.Single UnityStandardAssets.ImageEffects.BloomAndFlares::lensflareThreshold
	float ___lensflareThreshold_20;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorA
	Color_t2555686324  ___flareColorA_21;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorB
	Color_t2555686324  ___flareColorB_22;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorC
	Color_t2555686324  ___flareColorC_23;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.BloomAndFlares::flareColorD
	Color_t2555686324  ___flareColorD_24;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.BloomAndFlares::lensFlareVignetteMask
	Texture2D_t3840446185 * ___lensFlareVignetteMask_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::lensFlareShader
	Shader_t4151988712 * ___lensFlareShader_26;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::lensFlareMaterial
	Material_t340375123 * ___lensFlareMaterial_27;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::vignetteShader
	Shader_t4151988712 * ___vignetteShader_28;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::vignetteMaterial
	Material_t340375123 * ___vignetteMaterial_29;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::separableBlurShader
	Shader_t4151988712 * ___separableBlurShader_30;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::separableBlurMaterial
	Material_t340375123 * ___separableBlurMaterial_31;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::addBrightStuffOneOneShader
	Shader_t4151988712 * ___addBrightStuffOneOneShader_32;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::addBrightStuffBlendOneOneMaterial
	Material_t340375123 * ___addBrightStuffBlendOneOneMaterial_33;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::screenBlendShader
	Shader_t4151988712 * ___screenBlendShader_34;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::screenBlend
	Material_t340375123 * ___screenBlend_35;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::hollywoodFlaresShader
	Shader_t4151988712 * ___hollywoodFlaresShader_36;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::hollywoodFlaresMaterial
	Material_t340375123 * ___hollywoodFlaresMaterial_37;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BloomAndFlares::brightPassFilterShader
	Shader_t4151988712 * ___brightPassFilterShader_38;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BloomAndFlares::brightPassFilterMaterial
	Material_t340375123 * ___brightPassFilterMaterial_39;

public:
	inline static int32_t get_offset_of_tweakMode_6() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___tweakMode_6)); }
	inline int32_t get_tweakMode_6() const { return ___tweakMode_6; }
	inline int32_t* get_address_of_tweakMode_6() { return &___tweakMode_6; }
	inline void set_tweakMode_6(int32_t value)
	{
		___tweakMode_6 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_7() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___screenBlendMode_7)); }
	inline int32_t get_screenBlendMode_7() const { return ___screenBlendMode_7; }
	inline int32_t* get_address_of_screenBlendMode_7() { return &___screenBlendMode_7; }
	inline void set_screenBlendMode_7(int32_t value)
	{
		___screenBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_hdr_8() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___hdr_8)); }
	inline int32_t get_hdr_8() const { return ___hdr_8; }
	inline int32_t* get_address_of_hdr_8() { return &___hdr_8; }
	inline void set_hdr_8(int32_t value)
	{
		___hdr_8 = value;
	}

	inline static int32_t get_offset_of_doHdr_9() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___doHdr_9)); }
	inline bool get_doHdr_9() const { return ___doHdr_9; }
	inline bool* get_address_of_doHdr_9() { return &___doHdr_9; }
	inline void set_doHdr_9(bool value)
	{
		___doHdr_9 = value;
	}

	inline static int32_t get_offset_of_sepBlurSpread_10() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___sepBlurSpread_10)); }
	inline float get_sepBlurSpread_10() const { return ___sepBlurSpread_10; }
	inline float* get_address_of_sepBlurSpread_10() { return &___sepBlurSpread_10; }
	inline void set_sepBlurSpread_10(float value)
	{
		___sepBlurSpread_10 = value;
	}

	inline static int32_t get_offset_of_useSrcAlphaAsMask_11() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___useSrcAlphaAsMask_11)); }
	inline float get_useSrcAlphaAsMask_11() const { return ___useSrcAlphaAsMask_11; }
	inline float* get_address_of_useSrcAlphaAsMask_11() { return &___useSrcAlphaAsMask_11; }
	inline void set_useSrcAlphaAsMask_11(float value)
	{
		___useSrcAlphaAsMask_11 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_12() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___bloomIntensity_12)); }
	inline float get_bloomIntensity_12() const { return ___bloomIntensity_12; }
	inline float* get_address_of_bloomIntensity_12() { return &___bloomIntensity_12; }
	inline void set_bloomIntensity_12(float value)
	{
		___bloomIntensity_12 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_13() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___bloomThreshold_13)); }
	inline float get_bloomThreshold_13() const { return ___bloomThreshold_13; }
	inline float* get_address_of_bloomThreshold_13() { return &___bloomThreshold_13; }
	inline void set_bloomThreshold_13(float value)
	{
		___bloomThreshold_13 = value;
	}

	inline static int32_t get_offset_of_bloomBlurIterations_14() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___bloomBlurIterations_14)); }
	inline int32_t get_bloomBlurIterations_14() const { return ___bloomBlurIterations_14; }
	inline int32_t* get_address_of_bloomBlurIterations_14() { return &___bloomBlurIterations_14; }
	inline void set_bloomBlurIterations_14(int32_t value)
	{
		___bloomBlurIterations_14 = value;
	}

	inline static int32_t get_offset_of_lensflares_15() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensflares_15)); }
	inline bool get_lensflares_15() const { return ___lensflares_15; }
	inline bool* get_address_of_lensflares_15() { return &___lensflares_15; }
	inline void set_lensflares_15(bool value)
	{
		___lensflares_15 = value;
	}

	inline static int32_t get_offset_of_hollywoodFlareBlurIterations_16() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___hollywoodFlareBlurIterations_16)); }
	inline int32_t get_hollywoodFlareBlurIterations_16() const { return ___hollywoodFlareBlurIterations_16; }
	inline int32_t* get_address_of_hollywoodFlareBlurIterations_16() { return &___hollywoodFlareBlurIterations_16; }
	inline void set_hollywoodFlareBlurIterations_16(int32_t value)
	{
		___hollywoodFlareBlurIterations_16 = value;
	}

	inline static int32_t get_offset_of_lensflareMode_17() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensflareMode_17)); }
	inline int32_t get_lensflareMode_17() const { return ___lensflareMode_17; }
	inline int32_t* get_address_of_lensflareMode_17() { return &___lensflareMode_17; }
	inline void set_lensflareMode_17(int32_t value)
	{
		___lensflareMode_17 = value;
	}

	inline static int32_t get_offset_of_hollyStretchWidth_18() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___hollyStretchWidth_18)); }
	inline float get_hollyStretchWidth_18() const { return ___hollyStretchWidth_18; }
	inline float* get_address_of_hollyStretchWidth_18() { return &___hollyStretchWidth_18; }
	inline void set_hollyStretchWidth_18(float value)
	{
		___hollyStretchWidth_18 = value;
	}

	inline static int32_t get_offset_of_lensflareIntensity_19() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensflareIntensity_19)); }
	inline float get_lensflareIntensity_19() const { return ___lensflareIntensity_19; }
	inline float* get_address_of_lensflareIntensity_19() { return &___lensflareIntensity_19; }
	inline void set_lensflareIntensity_19(float value)
	{
		___lensflareIntensity_19 = value;
	}

	inline static int32_t get_offset_of_lensflareThreshold_20() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensflareThreshold_20)); }
	inline float get_lensflareThreshold_20() const { return ___lensflareThreshold_20; }
	inline float* get_address_of_lensflareThreshold_20() { return &___lensflareThreshold_20; }
	inline void set_lensflareThreshold_20(float value)
	{
		___lensflareThreshold_20 = value;
	}

	inline static int32_t get_offset_of_flareColorA_21() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___flareColorA_21)); }
	inline Color_t2555686324  get_flareColorA_21() const { return ___flareColorA_21; }
	inline Color_t2555686324 * get_address_of_flareColorA_21() { return &___flareColorA_21; }
	inline void set_flareColorA_21(Color_t2555686324  value)
	{
		___flareColorA_21 = value;
	}

	inline static int32_t get_offset_of_flareColorB_22() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___flareColorB_22)); }
	inline Color_t2555686324  get_flareColorB_22() const { return ___flareColorB_22; }
	inline Color_t2555686324 * get_address_of_flareColorB_22() { return &___flareColorB_22; }
	inline void set_flareColorB_22(Color_t2555686324  value)
	{
		___flareColorB_22 = value;
	}

	inline static int32_t get_offset_of_flareColorC_23() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___flareColorC_23)); }
	inline Color_t2555686324  get_flareColorC_23() const { return ___flareColorC_23; }
	inline Color_t2555686324 * get_address_of_flareColorC_23() { return &___flareColorC_23; }
	inline void set_flareColorC_23(Color_t2555686324  value)
	{
		___flareColorC_23 = value;
	}

	inline static int32_t get_offset_of_flareColorD_24() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___flareColorD_24)); }
	inline Color_t2555686324  get_flareColorD_24() const { return ___flareColorD_24; }
	inline Color_t2555686324 * get_address_of_flareColorD_24() { return &___flareColorD_24; }
	inline void set_flareColorD_24(Color_t2555686324  value)
	{
		___flareColorD_24 = value;
	}

	inline static int32_t get_offset_of_lensFlareVignetteMask_25() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensFlareVignetteMask_25)); }
	inline Texture2D_t3840446185 * get_lensFlareVignetteMask_25() const { return ___lensFlareVignetteMask_25; }
	inline Texture2D_t3840446185 ** get_address_of_lensFlareVignetteMask_25() { return &___lensFlareVignetteMask_25; }
	inline void set_lensFlareVignetteMask_25(Texture2D_t3840446185 * value)
	{
		___lensFlareVignetteMask_25 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareVignetteMask_25), value);
	}

	inline static int32_t get_offset_of_lensFlareShader_26() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensFlareShader_26)); }
	inline Shader_t4151988712 * get_lensFlareShader_26() const { return ___lensFlareShader_26; }
	inline Shader_t4151988712 ** get_address_of_lensFlareShader_26() { return &___lensFlareShader_26; }
	inline void set_lensFlareShader_26(Shader_t4151988712 * value)
	{
		___lensFlareShader_26 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareShader_26), value);
	}

	inline static int32_t get_offset_of_lensFlareMaterial_27() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___lensFlareMaterial_27)); }
	inline Material_t340375123 * get_lensFlareMaterial_27() const { return ___lensFlareMaterial_27; }
	inline Material_t340375123 ** get_address_of_lensFlareMaterial_27() { return &___lensFlareMaterial_27; }
	inline void set_lensFlareMaterial_27(Material_t340375123 * value)
	{
		___lensFlareMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareMaterial_27), value);
	}

	inline static int32_t get_offset_of_vignetteShader_28() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___vignetteShader_28)); }
	inline Shader_t4151988712 * get_vignetteShader_28() const { return ___vignetteShader_28; }
	inline Shader_t4151988712 ** get_address_of_vignetteShader_28() { return &___vignetteShader_28; }
	inline void set_vignetteShader_28(Shader_t4151988712 * value)
	{
		___vignetteShader_28 = value;
		Il2CppCodeGenWriteBarrier((&___vignetteShader_28), value);
	}

	inline static int32_t get_offset_of_vignetteMaterial_29() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___vignetteMaterial_29)); }
	inline Material_t340375123 * get_vignetteMaterial_29() const { return ___vignetteMaterial_29; }
	inline Material_t340375123 ** get_address_of_vignetteMaterial_29() { return &___vignetteMaterial_29; }
	inline void set_vignetteMaterial_29(Material_t340375123 * value)
	{
		___vignetteMaterial_29 = value;
		Il2CppCodeGenWriteBarrier((&___vignetteMaterial_29), value);
	}

	inline static int32_t get_offset_of_separableBlurShader_30() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___separableBlurShader_30)); }
	inline Shader_t4151988712 * get_separableBlurShader_30() const { return ___separableBlurShader_30; }
	inline Shader_t4151988712 ** get_address_of_separableBlurShader_30() { return &___separableBlurShader_30; }
	inline void set_separableBlurShader_30(Shader_t4151988712 * value)
	{
		___separableBlurShader_30 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurShader_30), value);
	}

	inline static int32_t get_offset_of_separableBlurMaterial_31() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___separableBlurMaterial_31)); }
	inline Material_t340375123 * get_separableBlurMaterial_31() const { return ___separableBlurMaterial_31; }
	inline Material_t340375123 ** get_address_of_separableBlurMaterial_31() { return &___separableBlurMaterial_31; }
	inline void set_separableBlurMaterial_31(Material_t340375123 * value)
	{
		___separableBlurMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurMaterial_31), value);
	}

	inline static int32_t get_offset_of_addBrightStuffOneOneShader_32() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___addBrightStuffOneOneShader_32)); }
	inline Shader_t4151988712 * get_addBrightStuffOneOneShader_32() const { return ___addBrightStuffOneOneShader_32; }
	inline Shader_t4151988712 ** get_address_of_addBrightStuffOneOneShader_32() { return &___addBrightStuffOneOneShader_32; }
	inline void set_addBrightStuffOneOneShader_32(Shader_t4151988712 * value)
	{
		___addBrightStuffOneOneShader_32 = value;
		Il2CppCodeGenWriteBarrier((&___addBrightStuffOneOneShader_32), value);
	}

	inline static int32_t get_offset_of_addBrightStuffBlendOneOneMaterial_33() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___addBrightStuffBlendOneOneMaterial_33)); }
	inline Material_t340375123 * get_addBrightStuffBlendOneOneMaterial_33() const { return ___addBrightStuffBlendOneOneMaterial_33; }
	inline Material_t340375123 ** get_address_of_addBrightStuffBlendOneOneMaterial_33() { return &___addBrightStuffBlendOneOneMaterial_33; }
	inline void set_addBrightStuffBlendOneOneMaterial_33(Material_t340375123 * value)
	{
		___addBrightStuffBlendOneOneMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___addBrightStuffBlendOneOneMaterial_33), value);
	}

	inline static int32_t get_offset_of_screenBlendShader_34() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___screenBlendShader_34)); }
	inline Shader_t4151988712 * get_screenBlendShader_34() const { return ___screenBlendShader_34; }
	inline Shader_t4151988712 ** get_address_of_screenBlendShader_34() { return &___screenBlendShader_34; }
	inline void set_screenBlendShader_34(Shader_t4151988712 * value)
	{
		___screenBlendShader_34 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlendShader_34), value);
	}

	inline static int32_t get_offset_of_screenBlend_35() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___screenBlend_35)); }
	inline Material_t340375123 * get_screenBlend_35() const { return ___screenBlend_35; }
	inline Material_t340375123 ** get_address_of_screenBlend_35() { return &___screenBlend_35; }
	inline void set_screenBlend_35(Material_t340375123 * value)
	{
		___screenBlend_35 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlend_35), value);
	}

	inline static int32_t get_offset_of_hollywoodFlaresShader_36() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___hollywoodFlaresShader_36)); }
	inline Shader_t4151988712 * get_hollywoodFlaresShader_36() const { return ___hollywoodFlaresShader_36; }
	inline Shader_t4151988712 ** get_address_of_hollywoodFlaresShader_36() { return &___hollywoodFlaresShader_36; }
	inline void set_hollywoodFlaresShader_36(Shader_t4151988712 * value)
	{
		___hollywoodFlaresShader_36 = value;
		Il2CppCodeGenWriteBarrier((&___hollywoodFlaresShader_36), value);
	}

	inline static int32_t get_offset_of_hollywoodFlaresMaterial_37() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___hollywoodFlaresMaterial_37)); }
	inline Material_t340375123 * get_hollywoodFlaresMaterial_37() const { return ___hollywoodFlaresMaterial_37; }
	inline Material_t340375123 ** get_address_of_hollywoodFlaresMaterial_37() { return &___hollywoodFlaresMaterial_37; }
	inline void set_hollywoodFlaresMaterial_37(Material_t340375123 * value)
	{
		___hollywoodFlaresMaterial_37 = value;
		Il2CppCodeGenWriteBarrier((&___hollywoodFlaresMaterial_37), value);
	}

	inline static int32_t get_offset_of_brightPassFilterShader_38() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___brightPassFilterShader_38)); }
	inline Shader_t4151988712 * get_brightPassFilterShader_38() const { return ___brightPassFilterShader_38; }
	inline Shader_t4151988712 ** get_address_of_brightPassFilterShader_38() { return &___brightPassFilterShader_38; }
	inline void set_brightPassFilterShader_38(Shader_t4151988712 * value)
	{
		___brightPassFilterShader_38 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterShader_38), value);
	}

	inline static int32_t get_offset_of_brightPassFilterMaterial_39() { return static_cast<int32_t>(offsetof(BloomAndFlares_t2848767628, ___brightPassFilterMaterial_39)); }
	inline Material_t340375123 * get_brightPassFilterMaterial_39() const { return ___brightPassFilterMaterial_39; }
	inline Material_t340375123 ** get_address_of_brightPassFilterMaterial_39() { return &___brightPassFilterMaterial_39; }
	inline void set_brightPassFilterMaterial_39(Material_t340375123 * value)
	{
		___brightPassFilterMaterial_39 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterMaterial_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMANDFLARES_T2848767628_H
#ifndef BLOOM_T1125654350_H
#define BLOOM_T1125654350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom
struct  Bloom_t1125654350  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.Bloom/TweakMode UnityStandardAssets.ImageEffects.Bloom::tweakMode
	int32_t ___tweakMode_6;
	// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode UnityStandardAssets.ImageEffects.Bloom::screenBlendMode
	int32_t ___screenBlendMode_7;
	// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode UnityStandardAssets.ImageEffects.Bloom::hdr
	int32_t ___hdr_8;
	// System.Boolean UnityStandardAssets.ImageEffects.Bloom::doHdr
	bool ___doHdr_9;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::sepBlurSpread
	float ___sepBlurSpread_10;
	// UnityStandardAssets.ImageEffects.Bloom/BloomQuality UnityStandardAssets.ImageEffects.Bloom::quality
	int32_t ___quality_11;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::bloomIntensity
	float ___bloomIntensity_12;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::bloomThreshold
	float ___bloomThreshold_13;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::bloomThresholdColor
	Color_t2555686324  ___bloomThresholdColor_14;
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom::bloomBlurIterations
	int32_t ___bloomBlurIterations_15;
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom::hollywoodFlareBlurIterations
	int32_t ___hollywoodFlareBlurIterations_16;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::flareRotation
	float ___flareRotation_17;
	// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle UnityStandardAssets.ImageEffects.Bloom::lensflareMode
	int32_t ___lensflareMode_18;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::hollyStretchWidth
	float ___hollyStretchWidth_19;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensflareIntensity
	float ___lensflareIntensity_20;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensflareThreshold
	float ___lensflareThreshold_21;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensFlareSaturation
	float ___lensFlareSaturation_22;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorA
	Color_t2555686324  ___flareColorA_23;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorB
	Color_t2555686324  ___flareColorB_24;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorC
	Color_t2555686324  ___flareColorC_25;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorD
	Color_t2555686324  ___flareColorD_26;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.Bloom::lensFlareVignetteMask
	Texture2D_t3840446185 * ___lensFlareVignetteMask_27;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::lensFlareShader
	Shader_t4151988712 * ___lensFlareShader_28;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::lensFlareMaterial
	Material_t340375123 * ___lensFlareMaterial_29;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::screenBlendShader
	Shader_t4151988712 * ___screenBlendShader_30;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::screenBlend
	Material_t340375123 * ___screenBlend_31;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::blurAndFlaresShader
	Shader_t4151988712 * ___blurAndFlaresShader_32;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::blurAndFlaresMaterial
	Material_t340375123 * ___blurAndFlaresMaterial_33;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::brightPassFilterShader
	Shader_t4151988712 * ___brightPassFilterShader_34;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::brightPassFilterMaterial
	Material_t340375123 * ___brightPassFilterMaterial_35;

public:
	inline static int32_t get_offset_of_tweakMode_6() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___tweakMode_6)); }
	inline int32_t get_tweakMode_6() const { return ___tweakMode_6; }
	inline int32_t* get_address_of_tweakMode_6() { return &___tweakMode_6; }
	inline void set_tweakMode_6(int32_t value)
	{
		___tweakMode_6 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_7() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___screenBlendMode_7)); }
	inline int32_t get_screenBlendMode_7() const { return ___screenBlendMode_7; }
	inline int32_t* get_address_of_screenBlendMode_7() { return &___screenBlendMode_7; }
	inline void set_screenBlendMode_7(int32_t value)
	{
		___screenBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_hdr_8() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___hdr_8)); }
	inline int32_t get_hdr_8() const { return ___hdr_8; }
	inline int32_t* get_address_of_hdr_8() { return &___hdr_8; }
	inline void set_hdr_8(int32_t value)
	{
		___hdr_8 = value;
	}

	inline static int32_t get_offset_of_doHdr_9() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___doHdr_9)); }
	inline bool get_doHdr_9() const { return ___doHdr_9; }
	inline bool* get_address_of_doHdr_9() { return &___doHdr_9; }
	inline void set_doHdr_9(bool value)
	{
		___doHdr_9 = value;
	}

	inline static int32_t get_offset_of_sepBlurSpread_10() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___sepBlurSpread_10)); }
	inline float get_sepBlurSpread_10() const { return ___sepBlurSpread_10; }
	inline float* get_address_of_sepBlurSpread_10() { return &___sepBlurSpread_10; }
	inline void set_sepBlurSpread_10(float value)
	{
		___sepBlurSpread_10 = value;
	}

	inline static int32_t get_offset_of_quality_11() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___quality_11)); }
	inline int32_t get_quality_11() const { return ___quality_11; }
	inline int32_t* get_address_of_quality_11() { return &___quality_11; }
	inline void set_quality_11(int32_t value)
	{
		___quality_11 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_12() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomIntensity_12)); }
	inline float get_bloomIntensity_12() const { return ___bloomIntensity_12; }
	inline float* get_address_of_bloomIntensity_12() { return &___bloomIntensity_12; }
	inline void set_bloomIntensity_12(float value)
	{
		___bloomIntensity_12 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_13() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomThreshold_13)); }
	inline float get_bloomThreshold_13() const { return ___bloomThreshold_13; }
	inline float* get_address_of_bloomThreshold_13() { return &___bloomThreshold_13; }
	inline void set_bloomThreshold_13(float value)
	{
		___bloomThreshold_13 = value;
	}

	inline static int32_t get_offset_of_bloomThresholdColor_14() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomThresholdColor_14)); }
	inline Color_t2555686324  get_bloomThresholdColor_14() const { return ___bloomThresholdColor_14; }
	inline Color_t2555686324 * get_address_of_bloomThresholdColor_14() { return &___bloomThresholdColor_14; }
	inline void set_bloomThresholdColor_14(Color_t2555686324  value)
	{
		___bloomThresholdColor_14 = value;
	}

	inline static int32_t get_offset_of_bloomBlurIterations_15() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomBlurIterations_15)); }
	inline int32_t get_bloomBlurIterations_15() const { return ___bloomBlurIterations_15; }
	inline int32_t* get_address_of_bloomBlurIterations_15() { return &___bloomBlurIterations_15; }
	inline void set_bloomBlurIterations_15(int32_t value)
	{
		___bloomBlurIterations_15 = value;
	}

	inline static int32_t get_offset_of_hollywoodFlareBlurIterations_16() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___hollywoodFlareBlurIterations_16)); }
	inline int32_t get_hollywoodFlareBlurIterations_16() const { return ___hollywoodFlareBlurIterations_16; }
	inline int32_t* get_address_of_hollywoodFlareBlurIterations_16() { return &___hollywoodFlareBlurIterations_16; }
	inline void set_hollywoodFlareBlurIterations_16(int32_t value)
	{
		___hollywoodFlareBlurIterations_16 = value;
	}

	inline static int32_t get_offset_of_flareRotation_17() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareRotation_17)); }
	inline float get_flareRotation_17() const { return ___flareRotation_17; }
	inline float* get_address_of_flareRotation_17() { return &___flareRotation_17; }
	inline void set_flareRotation_17(float value)
	{
		___flareRotation_17 = value;
	}

	inline static int32_t get_offset_of_lensflareMode_18() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensflareMode_18)); }
	inline int32_t get_lensflareMode_18() const { return ___lensflareMode_18; }
	inline int32_t* get_address_of_lensflareMode_18() { return &___lensflareMode_18; }
	inline void set_lensflareMode_18(int32_t value)
	{
		___lensflareMode_18 = value;
	}

	inline static int32_t get_offset_of_hollyStretchWidth_19() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___hollyStretchWidth_19)); }
	inline float get_hollyStretchWidth_19() const { return ___hollyStretchWidth_19; }
	inline float* get_address_of_hollyStretchWidth_19() { return &___hollyStretchWidth_19; }
	inline void set_hollyStretchWidth_19(float value)
	{
		___hollyStretchWidth_19 = value;
	}

	inline static int32_t get_offset_of_lensflareIntensity_20() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensflareIntensity_20)); }
	inline float get_lensflareIntensity_20() const { return ___lensflareIntensity_20; }
	inline float* get_address_of_lensflareIntensity_20() { return &___lensflareIntensity_20; }
	inline void set_lensflareIntensity_20(float value)
	{
		___lensflareIntensity_20 = value;
	}

	inline static int32_t get_offset_of_lensflareThreshold_21() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensflareThreshold_21)); }
	inline float get_lensflareThreshold_21() const { return ___lensflareThreshold_21; }
	inline float* get_address_of_lensflareThreshold_21() { return &___lensflareThreshold_21; }
	inline void set_lensflareThreshold_21(float value)
	{
		___lensflareThreshold_21 = value;
	}

	inline static int32_t get_offset_of_lensFlareSaturation_22() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareSaturation_22)); }
	inline float get_lensFlareSaturation_22() const { return ___lensFlareSaturation_22; }
	inline float* get_address_of_lensFlareSaturation_22() { return &___lensFlareSaturation_22; }
	inline void set_lensFlareSaturation_22(float value)
	{
		___lensFlareSaturation_22 = value;
	}

	inline static int32_t get_offset_of_flareColorA_23() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorA_23)); }
	inline Color_t2555686324  get_flareColorA_23() const { return ___flareColorA_23; }
	inline Color_t2555686324 * get_address_of_flareColorA_23() { return &___flareColorA_23; }
	inline void set_flareColorA_23(Color_t2555686324  value)
	{
		___flareColorA_23 = value;
	}

	inline static int32_t get_offset_of_flareColorB_24() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorB_24)); }
	inline Color_t2555686324  get_flareColorB_24() const { return ___flareColorB_24; }
	inline Color_t2555686324 * get_address_of_flareColorB_24() { return &___flareColorB_24; }
	inline void set_flareColorB_24(Color_t2555686324  value)
	{
		___flareColorB_24 = value;
	}

	inline static int32_t get_offset_of_flareColorC_25() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorC_25)); }
	inline Color_t2555686324  get_flareColorC_25() const { return ___flareColorC_25; }
	inline Color_t2555686324 * get_address_of_flareColorC_25() { return &___flareColorC_25; }
	inline void set_flareColorC_25(Color_t2555686324  value)
	{
		___flareColorC_25 = value;
	}

	inline static int32_t get_offset_of_flareColorD_26() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorD_26)); }
	inline Color_t2555686324  get_flareColorD_26() const { return ___flareColorD_26; }
	inline Color_t2555686324 * get_address_of_flareColorD_26() { return &___flareColorD_26; }
	inline void set_flareColorD_26(Color_t2555686324  value)
	{
		___flareColorD_26 = value;
	}

	inline static int32_t get_offset_of_lensFlareVignetteMask_27() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareVignetteMask_27)); }
	inline Texture2D_t3840446185 * get_lensFlareVignetteMask_27() const { return ___lensFlareVignetteMask_27; }
	inline Texture2D_t3840446185 ** get_address_of_lensFlareVignetteMask_27() { return &___lensFlareVignetteMask_27; }
	inline void set_lensFlareVignetteMask_27(Texture2D_t3840446185 * value)
	{
		___lensFlareVignetteMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareVignetteMask_27), value);
	}

	inline static int32_t get_offset_of_lensFlareShader_28() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareShader_28)); }
	inline Shader_t4151988712 * get_lensFlareShader_28() const { return ___lensFlareShader_28; }
	inline Shader_t4151988712 ** get_address_of_lensFlareShader_28() { return &___lensFlareShader_28; }
	inline void set_lensFlareShader_28(Shader_t4151988712 * value)
	{
		___lensFlareShader_28 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareShader_28), value);
	}

	inline static int32_t get_offset_of_lensFlareMaterial_29() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareMaterial_29)); }
	inline Material_t340375123 * get_lensFlareMaterial_29() const { return ___lensFlareMaterial_29; }
	inline Material_t340375123 ** get_address_of_lensFlareMaterial_29() { return &___lensFlareMaterial_29; }
	inline void set_lensFlareMaterial_29(Material_t340375123 * value)
	{
		___lensFlareMaterial_29 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareMaterial_29), value);
	}

	inline static int32_t get_offset_of_screenBlendShader_30() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___screenBlendShader_30)); }
	inline Shader_t4151988712 * get_screenBlendShader_30() const { return ___screenBlendShader_30; }
	inline Shader_t4151988712 ** get_address_of_screenBlendShader_30() { return &___screenBlendShader_30; }
	inline void set_screenBlendShader_30(Shader_t4151988712 * value)
	{
		___screenBlendShader_30 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlendShader_30), value);
	}

	inline static int32_t get_offset_of_screenBlend_31() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___screenBlend_31)); }
	inline Material_t340375123 * get_screenBlend_31() const { return ___screenBlend_31; }
	inline Material_t340375123 ** get_address_of_screenBlend_31() { return &___screenBlend_31; }
	inline void set_screenBlend_31(Material_t340375123 * value)
	{
		___screenBlend_31 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlend_31), value);
	}

	inline static int32_t get_offset_of_blurAndFlaresShader_32() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___blurAndFlaresShader_32)); }
	inline Shader_t4151988712 * get_blurAndFlaresShader_32() const { return ___blurAndFlaresShader_32; }
	inline Shader_t4151988712 ** get_address_of_blurAndFlaresShader_32() { return &___blurAndFlaresShader_32; }
	inline void set_blurAndFlaresShader_32(Shader_t4151988712 * value)
	{
		___blurAndFlaresShader_32 = value;
		Il2CppCodeGenWriteBarrier((&___blurAndFlaresShader_32), value);
	}

	inline static int32_t get_offset_of_blurAndFlaresMaterial_33() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___blurAndFlaresMaterial_33)); }
	inline Material_t340375123 * get_blurAndFlaresMaterial_33() const { return ___blurAndFlaresMaterial_33; }
	inline Material_t340375123 ** get_address_of_blurAndFlaresMaterial_33() { return &___blurAndFlaresMaterial_33; }
	inline void set_blurAndFlaresMaterial_33(Material_t340375123 * value)
	{
		___blurAndFlaresMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___blurAndFlaresMaterial_33), value);
	}

	inline static int32_t get_offset_of_brightPassFilterShader_34() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___brightPassFilterShader_34)); }
	inline Shader_t4151988712 * get_brightPassFilterShader_34() const { return ___brightPassFilterShader_34; }
	inline Shader_t4151988712 ** get_address_of_brightPassFilterShader_34() { return &___brightPassFilterShader_34; }
	inline void set_brightPassFilterShader_34(Shader_t4151988712 * value)
	{
		___brightPassFilterShader_34 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterShader_34), value);
	}

	inline static int32_t get_offset_of_brightPassFilterMaterial_35() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___brightPassFilterMaterial_35)); }
	inline Material_t340375123 * get_brightPassFilterMaterial_35() const { return ___brightPassFilterMaterial_35; }
	inline Material_t340375123 ** get_address_of_brightPassFilterMaterial_35() { return &___brightPassFilterMaterial_35; }
	inline void set_brightPassFilterMaterial_35(Material_t340375123 * value)
	{
		___brightPassFilterMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterMaterial_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOM_T1125654350_H
#ifndef ANTIALIASING_T1691315015_H
#define ANTIALIASING_T1691315015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Antialiasing
struct  Antialiasing_t1691315015  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.AAMode UnityStandardAssets.ImageEffects.Antialiasing::mode
	int32_t ___mode_6;
	// System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::showGeneratedNormals
	bool ___showGeneratedNormals_7;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::offsetScale
	float ___offsetScale_8;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::blurRadius
	float ___blurRadius_9;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::edgeThresholdMin
	float ___edgeThresholdMin_10;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::edgeThreshold
	float ___edgeThreshold_11;
	// System.Single UnityStandardAssets.ImageEffects.Antialiasing::edgeSharpness
	float ___edgeSharpness_12;
	// System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::dlaaSharp
	bool ___dlaaSharp_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::ssaaShader
	Shader_t4151988712 * ___ssaaShader_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::ssaa
	Material_t340375123 * ___ssaa_15;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::dlaaShader
	Shader_t4151988712 * ___dlaaShader_16;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::dlaa
	Material_t340375123 * ___dlaa_17;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::nfaaShader
	Shader_t4151988712 * ___nfaaShader_18;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::nfaa
	Material_t340375123 * ___nfaa_19;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAPreset2
	Shader_t4151988712 * ___shaderFXAAPreset2_20;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAPreset2
	Material_t340375123 * ___materialFXAAPreset2_21;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAPreset3
	Shader_t4151988712 * ___shaderFXAAPreset3_22;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAPreset3
	Material_t340375123 * ___materialFXAAPreset3_23;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAII
	Shader_t4151988712 * ___shaderFXAAII_24;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAII
	Material_t340375123 * ___materialFXAAII_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Antialiasing::shaderFXAAIII
	Shader_t4151988712 * ___shaderFXAAIII_26;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::materialFXAAIII
	Material_t340375123 * ___materialFXAAIII_27;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_showGeneratedNormals_7() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___showGeneratedNormals_7)); }
	inline bool get_showGeneratedNormals_7() const { return ___showGeneratedNormals_7; }
	inline bool* get_address_of_showGeneratedNormals_7() { return &___showGeneratedNormals_7; }
	inline void set_showGeneratedNormals_7(bool value)
	{
		___showGeneratedNormals_7 = value;
	}

	inline static int32_t get_offset_of_offsetScale_8() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___offsetScale_8)); }
	inline float get_offsetScale_8() const { return ___offsetScale_8; }
	inline float* get_address_of_offsetScale_8() { return &___offsetScale_8; }
	inline void set_offsetScale_8(float value)
	{
		___offsetScale_8 = value;
	}

	inline static int32_t get_offset_of_blurRadius_9() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___blurRadius_9)); }
	inline float get_blurRadius_9() const { return ___blurRadius_9; }
	inline float* get_address_of_blurRadius_9() { return &___blurRadius_9; }
	inline void set_blurRadius_9(float value)
	{
		___blurRadius_9 = value;
	}

	inline static int32_t get_offset_of_edgeThresholdMin_10() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___edgeThresholdMin_10)); }
	inline float get_edgeThresholdMin_10() const { return ___edgeThresholdMin_10; }
	inline float* get_address_of_edgeThresholdMin_10() { return &___edgeThresholdMin_10; }
	inline void set_edgeThresholdMin_10(float value)
	{
		___edgeThresholdMin_10 = value;
	}

	inline static int32_t get_offset_of_edgeThreshold_11() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___edgeThreshold_11)); }
	inline float get_edgeThreshold_11() const { return ___edgeThreshold_11; }
	inline float* get_address_of_edgeThreshold_11() { return &___edgeThreshold_11; }
	inline void set_edgeThreshold_11(float value)
	{
		___edgeThreshold_11 = value;
	}

	inline static int32_t get_offset_of_edgeSharpness_12() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___edgeSharpness_12)); }
	inline float get_edgeSharpness_12() const { return ___edgeSharpness_12; }
	inline float* get_address_of_edgeSharpness_12() { return &___edgeSharpness_12; }
	inline void set_edgeSharpness_12(float value)
	{
		___edgeSharpness_12 = value;
	}

	inline static int32_t get_offset_of_dlaaSharp_13() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___dlaaSharp_13)); }
	inline bool get_dlaaSharp_13() const { return ___dlaaSharp_13; }
	inline bool* get_address_of_dlaaSharp_13() { return &___dlaaSharp_13; }
	inline void set_dlaaSharp_13(bool value)
	{
		___dlaaSharp_13 = value;
	}

	inline static int32_t get_offset_of_ssaaShader_14() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___ssaaShader_14)); }
	inline Shader_t4151988712 * get_ssaaShader_14() const { return ___ssaaShader_14; }
	inline Shader_t4151988712 ** get_address_of_ssaaShader_14() { return &___ssaaShader_14; }
	inline void set_ssaaShader_14(Shader_t4151988712 * value)
	{
		___ssaaShader_14 = value;
		Il2CppCodeGenWriteBarrier((&___ssaaShader_14), value);
	}

	inline static int32_t get_offset_of_ssaa_15() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___ssaa_15)); }
	inline Material_t340375123 * get_ssaa_15() const { return ___ssaa_15; }
	inline Material_t340375123 ** get_address_of_ssaa_15() { return &___ssaa_15; }
	inline void set_ssaa_15(Material_t340375123 * value)
	{
		___ssaa_15 = value;
		Il2CppCodeGenWriteBarrier((&___ssaa_15), value);
	}

	inline static int32_t get_offset_of_dlaaShader_16() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___dlaaShader_16)); }
	inline Shader_t4151988712 * get_dlaaShader_16() const { return ___dlaaShader_16; }
	inline Shader_t4151988712 ** get_address_of_dlaaShader_16() { return &___dlaaShader_16; }
	inline void set_dlaaShader_16(Shader_t4151988712 * value)
	{
		___dlaaShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___dlaaShader_16), value);
	}

	inline static int32_t get_offset_of_dlaa_17() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___dlaa_17)); }
	inline Material_t340375123 * get_dlaa_17() const { return ___dlaa_17; }
	inline Material_t340375123 ** get_address_of_dlaa_17() { return &___dlaa_17; }
	inline void set_dlaa_17(Material_t340375123 * value)
	{
		___dlaa_17 = value;
		Il2CppCodeGenWriteBarrier((&___dlaa_17), value);
	}

	inline static int32_t get_offset_of_nfaaShader_18() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___nfaaShader_18)); }
	inline Shader_t4151988712 * get_nfaaShader_18() const { return ___nfaaShader_18; }
	inline Shader_t4151988712 ** get_address_of_nfaaShader_18() { return &___nfaaShader_18; }
	inline void set_nfaaShader_18(Shader_t4151988712 * value)
	{
		___nfaaShader_18 = value;
		Il2CppCodeGenWriteBarrier((&___nfaaShader_18), value);
	}

	inline static int32_t get_offset_of_nfaa_19() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___nfaa_19)); }
	inline Material_t340375123 * get_nfaa_19() const { return ___nfaa_19; }
	inline Material_t340375123 ** get_address_of_nfaa_19() { return &___nfaa_19; }
	inline void set_nfaa_19(Material_t340375123 * value)
	{
		___nfaa_19 = value;
		Il2CppCodeGenWriteBarrier((&___nfaa_19), value);
	}

	inline static int32_t get_offset_of_shaderFXAAPreset2_20() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___shaderFXAAPreset2_20)); }
	inline Shader_t4151988712 * get_shaderFXAAPreset2_20() const { return ___shaderFXAAPreset2_20; }
	inline Shader_t4151988712 ** get_address_of_shaderFXAAPreset2_20() { return &___shaderFXAAPreset2_20; }
	inline void set_shaderFXAAPreset2_20(Shader_t4151988712 * value)
	{
		___shaderFXAAPreset2_20 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAPreset2_20), value);
	}

	inline static int32_t get_offset_of_materialFXAAPreset2_21() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___materialFXAAPreset2_21)); }
	inline Material_t340375123 * get_materialFXAAPreset2_21() const { return ___materialFXAAPreset2_21; }
	inline Material_t340375123 ** get_address_of_materialFXAAPreset2_21() { return &___materialFXAAPreset2_21; }
	inline void set_materialFXAAPreset2_21(Material_t340375123 * value)
	{
		___materialFXAAPreset2_21 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAPreset2_21), value);
	}

	inline static int32_t get_offset_of_shaderFXAAPreset3_22() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___shaderFXAAPreset3_22)); }
	inline Shader_t4151988712 * get_shaderFXAAPreset3_22() const { return ___shaderFXAAPreset3_22; }
	inline Shader_t4151988712 ** get_address_of_shaderFXAAPreset3_22() { return &___shaderFXAAPreset3_22; }
	inline void set_shaderFXAAPreset3_22(Shader_t4151988712 * value)
	{
		___shaderFXAAPreset3_22 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAPreset3_22), value);
	}

	inline static int32_t get_offset_of_materialFXAAPreset3_23() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___materialFXAAPreset3_23)); }
	inline Material_t340375123 * get_materialFXAAPreset3_23() const { return ___materialFXAAPreset3_23; }
	inline Material_t340375123 ** get_address_of_materialFXAAPreset3_23() { return &___materialFXAAPreset3_23; }
	inline void set_materialFXAAPreset3_23(Material_t340375123 * value)
	{
		___materialFXAAPreset3_23 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAPreset3_23), value);
	}

	inline static int32_t get_offset_of_shaderFXAAII_24() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___shaderFXAAII_24)); }
	inline Shader_t4151988712 * get_shaderFXAAII_24() const { return ___shaderFXAAII_24; }
	inline Shader_t4151988712 ** get_address_of_shaderFXAAII_24() { return &___shaderFXAAII_24; }
	inline void set_shaderFXAAII_24(Shader_t4151988712 * value)
	{
		___shaderFXAAII_24 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAII_24), value);
	}

	inline static int32_t get_offset_of_materialFXAAII_25() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___materialFXAAII_25)); }
	inline Material_t340375123 * get_materialFXAAII_25() const { return ___materialFXAAII_25; }
	inline Material_t340375123 ** get_address_of_materialFXAAII_25() { return &___materialFXAAII_25; }
	inline void set_materialFXAAII_25(Material_t340375123 * value)
	{
		___materialFXAAII_25 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAII_25), value);
	}

	inline static int32_t get_offset_of_shaderFXAAIII_26() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___shaderFXAAIII_26)); }
	inline Shader_t4151988712 * get_shaderFXAAIII_26() const { return ___shaderFXAAIII_26; }
	inline Shader_t4151988712 ** get_address_of_shaderFXAAIII_26() { return &___shaderFXAAIII_26; }
	inline void set_shaderFXAAIII_26(Shader_t4151988712 * value)
	{
		___shaderFXAAIII_26 = value;
		Il2CppCodeGenWriteBarrier((&___shaderFXAAIII_26), value);
	}

	inline static int32_t get_offset_of_materialFXAAIII_27() { return static_cast<int32_t>(offsetof(Antialiasing_t1691315015, ___materialFXAAIII_27)); }
	inline Material_t340375123 * get_materialFXAAIII_27() const { return ___materialFXAAIII_27; }
	inline Material_t340375123 ** get_address_of_materialFXAAIII_27() { return &___materialFXAAIII_27; }
	inline void set_materialFXAAIII_27(Material_t340375123 * value)
	{
		___materialFXAAIII_27 = value;
		Il2CppCodeGenWriteBarrier((&___materialFXAAIII_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASING_T1691315015_H
#ifndef BLUROPTIMIZED_T3334654964_H
#define BLUROPTIMIZED_T3334654964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.BlurOptimized
struct  BlurOptimized_t3334654964  : public PostEffectsBase_t2404315739
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.BlurOptimized::downsample
	int32_t ___downsample_6;
	// System.Single UnityStandardAssets.ImageEffects.BlurOptimized::blurSize
	float ___blurSize_7;
	// System.Int32 UnityStandardAssets.ImageEffects.BlurOptimized::blurIterations
	int32_t ___blurIterations_8;
	// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType UnityStandardAssets.ImageEffects.BlurOptimized::blurType
	int32_t ___blurType_9;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.BlurOptimized::blurShader
	Shader_t4151988712 * ___blurShader_10;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.BlurOptimized::blurMaterial
	Material_t340375123 * ___blurMaterial_11;

public:
	inline static int32_t get_offset_of_downsample_6() { return static_cast<int32_t>(offsetof(BlurOptimized_t3334654964, ___downsample_6)); }
	inline int32_t get_downsample_6() const { return ___downsample_6; }
	inline int32_t* get_address_of_downsample_6() { return &___downsample_6; }
	inline void set_downsample_6(int32_t value)
	{
		___downsample_6 = value;
	}

	inline static int32_t get_offset_of_blurSize_7() { return static_cast<int32_t>(offsetof(BlurOptimized_t3334654964, ___blurSize_7)); }
	inline float get_blurSize_7() const { return ___blurSize_7; }
	inline float* get_address_of_blurSize_7() { return &___blurSize_7; }
	inline void set_blurSize_7(float value)
	{
		___blurSize_7 = value;
	}

	inline static int32_t get_offset_of_blurIterations_8() { return static_cast<int32_t>(offsetof(BlurOptimized_t3334654964, ___blurIterations_8)); }
	inline int32_t get_blurIterations_8() const { return ___blurIterations_8; }
	inline int32_t* get_address_of_blurIterations_8() { return &___blurIterations_8; }
	inline void set_blurIterations_8(int32_t value)
	{
		___blurIterations_8 = value;
	}

	inline static int32_t get_offset_of_blurType_9() { return static_cast<int32_t>(offsetof(BlurOptimized_t3334654964, ___blurType_9)); }
	inline int32_t get_blurType_9() const { return ___blurType_9; }
	inline int32_t* get_address_of_blurType_9() { return &___blurType_9; }
	inline void set_blurType_9(int32_t value)
	{
		___blurType_9 = value;
	}

	inline static int32_t get_offset_of_blurShader_10() { return static_cast<int32_t>(offsetof(BlurOptimized_t3334654964, ___blurShader_10)); }
	inline Shader_t4151988712 * get_blurShader_10() const { return ___blurShader_10; }
	inline Shader_t4151988712 ** get_address_of_blurShader_10() { return &___blurShader_10; }
	inline void set_blurShader_10(Shader_t4151988712 * value)
	{
		___blurShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___blurShader_10), value);
	}

	inline static int32_t get_offset_of_blurMaterial_11() { return static_cast<int32_t>(offsetof(BlurOptimized_t3334654964, ___blurMaterial_11)); }
	inline Material_t340375123 * get_blurMaterial_11() const { return ___blurMaterial_11; }
	inline Material_t340375123 ** get_address_of_blurMaterial_11() { return &___blurMaterial_11; }
	inline void set_blurMaterial_11(Material_t340375123 * value)
	{
		___blurMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___blurMaterial_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUROPTIMIZED_T3334654964_H
#ifndef CAMERAMOTIONBLUR_T2812046500_H
#define CAMERAMOTIONBLUR_T2812046500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.CameraMotionBlur
struct  CameraMotionBlur_t2812046500  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter UnityStandardAssets.ImageEffects.CameraMotionBlur::filterType
	int32_t ___filterType_7;
	// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::preview
	bool ___preview_8;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::previewScale
	Vector3_t3722313464  ___previewScale_9;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::movementScale
	float ___movementScale_10;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::rotationScale
	float ___rotationScale_11;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::maxVelocity
	float ___maxVelocity_12;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::minVelocity
	float ___minVelocity_13;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::velocityScale
	float ___velocityScale_14;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::softZDistance
	float ___softZDistance_15;
	// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::velocityDownsample
	int32_t ___velocityDownsample_16;
	// UnityEngine.LayerMask UnityStandardAssets.ImageEffects.CameraMotionBlur::excludeLayers
	LayerMask_t3493934918  ___excludeLayers_17;
	// UnityEngine.GameObject UnityStandardAssets.ImageEffects.CameraMotionBlur::tmpCam
	GameObject_t1113636619 * ___tmpCam_18;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CameraMotionBlur::shader
	Shader_t4151988712 * ___shader_19;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CameraMotionBlur::dx11MotionBlurShader
	Shader_t4151988712 * ___dx11MotionBlurShader_20;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CameraMotionBlur::replacementClear
	Shader_t4151988712 * ___replacementClear_21;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CameraMotionBlur::motionBlurMaterial
	Material_t340375123 * ___motionBlurMaterial_22;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CameraMotionBlur::dx11MotionBlurMaterial
	Material_t340375123 * ___dx11MotionBlurMaterial_23;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.CameraMotionBlur::noiseTexture
	Texture2D_t3840446185 * ___noiseTexture_24;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::jitter
	float ___jitter_25;
	// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::showVelocity
	bool ___showVelocity_26;
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::showVelocityScale
	float ___showVelocityScale_27;
	// UnityEngine.Matrix4x4 UnityStandardAssets.ImageEffects.CameraMotionBlur::currentViewProjMat
	Matrix4x4_t1817901843  ___currentViewProjMat_28;
	// UnityEngine.Matrix4x4[] UnityStandardAssets.ImageEffects.CameraMotionBlur::currentStereoViewProjMat
	Matrix4x4U5BU5D_t2302988098* ___currentStereoViewProjMat_29;
	// UnityEngine.Matrix4x4 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevViewProjMat
	Matrix4x4_t1817901843  ___prevViewProjMat_30;
	// UnityEngine.Matrix4x4[] UnityStandardAssets.ImageEffects.CameraMotionBlur::prevStereoViewProjMat
	Matrix4x4U5BU5D_t2302988098* ___prevStereoViewProjMat_31;
	// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFrameCount
	int32_t ___prevFrameCount_32;
	// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::wasActive
	bool ___wasActive_33;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFrameForward
	Vector3_t3722313464  ___prevFrameForward_34;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFrameUp
	Vector3_t3722313464  ___prevFrameUp_35;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.CameraMotionBlur::prevFramePos
	Vector3_t3722313464  ___prevFramePos_36;
	// UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::_camera
	Camera_t4157153871 * ____camera_37;

public:
	inline static int32_t get_offset_of_filterType_7() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___filterType_7)); }
	inline int32_t get_filterType_7() const { return ___filterType_7; }
	inline int32_t* get_address_of_filterType_7() { return &___filterType_7; }
	inline void set_filterType_7(int32_t value)
	{
		___filterType_7 = value;
	}

	inline static int32_t get_offset_of_preview_8() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___preview_8)); }
	inline bool get_preview_8() const { return ___preview_8; }
	inline bool* get_address_of_preview_8() { return &___preview_8; }
	inline void set_preview_8(bool value)
	{
		___preview_8 = value;
	}

	inline static int32_t get_offset_of_previewScale_9() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___previewScale_9)); }
	inline Vector3_t3722313464  get_previewScale_9() const { return ___previewScale_9; }
	inline Vector3_t3722313464 * get_address_of_previewScale_9() { return &___previewScale_9; }
	inline void set_previewScale_9(Vector3_t3722313464  value)
	{
		___previewScale_9 = value;
	}

	inline static int32_t get_offset_of_movementScale_10() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___movementScale_10)); }
	inline float get_movementScale_10() const { return ___movementScale_10; }
	inline float* get_address_of_movementScale_10() { return &___movementScale_10; }
	inline void set_movementScale_10(float value)
	{
		___movementScale_10 = value;
	}

	inline static int32_t get_offset_of_rotationScale_11() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___rotationScale_11)); }
	inline float get_rotationScale_11() const { return ___rotationScale_11; }
	inline float* get_address_of_rotationScale_11() { return &___rotationScale_11; }
	inline void set_rotationScale_11(float value)
	{
		___rotationScale_11 = value;
	}

	inline static int32_t get_offset_of_maxVelocity_12() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___maxVelocity_12)); }
	inline float get_maxVelocity_12() const { return ___maxVelocity_12; }
	inline float* get_address_of_maxVelocity_12() { return &___maxVelocity_12; }
	inline void set_maxVelocity_12(float value)
	{
		___maxVelocity_12 = value;
	}

	inline static int32_t get_offset_of_minVelocity_13() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___minVelocity_13)); }
	inline float get_minVelocity_13() const { return ___minVelocity_13; }
	inline float* get_address_of_minVelocity_13() { return &___minVelocity_13; }
	inline void set_minVelocity_13(float value)
	{
		___minVelocity_13 = value;
	}

	inline static int32_t get_offset_of_velocityScale_14() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___velocityScale_14)); }
	inline float get_velocityScale_14() const { return ___velocityScale_14; }
	inline float* get_address_of_velocityScale_14() { return &___velocityScale_14; }
	inline void set_velocityScale_14(float value)
	{
		___velocityScale_14 = value;
	}

	inline static int32_t get_offset_of_softZDistance_15() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___softZDistance_15)); }
	inline float get_softZDistance_15() const { return ___softZDistance_15; }
	inline float* get_address_of_softZDistance_15() { return &___softZDistance_15; }
	inline void set_softZDistance_15(float value)
	{
		___softZDistance_15 = value;
	}

	inline static int32_t get_offset_of_velocityDownsample_16() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___velocityDownsample_16)); }
	inline int32_t get_velocityDownsample_16() const { return ___velocityDownsample_16; }
	inline int32_t* get_address_of_velocityDownsample_16() { return &___velocityDownsample_16; }
	inline void set_velocityDownsample_16(int32_t value)
	{
		___velocityDownsample_16 = value;
	}

	inline static int32_t get_offset_of_excludeLayers_17() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___excludeLayers_17)); }
	inline LayerMask_t3493934918  get_excludeLayers_17() const { return ___excludeLayers_17; }
	inline LayerMask_t3493934918 * get_address_of_excludeLayers_17() { return &___excludeLayers_17; }
	inline void set_excludeLayers_17(LayerMask_t3493934918  value)
	{
		___excludeLayers_17 = value;
	}

	inline static int32_t get_offset_of_tmpCam_18() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___tmpCam_18)); }
	inline GameObject_t1113636619 * get_tmpCam_18() const { return ___tmpCam_18; }
	inline GameObject_t1113636619 ** get_address_of_tmpCam_18() { return &___tmpCam_18; }
	inline void set_tmpCam_18(GameObject_t1113636619 * value)
	{
		___tmpCam_18 = value;
		Il2CppCodeGenWriteBarrier((&___tmpCam_18), value);
	}

	inline static int32_t get_offset_of_shader_19() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___shader_19)); }
	inline Shader_t4151988712 * get_shader_19() const { return ___shader_19; }
	inline Shader_t4151988712 ** get_address_of_shader_19() { return &___shader_19; }
	inline void set_shader_19(Shader_t4151988712 * value)
	{
		___shader_19 = value;
		Il2CppCodeGenWriteBarrier((&___shader_19), value);
	}

	inline static int32_t get_offset_of_dx11MotionBlurShader_20() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___dx11MotionBlurShader_20)); }
	inline Shader_t4151988712 * get_dx11MotionBlurShader_20() const { return ___dx11MotionBlurShader_20; }
	inline Shader_t4151988712 ** get_address_of_dx11MotionBlurShader_20() { return &___dx11MotionBlurShader_20; }
	inline void set_dx11MotionBlurShader_20(Shader_t4151988712 * value)
	{
		___dx11MotionBlurShader_20 = value;
		Il2CppCodeGenWriteBarrier((&___dx11MotionBlurShader_20), value);
	}

	inline static int32_t get_offset_of_replacementClear_21() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___replacementClear_21)); }
	inline Shader_t4151988712 * get_replacementClear_21() const { return ___replacementClear_21; }
	inline Shader_t4151988712 ** get_address_of_replacementClear_21() { return &___replacementClear_21; }
	inline void set_replacementClear_21(Shader_t4151988712 * value)
	{
		___replacementClear_21 = value;
		Il2CppCodeGenWriteBarrier((&___replacementClear_21), value);
	}

	inline static int32_t get_offset_of_motionBlurMaterial_22() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___motionBlurMaterial_22)); }
	inline Material_t340375123 * get_motionBlurMaterial_22() const { return ___motionBlurMaterial_22; }
	inline Material_t340375123 ** get_address_of_motionBlurMaterial_22() { return &___motionBlurMaterial_22; }
	inline void set_motionBlurMaterial_22(Material_t340375123 * value)
	{
		___motionBlurMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlurMaterial_22), value);
	}

	inline static int32_t get_offset_of_dx11MotionBlurMaterial_23() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___dx11MotionBlurMaterial_23)); }
	inline Material_t340375123 * get_dx11MotionBlurMaterial_23() const { return ___dx11MotionBlurMaterial_23; }
	inline Material_t340375123 ** get_address_of_dx11MotionBlurMaterial_23() { return &___dx11MotionBlurMaterial_23; }
	inline void set_dx11MotionBlurMaterial_23(Material_t340375123 * value)
	{
		___dx11MotionBlurMaterial_23 = value;
		Il2CppCodeGenWriteBarrier((&___dx11MotionBlurMaterial_23), value);
	}

	inline static int32_t get_offset_of_noiseTexture_24() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___noiseTexture_24)); }
	inline Texture2D_t3840446185 * get_noiseTexture_24() const { return ___noiseTexture_24; }
	inline Texture2D_t3840446185 ** get_address_of_noiseTexture_24() { return &___noiseTexture_24; }
	inline void set_noiseTexture_24(Texture2D_t3840446185 * value)
	{
		___noiseTexture_24 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTexture_24), value);
	}

	inline static int32_t get_offset_of_jitter_25() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___jitter_25)); }
	inline float get_jitter_25() const { return ___jitter_25; }
	inline float* get_address_of_jitter_25() { return &___jitter_25; }
	inline void set_jitter_25(float value)
	{
		___jitter_25 = value;
	}

	inline static int32_t get_offset_of_showVelocity_26() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___showVelocity_26)); }
	inline bool get_showVelocity_26() const { return ___showVelocity_26; }
	inline bool* get_address_of_showVelocity_26() { return &___showVelocity_26; }
	inline void set_showVelocity_26(bool value)
	{
		___showVelocity_26 = value;
	}

	inline static int32_t get_offset_of_showVelocityScale_27() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___showVelocityScale_27)); }
	inline float get_showVelocityScale_27() const { return ___showVelocityScale_27; }
	inline float* get_address_of_showVelocityScale_27() { return &___showVelocityScale_27; }
	inline void set_showVelocityScale_27(float value)
	{
		___showVelocityScale_27 = value;
	}

	inline static int32_t get_offset_of_currentViewProjMat_28() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___currentViewProjMat_28)); }
	inline Matrix4x4_t1817901843  get_currentViewProjMat_28() const { return ___currentViewProjMat_28; }
	inline Matrix4x4_t1817901843 * get_address_of_currentViewProjMat_28() { return &___currentViewProjMat_28; }
	inline void set_currentViewProjMat_28(Matrix4x4_t1817901843  value)
	{
		___currentViewProjMat_28 = value;
	}

	inline static int32_t get_offset_of_currentStereoViewProjMat_29() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___currentStereoViewProjMat_29)); }
	inline Matrix4x4U5BU5D_t2302988098* get_currentStereoViewProjMat_29() const { return ___currentStereoViewProjMat_29; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_currentStereoViewProjMat_29() { return &___currentStereoViewProjMat_29; }
	inline void set_currentStereoViewProjMat_29(Matrix4x4U5BU5D_t2302988098* value)
	{
		___currentStereoViewProjMat_29 = value;
		Il2CppCodeGenWriteBarrier((&___currentStereoViewProjMat_29), value);
	}

	inline static int32_t get_offset_of_prevViewProjMat_30() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___prevViewProjMat_30)); }
	inline Matrix4x4_t1817901843  get_prevViewProjMat_30() const { return ___prevViewProjMat_30; }
	inline Matrix4x4_t1817901843 * get_address_of_prevViewProjMat_30() { return &___prevViewProjMat_30; }
	inline void set_prevViewProjMat_30(Matrix4x4_t1817901843  value)
	{
		___prevViewProjMat_30 = value;
	}

	inline static int32_t get_offset_of_prevStereoViewProjMat_31() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___prevStereoViewProjMat_31)); }
	inline Matrix4x4U5BU5D_t2302988098* get_prevStereoViewProjMat_31() const { return ___prevStereoViewProjMat_31; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_prevStereoViewProjMat_31() { return &___prevStereoViewProjMat_31; }
	inline void set_prevStereoViewProjMat_31(Matrix4x4U5BU5D_t2302988098* value)
	{
		___prevStereoViewProjMat_31 = value;
		Il2CppCodeGenWriteBarrier((&___prevStereoViewProjMat_31), value);
	}

	inline static int32_t get_offset_of_prevFrameCount_32() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___prevFrameCount_32)); }
	inline int32_t get_prevFrameCount_32() const { return ___prevFrameCount_32; }
	inline int32_t* get_address_of_prevFrameCount_32() { return &___prevFrameCount_32; }
	inline void set_prevFrameCount_32(int32_t value)
	{
		___prevFrameCount_32 = value;
	}

	inline static int32_t get_offset_of_wasActive_33() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___wasActive_33)); }
	inline bool get_wasActive_33() const { return ___wasActive_33; }
	inline bool* get_address_of_wasActive_33() { return &___wasActive_33; }
	inline void set_wasActive_33(bool value)
	{
		___wasActive_33 = value;
	}

	inline static int32_t get_offset_of_prevFrameForward_34() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___prevFrameForward_34)); }
	inline Vector3_t3722313464  get_prevFrameForward_34() const { return ___prevFrameForward_34; }
	inline Vector3_t3722313464 * get_address_of_prevFrameForward_34() { return &___prevFrameForward_34; }
	inline void set_prevFrameForward_34(Vector3_t3722313464  value)
	{
		___prevFrameForward_34 = value;
	}

	inline static int32_t get_offset_of_prevFrameUp_35() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___prevFrameUp_35)); }
	inline Vector3_t3722313464  get_prevFrameUp_35() const { return ___prevFrameUp_35; }
	inline Vector3_t3722313464 * get_address_of_prevFrameUp_35() { return &___prevFrameUp_35; }
	inline void set_prevFrameUp_35(Vector3_t3722313464  value)
	{
		___prevFrameUp_35 = value;
	}

	inline static int32_t get_offset_of_prevFramePos_36() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ___prevFramePos_36)); }
	inline Vector3_t3722313464  get_prevFramePos_36() const { return ___prevFramePos_36; }
	inline Vector3_t3722313464 * get_address_of_prevFramePos_36() { return &___prevFramePos_36; }
	inline void set_prevFramePos_36(Vector3_t3722313464  value)
	{
		___prevFramePos_36 = value;
	}

	inline static int32_t get_offset_of__camera_37() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500, ____camera_37)); }
	inline Camera_t4157153871 * get__camera_37() const { return ____camera_37; }
	inline Camera_t4157153871 ** get_address_of__camera_37() { return &____camera_37; }
	inline void set__camera_37(Camera_t4157153871 * value)
	{
		____camera_37 = value;
		Il2CppCodeGenWriteBarrier((&____camera_37), value);
	}
};

struct CameraMotionBlur_t2812046500_StaticFields
{
public:
	// System.Single UnityStandardAssets.ImageEffects.CameraMotionBlur::MAX_RADIUS
	float ___MAX_RADIUS_6;

public:
	inline static int32_t get_offset_of_MAX_RADIUS_6() { return static_cast<int32_t>(offsetof(CameraMotionBlur_t2812046500_StaticFields, ___MAX_RADIUS_6)); }
	inline float get_MAX_RADIUS_6() const { return ___MAX_RADIUS_6; }
	inline float* get_address_of_MAX_RADIUS_6() { return &___MAX_RADIUS_6; }
	inline void set_MAX_RADIUS_6(float value)
	{
		___MAX_RADIUS_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMOTIONBLUR_T2812046500_H
#ifndef CREASESHADING_T1200394124_H
#define CREASESHADING_T1200394124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.CreaseShading
struct  CreaseShading_t1200394124  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.CreaseShading::intensity
	float ___intensity_6;
	// System.Int32 UnityStandardAssets.ImageEffects.CreaseShading::softness
	int32_t ___softness_7;
	// System.Single UnityStandardAssets.ImageEffects.CreaseShading::spread
	float ___spread_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CreaseShading::blurShader
	Shader_t4151988712 * ___blurShader_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CreaseShading::blurMaterial
	Material_t340375123 * ___blurMaterial_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CreaseShading::depthFetchShader
	Shader_t4151988712 * ___depthFetchShader_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CreaseShading::depthFetchMaterial
	Material_t340375123 * ___depthFetchMaterial_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.CreaseShading::creaseApplyShader
	Shader_t4151988712 * ___creaseApplyShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.CreaseShading::creaseApplyMaterial
	Material_t340375123 * ___creaseApplyMaterial_14;

public:
	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_softness_7() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___softness_7)); }
	inline int32_t get_softness_7() const { return ___softness_7; }
	inline int32_t* get_address_of_softness_7() { return &___softness_7; }
	inline void set_softness_7(int32_t value)
	{
		___softness_7 = value;
	}

	inline static int32_t get_offset_of_spread_8() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___spread_8)); }
	inline float get_spread_8() const { return ___spread_8; }
	inline float* get_address_of_spread_8() { return &___spread_8; }
	inline void set_spread_8(float value)
	{
		___spread_8 = value;
	}

	inline static int32_t get_offset_of_blurShader_9() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___blurShader_9)); }
	inline Shader_t4151988712 * get_blurShader_9() const { return ___blurShader_9; }
	inline Shader_t4151988712 ** get_address_of_blurShader_9() { return &___blurShader_9; }
	inline void set_blurShader_9(Shader_t4151988712 * value)
	{
		___blurShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___blurShader_9), value);
	}

	inline static int32_t get_offset_of_blurMaterial_10() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___blurMaterial_10)); }
	inline Material_t340375123 * get_blurMaterial_10() const { return ___blurMaterial_10; }
	inline Material_t340375123 ** get_address_of_blurMaterial_10() { return &___blurMaterial_10; }
	inline void set_blurMaterial_10(Material_t340375123 * value)
	{
		___blurMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___blurMaterial_10), value);
	}

	inline static int32_t get_offset_of_depthFetchShader_11() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___depthFetchShader_11)); }
	inline Shader_t4151988712 * get_depthFetchShader_11() const { return ___depthFetchShader_11; }
	inline Shader_t4151988712 ** get_address_of_depthFetchShader_11() { return &___depthFetchShader_11; }
	inline void set_depthFetchShader_11(Shader_t4151988712 * value)
	{
		___depthFetchShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___depthFetchShader_11), value);
	}

	inline static int32_t get_offset_of_depthFetchMaterial_12() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___depthFetchMaterial_12)); }
	inline Material_t340375123 * get_depthFetchMaterial_12() const { return ___depthFetchMaterial_12; }
	inline Material_t340375123 ** get_address_of_depthFetchMaterial_12() { return &___depthFetchMaterial_12; }
	inline void set_depthFetchMaterial_12(Material_t340375123 * value)
	{
		___depthFetchMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___depthFetchMaterial_12), value);
	}

	inline static int32_t get_offset_of_creaseApplyShader_13() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___creaseApplyShader_13)); }
	inline Shader_t4151988712 * get_creaseApplyShader_13() const { return ___creaseApplyShader_13; }
	inline Shader_t4151988712 ** get_address_of_creaseApplyShader_13() { return &___creaseApplyShader_13; }
	inline void set_creaseApplyShader_13(Shader_t4151988712 * value)
	{
		___creaseApplyShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___creaseApplyShader_13), value);
	}

	inline static int32_t get_offset_of_creaseApplyMaterial_14() { return static_cast<int32_t>(offsetof(CreaseShading_t1200394124, ___creaseApplyMaterial_14)); }
	inline Material_t340375123 * get_creaseApplyMaterial_14() const { return ___creaseApplyMaterial_14; }
	inline Material_t340375123 ** get_address_of_creaseApplyMaterial_14() { return &___creaseApplyMaterial_14; }
	inline void set_creaseApplyMaterial_14(Material_t340375123 * value)
	{
		___creaseApplyMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___creaseApplyMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREASESHADING_T1200394124_H
#ifndef SEPIATONE_T4259761740_H
#define SEPIATONE_T4259761740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SepiaTone
struct  SepiaTone_t4259761740  : public ImageEffectBase_t2026006575
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEPIATONE_T4259761740_H
#ifndef SUNSHAFTS_T2328921421_H
#define SUNSHAFTS_T2328921421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.SunShafts
struct  SunShafts_t2328921421  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution UnityStandardAssets.ImageEffects.SunShafts::resolution
	int32_t ___resolution_6;
	// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode UnityStandardAssets.ImageEffects.SunShafts::screenBlendMode
	int32_t ___screenBlendMode_7;
	// UnityEngine.Transform UnityStandardAssets.ImageEffects.SunShafts::sunTransform
	Transform_t3600365921 * ___sunTransform_8;
	// System.Int32 UnityStandardAssets.ImageEffects.SunShafts::radialBlurIterations
	int32_t ___radialBlurIterations_9;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.SunShafts::sunColor
	Color_t2555686324  ___sunColor_10;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.SunShafts::sunThreshold
	Color_t2555686324  ___sunThreshold_11;
	// System.Single UnityStandardAssets.ImageEffects.SunShafts::sunShaftBlurRadius
	float ___sunShaftBlurRadius_12;
	// System.Single UnityStandardAssets.ImageEffects.SunShafts::sunShaftIntensity
	float ___sunShaftIntensity_13;
	// System.Single UnityStandardAssets.ImageEffects.SunShafts::maxRadius
	float ___maxRadius_14;
	// System.Boolean UnityStandardAssets.ImageEffects.SunShafts::useDepthTexture
	bool ___useDepthTexture_15;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.SunShafts::sunShaftsShader
	Shader_t4151988712 * ___sunShaftsShader_16;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.SunShafts::sunShaftsMaterial
	Material_t340375123 * ___sunShaftsMaterial_17;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.SunShafts::simpleClearShader
	Shader_t4151988712 * ___simpleClearShader_18;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.SunShafts::simpleClearMaterial
	Material_t340375123 * ___simpleClearMaterial_19;

public:
	inline static int32_t get_offset_of_resolution_6() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___resolution_6)); }
	inline int32_t get_resolution_6() const { return ___resolution_6; }
	inline int32_t* get_address_of_resolution_6() { return &___resolution_6; }
	inline void set_resolution_6(int32_t value)
	{
		___resolution_6 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_7() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___screenBlendMode_7)); }
	inline int32_t get_screenBlendMode_7() const { return ___screenBlendMode_7; }
	inline int32_t* get_address_of_screenBlendMode_7() { return &___screenBlendMode_7; }
	inline void set_screenBlendMode_7(int32_t value)
	{
		___screenBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_sunTransform_8() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunTransform_8)); }
	inline Transform_t3600365921 * get_sunTransform_8() const { return ___sunTransform_8; }
	inline Transform_t3600365921 ** get_address_of_sunTransform_8() { return &___sunTransform_8; }
	inline void set_sunTransform_8(Transform_t3600365921 * value)
	{
		___sunTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___sunTransform_8), value);
	}

	inline static int32_t get_offset_of_radialBlurIterations_9() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___radialBlurIterations_9)); }
	inline int32_t get_radialBlurIterations_9() const { return ___radialBlurIterations_9; }
	inline int32_t* get_address_of_radialBlurIterations_9() { return &___radialBlurIterations_9; }
	inline void set_radialBlurIterations_9(int32_t value)
	{
		___radialBlurIterations_9 = value;
	}

	inline static int32_t get_offset_of_sunColor_10() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunColor_10)); }
	inline Color_t2555686324  get_sunColor_10() const { return ___sunColor_10; }
	inline Color_t2555686324 * get_address_of_sunColor_10() { return &___sunColor_10; }
	inline void set_sunColor_10(Color_t2555686324  value)
	{
		___sunColor_10 = value;
	}

	inline static int32_t get_offset_of_sunThreshold_11() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunThreshold_11)); }
	inline Color_t2555686324  get_sunThreshold_11() const { return ___sunThreshold_11; }
	inline Color_t2555686324 * get_address_of_sunThreshold_11() { return &___sunThreshold_11; }
	inline void set_sunThreshold_11(Color_t2555686324  value)
	{
		___sunThreshold_11 = value;
	}

	inline static int32_t get_offset_of_sunShaftBlurRadius_12() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunShaftBlurRadius_12)); }
	inline float get_sunShaftBlurRadius_12() const { return ___sunShaftBlurRadius_12; }
	inline float* get_address_of_sunShaftBlurRadius_12() { return &___sunShaftBlurRadius_12; }
	inline void set_sunShaftBlurRadius_12(float value)
	{
		___sunShaftBlurRadius_12 = value;
	}

	inline static int32_t get_offset_of_sunShaftIntensity_13() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunShaftIntensity_13)); }
	inline float get_sunShaftIntensity_13() const { return ___sunShaftIntensity_13; }
	inline float* get_address_of_sunShaftIntensity_13() { return &___sunShaftIntensity_13; }
	inline void set_sunShaftIntensity_13(float value)
	{
		___sunShaftIntensity_13 = value;
	}

	inline static int32_t get_offset_of_maxRadius_14() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___maxRadius_14)); }
	inline float get_maxRadius_14() const { return ___maxRadius_14; }
	inline float* get_address_of_maxRadius_14() { return &___maxRadius_14; }
	inline void set_maxRadius_14(float value)
	{
		___maxRadius_14 = value;
	}

	inline static int32_t get_offset_of_useDepthTexture_15() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___useDepthTexture_15)); }
	inline bool get_useDepthTexture_15() const { return ___useDepthTexture_15; }
	inline bool* get_address_of_useDepthTexture_15() { return &___useDepthTexture_15; }
	inline void set_useDepthTexture_15(bool value)
	{
		___useDepthTexture_15 = value;
	}

	inline static int32_t get_offset_of_sunShaftsShader_16() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunShaftsShader_16)); }
	inline Shader_t4151988712 * get_sunShaftsShader_16() const { return ___sunShaftsShader_16; }
	inline Shader_t4151988712 ** get_address_of_sunShaftsShader_16() { return &___sunShaftsShader_16; }
	inline void set_sunShaftsShader_16(Shader_t4151988712 * value)
	{
		___sunShaftsShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___sunShaftsShader_16), value);
	}

	inline static int32_t get_offset_of_sunShaftsMaterial_17() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___sunShaftsMaterial_17)); }
	inline Material_t340375123 * get_sunShaftsMaterial_17() const { return ___sunShaftsMaterial_17; }
	inline Material_t340375123 ** get_address_of_sunShaftsMaterial_17() { return &___sunShaftsMaterial_17; }
	inline void set_sunShaftsMaterial_17(Material_t340375123 * value)
	{
		___sunShaftsMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___sunShaftsMaterial_17), value);
	}

	inline static int32_t get_offset_of_simpleClearShader_18() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___simpleClearShader_18)); }
	inline Shader_t4151988712 * get_simpleClearShader_18() const { return ___simpleClearShader_18; }
	inline Shader_t4151988712 ** get_address_of_simpleClearShader_18() { return &___simpleClearShader_18; }
	inline void set_simpleClearShader_18(Shader_t4151988712 * value)
	{
		___simpleClearShader_18 = value;
		Il2CppCodeGenWriteBarrier((&___simpleClearShader_18), value);
	}

	inline static int32_t get_offset_of_simpleClearMaterial_19() { return static_cast<int32_t>(offsetof(SunShafts_t2328921421, ___simpleClearMaterial_19)); }
	inline Material_t340375123 * get_simpleClearMaterial_19() const { return ___simpleClearMaterial_19; }
	inline Material_t340375123 ** get_address_of_simpleClearMaterial_19() { return &___simpleClearMaterial_19; }
	inline void set_simpleClearMaterial_19(Material_t340375123 * value)
	{
		___simpleClearMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___simpleClearMaterial_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUNSHAFTS_T2328921421_H
#ifndef TILTSHIFT_T2891301701_H
#define TILTSHIFT_T2891301701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.TiltShift
struct  TiltShift_t2891301701  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode UnityStandardAssets.ImageEffects.TiltShift::mode
	int32_t ___mode_6;
	// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality UnityStandardAssets.ImageEffects.TiltShift::quality
	int32_t ___quality_7;
	// System.Single UnityStandardAssets.ImageEffects.TiltShift::blurArea
	float ___blurArea_8;
	// System.Single UnityStandardAssets.ImageEffects.TiltShift::maxBlurSize
	float ___maxBlurSize_9;
	// System.Int32 UnityStandardAssets.ImageEffects.TiltShift::downsample
	int32_t ___downsample_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.TiltShift::tiltShiftShader
	Shader_t4151988712 * ___tiltShiftShader_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.TiltShift::tiltShiftMaterial
	Material_t340375123 * ___tiltShiftMaterial_12;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_quality_7() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___quality_7)); }
	inline int32_t get_quality_7() const { return ___quality_7; }
	inline int32_t* get_address_of_quality_7() { return &___quality_7; }
	inline void set_quality_7(int32_t value)
	{
		___quality_7 = value;
	}

	inline static int32_t get_offset_of_blurArea_8() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___blurArea_8)); }
	inline float get_blurArea_8() const { return ___blurArea_8; }
	inline float* get_address_of_blurArea_8() { return &___blurArea_8; }
	inline void set_blurArea_8(float value)
	{
		___blurArea_8 = value;
	}

	inline static int32_t get_offset_of_maxBlurSize_9() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___maxBlurSize_9)); }
	inline float get_maxBlurSize_9() const { return ___maxBlurSize_9; }
	inline float* get_address_of_maxBlurSize_9() { return &___maxBlurSize_9; }
	inline void set_maxBlurSize_9(float value)
	{
		___maxBlurSize_9 = value;
	}

	inline static int32_t get_offset_of_downsample_10() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___downsample_10)); }
	inline int32_t get_downsample_10() const { return ___downsample_10; }
	inline int32_t* get_address_of_downsample_10() { return &___downsample_10; }
	inline void set_downsample_10(int32_t value)
	{
		___downsample_10 = value;
	}

	inline static int32_t get_offset_of_tiltShiftShader_11() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___tiltShiftShader_11)); }
	inline Shader_t4151988712 * get_tiltShiftShader_11() const { return ___tiltShiftShader_11; }
	inline Shader_t4151988712 ** get_address_of_tiltShiftShader_11() { return &___tiltShiftShader_11; }
	inline void set_tiltShiftShader_11(Shader_t4151988712 * value)
	{
		___tiltShiftShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___tiltShiftShader_11), value);
	}

	inline static int32_t get_offset_of_tiltShiftMaterial_12() { return static_cast<int32_t>(offsetof(TiltShift_t2891301701, ___tiltShiftMaterial_12)); }
	inline Material_t340375123 * get_tiltShiftMaterial_12() const { return ___tiltShiftMaterial_12; }
	inline Material_t340375123 ** get_address_of_tiltShiftMaterial_12() { return &___tiltShiftMaterial_12; }
	inline void set_tiltShiftMaterial_12(Material_t340375123 * value)
	{
		___tiltShiftMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___tiltShiftMaterial_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTSHIFT_T2891301701_H
#ifndef TONEMAPPING_T2837480251_H
#define TONEMAPPING_T2837480251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Tonemapping
struct  Tonemapping_t2837480251  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType UnityStandardAssets.ImageEffects.Tonemapping::type
	int32_t ___type_6;
	// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize UnityStandardAssets.ImageEffects.Tonemapping::adaptiveTextureSize
	int32_t ___adaptiveTextureSize_7;
	// UnityEngine.AnimationCurve UnityStandardAssets.ImageEffects.Tonemapping::remapCurve
	AnimationCurve_t3046754366 * ___remapCurve_8;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.Tonemapping::curveTex
	Texture2D_t3840446185 * ___curveTex_9;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::exposureAdjustment
	float ___exposureAdjustment_10;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::middleGrey
	float ___middleGrey_11;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::white
	float ___white_12;
	// System.Single UnityStandardAssets.ImageEffects.Tonemapping::adaptionSpeed
	float ___adaptionSpeed_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Tonemapping::tonemapper
	Shader_t4151988712 * ___tonemapper_14;
	// System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::validRenderTextureFormat
	bool ___validRenderTextureFormat_15;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Tonemapping::tonemapMaterial
	Material_t340375123 * ___tonemapMaterial_16;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.Tonemapping::rt
	RenderTexture_t2108887433 * ___rt_17;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.ImageEffects.Tonemapping::rtFormat
	int32_t ___rtFormat_18;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_adaptiveTextureSize_7() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___adaptiveTextureSize_7)); }
	inline int32_t get_adaptiveTextureSize_7() const { return ___adaptiveTextureSize_7; }
	inline int32_t* get_address_of_adaptiveTextureSize_7() { return &___adaptiveTextureSize_7; }
	inline void set_adaptiveTextureSize_7(int32_t value)
	{
		___adaptiveTextureSize_7 = value;
	}

	inline static int32_t get_offset_of_remapCurve_8() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___remapCurve_8)); }
	inline AnimationCurve_t3046754366 * get_remapCurve_8() const { return ___remapCurve_8; }
	inline AnimationCurve_t3046754366 ** get_address_of_remapCurve_8() { return &___remapCurve_8; }
	inline void set_remapCurve_8(AnimationCurve_t3046754366 * value)
	{
		___remapCurve_8 = value;
		Il2CppCodeGenWriteBarrier((&___remapCurve_8), value);
	}

	inline static int32_t get_offset_of_curveTex_9() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___curveTex_9)); }
	inline Texture2D_t3840446185 * get_curveTex_9() const { return ___curveTex_9; }
	inline Texture2D_t3840446185 ** get_address_of_curveTex_9() { return &___curveTex_9; }
	inline void set_curveTex_9(Texture2D_t3840446185 * value)
	{
		___curveTex_9 = value;
		Il2CppCodeGenWriteBarrier((&___curveTex_9), value);
	}

	inline static int32_t get_offset_of_exposureAdjustment_10() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___exposureAdjustment_10)); }
	inline float get_exposureAdjustment_10() const { return ___exposureAdjustment_10; }
	inline float* get_address_of_exposureAdjustment_10() { return &___exposureAdjustment_10; }
	inline void set_exposureAdjustment_10(float value)
	{
		___exposureAdjustment_10 = value;
	}

	inline static int32_t get_offset_of_middleGrey_11() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___middleGrey_11)); }
	inline float get_middleGrey_11() const { return ___middleGrey_11; }
	inline float* get_address_of_middleGrey_11() { return &___middleGrey_11; }
	inline void set_middleGrey_11(float value)
	{
		___middleGrey_11 = value;
	}

	inline static int32_t get_offset_of_white_12() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___white_12)); }
	inline float get_white_12() const { return ___white_12; }
	inline float* get_address_of_white_12() { return &___white_12; }
	inline void set_white_12(float value)
	{
		___white_12 = value;
	}

	inline static int32_t get_offset_of_adaptionSpeed_13() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___adaptionSpeed_13)); }
	inline float get_adaptionSpeed_13() const { return ___adaptionSpeed_13; }
	inline float* get_address_of_adaptionSpeed_13() { return &___adaptionSpeed_13; }
	inline void set_adaptionSpeed_13(float value)
	{
		___adaptionSpeed_13 = value;
	}

	inline static int32_t get_offset_of_tonemapper_14() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___tonemapper_14)); }
	inline Shader_t4151988712 * get_tonemapper_14() const { return ___tonemapper_14; }
	inline Shader_t4151988712 ** get_address_of_tonemapper_14() { return &___tonemapper_14; }
	inline void set_tonemapper_14(Shader_t4151988712 * value)
	{
		___tonemapper_14 = value;
		Il2CppCodeGenWriteBarrier((&___tonemapper_14), value);
	}

	inline static int32_t get_offset_of_validRenderTextureFormat_15() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___validRenderTextureFormat_15)); }
	inline bool get_validRenderTextureFormat_15() const { return ___validRenderTextureFormat_15; }
	inline bool* get_address_of_validRenderTextureFormat_15() { return &___validRenderTextureFormat_15; }
	inline void set_validRenderTextureFormat_15(bool value)
	{
		___validRenderTextureFormat_15 = value;
	}

	inline static int32_t get_offset_of_tonemapMaterial_16() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___tonemapMaterial_16)); }
	inline Material_t340375123 * get_tonemapMaterial_16() const { return ___tonemapMaterial_16; }
	inline Material_t340375123 ** get_address_of_tonemapMaterial_16() { return &___tonemapMaterial_16; }
	inline void set_tonemapMaterial_16(Material_t340375123 * value)
	{
		___tonemapMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___tonemapMaterial_16), value);
	}

	inline static int32_t get_offset_of_rt_17() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___rt_17)); }
	inline RenderTexture_t2108887433 * get_rt_17() const { return ___rt_17; }
	inline RenderTexture_t2108887433 ** get_address_of_rt_17() { return &___rt_17; }
	inline void set_rt_17(RenderTexture_t2108887433 * value)
	{
		___rt_17 = value;
		Il2CppCodeGenWriteBarrier((&___rt_17), value);
	}

	inline static int32_t get_offset_of_rtFormat_18() { return static_cast<int32_t>(offsetof(Tonemapping_t2837480251, ___rtFormat_18)); }
	inline int32_t get_rtFormat_18() const { return ___rtFormat_18; }
	inline int32_t* get_address_of_rtFormat_18() { return &___rtFormat_18; }
	inline void set_rtFormat_18(int32_t value)
	{
		___rtFormat_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPING_T2837480251_H
#ifndef TWIRL_T2760508880_H
#define TWIRL_T2760508880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Twirl
struct  Twirl_t2760508880  : public ImageEffectBase_t2026006575
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Twirl::radius
	Vector2_t2156229523  ___radius_4;
	// System.Single UnityStandardAssets.ImageEffects.Twirl::angle
	float ___angle_5;
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Twirl::center
	Vector2_t2156229523  ___center_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(Twirl_t2760508880, ___radius_4)); }
	inline Vector2_t2156229523  get_radius_4() const { return ___radius_4; }
	inline Vector2_t2156229523 * get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(Vector2_t2156229523  value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_angle_5() { return static_cast<int32_t>(offsetof(Twirl_t2760508880, ___angle_5)); }
	inline float get_angle_5() const { return ___angle_5; }
	inline float* get_address_of_angle_5() { return &___angle_5; }
	inline void set_angle_5(float value)
	{
		___angle_5 = value;
	}

	inline static int32_t get_offset_of_center_6() { return static_cast<int32_t>(offsetof(Twirl_t2760508880, ___center_6)); }
	inline Vector2_t2156229523  get_center_6() const { return ___center_6; }
	inline Vector2_t2156229523 * get_address_of_center_6() { return &___center_6; }
	inline void set_center_6(Vector2_t2156229523  value)
	{
		___center_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWIRL_T2760508880_H
#ifndef VIGNETTEANDCHROMATICABERRATION_T3308099924_H
#define VIGNETTEANDCHROMATICABERRATION_T3308099924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration
struct  VignetteAndChromaticAberration_t3308099924  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration/AberrationMode UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::mode
	int32_t ___mode_6;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::intensity
	float ___intensity_7;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::chromaticAberration
	float ___chromaticAberration_8;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::axialAberration
	float ___axialAberration_9;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::blur
	float ___blur_10;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::blurSpread
	float ___blurSpread_11;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::luminanceDependency
	float ___luminanceDependency_12;
	// System.Single UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::blurDistance
	float ___blurDistance_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::vignetteShader
	Shader_t4151988712 * ___vignetteShader_14;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::separableBlurShader
	Shader_t4151988712 * ___separableBlurShader_15;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::chromAberrationShader
	Shader_t4151988712 * ___chromAberrationShader_16;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::m_VignetteMaterial
	Material_t340375123 * ___m_VignetteMaterial_17;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::m_SeparableBlurMaterial
	Material_t340375123 * ___m_SeparableBlurMaterial_18;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::m_ChromAberrationMaterial
	Material_t340375123 * ___m_ChromAberrationMaterial_19;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_chromaticAberration_8() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___chromaticAberration_8)); }
	inline float get_chromaticAberration_8() const { return ___chromaticAberration_8; }
	inline float* get_address_of_chromaticAberration_8() { return &___chromaticAberration_8; }
	inline void set_chromaticAberration_8(float value)
	{
		___chromaticAberration_8 = value;
	}

	inline static int32_t get_offset_of_axialAberration_9() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___axialAberration_9)); }
	inline float get_axialAberration_9() const { return ___axialAberration_9; }
	inline float* get_address_of_axialAberration_9() { return &___axialAberration_9; }
	inline void set_axialAberration_9(float value)
	{
		___axialAberration_9 = value;
	}

	inline static int32_t get_offset_of_blur_10() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___blur_10)); }
	inline float get_blur_10() const { return ___blur_10; }
	inline float* get_address_of_blur_10() { return &___blur_10; }
	inline void set_blur_10(float value)
	{
		___blur_10 = value;
	}

	inline static int32_t get_offset_of_blurSpread_11() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___blurSpread_11)); }
	inline float get_blurSpread_11() const { return ___blurSpread_11; }
	inline float* get_address_of_blurSpread_11() { return &___blurSpread_11; }
	inline void set_blurSpread_11(float value)
	{
		___blurSpread_11 = value;
	}

	inline static int32_t get_offset_of_luminanceDependency_12() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___luminanceDependency_12)); }
	inline float get_luminanceDependency_12() const { return ___luminanceDependency_12; }
	inline float* get_address_of_luminanceDependency_12() { return &___luminanceDependency_12; }
	inline void set_luminanceDependency_12(float value)
	{
		___luminanceDependency_12 = value;
	}

	inline static int32_t get_offset_of_blurDistance_13() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___blurDistance_13)); }
	inline float get_blurDistance_13() const { return ___blurDistance_13; }
	inline float* get_address_of_blurDistance_13() { return &___blurDistance_13; }
	inline void set_blurDistance_13(float value)
	{
		___blurDistance_13 = value;
	}

	inline static int32_t get_offset_of_vignetteShader_14() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___vignetteShader_14)); }
	inline Shader_t4151988712 * get_vignetteShader_14() const { return ___vignetteShader_14; }
	inline Shader_t4151988712 ** get_address_of_vignetteShader_14() { return &___vignetteShader_14; }
	inline void set_vignetteShader_14(Shader_t4151988712 * value)
	{
		___vignetteShader_14 = value;
		Il2CppCodeGenWriteBarrier((&___vignetteShader_14), value);
	}

	inline static int32_t get_offset_of_separableBlurShader_15() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___separableBlurShader_15)); }
	inline Shader_t4151988712 * get_separableBlurShader_15() const { return ___separableBlurShader_15; }
	inline Shader_t4151988712 ** get_address_of_separableBlurShader_15() { return &___separableBlurShader_15; }
	inline void set_separableBlurShader_15(Shader_t4151988712 * value)
	{
		___separableBlurShader_15 = value;
		Il2CppCodeGenWriteBarrier((&___separableBlurShader_15), value);
	}

	inline static int32_t get_offset_of_chromAberrationShader_16() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___chromAberrationShader_16)); }
	inline Shader_t4151988712 * get_chromAberrationShader_16() const { return ___chromAberrationShader_16; }
	inline Shader_t4151988712 ** get_address_of_chromAberrationShader_16() { return &___chromAberrationShader_16; }
	inline void set_chromAberrationShader_16(Shader_t4151988712 * value)
	{
		___chromAberrationShader_16 = value;
		Il2CppCodeGenWriteBarrier((&___chromAberrationShader_16), value);
	}

	inline static int32_t get_offset_of_m_VignetteMaterial_17() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___m_VignetteMaterial_17)); }
	inline Material_t340375123 * get_m_VignetteMaterial_17() const { return ___m_VignetteMaterial_17; }
	inline Material_t340375123 ** get_address_of_m_VignetteMaterial_17() { return &___m_VignetteMaterial_17; }
	inline void set_m_VignetteMaterial_17(Material_t340375123 * value)
	{
		___m_VignetteMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_VignetteMaterial_17), value);
	}

	inline static int32_t get_offset_of_m_SeparableBlurMaterial_18() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___m_SeparableBlurMaterial_18)); }
	inline Material_t340375123 * get_m_SeparableBlurMaterial_18() const { return ___m_SeparableBlurMaterial_18; }
	inline Material_t340375123 ** get_address_of_m_SeparableBlurMaterial_18() { return &___m_SeparableBlurMaterial_18; }
	inline void set_m_SeparableBlurMaterial_18(Material_t340375123 * value)
	{
		___m_SeparableBlurMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_SeparableBlurMaterial_18), value);
	}

	inline static int32_t get_offset_of_m_ChromAberrationMaterial_19() { return static_cast<int32_t>(offsetof(VignetteAndChromaticAberration_t3308099924, ___m_ChromAberrationMaterial_19)); }
	inline Material_t340375123 * get_m_ChromAberrationMaterial_19() const { return ___m_ChromAberrationMaterial_19; }
	inline Material_t340375123 ** get_address_of_m_ChromAberrationMaterial_19() { return &___m_ChromAberrationMaterial_19; }
	inline void set_m_ChromAberrationMaterial_19(Material_t340375123 * value)
	{
		___m_ChromAberrationMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChromAberrationMaterial_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEANDCHROMATICABERRATION_T3308099924_H
#ifndef VORTEX_T3420399868_H
#define VORTEX_T3420399868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Vortex
struct  Vortex_t3420399868  : public ImageEffectBase_t2026006575
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Vortex::radius
	Vector2_t2156229523  ___radius_4;
	// System.Single UnityStandardAssets.ImageEffects.Vortex::angle
	float ___angle_5;
	// UnityEngine.Vector2 UnityStandardAssets.ImageEffects.Vortex::center
	Vector2_t2156229523  ___center_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(Vortex_t3420399868, ___radius_4)); }
	inline Vector2_t2156229523  get_radius_4() const { return ___radius_4; }
	inline Vector2_t2156229523 * get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(Vector2_t2156229523  value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_angle_5() { return static_cast<int32_t>(offsetof(Vortex_t3420399868, ___angle_5)); }
	inline float get_angle_5() const { return ___angle_5; }
	inline float* get_address_of_angle_5() { return &___angle_5; }
	inline void set_angle_5(float value)
	{
		___angle_5 = value;
	}

	inline static int32_t get_offset_of_center_6() { return static_cast<int32_t>(offsetof(Vortex_t3420399868, ___center_6)); }
	inline Vector2_t2156229523  get_center_6() const { return ___center_6; }
	inline Vector2_t2156229523 * get_address_of_center_6() { return &___center_6; }
	inline void set_center_6(Vector2_t2156229523  value)
	{
		___center_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VORTEX_T3420399868_H
#ifndef CEFFECTGOLDSHELL_T2112892863_H
#define CEFFECTGOLDSHELL_T2112892863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEffectGoldShell
struct  CEffectGoldShell_t2112892863  : public CEffect_t3687947224
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CEFFECTGOLDSHELL_T2112892863_H
#ifndef SCREENOVERLAY_T3772274400_H
#define SCREENOVERLAY_T3772274400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenOverlay
struct  ScreenOverlay_t3772274400  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode UnityStandardAssets.ImageEffects.ScreenOverlay::blendMode
	int32_t ___blendMode_6;
	// System.Single UnityStandardAssets.ImageEffects.ScreenOverlay::intensity
	float ___intensity_7;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenOverlay::texture
	Texture2D_t3840446185 * ___texture_8;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenOverlay::overlayShader
	Shader_t4151988712 * ___overlayShader_9;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenOverlay::overlayMaterial
	Material_t340375123 * ___overlayMaterial_10;

public:
	inline static int32_t get_offset_of_blendMode_6() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___blendMode_6)); }
	inline int32_t get_blendMode_6() const { return ___blendMode_6; }
	inline int32_t* get_address_of_blendMode_6() { return &___blendMode_6; }
	inline void set_blendMode_6(int32_t value)
	{
		___blendMode_6 = value;
	}

	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_texture_8() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___texture_8)); }
	inline Texture2D_t3840446185 * get_texture_8() const { return ___texture_8; }
	inline Texture2D_t3840446185 ** get_address_of_texture_8() { return &___texture_8; }
	inline void set_texture_8(Texture2D_t3840446185 * value)
	{
		___texture_8 = value;
		Il2CppCodeGenWriteBarrier((&___texture_8), value);
	}

	inline static int32_t get_offset_of_overlayShader_9() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___overlayShader_9)); }
	inline Shader_t4151988712 * get_overlayShader_9() const { return ___overlayShader_9; }
	inline Shader_t4151988712 ** get_address_of_overlayShader_9() { return &___overlayShader_9; }
	inline void set_overlayShader_9(Shader_t4151988712 * value)
	{
		___overlayShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___overlayShader_9), value);
	}

	inline static int32_t get_offset_of_overlayMaterial_10() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___overlayMaterial_10)); }
	inline Material_t340375123 * get_overlayMaterial_10() const { return ___overlayMaterial_10; }
	inline Material_t340375123 ** get_address_of_overlayMaterial_10() { return &___overlayMaterial_10; }
	inline void set_overlayMaterial_10(Material_t340375123 * value)
	{
		___overlayMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___overlayMaterial_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENOVERLAY_T3772274400_H
#ifndef DEPTHOFFIELD_T1116783936_H
#define DEPTHOFFIELD_T1116783936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfField
struct  DepthOfField_t1116783936  : public PostEffectsBase_t2404315739
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::visualizeFocus
	bool ___visualizeFocus_6;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::focalLength
	float ___focalLength_7;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::focalSize
	float ___focalSize_8;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::aperture
	float ___aperture_9;
	// UnityEngine.Transform UnityStandardAssets.ImageEffects.DepthOfField::focalTransform
	Transform_t3600365921 * ___focalTransform_10;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::maxBlurSize
	float ___maxBlurSize_11;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::highResolution
	bool ___highResolution_12;
	// UnityStandardAssets.ImageEffects.DepthOfField/BlurType UnityStandardAssets.ImageEffects.DepthOfField::blurType
	int32_t ___blurType_13;
	// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount UnityStandardAssets.ImageEffects.DepthOfField::blurSampleCount
	int32_t ___blurSampleCount_14;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::nearBlur
	bool ___nearBlur_15;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::foregroundOverlap
	float ___foregroundOverlap_16;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfField::dofHdrShader
	Shader_t4151988712 * ___dofHdrShader_17;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfField::dofHdrMaterial
	Material_t340375123 * ___dofHdrMaterial_18;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehShader
	Shader_t4151988712 * ___dx11BokehShader_19;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfField::dx11bokehMaterial
	Material_t340375123 * ___dx11bokehMaterial_20;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehThreshold
	float ___dx11BokehThreshold_21;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11SpawnHeuristic
	float ___dx11SpawnHeuristic_22;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehTexture
	Texture2D_t3840446185 * ___dx11BokehTexture_23;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehScale
	float ___dx11BokehScale_24;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::dx11BokehIntensity
	float ___dx11BokehIntensity_25;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::focalDistance01
	float ___focalDistance01_26;
	// UnityEngine.ComputeBuffer UnityStandardAssets.ImageEffects.DepthOfField::cbDrawArgs
	ComputeBuffer_t1033194329 * ___cbDrawArgs_27;
	// UnityEngine.ComputeBuffer UnityStandardAssets.ImageEffects.DepthOfField::cbPoints
	ComputeBuffer_t1033194329 * ___cbPoints_28;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfField::internalBlurWidth
	float ___internalBlurWidth_29;
	// UnityEngine.Camera UnityStandardAssets.ImageEffects.DepthOfField::cachedCamera
	Camera_t4157153871 * ___cachedCamera_30;

public:
	inline static int32_t get_offset_of_visualizeFocus_6() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___visualizeFocus_6)); }
	inline bool get_visualizeFocus_6() const { return ___visualizeFocus_6; }
	inline bool* get_address_of_visualizeFocus_6() { return &___visualizeFocus_6; }
	inline void set_visualizeFocus_6(bool value)
	{
		___visualizeFocus_6 = value;
	}

	inline static int32_t get_offset_of_focalLength_7() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___focalLength_7)); }
	inline float get_focalLength_7() const { return ___focalLength_7; }
	inline float* get_address_of_focalLength_7() { return &___focalLength_7; }
	inline void set_focalLength_7(float value)
	{
		___focalLength_7 = value;
	}

	inline static int32_t get_offset_of_focalSize_8() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___focalSize_8)); }
	inline float get_focalSize_8() const { return ___focalSize_8; }
	inline float* get_address_of_focalSize_8() { return &___focalSize_8; }
	inline void set_focalSize_8(float value)
	{
		___focalSize_8 = value;
	}

	inline static int32_t get_offset_of_aperture_9() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___aperture_9)); }
	inline float get_aperture_9() const { return ___aperture_9; }
	inline float* get_address_of_aperture_9() { return &___aperture_9; }
	inline void set_aperture_9(float value)
	{
		___aperture_9 = value;
	}

	inline static int32_t get_offset_of_focalTransform_10() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___focalTransform_10)); }
	inline Transform_t3600365921 * get_focalTransform_10() const { return ___focalTransform_10; }
	inline Transform_t3600365921 ** get_address_of_focalTransform_10() { return &___focalTransform_10; }
	inline void set_focalTransform_10(Transform_t3600365921 * value)
	{
		___focalTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___focalTransform_10), value);
	}

	inline static int32_t get_offset_of_maxBlurSize_11() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___maxBlurSize_11)); }
	inline float get_maxBlurSize_11() const { return ___maxBlurSize_11; }
	inline float* get_address_of_maxBlurSize_11() { return &___maxBlurSize_11; }
	inline void set_maxBlurSize_11(float value)
	{
		___maxBlurSize_11 = value;
	}

	inline static int32_t get_offset_of_highResolution_12() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___highResolution_12)); }
	inline bool get_highResolution_12() const { return ___highResolution_12; }
	inline bool* get_address_of_highResolution_12() { return &___highResolution_12; }
	inline void set_highResolution_12(bool value)
	{
		___highResolution_12 = value;
	}

	inline static int32_t get_offset_of_blurType_13() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___blurType_13)); }
	inline int32_t get_blurType_13() const { return ___blurType_13; }
	inline int32_t* get_address_of_blurType_13() { return &___blurType_13; }
	inline void set_blurType_13(int32_t value)
	{
		___blurType_13 = value;
	}

	inline static int32_t get_offset_of_blurSampleCount_14() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___blurSampleCount_14)); }
	inline int32_t get_blurSampleCount_14() const { return ___blurSampleCount_14; }
	inline int32_t* get_address_of_blurSampleCount_14() { return &___blurSampleCount_14; }
	inline void set_blurSampleCount_14(int32_t value)
	{
		___blurSampleCount_14 = value;
	}

	inline static int32_t get_offset_of_nearBlur_15() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___nearBlur_15)); }
	inline bool get_nearBlur_15() const { return ___nearBlur_15; }
	inline bool* get_address_of_nearBlur_15() { return &___nearBlur_15; }
	inline void set_nearBlur_15(bool value)
	{
		___nearBlur_15 = value;
	}

	inline static int32_t get_offset_of_foregroundOverlap_16() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___foregroundOverlap_16)); }
	inline float get_foregroundOverlap_16() const { return ___foregroundOverlap_16; }
	inline float* get_address_of_foregroundOverlap_16() { return &___foregroundOverlap_16; }
	inline void set_foregroundOverlap_16(float value)
	{
		___foregroundOverlap_16 = value;
	}

	inline static int32_t get_offset_of_dofHdrShader_17() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dofHdrShader_17)); }
	inline Shader_t4151988712 * get_dofHdrShader_17() const { return ___dofHdrShader_17; }
	inline Shader_t4151988712 ** get_address_of_dofHdrShader_17() { return &___dofHdrShader_17; }
	inline void set_dofHdrShader_17(Shader_t4151988712 * value)
	{
		___dofHdrShader_17 = value;
		Il2CppCodeGenWriteBarrier((&___dofHdrShader_17), value);
	}

	inline static int32_t get_offset_of_dofHdrMaterial_18() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dofHdrMaterial_18)); }
	inline Material_t340375123 * get_dofHdrMaterial_18() const { return ___dofHdrMaterial_18; }
	inline Material_t340375123 ** get_address_of_dofHdrMaterial_18() { return &___dofHdrMaterial_18; }
	inline void set_dofHdrMaterial_18(Material_t340375123 * value)
	{
		___dofHdrMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___dofHdrMaterial_18), value);
	}

	inline static int32_t get_offset_of_dx11BokehShader_19() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11BokehShader_19)); }
	inline Shader_t4151988712 * get_dx11BokehShader_19() const { return ___dx11BokehShader_19; }
	inline Shader_t4151988712 ** get_address_of_dx11BokehShader_19() { return &___dx11BokehShader_19; }
	inline void set_dx11BokehShader_19(Shader_t4151988712 * value)
	{
		___dx11BokehShader_19 = value;
		Il2CppCodeGenWriteBarrier((&___dx11BokehShader_19), value);
	}

	inline static int32_t get_offset_of_dx11bokehMaterial_20() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11bokehMaterial_20)); }
	inline Material_t340375123 * get_dx11bokehMaterial_20() const { return ___dx11bokehMaterial_20; }
	inline Material_t340375123 ** get_address_of_dx11bokehMaterial_20() { return &___dx11bokehMaterial_20; }
	inline void set_dx11bokehMaterial_20(Material_t340375123 * value)
	{
		___dx11bokehMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___dx11bokehMaterial_20), value);
	}

	inline static int32_t get_offset_of_dx11BokehThreshold_21() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11BokehThreshold_21)); }
	inline float get_dx11BokehThreshold_21() const { return ___dx11BokehThreshold_21; }
	inline float* get_address_of_dx11BokehThreshold_21() { return &___dx11BokehThreshold_21; }
	inline void set_dx11BokehThreshold_21(float value)
	{
		___dx11BokehThreshold_21 = value;
	}

	inline static int32_t get_offset_of_dx11SpawnHeuristic_22() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11SpawnHeuristic_22)); }
	inline float get_dx11SpawnHeuristic_22() const { return ___dx11SpawnHeuristic_22; }
	inline float* get_address_of_dx11SpawnHeuristic_22() { return &___dx11SpawnHeuristic_22; }
	inline void set_dx11SpawnHeuristic_22(float value)
	{
		___dx11SpawnHeuristic_22 = value;
	}

	inline static int32_t get_offset_of_dx11BokehTexture_23() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11BokehTexture_23)); }
	inline Texture2D_t3840446185 * get_dx11BokehTexture_23() const { return ___dx11BokehTexture_23; }
	inline Texture2D_t3840446185 ** get_address_of_dx11BokehTexture_23() { return &___dx11BokehTexture_23; }
	inline void set_dx11BokehTexture_23(Texture2D_t3840446185 * value)
	{
		___dx11BokehTexture_23 = value;
		Il2CppCodeGenWriteBarrier((&___dx11BokehTexture_23), value);
	}

	inline static int32_t get_offset_of_dx11BokehScale_24() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11BokehScale_24)); }
	inline float get_dx11BokehScale_24() const { return ___dx11BokehScale_24; }
	inline float* get_address_of_dx11BokehScale_24() { return &___dx11BokehScale_24; }
	inline void set_dx11BokehScale_24(float value)
	{
		___dx11BokehScale_24 = value;
	}

	inline static int32_t get_offset_of_dx11BokehIntensity_25() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___dx11BokehIntensity_25)); }
	inline float get_dx11BokehIntensity_25() const { return ___dx11BokehIntensity_25; }
	inline float* get_address_of_dx11BokehIntensity_25() { return &___dx11BokehIntensity_25; }
	inline void set_dx11BokehIntensity_25(float value)
	{
		___dx11BokehIntensity_25 = value;
	}

	inline static int32_t get_offset_of_focalDistance01_26() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___focalDistance01_26)); }
	inline float get_focalDistance01_26() const { return ___focalDistance01_26; }
	inline float* get_address_of_focalDistance01_26() { return &___focalDistance01_26; }
	inline void set_focalDistance01_26(float value)
	{
		___focalDistance01_26 = value;
	}

	inline static int32_t get_offset_of_cbDrawArgs_27() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___cbDrawArgs_27)); }
	inline ComputeBuffer_t1033194329 * get_cbDrawArgs_27() const { return ___cbDrawArgs_27; }
	inline ComputeBuffer_t1033194329 ** get_address_of_cbDrawArgs_27() { return &___cbDrawArgs_27; }
	inline void set_cbDrawArgs_27(ComputeBuffer_t1033194329 * value)
	{
		___cbDrawArgs_27 = value;
		Il2CppCodeGenWriteBarrier((&___cbDrawArgs_27), value);
	}

	inline static int32_t get_offset_of_cbPoints_28() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___cbPoints_28)); }
	inline ComputeBuffer_t1033194329 * get_cbPoints_28() const { return ___cbPoints_28; }
	inline ComputeBuffer_t1033194329 ** get_address_of_cbPoints_28() { return &___cbPoints_28; }
	inline void set_cbPoints_28(ComputeBuffer_t1033194329 * value)
	{
		___cbPoints_28 = value;
		Il2CppCodeGenWriteBarrier((&___cbPoints_28), value);
	}

	inline static int32_t get_offset_of_internalBlurWidth_29() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___internalBlurWidth_29)); }
	inline float get_internalBlurWidth_29() const { return ___internalBlurWidth_29; }
	inline float* get_address_of_internalBlurWidth_29() { return &___internalBlurWidth_29; }
	inline void set_internalBlurWidth_29(float value)
	{
		___internalBlurWidth_29 = value;
	}

	inline static int32_t get_offset_of_cachedCamera_30() { return static_cast<int32_t>(offsetof(DepthOfField_t1116783936, ___cachedCamera_30)); }
	inline Camera_t4157153871 * get_cachedCamera_30() const { return ___cachedCamera_30; }
	inline Camera_t4157153871 ** get_address_of_cachedCamera_30() { return &___cachedCamera_30; }
	inline void set_cachedCamera_30(Camera_t4157153871 * value)
	{
		___cachedCamera_30 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCamera_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELD_T1116783936_H
#ifndef DEPTHOFFIELDDEPRECATED_T4187663194_H
#define DEPTHOFFIELDDEPRECATED_T4187663194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
struct  DepthOfFieldDeprecated_t4187663194  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::quality
	int32_t ___quality_8;
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::resolution
	int32_t ___resolution_9;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::simpleTweakMode
	bool ___simpleTweakMode_10;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalPoint
	float ___focalPoint_11;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::smoothness
	float ___smoothness_12;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalZDistance
	float ___focalZDistance_13;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalZStartCurve
	float ___focalZStartCurve_14;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalZEndCurve
	float ___focalZEndCurve_15;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalStartCurve
	float ___focalStartCurve_16;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalEndCurve
	float ___focalEndCurve_17;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalDistance01
	float ___focalDistance01_18;
	// UnityEngine.Transform UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::objectFocus
	Transform_t3600365921 * ___objectFocus_19;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::focalSize
	float ___focalSize_20;
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bluriness
	int32_t ___bluriness_21;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::maxBlurSpread
	float ___maxBlurSpread_22;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::foregroundBlurExtrude
	float ___foregroundBlurExtrude_23;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofBlurShader
	Shader_t4151988712 * ___dofBlurShader_24;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofBlurMaterial
	Material_t340375123 * ___dofBlurMaterial_25;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofShader
	Shader_t4151988712 * ___dofShader_26;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::dofMaterial
	Material_t340375123 * ___dofMaterial_27;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::visualize
	bool ___visualize_28;
	// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehDestination
	int32_t ___bokehDestination_29;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::widthOverHeight
	float ___widthOverHeight_30;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::oneOverBaseSize
	float ___oneOverBaseSize_31;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokeh
	bool ___bokeh_32;
	// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehSupport
	bool ___bokehSupport_33;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehShader
	Shader_t4151988712 * ___bokehShader_34;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehTexture
	Texture2D_t3840446185 * ___bokehTexture_35;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehScale
	float ___bokehScale_36;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehIntensity
	float ___bokehIntensity_37;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehThresholdContrast
	float ___bokehThresholdContrast_38;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehThresholdLuminance
	float ___bokehThresholdLuminance_39;
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehDownsample
	int32_t ___bokehDownsample_40;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehMaterial
	Material_t340375123 * ___bokehMaterial_41;
	// UnityEngine.Camera UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::_camera
	Camera_t4157153871 * ____camera_42;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::foregroundTexture
	RenderTexture_t2108887433 * ___foregroundTexture_43;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::mediumRezWorkTexture
	RenderTexture_t2108887433 * ___mediumRezWorkTexture_44;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::finalDefocus
	RenderTexture_t2108887433 * ___finalDefocus_45;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::lowRezWorkTexture
	RenderTexture_t2108887433 * ___lowRezWorkTexture_46;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehSource
	RenderTexture_t2108887433 * ___bokehSource_47;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::bokehSource2
	RenderTexture_t2108887433 * ___bokehSource2_48;

public:
	inline static int32_t get_offset_of_quality_8() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___quality_8)); }
	inline int32_t get_quality_8() const { return ___quality_8; }
	inline int32_t* get_address_of_quality_8() { return &___quality_8; }
	inline void set_quality_8(int32_t value)
	{
		___quality_8 = value;
	}

	inline static int32_t get_offset_of_resolution_9() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___resolution_9)); }
	inline int32_t get_resolution_9() const { return ___resolution_9; }
	inline int32_t* get_address_of_resolution_9() { return &___resolution_9; }
	inline void set_resolution_9(int32_t value)
	{
		___resolution_9 = value;
	}

	inline static int32_t get_offset_of_simpleTweakMode_10() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___simpleTweakMode_10)); }
	inline bool get_simpleTweakMode_10() const { return ___simpleTweakMode_10; }
	inline bool* get_address_of_simpleTweakMode_10() { return &___simpleTweakMode_10; }
	inline void set_simpleTweakMode_10(bool value)
	{
		___simpleTweakMode_10 = value;
	}

	inline static int32_t get_offset_of_focalPoint_11() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalPoint_11)); }
	inline float get_focalPoint_11() const { return ___focalPoint_11; }
	inline float* get_address_of_focalPoint_11() { return &___focalPoint_11; }
	inline void set_focalPoint_11(float value)
	{
		___focalPoint_11 = value;
	}

	inline static int32_t get_offset_of_smoothness_12() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___smoothness_12)); }
	inline float get_smoothness_12() const { return ___smoothness_12; }
	inline float* get_address_of_smoothness_12() { return &___smoothness_12; }
	inline void set_smoothness_12(float value)
	{
		___smoothness_12 = value;
	}

	inline static int32_t get_offset_of_focalZDistance_13() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalZDistance_13)); }
	inline float get_focalZDistance_13() const { return ___focalZDistance_13; }
	inline float* get_address_of_focalZDistance_13() { return &___focalZDistance_13; }
	inline void set_focalZDistance_13(float value)
	{
		___focalZDistance_13 = value;
	}

	inline static int32_t get_offset_of_focalZStartCurve_14() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalZStartCurve_14)); }
	inline float get_focalZStartCurve_14() const { return ___focalZStartCurve_14; }
	inline float* get_address_of_focalZStartCurve_14() { return &___focalZStartCurve_14; }
	inline void set_focalZStartCurve_14(float value)
	{
		___focalZStartCurve_14 = value;
	}

	inline static int32_t get_offset_of_focalZEndCurve_15() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalZEndCurve_15)); }
	inline float get_focalZEndCurve_15() const { return ___focalZEndCurve_15; }
	inline float* get_address_of_focalZEndCurve_15() { return &___focalZEndCurve_15; }
	inline void set_focalZEndCurve_15(float value)
	{
		___focalZEndCurve_15 = value;
	}

	inline static int32_t get_offset_of_focalStartCurve_16() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalStartCurve_16)); }
	inline float get_focalStartCurve_16() const { return ___focalStartCurve_16; }
	inline float* get_address_of_focalStartCurve_16() { return &___focalStartCurve_16; }
	inline void set_focalStartCurve_16(float value)
	{
		___focalStartCurve_16 = value;
	}

	inline static int32_t get_offset_of_focalEndCurve_17() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalEndCurve_17)); }
	inline float get_focalEndCurve_17() const { return ___focalEndCurve_17; }
	inline float* get_address_of_focalEndCurve_17() { return &___focalEndCurve_17; }
	inline void set_focalEndCurve_17(float value)
	{
		___focalEndCurve_17 = value;
	}

	inline static int32_t get_offset_of_focalDistance01_18() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalDistance01_18)); }
	inline float get_focalDistance01_18() const { return ___focalDistance01_18; }
	inline float* get_address_of_focalDistance01_18() { return &___focalDistance01_18; }
	inline void set_focalDistance01_18(float value)
	{
		___focalDistance01_18 = value;
	}

	inline static int32_t get_offset_of_objectFocus_19() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___objectFocus_19)); }
	inline Transform_t3600365921 * get_objectFocus_19() const { return ___objectFocus_19; }
	inline Transform_t3600365921 ** get_address_of_objectFocus_19() { return &___objectFocus_19; }
	inline void set_objectFocus_19(Transform_t3600365921 * value)
	{
		___objectFocus_19 = value;
		Il2CppCodeGenWriteBarrier((&___objectFocus_19), value);
	}

	inline static int32_t get_offset_of_focalSize_20() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___focalSize_20)); }
	inline float get_focalSize_20() const { return ___focalSize_20; }
	inline float* get_address_of_focalSize_20() { return &___focalSize_20; }
	inline void set_focalSize_20(float value)
	{
		___focalSize_20 = value;
	}

	inline static int32_t get_offset_of_bluriness_21() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bluriness_21)); }
	inline int32_t get_bluriness_21() const { return ___bluriness_21; }
	inline int32_t* get_address_of_bluriness_21() { return &___bluriness_21; }
	inline void set_bluriness_21(int32_t value)
	{
		___bluriness_21 = value;
	}

	inline static int32_t get_offset_of_maxBlurSpread_22() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___maxBlurSpread_22)); }
	inline float get_maxBlurSpread_22() const { return ___maxBlurSpread_22; }
	inline float* get_address_of_maxBlurSpread_22() { return &___maxBlurSpread_22; }
	inline void set_maxBlurSpread_22(float value)
	{
		___maxBlurSpread_22 = value;
	}

	inline static int32_t get_offset_of_foregroundBlurExtrude_23() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___foregroundBlurExtrude_23)); }
	inline float get_foregroundBlurExtrude_23() const { return ___foregroundBlurExtrude_23; }
	inline float* get_address_of_foregroundBlurExtrude_23() { return &___foregroundBlurExtrude_23; }
	inline void set_foregroundBlurExtrude_23(float value)
	{
		___foregroundBlurExtrude_23 = value;
	}

	inline static int32_t get_offset_of_dofBlurShader_24() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___dofBlurShader_24)); }
	inline Shader_t4151988712 * get_dofBlurShader_24() const { return ___dofBlurShader_24; }
	inline Shader_t4151988712 ** get_address_of_dofBlurShader_24() { return &___dofBlurShader_24; }
	inline void set_dofBlurShader_24(Shader_t4151988712 * value)
	{
		___dofBlurShader_24 = value;
		Il2CppCodeGenWriteBarrier((&___dofBlurShader_24), value);
	}

	inline static int32_t get_offset_of_dofBlurMaterial_25() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___dofBlurMaterial_25)); }
	inline Material_t340375123 * get_dofBlurMaterial_25() const { return ___dofBlurMaterial_25; }
	inline Material_t340375123 ** get_address_of_dofBlurMaterial_25() { return &___dofBlurMaterial_25; }
	inline void set_dofBlurMaterial_25(Material_t340375123 * value)
	{
		___dofBlurMaterial_25 = value;
		Il2CppCodeGenWriteBarrier((&___dofBlurMaterial_25), value);
	}

	inline static int32_t get_offset_of_dofShader_26() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___dofShader_26)); }
	inline Shader_t4151988712 * get_dofShader_26() const { return ___dofShader_26; }
	inline Shader_t4151988712 ** get_address_of_dofShader_26() { return &___dofShader_26; }
	inline void set_dofShader_26(Shader_t4151988712 * value)
	{
		___dofShader_26 = value;
		Il2CppCodeGenWriteBarrier((&___dofShader_26), value);
	}

	inline static int32_t get_offset_of_dofMaterial_27() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___dofMaterial_27)); }
	inline Material_t340375123 * get_dofMaterial_27() const { return ___dofMaterial_27; }
	inline Material_t340375123 ** get_address_of_dofMaterial_27() { return &___dofMaterial_27; }
	inline void set_dofMaterial_27(Material_t340375123 * value)
	{
		___dofMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((&___dofMaterial_27), value);
	}

	inline static int32_t get_offset_of_visualize_28() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___visualize_28)); }
	inline bool get_visualize_28() const { return ___visualize_28; }
	inline bool* get_address_of_visualize_28() { return &___visualize_28; }
	inline void set_visualize_28(bool value)
	{
		___visualize_28 = value;
	}

	inline static int32_t get_offset_of_bokehDestination_29() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehDestination_29)); }
	inline int32_t get_bokehDestination_29() const { return ___bokehDestination_29; }
	inline int32_t* get_address_of_bokehDestination_29() { return &___bokehDestination_29; }
	inline void set_bokehDestination_29(int32_t value)
	{
		___bokehDestination_29 = value;
	}

	inline static int32_t get_offset_of_widthOverHeight_30() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___widthOverHeight_30)); }
	inline float get_widthOverHeight_30() const { return ___widthOverHeight_30; }
	inline float* get_address_of_widthOverHeight_30() { return &___widthOverHeight_30; }
	inline void set_widthOverHeight_30(float value)
	{
		___widthOverHeight_30 = value;
	}

	inline static int32_t get_offset_of_oneOverBaseSize_31() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___oneOverBaseSize_31)); }
	inline float get_oneOverBaseSize_31() const { return ___oneOverBaseSize_31; }
	inline float* get_address_of_oneOverBaseSize_31() { return &___oneOverBaseSize_31; }
	inline void set_oneOverBaseSize_31(float value)
	{
		___oneOverBaseSize_31 = value;
	}

	inline static int32_t get_offset_of_bokeh_32() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokeh_32)); }
	inline bool get_bokeh_32() const { return ___bokeh_32; }
	inline bool* get_address_of_bokeh_32() { return &___bokeh_32; }
	inline void set_bokeh_32(bool value)
	{
		___bokeh_32 = value;
	}

	inline static int32_t get_offset_of_bokehSupport_33() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehSupport_33)); }
	inline bool get_bokehSupport_33() const { return ___bokehSupport_33; }
	inline bool* get_address_of_bokehSupport_33() { return &___bokehSupport_33; }
	inline void set_bokehSupport_33(bool value)
	{
		___bokehSupport_33 = value;
	}

	inline static int32_t get_offset_of_bokehShader_34() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehShader_34)); }
	inline Shader_t4151988712 * get_bokehShader_34() const { return ___bokehShader_34; }
	inline Shader_t4151988712 ** get_address_of_bokehShader_34() { return &___bokehShader_34; }
	inline void set_bokehShader_34(Shader_t4151988712 * value)
	{
		___bokehShader_34 = value;
		Il2CppCodeGenWriteBarrier((&___bokehShader_34), value);
	}

	inline static int32_t get_offset_of_bokehTexture_35() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehTexture_35)); }
	inline Texture2D_t3840446185 * get_bokehTexture_35() const { return ___bokehTexture_35; }
	inline Texture2D_t3840446185 ** get_address_of_bokehTexture_35() { return &___bokehTexture_35; }
	inline void set_bokehTexture_35(Texture2D_t3840446185 * value)
	{
		___bokehTexture_35 = value;
		Il2CppCodeGenWriteBarrier((&___bokehTexture_35), value);
	}

	inline static int32_t get_offset_of_bokehScale_36() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehScale_36)); }
	inline float get_bokehScale_36() const { return ___bokehScale_36; }
	inline float* get_address_of_bokehScale_36() { return &___bokehScale_36; }
	inline void set_bokehScale_36(float value)
	{
		___bokehScale_36 = value;
	}

	inline static int32_t get_offset_of_bokehIntensity_37() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehIntensity_37)); }
	inline float get_bokehIntensity_37() const { return ___bokehIntensity_37; }
	inline float* get_address_of_bokehIntensity_37() { return &___bokehIntensity_37; }
	inline void set_bokehIntensity_37(float value)
	{
		___bokehIntensity_37 = value;
	}

	inline static int32_t get_offset_of_bokehThresholdContrast_38() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehThresholdContrast_38)); }
	inline float get_bokehThresholdContrast_38() const { return ___bokehThresholdContrast_38; }
	inline float* get_address_of_bokehThresholdContrast_38() { return &___bokehThresholdContrast_38; }
	inline void set_bokehThresholdContrast_38(float value)
	{
		___bokehThresholdContrast_38 = value;
	}

	inline static int32_t get_offset_of_bokehThresholdLuminance_39() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehThresholdLuminance_39)); }
	inline float get_bokehThresholdLuminance_39() const { return ___bokehThresholdLuminance_39; }
	inline float* get_address_of_bokehThresholdLuminance_39() { return &___bokehThresholdLuminance_39; }
	inline void set_bokehThresholdLuminance_39(float value)
	{
		___bokehThresholdLuminance_39 = value;
	}

	inline static int32_t get_offset_of_bokehDownsample_40() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehDownsample_40)); }
	inline int32_t get_bokehDownsample_40() const { return ___bokehDownsample_40; }
	inline int32_t* get_address_of_bokehDownsample_40() { return &___bokehDownsample_40; }
	inline void set_bokehDownsample_40(int32_t value)
	{
		___bokehDownsample_40 = value;
	}

	inline static int32_t get_offset_of_bokehMaterial_41() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehMaterial_41)); }
	inline Material_t340375123 * get_bokehMaterial_41() const { return ___bokehMaterial_41; }
	inline Material_t340375123 ** get_address_of_bokehMaterial_41() { return &___bokehMaterial_41; }
	inline void set_bokehMaterial_41(Material_t340375123 * value)
	{
		___bokehMaterial_41 = value;
		Il2CppCodeGenWriteBarrier((&___bokehMaterial_41), value);
	}

	inline static int32_t get_offset_of__camera_42() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ____camera_42)); }
	inline Camera_t4157153871 * get__camera_42() const { return ____camera_42; }
	inline Camera_t4157153871 ** get_address_of__camera_42() { return &____camera_42; }
	inline void set__camera_42(Camera_t4157153871 * value)
	{
		____camera_42 = value;
		Il2CppCodeGenWriteBarrier((&____camera_42), value);
	}

	inline static int32_t get_offset_of_foregroundTexture_43() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___foregroundTexture_43)); }
	inline RenderTexture_t2108887433 * get_foregroundTexture_43() const { return ___foregroundTexture_43; }
	inline RenderTexture_t2108887433 ** get_address_of_foregroundTexture_43() { return &___foregroundTexture_43; }
	inline void set_foregroundTexture_43(RenderTexture_t2108887433 * value)
	{
		___foregroundTexture_43 = value;
		Il2CppCodeGenWriteBarrier((&___foregroundTexture_43), value);
	}

	inline static int32_t get_offset_of_mediumRezWorkTexture_44() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___mediumRezWorkTexture_44)); }
	inline RenderTexture_t2108887433 * get_mediumRezWorkTexture_44() const { return ___mediumRezWorkTexture_44; }
	inline RenderTexture_t2108887433 ** get_address_of_mediumRezWorkTexture_44() { return &___mediumRezWorkTexture_44; }
	inline void set_mediumRezWorkTexture_44(RenderTexture_t2108887433 * value)
	{
		___mediumRezWorkTexture_44 = value;
		Il2CppCodeGenWriteBarrier((&___mediumRezWorkTexture_44), value);
	}

	inline static int32_t get_offset_of_finalDefocus_45() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___finalDefocus_45)); }
	inline RenderTexture_t2108887433 * get_finalDefocus_45() const { return ___finalDefocus_45; }
	inline RenderTexture_t2108887433 ** get_address_of_finalDefocus_45() { return &___finalDefocus_45; }
	inline void set_finalDefocus_45(RenderTexture_t2108887433 * value)
	{
		___finalDefocus_45 = value;
		Il2CppCodeGenWriteBarrier((&___finalDefocus_45), value);
	}

	inline static int32_t get_offset_of_lowRezWorkTexture_46() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___lowRezWorkTexture_46)); }
	inline RenderTexture_t2108887433 * get_lowRezWorkTexture_46() const { return ___lowRezWorkTexture_46; }
	inline RenderTexture_t2108887433 ** get_address_of_lowRezWorkTexture_46() { return &___lowRezWorkTexture_46; }
	inline void set_lowRezWorkTexture_46(RenderTexture_t2108887433 * value)
	{
		___lowRezWorkTexture_46 = value;
		Il2CppCodeGenWriteBarrier((&___lowRezWorkTexture_46), value);
	}

	inline static int32_t get_offset_of_bokehSource_47() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehSource_47)); }
	inline RenderTexture_t2108887433 * get_bokehSource_47() const { return ___bokehSource_47; }
	inline RenderTexture_t2108887433 ** get_address_of_bokehSource_47() { return &___bokehSource_47; }
	inline void set_bokehSource_47(RenderTexture_t2108887433 * value)
	{
		___bokehSource_47 = value;
		Il2CppCodeGenWriteBarrier((&___bokehSource_47), value);
	}

	inline static int32_t get_offset_of_bokehSource2_48() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194, ___bokehSource2_48)); }
	inline RenderTexture_t2108887433 * get_bokehSource2_48() const { return ___bokehSource2_48; }
	inline RenderTexture_t2108887433 ** get_address_of_bokehSource2_48() { return &___bokehSource2_48; }
	inline void set_bokehSource2_48(RenderTexture_t2108887433 * value)
	{
		___bokehSource2_48 = value;
		Il2CppCodeGenWriteBarrier((&___bokehSource2_48), value);
	}
};

struct DepthOfFieldDeprecated_t4187663194_StaticFields
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::SMOOTH_DOWNSAMPLE_PASS
	int32_t ___SMOOTH_DOWNSAMPLE_PASS_6;
	// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BOKEH_EXTRA_BLUR
	float ___BOKEH_EXTRA_BLUR_7;

public:
	inline static int32_t get_offset_of_SMOOTH_DOWNSAMPLE_PASS_6() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194_StaticFields, ___SMOOTH_DOWNSAMPLE_PASS_6)); }
	inline int32_t get_SMOOTH_DOWNSAMPLE_PASS_6() const { return ___SMOOTH_DOWNSAMPLE_PASS_6; }
	inline int32_t* get_address_of_SMOOTH_DOWNSAMPLE_PASS_6() { return &___SMOOTH_DOWNSAMPLE_PASS_6; }
	inline void set_SMOOTH_DOWNSAMPLE_PASS_6(int32_t value)
	{
		___SMOOTH_DOWNSAMPLE_PASS_6 = value;
	}

	inline static int32_t get_offset_of_BOKEH_EXTRA_BLUR_7() { return static_cast<int32_t>(offsetof(DepthOfFieldDeprecated_t4187663194_StaticFields, ___BOKEH_EXTRA_BLUR_7)); }
	inline float get_BOKEH_EXTRA_BLUR_7() const { return ___BOKEH_EXTRA_BLUR_7; }
	inline float* get_address_of_BOKEH_EXTRA_BLUR_7() { return &___BOKEH_EXTRA_BLUR_7; }
	inline void set_BOKEH_EXTRA_BLUR_7(float value)
	{
		___BOKEH_EXTRA_BLUR_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDDEPRECATED_T4187663194_H
#ifndef EDGEDETECTION_T506487406_H
#define EDGEDETECTION_T506487406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.EdgeDetection
struct  EdgeDetection_t506487406  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode UnityStandardAssets.ImageEffects.EdgeDetection::mode
	int32_t ___mode_6;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::sensitivityDepth
	float ___sensitivityDepth_7;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::sensitivityNormals
	float ___sensitivityNormals_8;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::lumThreshold
	float ___lumThreshold_9;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::edgeExp
	float ___edgeExp_10;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::sampleDist
	float ___sampleDist_11;
	// System.Single UnityStandardAssets.ImageEffects.EdgeDetection::edgesOnly
	float ___edgesOnly_12;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.EdgeDetection::edgesOnlyBgColor
	Color_t2555686324  ___edgesOnlyBgColor_13;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.EdgeDetection::edgeDetectShader
	Shader_t4151988712 * ___edgeDetectShader_14;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.EdgeDetection::edgeDetectMaterial
	Material_t340375123 * ___edgeDetectMaterial_15;
	// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode UnityStandardAssets.ImageEffects.EdgeDetection::oldMode
	int32_t ___oldMode_16;

public:
	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_sensitivityDepth_7() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___sensitivityDepth_7)); }
	inline float get_sensitivityDepth_7() const { return ___sensitivityDepth_7; }
	inline float* get_address_of_sensitivityDepth_7() { return &___sensitivityDepth_7; }
	inline void set_sensitivityDepth_7(float value)
	{
		___sensitivityDepth_7 = value;
	}

	inline static int32_t get_offset_of_sensitivityNormals_8() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___sensitivityNormals_8)); }
	inline float get_sensitivityNormals_8() const { return ___sensitivityNormals_8; }
	inline float* get_address_of_sensitivityNormals_8() { return &___sensitivityNormals_8; }
	inline void set_sensitivityNormals_8(float value)
	{
		___sensitivityNormals_8 = value;
	}

	inline static int32_t get_offset_of_lumThreshold_9() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___lumThreshold_9)); }
	inline float get_lumThreshold_9() const { return ___lumThreshold_9; }
	inline float* get_address_of_lumThreshold_9() { return &___lumThreshold_9; }
	inline void set_lumThreshold_9(float value)
	{
		___lumThreshold_9 = value;
	}

	inline static int32_t get_offset_of_edgeExp_10() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___edgeExp_10)); }
	inline float get_edgeExp_10() const { return ___edgeExp_10; }
	inline float* get_address_of_edgeExp_10() { return &___edgeExp_10; }
	inline void set_edgeExp_10(float value)
	{
		___edgeExp_10 = value;
	}

	inline static int32_t get_offset_of_sampleDist_11() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___sampleDist_11)); }
	inline float get_sampleDist_11() const { return ___sampleDist_11; }
	inline float* get_address_of_sampleDist_11() { return &___sampleDist_11; }
	inline void set_sampleDist_11(float value)
	{
		___sampleDist_11 = value;
	}

	inline static int32_t get_offset_of_edgesOnly_12() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___edgesOnly_12)); }
	inline float get_edgesOnly_12() const { return ___edgesOnly_12; }
	inline float* get_address_of_edgesOnly_12() { return &___edgesOnly_12; }
	inline void set_edgesOnly_12(float value)
	{
		___edgesOnly_12 = value;
	}

	inline static int32_t get_offset_of_edgesOnlyBgColor_13() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___edgesOnlyBgColor_13)); }
	inline Color_t2555686324  get_edgesOnlyBgColor_13() const { return ___edgesOnlyBgColor_13; }
	inline Color_t2555686324 * get_address_of_edgesOnlyBgColor_13() { return &___edgesOnlyBgColor_13; }
	inline void set_edgesOnlyBgColor_13(Color_t2555686324  value)
	{
		___edgesOnlyBgColor_13 = value;
	}

	inline static int32_t get_offset_of_edgeDetectShader_14() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___edgeDetectShader_14)); }
	inline Shader_t4151988712 * get_edgeDetectShader_14() const { return ___edgeDetectShader_14; }
	inline Shader_t4151988712 ** get_address_of_edgeDetectShader_14() { return &___edgeDetectShader_14; }
	inline void set_edgeDetectShader_14(Shader_t4151988712 * value)
	{
		___edgeDetectShader_14 = value;
		Il2CppCodeGenWriteBarrier((&___edgeDetectShader_14), value);
	}

	inline static int32_t get_offset_of_edgeDetectMaterial_15() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___edgeDetectMaterial_15)); }
	inline Material_t340375123 * get_edgeDetectMaterial_15() const { return ___edgeDetectMaterial_15; }
	inline Material_t340375123 ** get_address_of_edgeDetectMaterial_15() { return &___edgeDetectMaterial_15; }
	inline void set_edgeDetectMaterial_15(Material_t340375123 * value)
	{
		___edgeDetectMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___edgeDetectMaterial_15), value);
	}

	inline static int32_t get_offset_of_oldMode_16() { return static_cast<int32_t>(offsetof(EdgeDetection_t506487406, ___oldMode_16)); }
	inline int32_t get_oldMode_16() const { return ___oldMode_16; }
	inline int32_t* get_address_of_oldMode_16() { return &___oldMode_16; }
	inline void set_oldMode_16(int32_t value)
	{
		___oldMode_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEDETECTION_T506487406_H
#ifndef FISHEYE_T4101461743_H
#define FISHEYE_T4101461743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Fisheye
struct  Fisheye_t4101461743  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.Fisheye::strengthX
	float ___strengthX_6;
	// System.Single UnityStandardAssets.ImageEffects.Fisheye::strengthY
	float ___strengthY_7;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Fisheye::fishEyeShader
	Shader_t4151988712 * ___fishEyeShader_8;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Fisheye::fisheyeMaterial
	Material_t340375123 * ___fisheyeMaterial_9;

public:
	inline static int32_t get_offset_of_strengthX_6() { return static_cast<int32_t>(offsetof(Fisheye_t4101461743, ___strengthX_6)); }
	inline float get_strengthX_6() const { return ___strengthX_6; }
	inline float* get_address_of_strengthX_6() { return &___strengthX_6; }
	inline void set_strengthX_6(float value)
	{
		___strengthX_6 = value;
	}

	inline static int32_t get_offset_of_strengthY_7() { return static_cast<int32_t>(offsetof(Fisheye_t4101461743, ___strengthY_7)); }
	inline float get_strengthY_7() const { return ___strengthY_7; }
	inline float* get_address_of_strengthY_7() { return &___strengthY_7; }
	inline void set_strengthY_7(float value)
	{
		___strengthY_7 = value;
	}

	inline static int32_t get_offset_of_fishEyeShader_8() { return static_cast<int32_t>(offsetof(Fisheye_t4101461743, ___fishEyeShader_8)); }
	inline Shader_t4151988712 * get_fishEyeShader_8() const { return ___fishEyeShader_8; }
	inline Shader_t4151988712 ** get_address_of_fishEyeShader_8() { return &___fishEyeShader_8; }
	inline void set_fishEyeShader_8(Shader_t4151988712 * value)
	{
		___fishEyeShader_8 = value;
		Il2CppCodeGenWriteBarrier((&___fishEyeShader_8), value);
	}

	inline static int32_t get_offset_of_fisheyeMaterial_9() { return static_cast<int32_t>(offsetof(Fisheye_t4101461743, ___fisheyeMaterial_9)); }
	inline Material_t340375123 * get_fisheyeMaterial_9() const { return ___fisheyeMaterial_9; }
	inline Material_t340375123 ** get_address_of_fisheyeMaterial_9() { return &___fisheyeMaterial_9; }
	inline void set_fisheyeMaterial_9(Material_t340375123 * value)
	{
		___fisheyeMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___fisheyeMaterial_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISHEYE_T4101461743_H
#ifndef GLOBALFOG_T900542613_H
#define GLOBALFOG_T900542613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.GlobalFog
struct  GlobalFog_t900542613  : public PostEffectsBase_t2404315739
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::distanceFog
	bool ___distanceFog_6;
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::excludeFarPixels
	bool ___excludeFarPixels_7;
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::useRadialDistance
	bool ___useRadialDistance_8;
	// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::heightFog
	bool ___heightFog_9;
	// System.Single UnityStandardAssets.ImageEffects.GlobalFog::height
	float ___height_10;
	// System.Single UnityStandardAssets.ImageEffects.GlobalFog::heightDensity
	float ___heightDensity_11;
	// System.Single UnityStandardAssets.ImageEffects.GlobalFog::startDistance
	float ___startDistance_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.GlobalFog::fogShader
	Shader_t4151988712 * ___fogShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.GlobalFog::fogMaterial
	Material_t340375123 * ___fogMaterial_14;

public:
	inline static int32_t get_offset_of_distanceFog_6() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___distanceFog_6)); }
	inline bool get_distanceFog_6() const { return ___distanceFog_6; }
	inline bool* get_address_of_distanceFog_6() { return &___distanceFog_6; }
	inline void set_distanceFog_6(bool value)
	{
		___distanceFog_6 = value;
	}

	inline static int32_t get_offset_of_excludeFarPixels_7() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___excludeFarPixels_7)); }
	inline bool get_excludeFarPixels_7() const { return ___excludeFarPixels_7; }
	inline bool* get_address_of_excludeFarPixels_7() { return &___excludeFarPixels_7; }
	inline void set_excludeFarPixels_7(bool value)
	{
		___excludeFarPixels_7 = value;
	}

	inline static int32_t get_offset_of_useRadialDistance_8() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___useRadialDistance_8)); }
	inline bool get_useRadialDistance_8() const { return ___useRadialDistance_8; }
	inline bool* get_address_of_useRadialDistance_8() { return &___useRadialDistance_8; }
	inline void set_useRadialDistance_8(bool value)
	{
		___useRadialDistance_8 = value;
	}

	inline static int32_t get_offset_of_heightFog_9() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___heightFog_9)); }
	inline bool get_heightFog_9() const { return ___heightFog_9; }
	inline bool* get_address_of_heightFog_9() { return &___heightFog_9; }
	inline void set_heightFog_9(bool value)
	{
		___heightFog_9 = value;
	}

	inline static int32_t get_offset_of_height_10() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___height_10)); }
	inline float get_height_10() const { return ___height_10; }
	inline float* get_address_of_height_10() { return &___height_10; }
	inline void set_height_10(float value)
	{
		___height_10 = value;
	}

	inline static int32_t get_offset_of_heightDensity_11() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___heightDensity_11)); }
	inline float get_heightDensity_11() const { return ___heightDensity_11; }
	inline float* get_address_of_heightDensity_11() { return &___heightDensity_11; }
	inline void set_heightDensity_11(float value)
	{
		___heightDensity_11 = value;
	}

	inline static int32_t get_offset_of_startDistance_12() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___startDistance_12)); }
	inline float get_startDistance_12() const { return ___startDistance_12; }
	inline float* get_address_of_startDistance_12() { return &___startDistance_12; }
	inline void set_startDistance_12(float value)
	{
		___startDistance_12 = value;
	}

	inline static int32_t get_offset_of_fogShader_13() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___fogShader_13)); }
	inline Shader_t4151988712 * get_fogShader_13() const { return ___fogShader_13; }
	inline Shader_t4151988712 ** get_address_of_fogShader_13() { return &___fogShader_13; }
	inline void set_fogShader_13(Shader_t4151988712 * value)
	{
		___fogShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___fogShader_13), value);
	}

	inline static int32_t get_offset_of_fogMaterial_14() { return static_cast<int32_t>(offsetof(GlobalFog_t900542613, ___fogMaterial_14)); }
	inline Material_t340375123 * get_fogMaterial_14() const { return ___fogMaterial_14; }
	inline Material_t340375123 ** get_address_of_fogMaterial_14() { return &___fogMaterial_14; }
	inline void set_fogMaterial_14(Material_t340375123 * value)
	{
		___fogMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___fogMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALFOG_T900542613_H
#ifndef GRAYSCALE_T1707485390_H
#define GRAYSCALE_T1707485390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Grayscale
struct  Grayscale_t1707485390  : public ImageEffectBase_t2026006575
{
public:
	// UnityEngine.Texture UnityStandardAssets.ImageEffects.Grayscale::textureRamp
	Texture_t3661962703 * ___textureRamp_4;
	// System.Single UnityStandardAssets.ImageEffects.Grayscale::rampOffset
	float ___rampOffset_5;

public:
	inline static int32_t get_offset_of_textureRamp_4() { return static_cast<int32_t>(offsetof(Grayscale_t1707485390, ___textureRamp_4)); }
	inline Texture_t3661962703 * get_textureRamp_4() const { return ___textureRamp_4; }
	inline Texture_t3661962703 ** get_address_of_textureRamp_4() { return &___textureRamp_4; }
	inline void set_textureRamp_4(Texture_t3661962703 * value)
	{
		___textureRamp_4 = value;
		Il2CppCodeGenWriteBarrier((&___textureRamp_4), value);
	}

	inline static int32_t get_offset_of_rampOffset_5() { return static_cast<int32_t>(offsetof(Grayscale_t1707485390, ___rampOffset_5)); }
	inline float get_rampOffset_5() const { return ___rampOffset_5; }
	inline float* get_address_of_rampOffset_5() { return &___rampOffset_5; }
	inline void set_rampOffset_5(float value)
	{
		___rampOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAYSCALE_T1707485390_H
#ifndef MOTIONBLUR_T1587267364_H
#define MOTIONBLUR_T1587267364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.MotionBlur
struct  MotionBlur_t1587267364  : public ImageEffectBase_t2026006575
{
public:
	// System.Single UnityStandardAssets.ImageEffects.MotionBlur::blurAmount
	float ___blurAmount_4;
	// System.Boolean UnityStandardAssets.ImageEffects.MotionBlur::extraBlur
	bool ___extraBlur_5;
	// UnityEngine.RenderTexture UnityStandardAssets.ImageEffects.MotionBlur::accumTexture
	RenderTexture_t2108887433 * ___accumTexture_6;

public:
	inline static int32_t get_offset_of_blurAmount_4() { return static_cast<int32_t>(offsetof(MotionBlur_t1587267364, ___blurAmount_4)); }
	inline float get_blurAmount_4() const { return ___blurAmount_4; }
	inline float* get_address_of_blurAmount_4() { return &___blurAmount_4; }
	inline void set_blurAmount_4(float value)
	{
		___blurAmount_4 = value;
	}

	inline static int32_t get_offset_of_extraBlur_5() { return static_cast<int32_t>(offsetof(MotionBlur_t1587267364, ___extraBlur_5)); }
	inline bool get_extraBlur_5() const { return ___extraBlur_5; }
	inline bool* get_address_of_extraBlur_5() { return &___extraBlur_5; }
	inline void set_extraBlur_5(bool value)
	{
		___extraBlur_5 = value;
	}

	inline static int32_t get_offset_of_accumTexture_6() { return static_cast<int32_t>(offsetof(MotionBlur_t1587267364, ___accumTexture_6)); }
	inline RenderTexture_t2108887433 * get_accumTexture_6() const { return ___accumTexture_6; }
	inline RenderTexture_t2108887433 ** get_address_of_accumTexture_6() { return &___accumTexture_6; }
	inline void set_accumTexture_6(RenderTexture_t2108887433 * value)
	{
		___accumTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___accumTexture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_T1587267364_H
#ifndef NOISEANDGRAIN_T3814230817_H
#define NOISEANDGRAIN_T3814230817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.NoiseAndGrain
struct  NoiseAndGrain_t3814230817  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::intensityMultiplier
	float ___intensityMultiplier_6;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::generalIntensity
	float ___generalIntensity_7;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::blackIntensity
	float ___blackIntensity_8;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::whiteIntensity
	float ___whiteIntensity_9;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::midGrey
	float ___midGrey_10;
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::dx11Grain
	bool ___dx11Grain_11;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::softness
	float ___softness_12;
	// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::monochrome
	bool ___monochrome_13;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.NoiseAndGrain::intensities
	Vector3_t3722313464  ___intensities_14;
	// UnityEngine.Vector3 UnityStandardAssets.ImageEffects.NoiseAndGrain::tiling
	Vector3_t3722313464  ___tiling_15;
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::monochromeTiling
	float ___monochromeTiling_16;
	// UnityEngine.FilterMode UnityStandardAssets.ImageEffects.NoiseAndGrain::filterMode
	int32_t ___filterMode_17;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.NoiseAndGrain::noiseTexture
	Texture2D_t3840446185 * ___noiseTexture_18;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndGrain::noiseShader
	Shader_t4151988712 * ___noiseShader_19;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndGrain::noiseMaterial
	Material_t340375123 * ___noiseMaterial_20;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.NoiseAndGrain::dx11NoiseShader
	Shader_t4151988712 * ___dx11NoiseShader_21;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndGrain::dx11NoiseMaterial
	Material_t340375123 * ___dx11NoiseMaterial_22;

public:
	inline static int32_t get_offset_of_intensityMultiplier_6() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___intensityMultiplier_6)); }
	inline float get_intensityMultiplier_6() const { return ___intensityMultiplier_6; }
	inline float* get_address_of_intensityMultiplier_6() { return &___intensityMultiplier_6; }
	inline void set_intensityMultiplier_6(float value)
	{
		___intensityMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_generalIntensity_7() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___generalIntensity_7)); }
	inline float get_generalIntensity_7() const { return ___generalIntensity_7; }
	inline float* get_address_of_generalIntensity_7() { return &___generalIntensity_7; }
	inline void set_generalIntensity_7(float value)
	{
		___generalIntensity_7 = value;
	}

	inline static int32_t get_offset_of_blackIntensity_8() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___blackIntensity_8)); }
	inline float get_blackIntensity_8() const { return ___blackIntensity_8; }
	inline float* get_address_of_blackIntensity_8() { return &___blackIntensity_8; }
	inline void set_blackIntensity_8(float value)
	{
		___blackIntensity_8 = value;
	}

	inline static int32_t get_offset_of_whiteIntensity_9() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___whiteIntensity_9)); }
	inline float get_whiteIntensity_9() const { return ___whiteIntensity_9; }
	inline float* get_address_of_whiteIntensity_9() { return &___whiteIntensity_9; }
	inline void set_whiteIntensity_9(float value)
	{
		___whiteIntensity_9 = value;
	}

	inline static int32_t get_offset_of_midGrey_10() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___midGrey_10)); }
	inline float get_midGrey_10() const { return ___midGrey_10; }
	inline float* get_address_of_midGrey_10() { return &___midGrey_10; }
	inline void set_midGrey_10(float value)
	{
		___midGrey_10 = value;
	}

	inline static int32_t get_offset_of_dx11Grain_11() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___dx11Grain_11)); }
	inline bool get_dx11Grain_11() const { return ___dx11Grain_11; }
	inline bool* get_address_of_dx11Grain_11() { return &___dx11Grain_11; }
	inline void set_dx11Grain_11(bool value)
	{
		___dx11Grain_11 = value;
	}

	inline static int32_t get_offset_of_softness_12() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___softness_12)); }
	inline float get_softness_12() const { return ___softness_12; }
	inline float* get_address_of_softness_12() { return &___softness_12; }
	inline void set_softness_12(float value)
	{
		___softness_12 = value;
	}

	inline static int32_t get_offset_of_monochrome_13() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___monochrome_13)); }
	inline bool get_monochrome_13() const { return ___monochrome_13; }
	inline bool* get_address_of_monochrome_13() { return &___monochrome_13; }
	inline void set_monochrome_13(bool value)
	{
		___monochrome_13 = value;
	}

	inline static int32_t get_offset_of_intensities_14() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___intensities_14)); }
	inline Vector3_t3722313464  get_intensities_14() const { return ___intensities_14; }
	inline Vector3_t3722313464 * get_address_of_intensities_14() { return &___intensities_14; }
	inline void set_intensities_14(Vector3_t3722313464  value)
	{
		___intensities_14 = value;
	}

	inline static int32_t get_offset_of_tiling_15() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___tiling_15)); }
	inline Vector3_t3722313464  get_tiling_15() const { return ___tiling_15; }
	inline Vector3_t3722313464 * get_address_of_tiling_15() { return &___tiling_15; }
	inline void set_tiling_15(Vector3_t3722313464  value)
	{
		___tiling_15 = value;
	}

	inline static int32_t get_offset_of_monochromeTiling_16() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___monochromeTiling_16)); }
	inline float get_monochromeTiling_16() const { return ___monochromeTiling_16; }
	inline float* get_address_of_monochromeTiling_16() { return &___monochromeTiling_16; }
	inline void set_monochromeTiling_16(float value)
	{
		___monochromeTiling_16 = value;
	}

	inline static int32_t get_offset_of_filterMode_17() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___filterMode_17)); }
	inline int32_t get_filterMode_17() const { return ___filterMode_17; }
	inline int32_t* get_address_of_filterMode_17() { return &___filterMode_17; }
	inline void set_filterMode_17(int32_t value)
	{
		___filterMode_17 = value;
	}

	inline static int32_t get_offset_of_noiseTexture_18() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___noiseTexture_18)); }
	inline Texture2D_t3840446185 * get_noiseTexture_18() const { return ___noiseTexture_18; }
	inline Texture2D_t3840446185 ** get_address_of_noiseTexture_18() { return &___noiseTexture_18; }
	inline void set_noiseTexture_18(Texture2D_t3840446185 * value)
	{
		___noiseTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTexture_18), value);
	}

	inline static int32_t get_offset_of_noiseShader_19() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___noiseShader_19)); }
	inline Shader_t4151988712 * get_noiseShader_19() const { return ___noiseShader_19; }
	inline Shader_t4151988712 ** get_address_of_noiseShader_19() { return &___noiseShader_19; }
	inline void set_noiseShader_19(Shader_t4151988712 * value)
	{
		___noiseShader_19 = value;
		Il2CppCodeGenWriteBarrier((&___noiseShader_19), value);
	}

	inline static int32_t get_offset_of_noiseMaterial_20() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___noiseMaterial_20)); }
	inline Material_t340375123 * get_noiseMaterial_20() const { return ___noiseMaterial_20; }
	inline Material_t340375123 ** get_address_of_noiseMaterial_20() { return &___noiseMaterial_20; }
	inline void set_noiseMaterial_20(Material_t340375123 * value)
	{
		___noiseMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___noiseMaterial_20), value);
	}

	inline static int32_t get_offset_of_dx11NoiseShader_21() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___dx11NoiseShader_21)); }
	inline Shader_t4151988712 * get_dx11NoiseShader_21() const { return ___dx11NoiseShader_21; }
	inline Shader_t4151988712 ** get_address_of_dx11NoiseShader_21() { return &___dx11NoiseShader_21; }
	inline void set_dx11NoiseShader_21(Shader_t4151988712 * value)
	{
		___dx11NoiseShader_21 = value;
		Il2CppCodeGenWriteBarrier((&___dx11NoiseShader_21), value);
	}

	inline static int32_t get_offset_of_dx11NoiseMaterial_22() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817, ___dx11NoiseMaterial_22)); }
	inline Material_t340375123 * get_dx11NoiseMaterial_22() const { return ___dx11NoiseMaterial_22; }
	inline Material_t340375123 ** get_address_of_dx11NoiseMaterial_22() { return &___dx11NoiseMaterial_22; }
	inline void set_dx11NoiseMaterial_22(Material_t340375123 * value)
	{
		___dx11NoiseMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___dx11NoiseMaterial_22), value);
	}
};

struct NoiseAndGrain_t3814230817_StaticFields
{
public:
	// System.Single UnityStandardAssets.ImageEffects.NoiseAndGrain::TILE_AMOUNT
	float ___TILE_AMOUNT_23;

public:
	inline static int32_t get_offset_of_TILE_AMOUNT_23() { return static_cast<int32_t>(offsetof(NoiseAndGrain_t3814230817_StaticFields, ___TILE_AMOUNT_23)); }
	inline float get_TILE_AMOUNT_23() const { return ___TILE_AMOUNT_23; }
	inline float* get_address_of_TILE_AMOUNT_23() { return &___TILE_AMOUNT_23; }
	inline void set_TILE_AMOUNT_23(float value)
	{
		___TILE_AMOUNT_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISEANDGRAIN_T3814230817_H
#ifndef SCREENSPACEAMBIENTOBSCURANCE_T1844081910_H
#define SCREENSPACEAMBIENTOBSCURANCE_T1844081910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
struct  ScreenSpaceAmbientObscurance_t1844081910  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::intensity
	float ___intensity_6;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::radius
	float ___radius_7;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::blurIterations
	int32_t ___blurIterations_8;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::blurFilterDistance
	float ___blurFilterDistance_9;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::downsample
	int32_t ___downsample_10;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::rand
	Texture2D_t3840446185 * ___rand_11;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::aoShader
	Shader_t4151988712 * ___aoShader_12;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::aoMaterial
	Material_t340375123 * ___aoMaterial_13;

public:
	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___radius_7)); }
	inline float get_radius_7() const { return ___radius_7; }
	inline float* get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(float value)
	{
		___radius_7 = value;
	}

	inline static int32_t get_offset_of_blurIterations_8() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___blurIterations_8)); }
	inline int32_t get_blurIterations_8() const { return ___blurIterations_8; }
	inline int32_t* get_address_of_blurIterations_8() { return &___blurIterations_8; }
	inline void set_blurIterations_8(int32_t value)
	{
		___blurIterations_8 = value;
	}

	inline static int32_t get_offset_of_blurFilterDistance_9() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___blurFilterDistance_9)); }
	inline float get_blurFilterDistance_9() const { return ___blurFilterDistance_9; }
	inline float* get_address_of_blurFilterDistance_9() { return &___blurFilterDistance_9; }
	inline void set_blurFilterDistance_9(float value)
	{
		___blurFilterDistance_9 = value;
	}

	inline static int32_t get_offset_of_downsample_10() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___downsample_10)); }
	inline int32_t get_downsample_10() const { return ___downsample_10; }
	inline int32_t* get_address_of_downsample_10() { return &___downsample_10; }
	inline void set_downsample_10(int32_t value)
	{
		___downsample_10 = value;
	}

	inline static int32_t get_offset_of_rand_11() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___rand_11)); }
	inline Texture2D_t3840446185 * get_rand_11() const { return ___rand_11; }
	inline Texture2D_t3840446185 ** get_address_of_rand_11() { return &___rand_11; }
	inline void set_rand_11(Texture2D_t3840446185 * value)
	{
		___rand_11 = value;
		Il2CppCodeGenWriteBarrier((&___rand_11), value);
	}

	inline static int32_t get_offset_of_aoShader_12() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___aoShader_12)); }
	inline Shader_t4151988712 * get_aoShader_12() const { return ___aoShader_12; }
	inline Shader_t4151988712 ** get_address_of_aoShader_12() { return &___aoShader_12; }
	inline void set_aoShader_12(Shader_t4151988712 * value)
	{
		___aoShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___aoShader_12), value);
	}

	inline static int32_t get_offset_of_aoMaterial_13() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___aoMaterial_13)); }
	inline Material_t340375123 * get_aoMaterial_13() const { return ___aoMaterial_13; }
	inline Material_t340375123 ** get_address_of_aoMaterial_13() { return &___aoMaterial_13; }
	inline void set_aoMaterial_13(Material_t340375123 * value)
	{
		___aoMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___aoMaterial_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEAMBIENTOBSCURANCE_T1844081910_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (CEffect_t3687947224), -1, sizeof(CEffect_t3687947224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3300[7] = 
{
	CEffect_t3687947224::get_offset_of_m_fRotateSpeed_2(),
	CEffect_t3687947224::get_offset_of_m_fRotationZ_3(),
	CEffect_t3687947224_StaticFields::get_offset_of_vecTempScale_4(),
	CEffect_t3687947224_StaticFields::get_offset_of_vecTempPos_5(),
	CEffect_t3687947224::get_offset_of__sprMain_6(),
	CEffect_t3687947224::get_offset_of__imgMain_7(),
	CEffect_t3687947224::get_offset_of__sprWhite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (CEffectManager_t184376453), -1, sizeof(CEffectManager_t184376453_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3301[14] = 
{
	CEffectManager_t184376453_StaticFields::get_offset_of_s_Instance_2(),
	CEffectManager_t184376453::get_offset_of_m_arySkillEffect_QianYao_3(),
	CEffectManager_t184376453::get_offset_of_m_arySkillEffect_ChiXu_4(),
	CEffectManager_t184376453::get_offset_of_m_arySkillEffectScale_QianYao_5(),
	CEffectManager_t184376453::get_offset_of_m_arySkillEffectScale_ChiXu_6(),
	CEffectManager_t184376453::get_offset_of_m_dicRecycledSkillEffect_QianYao_7(),
	CEffectManager_t184376453::get_offset_of_m_dicRecycledSkillEffect_ChiXu_8(),
	CEffectManager_t184376453::get_offset_of_m_aryMainFightEffects_9(),
	CEffectManager_t184376453::get_offset_of_m_aryMainFightEffects_FrameInterval_10(),
	CEffectManager_t184376453::get_offset_of_m_aryMainFightEffects_Scale_11(),
	CEffectManager_t184376453::get_offset_of_m_nNewTimes_12(),
	CEffectManager_t184376453::get_offset_of_m_nGuid_13(),
	CEffectManager_t184376453::get_offset_of_m_nDelTimes_14(),
	CEffectManager_t184376453::get_offset_of_m_dicMainFightEffefcts_Recycled_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (eSkillEffectType_t1495465422)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3302[3] = 
{
	eSkillEffectType_t1495465422::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (eMainFightEffectType_t3875050507)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3303[3] = 
{
	eMainFightEffectType_t3875050507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (CFrameAnimationEffect_t443605508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[16] = 
{
	CFrameAnimationEffect_t443605508::get_offset_of_m_aryFrameSprites_17(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_fFrameInterval_18(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_fTimeCount_19(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bPlaying_20(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_nFrameIndex_21(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bLoop_22(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bReverse_23(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bCurDir_24(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_fLoopInterval_25(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bWaiting_26(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_fWaitingTimeCount_27(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bPlayAuto_28(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_nLoopTimes_29(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bHideWhenEnd_30(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_bDestroyWhenEnd_31(),
	CFrameAnimationEffect_t443605508::get_offset_of_m_nId_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (CItemBuyEffect_t833204468), -1, sizeof(CItemBuyEffect_t833204468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3305[10] = 
{
	CItemBuyEffect_t833204468::get_offset_of__effectMain_2(),
	0,
	0,
	CItemBuyEffect_t833204468_StaticFields::get_offset_of_vecTempScale_5(),
	CItemBuyEffect_t833204468_StaticFields::get_offset_of_vecTempPos_6(),
	CItemBuyEffect_t833204468::get_offset_of_m_fSpeed_7(),
	CItemBuyEffect_t833204468::get_offset_of_m_Dir_8(),
	CItemBuyEffect_t833204468::get_offset_of_m_vecDest_9(),
	CItemBuyEffect_t833204468::get_offset_of_m_fStartTime_10(),
	CItemBuyEffect_t833204468::get_offset_of__ballDest_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (CMiaoHeEffect_t834080748), -1, sizeof(CMiaoHeEffect_t834080748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3306[16] = 
{
	0,
	0,
	CMiaoHeEffect_t834080748_StaticFields::get_offset_of_vecTempScale_4(),
	CMiaoHeEffect_t834080748_StaticFields::get_offset_of_vecTempPos_5(),
	CMiaoHeEffect_t834080748_StaticFields::get_offset_of_colorTemp_6(),
	CMiaoHeEffect_t834080748::get_offset_of_m_fStartTime_7(),
	CMiaoHeEffect_t834080748::get_offset_of_m_vecMoveSpeed_8(),
	CMiaoHeEffect_t834080748::get_offset_of_m_Dir_9(),
	CMiaoHeEffect_t834080748::get_offset_of__srMain_10(),
	CMiaoHeEffect_t834080748::get_offset_of__srWhite_11(),
	CMiaoHeEffect_t834080748::get_offset_of_m_bStart_12(),
	CMiaoHeEffect_t834080748::get_offset_of_m_nStatus_13(),
	CMiaoHeEffect_t834080748::get_offset_of_m_fTimeLapse_14(),
	CMiaoHeEffect_t834080748::get_offset_of_m_fRandomDelayTime_15(),
	CMiaoHeEffect_t834080748::get_offset_of_m_vecDestPos_16(),
	CMiaoHeEffect_t834080748::get_offset_of__ballDest_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (AAMode_t1871701680)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3307[8] = 
{
	AAMode_t1871701680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (Antialiasing_t1691315015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[22] = 
{
	Antialiasing_t1691315015::get_offset_of_mode_6(),
	Antialiasing_t1691315015::get_offset_of_showGeneratedNormals_7(),
	Antialiasing_t1691315015::get_offset_of_offsetScale_8(),
	Antialiasing_t1691315015::get_offset_of_blurRadius_9(),
	Antialiasing_t1691315015::get_offset_of_edgeThresholdMin_10(),
	Antialiasing_t1691315015::get_offset_of_edgeThreshold_11(),
	Antialiasing_t1691315015::get_offset_of_edgeSharpness_12(),
	Antialiasing_t1691315015::get_offset_of_dlaaSharp_13(),
	Antialiasing_t1691315015::get_offset_of_ssaaShader_14(),
	Antialiasing_t1691315015::get_offset_of_ssaa_15(),
	Antialiasing_t1691315015::get_offset_of_dlaaShader_16(),
	Antialiasing_t1691315015::get_offset_of_dlaa_17(),
	Antialiasing_t1691315015::get_offset_of_nfaaShader_18(),
	Antialiasing_t1691315015::get_offset_of_nfaa_19(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAPreset2_20(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAPreset2_21(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAPreset3_22(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAPreset3_23(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAII_24(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAII_25(),
	Antialiasing_t1691315015::get_offset_of_shaderFXAAIII_26(),
	Antialiasing_t1691315015::get_offset_of_materialFXAAIII_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (Bloom_t1125654350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[30] = 
{
	Bloom_t1125654350::get_offset_of_tweakMode_6(),
	Bloom_t1125654350::get_offset_of_screenBlendMode_7(),
	Bloom_t1125654350::get_offset_of_hdr_8(),
	Bloom_t1125654350::get_offset_of_doHdr_9(),
	Bloom_t1125654350::get_offset_of_sepBlurSpread_10(),
	Bloom_t1125654350::get_offset_of_quality_11(),
	Bloom_t1125654350::get_offset_of_bloomIntensity_12(),
	Bloom_t1125654350::get_offset_of_bloomThreshold_13(),
	Bloom_t1125654350::get_offset_of_bloomThresholdColor_14(),
	Bloom_t1125654350::get_offset_of_bloomBlurIterations_15(),
	Bloom_t1125654350::get_offset_of_hollywoodFlareBlurIterations_16(),
	Bloom_t1125654350::get_offset_of_flareRotation_17(),
	Bloom_t1125654350::get_offset_of_lensflareMode_18(),
	Bloom_t1125654350::get_offset_of_hollyStretchWidth_19(),
	Bloom_t1125654350::get_offset_of_lensflareIntensity_20(),
	Bloom_t1125654350::get_offset_of_lensflareThreshold_21(),
	Bloom_t1125654350::get_offset_of_lensFlareSaturation_22(),
	Bloom_t1125654350::get_offset_of_flareColorA_23(),
	Bloom_t1125654350::get_offset_of_flareColorB_24(),
	Bloom_t1125654350::get_offset_of_flareColorC_25(),
	Bloom_t1125654350::get_offset_of_flareColorD_26(),
	Bloom_t1125654350::get_offset_of_lensFlareVignetteMask_27(),
	Bloom_t1125654350::get_offset_of_lensFlareShader_28(),
	Bloom_t1125654350::get_offset_of_lensFlareMaterial_29(),
	Bloom_t1125654350::get_offset_of_screenBlendShader_30(),
	Bloom_t1125654350::get_offset_of_screenBlend_31(),
	Bloom_t1125654350::get_offset_of_blurAndFlaresShader_32(),
	Bloom_t1125654350::get_offset_of_blurAndFlaresMaterial_33(),
	Bloom_t1125654350::get_offset_of_brightPassFilterShader_34(),
	Bloom_t1125654350::get_offset_of_brightPassFilterMaterial_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (LensFlareStyle_t630413071)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3310[4] = 
{
	LensFlareStyle_t630413071::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (TweakMode_t747557136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3311[3] = 
{
	TweakMode_t747557136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (HDRBloomMode_t3774419504)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3312[4] = 
{
	HDRBloomMode_t3774419504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (BloomScreenBlendMode_t2012607685)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3313[3] = 
{
	BloomScreenBlendMode_t2012607685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (BloomQuality_t3369172721)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3314[3] = 
{
	BloomQuality_t3369172721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (LensflareStyle34_t4260782719)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3315[4] = 
{
	LensflareStyle34_t4260782719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (TweakMode34_t984135990)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3316[3] = 
{
	TweakMode34_t984135990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (HDRBloomMode_t4271191419)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3317[4] = 
{
	HDRBloomMode_t4271191419::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (BloomScreenBlendMode_t19712272)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3318[3] = 
{
	BloomScreenBlendMode_t19712272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (BloomAndFlares_t2848767628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[34] = 
{
	BloomAndFlares_t2848767628::get_offset_of_tweakMode_6(),
	BloomAndFlares_t2848767628::get_offset_of_screenBlendMode_7(),
	BloomAndFlares_t2848767628::get_offset_of_hdr_8(),
	BloomAndFlares_t2848767628::get_offset_of_doHdr_9(),
	BloomAndFlares_t2848767628::get_offset_of_sepBlurSpread_10(),
	BloomAndFlares_t2848767628::get_offset_of_useSrcAlphaAsMask_11(),
	BloomAndFlares_t2848767628::get_offset_of_bloomIntensity_12(),
	BloomAndFlares_t2848767628::get_offset_of_bloomThreshold_13(),
	BloomAndFlares_t2848767628::get_offset_of_bloomBlurIterations_14(),
	BloomAndFlares_t2848767628::get_offset_of_lensflares_15(),
	BloomAndFlares_t2848767628::get_offset_of_hollywoodFlareBlurIterations_16(),
	BloomAndFlares_t2848767628::get_offset_of_lensflareMode_17(),
	BloomAndFlares_t2848767628::get_offset_of_hollyStretchWidth_18(),
	BloomAndFlares_t2848767628::get_offset_of_lensflareIntensity_19(),
	BloomAndFlares_t2848767628::get_offset_of_lensflareThreshold_20(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorA_21(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorB_22(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorC_23(),
	BloomAndFlares_t2848767628::get_offset_of_flareColorD_24(),
	BloomAndFlares_t2848767628::get_offset_of_lensFlareVignetteMask_25(),
	BloomAndFlares_t2848767628::get_offset_of_lensFlareShader_26(),
	BloomAndFlares_t2848767628::get_offset_of_lensFlareMaterial_27(),
	BloomAndFlares_t2848767628::get_offset_of_vignetteShader_28(),
	BloomAndFlares_t2848767628::get_offset_of_vignetteMaterial_29(),
	BloomAndFlares_t2848767628::get_offset_of_separableBlurShader_30(),
	BloomAndFlares_t2848767628::get_offset_of_separableBlurMaterial_31(),
	BloomAndFlares_t2848767628::get_offset_of_addBrightStuffOneOneShader_32(),
	BloomAndFlares_t2848767628::get_offset_of_addBrightStuffBlendOneOneMaterial_33(),
	BloomAndFlares_t2848767628::get_offset_of_screenBlendShader_34(),
	BloomAndFlares_t2848767628::get_offset_of_screenBlend_35(),
	BloomAndFlares_t2848767628::get_offset_of_hollywoodFlaresShader_36(),
	BloomAndFlares_t2848767628::get_offset_of_hollywoodFlaresMaterial_37(),
	BloomAndFlares_t2848767628::get_offset_of_brightPassFilterShader_38(),
	BloomAndFlares_t2848767628::get_offset_of_brightPassFilterMaterial_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (BloomOptimized_t2685819829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[8] = 
{
	BloomOptimized_t2685819829::get_offset_of_threshold_6(),
	BloomOptimized_t2685819829::get_offset_of_intensity_7(),
	BloomOptimized_t2685819829::get_offset_of_blurSize_8(),
	BloomOptimized_t2685819829::get_offset_of_resolution_9(),
	BloomOptimized_t2685819829::get_offset_of_blurIterations_10(),
	BloomOptimized_t2685819829::get_offset_of_blurType_11(),
	BloomOptimized_t2685819829::get_offset_of_fastBloomShader_12(),
	BloomOptimized_t2685819829::get_offset_of_fastBloomMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (Resolution_t1804605042)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3321[3] = 
{
	Resolution_t1804605042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (BlurType_t2416258039)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3322[3] = 
{
	BlurType_t2416258039::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (Blur_t1038294851), -1, sizeof(Blur_t1038294851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3323[4] = 
{
	Blur_t1038294851::get_offset_of_iterations_2(),
	Blur_t1038294851::get_offset_of_blurSpread_3(),
	Blur_t1038294851::get_offset_of_blurShader_4(),
	Blur_t1038294851_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (BlurOptimized_t3334654964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[6] = 
{
	BlurOptimized_t3334654964::get_offset_of_downsample_6(),
	BlurOptimized_t3334654964::get_offset_of_blurSize_7(),
	BlurOptimized_t3334654964::get_offset_of_blurIterations_8(),
	BlurOptimized_t3334654964::get_offset_of_blurType_9(),
	BlurOptimized_t3334654964::get_offset_of_blurShader_10(),
	BlurOptimized_t3334654964::get_offset_of_blurMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (BlurType_t1046251128)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3325[3] = 
{
	BlurType_t1046251128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (CameraMotionBlur_t2812046500), -1, sizeof(CameraMotionBlur_t2812046500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3326[32] = 
{
	CameraMotionBlur_t2812046500_StaticFields::get_offset_of_MAX_RADIUS_6(),
	CameraMotionBlur_t2812046500::get_offset_of_filterType_7(),
	CameraMotionBlur_t2812046500::get_offset_of_preview_8(),
	CameraMotionBlur_t2812046500::get_offset_of_previewScale_9(),
	CameraMotionBlur_t2812046500::get_offset_of_movementScale_10(),
	CameraMotionBlur_t2812046500::get_offset_of_rotationScale_11(),
	CameraMotionBlur_t2812046500::get_offset_of_maxVelocity_12(),
	CameraMotionBlur_t2812046500::get_offset_of_minVelocity_13(),
	CameraMotionBlur_t2812046500::get_offset_of_velocityScale_14(),
	CameraMotionBlur_t2812046500::get_offset_of_softZDistance_15(),
	CameraMotionBlur_t2812046500::get_offset_of_velocityDownsample_16(),
	CameraMotionBlur_t2812046500::get_offset_of_excludeLayers_17(),
	CameraMotionBlur_t2812046500::get_offset_of_tmpCam_18(),
	CameraMotionBlur_t2812046500::get_offset_of_shader_19(),
	CameraMotionBlur_t2812046500::get_offset_of_dx11MotionBlurShader_20(),
	CameraMotionBlur_t2812046500::get_offset_of_replacementClear_21(),
	CameraMotionBlur_t2812046500::get_offset_of_motionBlurMaterial_22(),
	CameraMotionBlur_t2812046500::get_offset_of_dx11MotionBlurMaterial_23(),
	CameraMotionBlur_t2812046500::get_offset_of_noiseTexture_24(),
	CameraMotionBlur_t2812046500::get_offset_of_jitter_25(),
	CameraMotionBlur_t2812046500::get_offset_of_showVelocity_26(),
	CameraMotionBlur_t2812046500::get_offset_of_showVelocityScale_27(),
	CameraMotionBlur_t2812046500::get_offset_of_currentViewProjMat_28(),
	CameraMotionBlur_t2812046500::get_offset_of_currentStereoViewProjMat_29(),
	CameraMotionBlur_t2812046500::get_offset_of_prevViewProjMat_30(),
	CameraMotionBlur_t2812046500::get_offset_of_prevStereoViewProjMat_31(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFrameCount_32(),
	CameraMotionBlur_t2812046500::get_offset_of_wasActive_33(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFrameForward_34(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFrameUp_35(),
	CameraMotionBlur_t2812046500::get_offset_of_prevFramePos_36(),
	CameraMotionBlur_t2812046500::get_offset_of__camera_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (MotionBlurFilter_t520253047)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3327[6] = 
{
	MotionBlurFilter_t520253047::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (ColorCorrectionCurves_t3742166504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3328[24] = 
{
	ColorCorrectionCurves_t3742166504::get_offset_of_redChannel_6(),
	ColorCorrectionCurves_t3742166504::get_offset_of_greenChannel_7(),
	ColorCorrectionCurves_t3742166504::get_offset_of_blueChannel_8(),
	ColorCorrectionCurves_t3742166504::get_offset_of_useDepthCorrection_9(),
	ColorCorrectionCurves_t3742166504::get_offset_of_zCurve_10(),
	ColorCorrectionCurves_t3742166504::get_offset_of_depthRedChannel_11(),
	ColorCorrectionCurves_t3742166504::get_offset_of_depthGreenChannel_12(),
	ColorCorrectionCurves_t3742166504::get_offset_of_depthBlueChannel_13(),
	ColorCorrectionCurves_t3742166504::get_offset_of_ccMaterial_14(),
	ColorCorrectionCurves_t3742166504::get_offset_of_ccDepthMaterial_15(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveCcMaterial_16(),
	ColorCorrectionCurves_t3742166504::get_offset_of_rgbChannelTex_17(),
	ColorCorrectionCurves_t3742166504::get_offset_of_rgbDepthChannelTex_18(),
	ColorCorrectionCurves_t3742166504::get_offset_of_zCurveTex_19(),
	ColorCorrectionCurves_t3742166504::get_offset_of_saturation_20(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveCc_21(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveFromColor_22(),
	ColorCorrectionCurves_t3742166504::get_offset_of_selectiveToColor_23(),
	ColorCorrectionCurves_t3742166504::get_offset_of_mode_24(),
	ColorCorrectionCurves_t3742166504::get_offset_of_updateTextures_25(),
	ColorCorrectionCurves_t3742166504::get_offset_of_colorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t3742166504::get_offset_of_simpleColorCorrectionCurvesShader_27(),
	ColorCorrectionCurves_t3742166504::get_offset_of_colorCorrectionSelectiveShader_28(),
	ColorCorrectionCurves_t3742166504::get_offset_of_updateTexturesOnStartup_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (ColorCorrectionMode_t1416458051)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3329[3] = 
{
	ColorCorrectionMode_t1416458051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (ColorCorrectionLookup_t1159177774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[4] = 
{
	ColorCorrectionLookup_t1159177774::get_offset_of_shader_6(),
	ColorCorrectionLookup_t1159177774::get_offset_of_material_7(),
	ColorCorrectionLookup_t1159177774::get_offset_of_converted3DLut_8(),
	ColorCorrectionLookup_t1159177774::get_offset_of_basedOnTempTex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (ColorCorrectionRamp_t3562116199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[1] = 
{
	ColorCorrectionRamp_t3562116199::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (ContrastEnhance_t640919481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3332[7] = 
{
	ContrastEnhance_t640919481::get_offset_of_intensity_6(),
	ContrastEnhance_t640919481::get_offset_of_threshold_7(),
	ContrastEnhance_t640919481::get_offset_of_separableBlurMaterial_8(),
	ContrastEnhance_t640919481::get_offset_of_contrastCompositeMaterial_9(),
	ContrastEnhance_t640919481::get_offset_of_blurSpread_10(),
	ContrastEnhance_t640919481::get_offset_of_separableBlurShader_11(),
	ContrastEnhance_t640919481::get_offset_of_contrastCompositeShader_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (ContrastStretch_t3424449263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[13] = 
{
	ContrastStretch_t3424449263::get_offset_of_adaptationSpeed_2(),
	ContrastStretch_t3424449263::get_offset_of_limitMinimum_3(),
	ContrastStretch_t3424449263::get_offset_of_limitMaximum_4(),
	ContrastStretch_t3424449263::get_offset_of_adaptRenderTex_5(),
	ContrastStretch_t3424449263::get_offset_of_curAdaptIndex_6(),
	ContrastStretch_t3424449263::get_offset_of_shaderLum_7(),
	ContrastStretch_t3424449263::get_offset_of_m_materialLum_8(),
	ContrastStretch_t3424449263::get_offset_of_shaderReduce_9(),
	ContrastStretch_t3424449263::get_offset_of_m_materialReduce_10(),
	ContrastStretch_t3424449263::get_offset_of_shaderAdapt_11(),
	ContrastStretch_t3424449263::get_offset_of_m_materialAdapt_12(),
	ContrastStretch_t3424449263::get_offset_of_shaderApply_13(),
	ContrastStretch_t3424449263::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (CreaseShading_t1200394124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[9] = 
{
	CreaseShading_t1200394124::get_offset_of_intensity_6(),
	CreaseShading_t1200394124::get_offset_of_softness_7(),
	CreaseShading_t1200394124::get_offset_of_spread_8(),
	CreaseShading_t1200394124::get_offset_of_blurShader_9(),
	CreaseShading_t1200394124::get_offset_of_blurMaterial_10(),
	CreaseShading_t1200394124::get_offset_of_depthFetchShader_11(),
	CreaseShading_t1200394124::get_offset_of_depthFetchMaterial_12(),
	CreaseShading_t1200394124::get_offset_of_creaseApplyShader_13(),
	CreaseShading_t1200394124::get_offset_of_creaseApplyMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (DepthOfField_t1116783936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[25] = 
{
	DepthOfField_t1116783936::get_offset_of_visualizeFocus_6(),
	DepthOfField_t1116783936::get_offset_of_focalLength_7(),
	DepthOfField_t1116783936::get_offset_of_focalSize_8(),
	DepthOfField_t1116783936::get_offset_of_aperture_9(),
	DepthOfField_t1116783936::get_offset_of_focalTransform_10(),
	DepthOfField_t1116783936::get_offset_of_maxBlurSize_11(),
	DepthOfField_t1116783936::get_offset_of_highResolution_12(),
	DepthOfField_t1116783936::get_offset_of_blurType_13(),
	DepthOfField_t1116783936::get_offset_of_blurSampleCount_14(),
	DepthOfField_t1116783936::get_offset_of_nearBlur_15(),
	DepthOfField_t1116783936::get_offset_of_foregroundOverlap_16(),
	DepthOfField_t1116783936::get_offset_of_dofHdrShader_17(),
	DepthOfField_t1116783936::get_offset_of_dofHdrMaterial_18(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehShader_19(),
	DepthOfField_t1116783936::get_offset_of_dx11bokehMaterial_20(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehThreshold_21(),
	DepthOfField_t1116783936::get_offset_of_dx11SpawnHeuristic_22(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehTexture_23(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehScale_24(),
	DepthOfField_t1116783936::get_offset_of_dx11BokehIntensity_25(),
	DepthOfField_t1116783936::get_offset_of_focalDistance01_26(),
	DepthOfField_t1116783936::get_offset_of_cbDrawArgs_27(),
	DepthOfField_t1116783936::get_offset_of_cbPoints_28(),
	DepthOfField_t1116783936::get_offset_of_internalBlurWidth_29(),
	DepthOfField_t1116783936::get_offset_of_cachedCamera_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (BlurType_t3871645803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3336[3] = 
{
	BlurType_t3871645803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (BlurSampleCount_t3210294001)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3337[4] = 
{
	BlurSampleCount_t3210294001::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (DepthOfFieldDeprecated_t4187663194), -1, sizeof(DepthOfFieldDeprecated_t4187663194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3338[43] = 
{
	DepthOfFieldDeprecated_t4187663194_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_6(),
	DepthOfFieldDeprecated_t4187663194_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_7(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_quality_8(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_resolution_9(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_simpleTweakMode_10(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalPoint_11(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_smoothness_12(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalZDistance_13(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalZStartCurve_14(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalZEndCurve_15(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalStartCurve_16(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalEndCurve_17(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalDistance01_18(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_objectFocus_19(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_focalSize_20(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bluriness_21(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_maxBlurSpread_22(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_foregroundBlurExtrude_23(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofBlurShader_24(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofBlurMaterial_25(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofShader_26(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_dofMaterial_27(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_visualize_28(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehDestination_29(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_widthOverHeight_30(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_oneOverBaseSize_31(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokeh_32(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehSupport_33(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehShader_34(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehTexture_35(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehScale_36(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehIntensity_37(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehThresholdContrast_38(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehThresholdLuminance_39(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehDownsample_40(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehMaterial_41(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of__camera_42(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_foregroundTexture_43(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_mediumRezWorkTexture_44(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_finalDefocus_45(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_lowRezWorkTexture_46(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehSource_47(),
	DepthOfFieldDeprecated_t4187663194::get_offset_of_bokehSource2_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (Dof34QualitySetting_t3636551379)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3339[3] = 
{
	Dof34QualitySetting_t3636551379::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (DofResolution_t1566655669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3340[4] = 
{
	DofResolution_t1566655669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (DofBlurriness_t473098480)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3341[4] = 
{
	DofBlurriness_t473098480::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (BokehDestination_t1233703462)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3342[4] = 
{
	BokehDestination_t1233703462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (EdgeDetection_t506487406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[11] = 
{
	EdgeDetection_t506487406::get_offset_of_mode_6(),
	EdgeDetection_t506487406::get_offset_of_sensitivityDepth_7(),
	EdgeDetection_t506487406::get_offset_of_sensitivityNormals_8(),
	EdgeDetection_t506487406::get_offset_of_lumThreshold_9(),
	EdgeDetection_t506487406::get_offset_of_edgeExp_10(),
	EdgeDetection_t506487406::get_offset_of_sampleDist_11(),
	EdgeDetection_t506487406::get_offset_of_edgesOnly_12(),
	EdgeDetection_t506487406::get_offset_of_edgesOnlyBgColor_13(),
	EdgeDetection_t506487406::get_offset_of_edgeDetectShader_14(),
	EdgeDetection_t506487406::get_offset_of_edgeDetectMaterial_15(),
	EdgeDetection_t506487406::get_offset_of_oldMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (EdgeDetectMode_t1984240676)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3344[6] = 
{
	EdgeDetectMode_t1984240676::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (Fisheye_t4101461743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[4] = 
{
	Fisheye_t4101461743::get_offset_of_strengthX_6(),
	Fisheye_t4101461743::get_offset_of_strengthY_7(),
	Fisheye_t4101461743::get_offset_of_fishEyeShader_8(),
	Fisheye_t4101461743::get_offset_of_fisheyeMaterial_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (GlobalFog_t900542613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[9] = 
{
	GlobalFog_t900542613::get_offset_of_distanceFog_6(),
	GlobalFog_t900542613::get_offset_of_excludeFarPixels_7(),
	GlobalFog_t900542613::get_offset_of_useRadialDistance_8(),
	GlobalFog_t900542613::get_offset_of_heightFog_9(),
	GlobalFog_t900542613::get_offset_of_height_10(),
	GlobalFog_t900542613::get_offset_of_heightDensity_11(),
	GlobalFog_t900542613::get_offset_of_startDistance_12(),
	GlobalFog_t900542613::get_offset_of_fogShader_13(),
	GlobalFog_t900542613::get_offset_of_fogMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (Grayscale_t1707485390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[2] = 
{
	Grayscale_t1707485390::get_offset_of_textureRamp_4(),
	Grayscale_t1707485390::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (ImageEffectBase_t2026006575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[2] = 
{
	ImageEffectBase_t2026006575::get_offset_of_shader_2(),
	ImageEffectBase_t2026006575::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (ImageEffects_t1214077586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (MotionBlur_t1587267364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[3] = 
{
	MotionBlur_t1587267364::get_offset_of_blurAmount_4(),
	MotionBlur_t1587267364::get_offset_of_extraBlur_5(),
	MotionBlur_t1587267364::get_offset_of_accumTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (NoiseAndGrain_t3814230817), -1, sizeof(NoiseAndGrain_t3814230817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3351[18] = 
{
	NoiseAndGrain_t3814230817::get_offset_of_intensityMultiplier_6(),
	NoiseAndGrain_t3814230817::get_offset_of_generalIntensity_7(),
	NoiseAndGrain_t3814230817::get_offset_of_blackIntensity_8(),
	NoiseAndGrain_t3814230817::get_offset_of_whiteIntensity_9(),
	NoiseAndGrain_t3814230817::get_offset_of_midGrey_10(),
	NoiseAndGrain_t3814230817::get_offset_of_dx11Grain_11(),
	NoiseAndGrain_t3814230817::get_offset_of_softness_12(),
	NoiseAndGrain_t3814230817::get_offset_of_monochrome_13(),
	NoiseAndGrain_t3814230817::get_offset_of_intensities_14(),
	NoiseAndGrain_t3814230817::get_offset_of_tiling_15(),
	NoiseAndGrain_t3814230817::get_offset_of_monochromeTiling_16(),
	NoiseAndGrain_t3814230817::get_offset_of_filterMode_17(),
	NoiseAndGrain_t3814230817::get_offset_of_noiseTexture_18(),
	NoiseAndGrain_t3814230817::get_offset_of_noiseShader_19(),
	NoiseAndGrain_t3814230817::get_offset_of_noiseMaterial_20(),
	NoiseAndGrain_t3814230817::get_offset_of_dx11NoiseShader_21(),
	NoiseAndGrain_t3814230817::get_offset_of_dx11NoiseMaterial_22(),
	NoiseAndGrain_t3814230817_StaticFields::get_offset_of_TILE_AMOUNT_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (NoiseAndScratches_t1457296845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[18] = 
{
	NoiseAndScratches_t1457296845::get_offset_of_monochrome_2(),
	NoiseAndScratches_t1457296845::get_offset_of_rgbFallback_3(),
	NoiseAndScratches_t1457296845::get_offset_of_grainIntensityMin_4(),
	NoiseAndScratches_t1457296845::get_offset_of_grainIntensityMax_5(),
	NoiseAndScratches_t1457296845::get_offset_of_grainSize_6(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchIntensityMin_7(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchIntensityMax_8(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchFPS_9(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchJitter_10(),
	NoiseAndScratches_t1457296845::get_offset_of_grainTexture_11(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchTexture_12(),
	NoiseAndScratches_t1457296845::get_offset_of_shaderRGB_13(),
	NoiseAndScratches_t1457296845::get_offset_of_shaderYUV_14(),
	NoiseAndScratches_t1457296845::get_offset_of_m_MaterialRGB_15(),
	NoiseAndScratches_t1457296845::get_offset_of_m_MaterialYUV_16(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchTimeLeft_17(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchX_18(),
	NoiseAndScratches_t1457296845::get_offset_of_scratchY_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (PostEffectsBase_t2404315739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[4] = 
{
	PostEffectsBase_t2404315739::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t2404315739::get_offset_of_supportDX11_3(),
	PostEffectsBase_t2404315739::get_offset_of_isSupported_4(),
	PostEffectsBase_t2404315739::get_offset_of_createdMaterials_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (PostEffectsHelper_t675066462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (Quads_t1152577304), -1, sizeof(Quads_t1152577304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3355[2] = 
{
	Quads_t1152577304_StaticFields::get_offset_of_meshes_0(),
	Quads_t1152577304_StaticFields::get_offset_of_currentQuads_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (ScreenOverlay_t3772274400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[5] = 
{
	ScreenOverlay_t3772274400::get_offset_of_blendMode_6(),
	ScreenOverlay_t3772274400::get_offset_of_intensity_7(),
	ScreenOverlay_t3772274400::get_offset_of_texture_8(),
	ScreenOverlay_t3772274400::get_offset_of_overlayShader_9(),
	ScreenOverlay_t3772274400::get_offset_of_overlayMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (OverlayBlendMode_t429753458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3357[6] = 
{
	OverlayBlendMode_t429753458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (ScreenSpaceAmbientObscurance_t1844081910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[8] = 
{
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_intensity_6(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_radius_7(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_blurIterations_8(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_blurFilterDistance_9(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_downsample_10(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_rand_11(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_aoShader_12(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_aoMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (ScreenSpaceAmbientOcclusion_t1675618705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[11] = 
{
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Radius_2(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_SampleCount_3(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_OcclusionIntensity_4(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Blur_5(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Downsampling_6(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_OcclusionAttenuation_7(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_MinZ_8(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_SSAOShader_9(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_SSAOMaterial_10(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_RandomTexture_11(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Supported_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (SSAOSamples_t2619211009)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3360[4] = 
{
	SSAOSamples_t2619211009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (SepiaTone_t4259761740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (SunShafts_t2328921421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[14] = 
{
	SunShafts_t2328921421::get_offset_of_resolution_6(),
	SunShafts_t2328921421::get_offset_of_screenBlendMode_7(),
	SunShafts_t2328921421::get_offset_of_sunTransform_8(),
	SunShafts_t2328921421::get_offset_of_radialBlurIterations_9(),
	SunShafts_t2328921421::get_offset_of_sunColor_10(),
	SunShafts_t2328921421::get_offset_of_sunThreshold_11(),
	SunShafts_t2328921421::get_offset_of_sunShaftBlurRadius_12(),
	SunShafts_t2328921421::get_offset_of_sunShaftIntensity_13(),
	SunShafts_t2328921421::get_offset_of_maxRadius_14(),
	SunShafts_t2328921421::get_offset_of_useDepthTexture_15(),
	SunShafts_t2328921421::get_offset_of_sunShaftsShader_16(),
	SunShafts_t2328921421::get_offset_of_sunShaftsMaterial_17(),
	SunShafts_t2328921421::get_offset_of_simpleClearShader_18(),
	SunShafts_t2328921421::get_offset_of_simpleClearMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (SunShaftsResolution_t3826757637)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3363[4] = 
{
	SunShaftsResolution_t3826757637::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (ShaftsScreenBlendMode_t4165054462)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3364[3] = 
{
	ShaftsScreenBlendMode_t4165054462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (TiltShift_t2891301701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[7] = 
{
	TiltShift_t2891301701::get_offset_of_mode_6(),
	TiltShift_t2891301701::get_offset_of_quality_7(),
	TiltShift_t2891301701::get_offset_of_blurArea_8(),
	TiltShift_t2891301701::get_offset_of_maxBlurSize_9(),
	TiltShift_t2891301701::get_offset_of_downsample_10(),
	TiltShift_t2891301701::get_offset_of_tiltShiftShader_11(),
	TiltShift_t2891301701::get_offset_of_tiltShiftMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (TiltShiftMode_t1375727185)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3366[3] = 
{
	TiltShiftMode_t1375727185::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (TiltShiftQuality_t4173331534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3367[5] = 
{
	TiltShiftQuality_t4173331534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (Tonemapping_t2837480251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[13] = 
{
	Tonemapping_t2837480251::get_offset_of_type_6(),
	Tonemapping_t2837480251::get_offset_of_adaptiveTextureSize_7(),
	Tonemapping_t2837480251::get_offset_of_remapCurve_8(),
	Tonemapping_t2837480251::get_offset_of_curveTex_9(),
	Tonemapping_t2837480251::get_offset_of_exposureAdjustment_10(),
	Tonemapping_t2837480251::get_offset_of_middleGrey_11(),
	Tonemapping_t2837480251::get_offset_of_white_12(),
	Tonemapping_t2837480251::get_offset_of_adaptionSpeed_13(),
	Tonemapping_t2837480251::get_offset_of_tonemapper_14(),
	Tonemapping_t2837480251::get_offset_of_validRenderTextureFormat_15(),
	Tonemapping_t2837480251::get_offset_of_tonemapMaterial_16(),
	Tonemapping_t2837480251::get_offset_of_rt_17(),
	Tonemapping_t2837480251::get_offset_of_rtFormat_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (TonemapperType_t52991894)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3369[8] = 
{
	TonemapperType_t52991894::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (AdaptiveTexSize_t1062941056)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3370[8] = 
{
	AdaptiveTexSize_t1062941056::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (Triangles_t2090031681), -1, sizeof(Triangles_t2090031681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3371[2] = 
{
	Triangles_t2090031681_StaticFields::get_offset_of_meshes_0(),
	Triangles_t2090031681_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (Twirl_t2760508880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[3] = 
{
	Twirl_t2760508880::get_offset_of_radius_4(),
	Twirl_t2760508880::get_offset_of_angle_5(),
	Twirl_t2760508880::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (VignetteAndChromaticAberration_t3308099924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[14] = 
{
	VignetteAndChromaticAberration_t3308099924::get_offset_of_mode_6(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_intensity_7(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_chromaticAberration_8(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_axialAberration_9(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_blur_10(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_blurSpread_11(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_luminanceDependency_12(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_blurDistance_13(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_vignetteShader_14(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_separableBlurShader_15(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_chromAberrationShader_16(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_m_VignetteMaterial_17(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_m_SeparableBlurMaterial_18(),
	VignetteAndChromaticAberration_t3308099924::get_offset_of_m_ChromAberrationMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (AberrationMode_t218549536)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3374[3] = 
{
	AberrationMode_t218549536::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (Vortex_t3420399868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[3] = 
{
	Vortex_t3420399868::get_offset_of_radius_4(),
	Vortex_t3420399868::get_offset_of_angle_5(),
	Vortex_t3420399868::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (CShakeCamera_t3305427617), -1, sizeof(CShakeCamera_t3305427617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3376[11] = 
{
	CShakeCamera_t3305427617_StaticFields::get_offset_of_s_Instance_2(),
	CShakeCamera_t3305427617::get_offset_of_isshakeCamera_3(),
	CShakeCamera_t3305427617::get_offset_of_shakeLevel_4(),
	CShakeCamera_t3305427617::get_offset_of_setShakeTime_5(),
	CShakeCamera_t3305427617::get_offset_of_shakeFps_6(),
	CShakeCamera_t3305427617::get_offset_of_fps_7(),
	CShakeCamera_t3305427617::get_offset_of_shakeTime_8(),
	CShakeCamera_t3305427617::get_offset_of_frameTime_9(),
	CShakeCamera_t3305427617::get_offset_of_shakeDelta_10(),
	CShakeCamera_t3305427617::get_offset_of_selfCamera_11(),
	CShakeCamera_t3305427617::get_offset_of_changeRect_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (CEffectGoldShell_t2112892863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (CExplodeEvent_t2581699128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (CGameOverManager_t3022133880), -1, sizeof(CGameOverManager_t3022133880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3379[38] = 
{
	CGameOverManager_t3022133880_StaticFields::get_offset_of_vecTempPos_2(),
	CGameOverManager_t3022133880_StaticFields::get_offset_of_vecTempScale_3(),
	CGameOverManager_t3022133880_StaticFields::get_offset_of_s_Instance_4(),
	CGameOverManager_t3022133880::get_offset_of__goPanel_5(),
	CGameOverManager_t3022133880::get_offset_of_m_fCountingBorderMinFill_6(),
	CGameOverManager_t3022133880::get_offset_of_m_fCountingBorderMaxFill_7(),
	CGameOverManager_t3022133880::get_offset_of_m_fFillSpeed_8(),
	CGameOverManager_t3022133880::get_offset_of_m_fRankTextMaxScale_9(),
	CGameOverManager_t3022133880::get_offset_of_m_fRankTextMinScale_10(),
	CGameOverManager_t3022133880::get_offset_of_m_fRankTextAnimationTime_11(),
	CGameOverManager_t3022133880::get_offset_of_m_bRankTextAnimation_12(),
	CGameOverManager_t3022133880::get_offset_of_m_fRankTextAnimationScaleV_13(),
	CGameOverManager_t3022133880::get_offset_of__goTextRank_14(),
	CGameOverManager_t3022133880::get_offset_of__goTextRank_CanYing_15(),
	CGameOverManager_t3022133880::get_offset_of__goTextTitle_16(),
	CGameOverManager_t3022133880::get_offset_of__goTextTitle_CanYing_17(),
	CGameOverManager_t3022133880::get_offset_of_m_fOneTuoWeiTotalTime_18(),
	CGameOverManager_t3022133880::get_offset_of_m_fOneTuoWeiTotalTimeMiddle_19(),
	CGameOverManager_t3022133880::get_offset_of__txtRank_20(),
	CGameOverManager_t3022133880::get_offset_of__txtRank_CanYing_21(),
	CGameOverManager_t3022133880::get_offset_of__containerRank_22(),
	CGameOverManager_t3022133880_StaticFields::get_offset_of_s_bNowGameOvering_23(),
	0,
	CGameOverManager_t3022133880::get_offset_of_m_fTimeElpase_25(),
	CGameOverManager_t3022133880::get_offset_of_m_aryTuoWei_26(),
	CGameOverManager_t3022133880::get_offset_of__panelCounting_27(),
	CGameOverManager_t3022133880::get_offset_of__txtCounting_28(),
	CGameOverManager_t3022133880::get_offset_of__imgCountingBorder_29(),
	CGameOverManager_t3022133880::get_offset_of_m_arySubPanels_30(),
	CGameOverManager_t3022133880::get_offset_of_m_nTuoWeiStatus_31(),
	CGameOverManager_t3022133880::get_offset_of_m_fTuoWeiTimeElapse_32(),
	CGameOverManager_t3022133880::get_offset_of_m_bRankFirstFrame_33(),
	CGameOverManager_t3022133880::get_offset_of_m_bTitleFirstFrame_34(),
	CGameOverManager_t3022133880::get_offset_of_m_nFrameCount_35(),
	CGameOverManager_t3022133880::get_offset_of_m_bRankAnimationStarted_36(),
	CGameOverManager_t3022133880::get_offset_of_m_bTitleAnimationStarted_37(),
	CGameOverManager_t3022133880::get_offset_of_m_fCurFillAmount_38(),
	CGameOverManager_t3022133880::get_offset_of_m_nCountingBorderStatus_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (eGameOverSubPanelType_t2225654706)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3380[4] = 
{
	eGameOverSubPanelType_t2225654706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (U3CLoadSceneU3Ec__Iterator0_t2118664053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[4] = 
{
	U3CLoadSceneU3Ec__Iterator0_t2118664053::get_offset_of_scene_name_0(),
	U3CLoadSceneU3Ec__Iterator0_t2118664053::get_offset_of_U24current_1(),
	U3CLoadSceneU3Ec__Iterator0_t2118664053::get_offset_of_U24disposing_2(),
	U3CLoadSceneU3Ec__Iterator0_t2118664053::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (CGesture_t2181168874), -1, sizeof(CGesture_t2181168874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3382[6] = 
{
	CGesture_t2181168874_StaticFields::get_offset_of_vecTempScale_2(),
	CGesture_t2181168874::get_offset_of_m_bUI_3(),
	CGesture_t2181168874::get_offset_of_m_sprMain_4(),
	CGesture_t2181168874::get_offset_of_m_imgMain_5(),
	CGesture_t2181168874::get_offset_of_m_btnMain_6(),
	CGesture_t2181168874::get_offset_of_m_nId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (CGestureManager_t1993322746), -1, sizeof(CGestureManager_t1993322746_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3383[56] = 
{
	CGestureManager_t1993322746_StaticFields::get_offset_of_s_Instance_2(),
	CGestureManager_t1993322746_StaticFields::get_offset_of_vecTempPos_3(),
	CGestureManager_t1993322746_StaticFields::get_offset_of_vecTempScale_4(),
	CGestureManager_t1993322746::get_offset_of_m_nAvailableNum_5(),
	CGestureManager_t1993322746::get_offset_of_m_sprToggleButtongImg_NotOpen_6(),
	CGestureManager_t1993322746::get_offset_of_m_sprToggleButtongImg_Open_7(),
	CGestureManager_t1993322746::get_offset_of_m_aryGestures_8(),
	CGestureManager_t1993322746::get_offset_of_m_sprTerminalWithTail_9(),
	CGestureManager_t1993322746::get_offset_of_m_sprTerminalWithoutTail_10(),
	CGestureManager_t1993322746::get_offset_of__btnToggle_11(),
	CGestureManager_t1993322746::get_offset_of__inputGestureNumOneTime_12(),
	CGestureManager_t1993322746::get_offset_of__inputColdDown_13(),
	CGestureManager_t1993322746::get_offset_of__inputLastTime_14(),
	CGestureManager_t1993322746::get_offset_of__inputMaxEquippedNum_15(),
	CGestureManager_t1993322746::get_offset_of__inputSmallColdDown_16(),
	CGestureManager_t1993322746::get_offset_of__imgColdDown_17(),
	CGestureManager_t1993322746::get_offset_of__imgColdDownBg_18(),
	CGestureManager_t1993322746::get_offset_of_m_panelGesturesSystem_19(),
	CGestureManager_t1993322746::get_offset_of_m_goContainerBoughtGestures_20(),
	CGestureManager_t1993322746::get_offset_of_m_goContainerSelectedGestures_21(),
	CGestureManager_t1993322746::get_offset_of_m_goContainerPopo_22(),
	CGestureManager_t1993322746::get_offset_of_m_preGesture_23(),
	CGestureManager_t1993322746::get_offset_of_m_preGestureNotUI_24(),
	CGestureManager_t1993322746::get_offset_of_m_preGesturePopo_25(),
	CGestureManager_t1993322746::get_offset_of_m_preGesturePopoCenter_26(),
	CGestureManager_t1993322746::get_offset_of_m_fBoughtGesturesInterval_27(),
	CGestureManager_t1993322746::get_offset_of_m_fSelectedGesturesInterval_28(),
	CGestureManager_t1993322746::get_offset_of_m_fCastedGesturesInterval_29(),
	CGestureManager_t1993322746::get_offset_of_m_fBoughtGesturesScale_30(),
	CGestureManager_t1993322746::get_offset_of_m_fSelectedGesturesScale_31(),
	CGestureManager_t1993322746::get_offset_of_m_fCastedGesturesScale_32(),
	CGestureManager_t1993322746::get_offset_of_m_fUnitWidth_33(),
	CGestureManager_t1993322746::get_offset_of_m_fBgOffsetY_34(),
	CGestureManager_t1993322746::get_offset_of_m_fWidthPerCharaqcter_35(),
	CGestureManager_t1993322746::get_offset_of_m_fGenstureOriginalWidth_36(),
	CGestureManager_t1993322746::get_offset_of_m_fLeftAndRightOffset_37(),
	CGestureManager_t1993322746::get_offset_of_m_nMaxSelectedNum_38(),
	CGestureManager_t1993322746::get_offset_of_m_fColdDown_39(),
	CGestureManager_t1993322746::get_offset_of_m_fSmallColdDown_40(),
	CGestureManager_t1993322746::get_offset_of_m_fPopoLastTime_41(),
	CGestureManager_t1993322746::get_offset_of_m_fLastCastGestureTime_42(),
	CGestureManager_t1993322746::get_offset_of_m_fOffestX_43(),
	CGestureManager_t1993322746::get_offset_of_m_fOffestY_44(),
	CGestureManager_t1993322746::get_offset_of_m_lstSelcted_45(),
	CGestureManager_t1993322746::get_offset_of_m_eDir_46(),
	CGestureManager_t1993322746::get_offset_of_m_lstRecycledGestures_47(),
	CGestureManager_t1993322746::get_offset_of_m_lstRecycledGesturePopos_48(),
	CGestureManager_t1993322746::get_offset_of_m_lstRecycledPopoCenter_49(),
	CGestureManager_t1993322746::get_offset_of_m_bShowing_50(),
	CGestureManager_t1993322746::get_offset_of_m_fSmallColdDownTimeLeft_51(),
	CGestureManager_t1993322746::get_offset_of_m_fBigColdDownTimeLeft_52(),
	CGestureManager_t1993322746::get_offset_of_s_lstCastingGestureId_53(),
	CGestureManager_t1993322746::get_offset_of__testPopo_54(),
	CGestureManager_t1993322746::get_offset_of_m_goBoughtListLeft_55(),
	CGestureManager_t1993322746::get_offset_of_m_goBoughtListMain_56(),
	CGestureManager_t1993322746::get_offset_of_m_fBoughtUnitWidht_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (eGesturePopoDir_t4294713869)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3384[5] = 
{
	eGesturePopoDir_t4294713869::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (CGesturePopo_t2360635198), -1, sizeof(CGesturePopo_t2360635198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3385[24] = 
{
	CGesturePopo_t2360635198_StaticFields::get_offset_of_vecTempPos_2(),
	CGesturePopo_t2360635198_StaticFields::get_offset_of_vecTempScale_3(),
	CGesturePopo_t2360635198_StaticFields::get_offset_of_vecTempDir_4(),
	CGesturePopo_t2360635198::get_offset_of_m_nDir_5(),
	CGesturePopo_t2360635198::get_offset_of__sprLeft_6(),
	CGesturePopo_t2360635198::get_offset_of__sprRight_7(),
	CGesturePopo_t2360635198::get_offset_of_m_goLeft_8(),
	CGesturePopo_t2360635198::get_offset_of_m_goRighjt_9(),
	CGesturePopo_t2360635198::get_offset_of_m_goCenter_10(),
	CGesturePopo_t2360635198::get_offset_of_m_goGesturesContianer_11(),
	CGesturePopo_t2360635198::get_offset_of_m_goBgContainer_12(),
	CGesturePopo_t2360635198::get_offset_of_m_lstCenterSeg_13(),
	CGesturePopo_t2360635198::get_offset_of_m_lstGestures_14(),
	CGesturePopo_t2360635198::get_offset_of_m_fTimeElapse_15(),
	CGesturePopo_t2360635198::get_offset_of_m_bShowing_16(),
	CGesturePopo_t2360635198::get_offset_of__txtPlayerName_17(),
	CGesturePopo_t2360635198::get_offset_of_m_vecDir_18(),
	CGesturePopo_t2360635198::get_offset_of_m_eDir_19(),
	CGesturePopo_t2360635198::get_offset_of_m_fTotalWidth_20(),
	0,
	CGesturePopo_t2360635198::get_offset_of_m_fOffestX_22(),
	CGesturePopo_t2360635198::get_offset_of_m_fOffestY_23(),
	CGesturePopo_t2360635198::get_offset_of_m_fCurScale_24(),
	CGesturePopo_t2360635198::get_offset_of_m_Ball_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (CGMSystem_t2710125162), -1, sizeof(CGMSystem_t2710125162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3386[2] = 
{
	CGMSystem_t2710125162_StaticFields::get_offset_of_s_Instance_2(),
	CGMSystem_t2710125162_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (CGrowSystem_t4101788339), -1, sizeof(CGrowSystem_t4101788339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3387[78] = 
{
	CGrowSystem_t4101788339::get_offset_of__effectLevelUpNumber_2(),
	CGrowSystem_t4101788339_StaticFields::get_offset_of_vecTempPos_3(),
	CGrowSystem_t4101788339_StaticFields::get_offset_of_s_Instance_4(),
	0,
	CGrowSystem_t4101788339::get_offset_of__dropDownLevel_6(),
	CGrowSystem_t4101788339::get_offset_of__inputNeedExp_7(),
	CGrowSystem_t4101788339::get_offset_of__inputMP_8(),
	CGrowSystem_t4101788339::get_offset_of__inputMPRecoverSpeed_9(),
	CGrowSystem_t4101788339::get_offset_of__inputKillGainExp_10(),
	CGrowSystem_t4101788339::get_offset_of__inputKillGainMoney_11(),
	CGrowSystem_t4101788339::get_offset_of__inputBaseVolume_12(),
	CGrowSystem_t4101788339::get_offset_of__inputLevelMoney_13(),
	CGrowSystem_t4101788339::get_offset_of__inputDeadWaitingTime_14(),
	CGrowSystem_t4101788339::get_offset_of__imgMp_15(),
	CGrowSystem_t4101788339::get_offset_of__imgMp_PC_16(),
	CGrowSystem_t4101788339::get_offset_of__imgMp_MOBILE_17(),
	CGrowSystem_t4101788339::get_offset_of__txtMp_18(),
	CGrowSystem_t4101788339::get_offset_of__txtMp_PC_19(),
	CGrowSystem_t4101788339::get_offset_of__txtMp_MOBILE_20(),
	CGrowSystem_t4101788339::get_offset_of__txtLevel_21(),
	CGrowSystem_t4101788339::get_offset_of__txtExp_22(),
	CGrowSystem_t4101788339::get_offset_of__txtNextLevelExp_23(),
	CGrowSystem_t4101788339::get_offset_of__imgExpPercent_24(),
	CGrowSystem_t4101788339::get_offset_of__txtMyRank_25(),
	CGrowSystem_t4101788339::get_offset_of__txtTotalPlayerNum_26(),
	CGrowSystem_t4101788339::get_offset_of__txtMyRank_PC_27(),
	CGrowSystem_t4101788339::get_offset_of__txtMyRank_MOBILE_28(),
	CGrowSystem_t4101788339::get_offset_of__txtTotalPlayerNum_PC_29(),
	CGrowSystem_t4101788339::get_offset_of__txtTotalPlayerNum_MOBILE_30(),
	CGrowSystem_t4101788339::get_offset_of__txtCurTotalVolume_31(),
	CGrowSystem_t4101788339::get_offset_of__txtEatThornNum_32(),
	CGrowSystem_t4101788339::get_offset_of__txtPlayerSpeed_33(),
	CGrowSystem_t4101788339::get_offset_of__txtLiveBallNum_34(),
	CGrowSystem_t4101788339::get_offset_of__txtJiShaInfo_35(),
	CGrowSystem_t4101788339::get_offset_of__txtAttenuate_36(),
	CGrowSystem_t4101788339::get_offset_of__txtKill_37(),
	CGrowSystem_t4101788339::get_offset_of__txtBeKilled_38(),
	CGrowSystem_t4101788339::get_offset_of__txtAssist_39(),
	CGrowSystem_t4101788339::get_offset_of__txtCurTotalVolume_PC_40(),
	CGrowSystem_t4101788339::get_offset_of__txtEatThornNum_PC_41(),
	CGrowSystem_t4101788339::get_offset_of__txtPlayerSpeed_PC_42(),
	CGrowSystem_t4101788339::get_offset_of__txtKill_PC_43(),
	CGrowSystem_t4101788339::get_offset_of__txtBeKilled_PC_44(),
	CGrowSystem_t4101788339::get_offset_of__txtAssist_PC_45(),
	CGrowSystem_t4101788339::get_offset_of__txtCurTotalVolume_MOBILE_46(),
	CGrowSystem_t4101788339::get_offset_of__txtEatThornNum_MOBILE_47(),
	CGrowSystem_t4101788339::get_offset_of__txtPlayerSpeed_MOBILE_48(),
	CGrowSystem_t4101788339::get_offset_of__txtKill_MOBILE_49(),
	CGrowSystem_t4101788339::get_offset_of__txtBeKilled_MOBILE_50(),
	CGrowSystem_t4101788339::get_offset_of__txtAssist_MOBILE_51(),
	CGrowSystem_t4101788339::get_offset_of__imgBattery_52(),
	CGrowSystem_t4101788339::get_offset_of__wifiInfo_53(),
	CGrowSystem_t4101788339::get_offset_of_m_CurLevelConfig_54(),
	CGrowSystem_t4101788339::get_offset_of_m_dicLevelConfig_55(),
	CGrowSystem_t4101788339::get_offset_of_m_nCurLevel_56(),
	CGrowSystem_t4101788339::get_offset_of_m_nCurExp_57(),
	CGrowSystem_t4101788339::get_offset_of_m_nNextLevelExp_58(),
	CGrowSystem_t4101788339::get_offset_of_m_fCurMP_59(),
	CGrowSystem_t4101788339::get_offset_of_m_fCurMaxMP_60(),
	CGrowSystem_t4101788339::get_offset_of_m_nKillCount_61(),
	CGrowSystem_t4101788339::get_offset_of_m_nBeingKilledCount_62(),
	CGrowSystem_t4101788339::get_offset_of_m_nAssistAttackCount_63(),
	CGrowSystem_t4101788339::get_offset_of_m_fMpTextStartPos_64(),
	CGrowSystem_t4101788339::get_offset_of_m_fMpTextEndPos_65(),
	CGrowSystem_t4101788339::get_offset_of_m_fMpTextDistance_66(),
	CGrowSystem_t4101788339::get_offset_of_m_fMpEffectStartPos_67(),
	CGrowSystem_t4101788339::get_offset_of_m_fMpEffectEndPos_68(),
	CGrowSystem_t4101788339::get_offset_of_m_fMpEffectDistance_69(),
	CGrowSystem_t4101788339::get_offset_of__effectMp_70(),
	CGrowSystem_t4101788339::get_offset_of__effectMpStart_71(),
	CGrowSystem_t4101788339::get_offset_of__txtRecoverSpeed_72(),
	CGrowSystem_t4101788339::get_offset_of__goMpPanel_PC_73(),
	CGrowSystem_t4101788339::get_offset_of__goMpPanel_Mobile_74(),
	CGrowSystem_t4101788339::get_offset_of__Player_75(),
	CGrowSystem_t4101788339::get_offset_of_m_bUiInited_76(),
	CGrowSystem_t4101788339::get_offset_of_tempLevelConfig_77(),
	CGrowSystem_t4101788339::get_offset_of_m_bExpChanged_78(),
	CGrowSystem_t4101788339::get_offset_of_m_nExplodeThornNum_79(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (sLevelConfig_t4291590291)+ sizeof (RuntimeObject), sizeof(sLevelConfig_t4291590291 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3388[9] = 
{
	sLevelConfig_t4291590291::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fNeedExp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fMP_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fMPRecoverSpeed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fKillGainExp_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fKillGainMoney_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fBaseVolume_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_fLevelMoney_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelConfig_t4291590291::get_offset_of_nDeadWaitingTime_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (GUIAction_t288362040), -1, sizeof(GUIAction_t288362040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3389[26] = 
{
	0,
	GUIAction_t288362040::get_offset_of__canvas_3(),
	GUIAction_t288362040::get_offset_of__product_title_4(),
	GUIAction_t288362040::get_offset_of__bgm_title_5(),
	GUIAction_t288362040::get_offset_of__menu_button_6(),
	GUIAction_t288362040::get_offset_of__play_button_7(),
	GUIAction_t288362040::get_offset_of__next_button_8(),
	GUIAction_t288362040::get_offset_of__prev_button_9(),
	GUIAction_t288362040::get_offset_of__home_button_10(),
	GUIAction_t288362040::get_offset_of__shoot_button_11(),
	GUIAction_t288362040::get_offset_of__split_button_12(),
	GUIAction_t288362040::get_offset_of__ctrl_mode_dropdown_13(),
	GUIAction_t288362040::get_offset_of__dir_indicator_type_dropdown_14(),
	GUIAction_t288362040::get_offset_of__spit_spore_15(),
	GUIAction_t288362040::get_offset_of__spit_ball_16(),
	GUIAction_t288362040::get_offset_of__obscene_unfold_17(),
	GUIAction_t288362040::get_offset_of__spit_thorn_18(),
	GUIAction_t288362040::get_offset_of__lababa_19(),
	GUIAction_t288362040::get_offset_of__change_skin_20(),
	GUIAction_t288362040::get_offset_of__show_skill_point_ui_21(),
	GUIAction_t288362040::get_offset_of__MainControlPanel_22(),
	GUIAction_t288362040::get_offset_of__goSpitBall_23(),
	GUIAction_t288362040::get_offset_of__goSpitBall_AddPoint_24(),
	GUIAction_t288362040::get_offset_of__rhythm_rotate_factor_25(),
	GUIAction_t288362040::get_offset_of__rhythm_scale_factor_26(),
	GUIAction_t288362040_StaticFields::get_offset_of_s_Instance_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (CCosmosGunSight_t3630013301), -1, sizeof(CCosmosGunSight_t3630013301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3390[10] = 
{
	CCosmosGunSight_t3630013301_StaticFields::get_offset_of_vecTempPos_2(),
	CCosmosGunSight_t3630013301_StaticFields::get_offset_of_vecTempScale_3(),
	CCosmosGunSight_t3630013301::get_offset_of_m_goHead_4(),
	CCosmosGunSight_t3630013301::get_offset_of_m_goJoint_5(),
	CCosmosGunSight_t3630013301::get_offset_of_m_goStick_6(),
	CCosmosGunSight_t3630013301::get_offset_of_m_fAngle_7(),
	CCosmosGunSight_t3630013301::get_offset_of_m_fChildSize_8(),
	CCosmosGunSight_t3630013301::get_offset_of_m_fDistanceForStick_9(),
	CCosmosGunSight_t3630013301::get_offset_of_m_fTotalDistance_10(),
	CCosmosGunSight_t3630013301::get_offset_of_m_bPicked_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (CGunSightManager_t3371991777), -1, sizeof(CGunSightManager_t3371991777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3391[3] = 
{
	CGunSightManager_t3371991777_StaticFields::get_offset_of_s_Instance_2(),
	CGunSightManager_t3371991777::get_offset_of_m_preGunSight_3(),
	CGunSightManager_t3371991777::get_offset_of_m_lstRecycledGunSight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (CItem_t1170024128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (CItemSystem_t1283876235), -1, sizeof(CItemSystem_t1283876235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3393[27] = 
{
	CItemSystem_t1283876235_StaticFields::get_offset_of_vecTempPos_2(),
	CItemSystem_t1283876235_StaticFields::get_offset_of_s_Instance_3(),
	0,
	0,
	CItemSystem_t1283876235::get_offset_of__dropdownFunc_6(),
	CItemSystem_t1283876235::get_offset_of__dropdownLevel_7(),
	CItemSystem_t1283876235::get_offset_of__inputName_8(),
	CItemSystem_t1283876235::get_offset_of__inputDesc_9(),
	CItemSystem_t1283876235::get_offset_of__inputSpriteId_10(),
	CItemSystem_t1283876235::get_offset_of__inputShortKey_11(),
	CItemSystem_t1283876235::get_offset_of__inputPrice_12(),
	CItemSystem_t1283876235::get_offset_of__inputValue_13(),
	CItemSystem_t1283876235::get_offset_of_m_aryItemSprite_14(),
	CItemSystem_t1283876235::get_offset_of_m_aryUiItem_15(),
	CItemSystem_t1283876235::get_offset_of_m_aryUiItem_PC_16(),
	CItemSystem_t1283876235::get_offset_of_m_aryUiItem_Mobile_17(),
	CItemSystem_t1283876235::get_offset_of_m_CurItemConfig_18(),
	CItemSystem_t1283876235::get_offset_of_tempValueByLevel_19(),
	CItemSystem_t1283876235::get_offset_of_m_dicItemConfig_20(),
	CItemSystem_t1283876235::get_offset_of_tempItemConfig_21(),
	CItemSystem_t1283876235::get_offset_of_m_nCurConfigId_22(),
	CItemSystem_t1283876235::get_offset_of_m_nCurLevel_23(),
	CItemSystem_t1283876235::get_offset_of_m_dicItemNum_24(),
	CItemSystem_t1283876235::get_offset_of_m_bUiInited_25(),
	CItemSystem_t1283876235::get_offset_of_m_lstItemBuyEffect_26(),
	CItemSystem_t1283876235::get_offset_of_m_aryCounterPos_27(),
	CItemSystem_t1283876235::get_offset_of_m_aryCounterTaken_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (eItemFunc_t2560496039)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3394[6] = 
{
	eItemFunc_t2560496039::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (sItemConfig_t2971202336)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[7] = 
{
	sItemConfig_t2971202336::get_offset_of_nConfigId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t2971202336::get_offset_of_nShortcutKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t2971202336::get_offset_of_szName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t2971202336::get_offset_of_szDesc_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t2971202336::get_offset_of_nFunc_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t2971202336::get_offset_of_nSpriteId_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t2971202336::get_offset_of_dicValueByLevel_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (sValueByLevel_t810710842)+ sizeof (RuntimeObject), sizeof(sValueByLevel_t810710842 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3396[2] = 
{
	sValueByLevel_t810710842::get_offset_of_nPrice_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sValueByLevel_t810710842::get_offset_of_fValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (UIItem_t1605098621), -1, sizeof(UIItem_t1605098621_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3397[27] = 
{
	UIItem_t1605098621_StaticFields::get_offset_of_s_Instance_2(),
	UIItem_t1605098621::get_offset_of__canvasGroup_3(),
	UIItem_t1605098621::get_offset_of__btnAddPoint_4(),
	UIItem_t1605098621::get_offset_of__txtNum_5(),
	UIItem_t1605098621::get_offset_of__txtName_6(),
	UIItem_t1605098621::get_offset_of__txtDescValue_7(),
	UIItem_t1605098621::get_offset_of__txtDesc_8(),
	UIItem_t1605098621::get_offset_of__txtPrice_9(),
	UIItem_t1605098621::get_offset_of__txtNextLevelDesc_10(),
	UIItem_t1605098621::get_offset_of__imgMain_11(),
	UIItem_t1605098621::get_offset_of__tips_12(),
	UIItem_t1605098621::get_offset_of__imgLevel_13(),
	UIItem_t1605098621::get_offset_of__btnQiPao_14(),
	UIItem_t1605098621::get_offset_of__btnMain_15(),
	UIItem_t1605098621::get_offset_of_m_nShortcuteKey_16(),
	UIItem_t1605098621::get_offset_of_m_Config_17(),
	UIItem_t1605098621::get_offset_of__effectCanLevelUp_18(),
	UIItem_t1605098621::get_offset_of_m_nCurLevel_19(),
	UIItem_t1605098621::get_offset_of_m_bVisible_20(),
	0,
	UIItem_t1605098621::get_offset_of_m_nCurPosIndex_22(),
	UIItem_t1605098621_StaticFields::get_offset_of_vec2TempPos_23(),
	UIItem_t1605098621::get_offset_of_m_nTaleFadeStatus_24(),
	UIItem_t1605098621::get_offset_of_m_fTaleFadeAlpha_25(),
	UIItem_t1605098621::get_offset_of_m_fTaleFadeTimeElapse_26(),
	UIItem_t1605098621::get_offset_of_m_bFaded_27(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (CJieSuanManager_t2490531887), -1, sizeof(CJieSuanManager_t2490531887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3398[26] = 
{
	CJieSuanManager_t2490531887_StaticFields::get_offset_of_s_Instance_2(),
	CJieSuanManager_t2490531887::get_offset_of__panelJieSuan_3(),
	CJieSuanManager_t2490531887::get_offset_of__panelGeRenJieSuan_4(),
	CJieSuanManager_t2490531887::get_offset_of_m_aryCounterPos_5(),
	CJieSuanManager_t2490531887::get_offset_of_m_nMoveStatus_6(),
	CJieSuanManager_t2490531887::get_offset_of_m_nQuanChangZuiJiaStatus_7(),
	CJieSuanManager_t2490531887::get_offset_of_m_fCounterMoveSpeed_8(),
	CJieSuanManager_t2490531887::get_offset_of_m_fZuiJiaKuangScaleSpeed_9(),
	CJieSuanManager_t2490531887::get_offset_of_m_aryJiesuanCounter_10(),
	CJieSuanManager_t2490531887::get_offset_of__imgZuiJiaKuang_11(),
	CJieSuanManager_t2490531887::get_offset_of_m_vecDestPos_12(),
	CJieSuanManager_t2490531887_StaticFields::get_offset_of_vecTempDir_13(),
	CJieSuanManager_t2490531887_StaticFields::get_offset_of_vecTempPos_14(),
	CJieSuanManager_t2490531887_StaticFields::get_offset_of_vecTempScale_15(),
	0,
	0,
	CJieSuanManager_t2490531887::get_offset_of__goCounterContainer_18(),
	CJieSuanManager_t2490531887::get_offset_of__effectQuanChangZuiJia_QianZou_19(),
	CJieSuanManager_t2490531887::get_offset_of__effectQuanChangZuiJia_ChiXu_20(),
	CJieSuanManager_t2490531887::get_offset_of_m_fZhengTiJieSuanShowTime_21(),
	CJieSuanManager_t2490531887::get_offset_of_m_fZhengTiJieSuanTimeElapse_22(),
	CJieSuanManager_t2490531887::get_offset_of_m_aryScalingSpeed_23(),
	CJieSuanManager_t2490531887::get_offset_of_m_aryDestValue_24(),
	CJieSuanManager_t2490531887::get_offset_of_m_bGenRenJieSuan_25(),
	CJieSuanManager_t2490531887::get_offset_of_m_bZhengTiJieSuan_26(),
	CJieSuanManager_t2490531887::get_offset_of_m_bShowingZhengTiJieSuan_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (CLight_t1535733186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[1] = 
{
	CLight_t1535733186::get_offset_of_m_eType_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
