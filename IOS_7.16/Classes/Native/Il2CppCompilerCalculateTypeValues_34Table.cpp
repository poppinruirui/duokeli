﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// CRegister
struct CRegister_t4189215833;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.UI.Text
struct Text_t1901882714;
// CircleOutline
struct CircleOutline_t408400008;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// CSelectSkill
struct CSelectSkill_t3600552377;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// Shapes2D.PathSegment[]
struct PathSegmentU5BU5D_t2220036080;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2767979955;
// CCyberTreeList
struct CCyberTreeList_t4040119676;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// Outline8[]
struct Outline8U5BU5D_t830831472;
// CRechargeCounter[]
struct CRechargeCounterU5BU5D_t1203895268;
// CBuyConfirmUI
struct CBuyConfirmUI_t2670430472;
// CyberTreeScrollView
struct CyberTreeScrollView_t257211719;
// CyberTreeScrollView[]
struct CyberTreeScrollViewU5BU5D_t1486543294;
// System.Collections.Generic.Dictionary`2<ShoppingMallManager/eShoppingMallPageType,System.Collections.Generic.List`1<CShoppingCounter>>
struct Dictionary_2_t4055342882;
// CShoppingCounter
struct CShoppingCounter_t3637302240;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2297175928;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t1744877482;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3524055750;
// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Unity.SkeletonGraphic>
struct Dictionary_2_t633590813;
// System.Collections.Generic.Dictionary`2<System.Int32,CSkillSystem/sSkillInfo>
struct Dictionary_2_t3376406213;
// CProgressBar
struct CProgressBar_t881982788;
// CUISkill[]
struct CUISkillU5BU5D_t2575728562;
// UISkillConfig[]
struct UISkillConfigU5BU5D_t485806816;
// UISkillCastButton[]
struct UISkillCastButtonU5BU5D_t2561852851;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t736164020;
// CCommonJinDuTiao
struct CCommonJinDuTiao_t2836400377;
// CRoom[]
struct CRoomU5BU5D_t1935531732;
// CRoom
struct CRoom_t2285119849;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.Dictionary`2<CLightSystem/eLightType,System.Collections.Generic.List`1<CLight>>
struct Dictionary_2_t3959599972;
// CButtonClick[]
struct CButtonClickU5BU5D_t2296231296;
// CGameModeSelectCounter
struct CGameModeSelectCounter_t2640523211;
// PhotonView
struct PhotonView_t2207721820;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// System.Collections.Generic.List`1<CAvatar>
struct List_1_t2724846135;
// System.Collections.Generic.List`1<CJiShaInfo/sJiShaSystemMsgParam>
struct List_1_t3806197044;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Collections.Generic.Dictionary`2<System.String,CRegister/sAccountInfo>
struct Dictionary_2_t537133542;
// System.Collections.Queue
struct Queue_t3637523393;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Slider[]
struct SliderU5BU5D_t3847901219;
// CProgressBar[]
struct CProgressBarU5BU5D_t529576237;
// CArrow
struct CArrow_t1475493256;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t285980105;
// System.Collections.Generic.List`1<Ball>
struct List_1_t3678741308;
// Shapes2D.Shape[]
struct ShapeU5BU5D_t714462164;
// Main
struct Main_t2227614074;
// System.Collections.Generic.List`1<UnityEngine.UI.Text[]>
struct List_1_t1894159349;
// CSkillDetailGrid[]
struct CSkillDetailGridU5BU5D_t173934927;
// System.Collections.Generic.Dictionary`2<System.Int32,CSelectSkill/sSkillDetail>[]
struct Dictionary_2U5BU5D_t883166233;
// CFrameAnimationEffect
struct CFrameAnimationEffect_t443605508;
// Shapes2D.Shape
struct Shape_t3950352489;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// CPveObj
struct CPveObj_t2172873472;
// System.Collections.Generic.Dictionary`2<System.Int32,CSpryEditor/sSprayConfig>
struct Dictionary_2_t754550263;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t1172778254;
// System.Collections.Generic.Dictionary`2<System.Int32,CGrassEditor/sGrassConfig>
struct Dictionary_2_t3257632400;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite>
struct Dictionary_2_t3464337719;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// Shapes2D.Shape/UserProps
struct UserProps_t2498857227;
// Shapes2D.Shape/ShaderProps
struct ShaderProps_t3992238139;
// System.Predicate`1<Shapes2D.Shape>
struct Predicate_1_t480679317;
// System.Collections.Generic.List`1<CPaiHangBangItem>
struct List_1_t2754159026;
// System.Collections.Generic.List`1<CPaiHangBang/sPlayerAccount>
struct List_1_t230679895;
// System.Collections.Generic.List`1<CPaiHangBangItem_Mobile>
struct List_1_t2106334934;
// CPaiHangBangRecord[]
struct CPaiHangBangRecordU5BU5D_t544191923;
// System.Collections.Generic.List`1<CPaiHangBang_Mobile/sPlayerAccount>
struct List_1_t1569742014;
// System.Collections.Generic.Dictionary`2<System.Int32,CPaiHangBang_Mobile/sPlayerAccount>
struct Dictionary_2_t3281347899;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.Generic.List`1<CBeanCollection/sBeanInfo>
struct List_1_t323162583;
// System.Collections.Generic.List`1<UnityEngine.BoxCollider2D>
struct List_1_t758449277;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<CMonster>>
struct List_1_t590653126;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// Ball
struct Ball_t2206666566;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t3693186521;
// CQueueItem
struct CQueueItem_t1983427687;
// System.Collections.Generic.List`1<PhotonPlayer>
struct List_1_t482257003;
// System.Collections.Generic.List`1<CQueueItem>
struct List_1_t3455502429;

struct Color_t2555686324 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CGETVERICODEU3EC__ITERATOR0_T3257255021_H
#define U3CGETVERICODEU3EC__ITERATOR0_T3257255021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister/<GetVeriCode>c__Iterator0
struct  U3CGetVeriCodeU3Ec__Iterator0_t3257255021  : public RuntimeObject
{
public:
	// System.String CRegister/<GetVeriCode>c__Iterator0::<request>__0
	String_t* ___U3CrequestU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest CRegister/<GetVeriCode>c__Iterator0::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_1;
	// CRegister CRegister/<GetVeriCode>c__Iterator0::$this
	CRegister_t4189215833 * ___U24this_2;
	// System.Object CRegister/<GetVeriCode>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CRegister/<GetVeriCode>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 CRegister/<GetVeriCode>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetVeriCodeU3Ec__Iterator0_t3257255021, ___U3CrequestU3E__0_0)); }
	inline String_t* get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline String_t** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(String_t* value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetVeriCodeU3Ec__Iterator0_t3257255021, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetVeriCodeU3Ec__Iterator0_t3257255021, ___U24this_2)); }
	inline CRegister_t4189215833 * get_U24this_2() const { return ___U24this_2; }
	inline CRegister_t4189215833 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CRegister_t4189215833 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetVeriCodeU3Ec__Iterator0_t3257255021, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetVeriCodeU3Ec__Iterator0_t3257255021, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetVeriCodeU3Ec__Iterator0_t3257255021, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETVERICODEU3EC__ITERATOR0_T3257255021_H
#ifndef U3CDOREGISTERU3EC__ITERATOR1_T3660841303_H
#define U3CDOREGISTERU3EC__ITERATOR1_T3660841303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister/<DoRegister>c__Iterator1
struct  U3CDoRegisterU3Ec__Iterator1_t3660841303  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CRegister/<DoRegister>c__Iterator1::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest CRegister/<DoRegister>c__Iterator1::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_1;
	// CRegister CRegister/<DoRegister>c__Iterator1::$this
	CRegister_t4189215833 * ___U24this_2;
	// System.Object CRegister/<DoRegister>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CRegister/<DoRegister>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 CRegister/<DoRegister>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoRegisterU3Ec__Iterator1_t3660841303, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoRegisterU3Ec__Iterator1_t3660841303, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDoRegisterU3Ec__Iterator1_t3660841303, ___U24this_2)); }
	inline CRegister_t4189215833 * get_U24this_2() const { return ___U24this_2; }
	inline CRegister_t4189215833 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CRegister_t4189215833 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoRegisterU3Ec__Iterator1_t3660841303, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDoRegisterU3Ec__Iterator1_t3660841303, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDoRegisterU3Ec__Iterator1_t3660841303, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOREGISTERU3EC__ITERATOR1_T3660841303_H
#ifndef U3CDOLOGINBYPASSWORDU3EC__ITERATOR2_T1711557481_H
#define U3CDOLOGINBYPASSWORDU3EC__ITERATOR2_T1711557481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister/<DoLoginByPassword>c__Iterator2
struct  U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CRegister/<DoLoginByPassword>c__Iterator2::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest CRegister/<DoLoginByPassword>c__Iterator2::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_1;
	// System.String[] CRegister/<DoLoginByPassword>c__Iterator2::<aryTemp1>__0
	StringU5BU5D_t1281789340* ___U3CaryTemp1U3E__0_2;
	// System.String[] CRegister/<DoLoginByPassword>c__Iterator2::<aryTemp2>__0
	StringU5BU5D_t1281789340* ___U3CaryTemp2U3E__0_3;
	// CRegister CRegister/<DoLoginByPassword>c__Iterator2::$this
	CRegister_t4189215833 * ___U24this_4;
	// System.Object CRegister/<DoLoginByPassword>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean CRegister/<DoLoginByPassword>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 CRegister/<DoLoginByPassword>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CaryTemp1U3E__0_2() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U3CaryTemp1U3E__0_2)); }
	inline StringU5BU5D_t1281789340* get_U3CaryTemp1U3E__0_2() const { return ___U3CaryTemp1U3E__0_2; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CaryTemp1U3E__0_2() { return &___U3CaryTemp1U3E__0_2; }
	inline void set_U3CaryTemp1U3E__0_2(StringU5BU5D_t1281789340* value)
	{
		___U3CaryTemp1U3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaryTemp1U3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CaryTemp2U3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U3CaryTemp2U3E__0_3)); }
	inline StringU5BU5D_t1281789340* get_U3CaryTemp2U3E__0_3() const { return ___U3CaryTemp2U3E__0_3; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CaryTemp2U3E__0_3() { return &___U3CaryTemp2U3E__0_3; }
	inline void set_U3CaryTemp2U3E__0_3(StringU5BU5D_t1281789340* value)
	{
		___U3CaryTemp2U3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaryTemp2U3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U24this_4)); }
	inline CRegister_t4189215833 * get_U24this_4() const { return ___U24this_4; }
	inline CRegister_t4189215833 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(CRegister_t4189215833 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOLOGINBYPASSWORDU3EC__ITERATOR2_T1711557481_H
#ifndef U3CDOLOGINAUTOU3EC__ITERATOR3_T2803915295_H
#define U3CDOLOGINAUTOU3EC__ITERATOR3_T2803915295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister/<DoLoginAuto>c__Iterator3
struct  U3CDoLoginAutoU3Ec__Iterator3_t2803915295  : public RuntimeObject
{
public:
	// System.String CRegister/<DoLoginAuto>c__Iterator3::<request>__0
	String_t* ___U3CrequestU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest CRegister/<DoLoginAuto>c__Iterator3::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_1;
	// CRegister CRegister/<DoLoginAuto>c__Iterator3::$this
	CRegister_t4189215833 * ___U24this_2;
	// System.Object CRegister/<DoLoginAuto>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CRegister/<DoLoginAuto>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 CRegister/<DoLoginAuto>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoLoginAutoU3Ec__Iterator3_t2803915295, ___U3CrequestU3E__0_0)); }
	inline String_t* get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline String_t** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(String_t* value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoLoginAutoU3Ec__Iterator3_t2803915295, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDoLoginAutoU3Ec__Iterator3_t2803915295, ___U24this_2)); }
	inline CRegister_t4189215833 * get_U24this_2() const { return ___U24this_2; }
	inline CRegister_t4189215833 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CRegister_t4189215833 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoLoginAutoU3Ec__Iterator3_t2803915295, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDoLoginAutoU3Ec__Iterator3_t2803915295, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDoLoginAutoU3Ec__Iterator3_t2803915295, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOLOGINAUTOU3EC__ITERATOR3_T2803915295_H
#ifndef SHAPEDRAWORDERCOMPARER_T3317179762_H
#define SHAPEDRAWORDERCOMPARER_T3317179762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.Shape/ShapeDrawOrderComparer
struct  ShapeDrawOrderComparer_t3317179762  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPEDRAWORDERCOMPARER_T3317179762_H
#ifndef U3CLOADSCENEU3EC__ITERATOR0_T723620668_H
#define U3CLOADSCENEU3EC__ITERATOR0_T723620668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppingMallManager/<LoadScene>c__Iterator0
struct  U3CLoadSceneU3Ec__Iterator0_t723620668  : public RuntimeObject
{
public:
	// System.String ShoppingMallManager/<LoadScene>c__Iterator0::scene_name
	String_t* ___scene_name_0;
	// System.Object ShoppingMallManager/<LoadScene>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ShoppingMallManager/<LoadScene>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ShoppingMallManager/<LoadScene>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_scene_name_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t723620668, ___scene_name_0)); }
	inline String_t* get_scene_name_0() const { return ___scene_name_0; }
	inline String_t** get_address_of_scene_name_0() { return &___scene_name_0; }
	inline void set_scene_name_0(String_t* value)
	{
		___scene_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_name_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t723620668, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t723620668, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator0_t723620668, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3EC__ITERATOR0_T723620668_H
#ifndef U3CDOCREATEROLEU3EC__ITERATOR4_T29253059_H
#define U3CDOCREATEROLEU3EC__ITERATOR4_T29253059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister/<DoCreateRole>c__Iterator4
struct  U3CDoCreateRoleU3Ec__Iterator4_t29253059  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm CRegister/<DoCreateRole>c__Iterator4::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest CRegister/<DoCreateRole>c__Iterator4::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_1;
	// CRegister CRegister/<DoCreateRole>c__Iterator4::$this
	CRegister_t4189215833 * ___U24this_2;
	// System.Object CRegister/<DoCreateRole>c__Iterator4::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CRegister/<DoCreateRole>c__Iterator4::$disposing
	bool ___U24disposing_4;
	// System.Int32 CRegister/<DoCreateRole>c__Iterator4::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoCreateRoleU3Ec__Iterator4_t29253059, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoCreateRoleU3Ec__Iterator4_t29253059, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDoCreateRoleU3Ec__Iterator4_t29253059, ___U24this_2)); }
	inline CRegister_t4189215833 * get_U24this_2() const { return ___U24this_2; }
	inline CRegister_t4189215833 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CRegister_t4189215833 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoCreateRoleU3Ec__Iterator4_t29253059, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDoCreateRoleU3Ec__Iterator4_t29253059, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDoCreateRoleU3Ec__Iterator4_t29253059, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCREATEROLEU3EC__ITERATOR4_T29253059_H
#ifndef POLYMAP_T1466772104_H
#define POLYMAP_T1466772104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.PolyMap
struct  PolyMap_t1466772104  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYMAP_T1466772104_H
#ifndef OUTLINETEXT_T3202294091_H
#define OUTLINETEXT_T3202294091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OutlineText
struct  OutlineText_t3202294091  : public RuntimeObject
{
public:
	// UnityEngine.GameObject OutlineText::textGraphics
	GameObject_t1113636619 * ___textGraphics_0;
	// UnityEngine.Canvas OutlineText::canvas
	Canvas_t3310196443 * ___canvas_1;
	// UnityEngine.UI.Text OutlineText::text
	Text_t1901882714 * ___text_2;
	// CircleOutline OutlineText::outline
	CircleOutline_t408400008 * ___outline_3;

public:
	inline static int32_t get_offset_of_textGraphics_0() { return static_cast<int32_t>(offsetof(OutlineText_t3202294091, ___textGraphics_0)); }
	inline GameObject_t1113636619 * get_textGraphics_0() const { return ___textGraphics_0; }
	inline GameObject_t1113636619 ** get_address_of_textGraphics_0() { return &___textGraphics_0; }
	inline void set_textGraphics_0(GameObject_t1113636619 * value)
	{
		___textGraphics_0 = value;
		Il2CppCodeGenWriteBarrier((&___textGraphics_0), value);
	}

	inline static int32_t get_offset_of_canvas_1() { return static_cast<int32_t>(offsetof(OutlineText_t3202294091, ___canvas_1)); }
	inline Canvas_t3310196443 * get_canvas_1() const { return ___canvas_1; }
	inline Canvas_t3310196443 ** get_address_of_canvas_1() { return &___canvas_1; }
	inline void set_canvas_1(Canvas_t3310196443 * value)
	{
		___canvas_1 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_1), value);
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(OutlineText_t3202294091, ___text_2)); }
	inline Text_t1901882714 * get_text_2() const { return ___text_2; }
	inline Text_t1901882714 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t1901882714 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_outline_3() { return static_cast<int32_t>(offsetof(OutlineText_t3202294091, ___outline_3)); }
	inline CircleOutline_t408400008 * get_outline_3() const { return ___outline_3; }
	inline CircleOutline_t408400008 ** get_address_of_outline_3() { return &___outline_3; }
	inline void set_outline_3(CircleOutline_t408400008 * value)
	{
		___outline_3 = value;
		Il2CppCodeGenWriteBarrier((&___outline_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINETEXT_T3202294091_H
#ifndef U3CPARSEMAPU3EC__ITERATOR0_T4151616039_H
#define U3CPARSEMAPU3EC__ITERATOR0_T4151616039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectSkill/<ParseMap>c__Iterator0
struct  U3CParseMapU3Ec__Iterator0_t4151616039  : public RuntimeObject
{
public:
	// System.String CSelectSkill/<ParseMap>c__Iterator0::szFileName
	String_t* ___szFileName_0;
	// UnityEngine.WWW CSelectSkill/<ParseMap>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// System.Xml.XmlNode CSelectSkill/<ParseMap>c__Iterator0::<root>__0
	XmlNode_t3767805227 * ___U3CrootU3E__0_2;
	// System.Xml.XmlDocument CSelectSkill/<ParseMap>c__Iterator0::<myXmlDoc>__0
	XmlDocument_t2837193595 * ___U3CmyXmlDocU3E__0_3;
	// System.Xml.XmlNode CSelectSkill/<ParseMap>c__Iterator0::<nodeSkillDesc>__0
	XmlNode_t3767805227 * ___U3CnodeSkillDescU3E__0_4;
	// System.Xml.XmlNode CSelectSkill/<ParseMap>c__Iterator0::<nodeSkill>__0
	XmlNode_t3767805227 * ___U3CnodeSkillU3E__0_5;
	// System.Xml.XmlNode CSelectSkill/<ParseMap>c__Iterator0::<nodeCommon>__0
	XmlNode_t3767805227 * ___U3CnodeCommonU3E__0_6;
	// System.Xml.XmlNode CSelectSkill/<ParseMap>c__Iterator0::<nodePlayerNumToStart>__0
	XmlNode_t3767805227 * ___U3CnodePlayerNumToStartU3E__0_7;
	// CSelectSkill CSelectSkill/<ParseMap>c__Iterator0::$this
	CSelectSkill_t3600552377 * ___U24this_8;
	// System.Object CSelectSkill/<ParseMap>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean CSelectSkill/<ParseMap>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 CSelectSkill/<ParseMap>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_szFileName_0() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___szFileName_0)); }
	inline String_t* get_szFileName_0() const { return ___szFileName_0; }
	inline String_t** get_address_of_szFileName_0() { return &___szFileName_0; }
	inline void set_szFileName_0(String_t* value)
	{
		___szFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E__0_2() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CrootU3E__0_2)); }
	inline XmlNode_t3767805227 * get_U3CrootU3E__0_2() const { return ___U3CrootU3E__0_2; }
	inline XmlNode_t3767805227 ** get_address_of_U3CrootU3E__0_2() { return &___U3CrootU3E__0_2; }
	inline void set_U3CrootU3E__0_2(XmlNode_t3767805227 * value)
	{
		___U3CrootU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CmyXmlDocU3E__0_3() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CmyXmlDocU3E__0_3)); }
	inline XmlDocument_t2837193595 * get_U3CmyXmlDocU3E__0_3() const { return ___U3CmyXmlDocU3E__0_3; }
	inline XmlDocument_t2837193595 ** get_address_of_U3CmyXmlDocU3E__0_3() { return &___U3CmyXmlDocU3E__0_3; }
	inline void set_U3CmyXmlDocU3E__0_3(XmlDocument_t2837193595 * value)
	{
		___U3CmyXmlDocU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmyXmlDocU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnodeSkillDescU3E__0_4() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CnodeSkillDescU3E__0_4)); }
	inline XmlNode_t3767805227 * get_U3CnodeSkillDescU3E__0_4() const { return ___U3CnodeSkillDescU3E__0_4; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeSkillDescU3E__0_4() { return &___U3CnodeSkillDescU3E__0_4; }
	inline void set_U3CnodeSkillDescU3E__0_4(XmlNode_t3767805227 * value)
	{
		___U3CnodeSkillDescU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeSkillDescU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CnodeSkillU3E__0_5() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CnodeSkillU3E__0_5)); }
	inline XmlNode_t3767805227 * get_U3CnodeSkillU3E__0_5() const { return ___U3CnodeSkillU3E__0_5; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeSkillU3E__0_5() { return &___U3CnodeSkillU3E__0_5; }
	inline void set_U3CnodeSkillU3E__0_5(XmlNode_t3767805227 * value)
	{
		___U3CnodeSkillU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeSkillU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CnodeCommonU3E__0_6() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CnodeCommonU3E__0_6)); }
	inline XmlNode_t3767805227 * get_U3CnodeCommonU3E__0_6() const { return ___U3CnodeCommonU3E__0_6; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodeCommonU3E__0_6() { return &___U3CnodeCommonU3E__0_6; }
	inline void set_U3CnodeCommonU3E__0_6(XmlNode_t3767805227 * value)
	{
		___U3CnodeCommonU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeCommonU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CnodePlayerNumToStartU3E__0_7() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U3CnodePlayerNumToStartU3E__0_7)); }
	inline XmlNode_t3767805227 * get_U3CnodePlayerNumToStartU3E__0_7() const { return ___U3CnodePlayerNumToStartU3E__0_7; }
	inline XmlNode_t3767805227 ** get_address_of_U3CnodePlayerNumToStartU3E__0_7() { return &___U3CnodePlayerNumToStartU3E__0_7; }
	inline void set_U3CnodePlayerNumToStartU3E__0_7(XmlNode_t3767805227 * value)
	{
		___U3CnodePlayerNumToStartU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodePlayerNumToStartU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U24this_8)); }
	inline CSelectSkill_t3600552377 * get_U24this_8() const { return ___U24this_8; }
	inline CSelectSkill_t3600552377 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(CSelectSkill_t3600552377 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CParseMapU3Ec__Iterator0_t4151616039, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPARSEMAPU3EC__ITERATOR0_T4151616039_H
#ifndef SPLAYERACCOUNT_T3053572449_H
#define SPLAYERACCOUNT_T3053572449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBang/sPlayerAccount
struct  sPlayerAccount_t3053572449 
{
public:
	// System.String CPaiHangBang/sPlayerAccount::szName
	String_t* ___szName_0;
	// System.Int32 CPaiHangBang/sPlayerAccount::nOwnerId
	int32_t ___nOwnerId_1;
	// System.Single CPaiHangBang/sPlayerAccount::fTotalVolume
	float ___fTotalVolume_2;
	// System.Int32 CPaiHangBang/sPlayerAccount::nLevel
	int32_t ___nLevel_3;
	// System.Int32 CPaiHangBang/sPlayerAccount::nEatThornNum
	int32_t ___nEatThornNum_4;
	// System.Int32 CPaiHangBang/sPlayerAccount::nKillNum
	int32_t ___nKillNum_5;
	// System.Int32 CPaiHangBang/sPlayerAccount::nBeKilledNum
	int32_t ___nBeKilledNum_6;
	// System.Int32 CPaiHangBang/sPlayerAccount::nAssistNum
	int32_t ___nAssistNum_7;
	// System.Int32 CPaiHangBang/sPlayerAccount::nItem0Num
	int32_t ___nItem0Num_8;
	// System.Int32 CPaiHangBang/sPlayerAccount::nItem1Num
	int32_t ___nItem1Num_9;
	// System.Int32 CPaiHangBang/sPlayerAccount::nItem2Num
	int32_t ___nItem2Num_10;
	// System.Int32 CPaiHangBang/sPlayerAccount::nItem3Num
	int32_t ___nItem3Num_11;

public:
	inline static int32_t get_offset_of_szName_0() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___szName_0)); }
	inline String_t* get_szName_0() const { return ___szName_0; }
	inline String_t** get_address_of_szName_0() { return &___szName_0; }
	inline void set_szName_0(String_t* value)
	{
		___szName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szName_0), value);
	}

	inline static int32_t get_offset_of_nOwnerId_1() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nOwnerId_1)); }
	inline int32_t get_nOwnerId_1() const { return ___nOwnerId_1; }
	inline int32_t* get_address_of_nOwnerId_1() { return &___nOwnerId_1; }
	inline void set_nOwnerId_1(int32_t value)
	{
		___nOwnerId_1 = value;
	}

	inline static int32_t get_offset_of_fTotalVolume_2() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___fTotalVolume_2)); }
	inline float get_fTotalVolume_2() const { return ___fTotalVolume_2; }
	inline float* get_address_of_fTotalVolume_2() { return &___fTotalVolume_2; }
	inline void set_fTotalVolume_2(float value)
	{
		___fTotalVolume_2 = value;
	}

	inline static int32_t get_offset_of_nLevel_3() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nLevel_3)); }
	inline int32_t get_nLevel_3() const { return ___nLevel_3; }
	inline int32_t* get_address_of_nLevel_3() { return &___nLevel_3; }
	inline void set_nLevel_3(int32_t value)
	{
		___nLevel_3 = value;
	}

	inline static int32_t get_offset_of_nEatThornNum_4() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nEatThornNum_4)); }
	inline int32_t get_nEatThornNum_4() const { return ___nEatThornNum_4; }
	inline int32_t* get_address_of_nEatThornNum_4() { return &___nEatThornNum_4; }
	inline void set_nEatThornNum_4(int32_t value)
	{
		___nEatThornNum_4 = value;
	}

	inline static int32_t get_offset_of_nKillNum_5() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nKillNum_5)); }
	inline int32_t get_nKillNum_5() const { return ___nKillNum_5; }
	inline int32_t* get_address_of_nKillNum_5() { return &___nKillNum_5; }
	inline void set_nKillNum_5(int32_t value)
	{
		___nKillNum_5 = value;
	}

	inline static int32_t get_offset_of_nBeKilledNum_6() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nBeKilledNum_6)); }
	inline int32_t get_nBeKilledNum_6() const { return ___nBeKilledNum_6; }
	inline int32_t* get_address_of_nBeKilledNum_6() { return &___nBeKilledNum_6; }
	inline void set_nBeKilledNum_6(int32_t value)
	{
		___nBeKilledNum_6 = value;
	}

	inline static int32_t get_offset_of_nAssistNum_7() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nAssistNum_7)); }
	inline int32_t get_nAssistNum_7() const { return ___nAssistNum_7; }
	inline int32_t* get_address_of_nAssistNum_7() { return &___nAssistNum_7; }
	inline void set_nAssistNum_7(int32_t value)
	{
		___nAssistNum_7 = value;
	}

	inline static int32_t get_offset_of_nItem0Num_8() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nItem0Num_8)); }
	inline int32_t get_nItem0Num_8() const { return ___nItem0Num_8; }
	inline int32_t* get_address_of_nItem0Num_8() { return &___nItem0Num_8; }
	inline void set_nItem0Num_8(int32_t value)
	{
		___nItem0Num_8 = value;
	}

	inline static int32_t get_offset_of_nItem1Num_9() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nItem1Num_9)); }
	inline int32_t get_nItem1Num_9() const { return ___nItem1Num_9; }
	inline int32_t* get_address_of_nItem1Num_9() { return &___nItem1Num_9; }
	inline void set_nItem1Num_9(int32_t value)
	{
		___nItem1Num_9 = value;
	}

	inline static int32_t get_offset_of_nItem2Num_10() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nItem2Num_10)); }
	inline int32_t get_nItem2Num_10() const { return ___nItem2Num_10; }
	inline int32_t* get_address_of_nItem2Num_10() { return &___nItem2Num_10; }
	inline void set_nItem2Num_10(int32_t value)
	{
		___nItem2Num_10 = value;
	}

	inline static int32_t get_offset_of_nItem3Num_11() { return static_cast<int32_t>(offsetof(sPlayerAccount_t3053572449, ___nItem3Num_11)); }
	inline int32_t get_nItem3Num_11() const { return ___nItem3Num_11; }
	inline int32_t* get_address_of_nItem3Num_11() { return &___nItem3Num_11; }
	inline void set_nItem3Num_11(int32_t value)
	{
		___nItem3Num_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CPaiHangBang/sPlayerAccount
struct sPlayerAccount_t3053572449_marshaled_pinvoke
{
	char* ___szName_0;
	int32_t ___nOwnerId_1;
	float ___fTotalVolume_2;
	int32_t ___nLevel_3;
	int32_t ___nEatThornNum_4;
	int32_t ___nKillNum_5;
	int32_t ___nBeKilledNum_6;
	int32_t ___nAssistNum_7;
	int32_t ___nItem0Num_8;
	int32_t ___nItem1Num_9;
	int32_t ___nItem2Num_10;
	int32_t ___nItem3Num_11;
};
// Native definition for COM marshalling of CPaiHangBang/sPlayerAccount
struct sPlayerAccount_t3053572449_marshaled_com
{
	Il2CppChar* ___szName_0;
	int32_t ___nOwnerId_1;
	float ___fTotalVolume_2;
	int32_t ___nLevel_3;
	int32_t ___nEatThornNum_4;
	int32_t ___nKillNum_5;
	int32_t ___nBeKilledNum_6;
	int32_t ___nAssistNum_7;
	int32_t ___nItem0Num_8;
	int32_t ___nItem1Num_9;
	int32_t ___nItem2Num_10;
	int32_t ___nItem3Num_11;
};
#endif // SPLAYERACCOUNT_T3053572449_H
#ifndef DISTANCERESULT_T3958382930_H
#define DISTANCERESULT_T3958382930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.PolyMap/DistanceResult
struct  DistanceResult_t3958382930 
{
public:
	// System.Int32 Shapes2D.PolyMap/DistanceResult::index
	int32_t ___index_0;
	// System.Int32 Shapes2D.PolyMap/DistanceResult::index2
	int32_t ___index2_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(DistanceResult_t3958382930, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_index2_1() { return static_cast<int32_t>(offsetof(DistanceResult_t3958382930, ___index2_1)); }
	inline int32_t get_index2_1() const { return ___index2_1; }
	inline int32_t* get_address_of_index2_1() { return &___index2_1; }
	inline void set_index2_1(int32_t value)
	{
		___index2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTANCERESULT_T3958382930_H
#ifndef SPLAYERACCOUNT_T97667272_H
#define SPLAYERACCOUNT_T97667272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBang_Mobile/sPlayerAccount
struct  sPlayerAccount_t97667272 
{
public:
	// System.String CPaiHangBang_Mobile/sPlayerAccount::szName
	String_t* ___szName_0;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nOwnerId
	int32_t ___nOwnerId_1;
	// System.Single CPaiHangBang_Mobile/sPlayerAccount::fTotalVolume
	float ___fTotalVolume_2;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nLevel
	int32_t ___nLevel_3;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nEatThornNum
	int32_t ___nEatThornNum_4;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nKillNum
	int32_t ___nKillNum_5;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nBeKilledNum
	int32_t ___nBeKilledNum_6;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nAssistNum
	int32_t ___nAssistNum_7;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nItem0Num
	int32_t ___nItem0Num_8;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nItem1Num
	int32_t ___nItem1Num_9;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nItem2Num
	int32_t ___nItem2Num_10;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nItem3Num
	int32_t ___nItem3Num_11;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nSelectedSkillId
	int32_t ___nSelectedSkillId_12;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nSelectedSkillLevel
	int32_t ___nSelectedSkillLevel_13;
	// System.Boolean CPaiHangBang_Mobile/sPlayerAccount::bIsMainPlayer
	bool ___bIsMainPlayer_14;
	// System.Boolean CPaiHangBang_Mobile/sPlayerAccount::bDead
	bool ___bDead_15;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nSkinId
	int32_t ___nSkinId_16;
	// System.Int32 CPaiHangBang_Mobile/sPlayerAccount::nRank
	int32_t ___nRank_17;

public:
	inline static int32_t get_offset_of_szName_0() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___szName_0)); }
	inline String_t* get_szName_0() const { return ___szName_0; }
	inline String_t** get_address_of_szName_0() { return &___szName_0; }
	inline void set_szName_0(String_t* value)
	{
		___szName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szName_0), value);
	}

	inline static int32_t get_offset_of_nOwnerId_1() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nOwnerId_1)); }
	inline int32_t get_nOwnerId_1() const { return ___nOwnerId_1; }
	inline int32_t* get_address_of_nOwnerId_1() { return &___nOwnerId_1; }
	inline void set_nOwnerId_1(int32_t value)
	{
		___nOwnerId_1 = value;
	}

	inline static int32_t get_offset_of_fTotalVolume_2() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___fTotalVolume_2)); }
	inline float get_fTotalVolume_2() const { return ___fTotalVolume_2; }
	inline float* get_address_of_fTotalVolume_2() { return &___fTotalVolume_2; }
	inline void set_fTotalVolume_2(float value)
	{
		___fTotalVolume_2 = value;
	}

	inline static int32_t get_offset_of_nLevel_3() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nLevel_3)); }
	inline int32_t get_nLevel_3() const { return ___nLevel_3; }
	inline int32_t* get_address_of_nLevel_3() { return &___nLevel_3; }
	inline void set_nLevel_3(int32_t value)
	{
		___nLevel_3 = value;
	}

	inline static int32_t get_offset_of_nEatThornNum_4() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nEatThornNum_4)); }
	inline int32_t get_nEatThornNum_4() const { return ___nEatThornNum_4; }
	inline int32_t* get_address_of_nEatThornNum_4() { return &___nEatThornNum_4; }
	inline void set_nEatThornNum_4(int32_t value)
	{
		___nEatThornNum_4 = value;
	}

	inline static int32_t get_offset_of_nKillNum_5() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nKillNum_5)); }
	inline int32_t get_nKillNum_5() const { return ___nKillNum_5; }
	inline int32_t* get_address_of_nKillNum_5() { return &___nKillNum_5; }
	inline void set_nKillNum_5(int32_t value)
	{
		___nKillNum_5 = value;
	}

	inline static int32_t get_offset_of_nBeKilledNum_6() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nBeKilledNum_6)); }
	inline int32_t get_nBeKilledNum_6() const { return ___nBeKilledNum_6; }
	inline int32_t* get_address_of_nBeKilledNum_6() { return &___nBeKilledNum_6; }
	inline void set_nBeKilledNum_6(int32_t value)
	{
		___nBeKilledNum_6 = value;
	}

	inline static int32_t get_offset_of_nAssistNum_7() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nAssistNum_7)); }
	inline int32_t get_nAssistNum_7() const { return ___nAssistNum_7; }
	inline int32_t* get_address_of_nAssistNum_7() { return &___nAssistNum_7; }
	inline void set_nAssistNum_7(int32_t value)
	{
		___nAssistNum_7 = value;
	}

	inline static int32_t get_offset_of_nItem0Num_8() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nItem0Num_8)); }
	inline int32_t get_nItem0Num_8() const { return ___nItem0Num_8; }
	inline int32_t* get_address_of_nItem0Num_8() { return &___nItem0Num_8; }
	inline void set_nItem0Num_8(int32_t value)
	{
		___nItem0Num_8 = value;
	}

	inline static int32_t get_offset_of_nItem1Num_9() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nItem1Num_9)); }
	inline int32_t get_nItem1Num_9() const { return ___nItem1Num_9; }
	inline int32_t* get_address_of_nItem1Num_9() { return &___nItem1Num_9; }
	inline void set_nItem1Num_9(int32_t value)
	{
		___nItem1Num_9 = value;
	}

	inline static int32_t get_offset_of_nItem2Num_10() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nItem2Num_10)); }
	inline int32_t get_nItem2Num_10() const { return ___nItem2Num_10; }
	inline int32_t* get_address_of_nItem2Num_10() { return &___nItem2Num_10; }
	inline void set_nItem2Num_10(int32_t value)
	{
		___nItem2Num_10 = value;
	}

	inline static int32_t get_offset_of_nItem3Num_11() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nItem3Num_11)); }
	inline int32_t get_nItem3Num_11() const { return ___nItem3Num_11; }
	inline int32_t* get_address_of_nItem3Num_11() { return &___nItem3Num_11; }
	inline void set_nItem3Num_11(int32_t value)
	{
		___nItem3Num_11 = value;
	}

	inline static int32_t get_offset_of_nSelectedSkillId_12() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nSelectedSkillId_12)); }
	inline int32_t get_nSelectedSkillId_12() const { return ___nSelectedSkillId_12; }
	inline int32_t* get_address_of_nSelectedSkillId_12() { return &___nSelectedSkillId_12; }
	inline void set_nSelectedSkillId_12(int32_t value)
	{
		___nSelectedSkillId_12 = value;
	}

	inline static int32_t get_offset_of_nSelectedSkillLevel_13() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nSelectedSkillLevel_13)); }
	inline int32_t get_nSelectedSkillLevel_13() const { return ___nSelectedSkillLevel_13; }
	inline int32_t* get_address_of_nSelectedSkillLevel_13() { return &___nSelectedSkillLevel_13; }
	inline void set_nSelectedSkillLevel_13(int32_t value)
	{
		___nSelectedSkillLevel_13 = value;
	}

	inline static int32_t get_offset_of_bIsMainPlayer_14() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___bIsMainPlayer_14)); }
	inline bool get_bIsMainPlayer_14() const { return ___bIsMainPlayer_14; }
	inline bool* get_address_of_bIsMainPlayer_14() { return &___bIsMainPlayer_14; }
	inline void set_bIsMainPlayer_14(bool value)
	{
		___bIsMainPlayer_14 = value;
	}

	inline static int32_t get_offset_of_bDead_15() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___bDead_15)); }
	inline bool get_bDead_15() const { return ___bDead_15; }
	inline bool* get_address_of_bDead_15() { return &___bDead_15; }
	inline void set_bDead_15(bool value)
	{
		___bDead_15 = value;
	}

	inline static int32_t get_offset_of_nSkinId_16() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nSkinId_16)); }
	inline int32_t get_nSkinId_16() const { return ___nSkinId_16; }
	inline int32_t* get_address_of_nSkinId_16() { return &___nSkinId_16; }
	inline void set_nSkinId_16(int32_t value)
	{
		___nSkinId_16 = value;
	}

	inline static int32_t get_offset_of_nRank_17() { return static_cast<int32_t>(offsetof(sPlayerAccount_t97667272, ___nRank_17)); }
	inline int32_t get_nRank_17() const { return ___nRank_17; }
	inline int32_t* get_address_of_nRank_17() { return &___nRank_17; }
	inline void set_nRank_17(int32_t value)
	{
		___nRank_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CPaiHangBang_Mobile/sPlayerAccount
struct sPlayerAccount_t97667272_marshaled_pinvoke
{
	char* ___szName_0;
	int32_t ___nOwnerId_1;
	float ___fTotalVolume_2;
	int32_t ___nLevel_3;
	int32_t ___nEatThornNum_4;
	int32_t ___nKillNum_5;
	int32_t ___nBeKilledNum_6;
	int32_t ___nAssistNum_7;
	int32_t ___nItem0Num_8;
	int32_t ___nItem1Num_9;
	int32_t ___nItem2Num_10;
	int32_t ___nItem3Num_11;
	int32_t ___nSelectedSkillId_12;
	int32_t ___nSelectedSkillLevel_13;
	int32_t ___bIsMainPlayer_14;
	int32_t ___bDead_15;
	int32_t ___nSkinId_16;
	int32_t ___nRank_17;
};
// Native definition for COM marshalling of CPaiHangBang_Mobile/sPlayerAccount
struct sPlayerAccount_t97667272_marshaled_com
{
	Il2CppChar* ___szName_0;
	int32_t ___nOwnerId_1;
	float ___fTotalVolume_2;
	int32_t ___nLevel_3;
	int32_t ___nEatThornNum_4;
	int32_t ___nKillNum_5;
	int32_t ___nBeKilledNum_6;
	int32_t ___nAssistNum_7;
	int32_t ___nItem0Num_8;
	int32_t ___nItem1Num_9;
	int32_t ___nItem2Num_10;
	int32_t ___nItem3Num_11;
	int32_t ___nSelectedSkillId_12;
	int32_t ___nSelectedSkillLevel_13;
	int32_t ___bIsMainPlayer_14;
	int32_t ___bDead_15;
	int32_t ___nSkinId_16;
	int32_t ___nRank_17;
};
#endif // SPLAYERACCOUNT_T97667272_H
#ifndef SGRASSCONFIG_T73951773_H
#define SGRASSCONFIG_T73951773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrassEditor/sGrassConfig
struct  sGrassConfig_t73951773 
{
public:
	// System.Int32 CGrassEditor/sGrassConfig::nId
	int32_t ___nId_0;
	// System.Int32 CGrassEditor/sGrassConfig::nFunc
	int32_t ___nFunc_1;
	// System.Int32 CGrassEditor/sGrassConfig::nPolygonId
	int32_t ___nPolygonId_2;
	// System.String CGrassEditor/sGrassConfig::szColor
	String_t* ___szColor_3;
	// System.Single[] CGrassEditor/sGrassConfig::aryValue
	SingleU5BU5D_t1444911251* ___aryValue_4;
	// System.String[] CGrassEditor/sGrassConfig::aryDesc
	StringU5BU5D_t1281789340* ___aryDesc_5;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sGrassConfig_t73951773, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_nFunc_1() { return static_cast<int32_t>(offsetof(sGrassConfig_t73951773, ___nFunc_1)); }
	inline int32_t get_nFunc_1() const { return ___nFunc_1; }
	inline int32_t* get_address_of_nFunc_1() { return &___nFunc_1; }
	inline void set_nFunc_1(int32_t value)
	{
		___nFunc_1 = value;
	}

	inline static int32_t get_offset_of_nPolygonId_2() { return static_cast<int32_t>(offsetof(sGrassConfig_t73951773, ___nPolygonId_2)); }
	inline int32_t get_nPolygonId_2() const { return ___nPolygonId_2; }
	inline int32_t* get_address_of_nPolygonId_2() { return &___nPolygonId_2; }
	inline void set_nPolygonId_2(int32_t value)
	{
		___nPolygonId_2 = value;
	}

	inline static int32_t get_offset_of_szColor_3() { return static_cast<int32_t>(offsetof(sGrassConfig_t73951773, ___szColor_3)); }
	inline String_t* get_szColor_3() const { return ___szColor_3; }
	inline String_t** get_address_of_szColor_3() { return &___szColor_3; }
	inline void set_szColor_3(String_t* value)
	{
		___szColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___szColor_3), value);
	}

	inline static int32_t get_offset_of_aryValue_4() { return static_cast<int32_t>(offsetof(sGrassConfig_t73951773, ___aryValue_4)); }
	inline SingleU5BU5D_t1444911251* get_aryValue_4() const { return ___aryValue_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_aryValue_4() { return &___aryValue_4; }
	inline void set_aryValue_4(SingleU5BU5D_t1444911251* value)
	{
		___aryValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___aryValue_4), value);
	}

	inline static int32_t get_offset_of_aryDesc_5() { return static_cast<int32_t>(offsetof(sGrassConfig_t73951773, ___aryDesc_5)); }
	inline StringU5BU5D_t1281789340* get_aryDesc_5() const { return ___aryDesc_5; }
	inline StringU5BU5D_t1281789340** get_address_of_aryDesc_5() { return &___aryDesc_5; }
	inline void set_aryDesc_5(StringU5BU5D_t1281789340* value)
	{
		___aryDesc_5 = value;
		Il2CppCodeGenWriteBarrier((&___aryDesc_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CGrassEditor/sGrassConfig
struct sGrassConfig_t73951773_marshaled_pinvoke
{
	int32_t ___nId_0;
	int32_t ___nFunc_1;
	int32_t ___nPolygonId_2;
	char* ___szColor_3;
	float* ___aryValue_4;
	char** ___aryDesc_5;
};
// Native definition for COM marshalling of CGrassEditor/sGrassConfig
struct sGrassConfig_t73951773_marshaled_com
{
	int32_t ___nId_0;
	int32_t ___nFunc_1;
	int32_t ___nPolygonId_2;
	Il2CppChar* ___szColor_3;
	float* ___aryValue_4;
	Il2CppChar** ___aryDesc_5;
};
#endif // SGRASSCONFIG_T73951773_H
#ifndef SGRASSINFO_T1501140634_H
#define SGRASSINFO_T1501140634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrassEditor/sGrassInfo
struct  sGrassInfo_t1501140634 
{
public:
	// System.Int32 CGrassEditor/sGrassInfo::nConfigId
	int32_t ___nConfigId_0;
	// System.Single CGrassEditor/sGrassInfo::fScaleX
	float ___fScaleX_1;
	// System.Single CGrassEditor/sGrassInfo::fScaleY
	float ___fScaleY_2;
	// System.Single CGrassEditor/sGrassInfo::fRotation
	float ___fRotation_3;

public:
	inline static int32_t get_offset_of_nConfigId_0() { return static_cast<int32_t>(offsetof(sGrassInfo_t1501140634, ___nConfigId_0)); }
	inline int32_t get_nConfigId_0() const { return ___nConfigId_0; }
	inline int32_t* get_address_of_nConfigId_0() { return &___nConfigId_0; }
	inline void set_nConfigId_0(int32_t value)
	{
		___nConfigId_0 = value;
	}

	inline static int32_t get_offset_of_fScaleX_1() { return static_cast<int32_t>(offsetof(sGrassInfo_t1501140634, ___fScaleX_1)); }
	inline float get_fScaleX_1() const { return ___fScaleX_1; }
	inline float* get_address_of_fScaleX_1() { return &___fScaleX_1; }
	inline void set_fScaleX_1(float value)
	{
		___fScaleX_1 = value;
	}

	inline static int32_t get_offset_of_fScaleY_2() { return static_cast<int32_t>(offsetof(sGrassInfo_t1501140634, ___fScaleY_2)); }
	inline float get_fScaleY_2() const { return ___fScaleY_2; }
	inline float* get_address_of_fScaleY_2() { return &___fScaleY_2; }
	inline void set_fScaleY_2(float value)
	{
		___fScaleY_2 = value;
	}

	inline static int32_t get_offset_of_fRotation_3() { return static_cast<int32_t>(offsetof(sGrassInfo_t1501140634, ___fRotation_3)); }
	inline float get_fRotation_3() const { return ___fRotation_3; }
	inline float* get_address_of_fRotation_3() { return &___fRotation_3; }
	inline void set_fRotation_3(float value)
	{
		___fRotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SGRASSINFO_T1501140634_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SSKILLPARAM_T731668951_H
#define SSKILLPARAM_T731668951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/sSkillParam
struct  sSkillParam_t731668951 
{
public:
	// System.Int32 CSkillSystem/sSkillParam::nCurLevel
	int32_t ___nCurLevel_0;
	// System.Boolean CSkillSystem/sSkillParam::bAvailable
	bool ___bAvailable_1;
	// System.Single[] CSkillSystem/sSkillParam::aryValues
	SingleU5BU5D_t1444911251* ___aryValues_2;
	// System.String CSkillSystem/sSkillParam::szValue
	String_t* ___szValue_3;

public:
	inline static int32_t get_offset_of_nCurLevel_0() { return static_cast<int32_t>(offsetof(sSkillParam_t731668951, ___nCurLevel_0)); }
	inline int32_t get_nCurLevel_0() const { return ___nCurLevel_0; }
	inline int32_t* get_address_of_nCurLevel_0() { return &___nCurLevel_0; }
	inline void set_nCurLevel_0(int32_t value)
	{
		___nCurLevel_0 = value;
	}

	inline static int32_t get_offset_of_bAvailable_1() { return static_cast<int32_t>(offsetof(sSkillParam_t731668951, ___bAvailable_1)); }
	inline bool get_bAvailable_1() const { return ___bAvailable_1; }
	inline bool* get_address_of_bAvailable_1() { return &___bAvailable_1; }
	inline void set_bAvailable_1(bool value)
	{
		___bAvailable_1 = value;
	}

	inline static int32_t get_offset_of_aryValues_2() { return static_cast<int32_t>(offsetof(sSkillParam_t731668951, ___aryValues_2)); }
	inline SingleU5BU5D_t1444911251* get_aryValues_2() const { return ___aryValues_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_aryValues_2() { return &___aryValues_2; }
	inline void set_aryValues_2(SingleU5BU5D_t1444911251* value)
	{
		___aryValues_2 = value;
		Il2CppCodeGenWriteBarrier((&___aryValues_2), value);
	}

	inline static int32_t get_offset_of_szValue_3() { return static_cast<int32_t>(offsetof(sSkillParam_t731668951, ___szValue_3)); }
	inline String_t* get_szValue_3() const { return ___szValue_3; }
	inline String_t** get_address_of_szValue_3() { return &___szValue_3; }
	inline void set_szValue_3(String_t* value)
	{
		___szValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___szValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CSkillSystem/sSkillParam
struct sSkillParam_t731668951_marshaled_pinvoke
{
	int32_t ___nCurLevel_0;
	int32_t ___bAvailable_1;
	float* ___aryValues_2;
	char* ___szValue_3;
};
// Native definition for COM marshalling of CSkillSystem/sSkillParam
struct sSkillParam_t731668951_marshaled_com
{
	int32_t ___nCurLevel_0;
	int32_t ___bAvailable_1;
	float* ___aryValues_2;
	Il2CppChar* ___szValue_3;
};
#endif // SSKILLPARAM_T731668951_H
#ifndef SSPITSPOEREPARAM_T3807642459_H
#define SSPITSPOEREPARAM_T3807642459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/sSpitSpoereParam
struct  sSpitSpoereParam_t3807642459 
{
public:
	// System.Single CSkillSystem/sSpitSpoereParam::fRadiusMultiple
	float ___fRadiusMultiple_0;
	// System.Single CSkillSystem/sSpitSpoereParam::fCostMotherVolumePercent
	float ___fCostMotherVolumePercent_1;
	// System.Int32 CSkillSystem/sSpitSpoereParam::nNumPerSecond
	int32_t ___nNumPerSecond_2;
	// System.Single CSkillSystem/sSpitSpoereParam::fSpitInterval
	float ___fSpitInterval_3;
	// System.Single CSkillSystem/sSpitSpoereParam::fEjectSpeed
	float ___fEjectSpeed_4;
	// System.Single CSkillSystem/sSpitSpoereParam::fPushThornDis
	float ___fPushThornDis_5;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeMinVolume
	float ___fSporeMinVolume_6;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeMaxVolume
	float ___fSporeMaxVolume_7;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeMaxShowRadius
	float ___fSporeMaxShowRadius_8;
	// System.Single CSkillSystem/sSpitSpoereParam::fHuanTing
	float ___fHuanTing_9;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeA
	float ___fSporeA_10;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeX
	float ___fSporeX_11;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeB
	float ___fSporeB_12;
	// System.Single CSkillSystem/sSpitSpoereParam::fSporeC
	float ___fSporeC_13;

public:
	inline static int32_t get_offset_of_fRadiusMultiple_0() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fRadiusMultiple_0)); }
	inline float get_fRadiusMultiple_0() const { return ___fRadiusMultiple_0; }
	inline float* get_address_of_fRadiusMultiple_0() { return &___fRadiusMultiple_0; }
	inline void set_fRadiusMultiple_0(float value)
	{
		___fRadiusMultiple_0 = value;
	}

	inline static int32_t get_offset_of_fCostMotherVolumePercent_1() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fCostMotherVolumePercent_1)); }
	inline float get_fCostMotherVolumePercent_1() const { return ___fCostMotherVolumePercent_1; }
	inline float* get_address_of_fCostMotherVolumePercent_1() { return &___fCostMotherVolumePercent_1; }
	inline void set_fCostMotherVolumePercent_1(float value)
	{
		___fCostMotherVolumePercent_1 = value;
	}

	inline static int32_t get_offset_of_nNumPerSecond_2() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___nNumPerSecond_2)); }
	inline int32_t get_nNumPerSecond_2() const { return ___nNumPerSecond_2; }
	inline int32_t* get_address_of_nNumPerSecond_2() { return &___nNumPerSecond_2; }
	inline void set_nNumPerSecond_2(int32_t value)
	{
		___nNumPerSecond_2 = value;
	}

	inline static int32_t get_offset_of_fSpitInterval_3() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSpitInterval_3)); }
	inline float get_fSpitInterval_3() const { return ___fSpitInterval_3; }
	inline float* get_address_of_fSpitInterval_3() { return &___fSpitInterval_3; }
	inline void set_fSpitInterval_3(float value)
	{
		___fSpitInterval_3 = value;
	}

	inline static int32_t get_offset_of_fEjectSpeed_4() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fEjectSpeed_4)); }
	inline float get_fEjectSpeed_4() const { return ___fEjectSpeed_4; }
	inline float* get_address_of_fEjectSpeed_4() { return &___fEjectSpeed_4; }
	inline void set_fEjectSpeed_4(float value)
	{
		___fEjectSpeed_4 = value;
	}

	inline static int32_t get_offset_of_fPushThornDis_5() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fPushThornDis_5)); }
	inline float get_fPushThornDis_5() const { return ___fPushThornDis_5; }
	inline float* get_address_of_fPushThornDis_5() { return &___fPushThornDis_5; }
	inline void set_fPushThornDis_5(float value)
	{
		___fPushThornDis_5 = value;
	}

	inline static int32_t get_offset_of_fSporeMinVolume_6() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeMinVolume_6)); }
	inline float get_fSporeMinVolume_6() const { return ___fSporeMinVolume_6; }
	inline float* get_address_of_fSporeMinVolume_6() { return &___fSporeMinVolume_6; }
	inline void set_fSporeMinVolume_6(float value)
	{
		___fSporeMinVolume_6 = value;
	}

	inline static int32_t get_offset_of_fSporeMaxVolume_7() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeMaxVolume_7)); }
	inline float get_fSporeMaxVolume_7() const { return ___fSporeMaxVolume_7; }
	inline float* get_address_of_fSporeMaxVolume_7() { return &___fSporeMaxVolume_7; }
	inline void set_fSporeMaxVolume_7(float value)
	{
		___fSporeMaxVolume_7 = value;
	}

	inline static int32_t get_offset_of_fSporeMaxShowRadius_8() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeMaxShowRadius_8)); }
	inline float get_fSporeMaxShowRadius_8() const { return ___fSporeMaxShowRadius_8; }
	inline float* get_address_of_fSporeMaxShowRadius_8() { return &___fSporeMaxShowRadius_8; }
	inline void set_fSporeMaxShowRadius_8(float value)
	{
		___fSporeMaxShowRadius_8 = value;
	}

	inline static int32_t get_offset_of_fHuanTing_9() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fHuanTing_9)); }
	inline float get_fHuanTing_9() const { return ___fHuanTing_9; }
	inline float* get_address_of_fHuanTing_9() { return &___fHuanTing_9; }
	inline void set_fHuanTing_9(float value)
	{
		___fHuanTing_9 = value;
	}

	inline static int32_t get_offset_of_fSporeA_10() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeA_10)); }
	inline float get_fSporeA_10() const { return ___fSporeA_10; }
	inline float* get_address_of_fSporeA_10() { return &___fSporeA_10; }
	inline void set_fSporeA_10(float value)
	{
		___fSporeA_10 = value;
	}

	inline static int32_t get_offset_of_fSporeX_11() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeX_11)); }
	inline float get_fSporeX_11() const { return ___fSporeX_11; }
	inline float* get_address_of_fSporeX_11() { return &___fSporeX_11; }
	inline void set_fSporeX_11(float value)
	{
		___fSporeX_11 = value;
	}

	inline static int32_t get_offset_of_fSporeB_12() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeB_12)); }
	inline float get_fSporeB_12() const { return ___fSporeB_12; }
	inline float* get_address_of_fSporeB_12() { return &___fSporeB_12; }
	inline void set_fSporeB_12(float value)
	{
		___fSporeB_12 = value;
	}

	inline static int32_t get_offset_of_fSporeC_13() { return static_cast<int32_t>(offsetof(sSpitSpoereParam_t3807642459, ___fSporeC_13)); }
	inline float get_fSporeC_13() const { return ___fSporeC_13; }
	inline float* get_address_of_fSporeC_13() { return &___fSporeC_13; }
	inline void set_fSporeC_13(float value)
	{
		___fSporeC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSPITSPOEREPARAM_T3807642459_H
#ifndef SSKILLINFO_T192725586_H
#define SSKILLINFO_T192725586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/sSkillInfo
struct  sSkillInfo_t192725586 
{
public:
	// System.Int32 CSkillSystem/sSkillInfo::nSkillId
	int32_t ___nSkillId_0;
	// System.Int32 CSkillSystem/sSkillInfo::nMaxPoint
	int32_t ___nMaxPoint_1;
	// System.Int32 CSkillSystem/sSkillInfo::nCurPoint
	int32_t ___nCurPoint_2;

public:
	inline static int32_t get_offset_of_nSkillId_0() { return static_cast<int32_t>(offsetof(sSkillInfo_t192725586, ___nSkillId_0)); }
	inline int32_t get_nSkillId_0() const { return ___nSkillId_0; }
	inline int32_t* get_address_of_nSkillId_0() { return &___nSkillId_0; }
	inline void set_nSkillId_0(int32_t value)
	{
		___nSkillId_0 = value;
	}

	inline static int32_t get_offset_of_nMaxPoint_1() { return static_cast<int32_t>(offsetof(sSkillInfo_t192725586, ___nMaxPoint_1)); }
	inline int32_t get_nMaxPoint_1() const { return ___nMaxPoint_1; }
	inline int32_t* get_address_of_nMaxPoint_1() { return &___nMaxPoint_1; }
	inline void set_nMaxPoint_1(int32_t value)
	{
		___nMaxPoint_1 = value;
	}

	inline static int32_t get_offset_of_nCurPoint_2() { return static_cast<int32_t>(offsetof(sSkillInfo_t192725586, ___nCurPoint_2)); }
	inline int32_t get_nCurPoint_2() const { return ___nCurPoint_2; }
	inline int32_t* get_address_of_nCurPoint_2() { return &___nCurPoint_2; }
	inline void set_nCurPoint_2(int32_t value)
	{
		___nCurPoint_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSKILLINFO_T192725586_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef STHORNOFTHISCLASSCONFIG_T350464872_H
#define STHORNOFTHISCLASSCONFIG_T350464872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CClassEditor/sThornOfThisClassConfig
struct  sThornOfThisClassConfig_t350464872 
{
public:
	// System.String CClassEditor/sThornOfThisClassConfig::szThornId
	String_t* ___szThornId_0;
	// System.Single CClassEditor/sThornOfThisClassConfig::fDensity
	float ___fDensity_1;
	// System.Single CClassEditor/sThornOfThisClassConfig::fRebornTime
	float ___fRebornTime_2;

public:
	inline static int32_t get_offset_of_szThornId_0() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___szThornId_0)); }
	inline String_t* get_szThornId_0() const { return ___szThornId_0; }
	inline String_t** get_address_of_szThornId_0() { return &___szThornId_0; }
	inline void set_szThornId_0(String_t* value)
	{
		___szThornId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szThornId_0), value);
	}

	inline static int32_t get_offset_of_fDensity_1() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___fDensity_1)); }
	inline float get_fDensity_1() const { return ___fDensity_1; }
	inline float* get_address_of_fDensity_1() { return &___fDensity_1; }
	inline void set_fDensity_1(float value)
	{
		___fDensity_1 = value;
	}

	inline static int32_t get_offset_of_fRebornTime_2() { return static_cast<int32_t>(offsetof(sThornOfThisClassConfig_t350464872, ___fRebornTime_2)); }
	inline float get_fRebornTime_2() const { return ___fRebornTime_2; }
	inline float* get_address_of_fRebornTime_2() { return &___fRebornTime_2; }
	inline void set_fRebornTime_2(float value)
	{
		___fRebornTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CClassEditor/sThornOfThisClassConfig
struct sThornOfThisClassConfig_t350464872_marshaled_pinvoke
{
	char* ___szThornId_0;
	float ___fDensity_1;
	float ___fRebornTime_2;
};
// Native definition for COM marshalling of CClassEditor/sThornOfThisClassConfig
struct sThornOfThisClassConfig_t350464872_marshaled_com
{
	Il2CppChar* ___szThornId_0;
	float ___fDensity_1;
	float ___fRebornTime_2;
};
#endif // STHORNOFTHISCLASSCONFIG_T350464872_H
#ifndef STHORNCONFIG_T1242444451_H
#define STHORNCONFIG_T1242444451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/sThornConfig
struct  sThornConfig_t1242444451 
{
public:
	// System.String CMonsterEditor/sThornConfig::szId
	String_t* ___szId_0;
	// System.Int32 CMonsterEditor/sThornConfig::nType
	int32_t ___nType_1;
	// System.Single CMonsterEditor/sThornConfig::fFoodSize
	float ___fFoodSize_2;
	// System.String CMonsterEditor/sThornConfig::szColor
	String_t* ___szColor_3;
	// System.String CMonsterEditor/sThornConfig::szDesc
	String_t* ___szDesc_4;
	// System.Single CMonsterEditor/sThornConfig::fSelfSize
	float ___fSelfSize_5;
	// System.Single CMonsterEditor/sThornConfig::fExp
	float ___fExp_6;
	// System.Single CMonsterEditor/sThornConfig::fMoney
	float ___fMoney_7;
	// System.Single CMonsterEditor/sThornConfig::fBuffId
	float ___fBuffId_8;
	// System.Single CMonsterEditor/sThornConfig::fExplodeChildNum
	float ___fExplodeChildNum_9;
	// System.Single CMonsterEditor/sThornConfig::fExplodeMotherLeftPercent
	float ___fExplodeMotherLeftPercent_10;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunDistance
	float ___fExplodeRunDistance_11;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunTime
	float ___fExplodeRunTime_12;
	// System.Single CMonsterEditor/sThornConfig::fExplodeStayTime
	float ___fExplodeStayTime_13;
	// System.Single CMonsterEditor/sThornConfig::fExplodeShellTime
	float ___fExplodeShellTime_14;
	// System.Single CMonsterEditor/sThornConfig::fExplodeFormationId
	float ___fExplodeFormationId_15;
	// System.Int32 CMonsterEditor/sThornConfig::nSkillId
	int32_t ___nSkillId_16;

public:
	inline static int32_t get_offset_of_szId_0() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szId_0)); }
	inline String_t* get_szId_0() const { return ___szId_0; }
	inline String_t** get_address_of_szId_0() { return &___szId_0; }
	inline void set_szId_0(String_t* value)
	{
		___szId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szId_0), value);
	}

	inline static int32_t get_offset_of_nType_1() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nType_1)); }
	inline int32_t get_nType_1() const { return ___nType_1; }
	inline int32_t* get_address_of_nType_1() { return &___nType_1; }
	inline void set_nType_1(int32_t value)
	{
		___nType_1 = value;
	}

	inline static int32_t get_offset_of_fFoodSize_2() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fFoodSize_2)); }
	inline float get_fFoodSize_2() const { return ___fFoodSize_2; }
	inline float* get_address_of_fFoodSize_2() { return &___fFoodSize_2; }
	inline void set_fFoodSize_2(float value)
	{
		___fFoodSize_2 = value;
	}

	inline static int32_t get_offset_of_szColor_3() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szColor_3)); }
	inline String_t* get_szColor_3() const { return ___szColor_3; }
	inline String_t** get_address_of_szColor_3() { return &___szColor_3; }
	inline void set_szColor_3(String_t* value)
	{
		___szColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___szColor_3), value);
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_fSelfSize_5() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fSelfSize_5)); }
	inline float get_fSelfSize_5() const { return ___fSelfSize_5; }
	inline float* get_address_of_fSelfSize_5() { return &___fSelfSize_5; }
	inline void set_fSelfSize_5(float value)
	{
		___fSelfSize_5 = value;
	}

	inline static int32_t get_offset_of_fExp_6() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExp_6)); }
	inline float get_fExp_6() const { return ___fExp_6; }
	inline float* get_address_of_fExp_6() { return &___fExp_6; }
	inline void set_fExp_6(float value)
	{
		___fExp_6 = value;
	}

	inline static int32_t get_offset_of_fMoney_7() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fMoney_7)); }
	inline float get_fMoney_7() const { return ___fMoney_7; }
	inline float* get_address_of_fMoney_7() { return &___fMoney_7; }
	inline void set_fMoney_7(float value)
	{
		___fMoney_7 = value;
	}

	inline static int32_t get_offset_of_fBuffId_8() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fBuffId_8)); }
	inline float get_fBuffId_8() const { return ___fBuffId_8; }
	inline float* get_address_of_fBuffId_8() { return &___fBuffId_8; }
	inline void set_fBuffId_8(float value)
	{
		___fBuffId_8 = value;
	}

	inline static int32_t get_offset_of_fExplodeChildNum_9() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeChildNum_9)); }
	inline float get_fExplodeChildNum_9() const { return ___fExplodeChildNum_9; }
	inline float* get_address_of_fExplodeChildNum_9() { return &___fExplodeChildNum_9; }
	inline void set_fExplodeChildNum_9(float value)
	{
		___fExplodeChildNum_9 = value;
	}

	inline static int32_t get_offset_of_fExplodeMotherLeftPercent_10() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeMotherLeftPercent_10)); }
	inline float get_fExplodeMotherLeftPercent_10() const { return ___fExplodeMotherLeftPercent_10; }
	inline float* get_address_of_fExplodeMotherLeftPercent_10() { return &___fExplodeMotherLeftPercent_10; }
	inline void set_fExplodeMotherLeftPercent_10(float value)
	{
		___fExplodeMotherLeftPercent_10 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunDistance_11() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunDistance_11)); }
	inline float get_fExplodeRunDistance_11() const { return ___fExplodeRunDistance_11; }
	inline float* get_address_of_fExplodeRunDistance_11() { return &___fExplodeRunDistance_11; }
	inline void set_fExplodeRunDistance_11(float value)
	{
		___fExplodeRunDistance_11 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunTime_12() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunTime_12)); }
	inline float get_fExplodeRunTime_12() const { return ___fExplodeRunTime_12; }
	inline float* get_address_of_fExplodeRunTime_12() { return &___fExplodeRunTime_12; }
	inline void set_fExplodeRunTime_12(float value)
	{
		___fExplodeRunTime_12 = value;
	}

	inline static int32_t get_offset_of_fExplodeStayTime_13() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeStayTime_13)); }
	inline float get_fExplodeStayTime_13() const { return ___fExplodeStayTime_13; }
	inline float* get_address_of_fExplodeStayTime_13() { return &___fExplodeStayTime_13; }
	inline void set_fExplodeStayTime_13(float value)
	{
		___fExplodeStayTime_13 = value;
	}

	inline static int32_t get_offset_of_fExplodeShellTime_14() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeShellTime_14)); }
	inline float get_fExplodeShellTime_14() const { return ___fExplodeShellTime_14; }
	inline float* get_address_of_fExplodeShellTime_14() { return &___fExplodeShellTime_14; }
	inline void set_fExplodeShellTime_14(float value)
	{
		___fExplodeShellTime_14 = value;
	}

	inline static int32_t get_offset_of_fExplodeFormationId_15() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeFormationId_15)); }
	inline float get_fExplodeFormationId_15() const { return ___fExplodeFormationId_15; }
	inline float* get_address_of_fExplodeFormationId_15() { return &___fExplodeFormationId_15; }
	inline void set_fExplodeFormationId_15(float value)
	{
		___fExplodeFormationId_15 = value;
	}

	inline static int32_t get_offset_of_nSkillId_16() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nSkillId_16)); }
	inline int32_t get_nSkillId_16() const { return ___nSkillId_16; }
	inline int32_t* get_address_of_nSkillId_16() { return &___nSkillId_16; }
	inline void set_nSkillId_16(int32_t value)
	{
		___nSkillId_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_pinvoke
{
	char* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	char* ___szColor_3;
	char* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
// Native definition for COM marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_com
{
	Il2CppChar* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	Il2CppChar* ___szColor_3;
	Il2CppChar* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
#endif // STHORNCONFIG_T1242444451_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SSPRAYCONFIG_T1865836932_H
#define SSPRAYCONFIG_T1865836932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpryEditor/sSprayConfig
struct  sSprayConfig_t1865836932 
{
public:
	// System.Int32 CSpryEditor/sSprayConfig::nConfigId
	int32_t ___nConfigId_0;
	// System.String CSpryEditor/sSprayConfig::szThornId
	String_t* ___szThornId_1;
	// System.Single CSpryEditor/sSprayConfig::fDensity
	float ___fDensity_2;
	// System.Single CSpryEditor/sSprayConfig::fMaxDis
	float ___fMaxDis_3;
	// System.Single CSpryEditor/sSprayConfig::fMinDis
	float ___fMinDis_4;
	// System.Single CSpryEditor/sSprayConfig::fLifeTime
	float ___fLifeTime_5;
	// System.Single CSpryEditor/sSprayConfig::fInterval
	float ___fInterval_6;
	// System.Single CSpryEditor/sSprayConfig::fSpraySize
	float ___fSpraySize_7;

public:
	inline static int32_t get_offset_of_nConfigId_0() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___nConfigId_0)); }
	inline int32_t get_nConfigId_0() const { return ___nConfigId_0; }
	inline int32_t* get_address_of_nConfigId_0() { return &___nConfigId_0; }
	inline void set_nConfigId_0(int32_t value)
	{
		___nConfigId_0 = value;
	}

	inline static int32_t get_offset_of_szThornId_1() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___szThornId_1)); }
	inline String_t* get_szThornId_1() const { return ___szThornId_1; }
	inline String_t** get_address_of_szThornId_1() { return &___szThornId_1; }
	inline void set_szThornId_1(String_t* value)
	{
		___szThornId_1 = value;
		Il2CppCodeGenWriteBarrier((&___szThornId_1), value);
	}

	inline static int32_t get_offset_of_fDensity_2() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___fDensity_2)); }
	inline float get_fDensity_2() const { return ___fDensity_2; }
	inline float* get_address_of_fDensity_2() { return &___fDensity_2; }
	inline void set_fDensity_2(float value)
	{
		___fDensity_2 = value;
	}

	inline static int32_t get_offset_of_fMaxDis_3() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___fMaxDis_3)); }
	inline float get_fMaxDis_3() const { return ___fMaxDis_3; }
	inline float* get_address_of_fMaxDis_3() { return &___fMaxDis_3; }
	inline void set_fMaxDis_3(float value)
	{
		___fMaxDis_3 = value;
	}

	inline static int32_t get_offset_of_fMinDis_4() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___fMinDis_4)); }
	inline float get_fMinDis_4() const { return ___fMinDis_4; }
	inline float* get_address_of_fMinDis_4() { return &___fMinDis_4; }
	inline void set_fMinDis_4(float value)
	{
		___fMinDis_4 = value;
	}

	inline static int32_t get_offset_of_fLifeTime_5() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___fLifeTime_5)); }
	inline float get_fLifeTime_5() const { return ___fLifeTime_5; }
	inline float* get_address_of_fLifeTime_5() { return &___fLifeTime_5; }
	inline void set_fLifeTime_5(float value)
	{
		___fLifeTime_5 = value;
	}

	inline static int32_t get_offset_of_fInterval_6() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___fInterval_6)); }
	inline float get_fInterval_6() const { return ___fInterval_6; }
	inline float* get_address_of_fInterval_6() { return &___fInterval_6; }
	inline void set_fInterval_6(float value)
	{
		___fInterval_6 = value;
	}

	inline static int32_t get_offset_of_fSpraySize_7() { return static_cast<int32_t>(offsetof(sSprayConfig_t1865836932, ___fSpraySize_7)); }
	inline float get_fSpraySize_7() const { return ___fSpraySize_7; }
	inline float* get_address_of_fSpraySize_7() { return &___fSpraySize_7; }
	inline void set_fSpraySize_7(float value)
	{
		___fSpraySize_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CSpryEditor/sSprayConfig
struct sSprayConfig_t1865836932_marshaled_pinvoke
{
	int32_t ___nConfigId_0;
	char* ___szThornId_1;
	float ___fDensity_2;
	float ___fMaxDis_3;
	float ___fMinDis_4;
	float ___fLifeTime_5;
	float ___fInterval_6;
	float ___fSpraySize_7;
};
// Native definition for COM marshalling of CSpryEditor/sSprayConfig
struct sSprayConfig_t1865836932_marshaled_com
{
	int32_t ___nConfigId_0;
	Il2CppChar* ___szThornId_1;
	float ___fDensity_2;
	float ___fMaxDis_3;
	float ___fMinDis_4;
	float ___fLifeTime_5;
	float ___fInterval_6;
	float ___fSpraySize_7;
};
#endif // SSPRAYCONFIG_T1865836932_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SBEANREBORNINFO_T2785650986_H
#define SBEANREBORNINFO_T2785650986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBeanCollection/sBeanRebornInfo
struct  sBeanRebornInfo_t2785650986 
{
public:
	// System.UInt16 CBeanCollection/sBeanRebornInfo::collection_guid
	uint16_t ___collection_guid_0;
	// System.UInt16 CBeanCollection/sBeanRebornInfo::bean_idx
	uint16_t ___bean_idx_1;
	// System.Single CBeanCollection/sBeanRebornInfo::fDeadTime
	float ___fDeadTime_2;
	// System.Single CBeanCollection/sBeanRebornInfo::fRebornInterval
	float ___fRebornInterval_3;

public:
	inline static int32_t get_offset_of_collection_guid_0() { return static_cast<int32_t>(offsetof(sBeanRebornInfo_t2785650986, ___collection_guid_0)); }
	inline uint16_t get_collection_guid_0() const { return ___collection_guid_0; }
	inline uint16_t* get_address_of_collection_guid_0() { return &___collection_guid_0; }
	inline void set_collection_guid_0(uint16_t value)
	{
		___collection_guid_0 = value;
	}

	inline static int32_t get_offset_of_bean_idx_1() { return static_cast<int32_t>(offsetof(sBeanRebornInfo_t2785650986, ___bean_idx_1)); }
	inline uint16_t get_bean_idx_1() const { return ___bean_idx_1; }
	inline uint16_t* get_address_of_bean_idx_1() { return &___bean_idx_1; }
	inline void set_bean_idx_1(uint16_t value)
	{
		___bean_idx_1 = value;
	}

	inline static int32_t get_offset_of_fDeadTime_2() { return static_cast<int32_t>(offsetof(sBeanRebornInfo_t2785650986, ___fDeadTime_2)); }
	inline float get_fDeadTime_2() const { return ___fDeadTime_2; }
	inline float* get_address_of_fDeadTime_2() { return &___fDeadTime_2; }
	inline void set_fDeadTime_2(float value)
	{
		___fDeadTime_2 = value;
	}

	inline static int32_t get_offset_of_fRebornInterval_3() { return static_cast<int32_t>(offsetof(sBeanRebornInfo_t2785650986, ___fRebornInterval_3)); }
	inline float get_fRebornInterval_3() const { return ___fRebornInterval_3; }
	inline float* get_address_of_fRebornInterval_3() { return &___fRebornInterval_3; }
	inline void set_fRebornInterval_3(float value)
	{
		___fRebornInterval_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBEANREBORNINFO_T2785650986_H
#ifndef SACCOUNTINFO_T751877243_H
#define SACCOUNTINFO_T751877243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister/sAccountInfo
struct  sAccountInfo_t751877243 
{
public:
	// System.String CRegister/sAccountInfo::szPhoneNum
	String_t* ___szPhoneNum_0;
	// System.String CRegister/sAccountInfo::szSessionId
	String_t* ___szSessionId_1;

public:
	inline static int32_t get_offset_of_szPhoneNum_0() { return static_cast<int32_t>(offsetof(sAccountInfo_t751877243, ___szPhoneNum_0)); }
	inline String_t* get_szPhoneNum_0() const { return ___szPhoneNum_0; }
	inline String_t** get_address_of_szPhoneNum_0() { return &___szPhoneNum_0; }
	inline void set_szPhoneNum_0(String_t* value)
	{
		___szPhoneNum_0 = value;
		Il2CppCodeGenWriteBarrier((&___szPhoneNum_0), value);
	}

	inline static int32_t get_offset_of_szSessionId_1() { return static_cast<int32_t>(offsetof(sAccountInfo_t751877243, ___szSessionId_1)); }
	inline String_t* get_szSessionId_1() const { return ___szSessionId_1; }
	inline String_t** get_address_of_szSessionId_1() { return &___szSessionId_1; }
	inline void set_szSessionId_1(String_t* value)
	{
		___szSessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___szSessionId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CRegister/sAccountInfo
struct sAccountInfo_t751877243_marshaled_pinvoke
{
	char* ___szPhoneNum_0;
	char* ___szSessionId_1;
};
// Native definition for COM marshalling of CRegister/sAccountInfo
struct sAccountInfo_t751877243_marshaled_com
{
	Il2CppChar* ___szPhoneNum_0;
	Il2CppChar* ___szSessionId_1;
};
#endif // SACCOUNTINFO_T751877243_H
#ifndef SBEANSYNCINFO_T1176654545_H
#define SBEANSYNCINFO_T1176654545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBeanCollection/sBeanSyncInfo
struct  sBeanSyncInfo_t1176654545 
{
public:
	// System.UInt16 CBeanCollection/sBeanSyncInfo::collection_guid
	uint16_t ___collection_guid_0;
	// System.UInt16 CBeanCollection/sBeanSyncInfo::bean_idx
	uint16_t ___bean_idx_1;
	// System.Single CBeanCollection/sBeanSyncInfo::fDeadTime
	float ___fDeadTime_2;

public:
	inline static int32_t get_offset_of_collection_guid_0() { return static_cast<int32_t>(offsetof(sBeanSyncInfo_t1176654545, ___collection_guid_0)); }
	inline uint16_t get_collection_guid_0() const { return ___collection_guid_0; }
	inline uint16_t* get_address_of_collection_guid_0() { return &___collection_guid_0; }
	inline void set_collection_guid_0(uint16_t value)
	{
		___collection_guid_0 = value;
	}

	inline static int32_t get_offset_of_bean_idx_1() { return static_cast<int32_t>(offsetof(sBeanSyncInfo_t1176654545, ___bean_idx_1)); }
	inline uint16_t get_bean_idx_1() const { return ___bean_idx_1; }
	inline uint16_t* get_address_of_bean_idx_1() { return &___bean_idx_1; }
	inline void set_bean_idx_1(uint16_t value)
	{
		___bean_idx_1 = value;
	}

	inline static int32_t get_offset_of_fDeadTime_2() { return static_cast<int32_t>(offsetof(sBeanSyncInfo_t1176654545, ___fDeadTime_2)); }
	inline float get_fDeadTime_2() const { return ___fDeadTime_2; }
	inline float* get_address_of_fDeadTime_2() { return &___fDeadTime_2; }
	inline void set_fDeadTime_2(float value)
	{
		___fDeadTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBEANSYNCINFO_T1176654545_H
#ifndef SSKILLDETAIL_T1561700181_H
#define SSKILLDETAIL_T1561700181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectSkill/sSkillDetail
struct  sSkillDetail_t1561700181 
{
public:
	// System.Single CSelectSkill/sSkillDetail::fSpeedAffect
	float ___fSpeedAffect_0;
	// System.Single CSelectSkill/sSkillDetail::fMpCost
	float ___fMpCost_1;
	// System.Single CSelectSkill/sSkillDetail::fDuration
	float ___fDuration_2;
	// System.Single CSelectSkill/sSkillDetail::fColdDown
	float ___fColdDown_3;
	// System.String[] CSelectSkill/sSkillDetail::aryValues
	StringU5BU5D_t1281789340* ___aryValues_4;

public:
	inline static int32_t get_offset_of_fSpeedAffect_0() { return static_cast<int32_t>(offsetof(sSkillDetail_t1561700181, ___fSpeedAffect_0)); }
	inline float get_fSpeedAffect_0() const { return ___fSpeedAffect_0; }
	inline float* get_address_of_fSpeedAffect_0() { return &___fSpeedAffect_0; }
	inline void set_fSpeedAffect_0(float value)
	{
		___fSpeedAffect_0 = value;
	}

	inline static int32_t get_offset_of_fMpCost_1() { return static_cast<int32_t>(offsetof(sSkillDetail_t1561700181, ___fMpCost_1)); }
	inline float get_fMpCost_1() const { return ___fMpCost_1; }
	inline float* get_address_of_fMpCost_1() { return &___fMpCost_1; }
	inline void set_fMpCost_1(float value)
	{
		___fMpCost_1 = value;
	}

	inline static int32_t get_offset_of_fDuration_2() { return static_cast<int32_t>(offsetof(sSkillDetail_t1561700181, ___fDuration_2)); }
	inline float get_fDuration_2() const { return ___fDuration_2; }
	inline float* get_address_of_fDuration_2() { return &___fDuration_2; }
	inline void set_fDuration_2(float value)
	{
		___fDuration_2 = value;
	}

	inline static int32_t get_offset_of_fColdDown_3() { return static_cast<int32_t>(offsetof(sSkillDetail_t1561700181, ___fColdDown_3)); }
	inline float get_fColdDown_3() const { return ___fColdDown_3; }
	inline float* get_address_of_fColdDown_3() { return &___fColdDown_3; }
	inline void set_fColdDown_3(float value)
	{
		___fColdDown_3 = value;
	}

	inline static int32_t get_offset_of_aryValues_4() { return static_cast<int32_t>(offsetof(sSkillDetail_t1561700181, ___aryValues_4)); }
	inline StringU5BU5D_t1281789340* get_aryValues_4() const { return ___aryValues_4; }
	inline StringU5BU5D_t1281789340** get_address_of_aryValues_4() { return &___aryValues_4; }
	inline void set_aryValues_4(StringU5BU5D_t1281789340* value)
	{
		___aryValues_4 = value;
		Il2CppCodeGenWriteBarrier((&___aryValues_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CSelectSkill/sSkillDetail
struct sSkillDetail_t1561700181_marshaled_pinvoke
{
	float ___fSpeedAffect_0;
	float ___fMpCost_1;
	float ___fDuration_2;
	float ___fColdDown_3;
	char** ___aryValues_4;
};
// Native definition for COM marshalling of CSelectSkill/sSkillDetail
struct sSkillDetail_t1561700181_marshaled_com
{
	float ___fSpeedAffect_0;
	float ___fMpCost_1;
	float ___fDuration_2;
	float ___fColdDown_3;
	Il2CppChar** ___aryValues_4;
};
#endif // SSKILLDETAIL_T1561700181_H
#ifndef PATHSEGMENT_T2930971005_H
#define PATHSEGMENT_T2930971005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.PathSegment
struct  PathSegment_t2930971005 
{
public:
	// UnityEngine.Vector3 Shapes2D.PathSegment::p0
	Vector3_t3722313464  ___p0_0;
	// UnityEngine.Vector3 Shapes2D.PathSegment::p1
	Vector3_t3722313464  ___p1_1;
	// UnityEngine.Vector3 Shapes2D.PathSegment::p2
	Vector3_t3722313464  ___p2_2;

public:
	inline static int32_t get_offset_of_p0_0() { return static_cast<int32_t>(offsetof(PathSegment_t2930971005, ___p0_0)); }
	inline Vector3_t3722313464  get_p0_0() const { return ___p0_0; }
	inline Vector3_t3722313464 * get_address_of_p0_0() { return &___p0_0; }
	inline void set_p0_0(Vector3_t3722313464  value)
	{
		___p0_0 = value;
	}

	inline static int32_t get_offset_of_p1_1() { return static_cast<int32_t>(offsetof(PathSegment_t2930971005, ___p1_1)); }
	inline Vector3_t3722313464  get_p1_1() const { return ___p1_1; }
	inline Vector3_t3722313464 * get_address_of_p1_1() { return &___p1_1; }
	inline void set_p1_1(Vector3_t3722313464  value)
	{
		___p1_1 = value;
	}

	inline static int32_t get_offset_of_p2_2() { return static_cast<int32_t>(offsetof(PathSegment_t2930971005, ___p2_2)); }
	inline Vector3_t3722313464  get_p2_2() const { return ___p2_2; }
	inline Vector3_t3722313464 * get_address_of_p2_2() { return &___p2_2; }
	inline void set_p2_2(Vector3_t3722313464  value)
	{
		___p2_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHSEGMENT_T2930971005_H
#ifndef POLYGONPRESET_T3238801606_H
#define POLYGONPRESET_T3238801606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.PolygonPreset
struct  PolygonPreset_t3238801606 
{
public:
	// System.Int32 Shapes2D.PolygonPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolygonPreset_t3238801606, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONPRESET_T3238801606_H
#ifndef GRADIENTAXIS_T2967222705_H
#define GRADIENTAXIS_T2967222705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.GradientAxis
struct  GradientAxis_t2967222705 
{
public:
	// System.Int32 Shapes2D.GradientAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GradientAxis_t2967222705, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTAXIS_T2967222705_H
#ifndef GRADIENTTYPE_T2325005612_H
#define GRADIENTTYPE_T2325005612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.GradientType
struct  GradientType_t2325005612 
{
public:
	// System.Int32 Shapes2D.GradientType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GradientType_t2325005612, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTTYPE_T2325005612_H
#ifndef FILLTYPE_T681241845_H
#define FILLTYPE_T681241845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.FillType
struct  FillType_t681241845 
{
public:
	// System.Int32 Shapes2D.FillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillType_t681241845, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLTYPE_T681241845_H
#ifndef SHAPETYPE_T1878054890_H
#define SHAPETYPE_T1878054890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.ShapeType
struct  ShapeType_t1878054890 
{
public:
	// System.Int32 Shapes2D.ShapeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShapeType_t1878054890, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPETYPE_T1878054890_H
#ifndef ECTRLTYPE_T1729920274_H
#define ECTRLTYPE_T1729920274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectSkill/eCtrlType
struct  eCtrlType_t1729920274 
{
public:
	// System.Int32 CSelectSkill/eCtrlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCtrlType_t1729920274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECTRLTYPE_T1729920274_H
#ifndef ECTRLTYPE_T553487726_H
#define ECTRLTYPE_T553487726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSettingsManager/eCtrlType
struct  eCtrlType_t553487726 
{
public:
	// System.Int32 CSettingsManager/eCtrlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCtrlType_t553487726, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECTRLTYPE_T553487726_H
#ifndef SCENESTATUS_T2403225508_H
#define SCENESTATUS_T2403225508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneStatus
struct  SceneStatus_t2403225508 
{
public:
	// System.Int32 SceneStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneStatus_t2403225508, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENESTATUS_T2403225508_H
#ifndef ECTRLTYPE_T499920991_H
#define ECTRLTYPE_T499920991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectRoomManager/eCtrlType
struct  eCtrlType_t499920991 
{
public:
	// System.Int32 CSelectRoomManager/eCtrlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCtrlType_t499920991, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECTRLTYPE_T499920991_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ELIGHTTYPE_T683687508_H
#define ELIGHTTYPE_T683687508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLightSystem/eLightType
struct  eLightType_t683687508 
{
public:
	// System.Int32 CLightSystem/eLightType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eLightType_t683687508, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELIGHTTYPE_T683687508_H
#ifndef ESKILLSYSPROGRESSBARTYPE_T1408520632_H
#define ESKILLSYSPROGRESSBARTYPE_T1408520632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/eSkillSysProgressBarType
struct  eSkillSysProgressBarType_t1408520632 
{
public:
	// System.Int32 CSkillSystem/eSkillSysProgressBarType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillSysProgressBarType_t1408520632, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLSYSPROGRESSBARTYPE_T1408520632_H
#ifndef ECONFIGID_T1122251472_H
#define ECONFIGID_T1122251472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CConfigManager/eConfigId
struct  eConfigId_t1122251472 
{
public:
	// System.Int32 CConfigManager/eConfigId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eConfigId_t1122251472, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECONFIGID_T1122251472_H
#ifndef EGAMEMODE_T32794200_H
#define EGAMEMODE_T32794200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectGameMode/eGameMode
struct  eGameMode_t32794200 
{
public:
	// System.Int32 CSelectGameMode/eGameMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eGameMode_t32794200, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGAMEMODE_T32794200_H
#ifndef ESKILLID_T2951149716_H
#define ESKILLID_T2951149716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/eSkillId
struct  eSkillId_t2951149716 
{
public:
	// System.Int32 CSkillSystem/eSkillId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillId_t2951149716, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLID_T2951149716_H
#ifndef ECTRLTYPE_T732417545_H
#define ECTRLTYPE_T732417545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppingMallManager/eCtrlTYpe
struct  eCtrlTYpe_t732417545 
{
public:
	// System.Int32 ShoppingMallManager/eCtrlTYpe::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eCtrlTYpe_t732417545, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECTRLTYPE_T732417545_H
#ifndef ESHOPPINGMALLPAGETYPE_T1431384372_H
#define ESHOPPINGMALLPAGETYPE_T1431384372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppingMallManager/eShoppingMallPageType
struct  eShoppingMallPageType_t1431384372 
{
public:
	// System.Int32 ShoppingMallManager/eShoppingMallPageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eShoppingMallPageType_t1431384372, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESHOPPINGMALLPAGETYPE_T1431384372_H
#ifndef EMONEYTYPE_T1561655355_H
#define EMONEYTYPE_T1561655355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppingMallManager/eMoneyType
struct  eMoneyType_t1561655355 
{
public:
	// System.Int32 ShoppingMallManager/eMoneyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMoneyType_t1561655355, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONEYTYPE_T1561655355_H
#ifndef EITEMSTATUS_T193384828_H
#define EITEMSTATUS_T193384828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CShoppingCounter/eItemStatus
struct  eItemStatus_t193384828 
{
public:
	// System.Int32 CShoppingCounter/eItemStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eItemStatus_t193384828, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EITEMSTATUS_T193384828_H
#ifndef SBEANINFO_T3146055137_H
#define SBEANINFO_T3146055137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBeanCollection/sBeanInfo
struct  sBeanInfo_t3146055137 
{
public:
	// UnityEngine.Vector2 CBeanCollection/sBeanInfo::pos
	Vector2_t2156229523  ___pos_0;
	// System.Boolean CBeanCollection/sBeanInfo::bDead
	bool ___bDead_1;
	// System.Int32 CBeanCollection/sBeanInfo::nId
	int32_t ___nId_2;
	// UnityEngine.Color[] CBeanCollection/sBeanInfo::colors
	ColorU5BU5D_t941916413* ___colors_3;
	// CMonsterEditor/sThornConfig CBeanCollection/sBeanInfo::buildin_config
	sThornConfig_t1242444451  ___buildin_config_4;
	// CClassEditor/sThornOfThisClassConfig CBeanCollection/sBeanInfo::class_config
	sThornOfThisClassConfig_t350464872  ___class_config_5;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(sBeanInfo_t3146055137, ___pos_0)); }
	inline Vector2_t2156229523  get_pos_0() const { return ___pos_0; }
	inline Vector2_t2156229523 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector2_t2156229523  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_bDead_1() { return static_cast<int32_t>(offsetof(sBeanInfo_t3146055137, ___bDead_1)); }
	inline bool get_bDead_1() const { return ___bDead_1; }
	inline bool* get_address_of_bDead_1() { return &___bDead_1; }
	inline void set_bDead_1(bool value)
	{
		___bDead_1 = value;
	}

	inline static int32_t get_offset_of_nId_2() { return static_cast<int32_t>(offsetof(sBeanInfo_t3146055137, ___nId_2)); }
	inline int32_t get_nId_2() const { return ___nId_2; }
	inline int32_t* get_address_of_nId_2() { return &___nId_2; }
	inline void set_nId_2(int32_t value)
	{
		___nId_2 = value;
	}

	inline static int32_t get_offset_of_colors_3() { return static_cast<int32_t>(offsetof(sBeanInfo_t3146055137, ___colors_3)); }
	inline ColorU5BU5D_t941916413* get_colors_3() const { return ___colors_3; }
	inline ColorU5BU5D_t941916413** get_address_of_colors_3() { return &___colors_3; }
	inline void set_colors_3(ColorU5BU5D_t941916413* value)
	{
		___colors_3 = value;
		Il2CppCodeGenWriteBarrier((&___colors_3), value);
	}

	inline static int32_t get_offset_of_buildin_config_4() { return static_cast<int32_t>(offsetof(sBeanInfo_t3146055137, ___buildin_config_4)); }
	inline sThornConfig_t1242444451  get_buildin_config_4() const { return ___buildin_config_4; }
	inline sThornConfig_t1242444451 * get_address_of_buildin_config_4() { return &___buildin_config_4; }
	inline void set_buildin_config_4(sThornConfig_t1242444451  value)
	{
		___buildin_config_4 = value;
	}

	inline static int32_t get_offset_of_class_config_5() { return static_cast<int32_t>(offsetof(sBeanInfo_t3146055137, ___class_config_5)); }
	inline sThornOfThisClassConfig_t350464872  get_class_config_5() const { return ___class_config_5; }
	inline sThornOfThisClassConfig_t350464872 * get_address_of_class_config_5() { return &___class_config_5; }
	inline void set_class_config_5(sThornOfThisClassConfig_t350464872  value)
	{
		___class_config_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CBeanCollection/sBeanInfo
struct sBeanInfo_t3146055137_marshaled_pinvoke
{
	Vector2_t2156229523  ___pos_0;
	int32_t ___bDead_1;
	int32_t ___nId_2;
	Color_t2555686324 * ___colors_3;
	sThornConfig_t1242444451_marshaled_pinvoke ___buildin_config_4;
	sThornOfThisClassConfig_t350464872_marshaled_pinvoke ___class_config_5;
};
// Native definition for COM marshalling of CBeanCollection/sBeanInfo
struct sBeanInfo_t3146055137_marshaled_com
{
	Vector2_t2156229523  ___pos_0;
	int32_t ___bDead_1;
	int32_t ___nId_2;
	Color_t2555686324 * ___colors_3;
	sThornConfig_t1242444451_marshaled_com ___buildin_config_4;
	sThornOfThisClassConfig_t350464872_marshaled_com ___class_config_5;
};
#endif // SBEANINFO_T3146055137_H
#ifndef EGRASSFUNC_T2798151921_H
#define EGRASSFUNC_T2798151921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrassEditor/eGrassFunc
struct  eGrassFunc_t2798151921 
{
public:
	// System.Int32 CGrassEditor/eGrassFunc::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eGrassFunc_t2798151921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGRASSFUNC_T2798151921_H
#ifndef ESYSMSGTYPE_T661633863_H
#define ESYSMSGTYPE_T661633863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMsgSystem/eSysMsgType
struct  eSysMsgType_t661633863 
{
public:
	// System.Int32 CMsgSystem/eSysMsgType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSysMsgType_t661633863, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESYSMSGTYPE_T661633863_H
#ifndef EMSGID_T264670947_H
#define EMSGID_T264670947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMsgSystem/eMsgId
struct  eMsgId_t264670947 
{
public:
	// System.Int32 CMsgSystem/eMsgId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eMsgId_t264670947, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMSGID_T264670947_H
#ifndef EJISHASYSTEMMSGTYPE_T1704052883_H
#define EJISHASYSTEMMSGTYPE_T1704052883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJiShaInfo/eJiShaSystemMsgType
struct  eJiShaSystemMsgType_t1704052883 
{
public:
	// System.Int32 CJiShaInfo/eJiShaSystemMsgType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eJiShaSystemMsgType_t1704052883, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EJISHASYSTEMMSGTYPE_T1704052883_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef USERPROPS_T2498857227_H
#define USERPROPS_T2498857227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.Shape/UserProps
struct  UserProps_t2498857227  : public RuntimeObject
{
public:
	// System.Boolean Shapes2D.Shape/UserProps::dirty
	bool ___dirty_0;
	// System.Boolean Shapes2D.Shape/UserProps::polyMapNeedsRegen
	bool ___polyMapNeedsRegen_1;
	// System.Single Shapes2D.Shape/UserProps::_blur
	float ____blur_2;
	// System.Boolean Shapes2D.Shape/UserProps::_correctScaling
	bool ____correctScaling_3;
	// System.Single Shapes2D.Shape/UserProps::_endAngle
	float ____endAngle_4;
	// UnityEngine.Color Shapes2D.Shape/UserProps::_fillColor
	Color_t2555686324  ____fillColor_5;
	// UnityEngine.Color Shapes2D.Shape/UserProps::_fillColor2
	Color_t2555686324  ____fillColor2_6;
	// UnityEngine.Vector2 Shapes2D.Shape/UserProps::_fillOffset
	Vector2_t2156229523  ____fillOffset_7;
	// System.Boolean Shapes2D.Shape/UserProps::_fillPathLoops
	bool ____fillPathLoops_8;
	// System.Single Shapes2D.Shape/UserProps::_fillRotation
	float ____fillRotation_9;
	// UnityEngine.Vector2 Shapes2D.Shape/UserProps::_fillScale
	Vector2_t2156229523  ____fillScale_10;
	// UnityEngine.Texture2D Shapes2D.Shape/UserProps::_fillTexture
	Texture2D_t3840446185 * ____fillTexture_11;
	// Shapes2D.FillType Shapes2D.Shape/UserProps::_fillType
	int32_t ____fillType_12;
	// Shapes2D.GradientAxis Shapes2D.Shape/UserProps::_gradientAxis
	int32_t ____gradientAxis_13;
	// System.Single Shapes2D.Shape/UserProps::_gradientStart
	float ____gradientStart_14;
	// Shapes2D.GradientType Shapes2D.Shape/UserProps::_gradientType
	int32_t ____gradientType_15;
	// System.Single Shapes2D.Shape/UserProps::_gridSize
	float ____gridSize_16;
	// UnityEngine.Vector2 Shapes2D.Shape/UserProps::_innerCutout
	Vector2_t2156229523  ____innerCutout_17;
	// System.Boolean Shapes2D.Shape/UserProps::_invertArc
	bool ____invertArc_18;
	// System.Single Shapes2D.Shape/UserProps::_lineSize
	float ____lineSize_19;
	// System.Int32 Shapes2D.Shape/UserProps::_obsoleteNumSides
	int32_t ____obsoleteNumSides_20;
	// UnityEngine.Color Shapes2D.Shape/UserProps::_outlineColor
	Color_t2555686324  ____outlineColor_21;
	// System.Single Shapes2D.Shape/UserProps::_outlineSize
	float ____outlineSize_22;
	// Shapes2D.PathSegment[] Shapes2D.Shape/UserProps::_pathSegments
	PathSegmentU5BU5D_t2220036080* ____pathSegments_23;
	// System.Single Shapes2D.Shape/UserProps::_pathThickness
	float ____pathThickness_24;
	// Shapes2D.PolygonPreset Shapes2D.Shape/UserProps::_polygonPreset
	int32_t ____polygonPreset_25;
	// UnityEngine.Vector2[] Shapes2D.Shape/UserProps::_polyVertices
	Vector2U5BU5D_t1457185986* ____polyVertices_26;
	// System.Single Shapes2D.Shape/UserProps::_roundness
	float ____roundness_27;
	// System.Single Shapes2D.Shape/UserProps::_roundnessBottomLeft
	float ____roundnessBottomLeft_28;
	// System.Single Shapes2D.Shape/UserProps::_roundnessBottomRight
	float ____roundnessBottomRight_29;
	// System.Boolean Shapes2D.Shape/UserProps::_roundnessPerCorner
	bool ____roundnessPerCorner_30;
	// System.Single Shapes2D.Shape/UserProps::_roundnessTopLeft
	float ____roundnessTopLeft_31;
	// System.Single Shapes2D.Shape/UserProps::_roundnessTopRight
	float ____roundnessTopRight_32;
	// Shapes2D.ShapeType Shapes2D.Shape/UserProps::_shapeType
	int32_t ____shapeType_33;
	// System.Single Shapes2D.Shape/UserProps::_startAngle
	float ____startAngle_34;
	// System.Single Shapes2D.Shape/UserProps::_triangleOffset
	float ____triangleOffset_35;
	// System.Boolean Shapes2D.Shape/UserProps::_usePolygonMap
	bool ____usePolygonMap_36;

public:
	inline static int32_t get_offset_of_dirty_0() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ___dirty_0)); }
	inline bool get_dirty_0() const { return ___dirty_0; }
	inline bool* get_address_of_dirty_0() { return &___dirty_0; }
	inline void set_dirty_0(bool value)
	{
		___dirty_0 = value;
	}

	inline static int32_t get_offset_of_polyMapNeedsRegen_1() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ___polyMapNeedsRegen_1)); }
	inline bool get_polyMapNeedsRegen_1() const { return ___polyMapNeedsRegen_1; }
	inline bool* get_address_of_polyMapNeedsRegen_1() { return &___polyMapNeedsRegen_1; }
	inline void set_polyMapNeedsRegen_1(bool value)
	{
		___polyMapNeedsRegen_1 = value;
	}

	inline static int32_t get_offset_of__blur_2() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____blur_2)); }
	inline float get__blur_2() const { return ____blur_2; }
	inline float* get_address_of__blur_2() { return &____blur_2; }
	inline void set__blur_2(float value)
	{
		____blur_2 = value;
	}

	inline static int32_t get_offset_of__correctScaling_3() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____correctScaling_3)); }
	inline bool get__correctScaling_3() const { return ____correctScaling_3; }
	inline bool* get_address_of__correctScaling_3() { return &____correctScaling_3; }
	inline void set__correctScaling_3(bool value)
	{
		____correctScaling_3 = value;
	}

	inline static int32_t get_offset_of__endAngle_4() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____endAngle_4)); }
	inline float get__endAngle_4() const { return ____endAngle_4; }
	inline float* get_address_of__endAngle_4() { return &____endAngle_4; }
	inline void set__endAngle_4(float value)
	{
		____endAngle_4 = value;
	}

	inline static int32_t get_offset_of__fillColor_5() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillColor_5)); }
	inline Color_t2555686324  get__fillColor_5() const { return ____fillColor_5; }
	inline Color_t2555686324 * get_address_of__fillColor_5() { return &____fillColor_5; }
	inline void set__fillColor_5(Color_t2555686324  value)
	{
		____fillColor_5 = value;
	}

	inline static int32_t get_offset_of__fillColor2_6() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillColor2_6)); }
	inline Color_t2555686324  get__fillColor2_6() const { return ____fillColor2_6; }
	inline Color_t2555686324 * get_address_of__fillColor2_6() { return &____fillColor2_6; }
	inline void set__fillColor2_6(Color_t2555686324  value)
	{
		____fillColor2_6 = value;
	}

	inline static int32_t get_offset_of__fillOffset_7() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillOffset_7)); }
	inline Vector2_t2156229523  get__fillOffset_7() const { return ____fillOffset_7; }
	inline Vector2_t2156229523 * get_address_of__fillOffset_7() { return &____fillOffset_7; }
	inline void set__fillOffset_7(Vector2_t2156229523  value)
	{
		____fillOffset_7 = value;
	}

	inline static int32_t get_offset_of__fillPathLoops_8() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillPathLoops_8)); }
	inline bool get__fillPathLoops_8() const { return ____fillPathLoops_8; }
	inline bool* get_address_of__fillPathLoops_8() { return &____fillPathLoops_8; }
	inline void set__fillPathLoops_8(bool value)
	{
		____fillPathLoops_8 = value;
	}

	inline static int32_t get_offset_of__fillRotation_9() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillRotation_9)); }
	inline float get__fillRotation_9() const { return ____fillRotation_9; }
	inline float* get_address_of__fillRotation_9() { return &____fillRotation_9; }
	inline void set__fillRotation_9(float value)
	{
		____fillRotation_9 = value;
	}

	inline static int32_t get_offset_of__fillScale_10() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillScale_10)); }
	inline Vector2_t2156229523  get__fillScale_10() const { return ____fillScale_10; }
	inline Vector2_t2156229523 * get_address_of__fillScale_10() { return &____fillScale_10; }
	inline void set__fillScale_10(Vector2_t2156229523  value)
	{
		____fillScale_10 = value;
	}

	inline static int32_t get_offset_of__fillTexture_11() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillTexture_11)); }
	inline Texture2D_t3840446185 * get__fillTexture_11() const { return ____fillTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of__fillTexture_11() { return &____fillTexture_11; }
	inline void set__fillTexture_11(Texture2D_t3840446185 * value)
	{
		____fillTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&____fillTexture_11), value);
	}

	inline static int32_t get_offset_of__fillType_12() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____fillType_12)); }
	inline int32_t get__fillType_12() const { return ____fillType_12; }
	inline int32_t* get_address_of__fillType_12() { return &____fillType_12; }
	inline void set__fillType_12(int32_t value)
	{
		____fillType_12 = value;
	}

	inline static int32_t get_offset_of__gradientAxis_13() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____gradientAxis_13)); }
	inline int32_t get__gradientAxis_13() const { return ____gradientAxis_13; }
	inline int32_t* get_address_of__gradientAxis_13() { return &____gradientAxis_13; }
	inline void set__gradientAxis_13(int32_t value)
	{
		____gradientAxis_13 = value;
	}

	inline static int32_t get_offset_of__gradientStart_14() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____gradientStart_14)); }
	inline float get__gradientStart_14() const { return ____gradientStart_14; }
	inline float* get_address_of__gradientStart_14() { return &____gradientStart_14; }
	inline void set__gradientStart_14(float value)
	{
		____gradientStart_14 = value;
	}

	inline static int32_t get_offset_of__gradientType_15() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____gradientType_15)); }
	inline int32_t get__gradientType_15() const { return ____gradientType_15; }
	inline int32_t* get_address_of__gradientType_15() { return &____gradientType_15; }
	inline void set__gradientType_15(int32_t value)
	{
		____gradientType_15 = value;
	}

	inline static int32_t get_offset_of__gridSize_16() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____gridSize_16)); }
	inline float get__gridSize_16() const { return ____gridSize_16; }
	inline float* get_address_of__gridSize_16() { return &____gridSize_16; }
	inline void set__gridSize_16(float value)
	{
		____gridSize_16 = value;
	}

	inline static int32_t get_offset_of__innerCutout_17() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____innerCutout_17)); }
	inline Vector2_t2156229523  get__innerCutout_17() const { return ____innerCutout_17; }
	inline Vector2_t2156229523 * get_address_of__innerCutout_17() { return &____innerCutout_17; }
	inline void set__innerCutout_17(Vector2_t2156229523  value)
	{
		____innerCutout_17 = value;
	}

	inline static int32_t get_offset_of__invertArc_18() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____invertArc_18)); }
	inline bool get__invertArc_18() const { return ____invertArc_18; }
	inline bool* get_address_of__invertArc_18() { return &____invertArc_18; }
	inline void set__invertArc_18(bool value)
	{
		____invertArc_18 = value;
	}

	inline static int32_t get_offset_of__lineSize_19() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____lineSize_19)); }
	inline float get__lineSize_19() const { return ____lineSize_19; }
	inline float* get_address_of__lineSize_19() { return &____lineSize_19; }
	inline void set__lineSize_19(float value)
	{
		____lineSize_19 = value;
	}

	inline static int32_t get_offset_of__obsoleteNumSides_20() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____obsoleteNumSides_20)); }
	inline int32_t get__obsoleteNumSides_20() const { return ____obsoleteNumSides_20; }
	inline int32_t* get_address_of__obsoleteNumSides_20() { return &____obsoleteNumSides_20; }
	inline void set__obsoleteNumSides_20(int32_t value)
	{
		____obsoleteNumSides_20 = value;
	}

	inline static int32_t get_offset_of__outlineColor_21() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____outlineColor_21)); }
	inline Color_t2555686324  get__outlineColor_21() const { return ____outlineColor_21; }
	inline Color_t2555686324 * get_address_of__outlineColor_21() { return &____outlineColor_21; }
	inline void set__outlineColor_21(Color_t2555686324  value)
	{
		____outlineColor_21 = value;
	}

	inline static int32_t get_offset_of__outlineSize_22() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____outlineSize_22)); }
	inline float get__outlineSize_22() const { return ____outlineSize_22; }
	inline float* get_address_of__outlineSize_22() { return &____outlineSize_22; }
	inline void set__outlineSize_22(float value)
	{
		____outlineSize_22 = value;
	}

	inline static int32_t get_offset_of__pathSegments_23() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____pathSegments_23)); }
	inline PathSegmentU5BU5D_t2220036080* get__pathSegments_23() const { return ____pathSegments_23; }
	inline PathSegmentU5BU5D_t2220036080** get_address_of__pathSegments_23() { return &____pathSegments_23; }
	inline void set__pathSegments_23(PathSegmentU5BU5D_t2220036080* value)
	{
		____pathSegments_23 = value;
		Il2CppCodeGenWriteBarrier((&____pathSegments_23), value);
	}

	inline static int32_t get_offset_of__pathThickness_24() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____pathThickness_24)); }
	inline float get__pathThickness_24() const { return ____pathThickness_24; }
	inline float* get_address_of__pathThickness_24() { return &____pathThickness_24; }
	inline void set__pathThickness_24(float value)
	{
		____pathThickness_24 = value;
	}

	inline static int32_t get_offset_of__polygonPreset_25() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____polygonPreset_25)); }
	inline int32_t get__polygonPreset_25() const { return ____polygonPreset_25; }
	inline int32_t* get_address_of__polygonPreset_25() { return &____polygonPreset_25; }
	inline void set__polygonPreset_25(int32_t value)
	{
		____polygonPreset_25 = value;
	}

	inline static int32_t get_offset_of__polyVertices_26() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____polyVertices_26)); }
	inline Vector2U5BU5D_t1457185986* get__polyVertices_26() const { return ____polyVertices_26; }
	inline Vector2U5BU5D_t1457185986** get_address_of__polyVertices_26() { return &____polyVertices_26; }
	inline void set__polyVertices_26(Vector2U5BU5D_t1457185986* value)
	{
		____polyVertices_26 = value;
		Il2CppCodeGenWriteBarrier((&____polyVertices_26), value);
	}

	inline static int32_t get_offset_of__roundness_27() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____roundness_27)); }
	inline float get__roundness_27() const { return ____roundness_27; }
	inline float* get_address_of__roundness_27() { return &____roundness_27; }
	inline void set__roundness_27(float value)
	{
		____roundness_27 = value;
	}

	inline static int32_t get_offset_of__roundnessBottomLeft_28() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____roundnessBottomLeft_28)); }
	inline float get__roundnessBottomLeft_28() const { return ____roundnessBottomLeft_28; }
	inline float* get_address_of__roundnessBottomLeft_28() { return &____roundnessBottomLeft_28; }
	inline void set__roundnessBottomLeft_28(float value)
	{
		____roundnessBottomLeft_28 = value;
	}

	inline static int32_t get_offset_of__roundnessBottomRight_29() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____roundnessBottomRight_29)); }
	inline float get__roundnessBottomRight_29() const { return ____roundnessBottomRight_29; }
	inline float* get_address_of__roundnessBottomRight_29() { return &____roundnessBottomRight_29; }
	inline void set__roundnessBottomRight_29(float value)
	{
		____roundnessBottomRight_29 = value;
	}

	inline static int32_t get_offset_of__roundnessPerCorner_30() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____roundnessPerCorner_30)); }
	inline bool get__roundnessPerCorner_30() const { return ____roundnessPerCorner_30; }
	inline bool* get_address_of__roundnessPerCorner_30() { return &____roundnessPerCorner_30; }
	inline void set__roundnessPerCorner_30(bool value)
	{
		____roundnessPerCorner_30 = value;
	}

	inline static int32_t get_offset_of__roundnessTopLeft_31() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____roundnessTopLeft_31)); }
	inline float get__roundnessTopLeft_31() const { return ____roundnessTopLeft_31; }
	inline float* get_address_of__roundnessTopLeft_31() { return &____roundnessTopLeft_31; }
	inline void set__roundnessTopLeft_31(float value)
	{
		____roundnessTopLeft_31 = value;
	}

	inline static int32_t get_offset_of__roundnessTopRight_32() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____roundnessTopRight_32)); }
	inline float get__roundnessTopRight_32() const { return ____roundnessTopRight_32; }
	inline float* get_address_of__roundnessTopRight_32() { return &____roundnessTopRight_32; }
	inline void set__roundnessTopRight_32(float value)
	{
		____roundnessTopRight_32 = value;
	}

	inline static int32_t get_offset_of__shapeType_33() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____shapeType_33)); }
	inline int32_t get__shapeType_33() const { return ____shapeType_33; }
	inline int32_t* get_address_of__shapeType_33() { return &____shapeType_33; }
	inline void set__shapeType_33(int32_t value)
	{
		____shapeType_33 = value;
	}

	inline static int32_t get_offset_of__startAngle_34() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____startAngle_34)); }
	inline float get__startAngle_34() const { return ____startAngle_34; }
	inline float* get_address_of__startAngle_34() { return &____startAngle_34; }
	inline void set__startAngle_34(float value)
	{
		____startAngle_34 = value;
	}

	inline static int32_t get_offset_of__triangleOffset_35() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____triangleOffset_35)); }
	inline float get__triangleOffset_35() const { return ____triangleOffset_35; }
	inline float* get_address_of__triangleOffset_35() { return &____triangleOffset_35; }
	inline void set__triangleOffset_35(float value)
	{
		____triangleOffset_35 = value;
	}

	inline static int32_t get_offset_of__usePolygonMap_36() { return static_cast<int32_t>(offsetof(UserProps_t2498857227, ____usePolygonMap_36)); }
	inline bool get__usePolygonMap_36() const { return ____usePolygonMap_36; }
	inline bool* get_address_of__usePolygonMap_36() { return &____usePolygonMap_36; }
	inline void set__usePolygonMap_36(bool value)
	{
		____usePolygonMap_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPROPS_T2498857227_H
#ifndef SJISHASYSTEMMSGPARAM_T2334122302_H
#define SJISHASYSTEMMSGPARAM_T2334122302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJiShaInfo/sJiShaSystemMsgParam
struct  sJiShaSystemMsgParam_t2334122302 
{
public:
	// CJiShaInfo/eJiShaSystemMsgType CJiShaInfo/sJiShaSystemMsgParam::type
	int32_t ___type_0;
	// System.String CJiShaInfo/sJiShaSystemMsgParam::szEaterName
	String_t* ___szEaterName_1;
	// System.String CJiShaInfo/sJiShaSystemMsgParam::szDeadmeatName
	String_t* ___szDeadmeatName_2;
	// UnityEngine.Sprite CJiShaInfo/sJiShaSystemMsgParam::sprKillerAvatar
	Sprite_t280657092 * ___sprKillerAvatar_3;
	// UnityEngine.Sprite CJiShaInfo/sJiShaSystemMsgParam::sprDeadMeatAvatar
	Sprite_t280657092 * ___sprDeadMeatAvatar_4;
	// System.Int32[] CJiShaInfo/sJiShaSystemMsgParam::aryAssistId
	Int32U5BU5D_t385246372* ___aryAssistId_5;
	// System.Int32 CJiShaInfo/sJiShaSystemMsgParam::nDuoShaNum
	int32_t ___nDuoShaNum_6;
	// System.Int32 CJiShaInfo/sJiShaSystemMsgParam::nLianXuShaNum
	int32_t ___nLianXuShaNum_7;
	// System.Boolean CJiShaInfo/sJiShaSystemMsgParam::bFirstBlood
	bool ___bFirstBlood_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_szEaterName_1() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___szEaterName_1)); }
	inline String_t* get_szEaterName_1() const { return ___szEaterName_1; }
	inline String_t** get_address_of_szEaterName_1() { return &___szEaterName_1; }
	inline void set_szEaterName_1(String_t* value)
	{
		___szEaterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szEaterName_1), value);
	}

	inline static int32_t get_offset_of_szDeadmeatName_2() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___szDeadmeatName_2)); }
	inline String_t* get_szDeadmeatName_2() const { return ___szDeadmeatName_2; }
	inline String_t** get_address_of_szDeadmeatName_2() { return &___szDeadmeatName_2; }
	inline void set_szDeadmeatName_2(String_t* value)
	{
		___szDeadmeatName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szDeadmeatName_2), value);
	}

	inline static int32_t get_offset_of_sprKillerAvatar_3() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___sprKillerAvatar_3)); }
	inline Sprite_t280657092 * get_sprKillerAvatar_3() const { return ___sprKillerAvatar_3; }
	inline Sprite_t280657092 ** get_address_of_sprKillerAvatar_3() { return &___sprKillerAvatar_3; }
	inline void set_sprKillerAvatar_3(Sprite_t280657092 * value)
	{
		___sprKillerAvatar_3 = value;
		Il2CppCodeGenWriteBarrier((&___sprKillerAvatar_3), value);
	}

	inline static int32_t get_offset_of_sprDeadMeatAvatar_4() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___sprDeadMeatAvatar_4)); }
	inline Sprite_t280657092 * get_sprDeadMeatAvatar_4() const { return ___sprDeadMeatAvatar_4; }
	inline Sprite_t280657092 ** get_address_of_sprDeadMeatAvatar_4() { return &___sprDeadMeatAvatar_4; }
	inline void set_sprDeadMeatAvatar_4(Sprite_t280657092 * value)
	{
		___sprDeadMeatAvatar_4 = value;
		Il2CppCodeGenWriteBarrier((&___sprDeadMeatAvatar_4), value);
	}

	inline static int32_t get_offset_of_aryAssistId_5() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___aryAssistId_5)); }
	inline Int32U5BU5D_t385246372* get_aryAssistId_5() const { return ___aryAssistId_5; }
	inline Int32U5BU5D_t385246372** get_address_of_aryAssistId_5() { return &___aryAssistId_5; }
	inline void set_aryAssistId_5(Int32U5BU5D_t385246372* value)
	{
		___aryAssistId_5 = value;
		Il2CppCodeGenWriteBarrier((&___aryAssistId_5), value);
	}

	inline static int32_t get_offset_of_nDuoShaNum_6() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___nDuoShaNum_6)); }
	inline int32_t get_nDuoShaNum_6() const { return ___nDuoShaNum_6; }
	inline int32_t* get_address_of_nDuoShaNum_6() { return &___nDuoShaNum_6; }
	inline void set_nDuoShaNum_6(int32_t value)
	{
		___nDuoShaNum_6 = value;
	}

	inline static int32_t get_offset_of_nLianXuShaNum_7() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___nLianXuShaNum_7)); }
	inline int32_t get_nLianXuShaNum_7() const { return ___nLianXuShaNum_7; }
	inline int32_t* get_address_of_nLianXuShaNum_7() { return &___nLianXuShaNum_7; }
	inline void set_nLianXuShaNum_7(int32_t value)
	{
		___nLianXuShaNum_7 = value;
	}

	inline static int32_t get_offset_of_bFirstBlood_8() { return static_cast<int32_t>(offsetof(sJiShaSystemMsgParam_t2334122302, ___bFirstBlood_8)); }
	inline bool get_bFirstBlood_8() const { return ___bFirstBlood_8; }
	inline bool* get_address_of_bFirstBlood_8() { return &___bFirstBlood_8; }
	inline void set_bFirstBlood_8(bool value)
	{
		___bFirstBlood_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CJiShaInfo/sJiShaSystemMsgParam
struct sJiShaSystemMsgParam_t2334122302_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___szEaterName_1;
	char* ___szDeadmeatName_2;
	Sprite_t280657092 * ___sprKillerAvatar_3;
	Sprite_t280657092 * ___sprDeadMeatAvatar_4;
	int32_t* ___aryAssistId_5;
	int32_t ___nDuoShaNum_6;
	int32_t ___nLianXuShaNum_7;
	int32_t ___bFirstBlood_8;
};
// Native definition for COM marshalling of CJiShaInfo/sJiShaSystemMsgParam
struct sJiShaSystemMsgParam_t2334122302_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___szEaterName_1;
	Il2CppChar* ___szDeadmeatName_2;
	Sprite_t280657092 * ___sprKillerAvatar_3;
	Sprite_t280657092 * ___sprDeadMeatAvatar_4;
	int32_t* ___aryAssistId_5;
	int32_t ___nDuoShaNum_6;
	int32_t ___nLianXuShaNum_7;
	int32_t ___bFirstBlood_8;
};
#endif // SJISHASYSTEMMSGPARAM_T2334122302_H
#ifndef SHADERPROPS_T3992238139_H
#define SHADERPROPS_T3992238139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.Shape/ShaderProps
struct  ShaderProps_t3992238139  : public RuntimeObject
{
public:
	// System.Single Shapes2D.Shape/ShaderProps::xScale
	float ___xScale_0;
	// System.Single Shapes2D.Shape/ShaderProps::yScale
	float ___yScale_1;
	// Shapes2D.ShapeType Shapes2D.Shape/ShaderProps::shapeType
	int32_t ___shapeType_2;
	// System.Single Shapes2D.Shape/ShaderProps::outlineSize
	float ___outlineSize_3;
	// System.Single Shapes2D.Shape/ShaderProps::blur
	float ___blur_4;
	// UnityEngine.Color Shapes2D.Shape/ShaderProps::outlineColor
	Color_t2555686324  ___outlineColor_5;
	// Shapes2D.FillType Shapes2D.Shape/ShaderProps::fillType
	int32_t ___fillType_6;
	// UnityEngine.Color Shapes2D.Shape/ShaderProps::fillColor
	Color_t2555686324  ___fillColor_7;
	// UnityEngine.Color Shapes2D.Shape/ShaderProps::fillColor2
	Color_t2555686324  ___fillColor2_8;
	// System.Single Shapes2D.Shape/ShaderProps::fillRotation
	float ___fillRotation_9;
	// UnityEngine.Vector2 Shapes2D.Shape/ShaderProps::fillOffset
	Vector2_t2156229523  ___fillOffset_10;
	// UnityEngine.Vector2 Shapes2D.Shape/ShaderProps::fillScale
	Vector2_t2156229523  ___fillScale_11;
	// Shapes2D.GradientType Shapes2D.Shape/ShaderProps::gradientType
	int32_t ___gradientType_12;
	// Shapes2D.GradientAxis Shapes2D.Shape/ShaderProps::gradientAxis
	int32_t ___gradientAxis_13;
	// System.Single Shapes2D.Shape/ShaderProps::gradientStart
	float ___gradientStart_14;
	// UnityEngine.Texture2D Shapes2D.Shape/ShaderProps::fillTexture
	Texture2D_t3840446185 * ___fillTexture_15;
	// System.Single Shapes2D.Shape/ShaderProps::gridSize
	float ___gridSize_16;
	// System.Single Shapes2D.Shape/ShaderProps::lineSize
	float ___lineSize_17;
	// UnityEngine.Vector4 Shapes2D.Shape/ShaderProps::roundnessVec
	Vector4_t3319028937  ___roundnessVec_18;
	// UnityEngine.Vector4 Shapes2D.Shape/ShaderProps::innerRadii
	Vector4_t3319028937  ___innerRadii_19;
	// System.Single Shapes2D.Shape/ShaderProps::arcMinAngle
	float ___arcMinAngle_20;
	// System.Single Shapes2D.Shape/ShaderProps::arcMaxAngle
	float ___arcMaxAngle_21;
	// System.Boolean Shapes2D.Shape/ShaderProps::invertArc
	bool ___invertArc_22;
	// System.Boolean Shapes2D.Shape/ShaderProps::useArc
	bool ___useArc_23;
	// System.Boolean Shapes2D.Shape/ShaderProps::correctScaling
	bool ___correctScaling_24;
	// System.Single Shapes2D.Shape/ShaderProps::triangleOffset
	float ___triangleOffset_25;
	// System.Int32 Shapes2D.Shape/ShaderProps::numPolyVerts
	int32_t ___numPolyVerts_26;
	// UnityEngine.Vector4[] Shapes2D.Shape/ShaderProps::polyVertices
	Vector4U5BU5D_t934056436* ___polyVertices_27;
	// System.Boolean Shapes2D.Shape/ShaderProps::usePolygonMap
	bool ___usePolygonMap_28;
	// UnityEngine.Texture2D Shapes2D.Shape/ShaderProps::polyMap
	Texture2D_t3840446185 * ___polyMap_29;
	// System.Single Shapes2D.Shape/ShaderProps::pathThickness
	float ___pathThickness_30;
	// System.Int32 Shapes2D.Shape/ShaderProps::numPathSegments
	int32_t ___numPathSegments_31;
	// UnityEngine.Vector4[] Shapes2D.Shape/ShaderProps::pathPoints
	Vector4U5BU5D_t934056436* ___pathPoints_32;
	// System.Boolean Shapes2D.Shape/ShaderProps::fillPathLoops
	bool ___fillPathLoops_33;

public:
	inline static int32_t get_offset_of_xScale_0() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___xScale_0)); }
	inline float get_xScale_0() const { return ___xScale_0; }
	inline float* get_address_of_xScale_0() { return &___xScale_0; }
	inline void set_xScale_0(float value)
	{
		___xScale_0 = value;
	}

	inline static int32_t get_offset_of_yScale_1() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___yScale_1)); }
	inline float get_yScale_1() const { return ___yScale_1; }
	inline float* get_address_of_yScale_1() { return &___yScale_1; }
	inline void set_yScale_1(float value)
	{
		___yScale_1 = value;
	}

	inline static int32_t get_offset_of_shapeType_2() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___shapeType_2)); }
	inline int32_t get_shapeType_2() const { return ___shapeType_2; }
	inline int32_t* get_address_of_shapeType_2() { return &___shapeType_2; }
	inline void set_shapeType_2(int32_t value)
	{
		___shapeType_2 = value;
	}

	inline static int32_t get_offset_of_outlineSize_3() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___outlineSize_3)); }
	inline float get_outlineSize_3() const { return ___outlineSize_3; }
	inline float* get_address_of_outlineSize_3() { return &___outlineSize_3; }
	inline void set_outlineSize_3(float value)
	{
		___outlineSize_3 = value;
	}

	inline static int32_t get_offset_of_blur_4() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___blur_4)); }
	inline float get_blur_4() const { return ___blur_4; }
	inline float* get_address_of_blur_4() { return &___blur_4; }
	inline void set_blur_4(float value)
	{
		___blur_4 = value;
	}

	inline static int32_t get_offset_of_outlineColor_5() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___outlineColor_5)); }
	inline Color_t2555686324  get_outlineColor_5() const { return ___outlineColor_5; }
	inline Color_t2555686324 * get_address_of_outlineColor_5() { return &___outlineColor_5; }
	inline void set_outlineColor_5(Color_t2555686324  value)
	{
		___outlineColor_5 = value;
	}

	inline static int32_t get_offset_of_fillType_6() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillType_6)); }
	inline int32_t get_fillType_6() const { return ___fillType_6; }
	inline int32_t* get_address_of_fillType_6() { return &___fillType_6; }
	inline void set_fillType_6(int32_t value)
	{
		___fillType_6 = value;
	}

	inline static int32_t get_offset_of_fillColor_7() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillColor_7)); }
	inline Color_t2555686324  get_fillColor_7() const { return ___fillColor_7; }
	inline Color_t2555686324 * get_address_of_fillColor_7() { return &___fillColor_7; }
	inline void set_fillColor_7(Color_t2555686324  value)
	{
		___fillColor_7 = value;
	}

	inline static int32_t get_offset_of_fillColor2_8() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillColor2_8)); }
	inline Color_t2555686324  get_fillColor2_8() const { return ___fillColor2_8; }
	inline Color_t2555686324 * get_address_of_fillColor2_8() { return &___fillColor2_8; }
	inline void set_fillColor2_8(Color_t2555686324  value)
	{
		___fillColor2_8 = value;
	}

	inline static int32_t get_offset_of_fillRotation_9() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillRotation_9)); }
	inline float get_fillRotation_9() const { return ___fillRotation_9; }
	inline float* get_address_of_fillRotation_9() { return &___fillRotation_9; }
	inline void set_fillRotation_9(float value)
	{
		___fillRotation_9 = value;
	}

	inline static int32_t get_offset_of_fillOffset_10() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillOffset_10)); }
	inline Vector2_t2156229523  get_fillOffset_10() const { return ___fillOffset_10; }
	inline Vector2_t2156229523 * get_address_of_fillOffset_10() { return &___fillOffset_10; }
	inline void set_fillOffset_10(Vector2_t2156229523  value)
	{
		___fillOffset_10 = value;
	}

	inline static int32_t get_offset_of_fillScale_11() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillScale_11)); }
	inline Vector2_t2156229523  get_fillScale_11() const { return ___fillScale_11; }
	inline Vector2_t2156229523 * get_address_of_fillScale_11() { return &___fillScale_11; }
	inline void set_fillScale_11(Vector2_t2156229523  value)
	{
		___fillScale_11 = value;
	}

	inline static int32_t get_offset_of_gradientType_12() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___gradientType_12)); }
	inline int32_t get_gradientType_12() const { return ___gradientType_12; }
	inline int32_t* get_address_of_gradientType_12() { return &___gradientType_12; }
	inline void set_gradientType_12(int32_t value)
	{
		___gradientType_12 = value;
	}

	inline static int32_t get_offset_of_gradientAxis_13() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___gradientAxis_13)); }
	inline int32_t get_gradientAxis_13() const { return ___gradientAxis_13; }
	inline int32_t* get_address_of_gradientAxis_13() { return &___gradientAxis_13; }
	inline void set_gradientAxis_13(int32_t value)
	{
		___gradientAxis_13 = value;
	}

	inline static int32_t get_offset_of_gradientStart_14() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___gradientStart_14)); }
	inline float get_gradientStart_14() const { return ___gradientStart_14; }
	inline float* get_address_of_gradientStart_14() { return &___gradientStart_14; }
	inline void set_gradientStart_14(float value)
	{
		___gradientStart_14 = value;
	}

	inline static int32_t get_offset_of_fillTexture_15() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillTexture_15)); }
	inline Texture2D_t3840446185 * get_fillTexture_15() const { return ___fillTexture_15; }
	inline Texture2D_t3840446185 ** get_address_of_fillTexture_15() { return &___fillTexture_15; }
	inline void set_fillTexture_15(Texture2D_t3840446185 * value)
	{
		___fillTexture_15 = value;
		Il2CppCodeGenWriteBarrier((&___fillTexture_15), value);
	}

	inline static int32_t get_offset_of_gridSize_16() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___gridSize_16)); }
	inline float get_gridSize_16() const { return ___gridSize_16; }
	inline float* get_address_of_gridSize_16() { return &___gridSize_16; }
	inline void set_gridSize_16(float value)
	{
		___gridSize_16 = value;
	}

	inline static int32_t get_offset_of_lineSize_17() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___lineSize_17)); }
	inline float get_lineSize_17() const { return ___lineSize_17; }
	inline float* get_address_of_lineSize_17() { return &___lineSize_17; }
	inline void set_lineSize_17(float value)
	{
		___lineSize_17 = value;
	}

	inline static int32_t get_offset_of_roundnessVec_18() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___roundnessVec_18)); }
	inline Vector4_t3319028937  get_roundnessVec_18() const { return ___roundnessVec_18; }
	inline Vector4_t3319028937 * get_address_of_roundnessVec_18() { return &___roundnessVec_18; }
	inline void set_roundnessVec_18(Vector4_t3319028937  value)
	{
		___roundnessVec_18 = value;
	}

	inline static int32_t get_offset_of_innerRadii_19() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___innerRadii_19)); }
	inline Vector4_t3319028937  get_innerRadii_19() const { return ___innerRadii_19; }
	inline Vector4_t3319028937 * get_address_of_innerRadii_19() { return &___innerRadii_19; }
	inline void set_innerRadii_19(Vector4_t3319028937  value)
	{
		___innerRadii_19 = value;
	}

	inline static int32_t get_offset_of_arcMinAngle_20() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___arcMinAngle_20)); }
	inline float get_arcMinAngle_20() const { return ___arcMinAngle_20; }
	inline float* get_address_of_arcMinAngle_20() { return &___arcMinAngle_20; }
	inline void set_arcMinAngle_20(float value)
	{
		___arcMinAngle_20 = value;
	}

	inline static int32_t get_offset_of_arcMaxAngle_21() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___arcMaxAngle_21)); }
	inline float get_arcMaxAngle_21() const { return ___arcMaxAngle_21; }
	inline float* get_address_of_arcMaxAngle_21() { return &___arcMaxAngle_21; }
	inline void set_arcMaxAngle_21(float value)
	{
		___arcMaxAngle_21 = value;
	}

	inline static int32_t get_offset_of_invertArc_22() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___invertArc_22)); }
	inline bool get_invertArc_22() const { return ___invertArc_22; }
	inline bool* get_address_of_invertArc_22() { return &___invertArc_22; }
	inline void set_invertArc_22(bool value)
	{
		___invertArc_22 = value;
	}

	inline static int32_t get_offset_of_useArc_23() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___useArc_23)); }
	inline bool get_useArc_23() const { return ___useArc_23; }
	inline bool* get_address_of_useArc_23() { return &___useArc_23; }
	inline void set_useArc_23(bool value)
	{
		___useArc_23 = value;
	}

	inline static int32_t get_offset_of_correctScaling_24() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___correctScaling_24)); }
	inline bool get_correctScaling_24() const { return ___correctScaling_24; }
	inline bool* get_address_of_correctScaling_24() { return &___correctScaling_24; }
	inline void set_correctScaling_24(bool value)
	{
		___correctScaling_24 = value;
	}

	inline static int32_t get_offset_of_triangleOffset_25() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___triangleOffset_25)); }
	inline float get_triangleOffset_25() const { return ___triangleOffset_25; }
	inline float* get_address_of_triangleOffset_25() { return &___triangleOffset_25; }
	inline void set_triangleOffset_25(float value)
	{
		___triangleOffset_25 = value;
	}

	inline static int32_t get_offset_of_numPolyVerts_26() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___numPolyVerts_26)); }
	inline int32_t get_numPolyVerts_26() const { return ___numPolyVerts_26; }
	inline int32_t* get_address_of_numPolyVerts_26() { return &___numPolyVerts_26; }
	inline void set_numPolyVerts_26(int32_t value)
	{
		___numPolyVerts_26 = value;
	}

	inline static int32_t get_offset_of_polyVertices_27() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___polyVertices_27)); }
	inline Vector4U5BU5D_t934056436* get_polyVertices_27() const { return ___polyVertices_27; }
	inline Vector4U5BU5D_t934056436** get_address_of_polyVertices_27() { return &___polyVertices_27; }
	inline void set_polyVertices_27(Vector4U5BU5D_t934056436* value)
	{
		___polyVertices_27 = value;
		Il2CppCodeGenWriteBarrier((&___polyVertices_27), value);
	}

	inline static int32_t get_offset_of_usePolygonMap_28() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___usePolygonMap_28)); }
	inline bool get_usePolygonMap_28() const { return ___usePolygonMap_28; }
	inline bool* get_address_of_usePolygonMap_28() { return &___usePolygonMap_28; }
	inline void set_usePolygonMap_28(bool value)
	{
		___usePolygonMap_28 = value;
	}

	inline static int32_t get_offset_of_polyMap_29() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___polyMap_29)); }
	inline Texture2D_t3840446185 * get_polyMap_29() const { return ___polyMap_29; }
	inline Texture2D_t3840446185 ** get_address_of_polyMap_29() { return &___polyMap_29; }
	inline void set_polyMap_29(Texture2D_t3840446185 * value)
	{
		___polyMap_29 = value;
		Il2CppCodeGenWriteBarrier((&___polyMap_29), value);
	}

	inline static int32_t get_offset_of_pathThickness_30() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___pathThickness_30)); }
	inline float get_pathThickness_30() const { return ___pathThickness_30; }
	inline float* get_address_of_pathThickness_30() { return &___pathThickness_30; }
	inline void set_pathThickness_30(float value)
	{
		___pathThickness_30 = value;
	}

	inline static int32_t get_offset_of_numPathSegments_31() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___numPathSegments_31)); }
	inline int32_t get_numPathSegments_31() const { return ___numPathSegments_31; }
	inline int32_t* get_address_of_numPathSegments_31() { return &___numPathSegments_31; }
	inline void set_numPathSegments_31(int32_t value)
	{
		___numPathSegments_31 = value;
	}

	inline static int32_t get_offset_of_pathPoints_32() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___pathPoints_32)); }
	inline Vector4U5BU5D_t934056436* get_pathPoints_32() const { return ___pathPoints_32; }
	inline Vector4U5BU5D_t934056436** get_address_of_pathPoints_32() { return &___pathPoints_32; }
	inline void set_pathPoints_32(Vector4U5BU5D_t934056436* value)
	{
		___pathPoints_32 = value;
		Il2CppCodeGenWriteBarrier((&___pathPoints_32), value);
	}

	inline static int32_t get_offset_of_fillPathLoops_33() { return static_cast<int32_t>(offsetof(ShaderProps_t3992238139, ___fillPathLoops_33)); }
	inline bool get_fillPathLoops_33() const { return ___fillPathLoops_33; }
	inline bool* get_address_of_fillPathLoops_33() { return &___fillPathLoops_33; }
	inline void set_fillPathLoops_33(bool value)
	{
		___fillPathLoops_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPS_T3992238139_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SHAPES2DPREFS_T998444370_H
#define SHAPES2DPREFS_T998444370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2DPrefs
struct  Shapes2DPrefs_t998444370  : public ScriptableObject_t2528358522
{
public:
	// System.Single Shapes2DPrefs::pixelsPerUnit
	float ___pixelsPerUnit_2;

public:
	inline static int32_t get_offset_of_pixelsPerUnit_2() { return static_cast<int32_t>(offsetof(Shapes2DPrefs_t998444370, ___pixelsPerUnit_2)); }
	inline float get_pixelsPerUnit_2() const { return ___pixelsPerUnit_2; }
	inline float* get_address_of_pixelsPerUnit_2() { return &___pixelsPerUnit_2; }
	inline void set_pixelsPerUnit_2(float value)
	{
		___pixelsPerUnit_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPES2DPREFS_T998444370_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SHOPPINGMALLMANAGER_T2900135047_H
#define SHOPPINGMALLMANAGER_T2900135047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppingMallManager
struct  ShoppingMallManager_t2900135047  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] ShoppingMallManager::m_aryContainerAll
	GameObjectU5BU5D_t3328599146* ___m_aryContainerAll_3;
	// UnityEngine.UI.CanvasScaler ShoppingMallManager::_CanvasScaler
	CanvasScaler_t2767979955 * ____CanvasScaler_4;
	// System.Single ShoppingMallManager::m_fItemCounterInitScale
	float ___m_fItemCounterInitScale_5;
	// System.Single ShoppingMallManager::m_fItemCounterSelectedScale
	float ___m_fItemCounterSelectedScale_6;
	// System.Single ShoppingMallManager::m_fSkeletonPosY
	float ___m_fSkeletonPosY_7;
	// CCyberTreeList ShoppingMallManager::m_clItems
	CCyberTreeList_t4040119676 * ___m_clItems_8;
	// UnityEngine.GameObject ShoppingMallManager::m_preShoppingCounter
	GameObject_t1113636619 * ___m_preShoppingCounter_9;
	// UnityEngine.Sprite ShoppingMallManager::m_sprNotEquiped
	Sprite_t280657092 * ___m_sprNotEquiped_10;
	// UnityEngine.Sprite ShoppingMallManager::m_sprEquiped
	Sprite_t280657092 * ___m_sprEquiped_11;
	// UnityEngine.Sprite ShoppingMallManager::m_sprMoneyBg_NotBought
	Sprite_t280657092 * ___m_sprMoneyBg_NotBought_12;
	// UnityEngine.Sprite ShoppingMallManager::m_sprMoneyBg_Bought
	Sprite_t280657092 * ___m_sprMoneyBg_Bought_13;
	// System.Single ShoppingMallManager::m_fMoneyBgWidth_NotBought
	float ___m_fMoneyBgWidth_NotBought_14;
	// System.Single ShoppingMallManager::m_fMoneyBgWidth_Bought
	float ___m_fMoneyBgWidth_Bought_15;
	// System.Single ShoppingMallManager::m_fMoneyBgHeight
	float ___m_fMoneyBgHeight_16;
	// UnityEngine.Sprite[] ShoppingMallManager::m_aryMoneyTypeSpr
	SpriteU5BU5D_t2581906349* ___m_aryMoneyTypeSpr_17;
	// UnityEngine.Color ShoppingMallManager::m_colorBought_Text
	Color_t2555686324  ___m_colorBought_Text_18;
	// UnityEngine.Color ShoppingMallManager::m_colorBought_Bg
	Color_t2555686324  ___m_colorBought_Bg_19;
	// UnityEngine.Color ShoppingMallManager::m_colorNotBought_Text
	Color_t2555686324  ___m_colorNotBought_Text_20;
	// UnityEngine.Color ShoppingMallManager::m_colorNotBought_Bg
	Color_t2555686324  ___m_colorNotBought_Bg_21;
	// UnityEngine.Color ShoppingMallManager::m_colorBtnTimeLimit_Selected
	Color_t2555686324  ___m_colorBtnTimeLimit_Selected_22;
	// UnityEngine.Color ShoppingMallManager::m_colorBtnTimeLimit_NotSelected
	Color_t2555686324  ___m_colorBtnTimeLimit_NotSelected_23;
	// UnityEngine.Sprite[] ShoppingMallManager::m_aryBuyButtonStatus
	SpriteU5BU5D_t2581906349* ___m_aryBuyButtonStatus_24;
	// System.String[] ShoppingMallManager::m_aryPageTitle
	StringU5BU5D_t1281789340* ___m_aryPageTitle_25;
	// UnityEngine.Sprite ShoppingMallManager::m_sprPageSelected
	Sprite_t280657092 * ___m_sprPageSelected_26;
	// UnityEngine.Sprite ShoppingMallManager::m_sprPageNotSelected
	Sprite_t280657092 * ___m_sprPageNotSelected_27;
	// UnityEngine.Color ShoppingMallManager::m_colorPageSelected_TextContent
	Color_t2555686324  ___m_colorPageSelected_TextContent_28;
	// UnityEngine.Color ShoppingMallManager::m_colorPageNotSelected_TextContent
	Color_t2555686324  ___m_colorPageNotSelected_TextContent_29;
	// UnityEngine.Color ShoppingMallManager::m_colorPageSelected_TextOutline
	Color_t2555686324  ___m_colorPageSelected_TextOutline_30;
	// UnityEngine.Color ShoppingMallManager::m_colorPageNotSelected_TextOutline
	Color_t2555686324  ___m_colorPageNotSelected_TextOutline_31;
	// UnityEngine.UI.Image[] ShoppingMallManager::m_aryPageButtons
	ImageU5BU5D_t2439009922* ___m_aryPageButtons_32;
	// UnityEngine.UI.Image[] ShoppingMallManager::m_aryPageButtons_Others
	ImageU5BU5D_t2439009922* ___m_aryPageButtons_Others_33;
	// UnityEngine.UI.Text[] ShoppingMallManager::m_aryPageTitleText
	TextU5BU5D_t422084607* ___m_aryPageTitleText_34;
	// Outline8[] ShoppingMallManager::m_aryPageTitleOutline
	Outline8U5BU5D_t830831472* ___m_aryPageTitleOutline_35;
	// System.Single ShoppingMallManager::m_fTextPricePos_NotBought
	float ___m_fTextPricePos_NotBought_36;
	// System.Single ShoppingMallManager::m_fTextPricePos_Bought
	float ___m_fTextPricePos_Bought_37;
	// System.String ShoppingMallManager::m_szExpireInfo
	String_t* ___m_szExpireInfo_38;
	// System.Int32 ShoppingMallManager::m_nFontSize_NotBought
	int32_t ___m_nFontSize_NotBought_39;
	// System.Int32 ShoppingMallManager::m_nFontSize_Bought
	int32_t ___m_nFontSize_Bought_40;
	// UnityEngine.UI.Text ShoppingMallManager::m_txtXiaoLianBiNum
	Text_t1901882714 * ___m_txtXiaoLianBiNum_41;
	// UnityEngine.UI.Text ShoppingMallManager::m_txtMieBiNum
	Text_t1901882714 * ___m_txtMieBiNum_42;
	// UnityEngine.GameObject ShoppingMallManager::_panelMall
	GameObject_t1113636619 * ____panelMall_43;
	// CRechargeCounter[] ShoppingMallManager::m_aryRechargeCounters
	CRechargeCounterU5BU5D_t1203895268* ___m_aryRechargeCounters_44;
	// CBuyConfirmUI ShoppingMallManager::_RechargeConfirmUI
	CBuyConfirmUI_t2670430472 * ____RechargeConfirmUI_45;
	// UnityEngine.GameObject ShoppingMallManager::_panelRecharge
	GameObject_t1113636619 * ____panelRecharge_46;
	// CyberTreeScrollView ShoppingMallManager::_svItemList
	CyberTreeScrollView_t257211719 * ____svItemList_47;
	// CyberTreeScrollView[] ShoppingMallManager::m_aryItemList
	CyberTreeScrollViewU5BU5D_t1486543294* ___m_aryItemList_48;
	// ShoppingMallManager/eShoppingMallPageType ShoppingMallManager::m_eCurPage
	int32_t ___m_eCurPage_49;
	// System.Collections.Generic.Dictionary`2<ShoppingMallManager/eShoppingMallPageType,System.Collections.Generic.List`1<CShoppingCounter>> ShoppingMallManager::m_dicItemId
	Dictionary_2_t4055342882 * ___m_dicItemId_50;
	// CShoppingCounter ShoppingMallManager::m_CurBuyingCounter
	CShoppingCounter_t3637302240 * ___m_CurBuyingCounter_54;
	// CShoppingCounter ShoppingMallManager::m_CurEquipedCounter
	CShoppingCounter_t3637302240 * ___m_CurEquipedCounter_55;
	// UnityEngine.UI.Button ShoppingMallManager::_btnConfirmBuy
	Button_t4055032469 * ____btnConfirmBuy_56;
	// UnityEngine.UI.Button ShoppingMallManager::_btnCancelBuy
	Button_t4055032469 * ____btnCancelBuy_57;
	// UnityEngine.GameObject ShoppingMallManager::_panelConfirmBuyUI
	GameObject_t1113636619 * ____panelConfirmBuyUI_58;
	// UnityEngine.UI.Text ShoppingMallManager::_txtItemName
	Text_t1901882714 * ____txtItemName_59;
	// UnityEngine.UI.Text ShoppingMallManager::_txtCost
	Text_t1901882714 * ____txtCost_60;
	// UnityEngine.UI.Image ShoppingMallManager::_imgMoneyType
	Image_t2670269651 * ____imgMoneyType_61;
	// UnityEngine.UI.Image ShoppingMallManager::_imgConfirmBuyAvatar
	Image_t2670269651 * ____imgConfirmBuyAvatar_62;
	// UnityEngine.UI.Button[] ShoppingMallManager::_aryConfirmBuy
	ButtonU5BU5D_t2297175928* ____aryConfirmBuy_63;
	// UnityEngine.UI.Button[] ShoppingMallManager::_aryCancelBuy
	ButtonU5BU5D_t2297175928* ____aryCancelBuy_64;
	// UnityEngine.GameObject[] ShoppingMallManager::_aryConfirmBuyUIPanel
	GameObjectU5BU5D_t3328599146* ____aryConfirmBuyUIPanel_65;
	// UnityEngine.UI.Text[] ShoppingMallManager::_aryItemName
	TextU5BU5D_t422084607* ____aryItemName_66;
	// UnityEngine.UI.Text[] ShoppingMallManager::_aryCost
	TextU5BU5D_t422084607* ____aryCost_67;
	// UnityEngine.UI.Image[] ShoppingMallManager::_aryMoneyType
	ImageU5BU5D_t2439009922* ____aryMoneyType_68;
	// UnityEngine.UI.Image[] ShoppingMallManager::_aryConfirmBuyAvatar
	ImageU5BU5D_t2439009922* ____aryConfirmBuyAvatar_69;
	// Spine.Unity.SkeletonGraphic ShoppingMallManager::m_skeletonGraphic
	SkeletonGraphic_t1744877482 * ___m_skeletonGraphic_70;
	// System.Boolean ShoppingMallManager::m_bRecharging
	bool ___m_bRecharging_71;
	// System.Single ShoppingMallManager::m_fBuyResultTimeCount
	float ___m_fBuyResultTimeCount_72;
	// CShoppingCounter ShoppingMallManager::m_CurSelectedCounter
	CShoppingCounter_t3637302240 * ___m_CurSelectedCounter_73;
	// UnityEngine.GameObject ShoppingMallManager::m_mutiskeletonGraphic
	GameObject_t1113636619 * ___m_mutiskeletonGraphic_74;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> ShoppingMallManager::m_dicMaterialForSkeleton
	Dictionary_2_t3524055750 * ___m_dicMaterialForSkeleton_75;
	// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Unity.SkeletonGraphic> ShoppingMallManager::m_dicSkeletonForSkin
	Dictionary_2_t633590813 * ___m_dicSkeletonForSkin_76;
	// UnityEngine.Sprite ShoppingMallManager::m_sprNoSkin
	Sprite_t280657092 * ___m_sprNoSkin_77;

public:
	inline static int32_t get_offset_of_m_aryContainerAll_3() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryContainerAll_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryContainerAll_3() const { return ___m_aryContainerAll_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryContainerAll_3() { return &___m_aryContainerAll_3; }
	inline void set_m_aryContainerAll_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryContainerAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryContainerAll_3), value);
	}

	inline static int32_t get_offset_of__CanvasScaler_4() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____CanvasScaler_4)); }
	inline CanvasScaler_t2767979955 * get__CanvasScaler_4() const { return ____CanvasScaler_4; }
	inline CanvasScaler_t2767979955 ** get_address_of__CanvasScaler_4() { return &____CanvasScaler_4; }
	inline void set__CanvasScaler_4(CanvasScaler_t2767979955 * value)
	{
		____CanvasScaler_4 = value;
		Il2CppCodeGenWriteBarrier((&____CanvasScaler_4), value);
	}

	inline static int32_t get_offset_of_m_fItemCounterInitScale_5() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fItemCounterInitScale_5)); }
	inline float get_m_fItemCounterInitScale_5() const { return ___m_fItemCounterInitScale_5; }
	inline float* get_address_of_m_fItemCounterInitScale_5() { return &___m_fItemCounterInitScale_5; }
	inline void set_m_fItemCounterInitScale_5(float value)
	{
		___m_fItemCounterInitScale_5 = value;
	}

	inline static int32_t get_offset_of_m_fItemCounterSelectedScale_6() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fItemCounterSelectedScale_6)); }
	inline float get_m_fItemCounterSelectedScale_6() const { return ___m_fItemCounterSelectedScale_6; }
	inline float* get_address_of_m_fItemCounterSelectedScale_6() { return &___m_fItemCounterSelectedScale_6; }
	inline void set_m_fItemCounterSelectedScale_6(float value)
	{
		___m_fItemCounterSelectedScale_6 = value;
	}

	inline static int32_t get_offset_of_m_fSkeletonPosY_7() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fSkeletonPosY_7)); }
	inline float get_m_fSkeletonPosY_7() const { return ___m_fSkeletonPosY_7; }
	inline float* get_address_of_m_fSkeletonPosY_7() { return &___m_fSkeletonPosY_7; }
	inline void set_m_fSkeletonPosY_7(float value)
	{
		___m_fSkeletonPosY_7 = value;
	}

	inline static int32_t get_offset_of_m_clItems_8() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_clItems_8)); }
	inline CCyberTreeList_t4040119676 * get_m_clItems_8() const { return ___m_clItems_8; }
	inline CCyberTreeList_t4040119676 ** get_address_of_m_clItems_8() { return &___m_clItems_8; }
	inline void set_m_clItems_8(CCyberTreeList_t4040119676 * value)
	{
		___m_clItems_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_clItems_8), value);
	}

	inline static int32_t get_offset_of_m_preShoppingCounter_9() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_preShoppingCounter_9)); }
	inline GameObject_t1113636619 * get_m_preShoppingCounter_9() const { return ___m_preShoppingCounter_9; }
	inline GameObject_t1113636619 ** get_address_of_m_preShoppingCounter_9() { return &___m_preShoppingCounter_9; }
	inline void set_m_preShoppingCounter_9(GameObject_t1113636619 * value)
	{
		___m_preShoppingCounter_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preShoppingCounter_9), value);
	}

	inline static int32_t get_offset_of_m_sprNotEquiped_10() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprNotEquiped_10)); }
	inline Sprite_t280657092 * get_m_sprNotEquiped_10() const { return ___m_sprNotEquiped_10; }
	inline Sprite_t280657092 ** get_address_of_m_sprNotEquiped_10() { return &___m_sprNotEquiped_10; }
	inline void set_m_sprNotEquiped_10(Sprite_t280657092 * value)
	{
		___m_sprNotEquiped_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprNotEquiped_10), value);
	}

	inline static int32_t get_offset_of_m_sprEquiped_11() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprEquiped_11)); }
	inline Sprite_t280657092 * get_m_sprEquiped_11() const { return ___m_sprEquiped_11; }
	inline Sprite_t280657092 ** get_address_of_m_sprEquiped_11() { return &___m_sprEquiped_11; }
	inline void set_m_sprEquiped_11(Sprite_t280657092 * value)
	{
		___m_sprEquiped_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprEquiped_11), value);
	}

	inline static int32_t get_offset_of_m_sprMoneyBg_NotBought_12() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprMoneyBg_NotBought_12)); }
	inline Sprite_t280657092 * get_m_sprMoneyBg_NotBought_12() const { return ___m_sprMoneyBg_NotBought_12; }
	inline Sprite_t280657092 ** get_address_of_m_sprMoneyBg_NotBought_12() { return &___m_sprMoneyBg_NotBought_12; }
	inline void set_m_sprMoneyBg_NotBought_12(Sprite_t280657092 * value)
	{
		___m_sprMoneyBg_NotBought_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMoneyBg_NotBought_12), value);
	}

	inline static int32_t get_offset_of_m_sprMoneyBg_Bought_13() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprMoneyBg_Bought_13)); }
	inline Sprite_t280657092 * get_m_sprMoneyBg_Bought_13() const { return ___m_sprMoneyBg_Bought_13; }
	inline Sprite_t280657092 ** get_address_of_m_sprMoneyBg_Bought_13() { return &___m_sprMoneyBg_Bought_13; }
	inline void set_m_sprMoneyBg_Bought_13(Sprite_t280657092 * value)
	{
		___m_sprMoneyBg_Bought_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMoneyBg_Bought_13), value);
	}

	inline static int32_t get_offset_of_m_fMoneyBgWidth_NotBought_14() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fMoneyBgWidth_NotBought_14)); }
	inline float get_m_fMoneyBgWidth_NotBought_14() const { return ___m_fMoneyBgWidth_NotBought_14; }
	inline float* get_address_of_m_fMoneyBgWidth_NotBought_14() { return &___m_fMoneyBgWidth_NotBought_14; }
	inline void set_m_fMoneyBgWidth_NotBought_14(float value)
	{
		___m_fMoneyBgWidth_NotBought_14 = value;
	}

	inline static int32_t get_offset_of_m_fMoneyBgWidth_Bought_15() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fMoneyBgWidth_Bought_15)); }
	inline float get_m_fMoneyBgWidth_Bought_15() const { return ___m_fMoneyBgWidth_Bought_15; }
	inline float* get_address_of_m_fMoneyBgWidth_Bought_15() { return &___m_fMoneyBgWidth_Bought_15; }
	inline void set_m_fMoneyBgWidth_Bought_15(float value)
	{
		___m_fMoneyBgWidth_Bought_15 = value;
	}

	inline static int32_t get_offset_of_m_fMoneyBgHeight_16() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fMoneyBgHeight_16)); }
	inline float get_m_fMoneyBgHeight_16() const { return ___m_fMoneyBgHeight_16; }
	inline float* get_address_of_m_fMoneyBgHeight_16() { return &___m_fMoneyBgHeight_16; }
	inline void set_m_fMoneyBgHeight_16(float value)
	{
		___m_fMoneyBgHeight_16 = value;
	}

	inline static int32_t get_offset_of_m_aryMoneyTypeSpr_17() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryMoneyTypeSpr_17)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryMoneyTypeSpr_17() const { return ___m_aryMoneyTypeSpr_17; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryMoneyTypeSpr_17() { return &___m_aryMoneyTypeSpr_17; }
	inline void set_m_aryMoneyTypeSpr_17(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryMoneyTypeSpr_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoneyTypeSpr_17), value);
	}

	inline static int32_t get_offset_of_m_colorBought_Text_18() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorBought_Text_18)); }
	inline Color_t2555686324  get_m_colorBought_Text_18() const { return ___m_colorBought_Text_18; }
	inline Color_t2555686324 * get_address_of_m_colorBought_Text_18() { return &___m_colorBought_Text_18; }
	inline void set_m_colorBought_Text_18(Color_t2555686324  value)
	{
		___m_colorBought_Text_18 = value;
	}

	inline static int32_t get_offset_of_m_colorBought_Bg_19() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorBought_Bg_19)); }
	inline Color_t2555686324  get_m_colorBought_Bg_19() const { return ___m_colorBought_Bg_19; }
	inline Color_t2555686324 * get_address_of_m_colorBought_Bg_19() { return &___m_colorBought_Bg_19; }
	inline void set_m_colorBought_Bg_19(Color_t2555686324  value)
	{
		___m_colorBought_Bg_19 = value;
	}

	inline static int32_t get_offset_of_m_colorNotBought_Text_20() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorNotBought_Text_20)); }
	inline Color_t2555686324  get_m_colorNotBought_Text_20() const { return ___m_colorNotBought_Text_20; }
	inline Color_t2555686324 * get_address_of_m_colorNotBought_Text_20() { return &___m_colorNotBought_Text_20; }
	inline void set_m_colorNotBought_Text_20(Color_t2555686324  value)
	{
		___m_colorNotBought_Text_20 = value;
	}

	inline static int32_t get_offset_of_m_colorNotBought_Bg_21() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorNotBought_Bg_21)); }
	inline Color_t2555686324  get_m_colorNotBought_Bg_21() const { return ___m_colorNotBought_Bg_21; }
	inline Color_t2555686324 * get_address_of_m_colorNotBought_Bg_21() { return &___m_colorNotBought_Bg_21; }
	inline void set_m_colorNotBought_Bg_21(Color_t2555686324  value)
	{
		___m_colorNotBought_Bg_21 = value;
	}

	inline static int32_t get_offset_of_m_colorBtnTimeLimit_Selected_22() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorBtnTimeLimit_Selected_22)); }
	inline Color_t2555686324  get_m_colorBtnTimeLimit_Selected_22() const { return ___m_colorBtnTimeLimit_Selected_22; }
	inline Color_t2555686324 * get_address_of_m_colorBtnTimeLimit_Selected_22() { return &___m_colorBtnTimeLimit_Selected_22; }
	inline void set_m_colorBtnTimeLimit_Selected_22(Color_t2555686324  value)
	{
		___m_colorBtnTimeLimit_Selected_22 = value;
	}

	inline static int32_t get_offset_of_m_colorBtnTimeLimit_NotSelected_23() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorBtnTimeLimit_NotSelected_23)); }
	inline Color_t2555686324  get_m_colorBtnTimeLimit_NotSelected_23() const { return ___m_colorBtnTimeLimit_NotSelected_23; }
	inline Color_t2555686324 * get_address_of_m_colorBtnTimeLimit_NotSelected_23() { return &___m_colorBtnTimeLimit_NotSelected_23; }
	inline void set_m_colorBtnTimeLimit_NotSelected_23(Color_t2555686324  value)
	{
		___m_colorBtnTimeLimit_NotSelected_23 = value;
	}

	inline static int32_t get_offset_of_m_aryBuyButtonStatus_24() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryBuyButtonStatus_24)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryBuyButtonStatus_24() const { return ___m_aryBuyButtonStatus_24; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryBuyButtonStatus_24() { return &___m_aryBuyButtonStatus_24; }
	inline void set_m_aryBuyButtonStatus_24(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryBuyButtonStatus_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBuyButtonStatus_24), value);
	}

	inline static int32_t get_offset_of_m_aryPageTitle_25() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryPageTitle_25)); }
	inline StringU5BU5D_t1281789340* get_m_aryPageTitle_25() const { return ___m_aryPageTitle_25; }
	inline StringU5BU5D_t1281789340** get_address_of_m_aryPageTitle_25() { return &___m_aryPageTitle_25; }
	inline void set_m_aryPageTitle_25(StringU5BU5D_t1281789340* value)
	{
		___m_aryPageTitle_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPageTitle_25), value);
	}

	inline static int32_t get_offset_of_m_sprPageSelected_26() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprPageSelected_26)); }
	inline Sprite_t280657092 * get_m_sprPageSelected_26() const { return ___m_sprPageSelected_26; }
	inline Sprite_t280657092 ** get_address_of_m_sprPageSelected_26() { return &___m_sprPageSelected_26; }
	inline void set_m_sprPageSelected_26(Sprite_t280657092 * value)
	{
		___m_sprPageSelected_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprPageSelected_26), value);
	}

	inline static int32_t get_offset_of_m_sprPageNotSelected_27() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprPageNotSelected_27)); }
	inline Sprite_t280657092 * get_m_sprPageNotSelected_27() const { return ___m_sprPageNotSelected_27; }
	inline Sprite_t280657092 ** get_address_of_m_sprPageNotSelected_27() { return &___m_sprPageNotSelected_27; }
	inline void set_m_sprPageNotSelected_27(Sprite_t280657092 * value)
	{
		___m_sprPageNotSelected_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprPageNotSelected_27), value);
	}

	inline static int32_t get_offset_of_m_colorPageSelected_TextContent_28() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorPageSelected_TextContent_28)); }
	inline Color_t2555686324  get_m_colorPageSelected_TextContent_28() const { return ___m_colorPageSelected_TextContent_28; }
	inline Color_t2555686324 * get_address_of_m_colorPageSelected_TextContent_28() { return &___m_colorPageSelected_TextContent_28; }
	inline void set_m_colorPageSelected_TextContent_28(Color_t2555686324  value)
	{
		___m_colorPageSelected_TextContent_28 = value;
	}

	inline static int32_t get_offset_of_m_colorPageNotSelected_TextContent_29() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorPageNotSelected_TextContent_29)); }
	inline Color_t2555686324  get_m_colorPageNotSelected_TextContent_29() const { return ___m_colorPageNotSelected_TextContent_29; }
	inline Color_t2555686324 * get_address_of_m_colorPageNotSelected_TextContent_29() { return &___m_colorPageNotSelected_TextContent_29; }
	inline void set_m_colorPageNotSelected_TextContent_29(Color_t2555686324  value)
	{
		___m_colorPageNotSelected_TextContent_29 = value;
	}

	inline static int32_t get_offset_of_m_colorPageSelected_TextOutline_30() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorPageSelected_TextOutline_30)); }
	inline Color_t2555686324  get_m_colorPageSelected_TextOutline_30() const { return ___m_colorPageSelected_TextOutline_30; }
	inline Color_t2555686324 * get_address_of_m_colorPageSelected_TextOutline_30() { return &___m_colorPageSelected_TextOutline_30; }
	inline void set_m_colorPageSelected_TextOutline_30(Color_t2555686324  value)
	{
		___m_colorPageSelected_TextOutline_30 = value;
	}

	inline static int32_t get_offset_of_m_colorPageNotSelected_TextOutline_31() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_colorPageNotSelected_TextOutline_31)); }
	inline Color_t2555686324  get_m_colorPageNotSelected_TextOutline_31() const { return ___m_colorPageNotSelected_TextOutline_31; }
	inline Color_t2555686324 * get_address_of_m_colorPageNotSelected_TextOutline_31() { return &___m_colorPageNotSelected_TextOutline_31; }
	inline void set_m_colorPageNotSelected_TextOutline_31(Color_t2555686324  value)
	{
		___m_colorPageNotSelected_TextOutline_31 = value;
	}

	inline static int32_t get_offset_of_m_aryPageButtons_32() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryPageButtons_32)); }
	inline ImageU5BU5D_t2439009922* get_m_aryPageButtons_32() const { return ___m_aryPageButtons_32; }
	inline ImageU5BU5D_t2439009922** get_address_of_m_aryPageButtons_32() { return &___m_aryPageButtons_32; }
	inline void set_m_aryPageButtons_32(ImageU5BU5D_t2439009922* value)
	{
		___m_aryPageButtons_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPageButtons_32), value);
	}

	inline static int32_t get_offset_of_m_aryPageButtons_Others_33() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryPageButtons_Others_33)); }
	inline ImageU5BU5D_t2439009922* get_m_aryPageButtons_Others_33() const { return ___m_aryPageButtons_Others_33; }
	inline ImageU5BU5D_t2439009922** get_address_of_m_aryPageButtons_Others_33() { return &___m_aryPageButtons_Others_33; }
	inline void set_m_aryPageButtons_Others_33(ImageU5BU5D_t2439009922* value)
	{
		___m_aryPageButtons_Others_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPageButtons_Others_33), value);
	}

	inline static int32_t get_offset_of_m_aryPageTitleText_34() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryPageTitleText_34)); }
	inline TextU5BU5D_t422084607* get_m_aryPageTitleText_34() const { return ___m_aryPageTitleText_34; }
	inline TextU5BU5D_t422084607** get_address_of_m_aryPageTitleText_34() { return &___m_aryPageTitleText_34; }
	inline void set_m_aryPageTitleText_34(TextU5BU5D_t422084607* value)
	{
		___m_aryPageTitleText_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPageTitleText_34), value);
	}

	inline static int32_t get_offset_of_m_aryPageTitleOutline_35() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryPageTitleOutline_35)); }
	inline Outline8U5BU5D_t830831472* get_m_aryPageTitleOutline_35() const { return ___m_aryPageTitleOutline_35; }
	inline Outline8U5BU5D_t830831472** get_address_of_m_aryPageTitleOutline_35() { return &___m_aryPageTitleOutline_35; }
	inline void set_m_aryPageTitleOutline_35(Outline8U5BU5D_t830831472* value)
	{
		___m_aryPageTitleOutline_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPageTitleOutline_35), value);
	}

	inline static int32_t get_offset_of_m_fTextPricePos_NotBought_36() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fTextPricePos_NotBought_36)); }
	inline float get_m_fTextPricePos_NotBought_36() const { return ___m_fTextPricePos_NotBought_36; }
	inline float* get_address_of_m_fTextPricePos_NotBought_36() { return &___m_fTextPricePos_NotBought_36; }
	inline void set_m_fTextPricePos_NotBought_36(float value)
	{
		___m_fTextPricePos_NotBought_36 = value;
	}

	inline static int32_t get_offset_of_m_fTextPricePos_Bought_37() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fTextPricePos_Bought_37)); }
	inline float get_m_fTextPricePos_Bought_37() const { return ___m_fTextPricePos_Bought_37; }
	inline float* get_address_of_m_fTextPricePos_Bought_37() { return &___m_fTextPricePos_Bought_37; }
	inline void set_m_fTextPricePos_Bought_37(float value)
	{
		___m_fTextPricePos_Bought_37 = value;
	}

	inline static int32_t get_offset_of_m_szExpireInfo_38() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_szExpireInfo_38)); }
	inline String_t* get_m_szExpireInfo_38() const { return ___m_szExpireInfo_38; }
	inline String_t** get_address_of_m_szExpireInfo_38() { return &___m_szExpireInfo_38; }
	inline void set_m_szExpireInfo_38(String_t* value)
	{
		___m_szExpireInfo_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_szExpireInfo_38), value);
	}

	inline static int32_t get_offset_of_m_nFontSize_NotBought_39() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_nFontSize_NotBought_39)); }
	inline int32_t get_m_nFontSize_NotBought_39() const { return ___m_nFontSize_NotBought_39; }
	inline int32_t* get_address_of_m_nFontSize_NotBought_39() { return &___m_nFontSize_NotBought_39; }
	inline void set_m_nFontSize_NotBought_39(int32_t value)
	{
		___m_nFontSize_NotBought_39 = value;
	}

	inline static int32_t get_offset_of_m_nFontSize_Bought_40() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_nFontSize_Bought_40)); }
	inline int32_t get_m_nFontSize_Bought_40() const { return ___m_nFontSize_Bought_40; }
	inline int32_t* get_address_of_m_nFontSize_Bought_40() { return &___m_nFontSize_Bought_40; }
	inline void set_m_nFontSize_Bought_40(int32_t value)
	{
		___m_nFontSize_Bought_40 = value;
	}

	inline static int32_t get_offset_of_m_txtXiaoLianBiNum_41() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_txtXiaoLianBiNum_41)); }
	inline Text_t1901882714 * get_m_txtXiaoLianBiNum_41() const { return ___m_txtXiaoLianBiNum_41; }
	inline Text_t1901882714 ** get_address_of_m_txtXiaoLianBiNum_41() { return &___m_txtXiaoLianBiNum_41; }
	inline void set_m_txtXiaoLianBiNum_41(Text_t1901882714 * value)
	{
		___m_txtXiaoLianBiNum_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtXiaoLianBiNum_41), value);
	}

	inline static int32_t get_offset_of_m_txtMieBiNum_42() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_txtMieBiNum_42)); }
	inline Text_t1901882714 * get_m_txtMieBiNum_42() const { return ___m_txtMieBiNum_42; }
	inline Text_t1901882714 ** get_address_of_m_txtMieBiNum_42() { return &___m_txtMieBiNum_42; }
	inline void set_m_txtMieBiNum_42(Text_t1901882714 * value)
	{
		___m_txtMieBiNum_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtMieBiNum_42), value);
	}

	inline static int32_t get_offset_of__panelMall_43() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____panelMall_43)); }
	inline GameObject_t1113636619 * get__panelMall_43() const { return ____panelMall_43; }
	inline GameObject_t1113636619 ** get_address_of__panelMall_43() { return &____panelMall_43; }
	inline void set__panelMall_43(GameObject_t1113636619 * value)
	{
		____panelMall_43 = value;
		Il2CppCodeGenWriteBarrier((&____panelMall_43), value);
	}

	inline static int32_t get_offset_of_m_aryRechargeCounters_44() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryRechargeCounters_44)); }
	inline CRechargeCounterU5BU5D_t1203895268* get_m_aryRechargeCounters_44() const { return ___m_aryRechargeCounters_44; }
	inline CRechargeCounterU5BU5D_t1203895268** get_address_of_m_aryRechargeCounters_44() { return &___m_aryRechargeCounters_44; }
	inline void set_m_aryRechargeCounters_44(CRechargeCounterU5BU5D_t1203895268* value)
	{
		___m_aryRechargeCounters_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRechargeCounters_44), value);
	}

	inline static int32_t get_offset_of__RechargeConfirmUI_45() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____RechargeConfirmUI_45)); }
	inline CBuyConfirmUI_t2670430472 * get__RechargeConfirmUI_45() const { return ____RechargeConfirmUI_45; }
	inline CBuyConfirmUI_t2670430472 ** get_address_of__RechargeConfirmUI_45() { return &____RechargeConfirmUI_45; }
	inline void set__RechargeConfirmUI_45(CBuyConfirmUI_t2670430472 * value)
	{
		____RechargeConfirmUI_45 = value;
		Il2CppCodeGenWriteBarrier((&____RechargeConfirmUI_45), value);
	}

	inline static int32_t get_offset_of__panelRecharge_46() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____panelRecharge_46)); }
	inline GameObject_t1113636619 * get__panelRecharge_46() const { return ____panelRecharge_46; }
	inline GameObject_t1113636619 ** get_address_of__panelRecharge_46() { return &____panelRecharge_46; }
	inline void set__panelRecharge_46(GameObject_t1113636619 * value)
	{
		____panelRecharge_46 = value;
		Il2CppCodeGenWriteBarrier((&____panelRecharge_46), value);
	}

	inline static int32_t get_offset_of__svItemList_47() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____svItemList_47)); }
	inline CyberTreeScrollView_t257211719 * get__svItemList_47() const { return ____svItemList_47; }
	inline CyberTreeScrollView_t257211719 ** get_address_of__svItemList_47() { return &____svItemList_47; }
	inline void set__svItemList_47(CyberTreeScrollView_t257211719 * value)
	{
		____svItemList_47 = value;
		Il2CppCodeGenWriteBarrier((&____svItemList_47), value);
	}

	inline static int32_t get_offset_of_m_aryItemList_48() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_aryItemList_48)); }
	inline CyberTreeScrollViewU5BU5D_t1486543294* get_m_aryItemList_48() const { return ___m_aryItemList_48; }
	inline CyberTreeScrollViewU5BU5D_t1486543294** get_address_of_m_aryItemList_48() { return &___m_aryItemList_48; }
	inline void set_m_aryItemList_48(CyberTreeScrollViewU5BU5D_t1486543294* value)
	{
		___m_aryItemList_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemList_48), value);
	}

	inline static int32_t get_offset_of_m_eCurPage_49() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_eCurPage_49)); }
	inline int32_t get_m_eCurPage_49() const { return ___m_eCurPage_49; }
	inline int32_t* get_address_of_m_eCurPage_49() { return &___m_eCurPage_49; }
	inline void set_m_eCurPage_49(int32_t value)
	{
		___m_eCurPage_49 = value;
	}

	inline static int32_t get_offset_of_m_dicItemId_50() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_dicItemId_50)); }
	inline Dictionary_2_t4055342882 * get_m_dicItemId_50() const { return ___m_dicItemId_50; }
	inline Dictionary_2_t4055342882 ** get_address_of_m_dicItemId_50() { return &___m_dicItemId_50; }
	inline void set_m_dicItemId_50(Dictionary_2_t4055342882 * value)
	{
		___m_dicItemId_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicItemId_50), value);
	}

	inline static int32_t get_offset_of_m_CurBuyingCounter_54() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_CurBuyingCounter_54)); }
	inline CShoppingCounter_t3637302240 * get_m_CurBuyingCounter_54() const { return ___m_CurBuyingCounter_54; }
	inline CShoppingCounter_t3637302240 ** get_address_of_m_CurBuyingCounter_54() { return &___m_CurBuyingCounter_54; }
	inline void set_m_CurBuyingCounter_54(CShoppingCounter_t3637302240 * value)
	{
		___m_CurBuyingCounter_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurBuyingCounter_54), value);
	}

	inline static int32_t get_offset_of_m_CurEquipedCounter_55() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_CurEquipedCounter_55)); }
	inline CShoppingCounter_t3637302240 * get_m_CurEquipedCounter_55() const { return ___m_CurEquipedCounter_55; }
	inline CShoppingCounter_t3637302240 ** get_address_of_m_CurEquipedCounter_55() { return &___m_CurEquipedCounter_55; }
	inline void set_m_CurEquipedCounter_55(CShoppingCounter_t3637302240 * value)
	{
		___m_CurEquipedCounter_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurEquipedCounter_55), value);
	}

	inline static int32_t get_offset_of__btnConfirmBuy_56() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____btnConfirmBuy_56)); }
	inline Button_t4055032469 * get__btnConfirmBuy_56() const { return ____btnConfirmBuy_56; }
	inline Button_t4055032469 ** get_address_of__btnConfirmBuy_56() { return &____btnConfirmBuy_56; }
	inline void set__btnConfirmBuy_56(Button_t4055032469 * value)
	{
		____btnConfirmBuy_56 = value;
		Il2CppCodeGenWriteBarrier((&____btnConfirmBuy_56), value);
	}

	inline static int32_t get_offset_of__btnCancelBuy_57() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____btnCancelBuy_57)); }
	inline Button_t4055032469 * get__btnCancelBuy_57() const { return ____btnCancelBuy_57; }
	inline Button_t4055032469 ** get_address_of__btnCancelBuy_57() { return &____btnCancelBuy_57; }
	inline void set__btnCancelBuy_57(Button_t4055032469 * value)
	{
		____btnCancelBuy_57 = value;
		Il2CppCodeGenWriteBarrier((&____btnCancelBuy_57), value);
	}

	inline static int32_t get_offset_of__panelConfirmBuyUI_58() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____panelConfirmBuyUI_58)); }
	inline GameObject_t1113636619 * get__panelConfirmBuyUI_58() const { return ____panelConfirmBuyUI_58; }
	inline GameObject_t1113636619 ** get_address_of__panelConfirmBuyUI_58() { return &____panelConfirmBuyUI_58; }
	inline void set__panelConfirmBuyUI_58(GameObject_t1113636619 * value)
	{
		____panelConfirmBuyUI_58 = value;
		Il2CppCodeGenWriteBarrier((&____panelConfirmBuyUI_58), value);
	}

	inline static int32_t get_offset_of__txtItemName_59() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____txtItemName_59)); }
	inline Text_t1901882714 * get__txtItemName_59() const { return ____txtItemName_59; }
	inline Text_t1901882714 ** get_address_of__txtItemName_59() { return &____txtItemName_59; }
	inline void set__txtItemName_59(Text_t1901882714 * value)
	{
		____txtItemName_59 = value;
		Il2CppCodeGenWriteBarrier((&____txtItemName_59), value);
	}

	inline static int32_t get_offset_of__txtCost_60() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____txtCost_60)); }
	inline Text_t1901882714 * get__txtCost_60() const { return ____txtCost_60; }
	inline Text_t1901882714 ** get_address_of__txtCost_60() { return &____txtCost_60; }
	inline void set__txtCost_60(Text_t1901882714 * value)
	{
		____txtCost_60 = value;
		Il2CppCodeGenWriteBarrier((&____txtCost_60), value);
	}

	inline static int32_t get_offset_of__imgMoneyType_61() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____imgMoneyType_61)); }
	inline Image_t2670269651 * get__imgMoneyType_61() const { return ____imgMoneyType_61; }
	inline Image_t2670269651 ** get_address_of__imgMoneyType_61() { return &____imgMoneyType_61; }
	inline void set__imgMoneyType_61(Image_t2670269651 * value)
	{
		____imgMoneyType_61 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyType_61), value);
	}

	inline static int32_t get_offset_of__imgConfirmBuyAvatar_62() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____imgConfirmBuyAvatar_62)); }
	inline Image_t2670269651 * get__imgConfirmBuyAvatar_62() const { return ____imgConfirmBuyAvatar_62; }
	inline Image_t2670269651 ** get_address_of__imgConfirmBuyAvatar_62() { return &____imgConfirmBuyAvatar_62; }
	inline void set__imgConfirmBuyAvatar_62(Image_t2670269651 * value)
	{
		____imgConfirmBuyAvatar_62 = value;
		Il2CppCodeGenWriteBarrier((&____imgConfirmBuyAvatar_62), value);
	}

	inline static int32_t get_offset_of__aryConfirmBuy_63() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryConfirmBuy_63)); }
	inline ButtonU5BU5D_t2297175928* get__aryConfirmBuy_63() const { return ____aryConfirmBuy_63; }
	inline ButtonU5BU5D_t2297175928** get_address_of__aryConfirmBuy_63() { return &____aryConfirmBuy_63; }
	inline void set__aryConfirmBuy_63(ButtonU5BU5D_t2297175928* value)
	{
		____aryConfirmBuy_63 = value;
		Il2CppCodeGenWriteBarrier((&____aryConfirmBuy_63), value);
	}

	inline static int32_t get_offset_of__aryCancelBuy_64() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryCancelBuy_64)); }
	inline ButtonU5BU5D_t2297175928* get__aryCancelBuy_64() const { return ____aryCancelBuy_64; }
	inline ButtonU5BU5D_t2297175928** get_address_of__aryCancelBuy_64() { return &____aryCancelBuy_64; }
	inline void set__aryCancelBuy_64(ButtonU5BU5D_t2297175928* value)
	{
		____aryCancelBuy_64 = value;
		Il2CppCodeGenWriteBarrier((&____aryCancelBuy_64), value);
	}

	inline static int32_t get_offset_of__aryConfirmBuyUIPanel_65() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryConfirmBuyUIPanel_65)); }
	inline GameObjectU5BU5D_t3328599146* get__aryConfirmBuyUIPanel_65() const { return ____aryConfirmBuyUIPanel_65; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__aryConfirmBuyUIPanel_65() { return &____aryConfirmBuyUIPanel_65; }
	inline void set__aryConfirmBuyUIPanel_65(GameObjectU5BU5D_t3328599146* value)
	{
		____aryConfirmBuyUIPanel_65 = value;
		Il2CppCodeGenWriteBarrier((&____aryConfirmBuyUIPanel_65), value);
	}

	inline static int32_t get_offset_of__aryItemName_66() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryItemName_66)); }
	inline TextU5BU5D_t422084607* get__aryItemName_66() const { return ____aryItemName_66; }
	inline TextU5BU5D_t422084607** get_address_of__aryItemName_66() { return &____aryItemName_66; }
	inline void set__aryItemName_66(TextU5BU5D_t422084607* value)
	{
		____aryItemName_66 = value;
		Il2CppCodeGenWriteBarrier((&____aryItemName_66), value);
	}

	inline static int32_t get_offset_of__aryCost_67() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryCost_67)); }
	inline TextU5BU5D_t422084607* get__aryCost_67() const { return ____aryCost_67; }
	inline TextU5BU5D_t422084607** get_address_of__aryCost_67() { return &____aryCost_67; }
	inline void set__aryCost_67(TextU5BU5D_t422084607* value)
	{
		____aryCost_67 = value;
		Il2CppCodeGenWriteBarrier((&____aryCost_67), value);
	}

	inline static int32_t get_offset_of__aryMoneyType_68() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryMoneyType_68)); }
	inline ImageU5BU5D_t2439009922* get__aryMoneyType_68() const { return ____aryMoneyType_68; }
	inline ImageU5BU5D_t2439009922** get_address_of__aryMoneyType_68() { return &____aryMoneyType_68; }
	inline void set__aryMoneyType_68(ImageU5BU5D_t2439009922* value)
	{
		____aryMoneyType_68 = value;
		Il2CppCodeGenWriteBarrier((&____aryMoneyType_68), value);
	}

	inline static int32_t get_offset_of__aryConfirmBuyAvatar_69() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ____aryConfirmBuyAvatar_69)); }
	inline ImageU5BU5D_t2439009922* get__aryConfirmBuyAvatar_69() const { return ____aryConfirmBuyAvatar_69; }
	inline ImageU5BU5D_t2439009922** get_address_of__aryConfirmBuyAvatar_69() { return &____aryConfirmBuyAvatar_69; }
	inline void set__aryConfirmBuyAvatar_69(ImageU5BU5D_t2439009922* value)
	{
		____aryConfirmBuyAvatar_69 = value;
		Il2CppCodeGenWriteBarrier((&____aryConfirmBuyAvatar_69), value);
	}

	inline static int32_t get_offset_of_m_skeletonGraphic_70() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_skeletonGraphic_70)); }
	inline SkeletonGraphic_t1744877482 * get_m_skeletonGraphic_70() const { return ___m_skeletonGraphic_70; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_m_skeletonGraphic_70() { return &___m_skeletonGraphic_70; }
	inline void set_m_skeletonGraphic_70(SkeletonGraphic_t1744877482 * value)
	{
		___m_skeletonGraphic_70 = value;
		Il2CppCodeGenWriteBarrier((&___m_skeletonGraphic_70), value);
	}

	inline static int32_t get_offset_of_m_bRecharging_71() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_bRecharging_71)); }
	inline bool get_m_bRecharging_71() const { return ___m_bRecharging_71; }
	inline bool* get_address_of_m_bRecharging_71() { return &___m_bRecharging_71; }
	inline void set_m_bRecharging_71(bool value)
	{
		___m_bRecharging_71 = value;
	}

	inline static int32_t get_offset_of_m_fBuyResultTimeCount_72() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_fBuyResultTimeCount_72)); }
	inline float get_m_fBuyResultTimeCount_72() const { return ___m_fBuyResultTimeCount_72; }
	inline float* get_address_of_m_fBuyResultTimeCount_72() { return &___m_fBuyResultTimeCount_72; }
	inline void set_m_fBuyResultTimeCount_72(float value)
	{
		___m_fBuyResultTimeCount_72 = value;
	}

	inline static int32_t get_offset_of_m_CurSelectedCounter_73() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_CurSelectedCounter_73)); }
	inline CShoppingCounter_t3637302240 * get_m_CurSelectedCounter_73() const { return ___m_CurSelectedCounter_73; }
	inline CShoppingCounter_t3637302240 ** get_address_of_m_CurSelectedCounter_73() { return &___m_CurSelectedCounter_73; }
	inline void set_m_CurSelectedCounter_73(CShoppingCounter_t3637302240 * value)
	{
		___m_CurSelectedCounter_73 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedCounter_73), value);
	}

	inline static int32_t get_offset_of_m_mutiskeletonGraphic_74() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_mutiskeletonGraphic_74)); }
	inline GameObject_t1113636619 * get_m_mutiskeletonGraphic_74() const { return ___m_mutiskeletonGraphic_74; }
	inline GameObject_t1113636619 ** get_address_of_m_mutiskeletonGraphic_74() { return &___m_mutiskeletonGraphic_74; }
	inline void set_m_mutiskeletonGraphic_74(GameObject_t1113636619 * value)
	{
		___m_mutiskeletonGraphic_74 = value;
		Il2CppCodeGenWriteBarrier((&___m_mutiskeletonGraphic_74), value);
	}

	inline static int32_t get_offset_of_m_dicMaterialForSkeleton_75() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_dicMaterialForSkeleton_75)); }
	inline Dictionary_2_t3524055750 * get_m_dicMaterialForSkeleton_75() const { return ___m_dicMaterialForSkeleton_75; }
	inline Dictionary_2_t3524055750 ** get_address_of_m_dicMaterialForSkeleton_75() { return &___m_dicMaterialForSkeleton_75; }
	inline void set_m_dicMaterialForSkeleton_75(Dictionary_2_t3524055750 * value)
	{
		___m_dicMaterialForSkeleton_75 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicMaterialForSkeleton_75), value);
	}

	inline static int32_t get_offset_of_m_dicSkeletonForSkin_76() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_dicSkeletonForSkin_76)); }
	inline Dictionary_2_t633590813 * get_m_dicSkeletonForSkin_76() const { return ___m_dicSkeletonForSkin_76; }
	inline Dictionary_2_t633590813 ** get_address_of_m_dicSkeletonForSkin_76() { return &___m_dicSkeletonForSkin_76; }
	inline void set_m_dicSkeletonForSkin_76(Dictionary_2_t633590813 * value)
	{
		___m_dicSkeletonForSkin_76 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkeletonForSkin_76), value);
	}

	inline static int32_t get_offset_of_m_sprNoSkin_77() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047, ___m_sprNoSkin_77)); }
	inline Sprite_t280657092 * get_m_sprNoSkin_77() const { return ___m_sprNoSkin_77; }
	inline Sprite_t280657092 ** get_address_of_m_sprNoSkin_77() { return &___m_sprNoSkin_77; }
	inline void set_m_sprNoSkin_77(Sprite_t280657092 * value)
	{
		___m_sprNoSkin_77 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprNoSkin_77), value);
	}
};

struct ShoppingMallManager_t2900135047_StaticFields
{
public:
	// ShoppingMallManager ShoppingMallManager::s_Instance
	ShoppingMallManager_t2900135047 * ___s_Instance_51;
	// System.Int32 ShoppingMallManager::m_sCurEquipedSkinId
	int32_t ___m_sCurEquipedSkinId_52;
	// System.String ShoppingMallManager::s_szCurEquipSkinPath
	String_t* ___s_szCurEquipSkinPath_53;

public:
	inline static int32_t get_offset_of_s_Instance_51() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047_StaticFields, ___s_Instance_51)); }
	inline ShoppingMallManager_t2900135047 * get_s_Instance_51() const { return ___s_Instance_51; }
	inline ShoppingMallManager_t2900135047 ** get_address_of_s_Instance_51() { return &___s_Instance_51; }
	inline void set_s_Instance_51(ShoppingMallManager_t2900135047 * value)
	{
		___s_Instance_51 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_51), value);
	}

	inline static int32_t get_offset_of_m_sCurEquipedSkinId_52() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047_StaticFields, ___m_sCurEquipedSkinId_52)); }
	inline int32_t get_m_sCurEquipedSkinId_52() const { return ___m_sCurEquipedSkinId_52; }
	inline int32_t* get_address_of_m_sCurEquipedSkinId_52() { return &___m_sCurEquipedSkinId_52; }
	inline void set_m_sCurEquipedSkinId_52(int32_t value)
	{
		___m_sCurEquipedSkinId_52 = value;
	}

	inline static int32_t get_offset_of_s_szCurEquipSkinPath_53() { return static_cast<int32_t>(offsetof(ShoppingMallManager_t2900135047_StaticFields, ___s_szCurEquipSkinPath_53)); }
	inline String_t* get_s_szCurEquipSkinPath_53() const { return ___s_szCurEquipSkinPath_53; }
	inline String_t** get_address_of_s_szCurEquipSkinPath_53() { return &___s_szCurEquipSkinPath_53; }
	inline void set_s_szCurEquipSkinPath_53(String_t* value)
	{
		___s_szCurEquipSkinPath_53 = value;
		Il2CppCodeGenWriteBarrier((&___s_szCurEquipSkinPath_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOPPINGMALLMANAGER_T2900135047_H
#ifndef CSKILLSYSTEM_T2262672040_H
#define CSKILLSYSTEM_T2262672040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem
struct  CSkillSystem_t2262672040  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CSkillSystem::_imgSpitSporeButtonBg
	Image_t2670269651 * ____imgSpitSporeButtonBg_3;
	// UnityEngine.GameObject CSkillSystem::_goCastButtonPanel_PC
	GameObject_t1113636619 * ____goCastButtonPanel_PC_4;
	// UnityEngine.GameObject CSkillSystem::_goCastButtonPanel_Mobile
	GameObject_t1113636619 * ____goCastButtonPanel_Mobile_5;
	// System.Int32 CSkillSystem::m_nMax_R_Num
	int32_t ___m_nMax_R_Num_9;
	// UnityEngine.Sprite[] CSkillSystem::_arySkillLevelUpSprites
	SpriteU5BU5D_t2581906349* ____arySkillLevelUpSprites_10;
	// UnityEngine.Sprite[] CSkillSystem::_arySkillArrowAppearSprites
	SpriteU5BU5D_t2581906349* ____arySkillArrowAppearSprites_11;
	// UnityEngine.Sprite[] CSkillSystem::_arySkillArrowBreatheSprites
	SpriteU5BU5D_t2581906349* ____arySkillArrowBreatheSprites_12;
	// UnityEngine.Sprite[] CSkillSystem::_arySkillSelectSkillSprites
	SpriteU5BU5D_t2581906349* ____arySkillSelectSkillSprites_13;
	// UnityEngine.UI.Button CSkillSystem::_btnPrevBtn
	Button_t4055032469 * ____btnPrevBtn_14;
	// UnityEngine.UI.Button CSkillSystem::_btnNextBtn
	Button_t4055032469 * ____btnNextBtn_15;
	// System.Int32 CSkillSystem::m_nCurPageIndex
	int32_t ___m_nCurPageIndex_16;
	// UnityEngine.GameObject[] CSkillSystem::_aryPages
	GameObjectU5BU5D_t3328599146* ____aryPages_17;
	// System.Int32 CSkillSystem::m_nPickSkillId
	int32_t ___m_nPickSkillId_21;
	// CSkillSystem/sSkillParam CSkillSystem::tempSkillParam
	sSkillParam_t731668951  ___tempSkillParam_23;
	// CSkillSystem/sSkillInfo CSkillSystem::tempSkillInfo
	sSkillInfo_t192725586  ___tempSkillInfo_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,CSkillSystem/sSkillInfo> CSkillSystem::m_dicSkillInfo
	Dictionary_2_t3376406213 * ___m_dicSkillInfo_25;
	// CSkillSystem/eSkillSysProgressBarType CSkillSystem::m_eCurSkillType
	int32_t ___m_eCurSkillType_26;
	// CProgressBar CSkillSystem::_progressBar_PC
	CProgressBar_t881982788 * ____progressBar_PC_27;
	// CProgressBar CSkillSystem::_progressBar_MOBILE
	CProgressBar_t881982788 * ____progressBar_MOBILE_28;
	// CProgressBar CSkillSystem::_progressBar
	CProgressBar_t881982788 * ____progressBar_29;
	// UnityEngine.GameObject CSkillSystem::_panelThis
	GameObject_t1113636619 * ____panelThis_30;
	// UnityEngine.UI.Text CSkillSystem::_txtProgressBar
	Text_t1901882714 * ____txtProgressBar_31;
	// UnityEngine.UI.Image CSkillSystem::_imgProgressBar
	Image_t2670269651 * ____imgProgressBar_32;
	// UnityEngine.UI.Text CSkillSystem::_txtCurTotalPoints
	Text_t1901882714 * ____txtCurTotalPoints_33;
	// System.Int32 CSkillSystem::m_nPoints
	int32_t ___m_nPoints_35;
	// CUISkill[] CSkillSystem::_arySkillCounter
	CUISkillU5BU5D_t2575728562* ____arySkillCounter_36;
	// UISkillConfig[] CSkillSystem::_arySkillParamsConfig
	UISkillConfigU5BU5D_t485806816* ____arySkillParamsConfig_37;
	// UISkillCastButton[] CSkillSystem::_arySkillCastButton
	UISkillCastButtonU5BU5D_t2561852851* ____arySkillCastButton_38;
	// UISkillCastButton[] CSkillSystem::_arySkillCastButton_PC
	UISkillCastButtonU5BU5D_t2561852851* ____arySkillCastButton_PC_39;
	// UISkillCastButton[] CSkillSystem::_arySkillCastButton_Mobile
	UISkillCastButtonU5BU5D_t2561852851* ____arySkillCastButton_Mobile_40;
	// UnityEngine.GameObject CSkillSystem::_panelEditSkillDesc
	GameObject_t1113636619 * ____panelEditSkillDesc_41;
	// UnityEngine.UI.InputField CSkillSystem::_inputSkillDescInput
	InputField_t3762917431 * ____inputSkillDescInput_42;
	// UnityEngine.UI.Button CSkillSystem::_btnQuitSkillDescEdit
	Button_t4055032469 * ____btnQuitSkillDescEdit_43;
	// UnityEngine.UI.Toggle CSkillSystem::_toggleShowCOlor
	Toggle_t2735377061 * ____toggleShowCOlor_44;
	// UnityEngine.UI.Dropdown CSkillSystem::_dropdownSkillId
	Dropdown_t2274391225 * ____dropdownSkillId_45;
	// System.Boolean CSkillSystem::m_bUiInited
	bool ___m_bUiInited_46;
	// System.Single CSkillSystem::m_fRefreshCastButtonTimeCount
	float ___m_fRefreshCastButtonTimeCount_47;
	// System.Int32 CSkillSystem::m_nSelectSkillCurPoint
	int32_t ___m_nSelectSkillCurPoint_48;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> CSkillSystem::m_dicSkillDesc
	Dictionary_2_t736164020 * ___m_dicSkillDesc_49;
	// System.String CSkillSystem::m_szCurEditingSkillDesc
	String_t* ___m_szCurEditingSkillDesc_50;
	// System.Int32 CSkillSystem::m_nCurEditingSkillDescSkillId
	int32_t ___m_nCurEditingSkillDescSkillId_51;
	// UnityEngine.UI.Text CSkillSystem::_textDesc
	Text_t1901882714 * ____textDesc_52;
	// CSkillSystem/sSpitSpoereParam CSkillSystem::m_SpitSporeParam
	sSpitSpoereParam_t3807642459  ___m_SpitSporeParam_53;
	// UnityEngine.UI.InputField CSkillSystem::_inputRadiusMultiple
	InputField_t3762917431 * ____inputRadiusMultiple_54;
	// UnityEngine.UI.InputField CSkillSystem::_inputCostMotherVolumePercent
	InputField_t3762917431 * ____inputCostMotherVolumePercent_55;
	// UnityEngine.UI.InputField CSkillSystem::_inputnNumPerSecond
	InputField_t3762917431 * ____inputnNumPerSecond_56;
	// UnityEngine.UI.InputField CSkillSystem::_inputEjectSpeed
	InputField_t3762917431 * ____inputEjectSpeed_57;
	// UnityEngine.UI.InputField CSkillSystem::_inputPushThornDis
	InputField_t3762917431 * ____inputPushThornDis_58;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeMinVolume
	InputField_t3762917431 * ____inputSporeMinVolume_59;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeMaxVolume
	InputField_t3762917431 * ____inputSporeMaxVolume_60;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeMaxShowRadius
	InputField_t3762917431 * ____inputSporeMaxShowRadius_61;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeA
	InputField_t3762917431 * ____inputSporeA_62;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeX
	InputField_t3762917431 * ____inputSporeX_63;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeB
	InputField_t3762917431 * ____inputSporeB_64;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeC
	InputField_t3762917431 * ____inputSporeC_65;
	// UnityEngine.UI.InputField CSkillSystem::_inputSporeHuanTingDistance
	InputField_t3762917431 * ____inputSporeHuanTingDistance_66;
	// System.Int32 CSkillSystem::m_nSporeBtnBlingCount
	int32_t ___m_nSporeBtnBlingCount_67;
	// UnityEngine.Color CSkillSystem::m_colorBling
	Color_t2555686324  ___m_colorBling_68;

public:
	inline static int32_t get_offset_of__imgSpitSporeButtonBg_3() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____imgSpitSporeButtonBg_3)); }
	inline Image_t2670269651 * get__imgSpitSporeButtonBg_3() const { return ____imgSpitSporeButtonBg_3; }
	inline Image_t2670269651 ** get_address_of__imgSpitSporeButtonBg_3() { return &____imgSpitSporeButtonBg_3; }
	inline void set__imgSpitSporeButtonBg_3(Image_t2670269651 * value)
	{
		____imgSpitSporeButtonBg_3 = value;
		Il2CppCodeGenWriteBarrier((&____imgSpitSporeButtonBg_3), value);
	}

	inline static int32_t get_offset_of__goCastButtonPanel_PC_4() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____goCastButtonPanel_PC_4)); }
	inline GameObject_t1113636619 * get__goCastButtonPanel_PC_4() const { return ____goCastButtonPanel_PC_4; }
	inline GameObject_t1113636619 ** get_address_of__goCastButtonPanel_PC_4() { return &____goCastButtonPanel_PC_4; }
	inline void set__goCastButtonPanel_PC_4(GameObject_t1113636619 * value)
	{
		____goCastButtonPanel_PC_4 = value;
		Il2CppCodeGenWriteBarrier((&____goCastButtonPanel_PC_4), value);
	}

	inline static int32_t get_offset_of__goCastButtonPanel_Mobile_5() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____goCastButtonPanel_Mobile_5)); }
	inline GameObject_t1113636619 * get__goCastButtonPanel_Mobile_5() const { return ____goCastButtonPanel_Mobile_5; }
	inline GameObject_t1113636619 ** get_address_of__goCastButtonPanel_Mobile_5() { return &____goCastButtonPanel_Mobile_5; }
	inline void set__goCastButtonPanel_Mobile_5(GameObject_t1113636619 * value)
	{
		____goCastButtonPanel_Mobile_5 = value;
		Il2CppCodeGenWriteBarrier((&____goCastButtonPanel_Mobile_5), value);
	}

	inline static int32_t get_offset_of_m_nMax_R_Num_9() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nMax_R_Num_9)); }
	inline int32_t get_m_nMax_R_Num_9() const { return ___m_nMax_R_Num_9; }
	inline int32_t* get_address_of_m_nMax_R_Num_9() { return &___m_nMax_R_Num_9; }
	inline void set_m_nMax_R_Num_9(int32_t value)
	{
		___m_nMax_R_Num_9 = value;
	}

	inline static int32_t get_offset_of__arySkillLevelUpSprites_10() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillLevelUpSprites_10)); }
	inline SpriteU5BU5D_t2581906349* get__arySkillLevelUpSprites_10() const { return ____arySkillLevelUpSprites_10; }
	inline SpriteU5BU5D_t2581906349** get_address_of__arySkillLevelUpSprites_10() { return &____arySkillLevelUpSprites_10; }
	inline void set__arySkillLevelUpSprites_10(SpriteU5BU5D_t2581906349* value)
	{
		____arySkillLevelUpSprites_10 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillLevelUpSprites_10), value);
	}

	inline static int32_t get_offset_of__arySkillArrowAppearSprites_11() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillArrowAppearSprites_11)); }
	inline SpriteU5BU5D_t2581906349* get__arySkillArrowAppearSprites_11() const { return ____arySkillArrowAppearSprites_11; }
	inline SpriteU5BU5D_t2581906349** get_address_of__arySkillArrowAppearSprites_11() { return &____arySkillArrowAppearSprites_11; }
	inline void set__arySkillArrowAppearSprites_11(SpriteU5BU5D_t2581906349* value)
	{
		____arySkillArrowAppearSprites_11 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillArrowAppearSprites_11), value);
	}

	inline static int32_t get_offset_of__arySkillArrowBreatheSprites_12() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillArrowBreatheSprites_12)); }
	inline SpriteU5BU5D_t2581906349* get__arySkillArrowBreatheSprites_12() const { return ____arySkillArrowBreatheSprites_12; }
	inline SpriteU5BU5D_t2581906349** get_address_of__arySkillArrowBreatheSprites_12() { return &____arySkillArrowBreatheSprites_12; }
	inline void set__arySkillArrowBreatheSprites_12(SpriteU5BU5D_t2581906349* value)
	{
		____arySkillArrowBreatheSprites_12 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillArrowBreatheSprites_12), value);
	}

	inline static int32_t get_offset_of__arySkillSelectSkillSprites_13() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillSelectSkillSprites_13)); }
	inline SpriteU5BU5D_t2581906349* get__arySkillSelectSkillSprites_13() const { return ____arySkillSelectSkillSprites_13; }
	inline SpriteU5BU5D_t2581906349** get_address_of__arySkillSelectSkillSprites_13() { return &____arySkillSelectSkillSprites_13; }
	inline void set__arySkillSelectSkillSprites_13(SpriteU5BU5D_t2581906349* value)
	{
		____arySkillSelectSkillSprites_13 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillSelectSkillSprites_13), value);
	}

	inline static int32_t get_offset_of__btnPrevBtn_14() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____btnPrevBtn_14)); }
	inline Button_t4055032469 * get__btnPrevBtn_14() const { return ____btnPrevBtn_14; }
	inline Button_t4055032469 ** get_address_of__btnPrevBtn_14() { return &____btnPrevBtn_14; }
	inline void set__btnPrevBtn_14(Button_t4055032469 * value)
	{
		____btnPrevBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____btnPrevBtn_14), value);
	}

	inline static int32_t get_offset_of__btnNextBtn_15() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____btnNextBtn_15)); }
	inline Button_t4055032469 * get__btnNextBtn_15() const { return ____btnNextBtn_15; }
	inline Button_t4055032469 ** get_address_of__btnNextBtn_15() { return &____btnNextBtn_15; }
	inline void set__btnNextBtn_15(Button_t4055032469 * value)
	{
		____btnNextBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____btnNextBtn_15), value);
	}

	inline static int32_t get_offset_of_m_nCurPageIndex_16() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nCurPageIndex_16)); }
	inline int32_t get_m_nCurPageIndex_16() const { return ___m_nCurPageIndex_16; }
	inline int32_t* get_address_of_m_nCurPageIndex_16() { return &___m_nCurPageIndex_16; }
	inline void set_m_nCurPageIndex_16(int32_t value)
	{
		___m_nCurPageIndex_16 = value;
	}

	inline static int32_t get_offset_of__aryPages_17() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____aryPages_17)); }
	inline GameObjectU5BU5D_t3328599146* get__aryPages_17() const { return ____aryPages_17; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__aryPages_17() { return &____aryPages_17; }
	inline void set__aryPages_17(GameObjectU5BU5D_t3328599146* value)
	{
		____aryPages_17 = value;
		Il2CppCodeGenWriteBarrier((&____aryPages_17), value);
	}

	inline static int32_t get_offset_of_m_nPickSkillId_21() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nPickSkillId_21)); }
	inline int32_t get_m_nPickSkillId_21() const { return ___m_nPickSkillId_21; }
	inline int32_t* get_address_of_m_nPickSkillId_21() { return &___m_nPickSkillId_21; }
	inline void set_m_nPickSkillId_21(int32_t value)
	{
		___m_nPickSkillId_21 = value;
	}

	inline static int32_t get_offset_of_tempSkillParam_23() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___tempSkillParam_23)); }
	inline sSkillParam_t731668951  get_tempSkillParam_23() const { return ___tempSkillParam_23; }
	inline sSkillParam_t731668951 * get_address_of_tempSkillParam_23() { return &___tempSkillParam_23; }
	inline void set_tempSkillParam_23(sSkillParam_t731668951  value)
	{
		___tempSkillParam_23 = value;
	}

	inline static int32_t get_offset_of_tempSkillInfo_24() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___tempSkillInfo_24)); }
	inline sSkillInfo_t192725586  get_tempSkillInfo_24() const { return ___tempSkillInfo_24; }
	inline sSkillInfo_t192725586 * get_address_of_tempSkillInfo_24() { return &___tempSkillInfo_24; }
	inline void set_tempSkillInfo_24(sSkillInfo_t192725586  value)
	{
		___tempSkillInfo_24 = value;
	}

	inline static int32_t get_offset_of_m_dicSkillInfo_25() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_dicSkillInfo_25)); }
	inline Dictionary_2_t3376406213 * get_m_dicSkillInfo_25() const { return ___m_dicSkillInfo_25; }
	inline Dictionary_2_t3376406213 ** get_address_of_m_dicSkillInfo_25() { return &___m_dicSkillInfo_25; }
	inline void set_m_dicSkillInfo_25(Dictionary_2_t3376406213 * value)
	{
		___m_dicSkillInfo_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkillInfo_25), value);
	}

	inline static int32_t get_offset_of_m_eCurSkillType_26() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_eCurSkillType_26)); }
	inline int32_t get_m_eCurSkillType_26() const { return ___m_eCurSkillType_26; }
	inline int32_t* get_address_of_m_eCurSkillType_26() { return &___m_eCurSkillType_26; }
	inline void set_m_eCurSkillType_26(int32_t value)
	{
		___m_eCurSkillType_26 = value;
	}

	inline static int32_t get_offset_of__progressBar_PC_27() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____progressBar_PC_27)); }
	inline CProgressBar_t881982788 * get__progressBar_PC_27() const { return ____progressBar_PC_27; }
	inline CProgressBar_t881982788 ** get_address_of__progressBar_PC_27() { return &____progressBar_PC_27; }
	inline void set__progressBar_PC_27(CProgressBar_t881982788 * value)
	{
		____progressBar_PC_27 = value;
		Il2CppCodeGenWriteBarrier((&____progressBar_PC_27), value);
	}

	inline static int32_t get_offset_of__progressBar_MOBILE_28() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____progressBar_MOBILE_28)); }
	inline CProgressBar_t881982788 * get__progressBar_MOBILE_28() const { return ____progressBar_MOBILE_28; }
	inline CProgressBar_t881982788 ** get_address_of__progressBar_MOBILE_28() { return &____progressBar_MOBILE_28; }
	inline void set__progressBar_MOBILE_28(CProgressBar_t881982788 * value)
	{
		____progressBar_MOBILE_28 = value;
		Il2CppCodeGenWriteBarrier((&____progressBar_MOBILE_28), value);
	}

	inline static int32_t get_offset_of__progressBar_29() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____progressBar_29)); }
	inline CProgressBar_t881982788 * get__progressBar_29() const { return ____progressBar_29; }
	inline CProgressBar_t881982788 ** get_address_of__progressBar_29() { return &____progressBar_29; }
	inline void set__progressBar_29(CProgressBar_t881982788 * value)
	{
		____progressBar_29 = value;
		Il2CppCodeGenWriteBarrier((&____progressBar_29), value);
	}

	inline static int32_t get_offset_of__panelThis_30() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____panelThis_30)); }
	inline GameObject_t1113636619 * get__panelThis_30() const { return ____panelThis_30; }
	inline GameObject_t1113636619 ** get_address_of__panelThis_30() { return &____panelThis_30; }
	inline void set__panelThis_30(GameObject_t1113636619 * value)
	{
		____panelThis_30 = value;
		Il2CppCodeGenWriteBarrier((&____panelThis_30), value);
	}

	inline static int32_t get_offset_of__txtProgressBar_31() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____txtProgressBar_31)); }
	inline Text_t1901882714 * get__txtProgressBar_31() const { return ____txtProgressBar_31; }
	inline Text_t1901882714 ** get_address_of__txtProgressBar_31() { return &____txtProgressBar_31; }
	inline void set__txtProgressBar_31(Text_t1901882714 * value)
	{
		____txtProgressBar_31 = value;
		Il2CppCodeGenWriteBarrier((&____txtProgressBar_31), value);
	}

	inline static int32_t get_offset_of__imgProgressBar_32() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____imgProgressBar_32)); }
	inline Image_t2670269651 * get__imgProgressBar_32() const { return ____imgProgressBar_32; }
	inline Image_t2670269651 ** get_address_of__imgProgressBar_32() { return &____imgProgressBar_32; }
	inline void set__imgProgressBar_32(Image_t2670269651 * value)
	{
		____imgProgressBar_32 = value;
		Il2CppCodeGenWriteBarrier((&____imgProgressBar_32), value);
	}

	inline static int32_t get_offset_of__txtCurTotalPoints_33() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____txtCurTotalPoints_33)); }
	inline Text_t1901882714 * get__txtCurTotalPoints_33() const { return ____txtCurTotalPoints_33; }
	inline Text_t1901882714 ** get_address_of__txtCurTotalPoints_33() { return &____txtCurTotalPoints_33; }
	inline void set__txtCurTotalPoints_33(Text_t1901882714 * value)
	{
		____txtCurTotalPoints_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurTotalPoints_33), value);
	}

	inline static int32_t get_offset_of_m_nPoints_35() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nPoints_35)); }
	inline int32_t get_m_nPoints_35() const { return ___m_nPoints_35; }
	inline int32_t* get_address_of_m_nPoints_35() { return &___m_nPoints_35; }
	inline void set_m_nPoints_35(int32_t value)
	{
		___m_nPoints_35 = value;
	}

	inline static int32_t get_offset_of__arySkillCounter_36() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillCounter_36)); }
	inline CUISkillU5BU5D_t2575728562* get__arySkillCounter_36() const { return ____arySkillCounter_36; }
	inline CUISkillU5BU5D_t2575728562** get_address_of__arySkillCounter_36() { return &____arySkillCounter_36; }
	inline void set__arySkillCounter_36(CUISkillU5BU5D_t2575728562* value)
	{
		____arySkillCounter_36 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillCounter_36), value);
	}

	inline static int32_t get_offset_of__arySkillParamsConfig_37() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillParamsConfig_37)); }
	inline UISkillConfigU5BU5D_t485806816* get__arySkillParamsConfig_37() const { return ____arySkillParamsConfig_37; }
	inline UISkillConfigU5BU5D_t485806816** get_address_of__arySkillParamsConfig_37() { return &____arySkillParamsConfig_37; }
	inline void set__arySkillParamsConfig_37(UISkillConfigU5BU5D_t485806816* value)
	{
		____arySkillParamsConfig_37 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillParamsConfig_37), value);
	}

	inline static int32_t get_offset_of__arySkillCastButton_38() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillCastButton_38)); }
	inline UISkillCastButtonU5BU5D_t2561852851* get__arySkillCastButton_38() const { return ____arySkillCastButton_38; }
	inline UISkillCastButtonU5BU5D_t2561852851** get_address_of__arySkillCastButton_38() { return &____arySkillCastButton_38; }
	inline void set__arySkillCastButton_38(UISkillCastButtonU5BU5D_t2561852851* value)
	{
		____arySkillCastButton_38 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillCastButton_38), value);
	}

	inline static int32_t get_offset_of__arySkillCastButton_PC_39() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillCastButton_PC_39)); }
	inline UISkillCastButtonU5BU5D_t2561852851* get__arySkillCastButton_PC_39() const { return ____arySkillCastButton_PC_39; }
	inline UISkillCastButtonU5BU5D_t2561852851** get_address_of__arySkillCastButton_PC_39() { return &____arySkillCastButton_PC_39; }
	inline void set__arySkillCastButton_PC_39(UISkillCastButtonU5BU5D_t2561852851* value)
	{
		____arySkillCastButton_PC_39 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillCastButton_PC_39), value);
	}

	inline static int32_t get_offset_of__arySkillCastButton_Mobile_40() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____arySkillCastButton_Mobile_40)); }
	inline UISkillCastButtonU5BU5D_t2561852851* get__arySkillCastButton_Mobile_40() const { return ____arySkillCastButton_Mobile_40; }
	inline UISkillCastButtonU5BU5D_t2561852851** get_address_of__arySkillCastButton_Mobile_40() { return &____arySkillCastButton_Mobile_40; }
	inline void set__arySkillCastButton_Mobile_40(UISkillCastButtonU5BU5D_t2561852851* value)
	{
		____arySkillCastButton_Mobile_40 = value;
		Il2CppCodeGenWriteBarrier((&____arySkillCastButton_Mobile_40), value);
	}

	inline static int32_t get_offset_of__panelEditSkillDesc_41() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____panelEditSkillDesc_41)); }
	inline GameObject_t1113636619 * get__panelEditSkillDesc_41() const { return ____panelEditSkillDesc_41; }
	inline GameObject_t1113636619 ** get_address_of__panelEditSkillDesc_41() { return &____panelEditSkillDesc_41; }
	inline void set__panelEditSkillDesc_41(GameObject_t1113636619 * value)
	{
		____panelEditSkillDesc_41 = value;
		Il2CppCodeGenWriteBarrier((&____panelEditSkillDesc_41), value);
	}

	inline static int32_t get_offset_of__inputSkillDescInput_42() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSkillDescInput_42)); }
	inline InputField_t3762917431 * get__inputSkillDescInput_42() const { return ____inputSkillDescInput_42; }
	inline InputField_t3762917431 ** get_address_of__inputSkillDescInput_42() { return &____inputSkillDescInput_42; }
	inline void set__inputSkillDescInput_42(InputField_t3762917431 * value)
	{
		____inputSkillDescInput_42 = value;
		Il2CppCodeGenWriteBarrier((&____inputSkillDescInput_42), value);
	}

	inline static int32_t get_offset_of__btnQuitSkillDescEdit_43() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____btnQuitSkillDescEdit_43)); }
	inline Button_t4055032469 * get__btnQuitSkillDescEdit_43() const { return ____btnQuitSkillDescEdit_43; }
	inline Button_t4055032469 ** get_address_of__btnQuitSkillDescEdit_43() { return &____btnQuitSkillDescEdit_43; }
	inline void set__btnQuitSkillDescEdit_43(Button_t4055032469 * value)
	{
		____btnQuitSkillDescEdit_43 = value;
		Il2CppCodeGenWriteBarrier((&____btnQuitSkillDescEdit_43), value);
	}

	inline static int32_t get_offset_of__toggleShowCOlor_44() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____toggleShowCOlor_44)); }
	inline Toggle_t2735377061 * get__toggleShowCOlor_44() const { return ____toggleShowCOlor_44; }
	inline Toggle_t2735377061 ** get_address_of__toggleShowCOlor_44() { return &____toggleShowCOlor_44; }
	inline void set__toggleShowCOlor_44(Toggle_t2735377061 * value)
	{
		____toggleShowCOlor_44 = value;
		Il2CppCodeGenWriteBarrier((&____toggleShowCOlor_44), value);
	}

	inline static int32_t get_offset_of__dropdownSkillId_45() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____dropdownSkillId_45)); }
	inline Dropdown_t2274391225 * get__dropdownSkillId_45() const { return ____dropdownSkillId_45; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownSkillId_45() { return &____dropdownSkillId_45; }
	inline void set__dropdownSkillId_45(Dropdown_t2274391225 * value)
	{
		____dropdownSkillId_45 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownSkillId_45), value);
	}

	inline static int32_t get_offset_of_m_bUiInited_46() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_bUiInited_46)); }
	inline bool get_m_bUiInited_46() const { return ___m_bUiInited_46; }
	inline bool* get_address_of_m_bUiInited_46() { return &___m_bUiInited_46; }
	inline void set_m_bUiInited_46(bool value)
	{
		___m_bUiInited_46 = value;
	}

	inline static int32_t get_offset_of_m_fRefreshCastButtonTimeCount_47() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_fRefreshCastButtonTimeCount_47)); }
	inline float get_m_fRefreshCastButtonTimeCount_47() const { return ___m_fRefreshCastButtonTimeCount_47; }
	inline float* get_address_of_m_fRefreshCastButtonTimeCount_47() { return &___m_fRefreshCastButtonTimeCount_47; }
	inline void set_m_fRefreshCastButtonTimeCount_47(float value)
	{
		___m_fRefreshCastButtonTimeCount_47 = value;
	}

	inline static int32_t get_offset_of_m_nSelectSkillCurPoint_48() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nSelectSkillCurPoint_48)); }
	inline int32_t get_m_nSelectSkillCurPoint_48() const { return ___m_nSelectSkillCurPoint_48; }
	inline int32_t* get_address_of_m_nSelectSkillCurPoint_48() { return &___m_nSelectSkillCurPoint_48; }
	inline void set_m_nSelectSkillCurPoint_48(int32_t value)
	{
		___m_nSelectSkillCurPoint_48 = value;
	}

	inline static int32_t get_offset_of_m_dicSkillDesc_49() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_dicSkillDesc_49)); }
	inline Dictionary_2_t736164020 * get_m_dicSkillDesc_49() const { return ___m_dicSkillDesc_49; }
	inline Dictionary_2_t736164020 ** get_address_of_m_dicSkillDesc_49() { return &___m_dicSkillDesc_49; }
	inline void set_m_dicSkillDesc_49(Dictionary_2_t736164020 * value)
	{
		___m_dicSkillDesc_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkillDesc_49), value);
	}

	inline static int32_t get_offset_of_m_szCurEditingSkillDesc_50() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_szCurEditingSkillDesc_50)); }
	inline String_t* get_m_szCurEditingSkillDesc_50() const { return ___m_szCurEditingSkillDesc_50; }
	inline String_t** get_address_of_m_szCurEditingSkillDesc_50() { return &___m_szCurEditingSkillDesc_50; }
	inline void set_m_szCurEditingSkillDesc_50(String_t* value)
	{
		___m_szCurEditingSkillDesc_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCurEditingSkillDesc_50), value);
	}

	inline static int32_t get_offset_of_m_nCurEditingSkillDescSkillId_51() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nCurEditingSkillDescSkillId_51)); }
	inline int32_t get_m_nCurEditingSkillDescSkillId_51() const { return ___m_nCurEditingSkillDescSkillId_51; }
	inline int32_t* get_address_of_m_nCurEditingSkillDescSkillId_51() { return &___m_nCurEditingSkillDescSkillId_51; }
	inline void set_m_nCurEditingSkillDescSkillId_51(int32_t value)
	{
		___m_nCurEditingSkillDescSkillId_51 = value;
	}

	inline static int32_t get_offset_of__textDesc_52() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____textDesc_52)); }
	inline Text_t1901882714 * get__textDesc_52() const { return ____textDesc_52; }
	inline Text_t1901882714 ** get_address_of__textDesc_52() { return &____textDesc_52; }
	inline void set__textDesc_52(Text_t1901882714 * value)
	{
		____textDesc_52 = value;
		Il2CppCodeGenWriteBarrier((&____textDesc_52), value);
	}

	inline static int32_t get_offset_of_m_SpitSporeParam_53() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_SpitSporeParam_53)); }
	inline sSpitSpoereParam_t3807642459  get_m_SpitSporeParam_53() const { return ___m_SpitSporeParam_53; }
	inline sSpitSpoereParam_t3807642459 * get_address_of_m_SpitSporeParam_53() { return &___m_SpitSporeParam_53; }
	inline void set_m_SpitSporeParam_53(sSpitSpoereParam_t3807642459  value)
	{
		___m_SpitSporeParam_53 = value;
	}

	inline static int32_t get_offset_of__inputRadiusMultiple_54() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputRadiusMultiple_54)); }
	inline InputField_t3762917431 * get__inputRadiusMultiple_54() const { return ____inputRadiusMultiple_54; }
	inline InputField_t3762917431 ** get_address_of__inputRadiusMultiple_54() { return &____inputRadiusMultiple_54; }
	inline void set__inputRadiusMultiple_54(InputField_t3762917431 * value)
	{
		____inputRadiusMultiple_54 = value;
		Il2CppCodeGenWriteBarrier((&____inputRadiusMultiple_54), value);
	}

	inline static int32_t get_offset_of__inputCostMotherVolumePercent_55() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputCostMotherVolumePercent_55)); }
	inline InputField_t3762917431 * get__inputCostMotherVolumePercent_55() const { return ____inputCostMotherVolumePercent_55; }
	inline InputField_t3762917431 ** get_address_of__inputCostMotherVolumePercent_55() { return &____inputCostMotherVolumePercent_55; }
	inline void set__inputCostMotherVolumePercent_55(InputField_t3762917431 * value)
	{
		____inputCostMotherVolumePercent_55 = value;
		Il2CppCodeGenWriteBarrier((&____inputCostMotherVolumePercent_55), value);
	}

	inline static int32_t get_offset_of__inputnNumPerSecond_56() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputnNumPerSecond_56)); }
	inline InputField_t3762917431 * get__inputnNumPerSecond_56() const { return ____inputnNumPerSecond_56; }
	inline InputField_t3762917431 ** get_address_of__inputnNumPerSecond_56() { return &____inputnNumPerSecond_56; }
	inline void set__inputnNumPerSecond_56(InputField_t3762917431 * value)
	{
		____inputnNumPerSecond_56 = value;
		Il2CppCodeGenWriteBarrier((&____inputnNumPerSecond_56), value);
	}

	inline static int32_t get_offset_of__inputEjectSpeed_57() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputEjectSpeed_57)); }
	inline InputField_t3762917431 * get__inputEjectSpeed_57() const { return ____inputEjectSpeed_57; }
	inline InputField_t3762917431 ** get_address_of__inputEjectSpeed_57() { return &____inputEjectSpeed_57; }
	inline void set__inputEjectSpeed_57(InputField_t3762917431 * value)
	{
		____inputEjectSpeed_57 = value;
		Il2CppCodeGenWriteBarrier((&____inputEjectSpeed_57), value);
	}

	inline static int32_t get_offset_of__inputPushThornDis_58() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputPushThornDis_58)); }
	inline InputField_t3762917431 * get__inputPushThornDis_58() const { return ____inputPushThornDis_58; }
	inline InputField_t3762917431 ** get_address_of__inputPushThornDis_58() { return &____inputPushThornDis_58; }
	inline void set__inputPushThornDis_58(InputField_t3762917431 * value)
	{
		____inputPushThornDis_58 = value;
		Il2CppCodeGenWriteBarrier((&____inputPushThornDis_58), value);
	}

	inline static int32_t get_offset_of__inputSporeMinVolume_59() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeMinVolume_59)); }
	inline InputField_t3762917431 * get__inputSporeMinVolume_59() const { return ____inputSporeMinVolume_59; }
	inline InputField_t3762917431 ** get_address_of__inputSporeMinVolume_59() { return &____inputSporeMinVolume_59; }
	inline void set__inputSporeMinVolume_59(InputField_t3762917431 * value)
	{
		____inputSporeMinVolume_59 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeMinVolume_59), value);
	}

	inline static int32_t get_offset_of__inputSporeMaxVolume_60() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeMaxVolume_60)); }
	inline InputField_t3762917431 * get__inputSporeMaxVolume_60() const { return ____inputSporeMaxVolume_60; }
	inline InputField_t3762917431 ** get_address_of__inputSporeMaxVolume_60() { return &____inputSporeMaxVolume_60; }
	inline void set__inputSporeMaxVolume_60(InputField_t3762917431 * value)
	{
		____inputSporeMaxVolume_60 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeMaxVolume_60), value);
	}

	inline static int32_t get_offset_of__inputSporeMaxShowRadius_61() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeMaxShowRadius_61)); }
	inline InputField_t3762917431 * get__inputSporeMaxShowRadius_61() const { return ____inputSporeMaxShowRadius_61; }
	inline InputField_t3762917431 ** get_address_of__inputSporeMaxShowRadius_61() { return &____inputSporeMaxShowRadius_61; }
	inline void set__inputSporeMaxShowRadius_61(InputField_t3762917431 * value)
	{
		____inputSporeMaxShowRadius_61 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeMaxShowRadius_61), value);
	}

	inline static int32_t get_offset_of__inputSporeA_62() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeA_62)); }
	inline InputField_t3762917431 * get__inputSporeA_62() const { return ____inputSporeA_62; }
	inline InputField_t3762917431 ** get_address_of__inputSporeA_62() { return &____inputSporeA_62; }
	inline void set__inputSporeA_62(InputField_t3762917431 * value)
	{
		____inputSporeA_62 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeA_62), value);
	}

	inline static int32_t get_offset_of__inputSporeX_63() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeX_63)); }
	inline InputField_t3762917431 * get__inputSporeX_63() const { return ____inputSporeX_63; }
	inline InputField_t3762917431 ** get_address_of__inputSporeX_63() { return &____inputSporeX_63; }
	inline void set__inputSporeX_63(InputField_t3762917431 * value)
	{
		____inputSporeX_63 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeX_63), value);
	}

	inline static int32_t get_offset_of__inputSporeB_64() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeB_64)); }
	inline InputField_t3762917431 * get__inputSporeB_64() const { return ____inputSporeB_64; }
	inline InputField_t3762917431 ** get_address_of__inputSporeB_64() { return &____inputSporeB_64; }
	inline void set__inputSporeB_64(InputField_t3762917431 * value)
	{
		____inputSporeB_64 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeB_64), value);
	}

	inline static int32_t get_offset_of__inputSporeC_65() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeC_65)); }
	inline InputField_t3762917431 * get__inputSporeC_65() const { return ____inputSporeC_65; }
	inline InputField_t3762917431 ** get_address_of__inputSporeC_65() { return &____inputSporeC_65; }
	inline void set__inputSporeC_65(InputField_t3762917431 * value)
	{
		____inputSporeC_65 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeC_65), value);
	}

	inline static int32_t get_offset_of__inputSporeHuanTingDistance_66() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ____inputSporeHuanTingDistance_66)); }
	inline InputField_t3762917431 * get__inputSporeHuanTingDistance_66() const { return ____inputSporeHuanTingDistance_66; }
	inline InputField_t3762917431 ** get_address_of__inputSporeHuanTingDistance_66() { return &____inputSporeHuanTingDistance_66; }
	inline void set__inputSporeHuanTingDistance_66(InputField_t3762917431 * value)
	{
		____inputSporeHuanTingDistance_66 = value;
		Il2CppCodeGenWriteBarrier((&____inputSporeHuanTingDistance_66), value);
	}

	inline static int32_t get_offset_of_m_nSporeBtnBlingCount_67() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_nSporeBtnBlingCount_67)); }
	inline int32_t get_m_nSporeBtnBlingCount_67() const { return ___m_nSporeBtnBlingCount_67; }
	inline int32_t* get_address_of_m_nSporeBtnBlingCount_67() { return &___m_nSporeBtnBlingCount_67; }
	inline void set_m_nSporeBtnBlingCount_67(int32_t value)
	{
		___m_nSporeBtnBlingCount_67 = value;
	}

	inline static int32_t get_offset_of_m_colorBling_68() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040, ___m_colorBling_68)); }
	inline Color_t2555686324  get_m_colorBling_68() const { return ___m_colorBling_68; }
	inline Color_t2555686324 * get_address_of_m_colorBling_68() { return &___m_colorBling_68; }
	inline void set_m_colorBling_68(Color_t2555686324  value)
	{
		___m_colorBling_68 = value;
	}
};

struct CSkillSystem_t2262672040_StaticFields
{
public:
	// UnityEngine.Color CSkillSystem::colorTemp
	Color_t2555686324  ___colorTemp_2;
	// CSkillSystem CSkillSystem::s_Instance
	CSkillSystem_t2262672040 * ___s_Instance_34;

public:
	inline static int32_t get_offset_of_colorTemp_2() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040_StaticFields, ___colorTemp_2)); }
	inline Color_t2555686324  get_colorTemp_2() const { return ___colorTemp_2; }
	inline Color_t2555686324 * get_address_of_colorTemp_2() { return &___colorTemp_2; }
	inline void set_colorTemp_2(Color_t2555686324  value)
	{
		___colorTemp_2 = value;
	}

	inline static int32_t get_offset_of_s_Instance_34() { return static_cast<int32_t>(offsetof(CSkillSystem_t2262672040_StaticFields, ___s_Instance_34)); }
	inline CSkillSystem_t2262672040 * get_s_Instance_34() const { return ___s_Instance_34; }
	inline CSkillSystem_t2262672040 ** get_address_of_s_Instance_34() { return &___s_Instance_34; }
	inline void set_s_Instance_34(CSkillSystem_t2262672040 * value)
	{
		___s_Instance_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSKILLSYSTEM_T2262672040_H
#ifndef CSKILLDETAILGRID_T2161361482_H
#define CSKILLDETAILGRID_T2161361482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillDetailGrid
struct  CSkillDetailGrid_t2161361482  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CSkillDetailGrid::_txtSpeedAffect
	Text_t1901882714 * ____txtSpeedAffect_2;
	// UnityEngine.UI.Text CSkillDetailGrid::_txtCD
	Text_t1901882714 * ____txtCD_3;
	// UnityEngine.UI.Text CSkillDetailGrid::_txtDuration
	Text_t1901882714 * ____txtDuration_4;
	// UnityEngine.UI.Text CSkillDetailGrid::_txtMpCost
	Text_t1901882714 * ____txtMpCost_5;

public:
	inline static int32_t get_offset_of__txtSpeedAffect_2() { return static_cast<int32_t>(offsetof(CSkillDetailGrid_t2161361482, ____txtSpeedAffect_2)); }
	inline Text_t1901882714 * get__txtSpeedAffect_2() const { return ____txtSpeedAffect_2; }
	inline Text_t1901882714 ** get_address_of__txtSpeedAffect_2() { return &____txtSpeedAffect_2; }
	inline void set__txtSpeedAffect_2(Text_t1901882714 * value)
	{
		____txtSpeedAffect_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtSpeedAffect_2), value);
	}

	inline static int32_t get_offset_of__txtCD_3() { return static_cast<int32_t>(offsetof(CSkillDetailGrid_t2161361482, ____txtCD_3)); }
	inline Text_t1901882714 * get__txtCD_3() const { return ____txtCD_3; }
	inline Text_t1901882714 ** get_address_of__txtCD_3() { return &____txtCD_3; }
	inline void set__txtCD_3(Text_t1901882714 * value)
	{
		____txtCD_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtCD_3), value);
	}

	inline static int32_t get_offset_of__txtDuration_4() { return static_cast<int32_t>(offsetof(CSkillDetailGrid_t2161361482, ____txtDuration_4)); }
	inline Text_t1901882714 * get__txtDuration_4() const { return ____txtDuration_4; }
	inline Text_t1901882714 ** get_address_of__txtDuration_4() { return &____txtDuration_4; }
	inline void set__txtDuration_4(Text_t1901882714 * value)
	{
		____txtDuration_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtDuration_4), value);
	}

	inline static int32_t get_offset_of__txtMpCost_5() { return static_cast<int32_t>(offsetof(CSkillDetailGrid_t2161361482, ____txtMpCost_5)); }
	inline Text_t1901882714 * get__txtMpCost_5() const { return ____txtMpCost_5; }
	inline Text_t1901882714 ** get_address_of__txtMpCost_5() { return &____txtMpCost_5; }
	inline void set__txtMpCost_5(Text_t1901882714 * value)
	{
		____txtMpCost_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtMpCost_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSKILLDETAILGRID_T2161361482_H
#ifndef CSELECTROOMMANAGER_T1525971949_H
#define CSELECTROOMMANAGER_T1525971949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectRoomManager
struct  CSelectRoomManager_t1525971949  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color CSelectRoomManager::m_colorSelected
	Color_t2555686324  ___m_colorSelected_3;
	// UnityEngine.GameObject CSelectRoomManager::_panelSelectRoom
	GameObject_t1113636619 * ____panelSelectRoom_5;
	// UnityEngine.GameObject[] CSelectRoomManager::m_arySelectRoomPanel
	GameObjectU5BU5D_t3328599146* ___m_arySelectRoomPanel_6;
	// CCommonJinDuTiao CSelectRoomManager::_jindutiao
	CCommonJinDuTiao_t2836400377 * ____jindutiao_7;
	// UnityEngine.UI.Toggle CSelectRoomManager::_toggleEnterTestRoom
	Toggle_t2735377061 * ____toggleEnterTestRoom_8;
	// UnityEngine.UI.Toggle CSelectRoomManager::_toggleReadFromLocal
	Toggle_t2735377061 * ____toggleReadFromLocal_9;
	// CRoom[] CSelectRoomManager::m_aryRooms
	CRoomU5BU5D_t1935531732* ___m_aryRooms_10;
	// CRoom[] CSelectRoomManager::m_aryRooms_Others
	CRoomU5BU5D_t1935531732* ___m_aryRooms_Others_11;
	// UnityEngine.Sprite[] CSelectRoomManager::m_aryRoomAvatar
	SpriteU5BU5D_t2581906349* ___m_aryRoomAvatar_12;
	// UnityEngine.Sprite CSelectRoomManager::m_sprNotSelected
	Sprite_t280657092 * ___m_sprNotSelected_13;
	// UnityEngine.Sprite CSelectRoomManager::m_sprSelected
	Sprite_t280657092 * ___m_sprSelected_14;
	// UnityEngine.UI.Text CSelectRoomManager::_txtCurSelectedRoomName
	Text_t1901882714 * ____txtCurSelectedRoomName_15;
	// UnityEngine.UI.Text[] CSelectRoomManager::m_arySelectedRoomName
	TextU5BU5D_t422084607* ___m_arySelectedRoomName_16;
	// CRoom CSelectRoomManager::m_CurSelectedRoom
	CRoom_t2285119849 * ___m_CurSelectedRoom_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CSelectRoomManager::m_dicCurRoomInfo
	Dictionary_2_t1839659084 * ___m_dicCurRoomInfo_18;

public:
	inline static int32_t get_offset_of_m_colorSelected_3() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_colorSelected_3)); }
	inline Color_t2555686324  get_m_colorSelected_3() const { return ___m_colorSelected_3; }
	inline Color_t2555686324 * get_address_of_m_colorSelected_3() { return &___m_colorSelected_3; }
	inline void set_m_colorSelected_3(Color_t2555686324  value)
	{
		___m_colorSelected_3 = value;
	}

	inline static int32_t get_offset_of__panelSelectRoom_5() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ____panelSelectRoom_5)); }
	inline GameObject_t1113636619 * get__panelSelectRoom_5() const { return ____panelSelectRoom_5; }
	inline GameObject_t1113636619 ** get_address_of__panelSelectRoom_5() { return &____panelSelectRoom_5; }
	inline void set__panelSelectRoom_5(GameObject_t1113636619 * value)
	{
		____panelSelectRoom_5 = value;
		Il2CppCodeGenWriteBarrier((&____panelSelectRoom_5), value);
	}

	inline static int32_t get_offset_of_m_arySelectRoomPanel_6() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_arySelectRoomPanel_6)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySelectRoomPanel_6() const { return ___m_arySelectRoomPanel_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySelectRoomPanel_6() { return &___m_arySelectRoomPanel_6; }
	inline void set_m_arySelectRoomPanel_6(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySelectRoomPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySelectRoomPanel_6), value);
	}

	inline static int32_t get_offset_of__jindutiao_7() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ____jindutiao_7)); }
	inline CCommonJinDuTiao_t2836400377 * get__jindutiao_7() const { return ____jindutiao_7; }
	inline CCommonJinDuTiao_t2836400377 ** get_address_of__jindutiao_7() { return &____jindutiao_7; }
	inline void set__jindutiao_7(CCommonJinDuTiao_t2836400377 * value)
	{
		____jindutiao_7 = value;
		Il2CppCodeGenWriteBarrier((&____jindutiao_7), value);
	}

	inline static int32_t get_offset_of__toggleEnterTestRoom_8() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ____toggleEnterTestRoom_8)); }
	inline Toggle_t2735377061 * get__toggleEnterTestRoom_8() const { return ____toggleEnterTestRoom_8; }
	inline Toggle_t2735377061 ** get_address_of__toggleEnterTestRoom_8() { return &____toggleEnterTestRoom_8; }
	inline void set__toggleEnterTestRoom_8(Toggle_t2735377061 * value)
	{
		____toggleEnterTestRoom_8 = value;
		Il2CppCodeGenWriteBarrier((&____toggleEnterTestRoom_8), value);
	}

	inline static int32_t get_offset_of__toggleReadFromLocal_9() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ____toggleReadFromLocal_9)); }
	inline Toggle_t2735377061 * get__toggleReadFromLocal_9() const { return ____toggleReadFromLocal_9; }
	inline Toggle_t2735377061 ** get_address_of__toggleReadFromLocal_9() { return &____toggleReadFromLocal_9; }
	inline void set__toggleReadFromLocal_9(Toggle_t2735377061 * value)
	{
		____toggleReadFromLocal_9 = value;
		Il2CppCodeGenWriteBarrier((&____toggleReadFromLocal_9), value);
	}

	inline static int32_t get_offset_of_m_aryRooms_10() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_aryRooms_10)); }
	inline CRoomU5BU5D_t1935531732* get_m_aryRooms_10() const { return ___m_aryRooms_10; }
	inline CRoomU5BU5D_t1935531732** get_address_of_m_aryRooms_10() { return &___m_aryRooms_10; }
	inline void set_m_aryRooms_10(CRoomU5BU5D_t1935531732* value)
	{
		___m_aryRooms_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRooms_10), value);
	}

	inline static int32_t get_offset_of_m_aryRooms_Others_11() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_aryRooms_Others_11)); }
	inline CRoomU5BU5D_t1935531732* get_m_aryRooms_Others_11() const { return ___m_aryRooms_Others_11; }
	inline CRoomU5BU5D_t1935531732** get_address_of_m_aryRooms_Others_11() { return &___m_aryRooms_Others_11; }
	inline void set_m_aryRooms_Others_11(CRoomU5BU5D_t1935531732* value)
	{
		___m_aryRooms_Others_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRooms_Others_11), value);
	}

	inline static int32_t get_offset_of_m_aryRoomAvatar_12() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_aryRoomAvatar_12)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryRoomAvatar_12() const { return ___m_aryRoomAvatar_12; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryRoomAvatar_12() { return &___m_aryRoomAvatar_12; }
	inline void set_m_aryRoomAvatar_12(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryRoomAvatar_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRoomAvatar_12), value);
	}

	inline static int32_t get_offset_of_m_sprNotSelected_13() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_sprNotSelected_13)); }
	inline Sprite_t280657092 * get_m_sprNotSelected_13() const { return ___m_sprNotSelected_13; }
	inline Sprite_t280657092 ** get_address_of_m_sprNotSelected_13() { return &___m_sprNotSelected_13; }
	inline void set_m_sprNotSelected_13(Sprite_t280657092 * value)
	{
		___m_sprNotSelected_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprNotSelected_13), value);
	}

	inline static int32_t get_offset_of_m_sprSelected_14() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_sprSelected_14)); }
	inline Sprite_t280657092 * get_m_sprSelected_14() const { return ___m_sprSelected_14; }
	inline Sprite_t280657092 ** get_address_of_m_sprSelected_14() { return &___m_sprSelected_14; }
	inline void set_m_sprSelected_14(Sprite_t280657092 * value)
	{
		___m_sprSelected_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprSelected_14), value);
	}

	inline static int32_t get_offset_of__txtCurSelectedRoomName_15() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ____txtCurSelectedRoomName_15)); }
	inline Text_t1901882714 * get__txtCurSelectedRoomName_15() const { return ____txtCurSelectedRoomName_15; }
	inline Text_t1901882714 ** get_address_of__txtCurSelectedRoomName_15() { return &____txtCurSelectedRoomName_15; }
	inline void set__txtCurSelectedRoomName_15(Text_t1901882714 * value)
	{
		____txtCurSelectedRoomName_15 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurSelectedRoomName_15), value);
	}

	inline static int32_t get_offset_of_m_arySelectedRoomName_16() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_arySelectedRoomName_16)); }
	inline TextU5BU5D_t422084607* get_m_arySelectedRoomName_16() const { return ___m_arySelectedRoomName_16; }
	inline TextU5BU5D_t422084607** get_address_of_m_arySelectedRoomName_16() { return &___m_arySelectedRoomName_16; }
	inline void set_m_arySelectedRoomName_16(TextU5BU5D_t422084607* value)
	{
		___m_arySelectedRoomName_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySelectedRoomName_16), value);
	}

	inline static int32_t get_offset_of_m_CurSelectedRoom_17() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_CurSelectedRoom_17)); }
	inline CRoom_t2285119849 * get_m_CurSelectedRoom_17() const { return ___m_CurSelectedRoom_17; }
	inline CRoom_t2285119849 ** get_address_of_m_CurSelectedRoom_17() { return &___m_CurSelectedRoom_17; }
	inline void set_m_CurSelectedRoom_17(CRoom_t2285119849 * value)
	{
		___m_CurSelectedRoom_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedRoom_17), value);
	}

	inline static int32_t get_offset_of_m_dicCurRoomInfo_18() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949, ___m_dicCurRoomInfo_18)); }
	inline Dictionary_2_t1839659084 * get_m_dicCurRoomInfo_18() const { return ___m_dicCurRoomInfo_18; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicCurRoomInfo_18() { return &___m_dicCurRoomInfo_18; }
	inline void set_m_dicCurRoomInfo_18(Dictionary_2_t1839659084 * value)
	{
		___m_dicCurRoomInfo_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCurRoomInfo_18), value);
	}
};

struct CSelectRoomManager_t1525971949_StaticFields
{
public:
	// CSelectRoomManager CSelectRoomManager::s_Instance
	CSelectRoomManager_t1525971949 * ___s_Instance_2;
	// System.Boolean CSelectRoomManager::s_bEnterTestRoom
	bool ___s_bEnterTestRoom_19;
	// System.Boolean CSelectRoomManager::s_bReadFromLocal
	bool ___s_bReadFromLocal_20;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949_StaticFields, ___s_Instance_2)); }
	inline CSelectRoomManager_t1525971949 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CSelectRoomManager_t1525971949 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CSelectRoomManager_t1525971949 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_s_bEnterTestRoom_19() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949_StaticFields, ___s_bEnterTestRoom_19)); }
	inline bool get_s_bEnterTestRoom_19() const { return ___s_bEnterTestRoom_19; }
	inline bool* get_address_of_s_bEnterTestRoom_19() { return &___s_bEnterTestRoom_19; }
	inline void set_s_bEnterTestRoom_19(bool value)
	{
		___s_bEnterTestRoom_19 = value;
	}

	inline static int32_t get_offset_of_s_bReadFromLocal_20() { return static_cast<int32_t>(offsetof(CSelectRoomManager_t1525971949_StaticFields, ___s_bReadFromLocal_20)); }
	inline bool get_s_bReadFromLocal_20() const { return ___s_bReadFromLocal_20; }
	inline bool* get_address_of_s_bReadFromLocal_20() { return &___s_bReadFromLocal_20; }
	inline void set_s_bReadFromLocal_20(bool value)
	{
		___s_bReadFromLocal_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSELECTROOMMANAGER_T1525971949_H
#ifndef CLIGHTSYSTEM_T2157026498_H
#define CLIGHTSYSTEM_T2157026498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLightSystem
struct  CLightSystem_t2157026498  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CLightSystem::m_aryPrePoint
	GameObjectU5BU5D_t3328599146* ___m_aryPrePoint_2;
	// System.Collections.Generic.Dictionary`2<CLightSystem/eLightType,System.Collections.Generic.List`1<CLight>> CLightSystem::m_dicRecycledLights
	Dictionary_2_t3959599972 * ___m_dicRecycledLights_3;

public:
	inline static int32_t get_offset_of_m_aryPrePoint_2() { return static_cast<int32_t>(offsetof(CLightSystem_t2157026498, ___m_aryPrePoint_2)); }
	inline GameObjectU5BU5D_t3328599146* get_m_aryPrePoint_2() const { return ___m_aryPrePoint_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_aryPrePoint_2() { return &___m_aryPrePoint_2; }
	inline void set_m_aryPrePoint_2(GameObjectU5BU5D_t3328599146* value)
	{
		___m_aryPrePoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPrePoint_2), value);
	}

	inline static int32_t get_offset_of_m_dicRecycledLights_3() { return static_cast<int32_t>(offsetof(CLightSystem_t2157026498, ___m_dicRecycledLights_3)); }
	inline Dictionary_2_t3959599972 * get_m_dicRecycledLights_3() const { return ___m_dicRecycledLights_3; }
	inline Dictionary_2_t3959599972 ** get_address_of_m_dicRecycledLights_3() { return &___m_dicRecycledLights_3; }
	inline void set_m_dicRecycledLights_3(Dictionary_2_t3959599972 * value)
	{
		___m_dicRecycledLights_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledLights_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIGHTSYSTEM_T2157026498_H
#ifndef CSELECTGAMEMODE_T2151979979_H
#define CSELECTGAMEMODE_T2151979979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectGameMode
struct  CSelectGameMode_t2151979979  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CSelectGameMode::_panelSelectGameMode
	GameObject_t1113636619 * ____panelSelectGameMode_3;
	// UnityEngine.GameObject CSelectGameMode::_panelSelectSkill
	GameObject_t1113636619 * ____panelSelectSkill_4;
	// CSelectGameMode/eGameMode CSelectGameMode::m_eGameMode
	int32_t ___m_eGameMode_5;
	// UnityEngine.GameObject[] CSelectGameMode::_goContainerPages
	GameObjectU5BU5D_t3328599146* ____goContainerPages_6;
	// System.Int32 CSelectGameMode::m_nCurSelectedPageIndex
	int32_t ___m_nCurSelectedPageIndex_7;
	// CButtonClick[] CSelectGameMode::_aryPageBtns
	CButtonClickU5BU5D_t2296231296* ____aryPageBtns_8;
	// UnityEngine.UI.Text CSelectGameMode::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_9;
	// UnityEngine.UI.Button CSelectGameMode::_btnSelectGameMode_DaLuanDou
	Button_t4055032469 * ____btnSelectGameMode_DaLuanDou_10;
	// UnityEngine.UI.Button CSelectGameMode::_btnSelectGameMode_WangZhongWang
	Button_t4055032469 * ____btnSelectGameMode_WangZhongWang_11;
	// CGameModeSelectCounter CSelectGameMode::_counterDaLuanDou
	CGameModeSelectCounter_t2640523211 * ____counterDaLuanDou_12;
	// CGameModeSelectCounter CSelectGameMode::_counterDaWangZHongWang
	CGameModeSelectCounter_t2640523211 * ____counterDaWangZHongWang_13;

public:
	inline static int32_t get_offset_of__panelSelectGameMode_3() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____panelSelectGameMode_3)); }
	inline GameObject_t1113636619 * get__panelSelectGameMode_3() const { return ____panelSelectGameMode_3; }
	inline GameObject_t1113636619 ** get_address_of__panelSelectGameMode_3() { return &____panelSelectGameMode_3; }
	inline void set__panelSelectGameMode_3(GameObject_t1113636619 * value)
	{
		____panelSelectGameMode_3 = value;
		Il2CppCodeGenWriteBarrier((&____panelSelectGameMode_3), value);
	}

	inline static int32_t get_offset_of__panelSelectSkill_4() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____panelSelectSkill_4)); }
	inline GameObject_t1113636619 * get__panelSelectSkill_4() const { return ____panelSelectSkill_4; }
	inline GameObject_t1113636619 ** get_address_of__panelSelectSkill_4() { return &____panelSelectSkill_4; }
	inline void set__panelSelectSkill_4(GameObject_t1113636619 * value)
	{
		____panelSelectSkill_4 = value;
		Il2CppCodeGenWriteBarrier((&____panelSelectSkill_4), value);
	}

	inline static int32_t get_offset_of_m_eGameMode_5() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ___m_eGameMode_5)); }
	inline int32_t get_m_eGameMode_5() const { return ___m_eGameMode_5; }
	inline int32_t* get_address_of_m_eGameMode_5() { return &___m_eGameMode_5; }
	inline void set_m_eGameMode_5(int32_t value)
	{
		___m_eGameMode_5 = value;
	}

	inline static int32_t get_offset_of__goContainerPages_6() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____goContainerPages_6)); }
	inline GameObjectU5BU5D_t3328599146* get__goContainerPages_6() const { return ____goContainerPages_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__goContainerPages_6() { return &____goContainerPages_6; }
	inline void set__goContainerPages_6(GameObjectU5BU5D_t3328599146* value)
	{
		____goContainerPages_6 = value;
		Il2CppCodeGenWriteBarrier((&____goContainerPages_6), value);
	}

	inline static int32_t get_offset_of_m_nCurSelectedPageIndex_7() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ___m_nCurSelectedPageIndex_7)); }
	inline int32_t get_m_nCurSelectedPageIndex_7() const { return ___m_nCurSelectedPageIndex_7; }
	inline int32_t* get_address_of_m_nCurSelectedPageIndex_7() { return &___m_nCurSelectedPageIndex_7; }
	inline void set_m_nCurSelectedPageIndex_7(int32_t value)
	{
		___m_nCurSelectedPageIndex_7 = value;
	}

	inline static int32_t get_offset_of__aryPageBtns_8() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____aryPageBtns_8)); }
	inline CButtonClickU5BU5D_t2296231296* get__aryPageBtns_8() const { return ____aryPageBtns_8; }
	inline CButtonClickU5BU5D_t2296231296** get_address_of__aryPageBtns_8() { return &____aryPageBtns_8; }
	inline void set__aryPageBtns_8(CButtonClickU5BU5D_t2296231296* value)
	{
		____aryPageBtns_8 = value;
		Il2CppCodeGenWriteBarrier((&____aryPageBtns_8), value);
	}

	inline static int32_t get_offset_of__txtPlayerName_9() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____txtPlayerName_9)); }
	inline Text_t1901882714 * get__txtPlayerName_9() const { return ____txtPlayerName_9; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_9() { return &____txtPlayerName_9; }
	inline void set__txtPlayerName_9(Text_t1901882714 * value)
	{
		____txtPlayerName_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_9), value);
	}

	inline static int32_t get_offset_of__btnSelectGameMode_DaLuanDou_10() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____btnSelectGameMode_DaLuanDou_10)); }
	inline Button_t4055032469 * get__btnSelectGameMode_DaLuanDou_10() const { return ____btnSelectGameMode_DaLuanDou_10; }
	inline Button_t4055032469 ** get_address_of__btnSelectGameMode_DaLuanDou_10() { return &____btnSelectGameMode_DaLuanDou_10; }
	inline void set__btnSelectGameMode_DaLuanDou_10(Button_t4055032469 * value)
	{
		____btnSelectGameMode_DaLuanDou_10 = value;
		Il2CppCodeGenWriteBarrier((&____btnSelectGameMode_DaLuanDou_10), value);
	}

	inline static int32_t get_offset_of__btnSelectGameMode_WangZhongWang_11() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____btnSelectGameMode_WangZhongWang_11)); }
	inline Button_t4055032469 * get__btnSelectGameMode_WangZhongWang_11() const { return ____btnSelectGameMode_WangZhongWang_11; }
	inline Button_t4055032469 ** get_address_of__btnSelectGameMode_WangZhongWang_11() { return &____btnSelectGameMode_WangZhongWang_11; }
	inline void set__btnSelectGameMode_WangZhongWang_11(Button_t4055032469 * value)
	{
		____btnSelectGameMode_WangZhongWang_11 = value;
		Il2CppCodeGenWriteBarrier((&____btnSelectGameMode_WangZhongWang_11), value);
	}

	inline static int32_t get_offset_of__counterDaLuanDou_12() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____counterDaLuanDou_12)); }
	inline CGameModeSelectCounter_t2640523211 * get__counterDaLuanDou_12() const { return ____counterDaLuanDou_12; }
	inline CGameModeSelectCounter_t2640523211 ** get_address_of__counterDaLuanDou_12() { return &____counterDaLuanDou_12; }
	inline void set__counterDaLuanDou_12(CGameModeSelectCounter_t2640523211 * value)
	{
		____counterDaLuanDou_12 = value;
		Il2CppCodeGenWriteBarrier((&____counterDaLuanDou_12), value);
	}

	inline static int32_t get_offset_of__counterDaWangZHongWang_13() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979, ____counterDaWangZHongWang_13)); }
	inline CGameModeSelectCounter_t2640523211 * get__counterDaWangZHongWang_13() const { return ____counterDaWangZHongWang_13; }
	inline CGameModeSelectCounter_t2640523211 ** get_address_of__counterDaWangZHongWang_13() { return &____counterDaWangZHongWang_13; }
	inline void set__counterDaWangZHongWang_13(CGameModeSelectCounter_t2640523211 * value)
	{
		____counterDaWangZHongWang_13 = value;
		Il2CppCodeGenWriteBarrier((&____counterDaWangZHongWang_13), value);
	}
};

struct CSelectGameMode_t2151979979_StaticFields
{
public:
	// CSelectGameMode CSelectGameMode::s_Instance
	CSelectGameMode_t2151979979 * ___s_Instance_2;
	// UnityEngine.Vector3 CSelectGameMode::vecTempScale
	Vector3_t3722313464  ___vecTempScale_14;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979_StaticFields, ___s_Instance_2)); }
	inline CSelectGameMode_t2151979979 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CSelectGameMode_t2151979979 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CSelectGameMode_t2151979979 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempScale_14() { return static_cast<int32_t>(offsetof(CSelectGameMode_t2151979979_StaticFields, ___vecTempScale_14)); }
	inline Vector3_t3722313464  get_vecTempScale_14() const { return ___vecTempScale_14; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_14() { return &___vecTempScale_14; }
	inline void set_vecTempScale_14(Vector3_t3722313464  value)
	{
		___vecTempScale_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSELECTGAMEMODE_T2151979979_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef CJISHAINFO_T3249703697_H
#define CJISHAINFO_T3249703697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CJiShaInfo
struct  CJiShaInfo_t3249703697  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CJiShaInfo::m_fShowTime
	float ___m_fShowTime_5;
	// System.Single CJiShaInfo::m_fShowTimeCount
	float ___m_fShowTimeCount_6;
	// UnityEngine.GameObject CJiShaInfo::_panelJiSha
	GameObject_t1113636619 * ____panelJiSha_7;
	// UnityEngine.UI.Text CJiShaInfo::_txtKillerName
	Text_t1901882714 * ____txtKillerName_8;
	// UnityEngine.UI.Text CJiShaInfo::_txtDeadMeatName
	Text_t1901882714 * ____txtDeadMeatName_9;
	// UnityEngine.UI.Image CJiShaInfo::_imgKillerAvatar
	Image_t2670269651 * ____imgKillerAvatar_10;
	// UnityEngine.UI.Image CJiShaInfo::_imgDeadMeatAvatar
	Image_t2670269651 * ____imgDeadMeatAvatar_11;
	// UnityEngine.GameObject CJiShaInfo::_panelJiSha_PC
	GameObject_t1113636619 * ____panelJiSha_PC_12;
	// UnityEngine.UI.Text CJiShaInfo::_txtKillerName_PC
	Text_t1901882714 * ____txtKillerName_PC_13;
	// UnityEngine.UI.Text CJiShaInfo::_txtDeadMeatName_PC
	Text_t1901882714 * ____txtDeadMeatName_PC_14;
	// UnityEngine.UI.Image CJiShaInfo::_imgKillerAvatar_PC
	Image_t2670269651 * ____imgKillerAvatar_PC_15;
	// UnityEngine.UI.Image CJiShaInfo::_imgDeadMeatAvatar_PC
	Image_t2670269651 * ____imgDeadMeatAvatar_PC_16;
	// UnityEngine.GameObject CJiShaInfo::_panelJiSha_MOBILE
	GameObject_t1113636619 * ____panelJiSha_MOBILE_17;
	// UnityEngine.UI.Text CJiShaInfo::_txtKillerName_MOBILE
	Text_t1901882714 * ____txtKillerName_MOBILE_18;
	// UnityEngine.UI.Text CJiShaInfo::_txtDeadMeatName_MOBILE
	Text_t1901882714 * ____txtDeadMeatName_MOBILE_19;
	// UnityEngine.UI.Image CJiShaInfo::_imgKillerAvatar_MOBILE
	Image_t2670269651 * ____imgKillerAvatar_MOBILE_20;
	// UnityEngine.UI.Image CJiShaInfo::_imgDeadMeatAvatar_MOBILE
	Image_t2670269651 * ____imgDeadMeatAvatar_MOBILE_21;
	// UnityEngine.UI.Image CJiShaInfo::_imgBgBar
	Image_t2670269651 * ____imgBgBar_22;
	// UnityEngine.UI.Text CJiShaInfo::_txtAssistTitle
	Text_t1901882714 * ____txtAssistTitle_23;
	// UnityEngine.GameObject CJiShaInfo::_goAssistAvatarContainer
	GameObject_t1113636619 * ____goAssistAvatarContainer_24;
	// UnityEngine.GameObject CJiShaInfo::_preAvatar
	GameObject_t1113636619 * ____preAvatar_25;
	// UnityEngine.UI.Text CJiShaInfo::_txtJiSha
	Text_t1901882714 * ____txtJiSha_26;
	// UnityEngine.CanvasGroup CJiShaInfo::_CanvasGroup
	CanvasGroup_t4083511760 * ____CanvasGroup_27;
	// UnityEngine.GameObject CJiShaInfo::_containerKillerAvatar
	GameObject_t1113636619 * ____containerKillerAvatar_28;
	// UnityEngine.GameObject CJiShaInfo::_containerDeadmeatAvatar
	GameObject_t1113636619 * ____containerDeadmeatAvatar_29;
	// UnityEngine.UI.Toggle CJiShaInfo::_toggleNoName
	Toggle_t2735377061 * ____toggleNoName_30;
	// System.Boolean CJiShaInfo::m_bShow
	bool ___m_bShow_31;
	// System.Single CJiShaInfo::m_fAssistAvatarGap
	float ___m_fAssistAvatarGap_32;
	// System.Single CJiShaInfo::m_fAssistAvatarScale
	float ___m_fAssistAvatarScale_33;
	// System.Single CJiShaInfo::m_fDuoShaTime
	float ___m_fDuoShaTime_34;
	// System.Single CJiShaInfo::m_fAssistTime
	float ___m_fAssistTime_35;
	// System.Single CJiShaInfo::m_nContinuousKillStartThreshold
	float ___m_nContinuousKillStartThreshold_36;
	// System.Single CJiShaInfo::m_fFadeTime
	float ___m_fFadeTime_37;
	// System.Boolean CJiShaInfo::m_bShowName
	bool ___m_bShowName_38;
	// System.Single CJiShaInfo::m_fAssistContainerPosY
	float ___m_fAssistContainerPosY_39;
	// System.Single CJiShaInfo::m_fNoNameModeKillerAvatarPosX
	float ___m_fNoNameModeKillerAvatarPosX_40;
	// System.Single CJiShaInfo::m_fNoNameModeDeadMeatAvatarPosX
	float ___m_fNoNameModeDeadMeatAvatarPosX_41;
	// System.Single CJiShaInfo::m_fNoNameBgBarScale
	float ___m_fNoNameBgBarScale_42;
	// System.Single CJiShaInfo::m_fAssistContainerPosX_NoName
	float ___m_fAssistContainerPosX_NoName_43;
	// System.Single CJiShaInfo::m_fHaveNameBgWidth
	float ___m_fHaveNameBgWidth_44;
	// System.Single CJiShaInfo::m_fAssistContainerPosX_HaveName
	float ___m_fAssistContainerPosX_HaveName_45;
	// System.Boolean CJiShaInfo::m_bFirstBlood
	bool ___m_bFirstBlood_46;
	// System.String[] CJiShaInfo::m_aryDuoShaSystemInfo
	StringU5BU5D_t1281789340* ___m_aryDuoShaSystemInfo_47;
	// System.String[] CJiShaInfo::m_aryContinuousKillSystemInfo
	StringU5BU5D_t1281789340* ___m_aryContinuousKillSystemInfo_48;
	// System.String CJiShaInfo::m_szFirstBloodSystemInfo
	String_t* ___m_szFirstBloodSystemInfo_49;
	// System.String CJiShaInfo::m_szJiShaSystemInfo
	String_t* ___m_szJiShaSystemInfo_50;
	// System.Collections.Generic.List`1<CAvatar> CJiShaInfo::m_lstAssistAvatar
	List_1_t2724846135 * ___m_lstAssistAvatar_51;
	// System.Collections.Generic.List`1<CJiShaInfo/sJiShaSystemMsgParam> CJiShaInfo::m_lstJiShaSysMsgQueue
	List_1_t3806197044 * ___m_lstJiShaSysMsgQueue_54;
	// System.Boolean CJiShaInfo::m_bUIInited
	bool ___m_bUIInited_55;
	// System.Collections.Generic.List`1<System.Int32> CJiShaInfo::lstAssistId
	List_1_t128053199 * ___lstAssistId_56;
	// System.Boolean CJiShaInfo::m_bFading
	bool ___m_bFading_57;

public:
	inline static int32_t get_offset_of_m_fShowTime_5() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fShowTime_5)); }
	inline float get_m_fShowTime_5() const { return ___m_fShowTime_5; }
	inline float* get_address_of_m_fShowTime_5() { return &___m_fShowTime_5; }
	inline void set_m_fShowTime_5(float value)
	{
		___m_fShowTime_5 = value;
	}

	inline static int32_t get_offset_of_m_fShowTimeCount_6() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fShowTimeCount_6)); }
	inline float get_m_fShowTimeCount_6() const { return ___m_fShowTimeCount_6; }
	inline float* get_address_of_m_fShowTimeCount_6() { return &___m_fShowTimeCount_6; }
	inline void set_m_fShowTimeCount_6(float value)
	{
		___m_fShowTimeCount_6 = value;
	}

	inline static int32_t get_offset_of__panelJiSha_7() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____panelJiSha_7)); }
	inline GameObject_t1113636619 * get__panelJiSha_7() const { return ____panelJiSha_7; }
	inline GameObject_t1113636619 ** get_address_of__panelJiSha_7() { return &____panelJiSha_7; }
	inline void set__panelJiSha_7(GameObject_t1113636619 * value)
	{
		____panelJiSha_7 = value;
		Il2CppCodeGenWriteBarrier((&____panelJiSha_7), value);
	}

	inline static int32_t get_offset_of__txtKillerName_8() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtKillerName_8)); }
	inline Text_t1901882714 * get__txtKillerName_8() const { return ____txtKillerName_8; }
	inline Text_t1901882714 ** get_address_of__txtKillerName_8() { return &____txtKillerName_8; }
	inline void set__txtKillerName_8(Text_t1901882714 * value)
	{
		____txtKillerName_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtKillerName_8), value);
	}

	inline static int32_t get_offset_of__txtDeadMeatName_9() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtDeadMeatName_9)); }
	inline Text_t1901882714 * get__txtDeadMeatName_9() const { return ____txtDeadMeatName_9; }
	inline Text_t1901882714 ** get_address_of__txtDeadMeatName_9() { return &____txtDeadMeatName_9; }
	inline void set__txtDeadMeatName_9(Text_t1901882714 * value)
	{
		____txtDeadMeatName_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtDeadMeatName_9), value);
	}

	inline static int32_t get_offset_of__imgKillerAvatar_10() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgKillerAvatar_10)); }
	inline Image_t2670269651 * get__imgKillerAvatar_10() const { return ____imgKillerAvatar_10; }
	inline Image_t2670269651 ** get_address_of__imgKillerAvatar_10() { return &____imgKillerAvatar_10; }
	inline void set__imgKillerAvatar_10(Image_t2670269651 * value)
	{
		____imgKillerAvatar_10 = value;
		Il2CppCodeGenWriteBarrier((&____imgKillerAvatar_10), value);
	}

	inline static int32_t get_offset_of__imgDeadMeatAvatar_11() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgDeadMeatAvatar_11)); }
	inline Image_t2670269651 * get__imgDeadMeatAvatar_11() const { return ____imgDeadMeatAvatar_11; }
	inline Image_t2670269651 ** get_address_of__imgDeadMeatAvatar_11() { return &____imgDeadMeatAvatar_11; }
	inline void set__imgDeadMeatAvatar_11(Image_t2670269651 * value)
	{
		____imgDeadMeatAvatar_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgDeadMeatAvatar_11), value);
	}

	inline static int32_t get_offset_of__panelJiSha_PC_12() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____panelJiSha_PC_12)); }
	inline GameObject_t1113636619 * get__panelJiSha_PC_12() const { return ____panelJiSha_PC_12; }
	inline GameObject_t1113636619 ** get_address_of__panelJiSha_PC_12() { return &____panelJiSha_PC_12; }
	inline void set__panelJiSha_PC_12(GameObject_t1113636619 * value)
	{
		____panelJiSha_PC_12 = value;
		Il2CppCodeGenWriteBarrier((&____panelJiSha_PC_12), value);
	}

	inline static int32_t get_offset_of__txtKillerName_PC_13() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtKillerName_PC_13)); }
	inline Text_t1901882714 * get__txtKillerName_PC_13() const { return ____txtKillerName_PC_13; }
	inline Text_t1901882714 ** get_address_of__txtKillerName_PC_13() { return &____txtKillerName_PC_13; }
	inline void set__txtKillerName_PC_13(Text_t1901882714 * value)
	{
		____txtKillerName_PC_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtKillerName_PC_13), value);
	}

	inline static int32_t get_offset_of__txtDeadMeatName_PC_14() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtDeadMeatName_PC_14)); }
	inline Text_t1901882714 * get__txtDeadMeatName_PC_14() const { return ____txtDeadMeatName_PC_14; }
	inline Text_t1901882714 ** get_address_of__txtDeadMeatName_PC_14() { return &____txtDeadMeatName_PC_14; }
	inline void set__txtDeadMeatName_PC_14(Text_t1901882714 * value)
	{
		____txtDeadMeatName_PC_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtDeadMeatName_PC_14), value);
	}

	inline static int32_t get_offset_of__imgKillerAvatar_PC_15() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgKillerAvatar_PC_15)); }
	inline Image_t2670269651 * get__imgKillerAvatar_PC_15() const { return ____imgKillerAvatar_PC_15; }
	inline Image_t2670269651 ** get_address_of__imgKillerAvatar_PC_15() { return &____imgKillerAvatar_PC_15; }
	inline void set__imgKillerAvatar_PC_15(Image_t2670269651 * value)
	{
		____imgKillerAvatar_PC_15 = value;
		Il2CppCodeGenWriteBarrier((&____imgKillerAvatar_PC_15), value);
	}

	inline static int32_t get_offset_of__imgDeadMeatAvatar_PC_16() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgDeadMeatAvatar_PC_16)); }
	inline Image_t2670269651 * get__imgDeadMeatAvatar_PC_16() const { return ____imgDeadMeatAvatar_PC_16; }
	inline Image_t2670269651 ** get_address_of__imgDeadMeatAvatar_PC_16() { return &____imgDeadMeatAvatar_PC_16; }
	inline void set__imgDeadMeatAvatar_PC_16(Image_t2670269651 * value)
	{
		____imgDeadMeatAvatar_PC_16 = value;
		Il2CppCodeGenWriteBarrier((&____imgDeadMeatAvatar_PC_16), value);
	}

	inline static int32_t get_offset_of__panelJiSha_MOBILE_17() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____panelJiSha_MOBILE_17)); }
	inline GameObject_t1113636619 * get__panelJiSha_MOBILE_17() const { return ____panelJiSha_MOBILE_17; }
	inline GameObject_t1113636619 ** get_address_of__panelJiSha_MOBILE_17() { return &____panelJiSha_MOBILE_17; }
	inline void set__panelJiSha_MOBILE_17(GameObject_t1113636619 * value)
	{
		____panelJiSha_MOBILE_17 = value;
		Il2CppCodeGenWriteBarrier((&____panelJiSha_MOBILE_17), value);
	}

	inline static int32_t get_offset_of__txtKillerName_MOBILE_18() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtKillerName_MOBILE_18)); }
	inline Text_t1901882714 * get__txtKillerName_MOBILE_18() const { return ____txtKillerName_MOBILE_18; }
	inline Text_t1901882714 ** get_address_of__txtKillerName_MOBILE_18() { return &____txtKillerName_MOBILE_18; }
	inline void set__txtKillerName_MOBILE_18(Text_t1901882714 * value)
	{
		____txtKillerName_MOBILE_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtKillerName_MOBILE_18), value);
	}

	inline static int32_t get_offset_of__txtDeadMeatName_MOBILE_19() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtDeadMeatName_MOBILE_19)); }
	inline Text_t1901882714 * get__txtDeadMeatName_MOBILE_19() const { return ____txtDeadMeatName_MOBILE_19; }
	inline Text_t1901882714 ** get_address_of__txtDeadMeatName_MOBILE_19() { return &____txtDeadMeatName_MOBILE_19; }
	inline void set__txtDeadMeatName_MOBILE_19(Text_t1901882714 * value)
	{
		____txtDeadMeatName_MOBILE_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtDeadMeatName_MOBILE_19), value);
	}

	inline static int32_t get_offset_of__imgKillerAvatar_MOBILE_20() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgKillerAvatar_MOBILE_20)); }
	inline Image_t2670269651 * get__imgKillerAvatar_MOBILE_20() const { return ____imgKillerAvatar_MOBILE_20; }
	inline Image_t2670269651 ** get_address_of__imgKillerAvatar_MOBILE_20() { return &____imgKillerAvatar_MOBILE_20; }
	inline void set__imgKillerAvatar_MOBILE_20(Image_t2670269651 * value)
	{
		____imgKillerAvatar_MOBILE_20 = value;
		Il2CppCodeGenWriteBarrier((&____imgKillerAvatar_MOBILE_20), value);
	}

	inline static int32_t get_offset_of__imgDeadMeatAvatar_MOBILE_21() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgDeadMeatAvatar_MOBILE_21)); }
	inline Image_t2670269651 * get__imgDeadMeatAvatar_MOBILE_21() const { return ____imgDeadMeatAvatar_MOBILE_21; }
	inline Image_t2670269651 ** get_address_of__imgDeadMeatAvatar_MOBILE_21() { return &____imgDeadMeatAvatar_MOBILE_21; }
	inline void set__imgDeadMeatAvatar_MOBILE_21(Image_t2670269651 * value)
	{
		____imgDeadMeatAvatar_MOBILE_21 = value;
		Il2CppCodeGenWriteBarrier((&____imgDeadMeatAvatar_MOBILE_21), value);
	}

	inline static int32_t get_offset_of__imgBgBar_22() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____imgBgBar_22)); }
	inline Image_t2670269651 * get__imgBgBar_22() const { return ____imgBgBar_22; }
	inline Image_t2670269651 ** get_address_of__imgBgBar_22() { return &____imgBgBar_22; }
	inline void set__imgBgBar_22(Image_t2670269651 * value)
	{
		____imgBgBar_22 = value;
		Il2CppCodeGenWriteBarrier((&____imgBgBar_22), value);
	}

	inline static int32_t get_offset_of__txtAssistTitle_23() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtAssistTitle_23)); }
	inline Text_t1901882714 * get__txtAssistTitle_23() const { return ____txtAssistTitle_23; }
	inline Text_t1901882714 ** get_address_of__txtAssistTitle_23() { return &____txtAssistTitle_23; }
	inline void set__txtAssistTitle_23(Text_t1901882714 * value)
	{
		____txtAssistTitle_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtAssistTitle_23), value);
	}

	inline static int32_t get_offset_of__goAssistAvatarContainer_24() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____goAssistAvatarContainer_24)); }
	inline GameObject_t1113636619 * get__goAssistAvatarContainer_24() const { return ____goAssistAvatarContainer_24; }
	inline GameObject_t1113636619 ** get_address_of__goAssistAvatarContainer_24() { return &____goAssistAvatarContainer_24; }
	inline void set__goAssistAvatarContainer_24(GameObject_t1113636619 * value)
	{
		____goAssistAvatarContainer_24 = value;
		Il2CppCodeGenWriteBarrier((&____goAssistAvatarContainer_24), value);
	}

	inline static int32_t get_offset_of__preAvatar_25() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____preAvatar_25)); }
	inline GameObject_t1113636619 * get__preAvatar_25() const { return ____preAvatar_25; }
	inline GameObject_t1113636619 ** get_address_of__preAvatar_25() { return &____preAvatar_25; }
	inline void set__preAvatar_25(GameObject_t1113636619 * value)
	{
		____preAvatar_25 = value;
		Il2CppCodeGenWriteBarrier((&____preAvatar_25), value);
	}

	inline static int32_t get_offset_of__txtJiSha_26() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____txtJiSha_26)); }
	inline Text_t1901882714 * get__txtJiSha_26() const { return ____txtJiSha_26; }
	inline Text_t1901882714 ** get_address_of__txtJiSha_26() { return &____txtJiSha_26; }
	inline void set__txtJiSha_26(Text_t1901882714 * value)
	{
		____txtJiSha_26 = value;
		Il2CppCodeGenWriteBarrier((&____txtJiSha_26), value);
	}

	inline static int32_t get_offset_of__CanvasGroup_27() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____CanvasGroup_27)); }
	inline CanvasGroup_t4083511760 * get__CanvasGroup_27() const { return ____CanvasGroup_27; }
	inline CanvasGroup_t4083511760 ** get_address_of__CanvasGroup_27() { return &____CanvasGroup_27; }
	inline void set__CanvasGroup_27(CanvasGroup_t4083511760 * value)
	{
		____CanvasGroup_27 = value;
		Il2CppCodeGenWriteBarrier((&____CanvasGroup_27), value);
	}

	inline static int32_t get_offset_of__containerKillerAvatar_28() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____containerKillerAvatar_28)); }
	inline GameObject_t1113636619 * get__containerKillerAvatar_28() const { return ____containerKillerAvatar_28; }
	inline GameObject_t1113636619 ** get_address_of__containerKillerAvatar_28() { return &____containerKillerAvatar_28; }
	inline void set__containerKillerAvatar_28(GameObject_t1113636619 * value)
	{
		____containerKillerAvatar_28 = value;
		Il2CppCodeGenWriteBarrier((&____containerKillerAvatar_28), value);
	}

	inline static int32_t get_offset_of__containerDeadmeatAvatar_29() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____containerDeadmeatAvatar_29)); }
	inline GameObject_t1113636619 * get__containerDeadmeatAvatar_29() const { return ____containerDeadmeatAvatar_29; }
	inline GameObject_t1113636619 ** get_address_of__containerDeadmeatAvatar_29() { return &____containerDeadmeatAvatar_29; }
	inline void set__containerDeadmeatAvatar_29(GameObject_t1113636619 * value)
	{
		____containerDeadmeatAvatar_29 = value;
		Il2CppCodeGenWriteBarrier((&____containerDeadmeatAvatar_29), value);
	}

	inline static int32_t get_offset_of__toggleNoName_30() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ____toggleNoName_30)); }
	inline Toggle_t2735377061 * get__toggleNoName_30() const { return ____toggleNoName_30; }
	inline Toggle_t2735377061 ** get_address_of__toggleNoName_30() { return &____toggleNoName_30; }
	inline void set__toggleNoName_30(Toggle_t2735377061 * value)
	{
		____toggleNoName_30 = value;
		Il2CppCodeGenWriteBarrier((&____toggleNoName_30), value);
	}

	inline static int32_t get_offset_of_m_bShow_31() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_bShow_31)); }
	inline bool get_m_bShow_31() const { return ___m_bShow_31; }
	inline bool* get_address_of_m_bShow_31() { return &___m_bShow_31; }
	inline void set_m_bShow_31(bool value)
	{
		___m_bShow_31 = value;
	}

	inline static int32_t get_offset_of_m_fAssistAvatarGap_32() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fAssistAvatarGap_32)); }
	inline float get_m_fAssistAvatarGap_32() const { return ___m_fAssistAvatarGap_32; }
	inline float* get_address_of_m_fAssistAvatarGap_32() { return &___m_fAssistAvatarGap_32; }
	inline void set_m_fAssistAvatarGap_32(float value)
	{
		___m_fAssistAvatarGap_32 = value;
	}

	inline static int32_t get_offset_of_m_fAssistAvatarScale_33() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fAssistAvatarScale_33)); }
	inline float get_m_fAssistAvatarScale_33() const { return ___m_fAssistAvatarScale_33; }
	inline float* get_address_of_m_fAssistAvatarScale_33() { return &___m_fAssistAvatarScale_33; }
	inline void set_m_fAssistAvatarScale_33(float value)
	{
		___m_fAssistAvatarScale_33 = value;
	}

	inline static int32_t get_offset_of_m_fDuoShaTime_34() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fDuoShaTime_34)); }
	inline float get_m_fDuoShaTime_34() const { return ___m_fDuoShaTime_34; }
	inline float* get_address_of_m_fDuoShaTime_34() { return &___m_fDuoShaTime_34; }
	inline void set_m_fDuoShaTime_34(float value)
	{
		___m_fDuoShaTime_34 = value;
	}

	inline static int32_t get_offset_of_m_fAssistTime_35() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fAssistTime_35)); }
	inline float get_m_fAssistTime_35() const { return ___m_fAssistTime_35; }
	inline float* get_address_of_m_fAssistTime_35() { return &___m_fAssistTime_35; }
	inline void set_m_fAssistTime_35(float value)
	{
		___m_fAssistTime_35 = value;
	}

	inline static int32_t get_offset_of_m_nContinuousKillStartThreshold_36() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_nContinuousKillStartThreshold_36)); }
	inline float get_m_nContinuousKillStartThreshold_36() const { return ___m_nContinuousKillStartThreshold_36; }
	inline float* get_address_of_m_nContinuousKillStartThreshold_36() { return &___m_nContinuousKillStartThreshold_36; }
	inline void set_m_nContinuousKillStartThreshold_36(float value)
	{
		___m_nContinuousKillStartThreshold_36 = value;
	}

	inline static int32_t get_offset_of_m_fFadeTime_37() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fFadeTime_37)); }
	inline float get_m_fFadeTime_37() const { return ___m_fFadeTime_37; }
	inline float* get_address_of_m_fFadeTime_37() { return &___m_fFadeTime_37; }
	inline void set_m_fFadeTime_37(float value)
	{
		___m_fFadeTime_37 = value;
	}

	inline static int32_t get_offset_of_m_bShowName_38() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_bShowName_38)); }
	inline bool get_m_bShowName_38() const { return ___m_bShowName_38; }
	inline bool* get_address_of_m_bShowName_38() { return &___m_bShowName_38; }
	inline void set_m_bShowName_38(bool value)
	{
		___m_bShowName_38 = value;
	}

	inline static int32_t get_offset_of_m_fAssistContainerPosY_39() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fAssistContainerPosY_39)); }
	inline float get_m_fAssistContainerPosY_39() const { return ___m_fAssistContainerPosY_39; }
	inline float* get_address_of_m_fAssistContainerPosY_39() { return &___m_fAssistContainerPosY_39; }
	inline void set_m_fAssistContainerPosY_39(float value)
	{
		___m_fAssistContainerPosY_39 = value;
	}

	inline static int32_t get_offset_of_m_fNoNameModeKillerAvatarPosX_40() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fNoNameModeKillerAvatarPosX_40)); }
	inline float get_m_fNoNameModeKillerAvatarPosX_40() const { return ___m_fNoNameModeKillerAvatarPosX_40; }
	inline float* get_address_of_m_fNoNameModeKillerAvatarPosX_40() { return &___m_fNoNameModeKillerAvatarPosX_40; }
	inline void set_m_fNoNameModeKillerAvatarPosX_40(float value)
	{
		___m_fNoNameModeKillerAvatarPosX_40 = value;
	}

	inline static int32_t get_offset_of_m_fNoNameModeDeadMeatAvatarPosX_41() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fNoNameModeDeadMeatAvatarPosX_41)); }
	inline float get_m_fNoNameModeDeadMeatAvatarPosX_41() const { return ___m_fNoNameModeDeadMeatAvatarPosX_41; }
	inline float* get_address_of_m_fNoNameModeDeadMeatAvatarPosX_41() { return &___m_fNoNameModeDeadMeatAvatarPosX_41; }
	inline void set_m_fNoNameModeDeadMeatAvatarPosX_41(float value)
	{
		___m_fNoNameModeDeadMeatAvatarPosX_41 = value;
	}

	inline static int32_t get_offset_of_m_fNoNameBgBarScale_42() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fNoNameBgBarScale_42)); }
	inline float get_m_fNoNameBgBarScale_42() const { return ___m_fNoNameBgBarScale_42; }
	inline float* get_address_of_m_fNoNameBgBarScale_42() { return &___m_fNoNameBgBarScale_42; }
	inline void set_m_fNoNameBgBarScale_42(float value)
	{
		___m_fNoNameBgBarScale_42 = value;
	}

	inline static int32_t get_offset_of_m_fAssistContainerPosX_NoName_43() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fAssistContainerPosX_NoName_43)); }
	inline float get_m_fAssistContainerPosX_NoName_43() const { return ___m_fAssistContainerPosX_NoName_43; }
	inline float* get_address_of_m_fAssistContainerPosX_NoName_43() { return &___m_fAssistContainerPosX_NoName_43; }
	inline void set_m_fAssistContainerPosX_NoName_43(float value)
	{
		___m_fAssistContainerPosX_NoName_43 = value;
	}

	inline static int32_t get_offset_of_m_fHaveNameBgWidth_44() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fHaveNameBgWidth_44)); }
	inline float get_m_fHaveNameBgWidth_44() const { return ___m_fHaveNameBgWidth_44; }
	inline float* get_address_of_m_fHaveNameBgWidth_44() { return &___m_fHaveNameBgWidth_44; }
	inline void set_m_fHaveNameBgWidth_44(float value)
	{
		___m_fHaveNameBgWidth_44 = value;
	}

	inline static int32_t get_offset_of_m_fAssistContainerPosX_HaveName_45() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_fAssistContainerPosX_HaveName_45)); }
	inline float get_m_fAssistContainerPosX_HaveName_45() const { return ___m_fAssistContainerPosX_HaveName_45; }
	inline float* get_address_of_m_fAssistContainerPosX_HaveName_45() { return &___m_fAssistContainerPosX_HaveName_45; }
	inline void set_m_fAssistContainerPosX_HaveName_45(float value)
	{
		___m_fAssistContainerPosX_HaveName_45 = value;
	}

	inline static int32_t get_offset_of_m_bFirstBlood_46() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_bFirstBlood_46)); }
	inline bool get_m_bFirstBlood_46() const { return ___m_bFirstBlood_46; }
	inline bool* get_address_of_m_bFirstBlood_46() { return &___m_bFirstBlood_46; }
	inline void set_m_bFirstBlood_46(bool value)
	{
		___m_bFirstBlood_46 = value;
	}

	inline static int32_t get_offset_of_m_aryDuoShaSystemInfo_47() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_aryDuoShaSystemInfo_47)); }
	inline StringU5BU5D_t1281789340* get_m_aryDuoShaSystemInfo_47() const { return ___m_aryDuoShaSystemInfo_47; }
	inline StringU5BU5D_t1281789340** get_address_of_m_aryDuoShaSystemInfo_47() { return &___m_aryDuoShaSystemInfo_47; }
	inline void set_m_aryDuoShaSystemInfo_47(StringU5BU5D_t1281789340* value)
	{
		___m_aryDuoShaSystemInfo_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDuoShaSystemInfo_47), value);
	}

	inline static int32_t get_offset_of_m_aryContinuousKillSystemInfo_48() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_aryContinuousKillSystemInfo_48)); }
	inline StringU5BU5D_t1281789340* get_m_aryContinuousKillSystemInfo_48() const { return ___m_aryContinuousKillSystemInfo_48; }
	inline StringU5BU5D_t1281789340** get_address_of_m_aryContinuousKillSystemInfo_48() { return &___m_aryContinuousKillSystemInfo_48; }
	inline void set_m_aryContinuousKillSystemInfo_48(StringU5BU5D_t1281789340* value)
	{
		___m_aryContinuousKillSystemInfo_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryContinuousKillSystemInfo_48), value);
	}

	inline static int32_t get_offset_of_m_szFirstBloodSystemInfo_49() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_szFirstBloodSystemInfo_49)); }
	inline String_t* get_m_szFirstBloodSystemInfo_49() const { return ___m_szFirstBloodSystemInfo_49; }
	inline String_t** get_address_of_m_szFirstBloodSystemInfo_49() { return &___m_szFirstBloodSystemInfo_49; }
	inline void set_m_szFirstBloodSystemInfo_49(String_t* value)
	{
		___m_szFirstBloodSystemInfo_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_szFirstBloodSystemInfo_49), value);
	}

	inline static int32_t get_offset_of_m_szJiShaSystemInfo_50() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_szJiShaSystemInfo_50)); }
	inline String_t* get_m_szJiShaSystemInfo_50() const { return ___m_szJiShaSystemInfo_50; }
	inline String_t** get_address_of_m_szJiShaSystemInfo_50() { return &___m_szJiShaSystemInfo_50; }
	inline void set_m_szJiShaSystemInfo_50(String_t* value)
	{
		___m_szJiShaSystemInfo_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_szJiShaSystemInfo_50), value);
	}

	inline static int32_t get_offset_of_m_lstAssistAvatar_51() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_lstAssistAvatar_51)); }
	inline List_1_t2724846135 * get_m_lstAssistAvatar_51() const { return ___m_lstAssistAvatar_51; }
	inline List_1_t2724846135 ** get_address_of_m_lstAssistAvatar_51() { return &___m_lstAssistAvatar_51; }
	inline void set_m_lstAssistAvatar_51(List_1_t2724846135 * value)
	{
		___m_lstAssistAvatar_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAssistAvatar_51), value);
	}

	inline static int32_t get_offset_of_m_lstJiShaSysMsgQueue_54() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_lstJiShaSysMsgQueue_54)); }
	inline List_1_t3806197044 * get_m_lstJiShaSysMsgQueue_54() const { return ___m_lstJiShaSysMsgQueue_54; }
	inline List_1_t3806197044 ** get_address_of_m_lstJiShaSysMsgQueue_54() { return &___m_lstJiShaSysMsgQueue_54; }
	inline void set_m_lstJiShaSysMsgQueue_54(List_1_t3806197044 * value)
	{
		___m_lstJiShaSysMsgQueue_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstJiShaSysMsgQueue_54), value);
	}

	inline static int32_t get_offset_of_m_bUIInited_55() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_bUIInited_55)); }
	inline bool get_m_bUIInited_55() const { return ___m_bUIInited_55; }
	inline bool* get_address_of_m_bUIInited_55() { return &___m_bUIInited_55; }
	inline void set_m_bUIInited_55(bool value)
	{
		___m_bUIInited_55 = value;
	}

	inline static int32_t get_offset_of_lstAssistId_56() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___lstAssistId_56)); }
	inline List_1_t128053199 * get_lstAssistId_56() const { return ___lstAssistId_56; }
	inline List_1_t128053199 ** get_address_of_lstAssistId_56() { return &___lstAssistId_56; }
	inline void set_lstAssistId_56(List_1_t128053199 * value)
	{
		___lstAssistId_56 = value;
		Il2CppCodeGenWriteBarrier((&___lstAssistId_56), value);
	}

	inline static int32_t get_offset_of_m_bFading_57() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697, ___m_bFading_57)); }
	inline bool get_m_bFading_57() const { return ___m_bFading_57; }
	inline bool* get_address_of_m_bFading_57() { return &___m_bFading_57; }
	inline void set_m_bFading_57(bool value)
	{
		___m_bFading_57 = value;
	}
};

struct CJiShaInfo_t3249703697_StaticFields
{
public:
	// CJiShaInfo CJiShaInfo::s_Instance
	CJiShaInfo_t3249703697 * ___s_Instance_2;
	// UnityEngine.Vector3 CJiShaInfo::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CJiShaInfo::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697_StaticFields, ___s_Instance_2)); }
	inline CJiShaInfo_t3249703697 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CJiShaInfo_t3249703697 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CJiShaInfo_t3249703697 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CJiShaInfo_t3249703697_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CJISHAINFO_T3249703697_H
#ifndef CCOSMOSEFFECT_T495978253_H
#define CCOSMOSEFFECT_T495978253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCosmosEffect
struct  CCosmosEffect_t495978253  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup CCosmosEffect::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_4;
	// UnityEngine.UI.Image CCosmosEffect::_imgMain
	Image_t2670269651 * ____imgMain_5;
	// UnityEngine.SpriteRenderer CCosmosEffect::_sprMain
	SpriteRenderer_t3235626157 * ____sprMain_6;
	// System.Boolean CCosmosEffect::m_bUI
	bool ___m_bUI_7;
	// UnityEngine.SpriteRenderer CCosmosEffect::_srRotationPic
	SpriteRenderer_t3235626157 * ____srRotationPic_8;
	// UnityEngine.UI.Image CCosmosEffect::_imgRotationPic
	Image_t2670269651 * ____imgRotationPic_9;
	// System.Boolean CCosmosEffect::m_bRotating
	bool ___m_bRotating_10;
	// System.Single CCosmosEffect::m_fRotatingSpeed
	float ___m_fRotatingSpeed_11;
	// System.Single CCosmosEffect::m_fRotatingAngle
	float ___m_fRotatingAngle_12;
	// System.Boolean CCosmosEffect::m_bAlphaChange
	bool ___m_bAlphaChange_13;
	// System.Single CCosmosEffect::m_fMaxAlpha
	float ___m_fMaxAlpha_14;
	// System.Single CCosmosEffect::m_fMinAlpha
	float ___m_fMinAlpha_15;
	// System.Single CCosmosEffect::m_fAlphaChangeSpeed
	float ___m_fAlphaChangeSpeed_16;

public:
	inline static int32_t get_offset_of__canvasGroup_4() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____canvasGroup_4)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_4() const { return ____canvasGroup_4; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_4() { return &____canvasGroup_4; }
	inline void set__canvasGroup_4(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_4), value);
	}

	inline static int32_t get_offset_of__imgMain_5() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____imgMain_5)); }
	inline Image_t2670269651 * get__imgMain_5() const { return ____imgMain_5; }
	inline Image_t2670269651 ** get_address_of__imgMain_5() { return &____imgMain_5; }
	inline void set__imgMain_5(Image_t2670269651 * value)
	{
		____imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_5), value);
	}

	inline static int32_t get_offset_of__sprMain_6() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____sprMain_6)); }
	inline SpriteRenderer_t3235626157 * get__sprMain_6() const { return ____sprMain_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprMain_6() { return &____sprMain_6; }
	inline void set__sprMain_6(SpriteRenderer_t3235626157 * value)
	{
		____sprMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprMain_6), value);
	}

	inline static int32_t get_offset_of_m_bUI_7() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bUI_7)); }
	inline bool get_m_bUI_7() const { return ___m_bUI_7; }
	inline bool* get_address_of_m_bUI_7() { return &___m_bUI_7; }
	inline void set_m_bUI_7(bool value)
	{
		___m_bUI_7 = value;
	}

	inline static int32_t get_offset_of__srRotationPic_8() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____srRotationPic_8)); }
	inline SpriteRenderer_t3235626157 * get__srRotationPic_8() const { return ____srRotationPic_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srRotationPic_8() { return &____srRotationPic_8; }
	inline void set__srRotationPic_8(SpriteRenderer_t3235626157 * value)
	{
		____srRotationPic_8 = value;
		Il2CppCodeGenWriteBarrier((&____srRotationPic_8), value);
	}

	inline static int32_t get_offset_of__imgRotationPic_9() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ____imgRotationPic_9)); }
	inline Image_t2670269651 * get__imgRotationPic_9() const { return ____imgRotationPic_9; }
	inline Image_t2670269651 ** get_address_of__imgRotationPic_9() { return &____imgRotationPic_9; }
	inline void set__imgRotationPic_9(Image_t2670269651 * value)
	{
		____imgRotationPic_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgRotationPic_9), value);
	}

	inline static int32_t get_offset_of_m_bRotating_10() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bRotating_10)); }
	inline bool get_m_bRotating_10() const { return ___m_bRotating_10; }
	inline bool* get_address_of_m_bRotating_10() { return &___m_bRotating_10; }
	inline void set_m_bRotating_10(bool value)
	{
		___m_bRotating_10 = value;
	}

	inline static int32_t get_offset_of_m_fRotatingSpeed_11() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fRotatingSpeed_11)); }
	inline float get_m_fRotatingSpeed_11() const { return ___m_fRotatingSpeed_11; }
	inline float* get_address_of_m_fRotatingSpeed_11() { return &___m_fRotatingSpeed_11; }
	inline void set_m_fRotatingSpeed_11(float value)
	{
		___m_fRotatingSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_fRotatingAngle_12() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fRotatingAngle_12)); }
	inline float get_m_fRotatingAngle_12() const { return ___m_fRotatingAngle_12; }
	inline float* get_address_of_m_fRotatingAngle_12() { return &___m_fRotatingAngle_12; }
	inline void set_m_fRotatingAngle_12(float value)
	{
		___m_fRotatingAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_bAlphaChange_13() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_bAlphaChange_13)); }
	inline bool get_m_bAlphaChange_13() const { return ___m_bAlphaChange_13; }
	inline bool* get_address_of_m_bAlphaChange_13() { return &___m_bAlphaChange_13; }
	inline void set_m_bAlphaChange_13(bool value)
	{
		___m_bAlphaChange_13 = value;
	}

	inline static int32_t get_offset_of_m_fMaxAlpha_14() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fMaxAlpha_14)); }
	inline float get_m_fMaxAlpha_14() const { return ___m_fMaxAlpha_14; }
	inline float* get_address_of_m_fMaxAlpha_14() { return &___m_fMaxAlpha_14; }
	inline void set_m_fMaxAlpha_14(float value)
	{
		___m_fMaxAlpha_14 = value;
	}

	inline static int32_t get_offset_of_m_fMinAlpha_15() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fMinAlpha_15)); }
	inline float get_m_fMinAlpha_15() const { return ___m_fMinAlpha_15; }
	inline float* get_address_of_m_fMinAlpha_15() { return &___m_fMinAlpha_15; }
	inline void set_m_fMinAlpha_15(float value)
	{
		___m_fMinAlpha_15 = value;
	}

	inline static int32_t get_offset_of_m_fAlphaChangeSpeed_16() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253, ___m_fAlphaChangeSpeed_16)); }
	inline float get_m_fAlphaChangeSpeed_16() const { return ___m_fAlphaChangeSpeed_16; }
	inline float* get_address_of_m_fAlphaChangeSpeed_16() { return &___m_fAlphaChangeSpeed_16; }
	inline void set_m_fAlphaChangeSpeed_16(float value)
	{
		___m_fAlphaChangeSpeed_16 = value;
	}
};

struct CCosmosEffect_t495978253_StaticFields
{
public:
	// UnityEngine.Vector3 CCosmosEffect::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CCosmosEffect::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CCosmosEffect_t495978253_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCOSMOSEFFECT_T495978253_H
#ifndef CCONFIGMANAGER_T1106484817_H
#define CCONFIGMANAGER_T1106484817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CConfigManager
struct  CConfigManager_t1106484817  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CConfigManager_t1106484817_StaticFields
{
public:
	// System.Boolean[] CConfigManager::s_aryConfigLoaded
	BooleanU5BU5D_t2897418192* ___s_aryConfigLoaded_2;

public:
	inline static int32_t get_offset_of_s_aryConfigLoaded_2() { return static_cast<int32_t>(offsetof(CConfigManager_t1106484817_StaticFields, ___s_aryConfigLoaded_2)); }
	inline BooleanU5BU5D_t2897418192* get_s_aryConfigLoaded_2() const { return ___s_aryConfigLoaded_2; }
	inline BooleanU5BU5D_t2897418192** get_address_of_s_aryConfigLoaded_2() { return &___s_aryConfigLoaded_2; }
	inline void set_s_aryConfigLoaded_2(BooleanU5BU5D_t2897418192* value)
	{
		___s_aryConfigLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_aryConfigLoaded_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCONFIGMANAGER_T1106484817_H
#ifndef CGAMEMODESELECTCOUNTER_T2640523211_H
#define CGAMEMODESELECTCOUNTER_T2640523211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGameModeSelectCounter
struct  CGameModeSelectCounter_t2640523211  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CGameModeSelectCounter::_goSelected
	GameObject_t1113636619 * ____goSelected_2;

public:
	inline static int32_t get_offset_of__goSelected_2() { return static_cast<int32_t>(offsetof(CGameModeSelectCounter_t2640523211, ____goSelected_2)); }
	inline GameObject_t1113636619 * get__goSelected_2() const { return ____goSelected_2; }
	inline GameObject_t1113636619 ** get_address_of__goSelected_2() { return &____goSelected_2; }
	inline void set__goSelected_2(GameObject_t1113636619 * value)
	{
		____goSelected_2 = value;
		Il2CppCodeGenWriteBarrier((&____goSelected_2), value);
	}
};

struct CGameModeSelectCounter_t2640523211_StaticFields
{
public:
	// UnityEngine.Vector3 CGameModeSelectCounter::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CGameModeSelectCounter_t2640523211_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGAMEMODESELECTCOUNTER_T2640523211_H
#ifndef CLIGHT_T1535733186_H
#define CLIGHT_T1535733186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLight
struct  CLight_t1535733186  : public MonoBehaviour_t3962482529
{
public:
	// CLightSystem/eLightType CLight::m_eType
	int32_t ___m_eType_2;

public:
	inline static int32_t get_offset_of_m_eType_2() { return static_cast<int32_t>(offsetof(CLight_t1535733186, ___m_eType_2)); }
	inline int32_t get_m_eType_2() const { return ___m_eType_2; }
	inline int32_t* get_address_of_m_eType_2() { return &___m_eType_2; }
	inline void set_m_eType_2(int32_t value)
	{
		___m_eType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIGHT_T1535733186_H
#ifndef CROOM_T2285119849_H
#define CROOM_T2285119849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRoom
struct  CRoom_t2285119849  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CRoom::_txtRoomName
	Text_t1901882714 * ____txtRoomName_3;
	// UnityEngine.UI.Image CRoom::_imgAvatar
	Image_t2670269651 * ____imgAvatar_4;
	// UnityEngine.UI.Text CRoom::_txtCurPlayerCount
	Text_t1901882714 * ____txtCurPlayerCount_5;
	// UnityEngine.UI.Button CRoom::_btnEnter
	Button_t4055032469 * ____btnEnter_6;
	// UnityEngine.UI.Image CRoom::_imgBg
	Image_t2670269651 * ____imgBg_7;
	// System.Int32 CRoom::m_nIndex
	int32_t ___m_nIndex_8;
	// System.Boolean CRoom::m_bSelected
	bool ___m_bSelected_9;

public:
	inline static int32_t get_offset_of__txtRoomName_3() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ____txtRoomName_3)); }
	inline Text_t1901882714 * get__txtRoomName_3() const { return ____txtRoomName_3; }
	inline Text_t1901882714 ** get_address_of__txtRoomName_3() { return &____txtRoomName_3; }
	inline void set__txtRoomName_3(Text_t1901882714 * value)
	{
		____txtRoomName_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtRoomName_3), value);
	}

	inline static int32_t get_offset_of__imgAvatar_4() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ____imgAvatar_4)); }
	inline Image_t2670269651 * get__imgAvatar_4() const { return ____imgAvatar_4; }
	inline Image_t2670269651 ** get_address_of__imgAvatar_4() { return &____imgAvatar_4; }
	inline void set__imgAvatar_4(Image_t2670269651 * value)
	{
		____imgAvatar_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_4), value);
	}

	inline static int32_t get_offset_of__txtCurPlayerCount_5() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ____txtCurPlayerCount_5)); }
	inline Text_t1901882714 * get__txtCurPlayerCount_5() const { return ____txtCurPlayerCount_5; }
	inline Text_t1901882714 ** get_address_of__txtCurPlayerCount_5() { return &____txtCurPlayerCount_5; }
	inline void set__txtCurPlayerCount_5(Text_t1901882714 * value)
	{
		____txtCurPlayerCount_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPlayerCount_5), value);
	}

	inline static int32_t get_offset_of__btnEnter_6() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ____btnEnter_6)); }
	inline Button_t4055032469 * get__btnEnter_6() const { return ____btnEnter_6; }
	inline Button_t4055032469 ** get_address_of__btnEnter_6() { return &____btnEnter_6; }
	inline void set__btnEnter_6(Button_t4055032469 * value)
	{
		____btnEnter_6 = value;
		Il2CppCodeGenWriteBarrier((&____btnEnter_6), value);
	}

	inline static int32_t get_offset_of__imgBg_7() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ____imgBg_7)); }
	inline Image_t2670269651 * get__imgBg_7() const { return ____imgBg_7; }
	inline Image_t2670269651 ** get_address_of__imgBg_7() { return &____imgBg_7; }
	inline void set__imgBg_7(Image_t2670269651 * value)
	{
		____imgBg_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgBg_7), value);
	}

	inline static int32_t get_offset_of_m_nIndex_8() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ___m_nIndex_8)); }
	inline int32_t get_m_nIndex_8() const { return ___m_nIndex_8; }
	inline int32_t* get_address_of_m_nIndex_8() { return &___m_nIndex_8; }
	inline void set_m_nIndex_8(int32_t value)
	{
		___m_nIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_bSelected_9() { return static_cast<int32_t>(offsetof(CRoom_t2285119849, ___m_bSelected_9)); }
	inline bool get_m_bSelected_9() const { return ___m_bSelected_9; }
	inline bool* get_address_of_m_bSelected_9() { return &___m_bSelected_9; }
	inline void set_m_bSelected_9(bool value)
	{
		___m_bSelected_9 = value;
	}
};

struct CRoom_t2285119849_StaticFields
{
public:
	// UnityEngine.Vector3 CRoom::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CRoom_t2285119849_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROOM_T2285119849_H
#ifndef CCYBERTREELISTITEM_T2642449450_H
#define CCYBERTREELISTITEM_T2642449450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CCyberTreeListItem
struct  CCyberTreeListItem_t2642449450  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CCyberTreeListItem::m_nIndex
	int32_t ___m_nIndex_2;

public:
	inline static int32_t get_offset_of_m_nIndex_2() { return static_cast<int32_t>(offsetof(CCyberTreeListItem_t2642449450, ___m_nIndex_2)); }
	inline int32_t get_m_nIndex_2() const { return ___m_nIndex_2; }
	inline int32_t* get_address_of_m_nIndex_2() { return &___m_nIndex_2; }
	inline void set_m_nIndex_2(int32_t value)
	{
		___m_nIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCYBERTREELISTITEM_T2642449450_H
#ifndef CMSGSYSTEM_T1578259481_H
#define CMSGSYSTEM_T1578259481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMsgSystem
struct  CMsgSystem_t1578259481  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text[] CMsgSystem::_aryMsg
	TextU5BU5D_t422084607* ____aryMsg_3;
	// System.String[] CMsgSystem::m_aryMsgContent
	StringU5BU5D_t1281789340* ___m_aryMsgContent_4;

public:
	inline static int32_t get_offset_of__aryMsg_3() { return static_cast<int32_t>(offsetof(CMsgSystem_t1578259481, ____aryMsg_3)); }
	inline TextU5BU5D_t422084607* get__aryMsg_3() const { return ____aryMsg_3; }
	inline TextU5BU5D_t422084607** get_address_of__aryMsg_3() { return &____aryMsg_3; }
	inline void set__aryMsg_3(TextU5BU5D_t422084607* value)
	{
		____aryMsg_3 = value;
		Il2CppCodeGenWriteBarrier((&____aryMsg_3), value);
	}

	inline static int32_t get_offset_of_m_aryMsgContent_4() { return static_cast<int32_t>(offsetof(CMsgSystem_t1578259481, ___m_aryMsgContent_4)); }
	inline StringU5BU5D_t1281789340* get_m_aryMsgContent_4() const { return ___m_aryMsgContent_4; }
	inline StringU5BU5D_t1281789340** get_address_of_m_aryMsgContent_4() { return &___m_aryMsgContent_4; }
	inline void set_m_aryMsgContent_4(StringU5BU5D_t1281789340* value)
	{
		___m_aryMsgContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMsgContent_4), value);
	}
};

struct CMsgSystem_t1578259481_StaticFields
{
public:
	// CMsgSystem CMsgSystem::s_Instance
	CMsgSystem_t1578259481 * ___s_Instance_2;
	// System.String[] CMsgSystem::m_sMsgContent
	StringU5BU5D_t1281789340* ___m_sMsgContent_5;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CMsgSystem_t1578259481_StaticFields, ___s_Instance_2)); }
	inline CMsgSystem_t1578259481 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CMsgSystem_t1578259481 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CMsgSystem_t1578259481 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_m_sMsgContent_5() { return static_cast<int32_t>(offsetof(CMsgSystem_t1578259481_StaticFields, ___m_sMsgContent_5)); }
	inline StringU5BU5D_t1281789340* get_m_sMsgContent_5() const { return ___m_sMsgContent_5; }
	inline StringU5BU5D_t1281789340** get_address_of_m_sMsgContent_5() { return &___m_sMsgContent_5; }
	inline void set_m_sMsgContent_5(StringU5BU5D_t1281789340* value)
	{
		___m_sMsgContent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sMsgContent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSGSYSTEM_T1578259481_H
#ifndef CREGISTER_T4189215833_H
#define CREGISTER_T4189215833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRegister
struct  CRegister_t4189215833  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField CRegister::_inputPhoneNum
	InputField_t3762917431 * ____inputPhoneNum_8;
	// UnityEngine.UI.InputField CRegister::_inputVeriCode
	InputField_t3762917431 * ____inputVeriCode_9;
	// UnityEngine.UI.InputField CRegister::_inputPassword
	InputField_t3762917431 * ____inputPassword_10;
	// UnityEngine.UI.InputField CRegister::_inputPhoneNumWhenLogin
	InputField_t3762917431 * ____inputPhoneNumWhenLogin_11;
	// UnityEngine.UI.InputField CRegister::_inputPasswordWhenLogin
	InputField_t3762917431 * ____inputPasswordWhenLogin_12;
	// UnityEngine.UI.Toggle CRegister::_toggleAutoLogin
	Toggle_t2735377061 * ____toggleAutoLogin_13;
	// UnityEngine.UI.InputField CRegister::_inputRoleName
	InputField_t3762917431 * ____inputRoleName_14;
	// System.Collections.Generic.Dictionary`2<System.String,CRegister/sAccountInfo> CRegister::m_dicAccountInfo
	Dictionary_2_t537133542 * ___m_dicAccountInfo_15;
	// CRegister/sAccountInfo CRegister::m_CurAccoutInfo
	sAccountInfo_t751877243  ___m_CurAccoutInfo_16;
	// System.String CRegister::m_szCurLoginPhoneNum
	String_t* ___m_szCurLoginPhoneNum_17;

public:
	inline static int32_t get_offset_of__inputPhoneNum_8() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____inputPhoneNum_8)); }
	inline InputField_t3762917431 * get__inputPhoneNum_8() const { return ____inputPhoneNum_8; }
	inline InputField_t3762917431 ** get_address_of__inputPhoneNum_8() { return &____inputPhoneNum_8; }
	inline void set__inputPhoneNum_8(InputField_t3762917431 * value)
	{
		____inputPhoneNum_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputPhoneNum_8), value);
	}

	inline static int32_t get_offset_of__inputVeriCode_9() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____inputVeriCode_9)); }
	inline InputField_t3762917431 * get__inputVeriCode_9() const { return ____inputVeriCode_9; }
	inline InputField_t3762917431 ** get_address_of__inputVeriCode_9() { return &____inputVeriCode_9; }
	inline void set__inputVeriCode_9(InputField_t3762917431 * value)
	{
		____inputVeriCode_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputVeriCode_9), value);
	}

	inline static int32_t get_offset_of__inputPassword_10() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____inputPassword_10)); }
	inline InputField_t3762917431 * get__inputPassword_10() const { return ____inputPassword_10; }
	inline InputField_t3762917431 ** get_address_of__inputPassword_10() { return &____inputPassword_10; }
	inline void set__inputPassword_10(InputField_t3762917431 * value)
	{
		____inputPassword_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputPassword_10), value);
	}

	inline static int32_t get_offset_of__inputPhoneNumWhenLogin_11() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____inputPhoneNumWhenLogin_11)); }
	inline InputField_t3762917431 * get__inputPhoneNumWhenLogin_11() const { return ____inputPhoneNumWhenLogin_11; }
	inline InputField_t3762917431 ** get_address_of__inputPhoneNumWhenLogin_11() { return &____inputPhoneNumWhenLogin_11; }
	inline void set__inputPhoneNumWhenLogin_11(InputField_t3762917431 * value)
	{
		____inputPhoneNumWhenLogin_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputPhoneNumWhenLogin_11), value);
	}

	inline static int32_t get_offset_of__inputPasswordWhenLogin_12() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____inputPasswordWhenLogin_12)); }
	inline InputField_t3762917431 * get__inputPasswordWhenLogin_12() const { return ____inputPasswordWhenLogin_12; }
	inline InputField_t3762917431 ** get_address_of__inputPasswordWhenLogin_12() { return &____inputPasswordWhenLogin_12; }
	inline void set__inputPasswordWhenLogin_12(InputField_t3762917431 * value)
	{
		____inputPasswordWhenLogin_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputPasswordWhenLogin_12), value);
	}

	inline static int32_t get_offset_of__toggleAutoLogin_13() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____toggleAutoLogin_13)); }
	inline Toggle_t2735377061 * get__toggleAutoLogin_13() const { return ____toggleAutoLogin_13; }
	inline Toggle_t2735377061 ** get_address_of__toggleAutoLogin_13() { return &____toggleAutoLogin_13; }
	inline void set__toggleAutoLogin_13(Toggle_t2735377061 * value)
	{
		____toggleAutoLogin_13 = value;
		Il2CppCodeGenWriteBarrier((&____toggleAutoLogin_13), value);
	}

	inline static int32_t get_offset_of__inputRoleName_14() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ____inputRoleName_14)); }
	inline InputField_t3762917431 * get__inputRoleName_14() const { return ____inputRoleName_14; }
	inline InputField_t3762917431 ** get_address_of__inputRoleName_14() { return &____inputRoleName_14; }
	inline void set__inputRoleName_14(InputField_t3762917431 * value)
	{
		____inputRoleName_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputRoleName_14), value);
	}

	inline static int32_t get_offset_of_m_dicAccountInfo_15() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ___m_dicAccountInfo_15)); }
	inline Dictionary_2_t537133542 * get_m_dicAccountInfo_15() const { return ___m_dicAccountInfo_15; }
	inline Dictionary_2_t537133542 ** get_address_of_m_dicAccountInfo_15() { return &___m_dicAccountInfo_15; }
	inline void set_m_dicAccountInfo_15(Dictionary_2_t537133542 * value)
	{
		___m_dicAccountInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAccountInfo_15), value);
	}

	inline static int32_t get_offset_of_m_CurAccoutInfo_16() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ___m_CurAccoutInfo_16)); }
	inline sAccountInfo_t751877243  get_m_CurAccoutInfo_16() const { return ___m_CurAccoutInfo_16; }
	inline sAccountInfo_t751877243 * get_address_of_m_CurAccoutInfo_16() { return &___m_CurAccoutInfo_16; }
	inline void set_m_CurAccoutInfo_16(sAccountInfo_t751877243  value)
	{
		___m_CurAccoutInfo_16 = value;
	}

	inline static int32_t get_offset_of_m_szCurLoginPhoneNum_17() { return static_cast<int32_t>(offsetof(CRegister_t4189215833, ___m_szCurLoginPhoneNum_17)); }
	inline String_t* get_m_szCurLoginPhoneNum_17() const { return ___m_szCurLoginPhoneNum_17; }
	inline String_t** get_address_of_m_szCurLoginPhoneNum_17() { return &___m_szCurLoginPhoneNum_17; }
	inline void set_m_szCurLoginPhoneNum_17(String_t* value)
	{
		___m_szCurLoginPhoneNum_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCurLoginPhoneNum_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREGISTER_T4189215833_H
#ifndef CLOADING_T426279320_H
#define CLOADING_T426279320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLoading
struct  CLoading_t426279320  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CLoading::_aryDots
	GameObjectU5BU5D_t3328599146* ____aryDots_2;
	// System.Int32 CLoading::m_nIndex
	int32_t ___m_nIndex_3;
	// System.Single CLoading::m_fShowDotTimeLapse
	float ___m_fShowDotTimeLapse_5;
	// System.Single CLoading::m_fTotalShowTimeLapse
	float ___m_fTotalShowTimeLapse_6;
	// System.Single CLoading::m_fTotalShowTime
	float ___m_fTotalShowTime_7;
	// System.Boolean CLoading::m_bTotalShowTime
	bool ___m_bTotalShowTime_8;
	// CCommonJinDuTiao CLoading::_commonJinDuTiao
	CCommonJinDuTiao_t2836400377 * ____commonJinDuTiao_9;

public:
	inline static int32_t get_offset_of__aryDots_2() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ____aryDots_2)); }
	inline GameObjectU5BU5D_t3328599146* get__aryDots_2() const { return ____aryDots_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__aryDots_2() { return &____aryDots_2; }
	inline void set__aryDots_2(GameObjectU5BU5D_t3328599146* value)
	{
		____aryDots_2 = value;
		Il2CppCodeGenWriteBarrier((&____aryDots_2), value);
	}

	inline static int32_t get_offset_of_m_nIndex_3() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ___m_nIndex_3)); }
	inline int32_t get_m_nIndex_3() const { return ___m_nIndex_3; }
	inline int32_t* get_address_of_m_nIndex_3() { return &___m_nIndex_3; }
	inline void set_m_nIndex_3(int32_t value)
	{
		___m_nIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_fShowDotTimeLapse_5() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ___m_fShowDotTimeLapse_5)); }
	inline float get_m_fShowDotTimeLapse_5() const { return ___m_fShowDotTimeLapse_5; }
	inline float* get_address_of_m_fShowDotTimeLapse_5() { return &___m_fShowDotTimeLapse_5; }
	inline void set_m_fShowDotTimeLapse_5(float value)
	{
		___m_fShowDotTimeLapse_5 = value;
	}

	inline static int32_t get_offset_of_m_fTotalShowTimeLapse_6() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ___m_fTotalShowTimeLapse_6)); }
	inline float get_m_fTotalShowTimeLapse_6() const { return ___m_fTotalShowTimeLapse_6; }
	inline float* get_address_of_m_fTotalShowTimeLapse_6() { return &___m_fTotalShowTimeLapse_6; }
	inline void set_m_fTotalShowTimeLapse_6(float value)
	{
		___m_fTotalShowTimeLapse_6 = value;
	}

	inline static int32_t get_offset_of_m_fTotalShowTime_7() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ___m_fTotalShowTime_7)); }
	inline float get_m_fTotalShowTime_7() const { return ___m_fTotalShowTime_7; }
	inline float* get_address_of_m_fTotalShowTime_7() { return &___m_fTotalShowTime_7; }
	inline void set_m_fTotalShowTime_7(float value)
	{
		___m_fTotalShowTime_7 = value;
	}

	inline static int32_t get_offset_of_m_bTotalShowTime_8() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ___m_bTotalShowTime_8)); }
	inline bool get_m_bTotalShowTime_8() const { return ___m_bTotalShowTime_8; }
	inline bool* get_address_of_m_bTotalShowTime_8() { return &___m_bTotalShowTime_8; }
	inline void set_m_bTotalShowTime_8(bool value)
	{
		___m_bTotalShowTime_8 = value;
	}

	inline static int32_t get_offset_of__commonJinDuTiao_9() { return static_cast<int32_t>(offsetof(CLoading_t426279320, ____commonJinDuTiao_9)); }
	inline CCommonJinDuTiao_t2836400377 * get__commonJinDuTiao_9() const { return ____commonJinDuTiao_9; }
	inline CCommonJinDuTiao_t2836400377 ** get_address_of__commonJinDuTiao_9() { return &____commonJinDuTiao_9; }
	inline void set__commonJinDuTiao_9(CCommonJinDuTiao_t2836400377 * value)
	{
		____commonJinDuTiao_9 = value;
		Il2CppCodeGenWriteBarrier((&____commonJinDuTiao_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOADING_T426279320_H
#ifndef CRECHARGECOUNTER_T1817650585_H
#define CRECHARGECOUNTER_T1817650585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRechargeCounter
struct  CRechargeCounter_t1817650585  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CRechargeCounter::m_nPrice
	int32_t ___m_nPrice_2;
	// ShoppingMallManager/eMoneyType CRechargeCounter::m_eMonryType
	int32_t ___m_eMonryType_3;
	// System.UInt32 CRechargeCounter::m_nGainMoneyNum
	uint32_t ___m_nGainMoneyNum_4;
	// System.String CRechargeCounter::m_szChargeProductId
	String_t* ___m_szChargeProductId_5;
	// UnityEngine.UI.Text CRechargeCounter::_txtGainMoneyNum
	Text_t1901882714 * ____txtGainMoneyNum_6;

public:
	inline static int32_t get_offset_of_m_nPrice_2() { return static_cast<int32_t>(offsetof(CRechargeCounter_t1817650585, ___m_nPrice_2)); }
	inline int32_t get_m_nPrice_2() const { return ___m_nPrice_2; }
	inline int32_t* get_address_of_m_nPrice_2() { return &___m_nPrice_2; }
	inline void set_m_nPrice_2(int32_t value)
	{
		___m_nPrice_2 = value;
	}

	inline static int32_t get_offset_of_m_eMonryType_3() { return static_cast<int32_t>(offsetof(CRechargeCounter_t1817650585, ___m_eMonryType_3)); }
	inline int32_t get_m_eMonryType_3() const { return ___m_eMonryType_3; }
	inline int32_t* get_address_of_m_eMonryType_3() { return &___m_eMonryType_3; }
	inline void set_m_eMonryType_3(int32_t value)
	{
		___m_eMonryType_3 = value;
	}

	inline static int32_t get_offset_of_m_nGainMoneyNum_4() { return static_cast<int32_t>(offsetof(CRechargeCounter_t1817650585, ___m_nGainMoneyNum_4)); }
	inline uint32_t get_m_nGainMoneyNum_4() const { return ___m_nGainMoneyNum_4; }
	inline uint32_t* get_address_of_m_nGainMoneyNum_4() { return &___m_nGainMoneyNum_4; }
	inline void set_m_nGainMoneyNum_4(uint32_t value)
	{
		___m_nGainMoneyNum_4 = value;
	}

	inline static int32_t get_offset_of_m_szChargeProductId_5() { return static_cast<int32_t>(offsetof(CRechargeCounter_t1817650585, ___m_szChargeProductId_5)); }
	inline String_t* get_m_szChargeProductId_5() const { return ___m_szChargeProductId_5; }
	inline String_t** get_address_of_m_szChargeProductId_5() { return &___m_szChargeProductId_5; }
	inline void set_m_szChargeProductId_5(String_t* value)
	{
		___m_szChargeProductId_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_szChargeProductId_5), value);
	}

	inline static int32_t get_offset_of__txtGainMoneyNum_6() { return static_cast<int32_t>(offsetof(CRechargeCounter_t1817650585, ____txtGainMoneyNum_6)); }
	inline Text_t1901882714 * get__txtGainMoneyNum_6() const { return ____txtGainMoneyNum_6; }
	inline Text_t1901882714 ** get_address_of__txtGainMoneyNum_6() { return &____txtGainMoneyNum_6; }
	inline void set__txtGainMoneyNum_6(Text_t1901882714 * value)
	{
		____txtGainMoneyNum_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtGainMoneyNum_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRECHARGECOUNTER_T1817650585_H
#ifndef SCENEACTION_T888531997_H
#define SCENEACTION_T888531997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneAction
struct  SceneAction_t888531997  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Queue SceneAction::_ball_queue
	Queue_t3637523393 * ____ball_queue_3;
	// UnityEngine.GameObject SceneAction::_ball_prefab
	GameObject_t1113636619 * ____ball_prefab_4;
	// System.Int32 SceneAction::_num_of_ball_per_batch
	int32_t ____num_of_ball_per_batch_5;
	// System.Int32 SceneAction::_num_of_ball_batch
	int32_t ____num_of_ball_batch_6;
	// UnityEngine.GameObject[] SceneAction::_food_prefabs
	GameObjectU5BU5D_t3328599146* ____food_prefabs_7;
	// System.Int32 SceneAction::_num_of_food_per_batch
	int32_t ____num_of_food_per_batch_8;
	// System.Int32 SceneAction::_num_of_food_batch
	int32_t ____num_of_food_batch_9;
	// System.Collections.Queue SceneAction::_food_queue
	Queue_t3637523393 * ____food_queue_10;
	// System.Single SceneAction::_last_refresh_food_time
	float ____last_refresh_food_time_11;
	// System.Single SceneAction::_refresh_food_interval
	float ____refresh_food_interval_12;
	// UnityEngine.GameObject SceneAction::_hedge_prefab
	GameObject_t1113636619 * ____hedge_prefab_13;
	// System.Int32 SceneAction::_num_of_hedge
	int32_t ____num_of_hedge_14;
	// SceneStatus SceneAction::_scene_status
	int32_t ____scene_status_15;

public:
	inline static int32_t get_offset_of__ball_queue_3() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____ball_queue_3)); }
	inline Queue_t3637523393 * get__ball_queue_3() const { return ____ball_queue_3; }
	inline Queue_t3637523393 ** get_address_of__ball_queue_3() { return &____ball_queue_3; }
	inline void set__ball_queue_3(Queue_t3637523393 * value)
	{
		____ball_queue_3 = value;
		Il2CppCodeGenWriteBarrier((&____ball_queue_3), value);
	}

	inline static int32_t get_offset_of__ball_prefab_4() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____ball_prefab_4)); }
	inline GameObject_t1113636619 * get__ball_prefab_4() const { return ____ball_prefab_4; }
	inline GameObject_t1113636619 ** get_address_of__ball_prefab_4() { return &____ball_prefab_4; }
	inline void set__ball_prefab_4(GameObject_t1113636619 * value)
	{
		____ball_prefab_4 = value;
		Il2CppCodeGenWriteBarrier((&____ball_prefab_4), value);
	}

	inline static int32_t get_offset_of__num_of_ball_per_batch_5() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____num_of_ball_per_batch_5)); }
	inline int32_t get__num_of_ball_per_batch_5() const { return ____num_of_ball_per_batch_5; }
	inline int32_t* get_address_of__num_of_ball_per_batch_5() { return &____num_of_ball_per_batch_5; }
	inline void set__num_of_ball_per_batch_5(int32_t value)
	{
		____num_of_ball_per_batch_5 = value;
	}

	inline static int32_t get_offset_of__num_of_ball_batch_6() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____num_of_ball_batch_6)); }
	inline int32_t get__num_of_ball_batch_6() const { return ____num_of_ball_batch_6; }
	inline int32_t* get_address_of__num_of_ball_batch_6() { return &____num_of_ball_batch_6; }
	inline void set__num_of_ball_batch_6(int32_t value)
	{
		____num_of_ball_batch_6 = value;
	}

	inline static int32_t get_offset_of__food_prefabs_7() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____food_prefabs_7)); }
	inline GameObjectU5BU5D_t3328599146* get__food_prefabs_7() const { return ____food_prefabs_7; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__food_prefabs_7() { return &____food_prefabs_7; }
	inline void set__food_prefabs_7(GameObjectU5BU5D_t3328599146* value)
	{
		____food_prefabs_7 = value;
		Il2CppCodeGenWriteBarrier((&____food_prefabs_7), value);
	}

	inline static int32_t get_offset_of__num_of_food_per_batch_8() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____num_of_food_per_batch_8)); }
	inline int32_t get__num_of_food_per_batch_8() const { return ____num_of_food_per_batch_8; }
	inline int32_t* get_address_of__num_of_food_per_batch_8() { return &____num_of_food_per_batch_8; }
	inline void set__num_of_food_per_batch_8(int32_t value)
	{
		____num_of_food_per_batch_8 = value;
	}

	inline static int32_t get_offset_of__num_of_food_batch_9() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____num_of_food_batch_9)); }
	inline int32_t get__num_of_food_batch_9() const { return ____num_of_food_batch_9; }
	inline int32_t* get_address_of__num_of_food_batch_9() { return &____num_of_food_batch_9; }
	inline void set__num_of_food_batch_9(int32_t value)
	{
		____num_of_food_batch_9 = value;
	}

	inline static int32_t get_offset_of__food_queue_10() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____food_queue_10)); }
	inline Queue_t3637523393 * get__food_queue_10() const { return ____food_queue_10; }
	inline Queue_t3637523393 ** get_address_of__food_queue_10() { return &____food_queue_10; }
	inline void set__food_queue_10(Queue_t3637523393 * value)
	{
		____food_queue_10 = value;
		Il2CppCodeGenWriteBarrier((&____food_queue_10), value);
	}

	inline static int32_t get_offset_of__last_refresh_food_time_11() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____last_refresh_food_time_11)); }
	inline float get__last_refresh_food_time_11() const { return ____last_refresh_food_time_11; }
	inline float* get_address_of__last_refresh_food_time_11() { return &____last_refresh_food_time_11; }
	inline void set__last_refresh_food_time_11(float value)
	{
		____last_refresh_food_time_11 = value;
	}

	inline static int32_t get_offset_of__refresh_food_interval_12() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____refresh_food_interval_12)); }
	inline float get__refresh_food_interval_12() const { return ____refresh_food_interval_12; }
	inline float* get_address_of__refresh_food_interval_12() { return &____refresh_food_interval_12; }
	inline void set__refresh_food_interval_12(float value)
	{
		____refresh_food_interval_12 = value;
	}

	inline static int32_t get_offset_of__hedge_prefab_13() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____hedge_prefab_13)); }
	inline GameObject_t1113636619 * get__hedge_prefab_13() const { return ____hedge_prefab_13; }
	inline GameObject_t1113636619 ** get_address_of__hedge_prefab_13() { return &____hedge_prefab_13; }
	inline void set__hedge_prefab_13(GameObject_t1113636619 * value)
	{
		____hedge_prefab_13 = value;
		Il2CppCodeGenWriteBarrier((&____hedge_prefab_13), value);
	}

	inline static int32_t get_offset_of__num_of_hedge_14() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____num_of_hedge_14)); }
	inline int32_t get__num_of_hedge_14() const { return ____num_of_hedge_14; }
	inline int32_t* get_address_of__num_of_hedge_14() { return &____num_of_hedge_14; }
	inline void set__num_of_hedge_14(int32_t value)
	{
		____num_of_hedge_14 = value;
	}

	inline static int32_t get_offset_of__scene_status_15() { return static_cast<int32_t>(offsetof(SceneAction_t888531997, ____scene_status_15)); }
	inline int32_t get__scene_status_15() const { return ____scene_status_15; }
	inline int32_t* get_address_of__scene_status_15() { return &____scene_status_15; }
	inline void set__scene_status_15(int32_t value)
	{
		____scene_status_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEACTION_T888531997_H
#ifndef CSETTINGSMANAGER_T1554003656_H
#define CSETTINGSMANAGER_T1554003656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSettingsManager
struct  CSettingsManager_t1554003656  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CSettingsManager::m_fVolumeChangeAmount
	float ___m_fVolumeChangeAmount_3;
	// UnityEngine.UI.Slider CSettingsManager::_sliderMusic
	Slider_t3903728902 * ____sliderMusic_4;
	// UnityEngine.UI.Slider[] CSettingsManager::m_arySliderMusicVolume
	SliderU5BU5D_t3847901219* ___m_arySliderMusicVolume_5;
	// UnityEngine.UI.Slider CSettingsManager::_sliderAudio
	Slider_t3903728902 * ____sliderAudio_6;
	// UnityEngine.UI.Slider[] CSettingsManager::m_arySliderAudioVolume
	SliderU5BU5D_t3847901219* ___m_arySliderAudioVolume_7;
	// CProgressBar[] CSettingsManager::m_aryMusicVolume
	CProgressBarU5BU5D_t529576237* ___m_aryMusicVolume_8;
	// CProgressBar[] CSettingsManager::m_aryAudioVolume
	CProgressBarU5BU5D_t529576237* ___m_aryAudioVolume_9;
	// System.Single CSettingsManager::m_fMusicVolume
	float ___m_fMusicVolume_10;
	// System.Single CSettingsManager::m_fAudioVolume
	float ___m_fAudioVolume_11;

public:
	inline static int32_t get_offset_of_m_fVolumeChangeAmount_3() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_fVolumeChangeAmount_3)); }
	inline float get_m_fVolumeChangeAmount_3() const { return ___m_fVolumeChangeAmount_3; }
	inline float* get_address_of_m_fVolumeChangeAmount_3() { return &___m_fVolumeChangeAmount_3; }
	inline void set_m_fVolumeChangeAmount_3(float value)
	{
		___m_fVolumeChangeAmount_3 = value;
	}

	inline static int32_t get_offset_of__sliderMusic_4() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ____sliderMusic_4)); }
	inline Slider_t3903728902 * get__sliderMusic_4() const { return ____sliderMusic_4; }
	inline Slider_t3903728902 ** get_address_of__sliderMusic_4() { return &____sliderMusic_4; }
	inline void set__sliderMusic_4(Slider_t3903728902 * value)
	{
		____sliderMusic_4 = value;
		Il2CppCodeGenWriteBarrier((&____sliderMusic_4), value);
	}

	inline static int32_t get_offset_of_m_arySliderMusicVolume_5() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_arySliderMusicVolume_5)); }
	inline SliderU5BU5D_t3847901219* get_m_arySliderMusicVolume_5() const { return ___m_arySliderMusicVolume_5; }
	inline SliderU5BU5D_t3847901219** get_address_of_m_arySliderMusicVolume_5() { return &___m_arySliderMusicVolume_5; }
	inline void set_m_arySliderMusicVolume_5(SliderU5BU5D_t3847901219* value)
	{
		___m_arySliderMusicVolume_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySliderMusicVolume_5), value);
	}

	inline static int32_t get_offset_of__sliderAudio_6() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ____sliderAudio_6)); }
	inline Slider_t3903728902 * get__sliderAudio_6() const { return ____sliderAudio_6; }
	inline Slider_t3903728902 ** get_address_of__sliderAudio_6() { return &____sliderAudio_6; }
	inline void set__sliderAudio_6(Slider_t3903728902 * value)
	{
		____sliderAudio_6 = value;
		Il2CppCodeGenWriteBarrier((&____sliderAudio_6), value);
	}

	inline static int32_t get_offset_of_m_arySliderAudioVolume_7() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_arySliderAudioVolume_7)); }
	inline SliderU5BU5D_t3847901219* get_m_arySliderAudioVolume_7() const { return ___m_arySliderAudioVolume_7; }
	inline SliderU5BU5D_t3847901219** get_address_of_m_arySliderAudioVolume_7() { return &___m_arySliderAudioVolume_7; }
	inline void set_m_arySliderAudioVolume_7(SliderU5BU5D_t3847901219* value)
	{
		___m_arySliderAudioVolume_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySliderAudioVolume_7), value);
	}

	inline static int32_t get_offset_of_m_aryMusicVolume_8() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_aryMusicVolume_8)); }
	inline CProgressBarU5BU5D_t529576237* get_m_aryMusicVolume_8() const { return ___m_aryMusicVolume_8; }
	inline CProgressBarU5BU5D_t529576237** get_address_of_m_aryMusicVolume_8() { return &___m_aryMusicVolume_8; }
	inline void set_m_aryMusicVolume_8(CProgressBarU5BU5D_t529576237* value)
	{
		___m_aryMusicVolume_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMusicVolume_8), value);
	}

	inline static int32_t get_offset_of_m_aryAudioVolume_9() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_aryAudioVolume_9)); }
	inline CProgressBarU5BU5D_t529576237* get_m_aryAudioVolume_9() const { return ___m_aryAudioVolume_9; }
	inline CProgressBarU5BU5D_t529576237** get_address_of_m_aryAudioVolume_9() { return &___m_aryAudioVolume_9; }
	inline void set_m_aryAudioVolume_9(CProgressBarU5BU5D_t529576237* value)
	{
		___m_aryAudioVolume_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAudioVolume_9), value);
	}

	inline static int32_t get_offset_of_m_fMusicVolume_10() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_fMusicVolume_10)); }
	inline float get_m_fMusicVolume_10() const { return ___m_fMusicVolume_10; }
	inline float* get_address_of_m_fMusicVolume_10() { return &___m_fMusicVolume_10; }
	inline void set_m_fMusicVolume_10(float value)
	{
		___m_fMusicVolume_10 = value;
	}

	inline static int32_t get_offset_of_m_fAudioVolume_11() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656, ___m_fAudioVolume_11)); }
	inline float get_m_fAudioVolume_11() const { return ___m_fAudioVolume_11; }
	inline float* get_address_of_m_fAudioVolume_11() { return &___m_fAudioVolume_11; }
	inline void set_m_fAudioVolume_11(float value)
	{
		___m_fAudioVolume_11 = value;
	}
};

struct CSettingsManager_t1554003656_StaticFields
{
public:
	// CSettingsManager CSettingsManager::s_Instance
	CSettingsManager_t1554003656 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CSettingsManager_t1554003656_StaticFields, ___s_Instance_2)); }
	inline CSettingsManager_t1554003656 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CSettingsManager_t1554003656 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CSettingsManager_t1554003656 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSETTINGSMANAGER_T1554003656_H
#ifndef PLAYERACTION_T3782228511_H
#define PLAYERACTION_T3782228511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerAction
struct  PlayerAction_t3782228511  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PlayerAction::m_fArrowRadius
	float ___m_fArrowRadius_3;
	// CArrow PlayerAction::_arrowYaoGan
	CArrow_t1475493256 * ____arrowYaoGan_4;
	// UnityEngine.Canvas PlayerAction::_canvas
	Canvas_t3310196443 * ____canvas_5;
	// UnityEngine.GameObject PlayerAction::_global
	GameObject_t1113636619 * ____global_6;
	// UnityEngine.GameObject PlayerAction::_center
	GameObject_t1113636619 * ____center_7;
	// UnityEngine.GameObject PlayerAction::_world_cursor
	GameObject_t1113636619 * ____world_cursor_8;
	// UnityEngine.GameObject PlayerAction::_screen_cursor
	GameObject_t1113636619 * ____screen_cursor_9;
	// UnityEngine.Vector2 PlayerAction::_cursor_direction
	Vector2_t2156229523  ____cursor_direction_10;
	// UnityEngine.Vector2 PlayerAction::_joystick_movement
	Vector2_t2156229523  ____joystick_movement_11;
	// System.Single PlayerAction::_cursor_velocity
	float ____cursor_velocity_12;
	// System.Single PlayerAction::_cursor_realtime_velocity
	float ____cursor_realtime_velocity_13;
	// System.Single PlayerAction::_cursor_radius
	float ____cursor_radius_14;
	// System.Single PlayerAction::_cursor_move_time_velocity
	float ____cursor_move_time_velocity_15;
	// System.Single PlayerAction::_ball_velocity
	float ____ball_velocity_17;
	// System.Single PlayerAction::_ball_realtime_velocity
	float ____ball_realtime_velocity_18;
	// System.Single PlayerAction::_ball_realtime_velocity_without_size
	float ____ball_realtime_velocity_without_size_19;
	// UnityEngine.Vector3 PlayerAction::_center_move_velocity
	Vector3_t3722313464  ____center_move_velocity_20;
	// System.Single PlayerAction::_center_move_time_velocity
	float ____center_move_time_velocity_23;
	// System.Single PlayerAction::_center_move_time
	float ____center_move_time_24;
	// System.Boolean PlayerAction::_release_joystick_keep_position
	bool ____release_joystick_keep_position_25;
	// System.Boolean PlayerAction::_release_joystick_fast_stop
	bool ____release_joystick_fast_stop_26;
	// System.Boolean PlayerAction::_release_joystick_fast_rotation
	bool ____release_joystick_fast_rotation_27;
	// UnityEngine.Vector2 PlayerAction::_touch_screen_position
	Vector2_t2156229523  ____touch_screen_position_28;
	// UnityEngine.Vector3 PlayerAction::_touch_world_position
	Vector3_t3722313464  ____touch_world_position_29;
	// System.Single PlayerAction::_touch_start_time
	float ____touch_start_time_31;
	// System.Single PlayerAction::joystick_radius_multiple
	float ___joystick_radius_multiple_32;
	// System.Single PlayerAction::m_nFrameCount
	float ___m_nFrameCount_34;
	// UnityEngine.Vector2 PlayerAction::m_vec2MoveInfo
	Vector2_t2156229523  ___m_vec2MoveInfo_36;
	// System.Double PlayerAction::m_dMouseDownTime
	double ___m_dMouseDownTime_37;
	// UnityEngine.Vector3 PlayerAction::balls_center_position
	Vector3_t3722313464  ___balls_center_position_38;
	// UnityEngine.Vector3 PlayerAction::balls_min_position
	Vector3_t3722313464  ___balls_min_position_39;
	// UnityEngine.Vector3 PlayerAction::balls_max_position
	Vector3_t3722313464  ___balls_max_position_40;
	// UnityEngine.Vector3 PlayerAction::m_vecBallsCenter
	Vector3_t3722313464  ___m_vecBallsCenter_41;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> PlayerAction::dicTempGroupInGrass
	Dictionary_2_t285980105 * ___dicTempGroupInGrass_42;
	// System.Single PlayerAction::m_fPkSelfBallTimeCount
	float ___m_fPkSelfBallTimeCount_43;
	// System.Collections.Generic.List`1<Ball> PlayerAction::m_lstMostBigBallsToShowName
	List_1_t3678741308 * ___m_lstMostBigBallsToShowName_49;
	// System.Boolean PlayerAction::m_bPosInfoCalculated
	bool ___m_bPosInfoCalculated_50;
	// System.Single PlayerAction::m_fShitTimeElapse
	float ___m_fShitTimeElapse_51;
	// UnityEngine.Vector3 PlayerAction::m_vecLastFrameBallsCenterMovement
	Vector3_t3722313464  ___m_vecLastFrameBallsCenterMovement_53;
	// UnityEngine.Vector3 PlayerAction::m_vecLastFrameBallsPostion
	Vector3_t3722313464  ___m_vecLastFrameBallsPostion_54;
	// System.Boolean PlayerAction::m_bFirstForBallsCenterMovement
	bool ___m_bFirstForBallsCenterMovement_55;
	// System.Single PlayerAction::m_fBallTeamRadius
	float ___m_fBallTeamRadius_56;
	// UnityEngine.Vector3 PlayerAction::vecLeftBottom
	Vector3_t3722313464  ___vecLeftBottom_57;
	// UnityEngine.Vector3 PlayerAction::vecRightTop
	Vector3_t3722313464  ___vecRightTop_58;
	// System.Collections.Generic.List`1<System.Int32> PlayerAction::m_lstMoveOrderX
	List_1_t128053199 * ___m_lstMoveOrderX_59;
	// System.Collections.Generic.List`1<System.Int32> PlayerAction::m_lstMoveOrderY
	List_1_t128053199 * ___m_lstMoveOrderY_60;
	// UnityEngine.Vector2 PlayerAction::_vecYaoGanDir
	Vector2_t2156229523  ____vecYaoGanDir_61;

public:
	inline static int32_t get_offset_of_m_fArrowRadius_3() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_fArrowRadius_3)); }
	inline float get_m_fArrowRadius_3() const { return ___m_fArrowRadius_3; }
	inline float* get_address_of_m_fArrowRadius_3() { return &___m_fArrowRadius_3; }
	inline void set_m_fArrowRadius_3(float value)
	{
		___m_fArrowRadius_3 = value;
	}

	inline static int32_t get_offset_of__arrowYaoGan_4() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____arrowYaoGan_4)); }
	inline CArrow_t1475493256 * get__arrowYaoGan_4() const { return ____arrowYaoGan_4; }
	inline CArrow_t1475493256 ** get_address_of__arrowYaoGan_4() { return &____arrowYaoGan_4; }
	inline void set__arrowYaoGan_4(CArrow_t1475493256 * value)
	{
		____arrowYaoGan_4 = value;
		Il2CppCodeGenWriteBarrier((&____arrowYaoGan_4), value);
	}

	inline static int32_t get_offset_of__canvas_5() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____canvas_5)); }
	inline Canvas_t3310196443 * get__canvas_5() const { return ____canvas_5; }
	inline Canvas_t3310196443 ** get_address_of__canvas_5() { return &____canvas_5; }
	inline void set__canvas_5(Canvas_t3310196443 * value)
	{
		____canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_5), value);
	}

	inline static int32_t get_offset_of__global_6() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____global_6)); }
	inline GameObject_t1113636619 * get__global_6() const { return ____global_6; }
	inline GameObject_t1113636619 ** get_address_of__global_6() { return &____global_6; }
	inline void set__global_6(GameObject_t1113636619 * value)
	{
		____global_6 = value;
		Il2CppCodeGenWriteBarrier((&____global_6), value);
	}

	inline static int32_t get_offset_of__center_7() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____center_7)); }
	inline GameObject_t1113636619 * get__center_7() const { return ____center_7; }
	inline GameObject_t1113636619 ** get_address_of__center_7() { return &____center_7; }
	inline void set__center_7(GameObject_t1113636619 * value)
	{
		____center_7 = value;
		Il2CppCodeGenWriteBarrier((&____center_7), value);
	}

	inline static int32_t get_offset_of__world_cursor_8() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____world_cursor_8)); }
	inline GameObject_t1113636619 * get__world_cursor_8() const { return ____world_cursor_8; }
	inline GameObject_t1113636619 ** get_address_of__world_cursor_8() { return &____world_cursor_8; }
	inline void set__world_cursor_8(GameObject_t1113636619 * value)
	{
		____world_cursor_8 = value;
		Il2CppCodeGenWriteBarrier((&____world_cursor_8), value);
	}

	inline static int32_t get_offset_of__screen_cursor_9() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____screen_cursor_9)); }
	inline GameObject_t1113636619 * get__screen_cursor_9() const { return ____screen_cursor_9; }
	inline GameObject_t1113636619 ** get_address_of__screen_cursor_9() { return &____screen_cursor_9; }
	inline void set__screen_cursor_9(GameObject_t1113636619 * value)
	{
		____screen_cursor_9 = value;
		Il2CppCodeGenWriteBarrier((&____screen_cursor_9), value);
	}

	inline static int32_t get_offset_of__cursor_direction_10() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____cursor_direction_10)); }
	inline Vector2_t2156229523  get__cursor_direction_10() const { return ____cursor_direction_10; }
	inline Vector2_t2156229523 * get_address_of__cursor_direction_10() { return &____cursor_direction_10; }
	inline void set__cursor_direction_10(Vector2_t2156229523  value)
	{
		____cursor_direction_10 = value;
	}

	inline static int32_t get_offset_of__joystick_movement_11() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____joystick_movement_11)); }
	inline Vector2_t2156229523  get__joystick_movement_11() const { return ____joystick_movement_11; }
	inline Vector2_t2156229523 * get_address_of__joystick_movement_11() { return &____joystick_movement_11; }
	inline void set__joystick_movement_11(Vector2_t2156229523  value)
	{
		____joystick_movement_11 = value;
	}

	inline static int32_t get_offset_of__cursor_velocity_12() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____cursor_velocity_12)); }
	inline float get__cursor_velocity_12() const { return ____cursor_velocity_12; }
	inline float* get_address_of__cursor_velocity_12() { return &____cursor_velocity_12; }
	inline void set__cursor_velocity_12(float value)
	{
		____cursor_velocity_12 = value;
	}

	inline static int32_t get_offset_of__cursor_realtime_velocity_13() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____cursor_realtime_velocity_13)); }
	inline float get__cursor_realtime_velocity_13() const { return ____cursor_realtime_velocity_13; }
	inline float* get_address_of__cursor_realtime_velocity_13() { return &____cursor_realtime_velocity_13; }
	inline void set__cursor_realtime_velocity_13(float value)
	{
		____cursor_realtime_velocity_13 = value;
	}

	inline static int32_t get_offset_of__cursor_radius_14() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____cursor_radius_14)); }
	inline float get__cursor_radius_14() const { return ____cursor_radius_14; }
	inline float* get_address_of__cursor_radius_14() { return &____cursor_radius_14; }
	inline void set__cursor_radius_14(float value)
	{
		____cursor_radius_14 = value;
	}

	inline static int32_t get_offset_of__cursor_move_time_velocity_15() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____cursor_move_time_velocity_15)); }
	inline float get__cursor_move_time_velocity_15() const { return ____cursor_move_time_velocity_15; }
	inline float* get_address_of__cursor_move_time_velocity_15() { return &____cursor_move_time_velocity_15; }
	inline void set__cursor_move_time_velocity_15(float value)
	{
		____cursor_move_time_velocity_15 = value;
	}

	inline static int32_t get_offset_of__ball_velocity_17() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____ball_velocity_17)); }
	inline float get__ball_velocity_17() const { return ____ball_velocity_17; }
	inline float* get_address_of__ball_velocity_17() { return &____ball_velocity_17; }
	inline void set__ball_velocity_17(float value)
	{
		____ball_velocity_17 = value;
	}

	inline static int32_t get_offset_of__ball_realtime_velocity_18() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____ball_realtime_velocity_18)); }
	inline float get__ball_realtime_velocity_18() const { return ____ball_realtime_velocity_18; }
	inline float* get_address_of__ball_realtime_velocity_18() { return &____ball_realtime_velocity_18; }
	inline void set__ball_realtime_velocity_18(float value)
	{
		____ball_realtime_velocity_18 = value;
	}

	inline static int32_t get_offset_of__ball_realtime_velocity_without_size_19() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____ball_realtime_velocity_without_size_19)); }
	inline float get__ball_realtime_velocity_without_size_19() const { return ____ball_realtime_velocity_without_size_19; }
	inline float* get_address_of__ball_realtime_velocity_without_size_19() { return &____ball_realtime_velocity_without_size_19; }
	inline void set__ball_realtime_velocity_without_size_19(float value)
	{
		____ball_realtime_velocity_without_size_19 = value;
	}

	inline static int32_t get_offset_of__center_move_velocity_20() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____center_move_velocity_20)); }
	inline Vector3_t3722313464  get__center_move_velocity_20() const { return ____center_move_velocity_20; }
	inline Vector3_t3722313464 * get_address_of__center_move_velocity_20() { return &____center_move_velocity_20; }
	inline void set__center_move_velocity_20(Vector3_t3722313464  value)
	{
		____center_move_velocity_20 = value;
	}

	inline static int32_t get_offset_of__center_move_time_velocity_23() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____center_move_time_velocity_23)); }
	inline float get__center_move_time_velocity_23() const { return ____center_move_time_velocity_23; }
	inline float* get_address_of__center_move_time_velocity_23() { return &____center_move_time_velocity_23; }
	inline void set__center_move_time_velocity_23(float value)
	{
		____center_move_time_velocity_23 = value;
	}

	inline static int32_t get_offset_of__center_move_time_24() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____center_move_time_24)); }
	inline float get__center_move_time_24() const { return ____center_move_time_24; }
	inline float* get_address_of__center_move_time_24() { return &____center_move_time_24; }
	inline void set__center_move_time_24(float value)
	{
		____center_move_time_24 = value;
	}

	inline static int32_t get_offset_of__release_joystick_keep_position_25() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____release_joystick_keep_position_25)); }
	inline bool get__release_joystick_keep_position_25() const { return ____release_joystick_keep_position_25; }
	inline bool* get_address_of__release_joystick_keep_position_25() { return &____release_joystick_keep_position_25; }
	inline void set__release_joystick_keep_position_25(bool value)
	{
		____release_joystick_keep_position_25 = value;
	}

	inline static int32_t get_offset_of__release_joystick_fast_stop_26() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____release_joystick_fast_stop_26)); }
	inline bool get__release_joystick_fast_stop_26() const { return ____release_joystick_fast_stop_26; }
	inline bool* get_address_of__release_joystick_fast_stop_26() { return &____release_joystick_fast_stop_26; }
	inline void set__release_joystick_fast_stop_26(bool value)
	{
		____release_joystick_fast_stop_26 = value;
	}

	inline static int32_t get_offset_of__release_joystick_fast_rotation_27() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____release_joystick_fast_rotation_27)); }
	inline bool get__release_joystick_fast_rotation_27() const { return ____release_joystick_fast_rotation_27; }
	inline bool* get_address_of__release_joystick_fast_rotation_27() { return &____release_joystick_fast_rotation_27; }
	inline void set__release_joystick_fast_rotation_27(bool value)
	{
		____release_joystick_fast_rotation_27 = value;
	}

	inline static int32_t get_offset_of__touch_screen_position_28() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____touch_screen_position_28)); }
	inline Vector2_t2156229523  get__touch_screen_position_28() const { return ____touch_screen_position_28; }
	inline Vector2_t2156229523 * get_address_of__touch_screen_position_28() { return &____touch_screen_position_28; }
	inline void set__touch_screen_position_28(Vector2_t2156229523  value)
	{
		____touch_screen_position_28 = value;
	}

	inline static int32_t get_offset_of__touch_world_position_29() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____touch_world_position_29)); }
	inline Vector3_t3722313464  get__touch_world_position_29() const { return ____touch_world_position_29; }
	inline Vector3_t3722313464 * get_address_of__touch_world_position_29() { return &____touch_world_position_29; }
	inline void set__touch_world_position_29(Vector3_t3722313464  value)
	{
		____touch_world_position_29 = value;
	}

	inline static int32_t get_offset_of__touch_start_time_31() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____touch_start_time_31)); }
	inline float get__touch_start_time_31() const { return ____touch_start_time_31; }
	inline float* get_address_of__touch_start_time_31() { return &____touch_start_time_31; }
	inline void set__touch_start_time_31(float value)
	{
		____touch_start_time_31 = value;
	}

	inline static int32_t get_offset_of_joystick_radius_multiple_32() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___joystick_radius_multiple_32)); }
	inline float get_joystick_radius_multiple_32() const { return ___joystick_radius_multiple_32; }
	inline float* get_address_of_joystick_radius_multiple_32() { return &___joystick_radius_multiple_32; }
	inline void set_joystick_radius_multiple_32(float value)
	{
		___joystick_radius_multiple_32 = value;
	}

	inline static int32_t get_offset_of_m_nFrameCount_34() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_nFrameCount_34)); }
	inline float get_m_nFrameCount_34() const { return ___m_nFrameCount_34; }
	inline float* get_address_of_m_nFrameCount_34() { return &___m_nFrameCount_34; }
	inline void set_m_nFrameCount_34(float value)
	{
		___m_nFrameCount_34 = value;
	}

	inline static int32_t get_offset_of_m_vec2MoveInfo_36() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_vec2MoveInfo_36)); }
	inline Vector2_t2156229523  get_m_vec2MoveInfo_36() const { return ___m_vec2MoveInfo_36; }
	inline Vector2_t2156229523 * get_address_of_m_vec2MoveInfo_36() { return &___m_vec2MoveInfo_36; }
	inline void set_m_vec2MoveInfo_36(Vector2_t2156229523  value)
	{
		___m_vec2MoveInfo_36 = value;
	}

	inline static int32_t get_offset_of_m_dMouseDownTime_37() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_dMouseDownTime_37)); }
	inline double get_m_dMouseDownTime_37() const { return ___m_dMouseDownTime_37; }
	inline double* get_address_of_m_dMouseDownTime_37() { return &___m_dMouseDownTime_37; }
	inline void set_m_dMouseDownTime_37(double value)
	{
		___m_dMouseDownTime_37 = value;
	}

	inline static int32_t get_offset_of_balls_center_position_38() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___balls_center_position_38)); }
	inline Vector3_t3722313464  get_balls_center_position_38() const { return ___balls_center_position_38; }
	inline Vector3_t3722313464 * get_address_of_balls_center_position_38() { return &___balls_center_position_38; }
	inline void set_balls_center_position_38(Vector3_t3722313464  value)
	{
		___balls_center_position_38 = value;
	}

	inline static int32_t get_offset_of_balls_min_position_39() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___balls_min_position_39)); }
	inline Vector3_t3722313464  get_balls_min_position_39() const { return ___balls_min_position_39; }
	inline Vector3_t3722313464 * get_address_of_balls_min_position_39() { return &___balls_min_position_39; }
	inline void set_balls_min_position_39(Vector3_t3722313464  value)
	{
		___balls_min_position_39 = value;
	}

	inline static int32_t get_offset_of_balls_max_position_40() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___balls_max_position_40)); }
	inline Vector3_t3722313464  get_balls_max_position_40() const { return ___balls_max_position_40; }
	inline Vector3_t3722313464 * get_address_of_balls_max_position_40() { return &___balls_max_position_40; }
	inline void set_balls_max_position_40(Vector3_t3722313464  value)
	{
		___balls_max_position_40 = value;
	}

	inline static int32_t get_offset_of_m_vecBallsCenter_41() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_vecBallsCenter_41)); }
	inline Vector3_t3722313464  get_m_vecBallsCenter_41() const { return ___m_vecBallsCenter_41; }
	inline Vector3_t3722313464 * get_address_of_m_vecBallsCenter_41() { return &___m_vecBallsCenter_41; }
	inline void set_m_vecBallsCenter_41(Vector3_t3722313464  value)
	{
		___m_vecBallsCenter_41 = value;
	}

	inline static int32_t get_offset_of_dicTempGroupInGrass_42() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___dicTempGroupInGrass_42)); }
	inline Dictionary_2_t285980105 * get_dicTempGroupInGrass_42() const { return ___dicTempGroupInGrass_42; }
	inline Dictionary_2_t285980105 ** get_address_of_dicTempGroupInGrass_42() { return &___dicTempGroupInGrass_42; }
	inline void set_dicTempGroupInGrass_42(Dictionary_2_t285980105 * value)
	{
		___dicTempGroupInGrass_42 = value;
		Il2CppCodeGenWriteBarrier((&___dicTempGroupInGrass_42), value);
	}

	inline static int32_t get_offset_of_m_fPkSelfBallTimeCount_43() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_fPkSelfBallTimeCount_43)); }
	inline float get_m_fPkSelfBallTimeCount_43() const { return ___m_fPkSelfBallTimeCount_43; }
	inline float* get_address_of_m_fPkSelfBallTimeCount_43() { return &___m_fPkSelfBallTimeCount_43; }
	inline void set_m_fPkSelfBallTimeCount_43(float value)
	{
		___m_fPkSelfBallTimeCount_43 = value;
	}

	inline static int32_t get_offset_of_m_lstMostBigBallsToShowName_49() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_lstMostBigBallsToShowName_49)); }
	inline List_1_t3678741308 * get_m_lstMostBigBallsToShowName_49() const { return ___m_lstMostBigBallsToShowName_49; }
	inline List_1_t3678741308 ** get_address_of_m_lstMostBigBallsToShowName_49() { return &___m_lstMostBigBallsToShowName_49; }
	inline void set_m_lstMostBigBallsToShowName_49(List_1_t3678741308 * value)
	{
		___m_lstMostBigBallsToShowName_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMostBigBallsToShowName_49), value);
	}

	inline static int32_t get_offset_of_m_bPosInfoCalculated_50() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_bPosInfoCalculated_50)); }
	inline bool get_m_bPosInfoCalculated_50() const { return ___m_bPosInfoCalculated_50; }
	inline bool* get_address_of_m_bPosInfoCalculated_50() { return &___m_bPosInfoCalculated_50; }
	inline void set_m_bPosInfoCalculated_50(bool value)
	{
		___m_bPosInfoCalculated_50 = value;
	}

	inline static int32_t get_offset_of_m_fShitTimeElapse_51() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_fShitTimeElapse_51)); }
	inline float get_m_fShitTimeElapse_51() const { return ___m_fShitTimeElapse_51; }
	inline float* get_address_of_m_fShitTimeElapse_51() { return &___m_fShitTimeElapse_51; }
	inline void set_m_fShitTimeElapse_51(float value)
	{
		___m_fShitTimeElapse_51 = value;
	}

	inline static int32_t get_offset_of_m_vecLastFrameBallsCenterMovement_53() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_vecLastFrameBallsCenterMovement_53)); }
	inline Vector3_t3722313464  get_m_vecLastFrameBallsCenterMovement_53() const { return ___m_vecLastFrameBallsCenterMovement_53; }
	inline Vector3_t3722313464 * get_address_of_m_vecLastFrameBallsCenterMovement_53() { return &___m_vecLastFrameBallsCenterMovement_53; }
	inline void set_m_vecLastFrameBallsCenterMovement_53(Vector3_t3722313464  value)
	{
		___m_vecLastFrameBallsCenterMovement_53 = value;
	}

	inline static int32_t get_offset_of_m_vecLastFrameBallsPostion_54() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_vecLastFrameBallsPostion_54)); }
	inline Vector3_t3722313464  get_m_vecLastFrameBallsPostion_54() const { return ___m_vecLastFrameBallsPostion_54; }
	inline Vector3_t3722313464 * get_address_of_m_vecLastFrameBallsPostion_54() { return &___m_vecLastFrameBallsPostion_54; }
	inline void set_m_vecLastFrameBallsPostion_54(Vector3_t3722313464  value)
	{
		___m_vecLastFrameBallsPostion_54 = value;
	}

	inline static int32_t get_offset_of_m_bFirstForBallsCenterMovement_55() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_bFirstForBallsCenterMovement_55)); }
	inline bool get_m_bFirstForBallsCenterMovement_55() const { return ___m_bFirstForBallsCenterMovement_55; }
	inline bool* get_address_of_m_bFirstForBallsCenterMovement_55() { return &___m_bFirstForBallsCenterMovement_55; }
	inline void set_m_bFirstForBallsCenterMovement_55(bool value)
	{
		___m_bFirstForBallsCenterMovement_55 = value;
	}

	inline static int32_t get_offset_of_m_fBallTeamRadius_56() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_fBallTeamRadius_56)); }
	inline float get_m_fBallTeamRadius_56() const { return ___m_fBallTeamRadius_56; }
	inline float* get_address_of_m_fBallTeamRadius_56() { return &___m_fBallTeamRadius_56; }
	inline void set_m_fBallTeamRadius_56(float value)
	{
		___m_fBallTeamRadius_56 = value;
	}

	inline static int32_t get_offset_of_vecLeftBottom_57() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___vecLeftBottom_57)); }
	inline Vector3_t3722313464  get_vecLeftBottom_57() const { return ___vecLeftBottom_57; }
	inline Vector3_t3722313464 * get_address_of_vecLeftBottom_57() { return &___vecLeftBottom_57; }
	inline void set_vecLeftBottom_57(Vector3_t3722313464  value)
	{
		___vecLeftBottom_57 = value;
	}

	inline static int32_t get_offset_of_vecRightTop_58() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___vecRightTop_58)); }
	inline Vector3_t3722313464  get_vecRightTop_58() const { return ___vecRightTop_58; }
	inline Vector3_t3722313464 * get_address_of_vecRightTop_58() { return &___vecRightTop_58; }
	inline void set_vecRightTop_58(Vector3_t3722313464  value)
	{
		___vecRightTop_58 = value;
	}

	inline static int32_t get_offset_of_m_lstMoveOrderX_59() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_lstMoveOrderX_59)); }
	inline List_1_t128053199 * get_m_lstMoveOrderX_59() const { return ___m_lstMoveOrderX_59; }
	inline List_1_t128053199 ** get_address_of_m_lstMoveOrderX_59() { return &___m_lstMoveOrderX_59; }
	inline void set_m_lstMoveOrderX_59(List_1_t128053199 * value)
	{
		___m_lstMoveOrderX_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMoveOrderX_59), value);
	}

	inline static int32_t get_offset_of_m_lstMoveOrderY_60() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ___m_lstMoveOrderY_60)); }
	inline List_1_t128053199 * get_m_lstMoveOrderY_60() const { return ___m_lstMoveOrderY_60; }
	inline List_1_t128053199 ** get_address_of_m_lstMoveOrderY_60() { return &___m_lstMoveOrderY_60; }
	inline void set_m_lstMoveOrderY_60(List_1_t128053199 * value)
	{
		___m_lstMoveOrderY_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMoveOrderY_60), value);
	}

	inline static int32_t get_offset_of__vecYaoGanDir_61() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511, ____vecYaoGanDir_61)); }
	inline Vector2_t2156229523  get__vecYaoGanDir_61() const { return ____vecYaoGanDir_61; }
	inline Vector2_t2156229523 * get_address_of__vecYaoGanDir_61() { return &____vecYaoGanDir_61; }
	inline void set__vecYaoGanDir_61(Vector2_t2156229523  value)
	{
		____vecYaoGanDir_61 = value;
	}
};

struct PlayerAction_t3782228511_StaticFields
{
public:
	// UnityEngine.Vector3 PlayerAction::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// PlayerAction PlayerAction::s_Instance
	PlayerAction_t3782228511 * ___s_Instance_33;
	// System.Int32 PlayerAction::s_nfLeftBallIndex
	int32_t ___s_nfLeftBallIndex_44;
	// System.Int32 PlayerAction::s_nRightBallIndex
	int32_t ___s_nRightBallIndex_45;
	// System.Int32 PlayerAction::s_nTopBallIndex
	int32_t ___s_nTopBallIndex_46;
	// System.Int32 PlayerAction::s_nBottomBallIndex
	int32_t ___s_nBottomBallIndex_47;
	// System.Int32 PlayerAction::s_nBiggestBallIndex
	int32_t ___s_nBiggestBallIndex_48;
	// UnityEngine.Vector2 PlayerAction::vecTempDir
	Vector2_t2156229523  ___vecTempDir_52;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_s_Instance_33() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___s_Instance_33)); }
	inline PlayerAction_t3782228511 * get_s_Instance_33() const { return ___s_Instance_33; }
	inline PlayerAction_t3782228511 ** get_address_of_s_Instance_33() { return &___s_Instance_33; }
	inline void set_s_Instance_33(PlayerAction_t3782228511 * value)
	{
		___s_Instance_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_33), value);
	}

	inline static int32_t get_offset_of_s_nfLeftBallIndex_44() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___s_nfLeftBallIndex_44)); }
	inline int32_t get_s_nfLeftBallIndex_44() const { return ___s_nfLeftBallIndex_44; }
	inline int32_t* get_address_of_s_nfLeftBallIndex_44() { return &___s_nfLeftBallIndex_44; }
	inline void set_s_nfLeftBallIndex_44(int32_t value)
	{
		___s_nfLeftBallIndex_44 = value;
	}

	inline static int32_t get_offset_of_s_nRightBallIndex_45() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___s_nRightBallIndex_45)); }
	inline int32_t get_s_nRightBallIndex_45() const { return ___s_nRightBallIndex_45; }
	inline int32_t* get_address_of_s_nRightBallIndex_45() { return &___s_nRightBallIndex_45; }
	inline void set_s_nRightBallIndex_45(int32_t value)
	{
		___s_nRightBallIndex_45 = value;
	}

	inline static int32_t get_offset_of_s_nTopBallIndex_46() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___s_nTopBallIndex_46)); }
	inline int32_t get_s_nTopBallIndex_46() const { return ___s_nTopBallIndex_46; }
	inline int32_t* get_address_of_s_nTopBallIndex_46() { return &___s_nTopBallIndex_46; }
	inline void set_s_nTopBallIndex_46(int32_t value)
	{
		___s_nTopBallIndex_46 = value;
	}

	inline static int32_t get_offset_of_s_nBottomBallIndex_47() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___s_nBottomBallIndex_47)); }
	inline int32_t get_s_nBottomBallIndex_47() const { return ___s_nBottomBallIndex_47; }
	inline int32_t* get_address_of_s_nBottomBallIndex_47() { return &___s_nBottomBallIndex_47; }
	inline void set_s_nBottomBallIndex_47(int32_t value)
	{
		___s_nBottomBallIndex_47 = value;
	}

	inline static int32_t get_offset_of_s_nBiggestBallIndex_48() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___s_nBiggestBallIndex_48)); }
	inline int32_t get_s_nBiggestBallIndex_48() const { return ___s_nBiggestBallIndex_48; }
	inline int32_t* get_address_of_s_nBiggestBallIndex_48() { return &___s_nBiggestBallIndex_48; }
	inline void set_s_nBiggestBallIndex_48(int32_t value)
	{
		___s_nBiggestBallIndex_48 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_52() { return static_cast<int32_t>(offsetof(PlayerAction_t3782228511_StaticFields, ___vecTempDir_52)); }
	inline Vector2_t2156229523  get_vecTempDir_52() const { return ___vecTempDir_52; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_52() { return &___vecTempDir_52; }
	inline void set_vecTempDir_52(Vector2_t2156229523  value)
	{
		___vecTempDir_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERACTION_T3782228511_H
#ifndef CARC_T1499038007_H
#define CARC_T1499038007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CArc
struct  CArc_t1499038007  : public MonoBehaviour_t3962482529
{
public:
	// Shapes2D.Shape[] CArc::m_aryArcs
	ShapeU5BU5D_t714462164* ___m_aryArcs_2;
	// System.Single CArc::m_fArcLength
	float ___m_fArcLength_3;
	// UnityEngine.GameObject CArc::_goArcsContianer
	GameObject_t1113636619 * ____goArcsContianer_4;
	// System.Int32 CArc::s_Shit
	int32_t ___s_Shit_6;

public:
	inline static int32_t get_offset_of_m_aryArcs_2() { return static_cast<int32_t>(offsetof(CArc_t1499038007, ___m_aryArcs_2)); }
	inline ShapeU5BU5D_t714462164* get_m_aryArcs_2() const { return ___m_aryArcs_2; }
	inline ShapeU5BU5D_t714462164** get_address_of_m_aryArcs_2() { return &___m_aryArcs_2; }
	inline void set_m_aryArcs_2(ShapeU5BU5D_t714462164* value)
	{
		___m_aryArcs_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryArcs_2), value);
	}

	inline static int32_t get_offset_of_m_fArcLength_3() { return static_cast<int32_t>(offsetof(CArc_t1499038007, ___m_fArcLength_3)); }
	inline float get_m_fArcLength_3() const { return ___m_fArcLength_3; }
	inline float* get_address_of_m_fArcLength_3() { return &___m_fArcLength_3; }
	inline void set_m_fArcLength_3(float value)
	{
		___m_fArcLength_3 = value;
	}

	inline static int32_t get_offset_of__goArcsContianer_4() { return static_cast<int32_t>(offsetof(CArc_t1499038007, ____goArcsContianer_4)); }
	inline GameObject_t1113636619 * get__goArcsContianer_4() const { return ____goArcsContianer_4; }
	inline GameObject_t1113636619 ** get_address_of__goArcsContianer_4() { return &____goArcsContianer_4; }
	inline void set__goArcsContianer_4(GameObject_t1113636619 * value)
	{
		____goArcsContianer_4 = value;
		Il2CppCodeGenWriteBarrier((&____goArcsContianer_4), value);
	}

	inline static int32_t get_offset_of_s_Shit_6() { return static_cast<int32_t>(offsetof(CArc_t1499038007, ___s_Shit_6)); }
	inline int32_t get_s_Shit_6() const { return ___s_Shit_6; }
	inline int32_t* get_address_of_s_Shit_6() { return &___s_Shit_6; }
	inline void set_s_Shit_6(int32_t value)
	{
		___s_Shit_6 = value;
	}
};

struct CArc_t1499038007_StaticFields
{
public:
	// UnityEngine.Vector3 CArc::vecTempScale
	Vector3_t3722313464  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(CArc_t1499038007_StaticFields, ___vecTempScale_5)); }
	inline Vector3_t3722313464  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_t3722313464  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARC_T1499038007_H
#ifndef UI_SHOWPAIHANGBANG_T4182849592_H
#define UI_SHOWPAIHANGBANG_T4182849592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_ShowPaiHangBang
struct  UI_ShowPaiHangBang_t4182849592  : public MonoBehaviour_t3962482529
{
public:
	// Main UI_ShowPaiHangBang::m_Main
	Main_t2227614074 * ___m_Main_2;

public:
	inline static int32_t get_offset_of_m_Main_2() { return static_cast<int32_t>(offsetof(UI_ShowPaiHangBang_t4182849592, ___m_Main_2)); }
	inline Main_t2227614074 * get_m_Main_2() const { return ___m_Main_2; }
	inline Main_t2227614074 ** get_address_of_m_Main_2() { return &___m_Main_2; }
	inline void set_m_Main_2(Main_t2227614074 * value)
	{
		___m_Main_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Main_2), value);
	}
};

struct UI_ShowPaiHangBang_t4182849592_StaticFields
{
public:
	// System.Boolean UI_ShowPaiHangBang::s_bUsingUi
	bool ___s_bUsingUi_3;

public:
	inline static int32_t get_offset_of_s_bUsingUi_3() { return static_cast<int32_t>(offsetof(UI_ShowPaiHangBang_t4182849592_StaticFields, ___s_bUsingUi_3)); }
	inline bool get_s_bUsingUi_3() const { return ___s_bUsingUi_3; }
	inline bool* get_address_of_s_bUsingUi_3() { return &___s_bUsingUi_3; }
	inline void set_s_bUsingUi_3(bool value)
	{
		___s_bUsingUi_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_SHOWPAIHANGBANG_T4182849592_H
#ifndef CSELECTSKILL_T3600552377_H
#define CSELECTSKILL_T3600552377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSelectSkill
struct  CSelectSkill_t3600552377  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text[] CSelectSkill::m_aryPlayerNameForSkillSelectPanel
	TextU5BU5D_t422084607* ___m_aryPlayerNameForSkillSelectPanel_2;
	// System.Single CSelectSkill::m_fSelectedScale
	float ___m_fSelectedScale_3;
	// UnityEngine.UI.Text[] CSelectSkill::_aryTitle
	TextU5BU5D_t422084607* ____aryTitle_5;
	// UnityEngine.UI.Text[] CSelectSkill::_aryTitle_Others
	TextU5BU5D_t422084607* ____aryTitle_Others_6;
	// UnityEngine.UI.Text[] CSelectSkill::_aryValuesLevel0
	TextU5BU5D_t422084607* ____aryValuesLevel0_7;
	// UnityEngine.UI.Text[] CSelectSkill::_aryValuesLevel0_Others
	TextU5BU5D_t422084607* ____aryValuesLevel0_Others_8;
	// UnityEngine.UI.Text[] CSelectSkill::_aryValuesLevel1
	TextU5BU5D_t422084607* ____aryValuesLevel1_9;
	// UnityEngine.UI.Text[] CSelectSkill::_aryValuesLevel1_Others
	TextU5BU5D_t422084607* ____aryValuesLevel1_Others_10;
	// UnityEngine.UI.Text[] CSelectSkill::_aryValuesLevel2
	TextU5BU5D_t422084607* ____aryValuesLevel2_11;
	// UnityEngine.UI.Text[] CSelectSkill::_aryValuesLevel2_Others
	TextU5BU5D_t422084607* ____aryValuesLevel2_Others_12;
	// System.Collections.Generic.List`1<UnityEngine.UI.Text[]> CSelectSkill::m_lstValuesLevels
	List_1_t1894159349 * ___m_lstValuesLevels_13;
	// UnityEngine.UI.Button[] CSelectSkill::m_arySkillType
	ButtonU5BU5D_t2297175928* ___m_arySkillType_14;
	// UnityEngine.UI.Button[] CSelectSkill::m_arySkillType_Others
	ButtonU5BU5D_t2297175928* ___m_arySkillType_Others_15;
	// UnityEngine.UI.Text CSelectSkill::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_16;
	// UnityEngine.GameObject CSelectSkill::_panelSkillDetail
	GameObject_t1113636619 * ____panelSkillDetail_17;
	// UnityEngine.GameObject CSelectSkill::_panelSelectSkill
	GameObject_t1113636619 * ____panelSelectSkill_18;
	// UnityEngine.GameObject CSelectSkill::_panelSelectGameModePage
	GameObject_t1113636619 * ____panelSelectGameModePage_19;
	// UnityEngine.UI.Button CSelectSkill::_btnQianXing
	Button_t4055032469 * ____btnQianXing_20;
	// UnityEngine.UI.Button CSelectSkill::_btnBianCi
	Button_t4055032469 * ____btnBianCi_21;
	// UnityEngine.UI.Button CSelectSkill::_btnYanMie
	Button_t4055032469 * ____btnYanMie_22;
	// UnityEngine.UI.Button CSelectSkill::_btnMoDun
	Button_t4055032469 * ____btnMoDun_23;
	// UnityEngine.UI.Button CSelectSkill::_btnMiaoHe
	Button_t4055032469 * ____btnMiaoHe_24;
	// UnityEngine.UI.Button CSelectSkill::_btnKuangBao
	Button_t4055032469 * ____btnKuangBao_25;
	// UnityEngine.UI.Button CSelectSkill::_btnJinKe
	Button_t4055032469 * ____btnJinKe_26;
	// UnityEngine.UI.Text CSelectSkill::_txtSkillName
	Text_t1901882714 * ____txtSkillName_27;
	// UnityEngine.UI.Text[] CSelectSkill::m_arySkillName
	TextU5BU5D_t422084607* ___m_arySkillName_28;
	// UnityEngine.UI.Text CSelectSkill::_txtSkillDesc
	Text_t1901882714 * ____txtSkillDesc_29;
	// UnityEngine.UI.Text[] CSelectSkill::m_arySkillDesc
	TextU5BU5D_t422084607* ___m_arySkillDesc_30;
	// UnityEngine.UI.Button CSelectSkill::_btnPlayGame
	Button_t4055032469 * ____btnPlayGame_31;
	// UnityEngine.UI.Button CSelectSkill::_btnReturnLastPage
	Button_t4055032469 * ____btnReturnLastPage_32;
	// UnityEngine.UI.Image CSelectSkill::_imgSkillIcon
	Image_t2670269651 * ____imgSkillIcon_33;
	// UnityEngine.UI.Image[] CSelectSkill::m_arySkillIcon
	ImageU5BU5D_t2439009922* ___m_arySkillIcon_34;
	// CSkillSystem/eSkillId CSelectSkill::m_eCurSelectSkillId
	int32_t ___m_eCurSelectSkillId_35;
	// CSkillDetailGrid[] CSelectSkill::m_arySkillDetailGrid
	CSkillDetailGridU5BU5D_t173934927* ___m_arySkillDetailGrid_36;
	// UnityEngine.Sprite[] CSelectSkill::m_arySkillIconSprite
	SpriteU5BU5D_t2581906349* ___m_arySkillIconSprite_37;
	// System.Boolean CSelectSkill::m_bDefaultSkillSelected
	bool ___m_bDefaultSkillSelected_38;
	// System.Boolean CSelectSkill::m_bMapParsed
	bool ___m_bMapParsed_39;
	// System.Collections.Generic.Dictionary`2<System.Int32,CSelectSkill/sSkillDetail>[] CSelectSkill::m_arySkillDetail
	Dictionary_2U5BU5D_t883166233* ___m_arySkillDetail_40;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> CSelectSkill::m_dicSkillDesc
	Dictionary_2_t736164020 * ___m_dicSkillDesc_41;

public:
	inline static int32_t get_offset_of_m_aryPlayerNameForSkillSelectPanel_2() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_aryPlayerNameForSkillSelectPanel_2)); }
	inline TextU5BU5D_t422084607* get_m_aryPlayerNameForSkillSelectPanel_2() const { return ___m_aryPlayerNameForSkillSelectPanel_2; }
	inline TextU5BU5D_t422084607** get_address_of_m_aryPlayerNameForSkillSelectPanel_2() { return &___m_aryPlayerNameForSkillSelectPanel_2; }
	inline void set_m_aryPlayerNameForSkillSelectPanel_2(TextU5BU5D_t422084607* value)
	{
		___m_aryPlayerNameForSkillSelectPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlayerNameForSkillSelectPanel_2), value);
	}

	inline static int32_t get_offset_of_m_fSelectedScale_3() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_fSelectedScale_3)); }
	inline float get_m_fSelectedScale_3() const { return ___m_fSelectedScale_3; }
	inline float* get_address_of_m_fSelectedScale_3() { return &___m_fSelectedScale_3; }
	inline void set_m_fSelectedScale_3(float value)
	{
		___m_fSelectedScale_3 = value;
	}

	inline static int32_t get_offset_of__aryTitle_5() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryTitle_5)); }
	inline TextU5BU5D_t422084607* get__aryTitle_5() const { return ____aryTitle_5; }
	inline TextU5BU5D_t422084607** get_address_of__aryTitle_5() { return &____aryTitle_5; }
	inline void set__aryTitle_5(TextU5BU5D_t422084607* value)
	{
		____aryTitle_5 = value;
		Il2CppCodeGenWriteBarrier((&____aryTitle_5), value);
	}

	inline static int32_t get_offset_of__aryTitle_Others_6() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryTitle_Others_6)); }
	inline TextU5BU5D_t422084607* get__aryTitle_Others_6() const { return ____aryTitle_Others_6; }
	inline TextU5BU5D_t422084607** get_address_of__aryTitle_Others_6() { return &____aryTitle_Others_6; }
	inline void set__aryTitle_Others_6(TextU5BU5D_t422084607* value)
	{
		____aryTitle_Others_6 = value;
		Il2CppCodeGenWriteBarrier((&____aryTitle_Others_6), value);
	}

	inline static int32_t get_offset_of__aryValuesLevel0_7() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryValuesLevel0_7)); }
	inline TextU5BU5D_t422084607* get__aryValuesLevel0_7() const { return ____aryValuesLevel0_7; }
	inline TextU5BU5D_t422084607** get_address_of__aryValuesLevel0_7() { return &____aryValuesLevel0_7; }
	inline void set__aryValuesLevel0_7(TextU5BU5D_t422084607* value)
	{
		____aryValuesLevel0_7 = value;
		Il2CppCodeGenWriteBarrier((&____aryValuesLevel0_7), value);
	}

	inline static int32_t get_offset_of__aryValuesLevel0_Others_8() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryValuesLevel0_Others_8)); }
	inline TextU5BU5D_t422084607* get__aryValuesLevel0_Others_8() const { return ____aryValuesLevel0_Others_8; }
	inline TextU5BU5D_t422084607** get_address_of__aryValuesLevel0_Others_8() { return &____aryValuesLevel0_Others_8; }
	inline void set__aryValuesLevel0_Others_8(TextU5BU5D_t422084607* value)
	{
		____aryValuesLevel0_Others_8 = value;
		Il2CppCodeGenWriteBarrier((&____aryValuesLevel0_Others_8), value);
	}

	inline static int32_t get_offset_of__aryValuesLevel1_9() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryValuesLevel1_9)); }
	inline TextU5BU5D_t422084607* get__aryValuesLevel1_9() const { return ____aryValuesLevel1_9; }
	inline TextU5BU5D_t422084607** get_address_of__aryValuesLevel1_9() { return &____aryValuesLevel1_9; }
	inline void set__aryValuesLevel1_9(TextU5BU5D_t422084607* value)
	{
		____aryValuesLevel1_9 = value;
		Il2CppCodeGenWriteBarrier((&____aryValuesLevel1_9), value);
	}

	inline static int32_t get_offset_of__aryValuesLevel1_Others_10() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryValuesLevel1_Others_10)); }
	inline TextU5BU5D_t422084607* get__aryValuesLevel1_Others_10() const { return ____aryValuesLevel1_Others_10; }
	inline TextU5BU5D_t422084607** get_address_of__aryValuesLevel1_Others_10() { return &____aryValuesLevel1_Others_10; }
	inline void set__aryValuesLevel1_Others_10(TextU5BU5D_t422084607* value)
	{
		____aryValuesLevel1_Others_10 = value;
		Il2CppCodeGenWriteBarrier((&____aryValuesLevel1_Others_10), value);
	}

	inline static int32_t get_offset_of__aryValuesLevel2_11() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryValuesLevel2_11)); }
	inline TextU5BU5D_t422084607* get__aryValuesLevel2_11() const { return ____aryValuesLevel2_11; }
	inline TextU5BU5D_t422084607** get_address_of__aryValuesLevel2_11() { return &____aryValuesLevel2_11; }
	inline void set__aryValuesLevel2_11(TextU5BU5D_t422084607* value)
	{
		____aryValuesLevel2_11 = value;
		Il2CppCodeGenWriteBarrier((&____aryValuesLevel2_11), value);
	}

	inline static int32_t get_offset_of__aryValuesLevel2_Others_12() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____aryValuesLevel2_Others_12)); }
	inline TextU5BU5D_t422084607* get__aryValuesLevel2_Others_12() const { return ____aryValuesLevel2_Others_12; }
	inline TextU5BU5D_t422084607** get_address_of__aryValuesLevel2_Others_12() { return &____aryValuesLevel2_Others_12; }
	inline void set__aryValuesLevel2_Others_12(TextU5BU5D_t422084607* value)
	{
		____aryValuesLevel2_Others_12 = value;
		Il2CppCodeGenWriteBarrier((&____aryValuesLevel2_Others_12), value);
	}

	inline static int32_t get_offset_of_m_lstValuesLevels_13() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_lstValuesLevels_13)); }
	inline List_1_t1894159349 * get_m_lstValuesLevels_13() const { return ___m_lstValuesLevels_13; }
	inline List_1_t1894159349 ** get_address_of_m_lstValuesLevels_13() { return &___m_lstValuesLevels_13; }
	inline void set_m_lstValuesLevels_13(List_1_t1894159349 * value)
	{
		___m_lstValuesLevels_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstValuesLevels_13), value);
	}

	inline static int32_t get_offset_of_m_arySkillType_14() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillType_14)); }
	inline ButtonU5BU5D_t2297175928* get_m_arySkillType_14() const { return ___m_arySkillType_14; }
	inline ButtonU5BU5D_t2297175928** get_address_of_m_arySkillType_14() { return &___m_arySkillType_14; }
	inline void set_m_arySkillType_14(ButtonU5BU5D_t2297175928* value)
	{
		___m_arySkillType_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillType_14), value);
	}

	inline static int32_t get_offset_of_m_arySkillType_Others_15() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillType_Others_15)); }
	inline ButtonU5BU5D_t2297175928* get_m_arySkillType_Others_15() const { return ___m_arySkillType_Others_15; }
	inline ButtonU5BU5D_t2297175928** get_address_of_m_arySkillType_Others_15() { return &___m_arySkillType_Others_15; }
	inline void set_m_arySkillType_Others_15(ButtonU5BU5D_t2297175928* value)
	{
		___m_arySkillType_Others_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillType_Others_15), value);
	}

	inline static int32_t get_offset_of__txtPlayerName_16() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____txtPlayerName_16)); }
	inline Text_t1901882714 * get__txtPlayerName_16() const { return ____txtPlayerName_16; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_16() { return &____txtPlayerName_16; }
	inline void set__txtPlayerName_16(Text_t1901882714 * value)
	{
		____txtPlayerName_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_16), value);
	}

	inline static int32_t get_offset_of__panelSkillDetail_17() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____panelSkillDetail_17)); }
	inline GameObject_t1113636619 * get__panelSkillDetail_17() const { return ____panelSkillDetail_17; }
	inline GameObject_t1113636619 ** get_address_of__panelSkillDetail_17() { return &____panelSkillDetail_17; }
	inline void set__panelSkillDetail_17(GameObject_t1113636619 * value)
	{
		____panelSkillDetail_17 = value;
		Il2CppCodeGenWriteBarrier((&____panelSkillDetail_17), value);
	}

	inline static int32_t get_offset_of__panelSelectSkill_18() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____panelSelectSkill_18)); }
	inline GameObject_t1113636619 * get__panelSelectSkill_18() const { return ____panelSelectSkill_18; }
	inline GameObject_t1113636619 ** get_address_of__panelSelectSkill_18() { return &____panelSelectSkill_18; }
	inline void set__panelSelectSkill_18(GameObject_t1113636619 * value)
	{
		____panelSelectSkill_18 = value;
		Il2CppCodeGenWriteBarrier((&____panelSelectSkill_18), value);
	}

	inline static int32_t get_offset_of__panelSelectGameModePage_19() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____panelSelectGameModePage_19)); }
	inline GameObject_t1113636619 * get__panelSelectGameModePage_19() const { return ____panelSelectGameModePage_19; }
	inline GameObject_t1113636619 ** get_address_of__panelSelectGameModePage_19() { return &____panelSelectGameModePage_19; }
	inline void set__panelSelectGameModePage_19(GameObject_t1113636619 * value)
	{
		____panelSelectGameModePage_19 = value;
		Il2CppCodeGenWriteBarrier((&____panelSelectGameModePage_19), value);
	}

	inline static int32_t get_offset_of__btnQianXing_20() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnQianXing_20)); }
	inline Button_t4055032469 * get__btnQianXing_20() const { return ____btnQianXing_20; }
	inline Button_t4055032469 ** get_address_of__btnQianXing_20() { return &____btnQianXing_20; }
	inline void set__btnQianXing_20(Button_t4055032469 * value)
	{
		____btnQianXing_20 = value;
		Il2CppCodeGenWriteBarrier((&____btnQianXing_20), value);
	}

	inline static int32_t get_offset_of__btnBianCi_21() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnBianCi_21)); }
	inline Button_t4055032469 * get__btnBianCi_21() const { return ____btnBianCi_21; }
	inline Button_t4055032469 ** get_address_of__btnBianCi_21() { return &____btnBianCi_21; }
	inline void set__btnBianCi_21(Button_t4055032469 * value)
	{
		____btnBianCi_21 = value;
		Il2CppCodeGenWriteBarrier((&____btnBianCi_21), value);
	}

	inline static int32_t get_offset_of__btnYanMie_22() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnYanMie_22)); }
	inline Button_t4055032469 * get__btnYanMie_22() const { return ____btnYanMie_22; }
	inline Button_t4055032469 ** get_address_of__btnYanMie_22() { return &____btnYanMie_22; }
	inline void set__btnYanMie_22(Button_t4055032469 * value)
	{
		____btnYanMie_22 = value;
		Il2CppCodeGenWriteBarrier((&____btnYanMie_22), value);
	}

	inline static int32_t get_offset_of__btnMoDun_23() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnMoDun_23)); }
	inline Button_t4055032469 * get__btnMoDun_23() const { return ____btnMoDun_23; }
	inline Button_t4055032469 ** get_address_of__btnMoDun_23() { return &____btnMoDun_23; }
	inline void set__btnMoDun_23(Button_t4055032469 * value)
	{
		____btnMoDun_23 = value;
		Il2CppCodeGenWriteBarrier((&____btnMoDun_23), value);
	}

	inline static int32_t get_offset_of__btnMiaoHe_24() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnMiaoHe_24)); }
	inline Button_t4055032469 * get__btnMiaoHe_24() const { return ____btnMiaoHe_24; }
	inline Button_t4055032469 ** get_address_of__btnMiaoHe_24() { return &____btnMiaoHe_24; }
	inline void set__btnMiaoHe_24(Button_t4055032469 * value)
	{
		____btnMiaoHe_24 = value;
		Il2CppCodeGenWriteBarrier((&____btnMiaoHe_24), value);
	}

	inline static int32_t get_offset_of__btnKuangBao_25() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnKuangBao_25)); }
	inline Button_t4055032469 * get__btnKuangBao_25() const { return ____btnKuangBao_25; }
	inline Button_t4055032469 ** get_address_of__btnKuangBao_25() { return &____btnKuangBao_25; }
	inline void set__btnKuangBao_25(Button_t4055032469 * value)
	{
		____btnKuangBao_25 = value;
		Il2CppCodeGenWriteBarrier((&____btnKuangBao_25), value);
	}

	inline static int32_t get_offset_of__btnJinKe_26() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnJinKe_26)); }
	inline Button_t4055032469 * get__btnJinKe_26() const { return ____btnJinKe_26; }
	inline Button_t4055032469 ** get_address_of__btnJinKe_26() { return &____btnJinKe_26; }
	inline void set__btnJinKe_26(Button_t4055032469 * value)
	{
		____btnJinKe_26 = value;
		Il2CppCodeGenWriteBarrier((&____btnJinKe_26), value);
	}

	inline static int32_t get_offset_of__txtSkillName_27() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____txtSkillName_27)); }
	inline Text_t1901882714 * get__txtSkillName_27() const { return ____txtSkillName_27; }
	inline Text_t1901882714 ** get_address_of__txtSkillName_27() { return &____txtSkillName_27; }
	inline void set__txtSkillName_27(Text_t1901882714 * value)
	{
		____txtSkillName_27 = value;
		Il2CppCodeGenWriteBarrier((&____txtSkillName_27), value);
	}

	inline static int32_t get_offset_of_m_arySkillName_28() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillName_28)); }
	inline TextU5BU5D_t422084607* get_m_arySkillName_28() const { return ___m_arySkillName_28; }
	inline TextU5BU5D_t422084607** get_address_of_m_arySkillName_28() { return &___m_arySkillName_28; }
	inline void set_m_arySkillName_28(TextU5BU5D_t422084607* value)
	{
		___m_arySkillName_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillName_28), value);
	}

	inline static int32_t get_offset_of__txtSkillDesc_29() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____txtSkillDesc_29)); }
	inline Text_t1901882714 * get__txtSkillDesc_29() const { return ____txtSkillDesc_29; }
	inline Text_t1901882714 ** get_address_of__txtSkillDesc_29() { return &____txtSkillDesc_29; }
	inline void set__txtSkillDesc_29(Text_t1901882714 * value)
	{
		____txtSkillDesc_29 = value;
		Il2CppCodeGenWriteBarrier((&____txtSkillDesc_29), value);
	}

	inline static int32_t get_offset_of_m_arySkillDesc_30() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillDesc_30)); }
	inline TextU5BU5D_t422084607* get_m_arySkillDesc_30() const { return ___m_arySkillDesc_30; }
	inline TextU5BU5D_t422084607** get_address_of_m_arySkillDesc_30() { return &___m_arySkillDesc_30; }
	inline void set_m_arySkillDesc_30(TextU5BU5D_t422084607* value)
	{
		___m_arySkillDesc_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillDesc_30), value);
	}

	inline static int32_t get_offset_of__btnPlayGame_31() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnPlayGame_31)); }
	inline Button_t4055032469 * get__btnPlayGame_31() const { return ____btnPlayGame_31; }
	inline Button_t4055032469 ** get_address_of__btnPlayGame_31() { return &____btnPlayGame_31; }
	inline void set__btnPlayGame_31(Button_t4055032469 * value)
	{
		____btnPlayGame_31 = value;
		Il2CppCodeGenWriteBarrier((&____btnPlayGame_31), value);
	}

	inline static int32_t get_offset_of__btnReturnLastPage_32() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____btnReturnLastPage_32)); }
	inline Button_t4055032469 * get__btnReturnLastPage_32() const { return ____btnReturnLastPage_32; }
	inline Button_t4055032469 ** get_address_of__btnReturnLastPage_32() { return &____btnReturnLastPage_32; }
	inline void set__btnReturnLastPage_32(Button_t4055032469 * value)
	{
		____btnReturnLastPage_32 = value;
		Il2CppCodeGenWriteBarrier((&____btnReturnLastPage_32), value);
	}

	inline static int32_t get_offset_of__imgSkillIcon_33() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ____imgSkillIcon_33)); }
	inline Image_t2670269651 * get__imgSkillIcon_33() const { return ____imgSkillIcon_33; }
	inline Image_t2670269651 ** get_address_of__imgSkillIcon_33() { return &____imgSkillIcon_33; }
	inline void set__imgSkillIcon_33(Image_t2670269651 * value)
	{
		____imgSkillIcon_33 = value;
		Il2CppCodeGenWriteBarrier((&____imgSkillIcon_33), value);
	}

	inline static int32_t get_offset_of_m_arySkillIcon_34() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillIcon_34)); }
	inline ImageU5BU5D_t2439009922* get_m_arySkillIcon_34() const { return ___m_arySkillIcon_34; }
	inline ImageU5BU5D_t2439009922** get_address_of_m_arySkillIcon_34() { return &___m_arySkillIcon_34; }
	inline void set_m_arySkillIcon_34(ImageU5BU5D_t2439009922* value)
	{
		___m_arySkillIcon_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillIcon_34), value);
	}

	inline static int32_t get_offset_of_m_eCurSelectSkillId_35() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_eCurSelectSkillId_35)); }
	inline int32_t get_m_eCurSelectSkillId_35() const { return ___m_eCurSelectSkillId_35; }
	inline int32_t* get_address_of_m_eCurSelectSkillId_35() { return &___m_eCurSelectSkillId_35; }
	inline void set_m_eCurSelectSkillId_35(int32_t value)
	{
		___m_eCurSelectSkillId_35 = value;
	}

	inline static int32_t get_offset_of_m_arySkillDetailGrid_36() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillDetailGrid_36)); }
	inline CSkillDetailGridU5BU5D_t173934927* get_m_arySkillDetailGrid_36() const { return ___m_arySkillDetailGrid_36; }
	inline CSkillDetailGridU5BU5D_t173934927** get_address_of_m_arySkillDetailGrid_36() { return &___m_arySkillDetailGrid_36; }
	inline void set_m_arySkillDetailGrid_36(CSkillDetailGridU5BU5D_t173934927* value)
	{
		___m_arySkillDetailGrid_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillDetailGrid_36), value);
	}

	inline static int32_t get_offset_of_m_arySkillIconSprite_37() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillIconSprite_37)); }
	inline SpriteU5BU5D_t2581906349* get_m_arySkillIconSprite_37() const { return ___m_arySkillIconSprite_37; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_arySkillIconSprite_37() { return &___m_arySkillIconSprite_37; }
	inline void set_m_arySkillIconSprite_37(SpriteU5BU5D_t2581906349* value)
	{
		___m_arySkillIconSprite_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillIconSprite_37), value);
	}

	inline static int32_t get_offset_of_m_bDefaultSkillSelected_38() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_bDefaultSkillSelected_38)); }
	inline bool get_m_bDefaultSkillSelected_38() const { return ___m_bDefaultSkillSelected_38; }
	inline bool* get_address_of_m_bDefaultSkillSelected_38() { return &___m_bDefaultSkillSelected_38; }
	inline void set_m_bDefaultSkillSelected_38(bool value)
	{
		___m_bDefaultSkillSelected_38 = value;
	}

	inline static int32_t get_offset_of_m_bMapParsed_39() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_bMapParsed_39)); }
	inline bool get_m_bMapParsed_39() const { return ___m_bMapParsed_39; }
	inline bool* get_address_of_m_bMapParsed_39() { return &___m_bMapParsed_39; }
	inline void set_m_bMapParsed_39(bool value)
	{
		___m_bMapParsed_39 = value;
	}

	inline static int32_t get_offset_of_m_arySkillDetail_40() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_arySkillDetail_40)); }
	inline Dictionary_2U5BU5D_t883166233* get_m_arySkillDetail_40() const { return ___m_arySkillDetail_40; }
	inline Dictionary_2U5BU5D_t883166233** get_address_of_m_arySkillDetail_40() { return &___m_arySkillDetail_40; }
	inline void set_m_arySkillDetail_40(Dictionary_2U5BU5D_t883166233* value)
	{
		___m_arySkillDetail_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillDetail_40), value);
	}

	inline static int32_t get_offset_of_m_dicSkillDesc_41() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377, ___m_dicSkillDesc_41)); }
	inline Dictionary_2_t736164020 * get_m_dicSkillDesc_41() const { return ___m_dicSkillDesc_41; }
	inline Dictionary_2_t736164020 ** get_address_of_m_dicSkillDesc_41() { return &___m_dicSkillDesc_41; }
	inline void set_m_dicSkillDesc_41(Dictionary_2_t736164020 * value)
	{
		___m_dicSkillDesc_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkillDesc_41), value);
	}
};

struct CSelectSkill_t3600552377_StaticFields
{
public:
	// CSelectSkill CSelectSkill::s_Instance
	CSelectSkill_t3600552377 * ___s_Instance_4;
	// UnityEngine.Color CSelectSkill::colorSkillButtonLight
	Color_t2555686324  ___colorSkillButtonLight_42;
	// UnityEngine.Color CSelectSkill::colorSkillButtonDark
	Color_t2555686324  ___colorSkillButtonDark_43;
	// UnityEngine.Vector3 CSelectSkill::vecTempScale
	Vector3_t3722313464  ___vecTempScale_44;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377_StaticFields, ___s_Instance_4)); }
	inline CSelectSkill_t3600552377 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CSelectSkill_t3600552377 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CSelectSkill_t3600552377 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_colorSkillButtonLight_42() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377_StaticFields, ___colorSkillButtonLight_42)); }
	inline Color_t2555686324  get_colorSkillButtonLight_42() const { return ___colorSkillButtonLight_42; }
	inline Color_t2555686324 * get_address_of_colorSkillButtonLight_42() { return &___colorSkillButtonLight_42; }
	inline void set_colorSkillButtonLight_42(Color_t2555686324  value)
	{
		___colorSkillButtonLight_42 = value;
	}

	inline static int32_t get_offset_of_colorSkillButtonDark_43() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377_StaticFields, ___colorSkillButtonDark_43)); }
	inline Color_t2555686324  get_colorSkillButtonDark_43() const { return ___colorSkillButtonDark_43; }
	inline Color_t2555686324 * get_address_of_colorSkillButtonDark_43() { return &___colorSkillButtonDark_43; }
	inline void set_colorSkillButtonDark_43(Color_t2555686324  value)
	{
		___colorSkillButtonDark_43 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_44() { return static_cast<int32_t>(offsetof(CSelectSkill_t3600552377_StaticFields, ___vecTempScale_44)); }
	inline Vector3_t3722313464  get_vecTempScale_44() const { return ___vecTempScale_44; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_44() { return &___vecTempScale_44; }
	inline void set_vecTempScale_44(Vector3_t3722313464  value)
	{
		___vecTempScale_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSELECTSKILL_T3600552377_H
#ifndef CROBOTMANAGER_T2137154153_H
#define CROBOTMANAGER_T2137154153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRobotManager
struct  CRobotManager_t2137154153  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CRobotManager_t2137154153_StaticFields
{
public:
	// CRobotManager CRobotManager::s_Instance
	CRobotManager_t2137154153 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CRobotManager_t2137154153_StaticFields, ___s_Instance_2)); }
	inline CRobotManager_t2137154153 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CRobotManager_t2137154153 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CRobotManager_t2137154153 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROBOTMANAGER_T2137154153_H
#ifndef CMONEYSYSTEM_T1851651907_H
#define CMONEYSYSTEM_T1851651907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMoneySystem
struct  CMoneySystem_t1851651907  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CMoneySystem::_txtMoney
	Text_t1901882714 * ____txtMoney_3;
	// System.Single CMoneySystem::m_fMoney
	float ___m_fMoney_4;
	// System.Single CMoneySystem::m_fCurShowValue
	float ___m_fCurShowValue_5;
	// System.Single CMoneySystem::m_fDelta
	float ___m_fDelta_6;
	// UnityEngine.Sprite[] CMoneySystem::m_aryLiuGuangSprite
	SpriteU5BU5D_t2581906349* ___m_aryLiuGuangSprite_7;
	// System.Single CMoneySystem::m_fLiuGuangInterval
	float ___m_fLiuGuangInterval_8;
	// System.Single CMoneySystem::m_fLiuGuangSpeed
	float ___m_fLiuGuangSpeed_9;
	// CFrameAnimationEffect CMoneySystem::_effectLiuGuang
	CFrameAnimationEffect_t443605508 * ____effectLiuGuang_10;
	// System.Boolean CMoneySystem::m_bMoneyChanged
	bool ___m_bMoneyChanged_11;

public:
	inline static int32_t get_offset_of__txtMoney_3() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ____txtMoney_3)); }
	inline Text_t1901882714 * get__txtMoney_3() const { return ____txtMoney_3; }
	inline Text_t1901882714 ** get_address_of__txtMoney_3() { return &____txtMoney_3; }
	inline void set__txtMoney_3(Text_t1901882714 * value)
	{
		____txtMoney_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtMoney_3), value);
	}

	inline static int32_t get_offset_of_m_fMoney_4() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_fMoney_4)); }
	inline float get_m_fMoney_4() const { return ___m_fMoney_4; }
	inline float* get_address_of_m_fMoney_4() { return &___m_fMoney_4; }
	inline void set_m_fMoney_4(float value)
	{
		___m_fMoney_4 = value;
	}

	inline static int32_t get_offset_of_m_fCurShowValue_5() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_fCurShowValue_5)); }
	inline float get_m_fCurShowValue_5() const { return ___m_fCurShowValue_5; }
	inline float* get_address_of_m_fCurShowValue_5() { return &___m_fCurShowValue_5; }
	inline void set_m_fCurShowValue_5(float value)
	{
		___m_fCurShowValue_5 = value;
	}

	inline static int32_t get_offset_of_m_fDelta_6() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_fDelta_6)); }
	inline float get_m_fDelta_6() const { return ___m_fDelta_6; }
	inline float* get_address_of_m_fDelta_6() { return &___m_fDelta_6; }
	inline void set_m_fDelta_6(float value)
	{
		___m_fDelta_6 = value;
	}

	inline static int32_t get_offset_of_m_aryLiuGuangSprite_7() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_aryLiuGuangSprite_7)); }
	inline SpriteU5BU5D_t2581906349* get_m_aryLiuGuangSprite_7() const { return ___m_aryLiuGuangSprite_7; }
	inline SpriteU5BU5D_t2581906349** get_address_of_m_aryLiuGuangSprite_7() { return &___m_aryLiuGuangSprite_7; }
	inline void set_m_aryLiuGuangSprite_7(SpriteU5BU5D_t2581906349* value)
	{
		___m_aryLiuGuangSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLiuGuangSprite_7), value);
	}

	inline static int32_t get_offset_of_m_fLiuGuangInterval_8() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_fLiuGuangInterval_8)); }
	inline float get_m_fLiuGuangInterval_8() const { return ___m_fLiuGuangInterval_8; }
	inline float* get_address_of_m_fLiuGuangInterval_8() { return &___m_fLiuGuangInterval_8; }
	inline void set_m_fLiuGuangInterval_8(float value)
	{
		___m_fLiuGuangInterval_8 = value;
	}

	inline static int32_t get_offset_of_m_fLiuGuangSpeed_9() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_fLiuGuangSpeed_9)); }
	inline float get_m_fLiuGuangSpeed_9() const { return ___m_fLiuGuangSpeed_9; }
	inline float* get_address_of_m_fLiuGuangSpeed_9() { return &___m_fLiuGuangSpeed_9; }
	inline void set_m_fLiuGuangSpeed_9(float value)
	{
		___m_fLiuGuangSpeed_9 = value;
	}

	inline static int32_t get_offset_of__effectLiuGuang_10() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ____effectLiuGuang_10)); }
	inline CFrameAnimationEffect_t443605508 * get__effectLiuGuang_10() const { return ____effectLiuGuang_10; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectLiuGuang_10() { return &____effectLiuGuang_10; }
	inline void set__effectLiuGuang_10(CFrameAnimationEffect_t443605508 * value)
	{
		____effectLiuGuang_10 = value;
		Il2CppCodeGenWriteBarrier((&____effectLiuGuang_10), value);
	}

	inline static int32_t get_offset_of_m_bMoneyChanged_11() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907, ___m_bMoneyChanged_11)); }
	inline bool get_m_bMoneyChanged_11() const { return ___m_bMoneyChanged_11; }
	inline bool* get_address_of_m_bMoneyChanged_11() { return &___m_bMoneyChanged_11; }
	inline void set_m_bMoneyChanged_11(bool value)
	{
		___m_bMoneyChanged_11 = value;
	}
};

struct CMoneySystem_t1851651907_StaticFields
{
public:
	// CMoneySystem CMoneySystem::s_Instance
	CMoneySystem_t1851651907 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CMoneySystem_t1851651907_StaticFields, ___s_Instance_2)); }
	inline CMoneySystem_t1851651907 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CMoneySystem_t1851651907 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CMoneySystem_t1851651907 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMONEYSYSTEM_T1851651907_H
#ifndef CREBORNSPOT_T259671536_H
#define CREBORNSPOT_T259671536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRebornSpot
struct  CRebornSpot_t259671536  : public MonoBehaviour_t3962482529
{
public:
	// Shapes2D.Shape CRebornSpot::_shapeInner
	Shape_t3950352489 * ____shapeInner_5;
	// Shapes2D.Shape CRebornSpot::_shapeOuuter
	Shape_t3950352489 * ____shapeOuuter_6;
	// System.Single CRebornSpot::m_fAlphaSpeed
	float ___m_fAlphaSpeed_7;
	// System.Single CRebornSpot::m_fTotalTime
	float ___m_fTotalTime_8;
	// System.Single CRebornSpot::m_fTimeCounting
	float ___m_fTimeCounting_9;
	// System.Single CRebornSpot::m_fBlingSpeed
	float ___m_fBlingSpeed_10;
	// UnityEngine.TextMesh CRebornSpot::_txtLeftTime
	TextMesh_t1536577757 * ____txtLeftTime_11;
	// System.Single CRebornSpot::m_fAlpha
	float ___m_fAlpha_12;
	// System.Int32 CRebornSpot::m_nAlphaStatus
	int32_t ___m_nAlphaStatus_13;

public:
	inline static int32_t get_offset_of__shapeInner_5() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ____shapeInner_5)); }
	inline Shape_t3950352489 * get__shapeInner_5() const { return ____shapeInner_5; }
	inline Shape_t3950352489 ** get_address_of__shapeInner_5() { return &____shapeInner_5; }
	inline void set__shapeInner_5(Shape_t3950352489 * value)
	{
		____shapeInner_5 = value;
		Il2CppCodeGenWriteBarrier((&____shapeInner_5), value);
	}

	inline static int32_t get_offset_of__shapeOuuter_6() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ____shapeOuuter_6)); }
	inline Shape_t3950352489 * get__shapeOuuter_6() const { return ____shapeOuuter_6; }
	inline Shape_t3950352489 ** get_address_of__shapeOuuter_6() { return &____shapeOuuter_6; }
	inline void set__shapeOuuter_6(Shape_t3950352489 * value)
	{
		____shapeOuuter_6 = value;
		Il2CppCodeGenWriteBarrier((&____shapeOuuter_6), value);
	}

	inline static int32_t get_offset_of_m_fAlphaSpeed_7() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ___m_fAlphaSpeed_7)); }
	inline float get_m_fAlphaSpeed_7() const { return ___m_fAlphaSpeed_7; }
	inline float* get_address_of_m_fAlphaSpeed_7() { return &___m_fAlphaSpeed_7; }
	inline void set_m_fAlphaSpeed_7(float value)
	{
		___m_fAlphaSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_fTotalTime_8() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ___m_fTotalTime_8)); }
	inline float get_m_fTotalTime_8() const { return ___m_fTotalTime_8; }
	inline float* get_address_of_m_fTotalTime_8() { return &___m_fTotalTime_8; }
	inline void set_m_fTotalTime_8(float value)
	{
		___m_fTotalTime_8 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCounting_9() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ___m_fTimeCounting_9)); }
	inline float get_m_fTimeCounting_9() const { return ___m_fTimeCounting_9; }
	inline float* get_address_of_m_fTimeCounting_9() { return &___m_fTimeCounting_9; }
	inline void set_m_fTimeCounting_9(float value)
	{
		___m_fTimeCounting_9 = value;
	}

	inline static int32_t get_offset_of_m_fBlingSpeed_10() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ___m_fBlingSpeed_10)); }
	inline float get_m_fBlingSpeed_10() const { return ___m_fBlingSpeed_10; }
	inline float* get_address_of_m_fBlingSpeed_10() { return &___m_fBlingSpeed_10; }
	inline void set_m_fBlingSpeed_10(float value)
	{
		___m_fBlingSpeed_10 = value;
	}

	inline static int32_t get_offset_of__txtLeftTime_11() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ____txtLeftTime_11)); }
	inline TextMesh_t1536577757 * get__txtLeftTime_11() const { return ____txtLeftTime_11; }
	inline TextMesh_t1536577757 ** get_address_of__txtLeftTime_11() { return &____txtLeftTime_11; }
	inline void set__txtLeftTime_11(TextMesh_t1536577757 * value)
	{
		____txtLeftTime_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_11), value);
	}

	inline static int32_t get_offset_of_m_fAlpha_12() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ___m_fAlpha_12)); }
	inline float get_m_fAlpha_12() const { return ___m_fAlpha_12; }
	inline float* get_address_of_m_fAlpha_12() { return &___m_fAlpha_12; }
	inline void set_m_fAlpha_12(float value)
	{
		___m_fAlpha_12 = value;
	}

	inline static int32_t get_offset_of_m_nAlphaStatus_13() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536, ___m_nAlphaStatus_13)); }
	inline int32_t get_m_nAlphaStatus_13() const { return ___m_nAlphaStatus_13; }
	inline int32_t* get_address_of_m_nAlphaStatus_13() { return &___m_nAlphaStatus_13; }
	inline void set_m_nAlphaStatus_13(int32_t value)
	{
		___m_nAlphaStatus_13 = value;
	}
};

struct CRebornSpot_t259671536_StaticFields
{
public:
	// UnityEngine.Vector3 CRebornSpot::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CRebornSpot::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// UnityEngine.Color CRebornSpot::tempColor
	Color_t2555686324  ___tempColor_4;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_tempColor_4() { return static_cast<int32_t>(offsetof(CRebornSpot_t259671536_StaticFields, ___tempColor_4)); }
	inline Color_t2555686324  get_tempColor_4() const { return ___tempColor_4; }
	inline Color_t2555686324 * get_address_of_tempColor_4() { return &___tempColor_4; }
	inline void set_tempColor_4(Color_t2555686324  value)
	{
		___tempColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREBORNSPOT_T259671536_H
#ifndef CPVEEDITOR_T756955826_H
#define CPVEEDITOR_T756955826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPveEditor
struct  CPveEditor_t756955826  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CPveEditor::m_arySubPanel
	GameObjectU5BU5D_t3328599146* ___m_arySubPanel_2;
	// UnityEngine.UI.Button CPveEditor::_btnGrass
	Button_t4055032469 * ____btnGrass_5;
	// UnityEngine.UI.Button CPveEditor::_btnSpray
	Button_t4055032469 * ____btnSpray_6;
	// UnityEngine.UI.InputField CPveEditor::_inputSelectObjScaleX
	InputField_t3762917431 * ____inputSelectObjScaleX_7;
	// UnityEngine.UI.InputField CPveEditor::_inputSelectObjScaleY
	InputField_t3762917431 * ____inputSelectObjScaleY_8;
	// UnityEngine.UI.InputField CPveEditor::_inputSelectObjRotation
	InputField_t3762917431 * ____inputSelectObjRotation_9;
	// UnityEngine.UI.Button CPveEditor::_btnDelCurSelectedObj
	Button_t4055032469 * ____btnDelCurSelectedObj_10;
	// CPveObj CPveEditor::m_CurSelectedObj
	CPveObj_t2172873472 * ___m_CurSelectedObj_11;

public:
	inline static int32_t get_offset_of_m_arySubPanel_2() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ___m_arySubPanel_2)); }
	inline GameObjectU5BU5D_t3328599146* get_m_arySubPanel_2() const { return ___m_arySubPanel_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_arySubPanel_2() { return &___m_arySubPanel_2; }
	inline void set_m_arySubPanel_2(GameObjectU5BU5D_t3328599146* value)
	{
		___m_arySubPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySubPanel_2), value);
	}

	inline static int32_t get_offset_of__btnGrass_5() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ____btnGrass_5)); }
	inline Button_t4055032469 * get__btnGrass_5() const { return ____btnGrass_5; }
	inline Button_t4055032469 ** get_address_of__btnGrass_5() { return &____btnGrass_5; }
	inline void set__btnGrass_5(Button_t4055032469 * value)
	{
		____btnGrass_5 = value;
		Il2CppCodeGenWriteBarrier((&____btnGrass_5), value);
	}

	inline static int32_t get_offset_of__btnSpray_6() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ____btnSpray_6)); }
	inline Button_t4055032469 * get__btnSpray_6() const { return ____btnSpray_6; }
	inline Button_t4055032469 ** get_address_of__btnSpray_6() { return &____btnSpray_6; }
	inline void set__btnSpray_6(Button_t4055032469 * value)
	{
		____btnSpray_6 = value;
		Il2CppCodeGenWriteBarrier((&____btnSpray_6), value);
	}

	inline static int32_t get_offset_of__inputSelectObjScaleX_7() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ____inputSelectObjScaleX_7)); }
	inline InputField_t3762917431 * get__inputSelectObjScaleX_7() const { return ____inputSelectObjScaleX_7; }
	inline InputField_t3762917431 ** get_address_of__inputSelectObjScaleX_7() { return &____inputSelectObjScaleX_7; }
	inline void set__inputSelectObjScaleX_7(InputField_t3762917431 * value)
	{
		____inputSelectObjScaleX_7 = value;
		Il2CppCodeGenWriteBarrier((&____inputSelectObjScaleX_7), value);
	}

	inline static int32_t get_offset_of__inputSelectObjScaleY_8() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ____inputSelectObjScaleY_8)); }
	inline InputField_t3762917431 * get__inputSelectObjScaleY_8() const { return ____inputSelectObjScaleY_8; }
	inline InputField_t3762917431 ** get_address_of__inputSelectObjScaleY_8() { return &____inputSelectObjScaleY_8; }
	inline void set__inputSelectObjScaleY_8(InputField_t3762917431 * value)
	{
		____inputSelectObjScaleY_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputSelectObjScaleY_8), value);
	}

	inline static int32_t get_offset_of__inputSelectObjRotation_9() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ____inputSelectObjRotation_9)); }
	inline InputField_t3762917431 * get__inputSelectObjRotation_9() const { return ____inputSelectObjRotation_9; }
	inline InputField_t3762917431 ** get_address_of__inputSelectObjRotation_9() { return &____inputSelectObjRotation_9; }
	inline void set__inputSelectObjRotation_9(InputField_t3762917431 * value)
	{
		____inputSelectObjRotation_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputSelectObjRotation_9), value);
	}

	inline static int32_t get_offset_of__btnDelCurSelectedObj_10() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ____btnDelCurSelectedObj_10)); }
	inline Button_t4055032469 * get__btnDelCurSelectedObj_10() const { return ____btnDelCurSelectedObj_10; }
	inline Button_t4055032469 ** get_address_of__btnDelCurSelectedObj_10() { return &____btnDelCurSelectedObj_10; }
	inline void set__btnDelCurSelectedObj_10(Button_t4055032469 * value)
	{
		____btnDelCurSelectedObj_10 = value;
		Il2CppCodeGenWriteBarrier((&____btnDelCurSelectedObj_10), value);
	}

	inline static int32_t get_offset_of_m_CurSelectedObj_11() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826, ___m_CurSelectedObj_11)); }
	inline CPveObj_t2172873472 * get_m_CurSelectedObj_11() const { return ___m_CurSelectedObj_11; }
	inline CPveObj_t2172873472 ** get_address_of_m_CurSelectedObj_11() { return &___m_CurSelectedObj_11; }
	inline void set_m_CurSelectedObj_11(CPveObj_t2172873472 * value)
	{
		___m_CurSelectedObj_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedObj_11), value);
	}
};

struct CPveEditor_t756955826_StaticFields
{
public:
	// UnityEngine.Vector3 CPveEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// CPveEditor CPveEditor::s_Instance
	CPveEditor_t756955826 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CPveEditor_t756955826_StaticFields, ___s_Instance_4)); }
	inline CPveEditor_t756955826 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CPveEditor_t756955826 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CPveEditor_t756955826 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPVEEDITOR_T756955826_H
#ifndef CPVEOBJ_T2172873472_H
#define CPVEOBJ_T2172873472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPveObj
struct  CPveObj_t2172873472  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 CPveObj::m_vScale
	Vector3_t3722313464  ___m_vScale_2;
	// System.Single CPveObj::m_fRotation
	float ___m_fRotation_3;
	// System.Int32 CPveObj::m_nGuid
	int32_t ___m_nGuid_4;
	// System.Int32 CPveObj::m_ConfigId
	int32_t ___m_ConfigId_5;
	// UnityEngine.SpriteRenderer CPveObj::_srMain
	SpriteRenderer_t3235626157 * ____srMain_6;

public:
	inline static int32_t get_offset_of_m_vScale_2() { return static_cast<int32_t>(offsetof(CPveObj_t2172873472, ___m_vScale_2)); }
	inline Vector3_t3722313464  get_m_vScale_2() const { return ___m_vScale_2; }
	inline Vector3_t3722313464 * get_address_of_m_vScale_2() { return &___m_vScale_2; }
	inline void set_m_vScale_2(Vector3_t3722313464  value)
	{
		___m_vScale_2 = value;
	}

	inline static int32_t get_offset_of_m_fRotation_3() { return static_cast<int32_t>(offsetof(CPveObj_t2172873472, ___m_fRotation_3)); }
	inline float get_m_fRotation_3() const { return ___m_fRotation_3; }
	inline float* get_address_of_m_fRotation_3() { return &___m_fRotation_3; }
	inline void set_m_fRotation_3(float value)
	{
		___m_fRotation_3 = value;
	}

	inline static int32_t get_offset_of_m_nGuid_4() { return static_cast<int32_t>(offsetof(CPveObj_t2172873472, ___m_nGuid_4)); }
	inline int32_t get_m_nGuid_4() const { return ___m_nGuid_4; }
	inline int32_t* get_address_of_m_nGuid_4() { return &___m_nGuid_4; }
	inline void set_m_nGuid_4(int32_t value)
	{
		___m_nGuid_4 = value;
	}

	inline static int32_t get_offset_of_m_ConfigId_5() { return static_cast<int32_t>(offsetof(CPveObj_t2172873472, ___m_ConfigId_5)); }
	inline int32_t get_m_ConfigId_5() const { return ___m_ConfigId_5; }
	inline int32_t* get_address_of_m_ConfigId_5() { return &___m_ConfigId_5; }
	inline void set_m_ConfigId_5(int32_t value)
	{
		___m_ConfigId_5 = value;
	}

	inline static int32_t get_offset_of__srMain_6() { return static_cast<int32_t>(offsetof(CPveObj_t2172873472, ____srMain_6)); }
	inline SpriteRenderer_t3235626157 * get__srMain_6() const { return ____srMain_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_6() { return &____srMain_6; }
	inline void set__srMain_6(SpriteRenderer_t3235626157 * value)
	{
		____srMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPVEOBJ_T2172873472_H
#ifndef CSPRYEDITOR_T3631257256_H
#define CSPRYEDITOR_T3631257256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpryEditor
struct  CSpryEditor_t3631257256  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CSpryEditor::m_goSprayContainer
	GameObject_t1113636619 * ___m_goSprayContainer_5;
	// UnityEngine.UI.Toggle CSpryEditor::_toogleShowRealtimeBeanSpray
	Toggle_t2735377061 * ____toogleShowRealtimeBeanSpray_6;
	// UnityEngine.UI.Dropdown CSpryEditor::_dropdownSprayId
	Dropdown_t2274391225 * ____dropdownSprayId_7;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldThornId
	InputField_t3762917431 * ____inputfieldThornId_8;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldDensity
	InputField_t3762917431 * ____inputfieldDensity_9;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldMeatDensity
	InputField_t3762917431 * ____inputfieldMeatDensity_10;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldMaxDis
	InputField_t3762917431 * ____inputfieldMaxDis_11;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldMinDis
	InputField_t3762917431 * ____inputfieldMinDis_12;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldBeanLifeTime
	InputField_t3762917431 * ____inputfieldBeanLifeTime_13;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldSprayInterval
	InputField_t3762917431 * ____inputfieldSprayInterval_14;
	// UnityEngine.UI.InputField CSpryEditor::_inputfieldSpraySize
	InputField_t3762917431 * ____inputfieldSpraySize_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,CSpryEditor/sSprayConfig> CSpryEditor::m_dicSprayConfig
	Dictionary_2_t754550263 * ___m_dicSprayConfig_16;
	// CSpryEditor/sSprayConfig CSpryEditor::m_CurSprayConfig
	sSprayConfig_t1865836932  ___m_CurSprayConfig_17;
	// CSpryEditor/sSprayConfig CSpryEditor::tempSprayConfig
	sSprayConfig_t1865836932  ___tempSprayConfig_18;

public:
	inline static int32_t get_offset_of_m_goSprayContainer_5() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ___m_goSprayContainer_5)); }
	inline GameObject_t1113636619 * get_m_goSprayContainer_5() const { return ___m_goSprayContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_m_goSprayContainer_5() { return &___m_goSprayContainer_5; }
	inline void set_m_goSprayContainer_5(GameObject_t1113636619 * value)
	{
		___m_goSprayContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_goSprayContainer_5), value);
	}

	inline static int32_t get_offset_of__toogleShowRealtimeBeanSpray_6() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____toogleShowRealtimeBeanSpray_6)); }
	inline Toggle_t2735377061 * get__toogleShowRealtimeBeanSpray_6() const { return ____toogleShowRealtimeBeanSpray_6; }
	inline Toggle_t2735377061 ** get_address_of__toogleShowRealtimeBeanSpray_6() { return &____toogleShowRealtimeBeanSpray_6; }
	inline void set__toogleShowRealtimeBeanSpray_6(Toggle_t2735377061 * value)
	{
		____toogleShowRealtimeBeanSpray_6 = value;
		Il2CppCodeGenWriteBarrier((&____toogleShowRealtimeBeanSpray_6), value);
	}

	inline static int32_t get_offset_of__dropdownSprayId_7() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____dropdownSprayId_7)); }
	inline Dropdown_t2274391225 * get__dropdownSprayId_7() const { return ____dropdownSprayId_7; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownSprayId_7() { return &____dropdownSprayId_7; }
	inline void set__dropdownSprayId_7(Dropdown_t2274391225 * value)
	{
		____dropdownSprayId_7 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownSprayId_7), value);
	}

	inline static int32_t get_offset_of__inputfieldThornId_8() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldThornId_8)); }
	inline InputField_t3762917431 * get__inputfieldThornId_8() const { return ____inputfieldThornId_8; }
	inline InputField_t3762917431 ** get_address_of__inputfieldThornId_8() { return &____inputfieldThornId_8; }
	inline void set__inputfieldThornId_8(InputField_t3762917431 * value)
	{
		____inputfieldThornId_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldThornId_8), value);
	}

	inline static int32_t get_offset_of__inputfieldDensity_9() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldDensity_9)); }
	inline InputField_t3762917431 * get__inputfieldDensity_9() const { return ____inputfieldDensity_9; }
	inline InputField_t3762917431 ** get_address_of__inputfieldDensity_9() { return &____inputfieldDensity_9; }
	inline void set__inputfieldDensity_9(InputField_t3762917431 * value)
	{
		____inputfieldDensity_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldDensity_9), value);
	}

	inline static int32_t get_offset_of__inputfieldMeatDensity_10() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldMeatDensity_10)); }
	inline InputField_t3762917431 * get__inputfieldMeatDensity_10() const { return ____inputfieldMeatDensity_10; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMeatDensity_10() { return &____inputfieldMeatDensity_10; }
	inline void set__inputfieldMeatDensity_10(InputField_t3762917431 * value)
	{
		____inputfieldMeatDensity_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMeatDensity_10), value);
	}

	inline static int32_t get_offset_of__inputfieldMaxDis_11() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldMaxDis_11)); }
	inline InputField_t3762917431 * get__inputfieldMaxDis_11() const { return ____inputfieldMaxDis_11; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMaxDis_11() { return &____inputfieldMaxDis_11; }
	inline void set__inputfieldMaxDis_11(InputField_t3762917431 * value)
	{
		____inputfieldMaxDis_11 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMaxDis_11), value);
	}

	inline static int32_t get_offset_of__inputfieldMinDis_12() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldMinDis_12)); }
	inline InputField_t3762917431 * get__inputfieldMinDis_12() const { return ____inputfieldMinDis_12; }
	inline InputField_t3762917431 ** get_address_of__inputfieldMinDis_12() { return &____inputfieldMinDis_12; }
	inline void set__inputfieldMinDis_12(InputField_t3762917431 * value)
	{
		____inputfieldMinDis_12 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldMinDis_12), value);
	}

	inline static int32_t get_offset_of__inputfieldBeanLifeTime_13() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldBeanLifeTime_13)); }
	inline InputField_t3762917431 * get__inputfieldBeanLifeTime_13() const { return ____inputfieldBeanLifeTime_13; }
	inline InputField_t3762917431 ** get_address_of__inputfieldBeanLifeTime_13() { return &____inputfieldBeanLifeTime_13; }
	inline void set__inputfieldBeanLifeTime_13(InputField_t3762917431 * value)
	{
		____inputfieldBeanLifeTime_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldBeanLifeTime_13), value);
	}

	inline static int32_t get_offset_of__inputfieldSprayInterval_14() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldSprayInterval_14)); }
	inline InputField_t3762917431 * get__inputfieldSprayInterval_14() const { return ____inputfieldSprayInterval_14; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSprayInterval_14() { return &____inputfieldSprayInterval_14; }
	inline void set__inputfieldSprayInterval_14(InputField_t3762917431 * value)
	{
		____inputfieldSprayInterval_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSprayInterval_14), value);
	}

	inline static int32_t get_offset_of__inputfieldSpraySize_15() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ____inputfieldSpraySize_15)); }
	inline InputField_t3762917431 * get__inputfieldSpraySize_15() const { return ____inputfieldSpraySize_15; }
	inline InputField_t3762917431 ** get_address_of__inputfieldSpraySize_15() { return &____inputfieldSpraySize_15; }
	inline void set__inputfieldSpraySize_15(InputField_t3762917431 * value)
	{
		____inputfieldSpraySize_15 = value;
		Il2CppCodeGenWriteBarrier((&____inputfieldSpraySize_15), value);
	}

	inline static int32_t get_offset_of_m_dicSprayConfig_16() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ___m_dicSprayConfig_16)); }
	inline Dictionary_2_t754550263 * get_m_dicSprayConfig_16() const { return ___m_dicSprayConfig_16; }
	inline Dictionary_2_t754550263 ** get_address_of_m_dicSprayConfig_16() { return &___m_dicSprayConfig_16; }
	inline void set_m_dicSprayConfig_16(Dictionary_2_t754550263 * value)
	{
		___m_dicSprayConfig_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSprayConfig_16), value);
	}

	inline static int32_t get_offset_of_m_CurSprayConfig_17() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ___m_CurSprayConfig_17)); }
	inline sSprayConfig_t1865836932  get_m_CurSprayConfig_17() const { return ___m_CurSprayConfig_17; }
	inline sSprayConfig_t1865836932 * get_address_of_m_CurSprayConfig_17() { return &___m_CurSprayConfig_17; }
	inline void set_m_CurSprayConfig_17(sSprayConfig_t1865836932  value)
	{
		___m_CurSprayConfig_17 = value;
	}

	inline static int32_t get_offset_of_tempSprayConfig_18() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256, ___tempSprayConfig_18)); }
	inline sSprayConfig_t1865836932  get_tempSprayConfig_18() const { return ___tempSprayConfig_18; }
	inline sSprayConfig_t1865836932 * get_address_of_tempSprayConfig_18() { return &___tempSprayConfig_18; }
	inline void set_tempSprayConfig_18(sSprayConfig_t1865836932  value)
	{
		___tempSprayConfig_18 = value;
	}
};

struct CSpryEditor_t3631257256_StaticFields
{
public:
	// UnityEngine.Vector3 CSpryEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CSpryEditor::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// CSpryEditor CSpryEditor::s_Instance
	CSpryEditor_t3631257256 * ___s_Instance_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CSpryEditor::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_19;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256_StaticFields, ___s_Instance_4)); }
	inline CSpryEditor_t3631257256 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CSpryEditor_t3631257256 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CSpryEditor_t3631257256 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_19() { return static_cast<int32_t>(offsetof(CSpryEditor_t3631257256_StaticFields, ___U3CU3Ef__switchU24map2_19)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_19() const { return ___U3CU3Ef__switchU24map2_19; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_19() { return &___U3CU3Ef__switchU24map2_19; }
	inline void set_U3CU3Ef__switchU24map2_19(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPRYEDITOR_T3631257256_H
#ifndef CGRASSEDITOR_T1538907628_H
#define CGRASSEDITOR_T1538907628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrassEditor
struct  CGrassEditor_t1538907628  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CGrassEditor::m_goGrassContainer
	GameObject_t1113636619 * ___m_goGrassContainer_5;
	// UnityEngine.UI.Dropdown CGrassEditor::_dropdownGrassID
	Dropdown_t2274391225 * ____dropdownGrassID_6;
	// UnityEngine.UI.Dropdown CGrassEditor::_dropdownGrassType
	Dropdown_t2274391225 * ____dropdownGrassType_7;
	// UnityEngine.UI.InputField CGrassEditor::_inputPolygonId
	InputField_t3762917431 * ____inputPolygonId_8;
	// UnityEngine.UI.InputField CGrassEditor::_inputColor
	InputField_t3762917431 * ____inputColor_9;
	// UnityEngine.UI.InputField[] CGrassEditor::_aryInputValue
	InputFieldU5BU5D_t1172778254* ____aryInputValue_11;
	// UnityEngine.UI.InputField[] CGrassEditor::_aryInputDesc
	InputFieldU5BU5D_t1172778254* ____aryInputDesc_12;
	// UnityEngine.UI.Button CGrassEditor::_btnAddGrass
	Button_t4055032469 * ____btnAddGrass_13;
	// CGrassEditor/sGrassConfig CGrassEditor::m_CurGrassConfig
	sGrassConfig_t73951773  ___m_CurGrassConfig_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,CGrassEditor/sGrassConfig> CGrassEditor::m_dicGrassConfig
	Dictionary_2_t3257632400 * ___m_dicGrassConfig_15;
	// CGrassEditor/sGrassConfig CGrassEditor::tempGrassConfig
	sGrassConfig_t73951773  ___tempGrassConfig_16;

public:
	inline static int32_t get_offset_of_m_goGrassContainer_5() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ___m_goGrassContainer_5)); }
	inline GameObject_t1113636619 * get_m_goGrassContainer_5() const { return ___m_goGrassContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_m_goGrassContainer_5() { return &___m_goGrassContainer_5; }
	inline void set_m_goGrassContainer_5(GameObject_t1113636619 * value)
	{
		___m_goGrassContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_goGrassContainer_5), value);
	}

	inline static int32_t get_offset_of__dropdownGrassID_6() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____dropdownGrassID_6)); }
	inline Dropdown_t2274391225 * get__dropdownGrassID_6() const { return ____dropdownGrassID_6; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownGrassID_6() { return &____dropdownGrassID_6; }
	inline void set__dropdownGrassID_6(Dropdown_t2274391225 * value)
	{
		____dropdownGrassID_6 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownGrassID_6), value);
	}

	inline static int32_t get_offset_of__dropdownGrassType_7() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____dropdownGrassType_7)); }
	inline Dropdown_t2274391225 * get__dropdownGrassType_7() const { return ____dropdownGrassType_7; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownGrassType_7() { return &____dropdownGrassType_7; }
	inline void set__dropdownGrassType_7(Dropdown_t2274391225 * value)
	{
		____dropdownGrassType_7 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownGrassType_7), value);
	}

	inline static int32_t get_offset_of__inputPolygonId_8() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____inputPolygonId_8)); }
	inline InputField_t3762917431 * get__inputPolygonId_8() const { return ____inputPolygonId_8; }
	inline InputField_t3762917431 ** get_address_of__inputPolygonId_8() { return &____inputPolygonId_8; }
	inline void set__inputPolygonId_8(InputField_t3762917431 * value)
	{
		____inputPolygonId_8 = value;
		Il2CppCodeGenWriteBarrier((&____inputPolygonId_8), value);
	}

	inline static int32_t get_offset_of__inputColor_9() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____inputColor_9)); }
	inline InputField_t3762917431 * get__inputColor_9() const { return ____inputColor_9; }
	inline InputField_t3762917431 ** get_address_of__inputColor_9() { return &____inputColor_9; }
	inline void set__inputColor_9(InputField_t3762917431 * value)
	{
		____inputColor_9 = value;
		Il2CppCodeGenWriteBarrier((&____inputColor_9), value);
	}

	inline static int32_t get_offset_of__aryInputValue_11() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____aryInputValue_11)); }
	inline InputFieldU5BU5D_t1172778254* get__aryInputValue_11() const { return ____aryInputValue_11; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryInputValue_11() { return &____aryInputValue_11; }
	inline void set__aryInputValue_11(InputFieldU5BU5D_t1172778254* value)
	{
		____aryInputValue_11 = value;
		Il2CppCodeGenWriteBarrier((&____aryInputValue_11), value);
	}

	inline static int32_t get_offset_of__aryInputDesc_12() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____aryInputDesc_12)); }
	inline InputFieldU5BU5D_t1172778254* get__aryInputDesc_12() const { return ____aryInputDesc_12; }
	inline InputFieldU5BU5D_t1172778254** get_address_of__aryInputDesc_12() { return &____aryInputDesc_12; }
	inline void set__aryInputDesc_12(InputFieldU5BU5D_t1172778254* value)
	{
		____aryInputDesc_12 = value;
		Il2CppCodeGenWriteBarrier((&____aryInputDesc_12), value);
	}

	inline static int32_t get_offset_of__btnAddGrass_13() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ____btnAddGrass_13)); }
	inline Button_t4055032469 * get__btnAddGrass_13() const { return ____btnAddGrass_13; }
	inline Button_t4055032469 ** get_address_of__btnAddGrass_13() { return &____btnAddGrass_13; }
	inline void set__btnAddGrass_13(Button_t4055032469 * value)
	{
		____btnAddGrass_13 = value;
		Il2CppCodeGenWriteBarrier((&____btnAddGrass_13), value);
	}

	inline static int32_t get_offset_of_m_CurGrassConfig_14() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ___m_CurGrassConfig_14)); }
	inline sGrassConfig_t73951773  get_m_CurGrassConfig_14() const { return ___m_CurGrassConfig_14; }
	inline sGrassConfig_t73951773 * get_address_of_m_CurGrassConfig_14() { return &___m_CurGrassConfig_14; }
	inline void set_m_CurGrassConfig_14(sGrassConfig_t73951773  value)
	{
		___m_CurGrassConfig_14 = value;
	}

	inline static int32_t get_offset_of_m_dicGrassConfig_15() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ___m_dicGrassConfig_15)); }
	inline Dictionary_2_t3257632400 * get_m_dicGrassConfig_15() const { return ___m_dicGrassConfig_15; }
	inline Dictionary_2_t3257632400 ** get_address_of_m_dicGrassConfig_15() { return &___m_dicGrassConfig_15; }
	inline void set_m_dicGrassConfig_15(Dictionary_2_t3257632400 * value)
	{
		___m_dicGrassConfig_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicGrassConfig_15), value);
	}

	inline static int32_t get_offset_of_tempGrassConfig_16() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628, ___tempGrassConfig_16)); }
	inline sGrassConfig_t73951773  get_tempGrassConfig_16() const { return ___tempGrassConfig_16; }
	inline sGrassConfig_t73951773 * get_address_of_tempGrassConfig_16() { return &___tempGrassConfig_16; }
	inline void set_tempGrassConfig_16(sGrassConfig_t73951773  value)
	{
		___tempGrassConfig_16 = value;
	}
};

struct CGrassEditor_t1538907628_StaticFields
{
public:
	// CGrassEditor CGrassEditor::s_Instance
	CGrassEditor_t1538907628 * ___s_Instance_2;
	// UnityEngine.Vector3 CGrassEditor::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;
	// UnityEngine.Vector3 CGrassEditor::vecTempPos
	Vector3_t3722313464  ___vecTempPos_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CGrassEditor::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_17;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628_StaticFields, ___s_Instance_2)); }
	inline CGrassEditor_t1538907628 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CGrassEditor_t1538907628 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CGrassEditor_t1538907628 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628_StaticFields, ___vecTempPos_4)); }
	inline Vector3_t3722313464  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_t3722313464  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_17() { return static_cast<int32_t>(offsetof(CGrassEditor_t1538907628_StaticFields, ___U3CU3Ef__switchU24map1_17)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_17() const { return ___U3CU3Ef__switchU24map1_17; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_17() { return &___U3CU3Ef__switchU24map1_17; }
	inline void set_U3CU3Ef__switchU24map1_17(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGRASSEDITOR_T1538907628_H
#ifndef CSKINMANAGER_T2556315286_H
#define CSKINMANAGER_T2556315286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkinManager
struct  CSkinManager_t2556315286  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CSkinManager_t2556315286_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite> CSkinManager::m_dicSkinSprites
	Dictionary_2_t3464337719 * ___m_dicSkinSprites_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> CSkinManager::m_dicSkinMaterial
	Dictionary_2_t3524055750 * ___m_dicSkinMaterial_3;
	// UnityEngine.Material CSkinManager::m_matNoSkinMaterial
	Material_t340375123 * ___m_matNoSkinMaterial_5;

public:
	inline static int32_t get_offset_of_m_dicSkinSprites_2() { return static_cast<int32_t>(offsetof(CSkinManager_t2556315286_StaticFields, ___m_dicSkinSprites_2)); }
	inline Dictionary_2_t3464337719 * get_m_dicSkinSprites_2() const { return ___m_dicSkinSprites_2; }
	inline Dictionary_2_t3464337719 ** get_address_of_m_dicSkinSprites_2() { return &___m_dicSkinSprites_2; }
	inline void set_m_dicSkinSprites_2(Dictionary_2_t3464337719 * value)
	{
		___m_dicSkinSprites_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkinSprites_2), value);
	}

	inline static int32_t get_offset_of_m_dicSkinMaterial_3() { return static_cast<int32_t>(offsetof(CSkinManager_t2556315286_StaticFields, ___m_dicSkinMaterial_3)); }
	inline Dictionary_2_t3524055750 * get_m_dicSkinMaterial_3() const { return ___m_dicSkinMaterial_3; }
	inline Dictionary_2_t3524055750 ** get_address_of_m_dicSkinMaterial_3() { return &___m_dicSkinMaterial_3; }
	inline void set_m_dicSkinMaterial_3(Dictionary_2_t3524055750 * value)
	{
		___m_dicSkinMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkinMaterial_3), value);
	}

	inline static int32_t get_offset_of_m_matNoSkinMaterial_5() { return static_cast<int32_t>(offsetof(CSkinManager_t2556315286_StaticFields, ___m_matNoSkinMaterial_5)); }
	inline Material_t340375123 * get_m_matNoSkinMaterial_5() const { return ___m_matNoSkinMaterial_5; }
	inline Material_t340375123 ** get_address_of_m_matNoSkinMaterial_5() { return &___m_matNoSkinMaterial_5; }
	inline void set_m_matNoSkinMaterial_5(Material_t340375123 * value)
	{
		___m_matNoSkinMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_matNoSkinMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSKINMANAGER_T2556315286_H
#ifndef CPAIHANGBANGRECORD_T1494756022_H
#define CPAIHANGBANGRECORD_T1494756022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBangRecord
struct  CPaiHangBangRecord_t1494756022  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image CPaiHangBangRecord::_imgBg
	Image_t2670269651 * ____imgBg_2;
	// UnityEngine.UI.Image CPaiHangBangRecord::_imgAvatar
	Image_t2670269651 * ____imgAvatar_3;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtRank
	Text_t1901882714 * ____txtRank_4;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_5;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtPlayerLevel
	Text_t1901882714 * ____txtPlayerLevel_6;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtJiShaInfo
	Text_t1901882714 * ____txtJiShaInfo_7;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtTotalVolume
	Text_t1901882714 * ____txtTotalVolume_8;
	// UnityEngine.UI.Text[] CPaiHangBangRecord::_aryTxtItemLevel
	TextU5BU5D_t422084607* ____aryTxtItemLevel_9;
	// UnityEngine.UI.Image CPaiHangBangRecord::_imgSelectedSkillIcon
	Image_t2670269651 * ____imgSelectedSkillIcon_10;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtSelectedSkillLevel
	Text_t1901882714 * ____txtSelectedSkillLevel_11;
	// UnityEngine.UI.Text CPaiHangBangRecord::_txtEatThornNum
	Text_t1901882714 * ____txtEatThornNum_12;
	// UnityEngine.UI.Image CPaiHangBangRecord::_imgHighLight
	Image_t2670269651 * ____imgHighLight_13;
	// System.Int32 CPaiHangBangRecord::m_nIndex
	int32_t ___m_nIndex_14;

public:
	inline static int32_t get_offset_of__imgBg_2() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____imgBg_2)); }
	inline Image_t2670269651 * get__imgBg_2() const { return ____imgBg_2; }
	inline Image_t2670269651 ** get_address_of__imgBg_2() { return &____imgBg_2; }
	inline void set__imgBg_2(Image_t2670269651 * value)
	{
		____imgBg_2 = value;
		Il2CppCodeGenWriteBarrier((&____imgBg_2), value);
	}

	inline static int32_t get_offset_of__imgAvatar_3() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____imgAvatar_3)); }
	inline Image_t2670269651 * get__imgAvatar_3() const { return ____imgAvatar_3; }
	inline Image_t2670269651 ** get_address_of__imgAvatar_3() { return &____imgAvatar_3; }
	inline void set__imgAvatar_3(Image_t2670269651 * value)
	{
		____imgAvatar_3 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_3), value);
	}

	inline static int32_t get_offset_of__txtRank_4() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtRank_4)); }
	inline Text_t1901882714 * get__txtRank_4() const { return ____txtRank_4; }
	inline Text_t1901882714 ** get_address_of__txtRank_4() { return &____txtRank_4; }
	inline void set__txtRank_4(Text_t1901882714 * value)
	{
		____txtRank_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtRank_4), value);
	}

	inline static int32_t get_offset_of__txtPlayerName_5() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtPlayerName_5)); }
	inline Text_t1901882714 * get__txtPlayerName_5() const { return ____txtPlayerName_5; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_5() { return &____txtPlayerName_5; }
	inline void set__txtPlayerName_5(Text_t1901882714 * value)
	{
		____txtPlayerName_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_5), value);
	}

	inline static int32_t get_offset_of__txtPlayerLevel_6() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtPlayerLevel_6)); }
	inline Text_t1901882714 * get__txtPlayerLevel_6() const { return ____txtPlayerLevel_6; }
	inline Text_t1901882714 ** get_address_of__txtPlayerLevel_6() { return &____txtPlayerLevel_6; }
	inline void set__txtPlayerLevel_6(Text_t1901882714 * value)
	{
		____txtPlayerLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerLevel_6), value);
	}

	inline static int32_t get_offset_of__txtJiShaInfo_7() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtJiShaInfo_7)); }
	inline Text_t1901882714 * get__txtJiShaInfo_7() const { return ____txtJiShaInfo_7; }
	inline Text_t1901882714 ** get_address_of__txtJiShaInfo_7() { return &____txtJiShaInfo_7; }
	inline void set__txtJiShaInfo_7(Text_t1901882714 * value)
	{
		____txtJiShaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtJiShaInfo_7), value);
	}

	inline static int32_t get_offset_of__txtTotalVolume_8() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtTotalVolume_8)); }
	inline Text_t1901882714 * get__txtTotalVolume_8() const { return ____txtTotalVolume_8; }
	inline Text_t1901882714 ** get_address_of__txtTotalVolume_8() { return &____txtTotalVolume_8; }
	inline void set__txtTotalVolume_8(Text_t1901882714 * value)
	{
		____txtTotalVolume_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalVolume_8), value);
	}

	inline static int32_t get_offset_of__aryTxtItemLevel_9() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____aryTxtItemLevel_9)); }
	inline TextU5BU5D_t422084607* get__aryTxtItemLevel_9() const { return ____aryTxtItemLevel_9; }
	inline TextU5BU5D_t422084607** get_address_of__aryTxtItemLevel_9() { return &____aryTxtItemLevel_9; }
	inline void set__aryTxtItemLevel_9(TextU5BU5D_t422084607* value)
	{
		____aryTxtItemLevel_9 = value;
		Il2CppCodeGenWriteBarrier((&____aryTxtItemLevel_9), value);
	}

	inline static int32_t get_offset_of__imgSelectedSkillIcon_10() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____imgSelectedSkillIcon_10)); }
	inline Image_t2670269651 * get__imgSelectedSkillIcon_10() const { return ____imgSelectedSkillIcon_10; }
	inline Image_t2670269651 ** get_address_of__imgSelectedSkillIcon_10() { return &____imgSelectedSkillIcon_10; }
	inline void set__imgSelectedSkillIcon_10(Image_t2670269651 * value)
	{
		____imgSelectedSkillIcon_10 = value;
		Il2CppCodeGenWriteBarrier((&____imgSelectedSkillIcon_10), value);
	}

	inline static int32_t get_offset_of__txtSelectedSkillLevel_11() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtSelectedSkillLevel_11)); }
	inline Text_t1901882714 * get__txtSelectedSkillLevel_11() const { return ____txtSelectedSkillLevel_11; }
	inline Text_t1901882714 ** get_address_of__txtSelectedSkillLevel_11() { return &____txtSelectedSkillLevel_11; }
	inline void set__txtSelectedSkillLevel_11(Text_t1901882714 * value)
	{
		____txtSelectedSkillLevel_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtSelectedSkillLevel_11), value);
	}

	inline static int32_t get_offset_of__txtEatThornNum_12() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____txtEatThornNum_12)); }
	inline Text_t1901882714 * get__txtEatThornNum_12() const { return ____txtEatThornNum_12; }
	inline Text_t1901882714 ** get_address_of__txtEatThornNum_12() { return &____txtEatThornNum_12; }
	inline void set__txtEatThornNum_12(Text_t1901882714 * value)
	{
		____txtEatThornNum_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtEatThornNum_12), value);
	}

	inline static int32_t get_offset_of__imgHighLight_13() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ____imgHighLight_13)); }
	inline Image_t2670269651 * get__imgHighLight_13() const { return ____imgHighLight_13; }
	inline Image_t2670269651 ** get_address_of__imgHighLight_13() { return &____imgHighLight_13; }
	inline void set__imgHighLight_13(Image_t2670269651 * value)
	{
		____imgHighLight_13 = value;
		Il2CppCodeGenWriteBarrier((&____imgHighLight_13), value);
	}

	inline static int32_t get_offset_of_m_nIndex_14() { return static_cast<int32_t>(offsetof(CPaiHangBangRecord_t1494756022, ___m_nIndex_14)); }
	inline int32_t get_m_nIndex_14() const { return ___m_nIndex_14; }
	inline int32_t* get_address_of_m_nIndex_14() { return &___m_nIndex_14; }
	inline void set_m_nIndex_14(int32_t value)
	{
		___m_nIndex_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPAIHANGBANGRECORD_T1494756022_H
#ifndef SHAPE_T3950352489_H
#define SHAPE_T3950352489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shapes2D.Shape
struct  Shape_t3950352489  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material Shapes2D.Shape::material
	Material_t340375123 * ___material_5;
	// UnityEngine.SpriteRenderer Shapes2D.Shape::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_6;
	// UnityEngine.UI.Image Shapes2D.Shape::image
	Image_t2670269651 * ___image_7;
	// UnityEngine.RectTransform Shapes2D.Shape::rectTransform
	RectTransform_t3704657025 * ___rectTransform_8;
	// UnityEngine.Vector3 Shapes2D.Shape::computedScale
	Vector3_t3722313464  ___computedScale_9;
	// UnityEngine.Rect Shapes2D.Shape::computedRect
	Rect_t2360479859  ___computedRect_10;
	// Shapes2D.Shape/UserProps Shapes2D.Shape::settings
	UserProps_t2498857227 * ___settings_11;
	// Shapes2D.Shape/ShaderProps Shapes2D.Shape::shaderSettings
	ShaderProps_t3992238139 * ___shaderSettings_12;
	// System.Boolean Shapes2D.Shape::wasConverted
	bool ___wasConverted_13;
	// UnityEngine.Vector3 Shapes2D.Shape::preConvertedScale
	Vector3_t3722313464  ___preConvertedScale_14;

public:
	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___material_5)); }
	inline Material_t340375123 * get_material_5() const { return ___material_5; }
	inline Material_t340375123 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_t340375123 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_spriteRenderer_6() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___spriteRenderer_6)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_6() const { return ___spriteRenderer_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_6() { return &___spriteRenderer_6; }
	inline void set_spriteRenderer_6(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_6), value);
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___image_7)); }
	inline Image_t2670269651 * get_image_7() const { return ___image_7; }
	inline Image_t2670269651 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t2670269651 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}

	inline static int32_t get_offset_of_rectTransform_8() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___rectTransform_8)); }
	inline RectTransform_t3704657025 * get_rectTransform_8() const { return ___rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_8() { return &___rectTransform_8; }
	inline void set_rectTransform_8(RectTransform_t3704657025 * value)
	{
		___rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_8), value);
	}

	inline static int32_t get_offset_of_computedScale_9() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___computedScale_9)); }
	inline Vector3_t3722313464  get_computedScale_9() const { return ___computedScale_9; }
	inline Vector3_t3722313464 * get_address_of_computedScale_9() { return &___computedScale_9; }
	inline void set_computedScale_9(Vector3_t3722313464  value)
	{
		___computedScale_9 = value;
	}

	inline static int32_t get_offset_of_computedRect_10() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___computedRect_10)); }
	inline Rect_t2360479859  get_computedRect_10() const { return ___computedRect_10; }
	inline Rect_t2360479859 * get_address_of_computedRect_10() { return &___computedRect_10; }
	inline void set_computedRect_10(Rect_t2360479859  value)
	{
		___computedRect_10 = value;
	}

	inline static int32_t get_offset_of_settings_11() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___settings_11)); }
	inline UserProps_t2498857227 * get_settings_11() const { return ___settings_11; }
	inline UserProps_t2498857227 ** get_address_of_settings_11() { return &___settings_11; }
	inline void set_settings_11(UserProps_t2498857227 * value)
	{
		___settings_11 = value;
		Il2CppCodeGenWriteBarrier((&___settings_11), value);
	}

	inline static int32_t get_offset_of_shaderSettings_12() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___shaderSettings_12)); }
	inline ShaderProps_t3992238139 * get_shaderSettings_12() const { return ___shaderSettings_12; }
	inline ShaderProps_t3992238139 ** get_address_of_shaderSettings_12() { return &___shaderSettings_12; }
	inline void set_shaderSettings_12(ShaderProps_t3992238139 * value)
	{
		___shaderSettings_12 = value;
		Il2CppCodeGenWriteBarrier((&___shaderSettings_12), value);
	}

	inline static int32_t get_offset_of_wasConverted_13() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___wasConverted_13)); }
	inline bool get_wasConverted_13() const { return ___wasConverted_13; }
	inline bool* get_address_of_wasConverted_13() { return &___wasConverted_13; }
	inline void set_wasConverted_13(bool value)
	{
		___wasConverted_13 = value;
	}

	inline static int32_t get_offset_of_preConvertedScale_14() { return static_cast<int32_t>(offsetof(Shape_t3950352489, ___preConvertedScale_14)); }
	inline Vector3_t3722313464  get_preConvertedScale_14() const { return ___preConvertedScale_14; }
	inline Vector3_t3722313464 * get_address_of_preConvertedScale_14() { return &___preConvertedScale_14; }
	inline void set_preConvertedScale_14(Vector3_t3722313464  value)
	{
		___preConvertedScale_14 = value;
	}
};

struct Shape_t3950352489_StaticFields
{
public:
	// System.Int32 Shapes2D.Shape::MaxPolygonVertices
	int32_t ___MaxPolygonVertices_2;
	// System.Int32 Shapes2D.Shape::PolyMapResolution
	int32_t ___PolyMapResolution_3;
	// System.Int32 Shapes2D.Shape::MaxPathSegments
	int32_t ___MaxPathSegments_4;
	// System.String[] Shapes2D.Shape::_ShaderVertexVarNames
	StringU5BU5D_t1281789340* ____ShaderVertexVarNames_15;
	// System.String[] Shapes2D.Shape::_ShaderPointsVarNames
	StringU5BU5D_t1281789340* ____ShaderPointsVarNames_16;
	// System.Predicate`1<Shapes2D.Shape> Shapes2D.Shape::<>f__am$cache0
	Predicate_1_t480679317 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_MaxPolygonVertices_2() { return static_cast<int32_t>(offsetof(Shape_t3950352489_StaticFields, ___MaxPolygonVertices_2)); }
	inline int32_t get_MaxPolygonVertices_2() const { return ___MaxPolygonVertices_2; }
	inline int32_t* get_address_of_MaxPolygonVertices_2() { return &___MaxPolygonVertices_2; }
	inline void set_MaxPolygonVertices_2(int32_t value)
	{
		___MaxPolygonVertices_2 = value;
	}

	inline static int32_t get_offset_of_PolyMapResolution_3() { return static_cast<int32_t>(offsetof(Shape_t3950352489_StaticFields, ___PolyMapResolution_3)); }
	inline int32_t get_PolyMapResolution_3() const { return ___PolyMapResolution_3; }
	inline int32_t* get_address_of_PolyMapResolution_3() { return &___PolyMapResolution_3; }
	inline void set_PolyMapResolution_3(int32_t value)
	{
		___PolyMapResolution_3 = value;
	}

	inline static int32_t get_offset_of_MaxPathSegments_4() { return static_cast<int32_t>(offsetof(Shape_t3950352489_StaticFields, ___MaxPathSegments_4)); }
	inline int32_t get_MaxPathSegments_4() const { return ___MaxPathSegments_4; }
	inline int32_t* get_address_of_MaxPathSegments_4() { return &___MaxPathSegments_4; }
	inline void set_MaxPathSegments_4(int32_t value)
	{
		___MaxPathSegments_4 = value;
	}

	inline static int32_t get_offset_of__ShaderVertexVarNames_15() { return static_cast<int32_t>(offsetof(Shape_t3950352489_StaticFields, ____ShaderVertexVarNames_15)); }
	inline StringU5BU5D_t1281789340* get__ShaderVertexVarNames_15() const { return ____ShaderVertexVarNames_15; }
	inline StringU5BU5D_t1281789340** get_address_of__ShaderVertexVarNames_15() { return &____ShaderVertexVarNames_15; }
	inline void set__ShaderVertexVarNames_15(StringU5BU5D_t1281789340* value)
	{
		____ShaderVertexVarNames_15 = value;
		Il2CppCodeGenWriteBarrier((&____ShaderVertexVarNames_15), value);
	}

	inline static int32_t get_offset_of__ShaderPointsVarNames_16() { return static_cast<int32_t>(offsetof(Shape_t3950352489_StaticFields, ____ShaderPointsVarNames_16)); }
	inline StringU5BU5D_t1281789340* get__ShaderPointsVarNames_16() const { return ____ShaderPointsVarNames_16; }
	inline StringU5BU5D_t1281789340** get_address_of__ShaderPointsVarNames_16() { return &____ShaderPointsVarNames_16; }
	inline void set__ShaderPointsVarNames_16(StringU5BU5D_t1281789340* value)
	{
		____ShaderPointsVarNames_16 = value;
		Il2CppCodeGenWriteBarrier((&____ShaderPointsVarNames_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(Shape_t3950352489_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Predicate_1_t480679317 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Predicate_1_t480679317 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Predicate_1_t480679317 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPE_T3950352489_H
#ifndef CPAIHANGBANG_T2383002049_H
#define CPAIHANGBANG_T2383002049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBang
struct  CPaiHangBang_t2383002049  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CPaiHangBang::m_goPanel
	GameObject_t1113636619 * ___m_goPanel_2;
	// UnityEngine.GameObject CPaiHangBang::m_prePaiHangBangItem
	GameObject_t1113636619 * ___m_prePaiHangBangItem_3;
	// System.Single CPaiHangBang::m_fItemYGap
	float ___m_fItemYGap_4;
	// System.Single CPaiHangBang::m_fItemColGap
	float ___m_fItemColGap_5;
	// System.Int32 CPaiHangBang::m_nNumOfCol
	int32_t ___m_nNumOfCol_6;
	// UnityEngine.GameObject CPaiHangBang::m_goContainer
	GameObject_t1113636619 * ___m_goContainer_7;
	// System.Collections.Generic.List`1<CPaiHangBangItem> CPaiHangBang::m_lstItems
	List_1_t2754159026 * ___m_lstItems_11;
	// System.Single CPaiHangBang::c_fRefreshPlayerListInterval
	float ___c_fRefreshPlayerListInterval_12;
	// System.Single CPaiHangBang::m_fRefreshPlayerListTimeCount
	float ___m_fRefreshPlayerListTimeCount_13;
	// System.Collections.Generic.List`1<CPaiHangBang/sPlayerAccount> CPaiHangBang::lstTempPlayerAccount
	List_1_t230679895 * ___lstTempPlayerAccount_14;

public:
	inline static int32_t get_offset_of_m_goPanel_2() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_goPanel_2)); }
	inline GameObject_t1113636619 * get_m_goPanel_2() const { return ___m_goPanel_2; }
	inline GameObject_t1113636619 ** get_address_of_m_goPanel_2() { return &___m_goPanel_2; }
	inline void set_m_goPanel_2(GameObject_t1113636619 * value)
	{
		___m_goPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_goPanel_2), value);
	}

	inline static int32_t get_offset_of_m_prePaiHangBangItem_3() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_prePaiHangBangItem_3)); }
	inline GameObject_t1113636619 * get_m_prePaiHangBangItem_3() const { return ___m_prePaiHangBangItem_3; }
	inline GameObject_t1113636619 ** get_address_of_m_prePaiHangBangItem_3() { return &___m_prePaiHangBangItem_3; }
	inline void set_m_prePaiHangBangItem_3(GameObject_t1113636619 * value)
	{
		___m_prePaiHangBangItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePaiHangBangItem_3), value);
	}

	inline static int32_t get_offset_of_m_fItemYGap_4() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_fItemYGap_4)); }
	inline float get_m_fItemYGap_4() const { return ___m_fItemYGap_4; }
	inline float* get_address_of_m_fItemYGap_4() { return &___m_fItemYGap_4; }
	inline void set_m_fItemYGap_4(float value)
	{
		___m_fItemYGap_4 = value;
	}

	inline static int32_t get_offset_of_m_fItemColGap_5() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_fItemColGap_5)); }
	inline float get_m_fItemColGap_5() const { return ___m_fItemColGap_5; }
	inline float* get_address_of_m_fItemColGap_5() { return &___m_fItemColGap_5; }
	inline void set_m_fItemColGap_5(float value)
	{
		___m_fItemColGap_5 = value;
	}

	inline static int32_t get_offset_of_m_nNumOfCol_6() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_nNumOfCol_6)); }
	inline int32_t get_m_nNumOfCol_6() const { return ___m_nNumOfCol_6; }
	inline int32_t* get_address_of_m_nNumOfCol_6() { return &___m_nNumOfCol_6; }
	inline void set_m_nNumOfCol_6(int32_t value)
	{
		___m_nNumOfCol_6 = value;
	}

	inline static int32_t get_offset_of_m_goContainer_7() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_goContainer_7)); }
	inline GameObject_t1113636619 * get_m_goContainer_7() const { return ___m_goContainer_7; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainer_7() { return &___m_goContainer_7; }
	inline void set_m_goContainer_7(GameObject_t1113636619 * value)
	{
		___m_goContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainer_7), value);
	}

	inline static int32_t get_offset_of_m_lstItems_11() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_lstItems_11)); }
	inline List_1_t2754159026 * get_m_lstItems_11() const { return ___m_lstItems_11; }
	inline List_1_t2754159026 ** get_address_of_m_lstItems_11() { return &___m_lstItems_11; }
	inline void set_m_lstItems_11(List_1_t2754159026 * value)
	{
		___m_lstItems_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_11), value);
	}

	inline static int32_t get_offset_of_c_fRefreshPlayerListInterval_12() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___c_fRefreshPlayerListInterval_12)); }
	inline float get_c_fRefreshPlayerListInterval_12() const { return ___c_fRefreshPlayerListInterval_12; }
	inline float* get_address_of_c_fRefreshPlayerListInterval_12() { return &___c_fRefreshPlayerListInterval_12; }
	inline void set_c_fRefreshPlayerListInterval_12(float value)
	{
		___c_fRefreshPlayerListInterval_12 = value;
	}

	inline static int32_t get_offset_of_m_fRefreshPlayerListTimeCount_13() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___m_fRefreshPlayerListTimeCount_13)); }
	inline float get_m_fRefreshPlayerListTimeCount_13() const { return ___m_fRefreshPlayerListTimeCount_13; }
	inline float* get_address_of_m_fRefreshPlayerListTimeCount_13() { return &___m_fRefreshPlayerListTimeCount_13; }
	inline void set_m_fRefreshPlayerListTimeCount_13(float value)
	{
		___m_fRefreshPlayerListTimeCount_13 = value;
	}

	inline static int32_t get_offset_of_lstTempPlayerAccount_14() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049, ___lstTempPlayerAccount_14)); }
	inline List_1_t230679895 * get_lstTempPlayerAccount_14() const { return ___lstTempPlayerAccount_14; }
	inline List_1_t230679895 ** get_address_of_lstTempPlayerAccount_14() { return &___lstTempPlayerAccount_14; }
	inline void set_lstTempPlayerAccount_14(List_1_t230679895 * value)
	{
		___lstTempPlayerAccount_14 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempPlayerAccount_14), value);
	}
};

struct CPaiHangBang_t2383002049_StaticFields
{
public:
	// UnityEngine.Vector3 CPaiHangBang::vecTempPos
	Vector3_t3722313464  ___vecTempPos_8;
	// UnityEngine.Vector3 CPaiHangBang::vecTempScale
	Vector3_t3722313464  ___vecTempScale_9;
	// CPaiHangBang CPaiHangBang::s_Instance
	CPaiHangBang_t2383002049 * ___s_Instance_10;

public:
	inline static int32_t get_offset_of_vecTempPos_8() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049_StaticFields, ___vecTempPos_8)); }
	inline Vector3_t3722313464  get_vecTempPos_8() const { return ___vecTempPos_8; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_8() { return &___vecTempPos_8; }
	inline void set_vecTempPos_8(Vector3_t3722313464  value)
	{
		___vecTempPos_8 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_9() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049_StaticFields, ___vecTempScale_9)); }
	inline Vector3_t3722313464  get_vecTempScale_9() const { return ___vecTempScale_9; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_9() { return &___vecTempScale_9; }
	inline void set_vecTempScale_9(Vector3_t3722313464  value)
	{
		___vecTempScale_9 = value;
	}

	inline static int32_t get_offset_of_s_Instance_10() { return static_cast<int32_t>(offsetof(CPaiHangBang_t2383002049_StaticFields, ___s_Instance_10)); }
	inline CPaiHangBang_t2383002049 * get_s_Instance_10() const { return ___s_Instance_10; }
	inline CPaiHangBang_t2383002049 ** get_address_of_s_Instance_10() { return &___s_Instance_10; }
	inline void set_s_Instance_10(CPaiHangBang_t2383002049 * value)
	{
		___s_Instance_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPAIHANGBANG_T2383002049_H
#ifndef CBUYCONFIRMUI_T2670430472_H
#define CBUYCONFIRMUI_T2670430472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuyConfirmUI
struct  CBuyConfirmUI_t2670430472  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button[] CBuyConfirmUI::m_aryTimeLimitButtons
	ButtonU5BU5D_t2297175928* ___m_aryTimeLimitButtons_4;
	// UnityEngine.UI.Button CBuyConfirmUI::_btnBuy
	Button_t4055032469 * ____btnBuy_5;
	// UnityEngine.UI.Text CBuyConfirmUI::_txtCost
	Text_t1901882714 * ____txtCost_6;
	// UnityEngine.UI.Text CBuyConfirmUI::_txtCostType
	Text_t1901882714 * ____txtCostType_7;
	// UnityEngine.UI.Text CBuyConfirmUI::_txtGain
	Text_t1901882714 * ____txtGain_8;
	// UnityEngine.UI.Image CBuyConfirmUI::_imgItem
	Image_t2670269651 * ____imgItem_9;
	// System.UInt32 CBuyConfirmUI::m_nGainNum
	uint32_t ___m_nGainNum_10;
	// System.Int32 CBuyConfirmUI::m_nGainType
	int32_t ___m_nGainType_11;
	// System.String CBuyConfirmUI::m_szProductId
	String_t* ___m_szProductId_12;
	// System.Single CBuyConfirmUI::m_fAnimationScaleSpeed
	float ___m_fAnimationScaleSpeed_13;
	// System.Single CBuyConfirmUI::m_fAnimationScaleMin
	float ___m_fAnimationScaleMin_14;
	// System.Single CBuyConfirmUI::m_fAnimationScaleMax
	float ___m_fAnimationScaleMax_15;

public:
	inline static int32_t get_offset_of_m_aryTimeLimitButtons_4() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_aryTimeLimitButtons_4)); }
	inline ButtonU5BU5D_t2297175928* get_m_aryTimeLimitButtons_4() const { return ___m_aryTimeLimitButtons_4; }
	inline ButtonU5BU5D_t2297175928** get_address_of_m_aryTimeLimitButtons_4() { return &___m_aryTimeLimitButtons_4; }
	inline void set_m_aryTimeLimitButtons_4(ButtonU5BU5D_t2297175928* value)
	{
		___m_aryTimeLimitButtons_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTimeLimitButtons_4), value);
	}

	inline static int32_t get_offset_of__btnBuy_5() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ____btnBuy_5)); }
	inline Button_t4055032469 * get__btnBuy_5() const { return ____btnBuy_5; }
	inline Button_t4055032469 ** get_address_of__btnBuy_5() { return &____btnBuy_5; }
	inline void set__btnBuy_5(Button_t4055032469 * value)
	{
		____btnBuy_5 = value;
		Il2CppCodeGenWriteBarrier((&____btnBuy_5), value);
	}

	inline static int32_t get_offset_of__txtCost_6() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ____txtCost_6)); }
	inline Text_t1901882714 * get__txtCost_6() const { return ____txtCost_6; }
	inline Text_t1901882714 ** get_address_of__txtCost_6() { return &____txtCost_6; }
	inline void set__txtCost_6(Text_t1901882714 * value)
	{
		____txtCost_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtCost_6), value);
	}

	inline static int32_t get_offset_of__txtCostType_7() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ____txtCostType_7)); }
	inline Text_t1901882714 * get__txtCostType_7() const { return ____txtCostType_7; }
	inline Text_t1901882714 ** get_address_of__txtCostType_7() { return &____txtCostType_7; }
	inline void set__txtCostType_7(Text_t1901882714 * value)
	{
		____txtCostType_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtCostType_7), value);
	}

	inline static int32_t get_offset_of__txtGain_8() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ____txtGain_8)); }
	inline Text_t1901882714 * get__txtGain_8() const { return ____txtGain_8; }
	inline Text_t1901882714 ** get_address_of__txtGain_8() { return &____txtGain_8; }
	inline void set__txtGain_8(Text_t1901882714 * value)
	{
		____txtGain_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtGain_8), value);
	}

	inline static int32_t get_offset_of__imgItem_9() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ____imgItem_9)); }
	inline Image_t2670269651 * get__imgItem_9() const { return ____imgItem_9; }
	inline Image_t2670269651 ** get_address_of__imgItem_9() { return &____imgItem_9; }
	inline void set__imgItem_9(Image_t2670269651 * value)
	{
		____imgItem_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgItem_9), value);
	}

	inline static int32_t get_offset_of_m_nGainNum_10() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_nGainNum_10)); }
	inline uint32_t get_m_nGainNum_10() const { return ___m_nGainNum_10; }
	inline uint32_t* get_address_of_m_nGainNum_10() { return &___m_nGainNum_10; }
	inline void set_m_nGainNum_10(uint32_t value)
	{
		___m_nGainNum_10 = value;
	}

	inline static int32_t get_offset_of_m_nGainType_11() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_nGainType_11)); }
	inline int32_t get_m_nGainType_11() const { return ___m_nGainType_11; }
	inline int32_t* get_address_of_m_nGainType_11() { return &___m_nGainType_11; }
	inline void set_m_nGainType_11(int32_t value)
	{
		___m_nGainType_11 = value;
	}

	inline static int32_t get_offset_of_m_szProductId_12() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_szProductId_12)); }
	inline String_t* get_m_szProductId_12() const { return ___m_szProductId_12; }
	inline String_t** get_address_of_m_szProductId_12() { return &___m_szProductId_12; }
	inline void set_m_szProductId_12(String_t* value)
	{
		___m_szProductId_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_szProductId_12), value);
	}

	inline static int32_t get_offset_of_m_fAnimationScaleSpeed_13() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_fAnimationScaleSpeed_13)); }
	inline float get_m_fAnimationScaleSpeed_13() const { return ___m_fAnimationScaleSpeed_13; }
	inline float* get_address_of_m_fAnimationScaleSpeed_13() { return &___m_fAnimationScaleSpeed_13; }
	inline void set_m_fAnimationScaleSpeed_13(float value)
	{
		___m_fAnimationScaleSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_fAnimationScaleMin_14() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_fAnimationScaleMin_14)); }
	inline float get_m_fAnimationScaleMin_14() const { return ___m_fAnimationScaleMin_14; }
	inline float* get_address_of_m_fAnimationScaleMin_14() { return &___m_fAnimationScaleMin_14; }
	inline void set_m_fAnimationScaleMin_14(float value)
	{
		___m_fAnimationScaleMin_14 = value;
	}

	inline static int32_t get_offset_of_m_fAnimationScaleMax_15() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472, ___m_fAnimationScaleMax_15)); }
	inline float get_m_fAnimationScaleMax_15() const { return ___m_fAnimationScaleMax_15; }
	inline float* get_address_of_m_fAnimationScaleMax_15() { return &___m_fAnimationScaleMax_15; }
	inline void set_m_fAnimationScaleMax_15(float value)
	{
		___m_fAnimationScaleMax_15 = value;
	}
};

struct CBuyConfirmUI_t2670430472_StaticFields
{
public:
	// UnityEngine.Vector3 CBuyConfirmUI::vecTempScale
	Vector3_t3722313464  ___vecTempScale_2;
	// CBuyConfirmUI CBuyConfirmUI::s_Instance
	CBuyConfirmUI_t2670430472 * ___s_Instance_3;

public:
	inline static int32_t get_offset_of_vecTempScale_2() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472_StaticFields, ___vecTempScale_2)); }
	inline Vector3_t3722313464  get_vecTempScale_2() const { return ___vecTempScale_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_2() { return &___vecTempScale_2; }
	inline void set_vecTempScale_2(Vector3_t3722313464  value)
	{
		___vecTempScale_2 = value;
	}

	inline static int32_t get_offset_of_s_Instance_3() { return static_cast<int32_t>(offsetof(CBuyConfirmUI_t2670430472_StaticFields, ___s_Instance_3)); }
	inline CBuyConfirmUI_t2670430472 * get_s_Instance_3() const { return ___s_Instance_3; }
	inline CBuyConfirmUI_t2670430472 ** get_address_of_s_Instance_3() { return &___s_Instance_3; }
	inline void set_s_Instance_3(CBuyConfirmUI_t2670430472 * value)
	{
		___s_Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBUYCONFIRMUI_T2670430472_H
#ifndef CPAIHANGBANG_MOBILE_T896378859_H
#define CPAIHANGBANG_MOBILE_T896378859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBang_Mobile
struct  CPaiHangBang_Mobile_t896378859  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CPaiHangBang_Mobile::m_preItem
	GameObject_t1113636619 * ___m_preItem_3;
	// UnityEngine.GameObject CPaiHangBang_Mobile::m_goHighLight
	GameObject_t1113636619 * ___m_goHighLight_4;
	// UnityEngine.GameObject CPaiHangBang_Mobile::_panelPaiHangBang
	GameObject_t1113636619 * ____panelPaiHangBang_5;
	// UnityEngine.GameObject CPaiHangBang_Mobile::goList
	GameObject_t1113636619 * ___goList_6;
	// UnityEngine.GameObject CPaiHangBang_Mobile::goItemContainer
	GameObject_t1113636619 * ___goItemContainer_7;
	// System.Single CPaiHangBang_Mobile::m_fMoveSpeed
	float ___m_fMoveSpeed_8;
	// System.Single CPaiHangBang_Mobile::m_fVerticalSpace
	float ___m_fVerticalSpace_9;
	// System.Int32 CPaiHangBang_Mobile::m_nTotalNum
	int32_t ___m_nTotalNum_10;
	// System.Single CPaiHangBang_Mobile::m_fListHeight
	float ___m_fListHeight_11;
	// System.Boolean CPaiHangBang_Mobile::m_bMovingDown
	bool ___m_bMovingDown_12;
	// System.Boolean CPaiHangBang_Mobile::m_bMovingUp
	bool ___m_bMovingUp_13;
	// System.Collections.Generic.List`1<CPaiHangBangItem_Mobile> CPaiHangBang_Mobile::m_lstItems
	List_1_t2106334934 * ___m_lstItems_15;
	// CPaiHangBangRecord[] CPaiHangBang_Mobile::m_aryRecords
	CPaiHangBangRecordU5BU5D_t544191923* ___m_aryRecords_16;
	// UnityEngine.Sprite CPaiHangBang_Mobile::m_sprMainPlayer_Left
	Sprite_t280657092 * ___m_sprMainPlayer_Left_17;
	// UnityEngine.Sprite CPaiHangBang_Mobile::m_sprMainPlayer_Right
	Sprite_t280657092 * ___m_sprMainPlayer_Right_18;
	// UnityEngine.Sprite CPaiHangBang_Mobile::m_sprOtherClient_Left
	Sprite_t280657092 * ___m_sprOtherClient_Left_19;
	// UnityEngine.Sprite CPaiHangBang_Mobile::m_sprOtherClient_Right
	Sprite_t280657092 * ___m_sprOtherClient_Right_20;
	// System.Single CPaiHangBang_Mobile::m_fRefreshTimeCount
	float ___m_fRefreshTimeCount_21;
	// System.Single CPaiHangBang_Mobile::c_fRefreshPlayerListInterval
	float ___c_fRefreshPlayerListInterval_22;
	// System.Single CPaiHangBang_Mobile::m_fRefreshPlayerListTimeCount
	float ___m_fRefreshPlayerListTimeCount_23;
	// System.Collections.Generic.List`1<CPaiHangBang_Mobile/sPlayerAccount> CPaiHangBang_Mobile::lstTempPlayerAccount
	List_1_t1569742014 * ___lstTempPlayerAccount_24;
	// System.Collections.Generic.List`1<CPaiHangBang_Mobile/sPlayerAccount> CPaiHangBang_Mobile::m_lstFakePlayerInfo
	List_1_t1569742014 * ___m_lstFakePlayerInfo_25;
	// System.Int32 CPaiHangBang_Mobile::m_nMainPlayerRank
	int32_t ___m_nMainPlayerRank_27;
	// System.Collections.Generic.Dictionary`2<System.Int32,CPaiHangBang_Mobile/sPlayerAccount> CPaiHangBang_Mobile::m_dicPaiHangBangPlayerInfo
	Dictionary_2_t3281347899 * ___m_dicPaiHangBangPlayerInfo_28;

public:
	inline static int32_t get_offset_of_m_preItem_3() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_preItem_3)); }
	inline GameObject_t1113636619 * get_m_preItem_3() const { return ___m_preItem_3; }
	inline GameObject_t1113636619 ** get_address_of_m_preItem_3() { return &___m_preItem_3; }
	inline void set_m_preItem_3(GameObject_t1113636619 * value)
	{
		___m_preItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_preItem_3), value);
	}

	inline static int32_t get_offset_of_m_goHighLight_4() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_goHighLight_4)); }
	inline GameObject_t1113636619 * get_m_goHighLight_4() const { return ___m_goHighLight_4; }
	inline GameObject_t1113636619 ** get_address_of_m_goHighLight_4() { return &___m_goHighLight_4; }
	inline void set_m_goHighLight_4(GameObject_t1113636619 * value)
	{
		___m_goHighLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_goHighLight_4), value);
	}

	inline static int32_t get_offset_of__panelPaiHangBang_5() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ____panelPaiHangBang_5)); }
	inline GameObject_t1113636619 * get__panelPaiHangBang_5() const { return ____panelPaiHangBang_5; }
	inline GameObject_t1113636619 ** get_address_of__panelPaiHangBang_5() { return &____panelPaiHangBang_5; }
	inline void set__panelPaiHangBang_5(GameObject_t1113636619 * value)
	{
		____panelPaiHangBang_5 = value;
		Il2CppCodeGenWriteBarrier((&____panelPaiHangBang_5), value);
	}

	inline static int32_t get_offset_of_goList_6() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___goList_6)); }
	inline GameObject_t1113636619 * get_goList_6() const { return ___goList_6; }
	inline GameObject_t1113636619 ** get_address_of_goList_6() { return &___goList_6; }
	inline void set_goList_6(GameObject_t1113636619 * value)
	{
		___goList_6 = value;
		Il2CppCodeGenWriteBarrier((&___goList_6), value);
	}

	inline static int32_t get_offset_of_goItemContainer_7() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___goItemContainer_7)); }
	inline GameObject_t1113636619 * get_goItemContainer_7() const { return ___goItemContainer_7; }
	inline GameObject_t1113636619 ** get_address_of_goItemContainer_7() { return &___goItemContainer_7; }
	inline void set_goItemContainer_7(GameObject_t1113636619 * value)
	{
		___goItemContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___goItemContainer_7), value);
	}

	inline static int32_t get_offset_of_m_fMoveSpeed_8() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_fMoveSpeed_8)); }
	inline float get_m_fMoveSpeed_8() const { return ___m_fMoveSpeed_8; }
	inline float* get_address_of_m_fMoveSpeed_8() { return &___m_fMoveSpeed_8; }
	inline void set_m_fMoveSpeed_8(float value)
	{
		___m_fMoveSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_fVerticalSpace_9() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_fVerticalSpace_9)); }
	inline float get_m_fVerticalSpace_9() const { return ___m_fVerticalSpace_9; }
	inline float* get_address_of_m_fVerticalSpace_9() { return &___m_fVerticalSpace_9; }
	inline void set_m_fVerticalSpace_9(float value)
	{
		___m_fVerticalSpace_9 = value;
	}

	inline static int32_t get_offset_of_m_nTotalNum_10() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_nTotalNum_10)); }
	inline int32_t get_m_nTotalNum_10() const { return ___m_nTotalNum_10; }
	inline int32_t* get_address_of_m_nTotalNum_10() { return &___m_nTotalNum_10; }
	inline void set_m_nTotalNum_10(int32_t value)
	{
		___m_nTotalNum_10 = value;
	}

	inline static int32_t get_offset_of_m_fListHeight_11() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_fListHeight_11)); }
	inline float get_m_fListHeight_11() const { return ___m_fListHeight_11; }
	inline float* get_address_of_m_fListHeight_11() { return &___m_fListHeight_11; }
	inline void set_m_fListHeight_11(float value)
	{
		___m_fListHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_bMovingDown_12() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_bMovingDown_12)); }
	inline bool get_m_bMovingDown_12() const { return ___m_bMovingDown_12; }
	inline bool* get_address_of_m_bMovingDown_12() { return &___m_bMovingDown_12; }
	inline void set_m_bMovingDown_12(bool value)
	{
		___m_bMovingDown_12 = value;
	}

	inline static int32_t get_offset_of_m_bMovingUp_13() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_bMovingUp_13)); }
	inline bool get_m_bMovingUp_13() const { return ___m_bMovingUp_13; }
	inline bool* get_address_of_m_bMovingUp_13() { return &___m_bMovingUp_13; }
	inline void set_m_bMovingUp_13(bool value)
	{
		___m_bMovingUp_13 = value;
	}

	inline static int32_t get_offset_of_m_lstItems_15() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_lstItems_15)); }
	inline List_1_t2106334934 * get_m_lstItems_15() const { return ___m_lstItems_15; }
	inline List_1_t2106334934 ** get_address_of_m_lstItems_15() { return &___m_lstItems_15; }
	inline void set_m_lstItems_15(List_1_t2106334934 * value)
	{
		___m_lstItems_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_15), value);
	}

	inline static int32_t get_offset_of_m_aryRecords_16() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_aryRecords_16)); }
	inline CPaiHangBangRecordU5BU5D_t544191923* get_m_aryRecords_16() const { return ___m_aryRecords_16; }
	inline CPaiHangBangRecordU5BU5D_t544191923** get_address_of_m_aryRecords_16() { return &___m_aryRecords_16; }
	inline void set_m_aryRecords_16(CPaiHangBangRecordU5BU5D_t544191923* value)
	{
		___m_aryRecords_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRecords_16), value);
	}

	inline static int32_t get_offset_of_m_sprMainPlayer_Left_17() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_sprMainPlayer_Left_17)); }
	inline Sprite_t280657092 * get_m_sprMainPlayer_Left_17() const { return ___m_sprMainPlayer_Left_17; }
	inline Sprite_t280657092 ** get_address_of_m_sprMainPlayer_Left_17() { return &___m_sprMainPlayer_Left_17; }
	inline void set_m_sprMainPlayer_Left_17(Sprite_t280657092 * value)
	{
		___m_sprMainPlayer_Left_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMainPlayer_Left_17), value);
	}

	inline static int32_t get_offset_of_m_sprMainPlayer_Right_18() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_sprMainPlayer_Right_18)); }
	inline Sprite_t280657092 * get_m_sprMainPlayer_Right_18() const { return ___m_sprMainPlayer_Right_18; }
	inline Sprite_t280657092 ** get_address_of_m_sprMainPlayer_Right_18() { return &___m_sprMainPlayer_Right_18; }
	inline void set_m_sprMainPlayer_Right_18(Sprite_t280657092 * value)
	{
		___m_sprMainPlayer_Right_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMainPlayer_Right_18), value);
	}

	inline static int32_t get_offset_of_m_sprOtherClient_Left_19() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_sprOtherClient_Left_19)); }
	inline Sprite_t280657092 * get_m_sprOtherClient_Left_19() const { return ___m_sprOtherClient_Left_19; }
	inline Sprite_t280657092 ** get_address_of_m_sprOtherClient_Left_19() { return &___m_sprOtherClient_Left_19; }
	inline void set_m_sprOtherClient_Left_19(Sprite_t280657092 * value)
	{
		___m_sprOtherClient_Left_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprOtherClient_Left_19), value);
	}

	inline static int32_t get_offset_of_m_sprOtherClient_Right_20() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_sprOtherClient_Right_20)); }
	inline Sprite_t280657092 * get_m_sprOtherClient_Right_20() const { return ___m_sprOtherClient_Right_20; }
	inline Sprite_t280657092 ** get_address_of_m_sprOtherClient_Right_20() { return &___m_sprOtherClient_Right_20; }
	inline void set_m_sprOtherClient_Right_20(Sprite_t280657092 * value)
	{
		___m_sprOtherClient_Right_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprOtherClient_Right_20), value);
	}

	inline static int32_t get_offset_of_m_fRefreshTimeCount_21() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_fRefreshTimeCount_21)); }
	inline float get_m_fRefreshTimeCount_21() const { return ___m_fRefreshTimeCount_21; }
	inline float* get_address_of_m_fRefreshTimeCount_21() { return &___m_fRefreshTimeCount_21; }
	inline void set_m_fRefreshTimeCount_21(float value)
	{
		___m_fRefreshTimeCount_21 = value;
	}

	inline static int32_t get_offset_of_c_fRefreshPlayerListInterval_22() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___c_fRefreshPlayerListInterval_22)); }
	inline float get_c_fRefreshPlayerListInterval_22() const { return ___c_fRefreshPlayerListInterval_22; }
	inline float* get_address_of_c_fRefreshPlayerListInterval_22() { return &___c_fRefreshPlayerListInterval_22; }
	inline void set_c_fRefreshPlayerListInterval_22(float value)
	{
		___c_fRefreshPlayerListInterval_22 = value;
	}

	inline static int32_t get_offset_of_m_fRefreshPlayerListTimeCount_23() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_fRefreshPlayerListTimeCount_23)); }
	inline float get_m_fRefreshPlayerListTimeCount_23() const { return ___m_fRefreshPlayerListTimeCount_23; }
	inline float* get_address_of_m_fRefreshPlayerListTimeCount_23() { return &___m_fRefreshPlayerListTimeCount_23; }
	inline void set_m_fRefreshPlayerListTimeCount_23(float value)
	{
		___m_fRefreshPlayerListTimeCount_23 = value;
	}

	inline static int32_t get_offset_of_lstTempPlayerAccount_24() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___lstTempPlayerAccount_24)); }
	inline List_1_t1569742014 * get_lstTempPlayerAccount_24() const { return ___lstTempPlayerAccount_24; }
	inline List_1_t1569742014 ** get_address_of_lstTempPlayerAccount_24() { return &___lstTempPlayerAccount_24; }
	inline void set_lstTempPlayerAccount_24(List_1_t1569742014 * value)
	{
		___lstTempPlayerAccount_24 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempPlayerAccount_24), value);
	}

	inline static int32_t get_offset_of_m_lstFakePlayerInfo_25() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_lstFakePlayerInfo_25)); }
	inline List_1_t1569742014 * get_m_lstFakePlayerInfo_25() const { return ___m_lstFakePlayerInfo_25; }
	inline List_1_t1569742014 ** get_address_of_m_lstFakePlayerInfo_25() { return &___m_lstFakePlayerInfo_25; }
	inline void set_m_lstFakePlayerInfo_25(List_1_t1569742014 * value)
	{
		___m_lstFakePlayerInfo_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstFakePlayerInfo_25), value);
	}

	inline static int32_t get_offset_of_m_nMainPlayerRank_27() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_nMainPlayerRank_27)); }
	inline int32_t get_m_nMainPlayerRank_27() const { return ___m_nMainPlayerRank_27; }
	inline int32_t* get_address_of_m_nMainPlayerRank_27() { return &___m_nMainPlayerRank_27; }
	inline void set_m_nMainPlayerRank_27(int32_t value)
	{
		___m_nMainPlayerRank_27 = value;
	}

	inline static int32_t get_offset_of_m_dicPaiHangBangPlayerInfo_28() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859, ___m_dicPaiHangBangPlayerInfo_28)); }
	inline Dictionary_2_t3281347899 * get_m_dicPaiHangBangPlayerInfo_28() const { return ___m_dicPaiHangBangPlayerInfo_28; }
	inline Dictionary_2_t3281347899 ** get_address_of_m_dicPaiHangBangPlayerInfo_28() { return &___m_dicPaiHangBangPlayerInfo_28; }
	inline void set_m_dicPaiHangBangPlayerInfo_28(Dictionary_2_t3281347899 * value)
	{
		___m_dicPaiHangBangPlayerInfo_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPaiHangBangPlayerInfo_28), value);
	}
};

struct CPaiHangBang_Mobile_t896378859_StaticFields
{
public:
	// CPaiHangBang_Mobile CPaiHangBang_Mobile::s_Instance
	CPaiHangBang_Mobile_t896378859 * ___s_Instance_2;
	// UnityEngine.Vector3 CPaiHangBang_Mobile::vecTemp
	Vector3_t3722313464  ___vecTemp_14;
	// System.Int32 CPaiHangBang_Mobile::s_nFakePlayerId
	int32_t ___s_nFakePlayerId_26;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859_StaticFields, ___s_Instance_2)); }
	inline CPaiHangBang_Mobile_t896378859 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CPaiHangBang_Mobile_t896378859 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CPaiHangBang_Mobile_t896378859 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTemp_14() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859_StaticFields, ___vecTemp_14)); }
	inline Vector3_t3722313464  get_vecTemp_14() const { return ___vecTemp_14; }
	inline Vector3_t3722313464 * get_address_of_vecTemp_14() { return &___vecTemp_14; }
	inline void set_vecTemp_14(Vector3_t3722313464  value)
	{
		___vecTemp_14 = value;
	}

	inline static int32_t get_offset_of_s_nFakePlayerId_26() { return static_cast<int32_t>(offsetof(CPaiHangBang_Mobile_t896378859_StaticFields, ___s_nFakePlayerId_26)); }
	inline int32_t get_s_nFakePlayerId_26() const { return ___s_nFakePlayerId_26; }
	inline int32_t* get_address_of_s_nFakePlayerId_26() { return &___s_nFakePlayerId_26; }
	inline void set_s_nFakePlayerId_26(int32_t value)
	{
		___s_nFakePlayerId_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPAIHANGBANG_MOBILE_T896378859_H
#ifndef CBEANCOLLECTION_T2572073806_H
#define CBEANCOLLECTION_T2572073806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBeanCollection
struct  CBeanCollection_t2572073806  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider2D CBeanCollection::_colliderMain
	Collider2D_t2806799626 * ____colliderMain_2;
	// UnityEngine.SpriteRenderer CBeanCollection::_srMain
	SpriteRenderer_t3235626157 * ____srMain_3;
	// System.Collections.Generic.List`1<CBeanCollection/sBeanInfo> CBeanCollection::m_lstBeanPos
	List_1_t323162583 * ___m_lstBeanPos_4;
	// System.Collections.Generic.List`1<Ball> CBeanCollection::m_lstInteractingBall
	List_1_t3678741308 * ___m_lstInteractingBall_5;
	// System.Int32 CBeanCollection::m_nCollectionGuid
	int32_t ___m_nCollectionGuid_10;
	// System.Collections.Generic.List`1<UnityEngine.BoxCollider2D> CBeanCollection::m_lstBeanColliders
	List_1_t758449277 * ___m_lstBeanColliders_11;
	// System.String CBeanCollection::m_szCollectionGuid
	String_t* ___m_szCollectionGuid_12;
	// System.Int32 CBeanCollection::m_nIndex
	int32_t ___m_nIndex_13;
	// System.Boolean CBeanCollection::m_bInit
	bool ___m_bInit_14;
	// System.Int32 CBeanCollection::m_nBeanCount
	int32_t ___m_nBeanCount_15;
	// UnityEngine.Vector2 CBeanCollection::m_vecLeftTopPos
	Vector2_t2156229523  ___m_vecLeftTopPos_16;
	// UnityEngine.Vector2 CBeanCollection::m_vecBottomRight
	Vector2_t2156229523  ___m_vecBottomRight_17;
	// System.Boolean CBeanCollection::m_bActive
	bool ___m_bActive_18;

public:
	inline static int32_t get_offset_of__colliderMain_2() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ____colliderMain_2)); }
	inline Collider2D_t2806799626 * get__colliderMain_2() const { return ____colliderMain_2; }
	inline Collider2D_t2806799626 ** get_address_of__colliderMain_2() { return &____colliderMain_2; }
	inline void set__colliderMain_2(Collider2D_t2806799626 * value)
	{
		____colliderMain_2 = value;
		Il2CppCodeGenWriteBarrier((&____colliderMain_2), value);
	}

	inline static int32_t get_offset_of__srMain_3() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ____srMain_3)); }
	inline SpriteRenderer_t3235626157 * get__srMain_3() const { return ____srMain_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_3() { return &____srMain_3; }
	inline void set__srMain_3(SpriteRenderer_t3235626157 * value)
	{
		____srMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_3), value);
	}

	inline static int32_t get_offset_of_m_lstBeanPos_4() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_lstBeanPos_4)); }
	inline List_1_t323162583 * get_m_lstBeanPos_4() const { return ___m_lstBeanPos_4; }
	inline List_1_t323162583 ** get_address_of_m_lstBeanPos_4() { return &___m_lstBeanPos_4; }
	inline void set_m_lstBeanPos_4(List_1_t323162583 * value)
	{
		___m_lstBeanPos_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeanPos_4), value);
	}

	inline static int32_t get_offset_of_m_lstInteractingBall_5() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_lstInteractingBall_5)); }
	inline List_1_t3678741308 * get_m_lstInteractingBall_5() const { return ___m_lstInteractingBall_5; }
	inline List_1_t3678741308 ** get_address_of_m_lstInteractingBall_5() { return &___m_lstInteractingBall_5; }
	inline void set_m_lstInteractingBall_5(List_1_t3678741308 * value)
	{
		___m_lstInteractingBall_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstInteractingBall_5), value);
	}

	inline static int32_t get_offset_of_m_nCollectionGuid_10() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_nCollectionGuid_10)); }
	inline int32_t get_m_nCollectionGuid_10() const { return ___m_nCollectionGuid_10; }
	inline int32_t* get_address_of_m_nCollectionGuid_10() { return &___m_nCollectionGuid_10; }
	inline void set_m_nCollectionGuid_10(int32_t value)
	{
		___m_nCollectionGuid_10 = value;
	}

	inline static int32_t get_offset_of_m_lstBeanColliders_11() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_lstBeanColliders_11)); }
	inline List_1_t758449277 * get_m_lstBeanColliders_11() const { return ___m_lstBeanColliders_11; }
	inline List_1_t758449277 ** get_address_of_m_lstBeanColliders_11() { return &___m_lstBeanColliders_11; }
	inline void set_m_lstBeanColliders_11(List_1_t758449277 * value)
	{
		___m_lstBeanColliders_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBeanColliders_11), value);
	}

	inline static int32_t get_offset_of_m_szCollectionGuid_12() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_szCollectionGuid_12)); }
	inline String_t* get_m_szCollectionGuid_12() const { return ___m_szCollectionGuid_12; }
	inline String_t** get_address_of_m_szCollectionGuid_12() { return &___m_szCollectionGuid_12; }
	inline void set_m_szCollectionGuid_12(String_t* value)
	{
		___m_szCollectionGuid_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCollectionGuid_12), value);
	}

	inline static int32_t get_offset_of_m_nIndex_13() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_nIndex_13)); }
	inline int32_t get_m_nIndex_13() const { return ___m_nIndex_13; }
	inline int32_t* get_address_of_m_nIndex_13() { return &___m_nIndex_13; }
	inline void set_m_nIndex_13(int32_t value)
	{
		___m_nIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_bInit_14() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_bInit_14)); }
	inline bool get_m_bInit_14() const { return ___m_bInit_14; }
	inline bool* get_address_of_m_bInit_14() { return &___m_bInit_14; }
	inline void set_m_bInit_14(bool value)
	{
		___m_bInit_14 = value;
	}

	inline static int32_t get_offset_of_m_nBeanCount_15() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_nBeanCount_15)); }
	inline int32_t get_m_nBeanCount_15() const { return ___m_nBeanCount_15; }
	inline int32_t* get_address_of_m_nBeanCount_15() { return &___m_nBeanCount_15; }
	inline void set_m_nBeanCount_15(int32_t value)
	{
		___m_nBeanCount_15 = value;
	}

	inline static int32_t get_offset_of_m_vecLeftTopPos_16() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_vecLeftTopPos_16)); }
	inline Vector2_t2156229523  get_m_vecLeftTopPos_16() const { return ___m_vecLeftTopPos_16; }
	inline Vector2_t2156229523 * get_address_of_m_vecLeftTopPos_16() { return &___m_vecLeftTopPos_16; }
	inline void set_m_vecLeftTopPos_16(Vector2_t2156229523  value)
	{
		___m_vecLeftTopPos_16 = value;
	}

	inline static int32_t get_offset_of_m_vecBottomRight_17() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_vecBottomRight_17)); }
	inline Vector2_t2156229523  get_m_vecBottomRight_17() const { return ___m_vecBottomRight_17; }
	inline Vector2_t2156229523 * get_address_of_m_vecBottomRight_17() { return &___m_vecBottomRight_17; }
	inline void set_m_vecBottomRight_17(Vector2_t2156229523  value)
	{
		___m_vecBottomRight_17 = value;
	}

	inline static int32_t get_offset_of_m_bActive_18() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806, ___m_bActive_18)); }
	inline bool get_m_bActive_18() const { return ___m_bActive_18; }
	inline bool* get_address_of_m_bActive_18() { return &___m_bActive_18; }
	inline void set_m_bActive_18(bool value)
	{
		___m_bActive_18 = value;
	}
};

struct CBeanCollection_t2572073806_StaticFields
{
public:
	// UnityEngine.Vector2 CBeanCollection::vec2Pivot
	Vector2_t2156229523  ___vec2Pivot_6;
	// UnityEngine.Vector2 CBeanCollection::vec2TempPos
	Vector2_t2156229523  ___vec2TempPos_7;
	// UnityEngine.Vector2 CBeanCollection::vec2TempScale
	Vector2_t2156229523  ___vec2TempScale_8;
	// UnityEngine.Rect CBeanCollection::rectTemp
	Rect_t2360479859  ___rectTemp_9;

public:
	inline static int32_t get_offset_of_vec2Pivot_6() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806_StaticFields, ___vec2Pivot_6)); }
	inline Vector2_t2156229523  get_vec2Pivot_6() const { return ___vec2Pivot_6; }
	inline Vector2_t2156229523 * get_address_of_vec2Pivot_6() { return &___vec2Pivot_6; }
	inline void set_vec2Pivot_6(Vector2_t2156229523  value)
	{
		___vec2Pivot_6 = value;
	}

	inline static int32_t get_offset_of_vec2TempPos_7() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806_StaticFields, ___vec2TempPos_7)); }
	inline Vector2_t2156229523  get_vec2TempPos_7() const { return ___vec2TempPos_7; }
	inline Vector2_t2156229523 * get_address_of_vec2TempPos_7() { return &___vec2TempPos_7; }
	inline void set_vec2TempPos_7(Vector2_t2156229523  value)
	{
		___vec2TempPos_7 = value;
	}

	inline static int32_t get_offset_of_vec2TempScale_8() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806_StaticFields, ___vec2TempScale_8)); }
	inline Vector2_t2156229523  get_vec2TempScale_8() const { return ___vec2TempScale_8; }
	inline Vector2_t2156229523 * get_address_of_vec2TempScale_8() { return &___vec2TempScale_8; }
	inline void set_vec2TempScale_8(Vector2_t2156229523  value)
	{
		___vec2TempScale_8 = value;
	}

	inline static int32_t get_offset_of_rectTemp_9() { return static_cast<int32_t>(offsetof(CBeanCollection_t2572073806_StaticFields, ___rectTemp_9)); }
	inline Rect_t2360479859  get_rectTemp_9() const { return ___rectTemp_9; }
	inline Rect_t2360479859 * get_address_of_rectTemp_9() { return &___rectTemp_9; }
	inline void set_rectTemp_9(Rect_t2360479859  value)
	{
		___rectTemp_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBEANCOLLECTION_T2572073806_H
#ifndef CPAIHANGBANGITEM_T1282084284_H
#define CPAIHANGBANGITEM_T1282084284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBangItem
struct  CPaiHangBangItem_t1282084284  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CPaiHangBangItem::_txtName
	Text_t1901882714 * ____txtName_2;
	// UnityEngine.UI.Text CPaiHangBangItem::_txtNo
	Text_t1901882714 * ____txtNo_3;
	// UnityEngine.UI.Text CPaiHangBangItem::_txtLevel
	Text_t1901882714 * ____txtLevel_4;
	// UnityEngine.UI.Text CPaiHangBangItem::_txtTotalVolume
	Text_t1901882714 * ____txtTotalVolume_5;
	// UnityEngine.UI.Text CPaiHangBangItem::_txtExplodeThornNum
	Text_t1901882714 * ____txtExplodeThornNum_6;
	// UnityEngine.UI.Text CPaiHangBangItem::_txtJiShaInfo
	Text_t1901882714 * ____txtJiShaInfo_7;
	// UnityEngine.UI.Text[] CPaiHangBangItem::_aryItemNum
	TextU5BU5D_t422084607* ____aryItemNum_8;

public:
	inline static int32_t get_offset_of__txtName_2() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____txtName_2)); }
	inline Text_t1901882714 * get__txtName_2() const { return ____txtName_2; }
	inline Text_t1901882714 ** get_address_of__txtName_2() { return &____txtName_2; }
	inline void set__txtName_2(Text_t1901882714 * value)
	{
		____txtName_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_2), value);
	}

	inline static int32_t get_offset_of__txtNo_3() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____txtNo_3)); }
	inline Text_t1901882714 * get__txtNo_3() const { return ____txtNo_3; }
	inline Text_t1901882714 ** get_address_of__txtNo_3() { return &____txtNo_3; }
	inline void set__txtNo_3(Text_t1901882714 * value)
	{
		____txtNo_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtNo_3), value);
	}

	inline static int32_t get_offset_of__txtLevel_4() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____txtLevel_4)); }
	inline Text_t1901882714 * get__txtLevel_4() const { return ____txtLevel_4; }
	inline Text_t1901882714 ** get_address_of__txtLevel_4() { return &____txtLevel_4; }
	inline void set__txtLevel_4(Text_t1901882714 * value)
	{
		____txtLevel_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtLevel_4), value);
	}

	inline static int32_t get_offset_of__txtTotalVolume_5() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____txtTotalVolume_5)); }
	inline Text_t1901882714 * get__txtTotalVolume_5() const { return ____txtTotalVolume_5; }
	inline Text_t1901882714 ** get_address_of__txtTotalVolume_5() { return &____txtTotalVolume_5; }
	inline void set__txtTotalVolume_5(Text_t1901882714 * value)
	{
		____txtTotalVolume_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalVolume_5), value);
	}

	inline static int32_t get_offset_of__txtExplodeThornNum_6() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____txtExplodeThornNum_6)); }
	inline Text_t1901882714 * get__txtExplodeThornNum_6() const { return ____txtExplodeThornNum_6; }
	inline Text_t1901882714 ** get_address_of__txtExplodeThornNum_6() { return &____txtExplodeThornNum_6; }
	inline void set__txtExplodeThornNum_6(Text_t1901882714 * value)
	{
		____txtExplodeThornNum_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtExplodeThornNum_6), value);
	}

	inline static int32_t get_offset_of__txtJiShaInfo_7() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____txtJiShaInfo_7)); }
	inline Text_t1901882714 * get__txtJiShaInfo_7() const { return ____txtJiShaInfo_7; }
	inline Text_t1901882714 ** get_address_of__txtJiShaInfo_7() { return &____txtJiShaInfo_7; }
	inline void set__txtJiShaInfo_7(Text_t1901882714 * value)
	{
		____txtJiShaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtJiShaInfo_7), value);
	}

	inline static int32_t get_offset_of__aryItemNum_8() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_t1282084284, ____aryItemNum_8)); }
	inline TextU5BU5D_t422084607* get__aryItemNum_8() const { return ____aryItemNum_8; }
	inline TextU5BU5D_t422084607** get_address_of__aryItemNum_8() { return &____aryItemNum_8; }
	inline void set__aryItemNum_8(TextU5BU5D_t422084607* value)
	{
		____aryItemNum_8 = value;
		Il2CppCodeGenWriteBarrier((&____aryItemNum_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPAIHANGBANGITEM_T1282084284_H
#ifndef CPAIHANGBANGITEM_MOBILE_T634260192_H
#define CPAIHANGBANGITEM_MOBILE_T634260192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPaiHangBangItem_Mobile
struct  CPaiHangBangItem_Mobile_t634260192  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CPaiHangBangItem_Mobile::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_2;

public:
	inline static int32_t get_offset_of__txtPlayerName_2() { return static_cast<int32_t>(offsetof(CPaiHangBangItem_Mobile_t634260192, ____txtPlayerName_2)); }
	inline Text_t1901882714 * get__txtPlayerName_2() const { return ____txtPlayerName_2; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_2() { return &____txtPlayerName_2; }
	inline void set__txtPlayerName_2(Text_t1901882714 * value)
	{
		____txtPlayerName_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPAIHANGBANGITEM_MOBILE_T634260192_H
#ifndef CBUYRESULT_T3313452107_H
#define CBUYRESULT_T3313452107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBuyResult
struct  CBuyResult_t3313452107  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CBuyResult::_goContainerAll
	GameObject_t1113636619 * ____goContainerAll_3;
	// UnityEngine.UI.Text CBuyResult::_txtInfo
	Text_t1901882714 * ____txtInfo_4;
	// UnityEngine.UI.Text CBuyResult::_txtItemName
	Text_t1901882714 * ____txtItemName_5;
	// UnityEngine.UI.Image CBuyResult::_imgItemAvatar
	Image_t2670269651 * ____imgItemAvatar_6;

public:
	inline static int32_t get_offset_of__goContainerAll_3() { return static_cast<int32_t>(offsetof(CBuyResult_t3313452107, ____goContainerAll_3)); }
	inline GameObject_t1113636619 * get__goContainerAll_3() const { return ____goContainerAll_3; }
	inline GameObject_t1113636619 ** get_address_of__goContainerAll_3() { return &____goContainerAll_3; }
	inline void set__goContainerAll_3(GameObject_t1113636619 * value)
	{
		____goContainerAll_3 = value;
		Il2CppCodeGenWriteBarrier((&____goContainerAll_3), value);
	}

	inline static int32_t get_offset_of__txtInfo_4() { return static_cast<int32_t>(offsetof(CBuyResult_t3313452107, ____txtInfo_4)); }
	inline Text_t1901882714 * get__txtInfo_4() const { return ____txtInfo_4; }
	inline Text_t1901882714 ** get_address_of__txtInfo_4() { return &____txtInfo_4; }
	inline void set__txtInfo_4(Text_t1901882714 * value)
	{
		____txtInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtInfo_4), value);
	}

	inline static int32_t get_offset_of__txtItemName_5() { return static_cast<int32_t>(offsetof(CBuyResult_t3313452107, ____txtItemName_5)); }
	inline Text_t1901882714 * get__txtItemName_5() const { return ____txtItemName_5; }
	inline Text_t1901882714 ** get_address_of__txtItemName_5() { return &____txtItemName_5; }
	inline void set__txtItemName_5(Text_t1901882714 * value)
	{
		____txtItemName_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtItemName_5), value);
	}

	inline static int32_t get_offset_of__imgItemAvatar_6() { return static_cast<int32_t>(offsetof(CBuyResult_t3313452107, ____imgItemAvatar_6)); }
	inline Image_t2670269651 * get__imgItemAvatar_6() const { return ____imgItemAvatar_6; }
	inline Image_t2670269651 ** get_address_of__imgItemAvatar_6() { return &____imgItemAvatar_6; }
	inline void set__imgItemAvatar_6(Image_t2670269651 * value)
	{
		____imgItemAvatar_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgItemAvatar_6), value);
	}
};

struct CBuyResult_t3313452107_StaticFields
{
public:
	// CBuyResult CBuyResult::s_Instance
	CBuyResult_t3313452107 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CBuyResult_t3313452107_StaticFields, ___s_Instance_2)); }
	inline CBuyResult_t3313452107 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CBuyResult_t3313452107 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CBuyResult_t3313452107 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBUYRESULT_T3313452107_H
#ifndef CSPRAY_T348869720_H
#define CSPRAY_T348869720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpray
struct  CSpray_t348869720  : public CPveObj_t2172873472
{
public:
	// CSpryEditor/sSprayConfig CSpray::m_Config
	sSprayConfig_t1865836932  ___m_Config_8;
	// UnityEngine.GameObject CSpray::m_goMonsterContainer
	GameObject_t1113636619 * ___m_goMonsterContainer_9;
	// System.Single CSpray::m_fSprayTimeCount
	float ___m_fSprayTimeCount_10;
	// System.Collections.Generic.List`1<System.Single> CSpray::m_lstLifeTimeLoop_SprayTime
	List_1_t2869341516 * ___m_lstLifeTimeLoop_SprayTime_11;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<CMonster>> CSpray::m_lstLifeTimeLoop
	List_1_t590653126 * ___m_lstLifeTimeLoop_12;

public:
	inline static int32_t get_offset_of_m_Config_8() { return static_cast<int32_t>(offsetof(CSpray_t348869720, ___m_Config_8)); }
	inline sSprayConfig_t1865836932  get_m_Config_8() const { return ___m_Config_8; }
	inline sSprayConfig_t1865836932 * get_address_of_m_Config_8() { return &___m_Config_8; }
	inline void set_m_Config_8(sSprayConfig_t1865836932  value)
	{
		___m_Config_8 = value;
	}

	inline static int32_t get_offset_of_m_goMonsterContainer_9() { return static_cast<int32_t>(offsetof(CSpray_t348869720, ___m_goMonsterContainer_9)); }
	inline GameObject_t1113636619 * get_m_goMonsterContainer_9() const { return ___m_goMonsterContainer_9; }
	inline GameObject_t1113636619 ** get_address_of_m_goMonsterContainer_9() { return &___m_goMonsterContainer_9; }
	inline void set_m_goMonsterContainer_9(GameObject_t1113636619 * value)
	{
		___m_goMonsterContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_goMonsterContainer_9), value);
	}

	inline static int32_t get_offset_of_m_fSprayTimeCount_10() { return static_cast<int32_t>(offsetof(CSpray_t348869720, ___m_fSprayTimeCount_10)); }
	inline float get_m_fSprayTimeCount_10() const { return ___m_fSprayTimeCount_10; }
	inline float* get_address_of_m_fSprayTimeCount_10() { return &___m_fSprayTimeCount_10; }
	inline void set_m_fSprayTimeCount_10(float value)
	{
		___m_fSprayTimeCount_10 = value;
	}

	inline static int32_t get_offset_of_m_lstLifeTimeLoop_SprayTime_11() { return static_cast<int32_t>(offsetof(CSpray_t348869720, ___m_lstLifeTimeLoop_SprayTime_11)); }
	inline List_1_t2869341516 * get_m_lstLifeTimeLoop_SprayTime_11() const { return ___m_lstLifeTimeLoop_SprayTime_11; }
	inline List_1_t2869341516 ** get_address_of_m_lstLifeTimeLoop_SprayTime_11() { return &___m_lstLifeTimeLoop_SprayTime_11; }
	inline void set_m_lstLifeTimeLoop_SprayTime_11(List_1_t2869341516 * value)
	{
		___m_lstLifeTimeLoop_SprayTime_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstLifeTimeLoop_SprayTime_11), value);
	}

	inline static int32_t get_offset_of_m_lstLifeTimeLoop_12() { return static_cast<int32_t>(offsetof(CSpray_t348869720, ___m_lstLifeTimeLoop_12)); }
	inline List_1_t590653126 * get_m_lstLifeTimeLoop_12() const { return ___m_lstLifeTimeLoop_12; }
	inline List_1_t590653126 ** get_address_of_m_lstLifeTimeLoop_12() { return &___m_lstLifeTimeLoop_12; }
	inline void set_m_lstLifeTimeLoop_12(List_1_t590653126 * value)
	{
		___m_lstLifeTimeLoop_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstLifeTimeLoop_12), value);
	}
};

struct CSpray_t348869720_StaticFields
{
public:
	// UnityEngine.Vector3 CSpray::vecTempPos
	Vector3_t3722313464  ___vecTempPos_7;

public:
	inline static int32_t get_offset_of_vecTempPos_7() { return static_cast<int32_t>(offsetof(CSpray_t348869720_StaticFields, ___vecTempPos_7)); }
	inline Vector3_t3722313464  get_vecTempPos_7() const { return ___vecTempPos_7; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_7() { return &___vecTempPos_7; }
	inline void set_vecTempPos_7(Vector3_t3722313464  value)
	{
		___vecTempPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPRAY_T348869720_H
#ifndef CPOINTLIGHT_T2898614150_H
#define CPOINTLIGHT_T2898614150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPointLight
struct  CPointLight_t2898614150  : public CLight_t1535733186
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPOINTLIGHT_T2898614150_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef CMONSTERBALL_T654437011_H
#define CMONSTERBALL_T654437011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterBall
struct  CMonsterBall_t654437011  : public CPveObj_t2172873472
{
public:
	// Ball CMonsterBall::_ball
	Ball_t2206666566 * ____ball_7;

public:
	inline static int32_t get_offset_of__ball_7() { return static_cast<int32_t>(offsetof(CMonsterBall_t654437011, ____ball_7)); }
	inline Ball_t2206666566 * get__ball_7() const { return ____ball_7; }
	inline Ball_t2206666566 ** get_address_of__ball_7() { return &____ball_7; }
	inline void set__ball_7(Ball_t2206666566 * value)
	{
		____ball_7 = value;
		Il2CppCodeGenWriteBarrier((&____ball_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMONSTERBALL_T654437011_H
#ifndef CSHOPPINGCOUNTER_T3637302240_H
#define CSHOPPINGCOUNTER_T3637302240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CShoppingCounter
struct  CShoppingCounter_t3637302240  : public CCyberTreeListItem_t2642449450
{
public:
	// ShoppingMallManager/eMoneyType CShoppingCounter::m_eMoneyType
	int32_t ___m_eMoneyType_6;
	// System.Int32 CShoppingCounter::m_nMoneyValue
	int32_t ___m_nMoneyValue_7;
	// System.Boolean CShoppingCounter::m_bBought
	bool ___m_bBought_8;
	// System.String CShoppingCounter::m_szItemName
	String_t* ___m_szItemName_9;
	// System.Int32 CShoppingCounter::m_nItemId
	int32_t ___m_nItemId_10;
	// System.String CShoppingCounter::m_szItemDesc
	String_t* ___m_szItemDesc_11;
	// System.String CShoppingCounter::m_szItemPath
	String_t* ___m_szItemPath_12;
	// UnityEngine.UI.Image CShoppingCounter::m_imgBg
	Image_t2670269651 * ___m_imgBg_13;
	// UnityEngine.UI.Button CShoppingCounter::m_btnBuyAndEquip
	Button_t4055032469 * ___m_btnBuyAndEquip_14;
	// UnityEngine.UI.Image CShoppingCounter::m_imgStatus
	Image_t2670269651 * ___m_imgStatus_15;
	// UnityEngine.UI.Image CShoppingCounter::m_imgSkin
	Image_t2670269651 * ___m_imgSkin_16;
	// UnityEngine.UI.Image CShoppingCounter::m_imgMoneyType
	Image_t2670269651 * ___m_imgMoneyType_17;
	// UnityEngine.UI.Image CShoppingCounter::m_imgMoneyBg
	Image_t2670269651 * ___m_imgMoneyBg_18;
	// UnityEngine.UI.Text CShoppingCounter::m_txtMoneyValue
	Text_t1901882714 * ___m_txtMoneyValue_19;
	// UnityEngine.UI.Text CShoppingCounter::m_txtItemDesc
	Text_t1901882714 * ___m_txtItemDesc_20;
	// UnityEngine.UI.Text CShoppingCounter::m_txtItemName
	Text_t1901882714 * ___m_txtItemName_21;
	// UnityEngine.GameObject CShoppingCounter::m_goContainerEquippedLabel
	GameObject_t1113636619 * ___m_goContainerEquippedLabel_22;
	// Spine.Unity.SkeletonGraphic CShoppingCounter::_skeletonGraphic
	SkeletonGraphic_t1744877482 * ____skeletonGraphic_23;
	// CShoppingCounter/eItemStatus CShoppingCounter::m_eCurItemStatus
	int32_t ___m_eCurItemStatus_24;

public:
	inline static int32_t get_offset_of_m_eMoneyType_6() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_eMoneyType_6)); }
	inline int32_t get_m_eMoneyType_6() const { return ___m_eMoneyType_6; }
	inline int32_t* get_address_of_m_eMoneyType_6() { return &___m_eMoneyType_6; }
	inline void set_m_eMoneyType_6(int32_t value)
	{
		___m_eMoneyType_6 = value;
	}

	inline static int32_t get_offset_of_m_nMoneyValue_7() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_nMoneyValue_7)); }
	inline int32_t get_m_nMoneyValue_7() const { return ___m_nMoneyValue_7; }
	inline int32_t* get_address_of_m_nMoneyValue_7() { return &___m_nMoneyValue_7; }
	inline void set_m_nMoneyValue_7(int32_t value)
	{
		___m_nMoneyValue_7 = value;
	}

	inline static int32_t get_offset_of_m_bBought_8() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_bBought_8)); }
	inline bool get_m_bBought_8() const { return ___m_bBought_8; }
	inline bool* get_address_of_m_bBought_8() { return &___m_bBought_8; }
	inline void set_m_bBought_8(bool value)
	{
		___m_bBought_8 = value;
	}

	inline static int32_t get_offset_of_m_szItemName_9() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_szItemName_9)); }
	inline String_t* get_m_szItemName_9() const { return ___m_szItemName_9; }
	inline String_t** get_address_of_m_szItemName_9() { return &___m_szItemName_9; }
	inline void set_m_szItemName_9(String_t* value)
	{
		___m_szItemName_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_szItemName_9), value);
	}

	inline static int32_t get_offset_of_m_nItemId_10() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_nItemId_10)); }
	inline int32_t get_m_nItemId_10() const { return ___m_nItemId_10; }
	inline int32_t* get_address_of_m_nItemId_10() { return &___m_nItemId_10; }
	inline void set_m_nItemId_10(int32_t value)
	{
		___m_nItemId_10 = value;
	}

	inline static int32_t get_offset_of_m_szItemDesc_11() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_szItemDesc_11)); }
	inline String_t* get_m_szItemDesc_11() const { return ___m_szItemDesc_11; }
	inline String_t** get_address_of_m_szItemDesc_11() { return &___m_szItemDesc_11; }
	inline void set_m_szItemDesc_11(String_t* value)
	{
		___m_szItemDesc_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_szItemDesc_11), value);
	}

	inline static int32_t get_offset_of_m_szItemPath_12() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_szItemPath_12)); }
	inline String_t* get_m_szItemPath_12() const { return ___m_szItemPath_12; }
	inline String_t** get_address_of_m_szItemPath_12() { return &___m_szItemPath_12; }
	inline void set_m_szItemPath_12(String_t* value)
	{
		___m_szItemPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_szItemPath_12), value);
	}

	inline static int32_t get_offset_of_m_imgBg_13() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_imgBg_13)); }
	inline Image_t2670269651 * get_m_imgBg_13() const { return ___m_imgBg_13; }
	inline Image_t2670269651 ** get_address_of_m_imgBg_13() { return &___m_imgBg_13; }
	inline void set_m_imgBg_13(Image_t2670269651 * value)
	{
		___m_imgBg_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgBg_13), value);
	}

	inline static int32_t get_offset_of_m_btnBuyAndEquip_14() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_btnBuyAndEquip_14)); }
	inline Button_t4055032469 * get_m_btnBuyAndEquip_14() const { return ___m_btnBuyAndEquip_14; }
	inline Button_t4055032469 ** get_address_of_m_btnBuyAndEquip_14() { return &___m_btnBuyAndEquip_14; }
	inline void set_m_btnBuyAndEquip_14(Button_t4055032469 * value)
	{
		___m_btnBuyAndEquip_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_btnBuyAndEquip_14), value);
	}

	inline static int32_t get_offset_of_m_imgStatus_15() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_imgStatus_15)); }
	inline Image_t2670269651 * get_m_imgStatus_15() const { return ___m_imgStatus_15; }
	inline Image_t2670269651 ** get_address_of_m_imgStatus_15() { return &___m_imgStatus_15; }
	inline void set_m_imgStatus_15(Image_t2670269651 * value)
	{
		___m_imgStatus_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgStatus_15), value);
	}

	inline static int32_t get_offset_of_m_imgSkin_16() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_imgSkin_16)); }
	inline Image_t2670269651 * get_m_imgSkin_16() const { return ___m_imgSkin_16; }
	inline Image_t2670269651 ** get_address_of_m_imgSkin_16() { return &___m_imgSkin_16; }
	inline void set_m_imgSkin_16(Image_t2670269651 * value)
	{
		___m_imgSkin_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgSkin_16), value);
	}

	inline static int32_t get_offset_of_m_imgMoneyType_17() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_imgMoneyType_17)); }
	inline Image_t2670269651 * get_m_imgMoneyType_17() const { return ___m_imgMoneyType_17; }
	inline Image_t2670269651 ** get_address_of_m_imgMoneyType_17() { return &___m_imgMoneyType_17; }
	inline void set_m_imgMoneyType_17(Image_t2670269651 * value)
	{
		___m_imgMoneyType_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgMoneyType_17), value);
	}

	inline static int32_t get_offset_of_m_imgMoneyBg_18() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_imgMoneyBg_18)); }
	inline Image_t2670269651 * get_m_imgMoneyBg_18() const { return ___m_imgMoneyBg_18; }
	inline Image_t2670269651 ** get_address_of_m_imgMoneyBg_18() { return &___m_imgMoneyBg_18; }
	inline void set_m_imgMoneyBg_18(Image_t2670269651 * value)
	{
		___m_imgMoneyBg_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_imgMoneyBg_18), value);
	}

	inline static int32_t get_offset_of_m_txtMoneyValue_19() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_txtMoneyValue_19)); }
	inline Text_t1901882714 * get_m_txtMoneyValue_19() const { return ___m_txtMoneyValue_19; }
	inline Text_t1901882714 ** get_address_of_m_txtMoneyValue_19() { return &___m_txtMoneyValue_19; }
	inline void set_m_txtMoneyValue_19(Text_t1901882714 * value)
	{
		___m_txtMoneyValue_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtMoneyValue_19), value);
	}

	inline static int32_t get_offset_of_m_txtItemDesc_20() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_txtItemDesc_20)); }
	inline Text_t1901882714 * get_m_txtItemDesc_20() const { return ___m_txtItemDesc_20; }
	inline Text_t1901882714 ** get_address_of_m_txtItemDesc_20() { return &___m_txtItemDesc_20; }
	inline void set_m_txtItemDesc_20(Text_t1901882714 * value)
	{
		___m_txtItemDesc_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtItemDesc_20), value);
	}

	inline static int32_t get_offset_of_m_txtItemName_21() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_txtItemName_21)); }
	inline Text_t1901882714 * get_m_txtItemName_21() const { return ___m_txtItemName_21; }
	inline Text_t1901882714 ** get_address_of_m_txtItemName_21() { return &___m_txtItemName_21; }
	inline void set_m_txtItemName_21(Text_t1901882714 * value)
	{
		___m_txtItemName_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_txtItemName_21), value);
	}

	inline static int32_t get_offset_of_m_goContainerEquippedLabel_22() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_goContainerEquippedLabel_22)); }
	inline GameObject_t1113636619 * get_m_goContainerEquippedLabel_22() const { return ___m_goContainerEquippedLabel_22; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainerEquippedLabel_22() { return &___m_goContainerEquippedLabel_22; }
	inline void set_m_goContainerEquippedLabel_22(GameObject_t1113636619 * value)
	{
		___m_goContainerEquippedLabel_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerEquippedLabel_22), value);
	}

	inline static int32_t get_offset_of__skeletonGraphic_23() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ____skeletonGraphic_23)); }
	inline SkeletonGraphic_t1744877482 * get__skeletonGraphic_23() const { return ____skeletonGraphic_23; }
	inline SkeletonGraphic_t1744877482 ** get_address_of__skeletonGraphic_23() { return &____skeletonGraphic_23; }
	inline void set__skeletonGraphic_23(SkeletonGraphic_t1744877482 * value)
	{
		____skeletonGraphic_23 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonGraphic_23), value);
	}

	inline static int32_t get_offset_of_m_eCurItemStatus_24() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240, ___m_eCurItemStatus_24)); }
	inline int32_t get_m_eCurItemStatus_24() const { return ___m_eCurItemStatus_24; }
	inline int32_t* get_address_of_m_eCurItemStatus_24() { return &___m_eCurItemStatus_24; }
	inline void set_m_eCurItemStatus_24(int32_t value)
	{
		___m_eCurItemStatus_24 = value;
	}
};

struct CShoppingCounter_t3637302240_StaticFields
{
public:
	// UnityEngine.Vector2 CShoppingCounter::vecTempSize
	Vector2_t2156229523  ___vecTempSize_3;
	// UnityEngine.Vector3 CShoppingCounter::vec3TempSize
	Vector3_t3722313464  ___vec3TempSize_4;
	// UnityEngine.Vector3 CShoppingCounter::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;

public:
	inline static int32_t get_offset_of_vecTempSize_3() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240_StaticFields, ___vecTempSize_3)); }
	inline Vector2_t2156229523  get_vecTempSize_3() const { return ___vecTempSize_3; }
	inline Vector2_t2156229523 * get_address_of_vecTempSize_3() { return &___vecTempSize_3; }
	inline void set_vecTempSize_3(Vector2_t2156229523  value)
	{
		___vecTempSize_3 = value;
	}

	inline static int32_t get_offset_of_vec3TempSize_4() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240_StaticFields, ___vec3TempSize_4)); }
	inline Vector3_t3722313464  get_vec3TempSize_4() const { return ___vec3TempSize_4; }
	inline Vector3_t3722313464 * get_address_of_vec3TempSize_4() { return &___vec3TempSize_4; }
	inline void set_vec3TempSize_4(Vector3_t3722313464  value)
	{
		___vec3TempSize_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CShoppingCounter_t3637302240_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSHOPPINGCOUNTER_T3637302240_H
#ifndef CGRASS_T734084476_H
#define CGRASS_T734084476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CGrass
struct  CGrass_t734084476  : public CPveObj_t2172873472
{
public:
	// System.Collections.Generic.List`1<Ball> CGrass::m_lstBalls
	List_1_t3678741308 * ___m_lstBalls_8;
	// System.Boolean CGrass::m_bMainPlayerIn
	bool ___m_bMainPlayerIn_9;
	// CGrassEditor/sGrassConfig CGrass::m_Config
	sGrassConfig_t73951773  ___m_Config_10;
	// System.Single CGrass::m_fCheckMainPlayerInTimeCount
	float ___m_fCheckMainPlayerInTimeCount_12;
	// System.Boolean CGrass::m_bRecoveringAlpha
	bool ___m_bRecoveringAlpha_13;

public:
	inline static int32_t get_offset_of_m_lstBalls_8() { return static_cast<int32_t>(offsetof(CGrass_t734084476, ___m_lstBalls_8)); }
	inline List_1_t3678741308 * get_m_lstBalls_8() const { return ___m_lstBalls_8; }
	inline List_1_t3678741308 ** get_address_of_m_lstBalls_8() { return &___m_lstBalls_8; }
	inline void set_m_lstBalls_8(List_1_t3678741308 * value)
	{
		___m_lstBalls_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBalls_8), value);
	}

	inline static int32_t get_offset_of_m_bMainPlayerIn_9() { return static_cast<int32_t>(offsetof(CGrass_t734084476, ___m_bMainPlayerIn_9)); }
	inline bool get_m_bMainPlayerIn_9() const { return ___m_bMainPlayerIn_9; }
	inline bool* get_address_of_m_bMainPlayerIn_9() { return &___m_bMainPlayerIn_9; }
	inline void set_m_bMainPlayerIn_9(bool value)
	{
		___m_bMainPlayerIn_9 = value;
	}

	inline static int32_t get_offset_of_m_Config_10() { return static_cast<int32_t>(offsetof(CGrass_t734084476, ___m_Config_10)); }
	inline sGrassConfig_t73951773  get_m_Config_10() const { return ___m_Config_10; }
	inline sGrassConfig_t73951773 * get_address_of_m_Config_10() { return &___m_Config_10; }
	inline void set_m_Config_10(sGrassConfig_t73951773  value)
	{
		___m_Config_10 = value;
	}

	inline static int32_t get_offset_of_m_fCheckMainPlayerInTimeCount_12() { return static_cast<int32_t>(offsetof(CGrass_t734084476, ___m_fCheckMainPlayerInTimeCount_12)); }
	inline float get_m_fCheckMainPlayerInTimeCount_12() const { return ___m_fCheckMainPlayerInTimeCount_12; }
	inline float* get_address_of_m_fCheckMainPlayerInTimeCount_12() { return &___m_fCheckMainPlayerInTimeCount_12; }
	inline void set_m_fCheckMainPlayerInTimeCount_12(float value)
	{
		___m_fCheckMainPlayerInTimeCount_12 = value;
	}

	inline static int32_t get_offset_of_m_bRecoveringAlpha_13() { return static_cast<int32_t>(offsetof(CGrass_t734084476, ___m_bRecoveringAlpha_13)); }
	inline bool get_m_bRecoveringAlpha_13() const { return ___m_bRecoveringAlpha_13; }
	inline bool* get_address_of_m_bRecoveringAlpha_13() { return &___m_bRecoveringAlpha_13; }
	inline void set_m_bRecoveringAlpha_13(bool value)
	{
		___m_bRecoveringAlpha_13 = value;
	}
};

struct CGrass_t734084476_StaticFields
{
public:
	// UnityEngine.Color CGrass::cTempColor
	Color_t2555686324  ___cTempColor_7;

public:
	inline static int32_t get_offset_of_cTempColor_7() { return static_cast<int32_t>(offsetof(CGrass_t734084476_StaticFields, ___cTempColor_7)); }
	inline Color_t2555686324  get_cTempColor_7() const { return ___cTempColor_7; }
	inline Color_t2555686324 * get_address_of_cTempColor_7() { return &___cTempColor_7; }
	inline void set_cTempColor_7(Color_t2555686324  value)
	{
		___cTempColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CGRASS_T734084476_H
#ifndef CBRIEFRANKINGLISTITEM_T3256725699_H
#define CBRIEFRANKINGLISTITEM_T3256725699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBriefRankingListItem
struct  CBriefRankingListItem_t3256725699  : public CCyberTreeListItem_t2642449450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBRIEFRANKINGLISTITEM_T3256725699_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef CSKELETONANIMATION_T159686570_H
#define CSKELETONANIMATION_T159686570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkeletonAnimation
struct  CSkeletonAnimation_t159686570  : public CCosmosEffect_t495978253
{
public:
	// Spine.Unity.SkeletonAnimation CSkeletonAnimation::_skeletonMain
	SkeletonAnimation_t3693186521 * ____skeletonMain_17;

public:
	inline static int32_t get_offset_of__skeletonMain_17() { return static_cast<int32_t>(offsetof(CSkeletonAnimation_t159686570, ____skeletonMain_17)); }
	inline SkeletonAnimation_t3693186521 * get__skeletonMain_17() const { return ____skeletonMain_17; }
	inline SkeletonAnimation_t3693186521 ** get_address_of__skeletonMain_17() { return &____skeletonMain_17; }
	inline void set__skeletonMain_17(SkeletonAnimation_t3693186521 * value)
	{
		____skeletonMain_17 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonMain_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSKELETONANIMATION_T159686570_H
#ifndef CQUEUEITEM_T1983427687_H
#define CQUEUEITEM_T1983427687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CQueueItem
struct  CQueueItem_t1983427687  : public CCyberTreeListItem_t2642449450
{
public:
	// UnityEngine.UI.Image CQueueItem::_imgAvatar
	Image_t2670269651 * ____imgAvatar_3;
	// UnityEngine.UI.Text CQueueItem::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_4;
	// UnityEngine.UI.Text CQueueItem::_txtLevel
	Text_t1901882714 * ____txtLevel_5;
	// UnityEngine.UI.Text CQueueItem::_txtArea
	Text_t1901882714 * ____txtArea_6;
	// UnityEngine.UI.Text CQueueItem::_txtPlayerId
	Text_t1901882714 * ____txtPlayerId_7;
	// System.Int32 CQueueItem::m_nPlayerId
	int32_t ___m_nPlayerId_8;

public:
	inline static int32_t get_offset_of__imgAvatar_3() { return static_cast<int32_t>(offsetof(CQueueItem_t1983427687, ____imgAvatar_3)); }
	inline Image_t2670269651 * get__imgAvatar_3() const { return ____imgAvatar_3; }
	inline Image_t2670269651 ** get_address_of__imgAvatar_3() { return &____imgAvatar_3; }
	inline void set__imgAvatar_3(Image_t2670269651 * value)
	{
		____imgAvatar_3 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_3), value);
	}

	inline static int32_t get_offset_of__txtPlayerName_4() { return static_cast<int32_t>(offsetof(CQueueItem_t1983427687, ____txtPlayerName_4)); }
	inline Text_t1901882714 * get__txtPlayerName_4() const { return ____txtPlayerName_4; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_4() { return &____txtPlayerName_4; }
	inline void set__txtPlayerName_4(Text_t1901882714 * value)
	{
		____txtPlayerName_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_4), value);
	}

	inline static int32_t get_offset_of__txtLevel_5() { return static_cast<int32_t>(offsetof(CQueueItem_t1983427687, ____txtLevel_5)); }
	inline Text_t1901882714 * get__txtLevel_5() const { return ____txtLevel_5; }
	inline Text_t1901882714 ** get_address_of__txtLevel_5() { return &____txtLevel_5; }
	inline void set__txtLevel_5(Text_t1901882714 * value)
	{
		____txtLevel_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtLevel_5), value);
	}

	inline static int32_t get_offset_of__txtArea_6() { return static_cast<int32_t>(offsetof(CQueueItem_t1983427687, ____txtArea_6)); }
	inline Text_t1901882714 * get__txtArea_6() const { return ____txtArea_6; }
	inline Text_t1901882714 ** get_address_of__txtArea_6() { return &____txtArea_6; }
	inline void set__txtArea_6(Text_t1901882714 * value)
	{
		____txtArea_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtArea_6), value);
	}

	inline static int32_t get_offset_of__txtPlayerId_7() { return static_cast<int32_t>(offsetof(CQueueItem_t1983427687, ____txtPlayerId_7)); }
	inline Text_t1901882714 * get__txtPlayerId_7() const { return ____txtPlayerId_7; }
	inline Text_t1901882714 ** get_address_of__txtPlayerId_7() { return &____txtPlayerId_7; }
	inline void set__txtPlayerId_7(Text_t1901882714 * value)
	{
		____txtPlayerId_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerId_7), value);
	}

	inline static int32_t get_offset_of_m_nPlayerId_8() { return static_cast<int32_t>(offsetof(CQueueItem_t1983427687, ___m_nPlayerId_8)); }
	inline int32_t get_m_nPlayerId_8() const { return ___m_nPlayerId_8; }
	inline int32_t* get_address_of_m_nPlayerId_8() { return &___m_nPlayerId_8; }
	inline void set_m_nPlayerId_8(int32_t value)
	{
		___m_nPlayerId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CQUEUEITEM_T1983427687_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef CQUEUEMANAGER_T35825593_H
#define CQUEUEMANAGER_T35825593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CQueueManager
struct  CQueueManager_t35825593  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.GameObject CQueueManager::_panelQueue
	GameObject_t1113636619 * ____panelQueue_4;
	// UnityEngine.GameObject CQueueManager::m_preQueueItem
	GameObject_t1113636619 * ___m_preQueueItem_5;
	// UnityEngine.UI.InputField CQueueManager::_inputPlayerId
	InputField_t3762917431 * ____inputPlayerId_6;
	// CCyberTreeList CQueueManager::m_lstPlayers
	CCyberTreeList_t4040119676 * ___m_lstPlayers_7;
	// CQueueItem CQueueManager::m_itemMainPlayer
	CQueueItem_t1983427687 * ___m_itemMainPlayer_8;
	// UnityEngine.UI.Text CQueueManager::_txtWaitingTime
	Text_t1901882714 * ____txtWaitingTime_9;
	// UnityEngine.UI.Text CQueueManager::_txtTotalPlayerNum
	Text_t1901882714 * ____txtTotalPlayerNum_10;
	// UnityEngine.UI.Text CQueueManager::_txtCurPlayerNum
	Text_t1901882714 * ____txtCurPlayerNum_11;
	// System.Int32 CQueueManager::m_nTotalPlayerNum
	int32_t ___m_nTotalPlayerNum_12;
	// System.Single CQueueManager::m_fStartQueueTime
	float ___m_fStartQueueTime_13;
	// System.Boolean CQueueManager::m_bQueue
	bool ___m_bQueue_14;
	// System.Single CQueueManager::m_fEnterArenaCoutingTime
	float ___m_fEnterArenaCoutingTime_15;
	// System.Boolean CQueueManager::m_bQueueFinished
	bool ___m_bQueueFinished_16;
	// System.Collections.Generic.List`1<PhotonPlayer> CQueueManager::m_lstSortedPhotonPlayer
	List_1_t482257003 * ___m_lstSortedPhotonPlayer_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CQueueManager::m_dicPlayerId2SkinId
	Dictionary_2_t1839659084 * ___m_dicPlayerId2SkinId_18;
	// System.Single CQueueManager::m_fRefreshPlayerListTimeElapse
	float ___m_fRefreshPlayerListTimeElapse_20;
	// System.Collections.Generic.List`1<CQueueItem> CQueueManager::m_lstRecycleditems
	List_1_t3455502429 * ___m_lstRecycleditems_21;
	// System.Int32 CQueueManager::m_nShit
	int32_t ___m_nShit_22;

public:
	inline static int32_t get_offset_of__panelQueue_4() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ____panelQueue_4)); }
	inline GameObject_t1113636619 * get__panelQueue_4() const { return ____panelQueue_4; }
	inline GameObject_t1113636619 ** get_address_of__panelQueue_4() { return &____panelQueue_4; }
	inline void set__panelQueue_4(GameObject_t1113636619 * value)
	{
		____panelQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&____panelQueue_4), value);
	}

	inline static int32_t get_offset_of_m_preQueueItem_5() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_preQueueItem_5)); }
	inline GameObject_t1113636619 * get_m_preQueueItem_5() const { return ___m_preQueueItem_5; }
	inline GameObject_t1113636619 ** get_address_of_m_preQueueItem_5() { return &___m_preQueueItem_5; }
	inline void set_m_preQueueItem_5(GameObject_t1113636619 * value)
	{
		___m_preQueueItem_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_preQueueItem_5), value);
	}

	inline static int32_t get_offset_of__inputPlayerId_6() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ____inputPlayerId_6)); }
	inline InputField_t3762917431 * get__inputPlayerId_6() const { return ____inputPlayerId_6; }
	inline InputField_t3762917431 ** get_address_of__inputPlayerId_6() { return &____inputPlayerId_6; }
	inline void set__inputPlayerId_6(InputField_t3762917431 * value)
	{
		____inputPlayerId_6 = value;
		Il2CppCodeGenWriteBarrier((&____inputPlayerId_6), value);
	}

	inline static int32_t get_offset_of_m_lstPlayers_7() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_lstPlayers_7)); }
	inline CCyberTreeList_t4040119676 * get_m_lstPlayers_7() const { return ___m_lstPlayers_7; }
	inline CCyberTreeList_t4040119676 ** get_address_of_m_lstPlayers_7() { return &___m_lstPlayers_7; }
	inline void set_m_lstPlayers_7(CCyberTreeList_t4040119676 * value)
	{
		___m_lstPlayers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPlayers_7), value);
	}

	inline static int32_t get_offset_of_m_itemMainPlayer_8() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_itemMainPlayer_8)); }
	inline CQueueItem_t1983427687 * get_m_itemMainPlayer_8() const { return ___m_itemMainPlayer_8; }
	inline CQueueItem_t1983427687 ** get_address_of_m_itemMainPlayer_8() { return &___m_itemMainPlayer_8; }
	inline void set_m_itemMainPlayer_8(CQueueItem_t1983427687 * value)
	{
		___m_itemMainPlayer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_itemMainPlayer_8), value);
	}

	inline static int32_t get_offset_of__txtWaitingTime_9() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ____txtWaitingTime_9)); }
	inline Text_t1901882714 * get__txtWaitingTime_9() const { return ____txtWaitingTime_9; }
	inline Text_t1901882714 ** get_address_of__txtWaitingTime_9() { return &____txtWaitingTime_9; }
	inline void set__txtWaitingTime_9(Text_t1901882714 * value)
	{
		____txtWaitingTime_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtWaitingTime_9), value);
	}

	inline static int32_t get_offset_of__txtTotalPlayerNum_10() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ____txtTotalPlayerNum_10)); }
	inline Text_t1901882714 * get__txtTotalPlayerNum_10() const { return ____txtTotalPlayerNum_10; }
	inline Text_t1901882714 ** get_address_of__txtTotalPlayerNum_10() { return &____txtTotalPlayerNum_10; }
	inline void set__txtTotalPlayerNum_10(Text_t1901882714 * value)
	{
		____txtTotalPlayerNum_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalPlayerNum_10), value);
	}

	inline static int32_t get_offset_of__txtCurPlayerNum_11() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ____txtCurPlayerNum_11)); }
	inline Text_t1901882714 * get__txtCurPlayerNum_11() const { return ____txtCurPlayerNum_11; }
	inline Text_t1901882714 ** get_address_of__txtCurPlayerNum_11() { return &____txtCurPlayerNum_11; }
	inline void set__txtCurPlayerNum_11(Text_t1901882714 * value)
	{
		____txtCurPlayerNum_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPlayerNum_11), value);
	}

	inline static int32_t get_offset_of_m_nTotalPlayerNum_12() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_nTotalPlayerNum_12)); }
	inline int32_t get_m_nTotalPlayerNum_12() const { return ___m_nTotalPlayerNum_12; }
	inline int32_t* get_address_of_m_nTotalPlayerNum_12() { return &___m_nTotalPlayerNum_12; }
	inline void set_m_nTotalPlayerNum_12(int32_t value)
	{
		___m_nTotalPlayerNum_12 = value;
	}

	inline static int32_t get_offset_of_m_fStartQueueTime_13() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_fStartQueueTime_13)); }
	inline float get_m_fStartQueueTime_13() const { return ___m_fStartQueueTime_13; }
	inline float* get_address_of_m_fStartQueueTime_13() { return &___m_fStartQueueTime_13; }
	inline void set_m_fStartQueueTime_13(float value)
	{
		___m_fStartQueueTime_13 = value;
	}

	inline static int32_t get_offset_of_m_bQueue_14() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_bQueue_14)); }
	inline bool get_m_bQueue_14() const { return ___m_bQueue_14; }
	inline bool* get_address_of_m_bQueue_14() { return &___m_bQueue_14; }
	inline void set_m_bQueue_14(bool value)
	{
		___m_bQueue_14 = value;
	}

	inline static int32_t get_offset_of_m_fEnterArenaCoutingTime_15() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_fEnterArenaCoutingTime_15)); }
	inline float get_m_fEnterArenaCoutingTime_15() const { return ___m_fEnterArenaCoutingTime_15; }
	inline float* get_address_of_m_fEnterArenaCoutingTime_15() { return &___m_fEnterArenaCoutingTime_15; }
	inline void set_m_fEnterArenaCoutingTime_15(float value)
	{
		___m_fEnterArenaCoutingTime_15 = value;
	}

	inline static int32_t get_offset_of_m_bQueueFinished_16() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_bQueueFinished_16)); }
	inline bool get_m_bQueueFinished_16() const { return ___m_bQueueFinished_16; }
	inline bool* get_address_of_m_bQueueFinished_16() { return &___m_bQueueFinished_16; }
	inline void set_m_bQueueFinished_16(bool value)
	{
		___m_bQueueFinished_16 = value;
	}

	inline static int32_t get_offset_of_m_lstSortedPhotonPlayer_17() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_lstSortedPhotonPlayer_17)); }
	inline List_1_t482257003 * get_m_lstSortedPhotonPlayer_17() const { return ___m_lstSortedPhotonPlayer_17; }
	inline List_1_t482257003 ** get_address_of_m_lstSortedPhotonPlayer_17() { return &___m_lstSortedPhotonPlayer_17; }
	inline void set_m_lstSortedPhotonPlayer_17(List_1_t482257003 * value)
	{
		___m_lstSortedPhotonPlayer_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSortedPhotonPlayer_17), value);
	}

	inline static int32_t get_offset_of_m_dicPlayerId2SkinId_18() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_dicPlayerId2SkinId_18)); }
	inline Dictionary_2_t1839659084 * get_m_dicPlayerId2SkinId_18() const { return ___m_dicPlayerId2SkinId_18; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicPlayerId2SkinId_18() { return &___m_dicPlayerId2SkinId_18; }
	inline void set_m_dicPlayerId2SkinId_18(Dictionary_2_t1839659084 * value)
	{
		___m_dicPlayerId2SkinId_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayerId2SkinId_18), value);
	}

	inline static int32_t get_offset_of_m_fRefreshPlayerListTimeElapse_20() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_fRefreshPlayerListTimeElapse_20)); }
	inline float get_m_fRefreshPlayerListTimeElapse_20() const { return ___m_fRefreshPlayerListTimeElapse_20; }
	inline float* get_address_of_m_fRefreshPlayerListTimeElapse_20() { return &___m_fRefreshPlayerListTimeElapse_20; }
	inline void set_m_fRefreshPlayerListTimeElapse_20(float value)
	{
		___m_fRefreshPlayerListTimeElapse_20 = value;
	}

	inline static int32_t get_offset_of_m_lstRecycleditems_21() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_lstRecycleditems_21)); }
	inline List_1_t3455502429 * get_m_lstRecycleditems_21() const { return ___m_lstRecycleditems_21; }
	inline List_1_t3455502429 ** get_address_of_m_lstRecycleditems_21() { return &___m_lstRecycleditems_21; }
	inline void set_m_lstRecycleditems_21(List_1_t3455502429 * value)
	{
		___m_lstRecycleditems_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycleditems_21), value);
	}

	inline static int32_t get_offset_of_m_nShit_22() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593, ___m_nShit_22)); }
	inline int32_t get_m_nShit_22() const { return ___m_nShit_22; }
	inline int32_t* get_address_of_m_nShit_22() { return &___m_nShit_22; }
	inline void set_m_nShit_22(int32_t value)
	{
		___m_nShit_22 = value;
	}
};

struct CQueueManager_t35825593_StaticFields
{
public:
	// CQueueManager CQueueManager::s_Instance
	CQueueManager_t35825593 * ___s_Instance_3;

public:
	inline static int32_t get_offset_of_s_Instance_3() { return static_cast<int32_t>(offsetof(CQueueManager_t35825593_StaticFields, ___s_Instance_3)); }
	inline CQueueManager_t35825593 * get_s_Instance_3() const { return ___s_Instance_3; }
	inline CQueueManager_t35825593 ** get_address_of_s_Instance_3() { return &___s_Instance_3; }
	inline void set_s_Instance_3(CQueueManager_t35825593 * value)
	{
		___s_Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CQUEUEMANAGER_T35825593_H
#ifndef MODIFIEDSHADOW_T4034257529_H
#define MODIFIEDSHADOW_T4034257529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModifiedShadow
struct  ModifiedShadow_t4034257529  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFIEDSHADOW_T4034257529_H
#ifndef OUTLINE8_T1486056445_H
#define OUTLINE8_T1486056445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outline8
struct  Outline8_t1486056445  : public ModifiedShadow_t4034257529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE8_T1486056445_H
#ifndef CIRCLEOUTLINE_T408400008_H
#define CIRCLEOUTLINE_T408400008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleOutline
struct  CircleOutline_t408400008  : public ModifiedShadow_t4034257529
{
public:
	// System.Int32 CircleOutline::m_circleCount
	int32_t ___m_circleCount_7;
	// System.Int32 CircleOutline::m_firstSample
	int32_t ___m_firstSample_8;
	// System.Int32 CircleOutline::m_sampleIncrement
	int32_t ___m_sampleIncrement_9;

public:
	inline static int32_t get_offset_of_m_circleCount_7() { return static_cast<int32_t>(offsetof(CircleOutline_t408400008, ___m_circleCount_7)); }
	inline int32_t get_m_circleCount_7() const { return ___m_circleCount_7; }
	inline int32_t* get_address_of_m_circleCount_7() { return &___m_circleCount_7; }
	inline void set_m_circleCount_7(int32_t value)
	{
		___m_circleCount_7 = value;
	}

	inline static int32_t get_offset_of_m_firstSample_8() { return static_cast<int32_t>(offsetof(CircleOutline_t408400008, ___m_firstSample_8)); }
	inline int32_t get_m_firstSample_8() const { return ___m_firstSample_8; }
	inline int32_t* get_address_of_m_firstSample_8() { return &___m_firstSample_8; }
	inline void set_m_firstSample_8(int32_t value)
	{
		___m_firstSample_8 = value;
	}

	inline static int32_t get_offset_of_m_sampleIncrement_9() { return static_cast<int32_t>(offsetof(CircleOutline_t408400008, ___m_sampleIncrement_9)); }
	inline int32_t get_m_sampleIncrement_9() const { return ___m_sampleIncrement_9; }
	inline int32_t* get_address_of_m_sampleIncrement_9() { return &___m_sampleIncrement_9; }
	inline void set_m_sampleIncrement_9(int32_t value)
	{
		___m_sampleIncrement_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLEOUTLINE_T408400008_H
#ifndef BOXOUTLINE_T1562695836_H
#define BOXOUTLINE_T1562695836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoxOutline
struct  BoxOutline_t1562695836  : public ModifiedShadow_t4034257529
{
public:
	// System.Int32 BoxOutline::m_halfSampleCountX
	int32_t ___m_halfSampleCountX_8;
	// System.Int32 BoxOutline::m_halfSampleCountY
	int32_t ___m_halfSampleCountY_9;

public:
	inline static int32_t get_offset_of_m_halfSampleCountX_8() { return static_cast<int32_t>(offsetof(BoxOutline_t1562695836, ___m_halfSampleCountX_8)); }
	inline int32_t get_m_halfSampleCountX_8() const { return ___m_halfSampleCountX_8; }
	inline int32_t* get_address_of_m_halfSampleCountX_8() { return &___m_halfSampleCountX_8; }
	inline void set_m_halfSampleCountX_8(int32_t value)
	{
		___m_halfSampleCountX_8 = value;
	}

	inline static int32_t get_offset_of_m_halfSampleCountY_9() { return static_cast<int32_t>(offsetof(BoxOutline_t1562695836, ___m_halfSampleCountY_9)); }
	inline int32_t get_m_halfSampleCountY_9() const { return ___m_halfSampleCountY_9; }
	inline int32_t* get_address_of_m_halfSampleCountY_9() { return &___m_halfSampleCountY_9; }
	inline void set_m_halfSampleCountY_9(int32_t value)
	{
		___m_halfSampleCountY_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXOUTLINE_T1562695836_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (CLightSystem_t2157026498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3400[2] = 
{
	CLightSystem_t2157026498::get_offset_of_m_aryPrePoint_2(),
	CLightSystem_t2157026498::get_offset_of_m_dicRecycledLights_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (eLightType_t683687508)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3401[2] = 
{
	eLightType_t683687508::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (CPointLight_t2898614150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (CConfigManager_t1106484817), -1, sizeof(CConfigManager_t1106484817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3403[1] = 
{
	CConfigManager_t1106484817_StaticFields::get_offset_of_s_aryConfigLoaded_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (eConfigId_t1122251472)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3404[7] = 
{
	eConfigId_t1122251472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (CGameModeSelectCounter_t2640523211), -1, sizeof(CGameModeSelectCounter_t2640523211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3405[2] = 
{
	CGameModeSelectCounter_t2640523211::get_offset_of__goSelected_2(),
	CGameModeSelectCounter_t2640523211_StaticFields::get_offset_of_vecTempScale_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (CLoading_t426279320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3406[8] = 
{
	CLoading_t426279320::get_offset_of__aryDots_2(),
	CLoading_t426279320::get_offset_of_m_nIndex_3(),
	0,
	CLoading_t426279320::get_offset_of_m_fShowDotTimeLapse_5(),
	CLoading_t426279320::get_offset_of_m_fTotalShowTimeLapse_6(),
	CLoading_t426279320::get_offset_of_m_fTotalShowTime_7(),
	CLoading_t426279320::get_offset_of_m_bTotalShowTime_8(),
	CLoading_t426279320::get_offset_of__commonJinDuTiao_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (CQueueItem_t1983427687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[6] = 
{
	CQueueItem_t1983427687::get_offset_of__imgAvatar_3(),
	CQueueItem_t1983427687::get_offset_of__txtPlayerName_4(),
	CQueueItem_t1983427687::get_offset_of__txtLevel_5(),
	CQueueItem_t1983427687::get_offset_of__txtArea_6(),
	CQueueItem_t1983427687::get_offset_of__txtPlayerId_7(),
	CQueueItem_t1983427687::get_offset_of_m_nPlayerId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (CQueueManager_t35825593), -1, sizeof(CQueueManager_t35825593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3408[20] = 
{
	CQueueManager_t35825593_StaticFields::get_offset_of_s_Instance_3(),
	CQueueManager_t35825593::get_offset_of__panelQueue_4(),
	CQueueManager_t35825593::get_offset_of_m_preQueueItem_5(),
	CQueueManager_t35825593::get_offset_of__inputPlayerId_6(),
	CQueueManager_t35825593::get_offset_of_m_lstPlayers_7(),
	CQueueManager_t35825593::get_offset_of_m_itemMainPlayer_8(),
	CQueueManager_t35825593::get_offset_of__txtWaitingTime_9(),
	CQueueManager_t35825593::get_offset_of__txtTotalPlayerNum_10(),
	CQueueManager_t35825593::get_offset_of__txtCurPlayerNum_11(),
	CQueueManager_t35825593::get_offset_of_m_nTotalPlayerNum_12(),
	CQueueManager_t35825593::get_offset_of_m_fStartQueueTime_13(),
	CQueueManager_t35825593::get_offset_of_m_bQueue_14(),
	CQueueManager_t35825593::get_offset_of_m_fEnterArenaCoutingTime_15(),
	CQueueManager_t35825593::get_offset_of_m_bQueueFinished_16(),
	CQueueManager_t35825593::get_offset_of_m_lstSortedPhotonPlayer_17(),
	CQueueManager_t35825593::get_offset_of_m_dicPlayerId2SkinId_18(),
	0,
	CQueueManager_t35825593::get_offset_of_m_fRefreshPlayerListTimeElapse_20(),
	CQueueManager_t35825593::get_offset_of_m_lstRecycleditems_21(),
	CQueueManager_t35825593::get_offset_of_m_nShit_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (CRegister_t4189215833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	CRegister_t4189215833::get_offset_of__inputPhoneNum_8(),
	CRegister_t4189215833::get_offset_of__inputVeriCode_9(),
	CRegister_t4189215833::get_offset_of__inputPassword_10(),
	CRegister_t4189215833::get_offset_of__inputPhoneNumWhenLogin_11(),
	CRegister_t4189215833::get_offset_of__inputPasswordWhenLogin_12(),
	CRegister_t4189215833::get_offset_of__toggleAutoLogin_13(),
	CRegister_t4189215833::get_offset_of__inputRoleName_14(),
	CRegister_t4189215833::get_offset_of_m_dicAccountInfo_15(),
	CRegister_t4189215833::get_offset_of_m_CurAccoutInfo_16(),
	CRegister_t4189215833::get_offset_of_m_szCurLoginPhoneNum_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (sAccountInfo_t751877243)+ sizeof (RuntimeObject), sizeof(sAccountInfo_t751877243_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3410[2] = 
{
	sAccountInfo_t751877243::get_offset_of_szPhoneNum_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAccountInfo_t751877243::get_offset_of_szSessionId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (U3CGetVeriCodeU3Ec__Iterator0_t3257255021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3411[6] = 
{
	U3CGetVeriCodeU3Ec__Iterator0_t3257255021::get_offset_of_U3CrequestU3E__0_0(),
	U3CGetVeriCodeU3Ec__Iterator0_t3257255021::get_offset_of_U3CwwwU3E__0_1(),
	U3CGetVeriCodeU3Ec__Iterator0_t3257255021::get_offset_of_U24this_2(),
	U3CGetVeriCodeU3Ec__Iterator0_t3257255021::get_offset_of_U24current_3(),
	U3CGetVeriCodeU3Ec__Iterator0_t3257255021::get_offset_of_U24disposing_4(),
	U3CGetVeriCodeU3Ec__Iterator0_t3257255021::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (U3CDoRegisterU3Ec__Iterator1_t3660841303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[6] = 
{
	U3CDoRegisterU3Ec__Iterator1_t3660841303::get_offset_of_U3CformU3E__0_0(),
	U3CDoRegisterU3Ec__Iterator1_t3660841303::get_offset_of_U3CwwwU3E__0_1(),
	U3CDoRegisterU3Ec__Iterator1_t3660841303::get_offset_of_U24this_2(),
	U3CDoRegisterU3Ec__Iterator1_t3660841303::get_offset_of_U24current_3(),
	U3CDoRegisterU3Ec__Iterator1_t3660841303::get_offset_of_U24disposing_4(),
	U3CDoRegisterU3Ec__Iterator1_t3660841303::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[8] = 
{
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U3CformU3E__0_0(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U3CwwwU3E__0_1(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U3CaryTemp1U3E__0_2(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U3CaryTemp2U3E__0_3(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U24this_4(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U24current_5(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U24disposing_6(),
	U3CDoLoginByPasswordU3Ec__Iterator2_t1711557481::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (U3CDoLoginAutoU3Ec__Iterator3_t2803915295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[6] = 
{
	U3CDoLoginAutoU3Ec__Iterator3_t2803915295::get_offset_of_U3CrequestU3E__0_0(),
	U3CDoLoginAutoU3Ec__Iterator3_t2803915295::get_offset_of_U3CwwwU3E__0_1(),
	U3CDoLoginAutoU3Ec__Iterator3_t2803915295::get_offset_of_U24this_2(),
	U3CDoLoginAutoU3Ec__Iterator3_t2803915295::get_offset_of_U24current_3(),
	U3CDoLoginAutoU3Ec__Iterator3_t2803915295::get_offset_of_U24disposing_4(),
	U3CDoLoginAutoU3Ec__Iterator3_t2803915295::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (U3CDoCreateRoleU3Ec__Iterator4_t29253059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[6] = 
{
	U3CDoCreateRoleU3Ec__Iterator4_t29253059::get_offset_of_U3CformU3E__0_0(),
	U3CDoCreateRoleU3Ec__Iterator4_t29253059::get_offset_of_U3CwwwU3E__0_1(),
	U3CDoCreateRoleU3Ec__Iterator4_t29253059::get_offset_of_U24this_2(),
	U3CDoCreateRoleU3Ec__Iterator4_t29253059::get_offset_of_U24current_3(),
	U3CDoCreateRoleU3Ec__Iterator4_t29253059::get_offset_of_U24disposing_4(),
	U3CDoCreateRoleU3Ec__Iterator4_t29253059::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (CRoom_t2285119849), -1, sizeof(CRoom_t2285119849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3416[8] = 
{
	CRoom_t2285119849_StaticFields::get_offset_of_vecTempScale_2(),
	CRoom_t2285119849::get_offset_of__txtRoomName_3(),
	CRoom_t2285119849::get_offset_of__imgAvatar_4(),
	CRoom_t2285119849::get_offset_of__txtCurPlayerCount_5(),
	CRoom_t2285119849::get_offset_of__btnEnter_6(),
	CRoom_t2285119849::get_offset_of__imgBg_7(),
	CRoom_t2285119849::get_offset_of_m_nIndex_8(),
	CRoom_t2285119849::get_offset_of_m_bSelected_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (CSelectGameMode_t2151979979), -1, sizeof(CSelectGameMode_t2151979979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3417[13] = 
{
	CSelectGameMode_t2151979979_StaticFields::get_offset_of_s_Instance_2(),
	CSelectGameMode_t2151979979::get_offset_of__panelSelectGameMode_3(),
	CSelectGameMode_t2151979979::get_offset_of__panelSelectSkill_4(),
	CSelectGameMode_t2151979979::get_offset_of_m_eGameMode_5(),
	CSelectGameMode_t2151979979::get_offset_of__goContainerPages_6(),
	CSelectGameMode_t2151979979::get_offset_of_m_nCurSelectedPageIndex_7(),
	CSelectGameMode_t2151979979::get_offset_of__aryPageBtns_8(),
	CSelectGameMode_t2151979979::get_offset_of__txtPlayerName_9(),
	CSelectGameMode_t2151979979::get_offset_of__btnSelectGameMode_DaLuanDou_10(),
	CSelectGameMode_t2151979979::get_offset_of__btnSelectGameMode_WangZhongWang_11(),
	CSelectGameMode_t2151979979::get_offset_of__counterDaLuanDou_12(),
	CSelectGameMode_t2151979979::get_offset_of__counterDaWangZHongWang_13(),
	CSelectGameMode_t2151979979_StaticFields::get_offset_of_vecTempScale_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (eGameMode_t32794200)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3418[3] = 
{
	eGameMode_t32794200::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (CSelectRoomManager_t1525971949), -1, sizeof(CSelectRoomManager_t1525971949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3419[19] = 
{
	CSelectRoomManager_t1525971949_StaticFields::get_offset_of_s_Instance_2(),
	CSelectRoomManager_t1525971949::get_offset_of_m_colorSelected_3(),
	0,
	CSelectRoomManager_t1525971949::get_offset_of__panelSelectRoom_5(),
	CSelectRoomManager_t1525971949::get_offset_of_m_arySelectRoomPanel_6(),
	CSelectRoomManager_t1525971949::get_offset_of__jindutiao_7(),
	CSelectRoomManager_t1525971949::get_offset_of__toggleEnterTestRoom_8(),
	CSelectRoomManager_t1525971949::get_offset_of__toggleReadFromLocal_9(),
	CSelectRoomManager_t1525971949::get_offset_of_m_aryRooms_10(),
	CSelectRoomManager_t1525971949::get_offset_of_m_aryRooms_Others_11(),
	CSelectRoomManager_t1525971949::get_offset_of_m_aryRoomAvatar_12(),
	CSelectRoomManager_t1525971949::get_offset_of_m_sprNotSelected_13(),
	CSelectRoomManager_t1525971949::get_offset_of_m_sprSelected_14(),
	CSelectRoomManager_t1525971949::get_offset_of__txtCurSelectedRoomName_15(),
	CSelectRoomManager_t1525971949::get_offset_of_m_arySelectedRoomName_16(),
	CSelectRoomManager_t1525971949::get_offset_of_m_CurSelectedRoom_17(),
	CSelectRoomManager_t1525971949::get_offset_of_m_dicCurRoomInfo_18(),
	CSelectRoomManager_t1525971949_StaticFields::get_offset_of_s_bEnterTestRoom_19(),
	CSelectRoomManager_t1525971949_StaticFields::get_offset_of_s_bReadFromLocal_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (eCtrlType_t499920991)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3420[3] = 
{
	eCtrlType_t499920991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (CSelectSkill_t3600552377), -1, sizeof(CSelectSkill_t3600552377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3421[43] = 
{
	CSelectSkill_t3600552377::get_offset_of_m_aryPlayerNameForSkillSelectPanel_2(),
	CSelectSkill_t3600552377::get_offset_of_m_fSelectedScale_3(),
	CSelectSkill_t3600552377_StaticFields::get_offset_of_s_Instance_4(),
	CSelectSkill_t3600552377::get_offset_of__aryTitle_5(),
	CSelectSkill_t3600552377::get_offset_of__aryTitle_Others_6(),
	CSelectSkill_t3600552377::get_offset_of__aryValuesLevel0_7(),
	CSelectSkill_t3600552377::get_offset_of__aryValuesLevel0_Others_8(),
	CSelectSkill_t3600552377::get_offset_of__aryValuesLevel1_9(),
	CSelectSkill_t3600552377::get_offset_of__aryValuesLevel1_Others_10(),
	CSelectSkill_t3600552377::get_offset_of__aryValuesLevel2_11(),
	CSelectSkill_t3600552377::get_offset_of__aryValuesLevel2_Others_12(),
	CSelectSkill_t3600552377::get_offset_of_m_lstValuesLevels_13(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillType_14(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillType_Others_15(),
	CSelectSkill_t3600552377::get_offset_of__txtPlayerName_16(),
	CSelectSkill_t3600552377::get_offset_of__panelSkillDetail_17(),
	CSelectSkill_t3600552377::get_offset_of__panelSelectSkill_18(),
	CSelectSkill_t3600552377::get_offset_of__panelSelectGameModePage_19(),
	CSelectSkill_t3600552377::get_offset_of__btnQianXing_20(),
	CSelectSkill_t3600552377::get_offset_of__btnBianCi_21(),
	CSelectSkill_t3600552377::get_offset_of__btnYanMie_22(),
	CSelectSkill_t3600552377::get_offset_of__btnMoDun_23(),
	CSelectSkill_t3600552377::get_offset_of__btnMiaoHe_24(),
	CSelectSkill_t3600552377::get_offset_of__btnKuangBao_25(),
	CSelectSkill_t3600552377::get_offset_of__btnJinKe_26(),
	CSelectSkill_t3600552377::get_offset_of__txtSkillName_27(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillName_28(),
	CSelectSkill_t3600552377::get_offset_of__txtSkillDesc_29(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillDesc_30(),
	CSelectSkill_t3600552377::get_offset_of__btnPlayGame_31(),
	CSelectSkill_t3600552377::get_offset_of__btnReturnLastPage_32(),
	CSelectSkill_t3600552377::get_offset_of__imgSkillIcon_33(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillIcon_34(),
	CSelectSkill_t3600552377::get_offset_of_m_eCurSelectSkillId_35(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillDetailGrid_36(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillIconSprite_37(),
	CSelectSkill_t3600552377::get_offset_of_m_bDefaultSkillSelected_38(),
	CSelectSkill_t3600552377::get_offset_of_m_bMapParsed_39(),
	CSelectSkill_t3600552377::get_offset_of_m_arySkillDetail_40(),
	CSelectSkill_t3600552377::get_offset_of_m_dicSkillDesc_41(),
	CSelectSkill_t3600552377_StaticFields::get_offset_of_colorSkillButtonLight_42(),
	CSelectSkill_t3600552377_StaticFields::get_offset_of_colorSkillButtonDark_43(),
	CSelectSkill_t3600552377_StaticFields::get_offset_of_vecTempScale_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (eCtrlType_t1729920274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3422[5] = 
{
	eCtrlType_t1729920274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (sSkillDetail_t1561700181)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[5] = 
{
	sSkillDetail_t1561700181::get_offset_of_fSpeedAffect_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillDetail_t1561700181::get_offset_of_fMpCost_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillDetail_t1561700181::get_offset_of_fDuration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillDetail_t1561700181::get_offset_of_fColdDown_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillDetail_t1561700181::get_offset_of_aryValues_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (U3CParseMapU3Ec__Iterator0_t4151616039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3424[12] = 
{
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_szFileName_0(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CwwwU3E__0_1(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CrootU3E__0_2(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CmyXmlDocU3E__0_3(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CnodeSkillDescU3E__0_4(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CnodeSkillU3E__0_5(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CnodeCommonU3E__0_6(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U3CnodePlayerNumToStartU3E__0_7(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U24this_8(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U24current_9(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U24disposing_10(),
	U3CParseMapU3Ec__Iterator0_t4151616039::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (CRebornSpot_t259671536), -1, sizeof(CRebornSpot_t259671536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3425[12] = 
{
	CRebornSpot_t259671536_StaticFields::get_offset_of_vecTempPos_2(),
	CRebornSpot_t259671536_StaticFields::get_offset_of_vecTempScale_3(),
	CRebornSpot_t259671536_StaticFields::get_offset_of_tempColor_4(),
	CRebornSpot_t259671536::get_offset_of__shapeInner_5(),
	CRebornSpot_t259671536::get_offset_of__shapeOuuter_6(),
	CRebornSpot_t259671536::get_offset_of_m_fAlphaSpeed_7(),
	CRebornSpot_t259671536::get_offset_of_m_fTotalTime_8(),
	CRebornSpot_t259671536::get_offset_of_m_fTimeCounting_9(),
	CRebornSpot_t259671536::get_offset_of_m_fBlingSpeed_10(),
	CRebornSpot_t259671536::get_offset_of__txtLeftTime_11(),
	CRebornSpot_t259671536::get_offset_of_m_fAlpha_12(),
	CRebornSpot_t259671536::get_offset_of_m_nAlphaStatus_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (CMoneySystem_t1851651907), -1, sizeof(CMoneySystem_t1851651907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3426[10] = 
{
	CMoneySystem_t1851651907_StaticFields::get_offset_of_s_Instance_2(),
	CMoneySystem_t1851651907::get_offset_of__txtMoney_3(),
	CMoneySystem_t1851651907::get_offset_of_m_fMoney_4(),
	CMoneySystem_t1851651907::get_offset_of_m_fCurShowValue_5(),
	CMoneySystem_t1851651907::get_offset_of_m_fDelta_6(),
	CMoneySystem_t1851651907::get_offset_of_m_aryLiuGuangSprite_7(),
	CMoneySystem_t1851651907::get_offset_of_m_fLiuGuangInterval_8(),
	CMoneySystem_t1851651907::get_offset_of_m_fLiuGuangSpeed_9(),
	CMoneySystem_t1851651907::get_offset_of__effectLiuGuang_10(),
	CMoneySystem_t1851651907::get_offset_of_m_bMoneyChanged_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (CBeanCollection_t2572073806), -1, sizeof(CBeanCollection_t2572073806_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3427[17] = 
{
	CBeanCollection_t2572073806::get_offset_of__colliderMain_2(),
	CBeanCollection_t2572073806::get_offset_of__srMain_3(),
	CBeanCollection_t2572073806::get_offset_of_m_lstBeanPos_4(),
	CBeanCollection_t2572073806::get_offset_of_m_lstInteractingBall_5(),
	CBeanCollection_t2572073806_StaticFields::get_offset_of_vec2Pivot_6(),
	CBeanCollection_t2572073806_StaticFields::get_offset_of_vec2TempPos_7(),
	CBeanCollection_t2572073806_StaticFields::get_offset_of_vec2TempScale_8(),
	CBeanCollection_t2572073806_StaticFields::get_offset_of_rectTemp_9(),
	CBeanCollection_t2572073806::get_offset_of_m_nCollectionGuid_10(),
	CBeanCollection_t2572073806::get_offset_of_m_lstBeanColliders_11(),
	CBeanCollection_t2572073806::get_offset_of_m_szCollectionGuid_12(),
	CBeanCollection_t2572073806::get_offset_of_m_nIndex_13(),
	CBeanCollection_t2572073806::get_offset_of_m_bInit_14(),
	CBeanCollection_t2572073806::get_offset_of_m_nBeanCount_15(),
	CBeanCollection_t2572073806::get_offset_of_m_vecLeftTopPos_16(),
	CBeanCollection_t2572073806::get_offset_of_m_vecBottomRight_17(),
	CBeanCollection_t2572073806::get_offset_of_m_bActive_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (sBeanSyncInfo_t1176654545)+ sizeof (RuntimeObject), sizeof(sBeanSyncInfo_t1176654545 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3428[3] = 
{
	sBeanSyncInfo_t1176654545::get_offset_of_collection_guid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanSyncInfo_t1176654545::get_offset_of_bean_idx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanSyncInfo_t1176654545::get_offset_of_fDeadTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (sBeanRebornInfo_t2785650986)+ sizeof (RuntimeObject), sizeof(sBeanRebornInfo_t2785650986 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3429[4] = 
{
	sBeanRebornInfo_t2785650986::get_offset_of_collection_guid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanRebornInfo_t2785650986::get_offset_of_bean_idx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanRebornInfo_t2785650986::get_offset_of_fDeadTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanRebornInfo_t2785650986::get_offset_of_fRebornInterval_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (sBeanInfo_t3146055137)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[6] = 
{
	sBeanInfo_t3146055137::get_offset_of_pos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanInfo_t3146055137::get_offset_of_bDead_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanInfo_t3146055137::get_offset_of_nId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanInfo_t3146055137::get_offset_of_colors_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanInfo_t3146055137::get_offset_of_buildin_config_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sBeanInfo_t3146055137::get_offset_of_class_config_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (CMonsterBall_t654437011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[1] = 
{
	CMonsterBall_t654437011::get_offset_of__ball_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (CJiShaInfo_t3249703697), -1, sizeof(CJiShaInfo_t3249703697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3432[56] = 
{
	CJiShaInfo_t3249703697_StaticFields::get_offset_of_s_Instance_2(),
	CJiShaInfo_t3249703697_StaticFields::get_offset_of_vecTempPos_3(),
	CJiShaInfo_t3249703697_StaticFields::get_offset_of_vecTempScale_4(),
	CJiShaInfo_t3249703697::get_offset_of_m_fShowTime_5(),
	CJiShaInfo_t3249703697::get_offset_of_m_fShowTimeCount_6(),
	CJiShaInfo_t3249703697::get_offset_of__panelJiSha_7(),
	CJiShaInfo_t3249703697::get_offset_of__txtKillerName_8(),
	CJiShaInfo_t3249703697::get_offset_of__txtDeadMeatName_9(),
	CJiShaInfo_t3249703697::get_offset_of__imgKillerAvatar_10(),
	CJiShaInfo_t3249703697::get_offset_of__imgDeadMeatAvatar_11(),
	CJiShaInfo_t3249703697::get_offset_of__panelJiSha_PC_12(),
	CJiShaInfo_t3249703697::get_offset_of__txtKillerName_PC_13(),
	CJiShaInfo_t3249703697::get_offset_of__txtDeadMeatName_PC_14(),
	CJiShaInfo_t3249703697::get_offset_of__imgKillerAvatar_PC_15(),
	CJiShaInfo_t3249703697::get_offset_of__imgDeadMeatAvatar_PC_16(),
	CJiShaInfo_t3249703697::get_offset_of__panelJiSha_MOBILE_17(),
	CJiShaInfo_t3249703697::get_offset_of__txtKillerName_MOBILE_18(),
	CJiShaInfo_t3249703697::get_offset_of__txtDeadMeatName_MOBILE_19(),
	CJiShaInfo_t3249703697::get_offset_of__imgKillerAvatar_MOBILE_20(),
	CJiShaInfo_t3249703697::get_offset_of__imgDeadMeatAvatar_MOBILE_21(),
	CJiShaInfo_t3249703697::get_offset_of__imgBgBar_22(),
	CJiShaInfo_t3249703697::get_offset_of__txtAssistTitle_23(),
	CJiShaInfo_t3249703697::get_offset_of__goAssistAvatarContainer_24(),
	CJiShaInfo_t3249703697::get_offset_of__preAvatar_25(),
	CJiShaInfo_t3249703697::get_offset_of__txtJiSha_26(),
	CJiShaInfo_t3249703697::get_offset_of__CanvasGroup_27(),
	CJiShaInfo_t3249703697::get_offset_of__containerKillerAvatar_28(),
	CJiShaInfo_t3249703697::get_offset_of__containerDeadmeatAvatar_29(),
	CJiShaInfo_t3249703697::get_offset_of__toggleNoName_30(),
	CJiShaInfo_t3249703697::get_offset_of_m_bShow_31(),
	CJiShaInfo_t3249703697::get_offset_of_m_fAssistAvatarGap_32(),
	CJiShaInfo_t3249703697::get_offset_of_m_fAssistAvatarScale_33(),
	CJiShaInfo_t3249703697::get_offset_of_m_fDuoShaTime_34(),
	CJiShaInfo_t3249703697::get_offset_of_m_fAssistTime_35(),
	CJiShaInfo_t3249703697::get_offset_of_m_nContinuousKillStartThreshold_36(),
	CJiShaInfo_t3249703697::get_offset_of_m_fFadeTime_37(),
	CJiShaInfo_t3249703697::get_offset_of_m_bShowName_38(),
	CJiShaInfo_t3249703697::get_offset_of_m_fAssistContainerPosY_39(),
	CJiShaInfo_t3249703697::get_offset_of_m_fNoNameModeKillerAvatarPosX_40(),
	CJiShaInfo_t3249703697::get_offset_of_m_fNoNameModeDeadMeatAvatarPosX_41(),
	CJiShaInfo_t3249703697::get_offset_of_m_fNoNameBgBarScale_42(),
	CJiShaInfo_t3249703697::get_offset_of_m_fAssistContainerPosX_NoName_43(),
	CJiShaInfo_t3249703697::get_offset_of_m_fHaveNameBgWidth_44(),
	CJiShaInfo_t3249703697::get_offset_of_m_fAssistContainerPosX_HaveName_45(),
	CJiShaInfo_t3249703697::get_offset_of_m_bFirstBlood_46(),
	CJiShaInfo_t3249703697::get_offset_of_m_aryDuoShaSystemInfo_47(),
	CJiShaInfo_t3249703697::get_offset_of_m_aryContinuousKillSystemInfo_48(),
	CJiShaInfo_t3249703697::get_offset_of_m_szFirstBloodSystemInfo_49(),
	CJiShaInfo_t3249703697::get_offset_of_m_szJiShaSystemInfo_50(),
	CJiShaInfo_t3249703697::get_offset_of_m_lstAssistAvatar_51(),
	0,
	0,
	CJiShaInfo_t3249703697::get_offset_of_m_lstJiShaSysMsgQueue_54(),
	CJiShaInfo_t3249703697::get_offset_of_m_bUIInited_55(),
	CJiShaInfo_t3249703697::get_offset_of_lstAssistId_56(),
	CJiShaInfo_t3249703697::get_offset_of_m_bFading_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (eJiShaSystemMsgType_t1704052883)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3433[5] = 
{
	eJiShaSystemMsgType_t1704052883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (sJiShaSystemMsgParam_t2334122302)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[9] = 
{
	sJiShaSystemMsgParam_t2334122302::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_szEaterName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_szDeadmeatName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_sprKillerAvatar_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_sprDeadMeatAvatar_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_aryAssistId_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_nDuoShaNum_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_nLianXuShaNum_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sJiShaSystemMsgParam_t2334122302::get_offset_of_bFirstBlood_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (CMsgSystem_t1578259481), -1, sizeof(CMsgSystem_t1578259481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3435[4] = 
{
	CMsgSystem_t1578259481_StaticFields::get_offset_of_s_Instance_2(),
	CMsgSystem_t1578259481::get_offset_of__aryMsg_3(),
	CMsgSystem_t1578259481::get_offset_of_m_aryMsgContent_4(),
	CMsgSystem_t1578259481_StaticFields::get_offset_of_m_sMsgContent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (eSysMsgType_t661633863)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3436[9] = 
{
	eSysMsgType_t661633863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (eMsgId_t264670947)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3437[17] = 
{
	eMsgId_t264670947::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (BoxOutline_t1562695836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[3] = 
{
	0,
	BoxOutline_t1562695836::get_offset_of_m_halfSampleCountX_8(),
	BoxOutline_t1562695836::get_offset_of_m_halfSampleCountY_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (CircleOutline_t408400008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[3] = 
{
	CircleOutline_t408400008::get_offset_of_m_circleCount_7(),
	CircleOutline_t408400008::get_offset_of_m_firstSample_8(),
	CircleOutline_t408400008::get_offset_of_m_sampleIncrement_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3440[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (ModifiedShadow_t4034257529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (Outline8_t1486056445), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (OutlineText_t3202294091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[4] = 
{
	OutlineText_t3202294091::get_offset_of_textGraphics_0(),
	OutlineText_t3202294091::get_offset_of_canvas_1(),
	OutlineText_t3202294091::get_offset_of_text_2(),
	OutlineText_t3202294091::get_offset_of_outline_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (CBriefRankingListItem_t3256725699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (CPaiHangBang_t2383002049), -1, sizeof(CPaiHangBang_t2383002049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3446[13] = 
{
	CPaiHangBang_t2383002049::get_offset_of_m_goPanel_2(),
	CPaiHangBang_t2383002049::get_offset_of_m_prePaiHangBangItem_3(),
	CPaiHangBang_t2383002049::get_offset_of_m_fItemYGap_4(),
	CPaiHangBang_t2383002049::get_offset_of_m_fItemColGap_5(),
	CPaiHangBang_t2383002049::get_offset_of_m_nNumOfCol_6(),
	CPaiHangBang_t2383002049::get_offset_of_m_goContainer_7(),
	CPaiHangBang_t2383002049_StaticFields::get_offset_of_vecTempPos_8(),
	CPaiHangBang_t2383002049_StaticFields::get_offset_of_vecTempScale_9(),
	CPaiHangBang_t2383002049_StaticFields::get_offset_of_s_Instance_10(),
	CPaiHangBang_t2383002049::get_offset_of_m_lstItems_11(),
	CPaiHangBang_t2383002049::get_offset_of_c_fRefreshPlayerListInterval_12(),
	CPaiHangBang_t2383002049::get_offset_of_m_fRefreshPlayerListTimeCount_13(),
	CPaiHangBang_t2383002049::get_offset_of_lstTempPlayerAccount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (sPlayerAccount_t3053572449)+ sizeof (RuntimeObject), sizeof(sPlayerAccount_t3053572449_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3447[12] = 
{
	sPlayerAccount_t3053572449::get_offset_of_szName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nOwnerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_fTotalVolume_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nLevel_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nEatThornNum_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nKillNum_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nBeKilledNum_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nAssistNum_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nItem0Num_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nItem1Num_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nItem2Num_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t3053572449::get_offset_of_nItem3Num_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (CPaiHangBang_Mobile_t896378859), -1, sizeof(CPaiHangBang_Mobile_t896378859_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3448[27] = 
{
	CPaiHangBang_Mobile_t896378859_StaticFields::get_offset_of_s_Instance_2(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_preItem_3(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_goHighLight_4(),
	CPaiHangBang_Mobile_t896378859::get_offset_of__panelPaiHangBang_5(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_goList_6(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_goItemContainer_7(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_fMoveSpeed_8(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_fVerticalSpace_9(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_nTotalNum_10(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_fListHeight_11(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_bMovingDown_12(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_bMovingUp_13(),
	CPaiHangBang_Mobile_t896378859_StaticFields::get_offset_of_vecTemp_14(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_lstItems_15(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_aryRecords_16(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_sprMainPlayer_Left_17(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_sprMainPlayer_Right_18(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_sprOtherClient_Left_19(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_sprOtherClient_Right_20(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_fRefreshTimeCount_21(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_c_fRefreshPlayerListInterval_22(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_fRefreshPlayerListTimeCount_23(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_lstTempPlayerAccount_24(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_lstFakePlayerInfo_25(),
	CPaiHangBang_Mobile_t896378859_StaticFields::get_offset_of_s_nFakePlayerId_26(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_nMainPlayerRank_27(),
	CPaiHangBang_Mobile_t896378859::get_offset_of_m_dicPaiHangBangPlayerInfo_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (sPlayerAccount_t97667272)+ sizeof (RuntimeObject), sizeof(sPlayerAccount_t97667272_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3449[18] = 
{
	sPlayerAccount_t97667272::get_offset_of_szName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nOwnerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_fTotalVolume_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nLevel_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nEatThornNum_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nKillNum_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nBeKilledNum_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nAssistNum_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nItem0Num_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nItem1Num_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nItem2Num_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nItem3Num_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nSelectedSkillId_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nSelectedSkillLevel_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_bIsMainPlayer_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_bDead_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nSkinId_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlayerAccount_t97667272::get_offset_of_nRank_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (CPaiHangBangItem_t1282084284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3450[7] = 
{
	CPaiHangBangItem_t1282084284::get_offset_of__txtName_2(),
	CPaiHangBangItem_t1282084284::get_offset_of__txtNo_3(),
	CPaiHangBangItem_t1282084284::get_offset_of__txtLevel_4(),
	CPaiHangBangItem_t1282084284::get_offset_of__txtTotalVolume_5(),
	CPaiHangBangItem_t1282084284::get_offset_of__txtExplodeThornNum_6(),
	CPaiHangBangItem_t1282084284::get_offset_of__txtJiShaInfo_7(),
	CPaiHangBangItem_t1282084284::get_offset_of__aryItemNum_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (CPaiHangBangItem_Mobile_t634260192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[1] = 
{
	CPaiHangBangItem_Mobile_t634260192::get_offset_of__txtPlayerName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (CPaiHangBangRecord_t1494756022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[13] = 
{
	CPaiHangBangRecord_t1494756022::get_offset_of__imgBg_2(),
	CPaiHangBangRecord_t1494756022::get_offset_of__imgAvatar_3(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtRank_4(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtPlayerName_5(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtPlayerLevel_6(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtJiShaInfo_7(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtTotalVolume_8(),
	CPaiHangBangRecord_t1494756022::get_offset_of__aryTxtItemLevel_9(),
	CPaiHangBangRecord_t1494756022::get_offset_of__imgSelectedSkillIcon_10(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtSelectedSkillLevel_11(),
	CPaiHangBangRecord_t1494756022::get_offset_of__txtEatThornNum_12(),
	CPaiHangBangRecord_t1494756022::get_offset_of__imgHighLight_13(),
	CPaiHangBangRecord_t1494756022::get_offset_of_m_nIndex_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (UI_ShowPaiHangBang_t4182849592), -1, sizeof(UI_ShowPaiHangBang_t4182849592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3453[2] = 
{
	UI_ShowPaiHangBang_t4182849592::get_offset_of_m_Main_2(),
	UI_ShowPaiHangBang_t4182849592_StaticFields::get_offset_of_s_bUsingUi_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (PlayerAction_t3782228511), -1, sizeof(PlayerAction_t3782228511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3454[60] = 
{
	PlayerAction_t3782228511_StaticFields::get_offset_of_vecTempPos_2(),
	PlayerAction_t3782228511::get_offset_of_m_fArrowRadius_3(),
	PlayerAction_t3782228511::get_offset_of__arrowYaoGan_4(),
	PlayerAction_t3782228511::get_offset_of__canvas_5(),
	PlayerAction_t3782228511::get_offset_of__global_6(),
	PlayerAction_t3782228511::get_offset_of__center_7(),
	PlayerAction_t3782228511::get_offset_of__world_cursor_8(),
	PlayerAction_t3782228511::get_offset_of__screen_cursor_9(),
	PlayerAction_t3782228511::get_offset_of__cursor_direction_10(),
	PlayerAction_t3782228511::get_offset_of__joystick_movement_11(),
	PlayerAction_t3782228511::get_offset_of__cursor_velocity_12(),
	PlayerAction_t3782228511::get_offset_of__cursor_realtime_velocity_13(),
	PlayerAction_t3782228511::get_offset_of__cursor_radius_14(),
	PlayerAction_t3782228511::get_offset_of__cursor_move_time_velocity_15(),
	0,
	PlayerAction_t3782228511::get_offset_of__ball_velocity_17(),
	PlayerAction_t3782228511::get_offset_of__ball_realtime_velocity_18(),
	PlayerAction_t3782228511::get_offset_of__ball_realtime_velocity_without_size_19(),
	PlayerAction_t3782228511::get_offset_of__center_move_velocity_20(),
	0,
	0,
	PlayerAction_t3782228511::get_offset_of__center_move_time_velocity_23(),
	PlayerAction_t3782228511::get_offset_of__center_move_time_24(),
	PlayerAction_t3782228511::get_offset_of__release_joystick_keep_position_25(),
	PlayerAction_t3782228511::get_offset_of__release_joystick_fast_stop_26(),
	PlayerAction_t3782228511::get_offset_of__release_joystick_fast_rotation_27(),
	PlayerAction_t3782228511::get_offset_of__touch_screen_position_28(),
	PlayerAction_t3782228511::get_offset_of__touch_world_position_29(),
	0,
	PlayerAction_t3782228511::get_offset_of__touch_start_time_31(),
	PlayerAction_t3782228511::get_offset_of_joystick_radius_multiple_32(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_s_Instance_33(),
	PlayerAction_t3782228511::get_offset_of_m_nFrameCount_34(),
	0,
	PlayerAction_t3782228511::get_offset_of_m_vec2MoveInfo_36(),
	PlayerAction_t3782228511::get_offset_of_m_dMouseDownTime_37(),
	PlayerAction_t3782228511::get_offset_of_balls_center_position_38(),
	PlayerAction_t3782228511::get_offset_of_balls_min_position_39(),
	PlayerAction_t3782228511::get_offset_of_balls_max_position_40(),
	PlayerAction_t3782228511::get_offset_of_m_vecBallsCenter_41(),
	PlayerAction_t3782228511::get_offset_of_dicTempGroupInGrass_42(),
	PlayerAction_t3782228511::get_offset_of_m_fPkSelfBallTimeCount_43(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_s_nfLeftBallIndex_44(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_s_nRightBallIndex_45(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_s_nTopBallIndex_46(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_s_nBottomBallIndex_47(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_s_nBiggestBallIndex_48(),
	PlayerAction_t3782228511::get_offset_of_m_lstMostBigBallsToShowName_49(),
	PlayerAction_t3782228511::get_offset_of_m_bPosInfoCalculated_50(),
	PlayerAction_t3782228511::get_offset_of_m_fShitTimeElapse_51(),
	PlayerAction_t3782228511_StaticFields::get_offset_of_vecTempDir_52(),
	PlayerAction_t3782228511::get_offset_of_m_vecLastFrameBallsCenterMovement_53(),
	PlayerAction_t3782228511::get_offset_of_m_vecLastFrameBallsPostion_54(),
	PlayerAction_t3782228511::get_offset_of_m_bFirstForBallsCenterMovement_55(),
	PlayerAction_t3782228511::get_offset_of_m_fBallTeamRadius_56(),
	PlayerAction_t3782228511::get_offset_of_vecLeftBottom_57(),
	PlayerAction_t3782228511::get_offset_of_vecRightTop_58(),
	PlayerAction_t3782228511::get_offset_of_m_lstMoveOrderX_59(),
	PlayerAction_t3782228511::get_offset_of_m_lstMoveOrderY_60(),
	PlayerAction_t3782228511::get_offset_of__vecYaoGanDir_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (CGrass_t734084476), -1, sizeof(CGrass_t734084476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3455[7] = 
{
	CGrass_t734084476_StaticFields::get_offset_of_cTempColor_7(),
	CGrass_t734084476::get_offset_of_m_lstBalls_8(),
	CGrass_t734084476::get_offset_of_m_bMainPlayerIn_9(),
	CGrass_t734084476::get_offset_of_m_Config_10(),
	0,
	CGrass_t734084476::get_offset_of_m_fCheckMainPlayerInTimeCount_12(),
	CGrass_t734084476::get_offset_of_m_bRecoveringAlpha_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (CGrassEditor_t1538907628), -1, sizeof(CGrassEditor_t1538907628_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3456[16] = 
{
	CGrassEditor_t1538907628_StaticFields::get_offset_of_s_Instance_2(),
	CGrassEditor_t1538907628_StaticFields::get_offset_of_vecTempScale_3(),
	CGrassEditor_t1538907628_StaticFields::get_offset_of_vecTempPos_4(),
	CGrassEditor_t1538907628::get_offset_of_m_goGrassContainer_5(),
	CGrassEditor_t1538907628::get_offset_of__dropdownGrassID_6(),
	CGrassEditor_t1538907628::get_offset_of__dropdownGrassType_7(),
	CGrassEditor_t1538907628::get_offset_of__inputPolygonId_8(),
	CGrassEditor_t1538907628::get_offset_of__inputColor_9(),
	0,
	CGrassEditor_t1538907628::get_offset_of__aryInputValue_11(),
	CGrassEditor_t1538907628::get_offset_of__aryInputDesc_12(),
	CGrassEditor_t1538907628::get_offset_of__btnAddGrass_13(),
	CGrassEditor_t1538907628::get_offset_of_m_CurGrassConfig_14(),
	CGrassEditor_t1538907628::get_offset_of_m_dicGrassConfig_15(),
	CGrassEditor_t1538907628::get_offset_of_tempGrassConfig_16(),
	CGrassEditor_t1538907628_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (eGrassFunc_t2798151921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3457[4] = 
{
	eGrassFunc_t2798151921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (sGrassConfig_t73951773)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3458[6] = 
{
	sGrassConfig_t73951773::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassConfig_t73951773::get_offset_of_nFunc_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassConfig_t73951773::get_offset_of_nPolygonId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassConfig_t73951773::get_offset_of_szColor_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassConfig_t73951773::get_offset_of_aryValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassConfig_t73951773::get_offset_of_aryDesc_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (sGrassInfo_t1501140634)+ sizeof (RuntimeObject), sizeof(sGrassInfo_t1501140634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3459[4] = 
{
	sGrassInfo_t1501140634::get_offset_of_nConfigId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassInfo_t1501140634::get_offset_of_fScaleX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassInfo_t1501140634::get_offset_of_fScaleY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sGrassInfo_t1501140634::get_offset_of_fRotation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (CPveEditor_t756955826), -1, sizeof(CPveEditor_t756955826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3460[10] = 
{
	CPveEditor_t756955826::get_offset_of_m_arySubPanel_2(),
	CPveEditor_t756955826_StaticFields::get_offset_of_vecTempPos_3(),
	CPveEditor_t756955826_StaticFields::get_offset_of_s_Instance_4(),
	CPveEditor_t756955826::get_offset_of__btnGrass_5(),
	CPveEditor_t756955826::get_offset_of__btnSpray_6(),
	CPveEditor_t756955826::get_offset_of__inputSelectObjScaleX_7(),
	CPveEditor_t756955826::get_offset_of__inputSelectObjScaleY_8(),
	CPveEditor_t756955826::get_offset_of__inputSelectObjRotation_9(),
	CPveEditor_t756955826::get_offset_of__btnDelCurSelectedObj_10(),
	CPveEditor_t756955826::get_offset_of_m_CurSelectedObj_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (CPveObj_t2172873472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[5] = 
{
	CPveObj_t2172873472::get_offset_of_m_vScale_2(),
	CPveObj_t2172873472::get_offset_of_m_fRotation_3(),
	CPveObj_t2172873472::get_offset_of_m_nGuid_4(),
	CPveObj_t2172873472::get_offset_of_m_ConfigId_5(),
	CPveObj_t2172873472::get_offset_of__srMain_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (CSpray_t348869720), -1, sizeof(CSpray_t348869720_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3462[6] = 
{
	CSpray_t348869720_StaticFields::get_offset_of_vecTempPos_7(),
	CSpray_t348869720::get_offset_of_m_Config_8(),
	CSpray_t348869720::get_offset_of_m_goMonsterContainer_9(),
	CSpray_t348869720::get_offset_of_m_fSprayTimeCount_10(),
	CSpray_t348869720::get_offset_of_m_lstLifeTimeLoop_SprayTime_11(),
	CSpray_t348869720::get_offset_of_m_lstLifeTimeLoop_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (CSpryEditor_t3631257256), -1, sizeof(CSpryEditor_t3631257256_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3463[18] = 
{
	CSpryEditor_t3631257256_StaticFields::get_offset_of_vecTempPos_2(),
	CSpryEditor_t3631257256_StaticFields::get_offset_of_vecTempScale_3(),
	CSpryEditor_t3631257256_StaticFields::get_offset_of_s_Instance_4(),
	CSpryEditor_t3631257256::get_offset_of_m_goSprayContainer_5(),
	CSpryEditor_t3631257256::get_offset_of__toogleShowRealtimeBeanSpray_6(),
	CSpryEditor_t3631257256::get_offset_of__dropdownSprayId_7(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldThornId_8(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldDensity_9(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldMeatDensity_10(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldMaxDis_11(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldMinDis_12(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldBeanLifeTime_13(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldSprayInterval_14(),
	CSpryEditor_t3631257256::get_offset_of__inputfieldSpraySize_15(),
	CSpryEditor_t3631257256::get_offset_of_m_dicSprayConfig_16(),
	CSpryEditor_t3631257256::get_offset_of_m_CurSprayConfig_17(),
	CSpryEditor_t3631257256::get_offset_of_tempSprayConfig_18(),
	CSpryEditor_t3631257256_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (sSprayConfig_t1865836932)+ sizeof (RuntimeObject), sizeof(sSprayConfig_t1865836932_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3464[8] = 
{
	sSprayConfig_t1865836932::get_offset_of_nConfigId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_szThornId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_fDensity_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_fMaxDis_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_fMinDis_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_fLifeTime_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_fInterval_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSprayConfig_t1865836932::get_offset_of_fSpraySize_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (CSkinManager_t2556315286), -1, sizeof(CSkinManager_t2556315286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3465[4] = 
{
	CSkinManager_t2556315286_StaticFields::get_offset_of_m_dicSkinSprites_2(),
	CSkinManager_t2556315286_StaticFields::get_offset_of_m_dicSkinMaterial_3(),
	0,
	CSkinManager_t2556315286_StaticFields::get_offset_of_m_matNoSkinMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (CRobotManager_t2137154153), -1, sizeof(CRobotManager_t2137154153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3466[1] = 
{
	CRobotManager_t2137154153_StaticFields::get_offset_of_s_Instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (SceneStatus_t2403225508)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3467[4] = 
{
	SceneStatus_t2403225508::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (SceneAction_t888531997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[14] = 
{
	0,
	SceneAction_t888531997::get_offset_of__ball_queue_3(),
	SceneAction_t888531997::get_offset_of__ball_prefab_4(),
	SceneAction_t888531997::get_offset_of__num_of_ball_per_batch_5(),
	SceneAction_t888531997::get_offset_of__num_of_ball_batch_6(),
	SceneAction_t888531997::get_offset_of__food_prefabs_7(),
	SceneAction_t888531997::get_offset_of__num_of_food_per_batch_8(),
	SceneAction_t888531997::get_offset_of__num_of_food_batch_9(),
	SceneAction_t888531997::get_offset_of__food_queue_10(),
	SceneAction_t888531997::get_offset_of__last_refresh_food_time_11(),
	SceneAction_t888531997::get_offset_of__refresh_food_interval_12(),
	SceneAction_t888531997::get_offset_of__hedge_prefab_13(),
	SceneAction_t888531997::get_offset_of__num_of_hedge_14(),
	SceneAction_t888531997::get_offset_of__scene_status_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (CSettingsManager_t1554003656), -1, sizeof(CSettingsManager_t1554003656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3469[10] = 
{
	CSettingsManager_t1554003656_StaticFields::get_offset_of_s_Instance_2(),
	CSettingsManager_t1554003656::get_offset_of_m_fVolumeChangeAmount_3(),
	CSettingsManager_t1554003656::get_offset_of__sliderMusic_4(),
	CSettingsManager_t1554003656::get_offset_of_m_arySliderMusicVolume_5(),
	CSettingsManager_t1554003656::get_offset_of__sliderAudio_6(),
	CSettingsManager_t1554003656::get_offset_of_m_arySliderAudioVolume_7(),
	CSettingsManager_t1554003656::get_offset_of_m_aryMusicVolume_8(),
	CSettingsManager_t1554003656::get_offset_of_m_aryAudioVolume_9(),
	CSettingsManager_t1554003656::get_offset_of_m_fMusicVolume_10(),
	CSettingsManager_t1554003656::get_offset_of_m_fAudioVolume_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (eCtrlType_t553487726)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3470[3] = 
{
	eCtrlType_t553487726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (CArc_t1499038007), -1, sizeof(CArc_t1499038007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3471[5] = 
{
	CArc_t1499038007::get_offset_of_m_aryArcs_2(),
	CArc_t1499038007::get_offset_of_m_fArcLength_3(),
	CArc_t1499038007::get_offset_of__goArcsContianer_4(),
	CArc_t1499038007_StaticFields::get_offset_of_vecTempScale_5(),
	CArc_t1499038007::get_offset_of_s_Shit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (PolyMap_t1466772104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (DistanceResult_t3958382930)+ sizeof (RuntimeObject), sizeof(DistanceResult_t3958382930 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3473[2] = 
{
	DistanceResult_t3958382930::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistanceResult_t3958382930::get_offset_of_index2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (ShapeType_t1878054890)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3474[6] = 
{
	ShapeType_t1878054890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (FillType_t681241845)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3475[9] = 
{
	FillType_t681241845::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (GradientType_t2325005612)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3476[4] = 
{
	GradientType_t2325005612::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (GradientAxis_t2967222705)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3477[3] = 
{
	GradientAxis_t2967222705::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (PolygonPreset_t3238801606)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3478[14] = 
{
	PolygonPreset_t3238801606::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (PathSegment_t2930971005)+ sizeof (RuntimeObject), sizeof(PathSegment_t2930971005 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3479[3] = 
{
	PathSegment_t2930971005::get_offset_of_p0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathSegment_t2930971005::get_offset_of_p1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathSegment_t2930971005::get_offset_of_p2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (Shape_t3950352489), -1, sizeof(Shape_t3950352489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3480[16] = 
{
	Shape_t3950352489_StaticFields::get_offset_of_MaxPolygonVertices_2(),
	Shape_t3950352489_StaticFields::get_offset_of_PolyMapResolution_3(),
	Shape_t3950352489_StaticFields::get_offset_of_MaxPathSegments_4(),
	Shape_t3950352489::get_offset_of_material_5(),
	Shape_t3950352489::get_offset_of_spriteRenderer_6(),
	Shape_t3950352489::get_offset_of_image_7(),
	Shape_t3950352489::get_offset_of_rectTransform_8(),
	Shape_t3950352489::get_offset_of_computedScale_9(),
	Shape_t3950352489::get_offset_of_computedRect_10(),
	Shape_t3950352489::get_offset_of_settings_11(),
	Shape_t3950352489::get_offset_of_shaderSettings_12(),
	Shape_t3950352489::get_offset_of_wasConverted_13(),
	Shape_t3950352489::get_offset_of_preConvertedScale_14(),
	Shape_t3950352489_StaticFields::get_offset_of__ShaderVertexVarNames_15(),
	Shape_t3950352489_StaticFields::get_offset_of__ShaderPointsVarNames_16(),
	Shape_t3950352489_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (ShapeDrawOrderComparer_t3317179762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (UserProps_t2498857227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3482[37] = 
{
	UserProps_t2498857227::get_offset_of_dirty_0(),
	UserProps_t2498857227::get_offset_of_polyMapNeedsRegen_1(),
	UserProps_t2498857227::get_offset_of__blur_2(),
	UserProps_t2498857227::get_offset_of__correctScaling_3(),
	UserProps_t2498857227::get_offset_of__endAngle_4(),
	UserProps_t2498857227::get_offset_of__fillColor_5(),
	UserProps_t2498857227::get_offset_of__fillColor2_6(),
	UserProps_t2498857227::get_offset_of__fillOffset_7(),
	UserProps_t2498857227::get_offset_of__fillPathLoops_8(),
	UserProps_t2498857227::get_offset_of__fillRotation_9(),
	UserProps_t2498857227::get_offset_of__fillScale_10(),
	UserProps_t2498857227::get_offset_of__fillTexture_11(),
	UserProps_t2498857227::get_offset_of__fillType_12(),
	UserProps_t2498857227::get_offset_of__gradientAxis_13(),
	UserProps_t2498857227::get_offset_of__gradientStart_14(),
	UserProps_t2498857227::get_offset_of__gradientType_15(),
	UserProps_t2498857227::get_offset_of__gridSize_16(),
	UserProps_t2498857227::get_offset_of__innerCutout_17(),
	UserProps_t2498857227::get_offset_of__invertArc_18(),
	UserProps_t2498857227::get_offset_of__lineSize_19(),
	UserProps_t2498857227::get_offset_of__obsoleteNumSides_20(),
	UserProps_t2498857227::get_offset_of__outlineColor_21(),
	UserProps_t2498857227::get_offset_of__outlineSize_22(),
	UserProps_t2498857227::get_offset_of__pathSegments_23(),
	UserProps_t2498857227::get_offset_of__pathThickness_24(),
	UserProps_t2498857227::get_offset_of__polygonPreset_25(),
	UserProps_t2498857227::get_offset_of__polyVertices_26(),
	UserProps_t2498857227::get_offset_of__roundness_27(),
	UserProps_t2498857227::get_offset_of__roundnessBottomLeft_28(),
	UserProps_t2498857227::get_offset_of__roundnessBottomRight_29(),
	UserProps_t2498857227::get_offset_of__roundnessPerCorner_30(),
	UserProps_t2498857227::get_offset_of__roundnessTopLeft_31(),
	UserProps_t2498857227::get_offset_of__roundnessTopRight_32(),
	UserProps_t2498857227::get_offset_of__shapeType_33(),
	UserProps_t2498857227::get_offset_of__startAngle_34(),
	UserProps_t2498857227::get_offset_of__triangleOffset_35(),
	UserProps_t2498857227::get_offset_of__usePolygonMap_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (ShaderProps_t3992238139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[34] = 
{
	ShaderProps_t3992238139::get_offset_of_xScale_0(),
	ShaderProps_t3992238139::get_offset_of_yScale_1(),
	ShaderProps_t3992238139::get_offset_of_shapeType_2(),
	ShaderProps_t3992238139::get_offset_of_outlineSize_3(),
	ShaderProps_t3992238139::get_offset_of_blur_4(),
	ShaderProps_t3992238139::get_offset_of_outlineColor_5(),
	ShaderProps_t3992238139::get_offset_of_fillType_6(),
	ShaderProps_t3992238139::get_offset_of_fillColor_7(),
	ShaderProps_t3992238139::get_offset_of_fillColor2_8(),
	ShaderProps_t3992238139::get_offset_of_fillRotation_9(),
	ShaderProps_t3992238139::get_offset_of_fillOffset_10(),
	ShaderProps_t3992238139::get_offset_of_fillScale_11(),
	ShaderProps_t3992238139::get_offset_of_gradientType_12(),
	ShaderProps_t3992238139::get_offset_of_gradientAxis_13(),
	ShaderProps_t3992238139::get_offset_of_gradientStart_14(),
	ShaderProps_t3992238139::get_offset_of_fillTexture_15(),
	ShaderProps_t3992238139::get_offset_of_gridSize_16(),
	ShaderProps_t3992238139::get_offset_of_lineSize_17(),
	ShaderProps_t3992238139::get_offset_of_roundnessVec_18(),
	ShaderProps_t3992238139::get_offset_of_innerRadii_19(),
	ShaderProps_t3992238139::get_offset_of_arcMinAngle_20(),
	ShaderProps_t3992238139::get_offset_of_arcMaxAngle_21(),
	ShaderProps_t3992238139::get_offset_of_invertArc_22(),
	ShaderProps_t3992238139::get_offset_of_useArc_23(),
	ShaderProps_t3992238139::get_offset_of_correctScaling_24(),
	ShaderProps_t3992238139::get_offset_of_triangleOffset_25(),
	ShaderProps_t3992238139::get_offset_of_numPolyVerts_26(),
	ShaderProps_t3992238139::get_offset_of_polyVertices_27(),
	ShaderProps_t3992238139::get_offset_of_usePolygonMap_28(),
	ShaderProps_t3992238139::get_offset_of_polyMap_29(),
	ShaderProps_t3992238139::get_offset_of_pathThickness_30(),
	ShaderProps_t3992238139::get_offset_of_numPathSegments_31(),
	ShaderProps_t3992238139::get_offset_of_pathPoints_32(),
	ShaderProps_t3992238139::get_offset_of_fillPathLoops_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (Shapes2DPrefs_t998444370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[1] = 
{
	Shapes2DPrefs_t998444370::get_offset_of_pixelsPerUnit_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (CBuyConfirmUI_t2670430472), -1, sizeof(CBuyConfirmUI_t2670430472_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3485[14] = 
{
	CBuyConfirmUI_t2670430472_StaticFields::get_offset_of_vecTempScale_2(),
	CBuyConfirmUI_t2670430472_StaticFields::get_offset_of_s_Instance_3(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_aryTimeLimitButtons_4(),
	CBuyConfirmUI_t2670430472::get_offset_of__btnBuy_5(),
	CBuyConfirmUI_t2670430472::get_offset_of__txtCost_6(),
	CBuyConfirmUI_t2670430472::get_offset_of__txtCostType_7(),
	CBuyConfirmUI_t2670430472::get_offset_of__txtGain_8(),
	CBuyConfirmUI_t2670430472::get_offset_of__imgItem_9(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_nGainNum_10(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_nGainType_11(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_szProductId_12(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_fAnimationScaleSpeed_13(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_fAnimationScaleMin_14(),
	CBuyConfirmUI_t2670430472::get_offset_of_m_fAnimationScaleMax_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (CBuyResult_t3313452107), -1, sizeof(CBuyResult_t3313452107_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3486[5] = 
{
	CBuyResult_t3313452107_StaticFields::get_offset_of_s_Instance_2(),
	CBuyResult_t3313452107::get_offset_of__goContainerAll_3(),
	CBuyResult_t3313452107::get_offset_of__txtInfo_4(),
	CBuyResult_t3313452107::get_offset_of__txtItemName_5(),
	CBuyResult_t3313452107::get_offset_of__imgItemAvatar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (CRechargeCounter_t1817650585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3487[5] = 
{
	CRechargeCounter_t1817650585::get_offset_of_m_nPrice_2(),
	CRechargeCounter_t1817650585::get_offset_of_m_eMonryType_3(),
	CRechargeCounter_t1817650585::get_offset_of_m_nGainMoneyNum_4(),
	CRechargeCounter_t1817650585::get_offset_of_m_szChargeProductId_5(),
	CRechargeCounter_t1817650585::get_offset_of__txtGainMoneyNum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (CShoppingCounter_t3637302240), -1, sizeof(CShoppingCounter_t3637302240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3488[22] = 
{
	CShoppingCounter_t3637302240_StaticFields::get_offset_of_vecTempSize_3(),
	CShoppingCounter_t3637302240_StaticFields::get_offset_of_vec3TempSize_4(),
	CShoppingCounter_t3637302240_StaticFields::get_offset_of_vecTempPos_5(),
	CShoppingCounter_t3637302240::get_offset_of_m_eMoneyType_6(),
	CShoppingCounter_t3637302240::get_offset_of_m_nMoneyValue_7(),
	CShoppingCounter_t3637302240::get_offset_of_m_bBought_8(),
	CShoppingCounter_t3637302240::get_offset_of_m_szItemName_9(),
	CShoppingCounter_t3637302240::get_offset_of_m_nItemId_10(),
	CShoppingCounter_t3637302240::get_offset_of_m_szItemDesc_11(),
	CShoppingCounter_t3637302240::get_offset_of_m_szItemPath_12(),
	CShoppingCounter_t3637302240::get_offset_of_m_imgBg_13(),
	CShoppingCounter_t3637302240::get_offset_of_m_btnBuyAndEquip_14(),
	CShoppingCounter_t3637302240::get_offset_of_m_imgStatus_15(),
	CShoppingCounter_t3637302240::get_offset_of_m_imgSkin_16(),
	CShoppingCounter_t3637302240::get_offset_of_m_imgMoneyType_17(),
	CShoppingCounter_t3637302240::get_offset_of_m_imgMoneyBg_18(),
	CShoppingCounter_t3637302240::get_offset_of_m_txtMoneyValue_19(),
	CShoppingCounter_t3637302240::get_offset_of_m_txtItemDesc_20(),
	CShoppingCounter_t3637302240::get_offset_of_m_txtItemName_21(),
	CShoppingCounter_t3637302240::get_offset_of_m_goContainerEquippedLabel_22(),
	CShoppingCounter_t3637302240::get_offset_of__skeletonGraphic_23(),
	CShoppingCounter_t3637302240::get_offset_of_m_eCurItemStatus_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (eItemStatus_t193384828)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3489[4] = 
{
	eItemStatus_t193384828::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (ShoppingMallManager_t2900135047), -1, sizeof(ShoppingMallManager_t2900135047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3490[76] = 
{
	0,
	ShoppingMallManager_t2900135047::get_offset_of_m_aryContainerAll_3(),
	ShoppingMallManager_t2900135047::get_offset_of__CanvasScaler_4(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fItemCounterInitScale_5(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fItemCounterSelectedScale_6(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fSkeletonPosY_7(),
	ShoppingMallManager_t2900135047::get_offset_of_m_clItems_8(),
	ShoppingMallManager_t2900135047::get_offset_of_m_preShoppingCounter_9(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprNotEquiped_10(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprEquiped_11(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprMoneyBg_NotBought_12(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprMoneyBg_Bought_13(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fMoneyBgWidth_NotBought_14(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fMoneyBgWidth_Bought_15(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fMoneyBgHeight_16(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryMoneyTypeSpr_17(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorBought_Text_18(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorBought_Bg_19(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorNotBought_Text_20(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorNotBought_Bg_21(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorBtnTimeLimit_Selected_22(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorBtnTimeLimit_NotSelected_23(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryBuyButtonStatus_24(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryPageTitle_25(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprPageSelected_26(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprPageNotSelected_27(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorPageSelected_TextContent_28(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorPageNotSelected_TextContent_29(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorPageSelected_TextOutline_30(),
	ShoppingMallManager_t2900135047::get_offset_of_m_colorPageNotSelected_TextOutline_31(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryPageButtons_32(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryPageButtons_Others_33(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryPageTitleText_34(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryPageTitleOutline_35(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fTextPricePos_NotBought_36(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fTextPricePos_Bought_37(),
	ShoppingMallManager_t2900135047::get_offset_of_m_szExpireInfo_38(),
	ShoppingMallManager_t2900135047::get_offset_of_m_nFontSize_NotBought_39(),
	ShoppingMallManager_t2900135047::get_offset_of_m_nFontSize_Bought_40(),
	ShoppingMallManager_t2900135047::get_offset_of_m_txtXiaoLianBiNum_41(),
	ShoppingMallManager_t2900135047::get_offset_of_m_txtMieBiNum_42(),
	ShoppingMallManager_t2900135047::get_offset_of__panelMall_43(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryRechargeCounters_44(),
	ShoppingMallManager_t2900135047::get_offset_of__RechargeConfirmUI_45(),
	ShoppingMallManager_t2900135047::get_offset_of__panelRecharge_46(),
	ShoppingMallManager_t2900135047::get_offset_of__svItemList_47(),
	ShoppingMallManager_t2900135047::get_offset_of_m_aryItemList_48(),
	ShoppingMallManager_t2900135047::get_offset_of_m_eCurPage_49(),
	ShoppingMallManager_t2900135047::get_offset_of_m_dicItemId_50(),
	ShoppingMallManager_t2900135047_StaticFields::get_offset_of_s_Instance_51(),
	ShoppingMallManager_t2900135047_StaticFields::get_offset_of_m_sCurEquipedSkinId_52(),
	ShoppingMallManager_t2900135047_StaticFields::get_offset_of_s_szCurEquipSkinPath_53(),
	ShoppingMallManager_t2900135047::get_offset_of_m_CurBuyingCounter_54(),
	ShoppingMallManager_t2900135047::get_offset_of_m_CurEquipedCounter_55(),
	ShoppingMallManager_t2900135047::get_offset_of__btnConfirmBuy_56(),
	ShoppingMallManager_t2900135047::get_offset_of__btnCancelBuy_57(),
	ShoppingMallManager_t2900135047::get_offset_of__panelConfirmBuyUI_58(),
	ShoppingMallManager_t2900135047::get_offset_of__txtItemName_59(),
	ShoppingMallManager_t2900135047::get_offset_of__txtCost_60(),
	ShoppingMallManager_t2900135047::get_offset_of__imgMoneyType_61(),
	ShoppingMallManager_t2900135047::get_offset_of__imgConfirmBuyAvatar_62(),
	ShoppingMallManager_t2900135047::get_offset_of__aryConfirmBuy_63(),
	ShoppingMallManager_t2900135047::get_offset_of__aryCancelBuy_64(),
	ShoppingMallManager_t2900135047::get_offset_of__aryConfirmBuyUIPanel_65(),
	ShoppingMallManager_t2900135047::get_offset_of__aryItemName_66(),
	ShoppingMallManager_t2900135047::get_offset_of__aryCost_67(),
	ShoppingMallManager_t2900135047::get_offset_of__aryMoneyType_68(),
	ShoppingMallManager_t2900135047::get_offset_of__aryConfirmBuyAvatar_69(),
	ShoppingMallManager_t2900135047::get_offset_of_m_skeletonGraphic_70(),
	ShoppingMallManager_t2900135047::get_offset_of_m_bRecharging_71(),
	ShoppingMallManager_t2900135047::get_offset_of_m_fBuyResultTimeCount_72(),
	ShoppingMallManager_t2900135047::get_offset_of_m_CurSelectedCounter_73(),
	ShoppingMallManager_t2900135047::get_offset_of_m_mutiskeletonGraphic_74(),
	ShoppingMallManager_t2900135047::get_offset_of_m_dicMaterialForSkeleton_75(),
	ShoppingMallManager_t2900135047::get_offset_of_m_dicSkeletonForSkin_76(),
	ShoppingMallManager_t2900135047::get_offset_of_m_sprNoSkin_77(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (eMoneyType_t1561655355)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3491[3] = 
{
	eMoneyType_t1561655355::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (eShoppingMallPageType_t1431384372)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3492[3] = 
{
	eShoppingMallPageType_t1431384372::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (eCtrlTYpe_t732417545)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3493[9] = 
{
	eCtrlTYpe_t732417545::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (U3CLoadSceneU3Ec__Iterator0_t723620668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3494[4] = 
{
	U3CLoadSceneU3Ec__Iterator0_t723620668::get_offset_of_scene_name_0(),
	U3CLoadSceneU3Ec__Iterator0_t723620668::get_offset_of_U24current_1(),
	U3CLoadSceneU3Ec__Iterator0_t723620668::get_offset_of_U24disposing_2(),
	U3CLoadSceneU3Ec__Iterator0_t723620668::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (CSkeletonAnimation_t159686570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3495[1] = 
{
	CSkeletonAnimation_t159686570::get_offset_of__skeletonMain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (CSkillDetailGrid_t2161361482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[4] = 
{
	CSkillDetailGrid_t2161361482::get_offset_of__txtSpeedAffect_2(),
	CSkillDetailGrid_t2161361482::get_offset_of__txtCD_3(),
	CSkillDetailGrid_t2161361482::get_offset_of__txtDuration_4(),
	CSkillDetailGrid_t2161361482::get_offset_of__txtMpCost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (CSkillSystem_t2262672040), -1, sizeof(CSkillSystem_t2262672040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3497[67] = 
{
	CSkillSystem_t2262672040_StaticFields::get_offset_of_colorTemp_2(),
	CSkillSystem_t2262672040::get_offset_of__imgSpitSporeButtonBg_3(),
	CSkillSystem_t2262672040::get_offset_of__goCastButtonPanel_PC_4(),
	CSkillSystem_t2262672040::get_offset_of__goCastButtonPanel_Mobile_5(),
	0,
	0,
	0,
	CSkillSystem_t2262672040::get_offset_of_m_nMax_R_Num_9(),
	CSkillSystem_t2262672040::get_offset_of__arySkillLevelUpSprites_10(),
	CSkillSystem_t2262672040::get_offset_of__arySkillArrowAppearSprites_11(),
	CSkillSystem_t2262672040::get_offset_of__arySkillArrowBreatheSprites_12(),
	CSkillSystem_t2262672040::get_offset_of__arySkillSelectSkillSprites_13(),
	CSkillSystem_t2262672040::get_offset_of__btnPrevBtn_14(),
	CSkillSystem_t2262672040::get_offset_of__btnNextBtn_15(),
	CSkillSystem_t2262672040::get_offset_of_m_nCurPageIndex_16(),
	CSkillSystem_t2262672040::get_offset_of__aryPages_17(),
	0,
	0,
	0,
	CSkillSystem_t2262672040::get_offset_of_m_nPickSkillId_21(),
	0,
	CSkillSystem_t2262672040::get_offset_of_tempSkillParam_23(),
	CSkillSystem_t2262672040::get_offset_of_tempSkillInfo_24(),
	CSkillSystem_t2262672040::get_offset_of_m_dicSkillInfo_25(),
	CSkillSystem_t2262672040::get_offset_of_m_eCurSkillType_26(),
	CSkillSystem_t2262672040::get_offset_of__progressBar_PC_27(),
	CSkillSystem_t2262672040::get_offset_of__progressBar_MOBILE_28(),
	CSkillSystem_t2262672040::get_offset_of__progressBar_29(),
	CSkillSystem_t2262672040::get_offset_of__panelThis_30(),
	CSkillSystem_t2262672040::get_offset_of__txtProgressBar_31(),
	CSkillSystem_t2262672040::get_offset_of__imgProgressBar_32(),
	CSkillSystem_t2262672040::get_offset_of__txtCurTotalPoints_33(),
	CSkillSystem_t2262672040_StaticFields::get_offset_of_s_Instance_34(),
	CSkillSystem_t2262672040::get_offset_of_m_nPoints_35(),
	CSkillSystem_t2262672040::get_offset_of__arySkillCounter_36(),
	CSkillSystem_t2262672040::get_offset_of__arySkillParamsConfig_37(),
	CSkillSystem_t2262672040::get_offset_of__arySkillCastButton_38(),
	CSkillSystem_t2262672040::get_offset_of__arySkillCastButton_PC_39(),
	CSkillSystem_t2262672040::get_offset_of__arySkillCastButton_Mobile_40(),
	CSkillSystem_t2262672040::get_offset_of__panelEditSkillDesc_41(),
	CSkillSystem_t2262672040::get_offset_of__inputSkillDescInput_42(),
	CSkillSystem_t2262672040::get_offset_of__btnQuitSkillDescEdit_43(),
	CSkillSystem_t2262672040::get_offset_of__toggleShowCOlor_44(),
	CSkillSystem_t2262672040::get_offset_of__dropdownSkillId_45(),
	CSkillSystem_t2262672040::get_offset_of_m_bUiInited_46(),
	CSkillSystem_t2262672040::get_offset_of_m_fRefreshCastButtonTimeCount_47(),
	CSkillSystem_t2262672040::get_offset_of_m_nSelectSkillCurPoint_48(),
	CSkillSystem_t2262672040::get_offset_of_m_dicSkillDesc_49(),
	CSkillSystem_t2262672040::get_offset_of_m_szCurEditingSkillDesc_50(),
	CSkillSystem_t2262672040::get_offset_of_m_nCurEditingSkillDescSkillId_51(),
	CSkillSystem_t2262672040::get_offset_of__textDesc_52(),
	CSkillSystem_t2262672040::get_offset_of_m_SpitSporeParam_53(),
	CSkillSystem_t2262672040::get_offset_of__inputRadiusMultiple_54(),
	CSkillSystem_t2262672040::get_offset_of__inputCostMotherVolumePercent_55(),
	CSkillSystem_t2262672040::get_offset_of__inputnNumPerSecond_56(),
	CSkillSystem_t2262672040::get_offset_of__inputEjectSpeed_57(),
	CSkillSystem_t2262672040::get_offset_of__inputPushThornDis_58(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeMinVolume_59(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeMaxVolume_60(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeMaxShowRadius_61(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeA_62(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeX_63(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeB_64(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeC_65(),
	CSkillSystem_t2262672040::get_offset_of__inputSporeHuanTingDistance_66(),
	CSkillSystem_t2262672040::get_offset_of_m_nSporeBtnBlingCount_67(),
	CSkillSystem_t2262672040::get_offset_of_m_colorBling_68(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (eSkillId_t2951149716)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3498[12] = 
{
	eSkillId_t2951149716::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (sSkillParam_t731668951)+ sizeof (RuntimeObject), sizeof(sSkillParam_t731668951_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3499[4] = 
{
	sSkillParam_t731668951::get_offset_of_nCurLevel_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillParam_t731668951::get_offset_of_bAvailable_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillParam_t731668951::get_offset_of_aryValues_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillParam_t731668951::get_offset_of_szValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
