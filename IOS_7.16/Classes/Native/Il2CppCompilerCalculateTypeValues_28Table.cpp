﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HedgehogTeam.EasyTouch.EasyTouch
struct EasyTouch_t1229686981;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Camera
struct Camera_t4157153871;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t3351707245;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.String
struct String_t;
// ETCBase
struct ETCBase_t40313246;
// HedgehogTeam.EasyTouch.Finger
struct Finger_t386077194;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// HedgehogTeam.EasyTouch.EasyTouch/TouchCancelHandler
struct TouchCancelHandler_t1588360815;
// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler
struct Cancel2FingersHandler_t1819755313;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler
struct TouchStartHandler_t1924490190;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler
struct TouchDownHandler_t1840035764;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler
struct TouchUpHandler_t2315724297;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler
struct SimpleTapHandler_t2789537387;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapHandler
struct DoubleTapHandler_t2307588667;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStartHandler
struct LongTapStartHandler_t3842992360;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapHandler
struct LongTapHandler_t1819341489;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEndHandler
struct LongTapEndHandler_t661558872;
// HedgehogTeam.EasyTouch.EasyTouch/DragStartHandler
struct DragStartHandler_t2454096439;
// HedgehogTeam.EasyTouch.EasyTouch/DragHandler
struct DragHandler_t4081535653;
// HedgehogTeam.EasyTouch.EasyTouch/DragEndHandler
struct DragEndHandler_t378428410;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler
struct SwipeStartHandler_t372347372;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler
struct SwipeHandler_t1550509410;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler
struct SwipeEndHandler_t2662005585;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler
struct TouchStart2FingersHandler_t2004139386;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler
struct TouchDown2FingersHandler_t886882400;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler
struct TouchUp2FingersHandler_t2394109167;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler
struct SimpleTap2FingersHandler_t1325175573;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler
struct DoubleTap2FingersHandler_t904814161;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler
struct LongTapStart2FingersHandler_t938028626;
// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler
struct LongTap2FingersHandler_t3793547641;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler
struct LongTapEnd2FingersHandler_t2721930548;
// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler
struct TwistHandler_t3457605880;
// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler
struct TwistEndHandler_t429324375;
// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler
struct PinchHandler_t2742420652;
// HedgehogTeam.EasyTouch.EasyTouch/PinchInHandler
struct PinchInHandler_t1143385375;
// HedgehogTeam.EasyTouch.EasyTouch/PinchOutHandler
struct PinchOutHandler_t205849317;
// HedgehogTeam.EasyTouch.EasyTouch/PinchEndHandler
struct PinchEndHandler_t901660763;
// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler
struct DragStart2FingersHandler_t2484576551;
// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler
struct Drag2FingersHandler_t3289523355;
// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler
struct DragEnd2FingersHandler_t4228727231;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler
struct SwipeStart2FingersHandler_t2227626112;
// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler
struct Swipe2FingersHandler_t3165766933;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler
struct SwipeEnd2FingersHandler_t692742005;
// HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler
struct EasyTouchIsReadyHandler_t733707701;
// HedgehogTeam.EasyTouch.EasyTouch/OverUIElementHandler
struct OverUIElementHandler_t2097719920;
// HedgehogTeam.EasyTouch.EasyTouch/UIElementTouchUpHandler
struct UIElementTouchUpHandler_t73634063;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.Gesture>
struct List_1_t528814691;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.ECamera>
struct List_1_t1189928584;
// System.Collections.Generic.List`1<UnityEngine.Camera>
struct List_1_t1334261317;
// HedgehogTeam.EasyTouch.EasyTouchInput
struct EasyTouchInput_t1703601697;
// HedgehogTeam.EasyTouch.Finger[]
struct FingerU5BU5D_t1456390799;
// UnityEngine.Texture
struct Texture_t3661962703;
// HedgehogTeam.EasyTouch.TwoFingerGesture
struct TwoFingerGesture_t1936485437;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap[]
struct DoubleTapU5BU5D_t476294904;
// HedgehogTeam.EasyTouch.EasyTouch/PickedObject
struct PickedObject_t3768690803;
// System.Predicate`1<HedgehogTeam.EasyTouch.ECamera>
struct Predicate_1_t543147966;
// ETCDPad/OnMoveStartHandler
struct OnMoveStartHandler_t4101678161;
// ETCDPad/OnMoveHandler
struct OnMoveHandler_t1359337584;
// ETCDPad/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t2369786618;
// ETCDPad/OnMoveEndHandler
struct OnMoveEndHandler_t1386185292;
// ETCDPad/OnTouchStartHandler
struct OnTouchStartHandler_t1924473215;
// ETCDPad/OnTouchUPHandler
struct OnTouchUPHandler_t3432258596;
// ETCDPad/OnDownUpHandler
struct OnDownUpHandler_t3788923810;
// ETCDPad/OnDownDownHandler
struct OnDownDownHandler_t266120898;
// ETCDPad/OnDownLeftHandler
struct OnDownLeftHandler_t3608179835;
// ETCDPad/OnDownRightHandler
struct OnDownRightHandler_t491698460;
// ETCAxis
struct ETCAxis_t4105840343;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;
// HedgehogTeam.EasyTouch.QuickTap/OnTap
struct OnTap_t2551170262;
// HedgehogTeam.EasyTouch.QuickTwist/OnTwistAction
struct OnTwistAction_t1764407815;
// HedgehogTeam.EasyTouch.QuickTouch/OnTouch
struct OnTouch_t1220393512;
// HedgehogTeam.EasyTouch.QuickTouch/OnTouchNotOverMe
struct OnTouchNotOverMe_t101807583;
// ETCButton/OnDownHandler
struct OnDownHandler_t2248657866;
// ETCButton/OnPressedHandler
struct OnPressedHandler_t2010688751;
// ETCButton/OnPressedValueandler
struct OnPressedValueandler_t2312297359;
// ETCButton/OnUPHandler
struct OnUPHandler_t1013684368;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSINGLEORDOUBLEU3EC__ITERATOR0_T1496507498_H
#define U3CSINGLEORDOUBLEU3EC__ITERATOR0_T1496507498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0
struct  U3CSingleOrDoubleU3Ec__Iterator0_t1496507498  : public RuntimeObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::fingerIndex
	int32_t ___fingerIndex_0;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::<time2Wait>__0
	float ___U3Ctime2WaitU3E__0_1;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$this
	EasyTouch_t1229686981 * ___U24this_2;
	// System.Object HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_fingerIndex_0() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t1496507498, ___fingerIndex_0)); }
	inline int32_t get_fingerIndex_0() const { return ___fingerIndex_0; }
	inline int32_t* get_address_of_fingerIndex_0() { return &___fingerIndex_0; }
	inline void set_fingerIndex_0(int32_t value)
	{
		___fingerIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctime2WaitU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t1496507498, ___U3Ctime2WaitU3E__0_1)); }
	inline float get_U3Ctime2WaitU3E__0_1() const { return ___U3Ctime2WaitU3E__0_1; }
	inline float* get_address_of_U3Ctime2WaitU3E__0_1() { return &___U3Ctime2WaitU3E__0_1; }
	inline void set_U3Ctime2WaitU3E__0_1(float value)
	{
		___U3Ctime2WaitU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t1496507498, ___U24this_2)); }
	inline EasyTouch_t1229686981 * get_U24this_2() const { return ___U24this_2; }
	inline EasyTouch_t1229686981 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(EasyTouch_t1229686981 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t1496507498, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t1496507498, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t1496507498, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSINGLEORDOUBLEU3EC__ITERATOR0_T1496507498_H
#ifndef PICKEDOBJECT_T3768690803_H
#define PICKEDOBJECT_T3768690803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/PickedObject
struct  PickedObject_t3768690803  : public RuntimeObject
{
public:
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch/PickedObject::pickedObj
	GameObject_t1113636619 * ___pickedObj_0;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch/PickedObject::pickedCamera
	Camera_t4157153871 * ___pickedCamera_1;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/PickedObject::isGUI
	bool ___isGUI_2;

public:
	inline static int32_t get_offset_of_pickedObj_0() { return static_cast<int32_t>(offsetof(PickedObject_t3768690803, ___pickedObj_0)); }
	inline GameObject_t1113636619 * get_pickedObj_0() const { return ___pickedObj_0; }
	inline GameObject_t1113636619 ** get_address_of_pickedObj_0() { return &___pickedObj_0; }
	inline void set_pickedObj_0(GameObject_t1113636619 * value)
	{
		___pickedObj_0 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObj_0), value);
	}

	inline static int32_t get_offset_of_pickedCamera_1() { return static_cast<int32_t>(offsetof(PickedObject_t3768690803, ___pickedCamera_1)); }
	inline Camera_t4157153871 * get_pickedCamera_1() const { return ___pickedCamera_1; }
	inline Camera_t4157153871 ** get_address_of_pickedCamera_1() { return &___pickedCamera_1; }
	inline void set_pickedCamera_1(Camera_t4157153871 * value)
	{
		___pickedCamera_1 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_1), value);
	}

	inline static int32_t get_offset_of_isGUI_2() { return static_cast<int32_t>(offsetof(PickedObject_t3768690803, ___isGUI_2)); }
	inline bool get_isGUI_2() const { return ___isGUI_2; }
	inline bool* get_address_of_isGUI_2() { return &___isGUI_2; }
	inline void set_isGUI_2(bool value)
	{
		___isGUI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKEDOBJECT_T3768690803_H
#ifndef U3CSINGLEORDOUBLE2FINGERSU3EC__ITERATOR1_T508603925_H
#define U3CSINGLEORDOUBLE2FINGERSU3EC__ITERATOR1_T508603925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble2Fingers>c__Iterator1
struct  U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble2Fingers>c__Iterator1::$this
	EasyTouch_t1229686981 * ___U24this_0;
	// System.Object HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble2Fingers>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble2Fingers>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble2Fingers>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925, ___U24this_0)); }
	inline EasyTouch_t1229686981 * get_U24this_0() const { return ___U24this_0; }
	inline EasyTouch_t1229686981 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(EasyTouch_t1229686981 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSINGLEORDOUBLE2FINGERSU3EC__ITERATOR1_T508603925_H
#ifndef U3CRAISEEVENTU3EC__ANONSTOREY2_T963873430_H
#define U3CRAISEEVENTU3EC__ANONSTOREY2_T963873430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<RaiseEvent>c__AnonStorey2
struct  U3CRaiseEventU3Ec__AnonStorey2_t963873430  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch/<RaiseEvent>c__AnonStorey2::gesture
	Gesture_t3351707245 * ___gesture_0;

public:
	inline static int32_t get_offset_of_gesture_0() { return static_cast<int32_t>(offsetof(U3CRaiseEventU3Ec__AnonStorey2_t963873430, ___gesture_0)); }
	inline Gesture_t3351707245 * get_gesture_0() const { return ___gesture_0; }
	inline Gesture_t3351707245 ** get_address_of_gesture_0() { return &___gesture_0; }
	inline void set_gesture_0(Gesture_t3351707245 * value)
	{
		___gesture_0 = value;
		Il2CppCodeGenWriteBarrier((&___gesture_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRAISEEVENTU3EC__ANONSTOREY2_T963873430_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CUPDATEVIRTUALCONTROLU3EC__ITERATOR0_T845689544_H
#define U3CUPDATEVIRTUALCONTROLU3EC__ITERATOR0_T845689544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/<UpdateVirtualControl>c__Iterator0
struct  U3CUpdateVirtualControlU3Ec__Iterator0_t845689544  : public RuntimeObject
{
public:
	// ETCBase ETCBase/<UpdateVirtualControl>c__Iterator0::$this
	ETCBase_t40313246 * ___U24this_0;
	// System.Object ETCBase/<UpdateVirtualControl>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ETCBase/<UpdateVirtualControl>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ETCBase/<UpdateVirtualControl>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ec__Iterator0_t845689544, ___U24this_0)); }
	inline ETCBase_t40313246 * get_U24this_0() const { return ___U24this_0; }
	inline ETCBase_t40313246 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ETCBase_t40313246 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ec__Iterator0_t845689544, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ec__Iterator0_t845689544, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUpdateVirtualControlU3Ec__Iterator0_t845689544, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEVIRTUALCONTROLU3EC__ITERATOR0_T845689544_H
#ifndef U3CFIXEDUPDATEVIRTUALCONTROLU3EC__ITERATOR1_T2367558524_H
#define U3CFIXEDUPDATEVIRTUALCONTROLU3EC__ITERATOR1_T2367558524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/<FixedUpdateVirtualControl>c__Iterator1
struct  U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524  : public RuntimeObject
{
public:
	// ETCBase ETCBase/<FixedUpdateVirtualControl>c__Iterator1::$this
	ETCBase_t40313246 * ___U24this_0;
	// System.Object ETCBase/<FixedUpdateVirtualControl>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ETCBase/<FixedUpdateVirtualControl>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 ETCBase/<FixedUpdateVirtualControl>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524, ___U24this_0)); }
	inline ETCBase_t40313246 * get_U24this_0() const { return ___U24this_0; }
	inline ETCBase_t40313246 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ETCBase_t40313246 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFIXEDUPDATEVIRTUALCONTROLU3EC__ITERATOR1_T2367558524_H
#ifndef COMPONENTEXTENSIONS_T113312927_H
#define COMPONENTEXTENSIONS_T113312927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ComponentExtensions
struct  ComponentExtensions_t113312927  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEXTENSIONS_T113312927_H
#ifndef ECAMERA_T4012821138_H
#define ECAMERA_T4012821138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.ECamera
struct  ECamera_t4012821138  : public RuntimeObject
{
public:
	// UnityEngine.Camera HedgehogTeam.EasyTouch.ECamera::camera
	Camera_t4157153871 * ___camera_0;
	// System.Boolean HedgehogTeam.EasyTouch.ECamera::guiCamera
	bool ___guiCamera_1;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(ECamera_t4012821138, ___camera_0)); }
	inline Camera_t4157153871 * get_camera_0() const { return ___camera_0; }
	inline Camera_t4157153871 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t4157153871 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___camera_0), value);
	}

	inline static int32_t get_offset_of_guiCamera_1() { return static_cast<int32_t>(offsetof(ECamera_t4012821138, ___guiCamera_1)); }
	inline bool get_guiCamera_1() const { return ___guiCamera_1; }
	inline bool* get_address_of_guiCamera_1() { return &___guiCamera_1; }
	inline void set_guiCamera_1(bool value)
	{
		___guiCamera_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECAMERA_T4012821138_H
#ifndef U3CREMOVECAMERAU3EC__ANONSTOREY3_T2991675898_H
#define U3CREMOVECAMERAU3EC__ANONSTOREY3_T2991675898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<RemoveCamera>c__AnonStorey3
struct  U3CRemoveCameraU3Ec__AnonStorey3_t2991675898  : public RuntimeObject
{
public:
	// UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch/<RemoveCamera>c__AnonStorey3::cam
	Camera_t4157153871 * ___cam_0;

public:
	inline static int32_t get_offset_of_cam_0() { return static_cast<int32_t>(offsetof(U3CRemoveCameraU3Ec__AnonStorey3_t2991675898, ___cam_0)); }
	inline Camera_t4157153871 * get_cam_0() const { return ___cam_0; }
	inline Camera_t4157153871 ** get_address_of_cam_0() { return &___cam_0; }
	inline void set_cam_0(Camera_t4157153871 * value)
	{
		___cam_0 = value;
		Il2CppCodeGenWriteBarrier((&___cam_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVECAMERAU3EC__ANONSTOREY3_T2991675898_H
#ifndef DOUBLETAP_T205646101_H
#define DOUBLETAP_T205646101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap
struct  DoubleTap_t205646101  : public RuntimeObject
{
public:
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::inDoubleTap
	bool ___inDoubleTap_0;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::inWait
	bool ___inWait_1;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::time
	float ___time_2;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::count
	int32_t ___count_3;
	// HedgehogTeam.EasyTouch.Finger HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::finger
	Finger_t386077194 * ___finger_4;

public:
	inline static int32_t get_offset_of_inDoubleTap_0() { return static_cast<int32_t>(offsetof(DoubleTap_t205646101, ___inDoubleTap_0)); }
	inline bool get_inDoubleTap_0() const { return ___inDoubleTap_0; }
	inline bool* get_address_of_inDoubleTap_0() { return &___inDoubleTap_0; }
	inline void set_inDoubleTap_0(bool value)
	{
		___inDoubleTap_0 = value;
	}

	inline static int32_t get_offset_of_inWait_1() { return static_cast<int32_t>(offsetof(DoubleTap_t205646101, ___inWait_1)); }
	inline bool get_inWait_1() const { return ___inWait_1; }
	inline bool* get_address_of_inWait_1() { return &___inWait_1; }
	inline void set_inWait_1(bool value)
	{
		___inWait_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(DoubleTap_t205646101, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(DoubleTap_t205646101, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_finger_4() { return static_cast<int32_t>(offsetof(DoubleTap_t205646101, ___finger_4)); }
	inline Finger_t386077194 * get_finger_4() const { return ___finger_4; }
	inline Finger_t386077194 ** get_address_of_finger_4() { return &___finger_4; }
	inline void set_finger_4(Finger_t386077194 * value)
	{
		___finger_4 = value;
		Il2CppCodeGenWriteBarrier((&___finger_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAP_T205646101_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef UNITYEVENT_1_T4233366749_H
#define UNITYEVENT_1_T4233366749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<HedgehogTeam.EasyTouch.Gesture>
struct  UnityEvent_1_t4233366749  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4233366749, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4233366749_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_T3037889027_H
#define UNITYEVENT_1_T3037889027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t3037889027  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3037889027, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3037889027_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef AXISINFLUENCED_T3202684285_H
#define AXISINFLUENCED_T3202684285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis/AxisInfluenced
struct  AxisInfluenced_t3202684285 
{
public:
	// System.Int32 ETCAxis/AxisInfluenced::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisInfluenced_t3202684285, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISINFLUENCED_T3202684285_H
#ifndef DOUBLETAPDETECTION_T119625748_H
#define DOUBLETAPDETECTION_T119625748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapDetection
struct  DoubleTapDetection_t119625748 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/DoubleTapDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DoubleTapDetection_t119625748, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAPDETECTION_T119625748_H
#ifndef ACTIONON_T2347852592_H
#define ACTIONON_T2347852592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis/ActionOn
struct  ActionOn_t2347852592 
{
public:
	// System.Int32 ETCAxis/ActionOn::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionOn_t2347852592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONON_T2347852592_H
#ifndef AXISVALUEMETHOD_T3199242610_H
#define AXISVALUEMETHOD_T3199242610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis/AxisValueMethod
struct  AxisValueMethod_t3199242610 
{
public:
	// System.Int32 ETCAxis/AxisValueMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisValueMethod_t3199242610, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISVALUEMETHOD_T3199242610_H
#ifndef AXISSTATE_T4223862310_H
#define AXISSTATE_T4223862310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis/AxisState
struct  AxisState_t4223862310 
{
public:
	// System.Int32 ETCAxis/AxisState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisState_t4223862310, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSTATE_T4223862310_H
#ifndef DIRECTACTION_T2214449476_H
#define DIRECTACTION_T2214449476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis/DirectAction
struct  DirectAction_t2214449476 
{
public:
	// System.Int32 ETCAxis/DirectAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DirectAction_t2214449476, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTACTION_T2214449476_H
#ifndef ONSWIPEACTION_T1754534479_H
#define ONSWIPEACTION_T1754534479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe/OnSwipeAction
struct  OnSwipeAction_t1754534479  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSWIPEACTION_T1754534479_H
#ifndef EVTTYPE_T1031077730_H
#define EVTTYPE_T1031077730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/EvtType
struct  EvtType_t1031077730 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/EvtType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EvtType_t1031077730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVTTYPE_T1031077730_H
#ifndef TWOFINGERPICKMETHOD_T733771439_H
#define TWOFINGERPICKMETHOD_T733771439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TwoFingerPickMethod
struct  TwoFingerPickMethod_t733771439 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/TwoFingerPickMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TwoFingerPickMethod_t733771439, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOFINGERPICKMETHOD_T733771439_H
#ifndef EASYTOUCHINPUT_T1703601697_H
#define EASYTOUCHINPUT_T1703601697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchInput
struct  EasyTouchInput_t1703601697  : public RuntimeObject
{
public:
	// UnityEngine.Vector2[] HedgehogTeam.EasyTouch.EasyTouchInput::oldMousePosition
	Vector2U5BU5D_t1457185986* ___oldMousePosition_0;
	// System.Int32[] HedgehogTeam.EasyTouch.EasyTouchInput::tapCount
	Int32U5BU5D_t385246372* ___tapCount_1;
	// System.Single[] HedgehogTeam.EasyTouch.EasyTouchInput::startActionTime
	SingleU5BU5D_t1444911251* ___startActionTime_2;
	// System.Single[] HedgehogTeam.EasyTouch.EasyTouchInput::fixedDeltaTime
	SingleU5BU5D_t1444911251* ___fixedDeltaTime_3;
	// System.Single[] HedgehogTeam.EasyTouch.EasyTouchInput::tapeTime
	SingleU5BU5D_t1444911251* ___tapeTime_4;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchInput::bComplex
	bool ___bComplex_5;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::deltaFingerPosition
	Vector2_t2156229523  ___deltaFingerPosition_6;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::oldFinger2Position
	Vector2_t2156229523  ___oldFinger2Position_7;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.EasyTouchInput::complexCenter
	Vector2_t2156229523  ___complexCenter_8;

public:
	inline static int32_t get_offset_of_oldMousePosition_0() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___oldMousePosition_0)); }
	inline Vector2U5BU5D_t1457185986* get_oldMousePosition_0() const { return ___oldMousePosition_0; }
	inline Vector2U5BU5D_t1457185986** get_address_of_oldMousePosition_0() { return &___oldMousePosition_0; }
	inline void set_oldMousePosition_0(Vector2U5BU5D_t1457185986* value)
	{
		___oldMousePosition_0 = value;
		Il2CppCodeGenWriteBarrier((&___oldMousePosition_0), value);
	}

	inline static int32_t get_offset_of_tapCount_1() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___tapCount_1)); }
	inline Int32U5BU5D_t385246372* get_tapCount_1() const { return ___tapCount_1; }
	inline Int32U5BU5D_t385246372** get_address_of_tapCount_1() { return &___tapCount_1; }
	inline void set_tapCount_1(Int32U5BU5D_t385246372* value)
	{
		___tapCount_1 = value;
		Il2CppCodeGenWriteBarrier((&___tapCount_1), value);
	}

	inline static int32_t get_offset_of_startActionTime_2() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___startActionTime_2)); }
	inline SingleU5BU5D_t1444911251* get_startActionTime_2() const { return ___startActionTime_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_startActionTime_2() { return &___startActionTime_2; }
	inline void set_startActionTime_2(SingleU5BU5D_t1444911251* value)
	{
		___startActionTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___startActionTime_2), value);
	}

	inline static int32_t get_offset_of_fixedDeltaTime_3() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___fixedDeltaTime_3)); }
	inline SingleU5BU5D_t1444911251* get_fixedDeltaTime_3() const { return ___fixedDeltaTime_3; }
	inline SingleU5BU5D_t1444911251** get_address_of_fixedDeltaTime_3() { return &___fixedDeltaTime_3; }
	inline void set_fixedDeltaTime_3(SingleU5BU5D_t1444911251* value)
	{
		___fixedDeltaTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___fixedDeltaTime_3), value);
	}

	inline static int32_t get_offset_of_tapeTime_4() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___tapeTime_4)); }
	inline SingleU5BU5D_t1444911251* get_tapeTime_4() const { return ___tapeTime_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_tapeTime_4() { return &___tapeTime_4; }
	inline void set_tapeTime_4(SingleU5BU5D_t1444911251* value)
	{
		___tapeTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___tapeTime_4), value);
	}

	inline static int32_t get_offset_of_bComplex_5() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___bComplex_5)); }
	inline bool get_bComplex_5() const { return ___bComplex_5; }
	inline bool* get_address_of_bComplex_5() { return &___bComplex_5; }
	inline void set_bComplex_5(bool value)
	{
		___bComplex_5 = value;
	}

	inline static int32_t get_offset_of_deltaFingerPosition_6() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___deltaFingerPosition_6)); }
	inline Vector2_t2156229523  get_deltaFingerPosition_6() const { return ___deltaFingerPosition_6; }
	inline Vector2_t2156229523 * get_address_of_deltaFingerPosition_6() { return &___deltaFingerPosition_6; }
	inline void set_deltaFingerPosition_6(Vector2_t2156229523  value)
	{
		___deltaFingerPosition_6 = value;
	}

	inline static int32_t get_offset_of_oldFinger2Position_7() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___oldFinger2Position_7)); }
	inline Vector2_t2156229523  get_oldFinger2Position_7() const { return ___oldFinger2Position_7; }
	inline Vector2_t2156229523 * get_address_of_oldFinger2Position_7() { return &___oldFinger2Position_7; }
	inline void set_oldFinger2Position_7(Vector2_t2156229523  value)
	{
		___oldFinger2Position_7 = value;
	}

	inline static int32_t get_offset_of_complexCenter_8() { return static_cast<int32_t>(offsetof(EasyTouchInput_t1703601697, ___complexCenter_8)); }
	inline Vector2_t2156229523  get_complexCenter_8() const { return ___complexCenter_8; }
	inline Vector2_t2156229523 * get_address_of_complexCenter_8() { return &___complexCenter_8; }
	inline void set_complexCenter_8(Vector2_t2156229523  value)
	{
		___complexCenter_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHINPUT_T1703601697_H
#ifndef SWIPEDIRECTION_T3548341340_H
#define SWIPEDIRECTION_T3548341340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SwipeDirection
struct  SwipeDirection_t3548341340 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/SwipeDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SwipeDirection_t3548341340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_T3548341340_H
#ifndef GESTURETYPE_T4183725547_H
#define GESTURETYPE_T4183725547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/GestureType
struct  GestureType_t4183725547 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/GestureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureType_t4183725547, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURETYPE_T4183725547_H
#ifndef AREAPRESET_T2760894157_H
#define AREAPRESET_T2760894157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCArea/AreaPreset
struct  AreaPreset_t2760894157 
{
public:
	// System.Int32 ETCArea/AreaPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AreaPreset_t2760894157, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AREAPRESET_T2760894157_H
#ifndef CONTROLTYPE_T2136651667_H
#define CONTROLTYPE_T2136651667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/ControlType
struct  ControlType_t2136651667 
{
public:
	// System.Int32 ETCBase/ControlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlType_t2136651667, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTYPE_T2136651667_H
#ifndef ONMOVESPEEDHANDLER_T2369786618_H
#define ONMOVESPEEDHANDLER_T2369786618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnMoveSpeedHandler
struct  OnMoveSpeedHandler_t2369786618  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESPEEDHANDLER_T2369786618_H
#ifndef ONMOVEENDHANDLER_T1386185292_H
#define ONMOVEENDHANDLER_T1386185292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnMoveEndHandler
struct  OnMoveEndHandler_t1386185292  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEENDHANDLER_T1386185292_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef DIRECTACTION_T2452253191_H
#define DIRECTACTION_T2452253191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase/DirectAction
struct  DirectAction_t2452253191 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase/DirectAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DirectAction_t2452253191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTACTION_T2452253191_H
#ifndef AFFECTEDAXESACTION_T2927047446_H
#define AFFECTEDAXESACTION_T2927047446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction
struct  AffectedAxesAction_t2927047446 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AffectedAxesAction_t2927047446, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFFECTEDAXESACTION_T2927047446_H
#ifndef GAMEOBJECTTYPE_T2199397569_H
#define GAMEOBJECTTYPE_T2199397569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase/GameObjectType
struct  GameObjectType_t2199397569 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase/GameObjectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameObjectType_t2199397569, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTTYPE_T2199397569_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ONMOVEHANDLER_T1359337584_H
#define ONMOVEHANDLER_T1359337584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnMoveHandler
struct  OnMoveHandler_t1359337584  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVEHANDLER_T1359337584_H
#ifndef RECTANCHOR_T2504559136_H
#define RECTANCHOR_T2504559136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/RectAnchor
struct  RectAnchor_t2504559136 
{
public:
	// System.Int32 ETCBase/RectAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RectAnchor_t2504559136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANCHOR_T2504559136_H
#ifndef DPADAXIS_T1048722470_H
#define DPADAXIS_T1048722470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/DPadAxis
struct  DPadAxis_t1048722470 
{
public:
	// System.Int32 ETCBase/DPadAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DPadAxis_t1048722470, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPADAXIS_T1048722470_H
#ifndef CAMERAMODE_T364125053_H
#define CAMERAMODE_T364125053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/CameraMode
struct  CameraMode_t364125053 
{
public:
	// System.Int32 ETCBase/CameraMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraMode_t364125053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODE_T364125053_H
#ifndef CAMERATARGETMODE_T4109937804_H
#define CAMERATARGETMODE_T4109937804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase/CameraTargetMode
struct  CameraTargetMode_t4109937804 
{
public:
	// System.Int32 ETCBase/CameraTargetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraTargetMode_t4109937804, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATARGETMODE_T4109937804_H
#ifndef ONDOWNHANDLER_T2248657866_H
#define ONDOWNHANDLER_T2248657866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton/OnDownHandler
struct  OnDownHandler_t2248657866  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNHANDLER_T2248657866_H
#ifndef ONPRESSEDHANDLER_T2010688751_H
#define ONPRESSEDHANDLER_T2010688751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton/OnPressedHandler
struct  OnPressedHandler_t2010688751  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSEDHANDLER_T2010688751_H
#ifndef ONPRESSEDVALUEANDLER_T2312297359_H
#define ONPRESSEDVALUEANDLER_T2312297359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton/OnPressedValueandler
struct  OnPressedValueandler_t2312297359  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPRESSEDVALUEANDLER_T2312297359_H
#ifndef ONUPHANDLER_T1013684368_H
#define ONUPHANDLER_T1013684368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton/OnUPHandler
struct  OnUPHandler_t1013684368  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPHANDLER_T1013684368_H
#ifndef ONMOVESTARTHANDLER_T4101678161_H
#define ONMOVESTARTHANDLER_T4101678161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad/OnMoveStartHandler
struct  OnMoveStartHandler_t4101678161  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMOVESTARTHANDLER_T4101678161_H
#ifndef GESTUREPRIORITY_T2833837310_H
#define GESTUREPRIORITY_T2833837310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/GesturePriority
struct  GesturePriority_t2833837310 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/GesturePriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GesturePriority_t2833837310, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREPRIORITY_T2833837310_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ACTIONROTATIONDIRECTION_T3282353075_H
#define ACTIONROTATIONDIRECTION_T3282353075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist/ActionRotationDirection
struct  ActionRotationDirection_t3282353075 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTwist/ActionRotationDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionRotationDirection_t3282353075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONROTATIONDIRECTION_T3282353075_H
#ifndef ACTIONTIGGERING_T3746618738_H
#define ACTIONTIGGERING_T3746618738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist/ActionTiggering
struct  ActionTiggering_t3746618738 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTwist/ActionTiggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTiggering_t3746618738, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTIGGERING_T3746618738_H
#ifndef ONTWISTACTION_T1764407815_H
#define ONTWISTACTION_T1764407815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist/OnTwistAction
struct  OnTwistAction_t1764407815  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTWISTACTION_T1764407815_H
#ifndef ACTIONTRIGGERING_T2131777801_H
#define ACTIONTRIGGERING_T2131777801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch/ActionTriggering
struct  ActionTriggering_t2131777801 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTouch/ActionTriggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTriggering_t2131777801, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T2131777801_H
#ifndef ONTOUCHNOTOVERME_T101807583_H
#define ONTOUCHNOTOVERME_T101807583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch/OnTouchNotOverMe
struct  OnTouchNotOverMe_t101807583  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHNOTOVERME_T101807583_H
#ifndef ONTOUCH_T1220393512_H
#define ONTOUCH_T1220393512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch/OnTouch
struct  OnTouch_t1220393512  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCH_T1220393512_H
#ifndef ACTIONTRIGGERING_T1647944257_H
#define ACTIONTRIGGERING_T1647944257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap/ActionTriggering
struct  ActionTriggering_t1647944257 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickTap/ActionTriggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTriggering_t1647944257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T1647944257_H
#ifndef ONTAP_T2551170262_H
#define ONTAP_T2551170262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap/OnTap
struct  OnTap_t2551170262  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTAP_T2551170262_H
#ifndef SWIPEDIRECTION_T2936141849_H
#define SWIPEDIRECTION_T2936141849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe/SwipeDirection
struct  SwipeDirection_t2936141849 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickSwipe/SwipeDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SwipeDirection_t2936141849, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_T2936141849_H
#ifndef ACTIONTRIGGERING_T481153517_H
#define ACTIONTRIGGERING_T481153517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe/ActionTriggering
struct  ActionTriggering_t481153517 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickSwipe/ActionTriggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTriggering_t481153517, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T481153517_H
#ifndef ETCAXIS_T4105840343_H
#define ETCAXIS_T4105840343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis
struct  ETCAxis_t4105840343  : public RuntimeObject
{
public:
	// System.String ETCAxis::name
	String_t* ___name_0;
	// System.Boolean ETCAxis::autoLinkTagPlayer
	bool ___autoLinkTagPlayer_1;
	// System.String ETCAxis::autoTag
	String_t* ___autoTag_2;
	// UnityEngine.GameObject ETCAxis::player
	GameObject_t1113636619 * ___player_3;
	// System.Boolean ETCAxis::enable
	bool ___enable_4;
	// System.Boolean ETCAxis::invertedAxis
	bool ___invertedAxis_5;
	// System.Single ETCAxis::speed
	float ___speed_6;
	// System.Single ETCAxis::deadValue
	float ___deadValue_7;
	// ETCAxis/AxisValueMethod ETCAxis::valueMethod
	int32_t ___valueMethod_8;
	// UnityEngine.AnimationCurve ETCAxis::curveValue
	AnimationCurve_t3046754366 * ___curveValue_9;
	// System.Boolean ETCAxis::isEnertia
	bool ___isEnertia_10;
	// System.Single ETCAxis::inertia
	float ___inertia_11;
	// System.Single ETCAxis::inertiaThreshold
	float ___inertiaThreshold_12;
	// System.Boolean ETCAxis::isAutoStab
	bool ___isAutoStab_13;
	// System.Single ETCAxis::autoStabThreshold
	float ___autoStabThreshold_14;
	// System.Single ETCAxis::autoStabSpeed
	float ___autoStabSpeed_15;
	// System.Single ETCAxis::startAngle
	float ___startAngle_16;
	// System.Boolean ETCAxis::isClampRotation
	bool ___isClampRotation_17;
	// System.Single ETCAxis::maxAngle
	float ___maxAngle_18;
	// System.Single ETCAxis::minAngle
	float ___minAngle_19;
	// System.Boolean ETCAxis::isValueOverTime
	bool ___isValueOverTime_20;
	// System.Single ETCAxis::overTimeStep
	float ___overTimeStep_21;
	// System.Single ETCAxis::maxOverTimeValue
	float ___maxOverTimeValue_22;
	// System.Single ETCAxis::axisValue
	float ___axisValue_23;
	// System.Single ETCAxis::axisSpeedValue
	float ___axisSpeedValue_24;
	// System.Single ETCAxis::axisThreshold
	float ___axisThreshold_25;
	// System.Boolean ETCAxis::isLockinJump
	bool ___isLockinJump_26;
	// UnityEngine.Vector3 ETCAxis::lastMove
	Vector3_t3722313464  ___lastMove_27;
	// ETCAxis/AxisState ETCAxis::axisState
	int32_t ___axisState_28;
	// UnityEngine.Transform ETCAxis::_directTransform
	Transform_t3600365921 * ____directTransform_29;
	// ETCAxis/DirectAction ETCAxis::directAction
	int32_t ___directAction_30;
	// ETCAxis/AxisInfluenced ETCAxis::axisInfluenced
	int32_t ___axisInfluenced_31;
	// ETCAxis/ActionOn ETCAxis::actionOn
	int32_t ___actionOn_32;
	// UnityEngine.CharacterController ETCAxis::directCharacterController
	CharacterController_t1138636865 * ___directCharacterController_33;
	// UnityEngine.Rigidbody ETCAxis::directRigidBody
	Rigidbody_t3916780224 * ___directRigidBody_34;
	// System.Single ETCAxis::gravity
	float ___gravity_35;
	// System.Single ETCAxis::currentGravity
	float ___currentGravity_36;
	// System.Boolean ETCAxis::isJump
	bool ___isJump_37;
	// System.String ETCAxis::unityAxis
	String_t* ___unityAxis_38;
	// System.Boolean ETCAxis::showGeneralInspector
	bool ___showGeneralInspector_39;
	// System.Boolean ETCAxis::showDirectInspector
	bool ___showDirectInspector_40;
	// System.Boolean ETCAxis::showInertiaInspector
	bool ___showInertiaInspector_41;
	// System.Boolean ETCAxis::showSimulatinInspector
	bool ___showSimulatinInspector_42;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_autoLinkTagPlayer_1() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___autoLinkTagPlayer_1)); }
	inline bool get_autoLinkTagPlayer_1() const { return ___autoLinkTagPlayer_1; }
	inline bool* get_address_of_autoLinkTagPlayer_1() { return &___autoLinkTagPlayer_1; }
	inline void set_autoLinkTagPlayer_1(bool value)
	{
		___autoLinkTagPlayer_1 = value;
	}

	inline static int32_t get_offset_of_autoTag_2() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___autoTag_2)); }
	inline String_t* get_autoTag_2() const { return ___autoTag_2; }
	inline String_t** get_address_of_autoTag_2() { return &___autoTag_2; }
	inline void set_autoTag_2(String_t* value)
	{
		___autoTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___autoTag_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___player_3)); }
	inline GameObject_t1113636619 * get_player_3() const { return ___player_3; }
	inline GameObject_t1113636619 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1113636619 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}

	inline static int32_t get_offset_of_enable_4() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___enable_4)); }
	inline bool get_enable_4() const { return ___enable_4; }
	inline bool* get_address_of_enable_4() { return &___enable_4; }
	inline void set_enable_4(bool value)
	{
		___enable_4 = value;
	}

	inline static int32_t get_offset_of_invertedAxis_5() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___invertedAxis_5)); }
	inline bool get_invertedAxis_5() const { return ___invertedAxis_5; }
	inline bool* get_address_of_invertedAxis_5() { return &___invertedAxis_5; }
	inline void set_invertedAxis_5(bool value)
	{
		___invertedAxis_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_deadValue_7() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___deadValue_7)); }
	inline float get_deadValue_7() const { return ___deadValue_7; }
	inline float* get_address_of_deadValue_7() { return &___deadValue_7; }
	inline void set_deadValue_7(float value)
	{
		___deadValue_7 = value;
	}

	inline static int32_t get_offset_of_valueMethod_8() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___valueMethod_8)); }
	inline int32_t get_valueMethod_8() const { return ___valueMethod_8; }
	inline int32_t* get_address_of_valueMethod_8() { return &___valueMethod_8; }
	inline void set_valueMethod_8(int32_t value)
	{
		___valueMethod_8 = value;
	}

	inline static int32_t get_offset_of_curveValue_9() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___curveValue_9)); }
	inline AnimationCurve_t3046754366 * get_curveValue_9() const { return ___curveValue_9; }
	inline AnimationCurve_t3046754366 ** get_address_of_curveValue_9() { return &___curveValue_9; }
	inline void set_curveValue_9(AnimationCurve_t3046754366 * value)
	{
		___curveValue_9 = value;
		Il2CppCodeGenWriteBarrier((&___curveValue_9), value);
	}

	inline static int32_t get_offset_of_isEnertia_10() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___isEnertia_10)); }
	inline bool get_isEnertia_10() const { return ___isEnertia_10; }
	inline bool* get_address_of_isEnertia_10() { return &___isEnertia_10; }
	inline void set_isEnertia_10(bool value)
	{
		___isEnertia_10 = value;
	}

	inline static int32_t get_offset_of_inertia_11() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___inertia_11)); }
	inline float get_inertia_11() const { return ___inertia_11; }
	inline float* get_address_of_inertia_11() { return &___inertia_11; }
	inline void set_inertia_11(float value)
	{
		___inertia_11 = value;
	}

	inline static int32_t get_offset_of_inertiaThreshold_12() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___inertiaThreshold_12)); }
	inline float get_inertiaThreshold_12() const { return ___inertiaThreshold_12; }
	inline float* get_address_of_inertiaThreshold_12() { return &___inertiaThreshold_12; }
	inline void set_inertiaThreshold_12(float value)
	{
		___inertiaThreshold_12 = value;
	}

	inline static int32_t get_offset_of_isAutoStab_13() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___isAutoStab_13)); }
	inline bool get_isAutoStab_13() const { return ___isAutoStab_13; }
	inline bool* get_address_of_isAutoStab_13() { return &___isAutoStab_13; }
	inline void set_isAutoStab_13(bool value)
	{
		___isAutoStab_13 = value;
	}

	inline static int32_t get_offset_of_autoStabThreshold_14() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___autoStabThreshold_14)); }
	inline float get_autoStabThreshold_14() const { return ___autoStabThreshold_14; }
	inline float* get_address_of_autoStabThreshold_14() { return &___autoStabThreshold_14; }
	inline void set_autoStabThreshold_14(float value)
	{
		___autoStabThreshold_14 = value;
	}

	inline static int32_t get_offset_of_autoStabSpeed_15() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___autoStabSpeed_15)); }
	inline float get_autoStabSpeed_15() const { return ___autoStabSpeed_15; }
	inline float* get_address_of_autoStabSpeed_15() { return &___autoStabSpeed_15; }
	inline void set_autoStabSpeed_15(float value)
	{
		___autoStabSpeed_15 = value;
	}

	inline static int32_t get_offset_of_startAngle_16() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___startAngle_16)); }
	inline float get_startAngle_16() const { return ___startAngle_16; }
	inline float* get_address_of_startAngle_16() { return &___startAngle_16; }
	inline void set_startAngle_16(float value)
	{
		___startAngle_16 = value;
	}

	inline static int32_t get_offset_of_isClampRotation_17() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___isClampRotation_17)); }
	inline bool get_isClampRotation_17() const { return ___isClampRotation_17; }
	inline bool* get_address_of_isClampRotation_17() { return &___isClampRotation_17; }
	inline void set_isClampRotation_17(bool value)
	{
		___isClampRotation_17 = value;
	}

	inline static int32_t get_offset_of_maxAngle_18() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___maxAngle_18)); }
	inline float get_maxAngle_18() const { return ___maxAngle_18; }
	inline float* get_address_of_maxAngle_18() { return &___maxAngle_18; }
	inline void set_maxAngle_18(float value)
	{
		___maxAngle_18 = value;
	}

	inline static int32_t get_offset_of_minAngle_19() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___minAngle_19)); }
	inline float get_minAngle_19() const { return ___minAngle_19; }
	inline float* get_address_of_minAngle_19() { return &___minAngle_19; }
	inline void set_minAngle_19(float value)
	{
		___minAngle_19 = value;
	}

	inline static int32_t get_offset_of_isValueOverTime_20() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___isValueOverTime_20)); }
	inline bool get_isValueOverTime_20() const { return ___isValueOverTime_20; }
	inline bool* get_address_of_isValueOverTime_20() { return &___isValueOverTime_20; }
	inline void set_isValueOverTime_20(bool value)
	{
		___isValueOverTime_20 = value;
	}

	inline static int32_t get_offset_of_overTimeStep_21() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___overTimeStep_21)); }
	inline float get_overTimeStep_21() const { return ___overTimeStep_21; }
	inline float* get_address_of_overTimeStep_21() { return &___overTimeStep_21; }
	inline void set_overTimeStep_21(float value)
	{
		___overTimeStep_21 = value;
	}

	inline static int32_t get_offset_of_maxOverTimeValue_22() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___maxOverTimeValue_22)); }
	inline float get_maxOverTimeValue_22() const { return ___maxOverTimeValue_22; }
	inline float* get_address_of_maxOverTimeValue_22() { return &___maxOverTimeValue_22; }
	inline void set_maxOverTimeValue_22(float value)
	{
		___maxOverTimeValue_22 = value;
	}

	inline static int32_t get_offset_of_axisValue_23() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___axisValue_23)); }
	inline float get_axisValue_23() const { return ___axisValue_23; }
	inline float* get_address_of_axisValue_23() { return &___axisValue_23; }
	inline void set_axisValue_23(float value)
	{
		___axisValue_23 = value;
	}

	inline static int32_t get_offset_of_axisSpeedValue_24() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___axisSpeedValue_24)); }
	inline float get_axisSpeedValue_24() const { return ___axisSpeedValue_24; }
	inline float* get_address_of_axisSpeedValue_24() { return &___axisSpeedValue_24; }
	inline void set_axisSpeedValue_24(float value)
	{
		___axisSpeedValue_24 = value;
	}

	inline static int32_t get_offset_of_axisThreshold_25() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___axisThreshold_25)); }
	inline float get_axisThreshold_25() const { return ___axisThreshold_25; }
	inline float* get_address_of_axisThreshold_25() { return &___axisThreshold_25; }
	inline void set_axisThreshold_25(float value)
	{
		___axisThreshold_25 = value;
	}

	inline static int32_t get_offset_of_isLockinJump_26() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___isLockinJump_26)); }
	inline bool get_isLockinJump_26() const { return ___isLockinJump_26; }
	inline bool* get_address_of_isLockinJump_26() { return &___isLockinJump_26; }
	inline void set_isLockinJump_26(bool value)
	{
		___isLockinJump_26 = value;
	}

	inline static int32_t get_offset_of_lastMove_27() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___lastMove_27)); }
	inline Vector3_t3722313464  get_lastMove_27() const { return ___lastMove_27; }
	inline Vector3_t3722313464 * get_address_of_lastMove_27() { return &___lastMove_27; }
	inline void set_lastMove_27(Vector3_t3722313464  value)
	{
		___lastMove_27 = value;
	}

	inline static int32_t get_offset_of_axisState_28() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___axisState_28)); }
	inline int32_t get_axisState_28() const { return ___axisState_28; }
	inline int32_t* get_address_of_axisState_28() { return &___axisState_28; }
	inline void set_axisState_28(int32_t value)
	{
		___axisState_28 = value;
	}

	inline static int32_t get_offset_of__directTransform_29() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ____directTransform_29)); }
	inline Transform_t3600365921 * get__directTransform_29() const { return ____directTransform_29; }
	inline Transform_t3600365921 ** get_address_of__directTransform_29() { return &____directTransform_29; }
	inline void set__directTransform_29(Transform_t3600365921 * value)
	{
		____directTransform_29 = value;
		Il2CppCodeGenWriteBarrier((&____directTransform_29), value);
	}

	inline static int32_t get_offset_of_directAction_30() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___directAction_30)); }
	inline int32_t get_directAction_30() const { return ___directAction_30; }
	inline int32_t* get_address_of_directAction_30() { return &___directAction_30; }
	inline void set_directAction_30(int32_t value)
	{
		___directAction_30 = value;
	}

	inline static int32_t get_offset_of_axisInfluenced_31() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___axisInfluenced_31)); }
	inline int32_t get_axisInfluenced_31() const { return ___axisInfluenced_31; }
	inline int32_t* get_address_of_axisInfluenced_31() { return &___axisInfluenced_31; }
	inline void set_axisInfluenced_31(int32_t value)
	{
		___axisInfluenced_31 = value;
	}

	inline static int32_t get_offset_of_actionOn_32() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___actionOn_32)); }
	inline int32_t get_actionOn_32() const { return ___actionOn_32; }
	inline int32_t* get_address_of_actionOn_32() { return &___actionOn_32; }
	inline void set_actionOn_32(int32_t value)
	{
		___actionOn_32 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_33() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___directCharacterController_33)); }
	inline CharacterController_t1138636865 * get_directCharacterController_33() const { return ___directCharacterController_33; }
	inline CharacterController_t1138636865 ** get_address_of_directCharacterController_33() { return &___directCharacterController_33; }
	inline void set_directCharacterController_33(CharacterController_t1138636865 * value)
	{
		___directCharacterController_33 = value;
		Il2CppCodeGenWriteBarrier((&___directCharacterController_33), value);
	}

	inline static int32_t get_offset_of_directRigidBody_34() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___directRigidBody_34)); }
	inline Rigidbody_t3916780224 * get_directRigidBody_34() const { return ___directRigidBody_34; }
	inline Rigidbody_t3916780224 ** get_address_of_directRigidBody_34() { return &___directRigidBody_34; }
	inline void set_directRigidBody_34(Rigidbody_t3916780224 * value)
	{
		___directRigidBody_34 = value;
		Il2CppCodeGenWriteBarrier((&___directRigidBody_34), value);
	}

	inline static int32_t get_offset_of_gravity_35() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___gravity_35)); }
	inline float get_gravity_35() const { return ___gravity_35; }
	inline float* get_address_of_gravity_35() { return &___gravity_35; }
	inline void set_gravity_35(float value)
	{
		___gravity_35 = value;
	}

	inline static int32_t get_offset_of_currentGravity_36() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___currentGravity_36)); }
	inline float get_currentGravity_36() const { return ___currentGravity_36; }
	inline float* get_address_of_currentGravity_36() { return &___currentGravity_36; }
	inline void set_currentGravity_36(float value)
	{
		___currentGravity_36 = value;
	}

	inline static int32_t get_offset_of_isJump_37() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___isJump_37)); }
	inline bool get_isJump_37() const { return ___isJump_37; }
	inline bool* get_address_of_isJump_37() { return &___isJump_37; }
	inline void set_isJump_37(bool value)
	{
		___isJump_37 = value;
	}

	inline static int32_t get_offset_of_unityAxis_38() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___unityAxis_38)); }
	inline String_t* get_unityAxis_38() const { return ___unityAxis_38; }
	inline String_t** get_address_of_unityAxis_38() { return &___unityAxis_38; }
	inline void set_unityAxis_38(String_t* value)
	{
		___unityAxis_38 = value;
		Il2CppCodeGenWriteBarrier((&___unityAxis_38), value);
	}

	inline static int32_t get_offset_of_showGeneralInspector_39() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___showGeneralInspector_39)); }
	inline bool get_showGeneralInspector_39() const { return ___showGeneralInspector_39; }
	inline bool* get_address_of_showGeneralInspector_39() { return &___showGeneralInspector_39; }
	inline void set_showGeneralInspector_39(bool value)
	{
		___showGeneralInspector_39 = value;
	}

	inline static int32_t get_offset_of_showDirectInspector_40() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___showDirectInspector_40)); }
	inline bool get_showDirectInspector_40() const { return ___showDirectInspector_40; }
	inline bool* get_address_of_showDirectInspector_40() { return &___showDirectInspector_40; }
	inline void set_showDirectInspector_40(bool value)
	{
		___showDirectInspector_40 = value;
	}

	inline static int32_t get_offset_of_showInertiaInspector_41() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___showInertiaInspector_41)); }
	inline bool get_showInertiaInspector_41() const { return ___showInertiaInspector_41; }
	inline bool* get_address_of_showInertiaInspector_41() { return &___showInertiaInspector_41; }
	inline void set_showInertiaInspector_41(bool value)
	{
		___showInertiaInspector_41 = value;
	}

	inline static int32_t get_offset_of_showSimulatinInspector_42() { return static_cast<int32_t>(offsetof(ETCAxis_t4105840343, ___showSimulatinInspector_42)); }
	inline bool get_showSimulatinInspector_42() const { return ___showSimulatinInspector_42; }
	inline bool* get_address_of_showSimulatinInspector_42() { return &___showSimulatinInspector_42; }
	inline void set_showSimulatinInspector_42(bool value)
	{
		___showSimulatinInspector_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCAXIS_T4105840343_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BASEFINGER_T854008753_H
#define BASEFINGER_T854008753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.BaseFinger
struct  BaseFinger_t854008753  : public RuntimeObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::fingerIndex
	int32_t ___fingerIndex_0;
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::touchCount
	int32_t ___touchCount_1;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::startPosition
	Vector2_t2156229523  ___startPosition_2;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::position
	Vector2_t2156229523  ___position_3;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::deltaPosition
	Vector2_t2156229523  ___deltaPosition_4;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::actionTime
	float ___actionTime_5;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::fixedDeltaTime
	float ___fixedDeltaTime_6;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.BaseFinger::pickedCamera
	Camera_t4157153871 * ___pickedCamera_7;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedObject
	GameObject_t1113636619 * ___pickedObject_8;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isGuiCamera
	bool ___isGuiCamera_9;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isOverGui
	bool ___isOverGui_10;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedUIElement
	GameObject_t1113636619 * ___pickedUIElement_11;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::altitudeAngle
	float ___altitudeAngle_12;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::azimuthAngle
	float ___azimuthAngle_13;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::maximumPossiblePressure
	float ___maximumPossiblePressure_14;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::pressure
	float ___pressure_15;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radius
	float ___radius_16;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radiusVariance
	float ___radiusVariance_17;
	// UnityEngine.TouchType HedgehogTeam.EasyTouch.BaseFinger::touchType
	int32_t ___touchType_18;

public:
	inline static int32_t get_offset_of_fingerIndex_0() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___fingerIndex_0)); }
	inline int32_t get_fingerIndex_0() const { return ___fingerIndex_0; }
	inline int32_t* get_address_of_fingerIndex_0() { return &___fingerIndex_0; }
	inline void set_fingerIndex_0(int32_t value)
	{
		___fingerIndex_0 = value;
	}

	inline static int32_t get_offset_of_touchCount_1() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___touchCount_1)); }
	inline int32_t get_touchCount_1() const { return ___touchCount_1; }
	inline int32_t* get_address_of_touchCount_1() { return &___touchCount_1; }
	inline void set_touchCount_1(int32_t value)
	{
		___touchCount_1 = value;
	}

	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___startPosition_2)); }
	inline Vector2_t2156229523  get_startPosition_2() const { return ___startPosition_2; }
	inline Vector2_t2156229523 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Vector2_t2156229523  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___position_3)); }
	inline Vector2_t2156229523  get_position_3() const { return ___position_3; }
	inline Vector2_t2156229523 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector2_t2156229523  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_4() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___deltaPosition_4)); }
	inline Vector2_t2156229523  get_deltaPosition_4() const { return ___deltaPosition_4; }
	inline Vector2_t2156229523 * get_address_of_deltaPosition_4() { return &___deltaPosition_4; }
	inline void set_deltaPosition_4(Vector2_t2156229523  value)
	{
		___deltaPosition_4 = value;
	}

	inline static int32_t get_offset_of_actionTime_5() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___actionTime_5)); }
	inline float get_actionTime_5() const { return ___actionTime_5; }
	inline float* get_address_of_actionTime_5() { return &___actionTime_5; }
	inline void set_actionTime_5(float value)
	{
		___actionTime_5 = value;
	}

	inline static int32_t get_offset_of_fixedDeltaTime_6() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___fixedDeltaTime_6)); }
	inline float get_fixedDeltaTime_6() const { return ___fixedDeltaTime_6; }
	inline float* get_address_of_fixedDeltaTime_6() { return &___fixedDeltaTime_6; }
	inline void set_fixedDeltaTime_6(float value)
	{
		___fixedDeltaTime_6 = value;
	}

	inline static int32_t get_offset_of_pickedCamera_7() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___pickedCamera_7)); }
	inline Camera_t4157153871 * get_pickedCamera_7() const { return ___pickedCamera_7; }
	inline Camera_t4157153871 ** get_address_of_pickedCamera_7() { return &___pickedCamera_7; }
	inline void set_pickedCamera_7(Camera_t4157153871 * value)
	{
		___pickedCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_7), value);
	}

	inline static int32_t get_offset_of_pickedObject_8() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___pickedObject_8)); }
	inline GameObject_t1113636619 * get_pickedObject_8() const { return ___pickedObject_8; }
	inline GameObject_t1113636619 ** get_address_of_pickedObject_8() { return &___pickedObject_8; }
	inline void set_pickedObject_8(GameObject_t1113636619 * value)
	{
		___pickedObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_8), value);
	}

	inline static int32_t get_offset_of_isGuiCamera_9() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___isGuiCamera_9)); }
	inline bool get_isGuiCamera_9() const { return ___isGuiCamera_9; }
	inline bool* get_address_of_isGuiCamera_9() { return &___isGuiCamera_9; }
	inline void set_isGuiCamera_9(bool value)
	{
		___isGuiCamera_9 = value;
	}

	inline static int32_t get_offset_of_isOverGui_10() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___isOverGui_10)); }
	inline bool get_isOverGui_10() const { return ___isOverGui_10; }
	inline bool* get_address_of_isOverGui_10() { return &___isOverGui_10; }
	inline void set_isOverGui_10(bool value)
	{
		___isOverGui_10 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_11() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___pickedUIElement_11)); }
	inline GameObject_t1113636619 * get_pickedUIElement_11() const { return ___pickedUIElement_11; }
	inline GameObject_t1113636619 ** get_address_of_pickedUIElement_11() { return &___pickedUIElement_11; }
	inline void set_pickedUIElement_11(GameObject_t1113636619 * value)
	{
		___pickedUIElement_11 = value;
		Il2CppCodeGenWriteBarrier((&___pickedUIElement_11), value);
	}

	inline static int32_t get_offset_of_altitudeAngle_12() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___altitudeAngle_12)); }
	inline float get_altitudeAngle_12() const { return ___altitudeAngle_12; }
	inline float* get_address_of_altitudeAngle_12() { return &___altitudeAngle_12; }
	inline void set_altitudeAngle_12(float value)
	{
		___altitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_azimuthAngle_13() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___azimuthAngle_13)); }
	inline float get_azimuthAngle_13() const { return ___azimuthAngle_13; }
	inline float* get_address_of_azimuthAngle_13() { return &___azimuthAngle_13; }
	inline void set_azimuthAngle_13(float value)
	{
		___azimuthAngle_13 = value;
	}

	inline static int32_t get_offset_of_maximumPossiblePressure_14() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___maximumPossiblePressure_14)); }
	inline float get_maximumPossiblePressure_14() const { return ___maximumPossiblePressure_14; }
	inline float* get_address_of_maximumPossiblePressure_14() { return &___maximumPossiblePressure_14; }
	inline void set_maximumPossiblePressure_14(float value)
	{
		___maximumPossiblePressure_14 = value;
	}

	inline static int32_t get_offset_of_pressure_15() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___pressure_15)); }
	inline float get_pressure_15() const { return ___pressure_15; }
	inline float* get_address_of_pressure_15() { return &___pressure_15; }
	inline void set_pressure_15(float value)
	{
		___pressure_15 = value;
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___radius_16)); }
	inline float get_radius_16() const { return ___radius_16; }
	inline float* get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(float value)
	{
		___radius_16 = value;
	}

	inline static int32_t get_offset_of_radiusVariance_17() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___radiusVariance_17)); }
	inline float get_radiusVariance_17() const { return ___radiusVariance_17; }
	inline float* get_address_of_radiusVariance_17() { return &___radiusVariance_17; }
	inline void set_radiusVariance_17(float value)
	{
		___radiusVariance_17 = value;
	}

	inline static int32_t get_offset_of_touchType_18() { return static_cast<int32_t>(offsetof(BaseFinger_t854008753, ___touchType_18)); }
	inline int32_t get_touchType_18() const { return ___touchType_18; }
	inline int32_t* get_address_of_touchType_18() { return &___touchType_18; }
	inline void set_touchType_18(int32_t value)
	{
		___touchType_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFINGER_T854008753_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TWOFINGERGESTURE_T1936485437_H
#define TWOFINGERGESTURE_T1936485437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.TwoFingerGesture
struct  TwoFingerGesture_t1936485437  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/GestureType HedgehogTeam.EasyTouch.TwoFingerGesture::currentGesture
	int32_t ___currentGesture_0;
	// HedgehogTeam.EasyTouch.EasyTouch/GestureType HedgehogTeam.EasyTouch.TwoFingerGesture::oldGesture
	int32_t ___oldGesture_1;
	// System.Int32 HedgehogTeam.EasyTouch.TwoFingerGesture::finger0
	int32_t ___finger0_2;
	// System.Int32 HedgehogTeam.EasyTouch.TwoFingerGesture::finger1
	int32_t ___finger1_3;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::startTimeAction
	float ___startTimeAction_4;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::timeSinceStartAction
	float ___timeSinceStartAction_5;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::startPosition
	Vector2_t2156229523  ___startPosition_6;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::position
	Vector2_t2156229523  ___position_7;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::deltaPosition
	Vector2_t2156229523  ___deltaPosition_8;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::oldStartPosition
	Vector2_t2156229523  ___oldStartPosition_9;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::startDistance
	float ___startDistance_10;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::fingerDistance
	float ___fingerDistance_11;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::oldFingerDistance
	float ___oldFingerDistance_12;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::lockPinch
	bool ___lockPinch_13;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::lockTwist
	bool ___lockTwist_14;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::lastPinch
	float ___lastPinch_15;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::lastTwistAngle
	float ___lastTwistAngle_16;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::pickedObject
	GameObject_t1113636619 * ___pickedObject_17;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::oldPickedObject
	GameObject_t1113636619 * ___oldPickedObject_18;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.TwoFingerGesture::pickedCamera
	Camera_t4157153871 * ___pickedCamera_19;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::isGuiCamera
	bool ___isGuiCamera_20;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::isOverGui
	bool ___isOverGui_21;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::pickedUIElement
	GameObject_t1113636619 * ___pickedUIElement_22;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::dragStart
	bool ___dragStart_23;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::swipeStart
	bool ___swipeStart_24;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::inSingleDoubleTaps
	bool ___inSingleDoubleTaps_25;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::tapCurentTime
	float ___tapCurentTime_26;

public:
	inline static int32_t get_offset_of_currentGesture_0() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___currentGesture_0)); }
	inline int32_t get_currentGesture_0() const { return ___currentGesture_0; }
	inline int32_t* get_address_of_currentGesture_0() { return &___currentGesture_0; }
	inline void set_currentGesture_0(int32_t value)
	{
		___currentGesture_0 = value;
	}

	inline static int32_t get_offset_of_oldGesture_1() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___oldGesture_1)); }
	inline int32_t get_oldGesture_1() const { return ___oldGesture_1; }
	inline int32_t* get_address_of_oldGesture_1() { return &___oldGesture_1; }
	inline void set_oldGesture_1(int32_t value)
	{
		___oldGesture_1 = value;
	}

	inline static int32_t get_offset_of_finger0_2() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___finger0_2)); }
	inline int32_t get_finger0_2() const { return ___finger0_2; }
	inline int32_t* get_address_of_finger0_2() { return &___finger0_2; }
	inline void set_finger0_2(int32_t value)
	{
		___finger0_2 = value;
	}

	inline static int32_t get_offset_of_finger1_3() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___finger1_3)); }
	inline int32_t get_finger1_3() const { return ___finger1_3; }
	inline int32_t* get_address_of_finger1_3() { return &___finger1_3; }
	inline void set_finger1_3(int32_t value)
	{
		___finger1_3 = value;
	}

	inline static int32_t get_offset_of_startTimeAction_4() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___startTimeAction_4)); }
	inline float get_startTimeAction_4() const { return ___startTimeAction_4; }
	inline float* get_address_of_startTimeAction_4() { return &___startTimeAction_4; }
	inline void set_startTimeAction_4(float value)
	{
		___startTimeAction_4 = value;
	}

	inline static int32_t get_offset_of_timeSinceStartAction_5() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___timeSinceStartAction_5)); }
	inline float get_timeSinceStartAction_5() const { return ___timeSinceStartAction_5; }
	inline float* get_address_of_timeSinceStartAction_5() { return &___timeSinceStartAction_5; }
	inline void set_timeSinceStartAction_5(float value)
	{
		___timeSinceStartAction_5 = value;
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___startPosition_6)); }
	inline Vector2_t2156229523  get_startPosition_6() const { return ___startPosition_6; }
	inline Vector2_t2156229523 * get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(Vector2_t2156229523  value)
	{
		___startPosition_6 = value;
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___position_7)); }
	inline Vector2_t2156229523  get_position_7() const { return ___position_7; }
	inline Vector2_t2156229523 * get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(Vector2_t2156229523  value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_8() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___deltaPosition_8)); }
	inline Vector2_t2156229523  get_deltaPosition_8() const { return ___deltaPosition_8; }
	inline Vector2_t2156229523 * get_address_of_deltaPosition_8() { return &___deltaPosition_8; }
	inline void set_deltaPosition_8(Vector2_t2156229523  value)
	{
		___deltaPosition_8 = value;
	}

	inline static int32_t get_offset_of_oldStartPosition_9() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___oldStartPosition_9)); }
	inline Vector2_t2156229523  get_oldStartPosition_9() const { return ___oldStartPosition_9; }
	inline Vector2_t2156229523 * get_address_of_oldStartPosition_9() { return &___oldStartPosition_9; }
	inline void set_oldStartPosition_9(Vector2_t2156229523  value)
	{
		___oldStartPosition_9 = value;
	}

	inline static int32_t get_offset_of_startDistance_10() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___startDistance_10)); }
	inline float get_startDistance_10() const { return ___startDistance_10; }
	inline float* get_address_of_startDistance_10() { return &___startDistance_10; }
	inline void set_startDistance_10(float value)
	{
		___startDistance_10 = value;
	}

	inline static int32_t get_offset_of_fingerDistance_11() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___fingerDistance_11)); }
	inline float get_fingerDistance_11() const { return ___fingerDistance_11; }
	inline float* get_address_of_fingerDistance_11() { return &___fingerDistance_11; }
	inline void set_fingerDistance_11(float value)
	{
		___fingerDistance_11 = value;
	}

	inline static int32_t get_offset_of_oldFingerDistance_12() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___oldFingerDistance_12)); }
	inline float get_oldFingerDistance_12() const { return ___oldFingerDistance_12; }
	inline float* get_address_of_oldFingerDistance_12() { return &___oldFingerDistance_12; }
	inline void set_oldFingerDistance_12(float value)
	{
		___oldFingerDistance_12 = value;
	}

	inline static int32_t get_offset_of_lockPinch_13() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___lockPinch_13)); }
	inline bool get_lockPinch_13() const { return ___lockPinch_13; }
	inline bool* get_address_of_lockPinch_13() { return &___lockPinch_13; }
	inline void set_lockPinch_13(bool value)
	{
		___lockPinch_13 = value;
	}

	inline static int32_t get_offset_of_lockTwist_14() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___lockTwist_14)); }
	inline bool get_lockTwist_14() const { return ___lockTwist_14; }
	inline bool* get_address_of_lockTwist_14() { return &___lockTwist_14; }
	inline void set_lockTwist_14(bool value)
	{
		___lockTwist_14 = value;
	}

	inline static int32_t get_offset_of_lastPinch_15() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___lastPinch_15)); }
	inline float get_lastPinch_15() const { return ___lastPinch_15; }
	inline float* get_address_of_lastPinch_15() { return &___lastPinch_15; }
	inline void set_lastPinch_15(float value)
	{
		___lastPinch_15 = value;
	}

	inline static int32_t get_offset_of_lastTwistAngle_16() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___lastTwistAngle_16)); }
	inline float get_lastTwistAngle_16() const { return ___lastTwistAngle_16; }
	inline float* get_address_of_lastTwistAngle_16() { return &___lastTwistAngle_16; }
	inline void set_lastTwistAngle_16(float value)
	{
		___lastTwistAngle_16 = value;
	}

	inline static int32_t get_offset_of_pickedObject_17() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___pickedObject_17)); }
	inline GameObject_t1113636619 * get_pickedObject_17() const { return ___pickedObject_17; }
	inline GameObject_t1113636619 ** get_address_of_pickedObject_17() { return &___pickedObject_17; }
	inline void set_pickedObject_17(GameObject_t1113636619 * value)
	{
		___pickedObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_17), value);
	}

	inline static int32_t get_offset_of_oldPickedObject_18() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___oldPickedObject_18)); }
	inline GameObject_t1113636619 * get_oldPickedObject_18() const { return ___oldPickedObject_18; }
	inline GameObject_t1113636619 ** get_address_of_oldPickedObject_18() { return &___oldPickedObject_18; }
	inline void set_oldPickedObject_18(GameObject_t1113636619 * value)
	{
		___oldPickedObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___oldPickedObject_18), value);
	}

	inline static int32_t get_offset_of_pickedCamera_19() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___pickedCamera_19)); }
	inline Camera_t4157153871 * get_pickedCamera_19() const { return ___pickedCamera_19; }
	inline Camera_t4157153871 ** get_address_of_pickedCamera_19() { return &___pickedCamera_19; }
	inline void set_pickedCamera_19(Camera_t4157153871 * value)
	{
		___pickedCamera_19 = value;
		Il2CppCodeGenWriteBarrier((&___pickedCamera_19), value);
	}

	inline static int32_t get_offset_of_isGuiCamera_20() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___isGuiCamera_20)); }
	inline bool get_isGuiCamera_20() const { return ___isGuiCamera_20; }
	inline bool* get_address_of_isGuiCamera_20() { return &___isGuiCamera_20; }
	inline void set_isGuiCamera_20(bool value)
	{
		___isGuiCamera_20 = value;
	}

	inline static int32_t get_offset_of_isOverGui_21() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___isOverGui_21)); }
	inline bool get_isOverGui_21() const { return ___isOverGui_21; }
	inline bool* get_address_of_isOverGui_21() { return &___isOverGui_21; }
	inline void set_isOverGui_21(bool value)
	{
		___isOverGui_21 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_22() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___pickedUIElement_22)); }
	inline GameObject_t1113636619 * get_pickedUIElement_22() const { return ___pickedUIElement_22; }
	inline GameObject_t1113636619 ** get_address_of_pickedUIElement_22() { return &___pickedUIElement_22; }
	inline void set_pickedUIElement_22(GameObject_t1113636619 * value)
	{
		___pickedUIElement_22 = value;
		Il2CppCodeGenWriteBarrier((&___pickedUIElement_22), value);
	}

	inline static int32_t get_offset_of_dragStart_23() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___dragStart_23)); }
	inline bool get_dragStart_23() const { return ___dragStart_23; }
	inline bool* get_address_of_dragStart_23() { return &___dragStart_23; }
	inline void set_dragStart_23(bool value)
	{
		___dragStart_23 = value;
	}

	inline static int32_t get_offset_of_swipeStart_24() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___swipeStart_24)); }
	inline bool get_swipeStart_24() const { return ___swipeStart_24; }
	inline bool* get_address_of_swipeStart_24() { return &___swipeStart_24; }
	inline void set_swipeStart_24(bool value)
	{
		___swipeStart_24 = value;
	}

	inline static int32_t get_offset_of_inSingleDoubleTaps_25() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___inSingleDoubleTaps_25)); }
	inline bool get_inSingleDoubleTaps_25() const { return ___inSingleDoubleTaps_25; }
	inline bool* get_address_of_inSingleDoubleTaps_25() { return &___inSingleDoubleTaps_25; }
	inline void set_inSingleDoubleTaps_25(bool value)
	{
		___inSingleDoubleTaps_25 = value;
	}

	inline static int32_t get_offset_of_tapCurentTime_26() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t1936485437, ___tapCurentTime_26)); }
	inline float get_tapCurentTime_26() const { return ___tapCurentTime_26; }
	inline float* get_address_of_tapCurentTime_26() { return &___tapCurentTime_26; }
	inline void set_tapCurentTime_26(float value)
	{
		___tapCurentTime_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOFINGERGESTURE_T1936485437_H
#ifndef LONGTAPHANDLER_T1819341489_H
#define LONGTAPHANDLER_T1819341489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/LongTapHandler
struct  LongTapHandler_t1819341489  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPHANDLER_T1819341489_H
#ifndef TOUCHCANCELHANDLER_T1588360815_H
#define TOUCHCANCELHANDLER_T1588360815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchCancelHandler
struct  TouchCancelHandler_t1588360815  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCANCELHANDLER_T1588360815_H
#ifndef CANCEL2FINGERSHANDLER_T1819755313_H
#define CANCEL2FINGERSHANDLER_T1819755313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler
struct  Cancel2FingersHandler_t1819755313  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCEL2FINGERSHANDLER_T1819755313_H
#ifndef TOUCHSTARTHANDLER_T1924490190_H
#define TOUCHSTARTHANDLER_T1924490190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler
struct  TouchStartHandler_t1924490190  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTARTHANDLER_T1924490190_H
#ifndef TOUCHDOWNHANDLER_T1840035764_H
#define TOUCHDOWNHANDLER_T1840035764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler
struct  TouchDownHandler_t1840035764  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWNHANDLER_T1840035764_H
#ifndef TOUCHUPHANDLER_T2315724297_H
#define TOUCHUPHANDLER_T2315724297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler
struct  TouchUpHandler_t2315724297  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUPHANDLER_T2315724297_H
#ifndef SIMPLETAPHANDLER_T2789537387_H
#define SIMPLETAPHANDLER_T2789537387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler
struct  SimpleTapHandler_t2789537387  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAPHANDLER_T2789537387_H
#ifndef DOUBLETAPHANDLER_T2307588667_H
#define DOUBLETAPHANDLER_T2307588667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapHandler
struct  DoubleTapHandler_t2307588667  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAPHANDLER_T2307588667_H
#ifndef LONGTAPSTARTHANDLER_T3842992360_H
#define LONGTAPSTARTHANDLER_T3842992360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/LongTapStartHandler
struct  LongTapStartHandler_t3842992360  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPSTARTHANDLER_T3842992360_H
#ifndef FINGER_T386077194_H
#define FINGER_T386077194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Finger
struct  Finger_t386077194  : public BaseFinger_t854008753
{
public:
	// System.Single HedgehogTeam.EasyTouch.Finger::startTimeAction
	float ___startTimeAction_19;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Finger::oldPosition
	Vector2_t2156229523  ___oldPosition_20;
	// System.Int32 HedgehogTeam.EasyTouch.Finger::tapCount
	int32_t ___tapCount_21;
	// UnityEngine.TouchPhase HedgehogTeam.EasyTouch.Finger::phase
	int32_t ___phase_22;
	// HedgehogTeam.EasyTouch.EasyTouch/GestureType HedgehogTeam.EasyTouch.Finger::gesture
	int32_t ___gesture_23;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeDirection HedgehogTeam.EasyTouch.Finger::oldSwipeType
	int32_t ___oldSwipeType_24;

public:
	inline static int32_t get_offset_of_startTimeAction_19() { return static_cast<int32_t>(offsetof(Finger_t386077194, ___startTimeAction_19)); }
	inline float get_startTimeAction_19() const { return ___startTimeAction_19; }
	inline float* get_address_of_startTimeAction_19() { return &___startTimeAction_19; }
	inline void set_startTimeAction_19(float value)
	{
		___startTimeAction_19 = value;
	}

	inline static int32_t get_offset_of_oldPosition_20() { return static_cast<int32_t>(offsetof(Finger_t386077194, ___oldPosition_20)); }
	inline Vector2_t2156229523  get_oldPosition_20() const { return ___oldPosition_20; }
	inline Vector2_t2156229523 * get_address_of_oldPosition_20() { return &___oldPosition_20; }
	inline void set_oldPosition_20(Vector2_t2156229523  value)
	{
		___oldPosition_20 = value;
	}

	inline static int32_t get_offset_of_tapCount_21() { return static_cast<int32_t>(offsetof(Finger_t386077194, ___tapCount_21)); }
	inline int32_t get_tapCount_21() const { return ___tapCount_21; }
	inline int32_t* get_address_of_tapCount_21() { return &___tapCount_21; }
	inline void set_tapCount_21(int32_t value)
	{
		___tapCount_21 = value;
	}

	inline static int32_t get_offset_of_phase_22() { return static_cast<int32_t>(offsetof(Finger_t386077194, ___phase_22)); }
	inline int32_t get_phase_22() const { return ___phase_22; }
	inline int32_t* get_address_of_phase_22() { return &___phase_22; }
	inline void set_phase_22(int32_t value)
	{
		___phase_22 = value;
	}

	inline static int32_t get_offset_of_gesture_23() { return static_cast<int32_t>(offsetof(Finger_t386077194, ___gesture_23)); }
	inline int32_t get_gesture_23() const { return ___gesture_23; }
	inline int32_t* get_address_of_gesture_23() { return &___gesture_23; }
	inline void set_gesture_23(int32_t value)
	{
		___gesture_23 = value;
	}

	inline static int32_t get_offset_of_oldSwipeType_24() { return static_cast<int32_t>(offsetof(Finger_t386077194, ___oldSwipeType_24)); }
	inline int32_t get_oldSwipeType_24() const { return ___oldSwipeType_24; }
	inline int32_t* get_address_of_oldSwipeType_24() { return &___oldSwipeType_24; }
	inline void set_oldSwipeType_24(int32_t value)
	{
		___oldSwipeType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGER_T386077194_H
#ifndef UIELEMENTTOUCHUPHANDLER_T73634063_H
#define UIELEMENTTOUCHUPHANDLER_T73634063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/UIElementTouchUpHandler
struct  UIElementTouchUpHandler_t73634063  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIELEMENTTOUCHUPHANDLER_T73634063_H
#ifndef OVERUIELEMENTHANDLER_T2097719920_H
#define OVERUIELEMENTHANDLER_T2097719920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/OverUIElementHandler
struct  OverUIElementHandler_t2097719920  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERUIELEMENTHANDLER_T2097719920_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef EASYTOUCHISREADYHANDLER_T733707701_H
#define EASYTOUCHISREADYHANDLER_T733707701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler
struct  EasyTouchIsReadyHandler_t733707701  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHISREADYHANDLER_T733707701_H
#ifndef SWIPEEND2FINGERSHANDLER_T692742005_H
#define SWIPEEND2FINGERSHANDLER_T692742005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler
struct  SwipeEnd2FingersHandler_t692742005  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEEND2FINGERSHANDLER_T692742005_H
#ifndef SWIPE2FINGERSHANDLER_T3165766933_H
#define SWIPE2FINGERSHANDLER_T3165766933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler
struct  Swipe2FingersHandler_t3165766933  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPE2FINGERSHANDLER_T3165766933_H
#ifndef SWIPESTART2FINGERSHANDLER_T2227626112_H
#define SWIPESTART2FINGERSHANDLER_T2227626112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler
struct  SwipeStart2FingersHandler_t2227626112  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPESTART2FINGERSHANDLER_T2227626112_H
#ifndef DRAGEND2FINGERSHANDLER_T4228727231_H
#define DRAGEND2FINGERSHANDLER_T4228727231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler
struct  DragEnd2FingersHandler_t4228727231  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGEND2FINGERSHANDLER_T4228727231_H
#ifndef DRAG2FINGERSHANDLER_T3289523355_H
#define DRAG2FINGERSHANDLER_T3289523355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler
struct  Drag2FingersHandler_t3289523355  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAG2FINGERSHANDLER_T3289523355_H
#ifndef DRAGSTART2FINGERSHANDLER_T2484576551_H
#define DRAGSTART2FINGERSHANDLER_T2484576551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler
struct  DragStart2FingersHandler_t2484576551  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTART2FINGERSHANDLER_T2484576551_H
#ifndef PINCHHANDLER_T2742420652_H
#define PINCHHANDLER_T2742420652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler
struct  PinchHandler_t2742420652  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHHANDLER_T2742420652_H
#ifndef LONGTAPENDHANDLER_T661558872_H
#define LONGTAPENDHANDLER_T661558872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/LongTapEndHandler
struct  LongTapEndHandler_t661558872  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPENDHANDLER_T661558872_H
#ifndef TOUCHUP2FINGERSHANDLER_T2394109167_H
#define TOUCHUP2FINGERSHANDLER_T2394109167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler
struct  TouchUp2FingersHandler_t2394109167  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUP2FINGERSHANDLER_T2394109167_H
#ifndef SIMPLETAP2FINGERSHANDLER_T1325175573_H
#define SIMPLETAP2FINGERSHANDLER_T1325175573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler
struct  SimpleTap2FingersHandler_t1325175573  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAP2FINGERSHANDLER_T1325175573_H
#ifndef DOUBLETAP2FINGERSHANDLER_T904814161_H
#define DOUBLETAP2FINGERSHANDLER_T904814161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler
struct  DoubleTap2FingersHandler_t904814161  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLETAP2FINGERSHANDLER_T904814161_H
#ifndef LONGTAPSTART2FINGERSHANDLER_T938028626_H
#define LONGTAPSTART2FINGERSHANDLER_T938028626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler
struct  LongTapStart2FingersHandler_t938028626  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPSTART2FINGERSHANDLER_T938028626_H
#ifndef LONGTAP2FINGERSHANDLER_T3793547641_H
#define LONGTAP2FINGERSHANDLER_T3793547641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler
struct  LongTap2FingersHandler_t3793547641  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAP2FINGERSHANDLER_T3793547641_H
#ifndef LONGTAPEND2FINGERSHANDLER_T2721930548_H
#define LONGTAPEND2FINGERSHANDLER_T2721930548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler
struct  LongTapEnd2FingersHandler_t2721930548  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGTAPEND2FINGERSHANDLER_T2721930548_H
#ifndef TWISTHANDLER_T3457605880_H
#define TWISTHANDLER_T3457605880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler
struct  TwistHandler_t3457605880  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTHANDLER_T3457605880_H
#ifndef TWISTENDHANDLER_T429324375_H
#define TWISTENDHANDLER_T429324375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler
struct  TwistEndHandler_t429324375  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTENDHANDLER_T429324375_H
#ifndef PINCHINHANDLER_T1143385375_H
#define PINCHINHANDLER_T1143385375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/PinchInHandler
struct  PinchInHandler_t1143385375  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHINHANDLER_T1143385375_H
#ifndef PINCHOUTHANDLER_T205849317_H
#define PINCHOUTHANDLER_T205849317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/PinchOutHandler
struct  PinchOutHandler_t205849317  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHOUTHANDLER_T205849317_H
#ifndef GESTURE_T3351707245_H
#define GESTURE_T3351707245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Gesture
struct  Gesture_t3351707245  : public BaseFinger_t854008753
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeDirection HedgehogTeam.EasyTouch.Gesture::swipe
	int32_t ___swipe_19;
	// System.Single HedgehogTeam.EasyTouch.Gesture::swipeLength
	float ___swipeLength_20;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Gesture::swipeVector
	Vector2_t2156229523  ___swipeVector_21;
	// System.Single HedgehogTeam.EasyTouch.Gesture::deltaPinch
	float ___deltaPinch_22;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twistAngle
	float ___twistAngle_23;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twoFingerDistance
	float ___twoFingerDistance_24;
	// HedgehogTeam.EasyTouch.EasyTouch/EvtType HedgehogTeam.EasyTouch.Gesture::type
	int32_t ___type_25;

public:
	inline static int32_t get_offset_of_swipe_19() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___swipe_19)); }
	inline int32_t get_swipe_19() const { return ___swipe_19; }
	inline int32_t* get_address_of_swipe_19() { return &___swipe_19; }
	inline void set_swipe_19(int32_t value)
	{
		___swipe_19 = value;
	}

	inline static int32_t get_offset_of_swipeLength_20() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___swipeLength_20)); }
	inline float get_swipeLength_20() const { return ___swipeLength_20; }
	inline float* get_address_of_swipeLength_20() { return &___swipeLength_20; }
	inline void set_swipeLength_20(float value)
	{
		___swipeLength_20 = value;
	}

	inline static int32_t get_offset_of_swipeVector_21() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___swipeVector_21)); }
	inline Vector2_t2156229523  get_swipeVector_21() const { return ___swipeVector_21; }
	inline Vector2_t2156229523 * get_address_of_swipeVector_21() { return &___swipeVector_21; }
	inline void set_swipeVector_21(Vector2_t2156229523  value)
	{
		___swipeVector_21 = value;
	}

	inline static int32_t get_offset_of_deltaPinch_22() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___deltaPinch_22)); }
	inline float get_deltaPinch_22() const { return ___deltaPinch_22; }
	inline float* get_address_of_deltaPinch_22() { return &___deltaPinch_22; }
	inline void set_deltaPinch_22(float value)
	{
		___deltaPinch_22 = value;
	}

	inline static int32_t get_offset_of_twistAngle_23() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___twistAngle_23)); }
	inline float get_twistAngle_23() const { return ___twistAngle_23; }
	inline float* get_address_of_twistAngle_23() { return &___twistAngle_23; }
	inline void set_twistAngle_23(float value)
	{
		___twistAngle_23 = value;
	}

	inline static int32_t get_offset_of_twoFingerDistance_24() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___twoFingerDistance_24)); }
	inline float get_twoFingerDistance_24() const { return ___twoFingerDistance_24; }
	inline float* get_address_of_twoFingerDistance_24() { return &___twoFingerDistance_24; }
	inline void set_twoFingerDistance_24(float value)
	{
		___twoFingerDistance_24 = value;
	}

	inline static int32_t get_offset_of_type_25() { return static_cast<int32_t>(offsetof(Gesture_t3351707245, ___type_25)); }
	inline int32_t get_type_25() const { return ___type_25; }
	inline int32_t* get_address_of_type_25() { return &___type_25; }
	inline void set_type_25(int32_t value)
	{
		___type_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURE_T3351707245_H
#ifndef TOUCHDOWN2FINGERSHANDLER_T886882400_H
#define TOUCHDOWN2FINGERSHANDLER_T886882400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler
struct  TouchDown2FingersHandler_t886882400  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWN2FINGERSHANDLER_T886882400_H
#ifndef DRAGSTARTHANDLER_T2454096439_H
#define DRAGSTARTHANDLER_T2454096439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DragStartHandler
struct  DragStartHandler_t2454096439  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTARTHANDLER_T2454096439_H
#ifndef DRAGHANDLER_T4081535653_H
#define DRAGHANDLER_T4081535653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DragHandler
struct  DragHandler_t4081535653  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGHANDLER_T4081535653_H
#ifndef DRAGENDHANDLER_T378428410_H
#define DRAGENDHANDLER_T378428410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DragEndHandler
struct  DragEndHandler_t378428410  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGENDHANDLER_T378428410_H
#ifndef PINCHENDHANDLER_T901660763_H
#define PINCHENDHANDLER_T901660763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/PinchEndHandler
struct  PinchEndHandler_t901660763  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHENDHANDLER_T901660763_H
#ifndef SWIPESTARTHANDLER_T372347372_H
#define SWIPESTARTHANDLER_T372347372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler
struct  SwipeStartHandler_t372347372  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPESTARTHANDLER_T372347372_H
#ifndef SWIPEHANDLER_T1550509410_H
#define SWIPEHANDLER_T1550509410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler
struct  SwipeHandler_t1550509410  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEHANDLER_T1550509410_H
#ifndef TOUCHSTART2FINGERSHANDLER_T2004139386_H
#define TOUCHSTART2FINGERSHANDLER_T2004139386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler
struct  TouchStart2FingersHandler_t2004139386  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTART2FINGERSHANDLER_T2004139386_H
#ifndef SWIPEENDHANDLER_T2662005585_H
#define SWIPEENDHANDLER_T2662005585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler
struct  SwipeEndHandler_t2662005585  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEENDHANDLER_T2662005585_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ETCBASE_T40313246_H
#define ETCBASE_T40313246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase
struct  ETCBase_t40313246  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform ETCBase::cachedRectTransform
	RectTransform_t3704657025 * ___cachedRectTransform_2;
	// UnityEngine.Canvas ETCBase::cachedRootCanvas
	Canvas_t3310196443 * ___cachedRootCanvas_3;
	// System.Boolean ETCBase::isUnregisterAtDisable
	bool ___isUnregisterAtDisable_4;
	// System.Boolean ETCBase::visibleAtStart
	bool ___visibleAtStart_5;
	// System.Boolean ETCBase::activatedAtStart
	bool ___activatedAtStart_6;
	// ETCBase/RectAnchor ETCBase::_anchor
	int32_t ____anchor_7;
	// UnityEngine.Vector2 ETCBase::_anchorOffet
	Vector2_t2156229523  ____anchorOffet_8;
	// System.Boolean ETCBase::_visible
	bool ____visible_9;
	// System.Boolean ETCBase::_activated
	bool ____activated_10;
	// System.Boolean ETCBase::enableCamera
	bool ___enableCamera_11;
	// ETCBase/CameraMode ETCBase::cameraMode
	int32_t ___cameraMode_12;
	// System.String ETCBase::camTargetTag
	String_t* ___camTargetTag_13;
	// System.Boolean ETCBase::autoLinkTagCam
	bool ___autoLinkTagCam_14;
	// System.String ETCBase::autoCamTag
	String_t* ___autoCamTag_15;
	// UnityEngine.Transform ETCBase::cameraTransform
	Transform_t3600365921 * ___cameraTransform_16;
	// ETCBase/CameraTargetMode ETCBase::cameraTargetMode
	int32_t ___cameraTargetMode_17;
	// System.Boolean ETCBase::enableWallDetection
	bool ___enableWallDetection_18;
	// UnityEngine.LayerMask ETCBase::wallLayer
	LayerMask_t3493934918  ___wallLayer_19;
	// UnityEngine.Transform ETCBase::cameraLookAt
	Transform_t3600365921 * ___cameraLookAt_20;
	// UnityEngine.CharacterController ETCBase::cameraLookAtCC
	CharacterController_t1138636865 * ___cameraLookAtCC_21;
	// UnityEngine.Vector3 ETCBase::followOffset
	Vector3_t3722313464  ___followOffset_22;
	// System.Single ETCBase::followDistance
	float ___followDistance_23;
	// System.Single ETCBase::followHeight
	float ___followHeight_24;
	// System.Single ETCBase::followRotationDamping
	float ___followRotationDamping_25;
	// System.Single ETCBase::followHeightDamping
	float ___followHeightDamping_26;
	// System.Int32 ETCBase::pointId
	int32_t ___pointId_27;
	// System.Boolean ETCBase::enableKeySimulation
	bool ___enableKeySimulation_28;
	// System.Boolean ETCBase::allowSimulationStandalone
	bool ___allowSimulationStandalone_29;
	// System.Boolean ETCBase::visibleOnStandalone
	bool ___visibleOnStandalone_30;
	// ETCBase/DPadAxis ETCBase::dPadAxisCount
	int32_t ___dPadAxisCount_31;
	// System.Boolean ETCBase::useFixedUpdate
	bool ___useFixedUpdate_32;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> ETCBase::uiRaycastResultCache
	List_1_t537414295 * ___uiRaycastResultCache_33;
	// UnityEngine.EventSystems.PointerEventData ETCBase::uiPointerEventData
	PointerEventData_t3807901092 * ___uiPointerEventData_34;
	// UnityEngine.EventSystems.EventSystem ETCBase::uiEventSystem
	EventSystem_t1003666588 * ___uiEventSystem_35;
	// System.Boolean ETCBase::isOnDrag
	bool ___isOnDrag_36;
	// System.Boolean ETCBase::isSwipeIn
	bool ___isSwipeIn_37;
	// System.Boolean ETCBase::isSwipeOut
	bool ___isSwipeOut_38;
	// System.Boolean ETCBase::showPSInspector
	bool ___showPSInspector_39;
	// System.Boolean ETCBase::showSpriteInspector
	bool ___showSpriteInspector_40;
	// System.Boolean ETCBase::showEventInspector
	bool ___showEventInspector_41;
	// System.Boolean ETCBase::showBehaviourInspector
	bool ___showBehaviourInspector_42;
	// System.Boolean ETCBase::showAxesInspector
	bool ___showAxesInspector_43;
	// System.Boolean ETCBase::showTouchEventInspector
	bool ___showTouchEventInspector_44;
	// System.Boolean ETCBase::showDownEventInspector
	bool ___showDownEventInspector_45;
	// System.Boolean ETCBase::showPressEventInspector
	bool ___showPressEventInspector_46;
	// System.Boolean ETCBase::showCameraInspector
	bool ___showCameraInspector_47;

public:
	inline static int32_t get_offset_of_cachedRectTransform_2() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cachedRectTransform_2)); }
	inline RectTransform_t3704657025 * get_cachedRectTransform_2() const { return ___cachedRectTransform_2; }
	inline RectTransform_t3704657025 ** get_address_of_cachedRectTransform_2() { return &___cachedRectTransform_2; }
	inline void set_cachedRectTransform_2(RectTransform_t3704657025 * value)
	{
		___cachedRectTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRectTransform_2), value);
	}

	inline static int32_t get_offset_of_cachedRootCanvas_3() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cachedRootCanvas_3)); }
	inline Canvas_t3310196443 * get_cachedRootCanvas_3() const { return ___cachedRootCanvas_3; }
	inline Canvas_t3310196443 ** get_address_of_cachedRootCanvas_3() { return &___cachedRootCanvas_3; }
	inline void set_cachedRootCanvas_3(Canvas_t3310196443 * value)
	{
		___cachedRootCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRootCanvas_3), value);
	}

	inline static int32_t get_offset_of_isUnregisterAtDisable_4() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isUnregisterAtDisable_4)); }
	inline bool get_isUnregisterAtDisable_4() const { return ___isUnregisterAtDisable_4; }
	inline bool* get_address_of_isUnregisterAtDisable_4() { return &___isUnregisterAtDisable_4; }
	inline void set_isUnregisterAtDisable_4(bool value)
	{
		___isUnregisterAtDisable_4 = value;
	}

	inline static int32_t get_offset_of_visibleAtStart_5() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___visibleAtStart_5)); }
	inline bool get_visibleAtStart_5() const { return ___visibleAtStart_5; }
	inline bool* get_address_of_visibleAtStart_5() { return &___visibleAtStart_5; }
	inline void set_visibleAtStart_5(bool value)
	{
		___visibleAtStart_5 = value;
	}

	inline static int32_t get_offset_of_activatedAtStart_6() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___activatedAtStart_6)); }
	inline bool get_activatedAtStart_6() const { return ___activatedAtStart_6; }
	inline bool* get_address_of_activatedAtStart_6() { return &___activatedAtStart_6; }
	inline void set_activatedAtStart_6(bool value)
	{
		___activatedAtStart_6 = value;
	}

	inline static int32_t get_offset_of__anchor_7() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____anchor_7)); }
	inline int32_t get__anchor_7() const { return ____anchor_7; }
	inline int32_t* get_address_of__anchor_7() { return &____anchor_7; }
	inline void set__anchor_7(int32_t value)
	{
		____anchor_7 = value;
	}

	inline static int32_t get_offset_of__anchorOffet_8() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____anchorOffet_8)); }
	inline Vector2_t2156229523  get__anchorOffet_8() const { return ____anchorOffet_8; }
	inline Vector2_t2156229523 * get_address_of__anchorOffet_8() { return &____anchorOffet_8; }
	inline void set__anchorOffet_8(Vector2_t2156229523  value)
	{
		____anchorOffet_8 = value;
	}

	inline static int32_t get_offset_of__visible_9() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____visible_9)); }
	inline bool get__visible_9() const { return ____visible_9; }
	inline bool* get_address_of__visible_9() { return &____visible_9; }
	inline void set__visible_9(bool value)
	{
		____visible_9 = value;
	}

	inline static int32_t get_offset_of__activated_10() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ____activated_10)); }
	inline bool get__activated_10() const { return ____activated_10; }
	inline bool* get_address_of__activated_10() { return &____activated_10; }
	inline void set__activated_10(bool value)
	{
		____activated_10 = value;
	}

	inline static int32_t get_offset_of_enableCamera_11() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___enableCamera_11)); }
	inline bool get_enableCamera_11() const { return ___enableCamera_11; }
	inline bool* get_address_of_enableCamera_11() { return &___enableCamera_11; }
	inline void set_enableCamera_11(bool value)
	{
		___enableCamera_11 = value;
	}

	inline static int32_t get_offset_of_cameraMode_12() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraMode_12)); }
	inline int32_t get_cameraMode_12() const { return ___cameraMode_12; }
	inline int32_t* get_address_of_cameraMode_12() { return &___cameraMode_12; }
	inline void set_cameraMode_12(int32_t value)
	{
		___cameraMode_12 = value;
	}

	inline static int32_t get_offset_of_camTargetTag_13() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___camTargetTag_13)); }
	inline String_t* get_camTargetTag_13() const { return ___camTargetTag_13; }
	inline String_t** get_address_of_camTargetTag_13() { return &___camTargetTag_13; }
	inline void set_camTargetTag_13(String_t* value)
	{
		___camTargetTag_13 = value;
		Il2CppCodeGenWriteBarrier((&___camTargetTag_13), value);
	}

	inline static int32_t get_offset_of_autoLinkTagCam_14() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___autoLinkTagCam_14)); }
	inline bool get_autoLinkTagCam_14() const { return ___autoLinkTagCam_14; }
	inline bool* get_address_of_autoLinkTagCam_14() { return &___autoLinkTagCam_14; }
	inline void set_autoLinkTagCam_14(bool value)
	{
		___autoLinkTagCam_14 = value;
	}

	inline static int32_t get_offset_of_autoCamTag_15() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___autoCamTag_15)); }
	inline String_t* get_autoCamTag_15() const { return ___autoCamTag_15; }
	inline String_t** get_address_of_autoCamTag_15() { return &___autoCamTag_15; }
	inline void set_autoCamTag_15(String_t* value)
	{
		___autoCamTag_15 = value;
		Il2CppCodeGenWriteBarrier((&___autoCamTag_15), value);
	}

	inline static int32_t get_offset_of_cameraTransform_16() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraTransform_16)); }
	inline Transform_t3600365921 * get_cameraTransform_16() const { return ___cameraTransform_16; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_16() { return &___cameraTransform_16; }
	inline void set_cameraTransform_16(Transform_t3600365921 * value)
	{
		___cameraTransform_16 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_16), value);
	}

	inline static int32_t get_offset_of_cameraTargetMode_17() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraTargetMode_17)); }
	inline int32_t get_cameraTargetMode_17() const { return ___cameraTargetMode_17; }
	inline int32_t* get_address_of_cameraTargetMode_17() { return &___cameraTargetMode_17; }
	inline void set_cameraTargetMode_17(int32_t value)
	{
		___cameraTargetMode_17 = value;
	}

	inline static int32_t get_offset_of_enableWallDetection_18() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___enableWallDetection_18)); }
	inline bool get_enableWallDetection_18() const { return ___enableWallDetection_18; }
	inline bool* get_address_of_enableWallDetection_18() { return &___enableWallDetection_18; }
	inline void set_enableWallDetection_18(bool value)
	{
		___enableWallDetection_18 = value;
	}

	inline static int32_t get_offset_of_wallLayer_19() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___wallLayer_19)); }
	inline LayerMask_t3493934918  get_wallLayer_19() const { return ___wallLayer_19; }
	inline LayerMask_t3493934918 * get_address_of_wallLayer_19() { return &___wallLayer_19; }
	inline void set_wallLayer_19(LayerMask_t3493934918  value)
	{
		___wallLayer_19 = value;
	}

	inline static int32_t get_offset_of_cameraLookAt_20() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraLookAt_20)); }
	inline Transform_t3600365921 * get_cameraLookAt_20() const { return ___cameraLookAt_20; }
	inline Transform_t3600365921 ** get_address_of_cameraLookAt_20() { return &___cameraLookAt_20; }
	inline void set_cameraLookAt_20(Transform_t3600365921 * value)
	{
		___cameraLookAt_20 = value;
		Il2CppCodeGenWriteBarrier((&___cameraLookAt_20), value);
	}

	inline static int32_t get_offset_of_cameraLookAtCC_21() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___cameraLookAtCC_21)); }
	inline CharacterController_t1138636865 * get_cameraLookAtCC_21() const { return ___cameraLookAtCC_21; }
	inline CharacterController_t1138636865 ** get_address_of_cameraLookAtCC_21() { return &___cameraLookAtCC_21; }
	inline void set_cameraLookAtCC_21(CharacterController_t1138636865 * value)
	{
		___cameraLookAtCC_21 = value;
		Il2CppCodeGenWriteBarrier((&___cameraLookAtCC_21), value);
	}

	inline static int32_t get_offset_of_followOffset_22() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followOffset_22)); }
	inline Vector3_t3722313464  get_followOffset_22() const { return ___followOffset_22; }
	inline Vector3_t3722313464 * get_address_of_followOffset_22() { return &___followOffset_22; }
	inline void set_followOffset_22(Vector3_t3722313464  value)
	{
		___followOffset_22 = value;
	}

	inline static int32_t get_offset_of_followDistance_23() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followDistance_23)); }
	inline float get_followDistance_23() const { return ___followDistance_23; }
	inline float* get_address_of_followDistance_23() { return &___followDistance_23; }
	inline void set_followDistance_23(float value)
	{
		___followDistance_23 = value;
	}

	inline static int32_t get_offset_of_followHeight_24() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followHeight_24)); }
	inline float get_followHeight_24() const { return ___followHeight_24; }
	inline float* get_address_of_followHeight_24() { return &___followHeight_24; }
	inline void set_followHeight_24(float value)
	{
		___followHeight_24 = value;
	}

	inline static int32_t get_offset_of_followRotationDamping_25() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followRotationDamping_25)); }
	inline float get_followRotationDamping_25() const { return ___followRotationDamping_25; }
	inline float* get_address_of_followRotationDamping_25() { return &___followRotationDamping_25; }
	inline void set_followRotationDamping_25(float value)
	{
		___followRotationDamping_25 = value;
	}

	inline static int32_t get_offset_of_followHeightDamping_26() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___followHeightDamping_26)); }
	inline float get_followHeightDamping_26() const { return ___followHeightDamping_26; }
	inline float* get_address_of_followHeightDamping_26() { return &___followHeightDamping_26; }
	inline void set_followHeightDamping_26(float value)
	{
		___followHeightDamping_26 = value;
	}

	inline static int32_t get_offset_of_pointId_27() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___pointId_27)); }
	inline int32_t get_pointId_27() const { return ___pointId_27; }
	inline int32_t* get_address_of_pointId_27() { return &___pointId_27; }
	inline void set_pointId_27(int32_t value)
	{
		___pointId_27 = value;
	}

	inline static int32_t get_offset_of_enableKeySimulation_28() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___enableKeySimulation_28)); }
	inline bool get_enableKeySimulation_28() const { return ___enableKeySimulation_28; }
	inline bool* get_address_of_enableKeySimulation_28() { return &___enableKeySimulation_28; }
	inline void set_enableKeySimulation_28(bool value)
	{
		___enableKeySimulation_28 = value;
	}

	inline static int32_t get_offset_of_allowSimulationStandalone_29() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___allowSimulationStandalone_29)); }
	inline bool get_allowSimulationStandalone_29() const { return ___allowSimulationStandalone_29; }
	inline bool* get_address_of_allowSimulationStandalone_29() { return &___allowSimulationStandalone_29; }
	inline void set_allowSimulationStandalone_29(bool value)
	{
		___allowSimulationStandalone_29 = value;
	}

	inline static int32_t get_offset_of_visibleOnStandalone_30() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___visibleOnStandalone_30)); }
	inline bool get_visibleOnStandalone_30() const { return ___visibleOnStandalone_30; }
	inline bool* get_address_of_visibleOnStandalone_30() { return &___visibleOnStandalone_30; }
	inline void set_visibleOnStandalone_30(bool value)
	{
		___visibleOnStandalone_30 = value;
	}

	inline static int32_t get_offset_of_dPadAxisCount_31() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___dPadAxisCount_31)); }
	inline int32_t get_dPadAxisCount_31() const { return ___dPadAxisCount_31; }
	inline int32_t* get_address_of_dPadAxisCount_31() { return &___dPadAxisCount_31; }
	inline void set_dPadAxisCount_31(int32_t value)
	{
		___dPadAxisCount_31 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_32() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___useFixedUpdate_32)); }
	inline bool get_useFixedUpdate_32() const { return ___useFixedUpdate_32; }
	inline bool* get_address_of_useFixedUpdate_32() { return &___useFixedUpdate_32; }
	inline void set_useFixedUpdate_32(bool value)
	{
		___useFixedUpdate_32 = value;
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_33() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___uiRaycastResultCache_33)); }
	inline List_1_t537414295 * get_uiRaycastResultCache_33() const { return ___uiRaycastResultCache_33; }
	inline List_1_t537414295 ** get_address_of_uiRaycastResultCache_33() { return &___uiRaycastResultCache_33; }
	inline void set_uiRaycastResultCache_33(List_1_t537414295 * value)
	{
		___uiRaycastResultCache_33 = value;
		Il2CppCodeGenWriteBarrier((&___uiRaycastResultCache_33), value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_34() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___uiPointerEventData_34)); }
	inline PointerEventData_t3807901092 * get_uiPointerEventData_34() const { return ___uiPointerEventData_34; }
	inline PointerEventData_t3807901092 ** get_address_of_uiPointerEventData_34() { return &___uiPointerEventData_34; }
	inline void set_uiPointerEventData_34(PointerEventData_t3807901092 * value)
	{
		___uiPointerEventData_34 = value;
		Il2CppCodeGenWriteBarrier((&___uiPointerEventData_34), value);
	}

	inline static int32_t get_offset_of_uiEventSystem_35() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___uiEventSystem_35)); }
	inline EventSystem_t1003666588 * get_uiEventSystem_35() const { return ___uiEventSystem_35; }
	inline EventSystem_t1003666588 ** get_address_of_uiEventSystem_35() { return &___uiEventSystem_35; }
	inline void set_uiEventSystem_35(EventSystem_t1003666588 * value)
	{
		___uiEventSystem_35 = value;
		Il2CppCodeGenWriteBarrier((&___uiEventSystem_35), value);
	}

	inline static int32_t get_offset_of_isOnDrag_36() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isOnDrag_36)); }
	inline bool get_isOnDrag_36() const { return ___isOnDrag_36; }
	inline bool* get_address_of_isOnDrag_36() { return &___isOnDrag_36; }
	inline void set_isOnDrag_36(bool value)
	{
		___isOnDrag_36 = value;
	}

	inline static int32_t get_offset_of_isSwipeIn_37() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isSwipeIn_37)); }
	inline bool get_isSwipeIn_37() const { return ___isSwipeIn_37; }
	inline bool* get_address_of_isSwipeIn_37() { return &___isSwipeIn_37; }
	inline void set_isSwipeIn_37(bool value)
	{
		___isSwipeIn_37 = value;
	}

	inline static int32_t get_offset_of_isSwipeOut_38() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___isSwipeOut_38)); }
	inline bool get_isSwipeOut_38() const { return ___isSwipeOut_38; }
	inline bool* get_address_of_isSwipeOut_38() { return &___isSwipeOut_38; }
	inline void set_isSwipeOut_38(bool value)
	{
		___isSwipeOut_38 = value;
	}

	inline static int32_t get_offset_of_showPSInspector_39() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showPSInspector_39)); }
	inline bool get_showPSInspector_39() const { return ___showPSInspector_39; }
	inline bool* get_address_of_showPSInspector_39() { return &___showPSInspector_39; }
	inline void set_showPSInspector_39(bool value)
	{
		___showPSInspector_39 = value;
	}

	inline static int32_t get_offset_of_showSpriteInspector_40() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showSpriteInspector_40)); }
	inline bool get_showSpriteInspector_40() const { return ___showSpriteInspector_40; }
	inline bool* get_address_of_showSpriteInspector_40() { return &___showSpriteInspector_40; }
	inline void set_showSpriteInspector_40(bool value)
	{
		___showSpriteInspector_40 = value;
	}

	inline static int32_t get_offset_of_showEventInspector_41() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showEventInspector_41)); }
	inline bool get_showEventInspector_41() const { return ___showEventInspector_41; }
	inline bool* get_address_of_showEventInspector_41() { return &___showEventInspector_41; }
	inline void set_showEventInspector_41(bool value)
	{
		___showEventInspector_41 = value;
	}

	inline static int32_t get_offset_of_showBehaviourInspector_42() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showBehaviourInspector_42)); }
	inline bool get_showBehaviourInspector_42() const { return ___showBehaviourInspector_42; }
	inline bool* get_address_of_showBehaviourInspector_42() { return &___showBehaviourInspector_42; }
	inline void set_showBehaviourInspector_42(bool value)
	{
		___showBehaviourInspector_42 = value;
	}

	inline static int32_t get_offset_of_showAxesInspector_43() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showAxesInspector_43)); }
	inline bool get_showAxesInspector_43() const { return ___showAxesInspector_43; }
	inline bool* get_address_of_showAxesInspector_43() { return &___showAxesInspector_43; }
	inline void set_showAxesInspector_43(bool value)
	{
		___showAxesInspector_43 = value;
	}

	inline static int32_t get_offset_of_showTouchEventInspector_44() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showTouchEventInspector_44)); }
	inline bool get_showTouchEventInspector_44() const { return ___showTouchEventInspector_44; }
	inline bool* get_address_of_showTouchEventInspector_44() { return &___showTouchEventInspector_44; }
	inline void set_showTouchEventInspector_44(bool value)
	{
		___showTouchEventInspector_44 = value;
	}

	inline static int32_t get_offset_of_showDownEventInspector_45() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showDownEventInspector_45)); }
	inline bool get_showDownEventInspector_45() const { return ___showDownEventInspector_45; }
	inline bool* get_address_of_showDownEventInspector_45() { return &___showDownEventInspector_45; }
	inline void set_showDownEventInspector_45(bool value)
	{
		___showDownEventInspector_45 = value;
	}

	inline static int32_t get_offset_of_showPressEventInspector_46() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showPressEventInspector_46)); }
	inline bool get_showPressEventInspector_46() const { return ___showPressEventInspector_46; }
	inline bool* get_address_of_showPressEventInspector_46() { return &___showPressEventInspector_46; }
	inline void set_showPressEventInspector_46(bool value)
	{
		___showPressEventInspector_46 = value;
	}

	inline static int32_t get_offset_of_showCameraInspector_47() { return static_cast<int32_t>(offsetof(ETCBase_t40313246, ___showCameraInspector_47)); }
	inline bool get_showCameraInspector_47() const { return ___showCameraInspector_47; }
	inline bool* get_address_of_showCameraInspector_47() { return &___showCameraInspector_47; }
	inline void set_showCameraInspector_47(bool value)
	{
		___showCameraInspector_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCBASE_T40313246_H
#ifndef QUICKBASE_T718481069_H
#define QUICKBASE_T718481069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase
struct  QuickBase_t718481069  : public MonoBehaviour_t3962482529
{
public:
	// System.String HedgehogTeam.EasyTouch.QuickBase::quickActionName
	String_t* ___quickActionName_2;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isMultiTouch
	bool ___isMultiTouch_3;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::is2Finger
	bool ___is2Finger_4;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isOnTouch
	bool ___isOnTouch_5;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::enablePickOverUI
	bool ___enablePickOverUI_6;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::resetPhysic
	bool ___resetPhysic_7;
	// HedgehogTeam.EasyTouch.QuickBase/DirectAction HedgehogTeam.EasyTouch.QuickBase::directAction
	int32_t ___directAction_8;
	// HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction HedgehogTeam.EasyTouch.QuickBase::axesAction
	int32_t ___axesAction_9;
	// System.Single HedgehogTeam.EasyTouch.QuickBase::sensibility
	float ___sensibility_10;
	// UnityEngine.CharacterController HedgehogTeam.EasyTouch.QuickBase::directCharacterController
	CharacterController_t1138636865 * ___directCharacterController_11;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::inverseAxisValue
	bool ___inverseAxisValue_12;
	// UnityEngine.Rigidbody HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody
	Rigidbody_t3916780224 * ___cachedRigidBody_13;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic
	bool ___isKinematic_14;
	// UnityEngine.Rigidbody2D HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody2D
	Rigidbody2D_t939494601 * ___cachedRigidBody2D_15;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic2D
	bool ___isKinematic2D_16;
	// HedgehogTeam.EasyTouch.QuickBase/GameObjectType HedgehogTeam.EasyTouch.QuickBase::realType
	int32_t ___realType_17;
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase::fingerIndex
	int32_t ___fingerIndex_18;

public:
	inline static int32_t get_offset_of_quickActionName_2() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___quickActionName_2)); }
	inline String_t* get_quickActionName_2() const { return ___quickActionName_2; }
	inline String_t** get_address_of_quickActionName_2() { return &___quickActionName_2; }
	inline void set_quickActionName_2(String_t* value)
	{
		___quickActionName_2 = value;
		Il2CppCodeGenWriteBarrier((&___quickActionName_2), value);
	}

	inline static int32_t get_offset_of_isMultiTouch_3() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isMultiTouch_3)); }
	inline bool get_isMultiTouch_3() const { return ___isMultiTouch_3; }
	inline bool* get_address_of_isMultiTouch_3() { return &___isMultiTouch_3; }
	inline void set_isMultiTouch_3(bool value)
	{
		___isMultiTouch_3 = value;
	}

	inline static int32_t get_offset_of_is2Finger_4() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___is2Finger_4)); }
	inline bool get_is2Finger_4() const { return ___is2Finger_4; }
	inline bool* get_address_of_is2Finger_4() { return &___is2Finger_4; }
	inline void set_is2Finger_4(bool value)
	{
		___is2Finger_4 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_5() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isOnTouch_5)); }
	inline bool get_isOnTouch_5() const { return ___isOnTouch_5; }
	inline bool* get_address_of_isOnTouch_5() { return &___isOnTouch_5; }
	inline void set_isOnTouch_5(bool value)
	{
		___isOnTouch_5 = value;
	}

	inline static int32_t get_offset_of_enablePickOverUI_6() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___enablePickOverUI_6)); }
	inline bool get_enablePickOverUI_6() const { return ___enablePickOverUI_6; }
	inline bool* get_address_of_enablePickOverUI_6() { return &___enablePickOverUI_6; }
	inline void set_enablePickOverUI_6(bool value)
	{
		___enablePickOverUI_6 = value;
	}

	inline static int32_t get_offset_of_resetPhysic_7() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___resetPhysic_7)); }
	inline bool get_resetPhysic_7() const { return ___resetPhysic_7; }
	inline bool* get_address_of_resetPhysic_7() { return &___resetPhysic_7; }
	inline void set_resetPhysic_7(bool value)
	{
		___resetPhysic_7 = value;
	}

	inline static int32_t get_offset_of_directAction_8() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___directAction_8)); }
	inline int32_t get_directAction_8() const { return ___directAction_8; }
	inline int32_t* get_address_of_directAction_8() { return &___directAction_8; }
	inline void set_directAction_8(int32_t value)
	{
		___directAction_8 = value;
	}

	inline static int32_t get_offset_of_axesAction_9() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___axesAction_9)); }
	inline int32_t get_axesAction_9() const { return ___axesAction_9; }
	inline int32_t* get_address_of_axesAction_9() { return &___axesAction_9; }
	inline void set_axesAction_9(int32_t value)
	{
		___axesAction_9 = value;
	}

	inline static int32_t get_offset_of_sensibility_10() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___sensibility_10)); }
	inline float get_sensibility_10() const { return ___sensibility_10; }
	inline float* get_address_of_sensibility_10() { return &___sensibility_10; }
	inline void set_sensibility_10(float value)
	{
		___sensibility_10 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_11() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___directCharacterController_11)); }
	inline CharacterController_t1138636865 * get_directCharacterController_11() const { return ___directCharacterController_11; }
	inline CharacterController_t1138636865 ** get_address_of_directCharacterController_11() { return &___directCharacterController_11; }
	inline void set_directCharacterController_11(CharacterController_t1138636865 * value)
	{
		___directCharacterController_11 = value;
		Il2CppCodeGenWriteBarrier((&___directCharacterController_11), value);
	}

	inline static int32_t get_offset_of_inverseAxisValue_12() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___inverseAxisValue_12)); }
	inline bool get_inverseAxisValue_12() const { return ___inverseAxisValue_12; }
	inline bool* get_address_of_inverseAxisValue_12() { return &___inverseAxisValue_12; }
	inline void set_inverseAxisValue_12(bool value)
	{
		___inverseAxisValue_12 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody_13() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___cachedRigidBody_13)); }
	inline Rigidbody_t3916780224 * get_cachedRigidBody_13() const { return ___cachedRigidBody_13; }
	inline Rigidbody_t3916780224 ** get_address_of_cachedRigidBody_13() { return &___cachedRigidBody_13; }
	inline void set_cachedRigidBody_13(Rigidbody_t3916780224 * value)
	{
		___cachedRigidBody_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody_13), value);
	}

	inline static int32_t get_offset_of_isKinematic_14() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isKinematic_14)); }
	inline bool get_isKinematic_14() const { return ___isKinematic_14; }
	inline bool* get_address_of_isKinematic_14() { return &___isKinematic_14; }
	inline void set_isKinematic_14(bool value)
	{
		___isKinematic_14 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody2D_15() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___cachedRigidBody2D_15)); }
	inline Rigidbody2D_t939494601 * get_cachedRigidBody2D_15() const { return ___cachedRigidBody2D_15; }
	inline Rigidbody2D_t939494601 ** get_address_of_cachedRigidBody2D_15() { return &___cachedRigidBody2D_15; }
	inline void set_cachedRigidBody2D_15(Rigidbody2D_t939494601 * value)
	{
		___cachedRigidBody2D_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody2D_15), value);
	}

	inline static int32_t get_offset_of_isKinematic2D_16() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isKinematic2D_16)); }
	inline bool get_isKinematic2D_16() const { return ___isKinematic2D_16; }
	inline bool* get_address_of_isKinematic2D_16() { return &___isKinematic2D_16; }
	inline void set_isKinematic2D_16(bool value)
	{
		___isKinematic2D_16 = value;
	}

	inline static int32_t get_offset_of_realType_17() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___realType_17)); }
	inline int32_t get_realType_17() const { return ___realType_17; }
	inline int32_t* get_address_of_realType_17() { return &___realType_17; }
	inline void set_realType_17(int32_t value)
	{
		___realType_17 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_18() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___fingerIndex_18)); }
	inline int32_t get_fingerIndex_18() const { return ___fingerIndex_18; }
	inline int32_t* get_address_of_fingerIndex_18() { return &___fingerIndex_18; }
	inline void set_fingerIndex_18(int32_t value)
	{
		___fingerIndex_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKBASE_T718481069_H
#ifndef ETCSETDIRECTACTIONTRANSFORM_T826958744_H
#define ETCSETDIRECTACTIONTRANSFORM_T826958744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCSetDirectActionTransform
struct  ETCSetDirectActionTransform_t826958744  : public MonoBehaviour_t3962482529
{
public:
	// System.String ETCSetDirectActionTransform::axisName1
	String_t* ___axisName1_2;
	// System.String ETCSetDirectActionTransform::axisName2
	String_t* ___axisName2_3;

public:
	inline static int32_t get_offset_of_axisName1_2() { return static_cast<int32_t>(offsetof(ETCSetDirectActionTransform_t826958744, ___axisName1_2)); }
	inline String_t* get_axisName1_2() const { return ___axisName1_2; }
	inline String_t** get_address_of_axisName1_2() { return &___axisName1_2; }
	inline void set_axisName1_2(String_t* value)
	{
		___axisName1_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisName1_2), value);
	}

	inline static int32_t get_offset_of_axisName2_3() { return static_cast<int32_t>(offsetof(ETCSetDirectActionTransform_t826958744, ___axisName2_3)); }
	inline String_t* get_axisName2_3() const { return ___axisName2_3; }
	inline String_t** get_address_of_axisName2_3() { return &___axisName2_3; }
	inline void set_axisName2_3(String_t* value)
	{
		___axisName2_3 = value;
		Il2CppCodeGenWriteBarrier((&___axisName2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCSETDIRECTACTIONTRANSFORM_T826958744_H
#ifndef EASYTOUCH_T1229686981_H
#define EASYTOUCH_T1229686981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch
struct  EasyTouch_t1229686981  : public MonoBehaviour_t3962482529
{
public:
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch::_currentGesture
	Gesture_t3351707245 * ____currentGesture_42;
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.Gesture> HedgehogTeam.EasyTouch.EasyTouch::_currentGestures
	List_1_t528814691 * ____currentGestures_43;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable
	bool ___enable_44;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableRemote
	bool ___enableRemote_45;
	// HedgehogTeam.EasyTouch.EasyTouch/GesturePriority HedgehogTeam.EasyTouch.EasyTouch::gesturePriority
	int32_t ___gesturePriority_46;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::StationaryTolerance
	float ___StationaryTolerance_47;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::longTapTime
	float ___longTapTime_48;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::swipeTolerance
	float ___swipeTolerance_49;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::minPinchLength
	float ___minPinchLength_50;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::minTwistAngle
	float ___minTwistAngle_51;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapDetection HedgehogTeam.EasyTouch.EasyTouch::doubleTapDetection
	int32_t ___doubleTapDetection_52;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::doubleTapTime
	float ___doubleTapTime_53;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::alwaysSendSwipe
	bool ___alwaysSendSwipe_54;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2FingersGesture
	bool ___enable2FingersGesture_55;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableTwist
	bool ___enableTwist_56;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enablePinch
	bool ___enablePinch_57;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2FingersSwipe
	bool ___enable2FingersSwipe_58;
	// HedgehogTeam.EasyTouch.EasyTouch/TwoFingerPickMethod HedgehogTeam.EasyTouch.EasyTouch::twoFingerPickMethod
	int32_t ___twoFingerPickMethod_59;
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.ECamera> HedgehogTeam.EasyTouch.EasyTouch::touchCameras
	List_1_t1189928584 * ___touchCameras_60;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoSelect
	bool ___autoSelect_61;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::pickableLayers3D
	LayerMask_t3493934918  ___pickableLayers3D_62;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2D
	bool ___enable2D_63;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::pickableLayers2D
	LayerMask_t3493934918  ___pickableLayers2D_64;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoUpdatePickedObject
	bool ___autoUpdatePickedObject_65;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::allowUIDetection
	bool ___allowUIDetection_66;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableUIMode
	bool ___enableUIMode_67;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoUpdatePickedUI
	bool ___autoUpdatePickedUI_68;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enabledNGuiMode
	bool ___enabledNGuiMode_69;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::nGUILayers
	LayerMask_t3493934918  ___nGUILayers_70;
	// System.Collections.Generic.List`1<UnityEngine.Camera> HedgehogTeam.EasyTouch.EasyTouch::nGUICameras
	List_1_t1334261317 * ___nGUICameras_71;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableSimulation
	bool ___enableSimulation_72;
	// UnityEngine.KeyCode HedgehogTeam.EasyTouch.EasyTouch::twistKey
	int32_t ___twistKey_73;
	// UnityEngine.KeyCode HedgehogTeam.EasyTouch.EasyTouch::swipeKey
	int32_t ___swipeKey_74;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showGuiInspector
	bool ___showGuiInspector_75;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showSelectInspector
	bool ___showSelectInspector_76;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showGestureInspector
	bool ___showGestureInspector_77;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showTwoFingerInspector
	bool ___showTwoFingerInspector_78;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showSecondFingerInspector
	bool ___showSecondFingerInspector_79;
	// HedgehogTeam.EasyTouch.EasyTouchInput HedgehogTeam.EasyTouch.EasyTouch::input
	EasyTouchInput_t1703601697 * ___input_80;
	// HedgehogTeam.EasyTouch.Finger[] HedgehogTeam.EasyTouch.EasyTouch::fingers
	FingerU5BU5D_t1456390799* ___fingers_81;
	// UnityEngine.Texture HedgehogTeam.EasyTouch.EasyTouch::secondFingerTexture
	Texture_t3661962703 * ___secondFingerTexture_82;
	// HedgehogTeam.EasyTouch.TwoFingerGesture HedgehogTeam.EasyTouch.EasyTouch::twoFinger
	TwoFingerGesture_t1936485437 * ___twoFinger_83;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch::oldTouchCount
	int32_t ___oldTouchCount_84;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap[] HedgehogTeam.EasyTouch.EasyTouch::singleDoubleTap
	DoubleTapU5BU5D_t476294904* ___singleDoubleTap_85;
	// HedgehogTeam.EasyTouch.Finger[] HedgehogTeam.EasyTouch.EasyTouch::tmpArray
	FingerU5BU5D_t1456390799* ___tmpArray_86;
	// HedgehogTeam.EasyTouch.EasyTouch/PickedObject HedgehogTeam.EasyTouch.EasyTouch::pickedObject
	PickedObject_t3768690803 * ___pickedObject_87;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HedgehogTeam.EasyTouch.EasyTouch::uiRaycastResultCache
	List_1_t537414295 * ___uiRaycastResultCache_88;
	// UnityEngine.EventSystems.PointerEventData HedgehogTeam.EasyTouch.EasyTouch::uiPointerEventData
	PointerEventData_t3807901092 * ___uiPointerEventData_89;
	// UnityEngine.EventSystems.EventSystem HedgehogTeam.EasyTouch.EasyTouch::uiEventSystem
	EventSystem_t1003666588 * ___uiEventSystem_90;

public:
	inline static int32_t get_offset_of__currentGesture_42() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ____currentGesture_42)); }
	inline Gesture_t3351707245 * get__currentGesture_42() const { return ____currentGesture_42; }
	inline Gesture_t3351707245 ** get_address_of__currentGesture_42() { return &____currentGesture_42; }
	inline void set__currentGesture_42(Gesture_t3351707245 * value)
	{
		____currentGesture_42 = value;
		Il2CppCodeGenWriteBarrier((&____currentGesture_42), value);
	}

	inline static int32_t get_offset_of__currentGestures_43() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ____currentGestures_43)); }
	inline List_1_t528814691 * get__currentGestures_43() const { return ____currentGestures_43; }
	inline List_1_t528814691 ** get_address_of__currentGestures_43() { return &____currentGestures_43; }
	inline void set__currentGestures_43(List_1_t528814691 * value)
	{
		____currentGestures_43 = value;
		Il2CppCodeGenWriteBarrier((&____currentGestures_43), value);
	}

	inline static int32_t get_offset_of_enable_44() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enable_44)); }
	inline bool get_enable_44() const { return ___enable_44; }
	inline bool* get_address_of_enable_44() { return &___enable_44; }
	inline void set_enable_44(bool value)
	{
		___enable_44 = value;
	}

	inline static int32_t get_offset_of_enableRemote_45() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enableRemote_45)); }
	inline bool get_enableRemote_45() const { return ___enableRemote_45; }
	inline bool* get_address_of_enableRemote_45() { return &___enableRemote_45; }
	inline void set_enableRemote_45(bool value)
	{
		___enableRemote_45 = value;
	}

	inline static int32_t get_offset_of_gesturePriority_46() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___gesturePriority_46)); }
	inline int32_t get_gesturePriority_46() const { return ___gesturePriority_46; }
	inline int32_t* get_address_of_gesturePriority_46() { return &___gesturePriority_46; }
	inline void set_gesturePriority_46(int32_t value)
	{
		___gesturePriority_46 = value;
	}

	inline static int32_t get_offset_of_StationaryTolerance_47() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___StationaryTolerance_47)); }
	inline float get_StationaryTolerance_47() const { return ___StationaryTolerance_47; }
	inline float* get_address_of_StationaryTolerance_47() { return &___StationaryTolerance_47; }
	inline void set_StationaryTolerance_47(float value)
	{
		___StationaryTolerance_47 = value;
	}

	inline static int32_t get_offset_of_longTapTime_48() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___longTapTime_48)); }
	inline float get_longTapTime_48() const { return ___longTapTime_48; }
	inline float* get_address_of_longTapTime_48() { return &___longTapTime_48; }
	inline void set_longTapTime_48(float value)
	{
		___longTapTime_48 = value;
	}

	inline static int32_t get_offset_of_swipeTolerance_49() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___swipeTolerance_49)); }
	inline float get_swipeTolerance_49() const { return ___swipeTolerance_49; }
	inline float* get_address_of_swipeTolerance_49() { return &___swipeTolerance_49; }
	inline void set_swipeTolerance_49(float value)
	{
		___swipeTolerance_49 = value;
	}

	inline static int32_t get_offset_of_minPinchLength_50() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___minPinchLength_50)); }
	inline float get_minPinchLength_50() const { return ___minPinchLength_50; }
	inline float* get_address_of_minPinchLength_50() { return &___minPinchLength_50; }
	inline void set_minPinchLength_50(float value)
	{
		___minPinchLength_50 = value;
	}

	inline static int32_t get_offset_of_minTwistAngle_51() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___minTwistAngle_51)); }
	inline float get_minTwistAngle_51() const { return ___minTwistAngle_51; }
	inline float* get_address_of_minTwistAngle_51() { return &___minTwistAngle_51; }
	inline void set_minTwistAngle_51(float value)
	{
		___minTwistAngle_51 = value;
	}

	inline static int32_t get_offset_of_doubleTapDetection_52() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___doubleTapDetection_52)); }
	inline int32_t get_doubleTapDetection_52() const { return ___doubleTapDetection_52; }
	inline int32_t* get_address_of_doubleTapDetection_52() { return &___doubleTapDetection_52; }
	inline void set_doubleTapDetection_52(int32_t value)
	{
		___doubleTapDetection_52 = value;
	}

	inline static int32_t get_offset_of_doubleTapTime_53() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___doubleTapTime_53)); }
	inline float get_doubleTapTime_53() const { return ___doubleTapTime_53; }
	inline float* get_address_of_doubleTapTime_53() { return &___doubleTapTime_53; }
	inline void set_doubleTapTime_53(float value)
	{
		___doubleTapTime_53 = value;
	}

	inline static int32_t get_offset_of_alwaysSendSwipe_54() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___alwaysSendSwipe_54)); }
	inline bool get_alwaysSendSwipe_54() const { return ___alwaysSendSwipe_54; }
	inline bool* get_address_of_alwaysSendSwipe_54() { return &___alwaysSendSwipe_54; }
	inline void set_alwaysSendSwipe_54(bool value)
	{
		___alwaysSendSwipe_54 = value;
	}

	inline static int32_t get_offset_of_enable2FingersGesture_55() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enable2FingersGesture_55)); }
	inline bool get_enable2FingersGesture_55() const { return ___enable2FingersGesture_55; }
	inline bool* get_address_of_enable2FingersGesture_55() { return &___enable2FingersGesture_55; }
	inline void set_enable2FingersGesture_55(bool value)
	{
		___enable2FingersGesture_55 = value;
	}

	inline static int32_t get_offset_of_enableTwist_56() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enableTwist_56)); }
	inline bool get_enableTwist_56() const { return ___enableTwist_56; }
	inline bool* get_address_of_enableTwist_56() { return &___enableTwist_56; }
	inline void set_enableTwist_56(bool value)
	{
		___enableTwist_56 = value;
	}

	inline static int32_t get_offset_of_enablePinch_57() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enablePinch_57)); }
	inline bool get_enablePinch_57() const { return ___enablePinch_57; }
	inline bool* get_address_of_enablePinch_57() { return &___enablePinch_57; }
	inline void set_enablePinch_57(bool value)
	{
		___enablePinch_57 = value;
	}

	inline static int32_t get_offset_of_enable2FingersSwipe_58() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enable2FingersSwipe_58)); }
	inline bool get_enable2FingersSwipe_58() const { return ___enable2FingersSwipe_58; }
	inline bool* get_address_of_enable2FingersSwipe_58() { return &___enable2FingersSwipe_58; }
	inline void set_enable2FingersSwipe_58(bool value)
	{
		___enable2FingersSwipe_58 = value;
	}

	inline static int32_t get_offset_of_twoFingerPickMethod_59() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___twoFingerPickMethod_59)); }
	inline int32_t get_twoFingerPickMethod_59() const { return ___twoFingerPickMethod_59; }
	inline int32_t* get_address_of_twoFingerPickMethod_59() { return &___twoFingerPickMethod_59; }
	inline void set_twoFingerPickMethod_59(int32_t value)
	{
		___twoFingerPickMethod_59 = value;
	}

	inline static int32_t get_offset_of_touchCameras_60() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___touchCameras_60)); }
	inline List_1_t1189928584 * get_touchCameras_60() const { return ___touchCameras_60; }
	inline List_1_t1189928584 ** get_address_of_touchCameras_60() { return &___touchCameras_60; }
	inline void set_touchCameras_60(List_1_t1189928584 * value)
	{
		___touchCameras_60 = value;
		Il2CppCodeGenWriteBarrier((&___touchCameras_60), value);
	}

	inline static int32_t get_offset_of_autoSelect_61() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___autoSelect_61)); }
	inline bool get_autoSelect_61() const { return ___autoSelect_61; }
	inline bool* get_address_of_autoSelect_61() { return &___autoSelect_61; }
	inline void set_autoSelect_61(bool value)
	{
		___autoSelect_61 = value;
	}

	inline static int32_t get_offset_of_pickableLayers3D_62() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___pickableLayers3D_62)); }
	inline LayerMask_t3493934918  get_pickableLayers3D_62() const { return ___pickableLayers3D_62; }
	inline LayerMask_t3493934918 * get_address_of_pickableLayers3D_62() { return &___pickableLayers3D_62; }
	inline void set_pickableLayers3D_62(LayerMask_t3493934918  value)
	{
		___pickableLayers3D_62 = value;
	}

	inline static int32_t get_offset_of_enable2D_63() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enable2D_63)); }
	inline bool get_enable2D_63() const { return ___enable2D_63; }
	inline bool* get_address_of_enable2D_63() { return &___enable2D_63; }
	inline void set_enable2D_63(bool value)
	{
		___enable2D_63 = value;
	}

	inline static int32_t get_offset_of_pickableLayers2D_64() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___pickableLayers2D_64)); }
	inline LayerMask_t3493934918  get_pickableLayers2D_64() const { return ___pickableLayers2D_64; }
	inline LayerMask_t3493934918 * get_address_of_pickableLayers2D_64() { return &___pickableLayers2D_64; }
	inline void set_pickableLayers2D_64(LayerMask_t3493934918  value)
	{
		___pickableLayers2D_64 = value;
	}

	inline static int32_t get_offset_of_autoUpdatePickedObject_65() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___autoUpdatePickedObject_65)); }
	inline bool get_autoUpdatePickedObject_65() const { return ___autoUpdatePickedObject_65; }
	inline bool* get_address_of_autoUpdatePickedObject_65() { return &___autoUpdatePickedObject_65; }
	inline void set_autoUpdatePickedObject_65(bool value)
	{
		___autoUpdatePickedObject_65 = value;
	}

	inline static int32_t get_offset_of_allowUIDetection_66() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___allowUIDetection_66)); }
	inline bool get_allowUIDetection_66() const { return ___allowUIDetection_66; }
	inline bool* get_address_of_allowUIDetection_66() { return &___allowUIDetection_66; }
	inline void set_allowUIDetection_66(bool value)
	{
		___allowUIDetection_66 = value;
	}

	inline static int32_t get_offset_of_enableUIMode_67() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enableUIMode_67)); }
	inline bool get_enableUIMode_67() const { return ___enableUIMode_67; }
	inline bool* get_address_of_enableUIMode_67() { return &___enableUIMode_67; }
	inline void set_enableUIMode_67(bool value)
	{
		___enableUIMode_67 = value;
	}

	inline static int32_t get_offset_of_autoUpdatePickedUI_68() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___autoUpdatePickedUI_68)); }
	inline bool get_autoUpdatePickedUI_68() const { return ___autoUpdatePickedUI_68; }
	inline bool* get_address_of_autoUpdatePickedUI_68() { return &___autoUpdatePickedUI_68; }
	inline void set_autoUpdatePickedUI_68(bool value)
	{
		___autoUpdatePickedUI_68 = value;
	}

	inline static int32_t get_offset_of_enabledNGuiMode_69() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enabledNGuiMode_69)); }
	inline bool get_enabledNGuiMode_69() const { return ___enabledNGuiMode_69; }
	inline bool* get_address_of_enabledNGuiMode_69() { return &___enabledNGuiMode_69; }
	inline void set_enabledNGuiMode_69(bool value)
	{
		___enabledNGuiMode_69 = value;
	}

	inline static int32_t get_offset_of_nGUILayers_70() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___nGUILayers_70)); }
	inline LayerMask_t3493934918  get_nGUILayers_70() const { return ___nGUILayers_70; }
	inline LayerMask_t3493934918 * get_address_of_nGUILayers_70() { return &___nGUILayers_70; }
	inline void set_nGUILayers_70(LayerMask_t3493934918  value)
	{
		___nGUILayers_70 = value;
	}

	inline static int32_t get_offset_of_nGUICameras_71() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___nGUICameras_71)); }
	inline List_1_t1334261317 * get_nGUICameras_71() const { return ___nGUICameras_71; }
	inline List_1_t1334261317 ** get_address_of_nGUICameras_71() { return &___nGUICameras_71; }
	inline void set_nGUICameras_71(List_1_t1334261317 * value)
	{
		___nGUICameras_71 = value;
		Il2CppCodeGenWriteBarrier((&___nGUICameras_71), value);
	}

	inline static int32_t get_offset_of_enableSimulation_72() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___enableSimulation_72)); }
	inline bool get_enableSimulation_72() const { return ___enableSimulation_72; }
	inline bool* get_address_of_enableSimulation_72() { return &___enableSimulation_72; }
	inline void set_enableSimulation_72(bool value)
	{
		___enableSimulation_72 = value;
	}

	inline static int32_t get_offset_of_twistKey_73() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___twistKey_73)); }
	inline int32_t get_twistKey_73() const { return ___twistKey_73; }
	inline int32_t* get_address_of_twistKey_73() { return &___twistKey_73; }
	inline void set_twistKey_73(int32_t value)
	{
		___twistKey_73 = value;
	}

	inline static int32_t get_offset_of_swipeKey_74() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___swipeKey_74)); }
	inline int32_t get_swipeKey_74() const { return ___swipeKey_74; }
	inline int32_t* get_address_of_swipeKey_74() { return &___swipeKey_74; }
	inline void set_swipeKey_74(int32_t value)
	{
		___swipeKey_74 = value;
	}

	inline static int32_t get_offset_of_showGuiInspector_75() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___showGuiInspector_75)); }
	inline bool get_showGuiInspector_75() const { return ___showGuiInspector_75; }
	inline bool* get_address_of_showGuiInspector_75() { return &___showGuiInspector_75; }
	inline void set_showGuiInspector_75(bool value)
	{
		___showGuiInspector_75 = value;
	}

	inline static int32_t get_offset_of_showSelectInspector_76() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___showSelectInspector_76)); }
	inline bool get_showSelectInspector_76() const { return ___showSelectInspector_76; }
	inline bool* get_address_of_showSelectInspector_76() { return &___showSelectInspector_76; }
	inline void set_showSelectInspector_76(bool value)
	{
		___showSelectInspector_76 = value;
	}

	inline static int32_t get_offset_of_showGestureInspector_77() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___showGestureInspector_77)); }
	inline bool get_showGestureInspector_77() const { return ___showGestureInspector_77; }
	inline bool* get_address_of_showGestureInspector_77() { return &___showGestureInspector_77; }
	inline void set_showGestureInspector_77(bool value)
	{
		___showGestureInspector_77 = value;
	}

	inline static int32_t get_offset_of_showTwoFingerInspector_78() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___showTwoFingerInspector_78)); }
	inline bool get_showTwoFingerInspector_78() const { return ___showTwoFingerInspector_78; }
	inline bool* get_address_of_showTwoFingerInspector_78() { return &___showTwoFingerInspector_78; }
	inline void set_showTwoFingerInspector_78(bool value)
	{
		___showTwoFingerInspector_78 = value;
	}

	inline static int32_t get_offset_of_showSecondFingerInspector_79() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___showSecondFingerInspector_79)); }
	inline bool get_showSecondFingerInspector_79() const { return ___showSecondFingerInspector_79; }
	inline bool* get_address_of_showSecondFingerInspector_79() { return &___showSecondFingerInspector_79; }
	inline void set_showSecondFingerInspector_79(bool value)
	{
		___showSecondFingerInspector_79 = value;
	}

	inline static int32_t get_offset_of_input_80() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___input_80)); }
	inline EasyTouchInput_t1703601697 * get_input_80() const { return ___input_80; }
	inline EasyTouchInput_t1703601697 ** get_address_of_input_80() { return &___input_80; }
	inline void set_input_80(EasyTouchInput_t1703601697 * value)
	{
		___input_80 = value;
		Il2CppCodeGenWriteBarrier((&___input_80), value);
	}

	inline static int32_t get_offset_of_fingers_81() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___fingers_81)); }
	inline FingerU5BU5D_t1456390799* get_fingers_81() const { return ___fingers_81; }
	inline FingerU5BU5D_t1456390799** get_address_of_fingers_81() { return &___fingers_81; }
	inline void set_fingers_81(FingerU5BU5D_t1456390799* value)
	{
		___fingers_81 = value;
		Il2CppCodeGenWriteBarrier((&___fingers_81), value);
	}

	inline static int32_t get_offset_of_secondFingerTexture_82() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___secondFingerTexture_82)); }
	inline Texture_t3661962703 * get_secondFingerTexture_82() const { return ___secondFingerTexture_82; }
	inline Texture_t3661962703 ** get_address_of_secondFingerTexture_82() { return &___secondFingerTexture_82; }
	inline void set_secondFingerTexture_82(Texture_t3661962703 * value)
	{
		___secondFingerTexture_82 = value;
		Il2CppCodeGenWriteBarrier((&___secondFingerTexture_82), value);
	}

	inline static int32_t get_offset_of_twoFinger_83() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___twoFinger_83)); }
	inline TwoFingerGesture_t1936485437 * get_twoFinger_83() const { return ___twoFinger_83; }
	inline TwoFingerGesture_t1936485437 ** get_address_of_twoFinger_83() { return &___twoFinger_83; }
	inline void set_twoFinger_83(TwoFingerGesture_t1936485437 * value)
	{
		___twoFinger_83 = value;
		Il2CppCodeGenWriteBarrier((&___twoFinger_83), value);
	}

	inline static int32_t get_offset_of_oldTouchCount_84() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___oldTouchCount_84)); }
	inline int32_t get_oldTouchCount_84() const { return ___oldTouchCount_84; }
	inline int32_t* get_address_of_oldTouchCount_84() { return &___oldTouchCount_84; }
	inline void set_oldTouchCount_84(int32_t value)
	{
		___oldTouchCount_84 = value;
	}

	inline static int32_t get_offset_of_singleDoubleTap_85() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___singleDoubleTap_85)); }
	inline DoubleTapU5BU5D_t476294904* get_singleDoubleTap_85() const { return ___singleDoubleTap_85; }
	inline DoubleTapU5BU5D_t476294904** get_address_of_singleDoubleTap_85() { return &___singleDoubleTap_85; }
	inline void set_singleDoubleTap_85(DoubleTapU5BU5D_t476294904* value)
	{
		___singleDoubleTap_85 = value;
		Il2CppCodeGenWriteBarrier((&___singleDoubleTap_85), value);
	}

	inline static int32_t get_offset_of_tmpArray_86() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___tmpArray_86)); }
	inline FingerU5BU5D_t1456390799* get_tmpArray_86() const { return ___tmpArray_86; }
	inline FingerU5BU5D_t1456390799** get_address_of_tmpArray_86() { return &___tmpArray_86; }
	inline void set_tmpArray_86(FingerU5BU5D_t1456390799* value)
	{
		___tmpArray_86 = value;
		Il2CppCodeGenWriteBarrier((&___tmpArray_86), value);
	}

	inline static int32_t get_offset_of_pickedObject_87() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___pickedObject_87)); }
	inline PickedObject_t3768690803 * get_pickedObject_87() const { return ___pickedObject_87; }
	inline PickedObject_t3768690803 ** get_address_of_pickedObject_87() { return &___pickedObject_87; }
	inline void set_pickedObject_87(PickedObject_t3768690803 * value)
	{
		___pickedObject_87 = value;
		Il2CppCodeGenWriteBarrier((&___pickedObject_87), value);
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_88() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___uiRaycastResultCache_88)); }
	inline List_1_t537414295 * get_uiRaycastResultCache_88() const { return ___uiRaycastResultCache_88; }
	inline List_1_t537414295 ** get_address_of_uiRaycastResultCache_88() { return &___uiRaycastResultCache_88; }
	inline void set_uiRaycastResultCache_88(List_1_t537414295 * value)
	{
		___uiRaycastResultCache_88 = value;
		Il2CppCodeGenWriteBarrier((&___uiRaycastResultCache_88), value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_89() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___uiPointerEventData_89)); }
	inline PointerEventData_t3807901092 * get_uiPointerEventData_89() const { return ___uiPointerEventData_89; }
	inline PointerEventData_t3807901092 ** get_address_of_uiPointerEventData_89() { return &___uiPointerEventData_89; }
	inline void set_uiPointerEventData_89(PointerEventData_t3807901092 * value)
	{
		___uiPointerEventData_89 = value;
		Il2CppCodeGenWriteBarrier((&___uiPointerEventData_89), value);
	}

	inline static int32_t get_offset_of_uiEventSystem_90() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981, ___uiEventSystem_90)); }
	inline EventSystem_t1003666588 * get_uiEventSystem_90() const { return ___uiEventSystem_90; }
	inline EventSystem_t1003666588 ** get_address_of_uiEventSystem_90() { return &___uiEventSystem_90; }
	inline void set_uiEventSystem_90(EventSystem_t1003666588 * value)
	{
		___uiEventSystem_90 = value;
		Il2CppCodeGenWriteBarrier((&___uiEventSystem_90), value);
	}
};

struct EasyTouch_t1229686981_StaticFields
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/TouchCancelHandler HedgehogTeam.EasyTouch.EasyTouch::On_Cancel
	TouchCancelHandler_t1588360815 * ___On_Cancel_2;
	// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Cancel2Fingers
	Cancel2FingersHandler_t1819755313 * ___On_Cancel2Fingers_3;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchStart
	TouchStartHandler_t1924490190 * ___On_TouchStart_4;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchDown
	TouchDownHandler_t1840035764 * ___On_TouchDown_5;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchUp
	TouchUpHandler_t2315724297 * ___On_TouchUp_6;
	// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_SimpleTap
	SimpleTapHandler_t2789537387 * ___On_SimpleTap_7;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_DoubleTap
	DoubleTapHandler_t2307588667 * ___On_DoubleTap_8;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapStart
	LongTapStartHandler_t3842992360 * ___On_LongTapStart_9;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTap
	LongTapHandler_t1819341489 * ___On_LongTap_10;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapEnd
	LongTapEndHandler_t661558872 * ___On_LongTapEnd_11;
	// HedgehogTeam.EasyTouch.EasyTouch/DragStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragStart
	DragStartHandler_t2454096439 * ___On_DragStart_12;
	// HedgehogTeam.EasyTouch.EasyTouch/DragHandler HedgehogTeam.EasyTouch.EasyTouch::On_Drag
	DragHandler_t4081535653 * ___On_Drag_13;
	// HedgehogTeam.EasyTouch.EasyTouch/DragEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragEnd
	DragEndHandler_t378428410 * ___On_DragEnd_14;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeStart
	SwipeStartHandler_t372347372 * ___On_SwipeStart_15;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler HedgehogTeam.EasyTouch.EasyTouch::On_Swipe
	SwipeHandler_t1550509410 * ___On_Swipe_16;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeEnd
	SwipeEndHandler_t2662005585 * ___On_SwipeEnd_17;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchStart2Fingers
	TouchStart2FingersHandler_t2004139386 * ___On_TouchStart2Fingers_18;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchDown2Fingers
	TouchDown2FingersHandler_t886882400 * ___On_TouchDown2Fingers_19;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchUp2Fingers
	TouchUp2FingersHandler_t2394109167 * ___On_TouchUp2Fingers_20;
	// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SimpleTap2Fingers
	SimpleTap2FingersHandler_t1325175573 * ___On_SimpleTap2Fingers_21;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DoubleTap2Fingers
	DoubleTap2FingersHandler_t904814161 * ___On_DoubleTap2Fingers_22;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapStart2Fingers
	LongTapStart2FingersHandler_t938028626 * ___On_LongTapStart2Fingers_23;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTap2Fingers
	LongTap2FingersHandler_t3793547641 * ___On_LongTap2Fingers_24;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapEnd2Fingers
	LongTapEnd2FingersHandler_t2721930548 * ___On_LongTapEnd2Fingers_25;
	// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler HedgehogTeam.EasyTouch.EasyTouch::On_Twist
	TwistHandler_t3457605880 * ___On_Twist_26;
	// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_TwistEnd
	TwistEndHandler_t429324375 * ___On_TwistEnd_27;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler HedgehogTeam.EasyTouch.EasyTouch::On_Pinch
	PinchHandler_t2742420652 * ___On_Pinch_28;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchInHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchIn
	PinchInHandler_t1143385375 * ___On_PinchIn_29;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchOutHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchOut
	PinchOutHandler_t205849317 * ___On_PinchOut_30;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchEnd
	PinchEndHandler_t901660763 * ___On_PinchEnd_31;
	// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragStart2Fingers
	DragStart2FingersHandler_t2484576551 * ___On_DragStart2Fingers_32;
	// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Drag2Fingers
	Drag2FingersHandler_t3289523355 * ___On_Drag2Fingers_33;
	// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragEnd2Fingers
	DragEnd2FingersHandler_t4228727231 * ___On_DragEnd2Fingers_34;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeStart2Fingers
	SwipeStart2FingersHandler_t2227626112 * ___On_SwipeStart2Fingers_35;
	// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Swipe2Fingers
	Swipe2FingersHandler_t3165766933 * ___On_Swipe2Fingers_36;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeEnd2Fingers
	SwipeEnd2FingersHandler_t692742005 * ___On_SwipeEnd2Fingers_37;
	// HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler HedgehogTeam.EasyTouch.EasyTouch::On_EasyTouchIsReady
	EasyTouchIsReadyHandler_t733707701 * ___On_EasyTouchIsReady_38;
	// HedgehogTeam.EasyTouch.EasyTouch/OverUIElementHandler HedgehogTeam.EasyTouch.EasyTouch::On_OverUIElement
	OverUIElementHandler_t2097719920 * ___On_OverUIElement_39;
	// HedgehogTeam.EasyTouch.EasyTouch/UIElementTouchUpHandler HedgehogTeam.EasyTouch.EasyTouch::On_UIElementTouchUp
	UIElementTouchUpHandler_t73634063 * ___On_UIElementTouchUp_40;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch::_instance
	EasyTouch_t1229686981 * ____instance_41;
	// System.Predicate`1<HedgehogTeam.EasyTouch.ECamera> HedgehogTeam.EasyTouch.EasyTouch::<>f__am$cache0
	Predicate_1_t543147966 * ___U3CU3Ef__amU24cache0_91;

public:
	inline static int32_t get_offset_of_On_Cancel_2() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Cancel_2)); }
	inline TouchCancelHandler_t1588360815 * get_On_Cancel_2() const { return ___On_Cancel_2; }
	inline TouchCancelHandler_t1588360815 ** get_address_of_On_Cancel_2() { return &___On_Cancel_2; }
	inline void set_On_Cancel_2(TouchCancelHandler_t1588360815 * value)
	{
		___On_Cancel_2 = value;
		Il2CppCodeGenWriteBarrier((&___On_Cancel_2), value);
	}

	inline static int32_t get_offset_of_On_Cancel2Fingers_3() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Cancel2Fingers_3)); }
	inline Cancel2FingersHandler_t1819755313 * get_On_Cancel2Fingers_3() const { return ___On_Cancel2Fingers_3; }
	inline Cancel2FingersHandler_t1819755313 ** get_address_of_On_Cancel2Fingers_3() { return &___On_Cancel2Fingers_3; }
	inline void set_On_Cancel2Fingers_3(Cancel2FingersHandler_t1819755313 * value)
	{
		___On_Cancel2Fingers_3 = value;
		Il2CppCodeGenWriteBarrier((&___On_Cancel2Fingers_3), value);
	}

	inline static int32_t get_offset_of_On_TouchStart_4() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TouchStart_4)); }
	inline TouchStartHandler_t1924490190 * get_On_TouchStart_4() const { return ___On_TouchStart_4; }
	inline TouchStartHandler_t1924490190 ** get_address_of_On_TouchStart_4() { return &___On_TouchStart_4; }
	inline void set_On_TouchStart_4(TouchStartHandler_t1924490190 * value)
	{
		___On_TouchStart_4 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchStart_4), value);
	}

	inline static int32_t get_offset_of_On_TouchDown_5() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TouchDown_5)); }
	inline TouchDownHandler_t1840035764 * get_On_TouchDown_5() const { return ___On_TouchDown_5; }
	inline TouchDownHandler_t1840035764 ** get_address_of_On_TouchDown_5() { return &___On_TouchDown_5; }
	inline void set_On_TouchDown_5(TouchDownHandler_t1840035764 * value)
	{
		___On_TouchDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchDown_5), value);
	}

	inline static int32_t get_offset_of_On_TouchUp_6() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TouchUp_6)); }
	inline TouchUpHandler_t2315724297 * get_On_TouchUp_6() const { return ___On_TouchUp_6; }
	inline TouchUpHandler_t2315724297 ** get_address_of_On_TouchUp_6() { return &___On_TouchUp_6; }
	inline void set_On_TouchUp_6(TouchUpHandler_t2315724297 * value)
	{
		___On_TouchUp_6 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchUp_6), value);
	}

	inline static int32_t get_offset_of_On_SimpleTap_7() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_SimpleTap_7)); }
	inline SimpleTapHandler_t2789537387 * get_On_SimpleTap_7() const { return ___On_SimpleTap_7; }
	inline SimpleTapHandler_t2789537387 ** get_address_of_On_SimpleTap_7() { return &___On_SimpleTap_7; }
	inline void set_On_SimpleTap_7(SimpleTapHandler_t2789537387 * value)
	{
		___On_SimpleTap_7 = value;
		Il2CppCodeGenWriteBarrier((&___On_SimpleTap_7), value);
	}

	inline static int32_t get_offset_of_On_DoubleTap_8() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_DoubleTap_8)); }
	inline DoubleTapHandler_t2307588667 * get_On_DoubleTap_8() const { return ___On_DoubleTap_8; }
	inline DoubleTapHandler_t2307588667 ** get_address_of_On_DoubleTap_8() { return &___On_DoubleTap_8; }
	inline void set_On_DoubleTap_8(DoubleTapHandler_t2307588667 * value)
	{
		___On_DoubleTap_8 = value;
		Il2CppCodeGenWriteBarrier((&___On_DoubleTap_8), value);
	}

	inline static int32_t get_offset_of_On_LongTapStart_9() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_LongTapStart_9)); }
	inline LongTapStartHandler_t3842992360 * get_On_LongTapStart_9() const { return ___On_LongTapStart_9; }
	inline LongTapStartHandler_t3842992360 ** get_address_of_On_LongTapStart_9() { return &___On_LongTapStart_9; }
	inline void set_On_LongTapStart_9(LongTapStartHandler_t3842992360 * value)
	{
		___On_LongTapStart_9 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapStart_9), value);
	}

	inline static int32_t get_offset_of_On_LongTap_10() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_LongTap_10)); }
	inline LongTapHandler_t1819341489 * get_On_LongTap_10() const { return ___On_LongTap_10; }
	inline LongTapHandler_t1819341489 ** get_address_of_On_LongTap_10() { return &___On_LongTap_10; }
	inline void set_On_LongTap_10(LongTapHandler_t1819341489 * value)
	{
		___On_LongTap_10 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTap_10), value);
	}

	inline static int32_t get_offset_of_On_LongTapEnd_11() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_LongTapEnd_11)); }
	inline LongTapEndHandler_t661558872 * get_On_LongTapEnd_11() const { return ___On_LongTapEnd_11; }
	inline LongTapEndHandler_t661558872 ** get_address_of_On_LongTapEnd_11() { return &___On_LongTapEnd_11; }
	inline void set_On_LongTapEnd_11(LongTapEndHandler_t661558872 * value)
	{
		___On_LongTapEnd_11 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapEnd_11), value);
	}

	inline static int32_t get_offset_of_On_DragStart_12() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_DragStart_12)); }
	inline DragStartHandler_t2454096439 * get_On_DragStart_12() const { return ___On_DragStart_12; }
	inline DragStartHandler_t2454096439 ** get_address_of_On_DragStart_12() { return &___On_DragStart_12; }
	inline void set_On_DragStart_12(DragStartHandler_t2454096439 * value)
	{
		___On_DragStart_12 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragStart_12), value);
	}

	inline static int32_t get_offset_of_On_Drag_13() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Drag_13)); }
	inline DragHandler_t4081535653 * get_On_Drag_13() const { return ___On_Drag_13; }
	inline DragHandler_t4081535653 ** get_address_of_On_Drag_13() { return &___On_Drag_13; }
	inline void set_On_Drag_13(DragHandler_t4081535653 * value)
	{
		___On_Drag_13 = value;
		Il2CppCodeGenWriteBarrier((&___On_Drag_13), value);
	}

	inline static int32_t get_offset_of_On_DragEnd_14() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_DragEnd_14)); }
	inline DragEndHandler_t378428410 * get_On_DragEnd_14() const { return ___On_DragEnd_14; }
	inline DragEndHandler_t378428410 ** get_address_of_On_DragEnd_14() { return &___On_DragEnd_14; }
	inline void set_On_DragEnd_14(DragEndHandler_t378428410 * value)
	{
		___On_DragEnd_14 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragEnd_14), value);
	}

	inline static int32_t get_offset_of_On_SwipeStart_15() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_SwipeStart_15)); }
	inline SwipeStartHandler_t372347372 * get_On_SwipeStart_15() const { return ___On_SwipeStart_15; }
	inline SwipeStartHandler_t372347372 ** get_address_of_On_SwipeStart_15() { return &___On_SwipeStart_15; }
	inline void set_On_SwipeStart_15(SwipeStartHandler_t372347372 * value)
	{
		___On_SwipeStart_15 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeStart_15), value);
	}

	inline static int32_t get_offset_of_On_Swipe_16() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Swipe_16)); }
	inline SwipeHandler_t1550509410 * get_On_Swipe_16() const { return ___On_Swipe_16; }
	inline SwipeHandler_t1550509410 ** get_address_of_On_Swipe_16() { return &___On_Swipe_16; }
	inline void set_On_Swipe_16(SwipeHandler_t1550509410 * value)
	{
		___On_Swipe_16 = value;
		Il2CppCodeGenWriteBarrier((&___On_Swipe_16), value);
	}

	inline static int32_t get_offset_of_On_SwipeEnd_17() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_SwipeEnd_17)); }
	inline SwipeEndHandler_t2662005585 * get_On_SwipeEnd_17() const { return ___On_SwipeEnd_17; }
	inline SwipeEndHandler_t2662005585 ** get_address_of_On_SwipeEnd_17() { return &___On_SwipeEnd_17; }
	inline void set_On_SwipeEnd_17(SwipeEndHandler_t2662005585 * value)
	{
		___On_SwipeEnd_17 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeEnd_17), value);
	}

	inline static int32_t get_offset_of_On_TouchStart2Fingers_18() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TouchStart2Fingers_18)); }
	inline TouchStart2FingersHandler_t2004139386 * get_On_TouchStart2Fingers_18() const { return ___On_TouchStart2Fingers_18; }
	inline TouchStart2FingersHandler_t2004139386 ** get_address_of_On_TouchStart2Fingers_18() { return &___On_TouchStart2Fingers_18; }
	inline void set_On_TouchStart2Fingers_18(TouchStart2FingersHandler_t2004139386 * value)
	{
		___On_TouchStart2Fingers_18 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchStart2Fingers_18), value);
	}

	inline static int32_t get_offset_of_On_TouchDown2Fingers_19() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TouchDown2Fingers_19)); }
	inline TouchDown2FingersHandler_t886882400 * get_On_TouchDown2Fingers_19() const { return ___On_TouchDown2Fingers_19; }
	inline TouchDown2FingersHandler_t886882400 ** get_address_of_On_TouchDown2Fingers_19() { return &___On_TouchDown2Fingers_19; }
	inline void set_On_TouchDown2Fingers_19(TouchDown2FingersHandler_t886882400 * value)
	{
		___On_TouchDown2Fingers_19 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchDown2Fingers_19), value);
	}

	inline static int32_t get_offset_of_On_TouchUp2Fingers_20() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TouchUp2Fingers_20)); }
	inline TouchUp2FingersHandler_t2394109167 * get_On_TouchUp2Fingers_20() const { return ___On_TouchUp2Fingers_20; }
	inline TouchUp2FingersHandler_t2394109167 ** get_address_of_On_TouchUp2Fingers_20() { return &___On_TouchUp2Fingers_20; }
	inline void set_On_TouchUp2Fingers_20(TouchUp2FingersHandler_t2394109167 * value)
	{
		___On_TouchUp2Fingers_20 = value;
		Il2CppCodeGenWriteBarrier((&___On_TouchUp2Fingers_20), value);
	}

	inline static int32_t get_offset_of_On_SimpleTap2Fingers_21() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_SimpleTap2Fingers_21)); }
	inline SimpleTap2FingersHandler_t1325175573 * get_On_SimpleTap2Fingers_21() const { return ___On_SimpleTap2Fingers_21; }
	inline SimpleTap2FingersHandler_t1325175573 ** get_address_of_On_SimpleTap2Fingers_21() { return &___On_SimpleTap2Fingers_21; }
	inline void set_On_SimpleTap2Fingers_21(SimpleTap2FingersHandler_t1325175573 * value)
	{
		___On_SimpleTap2Fingers_21 = value;
		Il2CppCodeGenWriteBarrier((&___On_SimpleTap2Fingers_21), value);
	}

	inline static int32_t get_offset_of_On_DoubleTap2Fingers_22() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_DoubleTap2Fingers_22)); }
	inline DoubleTap2FingersHandler_t904814161 * get_On_DoubleTap2Fingers_22() const { return ___On_DoubleTap2Fingers_22; }
	inline DoubleTap2FingersHandler_t904814161 ** get_address_of_On_DoubleTap2Fingers_22() { return &___On_DoubleTap2Fingers_22; }
	inline void set_On_DoubleTap2Fingers_22(DoubleTap2FingersHandler_t904814161 * value)
	{
		___On_DoubleTap2Fingers_22 = value;
		Il2CppCodeGenWriteBarrier((&___On_DoubleTap2Fingers_22), value);
	}

	inline static int32_t get_offset_of_On_LongTapStart2Fingers_23() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_LongTapStart2Fingers_23)); }
	inline LongTapStart2FingersHandler_t938028626 * get_On_LongTapStart2Fingers_23() const { return ___On_LongTapStart2Fingers_23; }
	inline LongTapStart2FingersHandler_t938028626 ** get_address_of_On_LongTapStart2Fingers_23() { return &___On_LongTapStart2Fingers_23; }
	inline void set_On_LongTapStart2Fingers_23(LongTapStart2FingersHandler_t938028626 * value)
	{
		___On_LongTapStart2Fingers_23 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapStart2Fingers_23), value);
	}

	inline static int32_t get_offset_of_On_LongTap2Fingers_24() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_LongTap2Fingers_24)); }
	inline LongTap2FingersHandler_t3793547641 * get_On_LongTap2Fingers_24() const { return ___On_LongTap2Fingers_24; }
	inline LongTap2FingersHandler_t3793547641 ** get_address_of_On_LongTap2Fingers_24() { return &___On_LongTap2Fingers_24; }
	inline void set_On_LongTap2Fingers_24(LongTap2FingersHandler_t3793547641 * value)
	{
		___On_LongTap2Fingers_24 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTap2Fingers_24), value);
	}

	inline static int32_t get_offset_of_On_LongTapEnd2Fingers_25() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_LongTapEnd2Fingers_25)); }
	inline LongTapEnd2FingersHandler_t2721930548 * get_On_LongTapEnd2Fingers_25() const { return ___On_LongTapEnd2Fingers_25; }
	inline LongTapEnd2FingersHandler_t2721930548 ** get_address_of_On_LongTapEnd2Fingers_25() { return &___On_LongTapEnd2Fingers_25; }
	inline void set_On_LongTapEnd2Fingers_25(LongTapEnd2FingersHandler_t2721930548 * value)
	{
		___On_LongTapEnd2Fingers_25 = value;
		Il2CppCodeGenWriteBarrier((&___On_LongTapEnd2Fingers_25), value);
	}

	inline static int32_t get_offset_of_On_Twist_26() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Twist_26)); }
	inline TwistHandler_t3457605880 * get_On_Twist_26() const { return ___On_Twist_26; }
	inline TwistHandler_t3457605880 ** get_address_of_On_Twist_26() { return &___On_Twist_26; }
	inline void set_On_Twist_26(TwistHandler_t3457605880 * value)
	{
		___On_Twist_26 = value;
		Il2CppCodeGenWriteBarrier((&___On_Twist_26), value);
	}

	inline static int32_t get_offset_of_On_TwistEnd_27() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_TwistEnd_27)); }
	inline TwistEndHandler_t429324375 * get_On_TwistEnd_27() const { return ___On_TwistEnd_27; }
	inline TwistEndHandler_t429324375 ** get_address_of_On_TwistEnd_27() { return &___On_TwistEnd_27; }
	inline void set_On_TwistEnd_27(TwistEndHandler_t429324375 * value)
	{
		___On_TwistEnd_27 = value;
		Il2CppCodeGenWriteBarrier((&___On_TwistEnd_27), value);
	}

	inline static int32_t get_offset_of_On_Pinch_28() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Pinch_28)); }
	inline PinchHandler_t2742420652 * get_On_Pinch_28() const { return ___On_Pinch_28; }
	inline PinchHandler_t2742420652 ** get_address_of_On_Pinch_28() { return &___On_Pinch_28; }
	inline void set_On_Pinch_28(PinchHandler_t2742420652 * value)
	{
		___On_Pinch_28 = value;
		Il2CppCodeGenWriteBarrier((&___On_Pinch_28), value);
	}

	inline static int32_t get_offset_of_On_PinchIn_29() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_PinchIn_29)); }
	inline PinchInHandler_t1143385375 * get_On_PinchIn_29() const { return ___On_PinchIn_29; }
	inline PinchInHandler_t1143385375 ** get_address_of_On_PinchIn_29() { return &___On_PinchIn_29; }
	inline void set_On_PinchIn_29(PinchInHandler_t1143385375 * value)
	{
		___On_PinchIn_29 = value;
		Il2CppCodeGenWriteBarrier((&___On_PinchIn_29), value);
	}

	inline static int32_t get_offset_of_On_PinchOut_30() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_PinchOut_30)); }
	inline PinchOutHandler_t205849317 * get_On_PinchOut_30() const { return ___On_PinchOut_30; }
	inline PinchOutHandler_t205849317 ** get_address_of_On_PinchOut_30() { return &___On_PinchOut_30; }
	inline void set_On_PinchOut_30(PinchOutHandler_t205849317 * value)
	{
		___On_PinchOut_30 = value;
		Il2CppCodeGenWriteBarrier((&___On_PinchOut_30), value);
	}

	inline static int32_t get_offset_of_On_PinchEnd_31() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_PinchEnd_31)); }
	inline PinchEndHandler_t901660763 * get_On_PinchEnd_31() const { return ___On_PinchEnd_31; }
	inline PinchEndHandler_t901660763 ** get_address_of_On_PinchEnd_31() { return &___On_PinchEnd_31; }
	inline void set_On_PinchEnd_31(PinchEndHandler_t901660763 * value)
	{
		___On_PinchEnd_31 = value;
		Il2CppCodeGenWriteBarrier((&___On_PinchEnd_31), value);
	}

	inline static int32_t get_offset_of_On_DragStart2Fingers_32() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_DragStart2Fingers_32)); }
	inline DragStart2FingersHandler_t2484576551 * get_On_DragStart2Fingers_32() const { return ___On_DragStart2Fingers_32; }
	inline DragStart2FingersHandler_t2484576551 ** get_address_of_On_DragStart2Fingers_32() { return &___On_DragStart2Fingers_32; }
	inline void set_On_DragStart2Fingers_32(DragStart2FingersHandler_t2484576551 * value)
	{
		___On_DragStart2Fingers_32 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragStart2Fingers_32), value);
	}

	inline static int32_t get_offset_of_On_Drag2Fingers_33() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Drag2Fingers_33)); }
	inline Drag2FingersHandler_t3289523355 * get_On_Drag2Fingers_33() const { return ___On_Drag2Fingers_33; }
	inline Drag2FingersHandler_t3289523355 ** get_address_of_On_Drag2Fingers_33() { return &___On_Drag2Fingers_33; }
	inline void set_On_Drag2Fingers_33(Drag2FingersHandler_t3289523355 * value)
	{
		___On_Drag2Fingers_33 = value;
		Il2CppCodeGenWriteBarrier((&___On_Drag2Fingers_33), value);
	}

	inline static int32_t get_offset_of_On_DragEnd2Fingers_34() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_DragEnd2Fingers_34)); }
	inline DragEnd2FingersHandler_t4228727231 * get_On_DragEnd2Fingers_34() const { return ___On_DragEnd2Fingers_34; }
	inline DragEnd2FingersHandler_t4228727231 ** get_address_of_On_DragEnd2Fingers_34() { return &___On_DragEnd2Fingers_34; }
	inline void set_On_DragEnd2Fingers_34(DragEnd2FingersHandler_t4228727231 * value)
	{
		___On_DragEnd2Fingers_34 = value;
		Il2CppCodeGenWriteBarrier((&___On_DragEnd2Fingers_34), value);
	}

	inline static int32_t get_offset_of_On_SwipeStart2Fingers_35() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_SwipeStart2Fingers_35)); }
	inline SwipeStart2FingersHandler_t2227626112 * get_On_SwipeStart2Fingers_35() const { return ___On_SwipeStart2Fingers_35; }
	inline SwipeStart2FingersHandler_t2227626112 ** get_address_of_On_SwipeStart2Fingers_35() { return &___On_SwipeStart2Fingers_35; }
	inline void set_On_SwipeStart2Fingers_35(SwipeStart2FingersHandler_t2227626112 * value)
	{
		___On_SwipeStart2Fingers_35 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeStart2Fingers_35), value);
	}

	inline static int32_t get_offset_of_On_Swipe2Fingers_36() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_Swipe2Fingers_36)); }
	inline Swipe2FingersHandler_t3165766933 * get_On_Swipe2Fingers_36() const { return ___On_Swipe2Fingers_36; }
	inline Swipe2FingersHandler_t3165766933 ** get_address_of_On_Swipe2Fingers_36() { return &___On_Swipe2Fingers_36; }
	inline void set_On_Swipe2Fingers_36(Swipe2FingersHandler_t3165766933 * value)
	{
		___On_Swipe2Fingers_36 = value;
		Il2CppCodeGenWriteBarrier((&___On_Swipe2Fingers_36), value);
	}

	inline static int32_t get_offset_of_On_SwipeEnd2Fingers_37() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_SwipeEnd2Fingers_37)); }
	inline SwipeEnd2FingersHandler_t692742005 * get_On_SwipeEnd2Fingers_37() const { return ___On_SwipeEnd2Fingers_37; }
	inline SwipeEnd2FingersHandler_t692742005 ** get_address_of_On_SwipeEnd2Fingers_37() { return &___On_SwipeEnd2Fingers_37; }
	inline void set_On_SwipeEnd2Fingers_37(SwipeEnd2FingersHandler_t692742005 * value)
	{
		___On_SwipeEnd2Fingers_37 = value;
		Il2CppCodeGenWriteBarrier((&___On_SwipeEnd2Fingers_37), value);
	}

	inline static int32_t get_offset_of_On_EasyTouchIsReady_38() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_EasyTouchIsReady_38)); }
	inline EasyTouchIsReadyHandler_t733707701 * get_On_EasyTouchIsReady_38() const { return ___On_EasyTouchIsReady_38; }
	inline EasyTouchIsReadyHandler_t733707701 ** get_address_of_On_EasyTouchIsReady_38() { return &___On_EasyTouchIsReady_38; }
	inline void set_On_EasyTouchIsReady_38(EasyTouchIsReadyHandler_t733707701 * value)
	{
		___On_EasyTouchIsReady_38 = value;
		Il2CppCodeGenWriteBarrier((&___On_EasyTouchIsReady_38), value);
	}

	inline static int32_t get_offset_of_On_OverUIElement_39() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_OverUIElement_39)); }
	inline OverUIElementHandler_t2097719920 * get_On_OverUIElement_39() const { return ___On_OverUIElement_39; }
	inline OverUIElementHandler_t2097719920 ** get_address_of_On_OverUIElement_39() { return &___On_OverUIElement_39; }
	inline void set_On_OverUIElement_39(OverUIElementHandler_t2097719920 * value)
	{
		___On_OverUIElement_39 = value;
		Il2CppCodeGenWriteBarrier((&___On_OverUIElement_39), value);
	}

	inline static int32_t get_offset_of_On_UIElementTouchUp_40() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___On_UIElementTouchUp_40)); }
	inline UIElementTouchUpHandler_t73634063 * get_On_UIElementTouchUp_40() const { return ___On_UIElementTouchUp_40; }
	inline UIElementTouchUpHandler_t73634063 ** get_address_of_On_UIElementTouchUp_40() { return &___On_UIElementTouchUp_40; }
	inline void set_On_UIElementTouchUp_40(UIElementTouchUpHandler_t73634063 * value)
	{
		___On_UIElementTouchUp_40 = value;
		Il2CppCodeGenWriteBarrier((&___On_UIElementTouchUp_40), value);
	}

	inline static int32_t get_offset_of__instance_41() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ____instance_41)); }
	inline EasyTouch_t1229686981 * get__instance_41() const { return ____instance_41; }
	inline EasyTouch_t1229686981 ** get_address_of__instance_41() { return &____instance_41; }
	inline void set__instance_41(EasyTouch_t1229686981 * value)
	{
		____instance_41 = value;
		Il2CppCodeGenWriteBarrier((&____instance_41), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_91() { return static_cast<int32_t>(offsetof(EasyTouch_t1229686981_StaticFields, ___U3CU3Ef__amU24cache0_91)); }
	inline Predicate_1_t543147966 * get_U3CU3Ef__amU24cache0_91() const { return ___U3CU3Ef__amU24cache0_91; }
	inline Predicate_1_t543147966 ** get_address_of_U3CU3Ef__amU24cache0_91() { return &___U3CU3Ef__amU24cache0_91; }
	inline void set_U3CU3Ef__amU24cache0_91(Predicate_1_t543147966 * value)
	{
		___U3CU3Ef__amU24cache0_91 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_91), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCH_T1229686981_H
#ifndef ETCAREA_T2231366651_H
#define ETCAREA_T2231366651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCArea
struct  ETCArea_t2231366651  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ETCArea::show
	bool ___show_2;

public:
	inline static int32_t get_offset_of_show_2() { return static_cast<int32_t>(offsetof(ETCArea_t2231366651, ___show_2)); }
	inline bool get_show_2() const { return ___show_2; }
	inline bool* get_address_of_show_2() { return &___show_2; }
	inline void set_show_2(bool value)
	{
		___show_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCAREA_T2231366651_H
#ifndef ETCDPAD_T3036059144_H
#define ETCDPAD_T3036059144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad
struct  ETCDPad_t3036059144  : public ETCBase_t40313246
{
public:
	// ETCDPad/OnMoveStartHandler ETCDPad::onMoveStart
	OnMoveStartHandler_t4101678161 * ___onMoveStart_48;
	// ETCDPad/OnMoveHandler ETCDPad::onMove
	OnMoveHandler_t1359337584 * ___onMove_49;
	// ETCDPad/OnMoveSpeedHandler ETCDPad::onMoveSpeed
	OnMoveSpeedHandler_t2369786618 * ___onMoveSpeed_50;
	// ETCDPad/OnMoveEndHandler ETCDPad::onMoveEnd
	OnMoveEndHandler_t1386185292 * ___onMoveEnd_51;
	// ETCDPad/OnTouchStartHandler ETCDPad::onTouchStart
	OnTouchStartHandler_t1924473215 * ___onTouchStart_52;
	// ETCDPad/OnTouchUPHandler ETCDPad::onTouchUp
	OnTouchUPHandler_t3432258596 * ___onTouchUp_53;
	// ETCDPad/OnDownUpHandler ETCDPad::OnDownUp
	OnDownUpHandler_t3788923810 * ___OnDownUp_54;
	// ETCDPad/OnDownDownHandler ETCDPad::OnDownDown
	OnDownDownHandler_t266120898 * ___OnDownDown_55;
	// ETCDPad/OnDownLeftHandler ETCDPad::OnDownLeft
	OnDownLeftHandler_t3608179835 * ___OnDownLeft_56;
	// ETCDPad/OnDownRightHandler ETCDPad::OnDownRight
	OnDownRightHandler_t491698460 * ___OnDownRight_57;
	// ETCDPad/OnDownUpHandler ETCDPad::OnPressUp
	OnDownUpHandler_t3788923810 * ___OnPressUp_58;
	// ETCDPad/OnDownDownHandler ETCDPad::OnPressDown
	OnDownDownHandler_t266120898 * ___OnPressDown_59;
	// ETCDPad/OnDownLeftHandler ETCDPad::OnPressLeft
	OnDownLeftHandler_t3608179835 * ___OnPressLeft_60;
	// ETCDPad/OnDownRightHandler ETCDPad::OnPressRight
	OnDownRightHandler_t491698460 * ___OnPressRight_61;
	// ETCAxis ETCDPad::axisX
	ETCAxis_t4105840343 * ___axisX_62;
	// ETCAxis ETCDPad::axisY
	ETCAxis_t4105840343 * ___axisY_63;
	// UnityEngine.Sprite ETCDPad::normalSprite
	Sprite_t280657092 * ___normalSprite_64;
	// UnityEngine.Color ETCDPad::normalColor
	Color_t2555686324  ___normalColor_65;
	// UnityEngine.Sprite ETCDPad::pressedSprite
	Sprite_t280657092 * ___pressedSprite_66;
	// UnityEngine.Color ETCDPad::pressedColor
	Color_t2555686324  ___pressedColor_67;
	// UnityEngine.Vector2 ETCDPad::tmpAxis
	Vector2_t2156229523  ___tmpAxis_68;
	// UnityEngine.Vector2 ETCDPad::OldTmpAxis
	Vector2_t2156229523  ___OldTmpAxis_69;
	// System.Boolean ETCDPad::isOnTouch
	bool ___isOnTouch_70;
	// UnityEngine.UI.Image ETCDPad::cachedImage
	Image_t2670269651 * ___cachedImage_71;
	// System.Single ETCDPad::buttonSizeCoef
	float ___buttonSizeCoef_72;

public:
	inline static int32_t get_offset_of_onMoveStart_48() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___onMoveStart_48)); }
	inline OnMoveStartHandler_t4101678161 * get_onMoveStart_48() const { return ___onMoveStart_48; }
	inline OnMoveStartHandler_t4101678161 ** get_address_of_onMoveStart_48() { return &___onMoveStart_48; }
	inline void set_onMoveStart_48(OnMoveStartHandler_t4101678161 * value)
	{
		___onMoveStart_48 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveStart_48), value);
	}

	inline static int32_t get_offset_of_onMove_49() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___onMove_49)); }
	inline OnMoveHandler_t1359337584 * get_onMove_49() const { return ___onMove_49; }
	inline OnMoveHandler_t1359337584 ** get_address_of_onMove_49() { return &___onMove_49; }
	inline void set_onMove_49(OnMoveHandler_t1359337584 * value)
	{
		___onMove_49 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_49), value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_50() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___onMoveSpeed_50)); }
	inline OnMoveSpeedHandler_t2369786618 * get_onMoveSpeed_50() const { return ___onMoveSpeed_50; }
	inline OnMoveSpeedHandler_t2369786618 ** get_address_of_onMoveSpeed_50() { return &___onMoveSpeed_50; }
	inline void set_onMoveSpeed_50(OnMoveSpeedHandler_t2369786618 * value)
	{
		___onMoveSpeed_50 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveSpeed_50), value);
	}

	inline static int32_t get_offset_of_onMoveEnd_51() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___onMoveEnd_51)); }
	inline OnMoveEndHandler_t1386185292 * get_onMoveEnd_51() const { return ___onMoveEnd_51; }
	inline OnMoveEndHandler_t1386185292 ** get_address_of_onMoveEnd_51() { return &___onMoveEnd_51; }
	inline void set_onMoveEnd_51(OnMoveEndHandler_t1386185292 * value)
	{
		___onMoveEnd_51 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEnd_51), value);
	}

	inline static int32_t get_offset_of_onTouchStart_52() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___onTouchStart_52)); }
	inline OnTouchStartHandler_t1924473215 * get_onTouchStart_52() const { return ___onTouchStart_52; }
	inline OnTouchStartHandler_t1924473215 ** get_address_of_onTouchStart_52() { return &___onTouchStart_52; }
	inline void set_onTouchStart_52(OnTouchStartHandler_t1924473215 * value)
	{
		___onTouchStart_52 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchStart_52), value);
	}

	inline static int32_t get_offset_of_onTouchUp_53() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___onTouchUp_53)); }
	inline OnTouchUPHandler_t3432258596 * get_onTouchUp_53() const { return ___onTouchUp_53; }
	inline OnTouchUPHandler_t3432258596 ** get_address_of_onTouchUp_53() { return &___onTouchUp_53; }
	inline void set_onTouchUp_53(OnTouchUPHandler_t3432258596 * value)
	{
		___onTouchUp_53 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUp_53), value);
	}

	inline static int32_t get_offset_of_OnDownUp_54() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnDownUp_54)); }
	inline OnDownUpHandler_t3788923810 * get_OnDownUp_54() const { return ___OnDownUp_54; }
	inline OnDownUpHandler_t3788923810 ** get_address_of_OnDownUp_54() { return &___OnDownUp_54; }
	inline void set_OnDownUp_54(OnDownUpHandler_t3788923810 * value)
	{
		___OnDownUp_54 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownUp_54), value);
	}

	inline static int32_t get_offset_of_OnDownDown_55() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnDownDown_55)); }
	inline OnDownDownHandler_t266120898 * get_OnDownDown_55() const { return ___OnDownDown_55; }
	inline OnDownDownHandler_t266120898 ** get_address_of_OnDownDown_55() { return &___OnDownDown_55; }
	inline void set_OnDownDown_55(OnDownDownHandler_t266120898 * value)
	{
		___OnDownDown_55 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownDown_55), value);
	}

	inline static int32_t get_offset_of_OnDownLeft_56() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnDownLeft_56)); }
	inline OnDownLeftHandler_t3608179835 * get_OnDownLeft_56() const { return ___OnDownLeft_56; }
	inline OnDownLeftHandler_t3608179835 ** get_address_of_OnDownLeft_56() { return &___OnDownLeft_56; }
	inline void set_OnDownLeft_56(OnDownLeftHandler_t3608179835 * value)
	{
		___OnDownLeft_56 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownLeft_56), value);
	}

	inline static int32_t get_offset_of_OnDownRight_57() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnDownRight_57)); }
	inline OnDownRightHandler_t491698460 * get_OnDownRight_57() const { return ___OnDownRight_57; }
	inline OnDownRightHandler_t491698460 ** get_address_of_OnDownRight_57() { return &___OnDownRight_57; }
	inline void set_OnDownRight_57(OnDownRightHandler_t491698460 * value)
	{
		___OnDownRight_57 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownRight_57), value);
	}

	inline static int32_t get_offset_of_OnPressUp_58() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnPressUp_58)); }
	inline OnDownUpHandler_t3788923810 * get_OnPressUp_58() const { return ___OnPressUp_58; }
	inline OnDownUpHandler_t3788923810 ** get_address_of_OnPressUp_58() { return &___OnPressUp_58; }
	inline void set_OnPressUp_58(OnDownUpHandler_t3788923810 * value)
	{
		___OnPressUp_58 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressUp_58), value);
	}

	inline static int32_t get_offset_of_OnPressDown_59() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnPressDown_59)); }
	inline OnDownDownHandler_t266120898 * get_OnPressDown_59() const { return ___OnPressDown_59; }
	inline OnDownDownHandler_t266120898 ** get_address_of_OnPressDown_59() { return &___OnPressDown_59; }
	inline void set_OnPressDown_59(OnDownDownHandler_t266120898 * value)
	{
		___OnPressDown_59 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressDown_59), value);
	}

	inline static int32_t get_offset_of_OnPressLeft_60() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnPressLeft_60)); }
	inline OnDownLeftHandler_t3608179835 * get_OnPressLeft_60() const { return ___OnPressLeft_60; }
	inline OnDownLeftHandler_t3608179835 ** get_address_of_OnPressLeft_60() { return &___OnPressLeft_60; }
	inline void set_OnPressLeft_60(OnDownLeftHandler_t3608179835 * value)
	{
		___OnPressLeft_60 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressLeft_60), value);
	}

	inline static int32_t get_offset_of_OnPressRight_61() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OnPressRight_61)); }
	inline OnDownRightHandler_t491698460 * get_OnPressRight_61() const { return ___OnPressRight_61; }
	inline OnDownRightHandler_t491698460 ** get_address_of_OnPressRight_61() { return &___OnPressRight_61; }
	inline void set_OnPressRight_61(OnDownRightHandler_t491698460 * value)
	{
		___OnPressRight_61 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressRight_61), value);
	}

	inline static int32_t get_offset_of_axisX_62() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___axisX_62)); }
	inline ETCAxis_t4105840343 * get_axisX_62() const { return ___axisX_62; }
	inline ETCAxis_t4105840343 ** get_address_of_axisX_62() { return &___axisX_62; }
	inline void set_axisX_62(ETCAxis_t4105840343 * value)
	{
		___axisX_62 = value;
		Il2CppCodeGenWriteBarrier((&___axisX_62), value);
	}

	inline static int32_t get_offset_of_axisY_63() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___axisY_63)); }
	inline ETCAxis_t4105840343 * get_axisY_63() const { return ___axisY_63; }
	inline ETCAxis_t4105840343 ** get_address_of_axisY_63() { return &___axisY_63; }
	inline void set_axisY_63(ETCAxis_t4105840343 * value)
	{
		___axisY_63 = value;
		Il2CppCodeGenWriteBarrier((&___axisY_63), value);
	}

	inline static int32_t get_offset_of_normalSprite_64() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___normalSprite_64)); }
	inline Sprite_t280657092 * get_normalSprite_64() const { return ___normalSprite_64; }
	inline Sprite_t280657092 ** get_address_of_normalSprite_64() { return &___normalSprite_64; }
	inline void set_normalSprite_64(Sprite_t280657092 * value)
	{
		___normalSprite_64 = value;
		Il2CppCodeGenWriteBarrier((&___normalSprite_64), value);
	}

	inline static int32_t get_offset_of_normalColor_65() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___normalColor_65)); }
	inline Color_t2555686324  get_normalColor_65() const { return ___normalColor_65; }
	inline Color_t2555686324 * get_address_of_normalColor_65() { return &___normalColor_65; }
	inline void set_normalColor_65(Color_t2555686324  value)
	{
		___normalColor_65 = value;
	}

	inline static int32_t get_offset_of_pressedSprite_66() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___pressedSprite_66)); }
	inline Sprite_t280657092 * get_pressedSprite_66() const { return ___pressedSprite_66; }
	inline Sprite_t280657092 ** get_address_of_pressedSprite_66() { return &___pressedSprite_66; }
	inline void set_pressedSprite_66(Sprite_t280657092 * value)
	{
		___pressedSprite_66 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_66), value);
	}

	inline static int32_t get_offset_of_pressedColor_67() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___pressedColor_67)); }
	inline Color_t2555686324  get_pressedColor_67() const { return ___pressedColor_67; }
	inline Color_t2555686324 * get_address_of_pressedColor_67() { return &___pressedColor_67; }
	inline void set_pressedColor_67(Color_t2555686324  value)
	{
		___pressedColor_67 = value;
	}

	inline static int32_t get_offset_of_tmpAxis_68() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___tmpAxis_68)); }
	inline Vector2_t2156229523  get_tmpAxis_68() const { return ___tmpAxis_68; }
	inline Vector2_t2156229523 * get_address_of_tmpAxis_68() { return &___tmpAxis_68; }
	inline void set_tmpAxis_68(Vector2_t2156229523  value)
	{
		___tmpAxis_68 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_69() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___OldTmpAxis_69)); }
	inline Vector2_t2156229523  get_OldTmpAxis_69() const { return ___OldTmpAxis_69; }
	inline Vector2_t2156229523 * get_address_of_OldTmpAxis_69() { return &___OldTmpAxis_69; }
	inline void set_OldTmpAxis_69(Vector2_t2156229523  value)
	{
		___OldTmpAxis_69 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_70() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___isOnTouch_70)); }
	inline bool get_isOnTouch_70() const { return ___isOnTouch_70; }
	inline bool* get_address_of_isOnTouch_70() { return &___isOnTouch_70; }
	inline void set_isOnTouch_70(bool value)
	{
		___isOnTouch_70 = value;
	}

	inline static int32_t get_offset_of_cachedImage_71() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___cachedImage_71)); }
	inline Image_t2670269651 * get_cachedImage_71() const { return ___cachedImage_71; }
	inline Image_t2670269651 ** get_address_of_cachedImage_71() { return &___cachedImage_71; }
	inline void set_cachedImage_71(Image_t2670269651 * value)
	{
		___cachedImage_71 = value;
		Il2CppCodeGenWriteBarrier((&___cachedImage_71), value);
	}

	inline static int32_t get_offset_of_buttonSizeCoef_72() { return static_cast<int32_t>(offsetof(ETCDPad_t3036059144, ___buttonSizeCoef_72)); }
	inline float get_buttonSizeCoef_72() const { return ___buttonSizeCoef_72; }
	inline float* get_address_of_buttonSizeCoef_72() { return &___buttonSizeCoef_72; }
	inline void set_buttonSizeCoef_72(float value)
	{
		___buttonSizeCoef_72 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCDPAD_T3036059144_H
#ifndef QUICKTAP_T2679145608_H
#define QUICKTAP_T2679145608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap
struct  QuickTap_t2679145608  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickTap/OnTap HedgehogTeam.EasyTouch.QuickTap::onTap
	OnTap_t2551170262 * ___onTap_19;
	// HedgehogTeam.EasyTouch.QuickTap/ActionTriggering HedgehogTeam.EasyTouch.QuickTap::actionTriggering
	int32_t ___actionTriggering_20;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickTap::currentGesture
	Gesture_t3351707245 * ___currentGesture_21;

public:
	inline static int32_t get_offset_of_onTap_19() { return static_cast<int32_t>(offsetof(QuickTap_t2679145608, ___onTap_19)); }
	inline OnTap_t2551170262 * get_onTap_19() const { return ___onTap_19; }
	inline OnTap_t2551170262 ** get_address_of_onTap_19() { return &___onTap_19; }
	inline void set_onTap_19(OnTap_t2551170262 * value)
	{
		___onTap_19 = value;
		Il2CppCodeGenWriteBarrier((&___onTap_19), value);
	}

	inline static int32_t get_offset_of_actionTriggering_20() { return static_cast<int32_t>(offsetof(QuickTap_t2679145608, ___actionTriggering_20)); }
	inline int32_t get_actionTriggering_20() const { return ___actionTriggering_20; }
	inline int32_t* get_address_of_actionTriggering_20() { return &___actionTriggering_20; }
	inline void set_actionTriggering_20(int32_t value)
	{
		___actionTriggering_20 = value;
	}

	inline static int32_t get_offset_of_currentGesture_21() { return static_cast<int32_t>(offsetof(QuickTap_t2679145608, ___currentGesture_21)); }
	inline Gesture_t3351707245 * get_currentGesture_21() const { return ___currentGesture_21; }
	inline Gesture_t3351707245 ** get_address_of_currentGesture_21() { return &___currentGesture_21; }
	inline void set_currentGesture_21(Gesture_t3351707245 * value)
	{
		___currentGesture_21 = value;
		Il2CppCodeGenWriteBarrier((&___currentGesture_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKTAP_T2679145608_H
#ifndef QUICKTWIST_T887897417_H
#define QUICKTWIST_T887897417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist
struct  QuickTwist_t887897417  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickTwist/OnTwistAction HedgehogTeam.EasyTouch.QuickTwist::onTwistAction
	OnTwistAction_t1764407815 * ___onTwistAction_19;
	// System.Boolean HedgehogTeam.EasyTouch.QuickTwist::isGestureOnMe
	bool ___isGestureOnMe_20;
	// HedgehogTeam.EasyTouch.QuickTwist/ActionTiggering HedgehogTeam.EasyTouch.QuickTwist::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.QuickTwist/ActionRotationDirection HedgehogTeam.EasyTouch.QuickTwist::rotationDirection
	int32_t ___rotationDirection_22;
	// System.Single HedgehogTeam.EasyTouch.QuickTwist::axisActionValue
	float ___axisActionValue_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickTwist::enableSimpleAction
	bool ___enableSimpleAction_24;

public:
	inline static int32_t get_offset_of_onTwistAction_19() { return static_cast<int32_t>(offsetof(QuickTwist_t887897417, ___onTwistAction_19)); }
	inline OnTwistAction_t1764407815 * get_onTwistAction_19() const { return ___onTwistAction_19; }
	inline OnTwistAction_t1764407815 ** get_address_of_onTwistAction_19() { return &___onTwistAction_19; }
	inline void set_onTwistAction_19(OnTwistAction_t1764407815 * value)
	{
		___onTwistAction_19 = value;
		Il2CppCodeGenWriteBarrier((&___onTwistAction_19), value);
	}

	inline static int32_t get_offset_of_isGestureOnMe_20() { return static_cast<int32_t>(offsetof(QuickTwist_t887897417, ___isGestureOnMe_20)); }
	inline bool get_isGestureOnMe_20() const { return ___isGestureOnMe_20; }
	inline bool* get_address_of_isGestureOnMe_20() { return &___isGestureOnMe_20; }
	inline void set_isGestureOnMe_20(bool value)
	{
		___isGestureOnMe_20 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickTwist_t887897417, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_rotationDirection_22() { return static_cast<int32_t>(offsetof(QuickTwist_t887897417, ___rotationDirection_22)); }
	inline int32_t get_rotationDirection_22() const { return ___rotationDirection_22; }
	inline int32_t* get_address_of_rotationDirection_22() { return &___rotationDirection_22; }
	inline void set_rotationDirection_22(int32_t value)
	{
		___rotationDirection_22 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_23() { return static_cast<int32_t>(offsetof(QuickTwist_t887897417, ___axisActionValue_23)); }
	inline float get_axisActionValue_23() const { return ___axisActionValue_23; }
	inline float* get_address_of_axisActionValue_23() { return &___axisActionValue_23; }
	inline void set_axisActionValue_23(float value)
	{
		___axisActionValue_23 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_24() { return static_cast<int32_t>(offsetof(QuickTwist_t887897417, ___enableSimpleAction_24)); }
	inline bool get_enableSimpleAction_24() const { return ___enableSimpleAction_24; }
	inline bool* get_address_of_enableSimpleAction_24() { return &___enableSimpleAction_24; }
	inline void set_enableSimpleAction_24(bool value)
	{
		___enableSimpleAction_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKTWIST_T887897417_H
#ifndef QUICKTOUCH_T459125921_H
#define QUICKTOUCH_T459125921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch
struct  QuickTouch_t459125921  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickTouch/OnTouch HedgehogTeam.EasyTouch.QuickTouch::onTouch
	OnTouch_t1220393512 * ___onTouch_19;
	// HedgehogTeam.EasyTouch.QuickTouch/OnTouchNotOverMe HedgehogTeam.EasyTouch.QuickTouch::onTouchNotOverMe
	OnTouchNotOverMe_t101807583 * ___onTouchNotOverMe_20;
	// HedgehogTeam.EasyTouch.QuickTouch/ActionTriggering HedgehogTeam.EasyTouch.QuickTouch::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickTouch::currentGesture
	Gesture_t3351707245 * ___currentGesture_22;

public:
	inline static int32_t get_offset_of_onTouch_19() { return static_cast<int32_t>(offsetof(QuickTouch_t459125921, ___onTouch_19)); }
	inline OnTouch_t1220393512 * get_onTouch_19() const { return ___onTouch_19; }
	inline OnTouch_t1220393512 ** get_address_of_onTouch_19() { return &___onTouch_19; }
	inline void set_onTouch_19(OnTouch_t1220393512 * value)
	{
		___onTouch_19 = value;
		Il2CppCodeGenWriteBarrier((&___onTouch_19), value);
	}

	inline static int32_t get_offset_of_onTouchNotOverMe_20() { return static_cast<int32_t>(offsetof(QuickTouch_t459125921, ___onTouchNotOverMe_20)); }
	inline OnTouchNotOverMe_t101807583 * get_onTouchNotOverMe_20() const { return ___onTouchNotOverMe_20; }
	inline OnTouchNotOverMe_t101807583 ** get_address_of_onTouchNotOverMe_20() { return &___onTouchNotOverMe_20; }
	inline void set_onTouchNotOverMe_20(OnTouchNotOverMe_t101807583 * value)
	{
		___onTouchNotOverMe_20 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchNotOverMe_20), value);
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickTouch_t459125921, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_currentGesture_22() { return static_cast<int32_t>(offsetof(QuickTouch_t459125921, ___currentGesture_22)); }
	inline Gesture_t3351707245 * get_currentGesture_22() const { return ___currentGesture_22; }
	inline Gesture_t3351707245 ** get_address_of_currentGesture_22() { return &___currentGesture_22; }
	inline void set_currentGesture_22(Gesture_t3351707245 * value)
	{
		___currentGesture_22 = value;
		Il2CppCodeGenWriteBarrier((&___currentGesture_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKTOUCH_T459125921_H
#ifndef ETCBUTTON_T2997628389_H
#define ETCBUTTON_T2997628389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton
struct  ETCButton_t2997628389  : public ETCBase_t40313246
{
public:
	// ETCButton/OnDownHandler ETCButton::onDown
	OnDownHandler_t2248657866 * ___onDown_48;
	// ETCButton/OnPressedHandler ETCButton::onPressed
	OnPressedHandler_t2010688751 * ___onPressed_49;
	// ETCButton/OnPressedValueandler ETCButton::onPressedValue
	OnPressedValueandler_t2312297359 * ___onPressedValue_50;
	// ETCButton/OnUPHandler ETCButton::onUp
	OnUPHandler_t1013684368 * ___onUp_51;
	// ETCAxis ETCButton::axis
	ETCAxis_t4105840343 * ___axis_52;
	// UnityEngine.Sprite ETCButton::normalSprite
	Sprite_t280657092 * ___normalSprite_53;
	// UnityEngine.Color ETCButton::normalColor
	Color_t2555686324  ___normalColor_54;
	// UnityEngine.Sprite ETCButton::pressedSprite
	Sprite_t280657092 * ___pressedSprite_55;
	// UnityEngine.Color ETCButton::pressedColor
	Color_t2555686324  ___pressedColor_56;
	// UnityEngine.UI.Image ETCButton::cachedImage
	Image_t2670269651 * ___cachedImage_57;
	// System.Boolean ETCButton::isOnPress
	bool ___isOnPress_58;
	// UnityEngine.GameObject ETCButton::previousDargObject
	GameObject_t1113636619 * ___previousDargObject_59;
	// System.Boolean ETCButton::isOnTouch
	bool ___isOnTouch_60;

public:
	inline static int32_t get_offset_of_onDown_48() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___onDown_48)); }
	inline OnDownHandler_t2248657866 * get_onDown_48() const { return ___onDown_48; }
	inline OnDownHandler_t2248657866 ** get_address_of_onDown_48() { return &___onDown_48; }
	inline void set_onDown_48(OnDownHandler_t2248657866 * value)
	{
		___onDown_48 = value;
		Il2CppCodeGenWriteBarrier((&___onDown_48), value);
	}

	inline static int32_t get_offset_of_onPressed_49() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___onPressed_49)); }
	inline OnPressedHandler_t2010688751 * get_onPressed_49() const { return ___onPressed_49; }
	inline OnPressedHandler_t2010688751 ** get_address_of_onPressed_49() { return &___onPressed_49; }
	inline void set_onPressed_49(OnPressedHandler_t2010688751 * value)
	{
		___onPressed_49 = value;
		Il2CppCodeGenWriteBarrier((&___onPressed_49), value);
	}

	inline static int32_t get_offset_of_onPressedValue_50() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___onPressedValue_50)); }
	inline OnPressedValueandler_t2312297359 * get_onPressedValue_50() const { return ___onPressedValue_50; }
	inline OnPressedValueandler_t2312297359 ** get_address_of_onPressedValue_50() { return &___onPressedValue_50; }
	inline void set_onPressedValue_50(OnPressedValueandler_t2312297359 * value)
	{
		___onPressedValue_50 = value;
		Il2CppCodeGenWriteBarrier((&___onPressedValue_50), value);
	}

	inline static int32_t get_offset_of_onUp_51() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___onUp_51)); }
	inline OnUPHandler_t1013684368 * get_onUp_51() const { return ___onUp_51; }
	inline OnUPHandler_t1013684368 ** get_address_of_onUp_51() { return &___onUp_51; }
	inline void set_onUp_51(OnUPHandler_t1013684368 * value)
	{
		___onUp_51 = value;
		Il2CppCodeGenWriteBarrier((&___onUp_51), value);
	}

	inline static int32_t get_offset_of_axis_52() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___axis_52)); }
	inline ETCAxis_t4105840343 * get_axis_52() const { return ___axis_52; }
	inline ETCAxis_t4105840343 ** get_address_of_axis_52() { return &___axis_52; }
	inline void set_axis_52(ETCAxis_t4105840343 * value)
	{
		___axis_52 = value;
		Il2CppCodeGenWriteBarrier((&___axis_52), value);
	}

	inline static int32_t get_offset_of_normalSprite_53() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___normalSprite_53)); }
	inline Sprite_t280657092 * get_normalSprite_53() const { return ___normalSprite_53; }
	inline Sprite_t280657092 ** get_address_of_normalSprite_53() { return &___normalSprite_53; }
	inline void set_normalSprite_53(Sprite_t280657092 * value)
	{
		___normalSprite_53 = value;
		Il2CppCodeGenWriteBarrier((&___normalSprite_53), value);
	}

	inline static int32_t get_offset_of_normalColor_54() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___normalColor_54)); }
	inline Color_t2555686324  get_normalColor_54() const { return ___normalColor_54; }
	inline Color_t2555686324 * get_address_of_normalColor_54() { return &___normalColor_54; }
	inline void set_normalColor_54(Color_t2555686324  value)
	{
		___normalColor_54 = value;
	}

	inline static int32_t get_offset_of_pressedSprite_55() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___pressedSprite_55)); }
	inline Sprite_t280657092 * get_pressedSprite_55() const { return ___pressedSprite_55; }
	inline Sprite_t280657092 ** get_address_of_pressedSprite_55() { return &___pressedSprite_55; }
	inline void set_pressedSprite_55(Sprite_t280657092 * value)
	{
		___pressedSprite_55 = value;
		Il2CppCodeGenWriteBarrier((&___pressedSprite_55), value);
	}

	inline static int32_t get_offset_of_pressedColor_56() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___pressedColor_56)); }
	inline Color_t2555686324  get_pressedColor_56() const { return ___pressedColor_56; }
	inline Color_t2555686324 * get_address_of_pressedColor_56() { return &___pressedColor_56; }
	inline void set_pressedColor_56(Color_t2555686324  value)
	{
		___pressedColor_56 = value;
	}

	inline static int32_t get_offset_of_cachedImage_57() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___cachedImage_57)); }
	inline Image_t2670269651 * get_cachedImage_57() const { return ___cachedImage_57; }
	inline Image_t2670269651 ** get_address_of_cachedImage_57() { return &___cachedImage_57; }
	inline void set_cachedImage_57(Image_t2670269651 * value)
	{
		___cachedImage_57 = value;
		Il2CppCodeGenWriteBarrier((&___cachedImage_57), value);
	}

	inline static int32_t get_offset_of_isOnPress_58() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___isOnPress_58)); }
	inline bool get_isOnPress_58() const { return ___isOnPress_58; }
	inline bool* get_address_of_isOnPress_58() { return &___isOnPress_58; }
	inline void set_isOnPress_58(bool value)
	{
		___isOnPress_58 = value;
	}

	inline static int32_t get_offset_of_previousDargObject_59() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___previousDargObject_59)); }
	inline GameObject_t1113636619 * get_previousDargObject_59() const { return ___previousDargObject_59; }
	inline GameObject_t1113636619 ** get_address_of_previousDargObject_59() { return &___previousDargObject_59; }
	inline void set_previousDargObject_59(GameObject_t1113636619 * value)
	{
		___previousDargObject_59 = value;
		Il2CppCodeGenWriteBarrier((&___previousDargObject_59), value);
	}

	inline static int32_t get_offset_of_isOnTouch_60() { return static_cast<int32_t>(offsetof(ETCButton_t2997628389, ___isOnTouch_60)); }
	inline bool get_isOnTouch_60() const { return ___isOnTouch_60; }
	inline bool* get_address_of_isOnTouch_60() { return &___isOnTouch_60; }
	inline void set_isOnTouch_60(bool value)
	{
		___isOnTouch_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETCBUTTON_T2997628389_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (OnSwipeAction_t1754534479), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (ActionTriggering_t481153517)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2801[3] = 
{
	ActionTriggering_t481153517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (SwipeDirection_t2936141849)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2802[14] = 
{
	SwipeDirection_t2936141849::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (QuickTap_t2679145608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[3] = 
{
	QuickTap_t2679145608::get_offset_of_onTap_19(),
	QuickTap_t2679145608::get_offset_of_actionTriggering_20(),
	QuickTap_t2679145608::get_offset_of_currentGesture_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (OnTap_t2551170262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (ActionTriggering_t1647944257)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2805[3] = 
{
	ActionTriggering_t1647944257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (QuickTouch_t459125921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[4] = 
{
	QuickTouch_t459125921::get_offset_of_onTouch_19(),
	QuickTouch_t459125921::get_offset_of_onTouchNotOverMe_20(),
	QuickTouch_t459125921::get_offset_of_actionTriggering_21(),
	QuickTouch_t459125921::get_offset_of_currentGesture_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (OnTouch_t1220393512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (OnTouchNotOverMe_t101807583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (ActionTriggering_t2131777801)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2809[4] = 
{
	ActionTriggering_t2131777801::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (QuickTwist_t887897417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[6] = 
{
	QuickTwist_t887897417::get_offset_of_onTwistAction_19(),
	QuickTwist_t887897417::get_offset_of_isGestureOnMe_20(),
	QuickTwist_t887897417::get_offset_of_actionTriggering_21(),
	QuickTwist_t887897417::get_offset_of_rotationDirection_22(),
	QuickTwist_t887897417::get_offset_of_axisActionValue_23(),
	QuickTwist_t887897417::get_offset_of_enableSimpleAction_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (OnTwistAction_t1764407815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (ActionTiggering_t3746618738)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2812[3] = 
{
	ActionTiggering_t3746618738::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (ActionRotationDirection_t3282353075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2813[4] = 
{
	ActionRotationDirection_t3282353075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (BaseFinger_t854008753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[19] = 
{
	BaseFinger_t854008753::get_offset_of_fingerIndex_0(),
	BaseFinger_t854008753::get_offset_of_touchCount_1(),
	BaseFinger_t854008753::get_offset_of_startPosition_2(),
	BaseFinger_t854008753::get_offset_of_position_3(),
	BaseFinger_t854008753::get_offset_of_deltaPosition_4(),
	BaseFinger_t854008753::get_offset_of_actionTime_5(),
	BaseFinger_t854008753::get_offset_of_fixedDeltaTime_6(),
	BaseFinger_t854008753::get_offset_of_pickedCamera_7(),
	BaseFinger_t854008753::get_offset_of_pickedObject_8(),
	BaseFinger_t854008753::get_offset_of_isGuiCamera_9(),
	BaseFinger_t854008753::get_offset_of_isOverGui_10(),
	BaseFinger_t854008753::get_offset_of_pickedUIElement_11(),
	BaseFinger_t854008753::get_offset_of_altitudeAngle_12(),
	BaseFinger_t854008753::get_offset_of_azimuthAngle_13(),
	BaseFinger_t854008753::get_offset_of_maximumPossiblePressure_14(),
	BaseFinger_t854008753::get_offset_of_pressure_15(),
	BaseFinger_t854008753::get_offset_of_radius_16(),
	BaseFinger_t854008753::get_offset_of_radiusVariance_17(),
	BaseFinger_t854008753::get_offset_of_touchType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (EasyTouch_t1229686981), -1, sizeof(EasyTouch_t1229686981_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2815[90] = 
{
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Cancel_2(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Cancel2Fingers_3(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TouchStart_4(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TouchDown_5(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TouchUp_6(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_SimpleTap_7(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_DoubleTap_8(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_LongTapStart_9(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_LongTap_10(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_LongTapEnd_11(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_DragStart_12(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Drag_13(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_DragEnd_14(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_SwipeStart_15(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Swipe_16(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_SwipeEnd_17(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TouchStart2Fingers_18(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TouchDown2Fingers_19(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TouchUp2Fingers_20(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_SimpleTap2Fingers_21(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_DoubleTap2Fingers_22(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_LongTapStart2Fingers_23(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_LongTap2Fingers_24(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_LongTapEnd2Fingers_25(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Twist_26(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_TwistEnd_27(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Pinch_28(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_PinchIn_29(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_PinchOut_30(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_PinchEnd_31(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_DragStart2Fingers_32(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Drag2Fingers_33(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_DragEnd2Fingers_34(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_SwipeStart2Fingers_35(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_Swipe2Fingers_36(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_SwipeEnd2Fingers_37(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_EasyTouchIsReady_38(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_OverUIElement_39(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_On_UIElementTouchUp_40(),
	EasyTouch_t1229686981_StaticFields::get_offset_of__instance_41(),
	EasyTouch_t1229686981::get_offset_of__currentGesture_42(),
	EasyTouch_t1229686981::get_offset_of__currentGestures_43(),
	EasyTouch_t1229686981::get_offset_of_enable_44(),
	EasyTouch_t1229686981::get_offset_of_enableRemote_45(),
	EasyTouch_t1229686981::get_offset_of_gesturePriority_46(),
	EasyTouch_t1229686981::get_offset_of_StationaryTolerance_47(),
	EasyTouch_t1229686981::get_offset_of_longTapTime_48(),
	EasyTouch_t1229686981::get_offset_of_swipeTolerance_49(),
	EasyTouch_t1229686981::get_offset_of_minPinchLength_50(),
	EasyTouch_t1229686981::get_offset_of_minTwistAngle_51(),
	EasyTouch_t1229686981::get_offset_of_doubleTapDetection_52(),
	EasyTouch_t1229686981::get_offset_of_doubleTapTime_53(),
	EasyTouch_t1229686981::get_offset_of_alwaysSendSwipe_54(),
	EasyTouch_t1229686981::get_offset_of_enable2FingersGesture_55(),
	EasyTouch_t1229686981::get_offset_of_enableTwist_56(),
	EasyTouch_t1229686981::get_offset_of_enablePinch_57(),
	EasyTouch_t1229686981::get_offset_of_enable2FingersSwipe_58(),
	EasyTouch_t1229686981::get_offset_of_twoFingerPickMethod_59(),
	EasyTouch_t1229686981::get_offset_of_touchCameras_60(),
	EasyTouch_t1229686981::get_offset_of_autoSelect_61(),
	EasyTouch_t1229686981::get_offset_of_pickableLayers3D_62(),
	EasyTouch_t1229686981::get_offset_of_enable2D_63(),
	EasyTouch_t1229686981::get_offset_of_pickableLayers2D_64(),
	EasyTouch_t1229686981::get_offset_of_autoUpdatePickedObject_65(),
	EasyTouch_t1229686981::get_offset_of_allowUIDetection_66(),
	EasyTouch_t1229686981::get_offset_of_enableUIMode_67(),
	EasyTouch_t1229686981::get_offset_of_autoUpdatePickedUI_68(),
	EasyTouch_t1229686981::get_offset_of_enabledNGuiMode_69(),
	EasyTouch_t1229686981::get_offset_of_nGUILayers_70(),
	EasyTouch_t1229686981::get_offset_of_nGUICameras_71(),
	EasyTouch_t1229686981::get_offset_of_enableSimulation_72(),
	EasyTouch_t1229686981::get_offset_of_twistKey_73(),
	EasyTouch_t1229686981::get_offset_of_swipeKey_74(),
	EasyTouch_t1229686981::get_offset_of_showGuiInspector_75(),
	EasyTouch_t1229686981::get_offset_of_showSelectInspector_76(),
	EasyTouch_t1229686981::get_offset_of_showGestureInspector_77(),
	EasyTouch_t1229686981::get_offset_of_showTwoFingerInspector_78(),
	EasyTouch_t1229686981::get_offset_of_showSecondFingerInspector_79(),
	EasyTouch_t1229686981::get_offset_of_input_80(),
	EasyTouch_t1229686981::get_offset_of_fingers_81(),
	EasyTouch_t1229686981::get_offset_of_secondFingerTexture_82(),
	EasyTouch_t1229686981::get_offset_of_twoFinger_83(),
	EasyTouch_t1229686981::get_offset_of_oldTouchCount_84(),
	EasyTouch_t1229686981::get_offset_of_singleDoubleTap_85(),
	EasyTouch_t1229686981::get_offset_of_tmpArray_86(),
	EasyTouch_t1229686981::get_offset_of_pickedObject_87(),
	EasyTouch_t1229686981::get_offset_of_uiRaycastResultCache_88(),
	EasyTouch_t1229686981::get_offset_of_uiPointerEventData_89(),
	EasyTouch_t1229686981::get_offset_of_uiEventSystem_90(),
	EasyTouch_t1229686981_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_91(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (DoubleTap_t205646101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[5] = 
{
	DoubleTap_t205646101::get_offset_of_inDoubleTap_0(),
	DoubleTap_t205646101::get_offset_of_inWait_1(),
	DoubleTap_t205646101::get_offset_of_time_2(),
	DoubleTap_t205646101::get_offset_of_count_3(),
	DoubleTap_t205646101::get_offset_of_finger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (PickedObject_t3768690803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[3] = 
{
	PickedObject_t3768690803::get_offset_of_pickedObj_0(),
	PickedObject_t3768690803::get_offset_of_pickedCamera_1(),
	PickedObject_t3768690803::get_offset_of_isGUI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (TouchCancelHandler_t1588360815), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (Cancel2FingersHandler_t1819755313), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (TouchStartHandler_t1924490190), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (TouchDownHandler_t1840035764), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (TouchUpHandler_t2315724297), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (SimpleTapHandler_t2789537387), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (DoubleTapHandler_t2307588667), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (LongTapStartHandler_t3842992360), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (LongTapHandler_t1819341489), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (LongTapEndHandler_t661558872), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (DragStartHandler_t2454096439), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (DragHandler_t4081535653), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (DragEndHandler_t378428410), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (SwipeStartHandler_t372347372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (SwipeHandler_t1550509410), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (SwipeEndHandler_t2662005585), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (TouchStart2FingersHandler_t2004139386), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (TouchDown2FingersHandler_t886882400), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (TouchUp2FingersHandler_t2394109167), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (SimpleTap2FingersHandler_t1325175573), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (DoubleTap2FingersHandler_t904814161), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (LongTapStart2FingersHandler_t938028626), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (LongTap2FingersHandler_t3793547641), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (LongTapEnd2FingersHandler_t2721930548), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (TwistHandler_t3457605880), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (TwistEndHandler_t429324375), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (PinchInHandler_t1143385375), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (PinchOutHandler_t205849317), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (PinchEndHandler_t901660763), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (PinchHandler_t2742420652), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (DragStart2FingersHandler_t2484576551), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (Drag2FingersHandler_t3289523355), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (DragEnd2FingersHandler_t4228727231), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (SwipeStart2FingersHandler_t2227626112), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (Swipe2FingersHandler_t3165766933), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (SwipeEnd2FingersHandler_t692742005), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (EasyTouchIsReadyHandler_t733707701), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (OverUIElementHandler_t2097719920), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (UIElementTouchUpHandler_t73634063), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (GesturePriority_t2833837310)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2857[3] = 
{
	GesturePriority_t2833837310::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (DoubleTapDetection_t119625748)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2858[3] = 
{
	DoubleTapDetection_t119625748::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (GestureType_t4183725547)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2859[10] = 
{
	GestureType_t4183725547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (SwipeDirection_t3548341340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[12] = 
{
	SwipeDirection_t3548341340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (TwoFingerPickMethod_t733771439)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2861[3] = 
{
	TwoFingerPickMethod_t733771439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (EvtType_t1031077730)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2862[41] = 
{
	EvtType_t1031077730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (U3CSingleOrDoubleU3Ec__Iterator0_t1496507498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[6] = 
{
	U3CSingleOrDoubleU3Ec__Iterator0_t1496507498::get_offset_of_fingerIndex_0(),
	U3CSingleOrDoubleU3Ec__Iterator0_t1496507498::get_offset_of_U3Ctime2WaitU3E__0_1(),
	U3CSingleOrDoubleU3Ec__Iterator0_t1496507498::get_offset_of_U24this_2(),
	U3CSingleOrDoubleU3Ec__Iterator0_t1496507498::get_offset_of_U24current_3(),
	U3CSingleOrDoubleU3Ec__Iterator0_t1496507498::get_offset_of_U24disposing_4(),
	U3CSingleOrDoubleU3Ec__Iterator0_t1496507498::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[4] = 
{
	U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925::get_offset_of_U24this_0(),
	U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925::get_offset_of_U24current_1(),
	U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925::get_offset_of_U24disposing_2(),
	U3CSingleOrDouble2FingersU3Ec__Iterator1_t508603925::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (U3CRaiseEventU3Ec__AnonStorey2_t963873430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[1] = 
{
	U3CRaiseEventU3Ec__AnonStorey2_t963873430::get_offset_of_gesture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (U3CRemoveCameraU3Ec__AnonStorey3_t2991675898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[1] = 
{
	U3CRemoveCameraU3Ec__AnonStorey3_t2991675898::get_offset_of_cam_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (EasyTouchInput_t1703601697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[9] = 
{
	EasyTouchInput_t1703601697::get_offset_of_oldMousePosition_0(),
	EasyTouchInput_t1703601697::get_offset_of_tapCount_1(),
	EasyTouchInput_t1703601697::get_offset_of_startActionTime_2(),
	EasyTouchInput_t1703601697::get_offset_of_fixedDeltaTime_3(),
	EasyTouchInput_t1703601697::get_offset_of_tapeTime_4(),
	EasyTouchInput_t1703601697::get_offset_of_bComplex_5(),
	EasyTouchInput_t1703601697::get_offset_of_deltaFingerPosition_6(),
	EasyTouchInput_t1703601697::get_offset_of_oldFinger2Position_7(),
	EasyTouchInput_t1703601697::get_offset_of_complexCenter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (ECamera_t4012821138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[2] = 
{
	ECamera_t4012821138::get_offset_of_camera_0(),
	ECamera_t4012821138::get_offset_of_guiCamera_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (Finger_t386077194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[6] = 
{
	Finger_t386077194::get_offset_of_startTimeAction_19(),
	Finger_t386077194::get_offset_of_oldPosition_20(),
	Finger_t386077194::get_offset_of_tapCount_21(),
	Finger_t386077194::get_offset_of_phase_22(),
	Finger_t386077194::get_offset_of_gesture_23(),
	Finger_t386077194::get_offset_of_oldSwipeType_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (Gesture_t3351707245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[7] = 
{
	Gesture_t3351707245::get_offset_of_swipe_19(),
	Gesture_t3351707245::get_offset_of_swipeLength_20(),
	Gesture_t3351707245::get_offset_of_swipeVector_21(),
	Gesture_t3351707245::get_offset_of_deltaPinch_22(),
	Gesture_t3351707245::get_offset_of_twistAngle_23(),
	Gesture_t3351707245::get_offset_of_twoFingerDistance_24(),
	Gesture_t3351707245::get_offset_of_type_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (TwoFingerGesture_t1936485437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[27] = 
{
	TwoFingerGesture_t1936485437::get_offset_of_currentGesture_0(),
	TwoFingerGesture_t1936485437::get_offset_of_oldGesture_1(),
	TwoFingerGesture_t1936485437::get_offset_of_finger0_2(),
	TwoFingerGesture_t1936485437::get_offset_of_finger1_3(),
	TwoFingerGesture_t1936485437::get_offset_of_startTimeAction_4(),
	TwoFingerGesture_t1936485437::get_offset_of_timeSinceStartAction_5(),
	TwoFingerGesture_t1936485437::get_offset_of_startPosition_6(),
	TwoFingerGesture_t1936485437::get_offset_of_position_7(),
	TwoFingerGesture_t1936485437::get_offset_of_deltaPosition_8(),
	TwoFingerGesture_t1936485437::get_offset_of_oldStartPosition_9(),
	TwoFingerGesture_t1936485437::get_offset_of_startDistance_10(),
	TwoFingerGesture_t1936485437::get_offset_of_fingerDistance_11(),
	TwoFingerGesture_t1936485437::get_offset_of_oldFingerDistance_12(),
	TwoFingerGesture_t1936485437::get_offset_of_lockPinch_13(),
	TwoFingerGesture_t1936485437::get_offset_of_lockTwist_14(),
	TwoFingerGesture_t1936485437::get_offset_of_lastPinch_15(),
	TwoFingerGesture_t1936485437::get_offset_of_lastTwistAngle_16(),
	TwoFingerGesture_t1936485437::get_offset_of_pickedObject_17(),
	TwoFingerGesture_t1936485437::get_offset_of_oldPickedObject_18(),
	TwoFingerGesture_t1936485437::get_offset_of_pickedCamera_19(),
	TwoFingerGesture_t1936485437::get_offset_of_isGuiCamera_20(),
	TwoFingerGesture_t1936485437::get_offset_of_isOverGui_21(),
	TwoFingerGesture_t1936485437::get_offset_of_pickedUIElement_22(),
	TwoFingerGesture_t1936485437::get_offset_of_dragStart_23(),
	TwoFingerGesture_t1936485437::get_offset_of_swipeStart_24(),
	TwoFingerGesture_t1936485437::get_offset_of_inSingleDoubleTaps_25(),
	TwoFingerGesture_t1936485437::get_offset_of_tapCurentTime_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (ETCSetDirectActionTransform_t826958744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[2] = 
{
	ETCSetDirectActionTransform_t826958744::get_offset_of_axisName1_2(),
	ETCSetDirectActionTransform_t826958744::get_offset_of_axisName2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (ComponentExtensions_t113312927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (ETCArea_t2231366651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[1] = 
{
	ETCArea_t2231366651::get_offset_of_show_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (AreaPreset_t2760894157)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2875[6] = 
{
	AreaPreset_t2760894157::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (ETCAxis_t4105840343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[43] = 
{
	ETCAxis_t4105840343::get_offset_of_name_0(),
	ETCAxis_t4105840343::get_offset_of_autoLinkTagPlayer_1(),
	ETCAxis_t4105840343::get_offset_of_autoTag_2(),
	ETCAxis_t4105840343::get_offset_of_player_3(),
	ETCAxis_t4105840343::get_offset_of_enable_4(),
	ETCAxis_t4105840343::get_offset_of_invertedAxis_5(),
	ETCAxis_t4105840343::get_offset_of_speed_6(),
	ETCAxis_t4105840343::get_offset_of_deadValue_7(),
	ETCAxis_t4105840343::get_offset_of_valueMethod_8(),
	ETCAxis_t4105840343::get_offset_of_curveValue_9(),
	ETCAxis_t4105840343::get_offset_of_isEnertia_10(),
	ETCAxis_t4105840343::get_offset_of_inertia_11(),
	ETCAxis_t4105840343::get_offset_of_inertiaThreshold_12(),
	ETCAxis_t4105840343::get_offset_of_isAutoStab_13(),
	ETCAxis_t4105840343::get_offset_of_autoStabThreshold_14(),
	ETCAxis_t4105840343::get_offset_of_autoStabSpeed_15(),
	ETCAxis_t4105840343::get_offset_of_startAngle_16(),
	ETCAxis_t4105840343::get_offset_of_isClampRotation_17(),
	ETCAxis_t4105840343::get_offset_of_maxAngle_18(),
	ETCAxis_t4105840343::get_offset_of_minAngle_19(),
	ETCAxis_t4105840343::get_offset_of_isValueOverTime_20(),
	ETCAxis_t4105840343::get_offset_of_overTimeStep_21(),
	ETCAxis_t4105840343::get_offset_of_maxOverTimeValue_22(),
	ETCAxis_t4105840343::get_offset_of_axisValue_23(),
	ETCAxis_t4105840343::get_offset_of_axisSpeedValue_24(),
	ETCAxis_t4105840343::get_offset_of_axisThreshold_25(),
	ETCAxis_t4105840343::get_offset_of_isLockinJump_26(),
	ETCAxis_t4105840343::get_offset_of_lastMove_27(),
	ETCAxis_t4105840343::get_offset_of_axisState_28(),
	ETCAxis_t4105840343::get_offset_of__directTransform_29(),
	ETCAxis_t4105840343::get_offset_of_directAction_30(),
	ETCAxis_t4105840343::get_offset_of_axisInfluenced_31(),
	ETCAxis_t4105840343::get_offset_of_actionOn_32(),
	ETCAxis_t4105840343::get_offset_of_directCharacterController_33(),
	ETCAxis_t4105840343::get_offset_of_directRigidBody_34(),
	ETCAxis_t4105840343::get_offset_of_gravity_35(),
	ETCAxis_t4105840343::get_offset_of_currentGravity_36(),
	ETCAxis_t4105840343::get_offset_of_isJump_37(),
	ETCAxis_t4105840343::get_offset_of_unityAxis_38(),
	ETCAxis_t4105840343::get_offset_of_showGeneralInspector_39(),
	ETCAxis_t4105840343::get_offset_of_showDirectInspector_40(),
	ETCAxis_t4105840343::get_offset_of_showInertiaInspector_41(),
	ETCAxis_t4105840343::get_offset_of_showSimulatinInspector_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (DirectAction_t2214449476)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2877[11] = 
{
	DirectAction_t2214449476::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (AxisInfluenced_t3202684285)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2878[4] = 
{
	AxisInfluenced_t3202684285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (AxisValueMethod_t3199242610)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2879[3] = 
{
	AxisValueMethod_t3199242610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (AxisState_t4223862310)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2880[13] = 
{
	AxisState_t4223862310::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (ActionOn_t2347852592)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2881[3] = 
{
	ActionOn_t2347852592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (ETCBase_t40313246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[46] = 
{
	ETCBase_t40313246::get_offset_of_cachedRectTransform_2(),
	ETCBase_t40313246::get_offset_of_cachedRootCanvas_3(),
	ETCBase_t40313246::get_offset_of_isUnregisterAtDisable_4(),
	ETCBase_t40313246::get_offset_of_visibleAtStart_5(),
	ETCBase_t40313246::get_offset_of_activatedAtStart_6(),
	ETCBase_t40313246::get_offset_of__anchor_7(),
	ETCBase_t40313246::get_offset_of__anchorOffet_8(),
	ETCBase_t40313246::get_offset_of__visible_9(),
	ETCBase_t40313246::get_offset_of__activated_10(),
	ETCBase_t40313246::get_offset_of_enableCamera_11(),
	ETCBase_t40313246::get_offset_of_cameraMode_12(),
	ETCBase_t40313246::get_offset_of_camTargetTag_13(),
	ETCBase_t40313246::get_offset_of_autoLinkTagCam_14(),
	ETCBase_t40313246::get_offset_of_autoCamTag_15(),
	ETCBase_t40313246::get_offset_of_cameraTransform_16(),
	ETCBase_t40313246::get_offset_of_cameraTargetMode_17(),
	ETCBase_t40313246::get_offset_of_enableWallDetection_18(),
	ETCBase_t40313246::get_offset_of_wallLayer_19(),
	ETCBase_t40313246::get_offset_of_cameraLookAt_20(),
	ETCBase_t40313246::get_offset_of_cameraLookAtCC_21(),
	ETCBase_t40313246::get_offset_of_followOffset_22(),
	ETCBase_t40313246::get_offset_of_followDistance_23(),
	ETCBase_t40313246::get_offset_of_followHeight_24(),
	ETCBase_t40313246::get_offset_of_followRotationDamping_25(),
	ETCBase_t40313246::get_offset_of_followHeightDamping_26(),
	ETCBase_t40313246::get_offset_of_pointId_27(),
	ETCBase_t40313246::get_offset_of_enableKeySimulation_28(),
	ETCBase_t40313246::get_offset_of_allowSimulationStandalone_29(),
	ETCBase_t40313246::get_offset_of_visibleOnStandalone_30(),
	ETCBase_t40313246::get_offset_of_dPadAxisCount_31(),
	ETCBase_t40313246::get_offset_of_useFixedUpdate_32(),
	ETCBase_t40313246::get_offset_of_uiRaycastResultCache_33(),
	ETCBase_t40313246::get_offset_of_uiPointerEventData_34(),
	ETCBase_t40313246::get_offset_of_uiEventSystem_35(),
	ETCBase_t40313246::get_offset_of_isOnDrag_36(),
	ETCBase_t40313246::get_offset_of_isSwipeIn_37(),
	ETCBase_t40313246::get_offset_of_isSwipeOut_38(),
	ETCBase_t40313246::get_offset_of_showPSInspector_39(),
	ETCBase_t40313246::get_offset_of_showSpriteInspector_40(),
	ETCBase_t40313246::get_offset_of_showEventInspector_41(),
	ETCBase_t40313246::get_offset_of_showBehaviourInspector_42(),
	ETCBase_t40313246::get_offset_of_showAxesInspector_43(),
	ETCBase_t40313246::get_offset_of_showTouchEventInspector_44(),
	ETCBase_t40313246::get_offset_of_showDownEventInspector_45(),
	ETCBase_t40313246::get_offset_of_showPressEventInspector_46(),
	ETCBase_t40313246::get_offset_of_showCameraInspector_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (ControlType_t2136651667)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2883[5] = 
{
	ControlType_t2136651667::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (RectAnchor_t2504559136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2884[11] = 
{
	RectAnchor_t2504559136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (DPadAxis_t1048722470)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2885[3] = 
{
	DPadAxis_t1048722470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (CameraMode_t364125053)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2886[3] = 
{
	CameraMode_t364125053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (CameraTargetMode_t4109937804)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2887[5] = 
{
	CameraTargetMode_t4109937804::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (U3CUpdateVirtualControlU3Ec__Iterator0_t845689544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[4] = 
{
	U3CUpdateVirtualControlU3Ec__Iterator0_t845689544::get_offset_of_U24this_0(),
	U3CUpdateVirtualControlU3Ec__Iterator0_t845689544::get_offset_of_U24current_1(),
	U3CUpdateVirtualControlU3Ec__Iterator0_t845689544::get_offset_of_U24disposing_2(),
	U3CUpdateVirtualControlU3Ec__Iterator0_t845689544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[4] = 
{
	U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524::get_offset_of_U24this_0(),
	U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524::get_offset_of_U24current_1(),
	U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524::get_offset_of_U24disposing_2(),
	U3CFixedUpdateVirtualControlU3Ec__Iterator1_t2367558524::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (ETCButton_t2997628389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[13] = 
{
	ETCButton_t2997628389::get_offset_of_onDown_48(),
	ETCButton_t2997628389::get_offset_of_onPressed_49(),
	ETCButton_t2997628389::get_offset_of_onPressedValue_50(),
	ETCButton_t2997628389::get_offset_of_onUp_51(),
	ETCButton_t2997628389::get_offset_of_axis_52(),
	ETCButton_t2997628389::get_offset_of_normalSprite_53(),
	ETCButton_t2997628389::get_offset_of_normalColor_54(),
	ETCButton_t2997628389::get_offset_of_pressedSprite_55(),
	ETCButton_t2997628389::get_offset_of_pressedColor_56(),
	ETCButton_t2997628389::get_offset_of_cachedImage_57(),
	ETCButton_t2997628389::get_offset_of_isOnPress_58(),
	ETCButton_t2997628389::get_offset_of_previousDargObject_59(),
	ETCButton_t2997628389::get_offset_of_isOnTouch_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (OnDownHandler_t2248657866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (OnPressedHandler_t2010688751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (OnPressedValueandler_t2312297359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (OnUPHandler_t1013684368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (ETCDPad_t3036059144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[25] = 
{
	ETCDPad_t3036059144::get_offset_of_onMoveStart_48(),
	ETCDPad_t3036059144::get_offset_of_onMove_49(),
	ETCDPad_t3036059144::get_offset_of_onMoveSpeed_50(),
	ETCDPad_t3036059144::get_offset_of_onMoveEnd_51(),
	ETCDPad_t3036059144::get_offset_of_onTouchStart_52(),
	ETCDPad_t3036059144::get_offset_of_onTouchUp_53(),
	ETCDPad_t3036059144::get_offset_of_OnDownUp_54(),
	ETCDPad_t3036059144::get_offset_of_OnDownDown_55(),
	ETCDPad_t3036059144::get_offset_of_OnDownLeft_56(),
	ETCDPad_t3036059144::get_offset_of_OnDownRight_57(),
	ETCDPad_t3036059144::get_offset_of_OnPressUp_58(),
	ETCDPad_t3036059144::get_offset_of_OnPressDown_59(),
	ETCDPad_t3036059144::get_offset_of_OnPressLeft_60(),
	ETCDPad_t3036059144::get_offset_of_OnPressRight_61(),
	ETCDPad_t3036059144::get_offset_of_axisX_62(),
	ETCDPad_t3036059144::get_offset_of_axisY_63(),
	ETCDPad_t3036059144::get_offset_of_normalSprite_64(),
	ETCDPad_t3036059144::get_offset_of_normalColor_65(),
	ETCDPad_t3036059144::get_offset_of_pressedSprite_66(),
	ETCDPad_t3036059144::get_offset_of_pressedColor_67(),
	ETCDPad_t3036059144::get_offset_of_tmpAxis_68(),
	ETCDPad_t3036059144::get_offset_of_OldTmpAxis_69(),
	ETCDPad_t3036059144::get_offset_of_isOnTouch_70(),
	ETCDPad_t3036059144::get_offset_of_cachedImage_71(),
	ETCDPad_t3036059144::get_offset_of_buttonSizeCoef_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (OnMoveStartHandler_t4101678161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (OnMoveHandler_t1359337584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (OnMoveSpeedHandler_t2369786618), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (OnMoveEndHandler_t1386185292), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
