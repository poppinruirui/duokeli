﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t240936516;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2748928575;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t2508470592;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t235587086;
// System.String
struct String_t;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// AccountManager
struct AccountManager_t2668481700;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Ball
struct Ball_t2206666566;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Button
struct Button_t4055032469;
// CreateNewShit/DelegateMethod_OK
struct DelegateMethod_OK_t3925705237;
// System.Collections.Generic.Dictionary`2<System.String,CPlayerManager/sPlayerInfoCommon>
struct Dictionary_2_t154955445;
// System.Collections.Generic.Dictionary`2<System.Int32,Player>
struct Dictionary_2_t2155360643;
// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>
struct Dictionary_2_t3901903956;
// System.Collections.Generic.Dictionary`2<System.String,CPlayerIns>
struct Dictionary_2_t2993447027;
// System.Collections.Generic.Dictionary`2<System.String,Player>
struct Dictionary_2_t3051903611;
// System.Collections.Generic.List`1<CPlayerManager/sOrphanInfo>
struct List_1_t4261169767;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver>
struct List_1_t2329255381;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// PhotonView
struct PhotonView_t2207721820;
// Thorn
struct Thorn_t2540645209;
// CBlock[]
struct CBlockU5BU5D_t3695288511;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Collections.Generic.List`1<CBlock>
struct List_1_t4131839888;
// System.Collections.Generic.Dictionary`2<System.UInt32,CBlock>
struct Dictionary_2_t3934032808;
// System.Collections.Generic.List`1<ListItem>
struct List_1_t1343534547;
// ListItem
struct ListItem_t4166427101;
// ComoList/DelegateMethod_OnSelected
struct DelegateMethod_OnSelected_t3195943432;
// CArrow
struct CArrow_t1475493256;
// CCosmosGunSight
struct CCosmosGunSight_t3630013301;
// UnityEngine.Rendering.SortingGroup
struct SortingGroup_t3239126932;
// MeshShape
struct MeshShape_t3358450374;
// CSkillModuleForBall
struct CSkillModuleForBall_t1995272125;
// CFrameAnimationEffect
struct CFrameAnimationEffect_t443605508;
// CFrameAnimationEffect[]
struct CFrameAnimationEffectU5BU5D_t897573229;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t3693186521;
// CEffect
struct CEffect_t3687947224;
// BallTrigger_New
struct BallTrigger_New_t3129829938;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// Player
struct Player_t3266647312;
// CPlayerIns
struct CPlayerIns_t3208190728;
// BallAction
struct BallAction_t576009403;
// System.Collections.Generic.List`1<Ball>
struct List_1_t3678741308;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<CMonster>
struct List_1_t3413545680;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Collections.Generic.List`1<Ball/EatNode>
struct List_1_t2801720860;
// CGrass
struct CGrass_t734084476;
// System.Collections.Generic.List`1<CGrass>
struct List_1_t2206159218;
// System.Collections.Generic.List`1<CSpore>
struct List_1_t1482975291;
// SpitBallTarget
struct SpitBallTarget_t3997305173;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.Dictionary`2<System.Int32,CDust>
struct Dictionary_2_t1909897157;
// CZhangYu
struct CZhangYu_t3000622058;
// CTiaoZi[]
struct CTiaoZiU5BU5D_t3180227329;
// Ball[]
struct BallU5BU5D_t2391825635;
// System.Collections.Generic.List`1<CExplodeNode>
struct List_1_t1569919409;
// System.Collections.Generic.Dictionary`2<System.Int32,Ball>
struct Dictionary_2_t1095379897;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// Thorn[]
struct ThornU5BU5D_t4134692644;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// HedgehogTeam.EasyTouch.QuickSwipe/OnSwipeAction
struct OnSwipeAction_t1754534479;
// HedgehogTeam.EasyTouch.QuickPinch/OnPinchAction
struct OnPinchAction_t3145810715;
// HedgehogTeam.EasyTouch.QuickLongTap/OnLongTap
struct OnLongTap_t243381522;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t3351707245;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchEnter
struct OnTouchEnter_t1794713516;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchOver
struct OnTouchOver_t3317551264;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchExit
struct OnTouchExit_t2180465286;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// HedgehogTeam.EasyTouch.QuickDrag/OnDragStart
struct OnDragStart_t3676143812;
// HedgehogTeam.EasyTouch.QuickDrag/OnDrag
struct OnDrag_t3336846729;
// HedgehogTeam.EasyTouch.QuickDrag/OnDragEnd
struct OnDragEnd_t1175011983;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// ComoList
struct ComoList_t2152284863;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// MsgBox
struct MsgBox_t1665650521;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlNode
struct XmlNode_t3767805227;




#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3704657025 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___rectTransform_0)); }
	inline RectTransform_t3704657025 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3704657025 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifndef SPLAYERINFOCOMMON_T369699146_H
#define SPLAYERINFOCOMMON_T369699146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerManager/sPlayerInfoCommon
struct  sPlayerInfoCommon_t369699146  : public RuntimeObject
{
public:
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nLevel
	int16_t ___nLevel_0;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nExp
	int16_t ___nExp_1;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nMoney
	int16_t ___nMoney_2;
	// System.Single CPlayerManager/sPlayerInfoCommon::fBaseVolumeByLevel
	float ___fBaseVolumeByLevel_3;
	// System.Single CPlayerManager/sPlayerInfoCommon::fBaseSpeed
	float ___fBaseSpeed_4;
	// System.Single CPlayerManager/sPlayerInfoCommon::fBaseShellTime
	float ___fBaseShellTime_5;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nItem_BaseSpeed_level
	int16_t ___nItem_BaseSpeed_level_6;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nItem_BaseShellTime_level
	int16_t ___nItem_BaseShellTime_level_7;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nEatThornNum
	int16_t ___nEatThornNum_8;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nSelectedSkillId
	int16_t ___nSelectedSkillId_9;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nSkillWLevel
	int16_t ___nSkillWLevel_10;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nSkillELevel
	int16_t ___nSkillELevel_11;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nSkillRLevel
	int16_t ___nSkillRLevel_12;
	// System.Int16 CPlayerManager/sPlayerInfoCommon::nSkillPoint
	int16_t ___nSkillPoint_13;
	// System.Byte[] CPlayerManager/sPlayerInfoCommon::bytes
	ByteU5BU5D_t4116647657* ___bytes_14;

public:
	inline static int32_t get_offset_of_nLevel_0() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nLevel_0)); }
	inline int16_t get_nLevel_0() const { return ___nLevel_0; }
	inline int16_t* get_address_of_nLevel_0() { return &___nLevel_0; }
	inline void set_nLevel_0(int16_t value)
	{
		___nLevel_0 = value;
	}

	inline static int32_t get_offset_of_nExp_1() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nExp_1)); }
	inline int16_t get_nExp_1() const { return ___nExp_1; }
	inline int16_t* get_address_of_nExp_1() { return &___nExp_1; }
	inline void set_nExp_1(int16_t value)
	{
		___nExp_1 = value;
	}

	inline static int32_t get_offset_of_nMoney_2() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nMoney_2)); }
	inline int16_t get_nMoney_2() const { return ___nMoney_2; }
	inline int16_t* get_address_of_nMoney_2() { return &___nMoney_2; }
	inline void set_nMoney_2(int16_t value)
	{
		___nMoney_2 = value;
	}

	inline static int32_t get_offset_of_fBaseVolumeByLevel_3() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___fBaseVolumeByLevel_3)); }
	inline float get_fBaseVolumeByLevel_3() const { return ___fBaseVolumeByLevel_3; }
	inline float* get_address_of_fBaseVolumeByLevel_3() { return &___fBaseVolumeByLevel_3; }
	inline void set_fBaseVolumeByLevel_3(float value)
	{
		___fBaseVolumeByLevel_3 = value;
	}

	inline static int32_t get_offset_of_fBaseSpeed_4() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___fBaseSpeed_4)); }
	inline float get_fBaseSpeed_4() const { return ___fBaseSpeed_4; }
	inline float* get_address_of_fBaseSpeed_4() { return &___fBaseSpeed_4; }
	inline void set_fBaseSpeed_4(float value)
	{
		___fBaseSpeed_4 = value;
	}

	inline static int32_t get_offset_of_fBaseShellTime_5() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___fBaseShellTime_5)); }
	inline float get_fBaseShellTime_5() const { return ___fBaseShellTime_5; }
	inline float* get_address_of_fBaseShellTime_5() { return &___fBaseShellTime_5; }
	inline void set_fBaseShellTime_5(float value)
	{
		___fBaseShellTime_5 = value;
	}

	inline static int32_t get_offset_of_nItem_BaseSpeed_level_6() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nItem_BaseSpeed_level_6)); }
	inline int16_t get_nItem_BaseSpeed_level_6() const { return ___nItem_BaseSpeed_level_6; }
	inline int16_t* get_address_of_nItem_BaseSpeed_level_6() { return &___nItem_BaseSpeed_level_6; }
	inline void set_nItem_BaseSpeed_level_6(int16_t value)
	{
		___nItem_BaseSpeed_level_6 = value;
	}

	inline static int32_t get_offset_of_nItem_BaseShellTime_level_7() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nItem_BaseShellTime_level_7)); }
	inline int16_t get_nItem_BaseShellTime_level_7() const { return ___nItem_BaseShellTime_level_7; }
	inline int16_t* get_address_of_nItem_BaseShellTime_level_7() { return &___nItem_BaseShellTime_level_7; }
	inline void set_nItem_BaseShellTime_level_7(int16_t value)
	{
		___nItem_BaseShellTime_level_7 = value;
	}

	inline static int32_t get_offset_of_nEatThornNum_8() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nEatThornNum_8)); }
	inline int16_t get_nEatThornNum_8() const { return ___nEatThornNum_8; }
	inline int16_t* get_address_of_nEatThornNum_8() { return &___nEatThornNum_8; }
	inline void set_nEatThornNum_8(int16_t value)
	{
		___nEatThornNum_8 = value;
	}

	inline static int32_t get_offset_of_nSelectedSkillId_9() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nSelectedSkillId_9)); }
	inline int16_t get_nSelectedSkillId_9() const { return ___nSelectedSkillId_9; }
	inline int16_t* get_address_of_nSelectedSkillId_9() { return &___nSelectedSkillId_9; }
	inline void set_nSelectedSkillId_9(int16_t value)
	{
		___nSelectedSkillId_9 = value;
	}

	inline static int32_t get_offset_of_nSkillWLevel_10() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nSkillWLevel_10)); }
	inline int16_t get_nSkillWLevel_10() const { return ___nSkillWLevel_10; }
	inline int16_t* get_address_of_nSkillWLevel_10() { return &___nSkillWLevel_10; }
	inline void set_nSkillWLevel_10(int16_t value)
	{
		___nSkillWLevel_10 = value;
	}

	inline static int32_t get_offset_of_nSkillELevel_11() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nSkillELevel_11)); }
	inline int16_t get_nSkillELevel_11() const { return ___nSkillELevel_11; }
	inline int16_t* get_address_of_nSkillELevel_11() { return &___nSkillELevel_11; }
	inline void set_nSkillELevel_11(int16_t value)
	{
		___nSkillELevel_11 = value;
	}

	inline static int32_t get_offset_of_nSkillRLevel_12() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nSkillRLevel_12)); }
	inline int16_t get_nSkillRLevel_12() const { return ___nSkillRLevel_12; }
	inline int16_t* get_address_of_nSkillRLevel_12() { return &___nSkillRLevel_12; }
	inline void set_nSkillRLevel_12(int16_t value)
	{
		___nSkillRLevel_12 = value;
	}

	inline static int32_t get_offset_of_nSkillPoint_13() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___nSkillPoint_13)); }
	inline int16_t get_nSkillPoint_13() const { return ___nSkillPoint_13; }
	inline int16_t* get_address_of_nSkillPoint_13() { return &___nSkillPoint_13; }
	inline void set_nSkillPoint_13(int16_t value)
	{
		___nSkillPoint_13 = value;
	}

	inline static int32_t get_offset_of_bytes_14() { return static_cast<int32_t>(offsetof(sPlayerInfoCommon_t369699146, ___bytes_14)); }
	inline ByteU5BU5D_t4116647657* get_bytes_14() const { return ___bytes_14; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_14() { return &___bytes_14; }
	inline void set_bytes_14(ByteU5BU5D_t4116647657* value)
	{
		___bytes_14 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLAYERINFOCOMMON_T369699146_H
#ifndef LAYOUTREBUILDER_T541313304_H
#define LAYOUTREBUILDER_T541313304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t541313304  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3704657025 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_ToRebuild_0)); }
	inline RectTransform_t3704657025 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3704657025 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3704657025 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t541313304_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t240936516 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t1258266594 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2748928575 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t240936516 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t240936516 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t240936516 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t1258266594 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t1258266594 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2748928575 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2748928575 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2748928575 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T541313304_H
#ifndef LAYOUTUTILITY_T2745813735_H
#define LAYOUTUTILITY_T2745813735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t2745813735  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t2745813735_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t235587086 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t235587086 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t235587086 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t235587086 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t235587086 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t235587086 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t235587086 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t235587086 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T2745813735_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CGETTRIGGERU3EC__ANONSTOREY1_T3423710625_H
#define U3CGETTRIGGERU3EC__ANONSTOREY1_T3423710625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/<GetTrigger>c__AnonStorey1
struct  U3CGetTriggerU3Ec__AnonStorey1_t3423710625  : public RuntimeObject
{
public:
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger/<GetTrigger>c__AnonStorey1::triggerName
	String_t* ___triggerName_0;

public:
	inline static int32_t get_offset_of_triggerName_0() { return static_cast<int32_t>(offsetof(U3CGetTriggerU3Ec__AnonStorey1_t3423710625, ___triggerName_0)); }
	inline String_t* get_triggerName_0() const { return ___triggerName_0; }
	inline String_t** get_address_of_triggerName_0() { return &___triggerName_0; }
	inline void set_triggerName_0(String_t* value)
	{
		___triggerName_0 = value;
		Il2CppCodeGenWriteBarrier((&___triggerName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTRIGGERU3EC__ANONSTOREY1_T3423710625_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CUPLOADFILEU3EC__ITERATOR2_T2703782479_H
#define U3CUPLOADFILEU3EC__ITERATOR2_T2703782479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountManager/<UploadFile>c__Iterator2
struct  U3CUploadFileU3Ec__Iterator2_t2703782479  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AccountManager/<UploadFile>c__Iterator2::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_0;
	// System.String AccountManager/<UploadFile>c__Iterator2::szContent
	String_t* ___szContent_1;
	// UnityEngine.WWW AccountManager/<UploadFile>c__Iterator2::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Object AccountManager/<UploadFile>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean AccountManager/<UploadFile>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 AccountManager/<UploadFile>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwwwFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUploadFileU3Ec__Iterator2_t2703782479, ___U3CwwwFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_0() const { return ___U3CwwwFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_0() { return &___U3CwwwFormU3E__0_0; }
	inline void set_U3CwwwFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_szContent_1() { return static_cast<int32_t>(offsetof(U3CUploadFileU3Ec__Iterator2_t2703782479, ___szContent_1)); }
	inline String_t* get_szContent_1() const { return ___szContent_1; }
	inline String_t** get_address_of_szContent_1() { return &___szContent_1; }
	inline void set_szContent_1(String_t* value)
	{
		___szContent_1 = value;
		Il2CppCodeGenWriteBarrier((&___szContent_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUploadFileU3Ec__Iterator2_t2703782479, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CUploadFileU3Ec__Iterator2_t2703782479, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CUploadFileU3Ec__Iterator2_t2703782479, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CUploadFileU3Ec__Iterator2_t2703782479, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPLOADFILEU3EC__ITERATOR2_T2703782479_H
#ifndef U3CDOSAVEMAPXMLFILEU3EC__ITERATOR1_T2618994493_H
#define U3CDOSAVEMAPXMLFILEU3EC__ITERATOR1_T2618994493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountManager/<DoSaveMapXmlFile>c__Iterator1
struct  U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm AccountManager/<DoSaveMapXmlFile>c__Iterator1::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_0;
	// System.String AccountManager/<DoSaveMapXmlFile>c__Iterator1::szRoomName
	String_t* ___szRoomName_1;
	// System.String AccountManager/<DoSaveMapXmlFile>c__Iterator1::szContent
	String_t* ___szContent_2;
	// UnityEngine.WWW AccountManager/<DoSaveMapXmlFile>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_3;
	// System.Object AccountManager/<DoSaveMapXmlFile>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AccountManager/<DoSaveMapXmlFile>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 AccountManager/<DoSaveMapXmlFile>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwwwFormU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___U3CwwwFormU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_0() const { return ___U3CwwwFormU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_0() { return &___U3CwwwFormU3E__0_0; }
	inline void set_U3CwwwFormU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_0), value);
	}

	inline static int32_t get_offset_of_szRoomName_1() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___szRoomName_1)); }
	inline String_t* get_szRoomName_1() const { return ___szRoomName_1; }
	inline String_t** get_address_of_szRoomName_1() { return &___szRoomName_1; }
	inline void set_szRoomName_1(String_t* value)
	{
		___szRoomName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szRoomName_1), value);
	}

	inline static int32_t get_offset_of_szContent_2() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___szContent_2)); }
	inline String_t* get_szContent_2() const { return ___szContent_2; }
	inline String_t** get_address_of_szContent_2() { return &___szContent_2; }
	inline void set_szContent_2(String_t* value)
	{
		___szContent_2 = value;
		Il2CppCodeGenWriteBarrier((&___szContent_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___U3CwwwU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_3() const { return ___U3CwwwU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_3() { return &___U3CwwwU3E__0_3; }
	inline void set_U3CwwwU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSAVEMAPXMLFILEU3EC__ITERATOR1_T2618994493_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1483120107_H
#define U3CSTARTU3EC__ITERATOR0_T1483120107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountManager/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1483120107  : public RuntimeObject
{
public:
	// System.String AccountManager/<Start>c__Iterator0::<szFileName>__0
	String_t* ___U3CszFileNameU3E__0_0;
	// UnityEngine.WWW AccountManager/<Start>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// System.Collections.Generic.List`1<System.String> AccountManager/<Start>c__Iterator0::<lstSkills>__0
	List_1_t3319525431 * ___U3ClstSkillsU3E__0_2;
	// AccountManager AccountManager/<Start>c__Iterator0::$this
	AccountManager_t2668481700 * ___U24this_3;
	// System.Object AccountManager/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean AccountManager/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 AccountManager/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CszFileNameU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U3CszFileNameU3E__0_0)); }
	inline String_t* get_U3CszFileNameU3E__0_0() const { return ___U3CszFileNameU3E__0_0; }
	inline String_t** get_address_of_U3CszFileNameU3E__0_0() { return &___U3CszFileNameU3E__0_0; }
	inline void set_U3CszFileNameU3E__0_0(String_t* value)
	{
		___U3CszFileNameU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CszFileNameU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3ClstSkillsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U3ClstSkillsU3E__0_2)); }
	inline List_1_t3319525431 * get_U3ClstSkillsU3E__0_2() const { return ___U3ClstSkillsU3E__0_2; }
	inline List_1_t3319525431 ** get_address_of_U3ClstSkillsU3E__0_2() { return &___U3ClstSkillsU3E__0_2; }
	inline void set_U3ClstSkillsU3E__0_2(List_1_t3319525431 * value)
	{
		___U3ClstSkillsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClstSkillsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U24this_3)); }
	inline AccountManager_t2668481700 * get_U24this_3() const { return ___U24this_3; }
	inline AccountManager_t2668481700 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AccountManager_t2668481700 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1483120107, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1483120107_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef EATNODE_T1329646118_H
#define EATNODE_T1329646118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball/EatNode
struct  EatNode_t1329646118 
{
public:
	// Ball Ball/EatNode::eatenBall
	Ball_t2206666566 * ___eatenBall_0;
	// System.Single Ball/EatNode::eatenSize
	float ___eatenSize_1;

public:
	inline static int32_t get_offset_of_eatenBall_0() { return static_cast<int32_t>(offsetof(EatNode_t1329646118, ___eatenBall_0)); }
	inline Ball_t2206666566 * get_eatenBall_0() const { return ___eatenBall_0; }
	inline Ball_t2206666566 ** get_address_of_eatenBall_0() { return &___eatenBall_0; }
	inline void set_eatenBall_0(Ball_t2206666566 * value)
	{
		___eatenBall_0 = value;
		Il2CppCodeGenWriteBarrier((&___eatenBall_0), value);
	}

	inline static int32_t get_offset_of_eatenSize_1() { return static_cast<int32_t>(offsetof(EatNode_t1329646118, ___eatenSize_1)); }
	inline float get_eatenSize_1() const { return ___eatenSize_1; }
	inline float* get_address_of_eatenSize_1() { return &___eatenSize_1; }
	inline void set_eatenSize_1(float value)
	{
		___eatenSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Ball/EatNode
struct EatNode_t1329646118_marshaled_pinvoke
{
	Ball_t2206666566 * ___eatenBall_0;
	float ___eatenSize_1;
};
// Native definition for COM marshalling of Ball/EatNode
struct EatNode_t1329646118_marshaled_com
{
	Ball_t2206666566 * ___eatenBall_0;
	float ___eatenSize_1;
};
#endif // EATNODE_T1329646118_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef SORPHANINFO_T2789095025_H
#define SORPHANINFO_T2789095025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerManager/sOrphanInfo
struct  sOrphanInfo_t2789095025 
{
public:
	// System.String CPlayerManager/sOrphanInfo::szAccount
	String_t* ___szAccount_0;
	// System.String CPlayerManager/sOrphanInfo::szPlayerName
	String_t* ___szPlayerName_1;
	// System.Int32 CPlayerManager/sOrphanInfo::nColorId
	int32_t ___nColorId_2;
	// System.Int32 CPlayerManager/sOrphanInfo::nSkinId
	int32_t ___nSkinId_3;
	// System.Int32 CPlayerManager/sOrphanInfo::nBlobSize
	int32_t ___nBlobSize_4;
	// System.Byte[] CPlayerManager/sOrphanInfo::bytesBallsData
	ByteU5BU5D_t4116647657* ___bytesBallsData_5;

public:
	inline static int32_t get_offset_of_szAccount_0() { return static_cast<int32_t>(offsetof(sOrphanInfo_t2789095025, ___szAccount_0)); }
	inline String_t* get_szAccount_0() const { return ___szAccount_0; }
	inline String_t** get_address_of_szAccount_0() { return &___szAccount_0; }
	inline void set_szAccount_0(String_t* value)
	{
		___szAccount_0 = value;
		Il2CppCodeGenWriteBarrier((&___szAccount_0), value);
	}

	inline static int32_t get_offset_of_szPlayerName_1() { return static_cast<int32_t>(offsetof(sOrphanInfo_t2789095025, ___szPlayerName_1)); }
	inline String_t* get_szPlayerName_1() const { return ___szPlayerName_1; }
	inline String_t** get_address_of_szPlayerName_1() { return &___szPlayerName_1; }
	inline void set_szPlayerName_1(String_t* value)
	{
		___szPlayerName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szPlayerName_1), value);
	}

	inline static int32_t get_offset_of_nColorId_2() { return static_cast<int32_t>(offsetof(sOrphanInfo_t2789095025, ___nColorId_2)); }
	inline int32_t get_nColorId_2() const { return ___nColorId_2; }
	inline int32_t* get_address_of_nColorId_2() { return &___nColorId_2; }
	inline void set_nColorId_2(int32_t value)
	{
		___nColorId_2 = value;
	}

	inline static int32_t get_offset_of_nSkinId_3() { return static_cast<int32_t>(offsetof(sOrphanInfo_t2789095025, ___nSkinId_3)); }
	inline int32_t get_nSkinId_3() const { return ___nSkinId_3; }
	inline int32_t* get_address_of_nSkinId_3() { return &___nSkinId_3; }
	inline void set_nSkinId_3(int32_t value)
	{
		___nSkinId_3 = value;
	}

	inline static int32_t get_offset_of_nBlobSize_4() { return static_cast<int32_t>(offsetof(sOrphanInfo_t2789095025, ___nBlobSize_4)); }
	inline int32_t get_nBlobSize_4() const { return ___nBlobSize_4; }
	inline int32_t* get_address_of_nBlobSize_4() { return &___nBlobSize_4; }
	inline void set_nBlobSize_4(int32_t value)
	{
		___nBlobSize_4 = value;
	}

	inline static int32_t get_offset_of_bytesBallsData_5() { return static_cast<int32_t>(offsetof(sOrphanInfo_t2789095025, ___bytesBallsData_5)); }
	inline ByteU5BU5D_t4116647657* get_bytesBallsData_5() const { return ___bytesBallsData_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytesBallsData_5() { return &___bytesBallsData_5; }
	inline void set_bytesBallsData_5(ByteU5BU5D_t4116647657* value)
	{
		___bytesBallsData_5 = value;
		Il2CppCodeGenWriteBarrier((&___bytesBallsData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CPlayerManager/sOrphanInfo
struct sOrphanInfo_t2789095025_marshaled_pinvoke
{
	char* ___szAccount_0;
	char* ___szPlayerName_1;
	int32_t ___nColorId_2;
	int32_t ___nSkinId_3;
	int32_t ___nBlobSize_4;
	uint8_t* ___bytesBallsData_5;
};
// Native definition for COM marshalling of CPlayerManager/sOrphanInfo
struct sOrphanInfo_t2789095025_marshaled_com
{
	Il2CppChar* ___szAccount_0;
	Il2CppChar* ___szPlayerName_1;
	int32_t ___nColorId_2;
	int32_t ___nSkinId_3;
	int32_t ___nBlobSize_4;
	uint8_t* ___bytesBallsData_5;
};
#endif // SORPHANINFO_T2789095025_H
#ifndef UNITYEVENT_1_T4233366749_H
#define UNITYEVENT_1_T4233366749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<HedgehogTeam.EasyTouch.Gesture>
struct  UnityEvent_1_t4233366749  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4233366749, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4233366749_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef STHORNCONFIG_T1242444451_H
#define STHORNCONFIG_T1242444451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMonsterEditor/sThornConfig
struct  sThornConfig_t1242444451 
{
public:
	// System.String CMonsterEditor/sThornConfig::szId
	String_t* ___szId_0;
	// System.Int32 CMonsterEditor/sThornConfig::nType
	int32_t ___nType_1;
	// System.Single CMonsterEditor/sThornConfig::fFoodSize
	float ___fFoodSize_2;
	// System.String CMonsterEditor/sThornConfig::szColor
	String_t* ___szColor_3;
	// System.String CMonsterEditor/sThornConfig::szDesc
	String_t* ___szDesc_4;
	// System.Single CMonsterEditor/sThornConfig::fSelfSize
	float ___fSelfSize_5;
	// System.Single CMonsterEditor/sThornConfig::fExp
	float ___fExp_6;
	// System.Single CMonsterEditor/sThornConfig::fMoney
	float ___fMoney_7;
	// System.Single CMonsterEditor/sThornConfig::fBuffId
	float ___fBuffId_8;
	// System.Single CMonsterEditor/sThornConfig::fExplodeChildNum
	float ___fExplodeChildNum_9;
	// System.Single CMonsterEditor/sThornConfig::fExplodeMotherLeftPercent
	float ___fExplodeMotherLeftPercent_10;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunDistance
	float ___fExplodeRunDistance_11;
	// System.Single CMonsterEditor/sThornConfig::fExplodeRunTime
	float ___fExplodeRunTime_12;
	// System.Single CMonsterEditor/sThornConfig::fExplodeStayTime
	float ___fExplodeStayTime_13;
	// System.Single CMonsterEditor/sThornConfig::fExplodeShellTime
	float ___fExplodeShellTime_14;
	// System.Single CMonsterEditor/sThornConfig::fExplodeFormationId
	float ___fExplodeFormationId_15;
	// System.Int32 CMonsterEditor/sThornConfig::nSkillId
	int32_t ___nSkillId_16;

public:
	inline static int32_t get_offset_of_szId_0() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szId_0)); }
	inline String_t* get_szId_0() const { return ___szId_0; }
	inline String_t** get_address_of_szId_0() { return &___szId_0; }
	inline void set_szId_0(String_t* value)
	{
		___szId_0 = value;
		Il2CppCodeGenWriteBarrier((&___szId_0), value);
	}

	inline static int32_t get_offset_of_nType_1() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nType_1)); }
	inline int32_t get_nType_1() const { return ___nType_1; }
	inline int32_t* get_address_of_nType_1() { return &___nType_1; }
	inline void set_nType_1(int32_t value)
	{
		___nType_1 = value;
	}

	inline static int32_t get_offset_of_fFoodSize_2() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fFoodSize_2)); }
	inline float get_fFoodSize_2() const { return ___fFoodSize_2; }
	inline float* get_address_of_fFoodSize_2() { return &___fFoodSize_2; }
	inline void set_fFoodSize_2(float value)
	{
		___fFoodSize_2 = value;
	}

	inline static int32_t get_offset_of_szColor_3() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szColor_3)); }
	inline String_t* get_szColor_3() const { return ___szColor_3; }
	inline String_t** get_address_of_szColor_3() { return &___szColor_3; }
	inline void set_szColor_3(String_t* value)
	{
		___szColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___szColor_3), value);
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_fSelfSize_5() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fSelfSize_5)); }
	inline float get_fSelfSize_5() const { return ___fSelfSize_5; }
	inline float* get_address_of_fSelfSize_5() { return &___fSelfSize_5; }
	inline void set_fSelfSize_5(float value)
	{
		___fSelfSize_5 = value;
	}

	inline static int32_t get_offset_of_fExp_6() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExp_6)); }
	inline float get_fExp_6() const { return ___fExp_6; }
	inline float* get_address_of_fExp_6() { return &___fExp_6; }
	inline void set_fExp_6(float value)
	{
		___fExp_6 = value;
	}

	inline static int32_t get_offset_of_fMoney_7() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fMoney_7)); }
	inline float get_fMoney_7() const { return ___fMoney_7; }
	inline float* get_address_of_fMoney_7() { return &___fMoney_7; }
	inline void set_fMoney_7(float value)
	{
		___fMoney_7 = value;
	}

	inline static int32_t get_offset_of_fBuffId_8() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fBuffId_8)); }
	inline float get_fBuffId_8() const { return ___fBuffId_8; }
	inline float* get_address_of_fBuffId_8() { return &___fBuffId_8; }
	inline void set_fBuffId_8(float value)
	{
		___fBuffId_8 = value;
	}

	inline static int32_t get_offset_of_fExplodeChildNum_9() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeChildNum_9)); }
	inline float get_fExplodeChildNum_9() const { return ___fExplodeChildNum_9; }
	inline float* get_address_of_fExplodeChildNum_9() { return &___fExplodeChildNum_9; }
	inline void set_fExplodeChildNum_9(float value)
	{
		___fExplodeChildNum_9 = value;
	}

	inline static int32_t get_offset_of_fExplodeMotherLeftPercent_10() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeMotherLeftPercent_10)); }
	inline float get_fExplodeMotherLeftPercent_10() const { return ___fExplodeMotherLeftPercent_10; }
	inline float* get_address_of_fExplodeMotherLeftPercent_10() { return &___fExplodeMotherLeftPercent_10; }
	inline void set_fExplodeMotherLeftPercent_10(float value)
	{
		___fExplodeMotherLeftPercent_10 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunDistance_11() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunDistance_11)); }
	inline float get_fExplodeRunDistance_11() const { return ___fExplodeRunDistance_11; }
	inline float* get_address_of_fExplodeRunDistance_11() { return &___fExplodeRunDistance_11; }
	inline void set_fExplodeRunDistance_11(float value)
	{
		___fExplodeRunDistance_11 = value;
	}

	inline static int32_t get_offset_of_fExplodeRunTime_12() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeRunTime_12)); }
	inline float get_fExplodeRunTime_12() const { return ___fExplodeRunTime_12; }
	inline float* get_address_of_fExplodeRunTime_12() { return &___fExplodeRunTime_12; }
	inline void set_fExplodeRunTime_12(float value)
	{
		___fExplodeRunTime_12 = value;
	}

	inline static int32_t get_offset_of_fExplodeStayTime_13() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeStayTime_13)); }
	inline float get_fExplodeStayTime_13() const { return ___fExplodeStayTime_13; }
	inline float* get_address_of_fExplodeStayTime_13() { return &___fExplodeStayTime_13; }
	inline void set_fExplodeStayTime_13(float value)
	{
		___fExplodeStayTime_13 = value;
	}

	inline static int32_t get_offset_of_fExplodeShellTime_14() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeShellTime_14)); }
	inline float get_fExplodeShellTime_14() const { return ___fExplodeShellTime_14; }
	inline float* get_address_of_fExplodeShellTime_14() { return &___fExplodeShellTime_14; }
	inline void set_fExplodeShellTime_14(float value)
	{
		___fExplodeShellTime_14 = value;
	}

	inline static int32_t get_offset_of_fExplodeFormationId_15() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___fExplodeFormationId_15)); }
	inline float get_fExplodeFormationId_15() const { return ___fExplodeFormationId_15; }
	inline float* get_address_of_fExplodeFormationId_15() { return &___fExplodeFormationId_15; }
	inline void set_fExplodeFormationId_15(float value)
	{
		___fExplodeFormationId_15 = value;
	}

	inline static int32_t get_offset_of_nSkillId_16() { return static_cast<int32_t>(offsetof(sThornConfig_t1242444451, ___nSkillId_16)); }
	inline int32_t get_nSkillId_16() const { return ___nSkillId_16; }
	inline int32_t* get_address_of_nSkillId_16() { return &___nSkillId_16; }
	inline void set_nSkillId_16(int32_t value)
	{
		___nSkillId_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_pinvoke
{
	char* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	char* ___szColor_3;
	char* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
// Native definition for COM marshalling of CMonsterEditor/sThornConfig
struct sThornConfig_t1242444451_marshaled_com
{
	Il2CppChar* ___szId_0;
	int32_t ___nType_1;
	float ___fFoodSize_2;
	Il2CppChar* ___szColor_3;
	Il2CppChar* ___szDesc_4;
	float ___fSelfSize_5;
	float ___fExp_6;
	float ___fMoney_7;
	float ___fBuffId_8;
	float ___fExplodeChildNum_9;
	float ___fExplodeMotherLeftPercent_10;
	float ___fExplodeRunDistance_11;
	float ___fExplodeRunTime_12;
	float ___fExplodeStayTime_13;
	float ___fExplodeShellTime_14;
	float ___fExplodeFormationId_15;
	int32_t ___nSkillId_16;
};
#endif // STHORNCONFIG_T1242444451_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ONDRAGEND_T1175011983_H
#define ONDRAGEND_T1175011983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag/OnDragEnd
struct  OnDragEnd_t1175011983  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGEND_T1175011983_H
#ifndef ONTOUCHENTER_T1794713516_H
#define ONTOUCHENTER_T1794713516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchEnter
struct  OnTouchEnter_t1794713516  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHENTER_T1794713516_H
#ifndef ONDRAG_T3336846729_H
#define ONDRAG_T3336846729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag/OnDrag
struct  OnDrag_t3336846729  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAG_T3336846729_H
#ifndef ONTOUCHOVER_T3317551264_H
#define ONTOUCHOVER_T3317551264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchOver
struct  OnTouchOver_t3317551264  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHOVER_T3317551264_H
#ifndef ONDRAGSTART_T3676143812_H
#define ONDRAGSTART_T3676143812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag/OnDragStart
struct  OnDragStart_t3676143812  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDRAGSTART_T3676143812_H
#ifndef ESKILLTYPE_T3445085857_H
#define ESKILLTYPE_T3445085857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillManager/eSkillType
struct  eSkillType_t3445085857 
{
public:
	// System.Int32 CSkillManager/eSkillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillType_t3445085857, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLTYPE_T3445085857_H
#ifndef ETTTYPE_T2791900397_H
#define ETTTYPE_T2791900397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTType
struct  ETTType_t2791900397 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ETTType_t2791900397, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETTTYPE_T2791900397_H
#ifndef GAMEOBJECTTYPE_T2199397569_H
#define GAMEOBJECTTYPE_T2199397569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase/GameObjectType
struct  GameObjectType_t2199397569 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase/GameObjectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameObjectType_t2199397569, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTTYPE_T2199397569_H
#ifndef DIRECTACTION_T2452253191_H
#define DIRECTACTION_T2452253191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase/DirectAction
struct  DirectAction_t2452253191 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase/DirectAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DirectAction_t2452253191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTACTION_T2452253191_H
#ifndef AFFECTEDAXESACTION_T2927047446_H
#define AFFECTEDAXESACTION_T2927047446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction
struct  AffectedAxesAction_t2927047446 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AffectedAxesAction_t2927047446, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFFECTEDAXESACTION_T2927047446_H
#ifndef ETTPARAMETER_T3639556146_H
#define ETTPARAMETER_T3639556146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTParameter
struct  ETTParameter_t3639556146 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTParameter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ETTParameter_t3639556146, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETTPARAMETER_T3639556146_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ONTOUCHEXIT_T2180465286_H
#define ONTOUCHEXIT_T2180465286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchExit
struct  OnTouchExit_t2180465286  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHEXIT_T2180465286_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef ESKILLID_T2951149716_H
#define ESKILLID_T2951149716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillSystem/eSkillId
struct  eSkillId_t2951149716 
{
public:
	// System.Int32 CSkillSystem/eSkillId::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSkillId_t2951149716, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLID_T2951149716_H
#ifndef EVTTYPE_T1031077730_H
#define EVTTYPE_T1031077730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/EvtType
struct  EvtType_t1031077730 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/EvtType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EvtType_t1031077730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVTTYPE_T1031077730_H
#ifndef ACTIONTRIGGERING_T481153517_H
#define ACTIONTRIGGERING_T481153517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe/ActionTriggering
struct  ActionTriggering_t481153517 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickSwipe/ActionTriggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTriggering_t481153517, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T481153517_H
#ifndef SWIPEDIRECTION_T2936141849_H
#define SWIPEDIRECTION_T2936141849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe/SwipeDirection
struct  SwipeDirection_t2936141849 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickSwipe/SwipeDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SwipeDirection_t2936141849, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_T2936141849_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef ONLONGTAP_T243381522_H
#define ONLONGTAP_T243381522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap/OnLongTap
struct  OnLongTap_t243381522  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONLONGTAP_T243381522_H
#ifndef ACTIONTRIGGERING_T435375650_H
#define ACTIONTRIGGERING_T435375650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap/ActionTriggering
struct  ActionTriggering_t435375650 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickLongTap/ActionTriggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTriggering_t435375650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTRIGGERING_T435375650_H
#ifndef ONPINCHACTION_T3145810715_H
#define ONPINCHACTION_T3145810715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch/OnPinchAction
struct  OnPinchAction_t3145810715  : public UnityEvent_1_t4233366749
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPINCHACTION_T3145810715_H
#ifndef ACTIONTIGGERING_T2337630454_H
#define ACTIONTIGGERING_T2337630454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch/ActionTiggering
struct  ActionTiggering_t2337630454 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickPinch/ActionTiggering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionTiggering_t2337630454, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTIGGERING_T2337630454_H
#ifndef ACTIONPINCHDIRECTION_T755360707_H
#define ACTIONPINCHDIRECTION_T755360707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch/ActionPinchDirection
struct  ActionPinchDirection_t755360707 
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.QuickPinch/ActionPinchDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionPinchDirection_t755360707, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONPINCHDIRECTION_T755360707_H
#ifndef EEJECTMODE_T2913208038_H
#define EEJECTMODE_T2913208038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpore/eEjectMode
struct  eEjectMode_t2913208038 
{
public:
	// System.Int32 CSpore/eEjectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eEjectMode_t2913208038, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEJECTMODE_T2913208038_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef ETRIGGERTYPE_T2367132649_H
#define ETRIGGERTYPE_T2367132649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallTrigger_New/eTriggerType
struct  eTriggerType_t2367132649 
{
public:
	// System.Int32 BallTrigger_New/eTriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eTriggerType_t2367132649, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETRIGGERTYPE_T2367132649_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef CONSTRAINT_T814224393_H
#define CONSTRAINT_T814224393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t814224393 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t814224393, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T814224393_H
#ifndef ESCENEMODE_T3985604454_H
#define ESCENEMODE_T3985604454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountManager/eSceneMode
struct  eSceneMode_t3985604454 
{
public:
	// System.Int32 AccountManager/eSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSceneMode_t3985604454, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCENEMODE_T3985604454_H
#ifndef FITMODE_T3267881214_H
#define FITMODE_T3267881214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t3267881214 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t3267881214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T3267881214_H
#ifndef AXIS_T3613393006_H
#define AXIS_T3613393006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t3613393006 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t3613393006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T3613393006_H
#ifndef EBALLTYPE_T88636580_H
#define EBALLTYPE_T88636580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball/eBallType
struct  eBallType_t88636580 
{
public:
	// System.Int32 Ball/eBallType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBallType_t88636580, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBALLTYPE_T88636580_H
#ifndef EBLOCKPOSTYPE_T149092435_H
#define EBLOCKPOSTYPE_T149092435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBlockDivide/eBlockPosType
struct  eBlockPosType_t149092435 
{
public:
	// System.Int32 CBlockDivide/eBlockPosType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eBlockPosType_t149092435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBLOCKPOSTYPE_T149092435_H
#ifndef EEJECTMODE_T2915597278_H
#define EEJECTMODE_T2915597278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball/eEjectMode
struct  eEjectMode_t2915597278 
{
public:
	// System.Int32 Ball/eEjectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eEjectMode_t2915597278, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEJECTMODE_T2915597278_H
#ifndef CORNER_T1493259673_H
#define CORNER_T1493259673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1493259673 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t1493259673, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1493259673_H
#ifndef ESHELLTYPE_T2293241718_H
#define ESHELLTYPE_T2293241718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball/eShellType
struct  eShellType_t2293241718 
{
public:
	// System.Int32 Ball/eShellType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eShellType_t2293241718, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESHELLTYPE_T2293241718_H
#ifndef EKUOZHANGSTATUS_T123357593_H
#define EKUOZHANGSTATUS_T123357593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerIns/eKuoZhangStatus
struct  eKuoZhangStatus_t123357593 
{
public:
	// System.Int32 CPlayerIns/eKuoZhangStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eKuoZhangStatus_t123357593, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EKUOZHANGSTATUS_T123357593_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef EPLAYERCOMMONINFOTYPE_T3738107693_H
#define EPLAYERCOMMONINFOTYPE_T3738107693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerManager/ePlayerCommonInfoType
struct  ePlayerCommonInfoType_t3738107693 
{
public:
	// System.Int32 CPlayerManager/ePlayerCommonInfoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ePlayerCommonInfoType_t3738107693, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPLAYERCOMMONINFOTYPE_T3738107693_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef EASYTOUCHRECEIVER_T857180639_H
#define EASYTOUCHRECEIVER_T857180639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver
struct  EasyTouchReceiver_t857180639  : public RuntimeObject
{
public:
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::enable
	bool ___enable_0;
	// HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTType HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::triggerType
	int32_t ___triggerType_1;
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::name
	String_t* ___name_2;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::restricted
	bool ___restricted_3;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::gameObject
	GameObject_t1113636619 * ___gameObject_4;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::otherReceiver
	bool ___otherReceiver_5;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::gameObjectReceiver
	GameObject_t1113636619 * ___gameObjectReceiver_6;
	// HedgehogTeam.EasyTouch.EasyTouch/EvtType HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::eventName
	int32_t ___eventName_7;
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::methodName
	String_t* ___methodName_8;
	// HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTParameter HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::parameter
	int32_t ___parameter_9;

public:
	inline static int32_t get_offset_of_enable_0() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___enable_0)); }
	inline bool get_enable_0() const { return ___enable_0; }
	inline bool* get_address_of_enable_0() { return &___enable_0; }
	inline void set_enable_0(bool value)
	{
		___enable_0 = value;
	}

	inline static int32_t get_offset_of_triggerType_1() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___triggerType_1)); }
	inline int32_t get_triggerType_1() const { return ___triggerType_1; }
	inline int32_t* get_address_of_triggerType_1() { return &___triggerType_1; }
	inline void set_triggerType_1(int32_t value)
	{
		___triggerType_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_restricted_3() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___restricted_3)); }
	inline bool get_restricted_3() const { return ___restricted_3; }
	inline bool* get_address_of_restricted_3() { return &___restricted_3; }
	inline void set_restricted_3(bool value)
	{
		___restricted_3 = value;
	}

	inline static int32_t get_offset_of_gameObject_4() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___gameObject_4)); }
	inline GameObject_t1113636619 * get_gameObject_4() const { return ___gameObject_4; }
	inline GameObject_t1113636619 ** get_address_of_gameObject_4() { return &___gameObject_4; }
	inline void set_gameObject_4(GameObject_t1113636619 * value)
	{
		___gameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_4), value);
	}

	inline static int32_t get_offset_of_otherReceiver_5() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___otherReceiver_5)); }
	inline bool get_otherReceiver_5() const { return ___otherReceiver_5; }
	inline bool* get_address_of_otherReceiver_5() { return &___otherReceiver_5; }
	inline void set_otherReceiver_5(bool value)
	{
		___otherReceiver_5 = value;
	}

	inline static int32_t get_offset_of_gameObjectReceiver_6() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___gameObjectReceiver_6)); }
	inline GameObject_t1113636619 * get_gameObjectReceiver_6() const { return ___gameObjectReceiver_6; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectReceiver_6() { return &___gameObjectReceiver_6; }
	inline void set_gameObjectReceiver_6(GameObject_t1113636619 * value)
	{
		___gameObjectReceiver_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectReceiver_6), value);
	}

	inline static int32_t get_offset_of_eventName_7() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___eventName_7)); }
	inline int32_t get_eventName_7() const { return ___eventName_7; }
	inline int32_t* get_address_of_eventName_7() { return &___eventName_7; }
	inline void set_eventName_7(int32_t value)
	{
		___eventName_7 = value;
	}

	inline static int32_t get_offset_of_methodName_8() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___methodName_8)); }
	inline String_t* get_methodName_8() const { return ___methodName_8; }
	inline String_t** get_address_of_methodName_8() { return &___methodName_8; }
	inline void set_methodName_8(String_t* value)
	{
		___methodName_8 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_8), value);
	}

	inline static int32_t get_offset_of_parameter_9() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t857180639, ___parameter_9)); }
	inline int32_t get_parameter_9() const { return ___parameter_9; }
	inline int32_t* get_address_of_parameter_9() { return &___parameter_9; }
	inline void set_parameter_9(int32_t value)
	{
		___parameter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHRECEIVER_T857180639_H
#ifndef U3CISRECEVIER4U3EC__ANONSTOREY0_T2005754022_H
#define U3CISRECEVIER4U3EC__ANONSTOREY0_T2005754022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/<IsRecevier4>c__AnonStorey0
struct  U3CIsRecevier4U3Ec__AnonStorey0_t2005754022  : public RuntimeObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/EvtType HedgehogTeam.EasyTouch.EasyTouchTrigger/<IsRecevier4>c__AnonStorey0::evnt
	int32_t ___evnt_0;

public:
	inline static int32_t get_offset_of_evnt_0() { return static_cast<int32_t>(offsetof(U3CIsRecevier4U3Ec__AnonStorey0_t2005754022, ___evnt_0)); }
	inline int32_t get_evnt_0() const { return ___evnt_0; }
	inline int32_t* get_address_of_evnt_0() { return &___evnt_0; }
	inline void set_evnt_0(int32_t value)
	{
		___evnt_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CISRECEVIER4U3EC__ANONSTOREY0_T2005754022_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef DELEGATEMETHOD_ONSELECTED_T3195943432_H
#define DELEGATEMETHOD_ONSELECTED_T3195943432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ComoList/DelegateMethod_OnSelected
struct  DelegateMethod_OnSelected_t3195943432  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEMETHOD_ONSELECTED_T3195943432_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef DELEGATEMETHOD_OK_T3925705237_H
#define DELEGATEMETHOD_OK_T3925705237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateNewShit/DelegateMethod_OK
struct  DelegateMethod_OK_t3925705237  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEMETHOD_OK_T3925705237_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CREATENEWSHIT_T3983358377_H
#define CREATENEWSHIT_T3983358377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateNewShit
struct  CreateNewShit_t3983358377  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text CreateNewShit::_txtTitle
	Text_t1901882714 * ____txtTitle_2;
	// UnityEngine.UI.InputField CreateNewShit::_inputNewShitName
	InputField_t3762917431 * ____inputNewShitName_3;
	// UnityEngine.UI.Button CreateNewShit::_btnOK
	Button_t4055032469 * ____btnOK_4;
	// UnityEngine.UI.Button CreateNewShit::_btnCancel
	Button_t4055032469 * ____btnCancel_5;
	// CreateNewShit/DelegateMethod_OK CreateNewShit::delegateMethodOK
	DelegateMethod_OK_t3925705237 * ___delegateMethodOK_6;

public:
	inline static int32_t get_offset_of__txtTitle_2() { return static_cast<int32_t>(offsetof(CreateNewShit_t3983358377, ____txtTitle_2)); }
	inline Text_t1901882714 * get__txtTitle_2() const { return ____txtTitle_2; }
	inline Text_t1901882714 ** get_address_of__txtTitle_2() { return &____txtTitle_2; }
	inline void set__txtTitle_2(Text_t1901882714 * value)
	{
		____txtTitle_2 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitle_2), value);
	}

	inline static int32_t get_offset_of__inputNewShitName_3() { return static_cast<int32_t>(offsetof(CreateNewShit_t3983358377, ____inputNewShitName_3)); }
	inline InputField_t3762917431 * get__inputNewShitName_3() const { return ____inputNewShitName_3; }
	inline InputField_t3762917431 ** get_address_of__inputNewShitName_3() { return &____inputNewShitName_3; }
	inline void set__inputNewShitName_3(InputField_t3762917431 * value)
	{
		____inputNewShitName_3 = value;
		Il2CppCodeGenWriteBarrier((&____inputNewShitName_3), value);
	}

	inline static int32_t get_offset_of__btnOK_4() { return static_cast<int32_t>(offsetof(CreateNewShit_t3983358377, ____btnOK_4)); }
	inline Button_t4055032469 * get__btnOK_4() const { return ____btnOK_4; }
	inline Button_t4055032469 ** get_address_of__btnOK_4() { return &____btnOK_4; }
	inline void set__btnOK_4(Button_t4055032469 * value)
	{
		____btnOK_4 = value;
		Il2CppCodeGenWriteBarrier((&____btnOK_4), value);
	}

	inline static int32_t get_offset_of__btnCancel_5() { return static_cast<int32_t>(offsetof(CreateNewShit_t3983358377, ____btnCancel_5)); }
	inline Button_t4055032469 * get__btnCancel_5() const { return ____btnCancel_5; }
	inline Button_t4055032469 ** get_address_of__btnCancel_5() { return &____btnCancel_5; }
	inline void set__btnCancel_5(Button_t4055032469 * value)
	{
		____btnCancel_5 = value;
		Il2CppCodeGenWriteBarrier((&____btnCancel_5), value);
	}

	inline static int32_t get_offset_of_delegateMethodOK_6() { return static_cast<int32_t>(offsetof(CreateNewShit_t3983358377, ___delegateMethodOK_6)); }
	inline DelegateMethod_OK_t3925705237 * get_delegateMethodOK_6() const { return ___delegateMethodOK_6; }
	inline DelegateMethod_OK_t3925705237 ** get_address_of_delegateMethodOK_6() { return &___delegateMethodOK_6; }
	inline void set_delegateMethodOK_6(DelegateMethod_OK_t3925705237 * value)
	{
		___delegateMethodOK_6 = value;
		Il2CppCodeGenWriteBarrier((&___delegateMethodOK_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATENEWSHIT_T3983358377_H
#ifndef CPLAYERMANAGER_T2516829028_H
#define CPLAYERMANAGER_T2516829028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerManager
struct  CPlayerManager_t2516829028  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CPlayerManager::m_prePlayerIns
	GameObject_t1113636619 * ___m_prePlayerIns_2;
	// System.Collections.Generic.Dictionary`2<System.String,CPlayerManager/sPlayerInfoCommon> CPlayerManager::m_dicPlayerInfo_Common
	Dictionary_2_t154955445 * ___m_dicPlayerInfo_Common_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,Player> CPlayerManager::m_dicPlayers
	Dictionary_2_t2155360643 * ___m_dicPlayers_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]> CPlayerManager::m_dicPlayerInfo_RealTime
	Dictionary_2_t3901903956 * ___m_dicPlayerInfo_RealTime_6;
	// System.String CPlayerManager::m_szShengFuPlayerName
	String_t* ___m_szShengFuPlayerName_7;
	// System.Collections.Generic.Dictionary`2<System.String,CPlayerIns> CPlayerManager::m_dicPlayerIns
	Dictionary_2_t2993447027 * ___m_dicPlayerIns_8;
	// UnityEngine.GameObject CPlayerManager::m_goContainerPlayerIns
	GameObject_t1113636619 * ___m_goContainerPlayerIns_9;
	// System.Collections.Generic.Dictionary`2<System.String,Player> CPlayerManager::m_dicPlayerByAccount
	Dictionary_2_t3051903611 * ___m_dicPlayerByAccount_10;
	// System.Byte[] CPlayerManager::_byteOrphanBallDatat
	ByteU5BU5D_t4116647657* ____byteOrphanBallDatat_11;
	// System.Collections.Generic.List`1<CPlayerManager/sOrphanInfo> CPlayerManager::m_lstTempOrphanBallData
	List_1_t4261169767 * ___m_lstTempOrphanBallData_12;

public:
	inline static int32_t get_offset_of_m_prePlayerIns_2() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_prePlayerIns_2)); }
	inline GameObject_t1113636619 * get_m_prePlayerIns_2() const { return ___m_prePlayerIns_2; }
	inline GameObject_t1113636619 ** get_address_of_m_prePlayerIns_2() { return &___m_prePlayerIns_2; }
	inline void set_m_prePlayerIns_2(GameObject_t1113636619 * value)
	{
		___m_prePlayerIns_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePlayerIns_2), value);
	}

	inline static int32_t get_offset_of_m_dicPlayerInfo_Common_3() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_dicPlayerInfo_Common_3)); }
	inline Dictionary_2_t154955445 * get_m_dicPlayerInfo_Common_3() const { return ___m_dicPlayerInfo_Common_3; }
	inline Dictionary_2_t154955445 ** get_address_of_m_dicPlayerInfo_Common_3() { return &___m_dicPlayerInfo_Common_3; }
	inline void set_m_dicPlayerInfo_Common_3(Dictionary_2_t154955445 * value)
	{
		___m_dicPlayerInfo_Common_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayerInfo_Common_3), value);
	}

	inline static int32_t get_offset_of_m_dicPlayers_5() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_dicPlayers_5)); }
	inline Dictionary_2_t2155360643 * get_m_dicPlayers_5() const { return ___m_dicPlayers_5; }
	inline Dictionary_2_t2155360643 ** get_address_of_m_dicPlayers_5() { return &___m_dicPlayers_5; }
	inline void set_m_dicPlayers_5(Dictionary_2_t2155360643 * value)
	{
		___m_dicPlayers_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayers_5), value);
	}

	inline static int32_t get_offset_of_m_dicPlayerInfo_RealTime_6() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_dicPlayerInfo_RealTime_6)); }
	inline Dictionary_2_t3901903956 * get_m_dicPlayerInfo_RealTime_6() const { return ___m_dicPlayerInfo_RealTime_6; }
	inline Dictionary_2_t3901903956 ** get_address_of_m_dicPlayerInfo_RealTime_6() { return &___m_dicPlayerInfo_RealTime_6; }
	inline void set_m_dicPlayerInfo_RealTime_6(Dictionary_2_t3901903956 * value)
	{
		___m_dicPlayerInfo_RealTime_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayerInfo_RealTime_6), value);
	}

	inline static int32_t get_offset_of_m_szShengFuPlayerName_7() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_szShengFuPlayerName_7)); }
	inline String_t* get_m_szShengFuPlayerName_7() const { return ___m_szShengFuPlayerName_7; }
	inline String_t** get_address_of_m_szShengFuPlayerName_7() { return &___m_szShengFuPlayerName_7; }
	inline void set_m_szShengFuPlayerName_7(String_t* value)
	{
		___m_szShengFuPlayerName_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_szShengFuPlayerName_7), value);
	}

	inline static int32_t get_offset_of_m_dicPlayerIns_8() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_dicPlayerIns_8)); }
	inline Dictionary_2_t2993447027 * get_m_dicPlayerIns_8() const { return ___m_dicPlayerIns_8; }
	inline Dictionary_2_t2993447027 ** get_address_of_m_dicPlayerIns_8() { return &___m_dicPlayerIns_8; }
	inline void set_m_dicPlayerIns_8(Dictionary_2_t2993447027 * value)
	{
		___m_dicPlayerIns_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayerIns_8), value);
	}

	inline static int32_t get_offset_of_m_goContainerPlayerIns_9() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_goContainerPlayerIns_9)); }
	inline GameObject_t1113636619 * get_m_goContainerPlayerIns_9() const { return ___m_goContainerPlayerIns_9; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainerPlayerIns_9() { return &___m_goContainerPlayerIns_9; }
	inline void set_m_goContainerPlayerIns_9(GameObject_t1113636619 * value)
	{
		___m_goContainerPlayerIns_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerPlayerIns_9), value);
	}

	inline static int32_t get_offset_of_m_dicPlayerByAccount_10() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_dicPlayerByAccount_10)); }
	inline Dictionary_2_t3051903611 * get_m_dicPlayerByAccount_10() const { return ___m_dicPlayerByAccount_10; }
	inline Dictionary_2_t3051903611 ** get_address_of_m_dicPlayerByAccount_10() { return &___m_dicPlayerByAccount_10; }
	inline void set_m_dicPlayerByAccount_10(Dictionary_2_t3051903611 * value)
	{
		___m_dicPlayerByAccount_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayerByAccount_10), value);
	}

	inline static int32_t get_offset_of__byteOrphanBallDatat_11() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ____byteOrphanBallDatat_11)); }
	inline ByteU5BU5D_t4116647657* get__byteOrphanBallDatat_11() const { return ____byteOrphanBallDatat_11; }
	inline ByteU5BU5D_t4116647657** get_address_of__byteOrphanBallDatat_11() { return &____byteOrphanBallDatat_11; }
	inline void set__byteOrphanBallDatat_11(ByteU5BU5D_t4116647657* value)
	{
		____byteOrphanBallDatat_11 = value;
		Il2CppCodeGenWriteBarrier((&____byteOrphanBallDatat_11), value);
	}

	inline static int32_t get_offset_of_m_lstTempOrphanBallData_12() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028, ___m_lstTempOrphanBallData_12)); }
	inline List_1_t4261169767 * get_m_lstTempOrphanBallData_12() const { return ___m_lstTempOrphanBallData_12; }
	inline List_1_t4261169767 ** get_address_of_m_lstTempOrphanBallData_12() { return &___m_lstTempOrphanBallData_12; }
	inline void set_m_lstTempOrphanBallData_12(List_1_t4261169767 * value)
	{
		___m_lstTempOrphanBallData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstTempOrphanBallData_12), value);
	}
};

struct CPlayerManager_t2516829028_StaticFields
{
public:
	// CPlayerManager CPlayerManager::s_Instance
	CPlayerManager_t2516829028 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(CPlayerManager_t2516829028_StaticFields, ___s_Instance_4)); }
	inline CPlayerManager_t2516829028 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline CPlayerManager_t2516829028 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(CPlayerManager_t2516829028 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPLAYERMANAGER_T2516829028_H
#ifndef EASYTOUCHTRIGGER_T1019744654_H
#define EASYTOUCHTRIGGER_T1019744654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger
struct  EasyTouchTrigger_t1019744654  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver> HedgehogTeam.EasyTouch.EasyTouchTrigger::receivers
	List_1_t2329255381 * ___receivers_2;

public:
	inline static int32_t get_offset_of_receivers_2() { return static_cast<int32_t>(offsetof(EasyTouchTrigger_t1019744654, ___receivers_2)); }
	inline List_1_t2329255381 * get_receivers_2() const { return ___receivers_2; }
	inline List_1_t2329255381 ** get_address_of_receivers_2() { return &___receivers_2; }
	inline void set_receivers_2(List_1_t2329255381 * value)
	{
		___receivers_2 = value;
		Il2CppCodeGenWriteBarrier((&___receivers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYTOUCHTRIGGER_T1019744654_H
#ifndef DISTRICTANDROOMMANAGER_T4043287150_H
#define DISTRICTANDROOMMANAGER_T4043287150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DistrictAndRoomManager
struct  DistrictAndRoomManager_t4043287150  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DistrictAndRoomManager::_txtCurAccount
	Text_t1901882714 * ____txtCurAccount_3;
	// UnityEngine.UI.Text DistrictAndRoomManager::_txtHostAccount
	Text_t1901882714 * ____txtHostAccount_4;
	// UnityEngine.UI.Text DistrictAndRoomManager::_txtHostOrGuest
	Text_t1901882714 * ____txtHostOrGuest_5;
	// UnityEngine.UI.Text DistrictAndRoomManager::_txtCurSelectedDistrict
	Text_t1901882714 * ____txtCurSelectedDistrict_6;
	// UnityEngine.UI.Text DistrictAndRoomManager::_txtCurSelectedRoom
	Text_t1901882714 * ____txtCurSelectedRoom_7;
	// UnityEngine.UI.Dropdown DistrictAndRoomManager::_dropdownDistrict
	Dropdown_t2274391225 * ____dropdownDistrict_8;
	// UnityEngine.UI.Dropdown DistrictAndRoomManager::_dropdownRoom
	Dropdown_t2274391225 * ____dropdownRoom_9;

public:
	inline static int32_t get_offset_of__txtCurAccount_3() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____txtCurAccount_3)); }
	inline Text_t1901882714 * get__txtCurAccount_3() const { return ____txtCurAccount_3; }
	inline Text_t1901882714 ** get_address_of__txtCurAccount_3() { return &____txtCurAccount_3; }
	inline void set__txtCurAccount_3(Text_t1901882714 * value)
	{
		____txtCurAccount_3 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurAccount_3), value);
	}

	inline static int32_t get_offset_of__txtHostAccount_4() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____txtHostAccount_4)); }
	inline Text_t1901882714 * get__txtHostAccount_4() const { return ____txtHostAccount_4; }
	inline Text_t1901882714 ** get_address_of__txtHostAccount_4() { return &____txtHostAccount_4; }
	inline void set__txtHostAccount_4(Text_t1901882714 * value)
	{
		____txtHostAccount_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtHostAccount_4), value);
	}

	inline static int32_t get_offset_of__txtHostOrGuest_5() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____txtHostOrGuest_5)); }
	inline Text_t1901882714 * get__txtHostOrGuest_5() const { return ____txtHostOrGuest_5; }
	inline Text_t1901882714 ** get_address_of__txtHostOrGuest_5() { return &____txtHostOrGuest_5; }
	inline void set__txtHostOrGuest_5(Text_t1901882714 * value)
	{
		____txtHostOrGuest_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtHostOrGuest_5), value);
	}

	inline static int32_t get_offset_of__txtCurSelectedDistrict_6() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____txtCurSelectedDistrict_6)); }
	inline Text_t1901882714 * get__txtCurSelectedDistrict_6() const { return ____txtCurSelectedDistrict_6; }
	inline Text_t1901882714 ** get_address_of__txtCurSelectedDistrict_6() { return &____txtCurSelectedDistrict_6; }
	inline void set__txtCurSelectedDistrict_6(Text_t1901882714 * value)
	{
		____txtCurSelectedDistrict_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurSelectedDistrict_6), value);
	}

	inline static int32_t get_offset_of__txtCurSelectedRoom_7() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____txtCurSelectedRoom_7)); }
	inline Text_t1901882714 * get__txtCurSelectedRoom_7() const { return ____txtCurSelectedRoom_7; }
	inline Text_t1901882714 ** get_address_of__txtCurSelectedRoom_7() { return &____txtCurSelectedRoom_7; }
	inline void set__txtCurSelectedRoom_7(Text_t1901882714 * value)
	{
		____txtCurSelectedRoom_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurSelectedRoom_7), value);
	}

	inline static int32_t get_offset_of__dropdownDistrict_8() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____dropdownDistrict_8)); }
	inline Dropdown_t2274391225 * get__dropdownDistrict_8() const { return ____dropdownDistrict_8; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownDistrict_8() { return &____dropdownDistrict_8; }
	inline void set__dropdownDistrict_8(Dropdown_t2274391225 * value)
	{
		____dropdownDistrict_8 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownDistrict_8), value);
	}

	inline static int32_t get_offset_of__dropdownRoom_9() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150, ____dropdownRoom_9)); }
	inline Dropdown_t2274391225 * get__dropdownRoom_9() const { return ____dropdownRoom_9; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownRoom_9() { return &____dropdownRoom_9; }
	inline void set__dropdownRoom_9(Dropdown_t2274391225 * value)
	{
		____dropdownRoom_9 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownRoom_9), value);
	}
};

struct DistrictAndRoomManager_t4043287150_StaticFields
{
public:
	// DistrictAndRoomManager DistrictAndRoomManager::s_Instance
	DistrictAndRoomManager_t4043287150 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(DistrictAndRoomManager_t4043287150_StaticFields, ___s_Instance_2)); }
	inline DistrictAndRoomManager_t4043287150 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline DistrictAndRoomManager_t4043287150 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(DistrictAndRoomManager_t4043287150 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTRICTANDROOMMANAGER_T4043287150_H
#ifndef CSPORE_T10900549_H
#define CSPORE_T10900549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSpore
struct  CSpore_t10900549  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CSpore::m_nPlayerId
	int32_t ___m_nPlayerId_4;
	// System.UInt32 CSpore::m_uSporeId
	uint32_t ___m_uSporeId_5;
	// System.Single CSpore::m_fVolume
	float ___m_fVolume_6;
	// System.Single CSpore::m_fRadius
	float ___m_fRadius_7;
	// UnityEngine.Vector3 CSpore::m_vDir
	Vector3_t3722313464  ___m_vDir_8;
	// UnityEngine.Vector2 CSpore::m_vSpeed
	Vector2_t2156229523  ___m_vSpeed_9;
	// UnityEngine.Vector2 CSpore::m_vInitSpeed
	Vector2_t2156229523  ___m_vInitSpeed_10;
	// UnityEngine.Vector2 CSpore::m_vAcclerate
	Vector2_t2156229523  ___m_vAcclerate_11;
	// System.Boolean CSpore::m_bFinishedX
	bool ___m_bFinishedX_12;
	// System.Boolean CSpore::m_bFinishedY
	bool ___m_bFinishedY_13;
	// System.Single CSpore::m_fTotalEjectDistance
	float ___m_fTotalEjectDistance_14;
	// CSpore/eEjectMode CSpore::m_eEjectingStatus
	int32_t ___m_eEjectingStatus_15;
	// Ball CSpore::m_Ball
	Ball_t2206666566 * ___m_Ball_16;
	// UnityEngine.CircleCollider2D CSpore::_Collider
	CircleCollider2D_t662546754 * ____Collider_17;
	// UnityEngine.SpriteRenderer CSpore::_srMain
	SpriteRenderer_t3235626157 * ____srMain_18;
	// UnityEngine.GameObject CSpore::m_goContainer
	GameObject_t1113636619 * ___m_goContainer_19;
	// System.Boolean CSpore::m_bIsEjecting
	bool ___m_bIsEjecting_20;
	// System.Boolean CSpore::m_bAccelerating
	bool ___m_bAccelerating_21;
	// UnityEngine.Vector2 CSpore::m_vecA
	Vector2_t2156229523  ___m_vecA_22;
	// UnityEngine.Vector2 CSpore::m_vecEjectStartPos
	Vector2_t2156229523  ___m_vecEjectStartPos_23;
	// System.Boolean CSpore::m_bDead
	bool ___m_bDead_24;

public:
	inline static int32_t get_offset_of_m_nPlayerId_4() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_nPlayerId_4)); }
	inline int32_t get_m_nPlayerId_4() const { return ___m_nPlayerId_4; }
	inline int32_t* get_address_of_m_nPlayerId_4() { return &___m_nPlayerId_4; }
	inline void set_m_nPlayerId_4(int32_t value)
	{
		___m_nPlayerId_4 = value;
	}

	inline static int32_t get_offset_of_m_uSporeId_5() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_uSporeId_5)); }
	inline uint32_t get_m_uSporeId_5() const { return ___m_uSporeId_5; }
	inline uint32_t* get_address_of_m_uSporeId_5() { return &___m_uSporeId_5; }
	inline void set_m_uSporeId_5(uint32_t value)
	{
		___m_uSporeId_5 = value;
	}

	inline static int32_t get_offset_of_m_fVolume_6() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_fVolume_6)); }
	inline float get_m_fVolume_6() const { return ___m_fVolume_6; }
	inline float* get_address_of_m_fVolume_6() { return &___m_fVolume_6; }
	inline void set_m_fVolume_6(float value)
	{
		___m_fVolume_6 = value;
	}

	inline static int32_t get_offset_of_m_fRadius_7() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_fRadius_7)); }
	inline float get_m_fRadius_7() const { return ___m_fRadius_7; }
	inline float* get_address_of_m_fRadius_7() { return &___m_fRadius_7; }
	inline void set_m_fRadius_7(float value)
	{
		___m_fRadius_7 = value;
	}

	inline static int32_t get_offset_of_m_vDir_8() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_vDir_8)); }
	inline Vector3_t3722313464  get_m_vDir_8() const { return ___m_vDir_8; }
	inline Vector3_t3722313464 * get_address_of_m_vDir_8() { return &___m_vDir_8; }
	inline void set_m_vDir_8(Vector3_t3722313464  value)
	{
		___m_vDir_8 = value;
	}

	inline static int32_t get_offset_of_m_vSpeed_9() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_vSpeed_9)); }
	inline Vector2_t2156229523  get_m_vSpeed_9() const { return ___m_vSpeed_9; }
	inline Vector2_t2156229523 * get_address_of_m_vSpeed_9() { return &___m_vSpeed_9; }
	inline void set_m_vSpeed_9(Vector2_t2156229523  value)
	{
		___m_vSpeed_9 = value;
	}

	inline static int32_t get_offset_of_m_vInitSpeed_10() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_vInitSpeed_10)); }
	inline Vector2_t2156229523  get_m_vInitSpeed_10() const { return ___m_vInitSpeed_10; }
	inline Vector2_t2156229523 * get_address_of_m_vInitSpeed_10() { return &___m_vInitSpeed_10; }
	inline void set_m_vInitSpeed_10(Vector2_t2156229523  value)
	{
		___m_vInitSpeed_10 = value;
	}

	inline static int32_t get_offset_of_m_vAcclerate_11() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_vAcclerate_11)); }
	inline Vector2_t2156229523  get_m_vAcclerate_11() const { return ___m_vAcclerate_11; }
	inline Vector2_t2156229523 * get_address_of_m_vAcclerate_11() { return &___m_vAcclerate_11; }
	inline void set_m_vAcclerate_11(Vector2_t2156229523  value)
	{
		___m_vAcclerate_11 = value;
	}

	inline static int32_t get_offset_of_m_bFinishedX_12() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_bFinishedX_12)); }
	inline bool get_m_bFinishedX_12() const { return ___m_bFinishedX_12; }
	inline bool* get_address_of_m_bFinishedX_12() { return &___m_bFinishedX_12; }
	inline void set_m_bFinishedX_12(bool value)
	{
		___m_bFinishedX_12 = value;
	}

	inline static int32_t get_offset_of_m_bFinishedY_13() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_bFinishedY_13)); }
	inline bool get_m_bFinishedY_13() const { return ___m_bFinishedY_13; }
	inline bool* get_address_of_m_bFinishedY_13() { return &___m_bFinishedY_13; }
	inline void set_m_bFinishedY_13(bool value)
	{
		___m_bFinishedY_13 = value;
	}

	inline static int32_t get_offset_of_m_fTotalEjectDistance_14() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_fTotalEjectDistance_14)); }
	inline float get_m_fTotalEjectDistance_14() const { return ___m_fTotalEjectDistance_14; }
	inline float* get_address_of_m_fTotalEjectDistance_14() { return &___m_fTotalEjectDistance_14; }
	inline void set_m_fTotalEjectDistance_14(float value)
	{
		___m_fTotalEjectDistance_14 = value;
	}

	inline static int32_t get_offset_of_m_eEjectingStatus_15() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_eEjectingStatus_15)); }
	inline int32_t get_m_eEjectingStatus_15() const { return ___m_eEjectingStatus_15; }
	inline int32_t* get_address_of_m_eEjectingStatus_15() { return &___m_eEjectingStatus_15; }
	inline void set_m_eEjectingStatus_15(int32_t value)
	{
		___m_eEjectingStatus_15 = value;
	}

	inline static int32_t get_offset_of_m_Ball_16() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_Ball_16)); }
	inline Ball_t2206666566 * get_m_Ball_16() const { return ___m_Ball_16; }
	inline Ball_t2206666566 ** get_address_of_m_Ball_16() { return &___m_Ball_16; }
	inline void set_m_Ball_16(Ball_t2206666566 * value)
	{
		___m_Ball_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Ball_16), value);
	}

	inline static int32_t get_offset_of__Collider_17() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ____Collider_17)); }
	inline CircleCollider2D_t662546754 * get__Collider_17() const { return ____Collider_17; }
	inline CircleCollider2D_t662546754 ** get_address_of__Collider_17() { return &____Collider_17; }
	inline void set__Collider_17(CircleCollider2D_t662546754 * value)
	{
		____Collider_17 = value;
		Il2CppCodeGenWriteBarrier((&____Collider_17), value);
	}

	inline static int32_t get_offset_of__srMain_18() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ____srMain_18)); }
	inline SpriteRenderer_t3235626157 * get__srMain_18() const { return ____srMain_18; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMain_18() { return &____srMain_18; }
	inline void set__srMain_18(SpriteRenderer_t3235626157 * value)
	{
		____srMain_18 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_18), value);
	}

	inline static int32_t get_offset_of_m_goContainer_19() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_goContainer_19)); }
	inline GameObject_t1113636619 * get_m_goContainer_19() const { return ___m_goContainer_19; }
	inline GameObject_t1113636619 ** get_address_of_m_goContainer_19() { return &___m_goContainer_19; }
	inline void set_m_goContainer_19(GameObject_t1113636619 * value)
	{
		___m_goContainer_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainer_19), value);
	}

	inline static int32_t get_offset_of_m_bIsEjecting_20() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_bIsEjecting_20)); }
	inline bool get_m_bIsEjecting_20() const { return ___m_bIsEjecting_20; }
	inline bool* get_address_of_m_bIsEjecting_20() { return &___m_bIsEjecting_20; }
	inline void set_m_bIsEjecting_20(bool value)
	{
		___m_bIsEjecting_20 = value;
	}

	inline static int32_t get_offset_of_m_bAccelerating_21() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_bAccelerating_21)); }
	inline bool get_m_bAccelerating_21() const { return ___m_bAccelerating_21; }
	inline bool* get_address_of_m_bAccelerating_21() { return &___m_bAccelerating_21; }
	inline void set_m_bAccelerating_21(bool value)
	{
		___m_bAccelerating_21 = value;
	}

	inline static int32_t get_offset_of_m_vecA_22() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_vecA_22)); }
	inline Vector2_t2156229523  get_m_vecA_22() const { return ___m_vecA_22; }
	inline Vector2_t2156229523 * get_address_of_m_vecA_22() { return &___m_vecA_22; }
	inline void set_m_vecA_22(Vector2_t2156229523  value)
	{
		___m_vecA_22 = value;
	}

	inline static int32_t get_offset_of_m_vecEjectStartPos_23() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_vecEjectStartPos_23)); }
	inline Vector2_t2156229523  get_m_vecEjectStartPos_23() const { return ___m_vecEjectStartPos_23; }
	inline Vector2_t2156229523 * get_address_of_m_vecEjectStartPos_23() { return &___m_vecEjectStartPos_23; }
	inline void set_m_vecEjectStartPos_23(Vector2_t2156229523  value)
	{
		___m_vecEjectStartPos_23 = value;
	}

	inline static int32_t get_offset_of_m_bDead_24() { return static_cast<int32_t>(offsetof(CSpore_t10900549, ___m_bDead_24)); }
	inline bool get_m_bDead_24() const { return ___m_bDead_24; }
	inline bool* get_address_of_m_bDead_24() { return &___m_bDead_24; }
	inline void set_m_bDead_24(bool value)
	{
		___m_bDead_24 = value;
	}
};

struct CSpore_t10900549_StaticFields
{
public:
	// UnityEngine.Vector3 CSpore::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CSpore::vecTempSize
	Vector3_t3722313464  ___vecTempSize_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CSpore_t10900549_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempSize_3() { return static_cast<int32_t>(offsetof(CSpore_t10900549_StaticFields, ___vecTempSize_3)); }
	inline Vector3_t3722313464  get_vecTempSize_3() const { return ___vecTempSize_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempSize_3() { return &___vecTempSize_3; }
	inline void set_vecTempSize_3(Vector3_t3722313464  value)
	{
		___vecTempSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPORE_T10900549_H
#ifndef CYBERTREEMATH_T1979652797_H
#define CYBERTREEMATH_T1979652797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CyberTreeMath
struct  CyberTreeMath_t1979652797  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CyberTreeMath_t1979652797_StaticFields
{
public:
	// UnityEngine.Vector3 CyberTreeMath::s_vecTempPos
	Vector3_t3722313464  ___s_vecTempPos_2;
	// System.Single CyberTreeMath::c_fAngleToRadian
	float ___c_fAngleToRadian_3;

public:
	inline static int32_t get_offset_of_s_vecTempPos_2() { return static_cast<int32_t>(offsetof(CyberTreeMath_t1979652797_StaticFields, ___s_vecTempPos_2)); }
	inline Vector3_t3722313464  get_s_vecTempPos_2() const { return ___s_vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_s_vecTempPos_2() { return &___s_vecTempPos_2; }
	inline void set_s_vecTempPos_2(Vector3_t3722313464  value)
	{
		___s_vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_c_fAngleToRadian_3() { return static_cast<int32_t>(offsetof(CyberTreeMath_t1979652797_StaticFields, ___c_fAngleToRadian_3)); }
	inline float get_c_fAngleToRadian_3() const { return ___c_fAngleToRadian_3; }
	inline float* get_address_of_c_fAngleToRadian_3() { return &___c_fAngleToRadian_3; }
	inline void set_c_fAngleToRadian_3(float value)
	{
		___c_fAngleToRadian_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYBERTREEMATH_T1979652797_H
#ifndef QUICKBASE_T718481069_H
#define QUICKBASE_T718481069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase
struct  QuickBase_t718481069  : public MonoBehaviour_t3962482529
{
public:
	// System.String HedgehogTeam.EasyTouch.QuickBase::quickActionName
	String_t* ___quickActionName_2;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isMultiTouch
	bool ___isMultiTouch_3;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::is2Finger
	bool ___is2Finger_4;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isOnTouch
	bool ___isOnTouch_5;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::enablePickOverUI
	bool ___enablePickOverUI_6;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::resetPhysic
	bool ___resetPhysic_7;
	// HedgehogTeam.EasyTouch.QuickBase/DirectAction HedgehogTeam.EasyTouch.QuickBase::directAction
	int32_t ___directAction_8;
	// HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction HedgehogTeam.EasyTouch.QuickBase::axesAction
	int32_t ___axesAction_9;
	// System.Single HedgehogTeam.EasyTouch.QuickBase::sensibility
	float ___sensibility_10;
	// UnityEngine.CharacterController HedgehogTeam.EasyTouch.QuickBase::directCharacterController
	CharacterController_t1138636865 * ___directCharacterController_11;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::inverseAxisValue
	bool ___inverseAxisValue_12;
	// UnityEngine.Rigidbody HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody
	Rigidbody_t3916780224 * ___cachedRigidBody_13;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic
	bool ___isKinematic_14;
	// UnityEngine.Rigidbody2D HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody2D
	Rigidbody2D_t939494601 * ___cachedRigidBody2D_15;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic2D
	bool ___isKinematic2D_16;
	// HedgehogTeam.EasyTouch.QuickBase/GameObjectType HedgehogTeam.EasyTouch.QuickBase::realType
	int32_t ___realType_17;
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase::fingerIndex
	int32_t ___fingerIndex_18;

public:
	inline static int32_t get_offset_of_quickActionName_2() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___quickActionName_2)); }
	inline String_t* get_quickActionName_2() const { return ___quickActionName_2; }
	inline String_t** get_address_of_quickActionName_2() { return &___quickActionName_2; }
	inline void set_quickActionName_2(String_t* value)
	{
		___quickActionName_2 = value;
		Il2CppCodeGenWriteBarrier((&___quickActionName_2), value);
	}

	inline static int32_t get_offset_of_isMultiTouch_3() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isMultiTouch_3)); }
	inline bool get_isMultiTouch_3() const { return ___isMultiTouch_3; }
	inline bool* get_address_of_isMultiTouch_3() { return &___isMultiTouch_3; }
	inline void set_isMultiTouch_3(bool value)
	{
		___isMultiTouch_3 = value;
	}

	inline static int32_t get_offset_of_is2Finger_4() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___is2Finger_4)); }
	inline bool get_is2Finger_4() const { return ___is2Finger_4; }
	inline bool* get_address_of_is2Finger_4() { return &___is2Finger_4; }
	inline void set_is2Finger_4(bool value)
	{
		___is2Finger_4 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_5() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isOnTouch_5)); }
	inline bool get_isOnTouch_5() const { return ___isOnTouch_5; }
	inline bool* get_address_of_isOnTouch_5() { return &___isOnTouch_5; }
	inline void set_isOnTouch_5(bool value)
	{
		___isOnTouch_5 = value;
	}

	inline static int32_t get_offset_of_enablePickOverUI_6() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___enablePickOverUI_6)); }
	inline bool get_enablePickOverUI_6() const { return ___enablePickOverUI_6; }
	inline bool* get_address_of_enablePickOverUI_6() { return &___enablePickOverUI_6; }
	inline void set_enablePickOverUI_6(bool value)
	{
		___enablePickOverUI_6 = value;
	}

	inline static int32_t get_offset_of_resetPhysic_7() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___resetPhysic_7)); }
	inline bool get_resetPhysic_7() const { return ___resetPhysic_7; }
	inline bool* get_address_of_resetPhysic_7() { return &___resetPhysic_7; }
	inline void set_resetPhysic_7(bool value)
	{
		___resetPhysic_7 = value;
	}

	inline static int32_t get_offset_of_directAction_8() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___directAction_8)); }
	inline int32_t get_directAction_8() const { return ___directAction_8; }
	inline int32_t* get_address_of_directAction_8() { return &___directAction_8; }
	inline void set_directAction_8(int32_t value)
	{
		___directAction_8 = value;
	}

	inline static int32_t get_offset_of_axesAction_9() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___axesAction_9)); }
	inline int32_t get_axesAction_9() const { return ___axesAction_9; }
	inline int32_t* get_address_of_axesAction_9() { return &___axesAction_9; }
	inline void set_axesAction_9(int32_t value)
	{
		___axesAction_9 = value;
	}

	inline static int32_t get_offset_of_sensibility_10() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___sensibility_10)); }
	inline float get_sensibility_10() const { return ___sensibility_10; }
	inline float* get_address_of_sensibility_10() { return &___sensibility_10; }
	inline void set_sensibility_10(float value)
	{
		___sensibility_10 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_11() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___directCharacterController_11)); }
	inline CharacterController_t1138636865 * get_directCharacterController_11() const { return ___directCharacterController_11; }
	inline CharacterController_t1138636865 ** get_address_of_directCharacterController_11() { return &___directCharacterController_11; }
	inline void set_directCharacterController_11(CharacterController_t1138636865 * value)
	{
		___directCharacterController_11 = value;
		Il2CppCodeGenWriteBarrier((&___directCharacterController_11), value);
	}

	inline static int32_t get_offset_of_inverseAxisValue_12() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___inverseAxisValue_12)); }
	inline bool get_inverseAxisValue_12() const { return ___inverseAxisValue_12; }
	inline bool* get_address_of_inverseAxisValue_12() { return &___inverseAxisValue_12; }
	inline void set_inverseAxisValue_12(bool value)
	{
		___inverseAxisValue_12 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody_13() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___cachedRigidBody_13)); }
	inline Rigidbody_t3916780224 * get_cachedRigidBody_13() const { return ___cachedRigidBody_13; }
	inline Rigidbody_t3916780224 ** get_address_of_cachedRigidBody_13() { return &___cachedRigidBody_13; }
	inline void set_cachedRigidBody_13(Rigidbody_t3916780224 * value)
	{
		___cachedRigidBody_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody_13), value);
	}

	inline static int32_t get_offset_of_isKinematic_14() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isKinematic_14)); }
	inline bool get_isKinematic_14() const { return ___isKinematic_14; }
	inline bool* get_address_of_isKinematic_14() { return &___isKinematic_14; }
	inline void set_isKinematic_14(bool value)
	{
		___isKinematic_14 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody2D_15() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___cachedRigidBody2D_15)); }
	inline Rigidbody2D_t939494601 * get_cachedRigidBody2D_15() const { return ___cachedRigidBody2D_15; }
	inline Rigidbody2D_t939494601 ** get_address_of_cachedRigidBody2D_15() { return &___cachedRigidBody2D_15; }
	inline void set_cachedRigidBody2D_15(Rigidbody2D_t939494601 * value)
	{
		___cachedRigidBody2D_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRigidBody2D_15), value);
	}

	inline static int32_t get_offset_of_isKinematic2D_16() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___isKinematic2D_16)); }
	inline bool get_isKinematic2D_16() const { return ___isKinematic2D_16; }
	inline bool* get_address_of_isKinematic2D_16() { return &___isKinematic2D_16; }
	inline void set_isKinematic2D_16(bool value)
	{
		___isKinematic2D_16 = value;
	}

	inline static int32_t get_offset_of_realType_17() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___realType_17)); }
	inline int32_t get_realType_17() const { return ___realType_17; }
	inline int32_t* get_address_of_realType_17() { return &___realType_17; }
	inline void set_realType_17(int32_t value)
	{
		___realType_17 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_18() { return static_cast<int32_t>(offsetof(QuickBase_t718481069, ___fingerIndex_18)); }
	inline int32_t get_fingerIndex_18() const { return ___fingerIndex_18; }
	inline int32_t* get_address_of_fingerIndex_18() { return &___fingerIndex_18; }
	inline void set_fingerIndex_18(int32_t value)
	{
		___fingerIndex_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKBASE_T718481069_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_2;

public:
	inline static int32_t get_offset_of_pvCache_2() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_2)); }
	inline PhotonView_t2207721820 * get_pvCache_2() const { return ___pvCache_2; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_2() { return &___pvCache_2; }
	inline void set_pvCache_2(PhotonView_t2207721820 * value)
	{
		___pvCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef BALLTRIGGER_NEW_T3129829938_H
#define BALLTRIGGER_NEW_T3129829938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallTrigger_New
struct  BallTrigger_New_t3129829938  : public MonoBehaviour_t3962482529
{
public:
	// Ball BallTrigger_New::_ball
	Ball_t2206666566 * ____ball_3;
	// UnityEngine.Collider2D BallTrigger_New::_Trigger
	Collider2D_t2806799626 * ____Trigger_4;
	// Thorn BallTrigger_New::_thorn
	Thorn_t2540645209 * ____thorn_5;
	// BallTrigger_New/eTriggerType BallTrigger_New::_type
	int32_t ____type_6;

public:
	inline static int32_t get_offset_of__ball_3() { return static_cast<int32_t>(offsetof(BallTrigger_New_t3129829938, ____ball_3)); }
	inline Ball_t2206666566 * get__ball_3() const { return ____ball_3; }
	inline Ball_t2206666566 ** get_address_of__ball_3() { return &____ball_3; }
	inline void set__ball_3(Ball_t2206666566 * value)
	{
		____ball_3 = value;
		Il2CppCodeGenWriteBarrier((&____ball_3), value);
	}

	inline static int32_t get_offset_of__Trigger_4() { return static_cast<int32_t>(offsetof(BallTrigger_New_t3129829938, ____Trigger_4)); }
	inline Collider2D_t2806799626 * get__Trigger_4() const { return ____Trigger_4; }
	inline Collider2D_t2806799626 ** get_address_of__Trigger_4() { return &____Trigger_4; }
	inline void set__Trigger_4(Collider2D_t2806799626 * value)
	{
		____Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_4), value);
	}

	inline static int32_t get_offset_of__thorn_5() { return static_cast<int32_t>(offsetof(BallTrigger_New_t3129829938, ____thorn_5)); }
	inline Thorn_t2540645209 * get__thorn_5() const { return ____thorn_5; }
	inline Thorn_t2540645209 ** get_address_of__thorn_5() { return &____thorn_5; }
	inline void set__thorn_5(Thorn_t2540645209 * value)
	{
		____thorn_5 = value;
		Il2CppCodeGenWriteBarrier((&____thorn_5), value);
	}

	inline static int32_t get_offset_of__type_6() { return static_cast<int32_t>(offsetof(BallTrigger_New_t3129829938, ____type_6)); }
	inline int32_t get__type_6() const { return ____type_6; }
	inline int32_t* get_address_of__type_6() { return &____type_6; }
	inline void set__type_6(int32_t value)
	{
		____type_6 = value;
	}
};

struct BallTrigger_New_t3129829938_StaticFields
{
public:
	// UnityEngine.Vector2 BallTrigger_New::vec2Temp
	Vector2_t2156229523  ___vec2Temp_2;

public:
	inline static int32_t get_offset_of_vec2Temp_2() { return static_cast<int32_t>(offsetof(BallTrigger_New_t3129829938_StaticFields, ___vec2Temp_2)); }
	inline Vector2_t2156229523  get_vec2Temp_2() const { return ___vec2Temp_2; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_2() { return &___vec2Temp_2; }
	inline void set_vec2Temp_2(Vector2_t2156229523  value)
	{
		___vec2Temp_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLTRIGGER_NEW_T3129829938_H
#ifndef CBLOCK_T2659765146_H
#define CBLOCK_T2659765146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBlock
struct  CBlock_t2659765146  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CBlock::m_nHierarchy
	int32_t ___m_nHierarchy_4;
	// UnityEngine.Collider2D CBlock::_Collider
	Collider2D_t2806799626 * ____Collider_5;
	// CBlock[] CBlock::m_aryChildBlock
	CBlockU5BU5D_t3695288511* ___m_aryChildBlock_6;
	// CBlockDivide/eBlockPosType CBlock::m_ePosType
	int32_t ___m_ePosType_7;
	// CBlockDivide/eBlockPosType CBlock::m_eParentPosType
	int32_t ___m_eParentPosType_8;
	// System.UInt32 CBlock::m_uKey
	uint32_t ___m_uKey_9;
	// System.UInt32 CBlock::m_uParentKey
	uint32_t ___m_uParentKey_10;
	// System.UInt32 CBlock::m_uGuid
	uint32_t ___m_uGuid_11;
	// CBlock CBlock::m_blockParent
	CBlock_t2659765146 * ___m_blockParent_12;
	// UnityEngine.SpriteRenderer CBlock::m_sprMain
	SpriteRenderer_t3235626157 * ___m_sprMain_13;
	// System.Int32 CBlock::m_nBallNumInMe
	int32_t ___m_nBallNumInMe_14;

public:
	inline static int32_t get_offset_of_m_nHierarchy_4() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_nHierarchy_4)); }
	inline int32_t get_m_nHierarchy_4() const { return ___m_nHierarchy_4; }
	inline int32_t* get_address_of_m_nHierarchy_4() { return &___m_nHierarchy_4; }
	inline void set_m_nHierarchy_4(int32_t value)
	{
		___m_nHierarchy_4 = value;
	}

	inline static int32_t get_offset_of__Collider_5() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ____Collider_5)); }
	inline Collider2D_t2806799626 * get__Collider_5() const { return ____Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of__Collider_5() { return &____Collider_5; }
	inline void set__Collider_5(Collider2D_t2806799626 * value)
	{
		____Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&____Collider_5), value);
	}

	inline static int32_t get_offset_of_m_aryChildBlock_6() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_aryChildBlock_6)); }
	inline CBlockU5BU5D_t3695288511* get_m_aryChildBlock_6() const { return ___m_aryChildBlock_6; }
	inline CBlockU5BU5D_t3695288511** get_address_of_m_aryChildBlock_6() { return &___m_aryChildBlock_6; }
	inline void set_m_aryChildBlock_6(CBlockU5BU5D_t3695288511* value)
	{
		___m_aryChildBlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryChildBlock_6), value);
	}

	inline static int32_t get_offset_of_m_ePosType_7() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_ePosType_7)); }
	inline int32_t get_m_ePosType_7() const { return ___m_ePosType_7; }
	inline int32_t* get_address_of_m_ePosType_7() { return &___m_ePosType_7; }
	inline void set_m_ePosType_7(int32_t value)
	{
		___m_ePosType_7 = value;
	}

	inline static int32_t get_offset_of_m_eParentPosType_8() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_eParentPosType_8)); }
	inline int32_t get_m_eParentPosType_8() const { return ___m_eParentPosType_8; }
	inline int32_t* get_address_of_m_eParentPosType_8() { return &___m_eParentPosType_8; }
	inline void set_m_eParentPosType_8(int32_t value)
	{
		___m_eParentPosType_8 = value;
	}

	inline static int32_t get_offset_of_m_uKey_9() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_uKey_9)); }
	inline uint32_t get_m_uKey_9() const { return ___m_uKey_9; }
	inline uint32_t* get_address_of_m_uKey_9() { return &___m_uKey_9; }
	inline void set_m_uKey_9(uint32_t value)
	{
		___m_uKey_9 = value;
	}

	inline static int32_t get_offset_of_m_uParentKey_10() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_uParentKey_10)); }
	inline uint32_t get_m_uParentKey_10() const { return ___m_uParentKey_10; }
	inline uint32_t* get_address_of_m_uParentKey_10() { return &___m_uParentKey_10; }
	inline void set_m_uParentKey_10(uint32_t value)
	{
		___m_uParentKey_10 = value;
	}

	inline static int32_t get_offset_of_m_uGuid_11() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_uGuid_11)); }
	inline uint32_t get_m_uGuid_11() const { return ___m_uGuid_11; }
	inline uint32_t* get_address_of_m_uGuid_11() { return &___m_uGuid_11; }
	inline void set_m_uGuid_11(uint32_t value)
	{
		___m_uGuid_11 = value;
	}

	inline static int32_t get_offset_of_m_blockParent_12() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_blockParent_12)); }
	inline CBlock_t2659765146 * get_m_blockParent_12() const { return ___m_blockParent_12; }
	inline CBlock_t2659765146 ** get_address_of_m_blockParent_12() { return &___m_blockParent_12; }
	inline void set_m_blockParent_12(CBlock_t2659765146 * value)
	{
		___m_blockParent_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_blockParent_12), value);
	}

	inline static int32_t get_offset_of_m_sprMain_13() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_sprMain_13)); }
	inline SpriteRenderer_t3235626157 * get_m_sprMain_13() const { return ___m_sprMain_13; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_sprMain_13() { return &___m_sprMain_13; }
	inline void set_m_sprMain_13(SpriteRenderer_t3235626157 * value)
	{
		___m_sprMain_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprMain_13), value);
	}

	inline static int32_t get_offset_of_m_nBallNumInMe_14() { return static_cast<int32_t>(offsetof(CBlock_t2659765146, ___m_nBallNumInMe_14)); }
	inline int32_t get_m_nBallNumInMe_14() const { return ___m_nBallNumInMe_14; }
	inline int32_t* get_address_of_m_nBallNumInMe_14() { return &___m_nBallNumInMe_14; }
	inline void set_m_nBallNumInMe_14(int32_t value)
	{
		___m_nBallNumInMe_14 = value;
	}
};

struct CBlock_t2659765146_StaticFields
{
public:
	// UnityEngine.Vector3 CBlock::vecTempPos
	Vector3_t3722313464  ___vecTempPos_2;
	// UnityEngine.Vector3 CBlock::vecTempScale
	Vector3_t3722313464  ___vecTempScale_3;

public:
	inline static int32_t get_offset_of_vecTempPos_2() { return static_cast<int32_t>(offsetof(CBlock_t2659765146_StaticFields, ___vecTempPos_2)); }
	inline Vector3_t3722313464  get_vecTempPos_2() const { return ___vecTempPos_2; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_2() { return &___vecTempPos_2; }
	inline void set_vecTempPos_2(Vector3_t3722313464  value)
	{
		___vecTempPos_2 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_3() { return static_cast<int32_t>(offsetof(CBlock_t2659765146_StaticFields, ___vecTempScale_3)); }
	inline Vector3_t3722313464  get_vecTempScale_3() const { return ___vecTempScale_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_3() { return &___vecTempScale_3; }
	inline void set_vecTempScale_3(Vector3_t3722313464  value)
	{
		___vecTempScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBLOCK_T2659765146_H
#ifndef CBLOCKDIVIDE_T2736696553_H
#define CBLOCKDIVIDE_T2736696553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CBlockDivide
struct  CBlockDivide_t2736696553  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color[] CBlockDivide::m_aryLineColor
	ColorU5BU5D_t941916413* ___m_aryLineColor_6;
	// UnityEngine.Color[] CBlockDivide::m_aryBallColor
	ColorU5BU5D_t941916413* ___m_aryBallColor_7;
	// UnityEngine.GameObject CBlockDivide::m_preLine
	GameObject_t1113636619 * ___m_preLine_8;
	// UnityEngine.GameObject CBlockDivide::m_preBlock
	GameObject_t1113636619 * ___m_preBlock_9;
	// System.Single CBlockDivide::m_fWorldSize
	float ___m_fWorldSize_10;
	// System.Int32 CBlockDivide::m_nHierarchyNum
	int32_t ___m_nHierarchyNum_11;
	// System.Single[] CBlockDivide::m_aryBlockScaleOfEveryHierarchy
	SingleU5BU5D_t1444911251* ___m_aryBlockScaleOfEveryHierarchy_12;
	// System.UInt32[] CBlockDivide::m_aryHierachyReverseKey
	UInt32U5BU5D_t2770800703* ___m_aryHierachyReverseKey_13;
	// System.UInt32[] CBlockDivide::m_aryHierachyKey
	UInt32U5BU5D_t2770800703* ___m_aryHierachyKey_14;
	// UnityEngine.GameObject CBlockDivide::m_goArea0
	GameObject_t1113636619 * ___m_goArea0_15;
	// UnityEngine.GameObject CBlockDivide::m_goArea0_Stains
	GameObject_t1113636619 * ___m_goArea0_Stains_16;
	// System.Int32 CBlockDivide::m_nBlockNum
	int32_t ___m_nBlockNum_17;
	// UnityEngine.GameObject CBlockDivide::_ball
	GameObject_t1113636619 * ____ball_20;
	// System.Single CBlockDivide::m_fTimeElapse
	float ___m_fTimeElapse_21;
	// System.Boolean CBlockDivide::m_bMoving
	bool ___m_bMoving_22;
	// System.Collections.Generic.List`1<CBlock> CBlockDivide::m_lstRecycledBlocks
	List_1_t4131839888 * ___m_lstRecycledBlocks_23;
	// System.Collections.Generic.Dictionary`2<System.UInt32,CBlock> CBlockDivide::m_dicRecycledBlocks
	Dictionary_2_t3934032808 * ___m_dicRecycledBlocks_24;
	// System.Collections.Generic.Dictionary`2<System.UInt32,CBlock> CBlockDivide::m_dicBlocks
	Dictionary_2_t3934032808 * ___m_dicBlocks_26;

public:
	inline static int32_t get_offset_of_m_aryLineColor_6() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_aryLineColor_6)); }
	inline ColorU5BU5D_t941916413* get_m_aryLineColor_6() const { return ___m_aryLineColor_6; }
	inline ColorU5BU5D_t941916413** get_address_of_m_aryLineColor_6() { return &___m_aryLineColor_6; }
	inline void set_m_aryLineColor_6(ColorU5BU5D_t941916413* value)
	{
		___m_aryLineColor_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLineColor_6), value);
	}

	inline static int32_t get_offset_of_m_aryBallColor_7() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_aryBallColor_7)); }
	inline ColorU5BU5D_t941916413* get_m_aryBallColor_7() const { return ___m_aryBallColor_7; }
	inline ColorU5BU5D_t941916413** get_address_of_m_aryBallColor_7() { return &___m_aryBallColor_7; }
	inline void set_m_aryBallColor_7(ColorU5BU5D_t941916413* value)
	{
		___m_aryBallColor_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBallColor_7), value);
	}

	inline static int32_t get_offset_of_m_preLine_8() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_preLine_8)); }
	inline GameObject_t1113636619 * get_m_preLine_8() const { return ___m_preLine_8; }
	inline GameObject_t1113636619 ** get_address_of_m_preLine_8() { return &___m_preLine_8; }
	inline void set_m_preLine_8(GameObject_t1113636619 * value)
	{
		___m_preLine_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_preLine_8), value);
	}

	inline static int32_t get_offset_of_m_preBlock_9() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_preBlock_9)); }
	inline GameObject_t1113636619 * get_m_preBlock_9() const { return ___m_preBlock_9; }
	inline GameObject_t1113636619 ** get_address_of_m_preBlock_9() { return &___m_preBlock_9; }
	inline void set_m_preBlock_9(GameObject_t1113636619 * value)
	{
		___m_preBlock_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preBlock_9), value);
	}

	inline static int32_t get_offset_of_m_fWorldSize_10() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_fWorldSize_10)); }
	inline float get_m_fWorldSize_10() const { return ___m_fWorldSize_10; }
	inline float* get_address_of_m_fWorldSize_10() { return &___m_fWorldSize_10; }
	inline void set_m_fWorldSize_10(float value)
	{
		___m_fWorldSize_10 = value;
	}

	inline static int32_t get_offset_of_m_nHierarchyNum_11() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_nHierarchyNum_11)); }
	inline int32_t get_m_nHierarchyNum_11() const { return ___m_nHierarchyNum_11; }
	inline int32_t* get_address_of_m_nHierarchyNum_11() { return &___m_nHierarchyNum_11; }
	inline void set_m_nHierarchyNum_11(int32_t value)
	{
		___m_nHierarchyNum_11 = value;
	}

	inline static int32_t get_offset_of_m_aryBlockScaleOfEveryHierarchy_12() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_aryBlockScaleOfEveryHierarchy_12)); }
	inline SingleU5BU5D_t1444911251* get_m_aryBlockScaleOfEveryHierarchy_12() const { return ___m_aryBlockScaleOfEveryHierarchy_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_aryBlockScaleOfEveryHierarchy_12() { return &___m_aryBlockScaleOfEveryHierarchy_12; }
	inline void set_m_aryBlockScaleOfEveryHierarchy_12(SingleU5BU5D_t1444911251* value)
	{
		___m_aryBlockScaleOfEveryHierarchy_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBlockScaleOfEveryHierarchy_12), value);
	}

	inline static int32_t get_offset_of_m_aryHierachyReverseKey_13() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_aryHierachyReverseKey_13)); }
	inline UInt32U5BU5D_t2770800703* get_m_aryHierachyReverseKey_13() const { return ___m_aryHierachyReverseKey_13; }
	inline UInt32U5BU5D_t2770800703** get_address_of_m_aryHierachyReverseKey_13() { return &___m_aryHierachyReverseKey_13; }
	inline void set_m_aryHierachyReverseKey_13(UInt32U5BU5D_t2770800703* value)
	{
		___m_aryHierachyReverseKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryHierachyReverseKey_13), value);
	}

	inline static int32_t get_offset_of_m_aryHierachyKey_14() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_aryHierachyKey_14)); }
	inline UInt32U5BU5D_t2770800703* get_m_aryHierachyKey_14() const { return ___m_aryHierachyKey_14; }
	inline UInt32U5BU5D_t2770800703** get_address_of_m_aryHierachyKey_14() { return &___m_aryHierachyKey_14; }
	inline void set_m_aryHierachyKey_14(UInt32U5BU5D_t2770800703* value)
	{
		___m_aryHierachyKey_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryHierachyKey_14), value);
	}

	inline static int32_t get_offset_of_m_goArea0_15() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_goArea0_15)); }
	inline GameObject_t1113636619 * get_m_goArea0_15() const { return ___m_goArea0_15; }
	inline GameObject_t1113636619 ** get_address_of_m_goArea0_15() { return &___m_goArea0_15; }
	inline void set_m_goArea0_15(GameObject_t1113636619 * value)
	{
		___m_goArea0_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_goArea0_15), value);
	}

	inline static int32_t get_offset_of_m_goArea0_Stains_16() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_goArea0_Stains_16)); }
	inline GameObject_t1113636619 * get_m_goArea0_Stains_16() const { return ___m_goArea0_Stains_16; }
	inline GameObject_t1113636619 ** get_address_of_m_goArea0_Stains_16() { return &___m_goArea0_Stains_16; }
	inline void set_m_goArea0_Stains_16(GameObject_t1113636619 * value)
	{
		___m_goArea0_Stains_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_goArea0_Stains_16), value);
	}

	inline static int32_t get_offset_of_m_nBlockNum_17() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_nBlockNum_17)); }
	inline int32_t get_m_nBlockNum_17() const { return ___m_nBlockNum_17; }
	inline int32_t* get_address_of_m_nBlockNum_17() { return &___m_nBlockNum_17; }
	inline void set_m_nBlockNum_17(int32_t value)
	{
		___m_nBlockNum_17 = value;
	}

	inline static int32_t get_offset_of__ball_20() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ____ball_20)); }
	inline GameObject_t1113636619 * get__ball_20() const { return ____ball_20; }
	inline GameObject_t1113636619 ** get_address_of__ball_20() { return &____ball_20; }
	inline void set__ball_20(GameObject_t1113636619 * value)
	{
		____ball_20 = value;
		Il2CppCodeGenWriteBarrier((&____ball_20), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_21() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_fTimeElapse_21)); }
	inline float get_m_fTimeElapse_21() const { return ___m_fTimeElapse_21; }
	inline float* get_address_of_m_fTimeElapse_21() { return &___m_fTimeElapse_21; }
	inline void set_m_fTimeElapse_21(float value)
	{
		___m_fTimeElapse_21 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_22() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_bMoving_22)); }
	inline bool get_m_bMoving_22() const { return ___m_bMoving_22; }
	inline bool* get_address_of_m_bMoving_22() { return &___m_bMoving_22; }
	inline void set_m_bMoving_22(bool value)
	{
		___m_bMoving_22 = value;
	}

	inline static int32_t get_offset_of_m_lstRecycledBlocks_23() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_lstRecycledBlocks_23)); }
	inline List_1_t4131839888 * get_m_lstRecycledBlocks_23() const { return ___m_lstRecycledBlocks_23; }
	inline List_1_t4131839888 ** get_address_of_m_lstRecycledBlocks_23() { return &___m_lstRecycledBlocks_23; }
	inline void set_m_lstRecycledBlocks_23(List_1_t4131839888 * value)
	{
		___m_lstRecycledBlocks_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledBlocks_23), value);
	}

	inline static int32_t get_offset_of_m_dicRecycledBlocks_24() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_dicRecycledBlocks_24)); }
	inline Dictionary_2_t3934032808 * get_m_dicRecycledBlocks_24() const { return ___m_dicRecycledBlocks_24; }
	inline Dictionary_2_t3934032808 ** get_address_of_m_dicRecycledBlocks_24() { return &___m_dicRecycledBlocks_24; }
	inline void set_m_dicRecycledBlocks_24(Dictionary_2_t3934032808 * value)
	{
		___m_dicRecycledBlocks_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledBlocks_24), value);
	}

	inline static int32_t get_offset_of_m_dicBlocks_26() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553, ___m_dicBlocks_26)); }
	inline Dictionary_2_t3934032808 * get_m_dicBlocks_26() const { return ___m_dicBlocks_26; }
	inline Dictionary_2_t3934032808 ** get_address_of_m_dicBlocks_26() { return &___m_dicBlocks_26; }
	inline void set_m_dicBlocks_26(Dictionary_2_t3934032808 * value)
	{
		___m_dicBlocks_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBlocks_26), value);
	}
};

struct CBlockDivide_t2736696553_StaticFields
{
public:
	// CBlockDivide CBlockDivide::s_Instance
	CBlockDivide_t2736696553 * ___s_Instance_2;
	// UnityEngine.Vector3 CBlockDivide::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 CBlockDivide::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;
	// UnityEngine.Vector2 CBlockDivide::vecTempDir
	Vector2_t2156229523  ___vecTempDir_5;
	// System.UInt32 CBlockDivide::s_uBlockGuid
	uint32_t ___s_uBlockGuid_25;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553_StaticFields, ___s_Instance_2)); }
	inline CBlockDivide_t2736696553 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline CBlockDivide_t2736696553 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(CBlockDivide_t2736696553 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}

	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_5() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553_StaticFields, ___vecTempDir_5)); }
	inline Vector2_t2156229523  get_vecTempDir_5() const { return ___vecTempDir_5; }
	inline Vector2_t2156229523 * get_address_of_vecTempDir_5() { return &___vecTempDir_5; }
	inline void set_vecTempDir_5(Vector2_t2156229523  value)
	{
		___vecTempDir_5 = value;
	}

	inline static int32_t get_offset_of_s_uBlockGuid_25() { return static_cast<int32_t>(offsetof(CBlockDivide_t2736696553_StaticFields, ___s_uBlockGuid_25)); }
	inline uint32_t get_s_uBlockGuid_25() const { return ___s_uBlockGuid_25; }
	inline uint32_t* get_address_of_s_uBlockGuid_25() { return &___s_uBlockGuid_25; }
	inline void set_s_uBlockGuid_25(uint32_t value)
	{
		___s_uBlockGuid_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBLOCKDIVIDE_T2736696553_H
#ifndef COMOLIST_T2152284863_H
#define COMOLIST_T2152284863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ComoList
struct  ComoList_t2152284863  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ComoList::m_preListItem
	GameObject_t1113636619 * ___m_preListItem_2;
	// System.Collections.Generic.List`1<ListItem> ComoList::m_lstItems
	List_1_t1343534547 * ___m_lstItems_5;
	// UnityEngine.GameObject ComoList::m_goItemsContainer
	GameObject_t1113636619 * ___m_goItemsContainer_6;
	// UnityEngine.GameObject ComoList::m_goSelectedKuangKuang
	GameObject_t1113636619 * ___m_goSelectedKuangKuang_7;
	// System.Single ComoList::c_ItemGapHeight
	float ___c_ItemGapHeight_8;
	// System.Single ComoList::c_ItemHeight
	float ___c_ItemHeight_9;
	// System.Single ComoList::c_ItemWidth
	float ___c_ItemWidth_10;
	// ListItem ComoList::m_itemCurSelectedItem
	ListItem_t4166427101 * ___m_itemCurSelectedItem_12;
	// ComoList/DelegateMethod_OnSelected ComoList::delegateMethodOnSelect
	DelegateMethod_OnSelected_t3195943432 * ___delegateMethodOnSelect_13;

public:
	inline static int32_t get_offset_of_m_preListItem_2() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___m_preListItem_2)); }
	inline GameObject_t1113636619 * get_m_preListItem_2() const { return ___m_preListItem_2; }
	inline GameObject_t1113636619 ** get_address_of_m_preListItem_2() { return &___m_preListItem_2; }
	inline void set_m_preListItem_2(GameObject_t1113636619 * value)
	{
		___m_preListItem_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_preListItem_2), value);
	}

	inline static int32_t get_offset_of_m_lstItems_5() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___m_lstItems_5)); }
	inline List_1_t1343534547 * get_m_lstItems_5() const { return ___m_lstItems_5; }
	inline List_1_t1343534547 ** get_address_of_m_lstItems_5() { return &___m_lstItems_5; }
	inline void set_m_lstItems_5(List_1_t1343534547 * value)
	{
		___m_lstItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_5), value);
	}

	inline static int32_t get_offset_of_m_goItemsContainer_6() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___m_goItemsContainer_6)); }
	inline GameObject_t1113636619 * get_m_goItemsContainer_6() const { return ___m_goItemsContainer_6; }
	inline GameObject_t1113636619 ** get_address_of_m_goItemsContainer_6() { return &___m_goItemsContainer_6; }
	inline void set_m_goItemsContainer_6(GameObject_t1113636619 * value)
	{
		___m_goItemsContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_goItemsContainer_6), value);
	}

	inline static int32_t get_offset_of_m_goSelectedKuangKuang_7() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___m_goSelectedKuangKuang_7)); }
	inline GameObject_t1113636619 * get_m_goSelectedKuangKuang_7() const { return ___m_goSelectedKuangKuang_7; }
	inline GameObject_t1113636619 ** get_address_of_m_goSelectedKuangKuang_7() { return &___m_goSelectedKuangKuang_7; }
	inline void set_m_goSelectedKuangKuang_7(GameObject_t1113636619 * value)
	{
		___m_goSelectedKuangKuang_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_goSelectedKuangKuang_7), value);
	}

	inline static int32_t get_offset_of_c_ItemGapHeight_8() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___c_ItemGapHeight_8)); }
	inline float get_c_ItemGapHeight_8() const { return ___c_ItemGapHeight_8; }
	inline float* get_address_of_c_ItemGapHeight_8() { return &___c_ItemGapHeight_8; }
	inline void set_c_ItemGapHeight_8(float value)
	{
		___c_ItemGapHeight_8 = value;
	}

	inline static int32_t get_offset_of_c_ItemHeight_9() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___c_ItemHeight_9)); }
	inline float get_c_ItemHeight_9() const { return ___c_ItemHeight_9; }
	inline float* get_address_of_c_ItemHeight_9() { return &___c_ItemHeight_9; }
	inline void set_c_ItemHeight_9(float value)
	{
		___c_ItemHeight_9 = value;
	}

	inline static int32_t get_offset_of_c_ItemWidth_10() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___c_ItemWidth_10)); }
	inline float get_c_ItemWidth_10() const { return ___c_ItemWidth_10; }
	inline float* get_address_of_c_ItemWidth_10() { return &___c_ItemWidth_10; }
	inline void set_c_ItemWidth_10(float value)
	{
		___c_ItemWidth_10 = value;
	}

	inline static int32_t get_offset_of_m_itemCurSelectedItem_12() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___m_itemCurSelectedItem_12)); }
	inline ListItem_t4166427101 * get_m_itemCurSelectedItem_12() const { return ___m_itemCurSelectedItem_12; }
	inline ListItem_t4166427101 ** get_address_of_m_itemCurSelectedItem_12() { return &___m_itemCurSelectedItem_12; }
	inline void set_m_itemCurSelectedItem_12(ListItem_t4166427101 * value)
	{
		___m_itemCurSelectedItem_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_itemCurSelectedItem_12), value);
	}

	inline static int32_t get_offset_of_delegateMethodOnSelect_13() { return static_cast<int32_t>(offsetof(ComoList_t2152284863, ___delegateMethodOnSelect_13)); }
	inline DelegateMethod_OnSelected_t3195943432 * get_delegateMethodOnSelect_13() const { return ___delegateMethodOnSelect_13; }
	inline DelegateMethod_OnSelected_t3195943432 ** get_address_of_delegateMethodOnSelect_13() { return &___delegateMethodOnSelect_13; }
	inline void set_delegateMethodOnSelect_13(DelegateMethod_OnSelected_t3195943432 * value)
	{
		___delegateMethodOnSelect_13 = value;
		Il2CppCodeGenWriteBarrier((&___delegateMethodOnSelect_13), value);
	}
};

struct ComoList_t2152284863_StaticFields
{
public:
	// UnityEngine.Vector3 ComoList::vecTempPos
	Vector3_t3722313464  ___vecTempPos_3;
	// UnityEngine.Vector3 ComoList::vecTempScale
	Vector3_t3722313464  ___vecTempScale_4;

public:
	inline static int32_t get_offset_of_vecTempPos_3() { return static_cast<int32_t>(offsetof(ComoList_t2152284863_StaticFields, ___vecTempPos_3)); }
	inline Vector3_t3722313464  get_vecTempPos_3() const { return ___vecTempPos_3; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_3() { return &___vecTempPos_3; }
	inline void set_vecTempPos_3(Vector3_t3722313464  value)
	{
		___vecTempPos_3 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(ComoList_t2152284863_StaticFields, ___vecTempScale_4)); }
	inline Vector3_t3722313464  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_t3722313464  value)
	{
		___vecTempScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMOLIST_T2152284863_H
#ifndef BALL_T2206666566_H
#define BALL_T2206666566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball
struct  Ball_t2206666566  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Ball::_goUnfoldRing
	GameObject_t1113636619 * ____goUnfoldRing_2;
	// UnityEngine.SpriteRenderer Ball::_sprUnfoldSpr
	SpriteRenderer_t3235626157 * ____sprUnfoldSpr_3;
	// CArrow Ball::_arrowDir
	CArrow_t1475493256 * ____arrowDir_4;
	// CCosmosGunSight Ball::_gunsight
	CCosmosGunSight_t3630013301 * ____gunsight_5;
	// UnityEngine.Rendering.SortingGroup Ball::_sortingGroup
	SortingGroup_t3239126932 * ____sortingGroup_6;
	// UnityEngine.Rendering.SortingGroup Ball::_sortingGroup_mouth
	SortingGroup_t3239126932 * ____sortingGroup_mouth_7;
	// MeshShape Ball::m_msMain
	MeshShape_t3358450374 * ___m_msMain_9;
	// MeshShape Ball::m_msStaticShell
	MeshShape_t3358450374 * ___m_msStaticShell_10;
	// MeshShape Ball::m_msDynamicShell
	MeshShape_t3358450374 * ___m_msDynamicShell_11;
	// CSkillModuleForBall Ball::_skillModule
	CSkillModuleForBall_t1995272125 * ____skillModule_12;
	// UnityEngine.SpriteRenderer Ball::_srDustBling
	SpriteRenderer_t3235626157 * ____srDustBling_13;
	// CFrameAnimationEffect Ball::_effectPlayerLevelUp
	CFrameAnimationEffect_t443605508 * ____effectPlayerLevelUp_14;
	// CFrameAnimationEffect Ball::_effectSpitSmoke
	CFrameAnimationEffect_t443605508 * ____effectSpitSmoke_15;
	// UnityEngine.GameObject Ball::_goEffectContainer_KuangBao
	GameObject_t1113636619 * ____goEffectContainer_KuangBao_16;
	// CFrameAnimationEffect Ball::_effectKuangBao_QianYao
	CFrameAnimationEffect_t443605508 * ____effectKuangBao_QianYao_17;
	// CFrameAnimationEffect Ball::_effectKuangBao_Up
	CFrameAnimationEffect_t443605508 * ____effectKuangBao_Up_18;
	// CFrameAnimationEffect Ball::_effectKuangBao_Down
	CFrameAnimationEffect_t443605508 * ____effectKuangBao_Down_19;
	// CFrameAnimationEffect[] Ball::m_aryEffectQianZou
	CFrameAnimationEffectU5BU5D_t897573229* ___m_aryEffectQianZou_20;
	// CFrameAnimationEffect[] Ball::m_aryEffectChiXu
	CFrameAnimationEffectU5BU5D_t897573229* ___m_aryEffectChiXu_21;
	// CFrameAnimationEffect[] Ball::m_aryEffectChiXu1
	CFrameAnimationEffectU5BU5D_t897573229* ___m_aryEffectChiXu1_22;
	// Spine.Unity.SkeletonAnimation Ball::_skeaniEffectBianCi
	SkeletonAnimation_t3693186521 * ____skeaniEffectBianCi_23;
	// CFrameAnimationEffect Ball::_effectKill
	CFrameAnimationEffect_t443605508 * ____effectKill_24;
	// CEffect Ball::_effectMergeAll_QianYao
	CEffect_t3687947224 * ____effectMergeAll_QianYao_25;
	// UnityEngine.GameObject Ball::_goEffectContainer_QianYao
	GameObject_t1113636619 * ____goEffectContainer_QianYao_26;
	// MeshShape Ball::_effectUnfoldQianYao
	MeshShape_t3358450374 * ____effectUnfoldQianYao_27;
	// UnityEngine.GameObject Ball::_container
	GameObject_t1113636619 * ____container_28;
	// BallTrigger_New Ball::_scriptBallTrigger
	BallTrigger_New_t3129829938 * ____scriptBallTrigger_29;
	// Spine.Unity.SkeletonAnimation Ball::_skeletonAnimatnion
	SkeletonAnimation_t3693186521 * ____skeletonAnimatnion_30;
	// UnityEngine.MeshRenderer Ball::_mrSkeleton
	MeshRenderer_t587009260 * ____mrSkeleton_31;
	// UnityEngine.MeshRenderer Ball::_mrShellCircle
	MeshRenderer_t587009260 * ____mrShellCircle_32;
	// UnityEngine.GameObject Ball::_shell
	GameObject_t1113636619 * ____shell_33;
	// UnityEngine.GameObject Ball::_thorn
	GameObject_t1113636619 * ____thorn_34;
	// UnityEngine.CircleCollider2D Ball::_Trigger
	CircleCollider2D_t662546754 * ____Trigger_35;
	// UnityEngine.CircleCollider2D Ball::_TriggerMergeSelf
	CircleCollider2D_t662546754 * ____TriggerMergeSelf_36;
	// UnityEngine.CircleCollider2D Ball::_TriggerForBean
	CircleCollider2D_t662546754 * ____TriggerForBean_37;
	// UnityEngine.CircleCollider2D Ball::_TriggerForSpore
	CircleCollider2D_t662546754 * ____TriggerForSpore_38;
	// UnityEngine.CircleCollider2D Ball::_Collider
	CircleCollider2D_t662546754 * ____Collider_39;
	// UnityEngine.CircleCollider2D Ball::_ColliderTemp
	CircleCollider2D_t662546754 * ____ColliderTemp_40;
	// UnityEngine.CircleCollider2D Ball::_ColliderDust
	CircleCollider2D_t662546754 * ____ColliderDust_41;
	// UnityEngine.GameObject Ball::_Mouth
	GameObject_t1113636619 * ____Mouth_42;
	// UnityEngine.SpriteRenderer Ball::_srMouth
	SpriteRenderer_t3235626157 * ____srMouth_43;
	// UnityEngine.SpriteRenderer Ball::_srOutterRing
	SpriteRenderer_t3235626157 * ____srOutterRing_44;
	// UnityEngine.GameObject Ball::_main
	GameObject_t1113636619 * ____main_45;
	// UnityEngine.Rigidbody2D Ball::_rigid
	Rigidbody2D_t939494601 * ____rigid_46;
	// System.Boolean Ball::m_bIsEjecting
	bool ___m_bIsEjecting_47;
	// UnityEngine.Vector2 Ball::m_vDir
	Vector2_t2156229523  ___m_vDir_58;
	// UnityEngine.Vector2 Ball::m_vSpeed
	Vector2_t2156229523  ___m_vSpeed_59;
	// UnityEngine.Vector2 Ball::m_vInitSpeed
	Vector2_t2156229523  ___m_vInitSpeed_60;
	// UnityEngine.Vector2 Ball::m_vAcclerate
	Vector2_t2156229523  ___m_vAcclerate_61;
	// UnityEngine.GameObject Ball::_goPlayerName
	GameObject_t1113636619 * ____goPlayerName_62;
	// UnityEngine.UI.Text Ball::_txtPlayerName
	Text_t1901882714 * ____txtPlayerName_63;
	// UnityEngine.UI.Text Ball::_txtPlayerNameMiaoBian
	Text_t1901882714 * ____txtPlayerNameMiaoBian_64;
	// CEffect Ball::_effectPreUnfold
	CEffect_t3687947224 * ____effectPreUnfold_66;
	// System.Single Ball::_diry
	float ____diry_67;
	// UnityEngine.GameObject Ball::_obsceneCircle
	GameObject_t1113636619 * ____obsceneCircle_68;
	// System.Single Ball::m_fSpeed
	float ___m_fSpeed_69;
	// Player Ball::_Player
	Player_t3266647312 * ____Player_70;
	// CPlayerIns Ball::_PlayerIns
	CPlayerIns_t3208190728 * ____PlayerIns_71;
	// UnityEngine.GameObject Ball::_player
	GameObject_t1113636619 * ____player_72;
	// Ball/eBallType Ball::m_eBallType
	int32_t ___m_eBallType_73;
	// BallAction Ball::_ba
	BallAction_t576009403 * ____ba_74;
	// System.Boolean Ball::m_bDead
	bool ___m_bDead_75;
	// UnityEngine.SpriteRenderer Ball::m_sr
	SpriteRenderer_t3235626157 * ___m_sr_76;
	// System.Boolean Ball::m_bMatInit
	bool ___m_bMatInit_77;
	// System.Collections.Generic.List`1<Ball> Ball::m_lstIgnoredCollisionBalls
	List_1_t3678741308 * ___m_lstIgnoredCollisionBalls_78;
	// System.Boolean Ball::m_bSetNewPos
	bool ___m_bSetNewPos_79;
	// System.Int32[] Ball::m_aryEffectStatus
	Int32U5BU5D_t385246372* ___m_aryEffectStatus_80;
	// System.Byte Ball::m_byChiXuEffectIndex
	uint8_t ___m_byChiXuEffectIndex_81;
	// UnityEngine.MeshRenderer Ball::_shitRender
	MeshRenderer_t587009260 * ____shitRender_82;
	// System.Boolean Ball::m_bBallSkeletonMaterialSet
	bool ___m_bBallSkeletonMaterialSet_83;
	// System.Byte Ball::m_byPlayerLevel
	uint8_t ___m_byPlayerLevel_84;
	// System.Boolean Ball::m_bNeedSyncSize
	bool ___m_bNeedSyncSize_85;
	// System.Single Ball::m_fLastSyncSizeTime
	float ___m_fLastSyncSizeTime_86;
	// System.Single Ball::m_fDestSize
	float ___m_fDestSize_87;
	// System.Single Ball::m_fSizeUpdateSpeed
	float ___m_fSizeUpdateSpeed_88;
	// System.Boolean Ball::m_bSizeChanged
	bool ___m_bSizeChanged_91;
	// System.Boolean Ball::m_bSizeChanged_UnfoldSkill
	bool ___m_bSizeChanged_UnfoldSkill_92;
	// System.Single Ball::m_fSizeKaiGenHao
	float ___m_fSizeKaiGenHao_93;
	// System.Single Ball::m_fVolume
	float ___m_fVolume_94;
	// System.Single Ball::m_fRadius
	float ___m_fRadius_95;
	// System.Single Ball::m_fSize
	float ___m_fSize_96;
	// System.Single Ball::m_fVisionSize
	float ___m_fVisionSize_97;
	// System.Int32 Ball::m_bySizeChangeOp
	int32_t ___m_bySizeChangeOp_98;
	// System.Single Ball::m_fVisionSize_UnfoldSkill
	float ___m_fVisionSize_UnfoldSkill_99;
	// System.Single Ball::m_fSize_UnfoldSkill
	float ___m_fSize_UnfoldSkill_100;
	// UnityEngine.Vector2 Ball::m_vecEjectSpeed
	Vector2_t2156229523  ___m_vecEjectSpeed_101;
	// UnityEngine.Vector2 Ball::m_vecEjectStartPos
	Vector2_t2156229523  ___m_vecEjectStartPos_102;
	// System.Single Ball::m_fEjectTotalDis
	float ___m_fEjectTotalDis_103;
	// System.Boolean Ball::m_bUseColliderTemp
	bool ___m_bUseColliderTemp_104;
	// System.Single Ball::m_fStayTimeWhenEjectEnd
	float ___m_fStayTimeWhenEjectEnd_105;
	// System.Boolean Ball::m_bIgnoreDust
	bool ___m_bIgnoreDust_106;
	// System.Boolean Ball::m_bAccelerating
	bool ___m_bAccelerating_107;
	// UnityEngine.Vector2 Ball::m_vecA
	Vector2_t2156229523  ___m_vecA_108;
	// UnityEngine.Vector2 Ball::m_vecAccelerateSpeed
	Vector2_t2156229523  ___m_vecAccelerateSpeed_109;
	// UnityEngine.Vector2 Ball::m_vecV0
	Vector2_t2156229523  ___m_vecV0_110;
	// UnityEngine.Vector2 Ball::m_vecMotherMoveDir
	Vector2_t2156229523  ___m_vecMotherMoveDir_111;
	// System.Single Ball::m_fMotherRealTimeSpeed
	float ___m_fMotherRealTimeSpeed_112;
	// System.Boolean Ball::m_bAffectByMotherMove
	bool ___m_bAffectByMotherMove_113;
	// System.Single Ball::m_fCompensateDistance
	float ___m_fCompensateDistance_114;
	// System.Single Ball::m_fBaseDistance
	float ___m_fBaseDistance_115;
	// System.Single Ball::m_fTotalDistance
	float ___m_fTotalDistance_116;
	// System.Boolean Ball::m_bFinishedX
	bool ___m_bFinishedX_117;
	// System.Boolean Ball::m_bFinishedY
	bool ___m_bFinishedY_118;
	// Ball/eEjectMode Ball::m_eEjectingStatus
	int32_t ___m_eEjectingStatus_119;
	// System.Single Ball::m_fTempTimeCount
	float ___m_fTempTimeCount_120;
	// System.Single Ball::m_fInitSpeed
	float ___m_fInitSpeed_121;
	// System.Single Ball::m_fAccelerate
	float ___m_fAccelerate_122;
	// System.Boolean Ball::m_bStaying
	bool ___m_bStaying_123;
	// System.Int32 Ball::m_nUnfoldStatus
	int32_t ___m_nUnfoldStatus_124;
	// System.Boolean Ball::m_bPreunfolding
	bool ___m_bPreunfolding_125;
	// System.Single Ball::m_fUnfoldLeftTime
	float ___m_fUnfoldLeftTime_126;
	// System.Single Ball::m_fSizeBeforeUnfold
	float ___m_fSizeBeforeUnfold_127;
	// System.Single Ball::m_fMouthUnfoldScale
	float ___m_fMouthUnfoldScale_128;
	// System.Int32 Ball::m_nMouthUnfoldStatus
	int32_t ___m_nMouthUnfoldStatus_129;
	// System.Collections.Generic.List`1<CMonster> Ball::m_lstPkMonsterList
	List_1_t3413545680 * ___m_lstPkMonsterList_130;
	// System.Collections.Generic.List`1<Ball> Ball::m_lstPkListSelf
	List_1_t3678741308 * ___m_lstPkListSelf_131;
	// System.Collections.Generic.List`1<Ball> Ball::m_lstPkListOther
	List_1_t3678741308 * ___m_lstPkListOther_132;
	// System.Single Ball::m_fPreDrawDeltaTime
	float ___m_fPreDrawDeltaTime_135;
	// System.Single Ball::m_fThornSize
	float ___m_fThornSize_136;
	// System.Single Ball::m_fShitExplodeTime
	float ___m_fShitExplodeTime_138;
	// System.Single Ball::m_fMotherLeft
	float ___m_fMotherLeft_139;
	// System.Single Ball::m_fExplodeShellTime
	float ___m_fExplodeShellTime_140;
	// System.Int32 Ball::m_nTotalChildNum
	int32_t ___m_nTotalChildNum_141;
	// System.Int32 Ball::m_nCurChildNum
	int32_t ___m_nCurChildNum_142;
	// System.Boolean Ball::m_bExploding
	bool ___m_bExploding_144;
	// CMonsterEditor/sThornConfig Ball::m_ExplodeConfig
	sThornConfig_t1242444451  ___m_ExplodeConfig_145;
	// UnityEngine.Vector3 Ball::m_vecMotherPos
	Vector3_t3722313464  ___m_vecMotherPos_146;
	// System.Single Ball::m_fRunDistance
	float ___m_fRunDistance_147;
	// System.Single Ball::m_fRealRunTime
	float ___m_fRealRunTime_148;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Ball::m_lstDir
	List_1_t3628304265 * ___m_lstDir_149;
	// System.Single Ball::m_fChildSize
	float ___m_fChildSize_150;
	// System.Single Ball::m_fSegDis
	float ___m_fSegDis_151;
	// System.Int32[] Ball::m_lstChildBallsIndex
	Int32U5BU5D_t385246372* ___m_lstChildBallsIndex_152;
	// System.Int32 Ball::m_nExplodeRound
	int32_t ___m_nExplodeRound_153;
	// System.Collections.Generic.List`1<Ball/EatNode> Ball::m_lstEaten
	List_1_t2801720860 * ___m_lstEaten_154;
	// CGrass Ball::m_Grass
	CGrass_t734084476 * ___m_Grass_155;
	// System.Single Ball::m_fEnterGrassTime
	float ___m_fEnterGrassTime_156;
	// System.Collections.Generic.List`1<CGrass> Ball::m_lstGrass
	List_1_t2206159218 * ___m_lstGrass_157;
	// System.String Ball::m_szAccount
	String_t* ___m_szAccount_158;
	// System.Collections.Generic.List`1<CSpore> Ball::m_lstPreEatSpores
	List_1_t1482975291 * ___m_lstPreEatSpores_159;
	// System.Single Ball::m_fMotherLeftSize
	float ___m_fMotherLeftSize_160;
	// System.Single Ball::m_fChildThornSize
	float ___m_fChildThornSize_161;
	// System.Single Ball::m_fMotherLeftThornSize
	float ___m_fMotherLeftThornSize_162;
	// System.Boolean Ball::m_bShell
	bool ___m_bShell_163;
	// System.Single Ball::m_fShellShrinkCurTime
	float ___m_fShellShrinkCurTime_164;
	// System.Single Ball::m_fShellTotalTime
	float ___m_fShellTotalTime_165;
	// Ball/eShellType Ball::m_eShellType
	int32_t ___m_eShellType_166;
	// System.Single Ball::m_fBallTestValue
	float ___m_fBallTestValue_167;
	// System.Boolean Ball::m_bDynamicShellVisible
	bool ___m_bDynamicShellVisible_168;
	// System.Single Ball::m_fShellUnfoldScale
	float ___m_fShellUnfoldScale_169;
	// System.Single Ball::m_fTempShellCount
	float ___m_fTempShellCount_170;
	// System.Single Ball::m_fEndAngle
	float ___m_fEndAngle_171;
	// System.Single Ball::m_fSpitBallInitSpeed
	float ___m_fSpitBallInitSpeed_172;
	// System.Single Ball::m_fSpitBallAccelerate
	float ___m_fSpitBallAccelerate_173;
	// System.Single Ball::m_fExplodeInitSpeed
	float ___m_fExplodeInitSpeed_174;
	// System.Single Ball::m_fExplodeInitAccelerate
	float ___m_fExplodeInitAccelerate_175;
	// SpitBallTarget Ball::m_SpitBallTarget
	SpitBallTarget_t3997305173 * ___m_SpitBallTarget_176;
	// System.Single Ball::m_fSpitBallTargetParam
	float ___m_fSpitBallTargetParam_177;
	// System.Boolean Ball::m_bTargeting
	bool ___m_bTargeting_178;
	// SpitBallTarget Ball::m_SplitBallTarget
	SpitBallTarget_t3997305173 * ___m_SplitBallTarget_179;
	// SpitBallTarget Ball::_relateTarget
	SpitBallTarget_t3997305173 * ____relateTarget_180;
	// SpitBallTarget Ball::_relateSplitTarget
	SpitBallTarget_t3997305173 * ____relateSplitTarget_181;
	// System.Boolean Ball::m_bDoNotMove
	bool ___m_bDoNotMove_182;
	// System.Single Ball::m_fAdjustSpeedX
	float ___m_fAdjustSpeedX_183;
	// System.Single Ball::m_fAdjustSpeedY
	float ___m_fAdjustSpeedY_184;
	// System.Single Ball::m_fExtraSpeedX
	float ___m_fExtraSpeedX_185;
	// System.Single Ball::m_fExtraSpeedY
	float ___m_fExtraSpeedY_186;
	// System.Single Ball::m_fMoveSpeedX
	float ___m_fMoveSpeedX_188;
	// System.Single Ball::m_fMoveSpeedY
	float ___m_fMoveSpeedY_189;
	// UnityEngine.Vector2 Ball::m_vecdirection
	Vector2_t2156229523  ___m_vecdirection_190;
	// UnityEngine.Vector2 Ball::m_vecLastDirection
	Vector2_t2156229523  ___m_vecLastDirection_191;
	// System.Boolean Ball::m_bFirstCalculateMoveInfo
	bool ___m_bFirstCalculateMoveInfo_192;
	// UnityEngine.Vector2 Ball::m_vecMainSpeed
	Vector2_t2156229523  ___m_vecMainSpeed_193;
	// System.Boolean Ball::m_bInterpolateMoving
	bool ___m_bInterpolateMoving_195;
	// System.Single Ball::fChaZhiSpeedX
	float ___fChaZhiSpeedX_196;
	// System.Single Ball::fChaZhiSpeedY
	float ___fChaZhiSpeedY_197;
	// System.Boolean Ball::m_bbMovingToCenter
	bool ___m_bbMovingToCenter_198;
	// UnityEngine.Vector2 Ball::m_vecBallsCenter
	Vector2_t2156229523  ___m_vecBallsCenter_200;
	// System.Single Ball::m_fTotalMag
	float ___m_fTotalMag_201;
	// System.Int32 Ball::m_nCountMag
	int32_t ___m_nCountMag_202;
	// UnityEngine.Vector3 Ball::m_vecCurChaZhiDest
	Vector3_t3722313464  ___m_vecCurChaZhiDest_203;
	// UnityEngine.Vector2 Ball::m_vecCurMainPlayerSpeed
	Vector2_t2156229523  ___m_vecCurMainPlayerSpeed_204;
	// UnityEngine.Vector2 Ball::m_vecChaZhiSpeed
	Vector2_t2156229523  ___m_vecChaZhiSpeed_205;
	// System.Int32 Ball::m_nChaZhiCount
	int32_t ___m_nChaZhiCount_206;
	// System.Int32 Ball::m_nRealSplitNum
	int32_t ___m_nRealSplitNum_209;
	// System.Single Ball::m_fSplitSize
	float ___m_fSplitSize_210;
	// System.Single Ball::m_fSplitArea
	float ___m_fSplitArea_211;
	// System.Single Ball::m_fLeftArea
	float ___m_fLeftArea_212;
	// System.Single Ball::m_fTotalDis
	float ___m_fTotalDis_213;
	// System.Boolean Ball::m_bMotherDeadAfterSplit
	bool ___m_bMotherDeadAfterSplit_214;
	// System.Int32 Ball::m_nIndex
	int32_t ___m_nIndex_215;
	// System.Int32 Ball::m_nMovingToCenterStatus
	int32_t ___m_nMovingToCenterStatus_216;
	// System.Boolean Ball::m_bIsAdjustMoveProtect
	bool ___m_bIsAdjustMoveProtect_217;
	// System.Single Ball::m_fAdjustMoveProtectTime
	float ___m_fAdjustMoveProtectTime_218;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Ball::m_dicGroupRoundId
	Dictionary_2_t1839659084 * ___m_dicGroupRoundId_219;
	// System.Single Ball::m_fLocalOffsetX
	float ___m_fLocalOffsetX_220;
	// System.Single Ball::m_fLocalOffsetY
	float ___m_fLocalOffsetY_221;
	// System.Boolean Ball::m_bLockMove
	bool ___m_bLockMove_222;
	// System.Int32 Ball::m_nSizeClassId
	int32_t ___m_nSizeClassId_223;
	// System.Single Ball::m_fAttenuateSpeed
	float ___m_fAttenuateSpeed_224;
	// System.Single Ball::m_fClassInvasionAffectSpeed
	float ___m_fClassInvasionAffectSpeed_225;
	// System.Int32 Ball::m_nClassId
	int32_t ___m_nClassId_226;
	// System.Boolean Ball::m_bStayOnClassCircle
	bool ___m_bStayOnClassCircle_227;
	// System.Int32 Ball::m_nStayClassCircleId
	int32_t ___m_nStayClassCircleId_228;
	// System.Boolean Ball::m_bActive
	bool ___m_bActive_229;
	// System.Int32 Ball::m_nGuid
	int32_t ___m_nGuid_230;
	// System.Boolean Ball::m_bEaten
	bool ___m_bEaten_231;
	// UnityEngine.Vector3 Ball::m_vecDest
	Vector3_t3722313464  ___m_vecDest_232;
	// System.Boolean Ball::m_bIsMovingToDest
	bool ___m_bIsMovingToDest_233;
	// UnityEngine.Vector2 Ball::m_vecMoveToDestDir
	Vector2_t2156229523  ___m_vecMoveToDestDir_234;
	// System.Collections.Generic.Dictionary`2<System.Int32,CDust> Ball::m_dicDust
	Dictionary_2_t1909897157 * ___m_dicDust_235;
	// System.Int32 Ball::m_nOp
	int32_t ___m_nOp_236;
	// System.Int32 Ball::m_nOpScale
	int32_t ___m_nOpScale_237;
	// System.Single Ball::m_fBlingTime
	float ___m_fBlingTime_240;
	// System.Int32 Ball::m_nAutoMove
	int32_t ___m_nAutoMove_241;
	// CZhangYu Ball::m_goZhangYu
	CZhangYu_t3000622058 * ___m_goZhangYu_242;
	// System.Boolean Ball::bPlayerNameVisible
	bool ___bPlayerNameVisible_243;
	// System.Boolean Ball::bPlayerNameSizeControl
	bool ___bPlayerNameSizeControl_244;
	// System.Boolean Ball::m_bPicked
	bool ___m_bPicked_245;
	// CTiaoZi[] Ball::m_aryTiaoZiTunShi
	CTiaoZiU5BU5D_t3180227329* ___m_aryTiaoZiTunShi_246;
	// System.Collections.Generic.List`1<Ball> Ball::m_lstIgnoredTeammate
	List_1_t3678741308 * ___m_lstIgnoredTeammate_247;
	// System.Single Ball::m_flastTimeElapse
	float ___m_flastTimeElapse_248;

public:
	inline static int32_t get_offset_of__goUnfoldRing_2() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____goUnfoldRing_2)); }
	inline GameObject_t1113636619 * get__goUnfoldRing_2() const { return ____goUnfoldRing_2; }
	inline GameObject_t1113636619 ** get_address_of__goUnfoldRing_2() { return &____goUnfoldRing_2; }
	inline void set__goUnfoldRing_2(GameObject_t1113636619 * value)
	{
		____goUnfoldRing_2 = value;
		Il2CppCodeGenWriteBarrier((&____goUnfoldRing_2), value);
	}

	inline static int32_t get_offset_of__sprUnfoldSpr_3() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____sprUnfoldSpr_3)); }
	inline SpriteRenderer_t3235626157 * get__sprUnfoldSpr_3() const { return ____sprUnfoldSpr_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of__sprUnfoldSpr_3() { return &____sprUnfoldSpr_3; }
	inline void set__sprUnfoldSpr_3(SpriteRenderer_t3235626157 * value)
	{
		____sprUnfoldSpr_3 = value;
		Il2CppCodeGenWriteBarrier((&____sprUnfoldSpr_3), value);
	}

	inline static int32_t get_offset_of__arrowDir_4() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____arrowDir_4)); }
	inline CArrow_t1475493256 * get__arrowDir_4() const { return ____arrowDir_4; }
	inline CArrow_t1475493256 ** get_address_of__arrowDir_4() { return &____arrowDir_4; }
	inline void set__arrowDir_4(CArrow_t1475493256 * value)
	{
		____arrowDir_4 = value;
		Il2CppCodeGenWriteBarrier((&____arrowDir_4), value);
	}

	inline static int32_t get_offset_of__gunsight_5() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____gunsight_5)); }
	inline CCosmosGunSight_t3630013301 * get__gunsight_5() const { return ____gunsight_5; }
	inline CCosmosGunSight_t3630013301 ** get_address_of__gunsight_5() { return &____gunsight_5; }
	inline void set__gunsight_5(CCosmosGunSight_t3630013301 * value)
	{
		____gunsight_5 = value;
		Il2CppCodeGenWriteBarrier((&____gunsight_5), value);
	}

	inline static int32_t get_offset_of__sortingGroup_6() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____sortingGroup_6)); }
	inline SortingGroup_t3239126932 * get__sortingGroup_6() const { return ____sortingGroup_6; }
	inline SortingGroup_t3239126932 ** get_address_of__sortingGroup_6() { return &____sortingGroup_6; }
	inline void set__sortingGroup_6(SortingGroup_t3239126932 * value)
	{
		____sortingGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&____sortingGroup_6), value);
	}

	inline static int32_t get_offset_of__sortingGroup_mouth_7() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____sortingGroup_mouth_7)); }
	inline SortingGroup_t3239126932 * get__sortingGroup_mouth_7() const { return ____sortingGroup_mouth_7; }
	inline SortingGroup_t3239126932 ** get_address_of__sortingGroup_mouth_7() { return &____sortingGroup_mouth_7; }
	inline void set__sortingGroup_mouth_7(SortingGroup_t3239126932 * value)
	{
		____sortingGroup_mouth_7 = value;
		Il2CppCodeGenWriteBarrier((&____sortingGroup_mouth_7), value);
	}

	inline static int32_t get_offset_of_m_msMain_9() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_msMain_9)); }
	inline MeshShape_t3358450374 * get_m_msMain_9() const { return ___m_msMain_9; }
	inline MeshShape_t3358450374 ** get_address_of_m_msMain_9() { return &___m_msMain_9; }
	inline void set_m_msMain_9(MeshShape_t3358450374 * value)
	{
		___m_msMain_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_msMain_9), value);
	}

	inline static int32_t get_offset_of_m_msStaticShell_10() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_msStaticShell_10)); }
	inline MeshShape_t3358450374 * get_m_msStaticShell_10() const { return ___m_msStaticShell_10; }
	inline MeshShape_t3358450374 ** get_address_of_m_msStaticShell_10() { return &___m_msStaticShell_10; }
	inline void set_m_msStaticShell_10(MeshShape_t3358450374 * value)
	{
		___m_msStaticShell_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_msStaticShell_10), value);
	}

	inline static int32_t get_offset_of_m_msDynamicShell_11() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_msDynamicShell_11)); }
	inline MeshShape_t3358450374 * get_m_msDynamicShell_11() const { return ___m_msDynamicShell_11; }
	inline MeshShape_t3358450374 ** get_address_of_m_msDynamicShell_11() { return &___m_msDynamicShell_11; }
	inline void set_m_msDynamicShell_11(MeshShape_t3358450374 * value)
	{
		___m_msDynamicShell_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_msDynamicShell_11), value);
	}

	inline static int32_t get_offset_of__skillModule_12() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____skillModule_12)); }
	inline CSkillModuleForBall_t1995272125 * get__skillModule_12() const { return ____skillModule_12; }
	inline CSkillModuleForBall_t1995272125 ** get_address_of__skillModule_12() { return &____skillModule_12; }
	inline void set__skillModule_12(CSkillModuleForBall_t1995272125 * value)
	{
		____skillModule_12 = value;
		Il2CppCodeGenWriteBarrier((&____skillModule_12), value);
	}

	inline static int32_t get_offset_of__srDustBling_13() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____srDustBling_13)); }
	inline SpriteRenderer_t3235626157 * get__srDustBling_13() const { return ____srDustBling_13; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srDustBling_13() { return &____srDustBling_13; }
	inline void set__srDustBling_13(SpriteRenderer_t3235626157 * value)
	{
		____srDustBling_13 = value;
		Il2CppCodeGenWriteBarrier((&____srDustBling_13), value);
	}

	inline static int32_t get_offset_of__effectPlayerLevelUp_14() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectPlayerLevelUp_14)); }
	inline CFrameAnimationEffect_t443605508 * get__effectPlayerLevelUp_14() const { return ____effectPlayerLevelUp_14; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectPlayerLevelUp_14() { return &____effectPlayerLevelUp_14; }
	inline void set__effectPlayerLevelUp_14(CFrameAnimationEffect_t443605508 * value)
	{
		____effectPlayerLevelUp_14 = value;
		Il2CppCodeGenWriteBarrier((&____effectPlayerLevelUp_14), value);
	}

	inline static int32_t get_offset_of__effectSpitSmoke_15() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectSpitSmoke_15)); }
	inline CFrameAnimationEffect_t443605508 * get__effectSpitSmoke_15() const { return ____effectSpitSmoke_15; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectSpitSmoke_15() { return &____effectSpitSmoke_15; }
	inline void set__effectSpitSmoke_15(CFrameAnimationEffect_t443605508 * value)
	{
		____effectSpitSmoke_15 = value;
		Il2CppCodeGenWriteBarrier((&____effectSpitSmoke_15), value);
	}

	inline static int32_t get_offset_of__goEffectContainer_KuangBao_16() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____goEffectContainer_KuangBao_16)); }
	inline GameObject_t1113636619 * get__goEffectContainer_KuangBao_16() const { return ____goEffectContainer_KuangBao_16; }
	inline GameObject_t1113636619 ** get_address_of__goEffectContainer_KuangBao_16() { return &____goEffectContainer_KuangBao_16; }
	inline void set__goEffectContainer_KuangBao_16(GameObject_t1113636619 * value)
	{
		____goEffectContainer_KuangBao_16 = value;
		Il2CppCodeGenWriteBarrier((&____goEffectContainer_KuangBao_16), value);
	}

	inline static int32_t get_offset_of__effectKuangBao_QianYao_17() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectKuangBao_QianYao_17)); }
	inline CFrameAnimationEffect_t443605508 * get__effectKuangBao_QianYao_17() const { return ____effectKuangBao_QianYao_17; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectKuangBao_QianYao_17() { return &____effectKuangBao_QianYao_17; }
	inline void set__effectKuangBao_QianYao_17(CFrameAnimationEffect_t443605508 * value)
	{
		____effectKuangBao_QianYao_17 = value;
		Il2CppCodeGenWriteBarrier((&____effectKuangBao_QianYao_17), value);
	}

	inline static int32_t get_offset_of__effectKuangBao_Up_18() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectKuangBao_Up_18)); }
	inline CFrameAnimationEffect_t443605508 * get__effectKuangBao_Up_18() const { return ____effectKuangBao_Up_18; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectKuangBao_Up_18() { return &____effectKuangBao_Up_18; }
	inline void set__effectKuangBao_Up_18(CFrameAnimationEffect_t443605508 * value)
	{
		____effectKuangBao_Up_18 = value;
		Il2CppCodeGenWriteBarrier((&____effectKuangBao_Up_18), value);
	}

	inline static int32_t get_offset_of__effectKuangBao_Down_19() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectKuangBao_Down_19)); }
	inline CFrameAnimationEffect_t443605508 * get__effectKuangBao_Down_19() const { return ____effectKuangBao_Down_19; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectKuangBao_Down_19() { return &____effectKuangBao_Down_19; }
	inline void set__effectKuangBao_Down_19(CFrameAnimationEffect_t443605508 * value)
	{
		____effectKuangBao_Down_19 = value;
		Il2CppCodeGenWriteBarrier((&____effectKuangBao_Down_19), value);
	}

	inline static int32_t get_offset_of_m_aryEffectQianZou_20() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_aryEffectQianZou_20)); }
	inline CFrameAnimationEffectU5BU5D_t897573229* get_m_aryEffectQianZou_20() const { return ___m_aryEffectQianZou_20; }
	inline CFrameAnimationEffectU5BU5D_t897573229** get_address_of_m_aryEffectQianZou_20() { return &___m_aryEffectQianZou_20; }
	inline void set_m_aryEffectQianZou_20(CFrameAnimationEffectU5BU5D_t897573229* value)
	{
		___m_aryEffectQianZou_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEffectQianZou_20), value);
	}

	inline static int32_t get_offset_of_m_aryEffectChiXu_21() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_aryEffectChiXu_21)); }
	inline CFrameAnimationEffectU5BU5D_t897573229* get_m_aryEffectChiXu_21() const { return ___m_aryEffectChiXu_21; }
	inline CFrameAnimationEffectU5BU5D_t897573229** get_address_of_m_aryEffectChiXu_21() { return &___m_aryEffectChiXu_21; }
	inline void set_m_aryEffectChiXu_21(CFrameAnimationEffectU5BU5D_t897573229* value)
	{
		___m_aryEffectChiXu_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEffectChiXu_21), value);
	}

	inline static int32_t get_offset_of_m_aryEffectChiXu1_22() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_aryEffectChiXu1_22)); }
	inline CFrameAnimationEffectU5BU5D_t897573229* get_m_aryEffectChiXu1_22() const { return ___m_aryEffectChiXu1_22; }
	inline CFrameAnimationEffectU5BU5D_t897573229** get_address_of_m_aryEffectChiXu1_22() { return &___m_aryEffectChiXu1_22; }
	inline void set_m_aryEffectChiXu1_22(CFrameAnimationEffectU5BU5D_t897573229* value)
	{
		___m_aryEffectChiXu1_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEffectChiXu1_22), value);
	}

	inline static int32_t get_offset_of__skeaniEffectBianCi_23() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____skeaniEffectBianCi_23)); }
	inline SkeletonAnimation_t3693186521 * get__skeaniEffectBianCi_23() const { return ____skeaniEffectBianCi_23; }
	inline SkeletonAnimation_t3693186521 ** get_address_of__skeaniEffectBianCi_23() { return &____skeaniEffectBianCi_23; }
	inline void set__skeaniEffectBianCi_23(SkeletonAnimation_t3693186521 * value)
	{
		____skeaniEffectBianCi_23 = value;
		Il2CppCodeGenWriteBarrier((&____skeaniEffectBianCi_23), value);
	}

	inline static int32_t get_offset_of__effectKill_24() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectKill_24)); }
	inline CFrameAnimationEffect_t443605508 * get__effectKill_24() const { return ____effectKill_24; }
	inline CFrameAnimationEffect_t443605508 ** get_address_of__effectKill_24() { return &____effectKill_24; }
	inline void set__effectKill_24(CFrameAnimationEffect_t443605508 * value)
	{
		____effectKill_24 = value;
		Il2CppCodeGenWriteBarrier((&____effectKill_24), value);
	}

	inline static int32_t get_offset_of__effectMergeAll_QianYao_25() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectMergeAll_QianYao_25)); }
	inline CEffect_t3687947224 * get__effectMergeAll_QianYao_25() const { return ____effectMergeAll_QianYao_25; }
	inline CEffect_t3687947224 ** get_address_of__effectMergeAll_QianYao_25() { return &____effectMergeAll_QianYao_25; }
	inline void set__effectMergeAll_QianYao_25(CEffect_t3687947224 * value)
	{
		____effectMergeAll_QianYao_25 = value;
		Il2CppCodeGenWriteBarrier((&____effectMergeAll_QianYao_25), value);
	}

	inline static int32_t get_offset_of__goEffectContainer_QianYao_26() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____goEffectContainer_QianYao_26)); }
	inline GameObject_t1113636619 * get__goEffectContainer_QianYao_26() const { return ____goEffectContainer_QianYao_26; }
	inline GameObject_t1113636619 ** get_address_of__goEffectContainer_QianYao_26() { return &____goEffectContainer_QianYao_26; }
	inline void set__goEffectContainer_QianYao_26(GameObject_t1113636619 * value)
	{
		____goEffectContainer_QianYao_26 = value;
		Il2CppCodeGenWriteBarrier((&____goEffectContainer_QianYao_26), value);
	}

	inline static int32_t get_offset_of__effectUnfoldQianYao_27() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectUnfoldQianYao_27)); }
	inline MeshShape_t3358450374 * get__effectUnfoldQianYao_27() const { return ____effectUnfoldQianYao_27; }
	inline MeshShape_t3358450374 ** get_address_of__effectUnfoldQianYao_27() { return &____effectUnfoldQianYao_27; }
	inline void set__effectUnfoldQianYao_27(MeshShape_t3358450374 * value)
	{
		____effectUnfoldQianYao_27 = value;
		Il2CppCodeGenWriteBarrier((&____effectUnfoldQianYao_27), value);
	}

	inline static int32_t get_offset_of__container_28() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____container_28)); }
	inline GameObject_t1113636619 * get__container_28() const { return ____container_28; }
	inline GameObject_t1113636619 ** get_address_of__container_28() { return &____container_28; }
	inline void set__container_28(GameObject_t1113636619 * value)
	{
		____container_28 = value;
		Il2CppCodeGenWriteBarrier((&____container_28), value);
	}

	inline static int32_t get_offset_of__scriptBallTrigger_29() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____scriptBallTrigger_29)); }
	inline BallTrigger_New_t3129829938 * get__scriptBallTrigger_29() const { return ____scriptBallTrigger_29; }
	inline BallTrigger_New_t3129829938 ** get_address_of__scriptBallTrigger_29() { return &____scriptBallTrigger_29; }
	inline void set__scriptBallTrigger_29(BallTrigger_New_t3129829938 * value)
	{
		____scriptBallTrigger_29 = value;
		Il2CppCodeGenWriteBarrier((&____scriptBallTrigger_29), value);
	}

	inline static int32_t get_offset_of__skeletonAnimatnion_30() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____skeletonAnimatnion_30)); }
	inline SkeletonAnimation_t3693186521 * get__skeletonAnimatnion_30() const { return ____skeletonAnimatnion_30; }
	inline SkeletonAnimation_t3693186521 ** get_address_of__skeletonAnimatnion_30() { return &____skeletonAnimatnion_30; }
	inline void set__skeletonAnimatnion_30(SkeletonAnimation_t3693186521 * value)
	{
		____skeletonAnimatnion_30 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonAnimatnion_30), value);
	}

	inline static int32_t get_offset_of__mrSkeleton_31() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____mrSkeleton_31)); }
	inline MeshRenderer_t587009260 * get__mrSkeleton_31() const { return ____mrSkeleton_31; }
	inline MeshRenderer_t587009260 ** get_address_of__mrSkeleton_31() { return &____mrSkeleton_31; }
	inline void set__mrSkeleton_31(MeshRenderer_t587009260 * value)
	{
		____mrSkeleton_31 = value;
		Il2CppCodeGenWriteBarrier((&____mrSkeleton_31), value);
	}

	inline static int32_t get_offset_of__mrShellCircle_32() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____mrShellCircle_32)); }
	inline MeshRenderer_t587009260 * get__mrShellCircle_32() const { return ____mrShellCircle_32; }
	inline MeshRenderer_t587009260 ** get_address_of__mrShellCircle_32() { return &____mrShellCircle_32; }
	inline void set__mrShellCircle_32(MeshRenderer_t587009260 * value)
	{
		____mrShellCircle_32 = value;
		Il2CppCodeGenWriteBarrier((&____mrShellCircle_32), value);
	}

	inline static int32_t get_offset_of__shell_33() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____shell_33)); }
	inline GameObject_t1113636619 * get__shell_33() const { return ____shell_33; }
	inline GameObject_t1113636619 ** get_address_of__shell_33() { return &____shell_33; }
	inline void set__shell_33(GameObject_t1113636619 * value)
	{
		____shell_33 = value;
		Il2CppCodeGenWriteBarrier((&____shell_33), value);
	}

	inline static int32_t get_offset_of__thorn_34() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____thorn_34)); }
	inline GameObject_t1113636619 * get__thorn_34() const { return ____thorn_34; }
	inline GameObject_t1113636619 ** get_address_of__thorn_34() { return &____thorn_34; }
	inline void set__thorn_34(GameObject_t1113636619 * value)
	{
		____thorn_34 = value;
		Il2CppCodeGenWriteBarrier((&____thorn_34), value);
	}

	inline static int32_t get_offset_of__Trigger_35() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____Trigger_35)); }
	inline CircleCollider2D_t662546754 * get__Trigger_35() const { return ____Trigger_35; }
	inline CircleCollider2D_t662546754 ** get_address_of__Trigger_35() { return &____Trigger_35; }
	inline void set__Trigger_35(CircleCollider2D_t662546754 * value)
	{
		____Trigger_35 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_35), value);
	}

	inline static int32_t get_offset_of__TriggerMergeSelf_36() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____TriggerMergeSelf_36)); }
	inline CircleCollider2D_t662546754 * get__TriggerMergeSelf_36() const { return ____TriggerMergeSelf_36; }
	inline CircleCollider2D_t662546754 ** get_address_of__TriggerMergeSelf_36() { return &____TriggerMergeSelf_36; }
	inline void set__TriggerMergeSelf_36(CircleCollider2D_t662546754 * value)
	{
		____TriggerMergeSelf_36 = value;
		Il2CppCodeGenWriteBarrier((&____TriggerMergeSelf_36), value);
	}

	inline static int32_t get_offset_of__TriggerForBean_37() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____TriggerForBean_37)); }
	inline CircleCollider2D_t662546754 * get__TriggerForBean_37() const { return ____TriggerForBean_37; }
	inline CircleCollider2D_t662546754 ** get_address_of__TriggerForBean_37() { return &____TriggerForBean_37; }
	inline void set__TriggerForBean_37(CircleCollider2D_t662546754 * value)
	{
		____TriggerForBean_37 = value;
		Il2CppCodeGenWriteBarrier((&____TriggerForBean_37), value);
	}

	inline static int32_t get_offset_of__TriggerForSpore_38() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____TriggerForSpore_38)); }
	inline CircleCollider2D_t662546754 * get__TriggerForSpore_38() const { return ____TriggerForSpore_38; }
	inline CircleCollider2D_t662546754 ** get_address_of__TriggerForSpore_38() { return &____TriggerForSpore_38; }
	inline void set__TriggerForSpore_38(CircleCollider2D_t662546754 * value)
	{
		____TriggerForSpore_38 = value;
		Il2CppCodeGenWriteBarrier((&____TriggerForSpore_38), value);
	}

	inline static int32_t get_offset_of__Collider_39() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____Collider_39)); }
	inline CircleCollider2D_t662546754 * get__Collider_39() const { return ____Collider_39; }
	inline CircleCollider2D_t662546754 ** get_address_of__Collider_39() { return &____Collider_39; }
	inline void set__Collider_39(CircleCollider2D_t662546754 * value)
	{
		____Collider_39 = value;
		Il2CppCodeGenWriteBarrier((&____Collider_39), value);
	}

	inline static int32_t get_offset_of__ColliderTemp_40() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____ColliderTemp_40)); }
	inline CircleCollider2D_t662546754 * get__ColliderTemp_40() const { return ____ColliderTemp_40; }
	inline CircleCollider2D_t662546754 ** get_address_of__ColliderTemp_40() { return &____ColliderTemp_40; }
	inline void set__ColliderTemp_40(CircleCollider2D_t662546754 * value)
	{
		____ColliderTemp_40 = value;
		Il2CppCodeGenWriteBarrier((&____ColliderTemp_40), value);
	}

	inline static int32_t get_offset_of__ColliderDust_41() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____ColliderDust_41)); }
	inline CircleCollider2D_t662546754 * get__ColliderDust_41() const { return ____ColliderDust_41; }
	inline CircleCollider2D_t662546754 ** get_address_of__ColliderDust_41() { return &____ColliderDust_41; }
	inline void set__ColliderDust_41(CircleCollider2D_t662546754 * value)
	{
		____ColliderDust_41 = value;
		Il2CppCodeGenWriteBarrier((&____ColliderDust_41), value);
	}

	inline static int32_t get_offset_of__Mouth_42() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____Mouth_42)); }
	inline GameObject_t1113636619 * get__Mouth_42() const { return ____Mouth_42; }
	inline GameObject_t1113636619 ** get_address_of__Mouth_42() { return &____Mouth_42; }
	inline void set__Mouth_42(GameObject_t1113636619 * value)
	{
		____Mouth_42 = value;
		Il2CppCodeGenWriteBarrier((&____Mouth_42), value);
	}

	inline static int32_t get_offset_of__srMouth_43() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____srMouth_43)); }
	inline SpriteRenderer_t3235626157 * get__srMouth_43() const { return ____srMouth_43; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srMouth_43() { return &____srMouth_43; }
	inline void set__srMouth_43(SpriteRenderer_t3235626157 * value)
	{
		____srMouth_43 = value;
		Il2CppCodeGenWriteBarrier((&____srMouth_43), value);
	}

	inline static int32_t get_offset_of__srOutterRing_44() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____srOutterRing_44)); }
	inline SpriteRenderer_t3235626157 * get__srOutterRing_44() const { return ____srOutterRing_44; }
	inline SpriteRenderer_t3235626157 ** get_address_of__srOutterRing_44() { return &____srOutterRing_44; }
	inline void set__srOutterRing_44(SpriteRenderer_t3235626157 * value)
	{
		____srOutterRing_44 = value;
		Il2CppCodeGenWriteBarrier((&____srOutterRing_44), value);
	}

	inline static int32_t get_offset_of__main_45() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____main_45)); }
	inline GameObject_t1113636619 * get__main_45() const { return ____main_45; }
	inline GameObject_t1113636619 ** get_address_of__main_45() { return &____main_45; }
	inline void set__main_45(GameObject_t1113636619 * value)
	{
		____main_45 = value;
		Il2CppCodeGenWriteBarrier((&____main_45), value);
	}

	inline static int32_t get_offset_of__rigid_46() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____rigid_46)); }
	inline Rigidbody2D_t939494601 * get__rigid_46() const { return ____rigid_46; }
	inline Rigidbody2D_t939494601 ** get_address_of__rigid_46() { return &____rigid_46; }
	inline void set__rigid_46(Rigidbody2D_t939494601 * value)
	{
		____rigid_46 = value;
		Il2CppCodeGenWriteBarrier((&____rigid_46), value);
	}

	inline static int32_t get_offset_of_m_bIsEjecting_47() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bIsEjecting_47)); }
	inline bool get_m_bIsEjecting_47() const { return ___m_bIsEjecting_47; }
	inline bool* get_address_of_m_bIsEjecting_47() { return &___m_bIsEjecting_47; }
	inline void set_m_bIsEjecting_47(bool value)
	{
		___m_bIsEjecting_47 = value;
	}

	inline static int32_t get_offset_of_m_vDir_58() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vDir_58)); }
	inline Vector2_t2156229523  get_m_vDir_58() const { return ___m_vDir_58; }
	inline Vector2_t2156229523 * get_address_of_m_vDir_58() { return &___m_vDir_58; }
	inline void set_m_vDir_58(Vector2_t2156229523  value)
	{
		___m_vDir_58 = value;
	}

	inline static int32_t get_offset_of_m_vSpeed_59() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vSpeed_59)); }
	inline Vector2_t2156229523  get_m_vSpeed_59() const { return ___m_vSpeed_59; }
	inline Vector2_t2156229523 * get_address_of_m_vSpeed_59() { return &___m_vSpeed_59; }
	inline void set_m_vSpeed_59(Vector2_t2156229523  value)
	{
		___m_vSpeed_59 = value;
	}

	inline static int32_t get_offset_of_m_vInitSpeed_60() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vInitSpeed_60)); }
	inline Vector2_t2156229523  get_m_vInitSpeed_60() const { return ___m_vInitSpeed_60; }
	inline Vector2_t2156229523 * get_address_of_m_vInitSpeed_60() { return &___m_vInitSpeed_60; }
	inline void set_m_vInitSpeed_60(Vector2_t2156229523  value)
	{
		___m_vInitSpeed_60 = value;
	}

	inline static int32_t get_offset_of_m_vAcclerate_61() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vAcclerate_61)); }
	inline Vector2_t2156229523  get_m_vAcclerate_61() const { return ___m_vAcclerate_61; }
	inline Vector2_t2156229523 * get_address_of_m_vAcclerate_61() { return &___m_vAcclerate_61; }
	inline void set_m_vAcclerate_61(Vector2_t2156229523  value)
	{
		___m_vAcclerate_61 = value;
	}

	inline static int32_t get_offset_of__goPlayerName_62() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____goPlayerName_62)); }
	inline GameObject_t1113636619 * get__goPlayerName_62() const { return ____goPlayerName_62; }
	inline GameObject_t1113636619 ** get_address_of__goPlayerName_62() { return &____goPlayerName_62; }
	inline void set__goPlayerName_62(GameObject_t1113636619 * value)
	{
		____goPlayerName_62 = value;
		Il2CppCodeGenWriteBarrier((&____goPlayerName_62), value);
	}

	inline static int32_t get_offset_of__txtPlayerName_63() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____txtPlayerName_63)); }
	inline Text_t1901882714 * get__txtPlayerName_63() const { return ____txtPlayerName_63; }
	inline Text_t1901882714 ** get_address_of__txtPlayerName_63() { return &____txtPlayerName_63; }
	inline void set__txtPlayerName_63(Text_t1901882714 * value)
	{
		____txtPlayerName_63 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerName_63), value);
	}

	inline static int32_t get_offset_of__txtPlayerNameMiaoBian_64() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____txtPlayerNameMiaoBian_64)); }
	inline Text_t1901882714 * get__txtPlayerNameMiaoBian_64() const { return ____txtPlayerNameMiaoBian_64; }
	inline Text_t1901882714 ** get_address_of__txtPlayerNameMiaoBian_64() { return &____txtPlayerNameMiaoBian_64; }
	inline void set__txtPlayerNameMiaoBian_64(Text_t1901882714 * value)
	{
		____txtPlayerNameMiaoBian_64 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlayerNameMiaoBian_64), value);
	}

	inline static int32_t get_offset_of__effectPreUnfold_66() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____effectPreUnfold_66)); }
	inline CEffect_t3687947224 * get__effectPreUnfold_66() const { return ____effectPreUnfold_66; }
	inline CEffect_t3687947224 ** get_address_of__effectPreUnfold_66() { return &____effectPreUnfold_66; }
	inline void set__effectPreUnfold_66(CEffect_t3687947224 * value)
	{
		____effectPreUnfold_66 = value;
		Il2CppCodeGenWriteBarrier((&____effectPreUnfold_66), value);
	}

	inline static int32_t get_offset_of__diry_67() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____diry_67)); }
	inline float get__diry_67() const { return ____diry_67; }
	inline float* get_address_of__diry_67() { return &____diry_67; }
	inline void set__diry_67(float value)
	{
		____diry_67 = value;
	}

	inline static int32_t get_offset_of__obsceneCircle_68() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____obsceneCircle_68)); }
	inline GameObject_t1113636619 * get__obsceneCircle_68() const { return ____obsceneCircle_68; }
	inline GameObject_t1113636619 ** get_address_of__obsceneCircle_68() { return &____obsceneCircle_68; }
	inline void set__obsceneCircle_68(GameObject_t1113636619 * value)
	{
		____obsceneCircle_68 = value;
		Il2CppCodeGenWriteBarrier((&____obsceneCircle_68), value);
	}

	inline static int32_t get_offset_of_m_fSpeed_69() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSpeed_69)); }
	inline float get_m_fSpeed_69() const { return ___m_fSpeed_69; }
	inline float* get_address_of_m_fSpeed_69() { return &___m_fSpeed_69; }
	inline void set_m_fSpeed_69(float value)
	{
		___m_fSpeed_69 = value;
	}

	inline static int32_t get_offset_of__Player_70() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____Player_70)); }
	inline Player_t3266647312 * get__Player_70() const { return ____Player_70; }
	inline Player_t3266647312 ** get_address_of__Player_70() { return &____Player_70; }
	inline void set__Player_70(Player_t3266647312 * value)
	{
		____Player_70 = value;
		Il2CppCodeGenWriteBarrier((&____Player_70), value);
	}

	inline static int32_t get_offset_of__PlayerIns_71() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____PlayerIns_71)); }
	inline CPlayerIns_t3208190728 * get__PlayerIns_71() const { return ____PlayerIns_71; }
	inline CPlayerIns_t3208190728 ** get_address_of__PlayerIns_71() { return &____PlayerIns_71; }
	inline void set__PlayerIns_71(CPlayerIns_t3208190728 * value)
	{
		____PlayerIns_71 = value;
		Il2CppCodeGenWriteBarrier((&____PlayerIns_71), value);
	}

	inline static int32_t get_offset_of__player_72() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____player_72)); }
	inline GameObject_t1113636619 * get__player_72() const { return ____player_72; }
	inline GameObject_t1113636619 ** get_address_of__player_72() { return &____player_72; }
	inline void set__player_72(GameObject_t1113636619 * value)
	{
		____player_72 = value;
		Il2CppCodeGenWriteBarrier((&____player_72), value);
	}

	inline static int32_t get_offset_of_m_eBallType_73() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_eBallType_73)); }
	inline int32_t get_m_eBallType_73() const { return ___m_eBallType_73; }
	inline int32_t* get_address_of_m_eBallType_73() { return &___m_eBallType_73; }
	inline void set_m_eBallType_73(int32_t value)
	{
		___m_eBallType_73 = value;
	}

	inline static int32_t get_offset_of__ba_74() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____ba_74)); }
	inline BallAction_t576009403 * get__ba_74() const { return ____ba_74; }
	inline BallAction_t576009403 ** get_address_of__ba_74() { return &____ba_74; }
	inline void set__ba_74(BallAction_t576009403 * value)
	{
		____ba_74 = value;
		Il2CppCodeGenWriteBarrier((&____ba_74), value);
	}

	inline static int32_t get_offset_of_m_bDead_75() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bDead_75)); }
	inline bool get_m_bDead_75() const { return ___m_bDead_75; }
	inline bool* get_address_of_m_bDead_75() { return &___m_bDead_75; }
	inline void set_m_bDead_75(bool value)
	{
		___m_bDead_75 = value;
	}

	inline static int32_t get_offset_of_m_sr_76() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_sr_76)); }
	inline SpriteRenderer_t3235626157 * get_m_sr_76() const { return ___m_sr_76; }
	inline SpriteRenderer_t3235626157 ** get_address_of_m_sr_76() { return &___m_sr_76; }
	inline void set_m_sr_76(SpriteRenderer_t3235626157 * value)
	{
		___m_sr_76 = value;
		Il2CppCodeGenWriteBarrier((&___m_sr_76), value);
	}

	inline static int32_t get_offset_of_m_bMatInit_77() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bMatInit_77)); }
	inline bool get_m_bMatInit_77() const { return ___m_bMatInit_77; }
	inline bool* get_address_of_m_bMatInit_77() { return &___m_bMatInit_77; }
	inline void set_m_bMatInit_77(bool value)
	{
		___m_bMatInit_77 = value;
	}

	inline static int32_t get_offset_of_m_lstIgnoredCollisionBalls_78() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstIgnoredCollisionBalls_78)); }
	inline List_1_t3678741308 * get_m_lstIgnoredCollisionBalls_78() const { return ___m_lstIgnoredCollisionBalls_78; }
	inline List_1_t3678741308 ** get_address_of_m_lstIgnoredCollisionBalls_78() { return &___m_lstIgnoredCollisionBalls_78; }
	inline void set_m_lstIgnoredCollisionBalls_78(List_1_t3678741308 * value)
	{
		___m_lstIgnoredCollisionBalls_78 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstIgnoredCollisionBalls_78), value);
	}

	inline static int32_t get_offset_of_m_bSetNewPos_79() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bSetNewPos_79)); }
	inline bool get_m_bSetNewPos_79() const { return ___m_bSetNewPos_79; }
	inline bool* get_address_of_m_bSetNewPos_79() { return &___m_bSetNewPos_79; }
	inline void set_m_bSetNewPos_79(bool value)
	{
		___m_bSetNewPos_79 = value;
	}

	inline static int32_t get_offset_of_m_aryEffectStatus_80() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_aryEffectStatus_80)); }
	inline Int32U5BU5D_t385246372* get_m_aryEffectStatus_80() const { return ___m_aryEffectStatus_80; }
	inline Int32U5BU5D_t385246372** get_address_of_m_aryEffectStatus_80() { return &___m_aryEffectStatus_80; }
	inline void set_m_aryEffectStatus_80(Int32U5BU5D_t385246372* value)
	{
		___m_aryEffectStatus_80 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEffectStatus_80), value);
	}

	inline static int32_t get_offset_of_m_byChiXuEffectIndex_81() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_byChiXuEffectIndex_81)); }
	inline uint8_t get_m_byChiXuEffectIndex_81() const { return ___m_byChiXuEffectIndex_81; }
	inline uint8_t* get_address_of_m_byChiXuEffectIndex_81() { return &___m_byChiXuEffectIndex_81; }
	inline void set_m_byChiXuEffectIndex_81(uint8_t value)
	{
		___m_byChiXuEffectIndex_81 = value;
	}

	inline static int32_t get_offset_of__shitRender_82() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____shitRender_82)); }
	inline MeshRenderer_t587009260 * get__shitRender_82() const { return ____shitRender_82; }
	inline MeshRenderer_t587009260 ** get_address_of__shitRender_82() { return &____shitRender_82; }
	inline void set__shitRender_82(MeshRenderer_t587009260 * value)
	{
		____shitRender_82 = value;
		Il2CppCodeGenWriteBarrier((&____shitRender_82), value);
	}

	inline static int32_t get_offset_of_m_bBallSkeletonMaterialSet_83() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bBallSkeletonMaterialSet_83)); }
	inline bool get_m_bBallSkeletonMaterialSet_83() const { return ___m_bBallSkeletonMaterialSet_83; }
	inline bool* get_address_of_m_bBallSkeletonMaterialSet_83() { return &___m_bBallSkeletonMaterialSet_83; }
	inline void set_m_bBallSkeletonMaterialSet_83(bool value)
	{
		___m_bBallSkeletonMaterialSet_83 = value;
	}

	inline static int32_t get_offset_of_m_byPlayerLevel_84() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_byPlayerLevel_84)); }
	inline uint8_t get_m_byPlayerLevel_84() const { return ___m_byPlayerLevel_84; }
	inline uint8_t* get_address_of_m_byPlayerLevel_84() { return &___m_byPlayerLevel_84; }
	inline void set_m_byPlayerLevel_84(uint8_t value)
	{
		___m_byPlayerLevel_84 = value;
	}

	inline static int32_t get_offset_of_m_bNeedSyncSize_85() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bNeedSyncSize_85)); }
	inline bool get_m_bNeedSyncSize_85() const { return ___m_bNeedSyncSize_85; }
	inline bool* get_address_of_m_bNeedSyncSize_85() { return &___m_bNeedSyncSize_85; }
	inline void set_m_bNeedSyncSize_85(bool value)
	{
		___m_bNeedSyncSize_85 = value;
	}

	inline static int32_t get_offset_of_m_fLastSyncSizeTime_86() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fLastSyncSizeTime_86)); }
	inline float get_m_fLastSyncSizeTime_86() const { return ___m_fLastSyncSizeTime_86; }
	inline float* get_address_of_m_fLastSyncSizeTime_86() { return &___m_fLastSyncSizeTime_86; }
	inline void set_m_fLastSyncSizeTime_86(float value)
	{
		___m_fLastSyncSizeTime_86 = value;
	}

	inline static int32_t get_offset_of_m_fDestSize_87() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fDestSize_87)); }
	inline float get_m_fDestSize_87() const { return ___m_fDestSize_87; }
	inline float* get_address_of_m_fDestSize_87() { return &___m_fDestSize_87; }
	inline void set_m_fDestSize_87(float value)
	{
		___m_fDestSize_87 = value;
	}

	inline static int32_t get_offset_of_m_fSizeUpdateSpeed_88() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSizeUpdateSpeed_88)); }
	inline float get_m_fSizeUpdateSpeed_88() const { return ___m_fSizeUpdateSpeed_88; }
	inline float* get_address_of_m_fSizeUpdateSpeed_88() { return &___m_fSizeUpdateSpeed_88; }
	inline void set_m_fSizeUpdateSpeed_88(float value)
	{
		___m_fSizeUpdateSpeed_88 = value;
	}

	inline static int32_t get_offset_of_m_bSizeChanged_91() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bSizeChanged_91)); }
	inline bool get_m_bSizeChanged_91() const { return ___m_bSizeChanged_91; }
	inline bool* get_address_of_m_bSizeChanged_91() { return &___m_bSizeChanged_91; }
	inline void set_m_bSizeChanged_91(bool value)
	{
		___m_bSizeChanged_91 = value;
	}

	inline static int32_t get_offset_of_m_bSizeChanged_UnfoldSkill_92() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bSizeChanged_UnfoldSkill_92)); }
	inline bool get_m_bSizeChanged_UnfoldSkill_92() const { return ___m_bSizeChanged_UnfoldSkill_92; }
	inline bool* get_address_of_m_bSizeChanged_UnfoldSkill_92() { return &___m_bSizeChanged_UnfoldSkill_92; }
	inline void set_m_bSizeChanged_UnfoldSkill_92(bool value)
	{
		___m_bSizeChanged_UnfoldSkill_92 = value;
	}

	inline static int32_t get_offset_of_m_fSizeKaiGenHao_93() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSizeKaiGenHao_93)); }
	inline float get_m_fSizeKaiGenHao_93() const { return ___m_fSizeKaiGenHao_93; }
	inline float* get_address_of_m_fSizeKaiGenHao_93() { return &___m_fSizeKaiGenHao_93; }
	inline void set_m_fSizeKaiGenHao_93(float value)
	{
		___m_fSizeKaiGenHao_93 = value;
	}

	inline static int32_t get_offset_of_m_fVolume_94() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fVolume_94)); }
	inline float get_m_fVolume_94() const { return ___m_fVolume_94; }
	inline float* get_address_of_m_fVolume_94() { return &___m_fVolume_94; }
	inline void set_m_fVolume_94(float value)
	{
		___m_fVolume_94 = value;
	}

	inline static int32_t get_offset_of_m_fRadius_95() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fRadius_95)); }
	inline float get_m_fRadius_95() const { return ___m_fRadius_95; }
	inline float* get_address_of_m_fRadius_95() { return &___m_fRadius_95; }
	inline void set_m_fRadius_95(float value)
	{
		___m_fRadius_95 = value;
	}

	inline static int32_t get_offset_of_m_fSize_96() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSize_96)); }
	inline float get_m_fSize_96() const { return ___m_fSize_96; }
	inline float* get_address_of_m_fSize_96() { return &___m_fSize_96; }
	inline void set_m_fSize_96(float value)
	{
		___m_fSize_96 = value;
	}

	inline static int32_t get_offset_of_m_fVisionSize_97() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fVisionSize_97)); }
	inline float get_m_fVisionSize_97() const { return ___m_fVisionSize_97; }
	inline float* get_address_of_m_fVisionSize_97() { return &___m_fVisionSize_97; }
	inline void set_m_fVisionSize_97(float value)
	{
		___m_fVisionSize_97 = value;
	}

	inline static int32_t get_offset_of_m_bySizeChangeOp_98() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bySizeChangeOp_98)); }
	inline int32_t get_m_bySizeChangeOp_98() const { return ___m_bySizeChangeOp_98; }
	inline int32_t* get_address_of_m_bySizeChangeOp_98() { return &___m_bySizeChangeOp_98; }
	inline void set_m_bySizeChangeOp_98(int32_t value)
	{
		___m_bySizeChangeOp_98 = value;
	}

	inline static int32_t get_offset_of_m_fVisionSize_UnfoldSkill_99() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fVisionSize_UnfoldSkill_99)); }
	inline float get_m_fVisionSize_UnfoldSkill_99() const { return ___m_fVisionSize_UnfoldSkill_99; }
	inline float* get_address_of_m_fVisionSize_UnfoldSkill_99() { return &___m_fVisionSize_UnfoldSkill_99; }
	inline void set_m_fVisionSize_UnfoldSkill_99(float value)
	{
		___m_fVisionSize_UnfoldSkill_99 = value;
	}

	inline static int32_t get_offset_of_m_fSize_UnfoldSkill_100() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSize_UnfoldSkill_100)); }
	inline float get_m_fSize_UnfoldSkill_100() const { return ___m_fSize_UnfoldSkill_100; }
	inline float* get_address_of_m_fSize_UnfoldSkill_100() { return &___m_fSize_UnfoldSkill_100; }
	inline void set_m_fSize_UnfoldSkill_100(float value)
	{
		___m_fSize_UnfoldSkill_100 = value;
	}

	inline static int32_t get_offset_of_m_vecEjectSpeed_101() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecEjectSpeed_101)); }
	inline Vector2_t2156229523  get_m_vecEjectSpeed_101() const { return ___m_vecEjectSpeed_101; }
	inline Vector2_t2156229523 * get_address_of_m_vecEjectSpeed_101() { return &___m_vecEjectSpeed_101; }
	inline void set_m_vecEjectSpeed_101(Vector2_t2156229523  value)
	{
		___m_vecEjectSpeed_101 = value;
	}

	inline static int32_t get_offset_of_m_vecEjectStartPos_102() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecEjectStartPos_102)); }
	inline Vector2_t2156229523  get_m_vecEjectStartPos_102() const { return ___m_vecEjectStartPos_102; }
	inline Vector2_t2156229523 * get_address_of_m_vecEjectStartPos_102() { return &___m_vecEjectStartPos_102; }
	inline void set_m_vecEjectStartPos_102(Vector2_t2156229523  value)
	{
		___m_vecEjectStartPos_102 = value;
	}

	inline static int32_t get_offset_of_m_fEjectTotalDis_103() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fEjectTotalDis_103)); }
	inline float get_m_fEjectTotalDis_103() const { return ___m_fEjectTotalDis_103; }
	inline float* get_address_of_m_fEjectTotalDis_103() { return &___m_fEjectTotalDis_103; }
	inline void set_m_fEjectTotalDis_103(float value)
	{
		___m_fEjectTotalDis_103 = value;
	}

	inline static int32_t get_offset_of_m_bUseColliderTemp_104() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bUseColliderTemp_104)); }
	inline bool get_m_bUseColliderTemp_104() const { return ___m_bUseColliderTemp_104; }
	inline bool* get_address_of_m_bUseColliderTemp_104() { return &___m_bUseColliderTemp_104; }
	inline void set_m_bUseColliderTemp_104(bool value)
	{
		___m_bUseColliderTemp_104 = value;
	}

	inline static int32_t get_offset_of_m_fStayTimeWhenEjectEnd_105() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fStayTimeWhenEjectEnd_105)); }
	inline float get_m_fStayTimeWhenEjectEnd_105() const { return ___m_fStayTimeWhenEjectEnd_105; }
	inline float* get_address_of_m_fStayTimeWhenEjectEnd_105() { return &___m_fStayTimeWhenEjectEnd_105; }
	inline void set_m_fStayTimeWhenEjectEnd_105(float value)
	{
		___m_fStayTimeWhenEjectEnd_105 = value;
	}

	inline static int32_t get_offset_of_m_bIgnoreDust_106() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bIgnoreDust_106)); }
	inline bool get_m_bIgnoreDust_106() const { return ___m_bIgnoreDust_106; }
	inline bool* get_address_of_m_bIgnoreDust_106() { return &___m_bIgnoreDust_106; }
	inline void set_m_bIgnoreDust_106(bool value)
	{
		___m_bIgnoreDust_106 = value;
	}

	inline static int32_t get_offset_of_m_bAccelerating_107() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bAccelerating_107)); }
	inline bool get_m_bAccelerating_107() const { return ___m_bAccelerating_107; }
	inline bool* get_address_of_m_bAccelerating_107() { return &___m_bAccelerating_107; }
	inline void set_m_bAccelerating_107(bool value)
	{
		___m_bAccelerating_107 = value;
	}

	inline static int32_t get_offset_of_m_vecA_108() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecA_108)); }
	inline Vector2_t2156229523  get_m_vecA_108() const { return ___m_vecA_108; }
	inline Vector2_t2156229523 * get_address_of_m_vecA_108() { return &___m_vecA_108; }
	inline void set_m_vecA_108(Vector2_t2156229523  value)
	{
		___m_vecA_108 = value;
	}

	inline static int32_t get_offset_of_m_vecAccelerateSpeed_109() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecAccelerateSpeed_109)); }
	inline Vector2_t2156229523  get_m_vecAccelerateSpeed_109() const { return ___m_vecAccelerateSpeed_109; }
	inline Vector2_t2156229523 * get_address_of_m_vecAccelerateSpeed_109() { return &___m_vecAccelerateSpeed_109; }
	inline void set_m_vecAccelerateSpeed_109(Vector2_t2156229523  value)
	{
		___m_vecAccelerateSpeed_109 = value;
	}

	inline static int32_t get_offset_of_m_vecV0_110() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecV0_110)); }
	inline Vector2_t2156229523  get_m_vecV0_110() const { return ___m_vecV0_110; }
	inline Vector2_t2156229523 * get_address_of_m_vecV0_110() { return &___m_vecV0_110; }
	inline void set_m_vecV0_110(Vector2_t2156229523  value)
	{
		___m_vecV0_110 = value;
	}

	inline static int32_t get_offset_of_m_vecMotherMoveDir_111() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecMotherMoveDir_111)); }
	inline Vector2_t2156229523  get_m_vecMotherMoveDir_111() const { return ___m_vecMotherMoveDir_111; }
	inline Vector2_t2156229523 * get_address_of_m_vecMotherMoveDir_111() { return &___m_vecMotherMoveDir_111; }
	inline void set_m_vecMotherMoveDir_111(Vector2_t2156229523  value)
	{
		___m_vecMotherMoveDir_111 = value;
	}

	inline static int32_t get_offset_of_m_fMotherRealTimeSpeed_112() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMotherRealTimeSpeed_112)); }
	inline float get_m_fMotherRealTimeSpeed_112() const { return ___m_fMotherRealTimeSpeed_112; }
	inline float* get_address_of_m_fMotherRealTimeSpeed_112() { return &___m_fMotherRealTimeSpeed_112; }
	inline void set_m_fMotherRealTimeSpeed_112(float value)
	{
		___m_fMotherRealTimeSpeed_112 = value;
	}

	inline static int32_t get_offset_of_m_bAffectByMotherMove_113() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bAffectByMotherMove_113)); }
	inline bool get_m_bAffectByMotherMove_113() const { return ___m_bAffectByMotherMove_113; }
	inline bool* get_address_of_m_bAffectByMotherMove_113() { return &___m_bAffectByMotherMove_113; }
	inline void set_m_bAffectByMotherMove_113(bool value)
	{
		___m_bAffectByMotherMove_113 = value;
	}

	inline static int32_t get_offset_of_m_fCompensateDistance_114() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fCompensateDistance_114)); }
	inline float get_m_fCompensateDistance_114() const { return ___m_fCompensateDistance_114; }
	inline float* get_address_of_m_fCompensateDistance_114() { return &___m_fCompensateDistance_114; }
	inline void set_m_fCompensateDistance_114(float value)
	{
		___m_fCompensateDistance_114 = value;
	}

	inline static int32_t get_offset_of_m_fBaseDistance_115() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fBaseDistance_115)); }
	inline float get_m_fBaseDistance_115() const { return ___m_fBaseDistance_115; }
	inline float* get_address_of_m_fBaseDistance_115() { return &___m_fBaseDistance_115; }
	inline void set_m_fBaseDistance_115(float value)
	{
		___m_fBaseDistance_115 = value;
	}

	inline static int32_t get_offset_of_m_fTotalDistance_116() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fTotalDistance_116)); }
	inline float get_m_fTotalDistance_116() const { return ___m_fTotalDistance_116; }
	inline float* get_address_of_m_fTotalDistance_116() { return &___m_fTotalDistance_116; }
	inline void set_m_fTotalDistance_116(float value)
	{
		___m_fTotalDistance_116 = value;
	}

	inline static int32_t get_offset_of_m_bFinishedX_117() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bFinishedX_117)); }
	inline bool get_m_bFinishedX_117() const { return ___m_bFinishedX_117; }
	inline bool* get_address_of_m_bFinishedX_117() { return &___m_bFinishedX_117; }
	inline void set_m_bFinishedX_117(bool value)
	{
		___m_bFinishedX_117 = value;
	}

	inline static int32_t get_offset_of_m_bFinishedY_118() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bFinishedY_118)); }
	inline bool get_m_bFinishedY_118() const { return ___m_bFinishedY_118; }
	inline bool* get_address_of_m_bFinishedY_118() { return &___m_bFinishedY_118; }
	inline void set_m_bFinishedY_118(bool value)
	{
		___m_bFinishedY_118 = value;
	}

	inline static int32_t get_offset_of_m_eEjectingStatus_119() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_eEjectingStatus_119)); }
	inline int32_t get_m_eEjectingStatus_119() const { return ___m_eEjectingStatus_119; }
	inline int32_t* get_address_of_m_eEjectingStatus_119() { return &___m_eEjectingStatus_119; }
	inline void set_m_eEjectingStatus_119(int32_t value)
	{
		___m_eEjectingStatus_119 = value;
	}

	inline static int32_t get_offset_of_m_fTempTimeCount_120() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fTempTimeCount_120)); }
	inline float get_m_fTempTimeCount_120() const { return ___m_fTempTimeCount_120; }
	inline float* get_address_of_m_fTempTimeCount_120() { return &___m_fTempTimeCount_120; }
	inline void set_m_fTempTimeCount_120(float value)
	{
		___m_fTempTimeCount_120 = value;
	}

	inline static int32_t get_offset_of_m_fInitSpeed_121() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fInitSpeed_121)); }
	inline float get_m_fInitSpeed_121() const { return ___m_fInitSpeed_121; }
	inline float* get_address_of_m_fInitSpeed_121() { return &___m_fInitSpeed_121; }
	inline void set_m_fInitSpeed_121(float value)
	{
		___m_fInitSpeed_121 = value;
	}

	inline static int32_t get_offset_of_m_fAccelerate_122() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fAccelerate_122)); }
	inline float get_m_fAccelerate_122() const { return ___m_fAccelerate_122; }
	inline float* get_address_of_m_fAccelerate_122() { return &___m_fAccelerate_122; }
	inline void set_m_fAccelerate_122(float value)
	{
		___m_fAccelerate_122 = value;
	}

	inline static int32_t get_offset_of_m_bStaying_123() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bStaying_123)); }
	inline bool get_m_bStaying_123() const { return ___m_bStaying_123; }
	inline bool* get_address_of_m_bStaying_123() { return &___m_bStaying_123; }
	inline void set_m_bStaying_123(bool value)
	{
		___m_bStaying_123 = value;
	}

	inline static int32_t get_offset_of_m_nUnfoldStatus_124() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nUnfoldStatus_124)); }
	inline int32_t get_m_nUnfoldStatus_124() const { return ___m_nUnfoldStatus_124; }
	inline int32_t* get_address_of_m_nUnfoldStatus_124() { return &___m_nUnfoldStatus_124; }
	inline void set_m_nUnfoldStatus_124(int32_t value)
	{
		___m_nUnfoldStatus_124 = value;
	}

	inline static int32_t get_offset_of_m_bPreunfolding_125() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bPreunfolding_125)); }
	inline bool get_m_bPreunfolding_125() const { return ___m_bPreunfolding_125; }
	inline bool* get_address_of_m_bPreunfolding_125() { return &___m_bPreunfolding_125; }
	inline void set_m_bPreunfolding_125(bool value)
	{
		___m_bPreunfolding_125 = value;
	}

	inline static int32_t get_offset_of_m_fUnfoldLeftTime_126() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fUnfoldLeftTime_126)); }
	inline float get_m_fUnfoldLeftTime_126() const { return ___m_fUnfoldLeftTime_126; }
	inline float* get_address_of_m_fUnfoldLeftTime_126() { return &___m_fUnfoldLeftTime_126; }
	inline void set_m_fUnfoldLeftTime_126(float value)
	{
		___m_fUnfoldLeftTime_126 = value;
	}

	inline static int32_t get_offset_of_m_fSizeBeforeUnfold_127() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSizeBeforeUnfold_127)); }
	inline float get_m_fSizeBeforeUnfold_127() const { return ___m_fSizeBeforeUnfold_127; }
	inline float* get_address_of_m_fSizeBeforeUnfold_127() { return &___m_fSizeBeforeUnfold_127; }
	inline void set_m_fSizeBeforeUnfold_127(float value)
	{
		___m_fSizeBeforeUnfold_127 = value;
	}

	inline static int32_t get_offset_of_m_fMouthUnfoldScale_128() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMouthUnfoldScale_128)); }
	inline float get_m_fMouthUnfoldScale_128() const { return ___m_fMouthUnfoldScale_128; }
	inline float* get_address_of_m_fMouthUnfoldScale_128() { return &___m_fMouthUnfoldScale_128; }
	inline void set_m_fMouthUnfoldScale_128(float value)
	{
		___m_fMouthUnfoldScale_128 = value;
	}

	inline static int32_t get_offset_of_m_nMouthUnfoldStatus_129() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nMouthUnfoldStatus_129)); }
	inline int32_t get_m_nMouthUnfoldStatus_129() const { return ___m_nMouthUnfoldStatus_129; }
	inline int32_t* get_address_of_m_nMouthUnfoldStatus_129() { return &___m_nMouthUnfoldStatus_129; }
	inline void set_m_nMouthUnfoldStatus_129(int32_t value)
	{
		___m_nMouthUnfoldStatus_129 = value;
	}

	inline static int32_t get_offset_of_m_lstPkMonsterList_130() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstPkMonsterList_130)); }
	inline List_1_t3413545680 * get_m_lstPkMonsterList_130() const { return ___m_lstPkMonsterList_130; }
	inline List_1_t3413545680 ** get_address_of_m_lstPkMonsterList_130() { return &___m_lstPkMonsterList_130; }
	inline void set_m_lstPkMonsterList_130(List_1_t3413545680 * value)
	{
		___m_lstPkMonsterList_130 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPkMonsterList_130), value);
	}

	inline static int32_t get_offset_of_m_lstPkListSelf_131() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstPkListSelf_131)); }
	inline List_1_t3678741308 * get_m_lstPkListSelf_131() const { return ___m_lstPkListSelf_131; }
	inline List_1_t3678741308 ** get_address_of_m_lstPkListSelf_131() { return &___m_lstPkListSelf_131; }
	inline void set_m_lstPkListSelf_131(List_1_t3678741308 * value)
	{
		___m_lstPkListSelf_131 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPkListSelf_131), value);
	}

	inline static int32_t get_offset_of_m_lstPkListOther_132() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstPkListOther_132)); }
	inline List_1_t3678741308 * get_m_lstPkListOther_132() const { return ___m_lstPkListOther_132; }
	inline List_1_t3678741308 ** get_address_of_m_lstPkListOther_132() { return &___m_lstPkListOther_132; }
	inline void set_m_lstPkListOther_132(List_1_t3678741308 * value)
	{
		___m_lstPkListOther_132 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPkListOther_132), value);
	}

	inline static int32_t get_offset_of_m_fPreDrawDeltaTime_135() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fPreDrawDeltaTime_135)); }
	inline float get_m_fPreDrawDeltaTime_135() const { return ___m_fPreDrawDeltaTime_135; }
	inline float* get_address_of_m_fPreDrawDeltaTime_135() { return &___m_fPreDrawDeltaTime_135; }
	inline void set_m_fPreDrawDeltaTime_135(float value)
	{
		___m_fPreDrawDeltaTime_135 = value;
	}

	inline static int32_t get_offset_of_m_fThornSize_136() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fThornSize_136)); }
	inline float get_m_fThornSize_136() const { return ___m_fThornSize_136; }
	inline float* get_address_of_m_fThornSize_136() { return &___m_fThornSize_136; }
	inline void set_m_fThornSize_136(float value)
	{
		___m_fThornSize_136 = value;
	}

	inline static int32_t get_offset_of_m_fShitExplodeTime_138() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fShitExplodeTime_138)); }
	inline float get_m_fShitExplodeTime_138() const { return ___m_fShitExplodeTime_138; }
	inline float* get_address_of_m_fShitExplodeTime_138() { return &___m_fShitExplodeTime_138; }
	inline void set_m_fShitExplodeTime_138(float value)
	{
		___m_fShitExplodeTime_138 = value;
	}

	inline static int32_t get_offset_of_m_fMotherLeft_139() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMotherLeft_139)); }
	inline float get_m_fMotherLeft_139() const { return ___m_fMotherLeft_139; }
	inline float* get_address_of_m_fMotherLeft_139() { return &___m_fMotherLeft_139; }
	inline void set_m_fMotherLeft_139(float value)
	{
		___m_fMotherLeft_139 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeShellTime_140() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fExplodeShellTime_140)); }
	inline float get_m_fExplodeShellTime_140() const { return ___m_fExplodeShellTime_140; }
	inline float* get_address_of_m_fExplodeShellTime_140() { return &___m_fExplodeShellTime_140; }
	inline void set_m_fExplodeShellTime_140(float value)
	{
		___m_fExplodeShellTime_140 = value;
	}

	inline static int32_t get_offset_of_m_nTotalChildNum_141() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nTotalChildNum_141)); }
	inline int32_t get_m_nTotalChildNum_141() const { return ___m_nTotalChildNum_141; }
	inline int32_t* get_address_of_m_nTotalChildNum_141() { return &___m_nTotalChildNum_141; }
	inline void set_m_nTotalChildNum_141(int32_t value)
	{
		___m_nTotalChildNum_141 = value;
	}

	inline static int32_t get_offset_of_m_nCurChildNum_142() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nCurChildNum_142)); }
	inline int32_t get_m_nCurChildNum_142() const { return ___m_nCurChildNum_142; }
	inline int32_t* get_address_of_m_nCurChildNum_142() { return &___m_nCurChildNum_142; }
	inline void set_m_nCurChildNum_142(int32_t value)
	{
		___m_nCurChildNum_142 = value;
	}

	inline static int32_t get_offset_of_m_bExploding_144() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bExploding_144)); }
	inline bool get_m_bExploding_144() const { return ___m_bExploding_144; }
	inline bool* get_address_of_m_bExploding_144() { return &___m_bExploding_144; }
	inline void set_m_bExploding_144(bool value)
	{
		___m_bExploding_144 = value;
	}

	inline static int32_t get_offset_of_m_ExplodeConfig_145() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_ExplodeConfig_145)); }
	inline sThornConfig_t1242444451  get_m_ExplodeConfig_145() const { return ___m_ExplodeConfig_145; }
	inline sThornConfig_t1242444451 * get_address_of_m_ExplodeConfig_145() { return &___m_ExplodeConfig_145; }
	inline void set_m_ExplodeConfig_145(sThornConfig_t1242444451  value)
	{
		___m_ExplodeConfig_145 = value;
	}

	inline static int32_t get_offset_of_m_vecMotherPos_146() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecMotherPos_146)); }
	inline Vector3_t3722313464  get_m_vecMotherPos_146() const { return ___m_vecMotherPos_146; }
	inline Vector3_t3722313464 * get_address_of_m_vecMotherPos_146() { return &___m_vecMotherPos_146; }
	inline void set_m_vecMotherPos_146(Vector3_t3722313464  value)
	{
		___m_vecMotherPos_146 = value;
	}

	inline static int32_t get_offset_of_m_fRunDistance_147() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fRunDistance_147)); }
	inline float get_m_fRunDistance_147() const { return ___m_fRunDistance_147; }
	inline float* get_address_of_m_fRunDistance_147() { return &___m_fRunDistance_147; }
	inline void set_m_fRunDistance_147(float value)
	{
		___m_fRunDistance_147 = value;
	}

	inline static int32_t get_offset_of_m_fRealRunTime_148() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fRealRunTime_148)); }
	inline float get_m_fRealRunTime_148() const { return ___m_fRealRunTime_148; }
	inline float* get_address_of_m_fRealRunTime_148() { return &___m_fRealRunTime_148; }
	inline void set_m_fRealRunTime_148(float value)
	{
		___m_fRealRunTime_148 = value;
	}

	inline static int32_t get_offset_of_m_lstDir_149() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstDir_149)); }
	inline List_1_t3628304265 * get_m_lstDir_149() const { return ___m_lstDir_149; }
	inline List_1_t3628304265 ** get_address_of_m_lstDir_149() { return &___m_lstDir_149; }
	inline void set_m_lstDir_149(List_1_t3628304265 * value)
	{
		___m_lstDir_149 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstDir_149), value);
	}

	inline static int32_t get_offset_of_m_fChildSize_150() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fChildSize_150)); }
	inline float get_m_fChildSize_150() const { return ___m_fChildSize_150; }
	inline float* get_address_of_m_fChildSize_150() { return &___m_fChildSize_150; }
	inline void set_m_fChildSize_150(float value)
	{
		___m_fChildSize_150 = value;
	}

	inline static int32_t get_offset_of_m_fSegDis_151() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSegDis_151)); }
	inline float get_m_fSegDis_151() const { return ___m_fSegDis_151; }
	inline float* get_address_of_m_fSegDis_151() { return &___m_fSegDis_151; }
	inline void set_m_fSegDis_151(float value)
	{
		___m_fSegDis_151 = value;
	}

	inline static int32_t get_offset_of_m_lstChildBallsIndex_152() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstChildBallsIndex_152)); }
	inline Int32U5BU5D_t385246372* get_m_lstChildBallsIndex_152() const { return ___m_lstChildBallsIndex_152; }
	inline Int32U5BU5D_t385246372** get_address_of_m_lstChildBallsIndex_152() { return &___m_lstChildBallsIndex_152; }
	inline void set_m_lstChildBallsIndex_152(Int32U5BU5D_t385246372* value)
	{
		___m_lstChildBallsIndex_152 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstChildBallsIndex_152), value);
	}

	inline static int32_t get_offset_of_m_nExplodeRound_153() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nExplodeRound_153)); }
	inline int32_t get_m_nExplodeRound_153() const { return ___m_nExplodeRound_153; }
	inline int32_t* get_address_of_m_nExplodeRound_153() { return &___m_nExplodeRound_153; }
	inline void set_m_nExplodeRound_153(int32_t value)
	{
		___m_nExplodeRound_153 = value;
	}

	inline static int32_t get_offset_of_m_lstEaten_154() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstEaten_154)); }
	inline List_1_t2801720860 * get_m_lstEaten_154() const { return ___m_lstEaten_154; }
	inline List_1_t2801720860 ** get_address_of_m_lstEaten_154() { return &___m_lstEaten_154; }
	inline void set_m_lstEaten_154(List_1_t2801720860 * value)
	{
		___m_lstEaten_154 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEaten_154), value);
	}

	inline static int32_t get_offset_of_m_Grass_155() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_Grass_155)); }
	inline CGrass_t734084476 * get_m_Grass_155() const { return ___m_Grass_155; }
	inline CGrass_t734084476 ** get_address_of_m_Grass_155() { return &___m_Grass_155; }
	inline void set_m_Grass_155(CGrass_t734084476 * value)
	{
		___m_Grass_155 = value;
		Il2CppCodeGenWriteBarrier((&___m_Grass_155), value);
	}

	inline static int32_t get_offset_of_m_fEnterGrassTime_156() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fEnterGrassTime_156)); }
	inline float get_m_fEnterGrassTime_156() const { return ___m_fEnterGrassTime_156; }
	inline float* get_address_of_m_fEnterGrassTime_156() { return &___m_fEnterGrassTime_156; }
	inline void set_m_fEnterGrassTime_156(float value)
	{
		___m_fEnterGrassTime_156 = value;
	}

	inline static int32_t get_offset_of_m_lstGrass_157() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstGrass_157)); }
	inline List_1_t2206159218 * get_m_lstGrass_157() const { return ___m_lstGrass_157; }
	inline List_1_t2206159218 ** get_address_of_m_lstGrass_157() { return &___m_lstGrass_157; }
	inline void set_m_lstGrass_157(List_1_t2206159218 * value)
	{
		___m_lstGrass_157 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstGrass_157), value);
	}

	inline static int32_t get_offset_of_m_szAccount_158() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_szAccount_158)); }
	inline String_t* get_m_szAccount_158() const { return ___m_szAccount_158; }
	inline String_t** get_address_of_m_szAccount_158() { return &___m_szAccount_158; }
	inline void set_m_szAccount_158(String_t* value)
	{
		___m_szAccount_158 = value;
		Il2CppCodeGenWriteBarrier((&___m_szAccount_158), value);
	}

	inline static int32_t get_offset_of_m_lstPreEatSpores_159() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstPreEatSpores_159)); }
	inline List_1_t1482975291 * get_m_lstPreEatSpores_159() const { return ___m_lstPreEatSpores_159; }
	inline List_1_t1482975291 ** get_address_of_m_lstPreEatSpores_159() { return &___m_lstPreEatSpores_159; }
	inline void set_m_lstPreEatSpores_159(List_1_t1482975291 * value)
	{
		___m_lstPreEatSpores_159 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstPreEatSpores_159), value);
	}

	inline static int32_t get_offset_of_m_fMotherLeftSize_160() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMotherLeftSize_160)); }
	inline float get_m_fMotherLeftSize_160() const { return ___m_fMotherLeftSize_160; }
	inline float* get_address_of_m_fMotherLeftSize_160() { return &___m_fMotherLeftSize_160; }
	inline void set_m_fMotherLeftSize_160(float value)
	{
		___m_fMotherLeftSize_160 = value;
	}

	inline static int32_t get_offset_of_m_fChildThornSize_161() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fChildThornSize_161)); }
	inline float get_m_fChildThornSize_161() const { return ___m_fChildThornSize_161; }
	inline float* get_address_of_m_fChildThornSize_161() { return &___m_fChildThornSize_161; }
	inline void set_m_fChildThornSize_161(float value)
	{
		___m_fChildThornSize_161 = value;
	}

	inline static int32_t get_offset_of_m_fMotherLeftThornSize_162() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMotherLeftThornSize_162)); }
	inline float get_m_fMotherLeftThornSize_162() const { return ___m_fMotherLeftThornSize_162; }
	inline float* get_address_of_m_fMotherLeftThornSize_162() { return &___m_fMotherLeftThornSize_162; }
	inline void set_m_fMotherLeftThornSize_162(float value)
	{
		___m_fMotherLeftThornSize_162 = value;
	}

	inline static int32_t get_offset_of_m_bShell_163() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bShell_163)); }
	inline bool get_m_bShell_163() const { return ___m_bShell_163; }
	inline bool* get_address_of_m_bShell_163() { return &___m_bShell_163; }
	inline void set_m_bShell_163(bool value)
	{
		___m_bShell_163 = value;
	}

	inline static int32_t get_offset_of_m_fShellShrinkCurTime_164() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fShellShrinkCurTime_164)); }
	inline float get_m_fShellShrinkCurTime_164() const { return ___m_fShellShrinkCurTime_164; }
	inline float* get_address_of_m_fShellShrinkCurTime_164() { return &___m_fShellShrinkCurTime_164; }
	inline void set_m_fShellShrinkCurTime_164(float value)
	{
		___m_fShellShrinkCurTime_164 = value;
	}

	inline static int32_t get_offset_of_m_fShellTotalTime_165() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fShellTotalTime_165)); }
	inline float get_m_fShellTotalTime_165() const { return ___m_fShellTotalTime_165; }
	inline float* get_address_of_m_fShellTotalTime_165() { return &___m_fShellTotalTime_165; }
	inline void set_m_fShellTotalTime_165(float value)
	{
		___m_fShellTotalTime_165 = value;
	}

	inline static int32_t get_offset_of_m_eShellType_166() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_eShellType_166)); }
	inline int32_t get_m_eShellType_166() const { return ___m_eShellType_166; }
	inline int32_t* get_address_of_m_eShellType_166() { return &___m_eShellType_166; }
	inline void set_m_eShellType_166(int32_t value)
	{
		___m_eShellType_166 = value;
	}

	inline static int32_t get_offset_of_m_fBallTestValue_167() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fBallTestValue_167)); }
	inline float get_m_fBallTestValue_167() const { return ___m_fBallTestValue_167; }
	inline float* get_address_of_m_fBallTestValue_167() { return &___m_fBallTestValue_167; }
	inline void set_m_fBallTestValue_167(float value)
	{
		___m_fBallTestValue_167 = value;
	}

	inline static int32_t get_offset_of_m_bDynamicShellVisible_168() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bDynamicShellVisible_168)); }
	inline bool get_m_bDynamicShellVisible_168() const { return ___m_bDynamicShellVisible_168; }
	inline bool* get_address_of_m_bDynamicShellVisible_168() { return &___m_bDynamicShellVisible_168; }
	inline void set_m_bDynamicShellVisible_168(bool value)
	{
		___m_bDynamicShellVisible_168 = value;
	}

	inline static int32_t get_offset_of_m_fShellUnfoldScale_169() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fShellUnfoldScale_169)); }
	inline float get_m_fShellUnfoldScale_169() const { return ___m_fShellUnfoldScale_169; }
	inline float* get_address_of_m_fShellUnfoldScale_169() { return &___m_fShellUnfoldScale_169; }
	inline void set_m_fShellUnfoldScale_169(float value)
	{
		___m_fShellUnfoldScale_169 = value;
	}

	inline static int32_t get_offset_of_m_fTempShellCount_170() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fTempShellCount_170)); }
	inline float get_m_fTempShellCount_170() const { return ___m_fTempShellCount_170; }
	inline float* get_address_of_m_fTempShellCount_170() { return &___m_fTempShellCount_170; }
	inline void set_m_fTempShellCount_170(float value)
	{
		___m_fTempShellCount_170 = value;
	}

	inline static int32_t get_offset_of_m_fEndAngle_171() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fEndAngle_171)); }
	inline float get_m_fEndAngle_171() const { return ___m_fEndAngle_171; }
	inline float* get_address_of_m_fEndAngle_171() { return &___m_fEndAngle_171; }
	inline void set_m_fEndAngle_171(float value)
	{
		___m_fEndAngle_171 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallInitSpeed_172() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSpitBallInitSpeed_172)); }
	inline float get_m_fSpitBallInitSpeed_172() const { return ___m_fSpitBallInitSpeed_172; }
	inline float* get_address_of_m_fSpitBallInitSpeed_172() { return &___m_fSpitBallInitSpeed_172; }
	inline void set_m_fSpitBallInitSpeed_172(float value)
	{
		___m_fSpitBallInitSpeed_172 = value;
	}

	inline static int32_t get_offset_of_m_fSpitBallAccelerate_173() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSpitBallAccelerate_173)); }
	inline float get_m_fSpitBallAccelerate_173() const { return ___m_fSpitBallAccelerate_173; }
	inline float* get_address_of_m_fSpitBallAccelerate_173() { return &___m_fSpitBallAccelerate_173; }
	inline void set_m_fSpitBallAccelerate_173(float value)
	{
		___m_fSpitBallAccelerate_173 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeInitSpeed_174() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fExplodeInitSpeed_174)); }
	inline float get_m_fExplodeInitSpeed_174() const { return ___m_fExplodeInitSpeed_174; }
	inline float* get_address_of_m_fExplodeInitSpeed_174() { return &___m_fExplodeInitSpeed_174; }
	inline void set_m_fExplodeInitSpeed_174(float value)
	{
		___m_fExplodeInitSpeed_174 = value;
	}

	inline static int32_t get_offset_of_m_fExplodeInitAccelerate_175() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fExplodeInitAccelerate_175)); }
	inline float get_m_fExplodeInitAccelerate_175() const { return ___m_fExplodeInitAccelerate_175; }
	inline float* get_address_of_m_fExplodeInitAccelerate_175() { return &___m_fExplodeInitAccelerate_175; }
	inline void set_m_fExplodeInitAccelerate_175(float value)
	{
		___m_fExplodeInitAccelerate_175 = value;
	}

	inline static int32_t get_offset_of_m_SpitBallTarget_176() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_SpitBallTarget_176)); }
	inline SpitBallTarget_t3997305173 * get_m_SpitBallTarget_176() const { return ___m_SpitBallTarget_176; }
	inline SpitBallTarget_t3997305173 ** get_address_of_m_SpitBallTarget_176() { return &___m_SpitBallTarget_176; }
	inline void set_m_SpitBallTarget_176(SpitBallTarget_t3997305173 * value)
	{
		___m_SpitBallTarget_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpitBallTarget_176), value);
	}

	inline static int32_t get_offset_of_m_fSpitBallTargetParam_177() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSpitBallTargetParam_177)); }
	inline float get_m_fSpitBallTargetParam_177() const { return ___m_fSpitBallTargetParam_177; }
	inline float* get_address_of_m_fSpitBallTargetParam_177() { return &___m_fSpitBallTargetParam_177; }
	inline void set_m_fSpitBallTargetParam_177(float value)
	{
		___m_fSpitBallTargetParam_177 = value;
	}

	inline static int32_t get_offset_of_m_bTargeting_178() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bTargeting_178)); }
	inline bool get_m_bTargeting_178() const { return ___m_bTargeting_178; }
	inline bool* get_address_of_m_bTargeting_178() { return &___m_bTargeting_178; }
	inline void set_m_bTargeting_178(bool value)
	{
		___m_bTargeting_178 = value;
	}

	inline static int32_t get_offset_of_m_SplitBallTarget_179() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_SplitBallTarget_179)); }
	inline SpitBallTarget_t3997305173 * get_m_SplitBallTarget_179() const { return ___m_SplitBallTarget_179; }
	inline SpitBallTarget_t3997305173 ** get_address_of_m_SplitBallTarget_179() { return &___m_SplitBallTarget_179; }
	inline void set_m_SplitBallTarget_179(SpitBallTarget_t3997305173 * value)
	{
		___m_SplitBallTarget_179 = value;
		Il2CppCodeGenWriteBarrier((&___m_SplitBallTarget_179), value);
	}

	inline static int32_t get_offset_of__relateTarget_180() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____relateTarget_180)); }
	inline SpitBallTarget_t3997305173 * get__relateTarget_180() const { return ____relateTarget_180; }
	inline SpitBallTarget_t3997305173 ** get_address_of__relateTarget_180() { return &____relateTarget_180; }
	inline void set__relateTarget_180(SpitBallTarget_t3997305173 * value)
	{
		____relateTarget_180 = value;
		Il2CppCodeGenWriteBarrier((&____relateTarget_180), value);
	}

	inline static int32_t get_offset_of__relateSplitTarget_181() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ____relateSplitTarget_181)); }
	inline SpitBallTarget_t3997305173 * get__relateSplitTarget_181() const { return ____relateSplitTarget_181; }
	inline SpitBallTarget_t3997305173 ** get_address_of__relateSplitTarget_181() { return &____relateSplitTarget_181; }
	inline void set__relateSplitTarget_181(SpitBallTarget_t3997305173 * value)
	{
		____relateSplitTarget_181 = value;
		Il2CppCodeGenWriteBarrier((&____relateSplitTarget_181), value);
	}

	inline static int32_t get_offset_of_m_bDoNotMove_182() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bDoNotMove_182)); }
	inline bool get_m_bDoNotMove_182() const { return ___m_bDoNotMove_182; }
	inline bool* get_address_of_m_bDoNotMove_182() { return &___m_bDoNotMove_182; }
	inline void set_m_bDoNotMove_182(bool value)
	{
		___m_bDoNotMove_182 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustSpeedX_183() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fAdjustSpeedX_183)); }
	inline float get_m_fAdjustSpeedX_183() const { return ___m_fAdjustSpeedX_183; }
	inline float* get_address_of_m_fAdjustSpeedX_183() { return &___m_fAdjustSpeedX_183; }
	inline void set_m_fAdjustSpeedX_183(float value)
	{
		___m_fAdjustSpeedX_183 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustSpeedY_184() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fAdjustSpeedY_184)); }
	inline float get_m_fAdjustSpeedY_184() const { return ___m_fAdjustSpeedY_184; }
	inline float* get_address_of_m_fAdjustSpeedY_184() { return &___m_fAdjustSpeedY_184; }
	inline void set_m_fAdjustSpeedY_184(float value)
	{
		___m_fAdjustSpeedY_184 = value;
	}

	inline static int32_t get_offset_of_m_fExtraSpeedX_185() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fExtraSpeedX_185)); }
	inline float get_m_fExtraSpeedX_185() const { return ___m_fExtraSpeedX_185; }
	inline float* get_address_of_m_fExtraSpeedX_185() { return &___m_fExtraSpeedX_185; }
	inline void set_m_fExtraSpeedX_185(float value)
	{
		___m_fExtraSpeedX_185 = value;
	}

	inline static int32_t get_offset_of_m_fExtraSpeedY_186() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fExtraSpeedY_186)); }
	inline float get_m_fExtraSpeedY_186() const { return ___m_fExtraSpeedY_186; }
	inline float* get_address_of_m_fExtraSpeedY_186() { return &___m_fExtraSpeedY_186; }
	inline void set_m_fExtraSpeedY_186(float value)
	{
		___m_fExtraSpeedY_186 = value;
	}

	inline static int32_t get_offset_of_m_fMoveSpeedX_188() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMoveSpeedX_188)); }
	inline float get_m_fMoveSpeedX_188() const { return ___m_fMoveSpeedX_188; }
	inline float* get_address_of_m_fMoveSpeedX_188() { return &___m_fMoveSpeedX_188; }
	inline void set_m_fMoveSpeedX_188(float value)
	{
		___m_fMoveSpeedX_188 = value;
	}

	inline static int32_t get_offset_of_m_fMoveSpeedY_189() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fMoveSpeedY_189)); }
	inline float get_m_fMoveSpeedY_189() const { return ___m_fMoveSpeedY_189; }
	inline float* get_address_of_m_fMoveSpeedY_189() { return &___m_fMoveSpeedY_189; }
	inline void set_m_fMoveSpeedY_189(float value)
	{
		___m_fMoveSpeedY_189 = value;
	}

	inline static int32_t get_offset_of_m_vecdirection_190() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecdirection_190)); }
	inline Vector2_t2156229523  get_m_vecdirection_190() const { return ___m_vecdirection_190; }
	inline Vector2_t2156229523 * get_address_of_m_vecdirection_190() { return &___m_vecdirection_190; }
	inline void set_m_vecdirection_190(Vector2_t2156229523  value)
	{
		___m_vecdirection_190 = value;
	}

	inline static int32_t get_offset_of_m_vecLastDirection_191() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecLastDirection_191)); }
	inline Vector2_t2156229523  get_m_vecLastDirection_191() const { return ___m_vecLastDirection_191; }
	inline Vector2_t2156229523 * get_address_of_m_vecLastDirection_191() { return &___m_vecLastDirection_191; }
	inline void set_m_vecLastDirection_191(Vector2_t2156229523  value)
	{
		___m_vecLastDirection_191 = value;
	}

	inline static int32_t get_offset_of_m_bFirstCalculateMoveInfo_192() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bFirstCalculateMoveInfo_192)); }
	inline bool get_m_bFirstCalculateMoveInfo_192() const { return ___m_bFirstCalculateMoveInfo_192; }
	inline bool* get_address_of_m_bFirstCalculateMoveInfo_192() { return &___m_bFirstCalculateMoveInfo_192; }
	inline void set_m_bFirstCalculateMoveInfo_192(bool value)
	{
		___m_bFirstCalculateMoveInfo_192 = value;
	}

	inline static int32_t get_offset_of_m_vecMainSpeed_193() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecMainSpeed_193)); }
	inline Vector2_t2156229523  get_m_vecMainSpeed_193() const { return ___m_vecMainSpeed_193; }
	inline Vector2_t2156229523 * get_address_of_m_vecMainSpeed_193() { return &___m_vecMainSpeed_193; }
	inline void set_m_vecMainSpeed_193(Vector2_t2156229523  value)
	{
		___m_vecMainSpeed_193 = value;
	}

	inline static int32_t get_offset_of_m_bInterpolateMoving_195() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bInterpolateMoving_195)); }
	inline bool get_m_bInterpolateMoving_195() const { return ___m_bInterpolateMoving_195; }
	inline bool* get_address_of_m_bInterpolateMoving_195() { return &___m_bInterpolateMoving_195; }
	inline void set_m_bInterpolateMoving_195(bool value)
	{
		___m_bInterpolateMoving_195 = value;
	}

	inline static int32_t get_offset_of_fChaZhiSpeedX_196() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___fChaZhiSpeedX_196)); }
	inline float get_fChaZhiSpeedX_196() const { return ___fChaZhiSpeedX_196; }
	inline float* get_address_of_fChaZhiSpeedX_196() { return &___fChaZhiSpeedX_196; }
	inline void set_fChaZhiSpeedX_196(float value)
	{
		___fChaZhiSpeedX_196 = value;
	}

	inline static int32_t get_offset_of_fChaZhiSpeedY_197() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___fChaZhiSpeedY_197)); }
	inline float get_fChaZhiSpeedY_197() const { return ___fChaZhiSpeedY_197; }
	inline float* get_address_of_fChaZhiSpeedY_197() { return &___fChaZhiSpeedY_197; }
	inline void set_fChaZhiSpeedY_197(float value)
	{
		___fChaZhiSpeedY_197 = value;
	}

	inline static int32_t get_offset_of_m_bbMovingToCenter_198() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bbMovingToCenter_198)); }
	inline bool get_m_bbMovingToCenter_198() const { return ___m_bbMovingToCenter_198; }
	inline bool* get_address_of_m_bbMovingToCenter_198() { return &___m_bbMovingToCenter_198; }
	inline void set_m_bbMovingToCenter_198(bool value)
	{
		___m_bbMovingToCenter_198 = value;
	}

	inline static int32_t get_offset_of_m_vecBallsCenter_200() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecBallsCenter_200)); }
	inline Vector2_t2156229523  get_m_vecBallsCenter_200() const { return ___m_vecBallsCenter_200; }
	inline Vector2_t2156229523 * get_address_of_m_vecBallsCenter_200() { return &___m_vecBallsCenter_200; }
	inline void set_m_vecBallsCenter_200(Vector2_t2156229523  value)
	{
		___m_vecBallsCenter_200 = value;
	}

	inline static int32_t get_offset_of_m_fTotalMag_201() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fTotalMag_201)); }
	inline float get_m_fTotalMag_201() const { return ___m_fTotalMag_201; }
	inline float* get_address_of_m_fTotalMag_201() { return &___m_fTotalMag_201; }
	inline void set_m_fTotalMag_201(float value)
	{
		___m_fTotalMag_201 = value;
	}

	inline static int32_t get_offset_of_m_nCountMag_202() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nCountMag_202)); }
	inline int32_t get_m_nCountMag_202() const { return ___m_nCountMag_202; }
	inline int32_t* get_address_of_m_nCountMag_202() { return &___m_nCountMag_202; }
	inline void set_m_nCountMag_202(int32_t value)
	{
		___m_nCountMag_202 = value;
	}

	inline static int32_t get_offset_of_m_vecCurChaZhiDest_203() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecCurChaZhiDest_203)); }
	inline Vector3_t3722313464  get_m_vecCurChaZhiDest_203() const { return ___m_vecCurChaZhiDest_203; }
	inline Vector3_t3722313464 * get_address_of_m_vecCurChaZhiDest_203() { return &___m_vecCurChaZhiDest_203; }
	inline void set_m_vecCurChaZhiDest_203(Vector3_t3722313464  value)
	{
		___m_vecCurChaZhiDest_203 = value;
	}

	inline static int32_t get_offset_of_m_vecCurMainPlayerSpeed_204() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecCurMainPlayerSpeed_204)); }
	inline Vector2_t2156229523  get_m_vecCurMainPlayerSpeed_204() const { return ___m_vecCurMainPlayerSpeed_204; }
	inline Vector2_t2156229523 * get_address_of_m_vecCurMainPlayerSpeed_204() { return &___m_vecCurMainPlayerSpeed_204; }
	inline void set_m_vecCurMainPlayerSpeed_204(Vector2_t2156229523  value)
	{
		___m_vecCurMainPlayerSpeed_204 = value;
	}

	inline static int32_t get_offset_of_m_vecChaZhiSpeed_205() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecChaZhiSpeed_205)); }
	inline Vector2_t2156229523  get_m_vecChaZhiSpeed_205() const { return ___m_vecChaZhiSpeed_205; }
	inline Vector2_t2156229523 * get_address_of_m_vecChaZhiSpeed_205() { return &___m_vecChaZhiSpeed_205; }
	inline void set_m_vecChaZhiSpeed_205(Vector2_t2156229523  value)
	{
		___m_vecChaZhiSpeed_205 = value;
	}

	inline static int32_t get_offset_of_m_nChaZhiCount_206() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nChaZhiCount_206)); }
	inline int32_t get_m_nChaZhiCount_206() const { return ___m_nChaZhiCount_206; }
	inline int32_t* get_address_of_m_nChaZhiCount_206() { return &___m_nChaZhiCount_206; }
	inline void set_m_nChaZhiCount_206(int32_t value)
	{
		___m_nChaZhiCount_206 = value;
	}

	inline static int32_t get_offset_of_m_nRealSplitNum_209() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nRealSplitNum_209)); }
	inline int32_t get_m_nRealSplitNum_209() const { return ___m_nRealSplitNum_209; }
	inline int32_t* get_address_of_m_nRealSplitNum_209() { return &___m_nRealSplitNum_209; }
	inline void set_m_nRealSplitNum_209(int32_t value)
	{
		___m_nRealSplitNum_209 = value;
	}

	inline static int32_t get_offset_of_m_fSplitSize_210() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSplitSize_210)); }
	inline float get_m_fSplitSize_210() const { return ___m_fSplitSize_210; }
	inline float* get_address_of_m_fSplitSize_210() { return &___m_fSplitSize_210; }
	inline void set_m_fSplitSize_210(float value)
	{
		___m_fSplitSize_210 = value;
	}

	inline static int32_t get_offset_of_m_fSplitArea_211() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fSplitArea_211)); }
	inline float get_m_fSplitArea_211() const { return ___m_fSplitArea_211; }
	inline float* get_address_of_m_fSplitArea_211() { return &___m_fSplitArea_211; }
	inline void set_m_fSplitArea_211(float value)
	{
		___m_fSplitArea_211 = value;
	}

	inline static int32_t get_offset_of_m_fLeftArea_212() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fLeftArea_212)); }
	inline float get_m_fLeftArea_212() const { return ___m_fLeftArea_212; }
	inline float* get_address_of_m_fLeftArea_212() { return &___m_fLeftArea_212; }
	inline void set_m_fLeftArea_212(float value)
	{
		___m_fLeftArea_212 = value;
	}

	inline static int32_t get_offset_of_m_fTotalDis_213() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fTotalDis_213)); }
	inline float get_m_fTotalDis_213() const { return ___m_fTotalDis_213; }
	inline float* get_address_of_m_fTotalDis_213() { return &___m_fTotalDis_213; }
	inline void set_m_fTotalDis_213(float value)
	{
		___m_fTotalDis_213 = value;
	}

	inline static int32_t get_offset_of_m_bMotherDeadAfterSplit_214() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bMotherDeadAfterSplit_214)); }
	inline bool get_m_bMotherDeadAfterSplit_214() const { return ___m_bMotherDeadAfterSplit_214; }
	inline bool* get_address_of_m_bMotherDeadAfterSplit_214() { return &___m_bMotherDeadAfterSplit_214; }
	inline void set_m_bMotherDeadAfterSplit_214(bool value)
	{
		___m_bMotherDeadAfterSplit_214 = value;
	}

	inline static int32_t get_offset_of_m_nIndex_215() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nIndex_215)); }
	inline int32_t get_m_nIndex_215() const { return ___m_nIndex_215; }
	inline int32_t* get_address_of_m_nIndex_215() { return &___m_nIndex_215; }
	inline void set_m_nIndex_215(int32_t value)
	{
		___m_nIndex_215 = value;
	}

	inline static int32_t get_offset_of_m_nMovingToCenterStatus_216() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nMovingToCenterStatus_216)); }
	inline int32_t get_m_nMovingToCenterStatus_216() const { return ___m_nMovingToCenterStatus_216; }
	inline int32_t* get_address_of_m_nMovingToCenterStatus_216() { return &___m_nMovingToCenterStatus_216; }
	inline void set_m_nMovingToCenterStatus_216(int32_t value)
	{
		___m_nMovingToCenterStatus_216 = value;
	}

	inline static int32_t get_offset_of_m_bIsAdjustMoveProtect_217() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bIsAdjustMoveProtect_217)); }
	inline bool get_m_bIsAdjustMoveProtect_217() const { return ___m_bIsAdjustMoveProtect_217; }
	inline bool* get_address_of_m_bIsAdjustMoveProtect_217() { return &___m_bIsAdjustMoveProtect_217; }
	inline void set_m_bIsAdjustMoveProtect_217(bool value)
	{
		___m_bIsAdjustMoveProtect_217 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustMoveProtectTime_218() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fAdjustMoveProtectTime_218)); }
	inline float get_m_fAdjustMoveProtectTime_218() const { return ___m_fAdjustMoveProtectTime_218; }
	inline float* get_address_of_m_fAdjustMoveProtectTime_218() { return &___m_fAdjustMoveProtectTime_218; }
	inline void set_m_fAdjustMoveProtectTime_218(float value)
	{
		___m_fAdjustMoveProtectTime_218 = value;
	}

	inline static int32_t get_offset_of_m_dicGroupRoundId_219() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_dicGroupRoundId_219)); }
	inline Dictionary_2_t1839659084 * get_m_dicGroupRoundId_219() const { return ___m_dicGroupRoundId_219; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicGroupRoundId_219() { return &___m_dicGroupRoundId_219; }
	inline void set_m_dicGroupRoundId_219(Dictionary_2_t1839659084 * value)
	{
		___m_dicGroupRoundId_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicGroupRoundId_219), value);
	}

	inline static int32_t get_offset_of_m_fLocalOffsetX_220() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fLocalOffsetX_220)); }
	inline float get_m_fLocalOffsetX_220() const { return ___m_fLocalOffsetX_220; }
	inline float* get_address_of_m_fLocalOffsetX_220() { return &___m_fLocalOffsetX_220; }
	inline void set_m_fLocalOffsetX_220(float value)
	{
		___m_fLocalOffsetX_220 = value;
	}

	inline static int32_t get_offset_of_m_fLocalOffsetY_221() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fLocalOffsetY_221)); }
	inline float get_m_fLocalOffsetY_221() const { return ___m_fLocalOffsetY_221; }
	inline float* get_address_of_m_fLocalOffsetY_221() { return &___m_fLocalOffsetY_221; }
	inline void set_m_fLocalOffsetY_221(float value)
	{
		___m_fLocalOffsetY_221 = value;
	}

	inline static int32_t get_offset_of_m_bLockMove_222() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bLockMove_222)); }
	inline bool get_m_bLockMove_222() const { return ___m_bLockMove_222; }
	inline bool* get_address_of_m_bLockMove_222() { return &___m_bLockMove_222; }
	inline void set_m_bLockMove_222(bool value)
	{
		___m_bLockMove_222 = value;
	}

	inline static int32_t get_offset_of_m_nSizeClassId_223() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nSizeClassId_223)); }
	inline int32_t get_m_nSizeClassId_223() const { return ___m_nSizeClassId_223; }
	inline int32_t* get_address_of_m_nSizeClassId_223() { return &___m_nSizeClassId_223; }
	inline void set_m_nSizeClassId_223(int32_t value)
	{
		___m_nSizeClassId_223 = value;
	}

	inline static int32_t get_offset_of_m_fAttenuateSpeed_224() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fAttenuateSpeed_224)); }
	inline float get_m_fAttenuateSpeed_224() const { return ___m_fAttenuateSpeed_224; }
	inline float* get_address_of_m_fAttenuateSpeed_224() { return &___m_fAttenuateSpeed_224; }
	inline void set_m_fAttenuateSpeed_224(float value)
	{
		___m_fAttenuateSpeed_224 = value;
	}

	inline static int32_t get_offset_of_m_fClassInvasionAffectSpeed_225() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fClassInvasionAffectSpeed_225)); }
	inline float get_m_fClassInvasionAffectSpeed_225() const { return ___m_fClassInvasionAffectSpeed_225; }
	inline float* get_address_of_m_fClassInvasionAffectSpeed_225() { return &___m_fClassInvasionAffectSpeed_225; }
	inline void set_m_fClassInvasionAffectSpeed_225(float value)
	{
		___m_fClassInvasionAffectSpeed_225 = value;
	}

	inline static int32_t get_offset_of_m_nClassId_226() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nClassId_226)); }
	inline int32_t get_m_nClassId_226() const { return ___m_nClassId_226; }
	inline int32_t* get_address_of_m_nClassId_226() { return &___m_nClassId_226; }
	inline void set_m_nClassId_226(int32_t value)
	{
		___m_nClassId_226 = value;
	}

	inline static int32_t get_offset_of_m_bStayOnClassCircle_227() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bStayOnClassCircle_227)); }
	inline bool get_m_bStayOnClassCircle_227() const { return ___m_bStayOnClassCircle_227; }
	inline bool* get_address_of_m_bStayOnClassCircle_227() { return &___m_bStayOnClassCircle_227; }
	inline void set_m_bStayOnClassCircle_227(bool value)
	{
		___m_bStayOnClassCircle_227 = value;
	}

	inline static int32_t get_offset_of_m_nStayClassCircleId_228() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nStayClassCircleId_228)); }
	inline int32_t get_m_nStayClassCircleId_228() const { return ___m_nStayClassCircleId_228; }
	inline int32_t* get_address_of_m_nStayClassCircleId_228() { return &___m_nStayClassCircleId_228; }
	inline void set_m_nStayClassCircleId_228(int32_t value)
	{
		___m_nStayClassCircleId_228 = value;
	}

	inline static int32_t get_offset_of_m_bActive_229() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bActive_229)); }
	inline bool get_m_bActive_229() const { return ___m_bActive_229; }
	inline bool* get_address_of_m_bActive_229() { return &___m_bActive_229; }
	inline void set_m_bActive_229(bool value)
	{
		___m_bActive_229 = value;
	}

	inline static int32_t get_offset_of_m_nGuid_230() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nGuid_230)); }
	inline int32_t get_m_nGuid_230() const { return ___m_nGuid_230; }
	inline int32_t* get_address_of_m_nGuid_230() { return &___m_nGuid_230; }
	inline void set_m_nGuid_230(int32_t value)
	{
		___m_nGuid_230 = value;
	}

	inline static int32_t get_offset_of_m_bEaten_231() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bEaten_231)); }
	inline bool get_m_bEaten_231() const { return ___m_bEaten_231; }
	inline bool* get_address_of_m_bEaten_231() { return &___m_bEaten_231; }
	inline void set_m_bEaten_231(bool value)
	{
		___m_bEaten_231 = value;
	}

	inline static int32_t get_offset_of_m_vecDest_232() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecDest_232)); }
	inline Vector3_t3722313464  get_m_vecDest_232() const { return ___m_vecDest_232; }
	inline Vector3_t3722313464 * get_address_of_m_vecDest_232() { return &___m_vecDest_232; }
	inline void set_m_vecDest_232(Vector3_t3722313464  value)
	{
		___m_vecDest_232 = value;
	}

	inline static int32_t get_offset_of_m_bIsMovingToDest_233() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bIsMovingToDest_233)); }
	inline bool get_m_bIsMovingToDest_233() const { return ___m_bIsMovingToDest_233; }
	inline bool* get_address_of_m_bIsMovingToDest_233() { return &___m_bIsMovingToDest_233; }
	inline void set_m_bIsMovingToDest_233(bool value)
	{
		___m_bIsMovingToDest_233 = value;
	}

	inline static int32_t get_offset_of_m_vecMoveToDestDir_234() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_vecMoveToDestDir_234)); }
	inline Vector2_t2156229523  get_m_vecMoveToDestDir_234() const { return ___m_vecMoveToDestDir_234; }
	inline Vector2_t2156229523 * get_address_of_m_vecMoveToDestDir_234() { return &___m_vecMoveToDestDir_234; }
	inline void set_m_vecMoveToDestDir_234(Vector2_t2156229523  value)
	{
		___m_vecMoveToDestDir_234 = value;
	}

	inline static int32_t get_offset_of_m_dicDust_235() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_dicDust_235)); }
	inline Dictionary_2_t1909897157 * get_m_dicDust_235() const { return ___m_dicDust_235; }
	inline Dictionary_2_t1909897157 ** get_address_of_m_dicDust_235() { return &___m_dicDust_235; }
	inline void set_m_dicDust_235(Dictionary_2_t1909897157 * value)
	{
		___m_dicDust_235 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicDust_235), value);
	}

	inline static int32_t get_offset_of_m_nOp_236() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nOp_236)); }
	inline int32_t get_m_nOp_236() const { return ___m_nOp_236; }
	inline int32_t* get_address_of_m_nOp_236() { return &___m_nOp_236; }
	inline void set_m_nOp_236(int32_t value)
	{
		___m_nOp_236 = value;
	}

	inline static int32_t get_offset_of_m_nOpScale_237() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nOpScale_237)); }
	inline int32_t get_m_nOpScale_237() const { return ___m_nOpScale_237; }
	inline int32_t* get_address_of_m_nOpScale_237() { return &___m_nOpScale_237; }
	inline void set_m_nOpScale_237(int32_t value)
	{
		___m_nOpScale_237 = value;
	}

	inline static int32_t get_offset_of_m_fBlingTime_240() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_fBlingTime_240)); }
	inline float get_m_fBlingTime_240() const { return ___m_fBlingTime_240; }
	inline float* get_address_of_m_fBlingTime_240() { return &___m_fBlingTime_240; }
	inline void set_m_fBlingTime_240(float value)
	{
		___m_fBlingTime_240 = value;
	}

	inline static int32_t get_offset_of_m_nAutoMove_241() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_nAutoMove_241)); }
	inline int32_t get_m_nAutoMove_241() const { return ___m_nAutoMove_241; }
	inline int32_t* get_address_of_m_nAutoMove_241() { return &___m_nAutoMove_241; }
	inline void set_m_nAutoMove_241(int32_t value)
	{
		___m_nAutoMove_241 = value;
	}

	inline static int32_t get_offset_of_m_goZhangYu_242() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_goZhangYu_242)); }
	inline CZhangYu_t3000622058 * get_m_goZhangYu_242() const { return ___m_goZhangYu_242; }
	inline CZhangYu_t3000622058 ** get_address_of_m_goZhangYu_242() { return &___m_goZhangYu_242; }
	inline void set_m_goZhangYu_242(CZhangYu_t3000622058 * value)
	{
		___m_goZhangYu_242 = value;
		Il2CppCodeGenWriteBarrier((&___m_goZhangYu_242), value);
	}

	inline static int32_t get_offset_of_bPlayerNameVisible_243() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___bPlayerNameVisible_243)); }
	inline bool get_bPlayerNameVisible_243() const { return ___bPlayerNameVisible_243; }
	inline bool* get_address_of_bPlayerNameVisible_243() { return &___bPlayerNameVisible_243; }
	inline void set_bPlayerNameVisible_243(bool value)
	{
		___bPlayerNameVisible_243 = value;
	}

	inline static int32_t get_offset_of_bPlayerNameSizeControl_244() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___bPlayerNameSizeControl_244)); }
	inline bool get_bPlayerNameSizeControl_244() const { return ___bPlayerNameSizeControl_244; }
	inline bool* get_address_of_bPlayerNameSizeControl_244() { return &___bPlayerNameSizeControl_244; }
	inline void set_bPlayerNameSizeControl_244(bool value)
	{
		___bPlayerNameSizeControl_244 = value;
	}

	inline static int32_t get_offset_of_m_bPicked_245() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_bPicked_245)); }
	inline bool get_m_bPicked_245() const { return ___m_bPicked_245; }
	inline bool* get_address_of_m_bPicked_245() { return &___m_bPicked_245; }
	inline void set_m_bPicked_245(bool value)
	{
		___m_bPicked_245 = value;
	}

	inline static int32_t get_offset_of_m_aryTiaoZiTunShi_246() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_aryTiaoZiTunShi_246)); }
	inline CTiaoZiU5BU5D_t3180227329* get_m_aryTiaoZiTunShi_246() const { return ___m_aryTiaoZiTunShi_246; }
	inline CTiaoZiU5BU5D_t3180227329** get_address_of_m_aryTiaoZiTunShi_246() { return &___m_aryTiaoZiTunShi_246; }
	inline void set_m_aryTiaoZiTunShi_246(CTiaoZiU5BU5D_t3180227329* value)
	{
		___m_aryTiaoZiTunShi_246 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTiaoZiTunShi_246), value);
	}

	inline static int32_t get_offset_of_m_lstIgnoredTeammate_247() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_lstIgnoredTeammate_247)); }
	inline List_1_t3678741308 * get_m_lstIgnoredTeammate_247() const { return ___m_lstIgnoredTeammate_247; }
	inline List_1_t3678741308 ** get_address_of_m_lstIgnoredTeammate_247() { return &___m_lstIgnoredTeammate_247; }
	inline void set_m_lstIgnoredTeammate_247(List_1_t3678741308 * value)
	{
		___m_lstIgnoredTeammate_247 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstIgnoredTeammate_247), value);
	}

	inline static int32_t get_offset_of_m_flastTimeElapse_248() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___m_flastTimeElapse_248)); }
	inline float get_m_flastTimeElapse_248() const { return ___m_flastTimeElapse_248; }
	inline float* get_address_of_m_flastTimeElapse_248() { return &___m_flastTimeElapse_248; }
	inline void set_m_flastTimeElapse_248(float value)
	{
		___m_flastTimeElapse_248 = value;
	}
};

struct Ball_t2206666566_StaticFields
{
public:
	// UnityEngine.Vector3 Ball::vecTempSize
	Vector3_t3722313464  ___vecTempSize_48;
	// UnityEngine.Vector3 Ball::vecTempPos
	Vector3_t3722313464  ___vecTempPos_49;
	// UnityEngine.Vector3 Ball::vecTempPos2
	Vector3_t3722313464  ___vecTempPos2_50;
	// UnityEngine.Vector3 Ball::vecTempScale
	Vector3_t3722313464  ___vecTempScale_51;
	// UnityEngine.Vector2 Ball::vec2Temp
	Vector2_t2156229523  ___vec2Temp_52;
	// UnityEngine.Vector2 Ball::vec2Temp1
	Vector2_t2156229523  ___vec2Temp1_53;
	// UnityEngine.Color Ball::cTempColor
	Color_t2555686324  ___cTempColor_54;
	// UnityEngine.Color Ball::colorTemp
	Color_t2555686324  ___colorTemp_55;
	// UnityEngine.Vector3 Ball::vecTempDir
	Vector3_t3722313464  ___vecTempDir_56;
	// UnityEngine.Vector3 Ball::vecTempDir2
	Vector3_t3722313464  ___vecTempDir2_57;
	// UnityEngine.Vector3 Ball::s_vecShellInitScale
	Vector3_t3722313464  ___s_vecShellInitScale_65;
	// System.Int32 Ball::s_nShitSortOrderIndex
	int32_t ___s_nShitSortOrderIndex_89;
	// System.Single[] Ball::s_aryShitSortOrder
	SingleU5BU5D_t1444911251* ___s_aryShitSortOrder_90;
	// System.Int32 Ball::s_AddNum
	int32_t ___s_AddNum_133;
	// System.Int32 Ball::s_RemoveNum
	int32_t ___s_RemoveNum_134;
	// UnityEngine.Vector2[] Ball::s_aryBallExplodeDirection
	Vector2U5BU5D_t1457185986* ___s_aryBallExplodeDirection_137;
	// UnityEngine.Vector3 Ball::vecTempPianYi
	Vector3_t3722313464  ___vecTempPianYi_199;

public:
	inline static int32_t get_offset_of_vecTempSize_48() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempSize_48)); }
	inline Vector3_t3722313464  get_vecTempSize_48() const { return ___vecTempSize_48; }
	inline Vector3_t3722313464 * get_address_of_vecTempSize_48() { return &___vecTempSize_48; }
	inline void set_vecTempSize_48(Vector3_t3722313464  value)
	{
		___vecTempSize_48 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_49() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempPos_49)); }
	inline Vector3_t3722313464  get_vecTempPos_49() const { return ___vecTempPos_49; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_49() { return &___vecTempPos_49; }
	inline void set_vecTempPos_49(Vector3_t3722313464  value)
	{
		___vecTempPos_49 = value;
	}

	inline static int32_t get_offset_of_vecTempPos2_50() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempPos2_50)); }
	inline Vector3_t3722313464  get_vecTempPos2_50() const { return ___vecTempPos2_50; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos2_50() { return &___vecTempPos2_50; }
	inline void set_vecTempPos2_50(Vector3_t3722313464  value)
	{
		___vecTempPos2_50 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_51() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempScale_51)); }
	inline Vector3_t3722313464  get_vecTempScale_51() const { return ___vecTempScale_51; }
	inline Vector3_t3722313464 * get_address_of_vecTempScale_51() { return &___vecTempScale_51; }
	inline void set_vecTempScale_51(Vector3_t3722313464  value)
	{
		___vecTempScale_51 = value;
	}

	inline static int32_t get_offset_of_vec2Temp_52() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vec2Temp_52)); }
	inline Vector2_t2156229523  get_vec2Temp_52() const { return ___vec2Temp_52; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_52() { return &___vec2Temp_52; }
	inline void set_vec2Temp_52(Vector2_t2156229523  value)
	{
		___vec2Temp_52 = value;
	}

	inline static int32_t get_offset_of_vec2Temp1_53() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vec2Temp1_53)); }
	inline Vector2_t2156229523  get_vec2Temp1_53() const { return ___vec2Temp1_53; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp1_53() { return &___vec2Temp1_53; }
	inline void set_vec2Temp1_53(Vector2_t2156229523  value)
	{
		___vec2Temp1_53 = value;
	}

	inline static int32_t get_offset_of_cTempColor_54() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___cTempColor_54)); }
	inline Color_t2555686324  get_cTempColor_54() const { return ___cTempColor_54; }
	inline Color_t2555686324 * get_address_of_cTempColor_54() { return &___cTempColor_54; }
	inline void set_cTempColor_54(Color_t2555686324  value)
	{
		___cTempColor_54 = value;
	}

	inline static int32_t get_offset_of_colorTemp_55() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___colorTemp_55)); }
	inline Color_t2555686324  get_colorTemp_55() const { return ___colorTemp_55; }
	inline Color_t2555686324 * get_address_of_colorTemp_55() { return &___colorTemp_55; }
	inline void set_colorTemp_55(Color_t2555686324  value)
	{
		___colorTemp_55 = value;
	}

	inline static int32_t get_offset_of_vecTempDir_56() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempDir_56)); }
	inline Vector3_t3722313464  get_vecTempDir_56() const { return ___vecTempDir_56; }
	inline Vector3_t3722313464 * get_address_of_vecTempDir_56() { return &___vecTempDir_56; }
	inline void set_vecTempDir_56(Vector3_t3722313464  value)
	{
		___vecTempDir_56 = value;
	}

	inline static int32_t get_offset_of_vecTempDir2_57() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempDir2_57)); }
	inline Vector3_t3722313464  get_vecTempDir2_57() const { return ___vecTempDir2_57; }
	inline Vector3_t3722313464 * get_address_of_vecTempDir2_57() { return &___vecTempDir2_57; }
	inline void set_vecTempDir2_57(Vector3_t3722313464  value)
	{
		___vecTempDir2_57 = value;
	}

	inline static int32_t get_offset_of_s_vecShellInitScale_65() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___s_vecShellInitScale_65)); }
	inline Vector3_t3722313464  get_s_vecShellInitScale_65() const { return ___s_vecShellInitScale_65; }
	inline Vector3_t3722313464 * get_address_of_s_vecShellInitScale_65() { return &___s_vecShellInitScale_65; }
	inline void set_s_vecShellInitScale_65(Vector3_t3722313464  value)
	{
		___s_vecShellInitScale_65 = value;
	}

	inline static int32_t get_offset_of_s_nShitSortOrderIndex_89() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___s_nShitSortOrderIndex_89)); }
	inline int32_t get_s_nShitSortOrderIndex_89() const { return ___s_nShitSortOrderIndex_89; }
	inline int32_t* get_address_of_s_nShitSortOrderIndex_89() { return &___s_nShitSortOrderIndex_89; }
	inline void set_s_nShitSortOrderIndex_89(int32_t value)
	{
		___s_nShitSortOrderIndex_89 = value;
	}

	inline static int32_t get_offset_of_s_aryShitSortOrder_90() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___s_aryShitSortOrder_90)); }
	inline SingleU5BU5D_t1444911251* get_s_aryShitSortOrder_90() const { return ___s_aryShitSortOrder_90; }
	inline SingleU5BU5D_t1444911251** get_address_of_s_aryShitSortOrder_90() { return &___s_aryShitSortOrder_90; }
	inline void set_s_aryShitSortOrder_90(SingleU5BU5D_t1444911251* value)
	{
		___s_aryShitSortOrder_90 = value;
		Il2CppCodeGenWriteBarrier((&___s_aryShitSortOrder_90), value);
	}

	inline static int32_t get_offset_of_s_AddNum_133() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___s_AddNum_133)); }
	inline int32_t get_s_AddNum_133() const { return ___s_AddNum_133; }
	inline int32_t* get_address_of_s_AddNum_133() { return &___s_AddNum_133; }
	inline void set_s_AddNum_133(int32_t value)
	{
		___s_AddNum_133 = value;
	}

	inline static int32_t get_offset_of_s_RemoveNum_134() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___s_RemoveNum_134)); }
	inline int32_t get_s_RemoveNum_134() const { return ___s_RemoveNum_134; }
	inline int32_t* get_address_of_s_RemoveNum_134() { return &___s_RemoveNum_134; }
	inline void set_s_RemoveNum_134(int32_t value)
	{
		___s_RemoveNum_134 = value;
	}

	inline static int32_t get_offset_of_s_aryBallExplodeDirection_137() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___s_aryBallExplodeDirection_137)); }
	inline Vector2U5BU5D_t1457185986* get_s_aryBallExplodeDirection_137() const { return ___s_aryBallExplodeDirection_137; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_aryBallExplodeDirection_137() { return &___s_aryBallExplodeDirection_137; }
	inline void set_s_aryBallExplodeDirection_137(Vector2U5BU5D_t1457185986* value)
	{
		___s_aryBallExplodeDirection_137 = value;
		Il2CppCodeGenWriteBarrier((&___s_aryBallExplodeDirection_137), value);
	}

	inline static int32_t get_offset_of_vecTempPianYi_199() { return static_cast<int32_t>(offsetof(Ball_t2206666566_StaticFields, ___vecTempPianYi_199)); }
	inline Vector3_t3722313464  get_vecTempPianYi_199() const { return ___vecTempPianYi_199; }
	inline Vector3_t3722313464 * get_address_of_vecTempPianYi_199() { return &___vecTempPianYi_199; }
	inline void set_vecTempPianYi_199(Vector3_t3722313464  value)
	{
		___vecTempPianYi_199 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2206666566_H
#ifndef CPLAYERINS_T3208190728_H
#define CPLAYERINS_T3208190728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CPlayerIns
struct  CPlayerIns_t3208190728  : public MonoBehaviour_t3962482529
{
public:
	// System.String CPlayerIns::m_szAccount
	String_t* ___m_szAccount_2;
	// System.Collections.Generic.List`1<Ball> CPlayerIns::m_lstBalls
	List_1_t3678741308 * ___m_lstBalls_3;
	// Player CPlayerIns::_Player
	Player_t3266647312 * ____Player_4;
	// System.Boolean CPlayerIns::m_bIniting
	bool ___m_bIniting_6;
	// System.Int32 CPlayerIns::m_nSkinId
	int32_t ___m_nSkinId_7;
	// UnityEngine.Color CPlayerIns::m_Color
	Color_t2555686324  ___m_Color_8;
	// System.Int32 CPlayerIns::m_nColorId
	int32_t ___m_nColorId_9;
	// System.String CPlayerIns::m_szPlayerName
	String_t* ___m_szPlayerName_10;
	// System.Byte[] CPlayerIns::m_bytesBallsData
	ByteU5BU5D_t4116647657* ___m_bytesBallsData_11;
	// System.Single CPlayerIns::m_fBeginInitingTime
	float ___m_fBeginInitingTime_12;
	// System.Single CPlayerIns::m_fInitingBallsTimeCount
	float ___m_fInitingBallsTimeCount_14;
	// System.Int32 CPlayerIns::m_nInitingIndex
	int32_t ___m_nInitingIndex_15;
	// System.Byte[] CPlayerIns::_bytesLiveBallData
	ByteU5BU5D_t4116647657* ____bytesLiveBallData_16;
	// System.Int32 CPlayerIns::m_nKillCount
	int32_t ___m_nKillCount_17;
	// System.Int32 CPlayerIns::m_nBeKilledCount
	int32_t ___m_nBeKilledCount_18;
	// System.Single CPlayerIns::m_fIntervalLoopTimeElapse_0_3
	float ___m_fIntervalLoopTimeElapse_0_3_19;
	// System.Single CPlayerIns::m_fIntervalLoopTimeElapse_1
	float ___m_fIntervalLoopTimeElapse_1_20;
	// System.Single CPlayerIns::m_fIntervalLoopTimeElapse_0_1
	float ___m_fIntervalLoopTimeElapse_0_1_21;
	// System.Single CPlayerIns::m_fAttenuateTimeLapse
	float ___m_fAttenuateTimeLapse_22;
	// Ball[] CPlayerIns::m_aryLiveBalls
	BallU5BU5D_t2391825635* ___m_aryLiveBalls_23;
	// System.Collections.Generic.List`1<System.Int32> CPlayerIns::m_lstNewDeadBall
	List_1_t128053199 * ___m_lstNewDeadBall_24;
	// System.Boolean CPlayerIns::m_bDead
	bool ___m_bDead_25;
	// System.Collections.Generic.List`1<CExplodeNode> CPlayerIns::m_lstExplodingNode
	List_1_t1569919409 * ___m_lstExplodingNode_26;
	// System.Collections.Generic.List`1<CExplodeNode> CPlayerIns::m_lstRecycledExplodeNode
	List_1_t1569919409 * ___m_lstRecycledExplodeNode_27;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CPlayerIns::m_dicBallsToReuse_New
	Dictionary_2_t1839659084 * ___m_dicBallsToReuse_New_28;
	// System.Collections.Generic.Dictionary`2<System.Int32,Ball> CPlayerIns::m_dicBallsToReuse
	Dictionary_2_t1095379897 * ___m_dicBallsToReuse_29;
	// System.Int16[] CPlayerIns::m_arySkillStatus
	Int16U5BU5D_t3686840178* ___m_arySkillStatus_30;
	// CSkillSystem/eSkillId CPlayerIns::m_eSkillId
	int32_t ___m_eSkillId_31;
	// System.Single CPlayerIns::m_fSkillTimeElapse
	float ___m_fSkillTimeElapse_32;
	// System.Single CPlayerIns::m_fSkillQianYaoTime
	float ___m_fSkillQianYaoTime_33;
	// System.Single CPlayerIns::m_fSkillTimeElapse_Sneak
	float ___m_fSkillTimeElapse_Sneak_34;
	// System.Single CPlayerIns::m_fSkillTotalTime
	float ___m_fSkillTotalTime_35;
	// System.Single CPlayerIns::m_fSkillQianYaoTime_Sneak
	float ___m_fSkillQianYaoTime_Sneak_36;
	// System.Int16 CPlayerIns::m_nSKillStatus_KuoZhang
	int16_t ___m_nSKillStatus_KuoZhang_37;
	// System.Single CPlayerIns::m_nSKill_QianYao_KuoZhang
	float ___m_nSKill_QianYao_KuoZhang_38;
	// System.Single CPlayerIns::m_nSKill_ChiXu_KuoZhang
	float ___m_nSKill_ChiXu_KuoZhang_39;
	// System.Single CPlayerIns::m_fTimeElapse_KuoZhang
	float ___m_fTimeElapse_KuoZhang_40;
	// System.Single CPlayerIns::m_fKuoZhangAngle
	float ___m_fKuoZhangAngle_41;
	// System.Single CPlayerIns::m_fKuoZhangUnfoldScale
	float ___m_fKuoZhangUnfoldScale_42;
	// System.Single CPlayerIns::m_fKuoKuoZhangParam1
	float ___m_fKuoKuoZhangParam1_43;
	// System.Single CPlayerIns::m_fKuoKuoZhangParam2
	float ___m_fKuoKuoZhangParam2_44;
	// CPlayerIns/eKuoZhangStatus CPlayerIns::m_eKuoZhangStatus
	int32_t ___m_eKuoZhangStatus_45;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CPlayerIns::m_dicMostBigBallsToShowName
	Dictionary_2_t1839659084 * ___m_dicMostBigBallsToShowName_46;

public:
	inline static int32_t get_offset_of_m_szAccount_2() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_szAccount_2)); }
	inline String_t* get_m_szAccount_2() const { return ___m_szAccount_2; }
	inline String_t** get_address_of_m_szAccount_2() { return &___m_szAccount_2; }
	inline void set_m_szAccount_2(String_t* value)
	{
		___m_szAccount_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_szAccount_2), value);
	}

	inline static int32_t get_offset_of_m_lstBalls_3() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_lstBalls_3)); }
	inline List_1_t3678741308 * get_m_lstBalls_3() const { return ___m_lstBalls_3; }
	inline List_1_t3678741308 ** get_address_of_m_lstBalls_3() { return &___m_lstBalls_3; }
	inline void set_m_lstBalls_3(List_1_t3678741308 * value)
	{
		___m_lstBalls_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBalls_3), value);
	}

	inline static int32_t get_offset_of__Player_4() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ____Player_4)); }
	inline Player_t3266647312 * get__Player_4() const { return ____Player_4; }
	inline Player_t3266647312 ** get_address_of__Player_4() { return &____Player_4; }
	inline void set__Player_4(Player_t3266647312 * value)
	{
		____Player_4 = value;
		Il2CppCodeGenWriteBarrier((&____Player_4), value);
	}

	inline static int32_t get_offset_of_m_bIniting_6() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_bIniting_6)); }
	inline bool get_m_bIniting_6() const { return ___m_bIniting_6; }
	inline bool* get_address_of_m_bIniting_6() { return &___m_bIniting_6; }
	inline void set_m_bIniting_6(bool value)
	{
		___m_bIniting_6 = value;
	}

	inline static int32_t get_offset_of_m_nSkinId_7() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nSkinId_7)); }
	inline int32_t get_m_nSkinId_7() const { return ___m_nSkinId_7; }
	inline int32_t* get_address_of_m_nSkinId_7() { return &___m_nSkinId_7; }
	inline void set_m_nSkinId_7(int32_t value)
	{
		___m_nSkinId_7 = value;
	}

	inline static int32_t get_offset_of_m_Color_8() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_Color_8)); }
	inline Color_t2555686324  get_m_Color_8() const { return ___m_Color_8; }
	inline Color_t2555686324 * get_address_of_m_Color_8() { return &___m_Color_8; }
	inline void set_m_Color_8(Color_t2555686324  value)
	{
		___m_Color_8 = value;
	}

	inline static int32_t get_offset_of_m_nColorId_9() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nColorId_9)); }
	inline int32_t get_m_nColorId_9() const { return ___m_nColorId_9; }
	inline int32_t* get_address_of_m_nColorId_9() { return &___m_nColorId_9; }
	inline void set_m_nColorId_9(int32_t value)
	{
		___m_nColorId_9 = value;
	}

	inline static int32_t get_offset_of_m_szPlayerName_10() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_szPlayerName_10)); }
	inline String_t* get_m_szPlayerName_10() const { return ___m_szPlayerName_10; }
	inline String_t** get_address_of_m_szPlayerName_10() { return &___m_szPlayerName_10; }
	inline void set_m_szPlayerName_10(String_t* value)
	{
		___m_szPlayerName_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_szPlayerName_10), value);
	}

	inline static int32_t get_offset_of_m_bytesBallsData_11() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_bytesBallsData_11)); }
	inline ByteU5BU5D_t4116647657* get_m_bytesBallsData_11() const { return ___m_bytesBallsData_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_bytesBallsData_11() { return &___m_bytesBallsData_11; }
	inline void set_m_bytesBallsData_11(ByteU5BU5D_t4116647657* value)
	{
		___m_bytesBallsData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_bytesBallsData_11), value);
	}

	inline static int32_t get_offset_of_m_fBeginInitingTime_12() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fBeginInitingTime_12)); }
	inline float get_m_fBeginInitingTime_12() const { return ___m_fBeginInitingTime_12; }
	inline float* get_address_of_m_fBeginInitingTime_12() { return &___m_fBeginInitingTime_12; }
	inline void set_m_fBeginInitingTime_12(float value)
	{
		___m_fBeginInitingTime_12 = value;
	}

	inline static int32_t get_offset_of_m_fInitingBallsTimeCount_14() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fInitingBallsTimeCount_14)); }
	inline float get_m_fInitingBallsTimeCount_14() const { return ___m_fInitingBallsTimeCount_14; }
	inline float* get_address_of_m_fInitingBallsTimeCount_14() { return &___m_fInitingBallsTimeCount_14; }
	inline void set_m_fInitingBallsTimeCount_14(float value)
	{
		___m_fInitingBallsTimeCount_14 = value;
	}

	inline static int32_t get_offset_of_m_nInitingIndex_15() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nInitingIndex_15)); }
	inline int32_t get_m_nInitingIndex_15() const { return ___m_nInitingIndex_15; }
	inline int32_t* get_address_of_m_nInitingIndex_15() { return &___m_nInitingIndex_15; }
	inline void set_m_nInitingIndex_15(int32_t value)
	{
		___m_nInitingIndex_15 = value;
	}

	inline static int32_t get_offset_of__bytesLiveBallData_16() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ____bytesLiveBallData_16)); }
	inline ByteU5BU5D_t4116647657* get__bytesLiveBallData_16() const { return ____bytesLiveBallData_16; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytesLiveBallData_16() { return &____bytesLiveBallData_16; }
	inline void set__bytesLiveBallData_16(ByteU5BU5D_t4116647657* value)
	{
		____bytesLiveBallData_16 = value;
		Il2CppCodeGenWriteBarrier((&____bytesLiveBallData_16), value);
	}

	inline static int32_t get_offset_of_m_nKillCount_17() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nKillCount_17)); }
	inline int32_t get_m_nKillCount_17() const { return ___m_nKillCount_17; }
	inline int32_t* get_address_of_m_nKillCount_17() { return &___m_nKillCount_17; }
	inline void set_m_nKillCount_17(int32_t value)
	{
		___m_nKillCount_17 = value;
	}

	inline static int32_t get_offset_of_m_nBeKilledCount_18() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nBeKilledCount_18)); }
	inline int32_t get_m_nBeKilledCount_18() const { return ___m_nBeKilledCount_18; }
	inline int32_t* get_address_of_m_nBeKilledCount_18() { return &___m_nBeKilledCount_18; }
	inline void set_m_nBeKilledCount_18(int32_t value)
	{
		___m_nBeKilledCount_18 = value;
	}

	inline static int32_t get_offset_of_m_fIntervalLoopTimeElapse_0_3_19() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fIntervalLoopTimeElapse_0_3_19)); }
	inline float get_m_fIntervalLoopTimeElapse_0_3_19() const { return ___m_fIntervalLoopTimeElapse_0_3_19; }
	inline float* get_address_of_m_fIntervalLoopTimeElapse_0_3_19() { return &___m_fIntervalLoopTimeElapse_0_3_19; }
	inline void set_m_fIntervalLoopTimeElapse_0_3_19(float value)
	{
		___m_fIntervalLoopTimeElapse_0_3_19 = value;
	}

	inline static int32_t get_offset_of_m_fIntervalLoopTimeElapse_1_20() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fIntervalLoopTimeElapse_1_20)); }
	inline float get_m_fIntervalLoopTimeElapse_1_20() const { return ___m_fIntervalLoopTimeElapse_1_20; }
	inline float* get_address_of_m_fIntervalLoopTimeElapse_1_20() { return &___m_fIntervalLoopTimeElapse_1_20; }
	inline void set_m_fIntervalLoopTimeElapse_1_20(float value)
	{
		___m_fIntervalLoopTimeElapse_1_20 = value;
	}

	inline static int32_t get_offset_of_m_fIntervalLoopTimeElapse_0_1_21() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fIntervalLoopTimeElapse_0_1_21)); }
	inline float get_m_fIntervalLoopTimeElapse_0_1_21() const { return ___m_fIntervalLoopTimeElapse_0_1_21; }
	inline float* get_address_of_m_fIntervalLoopTimeElapse_0_1_21() { return &___m_fIntervalLoopTimeElapse_0_1_21; }
	inline void set_m_fIntervalLoopTimeElapse_0_1_21(float value)
	{
		___m_fIntervalLoopTimeElapse_0_1_21 = value;
	}

	inline static int32_t get_offset_of_m_fAttenuateTimeLapse_22() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fAttenuateTimeLapse_22)); }
	inline float get_m_fAttenuateTimeLapse_22() const { return ___m_fAttenuateTimeLapse_22; }
	inline float* get_address_of_m_fAttenuateTimeLapse_22() { return &___m_fAttenuateTimeLapse_22; }
	inline void set_m_fAttenuateTimeLapse_22(float value)
	{
		___m_fAttenuateTimeLapse_22 = value;
	}

	inline static int32_t get_offset_of_m_aryLiveBalls_23() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_aryLiveBalls_23)); }
	inline BallU5BU5D_t2391825635* get_m_aryLiveBalls_23() const { return ___m_aryLiveBalls_23; }
	inline BallU5BU5D_t2391825635** get_address_of_m_aryLiveBalls_23() { return &___m_aryLiveBalls_23; }
	inline void set_m_aryLiveBalls_23(BallU5BU5D_t2391825635* value)
	{
		___m_aryLiveBalls_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLiveBalls_23), value);
	}

	inline static int32_t get_offset_of_m_lstNewDeadBall_24() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_lstNewDeadBall_24)); }
	inline List_1_t128053199 * get_m_lstNewDeadBall_24() const { return ___m_lstNewDeadBall_24; }
	inline List_1_t128053199 ** get_address_of_m_lstNewDeadBall_24() { return &___m_lstNewDeadBall_24; }
	inline void set_m_lstNewDeadBall_24(List_1_t128053199 * value)
	{
		___m_lstNewDeadBall_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstNewDeadBall_24), value);
	}

	inline static int32_t get_offset_of_m_bDead_25() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_bDead_25)); }
	inline bool get_m_bDead_25() const { return ___m_bDead_25; }
	inline bool* get_address_of_m_bDead_25() { return &___m_bDead_25; }
	inline void set_m_bDead_25(bool value)
	{
		___m_bDead_25 = value;
	}

	inline static int32_t get_offset_of_m_lstExplodingNode_26() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_lstExplodingNode_26)); }
	inline List_1_t1569919409 * get_m_lstExplodingNode_26() const { return ___m_lstExplodingNode_26; }
	inline List_1_t1569919409 ** get_address_of_m_lstExplodingNode_26() { return &___m_lstExplodingNode_26; }
	inline void set_m_lstExplodingNode_26(List_1_t1569919409 * value)
	{
		___m_lstExplodingNode_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstExplodingNode_26), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledExplodeNode_27() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_lstRecycledExplodeNode_27)); }
	inline List_1_t1569919409 * get_m_lstRecycledExplodeNode_27() const { return ___m_lstRecycledExplodeNode_27; }
	inline List_1_t1569919409 ** get_address_of_m_lstRecycledExplodeNode_27() { return &___m_lstRecycledExplodeNode_27; }
	inline void set_m_lstRecycledExplodeNode_27(List_1_t1569919409 * value)
	{
		___m_lstRecycledExplodeNode_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledExplodeNode_27), value);
	}

	inline static int32_t get_offset_of_m_dicBallsToReuse_New_28() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_dicBallsToReuse_New_28)); }
	inline Dictionary_2_t1839659084 * get_m_dicBallsToReuse_New_28() const { return ___m_dicBallsToReuse_New_28; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicBallsToReuse_New_28() { return &___m_dicBallsToReuse_New_28; }
	inline void set_m_dicBallsToReuse_New_28(Dictionary_2_t1839659084 * value)
	{
		___m_dicBallsToReuse_New_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBallsToReuse_New_28), value);
	}

	inline static int32_t get_offset_of_m_dicBallsToReuse_29() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_dicBallsToReuse_29)); }
	inline Dictionary_2_t1095379897 * get_m_dicBallsToReuse_29() const { return ___m_dicBallsToReuse_29; }
	inline Dictionary_2_t1095379897 ** get_address_of_m_dicBallsToReuse_29() { return &___m_dicBallsToReuse_29; }
	inline void set_m_dicBallsToReuse_29(Dictionary_2_t1095379897 * value)
	{
		___m_dicBallsToReuse_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicBallsToReuse_29), value);
	}

	inline static int32_t get_offset_of_m_arySkillStatus_30() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_arySkillStatus_30)); }
	inline Int16U5BU5D_t3686840178* get_m_arySkillStatus_30() const { return ___m_arySkillStatus_30; }
	inline Int16U5BU5D_t3686840178** get_address_of_m_arySkillStatus_30() { return &___m_arySkillStatus_30; }
	inline void set_m_arySkillStatus_30(Int16U5BU5D_t3686840178* value)
	{
		___m_arySkillStatus_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillStatus_30), value);
	}

	inline static int32_t get_offset_of_m_eSkillId_31() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_eSkillId_31)); }
	inline int32_t get_m_eSkillId_31() const { return ___m_eSkillId_31; }
	inline int32_t* get_address_of_m_eSkillId_31() { return &___m_eSkillId_31; }
	inline void set_m_eSkillId_31(int32_t value)
	{
		___m_eSkillId_31 = value;
	}

	inline static int32_t get_offset_of_m_fSkillTimeElapse_32() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fSkillTimeElapse_32)); }
	inline float get_m_fSkillTimeElapse_32() const { return ___m_fSkillTimeElapse_32; }
	inline float* get_address_of_m_fSkillTimeElapse_32() { return &___m_fSkillTimeElapse_32; }
	inline void set_m_fSkillTimeElapse_32(float value)
	{
		___m_fSkillTimeElapse_32 = value;
	}

	inline static int32_t get_offset_of_m_fSkillQianYaoTime_33() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fSkillQianYaoTime_33)); }
	inline float get_m_fSkillQianYaoTime_33() const { return ___m_fSkillQianYaoTime_33; }
	inline float* get_address_of_m_fSkillQianYaoTime_33() { return &___m_fSkillQianYaoTime_33; }
	inline void set_m_fSkillQianYaoTime_33(float value)
	{
		___m_fSkillQianYaoTime_33 = value;
	}

	inline static int32_t get_offset_of_m_fSkillTimeElapse_Sneak_34() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fSkillTimeElapse_Sneak_34)); }
	inline float get_m_fSkillTimeElapse_Sneak_34() const { return ___m_fSkillTimeElapse_Sneak_34; }
	inline float* get_address_of_m_fSkillTimeElapse_Sneak_34() { return &___m_fSkillTimeElapse_Sneak_34; }
	inline void set_m_fSkillTimeElapse_Sneak_34(float value)
	{
		___m_fSkillTimeElapse_Sneak_34 = value;
	}

	inline static int32_t get_offset_of_m_fSkillTotalTime_35() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fSkillTotalTime_35)); }
	inline float get_m_fSkillTotalTime_35() const { return ___m_fSkillTotalTime_35; }
	inline float* get_address_of_m_fSkillTotalTime_35() { return &___m_fSkillTotalTime_35; }
	inline void set_m_fSkillTotalTime_35(float value)
	{
		___m_fSkillTotalTime_35 = value;
	}

	inline static int32_t get_offset_of_m_fSkillQianYaoTime_Sneak_36() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fSkillQianYaoTime_Sneak_36)); }
	inline float get_m_fSkillQianYaoTime_Sneak_36() const { return ___m_fSkillQianYaoTime_Sneak_36; }
	inline float* get_address_of_m_fSkillQianYaoTime_Sneak_36() { return &___m_fSkillQianYaoTime_Sneak_36; }
	inline void set_m_fSkillQianYaoTime_Sneak_36(float value)
	{
		___m_fSkillQianYaoTime_Sneak_36 = value;
	}

	inline static int32_t get_offset_of_m_nSKillStatus_KuoZhang_37() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nSKillStatus_KuoZhang_37)); }
	inline int16_t get_m_nSKillStatus_KuoZhang_37() const { return ___m_nSKillStatus_KuoZhang_37; }
	inline int16_t* get_address_of_m_nSKillStatus_KuoZhang_37() { return &___m_nSKillStatus_KuoZhang_37; }
	inline void set_m_nSKillStatus_KuoZhang_37(int16_t value)
	{
		___m_nSKillStatus_KuoZhang_37 = value;
	}

	inline static int32_t get_offset_of_m_nSKill_QianYao_KuoZhang_38() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nSKill_QianYao_KuoZhang_38)); }
	inline float get_m_nSKill_QianYao_KuoZhang_38() const { return ___m_nSKill_QianYao_KuoZhang_38; }
	inline float* get_address_of_m_nSKill_QianYao_KuoZhang_38() { return &___m_nSKill_QianYao_KuoZhang_38; }
	inline void set_m_nSKill_QianYao_KuoZhang_38(float value)
	{
		___m_nSKill_QianYao_KuoZhang_38 = value;
	}

	inline static int32_t get_offset_of_m_nSKill_ChiXu_KuoZhang_39() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_nSKill_ChiXu_KuoZhang_39)); }
	inline float get_m_nSKill_ChiXu_KuoZhang_39() const { return ___m_nSKill_ChiXu_KuoZhang_39; }
	inline float* get_address_of_m_nSKill_ChiXu_KuoZhang_39() { return &___m_nSKill_ChiXu_KuoZhang_39; }
	inline void set_m_nSKill_ChiXu_KuoZhang_39(float value)
	{
		___m_nSKill_ChiXu_KuoZhang_39 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_KuoZhang_40() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fTimeElapse_KuoZhang_40)); }
	inline float get_m_fTimeElapse_KuoZhang_40() const { return ___m_fTimeElapse_KuoZhang_40; }
	inline float* get_address_of_m_fTimeElapse_KuoZhang_40() { return &___m_fTimeElapse_KuoZhang_40; }
	inline void set_m_fTimeElapse_KuoZhang_40(float value)
	{
		___m_fTimeElapse_KuoZhang_40 = value;
	}

	inline static int32_t get_offset_of_m_fKuoZhangAngle_41() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fKuoZhangAngle_41)); }
	inline float get_m_fKuoZhangAngle_41() const { return ___m_fKuoZhangAngle_41; }
	inline float* get_address_of_m_fKuoZhangAngle_41() { return &___m_fKuoZhangAngle_41; }
	inline void set_m_fKuoZhangAngle_41(float value)
	{
		___m_fKuoZhangAngle_41 = value;
	}

	inline static int32_t get_offset_of_m_fKuoZhangUnfoldScale_42() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fKuoZhangUnfoldScale_42)); }
	inline float get_m_fKuoZhangUnfoldScale_42() const { return ___m_fKuoZhangUnfoldScale_42; }
	inline float* get_address_of_m_fKuoZhangUnfoldScale_42() { return &___m_fKuoZhangUnfoldScale_42; }
	inline void set_m_fKuoZhangUnfoldScale_42(float value)
	{
		___m_fKuoZhangUnfoldScale_42 = value;
	}

	inline static int32_t get_offset_of_m_fKuoKuoZhangParam1_43() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fKuoKuoZhangParam1_43)); }
	inline float get_m_fKuoKuoZhangParam1_43() const { return ___m_fKuoKuoZhangParam1_43; }
	inline float* get_address_of_m_fKuoKuoZhangParam1_43() { return &___m_fKuoKuoZhangParam1_43; }
	inline void set_m_fKuoKuoZhangParam1_43(float value)
	{
		___m_fKuoKuoZhangParam1_43 = value;
	}

	inline static int32_t get_offset_of_m_fKuoKuoZhangParam2_44() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_fKuoKuoZhangParam2_44)); }
	inline float get_m_fKuoKuoZhangParam2_44() const { return ___m_fKuoKuoZhangParam2_44; }
	inline float* get_address_of_m_fKuoKuoZhangParam2_44() { return &___m_fKuoKuoZhangParam2_44; }
	inline void set_m_fKuoKuoZhangParam2_44(float value)
	{
		___m_fKuoKuoZhangParam2_44 = value;
	}

	inline static int32_t get_offset_of_m_eKuoZhangStatus_45() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_eKuoZhangStatus_45)); }
	inline int32_t get_m_eKuoZhangStatus_45() const { return ___m_eKuoZhangStatus_45; }
	inline int32_t* get_address_of_m_eKuoZhangStatus_45() { return &___m_eKuoZhangStatus_45; }
	inline void set_m_eKuoZhangStatus_45(int32_t value)
	{
		___m_eKuoZhangStatus_45 = value;
	}

	inline static int32_t get_offset_of_m_dicMostBigBallsToShowName_46() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728, ___m_dicMostBigBallsToShowName_46)); }
	inline Dictionary_2_t1839659084 * get_m_dicMostBigBallsToShowName_46() const { return ___m_dicMostBigBallsToShowName_46; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_dicMostBigBallsToShowName_46() { return &___m_dicMostBigBallsToShowName_46; }
	inline void set_m_dicMostBigBallsToShowName_46(Dictionary_2_t1839659084 * value)
	{
		___m_dicMostBigBallsToShowName_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicMostBigBallsToShowName_46), value);
	}
};

struct CPlayerIns_t3208190728_StaticFields
{
public:
	// UnityEngine.Vector3 CPlayerIns::vecTempPos
	Vector3_t3722313464  ___vecTempPos_5;

public:
	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(CPlayerIns_t3208190728_StaticFields, ___vecTempPos_5)); }
	inline Vector3_t3722313464  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_t3722313464 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_t3722313464  value)
	{
		___vecTempPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CPLAYERINS_T3208190728_H
#ifndef COMSMOSPOLYGON_T3500719902_H
#define COMSMOSPOLYGON_T3500719902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ComsmosPolygon
struct  ComsmosPolygon_t3500719902  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMSMOSPOLYGON_T3500719902_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef CSKILLMANAGER_T2937013327_H
#define CSKILLMANAGER_T2937013327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSkillManager
struct  CSkillManager_t2937013327  : public MonoBehaviour_t3962482529
{
public:
	// Player CSkillManager::m_Player
	Player_t3266647312 * ___m_Player_2;
	// Thorn[] CSkillManager::m_lstBabaThorn
	ThornU5BU5D_t4134692644* ___m_lstBabaThorn_3;

public:
	inline static int32_t get_offset_of_m_Player_2() { return static_cast<int32_t>(offsetof(CSkillManager_t2937013327, ___m_Player_2)); }
	inline Player_t3266647312 * get_m_Player_2() const { return ___m_Player_2; }
	inline Player_t3266647312 ** get_address_of_m_Player_2() { return &___m_Player_2; }
	inline void set_m_Player_2(Player_t3266647312 * value)
	{
		___m_Player_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Player_2), value);
	}

	inline static int32_t get_offset_of_m_lstBabaThorn_3() { return static_cast<int32_t>(offsetof(CSkillManager_t2937013327, ___m_lstBabaThorn_3)); }
	inline ThornU5BU5D_t4134692644* get_m_lstBabaThorn_3() const { return ___m_lstBabaThorn_3; }
	inline ThornU5BU5D_t4134692644** get_address_of_m_lstBabaThorn_3() { return &___m_lstBabaThorn_3; }
	inline void set_m_lstBabaThorn_3(ThornU5BU5D_t4134692644* value)
	{
		___m_lstBabaThorn_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBabaThorn_3), value);
	}
};

struct CSkillManager_t2937013327_StaticFields
{
public:
	// UnityEngine.Vector3 CSkillManager::vec3Temp
	Vector3_t3722313464  ___vec3Temp_4;
	// UnityEngine.Vector2 CSkillManager::vec2Temp
	Vector2_t2156229523  ___vec2Temp_5;

public:
	inline static int32_t get_offset_of_vec3Temp_4() { return static_cast<int32_t>(offsetof(CSkillManager_t2937013327_StaticFields, ___vec3Temp_4)); }
	inline Vector3_t3722313464  get_vec3Temp_4() const { return ___vec3Temp_4; }
	inline Vector3_t3722313464 * get_address_of_vec3Temp_4() { return &___vec3Temp_4; }
	inline void set_vec3Temp_4(Vector3_t3722313464  value)
	{
		___vec3Temp_4 = value;
	}

	inline static int32_t get_offset_of_vec2Temp_5() { return static_cast<int32_t>(offsetof(CSkillManager_t2937013327_StaticFields, ___vec2Temp_5)); }
	inline Vector2_t2156229523  get_vec2Temp_5() const { return ___vec2Temp_5; }
	inline Vector2_t2156229523 * get_address_of_vec2Temp_5() { return &___vec2Temp_5; }
	inline void set_vec2Temp_5(Vector2_t2156229523  value)
	{
		___vec2Temp_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSKILLMANAGER_T2937013327_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_2)); }
	inline RectOffset_t1369453676 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t1369453676 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_6)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_9)); }
	inline List_1_t881764471 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t881764471 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef LAYOUTELEMENT_T1785403678_H
#define LAYOUTELEMENT_T1785403678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_t1785403678  : public UIBehaviour_t3495933518
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_2;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_3;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_4;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_8;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_9;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_2() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_IgnoreLayout_2)); }
	inline bool get_m_IgnoreLayout_2() const { return ___m_IgnoreLayout_2; }
	inline bool* get_address_of_m_IgnoreLayout_2() { return &___m_IgnoreLayout_2; }
	inline void set_m_IgnoreLayout_2(bool value)
	{
		___m_IgnoreLayout_2 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_3() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_MinWidth_3)); }
	inline float get_m_MinWidth_3() const { return ___m_MinWidth_3; }
	inline float* get_address_of_m_MinWidth_3() { return &___m_MinWidth_3; }
	inline void set_m_MinWidth_3(float value)
	{
		___m_MinWidth_3 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_4() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_MinHeight_4)); }
	inline float get_m_MinHeight_4() const { return ___m_MinHeight_4; }
	inline float* get_address_of_m_MinHeight_4() { return &___m_MinHeight_4; }
	inline void set_m_MinHeight_4(float value)
	{
		___m_MinHeight_4 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_PreferredWidth_5)); }
	inline float get_m_PreferredWidth_5() const { return ___m_PreferredWidth_5; }
	inline float* get_address_of_m_PreferredWidth_5() { return &___m_PreferredWidth_5; }
	inline void set_m_PreferredWidth_5(float value)
	{
		___m_PreferredWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_PreferredHeight_6)); }
	inline float get_m_PreferredHeight_6() const { return ___m_PreferredHeight_6; }
	inline float* get_address_of_m_PreferredHeight_6() { return &___m_PreferredHeight_6; }
	inline void set_m_PreferredHeight_6(float value)
	{
		___m_PreferredHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_FlexibleWidth_7)); }
	inline float get_m_FlexibleWidth_7() const { return ___m_FlexibleWidth_7; }
	inline float* get_address_of_m_FlexibleWidth_7() { return &___m_FlexibleWidth_7; }
	inline void set_m_FlexibleWidth_7(float value)
	{
		___m_FlexibleWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_FlexibleHeight_8)); }
	inline float get_m_FlexibleHeight_8() const { return ___m_FlexibleHeight_8; }
	inline float* get_address_of_m_FlexibleHeight_8() { return &___m_FlexibleHeight_8; }
	inline void set_m_FlexibleHeight_8(float value)
	{
		___m_FlexibleHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_9() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_LayoutPriority_9)); }
	inline int32_t get_m_LayoutPriority_9() const { return ___m_LayoutPriority_9; }
	inline int32_t* get_address_of_m_LayoutPriority_9() { return &___m_LayoutPriority_9; }
	inline void set_m_LayoutPriority_9(int32_t value)
	{
		___m_LayoutPriority_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_T1785403678_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef CONTENTSIZEFITTER_T3850442145_H
#define CONTENTSIZEFITTER_T3850442145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t3850442145  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_2;
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_3;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_2() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_HorizontalFit_2)); }
	inline int32_t get_m_HorizontalFit_2() const { return ___m_HorizontalFit_2; }
	inline int32_t* get_address_of_m_HorizontalFit_2() { return &___m_HorizontalFit_2; }
	inline void set_m_HorizontalFit_2(int32_t value)
	{
		___m_HorizontalFit_2 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_3() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_VerticalFit_3)); }
	inline int32_t get_m_VerticalFit_3() const { return ___m_VerticalFit_3; }
	inline int32_t* get_address_of_m_VerticalFit_3() { return &___m_VerticalFit_3; }
	inline void set_m_VerticalFit_3(int32_t value)
	{
		___m_VerticalFit_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T3850442145_H
#ifndef QUICKSWIPE_T3226549687_H
#define QUICKSWIPE_T3226549687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe
struct  QuickSwipe_t3226549687  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickSwipe/OnSwipeAction HedgehogTeam.EasyTouch.QuickSwipe::onSwipeAction
	OnSwipeAction_t1754534479 * ___onSwipeAction_19;
	// System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::allowSwipeStartOverMe
	bool ___allowSwipeStartOverMe_20;
	// HedgehogTeam.EasyTouch.QuickSwipe/ActionTriggering HedgehogTeam.EasyTouch.QuickSwipe::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.QuickSwipe/SwipeDirection HedgehogTeam.EasyTouch.QuickSwipe::swipeDirection
	int32_t ___swipeDirection_22;
	// System.Single HedgehogTeam.EasyTouch.QuickSwipe::axisActionValue
	float ___axisActionValue_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::enableSimpleAction
	bool ___enableSimpleAction_24;

public:
	inline static int32_t get_offset_of_onSwipeAction_19() { return static_cast<int32_t>(offsetof(QuickSwipe_t3226549687, ___onSwipeAction_19)); }
	inline OnSwipeAction_t1754534479 * get_onSwipeAction_19() const { return ___onSwipeAction_19; }
	inline OnSwipeAction_t1754534479 ** get_address_of_onSwipeAction_19() { return &___onSwipeAction_19; }
	inline void set_onSwipeAction_19(OnSwipeAction_t1754534479 * value)
	{
		___onSwipeAction_19 = value;
		Il2CppCodeGenWriteBarrier((&___onSwipeAction_19), value);
	}

	inline static int32_t get_offset_of_allowSwipeStartOverMe_20() { return static_cast<int32_t>(offsetof(QuickSwipe_t3226549687, ___allowSwipeStartOverMe_20)); }
	inline bool get_allowSwipeStartOverMe_20() const { return ___allowSwipeStartOverMe_20; }
	inline bool* get_address_of_allowSwipeStartOverMe_20() { return &___allowSwipeStartOverMe_20; }
	inline void set_allowSwipeStartOverMe_20(bool value)
	{
		___allowSwipeStartOverMe_20 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickSwipe_t3226549687, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_swipeDirection_22() { return static_cast<int32_t>(offsetof(QuickSwipe_t3226549687, ___swipeDirection_22)); }
	inline int32_t get_swipeDirection_22() const { return ___swipeDirection_22; }
	inline int32_t* get_address_of_swipeDirection_22() { return &___swipeDirection_22; }
	inline void set_swipeDirection_22(int32_t value)
	{
		___swipeDirection_22 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_23() { return static_cast<int32_t>(offsetof(QuickSwipe_t3226549687, ___axisActionValue_23)); }
	inline float get_axisActionValue_23() const { return ___axisActionValue_23; }
	inline float* get_address_of_axisActionValue_23() { return &___axisActionValue_23; }
	inline void set_axisActionValue_23(float value)
	{
		___axisActionValue_23 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_24() { return static_cast<int32_t>(offsetof(QuickSwipe_t3226549687, ___enableSimpleAction_24)); }
	inline bool get_enableSimpleAction_24() const { return ___enableSimpleAction_24; }
	inline bool* get_address_of_enableSimpleAction_24() { return &___enableSimpleAction_24; }
	inline void set_enableSimpleAction_24(bool value)
	{
		___enableSimpleAction_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKSWIPE_T3226549687_H
#ifndef QUICKPINCH_T1656003506_H
#define QUICKPINCH_T1656003506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch
struct  QuickPinch_t1656003506  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickPinch/OnPinchAction HedgehogTeam.EasyTouch.QuickPinch::onPinchAction
	OnPinchAction_t3145810715 * ___onPinchAction_19;
	// System.Boolean HedgehogTeam.EasyTouch.QuickPinch::isGestureOnMe
	bool ___isGestureOnMe_20;
	// HedgehogTeam.EasyTouch.QuickPinch/ActionTiggering HedgehogTeam.EasyTouch.QuickPinch::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.QuickPinch/ActionPinchDirection HedgehogTeam.EasyTouch.QuickPinch::pinchDirection
	int32_t ___pinchDirection_22;
	// System.Single HedgehogTeam.EasyTouch.QuickPinch::axisActionValue
	float ___axisActionValue_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickPinch::enableSimpleAction
	bool ___enableSimpleAction_24;

public:
	inline static int32_t get_offset_of_onPinchAction_19() { return static_cast<int32_t>(offsetof(QuickPinch_t1656003506, ___onPinchAction_19)); }
	inline OnPinchAction_t3145810715 * get_onPinchAction_19() const { return ___onPinchAction_19; }
	inline OnPinchAction_t3145810715 ** get_address_of_onPinchAction_19() { return &___onPinchAction_19; }
	inline void set_onPinchAction_19(OnPinchAction_t3145810715 * value)
	{
		___onPinchAction_19 = value;
		Il2CppCodeGenWriteBarrier((&___onPinchAction_19), value);
	}

	inline static int32_t get_offset_of_isGestureOnMe_20() { return static_cast<int32_t>(offsetof(QuickPinch_t1656003506, ___isGestureOnMe_20)); }
	inline bool get_isGestureOnMe_20() const { return ___isGestureOnMe_20; }
	inline bool* get_address_of_isGestureOnMe_20() { return &___isGestureOnMe_20; }
	inline void set_isGestureOnMe_20(bool value)
	{
		___isGestureOnMe_20 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickPinch_t1656003506, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_pinchDirection_22() { return static_cast<int32_t>(offsetof(QuickPinch_t1656003506, ___pinchDirection_22)); }
	inline int32_t get_pinchDirection_22() const { return ___pinchDirection_22; }
	inline int32_t* get_address_of_pinchDirection_22() { return &___pinchDirection_22; }
	inline void set_pinchDirection_22(int32_t value)
	{
		___pinchDirection_22 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_23() { return static_cast<int32_t>(offsetof(QuickPinch_t1656003506, ___axisActionValue_23)); }
	inline float get_axisActionValue_23() const { return ___axisActionValue_23; }
	inline float* get_address_of_axisActionValue_23() { return &___axisActionValue_23; }
	inline void set_axisActionValue_23(float value)
	{
		___axisActionValue_23 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_24() { return static_cast<int32_t>(offsetof(QuickPinch_t1656003506, ___enableSimpleAction_24)); }
	inline bool get_enableSimpleAction_24() const { return ___enableSimpleAction_24; }
	inline bool* get_address_of_enableSimpleAction_24() { return &___enableSimpleAction_24; }
	inline void set_enableSimpleAction_24(bool value)
	{
		___enableSimpleAction_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKPINCH_T1656003506_H
#ifndef QUICKLONGTAP_T332644307_H
#define QUICKLONGTAP_T332644307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap
struct  QuickLongTap_t332644307  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickLongTap/OnLongTap HedgehogTeam.EasyTouch.QuickLongTap::onLongTap
	OnLongTap_t243381522 * ___onLongTap_19;
	// HedgehogTeam.EasyTouch.QuickLongTap/ActionTriggering HedgehogTeam.EasyTouch.QuickLongTap::actionTriggering
	int32_t ___actionTriggering_20;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickLongTap::currentGesture
	Gesture_t3351707245 * ___currentGesture_21;

public:
	inline static int32_t get_offset_of_onLongTap_19() { return static_cast<int32_t>(offsetof(QuickLongTap_t332644307, ___onLongTap_19)); }
	inline OnLongTap_t243381522 * get_onLongTap_19() const { return ___onLongTap_19; }
	inline OnLongTap_t243381522 ** get_address_of_onLongTap_19() { return &___onLongTap_19; }
	inline void set_onLongTap_19(OnLongTap_t243381522 * value)
	{
		___onLongTap_19 = value;
		Il2CppCodeGenWriteBarrier((&___onLongTap_19), value);
	}

	inline static int32_t get_offset_of_actionTriggering_20() { return static_cast<int32_t>(offsetof(QuickLongTap_t332644307, ___actionTriggering_20)); }
	inline int32_t get_actionTriggering_20() const { return ___actionTriggering_20; }
	inline int32_t* get_address_of_actionTriggering_20() { return &___actionTriggering_20; }
	inline void set_actionTriggering_20(int32_t value)
	{
		___actionTriggering_20 = value;
	}

	inline static int32_t get_offset_of_currentGesture_21() { return static_cast<int32_t>(offsetof(QuickLongTap_t332644307, ___currentGesture_21)); }
	inline Gesture_t3351707245 * get_currentGesture_21() const { return ___currentGesture_21; }
	inline Gesture_t3351707245 ** get_address_of_currentGesture_21() { return &___currentGesture_21; }
	inline void set_currentGesture_21(Gesture_t3351707245 * value)
	{
		___currentGesture_21 = value;
		Il2CppCodeGenWriteBarrier((&___currentGesture_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKLONGTAP_T332644307_H
#ifndef QUICKENTEROVEREXIST_T931184099_H
#define QUICKENTEROVEREXIST_T931184099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist
struct  QuickEnterOverExist_t931184099  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchEnter HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchEnter
	OnTouchEnter_t1794713516 * ___onTouchEnter_19;
	// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchOver HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchOver
	OnTouchOver_t3317551264 * ___onTouchOver_20;
	// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchExit HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchExit
	OnTouchExit_t2180465286 * ___onTouchExit_21;
	// System.Boolean[] HedgehogTeam.EasyTouch.QuickEnterOverExist::fingerOver
	BooleanU5BU5D_t2897418192* ___fingerOver_22;

public:
	inline static int32_t get_offset_of_onTouchEnter_19() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t931184099, ___onTouchEnter_19)); }
	inline OnTouchEnter_t1794713516 * get_onTouchEnter_19() const { return ___onTouchEnter_19; }
	inline OnTouchEnter_t1794713516 ** get_address_of_onTouchEnter_19() { return &___onTouchEnter_19; }
	inline void set_onTouchEnter_19(OnTouchEnter_t1794713516 * value)
	{
		___onTouchEnter_19 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchEnter_19), value);
	}

	inline static int32_t get_offset_of_onTouchOver_20() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t931184099, ___onTouchOver_20)); }
	inline OnTouchOver_t3317551264 * get_onTouchOver_20() const { return ___onTouchOver_20; }
	inline OnTouchOver_t3317551264 ** get_address_of_onTouchOver_20() { return &___onTouchOver_20; }
	inline void set_onTouchOver_20(OnTouchOver_t3317551264 * value)
	{
		___onTouchOver_20 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchOver_20), value);
	}

	inline static int32_t get_offset_of_onTouchExit_21() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t931184099, ___onTouchExit_21)); }
	inline OnTouchExit_t2180465286 * get_onTouchExit_21() const { return ___onTouchExit_21; }
	inline OnTouchExit_t2180465286 ** get_address_of_onTouchExit_21() { return &___onTouchExit_21; }
	inline void set_onTouchExit_21(OnTouchExit_t2180465286 * value)
	{
		___onTouchExit_21 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchExit_21), value);
	}

	inline static int32_t get_offset_of_fingerOver_22() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t931184099, ___fingerOver_22)); }
	inline BooleanU5BU5D_t2897418192* get_fingerOver_22() const { return ___fingerOver_22; }
	inline BooleanU5BU5D_t2897418192** get_address_of_fingerOver_22() { return &___fingerOver_22; }
	inline void set_fingerOver_22(BooleanU5BU5D_t2897418192* value)
	{
		___fingerOver_22 = value;
		Il2CppCodeGenWriteBarrier((&___fingerOver_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKENTEROVEREXIST_T931184099_H
#ifndef QUICKDRAG_T787596845_H
#define QUICKDRAG_T787596845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag
struct  QuickDrag_t787596845  : public QuickBase_t718481069
{
public:
	// HedgehogTeam.EasyTouch.QuickDrag/OnDragStart HedgehogTeam.EasyTouch.QuickDrag::onDragStart
	OnDragStart_t3676143812 * ___onDragStart_19;
	// HedgehogTeam.EasyTouch.QuickDrag/OnDrag HedgehogTeam.EasyTouch.QuickDrag::onDrag
	OnDrag_t3336846729 * ___onDrag_20;
	// HedgehogTeam.EasyTouch.QuickDrag/OnDragEnd HedgehogTeam.EasyTouch.QuickDrag::onDragEnd
	OnDragEnd_t1175011983 * ___onDragEnd_21;
	// System.Boolean HedgehogTeam.EasyTouch.QuickDrag::isStopOncollisionEnter
	bool ___isStopOncollisionEnter_22;
	// UnityEngine.Vector3 HedgehogTeam.EasyTouch.QuickDrag::deltaPosition
	Vector3_t3722313464  ___deltaPosition_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickDrag::isOnDrag
	bool ___isOnDrag_24;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickDrag::lastGesture
	Gesture_t3351707245 * ___lastGesture_25;

public:
	inline static int32_t get_offset_of_onDragStart_19() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___onDragStart_19)); }
	inline OnDragStart_t3676143812 * get_onDragStart_19() const { return ___onDragStart_19; }
	inline OnDragStart_t3676143812 ** get_address_of_onDragStart_19() { return &___onDragStart_19; }
	inline void set_onDragStart_19(OnDragStart_t3676143812 * value)
	{
		___onDragStart_19 = value;
		Il2CppCodeGenWriteBarrier((&___onDragStart_19), value);
	}

	inline static int32_t get_offset_of_onDrag_20() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___onDrag_20)); }
	inline OnDrag_t3336846729 * get_onDrag_20() const { return ___onDrag_20; }
	inline OnDrag_t3336846729 ** get_address_of_onDrag_20() { return &___onDrag_20; }
	inline void set_onDrag_20(OnDrag_t3336846729 * value)
	{
		___onDrag_20 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_20), value);
	}

	inline static int32_t get_offset_of_onDragEnd_21() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___onDragEnd_21)); }
	inline OnDragEnd_t1175011983 * get_onDragEnd_21() const { return ___onDragEnd_21; }
	inline OnDragEnd_t1175011983 ** get_address_of_onDragEnd_21() { return &___onDragEnd_21; }
	inline void set_onDragEnd_21(OnDragEnd_t1175011983 * value)
	{
		___onDragEnd_21 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEnd_21), value);
	}

	inline static int32_t get_offset_of_isStopOncollisionEnter_22() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___isStopOncollisionEnter_22)); }
	inline bool get_isStopOncollisionEnter_22() const { return ___isStopOncollisionEnter_22; }
	inline bool* get_address_of_isStopOncollisionEnter_22() { return &___isStopOncollisionEnter_22; }
	inline void set_isStopOncollisionEnter_22(bool value)
	{
		___isStopOncollisionEnter_22 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_23() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___deltaPosition_23)); }
	inline Vector3_t3722313464  get_deltaPosition_23() const { return ___deltaPosition_23; }
	inline Vector3_t3722313464 * get_address_of_deltaPosition_23() { return &___deltaPosition_23; }
	inline void set_deltaPosition_23(Vector3_t3722313464  value)
	{
		___deltaPosition_23 = value;
	}

	inline static int32_t get_offset_of_isOnDrag_24() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___isOnDrag_24)); }
	inline bool get_isOnDrag_24() const { return ___isOnDrag_24; }
	inline bool* get_address_of_isOnDrag_24() { return &___isOnDrag_24; }
	inline void set_isOnDrag_24(bool value)
	{
		___isOnDrag_24 = value;
	}

	inline static int32_t get_offset_of_lastGesture_25() { return static_cast<int32_t>(offsetof(QuickDrag_t787596845, ___lastGesture_25)); }
	inline Gesture_t3351707245 * get_lastGesture_25() const { return ___lastGesture_25; }
	inline Gesture_t3351707245 ** get_address_of_lastGesture_25() { return &___lastGesture_25; }
	inline void set_lastGesture_25(Gesture_t3351707245 * value)
	{
		___lastGesture_25 = value;
		Il2CppCodeGenWriteBarrier((&___lastGesture_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKDRAG_T787596845_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef ACCOUNTMANAGER_T2668481700_H
#define ACCOUNTMANAGER_T2668481700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountManager
struct  AccountManager_t2668481700  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.UI.Toggle AccountManager::_toggleUseCheDan
	Toggle_t2735377061 * ____toggleUseCheDan_4;
	// UnityEngine.GameObject AccountManager::_uiLoggin
	GameObject_t1113636619 * ____uiLoggin_5;
	// System.Boolean AccountManager::m_bLogin
	bool ___m_bLogin_9;
	// UnityEngine.GameObject AccountManager::_panelSelectGameModePage
	GameObject_t1113636619 * ____panelSelectGameModePage_14;
	// UnityEngine.GameObject AccountManager::_panelPlayerInfo
	GameObject_t1113636619 * ____panelPlayerInfo_15;
	// UnityEngine.GameObject AccountManager::_panelPlayerName
	GameObject_t1113636619 * ____panelPlayerName_16;
	// UnityEngine.GameObject AccountManager::_panelSkill
	GameObject_t1113636619 * ____panelSkill_17;
	// UnityEngine.GameObject AccountManager::_panelMainLognIn
	GameObject_t1113636619 * ____panelMainLognIn_18;
	// UnityEngine.UI.Dropdown AccountManager::_dropdownRoomList
	Dropdown_t2274391225 * ____dropdownRoomList_19;
	// UnityEngine.UI.Dropdown AccountManager::_dropdownSkills
	Dropdown_t2274391225 * ____dropdownSkills_20;
	// UnityEngine.GameObject AccountManager::_panelNewRoom
	GameObject_t1113636619 * ____panelNewRoom_21;
	// UnityEngine.UI.InputField AccountManager::_inputNewRoomName
	InputField_t3762917431 * ____inputNewRoomName_22;
	// ComoList AccountManager::_lstRoomList
	ComoList_t2152284863 * ____lstRoomList_23;
	// ComoList AccountManager::_lstPlayerList
	ComoList_t2152284863 * ____lstPlayerList_24;
	// UnityEngine.GameObject AccountManager::_panelLogin
	GameObject_t1113636619 * ____panelLogin_25;
	// UnityEngine.UI.InputField AccountManager::_inputPlayerName
	InputField_t3762917431 * ____inputPlayerName_26;
	// UnityEngine.UI.InputField AccountManager::_inputNewNickName
	InputField_t3762917431 * ____inputNewNickName_27;
	// UnityEngine.UI.Image AccountManager::_imgAvatar
	Image_t2670269651 * ____imgAvatar_28;
	// UnityEngine.UI.Text[] AccountManager::_aryMoney
	TextU5BU5D_t422084607* ____aryMoney_29;
	// UnityEngine.UI.Toggle AccountManager::_toggleObserve
	Toggle_t2735377061 * ____toggleObserve_30;
	// UnityEngine.UI.Text AccountManager::_txtUserName
	Text_t1901882714 * ____txtUserName_31;
	// UnityEngine.UI.Text AccountManager::_txtPassword
	Text_t1901882714 * ____txtPassword_32;
	// MsgBox AccountManager::m_MsgBox
	MsgBox_t1665650521 * ___m_MsgBox_33;
	// System.Collections.Generic.List`1<System.String> AccountManager::m_lstRoomList
	List_1_t3319525431 * ___m_lstRoomList_34;
	// System.Xml.XmlDocument AccountManager::_XmlDoc
	XmlDocument_t2837193595 * ____XmlDoc_37;
	// System.Xml.XmlNode AccountManager::_root
	XmlNode_t3767805227 * ____root_38;
	// UnityEngine.GameObject AccountManager::_uiLogin
	GameObject_t1113636619 * ____uiLogin_41;
	// UnityEngine.GameObject AccountManager::_uiQueue
	GameObject_t1113636619 * ____uiQueue_42;
	// System.Int32 AccountManager::m_nMainPlayerRank
	int32_t ___m_nMainPlayerRank_47;

public:
	inline static int32_t get_offset_of__toggleUseCheDan_4() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____toggleUseCheDan_4)); }
	inline Toggle_t2735377061 * get__toggleUseCheDan_4() const { return ____toggleUseCheDan_4; }
	inline Toggle_t2735377061 ** get_address_of__toggleUseCheDan_4() { return &____toggleUseCheDan_4; }
	inline void set__toggleUseCheDan_4(Toggle_t2735377061 * value)
	{
		____toggleUseCheDan_4 = value;
		Il2CppCodeGenWriteBarrier((&____toggleUseCheDan_4), value);
	}

	inline static int32_t get_offset_of__uiLoggin_5() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____uiLoggin_5)); }
	inline GameObject_t1113636619 * get__uiLoggin_5() const { return ____uiLoggin_5; }
	inline GameObject_t1113636619 ** get_address_of__uiLoggin_5() { return &____uiLoggin_5; }
	inline void set__uiLoggin_5(GameObject_t1113636619 * value)
	{
		____uiLoggin_5 = value;
		Il2CppCodeGenWriteBarrier((&____uiLoggin_5), value);
	}

	inline static int32_t get_offset_of_m_bLogin_9() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ___m_bLogin_9)); }
	inline bool get_m_bLogin_9() const { return ___m_bLogin_9; }
	inline bool* get_address_of_m_bLogin_9() { return &___m_bLogin_9; }
	inline void set_m_bLogin_9(bool value)
	{
		___m_bLogin_9 = value;
	}

	inline static int32_t get_offset_of__panelSelectGameModePage_14() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelSelectGameModePage_14)); }
	inline GameObject_t1113636619 * get__panelSelectGameModePage_14() const { return ____panelSelectGameModePage_14; }
	inline GameObject_t1113636619 ** get_address_of__panelSelectGameModePage_14() { return &____panelSelectGameModePage_14; }
	inline void set__panelSelectGameModePage_14(GameObject_t1113636619 * value)
	{
		____panelSelectGameModePage_14 = value;
		Il2CppCodeGenWriteBarrier((&____panelSelectGameModePage_14), value);
	}

	inline static int32_t get_offset_of__panelPlayerInfo_15() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelPlayerInfo_15)); }
	inline GameObject_t1113636619 * get__panelPlayerInfo_15() const { return ____panelPlayerInfo_15; }
	inline GameObject_t1113636619 ** get_address_of__panelPlayerInfo_15() { return &____panelPlayerInfo_15; }
	inline void set__panelPlayerInfo_15(GameObject_t1113636619 * value)
	{
		____panelPlayerInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&____panelPlayerInfo_15), value);
	}

	inline static int32_t get_offset_of__panelPlayerName_16() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelPlayerName_16)); }
	inline GameObject_t1113636619 * get__panelPlayerName_16() const { return ____panelPlayerName_16; }
	inline GameObject_t1113636619 ** get_address_of__panelPlayerName_16() { return &____panelPlayerName_16; }
	inline void set__panelPlayerName_16(GameObject_t1113636619 * value)
	{
		____panelPlayerName_16 = value;
		Il2CppCodeGenWriteBarrier((&____panelPlayerName_16), value);
	}

	inline static int32_t get_offset_of__panelSkill_17() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelSkill_17)); }
	inline GameObject_t1113636619 * get__panelSkill_17() const { return ____panelSkill_17; }
	inline GameObject_t1113636619 ** get_address_of__panelSkill_17() { return &____panelSkill_17; }
	inline void set__panelSkill_17(GameObject_t1113636619 * value)
	{
		____panelSkill_17 = value;
		Il2CppCodeGenWriteBarrier((&____panelSkill_17), value);
	}

	inline static int32_t get_offset_of__panelMainLognIn_18() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelMainLognIn_18)); }
	inline GameObject_t1113636619 * get__panelMainLognIn_18() const { return ____panelMainLognIn_18; }
	inline GameObject_t1113636619 ** get_address_of__panelMainLognIn_18() { return &____panelMainLognIn_18; }
	inline void set__panelMainLognIn_18(GameObject_t1113636619 * value)
	{
		____panelMainLognIn_18 = value;
		Il2CppCodeGenWriteBarrier((&____panelMainLognIn_18), value);
	}

	inline static int32_t get_offset_of__dropdownRoomList_19() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____dropdownRoomList_19)); }
	inline Dropdown_t2274391225 * get__dropdownRoomList_19() const { return ____dropdownRoomList_19; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownRoomList_19() { return &____dropdownRoomList_19; }
	inline void set__dropdownRoomList_19(Dropdown_t2274391225 * value)
	{
		____dropdownRoomList_19 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownRoomList_19), value);
	}

	inline static int32_t get_offset_of__dropdownSkills_20() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____dropdownSkills_20)); }
	inline Dropdown_t2274391225 * get__dropdownSkills_20() const { return ____dropdownSkills_20; }
	inline Dropdown_t2274391225 ** get_address_of__dropdownSkills_20() { return &____dropdownSkills_20; }
	inline void set__dropdownSkills_20(Dropdown_t2274391225 * value)
	{
		____dropdownSkills_20 = value;
		Il2CppCodeGenWriteBarrier((&____dropdownSkills_20), value);
	}

	inline static int32_t get_offset_of__panelNewRoom_21() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelNewRoom_21)); }
	inline GameObject_t1113636619 * get__panelNewRoom_21() const { return ____panelNewRoom_21; }
	inline GameObject_t1113636619 ** get_address_of__panelNewRoom_21() { return &____panelNewRoom_21; }
	inline void set__panelNewRoom_21(GameObject_t1113636619 * value)
	{
		____panelNewRoom_21 = value;
		Il2CppCodeGenWriteBarrier((&____panelNewRoom_21), value);
	}

	inline static int32_t get_offset_of__inputNewRoomName_22() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____inputNewRoomName_22)); }
	inline InputField_t3762917431 * get__inputNewRoomName_22() const { return ____inputNewRoomName_22; }
	inline InputField_t3762917431 ** get_address_of__inputNewRoomName_22() { return &____inputNewRoomName_22; }
	inline void set__inputNewRoomName_22(InputField_t3762917431 * value)
	{
		____inputNewRoomName_22 = value;
		Il2CppCodeGenWriteBarrier((&____inputNewRoomName_22), value);
	}

	inline static int32_t get_offset_of__lstRoomList_23() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____lstRoomList_23)); }
	inline ComoList_t2152284863 * get__lstRoomList_23() const { return ____lstRoomList_23; }
	inline ComoList_t2152284863 ** get_address_of__lstRoomList_23() { return &____lstRoomList_23; }
	inline void set__lstRoomList_23(ComoList_t2152284863 * value)
	{
		____lstRoomList_23 = value;
		Il2CppCodeGenWriteBarrier((&____lstRoomList_23), value);
	}

	inline static int32_t get_offset_of__lstPlayerList_24() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____lstPlayerList_24)); }
	inline ComoList_t2152284863 * get__lstPlayerList_24() const { return ____lstPlayerList_24; }
	inline ComoList_t2152284863 ** get_address_of__lstPlayerList_24() { return &____lstPlayerList_24; }
	inline void set__lstPlayerList_24(ComoList_t2152284863 * value)
	{
		____lstPlayerList_24 = value;
		Il2CppCodeGenWriteBarrier((&____lstPlayerList_24), value);
	}

	inline static int32_t get_offset_of__panelLogin_25() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____panelLogin_25)); }
	inline GameObject_t1113636619 * get__panelLogin_25() const { return ____panelLogin_25; }
	inline GameObject_t1113636619 ** get_address_of__panelLogin_25() { return &____panelLogin_25; }
	inline void set__panelLogin_25(GameObject_t1113636619 * value)
	{
		____panelLogin_25 = value;
		Il2CppCodeGenWriteBarrier((&____panelLogin_25), value);
	}

	inline static int32_t get_offset_of__inputPlayerName_26() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____inputPlayerName_26)); }
	inline InputField_t3762917431 * get__inputPlayerName_26() const { return ____inputPlayerName_26; }
	inline InputField_t3762917431 ** get_address_of__inputPlayerName_26() { return &____inputPlayerName_26; }
	inline void set__inputPlayerName_26(InputField_t3762917431 * value)
	{
		____inputPlayerName_26 = value;
		Il2CppCodeGenWriteBarrier((&____inputPlayerName_26), value);
	}

	inline static int32_t get_offset_of__inputNewNickName_27() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____inputNewNickName_27)); }
	inline InputField_t3762917431 * get__inputNewNickName_27() const { return ____inputNewNickName_27; }
	inline InputField_t3762917431 ** get_address_of__inputNewNickName_27() { return &____inputNewNickName_27; }
	inline void set__inputNewNickName_27(InputField_t3762917431 * value)
	{
		____inputNewNickName_27 = value;
		Il2CppCodeGenWriteBarrier((&____inputNewNickName_27), value);
	}

	inline static int32_t get_offset_of__imgAvatar_28() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____imgAvatar_28)); }
	inline Image_t2670269651 * get__imgAvatar_28() const { return ____imgAvatar_28; }
	inline Image_t2670269651 ** get_address_of__imgAvatar_28() { return &____imgAvatar_28; }
	inline void set__imgAvatar_28(Image_t2670269651 * value)
	{
		____imgAvatar_28 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_28), value);
	}

	inline static int32_t get_offset_of__aryMoney_29() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____aryMoney_29)); }
	inline TextU5BU5D_t422084607* get__aryMoney_29() const { return ____aryMoney_29; }
	inline TextU5BU5D_t422084607** get_address_of__aryMoney_29() { return &____aryMoney_29; }
	inline void set__aryMoney_29(TextU5BU5D_t422084607* value)
	{
		____aryMoney_29 = value;
		Il2CppCodeGenWriteBarrier((&____aryMoney_29), value);
	}

	inline static int32_t get_offset_of__toggleObserve_30() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____toggleObserve_30)); }
	inline Toggle_t2735377061 * get__toggleObserve_30() const { return ____toggleObserve_30; }
	inline Toggle_t2735377061 ** get_address_of__toggleObserve_30() { return &____toggleObserve_30; }
	inline void set__toggleObserve_30(Toggle_t2735377061 * value)
	{
		____toggleObserve_30 = value;
		Il2CppCodeGenWriteBarrier((&____toggleObserve_30), value);
	}

	inline static int32_t get_offset_of__txtUserName_31() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____txtUserName_31)); }
	inline Text_t1901882714 * get__txtUserName_31() const { return ____txtUserName_31; }
	inline Text_t1901882714 ** get_address_of__txtUserName_31() { return &____txtUserName_31; }
	inline void set__txtUserName_31(Text_t1901882714 * value)
	{
		____txtUserName_31 = value;
		Il2CppCodeGenWriteBarrier((&____txtUserName_31), value);
	}

	inline static int32_t get_offset_of__txtPassword_32() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____txtPassword_32)); }
	inline Text_t1901882714 * get__txtPassword_32() const { return ____txtPassword_32; }
	inline Text_t1901882714 ** get_address_of__txtPassword_32() { return &____txtPassword_32; }
	inline void set__txtPassword_32(Text_t1901882714 * value)
	{
		____txtPassword_32 = value;
		Il2CppCodeGenWriteBarrier((&____txtPassword_32), value);
	}

	inline static int32_t get_offset_of_m_MsgBox_33() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ___m_MsgBox_33)); }
	inline MsgBox_t1665650521 * get_m_MsgBox_33() const { return ___m_MsgBox_33; }
	inline MsgBox_t1665650521 ** get_address_of_m_MsgBox_33() { return &___m_MsgBox_33; }
	inline void set_m_MsgBox_33(MsgBox_t1665650521 * value)
	{
		___m_MsgBox_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBox_33), value);
	}

	inline static int32_t get_offset_of_m_lstRoomList_34() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ___m_lstRoomList_34)); }
	inline List_1_t3319525431 * get_m_lstRoomList_34() const { return ___m_lstRoomList_34; }
	inline List_1_t3319525431 ** get_address_of_m_lstRoomList_34() { return &___m_lstRoomList_34; }
	inline void set_m_lstRoomList_34(List_1_t3319525431 * value)
	{
		___m_lstRoomList_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRoomList_34), value);
	}

	inline static int32_t get_offset_of__XmlDoc_37() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____XmlDoc_37)); }
	inline XmlDocument_t2837193595 * get__XmlDoc_37() const { return ____XmlDoc_37; }
	inline XmlDocument_t2837193595 ** get_address_of__XmlDoc_37() { return &____XmlDoc_37; }
	inline void set__XmlDoc_37(XmlDocument_t2837193595 * value)
	{
		____XmlDoc_37 = value;
		Il2CppCodeGenWriteBarrier((&____XmlDoc_37), value);
	}

	inline static int32_t get_offset_of__root_38() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____root_38)); }
	inline XmlNode_t3767805227 * get__root_38() const { return ____root_38; }
	inline XmlNode_t3767805227 ** get_address_of__root_38() { return &____root_38; }
	inline void set__root_38(XmlNode_t3767805227 * value)
	{
		____root_38 = value;
		Il2CppCodeGenWriteBarrier((&____root_38), value);
	}

	inline static int32_t get_offset_of__uiLogin_41() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____uiLogin_41)); }
	inline GameObject_t1113636619 * get__uiLogin_41() const { return ____uiLogin_41; }
	inline GameObject_t1113636619 ** get_address_of__uiLogin_41() { return &____uiLogin_41; }
	inline void set__uiLogin_41(GameObject_t1113636619 * value)
	{
		____uiLogin_41 = value;
		Il2CppCodeGenWriteBarrier((&____uiLogin_41), value);
	}

	inline static int32_t get_offset_of__uiQueue_42() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ____uiQueue_42)); }
	inline GameObject_t1113636619 * get__uiQueue_42() const { return ____uiQueue_42; }
	inline GameObject_t1113636619 ** get_address_of__uiQueue_42() { return &____uiQueue_42; }
	inline void set__uiQueue_42(GameObject_t1113636619 * value)
	{
		____uiQueue_42 = value;
		Il2CppCodeGenWriteBarrier((&____uiQueue_42), value);
	}

	inline static int32_t get_offset_of_m_nMainPlayerRank_47() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700, ___m_nMainPlayerRank_47)); }
	inline int32_t get_m_nMainPlayerRank_47() const { return ___m_nMainPlayerRank_47; }
	inline int32_t* get_address_of_m_nMainPlayerRank_47() { return &___m_nMainPlayerRank_47; }
	inline void set_m_nMainPlayerRank_47(int32_t value)
	{
		___m_nMainPlayerRank_47 = value;
	}
};

struct AccountManager_t2668481700_StaticFields
{
public:
	// System.Boolean AccountManager::s_UseCheDan
	bool ___s_UseCheDan_3;
	// AccountManager AccountManager::s_Instance
	AccountManager_t2668481700 * ___s_Instance_6;
	// System.Int32 AccountManager::s_nCurSelectRoomIndex
	int32_t ___s_nCurSelectRoomIndex_7;
	// System.String AccountManager::m_szCurAccount
	String_t* ___m_szCurAccount_8;
	// System.String AccountManager::c_szYingShe
	String_t* ___c_szYingShe_12;
	// System.String AccountManager::url
	String_t* ___url_13;
	// System.Int32 AccountManager::m_nCurSelectedIndex
	int32_t ___m_nCurSelectedIndex_35;
	// System.String AccountManager::m_szCurSelectedRoomName
	String_t* ___m_szCurSelectedRoomName_36;
	// AccountManager/eSceneMode AccountManager::m_eSceneMode
	int32_t ___m_eSceneMode_39;
	// System.Boolean AccountManager::m_bInGame
	bool ___m_bInGame_40;
	// System.Int32[] AccountManager::m_aryMoney
	Int32U5BU5D_t385246372* ___m_aryMoney_43;
	// System.Boolean AccountManager::s_bMoneyInited
	bool ___s_bMoneyInited_44;
	// System.String AccountManager::_gameVersion
	String_t* ____gameVersion_45;
	// System.Int32 AccountManager::s_nSelectedSkillId
	int32_t ___s_nSelectedSkillId_46;
	// System.String AccountManager::s_szPlayerName
	String_t* ___s_szPlayerName_48;
	// System.Boolean AccountManager::s_bObserve
	bool ___s_bObserve_49;
	// System.String AccountManager::s_szCurEnteredPlayerName
	String_t* ___s_szCurEnteredPlayerName_50;

public:
	inline static int32_t get_offset_of_s_UseCheDan_3() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_UseCheDan_3)); }
	inline bool get_s_UseCheDan_3() const { return ___s_UseCheDan_3; }
	inline bool* get_address_of_s_UseCheDan_3() { return &___s_UseCheDan_3; }
	inline void set_s_UseCheDan_3(bool value)
	{
		___s_UseCheDan_3 = value;
	}

	inline static int32_t get_offset_of_s_Instance_6() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_Instance_6)); }
	inline AccountManager_t2668481700 * get_s_Instance_6() const { return ___s_Instance_6; }
	inline AccountManager_t2668481700 ** get_address_of_s_Instance_6() { return &___s_Instance_6; }
	inline void set_s_Instance_6(AccountManager_t2668481700 * value)
	{
		___s_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_6), value);
	}

	inline static int32_t get_offset_of_s_nCurSelectRoomIndex_7() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_nCurSelectRoomIndex_7)); }
	inline int32_t get_s_nCurSelectRoomIndex_7() const { return ___s_nCurSelectRoomIndex_7; }
	inline int32_t* get_address_of_s_nCurSelectRoomIndex_7() { return &___s_nCurSelectRoomIndex_7; }
	inline void set_s_nCurSelectRoomIndex_7(int32_t value)
	{
		___s_nCurSelectRoomIndex_7 = value;
	}

	inline static int32_t get_offset_of_m_szCurAccount_8() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___m_szCurAccount_8)); }
	inline String_t* get_m_szCurAccount_8() const { return ___m_szCurAccount_8; }
	inline String_t** get_address_of_m_szCurAccount_8() { return &___m_szCurAccount_8; }
	inline void set_m_szCurAccount_8(String_t* value)
	{
		___m_szCurAccount_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCurAccount_8), value);
	}

	inline static int32_t get_offset_of_c_szYingShe_12() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___c_szYingShe_12)); }
	inline String_t* get_c_szYingShe_12() const { return ___c_szYingShe_12; }
	inline String_t** get_address_of_c_szYingShe_12() { return &___c_szYingShe_12; }
	inline void set_c_szYingShe_12(String_t* value)
	{
		___c_szYingShe_12 = value;
		Il2CppCodeGenWriteBarrier((&___c_szYingShe_12), value);
	}

	inline static int32_t get_offset_of_url_13() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___url_13)); }
	inline String_t* get_url_13() const { return ___url_13; }
	inline String_t** get_address_of_url_13() { return &___url_13; }
	inline void set_url_13(String_t* value)
	{
		___url_13 = value;
		Il2CppCodeGenWriteBarrier((&___url_13), value);
	}

	inline static int32_t get_offset_of_m_nCurSelectedIndex_35() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___m_nCurSelectedIndex_35)); }
	inline int32_t get_m_nCurSelectedIndex_35() const { return ___m_nCurSelectedIndex_35; }
	inline int32_t* get_address_of_m_nCurSelectedIndex_35() { return &___m_nCurSelectedIndex_35; }
	inline void set_m_nCurSelectedIndex_35(int32_t value)
	{
		___m_nCurSelectedIndex_35 = value;
	}

	inline static int32_t get_offset_of_m_szCurSelectedRoomName_36() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___m_szCurSelectedRoomName_36)); }
	inline String_t* get_m_szCurSelectedRoomName_36() const { return ___m_szCurSelectedRoomName_36; }
	inline String_t** get_address_of_m_szCurSelectedRoomName_36() { return &___m_szCurSelectedRoomName_36; }
	inline void set_m_szCurSelectedRoomName_36(String_t* value)
	{
		___m_szCurSelectedRoomName_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_szCurSelectedRoomName_36), value);
	}

	inline static int32_t get_offset_of_m_eSceneMode_39() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___m_eSceneMode_39)); }
	inline int32_t get_m_eSceneMode_39() const { return ___m_eSceneMode_39; }
	inline int32_t* get_address_of_m_eSceneMode_39() { return &___m_eSceneMode_39; }
	inline void set_m_eSceneMode_39(int32_t value)
	{
		___m_eSceneMode_39 = value;
	}

	inline static int32_t get_offset_of_m_bInGame_40() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___m_bInGame_40)); }
	inline bool get_m_bInGame_40() const { return ___m_bInGame_40; }
	inline bool* get_address_of_m_bInGame_40() { return &___m_bInGame_40; }
	inline void set_m_bInGame_40(bool value)
	{
		___m_bInGame_40 = value;
	}

	inline static int32_t get_offset_of_m_aryMoney_43() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___m_aryMoney_43)); }
	inline Int32U5BU5D_t385246372* get_m_aryMoney_43() const { return ___m_aryMoney_43; }
	inline Int32U5BU5D_t385246372** get_address_of_m_aryMoney_43() { return &___m_aryMoney_43; }
	inline void set_m_aryMoney_43(Int32U5BU5D_t385246372* value)
	{
		___m_aryMoney_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoney_43), value);
	}

	inline static int32_t get_offset_of_s_bMoneyInited_44() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_bMoneyInited_44)); }
	inline bool get_s_bMoneyInited_44() const { return ___s_bMoneyInited_44; }
	inline bool* get_address_of_s_bMoneyInited_44() { return &___s_bMoneyInited_44; }
	inline void set_s_bMoneyInited_44(bool value)
	{
		___s_bMoneyInited_44 = value;
	}

	inline static int32_t get_offset_of__gameVersion_45() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ____gameVersion_45)); }
	inline String_t* get__gameVersion_45() const { return ____gameVersion_45; }
	inline String_t** get_address_of__gameVersion_45() { return &____gameVersion_45; }
	inline void set__gameVersion_45(String_t* value)
	{
		____gameVersion_45 = value;
		Il2CppCodeGenWriteBarrier((&____gameVersion_45), value);
	}

	inline static int32_t get_offset_of_s_nSelectedSkillId_46() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_nSelectedSkillId_46)); }
	inline int32_t get_s_nSelectedSkillId_46() const { return ___s_nSelectedSkillId_46; }
	inline int32_t* get_address_of_s_nSelectedSkillId_46() { return &___s_nSelectedSkillId_46; }
	inline void set_s_nSelectedSkillId_46(int32_t value)
	{
		___s_nSelectedSkillId_46 = value;
	}

	inline static int32_t get_offset_of_s_szPlayerName_48() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_szPlayerName_48)); }
	inline String_t* get_s_szPlayerName_48() const { return ___s_szPlayerName_48; }
	inline String_t** get_address_of_s_szPlayerName_48() { return &___s_szPlayerName_48; }
	inline void set_s_szPlayerName_48(String_t* value)
	{
		___s_szPlayerName_48 = value;
		Il2CppCodeGenWriteBarrier((&___s_szPlayerName_48), value);
	}

	inline static int32_t get_offset_of_s_bObserve_49() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_bObserve_49)); }
	inline bool get_s_bObserve_49() const { return ___s_bObserve_49; }
	inline bool* get_address_of_s_bObserve_49() { return &___s_bObserve_49; }
	inline void set_s_bObserve_49(bool value)
	{
		___s_bObserve_49 = value;
	}

	inline static int32_t get_offset_of_s_szCurEnteredPlayerName_50() { return static_cast<int32_t>(offsetof(AccountManager_t2668481700_StaticFields, ___s_szCurEnteredPlayerName_50)); }
	inline String_t* get_s_szCurEnteredPlayerName_50() const { return ___s_szCurEnteredPlayerName_50; }
	inline String_t** get_address_of_s_szCurEnteredPlayerName_50() { return &___s_szCurEnteredPlayerName_50; }
	inline void set_s_szCurEnteredPlayerName_50(String_t* value)
	{
		___s_szCurEnteredPlayerName_50 = value;
		Il2CppCodeGenWriteBarrier((&___s_szCurEnteredPlayerName_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNTMANAGER_T2668481700_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t729725570  : public LayoutGroup_t2436138090
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef COMOSCENEMANAGER_T977619994_H
#define COMOSCENEMANAGER_T977619994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ComoSceneManager
struct  ComoSceneManager_t977619994  : public PunBehaviour_t987309092
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMOSCENEMANAGER_T977619994_H
#ifndef GRIDLAYOUTGROUP_T3046220461_H
#define GRIDLAYOUTGROUP_T3046220461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t3046220461  : public LayoutGroup_t2436138090
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_10;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_11;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2156229523  ___m_CellSize_12;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2156229523  ___m_Spacing_13;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_14;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_15;

public:
	inline static int32_t get_offset_of_m_StartCorner_10() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartCorner_10)); }
	inline int32_t get_m_StartCorner_10() const { return ___m_StartCorner_10; }
	inline int32_t* get_address_of_m_StartCorner_10() { return &___m_StartCorner_10; }
	inline void set_m_StartCorner_10(int32_t value)
	{
		___m_StartCorner_10 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_11() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartAxis_11)); }
	inline int32_t get_m_StartAxis_11() const { return ___m_StartAxis_11; }
	inline int32_t* get_address_of_m_StartAxis_11() { return &___m_StartAxis_11; }
	inline void set_m_StartAxis_11(int32_t value)
	{
		___m_StartAxis_11 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_CellSize_12)); }
	inline Vector2_t2156229523  get_m_CellSize_12() const { return ___m_CellSize_12; }
	inline Vector2_t2156229523 * get_address_of_m_CellSize_12() { return &___m_CellSize_12; }
	inline void set_m_CellSize_12(Vector2_t2156229523  value)
	{
		___m_CellSize_12 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Spacing_13)); }
	inline Vector2_t2156229523  get_m_Spacing_13() const { return ___m_Spacing_13; }
	inline Vector2_t2156229523 * get_address_of_m_Spacing_13() { return &___m_Spacing_13; }
	inline void set_m_Spacing_13(Vector2_t2156229523  value)
	{
		___m_Spacing_13 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Constraint_14)); }
	inline int32_t get_m_Constraint_14() const { return ___m_Constraint_14; }
	inline int32_t* get_address_of_m_Constraint_14() { return &___m_Constraint_14; }
	inline void set_m_Constraint_14(int32_t value)
	{
		___m_Constraint_14 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_ConstraintCount_15)); }
	inline int32_t get_m_ConstraintCount_15() const { return ___m_ConstraintCount_15; }
	inline int32_t* get_address_of_m_ConstraintCount_15() { return &___m_ConstraintCount_15; }
	inline void set_m_ConstraintCount_15(int32_t value)
	{
		___m_ConstraintCount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T3046220461_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H
#ifndef HORIZONTALLAYOUTGROUP_T2586782146_H
#define HORIZONTALLAYOUTGROUP_T2586782146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2586782146  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2586782146_H
#ifndef VERTICALLAYOUTGROUP_T923838031_H
#define VERTICALLAYOUTGROUP_T923838031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t923838031  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T923838031_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (ContentSizeFitter_t3850442145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	ContentSizeFitter_t3850442145::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t3850442145::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (FitMode_t3267881214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	FitMode_t3267881214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (GridLayoutGroup_t3046220461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[6] = 
{
	GridLayoutGroup_t3046220461::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t3046220461::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t3046220461::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t3046220461::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (Corner_t1493259673)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2703[5] = 
{
	Corner_t1493259673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (Axis_t3613393006)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2704[3] = 
{
	Axis_t3613393006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (Constraint_t814224393)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2705[4] = 
{
	Constraint_t814224393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (HorizontalLayoutGroup_t2586782146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (HorizontalOrVerticalLayoutGroup_t729725570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[5] = 
{
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (LayoutElement_t1785403678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[8] = 
{
	LayoutElement_t1785403678::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1785403678::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1785403678::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleHeight_8(),
	LayoutElement_t1785403678::get_offset_of_m_LayoutPriority_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_2(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_4(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_5(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2716[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2717[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2723[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2730[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (AccountManager_t2668481700), -1, sizeof(AccountManager_t2668481700_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2741[48] = 
{
	AccountManager_t2668481700_StaticFields::get_offset_of_s_UseCheDan_3(),
	AccountManager_t2668481700::get_offset_of__toggleUseCheDan_4(),
	AccountManager_t2668481700::get_offset_of__uiLoggin_5(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_Instance_6(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_nCurSelectRoomIndex_7(),
	AccountManager_t2668481700_StaticFields::get_offset_of_m_szCurAccount_8(),
	AccountManager_t2668481700::get_offset_of_m_bLogin_9(),
	0,
	0,
	AccountManager_t2668481700_StaticFields::get_offset_of_c_szYingShe_12(),
	AccountManager_t2668481700_StaticFields::get_offset_of_url_13(),
	AccountManager_t2668481700::get_offset_of__panelSelectGameModePage_14(),
	AccountManager_t2668481700::get_offset_of__panelPlayerInfo_15(),
	AccountManager_t2668481700::get_offset_of__panelPlayerName_16(),
	AccountManager_t2668481700::get_offset_of__panelSkill_17(),
	AccountManager_t2668481700::get_offset_of__panelMainLognIn_18(),
	AccountManager_t2668481700::get_offset_of__dropdownRoomList_19(),
	AccountManager_t2668481700::get_offset_of__dropdownSkills_20(),
	AccountManager_t2668481700::get_offset_of__panelNewRoom_21(),
	AccountManager_t2668481700::get_offset_of__inputNewRoomName_22(),
	AccountManager_t2668481700::get_offset_of__lstRoomList_23(),
	AccountManager_t2668481700::get_offset_of__lstPlayerList_24(),
	AccountManager_t2668481700::get_offset_of__panelLogin_25(),
	AccountManager_t2668481700::get_offset_of__inputPlayerName_26(),
	AccountManager_t2668481700::get_offset_of__inputNewNickName_27(),
	AccountManager_t2668481700::get_offset_of__imgAvatar_28(),
	AccountManager_t2668481700::get_offset_of__aryMoney_29(),
	AccountManager_t2668481700::get_offset_of__toggleObserve_30(),
	AccountManager_t2668481700::get_offset_of__txtUserName_31(),
	AccountManager_t2668481700::get_offset_of__txtPassword_32(),
	AccountManager_t2668481700::get_offset_of_m_MsgBox_33(),
	AccountManager_t2668481700::get_offset_of_m_lstRoomList_34(),
	AccountManager_t2668481700_StaticFields::get_offset_of_m_nCurSelectedIndex_35(),
	AccountManager_t2668481700_StaticFields::get_offset_of_m_szCurSelectedRoomName_36(),
	AccountManager_t2668481700::get_offset_of__XmlDoc_37(),
	AccountManager_t2668481700::get_offset_of__root_38(),
	AccountManager_t2668481700_StaticFields::get_offset_of_m_eSceneMode_39(),
	AccountManager_t2668481700_StaticFields::get_offset_of_m_bInGame_40(),
	AccountManager_t2668481700::get_offset_of__uiLogin_41(),
	AccountManager_t2668481700::get_offset_of__uiQueue_42(),
	AccountManager_t2668481700_StaticFields::get_offset_of_m_aryMoney_43(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_bMoneyInited_44(),
	AccountManager_t2668481700_StaticFields::get_offset_of__gameVersion_45(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_nSelectedSkillId_46(),
	AccountManager_t2668481700::get_offset_of_m_nMainPlayerRank_47(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_szPlayerName_48(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_bObserve_49(),
	AccountManager_t2668481700_StaticFields::get_offset_of_s_szCurEnteredPlayerName_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (eSceneMode_t3985604454)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[4] = 
{
	eSceneMode_t3985604454::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (U3CStartU3Ec__Iterator0_t1483120107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[7] = 
{
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U3CszFileNameU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U3CwwwU3E__0_1(),
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U3ClstSkillsU3E__0_2(),
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t1483120107::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[7] = 
{
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_U3CwwwFormU3E__0_0(),
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_szRoomName_1(),
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_szContent_2(),
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_U3CwwwU3E__0_3(),
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_U24current_4(),
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_U24disposing_5(),
	U3CDoSaveMapXmlFileU3Ec__Iterator1_t2618994493::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U3CUploadFileU3Ec__Iterator2_t2703782479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[6] = 
{
	U3CUploadFileU3Ec__Iterator2_t2703782479::get_offset_of_U3CwwwFormU3E__0_0(),
	U3CUploadFileU3Ec__Iterator2_t2703782479::get_offset_of_szContent_1(),
	U3CUploadFileU3Ec__Iterator2_t2703782479::get_offset_of_U3CwwwU3E__0_2(),
	U3CUploadFileU3Ec__Iterator2_t2703782479::get_offset_of_U24current_3(),
	U3CUploadFileU3Ec__Iterator2_t2703782479::get_offset_of_U24disposing_4(),
	U3CUploadFileU3Ec__Iterator2_t2703782479::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (Ball_t2206666566), -1, sizeof(Ball_t2206666566_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2746[247] = 
{
	Ball_t2206666566::get_offset_of__goUnfoldRing_2(),
	Ball_t2206666566::get_offset_of__sprUnfoldSpr_3(),
	Ball_t2206666566::get_offset_of__arrowDir_4(),
	Ball_t2206666566::get_offset_of__gunsight_5(),
	Ball_t2206666566::get_offset_of__sortingGroup_6(),
	Ball_t2206666566::get_offset_of__sortingGroup_mouth_7(),
	0,
	Ball_t2206666566::get_offset_of_m_msMain_9(),
	Ball_t2206666566::get_offset_of_m_msStaticShell_10(),
	Ball_t2206666566::get_offset_of_m_msDynamicShell_11(),
	Ball_t2206666566::get_offset_of__skillModule_12(),
	Ball_t2206666566::get_offset_of__srDustBling_13(),
	Ball_t2206666566::get_offset_of__effectPlayerLevelUp_14(),
	Ball_t2206666566::get_offset_of__effectSpitSmoke_15(),
	Ball_t2206666566::get_offset_of__goEffectContainer_KuangBao_16(),
	Ball_t2206666566::get_offset_of__effectKuangBao_QianYao_17(),
	Ball_t2206666566::get_offset_of__effectKuangBao_Up_18(),
	Ball_t2206666566::get_offset_of__effectKuangBao_Down_19(),
	Ball_t2206666566::get_offset_of_m_aryEffectQianZou_20(),
	Ball_t2206666566::get_offset_of_m_aryEffectChiXu_21(),
	Ball_t2206666566::get_offset_of_m_aryEffectChiXu1_22(),
	Ball_t2206666566::get_offset_of__skeaniEffectBianCi_23(),
	Ball_t2206666566::get_offset_of__effectKill_24(),
	Ball_t2206666566::get_offset_of__effectMergeAll_QianYao_25(),
	Ball_t2206666566::get_offset_of__goEffectContainer_QianYao_26(),
	Ball_t2206666566::get_offset_of__effectUnfoldQianYao_27(),
	Ball_t2206666566::get_offset_of__container_28(),
	Ball_t2206666566::get_offset_of__scriptBallTrigger_29(),
	Ball_t2206666566::get_offset_of__skeletonAnimatnion_30(),
	Ball_t2206666566::get_offset_of__mrSkeleton_31(),
	Ball_t2206666566::get_offset_of__mrShellCircle_32(),
	Ball_t2206666566::get_offset_of__shell_33(),
	Ball_t2206666566::get_offset_of__thorn_34(),
	Ball_t2206666566::get_offset_of__Trigger_35(),
	Ball_t2206666566::get_offset_of__TriggerMergeSelf_36(),
	Ball_t2206666566::get_offset_of__TriggerForBean_37(),
	Ball_t2206666566::get_offset_of__TriggerForSpore_38(),
	Ball_t2206666566::get_offset_of__Collider_39(),
	Ball_t2206666566::get_offset_of__ColliderTemp_40(),
	Ball_t2206666566::get_offset_of__ColliderDust_41(),
	Ball_t2206666566::get_offset_of__Mouth_42(),
	Ball_t2206666566::get_offset_of__srMouth_43(),
	Ball_t2206666566::get_offset_of__srOutterRing_44(),
	Ball_t2206666566::get_offset_of__main_45(),
	Ball_t2206666566::get_offset_of__rigid_46(),
	Ball_t2206666566::get_offset_of_m_bIsEjecting_47(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempSize_48(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempPos_49(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempPos2_50(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempScale_51(),
	Ball_t2206666566_StaticFields::get_offset_of_vec2Temp_52(),
	Ball_t2206666566_StaticFields::get_offset_of_vec2Temp1_53(),
	Ball_t2206666566_StaticFields::get_offset_of_cTempColor_54(),
	Ball_t2206666566_StaticFields::get_offset_of_colorTemp_55(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempDir_56(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempDir2_57(),
	Ball_t2206666566::get_offset_of_m_vDir_58(),
	Ball_t2206666566::get_offset_of_m_vSpeed_59(),
	Ball_t2206666566::get_offset_of_m_vInitSpeed_60(),
	Ball_t2206666566::get_offset_of_m_vAcclerate_61(),
	Ball_t2206666566::get_offset_of__goPlayerName_62(),
	Ball_t2206666566::get_offset_of__txtPlayerName_63(),
	Ball_t2206666566::get_offset_of__txtPlayerNameMiaoBian_64(),
	Ball_t2206666566_StaticFields::get_offset_of_s_vecShellInitScale_65(),
	Ball_t2206666566::get_offset_of__effectPreUnfold_66(),
	Ball_t2206666566::get_offset_of__diry_67(),
	Ball_t2206666566::get_offset_of__obsceneCircle_68(),
	Ball_t2206666566::get_offset_of_m_fSpeed_69(),
	Ball_t2206666566::get_offset_of__Player_70(),
	Ball_t2206666566::get_offset_of__PlayerIns_71(),
	Ball_t2206666566::get_offset_of__player_72(),
	Ball_t2206666566::get_offset_of_m_eBallType_73(),
	Ball_t2206666566::get_offset_of__ba_74(),
	Ball_t2206666566::get_offset_of_m_bDead_75(),
	Ball_t2206666566::get_offset_of_m_sr_76(),
	Ball_t2206666566::get_offset_of_m_bMatInit_77(),
	Ball_t2206666566::get_offset_of_m_lstIgnoredCollisionBalls_78(),
	Ball_t2206666566::get_offset_of_m_bSetNewPos_79(),
	Ball_t2206666566::get_offset_of_m_aryEffectStatus_80(),
	Ball_t2206666566::get_offset_of_m_byChiXuEffectIndex_81(),
	Ball_t2206666566::get_offset_of__shitRender_82(),
	Ball_t2206666566::get_offset_of_m_bBallSkeletonMaterialSet_83(),
	Ball_t2206666566::get_offset_of_m_byPlayerLevel_84(),
	Ball_t2206666566::get_offset_of_m_bNeedSyncSize_85(),
	Ball_t2206666566::get_offset_of_m_fLastSyncSizeTime_86(),
	Ball_t2206666566::get_offset_of_m_fDestSize_87(),
	Ball_t2206666566::get_offset_of_m_fSizeUpdateSpeed_88(),
	Ball_t2206666566_StaticFields::get_offset_of_s_nShitSortOrderIndex_89(),
	Ball_t2206666566_StaticFields::get_offset_of_s_aryShitSortOrder_90(),
	Ball_t2206666566::get_offset_of_m_bSizeChanged_91(),
	Ball_t2206666566::get_offset_of_m_bSizeChanged_UnfoldSkill_92(),
	Ball_t2206666566::get_offset_of_m_fSizeKaiGenHao_93(),
	Ball_t2206666566::get_offset_of_m_fVolume_94(),
	Ball_t2206666566::get_offset_of_m_fRadius_95(),
	Ball_t2206666566::get_offset_of_m_fSize_96(),
	Ball_t2206666566::get_offset_of_m_fVisionSize_97(),
	Ball_t2206666566::get_offset_of_m_bySizeChangeOp_98(),
	Ball_t2206666566::get_offset_of_m_fVisionSize_UnfoldSkill_99(),
	Ball_t2206666566::get_offset_of_m_fSize_UnfoldSkill_100(),
	Ball_t2206666566::get_offset_of_m_vecEjectSpeed_101(),
	Ball_t2206666566::get_offset_of_m_vecEjectStartPos_102(),
	Ball_t2206666566::get_offset_of_m_fEjectTotalDis_103(),
	Ball_t2206666566::get_offset_of_m_bUseColliderTemp_104(),
	Ball_t2206666566::get_offset_of_m_fStayTimeWhenEjectEnd_105(),
	Ball_t2206666566::get_offset_of_m_bIgnoreDust_106(),
	Ball_t2206666566::get_offset_of_m_bAccelerating_107(),
	Ball_t2206666566::get_offset_of_m_vecA_108(),
	Ball_t2206666566::get_offset_of_m_vecAccelerateSpeed_109(),
	Ball_t2206666566::get_offset_of_m_vecV0_110(),
	Ball_t2206666566::get_offset_of_m_vecMotherMoveDir_111(),
	Ball_t2206666566::get_offset_of_m_fMotherRealTimeSpeed_112(),
	Ball_t2206666566::get_offset_of_m_bAffectByMotherMove_113(),
	Ball_t2206666566::get_offset_of_m_fCompensateDistance_114(),
	Ball_t2206666566::get_offset_of_m_fBaseDistance_115(),
	Ball_t2206666566::get_offset_of_m_fTotalDistance_116(),
	Ball_t2206666566::get_offset_of_m_bFinishedX_117(),
	Ball_t2206666566::get_offset_of_m_bFinishedY_118(),
	Ball_t2206666566::get_offset_of_m_eEjectingStatus_119(),
	Ball_t2206666566::get_offset_of_m_fTempTimeCount_120(),
	Ball_t2206666566::get_offset_of_m_fInitSpeed_121(),
	Ball_t2206666566::get_offset_of_m_fAccelerate_122(),
	Ball_t2206666566::get_offset_of_m_bStaying_123(),
	Ball_t2206666566::get_offset_of_m_nUnfoldStatus_124(),
	Ball_t2206666566::get_offset_of_m_bPreunfolding_125(),
	Ball_t2206666566::get_offset_of_m_fUnfoldLeftTime_126(),
	Ball_t2206666566::get_offset_of_m_fSizeBeforeUnfold_127(),
	Ball_t2206666566::get_offset_of_m_fMouthUnfoldScale_128(),
	Ball_t2206666566::get_offset_of_m_nMouthUnfoldStatus_129(),
	Ball_t2206666566::get_offset_of_m_lstPkMonsterList_130(),
	Ball_t2206666566::get_offset_of_m_lstPkListSelf_131(),
	Ball_t2206666566::get_offset_of_m_lstPkListOther_132(),
	Ball_t2206666566_StaticFields::get_offset_of_s_AddNum_133(),
	Ball_t2206666566_StaticFields::get_offset_of_s_RemoveNum_134(),
	Ball_t2206666566::get_offset_of_m_fPreDrawDeltaTime_135(),
	Ball_t2206666566::get_offset_of_m_fThornSize_136(),
	Ball_t2206666566_StaticFields::get_offset_of_s_aryBallExplodeDirection_137(),
	Ball_t2206666566::get_offset_of_m_fShitExplodeTime_138(),
	Ball_t2206666566::get_offset_of_m_fMotherLeft_139(),
	Ball_t2206666566::get_offset_of_m_fExplodeShellTime_140(),
	Ball_t2206666566::get_offset_of_m_nTotalChildNum_141(),
	Ball_t2206666566::get_offset_of_m_nCurChildNum_142(),
	0,
	Ball_t2206666566::get_offset_of_m_bExploding_144(),
	Ball_t2206666566::get_offset_of_m_ExplodeConfig_145(),
	Ball_t2206666566::get_offset_of_m_vecMotherPos_146(),
	Ball_t2206666566::get_offset_of_m_fRunDistance_147(),
	Ball_t2206666566::get_offset_of_m_fRealRunTime_148(),
	Ball_t2206666566::get_offset_of_m_lstDir_149(),
	Ball_t2206666566::get_offset_of_m_fChildSize_150(),
	Ball_t2206666566::get_offset_of_m_fSegDis_151(),
	Ball_t2206666566::get_offset_of_m_lstChildBallsIndex_152(),
	Ball_t2206666566::get_offset_of_m_nExplodeRound_153(),
	Ball_t2206666566::get_offset_of_m_lstEaten_154(),
	Ball_t2206666566::get_offset_of_m_Grass_155(),
	Ball_t2206666566::get_offset_of_m_fEnterGrassTime_156(),
	Ball_t2206666566::get_offset_of_m_lstGrass_157(),
	Ball_t2206666566::get_offset_of_m_szAccount_158(),
	Ball_t2206666566::get_offset_of_m_lstPreEatSpores_159(),
	Ball_t2206666566::get_offset_of_m_fMotherLeftSize_160(),
	Ball_t2206666566::get_offset_of_m_fChildThornSize_161(),
	Ball_t2206666566::get_offset_of_m_fMotherLeftThornSize_162(),
	Ball_t2206666566::get_offset_of_m_bShell_163(),
	Ball_t2206666566::get_offset_of_m_fShellShrinkCurTime_164(),
	Ball_t2206666566::get_offset_of_m_fShellTotalTime_165(),
	Ball_t2206666566::get_offset_of_m_eShellType_166(),
	Ball_t2206666566::get_offset_of_m_fBallTestValue_167(),
	Ball_t2206666566::get_offset_of_m_bDynamicShellVisible_168(),
	Ball_t2206666566::get_offset_of_m_fShellUnfoldScale_169(),
	Ball_t2206666566::get_offset_of_m_fTempShellCount_170(),
	Ball_t2206666566::get_offset_of_m_fEndAngle_171(),
	Ball_t2206666566::get_offset_of_m_fSpitBallInitSpeed_172(),
	Ball_t2206666566::get_offset_of_m_fSpitBallAccelerate_173(),
	Ball_t2206666566::get_offset_of_m_fExplodeInitSpeed_174(),
	Ball_t2206666566::get_offset_of_m_fExplodeInitAccelerate_175(),
	Ball_t2206666566::get_offset_of_m_SpitBallTarget_176(),
	Ball_t2206666566::get_offset_of_m_fSpitBallTargetParam_177(),
	Ball_t2206666566::get_offset_of_m_bTargeting_178(),
	Ball_t2206666566::get_offset_of_m_SplitBallTarget_179(),
	Ball_t2206666566::get_offset_of__relateTarget_180(),
	Ball_t2206666566::get_offset_of__relateSplitTarget_181(),
	Ball_t2206666566::get_offset_of_m_bDoNotMove_182(),
	Ball_t2206666566::get_offset_of_m_fAdjustSpeedX_183(),
	Ball_t2206666566::get_offset_of_m_fAdjustSpeedY_184(),
	Ball_t2206666566::get_offset_of_m_fExtraSpeedX_185(),
	Ball_t2206666566::get_offset_of_m_fExtraSpeedY_186(),
	0,
	Ball_t2206666566::get_offset_of_m_fMoveSpeedX_188(),
	Ball_t2206666566::get_offset_of_m_fMoveSpeedY_189(),
	Ball_t2206666566::get_offset_of_m_vecdirection_190(),
	Ball_t2206666566::get_offset_of_m_vecLastDirection_191(),
	Ball_t2206666566::get_offset_of_m_bFirstCalculateMoveInfo_192(),
	Ball_t2206666566::get_offset_of_m_vecMainSpeed_193(),
	0,
	Ball_t2206666566::get_offset_of_m_bInterpolateMoving_195(),
	Ball_t2206666566::get_offset_of_fChaZhiSpeedX_196(),
	Ball_t2206666566::get_offset_of_fChaZhiSpeedY_197(),
	Ball_t2206666566::get_offset_of_m_bbMovingToCenter_198(),
	Ball_t2206666566_StaticFields::get_offset_of_vecTempPianYi_199(),
	Ball_t2206666566::get_offset_of_m_vecBallsCenter_200(),
	Ball_t2206666566::get_offset_of_m_fTotalMag_201(),
	Ball_t2206666566::get_offset_of_m_nCountMag_202(),
	Ball_t2206666566::get_offset_of_m_vecCurChaZhiDest_203(),
	Ball_t2206666566::get_offset_of_m_vecCurMainPlayerSpeed_204(),
	Ball_t2206666566::get_offset_of_m_vecChaZhiSpeed_205(),
	Ball_t2206666566::get_offset_of_m_nChaZhiCount_206(),
	0,
	0,
	Ball_t2206666566::get_offset_of_m_nRealSplitNum_209(),
	Ball_t2206666566::get_offset_of_m_fSplitSize_210(),
	Ball_t2206666566::get_offset_of_m_fSplitArea_211(),
	Ball_t2206666566::get_offset_of_m_fLeftArea_212(),
	Ball_t2206666566::get_offset_of_m_fTotalDis_213(),
	Ball_t2206666566::get_offset_of_m_bMotherDeadAfterSplit_214(),
	Ball_t2206666566::get_offset_of_m_nIndex_215(),
	Ball_t2206666566::get_offset_of_m_nMovingToCenterStatus_216(),
	Ball_t2206666566::get_offset_of_m_bIsAdjustMoveProtect_217(),
	Ball_t2206666566::get_offset_of_m_fAdjustMoveProtectTime_218(),
	Ball_t2206666566::get_offset_of_m_dicGroupRoundId_219(),
	Ball_t2206666566::get_offset_of_m_fLocalOffsetX_220(),
	Ball_t2206666566::get_offset_of_m_fLocalOffsetY_221(),
	Ball_t2206666566::get_offset_of_m_bLockMove_222(),
	Ball_t2206666566::get_offset_of_m_nSizeClassId_223(),
	Ball_t2206666566::get_offset_of_m_fAttenuateSpeed_224(),
	Ball_t2206666566::get_offset_of_m_fClassInvasionAffectSpeed_225(),
	Ball_t2206666566::get_offset_of_m_nClassId_226(),
	Ball_t2206666566::get_offset_of_m_bStayOnClassCircle_227(),
	Ball_t2206666566::get_offset_of_m_nStayClassCircleId_228(),
	Ball_t2206666566::get_offset_of_m_bActive_229(),
	Ball_t2206666566::get_offset_of_m_nGuid_230(),
	Ball_t2206666566::get_offset_of_m_bEaten_231(),
	Ball_t2206666566::get_offset_of_m_vecDest_232(),
	Ball_t2206666566::get_offset_of_m_bIsMovingToDest_233(),
	Ball_t2206666566::get_offset_of_m_vecMoveToDestDir_234(),
	Ball_t2206666566::get_offset_of_m_dicDust_235(),
	Ball_t2206666566::get_offset_of_m_nOp_236(),
	Ball_t2206666566::get_offset_of_m_nOpScale_237(),
	0,
	0,
	Ball_t2206666566::get_offset_of_m_fBlingTime_240(),
	Ball_t2206666566::get_offset_of_m_nAutoMove_241(),
	Ball_t2206666566::get_offset_of_m_goZhangYu_242(),
	Ball_t2206666566::get_offset_of_bPlayerNameVisible_243(),
	Ball_t2206666566::get_offset_of_bPlayerNameSizeControl_244(),
	Ball_t2206666566::get_offset_of_m_bPicked_245(),
	Ball_t2206666566::get_offset_of_m_aryTiaoZiTunShi_246(),
	Ball_t2206666566::get_offset_of_m_lstIgnoredTeammate_247(),
	Ball_t2206666566::get_offset_of_m_flastTimeElapse_248(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (eBallType_t88636580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[5] = 
{
	eBallType_t88636580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (eEjectMode_t2915597278)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[4] = 
{
	eEjectMode_t2915597278::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (EatNode_t1329646118)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[2] = 
{
	EatNode_t1329646118::get_offset_of_eatenBall_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EatNode_t1329646118::get_offset_of_eatenSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (eShellType_t2293241718)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2750[5] = 
{
	eShellType_t2293241718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (BallTrigger_New_t3129829938), -1, sizeof(BallTrigger_New_t3129829938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2751[5] = 
{
	BallTrigger_New_t3129829938_StaticFields::get_offset_of_vec2Temp_2(),
	BallTrigger_New_t3129829938::get_offset_of__ball_3(),
	BallTrigger_New_t3129829938::get_offset_of__Trigger_4(),
	BallTrigger_New_t3129829938::get_offset_of__thorn_5(),
	BallTrigger_New_t3129829938::get_offset_of__type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (eTriggerType_t2367132649)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2752[7] = 
{
	eTriggerType_t2367132649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (CBlock_t2659765146), -1, sizeof(CBlock_t2659765146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2753[13] = 
{
	CBlock_t2659765146_StaticFields::get_offset_of_vecTempPos_2(),
	CBlock_t2659765146_StaticFields::get_offset_of_vecTempScale_3(),
	CBlock_t2659765146::get_offset_of_m_nHierarchy_4(),
	CBlock_t2659765146::get_offset_of__Collider_5(),
	CBlock_t2659765146::get_offset_of_m_aryChildBlock_6(),
	CBlock_t2659765146::get_offset_of_m_ePosType_7(),
	CBlock_t2659765146::get_offset_of_m_eParentPosType_8(),
	CBlock_t2659765146::get_offset_of_m_uKey_9(),
	CBlock_t2659765146::get_offset_of_m_uParentKey_10(),
	CBlock_t2659765146::get_offset_of_m_uGuid_11(),
	CBlock_t2659765146::get_offset_of_m_blockParent_12(),
	CBlock_t2659765146::get_offset_of_m_sprMain_13(),
	CBlock_t2659765146::get_offset_of_m_nBallNumInMe_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (CBlockDivide_t2736696553), -1, sizeof(CBlockDivide_t2736696553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2754[25] = 
{
	CBlockDivide_t2736696553_StaticFields::get_offset_of_s_Instance_2(),
	CBlockDivide_t2736696553_StaticFields::get_offset_of_vecTempPos_3(),
	CBlockDivide_t2736696553_StaticFields::get_offset_of_vecTempScale_4(),
	CBlockDivide_t2736696553_StaticFields::get_offset_of_vecTempDir_5(),
	CBlockDivide_t2736696553::get_offset_of_m_aryLineColor_6(),
	CBlockDivide_t2736696553::get_offset_of_m_aryBallColor_7(),
	CBlockDivide_t2736696553::get_offset_of_m_preLine_8(),
	CBlockDivide_t2736696553::get_offset_of_m_preBlock_9(),
	CBlockDivide_t2736696553::get_offset_of_m_fWorldSize_10(),
	CBlockDivide_t2736696553::get_offset_of_m_nHierarchyNum_11(),
	CBlockDivide_t2736696553::get_offset_of_m_aryBlockScaleOfEveryHierarchy_12(),
	CBlockDivide_t2736696553::get_offset_of_m_aryHierachyReverseKey_13(),
	CBlockDivide_t2736696553::get_offset_of_m_aryHierachyKey_14(),
	CBlockDivide_t2736696553::get_offset_of_m_goArea0_15(),
	CBlockDivide_t2736696553::get_offset_of_m_goArea0_Stains_16(),
	CBlockDivide_t2736696553::get_offset_of_m_nBlockNum_17(),
	0,
	0,
	CBlockDivide_t2736696553::get_offset_of__ball_20(),
	CBlockDivide_t2736696553::get_offset_of_m_fTimeElapse_21(),
	CBlockDivide_t2736696553::get_offset_of_m_bMoving_22(),
	CBlockDivide_t2736696553::get_offset_of_m_lstRecycledBlocks_23(),
	CBlockDivide_t2736696553::get_offset_of_m_dicRecycledBlocks_24(),
	CBlockDivide_t2736696553_StaticFields::get_offset_of_s_uBlockGuid_25(),
	CBlockDivide_t2736696553::get_offset_of_m_dicBlocks_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (eBlockPosType_t149092435)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2755[6] = 
{
	eBlockPosType_t149092435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (ComoList_t2152284863), -1, sizeof(ComoList_t2152284863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2756[12] = 
{
	ComoList_t2152284863::get_offset_of_m_preListItem_2(),
	ComoList_t2152284863_StaticFields::get_offset_of_vecTempPos_3(),
	ComoList_t2152284863_StaticFields::get_offset_of_vecTempScale_4(),
	ComoList_t2152284863::get_offset_of_m_lstItems_5(),
	ComoList_t2152284863::get_offset_of_m_goItemsContainer_6(),
	ComoList_t2152284863::get_offset_of_m_goSelectedKuangKuang_7(),
	ComoList_t2152284863::get_offset_of_c_ItemGapHeight_8(),
	ComoList_t2152284863::get_offset_of_c_ItemHeight_9(),
	ComoList_t2152284863::get_offset_of_c_ItemWidth_10(),
	0,
	ComoList_t2152284863::get_offset_of_m_itemCurSelectedItem_12(),
	ComoList_t2152284863::get_offset_of_delegateMethodOnSelect_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (DelegateMethod_OnSelected_t3195943432), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (ComoSceneManager_t977619994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (ComsmosPolygon_t3500719902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (CPlayerIns_t3208190728), -1, sizeof(CPlayerIns_t3208190728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2760[45] = 
{
	CPlayerIns_t3208190728::get_offset_of_m_szAccount_2(),
	CPlayerIns_t3208190728::get_offset_of_m_lstBalls_3(),
	CPlayerIns_t3208190728::get_offset_of__Player_4(),
	CPlayerIns_t3208190728_StaticFields::get_offset_of_vecTempPos_5(),
	CPlayerIns_t3208190728::get_offset_of_m_bIniting_6(),
	CPlayerIns_t3208190728::get_offset_of_m_nSkinId_7(),
	CPlayerIns_t3208190728::get_offset_of_m_Color_8(),
	CPlayerIns_t3208190728::get_offset_of_m_nColorId_9(),
	CPlayerIns_t3208190728::get_offset_of_m_szPlayerName_10(),
	CPlayerIns_t3208190728::get_offset_of_m_bytesBallsData_11(),
	CPlayerIns_t3208190728::get_offset_of_m_fBeginInitingTime_12(),
	0,
	CPlayerIns_t3208190728::get_offset_of_m_fInitingBallsTimeCount_14(),
	CPlayerIns_t3208190728::get_offset_of_m_nInitingIndex_15(),
	CPlayerIns_t3208190728::get_offset_of__bytesLiveBallData_16(),
	CPlayerIns_t3208190728::get_offset_of_m_nKillCount_17(),
	CPlayerIns_t3208190728::get_offset_of_m_nBeKilledCount_18(),
	CPlayerIns_t3208190728::get_offset_of_m_fIntervalLoopTimeElapse_0_3_19(),
	CPlayerIns_t3208190728::get_offset_of_m_fIntervalLoopTimeElapse_1_20(),
	CPlayerIns_t3208190728::get_offset_of_m_fIntervalLoopTimeElapse_0_1_21(),
	CPlayerIns_t3208190728::get_offset_of_m_fAttenuateTimeLapse_22(),
	CPlayerIns_t3208190728::get_offset_of_m_aryLiveBalls_23(),
	CPlayerIns_t3208190728::get_offset_of_m_lstNewDeadBall_24(),
	CPlayerIns_t3208190728::get_offset_of_m_bDead_25(),
	CPlayerIns_t3208190728::get_offset_of_m_lstExplodingNode_26(),
	CPlayerIns_t3208190728::get_offset_of_m_lstRecycledExplodeNode_27(),
	CPlayerIns_t3208190728::get_offset_of_m_dicBallsToReuse_New_28(),
	CPlayerIns_t3208190728::get_offset_of_m_dicBallsToReuse_29(),
	CPlayerIns_t3208190728::get_offset_of_m_arySkillStatus_30(),
	CPlayerIns_t3208190728::get_offset_of_m_eSkillId_31(),
	CPlayerIns_t3208190728::get_offset_of_m_fSkillTimeElapse_32(),
	CPlayerIns_t3208190728::get_offset_of_m_fSkillQianYaoTime_33(),
	CPlayerIns_t3208190728::get_offset_of_m_fSkillTimeElapse_Sneak_34(),
	CPlayerIns_t3208190728::get_offset_of_m_fSkillTotalTime_35(),
	CPlayerIns_t3208190728::get_offset_of_m_fSkillQianYaoTime_Sneak_36(),
	CPlayerIns_t3208190728::get_offset_of_m_nSKillStatus_KuoZhang_37(),
	CPlayerIns_t3208190728::get_offset_of_m_nSKill_QianYao_KuoZhang_38(),
	CPlayerIns_t3208190728::get_offset_of_m_nSKill_ChiXu_KuoZhang_39(),
	CPlayerIns_t3208190728::get_offset_of_m_fTimeElapse_KuoZhang_40(),
	CPlayerIns_t3208190728::get_offset_of_m_fKuoZhangAngle_41(),
	CPlayerIns_t3208190728::get_offset_of_m_fKuoZhangUnfoldScale_42(),
	CPlayerIns_t3208190728::get_offset_of_m_fKuoKuoZhangParam1_43(),
	CPlayerIns_t3208190728::get_offset_of_m_fKuoKuoZhangParam2_44(),
	CPlayerIns_t3208190728::get_offset_of_m_eKuoZhangStatus_45(),
	CPlayerIns_t3208190728::get_offset_of_m_dicMostBigBallsToShowName_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (eKuoZhangStatus_t123357593)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[7] = 
{
	eKuoZhangStatus_t123357593::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (CPlayerManager_t2516829028), -1, sizeof(CPlayerManager_t2516829028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2762[11] = 
{
	CPlayerManager_t2516829028::get_offset_of_m_prePlayerIns_2(),
	CPlayerManager_t2516829028::get_offset_of_m_dicPlayerInfo_Common_3(),
	CPlayerManager_t2516829028_StaticFields::get_offset_of_s_Instance_4(),
	CPlayerManager_t2516829028::get_offset_of_m_dicPlayers_5(),
	CPlayerManager_t2516829028::get_offset_of_m_dicPlayerInfo_RealTime_6(),
	CPlayerManager_t2516829028::get_offset_of_m_szShengFuPlayerName_7(),
	CPlayerManager_t2516829028::get_offset_of_m_dicPlayerIns_8(),
	CPlayerManager_t2516829028::get_offset_of_m_goContainerPlayerIns_9(),
	CPlayerManager_t2516829028::get_offset_of_m_dicPlayerByAccount_10(),
	CPlayerManager_t2516829028::get_offset_of__byteOrphanBallDatat_11(),
	CPlayerManager_t2516829028::get_offset_of_m_lstTempOrphanBallData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (sPlayerInfoCommon_t369699146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[15] = 
{
	sPlayerInfoCommon_t369699146::get_offset_of_nLevel_0(),
	sPlayerInfoCommon_t369699146::get_offset_of_nExp_1(),
	sPlayerInfoCommon_t369699146::get_offset_of_nMoney_2(),
	sPlayerInfoCommon_t369699146::get_offset_of_fBaseVolumeByLevel_3(),
	sPlayerInfoCommon_t369699146::get_offset_of_fBaseSpeed_4(),
	sPlayerInfoCommon_t369699146::get_offset_of_fBaseShellTime_5(),
	sPlayerInfoCommon_t369699146::get_offset_of_nItem_BaseSpeed_level_6(),
	sPlayerInfoCommon_t369699146::get_offset_of_nItem_BaseShellTime_level_7(),
	sPlayerInfoCommon_t369699146::get_offset_of_nEatThornNum_8(),
	sPlayerInfoCommon_t369699146::get_offset_of_nSelectedSkillId_9(),
	sPlayerInfoCommon_t369699146::get_offset_of_nSkillWLevel_10(),
	sPlayerInfoCommon_t369699146::get_offset_of_nSkillELevel_11(),
	sPlayerInfoCommon_t369699146::get_offset_of_nSkillRLevel_12(),
	sPlayerInfoCommon_t369699146::get_offset_of_nSkillPoint_13(),
	sPlayerInfoCommon_t369699146::get_offset_of_bytes_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (ePlayerCommonInfoType_t3738107693)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2764[15] = 
{
	ePlayerCommonInfoType_t3738107693::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (sOrphanInfo_t2789095025)+ sizeof (RuntimeObject), sizeof(sOrphanInfo_t2789095025_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2765[6] = 
{
	sOrphanInfo_t2789095025::get_offset_of_szAccount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sOrphanInfo_t2789095025::get_offset_of_szPlayerName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sOrphanInfo_t2789095025::get_offset_of_nColorId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sOrphanInfo_t2789095025::get_offset_of_nSkinId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sOrphanInfo_t2789095025::get_offset_of_nBlobSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sOrphanInfo_t2789095025::get_offset_of_bytesBallsData_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (CreateNewShit_t3983358377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[5] = 
{
	CreateNewShit_t3983358377::get_offset_of__txtTitle_2(),
	CreateNewShit_t3983358377::get_offset_of__inputNewShitName_3(),
	CreateNewShit_t3983358377::get_offset_of__btnOK_4(),
	CreateNewShit_t3983358377::get_offset_of__btnCancel_5(),
	CreateNewShit_t3983358377::get_offset_of_delegateMethodOK_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (DelegateMethod_OK_t3925705237), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (CSkillManager_t2937013327), -1, sizeof(CSkillManager_t2937013327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2768[4] = 
{
	CSkillManager_t2937013327::get_offset_of_m_Player_2(),
	CSkillManager_t2937013327::get_offset_of_m_lstBabaThorn_3(),
	CSkillManager_t2937013327_StaticFields::get_offset_of_vec3Temp_4(),
	CSkillManager_t2937013327_StaticFields::get_offset_of_vec2Temp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (eSkillType_t3445085857)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2769[2] = 
{
	eSkillType_t3445085857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (CSpore_t10900549), -1, sizeof(CSpore_t10900549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2770[23] = 
{
	CSpore_t10900549_StaticFields::get_offset_of_vecTempPos_2(),
	CSpore_t10900549_StaticFields::get_offset_of_vecTempSize_3(),
	CSpore_t10900549::get_offset_of_m_nPlayerId_4(),
	CSpore_t10900549::get_offset_of_m_uSporeId_5(),
	CSpore_t10900549::get_offset_of_m_fVolume_6(),
	CSpore_t10900549::get_offset_of_m_fRadius_7(),
	CSpore_t10900549::get_offset_of_m_vDir_8(),
	CSpore_t10900549::get_offset_of_m_vSpeed_9(),
	CSpore_t10900549::get_offset_of_m_vInitSpeed_10(),
	CSpore_t10900549::get_offset_of_m_vAcclerate_11(),
	CSpore_t10900549::get_offset_of_m_bFinishedX_12(),
	CSpore_t10900549::get_offset_of_m_bFinishedY_13(),
	CSpore_t10900549::get_offset_of_m_fTotalEjectDistance_14(),
	CSpore_t10900549::get_offset_of_m_eEjectingStatus_15(),
	CSpore_t10900549::get_offset_of_m_Ball_16(),
	CSpore_t10900549::get_offset_of__Collider_17(),
	CSpore_t10900549::get_offset_of__srMain_18(),
	CSpore_t10900549::get_offset_of_m_goContainer_19(),
	CSpore_t10900549::get_offset_of_m_bIsEjecting_20(),
	CSpore_t10900549::get_offset_of_m_bAccelerating_21(),
	CSpore_t10900549::get_offset_of_m_vecA_22(),
	CSpore_t10900549::get_offset_of_m_vecEjectStartPos_23(),
	CSpore_t10900549::get_offset_of_m_bDead_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (eEjectMode_t2913208038)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2771[4] = 
{
	eEjectMode_t2913208038::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (CyberTreeMath_t1979652797), -1, sizeof(CyberTreeMath_t1979652797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2772[5] = 
{
	CyberTreeMath_t1979652797_StaticFields::get_offset_of_s_vecTempPos_2(),
	CyberTreeMath_t1979652797_StaticFields::get_offset_of_c_fAngleToRadian_3(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (DistrictAndRoomManager_t4043287150), -1, sizeof(DistrictAndRoomManager_t4043287150_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2773[8] = 
{
	DistrictAndRoomManager_t4043287150_StaticFields::get_offset_of_s_Instance_2(),
	DistrictAndRoomManager_t4043287150::get_offset_of__txtCurAccount_3(),
	DistrictAndRoomManager_t4043287150::get_offset_of__txtHostAccount_4(),
	DistrictAndRoomManager_t4043287150::get_offset_of__txtHostOrGuest_5(),
	DistrictAndRoomManager_t4043287150::get_offset_of__txtCurSelectedDistrict_6(),
	DistrictAndRoomManager_t4043287150::get_offset_of__txtCurSelectedRoom_7(),
	DistrictAndRoomManager_t4043287150::get_offset_of__dropdownDistrict_8(),
	DistrictAndRoomManager_t4043287150::get_offset_of__dropdownRoom_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (EasyTouchTrigger_t1019744654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[1] = 
{
	EasyTouchTrigger_t1019744654::get_offset_of_receivers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (ETTParameter_t3639556146)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2775[17] = 
{
	ETTParameter_t3639556146::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (ETTType_t2791900397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2776[3] = 
{
	ETTType_t2791900397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (EasyTouchReceiver_t857180639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[10] = 
{
	EasyTouchReceiver_t857180639::get_offset_of_enable_0(),
	EasyTouchReceiver_t857180639::get_offset_of_triggerType_1(),
	EasyTouchReceiver_t857180639::get_offset_of_name_2(),
	EasyTouchReceiver_t857180639::get_offset_of_restricted_3(),
	EasyTouchReceiver_t857180639::get_offset_of_gameObject_4(),
	EasyTouchReceiver_t857180639::get_offset_of_otherReceiver_5(),
	EasyTouchReceiver_t857180639::get_offset_of_gameObjectReceiver_6(),
	EasyTouchReceiver_t857180639::get_offset_of_eventName_7(),
	EasyTouchReceiver_t857180639::get_offset_of_methodName_8(),
	EasyTouchReceiver_t857180639::get_offset_of_parameter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (U3CIsRecevier4U3Ec__AnonStorey0_t2005754022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[1] = 
{
	U3CIsRecevier4U3Ec__AnonStorey0_t2005754022::get_offset_of_evnt_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (U3CGetTriggerU3Ec__AnonStorey1_t3423710625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[1] = 
{
	U3CGetTriggerU3Ec__AnonStorey1_t3423710625::get_offset_of_triggerName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (QuickBase_t718481069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[17] = 
{
	QuickBase_t718481069::get_offset_of_quickActionName_2(),
	QuickBase_t718481069::get_offset_of_isMultiTouch_3(),
	QuickBase_t718481069::get_offset_of_is2Finger_4(),
	QuickBase_t718481069::get_offset_of_isOnTouch_5(),
	QuickBase_t718481069::get_offset_of_enablePickOverUI_6(),
	QuickBase_t718481069::get_offset_of_resetPhysic_7(),
	QuickBase_t718481069::get_offset_of_directAction_8(),
	QuickBase_t718481069::get_offset_of_axesAction_9(),
	QuickBase_t718481069::get_offset_of_sensibility_10(),
	QuickBase_t718481069::get_offset_of_directCharacterController_11(),
	QuickBase_t718481069::get_offset_of_inverseAxisValue_12(),
	QuickBase_t718481069::get_offset_of_cachedRigidBody_13(),
	QuickBase_t718481069::get_offset_of_isKinematic_14(),
	QuickBase_t718481069::get_offset_of_cachedRigidBody2D_15(),
	QuickBase_t718481069::get_offset_of_isKinematic2D_16(),
	QuickBase_t718481069::get_offset_of_realType_17(),
	QuickBase_t718481069::get_offset_of_fingerIndex_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (GameObjectType_t2199397569)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2781[5] = 
{
	GameObjectType_t2199397569::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (DirectAction_t2452253191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[7] = 
{
	DirectAction_t2452253191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (AffectedAxesAction_t2927047446)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2783[8] = 
{
	AffectedAxesAction_t2927047446::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (QuickDrag_t787596845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[7] = 
{
	QuickDrag_t787596845::get_offset_of_onDragStart_19(),
	QuickDrag_t787596845::get_offset_of_onDrag_20(),
	QuickDrag_t787596845::get_offset_of_onDragEnd_21(),
	QuickDrag_t787596845::get_offset_of_isStopOncollisionEnter_22(),
	QuickDrag_t787596845::get_offset_of_deltaPosition_23(),
	QuickDrag_t787596845::get_offset_of_isOnDrag_24(),
	QuickDrag_t787596845::get_offset_of_lastGesture_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (OnDragStart_t3676143812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (OnDrag_t3336846729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (OnDragEnd_t1175011983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (QuickEnterOverExist_t931184099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[4] = 
{
	QuickEnterOverExist_t931184099::get_offset_of_onTouchEnter_19(),
	QuickEnterOverExist_t931184099::get_offset_of_onTouchOver_20(),
	QuickEnterOverExist_t931184099::get_offset_of_onTouchExit_21(),
	QuickEnterOverExist_t931184099::get_offset_of_fingerOver_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (OnTouchEnter_t1794713516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (OnTouchOver_t3317551264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (OnTouchExit_t2180465286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (QuickLongTap_t332644307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[3] = 
{
	QuickLongTap_t332644307::get_offset_of_onLongTap_19(),
	QuickLongTap_t332644307::get_offset_of_actionTriggering_20(),
	QuickLongTap_t332644307::get_offset_of_currentGesture_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (OnLongTap_t243381522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (ActionTriggering_t435375650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2794[4] = 
{
	ActionTriggering_t435375650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (QuickPinch_t1656003506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[6] = 
{
	QuickPinch_t1656003506::get_offset_of_onPinchAction_19(),
	QuickPinch_t1656003506::get_offset_of_isGestureOnMe_20(),
	QuickPinch_t1656003506::get_offset_of_actionTriggering_21(),
	QuickPinch_t1656003506::get_offset_of_pinchDirection_22(),
	QuickPinch_t1656003506::get_offset_of_axisActionValue_23(),
	QuickPinch_t1656003506::get_offset_of_enableSimpleAction_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (OnPinchAction_t3145810715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (ActionTiggering_t2337630454)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2797[3] = 
{
	ActionTiggering_t2337630454::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (ActionPinchDirection_t755360707)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2798[4] = 
{
	ActionPinchDirection_t755360707::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (QuickSwipe_t3226549687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[6] = 
{
	QuickSwipe_t3226549687::get_offset_of_onSwipeAction_19(),
	QuickSwipe_t3226549687::get_offset_of_allowSwipeStartOverMe_20(),
	QuickSwipe_t3226549687::get_offset_of_actionTriggering_21(),
	QuickSwipe_t3226549687::get_offset_of_swipeDirection_22(),
	QuickSwipe_t3226549687::get_offset_of_axisActionValue_23(),
	QuickSwipe_t3226549687::get_offset_of_enableSimpleAction_24(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
