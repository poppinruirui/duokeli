﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Single[]
struct SingleU5BU5D_t1444911251;
// Spine.ExposedList`1<Spine.Polygon>
struct ExposedList_1_t1727260827;
// Spine.ExposedList`1<Spine.BoundingBoxAttachment>
struct ExposedList_1_t1209651080;
// Spine.Unity.Modules.SkeletonRagdoll
struct SkeletonRagdoll_t480923817;
// Spine.Triangulator
struct Triangulator_t2502879214;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_t4104378640;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1363090323;
// Spine.ClippingAttachment
struct ClippingAttachment_t2586274570;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>>
struct ExposedList_1_t2516523210;
// System.String
struct String_t;
// Spine.ExposedList`1<Spine.BoneData>
struct ExposedList_1_t1542319060;
// Spine.ExposedList`1<Spine.SlotData>
struct ExposedList_1_t2861913768;
// Spine.ExposedList`1<Spine.Skin>
struct ExposedList_1_t3881696472;
// Spine.Skin
struct Skin_t1174584606;
// Spine.ExposedList`1<Spine.EventData>
struct ExposedList_1_t3431871853;
// Spine.ExposedList`1<Spine.Animation>
struct ExposedList_1_t3322895149;
// Spine.ExposedList`1<Spine.IkConstraintData>
struct ExposedList_1_t3166231995;
// Spine.ExposedList`1<Spine.TransformConstraintData>
struct ExposedList_1_t3236185212;
// Spine.ExposedList`1<Spine.PathConstraintData>
struct ExposedList_1_t3688408900;
// Spine.AttachmentLoader
struct AttachmentLoader_t3324778997;
// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh>
struct List_1_t3168994201;
// Spine.MeshAttachment
struct MeshAttachment_t1975337962;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Char[]
struct CharU5BU5D_t3528271667;
// SharpJson.Lexer
struct Lexer_t3897031801;
// Spine.PathConstraintData
struct PathConstraintData_t981297034;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_t3793468194;
// Spine.Slot
struct Slot_t3514940700;
// Spine.SkeletonData
struct SkeletonData_t2032710716;
// Spine.ExposedList`1<Spine.Slot>
struct ExposedList_1_t1927085270;
// Spine.ExposedList`1<Spine.IkConstraint>
struct ExposedList_1_t87334839;
// Spine.ExposedList`1<Spine.TransformConstraint>
struct ExposedList_1_t3161142095;
// Spine.ExposedList`1<Spine.PathConstraint>
struct ExposedList_1_t3687966776;
// Spine.ExposedList`1<Spine.IUpdatable>
struct ExposedList_1_t2549315650;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Spine.TransformMode[]
struct TransformModeU5BU5D_t3210249249;
// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment>
struct Dictionary_2_t968660385;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t651787775;
// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction>
struct ExposedList_1_t2759233236;
// Spine.ExposedList`1<Spine.Attachment>
struct ExposedList_1_t1455901122;
// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh>
struct DoubleBuffered_1_t2489386064;
// Spine.ExposedList`1<UnityEngine.Material>
struct ExposedList_1_t3047486989;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// Spine.SlotData
struct SlotData_t154801902;
// Spine.Bone
struct Bone_t1086356328;
// Spine.Attachment
struct Attachment_t3043756552;
// Spine.TransformConstraintData
struct TransformConstraintData_t529073346;
// Spine.BoneData
struct BoneData_t3130174490;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>>
struct ExposedList_1_t4070202189;
// Spine.ExposedList`1<System.Boolean>
struct ExposedList_1_t2804399831;
// Spine.Pool`1<Spine.ExposedList`1<System.Single>>
struct Pool_1_t3075278099;
// Spine.Pool`1<Spine.ExposedList`1<System.Int32>>
struct Pool_1_t333989782;
// Spine.Unity.AtlasAsset
struct AtlasAsset_t1167231206;
// Spine.Skeleton
struct Skeleton_t3686076450;
// Spine.Atlas[]
struct AtlasU5BU5D_t3463999232;
// Spine.AtlasPage
struct AtlasPage_t4077017671;
// System.Collections.Generic.List`1<Spine.AtlasPage>
struct List_1_t1254125117;
// System.Collections.Generic.List`1<Spine.AtlasRegion>
struct List_1_t1485978026;
// Spine.TextureLoader
struct TextureLoader_t3496536928;
// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single>
struct Dictionary_2_t680659097;
// Spine.EventData
struct EventData_t724759987;
// System.Collections.Generic.List`1<Spine.BoneData>
struct List_1_t307281936;
// Spine.IkConstraintData
struct IkConstraintData_t459120129;
// Spine.Animation
struct Animation_t615783283;
// UnityEngine.Material
struct Material_t340375123;
// System.Void
struct Void_t1185182177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t1354683548;
// Spine.Unity.Modules.SkeletonGhostRenderer
struct SkeletonGhostRenderer_t2445315009;
// Spine.ExposedList`1<UnityEngine.Vector3>
struct ExposedList_1_t2134458034;
// Spine.ExposedList`1<UnityEngine.Vector2>
struct ExposedList_1_t568374093;
// Spine.ExposedList`1<UnityEngine.Color32>
struct ExposedList_1_t1012645862;
// Spine.SkeletonClipping
struct SkeletonClipping_t1669006083;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Spine.TrackEntry
struct TrackEntry_t1316488441;
// Spine.Event
struct Event_t1378573841;
// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D>
struct Dictionary_2_t47542197;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// Spine.Atlas
struct Atlas_t4040192941;
// Spine.Unity.AtlasAsset[]
struct AtlasAssetU5BU5D_t395880643;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t2933699135;
// Spine.AnimationStateData
struct AnimationStateData_t3010651567;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_t3931555305;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform>
struct Dictionary_2_t2478265129;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Shader
struct Shader_t4151988712;
// Spine.Unity.Modules.SkeletonGhostRenderer[]
struct SkeletonGhostRendererU5BU5D_t513912860;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t2098681813;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t3700682020;
// Spine.BoundingBoxAttachment
struct BoundingBoxAttachment_t2797506510;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t57175488;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D>
struct Dictionary_2_t3714483914;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String>
struct Dictionary_2_t1209791819;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride>
struct List_1_t2474053923;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride>
struct List_1_t3907116131;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;

struct Vector3_t3722313464 ;
struct Vector2_t2156229523 ;
struct Color32_t2600501292 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef POLYGON_T3315116257_H
#define POLYGON_T3315116257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Polygon
struct  Polygon_t3315116257  : public RuntimeObject
{
public:
	// System.Single[] Spine.Polygon::<Vertices>k__BackingField
	SingleU5BU5D_t1444911251* ___U3CVerticesU3Ek__BackingField_0;
	// System.Int32 Spine.Polygon::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CVerticesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Polygon_t3315116257, ___U3CVerticesU3Ek__BackingField_0)); }
	inline SingleU5BU5D_t1444911251* get_U3CVerticesU3Ek__BackingField_0() const { return ___U3CVerticesU3Ek__BackingField_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_U3CVerticesU3Ek__BackingField_0() { return &___U3CVerticesU3Ek__BackingField_0; }
	inline void set_U3CVerticesU3Ek__BackingField_0(SingleU5BU5D_t1444911251* value)
	{
		___U3CVerticesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVerticesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Polygon_t3315116257, ___U3CCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CCountU3Ek__BackingField_1() const { return ___U3CCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_1() { return &___U3CCountU3Ek__BackingField_1; }
	inline void set_U3CCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CCountU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T3315116257_H
#ifndef SKELETONBOUNDS_T352077915_H
#define SKELETONBOUNDS_T352077915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBounds
struct  SkeletonBounds_t352077915  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::polygonPool
	ExposedList_1_t1727260827 * ___polygonPool_0;
	// System.Single Spine.SkeletonBounds::minX
	float ___minX_1;
	// System.Single Spine.SkeletonBounds::minY
	float ___minY_2;
	// System.Single Spine.SkeletonBounds::maxX
	float ___maxX_3;
	// System.Single Spine.SkeletonBounds::maxY
	float ___maxY_4;
	// Spine.ExposedList`1<Spine.BoundingBoxAttachment> Spine.SkeletonBounds::<BoundingBoxes>k__BackingField
	ExposedList_1_t1209651080 * ___U3CBoundingBoxesU3Ek__BackingField_5;
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::<Polygons>k__BackingField
	ExposedList_1_t1727260827 * ___U3CPolygonsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_polygonPool_0() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___polygonPool_0)); }
	inline ExposedList_1_t1727260827 * get_polygonPool_0() const { return ___polygonPool_0; }
	inline ExposedList_1_t1727260827 ** get_address_of_polygonPool_0() { return &___polygonPool_0; }
	inline void set_polygonPool_0(ExposedList_1_t1727260827 * value)
	{
		___polygonPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_0), value);
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___minX_1)); }
	inline float get_minX_1() const { return ___minX_1; }
	inline float* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(float value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___minY_2)); }
	inline float get_minY_2() const { return ___minY_2; }
	inline float* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(float value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_maxX_3() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___maxX_3)); }
	inline float get_maxX_3() const { return ___maxX_3; }
	inline float* get_address_of_maxX_3() { return &___maxX_3; }
	inline void set_maxX_3(float value)
	{
		___maxX_3 = value;
	}

	inline static int32_t get_offset_of_maxY_4() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___maxY_4)); }
	inline float get_maxY_4() const { return ___maxY_4; }
	inline float* get_address_of_maxY_4() { return &___maxY_4; }
	inline void set_maxY_4(float value)
	{
		___maxY_4 = value;
	}

	inline static int32_t get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___U3CBoundingBoxesU3Ek__BackingField_5)); }
	inline ExposedList_1_t1209651080 * get_U3CBoundingBoxesU3Ek__BackingField_5() const { return ___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline ExposedList_1_t1209651080 ** get_address_of_U3CBoundingBoxesU3Ek__BackingField_5() { return &___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline void set_U3CBoundingBoxesU3Ek__BackingField_5(ExposedList_1_t1209651080 * value)
	{
		___U3CBoundingBoxesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundingBoxesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPolygonsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___U3CPolygonsU3Ek__BackingField_6)); }
	inline ExposedList_1_t1727260827 * get_U3CPolygonsU3Ek__BackingField_6() const { return ___U3CPolygonsU3Ek__BackingField_6; }
	inline ExposedList_1_t1727260827 ** get_address_of_U3CPolygonsU3Ek__BackingField_6() { return &___U3CPolygonsU3Ek__BackingField_6; }
	inline void set_U3CPolygonsU3Ek__BackingField_6(ExposedList_1_t1727260827 * value)
	{
		___U3CPolygonsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPolygonsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBOUNDS_T352077915_H
#ifndef U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T2718417871_H
#define U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T2718417871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1
struct  U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::<startMix>__0
	float ___U3CstartMixU3E__0_1;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::target
	float ___target_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::duration
	float ___duration_3;
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$this
	SkeletonRagdoll_t480923817 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U3CstartMixU3E__0_1)); }
	inline float get_U3CstartMixU3E__0_1() const { return ___U3CstartMixU3E__0_1; }
	inline float* get_address_of_U3CstartMixU3E__0_1() { return &___U3CstartMixU3E__0_1; }
	inline void set_U3CstartMixU3E__0_1(float value)
	{
		___U3CstartMixU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24this_4)); }
	inline SkeletonRagdoll_t480923817 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonRagdoll_t480923817 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonRagdoll_t480923817 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T2718417871_H
#ifndef SKELETONCLIPPING_T1669006083_H
#define SKELETONCLIPPING_T1669006083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonClipping
struct  SkeletonClipping_t1669006083  : public RuntimeObject
{
public:
	// Spine.Triangulator Spine.SkeletonClipping::triangulator
	Triangulator_t2502879214 * ___triangulator_0;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippingPolygon
	ExposedList_1_t4104378640 * ___clippingPolygon_1;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clipOutput
	ExposedList_1_t4104378640 * ___clipOutput_2;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedVertices
	ExposedList_1_t4104378640 * ___clippedVertices_3;
	// Spine.ExposedList`1<System.Int32> Spine.SkeletonClipping::clippedTriangles
	ExposedList_1_t1363090323 * ___clippedTriangles_4;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedUVs
	ExposedList_1_t4104378640 * ___clippedUVs_5;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::scratch
	ExposedList_1_t4104378640 * ___scratch_6;
	// Spine.ClippingAttachment Spine.SkeletonClipping::clipAttachment
	ClippingAttachment_t2586274570 * ___clipAttachment_7;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.SkeletonClipping::clippingPolygons
	ExposedList_1_t2516523210 * ___clippingPolygons_8;

public:
	inline static int32_t get_offset_of_triangulator_0() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___triangulator_0)); }
	inline Triangulator_t2502879214 * get_triangulator_0() const { return ___triangulator_0; }
	inline Triangulator_t2502879214 ** get_address_of_triangulator_0() { return &___triangulator_0; }
	inline void set_triangulator_0(Triangulator_t2502879214 * value)
	{
		___triangulator_0 = value;
		Il2CppCodeGenWriteBarrier((&___triangulator_0), value);
	}

	inline static int32_t get_offset_of_clippingPolygon_1() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippingPolygon_1)); }
	inline ExposedList_1_t4104378640 * get_clippingPolygon_1() const { return ___clippingPolygon_1; }
	inline ExposedList_1_t4104378640 ** get_address_of_clippingPolygon_1() { return &___clippingPolygon_1; }
	inline void set_clippingPolygon_1(ExposedList_1_t4104378640 * value)
	{
		___clippingPolygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygon_1), value);
	}

	inline static int32_t get_offset_of_clipOutput_2() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clipOutput_2)); }
	inline ExposedList_1_t4104378640 * get_clipOutput_2() const { return ___clipOutput_2; }
	inline ExposedList_1_t4104378640 ** get_address_of_clipOutput_2() { return &___clipOutput_2; }
	inline void set_clipOutput_2(ExposedList_1_t4104378640 * value)
	{
		___clipOutput_2 = value;
		Il2CppCodeGenWriteBarrier((&___clipOutput_2), value);
	}

	inline static int32_t get_offset_of_clippedVertices_3() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippedVertices_3)); }
	inline ExposedList_1_t4104378640 * get_clippedVertices_3() const { return ___clippedVertices_3; }
	inline ExposedList_1_t4104378640 ** get_address_of_clippedVertices_3() { return &___clippedVertices_3; }
	inline void set_clippedVertices_3(ExposedList_1_t4104378640 * value)
	{
		___clippedVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___clippedVertices_3), value);
	}

	inline static int32_t get_offset_of_clippedTriangles_4() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippedTriangles_4)); }
	inline ExposedList_1_t1363090323 * get_clippedTriangles_4() const { return ___clippedTriangles_4; }
	inline ExposedList_1_t1363090323 ** get_address_of_clippedTriangles_4() { return &___clippedTriangles_4; }
	inline void set_clippedTriangles_4(ExposedList_1_t1363090323 * value)
	{
		___clippedTriangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___clippedTriangles_4), value);
	}

	inline static int32_t get_offset_of_clippedUVs_5() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippedUVs_5)); }
	inline ExposedList_1_t4104378640 * get_clippedUVs_5() const { return ___clippedUVs_5; }
	inline ExposedList_1_t4104378640 ** get_address_of_clippedUVs_5() { return &___clippedUVs_5; }
	inline void set_clippedUVs_5(ExposedList_1_t4104378640 * value)
	{
		___clippedUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___clippedUVs_5), value);
	}

	inline static int32_t get_offset_of_scratch_6() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___scratch_6)); }
	inline ExposedList_1_t4104378640 * get_scratch_6() const { return ___scratch_6; }
	inline ExposedList_1_t4104378640 ** get_address_of_scratch_6() { return &___scratch_6; }
	inline void set_scratch_6(ExposedList_1_t4104378640 * value)
	{
		___scratch_6 = value;
		Il2CppCodeGenWriteBarrier((&___scratch_6), value);
	}

	inline static int32_t get_offset_of_clipAttachment_7() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clipAttachment_7)); }
	inline ClippingAttachment_t2586274570 * get_clipAttachment_7() const { return ___clipAttachment_7; }
	inline ClippingAttachment_t2586274570 ** get_address_of_clipAttachment_7() { return &___clipAttachment_7; }
	inline void set_clipAttachment_7(ClippingAttachment_t2586274570 * value)
	{
		___clipAttachment_7 = value;
		Il2CppCodeGenWriteBarrier((&___clipAttachment_7), value);
	}

	inline static int32_t get_offset_of_clippingPolygons_8() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippingPolygons_8)); }
	inline ExposedList_1_t2516523210 * get_clippingPolygons_8() const { return ___clippingPolygons_8; }
	inline ExposedList_1_t2516523210 ** get_address_of_clippingPolygons_8() { return &___clippingPolygons_8; }
	inline void set_clippingPolygons_8(ExposedList_1_t2516523210 * value)
	{
		___clippingPolygons_8 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygons_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONCLIPPING_T1669006083_H
#ifndef SKELETONDATA_T2032710716_H
#define SKELETONDATA_T2032710716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonData
struct  SkeletonData_t2032710716  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonData::name
	String_t* ___name_0;
	// Spine.ExposedList`1<Spine.BoneData> Spine.SkeletonData::bones
	ExposedList_1_t1542319060 * ___bones_1;
	// Spine.ExposedList`1<Spine.SlotData> Spine.SkeletonData::slots
	ExposedList_1_t2861913768 * ___slots_2;
	// Spine.ExposedList`1<Spine.Skin> Spine.SkeletonData::skins
	ExposedList_1_t3881696472 * ___skins_3;
	// Spine.Skin Spine.SkeletonData::defaultSkin
	Skin_t1174584606 * ___defaultSkin_4;
	// Spine.ExposedList`1<Spine.EventData> Spine.SkeletonData::events
	ExposedList_1_t3431871853 * ___events_5;
	// Spine.ExposedList`1<Spine.Animation> Spine.SkeletonData::animations
	ExposedList_1_t3322895149 * ___animations_6;
	// Spine.ExposedList`1<Spine.IkConstraintData> Spine.SkeletonData::ikConstraints
	ExposedList_1_t3166231995 * ___ikConstraints_7;
	// Spine.ExposedList`1<Spine.TransformConstraintData> Spine.SkeletonData::transformConstraints
	ExposedList_1_t3236185212 * ___transformConstraints_8;
	// Spine.ExposedList`1<Spine.PathConstraintData> Spine.SkeletonData::pathConstraints
	ExposedList_1_t3688408900 * ___pathConstraints_9;
	// System.Single Spine.SkeletonData::width
	float ___width_10;
	// System.Single Spine.SkeletonData::height
	float ___height_11;
	// System.String Spine.SkeletonData::version
	String_t* ___version_12;
	// System.String Spine.SkeletonData::hash
	String_t* ___hash_13;
	// System.Single Spine.SkeletonData::fps
	float ___fps_14;
	// System.String Spine.SkeletonData::imagesPath
	String_t* ___imagesPath_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___bones_1)); }
	inline ExposedList_1_t1542319060 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t1542319060 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t1542319060 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___slots_2)); }
	inline ExposedList_1_t2861913768 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t2861913768 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t2861913768 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_skins_3() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___skins_3)); }
	inline ExposedList_1_t3881696472 * get_skins_3() const { return ___skins_3; }
	inline ExposedList_1_t3881696472 ** get_address_of_skins_3() { return &___skins_3; }
	inline void set_skins_3(ExposedList_1_t3881696472 * value)
	{
		___skins_3 = value;
		Il2CppCodeGenWriteBarrier((&___skins_3), value);
	}

	inline static int32_t get_offset_of_defaultSkin_4() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___defaultSkin_4)); }
	inline Skin_t1174584606 * get_defaultSkin_4() const { return ___defaultSkin_4; }
	inline Skin_t1174584606 ** get_address_of_defaultSkin_4() { return &___defaultSkin_4; }
	inline void set_defaultSkin_4(Skin_t1174584606 * value)
	{
		___defaultSkin_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSkin_4), value);
	}

	inline static int32_t get_offset_of_events_5() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___events_5)); }
	inline ExposedList_1_t3431871853 * get_events_5() const { return ___events_5; }
	inline ExposedList_1_t3431871853 ** get_address_of_events_5() { return &___events_5; }
	inline void set_events_5(ExposedList_1_t3431871853 * value)
	{
		___events_5 = value;
		Il2CppCodeGenWriteBarrier((&___events_5), value);
	}

	inline static int32_t get_offset_of_animations_6() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___animations_6)); }
	inline ExposedList_1_t3322895149 * get_animations_6() const { return ___animations_6; }
	inline ExposedList_1_t3322895149 ** get_address_of_animations_6() { return &___animations_6; }
	inline void set_animations_6(ExposedList_1_t3322895149 * value)
	{
		___animations_6 = value;
		Il2CppCodeGenWriteBarrier((&___animations_6), value);
	}

	inline static int32_t get_offset_of_ikConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___ikConstraints_7)); }
	inline ExposedList_1_t3166231995 * get_ikConstraints_7() const { return ___ikConstraints_7; }
	inline ExposedList_1_t3166231995 ** get_address_of_ikConstraints_7() { return &___ikConstraints_7; }
	inline void set_ikConstraints_7(ExposedList_1_t3166231995 * value)
	{
		___ikConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_7), value);
	}

	inline static int32_t get_offset_of_transformConstraints_8() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___transformConstraints_8)); }
	inline ExposedList_1_t3236185212 * get_transformConstraints_8() const { return ___transformConstraints_8; }
	inline ExposedList_1_t3236185212 ** get_address_of_transformConstraints_8() { return &___transformConstraints_8; }
	inline void set_transformConstraints_8(ExposedList_1_t3236185212 * value)
	{
		___transformConstraints_8 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_8), value);
	}

	inline static int32_t get_offset_of_pathConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___pathConstraints_9)); }
	inline ExposedList_1_t3688408900 * get_pathConstraints_9() const { return ___pathConstraints_9; }
	inline ExposedList_1_t3688408900 ** get_address_of_pathConstraints_9() { return &___pathConstraints_9; }
	inline void set_pathConstraints_9(ExposedList_1_t3688408900 * value)
	{
		___pathConstraints_9 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_9), value);
	}

	inline static int32_t get_offset_of_width_10() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___width_10)); }
	inline float get_width_10() const { return ___width_10; }
	inline float* get_address_of_width_10() { return &___width_10; }
	inline void set_width_10(float value)
	{
		___width_10 = value;
	}

	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___height_11)); }
	inline float get_height_11() const { return ___height_11; }
	inline float* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(float value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_version_12() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___version_12)); }
	inline String_t* get_version_12() const { return ___version_12; }
	inline String_t** get_address_of_version_12() { return &___version_12; }
	inline void set_version_12(String_t* value)
	{
		___version_12 = value;
		Il2CppCodeGenWriteBarrier((&___version_12), value);
	}

	inline static int32_t get_offset_of_hash_13() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___hash_13)); }
	inline String_t* get_hash_13() const { return ___hash_13; }
	inline String_t** get_address_of_hash_13() { return &___hash_13; }
	inline void set_hash_13(String_t* value)
	{
		___hash_13 = value;
		Il2CppCodeGenWriteBarrier((&___hash_13), value);
	}

	inline static int32_t get_offset_of_fps_14() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___fps_14)); }
	inline float get_fps_14() const { return ___fps_14; }
	inline float* get_address_of_fps_14() { return &___fps_14; }
	inline void set_fps_14(float value)
	{
		___fps_14 = value;
	}

	inline static int32_t get_offset_of_imagesPath_15() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___imagesPath_15)); }
	inline String_t* get_imagesPath_15() const { return ___imagesPath_15; }
	inline String_t** get_address_of_imagesPath_15() { return &___imagesPath_15; }
	inline void set_imagesPath_15(String_t* value)
	{
		___imagesPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___imagesPath_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATA_T2032710716_H
#ifndef SKELETONJSON_T4049771867_H
#define SKELETONJSON_T4049771867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson
struct  SkeletonJson_t4049771867  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonJson::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_0;
	// Spine.AttachmentLoader Spine.SkeletonJson::attachmentLoader
	RuntimeObject* ___attachmentLoader_1;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonJson::linkedMeshes
	List_1_t3168994201 * ___linkedMeshes_2;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SkeletonJson_t4049771867, ___U3CScaleU3Ek__BackingField_0)); }
	inline float get_U3CScaleU3Ek__BackingField_0() const { return ___U3CScaleU3Ek__BackingField_0; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_0() { return &___U3CScaleU3Ek__BackingField_0; }
	inline void set_U3CScaleU3Ek__BackingField_0(float value)
	{
		___U3CScaleU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_1() { return static_cast<int32_t>(offsetof(SkeletonJson_t4049771867, ___attachmentLoader_1)); }
	inline RuntimeObject* get_attachmentLoader_1() const { return ___attachmentLoader_1; }
	inline RuntimeObject** get_address_of_attachmentLoader_1() { return &___attachmentLoader_1; }
	inline void set_attachmentLoader_1(RuntimeObject* value)
	{
		___attachmentLoader_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_1), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_2() { return static_cast<int32_t>(offsetof(SkeletonJson_t4049771867, ___linkedMeshes_2)); }
	inline List_1_t3168994201 * get_linkedMeshes_2() const { return ___linkedMeshes_2; }
	inline List_1_t3168994201 ** get_address_of_linkedMeshes_2() { return &___linkedMeshes_2; }
	inline void set_linkedMeshes_2(List_1_t3168994201 * value)
	{
		___linkedMeshes_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONJSON_T4049771867_H
#ifndef LINKEDMESH_T1696919459_H
#define LINKEDMESH_T1696919459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson/LinkedMesh
struct  LinkedMesh_t1696919459  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonJson/LinkedMesh::parent
	String_t* ___parent_0;
	// System.String Spine.SkeletonJson/LinkedMesh::skin
	String_t* ___skin_1;
	// System.Int32 Spine.SkeletonJson/LinkedMesh::slotIndex
	int32_t ___slotIndex_2;
	// Spine.MeshAttachment Spine.SkeletonJson/LinkedMesh::mesh
	MeshAttachment_t1975337962 * ___mesh_3;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___parent_0)); }
	inline String_t* get_parent_0() const { return ___parent_0; }
	inline String_t** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(String_t* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_skin_1() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___skin_1)); }
	inline String_t* get_skin_1() const { return ___skin_1; }
	inline String_t** get_address_of_skin_1() { return &___skin_1; }
	inline void set_skin_1(String_t* value)
	{
		___skin_1 = value;
		Il2CppCodeGenWriteBarrier((&___skin_1), value);
	}

	inline static int32_t get_offset_of_slotIndex_2() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___slotIndex_2)); }
	inline int32_t get_slotIndex_2() const { return ___slotIndex_2; }
	inline int32_t* get_address_of_slotIndex_2() { return &___slotIndex_2; }
	inline void set_slotIndex_2(int32_t value)
	{
		___slotIndex_2 = value;
	}

	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___mesh_3)); }
	inline MeshAttachment_t1975337962 * get_mesh_3() const { return ___mesh_3; }
	inline MeshAttachment_t1975337962 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(MeshAttachment_t1975337962 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDMESH_T1696919459_H
#ifndef VERTICES_T807797978_H
#define VERTICES_T807797978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary/Vertices
struct  Vertices_t807797978  : public RuntimeObject
{
public:
	// System.Int32[] Spine.SkeletonBinary/Vertices::bones
	Int32U5BU5D_t385246372* ___bones_0;
	// System.Single[] Spine.SkeletonBinary/Vertices::vertices
	SingleU5BU5D_t1444911251* ___vertices_1;

public:
	inline static int32_t get_offset_of_bones_0() { return static_cast<int32_t>(offsetof(Vertices_t807797978, ___bones_0)); }
	inline Int32U5BU5D_t385246372* get_bones_0() const { return ___bones_0; }
	inline Int32U5BU5D_t385246372** get_address_of_bones_0() { return &___bones_0; }
	inline void set_bones_0(Int32U5BU5D_t385246372* value)
	{
		___bones_0 = value;
		Il2CppCodeGenWriteBarrier((&___bones_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(Vertices_t807797978, ___vertices_1)); }
	inline SingleU5BU5D_t1444911251* get_vertices_1() const { return ___vertices_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(SingleU5BU5D_t1444911251* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICES_T807797978_H
#ifndef LEXER_T3897031801_H
#define LEXER_T3897031801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer
struct  Lexer_t3897031801  : public RuntimeObject
{
public:
	// System.Int32 SharpJson.Lexer::<lineNumber>k__BackingField
	int32_t ___U3ClineNumberU3Ek__BackingField_0;
	// System.Boolean SharpJson.Lexer::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// System.Char[] SharpJson.Lexer::json
	CharU5BU5D_t3528271667* ___json_2;
	// System.Int32 SharpJson.Lexer::index
	int32_t ___index_3;
	// System.Boolean SharpJson.Lexer::success
	bool ___success_4;
	// System.Char[] SharpJson.Lexer::stringBuffer
	CharU5BU5D_t3528271667* ___stringBuffer_5;

public:
	inline static int32_t get_offset_of_U3ClineNumberU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___U3ClineNumberU3Ek__BackingField_0)); }
	inline int32_t get_U3ClineNumberU3Ek__BackingField_0() const { return ___U3ClineNumberU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ClineNumberU3Ek__BackingField_0() { return &___U3ClineNumberU3Ek__BackingField_0; }
	inline void set_U3ClineNumberU3Ek__BackingField_0(int32_t value)
	{
		___U3ClineNumberU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___json_2)); }
	inline CharU5BU5D_t3528271667* get_json_2() const { return ___json_2; }
	inline CharU5BU5D_t3528271667** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(CharU5BU5D_t3528271667* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier((&___json_2), value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_stringBuffer_5() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___stringBuffer_5)); }
	inline CharU5BU5D_t3528271667* get_stringBuffer_5() const { return ___stringBuffer_5; }
	inline CharU5BU5D_t3528271667** get_address_of_stringBuffer_5() { return &___stringBuffer_5; }
	inline void set_stringBuffer_5(CharU5BU5D_t3528271667* value)
	{
		___stringBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T3897031801_H
#ifndef JSONDECODER_T2143190644_H
#define JSONDECODER_T2143190644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.JsonDecoder
struct  JsonDecoder_t2143190644  : public RuntimeObject
{
public:
	// System.String SharpJson.JsonDecoder::<errorMessage>k__BackingField
	String_t* ___U3CerrorMessageU3Ek__BackingField_0;
	// System.Boolean SharpJson.JsonDecoder::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// SharpJson.Lexer SharpJson.JsonDecoder::lexer
	Lexer_t3897031801 * ___lexer_2;

public:
	inline static int32_t get_offset_of_U3CerrorMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonDecoder_t2143190644, ___U3CerrorMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CerrorMessageU3Ek__BackingField_0() const { return ___U3CerrorMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CerrorMessageU3Ek__BackingField_0() { return &___U3CerrorMessageU3Ek__BackingField_0; }
	inline void set_U3CerrorMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CerrorMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonDecoder_t2143190644, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_lexer_2() { return static_cast<int32_t>(offsetof(JsonDecoder_t2143190644, ___lexer_2)); }
	inline Lexer_t3897031801 * get_lexer_2() const { return ___lexer_2; }
	inline Lexer_t3897031801 ** get_address_of_lexer_2() { return &___lexer_2; }
	inline void set_lexer_2(Lexer_t3897031801 * value)
	{
		___lexer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDECODER_T2143190644_H
#ifndef MATHUTILS_T3604673275_H
#define MATHUTILS_T3604673275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MathUtils
struct  MathUtils_t3604673275  : public RuntimeObject
{
public:

public:
};

struct MathUtils_t3604673275_StaticFields
{
public:
	// System.Single[] Spine.MathUtils::sin
	SingleU5BU5D_t1444911251* ___sin_11;

public:
	inline static int32_t get_offset_of_sin_11() { return static_cast<int32_t>(offsetof(MathUtils_t3604673275_StaticFields, ___sin_11)); }
	inline SingleU5BU5D_t1444911251* get_sin_11() const { return ___sin_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_sin_11() { return &___sin_11; }
	inline void set_sin_11(SingleU5BU5D_t1444911251* value)
	{
		___sin_11 = value;
		Il2CppCodeGenWriteBarrier((&___sin_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T3604673275_H
#ifndef PATHCONSTRAINT_T980854910_H
#define PATHCONSTRAINT_T980854910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraint
struct  PathConstraint_t980854910  : public RuntimeObject
{
public:
	// Spine.PathConstraintData Spine.PathConstraint::data
	PathConstraintData_t981297034 * ___data_4;
	// Spine.ExposedList`1<Spine.Bone> Spine.PathConstraint::bones
	ExposedList_1_t3793468194 * ___bones_5;
	// Spine.Slot Spine.PathConstraint::target
	Slot_t3514940700 * ___target_6;
	// System.Single Spine.PathConstraint::position
	float ___position_7;
	// System.Single Spine.PathConstraint::spacing
	float ___spacing_8;
	// System.Single Spine.PathConstraint::rotateMix
	float ___rotateMix_9;
	// System.Single Spine.PathConstraint::translateMix
	float ___translateMix_10;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::spaces
	ExposedList_1_t4104378640 * ___spaces_11;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::positions
	ExposedList_1_t4104378640 * ___positions_12;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::world
	ExposedList_1_t4104378640 * ___world_13;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::curves
	ExposedList_1_t4104378640 * ___curves_14;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::lengths
	ExposedList_1_t4104378640 * ___lengths_15;
	// System.Single[] Spine.PathConstraint::segments
	SingleU5BU5D_t1444911251* ___segments_16;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___data_4)); }
	inline PathConstraintData_t981297034 * get_data_4() const { return ___data_4; }
	inline PathConstraintData_t981297034 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(PathConstraintData_t981297034 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_bones_5() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___bones_5)); }
	inline ExposedList_1_t3793468194 * get_bones_5() const { return ___bones_5; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_5() { return &___bones_5; }
	inline void set_bones_5(ExposedList_1_t3793468194 * value)
	{
		___bones_5 = value;
		Il2CppCodeGenWriteBarrier((&___bones_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___target_6)); }
	inline Slot_t3514940700 * get_target_6() const { return ___target_6; }
	inline Slot_t3514940700 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Slot_t3514940700 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___position_7)); }
	inline float get_position_7() const { return ___position_7; }
	inline float* get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(float value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_spacing_8() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___spacing_8)); }
	inline float get_spacing_8() const { return ___spacing_8; }
	inline float* get_address_of_spacing_8() { return &___spacing_8; }
	inline void set_spacing_8(float value)
	{
		___spacing_8 = value;
	}

	inline static int32_t get_offset_of_rotateMix_9() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___rotateMix_9)); }
	inline float get_rotateMix_9() const { return ___rotateMix_9; }
	inline float* get_address_of_rotateMix_9() { return &___rotateMix_9; }
	inline void set_rotateMix_9(float value)
	{
		___rotateMix_9 = value;
	}

	inline static int32_t get_offset_of_translateMix_10() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___translateMix_10)); }
	inline float get_translateMix_10() const { return ___translateMix_10; }
	inline float* get_address_of_translateMix_10() { return &___translateMix_10; }
	inline void set_translateMix_10(float value)
	{
		___translateMix_10 = value;
	}

	inline static int32_t get_offset_of_spaces_11() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___spaces_11)); }
	inline ExposedList_1_t4104378640 * get_spaces_11() const { return ___spaces_11; }
	inline ExposedList_1_t4104378640 ** get_address_of_spaces_11() { return &___spaces_11; }
	inline void set_spaces_11(ExposedList_1_t4104378640 * value)
	{
		___spaces_11 = value;
		Il2CppCodeGenWriteBarrier((&___spaces_11), value);
	}

	inline static int32_t get_offset_of_positions_12() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___positions_12)); }
	inline ExposedList_1_t4104378640 * get_positions_12() const { return ___positions_12; }
	inline ExposedList_1_t4104378640 ** get_address_of_positions_12() { return &___positions_12; }
	inline void set_positions_12(ExposedList_1_t4104378640 * value)
	{
		___positions_12 = value;
		Il2CppCodeGenWriteBarrier((&___positions_12), value);
	}

	inline static int32_t get_offset_of_world_13() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___world_13)); }
	inline ExposedList_1_t4104378640 * get_world_13() const { return ___world_13; }
	inline ExposedList_1_t4104378640 ** get_address_of_world_13() { return &___world_13; }
	inline void set_world_13(ExposedList_1_t4104378640 * value)
	{
		___world_13 = value;
		Il2CppCodeGenWriteBarrier((&___world_13), value);
	}

	inline static int32_t get_offset_of_curves_14() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___curves_14)); }
	inline ExposedList_1_t4104378640 * get_curves_14() const { return ___curves_14; }
	inline ExposedList_1_t4104378640 ** get_address_of_curves_14() { return &___curves_14; }
	inline void set_curves_14(ExposedList_1_t4104378640 * value)
	{
		___curves_14 = value;
		Il2CppCodeGenWriteBarrier((&___curves_14), value);
	}

	inline static int32_t get_offset_of_lengths_15() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___lengths_15)); }
	inline ExposedList_1_t4104378640 * get_lengths_15() const { return ___lengths_15; }
	inline ExposedList_1_t4104378640 ** get_address_of_lengths_15() { return &___lengths_15; }
	inline void set_lengths_15(ExposedList_1_t4104378640 * value)
	{
		___lengths_15 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_15), value);
	}

	inline static int32_t get_offset_of_segments_16() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___segments_16)); }
	inline SingleU5BU5D_t1444911251* get_segments_16() const { return ___segments_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_segments_16() { return &___segments_16; }
	inline void set_segments_16(SingleU5BU5D_t1444911251* value)
	{
		___segments_16 = value;
		Il2CppCodeGenWriteBarrier((&___segments_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINT_T980854910_H
#ifndef SKELETON_T3686076450_H
#define SKELETON_T3686076450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skeleton
struct  Skeleton_t3686076450  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.Skeleton::data
	SkeletonData_t2032710716 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::bones
	ExposedList_1_t3793468194 * ___bones_1;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::slots
	ExposedList_1_t1927085270 * ___slots_2;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::drawOrder
	ExposedList_1_t1927085270 * ___drawOrder_3;
	// Spine.ExposedList`1<Spine.IkConstraint> Spine.Skeleton::ikConstraints
	ExposedList_1_t87334839 * ___ikConstraints_4;
	// Spine.ExposedList`1<Spine.TransformConstraint> Spine.Skeleton::transformConstraints
	ExposedList_1_t3161142095 * ___transformConstraints_5;
	// Spine.ExposedList`1<Spine.PathConstraint> Spine.Skeleton::pathConstraints
	ExposedList_1_t3687966776 * ___pathConstraints_6;
	// Spine.ExposedList`1<Spine.IUpdatable> Spine.Skeleton::updateCache
	ExposedList_1_t2549315650 * ___updateCache_7;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::updateCacheReset
	ExposedList_1_t3793468194 * ___updateCacheReset_8;
	// Spine.Skin Spine.Skeleton::skin
	Skin_t1174584606 * ___skin_9;
	// System.Single Spine.Skeleton::r
	float ___r_10;
	// System.Single Spine.Skeleton::g
	float ___g_11;
	// System.Single Spine.Skeleton::b
	float ___b_12;
	// System.Single Spine.Skeleton::a
	float ___a_13;
	// System.Single Spine.Skeleton::time
	float ___time_14;
	// System.Boolean Spine.Skeleton::flipX
	bool ___flipX_15;
	// System.Boolean Spine.Skeleton::flipY
	bool ___flipY_16;
	// System.Single Spine.Skeleton::x
	float ___x_17;
	// System.Single Spine.Skeleton::y
	float ___y_18;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___data_0)); }
	inline SkeletonData_t2032710716 * get_data_0() const { return ___data_0; }
	inline SkeletonData_t2032710716 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SkeletonData_t2032710716 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___bones_1)); }
	inline ExposedList_1_t3793468194 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3793468194 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___slots_2)); }
	inline ExposedList_1_t1927085270 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t1927085270 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t1927085270 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_drawOrder_3() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___drawOrder_3)); }
	inline ExposedList_1_t1927085270 * get_drawOrder_3() const { return ___drawOrder_3; }
	inline ExposedList_1_t1927085270 ** get_address_of_drawOrder_3() { return &___drawOrder_3; }
	inline void set_drawOrder_3(ExposedList_1_t1927085270 * value)
	{
		___drawOrder_3 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrder_3), value);
	}

	inline static int32_t get_offset_of_ikConstraints_4() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___ikConstraints_4)); }
	inline ExposedList_1_t87334839 * get_ikConstraints_4() const { return ___ikConstraints_4; }
	inline ExposedList_1_t87334839 ** get_address_of_ikConstraints_4() { return &___ikConstraints_4; }
	inline void set_ikConstraints_4(ExposedList_1_t87334839 * value)
	{
		___ikConstraints_4 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_4), value);
	}

	inline static int32_t get_offset_of_transformConstraints_5() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___transformConstraints_5)); }
	inline ExposedList_1_t3161142095 * get_transformConstraints_5() const { return ___transformConstraints_5; }
	inline ExposedList_1_t3161142095 ** get_address_of_transformConstraints_5() { return &___transformConstraints_5; }
	inline void set_transformConstraints_5(ExposedList_1_t3161142095 * value)
	{
		___transformConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_5), value);
	}

	inline static int32_t get_offset_of_pathConstraints_6() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___pathConstraints_6)); }
	inline ExposedList_1_t3687966776 * get_pathConstraints_6() const { return ___pathConstraints_6; }
	inline ExposedList_1_t3687966776 ** get_address_of_pathConstraints_6() { return &___pathConstraints_6; }
	inline void set_pathConstraints_6(ExposedList_1_t3687966776 * value)
	{
		___pathConstraints_6 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_6), value);
	}

	inline static int32_t get_offset_of_updateCache_7() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___updateCache_7)); }
	inline ExposedList_1_t2549315650 * get_updateCache_7() const { return ___updateCache_7; }
	inline ExposedList_1_t2549315650 ** get_address_of_updateCache_7() { return &___updateCache_7; }
	inline void set_updateCache_7(ExposedList_1_t2549315650 * value)
	{
		___updateCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___updateCache_7), value);
	}

	inline static int32_t get_offset_of_updateCacheReset_8() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___updateCacheReset_8)); }
	inline ExposedList_1_t3793468194 * get_updateCacheReset_8() const { return ___updateCacheReset_8; }
	inline ExposedList_1_t3793468194 ** get_address_of_updateCacheReset_8() { return &___updateCacheReset_8; }
	inline void set_updateCacheReset_8(ExposedList_1_t3793468194 * value)
	{
		___updateCacheReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___updateCacheReset_8), value);
	}

	inline static int32_t get_offset_of_skin_9() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___skin_9)); }
	inline Skin_t1174584606 * get_skin_9() const { return ___skin_9; }
	inline Skin_t1174584606 ** get_address_of_skin_9() { return &___skin_9; }
	inline void set_skin_9(Skin_t1174584606 * value)
	{
		___skin_9 = value;
		Il2CppCodeGenWriteBarrier((&___skin_9), value);
	}

	inline static int32_t get_offset_of_r_10() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___r_10)); }
	inline float get_r_10() const { return ___r_10; }
	inline float* get_address_of_r_10() { return &___r_10; }
	inline void set_r_10(float value)
	{
		___r_10 = value;
	}

	inline static int32_t get_offset_of_g_11() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___g_11)); }
	inline float get_g_11() const { return ___g_11; }
	inline float* get_address_of_g_11() { return &___g_11; }
	inline void set_g_11(float value)
	{
		___g_11 = value;
	}

	inline static int32_t get_offset_of_b_12() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___b_12)); }
	inline float get_b_12() const { return ___b_12; }
	inline float* get_address_of_b_12() { return &___b_12; }
	inline void set_b_12(float value)
	{
		___b_12 = value;
	}

	inline static int32_t get_offset_of_a_13() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___a_13)); }
	inline float get_a_13() const { return ___a_13; }
	inline float* get_address_of_a_13() { return &___a_13; }
	inline void set_a_13(float value)
	{
		___a_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_flipX_15() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___flipX_15)); }
	inline bool get_flipX_15() const { return ___flipX_15; }
	inline bool* get_address_of_flipX_15() { return &___flipX_15; }
	inline void set_flipX_15(bool value)
	{
		___flipX_15 = value;
	}

	inline static int32_t get_offset_of_flipY_16() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___flipY_16)); }
	inline bool get_flipY_16() const { return ___flipY_16; }
	inline bool* get_address_of_flipY_16() { return &___flipY_16; }
	inline void set_flipY_16(bool value)
	{
		___flipY_16 = value;
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___x_17)); }
	inline float get_x_17() const { return ___x_17; }
	inline float* get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(float value)
	{
		___x_17 = value;
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___y_18)); }
	inline float get_y_18() const { return ___y_18; }
	inline float* get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(float value)
	{
		___y_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETON_T3686076450_H
#ifndef SKELETONBINARY_T1796686580_H
#define SKELETONBINARY_T1796686580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary
struct  SkeletonBinary_t1796686580  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonBinary::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_13;
	// Spine.AttachmentLoader Spine.SkeletonBinary::attachmentLoader
	RuntimeObject* ___attachmentLoader_14;
	// System.Byte[] Spine.SkeletonBinary::buffer
	ByteU5BU5D_t4116647657* ___buffer_15;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonBinary::linkedMeshes
	List_1_t3168994201 * ___linkedMeshes_16;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___U3CScaleU3Ek__BackingField_13)); }
	inline float get_U3CScaleU3Ek__BackingField_13() const { return ___U3CScaleU3Ek__BackingField_13; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_13() { return &___U3CScaleU3Ek__BackingField_13; }
	inline void set_U3CScaleU3Ek__BackingField_13(float value)
	{
		___U3CScaleU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_14() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___attachmentLoader_14)); }
	inline RuntimeObject* get_attachmentLoader_14() const { return ___attachmentLoader_14; }
	inline RuntimeObject** get_address_of_attachmentLoader_14() { return &___attachmentLoader_14; }
	inline void set_attachmentLoader_14(RuntimeObject* value)
	{
		___attachmentLoader_14 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_14), value);
	}

	inline static int32_t get_offset_of_buffer_15() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___buffer_15)); }
	inline ByteU5BU5D_t4116647657* get_buffer_15() const { return ___buffer_15; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_15() { return &___buffer_15; }
	inline void set_buffer_15(ByteU5BU5D_t4116647657* value)
	{
		___buffer_15 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_15), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_16() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___linkedMeshes_16)); }
	inline List_1_t3168994201 * get_linkedMeshes_16() const { return ___linkedMeshes_16; }
	inline List_1_t3168994201 ** get_address_of_linkedMeshes_16() { return &___linkedMeshes_16; }
	inline void set_linkedMeshes_16(List_1_t3168994201 * value)
	{
		___linkedMeshes_16 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_16), value);
	}
};

struct SkeletonBinary_t1796686580_StaticFields
{
public:
	// Spine.TransformMode[] Spine.SkeletonBinary::TransformModeValues
	TransformModeU5BU5D_t3210249249* ___TransformModeValues_17;

public:
	inline static int32_t get_offset_of_TransformModeValues_17() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580_StaticFields, ___TransformModeValues_17)); }
	inline TransformModeU5BU5D_t3210249249* get_TransformModeValues_17() const { return ___TransformModeValues_17; }
	inline TransformModeU5BU5D_t3210249249** get_address_of_TransformModeValues_17() { return &___TransformModeValues_17; }
	inline void set_TransformModeValues_17(TransformModeU5BU5D_t3210249249* value)
	{
		___TransformModeValues_17 = value;
		Il2CppCodeGenWriteBarrier((&___TransformModeValues_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBINARY_T1796686580_H
#ifndef SKIN_T1174584606_H
#define SKIN_T1174584606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin
struct  Skin_t1174584606  : public RuntimeObject
{
public:
	// System.String Spine.Skin::name
	String_t* ___name_0;
	// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment> Spine.Skin::attachments
	Dictionary_2_t968660385 * ___attachments_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Skin_t1174584606, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_attachments_1() { return static_cast<int32_t>(offsetof(Skin_t1174584606, ___attachments_1)); }
	inline Dictionary_2_t968660385 * get_attachments_1() const { return ___attachments_1; }
	inline Dictionary_2_t968660385 ** get_address_of_attachments_1() { return &___attachments_1; }
	inline void set_attachments_1(Dictionary_2_t968660385 * value)
	{
		___attachments_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_T1174584606_H
#ifndef SMARTMESH_T524811292_H
#define SMARTMESH_T524811292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers/SmartMesh
struct  SmartMesh_t524811292  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Spine.Unity.MeshRendererBuffers/SmartMesh::mesh
	Mesh_t3648964284 * ___mesh_0;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.MeshRendererBuffers/SmartMesh::instructionUsed
	SkeletonRendererInstruction_t651787775 * ___instructionUsed_1;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(SmartMesh_t524811292, ___mesh_0)); }
	inline Mesh_t3648964284 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t3648964284 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t3648964284 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_instructionUsed_1() { return static_cast<int32_t>(offsetof(SmartMesh_t524811292, ___instructionUsed_1)); }
	inline SkeletonRendererInstruction_t651787775 * get_instructionUsed_1() const { return ___instructionUsed_1; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_instructionUsed_1() { return &___instructionUsed_1; }
	inline void set_instructionUsed_1(SkeletonRendererInstruction_t651787775 * value)
	{
		___instructionUsed_1 = value;
		Il2CppCodeGenWriteBarrier((&___instructionUsed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTMESH_T524811292_H
#ifndef SKELETONRENDERERINSTRUCTION_T651787775_H
#define SKELETONRENDERERINSTRUCTION_T651787775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRendererInstruction
struct  SkeletonRendererInstruction_t651787775  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::immutableTriangles
	bool ___immutableTriangles_0;
	// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction> Spine.Unity.SkeletonRendererInstruction::submeshInstructions
	ExposedList_1_t2759233236 * ___submeshInstructions_1;
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::hasActiveClipping
	bool ___hasActiveClipping_2;
	// System.Int32 Spine.Unity.SkeletonRendererInstruction::rawVertexCount
	int32_t ___rawVertexCount_3;
	// Spine.ExposedList`1<Spine.Attachment> Spine.Unity.SkeletonRendererInstruction::attachments
	ExposedList_1_t1455901122 * ___attachments_4;

public:
	inline static int32_t get_offset_of_immutableTriangles_0() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___immutableTriangles_0)); }
	inline bool get_immutableTriangles_0() const { return ___immutableTriangles_0; }
	inline bool* get_address_of_immutableTriangles_0() { return &___immutableTriangles_0; }
	inline void set_immutableTriangles_0(bool value)
	{
		___immutableTriangles_0 = value;
	}

	inline static int32_t get_offset_of_submeshInstructions_1() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___submeshInstructions_1)); }
	inline ExposedList_1_t2759233236 * get_submeshInstructions_1() const { return ___submeshInstructions_1; }
	inline ExposedList_1_t2759233236 ** get_address_of_submeshInstructions_1() { return &___submeshInstructions_1; }
	inline void set_submeshInstructions_1(ExposedList_1_t2759233236 * value)
	{
		___submeshInstructions_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshInstructions_1), value);
	}

	inline static int32_t get_offset_of_hasActiveClipping_2() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___hasActiveClipping_2)); }
	inline bool get_hasActiveClipping_2() const { return ___hasActiveClipping_2; }
	inline bool* get_address_of_hasActiveClipping_2() { return &___hasActiveClipping_2; }
	inline void set_hasActiveClipping_2(bool value)
	{
		___hasActiveClipping_2 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_3() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___rawVertexCount_3)); }
	inline int32_t get_rawVertexCount_3() const { return ___rawVertexCount_3; }
	inline int32_t* get_address_of_rawVertexCount_3() { return &___rawVertexCount_3; }
	inline void set_rawVertexCount_3(int32_t value)
	{
		___rawVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_attachments_4() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___attachments_4)); }
	inline ExposedList_1_t1455901122 * get_attachments_4() const { return ___attachments_4; }
	inline ExposedList_1_t1455901122 ** get_address_of_attachments_4() { return &___attachments_4; }
	inline void set_attachments_4(ExposedList_1_t1455901122 * value)
	{
		___attachments_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERINSTRUCTION_T651787775_H
#ifndef ATTACHMENTREGIONEXTENSIONS_T1591891965_H
#define ATTACHMENTREGIONEXTENSIONS_T1591891965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentRegionExtensions
struct  AttachmentRegionExtensions_t1591891965  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTREGIONEXTENSIONS_T1591891965_H
#ifndef SKINUTILITIES_T498157063_H
#define SKINUTILITIES_T498157063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.SkinUtilities
struct  SkinUtilities_t498157063  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINUTILITIES_T498157063_H
#ifndef ATTACHMENTCLONEEXTENSIONS_T460514876_H
#define ATTACHMENTCLONEEXTENSIONS_T460514876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentCloneExtensions
struct  AttachmentCloneExtensions_t460514876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTCLONEEXTENSIONS_T460514876_H
#ifndef U3CSTARTU3EC__ITERATOR0_T294813647_H
#define U3CSTARTU3EC__ITERATOR0_T294813647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t294813647  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$this
	SkeletonRagdoll_t480923817 * ___U24this_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24this_0)); }
	inline SkeletonRagdoll_t480923817 * get_U24this_0() const { return ___U24this_0; }
	inline SkeletonRagdoll_t480923817 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkeletonRagdoll_t480923817 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T294813647_H
#ifndef MESHRENDERERBUFFERS_T756429994_H
#define MESHRENDERERBUFFERS_T756429994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers
struct  MeshRendererBuffers_t756429994  : public RuntimeObject
{
public:
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.MeshRendererBuffers::doubleBufferedMesh
	DoubleBuffered_1_t2489386064 * ___doubleBufferedMesh_0;
	// Spine.ExposedList`1<UnityEngine.Material> Spine.Unity.MeshRendererBuffers::submeshMaterials
	ExposedList_1_t3047486989 * ___submeshMaterials_1;
	// UnityEngine.Material[] Spine.Unity.MeshRendererBuffers::sharedMaterials
	MaterialU5BU5D_t561872642* ___sharedMaterials_2;

public:
	inline static int32_t get_offset_of_doubleBufferedMesh_0() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t756429994, ___doubleBufferedMesh_0)); }
	inline DoubleBuffered_1_t2489386064 * get_doubleBufferedMesh_0() const { return ___doubleBufferedMesh_0; }
	inline DoubleBuffered_1_t2489386064 ** get_address_of_doubleBufferedMesh_0() { return &___doubleBufferedMesh_0; }
	inline void set_doubleBufferedMesh_0(DoubleBuffered_1_t2489386064 * value)
	{
		___doubleBufferedMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___doubleBufferedMesh_0), value);
	}

	inline static int32_t get_offset_of_submeshMaterials_1() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t756429994, ___submeshMaterials_1)); }
	inline ExposedList_1_t3047486989 * get_submeshMaterials_1() const { return ___submeshMaterials_1; }
	inline ExposedList_1_t3047486989 ** get_address_of_submeshMaterials_1() { return &___submeshMaterials_1; }
	inline void set_submeshMaterials_1(ExposedList_1_t3047486989 * value)
	{
		___submeshMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshMaterials_1), value);
	}

	inline static int32_t get_offset_of_sharedMaterials_2() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t756429994, ___sharedMaterials_2)); }
	inline MaterialU5BU5D_t561872642* get_sharedMaterials_2() const { return ___sharedMaterials_2; }
	inline MaterialU5BU5D_t561872642** get_address_of_sharedMaterials_2() { return &___sharedMaterials_2; }
	inline void set_sharedMaterials_2(MaterialU5BU5D_t561872642* value)
	{
		___sharedMaterials_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterials_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERERBUFFERS_T756429994_H
#ifndef ATTACHMENTKEYTUPLECOMPARER_T1167996044_H
#define ATTACHMENTKEYTUPLECOMPARER_T1167996044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTupleComparer
struct  AttachmentKeyTupleComparer_t1167996044  : public RuntimeObject
{
public:

public:
};

struct AttachmentKeyTupleComparer_t1167996044_StaticFields
{
public:
	// Spine.Skin/AttachmentKeyTupleComparer Spine.Skin/AttachmentKeyTupleComparer::Instance
	AttachmentKeyTupleComparer_t1167996044 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTupleComparer_t1167996044_StaticFields, ___Instance_0)); }
	inline AttachmentKeyTupleComparer_t1167996044 * get_Instance_0() const { return ___Instance_0; }
	inline AttachmentKeyTupleComparer_t1167996044 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AttachmentKeyTupleComparer_t1167996044 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTKEYTUPLECOMPARER_T1167996044_H
#ifndef SLOT_T3514940700_H
#define SLOT_T3514940700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Slot
struct  Slot_t3514940700  : public RuntimeObject
{
public:
	// Spine.SlotData Spine.Slot::data
	SlotData_t154801902 * ___data_0;
	// Spine.Bone Spine.Slot::bone
	Bone_t1086356328 * ___bone_1;
	// System.Single Spine.Slot::r
	float ___r_2;
	// System.Single Spine.Slot::g
	float ___g_3;
	// System.Single Spine.Slot::b
	float ___b_4;
	// System.Single Spine.Slot::a
	float ___a_5;
	// System.Single Spine.Slot::r2
	float ___r2_6;
	// System.Single Spine.Slot::g2
	float ___g2_7;
	// System.Single Spine.Slot::b2
	float ___b2_8;
	// System.Boolean Spine.Slot::hasSecondColor
	bool ___hasSecondColor_9;
	// Spine.Attachment Spine.Slot::attachment
	Attachment_t3043756552 * ___attachment_10;
	// System.Single Spine.Slot::attachmentTime
	float ___attachmentTime_11;
	// Spine.ExposedList`1<System.Single> Spine.Slot::attachmentVertices
	ExposedList_1_t4104378640 * ___attachmentVertices_12;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___data_0)); }
	inline SlotData_t154801902 * get_data_0() const { return ___data_0; }
	inline SlotData_t154801902 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SlotData_t154801902 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bone_1() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___bone_1)); }
	inline Bone_t1086356328 * get_bone_1() const { return ___bone_1; }
	inline Bone_t1086356328 ** get_address_of_bone_1() { return &___bone_1; }
	inline void set_bone_1(Bone_t1086356328 * value)
	{
		___bone_1 = value;
		Il2CppCodeGenWriteBarrier((&___bone_1), value);
	}

	inline static int32_t get_offset_of_r_2() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___r_2)); }
	inline float get_r_2() const { return ___r_2; }
	inline float* get_address_of_r_2() { return &___r_2; }
	inline void set_r_2(float value)
	{
		___r_2 = value;
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___g_3)); }
	inline float get_g_3() const { return ___g_3; }
	inline float* get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(float value)
	{
		___g_3 = value;
	}

	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___b_4)); }
	inline float get_b_4() const { return ___b_4; }
	inline float* get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(float value)
	{
		___b_4 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_r2_6() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___r2_6)); }
	inline float get_r2_6() const { return ___r2_6; }
	inline float* get_address_of_r2_6() { return &___r2_6; }
	inline void set_r2_6(float value)
	{
		___r2_6 = value;
	}

	inline static int32_t get_offset_of_g2_7() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___g2_7)); }
	inline float get_g2_7() const { return ___g2_7; }
	inline float* get_address_of_g2_7() { return &___g2_7; }
	inline void set_g2_7(float value)
	{
		___g2_7 = value;
	}

	inline static int32_t get_offset_of_b2_8() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___b2_8)); }
	inline float get_b2_8() const { return ___b2_8; }
	inline float* get_address_of_b2_8() { return &___b2_8; }
	inline void set_b2_8(float value)
	{
		___b2_8 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_9() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___hasSecondColor_9)); }
	inline bool get_hasSecondColor_9() const { return ___hasSecondColor_9; }
	inline bool* get_address_of_hasSecondColor_9() { return &___hasSecondColor_9; }
	inline void set_hasSecondColor_9(bool value)
	{
		___hasSecondColor_9 = value;
	}

	inline static int32_t get_offset_of_attachment_10() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___attachment_10)); }
	inline Attachment_t3043756552 * get_attachment_10() const { return ___attachment_10; }
	inline Attachment_t3043756552 ** get_address_of_attachment_10() { return &___attachment_10; }
	inline void set_attachment_10(Attachment_t3043756552 * value)
	{
		___attachment_10 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_10), value);
	}

	inline static int32_t get_offset_of_attachmentTime_11() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___attachmentTime_11)); }
	inline float get_attachmentTime_11() const { return ___attachmentTime_11; }
	inline float* get_address_of_attachmentTime_11() { return &___attachmentTime_11; }
	inline void set_attachmentTime_11(float value)
	{
		___attachmentTime_11 = value;
	}

	inline static int32_t get_offset_of_attachmentVertices_12() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___attachmentVertices_12)); }
	inline ExposedList_1_t4104378640 * get_attachmentVertices_12() const { return ___attachmentVertices_12; }
	inline ExposedList_1_t4104378640 ** get_address_of_attachmentVertices_12() { return &___attachmentVertices_12; }
	inline void set_attachmentVertices_12(ExposedList_1_t4104378640 * value)
	{
		___attachmentVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentVertices_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3514940700_H
#ifndef TRANSFORMCONSTRAINT_T454030229_H
#define TRANSFORMCONSTRAINT_T454030229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraint
struct  TransformConstraint_t454030229  : public RuntimeObject
{
public:
	// Spine.TransformConstraintData Spine.TransformConstraint::data
	TransformConstraintData_t529073346 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.TransformConstraint::bones
	ExposedList_1_t3793468194 * ___bones_1;
	// Spine.Bone Spine.TransformConstraint::target
	Bone_t1086356328 * ___target_2;
	// System.Single Spine.TransformConstraint::rotateMix
	float ___rotateMix_3;
	// System.Single Spine.TransformConstraint::translateMix
	float ___translateMix_4;
	// System.Single Spine.TransformConstraint::scaleMix
	float ___scaleMix_5;
	// System.Single Spine.TransformConstraint::shearMix
	float ___shearMix_6;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___data_0)); }
	inline TransformConstraintData_t529073346 * get_data_0() const { return ___data_0; }
	inline TransformConstraintData_t529073346 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(TransformConstraintData_t529073346 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___bones_1)); }
	inline ExposedList_1_t3793468194 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3793468194 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___target_2)); }
	inline Bone_t1086356328 * get_target_2() const { return ___target_2; }
	inline Bone_t1086356328 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t1086356328 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_rotateMix_3() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___rotateMix_3)); }
	inline float get_rotateMix_3() const { return ___rotateMix_3; }
	inline float* get_address_of_rotateMix_3() { return &___rotateMix_3; }
	inline void set_rotateMix_3(float value)
	{
		___rotateMix_3 = value;
	}

	inline static int32_t get_offset_of_translateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___translateMix_4)); }
	inline float get_translateMix_4() const { return ___translateMix_4; }
	inline float* get_address_of_translateMix_4() { return &___translateMix_4; }
	inline void set_translateMix_4(float value)
	{
		___translateMix_4 = value;
	}

	inline static int32_t get_offset_of_scaleMix_5() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___scaleMix_5)); }
	inline float get_scaleMix_5() const { return ___scaleMix_5; }
	inline float* get_address_of_scaleMix_5() { return &___scaleMix_5; }
	inline void set_scaleMix_5(float value)
	{
		___scaleMix_5 = value;
	}

	inline static int32_t get_offset_of_shearMix_6() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___shearMix_6)); }
	inline float get_shearMix_6() const { return ___shearMix_6; }
	inline float* get_address_of_shearMix_6() { return &___shearMix_6; }
	inline void set_shearMix_6(float value)
	{
		___shearMix_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINT_T454030229_H
#ifndef TRANSFORMCONSTRAINTDATA_T529073346_H
#define TRANSFORMCONSTRAINTDATA_T529073346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintData
struct  TransformConstraintData_t529073346  : public RuntimeObject
{
public:
	// System.String Spine.TransformConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.TransformConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.TransformConstraintData::bones
	ExposedList_1_t1542319060 * ___bones_2;
	// Spine.BoneData Spine.TransformConstraintData::target
	BoneData_t3130174490 * ___target_3;
	// System.Single Spine.TransformConstraintData::rotateMix
	float ___rotateMix_4;
	// System.Single Spine.TransformConstraintData::translateMix
	float ___translateMix_5;
	// System.Single Spine.TransformConstraintData::scaleMix
	float ___scaleMix_6;
	// System.Single Spine.TransformConstraintData::shearMix
	float ___shearMix_7;
	// System.Single Spine.TransformConstraintData::offsetRotation
	float ___offsetRotation_8;
	// System.Single Spine.TransformConstraintData::offsetX
	float ___offsetX_9;
	// System.Single Spine.TransformConstraintData::offsetY
	float ___offsetY_10;
	// System.Single Spine.TransformConstraintData::offsetScaleX
	float ___offsetScaleX_11;
	// System.Single Spine.TransformConstraintData::offsetScaleY
	float ___offsetScaleY_12;
	// System.Single Spine.TransformConstraintData::offsetShearY
	float ___offsetShearY_13;
	// System.Boolean Spine.TransformConstraintData::relative
	bool ___relative_14;
	// System.Boolean Spine.TransformConstraintData::local
	bool ___local_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___bones_2)); }
	inline ExposedList_1_t1542319060 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t1542319060 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t1542319060 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___target_3)); }
	inline BoneData_t3130174490 * get_target_3() const { return ___target_3; }
	inline BoneData_t3130174490 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t3130174490 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_rotateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___rotateMix_4)); }
	inline float get_rotateMix_4() const { return ___rotateMix_4; }
	inline float* get_address_of_rotateMix_4() { return &___rotateMix_4; }
	inline void set_rotateMix_4(float value)
	{
		___rotateMix_4 = value;
	}

	inline static int32_t get_offset_of_translateMix_5() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___translateMix_5)); }
	inline float get_translateMix_5() const { return ___translateMix_5; }
	inline float* get_address_of_translateMix_5() { return &___translateMix_5; }
	inline void set_translateMix_5(float value)
	{
		___translateMix_5 = value;
	}

	inline static int32_t get_offset_of_scaleMix_6() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___scaleMix_6)); }
	inline float get_scaleMix_6() const { return ___scaleMix_6; }
	inline float* get_address_of_scaleMix_6() { return &___scaleMix_6; }
	inline void set_scaleMix_6(float value)
	{
		___scaleMix_6 = value;
	}

	inline static int32_t get_offset_of_shearMix_7() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___shearMix_7)); }
	inline float get_shearMix_7() const { return ___shearMix_7; }
	inline float* get_address_of_shearMix_7() { return &___shearMix_7; }
	inline void set_shearMix_7(float value)
	{
		___shearMix_7 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_8() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetRotation_8)); }
	inline float get_offsetRotation_8() const { return ___offsetRotation_8; }
	inline float* get_address_of_offsetRotation_8() { return &___offsetRotation_8; }
	inline void set_offsetRotation_8(float value)
	{
		___offsetRotation_8 = value;
	}

	inline static int32_t get_offset_of_offsetX_9() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetX_9)); }
	inline float get_offsetX_9() const { return ___offsetX_9; }
	inline float* get_address_of_offsetX_9() { return &___offsetX_9; }
	inline void set_offsetX_9(float value)
	{
		___offsetX_9 = value;
	}

	inline static int32_t get_offset_of_offsetY_10() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetY_10)); }
	inline float get_offsetY_10() const { return ___offsetY_10; }
	inline float* get_address_of_offsetY_10() { return &___offsetY_10; }
	inline void set_offsetY_10(float value)
	{
		___offsetY_10 = value;
	}

	inline static int32_t get_offset_of_offsetScaleX_11() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetScaleX_11)); }
	inline float get_offsetScaleX_11() const { return ___offsetScaleX_11; }
	inline float* get_address_of_offsetScaleX_11() { return &___offsetScaleX_11; }
	inline void set_offsetScaleX_11(float value)
	{
		___offsetScaleX_11 = value;
	}

	inline static int32_t get_offset_of_offsetScaleY_12() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetScaleY_12)); }
	inline float get_offsetScaleY_12() const { return ___offsetScaleY_12; }
	inline float* get_address_of_offsetScaleY_12() { return &___offsetScaleY_12; }
	inline void set_offsetScaleY_12(float value)
	{
		___offsetScaleY_12 = value;
	}

	inline static int32_t get_offset_of_offsetShearY_13() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetShearY_13)); }
	inline float get_offsetShearY_13() const { return ___offsetShearY_13; }
	inline float* get_address_of_offsetShearY_13() { return &___offsetShearY_13; }
	inline void set_offsetShearY_13(float value)
	{
		___offsetShearY_13 = value;
	}

	inline static int32_t get_offset_of_relative_14() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___relative_14)); }
	inline bool get_relative_14() const { return ___relative_14; }
	inline bool* get_address_of_relative_14() { return &___relative_14; }
	inline void set_relative_14(bool value)
	{
		___relative_14 = value;
	}

	inline static int32_t get_offset_of_local_15() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___local_15)); }
	inline bool get_local_15() const { return ___local_15; }
	inline bool* get_address_of_local_15() { return &___local_15; }
	inline void set_local_15(bool value)
	{
		___local_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTDATA_T529073346_H
#ifndef TRIANGULATOR_T2502879214_H
#define TRIANGULATOR_T2502879214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Triangulator
struct  Triangulator_t2502879214  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::convexPolygons
	ExposedList_1_t2516523210 * ___convexPolygons_0;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::convexPolygonsIndices
	ExposedList_1_t4070202189 * ___convexPolygonsIndices_1;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::indicesArray
	ExposedList_1_t1363090323 * ___indicesArray_2;
	// Spine.ExposedList`1<System.Boolean> Spine.Triangulator::isConcaveArray
	ExposedList_1_t2804399831 * ___isConcaveArray_3;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::triangles
	ExposedList_1_t1363090323 * ___triangles_4;
	// Spine.Pool`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::polygonPool
	Pool_1_t3075278099 * ___polygonPool_5;
	// Spine.Pool`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::polygonIndicesPool
	Pool_1_t333989782 * ___polygonIndicesPool_6;

public:
	inline static int32_t get_offset_of_convexPolygons_0() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___convexPolygons_0)); }
	inline ExposedList_1_t2516523210 * get_convexPolygons_0() const { return ___convexPolygons_0; }
	inline ExposedList_1_t2516523210 ** get_address_of_convexPolygons_0() { return &___convexPolygons_0; }
	inline void set_convexPolygons_0(ExposedList_1_t2516523210 * value)
	{
		___convexPolygons_0 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygons_0), value);
	}

	inline static int32_t get_offset_of_convexPolygonsIndices_1() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___convexPolygonsIndices_1)); }
	inline ExposedList_1_t4070202189 * get_convexPolygonsIndices_1() const { return ___convexPolygonsIndices_1; }
	inline ExposedList_1_t4070202189 ** get_address_of_convexPolygonsIndices_1() { return &___convexPolygonsIndices_1; }
	inline void set_convexPolygonsIndices_1(ExposedList_1_t4070202189 * value)
	{
		___convexPolygonsIndices_1 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygonsIndices_1), value);
	}

	inline static int32_t get_offset_of_indicesArray_2() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___indicesArray_2)); }
	inline ExposedList_1_t1363090323 * get_indicesArray_2() const { return ___indicesArray_2; }
	inline ExposedList_1_t1363090323 ** get_address_of_indicesArray_2() { return &___indicesArray_2; }
	inline void set_indicesArray_2(ExposedList_1_t1363090323 * value)
	{
		___indicesArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___indicesArray_2), value);
	}

	inline static int32_t get_offset_of_isConcaveArray_3() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___isConcaveArray_3)); }
	inline ExposedList_1_t2804399831 * get_isConcaveArray_3() const { return ___isConcaveArray_3; }
	inline ExposedList_1_t2804399831 ** get_address_of_isConcaveArray_3() { return &___isConcaveArray_3; }
	inline void set_isConcaveArray_3(ExposedList_1_t2804399831 * value)
	{
		___isConcaveArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___isConcaveArray_3), value);
	}

	inline static int32_t get_offset_of_triangles_4() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___triangles_4)); }
	inline ExposedList_1_t1363090323 * get_triangles_4() const { return ___triangles_4; }
	inline ExposedList_1_t1363090323 ** get_address_of_triangles_4() { return &___triangles_4; }
	inline void set_triangles_4(ExposedList_1_t1363090323 * value)
	{
		___triangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_4), value);
	}

	inline static int32_t get_offset_of_polygonPool_5() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___polygonPool_5)); }
	inline Pool_1_t3075278099 * get_polygonPool_5() const { return ___polygonPool_5; }
	inline Pool_1_t3075278099 ** get_address_of_polygonPool_5() { return &___polygonPool_5; }
	inline void set_polygonPool_5(Pool_1_t3075278099 * value)
	{
		___polygonPool_5 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_5), value);
	}

	inline static int32_t get_offset_of_polygonIndicesPool_6() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___polygonIndicesPool_6)); }
	inline Pool_1_t333989782 * get_polygonIndicesPool_6() const { return ___polygonIndicesPool_6; }
	inline Pool_1_t333989782 ** get_address_of_polygonIndicesPool_6() { return &___polygonIndicesPool_6; }
	inline void set_polygonIndicesPool_6(Pool_1_t333989782 * value)
	{
		___polygonIndicesPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___polygonIndicesPool_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATOR_T2502879214_H
#ifndef MATERIALSTEXTURELOADER_T1402074808_H
#define MATERIALSTEXTURELOADER_T1402074808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MaterialsTextureLoader
struct  MaterialsTextureLoader_t1402074808  : public RuntimeObject
{
public:
	// Spine.Unity.AtlasAsset Spine.Unity.MaterialsTextureLoader::atlasAsset
	AtlasAsset_t1167231206 * ___atlasAsset_0;

public:
	inline static int32_t get_offset_of_atlasAsset_0() { return static_cast<int32_t>(offsetof(MaterialsTextureLoader_t1402074808, ___atlasAsset_0)); }
	inline AtlasAsset_t1167231206 * get_atlasAsset_0() const { return ___atlasAsset_0; }
	inline AtlasAsset_t1167231206 ** get_address_of_atlasAsset_0() { return &___atlasAsset_0; }
	inline void set_atlasAsset_0(AtlasAsset_t1167231206 * value)
	{
		___atlasAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAsset_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSTEXTURELOADER_T1402074808_H
#ifndef JSON_T1494079119_H
#define JSON_T1494079119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Json
struct  Json_t1494079119  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T1494079119_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef BONE_T1086356328_H
#define BONE_T1086356328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Bone
struct  Bone_t1086356328  : public RuntimeObject
{
public:
	// Spine.BoneData Spine.Bone::data
	BoneData_t3130174490 * ___data_1;
	// Spine.Skeleton Spine.Bone::skeleton
	Skeleton_t3686076450 * ___skeleton_2;
	// Spine.Bone Spine.Bone::parent
	Bone_t1086356328 * ___parent_3;
	// Spine.ExposedList`1<Spine.Bone> Spine.Bone::children
	ExposedList_1_t3793468194 * ___children_4;
	// System.Single Spine.Bone::x
	float ___x_5;
	// System.Single Spine.Bone::y
	float ___y_6;
	// System.Single Spine.Bone::rotation
	float ___rotation_7;
	// System.Single Spine.Bone::scaleX
	float ___scaleX_8;
	// System.Single Spine.Bone::scaleY
	float ___scaleY_9;
	// System.Single Spine.Bone::shearX
	float ___shearX_10;
	// System.Single Spine.Bone::shearY
	float ___shearY_11;
	// System.Single Spine.Bone::ax
	float ___ax_12;
	// System.Single Spine.Bone::ay
	float ___ay_13;
	// System.Single Spine.Bone::arotation
	float ___arotation_14;
	// System.Single Spine.Bone::ascaleX
	float ___ascaleX_15;
	// System.Single Spine.Bone::ascaleY
	float ___ascaleY_16;
	// System.Single Spine.Bone::ashearX
	float ___ashearX_17;
	// System.Single Spine.Bone::ashearY
	float ___ashearY_18;
	// System.Boolean Spine.Bone::appliedValid
	bool ___appliedValid_19;
	// System.Single Spine.Bone::a
	float ___a_20;
	// System.Single Spine.Bone::b
	float ___b_21;
	// System.Single Spine.Bone::worldX
	float ___worldX_22;
	// System.Single Spine.Bone::c
	float ___c_23;
	// System.Single Spine.Bone::d
	float ___d_24;
	// System.Single Spine.Bone::worldY
	float ___worldY_25;
	// System.Boolean Spine.Bone::sorted
	bool ___sorted_26;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___data_1)); }
	inline BoneData_t3130174490 * get_data_1() const { return ___data_1; }
	inline BoneData_t3130174490 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(BoneData_t3130174490 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_skeleton_2() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___skeleton_2)); }
	inline Skeleton_t3686076450 * get_skeleton_2() const { return ___skeleton_2; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_2() { return &___skeleton_2; }
	inline void set_skeleton_2(Skeleton_t3686076450 * value)
	{
		___skeleton_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___parent_3)); }
	inline Bone_t1086356328 * get_parent_3() const { return ___parent_3; }
	inline Bone_t1086356328 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Bone_t1086356328 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___children_4)); }
	inline ExposedList_1_t3793468194 * get_children_4() const { return ___children_4; }
	inline ExposedList_1_t3793468194 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(ExposedList_1_t3793468194 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_4), value);
	}

	inline static int32_t get_offset_of_x_5() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___x_5)); }
	inline float get_x_5() const { return ___x_5; }
	inline float* get_address_of_x_5() { return &___x_5; }
	inline void set_x_5(float value)
	{
		___x_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_scaleX_8() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___scaleX_8)); }
	inline float get_scaleX_8() const { return ___scaleX_8; }
	inline float* get_address_of_scaleX_8() { return &___scaleX_8; }
	inline void set_scaleX_8(float value)
	{
		___scaleX_8 = value;
	}

	inline static int32_t get_offset_of_scaleY_9() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___scaleY_9)); }
	inline float get_scaleY_9() const { return ___scaleY_9; }
	inline float* get_address_of_scaleY_9() { return &___scaleY_9; }
	inline void set_scaleY_9(float value)
	{
		___scaleY_9 = value;
	}

	inline static int32_t get_offset_of_shearX_10() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___shearX_10)); }
	inline float get_shearX_10() const { return ___shearX_10; }
	inline float* get_address_of_shearX_10() { return &___shearX_10; }
	inline void set_shearX_10(float value)
	{
		___shearX_10 = value;
	}

	inline static int32_t get_offset_of_shearY_11() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___shearY_11)); }
	inline float get_shearY_11() const { return ___shearY_11; }
	inline float* get_address_of_shearY_11() { return &___shearY_11; }
	inline void set_shearY_11(float value)
	{
		___shearY_11 = value;
	}

	inline static int32_t get_offset_of_ax_12() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ax_12)); }
	inline float get_ax_12() const { return ___ax_12; }
	inline float* get_address_of_ax_12() { return &___ax_12; }
	inline void set_ax_12(float value)
	{
		___ax_12 = value;
	}

	inline static int32_t get_offset_of_ay_13() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ay_13)); }
	inline float get_ay_13() const { return ___ay_13; }
	inline float* get_address_of_ay_13() { return &___ay_13; }
	inline void set_ay_13(float value)
	{
		___ay_13 = value;
	}

	inline static int32_t get_offset_of_arotation_14() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___arotation_14)); }
	inline float get_arotation_14() const { return ___arotation_14; }
	inline float* get_address_of_arotation_14() { return &___arotation_14; }
	inline void set_arotation_14(float value)
	{
		___arotation_14 = value;
	}

	inline static int32_t get_offset_of_ascaleX_15() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ascaleX_15)); }
	inline float get_ascaleX_15() const { return ___ascaleX_15; }
	inline float* get_address_of_ascaleX_15() { return &___ascaleX_15; }
	inline void set_ascaleX_15(float value)
	{
		___ascaleX_15 = value;
	}

	inline static int32_t get_offset_of_ascaleY_16() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ascaleY_16)); }
	inline float get_ascaleY_16() const { return ___ascaleY_16; }
	inline float* get_address_of_ascaleY_16() { return &___ascaleY_16; }
	inline void set_ascaleY_16(float value)
	{
		___ascaleY_16 = value;
	}

	inline static int32_t get_offset_of_ashearX_17() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ashearX_17)); }
	inline float get_ashearX_17() const { return ___ashearX_17; }
	inline float* get_address_of_ashearX_17() { return &___ashearX_17; }
	inline void set_ashearX_17(float value)
	{
		___ashearX_17 = value;
	}

	inline static int32_t get_offset_of_ashearY_18() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ashearY_18)); }
	inline float get_ashearY_18() const { return ___ashearY_18; }
	inline float* get_address_of_ashearY_18() { return &___ashearY_18; }
	inline void set_ashearY_18(float value)
	{
		___ashearY_18 = value;
	}

	inline static int32_t get_offset_of_appliedValid_19() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___appliedValid_19)); }
	inline bool get_appliedValid_19() const { return ___appliedValid_19; }
	inline bool* get_address_of_appliedValid_19() { return &___appliedValid_19; }
	inline void set_appliedValid_19(bool value)
	{
		___appliedValid_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_b_21() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___b_21)); }
	inline float get_b_21() const { return ___b_21; }
	inline float* get_address_of_b_21() { return &___b_21; }
	inline void set_b_21(float value)
	{
		___b_21 = value;
	}

	inline static int32_t get_offset_of_worldX_22() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___worldX_22)); }
	inline float get_worldX_22() const { return ___worldX_22; }
	inline float* get_address_of_worldX_22() { return &___worldX_22; }
	inline void set_worldX_22(float value)
	{
		___worldX_22 = value;
	}

	inline static int32_t get_offset_of_c_23() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___c_23)); }
	inline float get_c_23() const { return ___c_23; }
	inline float* get_address_of_c_23() { return &___c_23; }
	inline void set_c_23(float value)
	{
		___c_23 = value;
	}

	inline static int32_t get_offset_of_d_24() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___d_24)); }
	inline float get_d_24() const { return ___d_24; }
	inline float* get_address_of_d_24() { return &___d_24; }
	inline void set_d_24(float value)
	{
		___d_24 = value;
	}

	inline static int32_t get_offset_of_worldY_25() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___worldY_25)); }
	inline float get_worldY_25() const { return ___worldY_25; }
	inline float* get_address_of_worldY_25() { return &___worldY_25; }
	inline void set_worldY_25(float value)
	{
		___worldY_25 = value;
	}

	inline static int32_t get_offset_of_sorted_26() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___sorted_26)); }
	inline bool get_sorted_26() const { return ___sorted_26; }
	inline bool* get_address_of_sorted_26() { return &___sorted_26; }
	inline void set_sorted_26(bool value)
	{
		___sorted_26 = value;
	}
};

struct Bone_t1086356328_StaticFields
{
public:
	// System.Boolean Spine.Bone::yDown
	bool ___yDown_0;

public:
	inline static int32_t get_offset_of_yDown_0() { return static_cast<int32_t>(offsetof(Bone_t1086356328_StaticFields, ___yDown_0)); }
	inline bool get_yDown_0() const { return ___yDown_0; }
	inline bool* get_address_of_yDown_0() { return &___yDown_0; }
	inline void set_yDown_0(bool value)
	{
		___yDown_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONE_T1086356328_H
#ifndef ATTACHMENT_T3043756552_H
#define ATTACHMENT_T3043756552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Attachment
struct  Attachment_t3043756552  : public RuntimeObject
{
public:
	// System.String Spine.Attachment::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Attachment_t3043756552, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENT_T3043756552_H
#ifndef ATLASATTACHMENTLOADER_T3966790320_H
#define ATLASATTACHMENTLOADER_T3966790320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasAttachmentLoader
struct  AtlasAttachmentLoader_t3966790320  : public RuntimeObject
{
public:
	// Spine.Atlas[] Spine.AtlasAttachmentLoader::atlasArray
	AtlasU5BU5D_t3463999232* ___atlasArray_0;

public:
	inline static int32_t get_offset_of_atlasArray_0() { return static_cast<int32_t>(offsetof(AtlasAttachmentLoader_t3966790320, ___atlasArray_0)); }
	inline AtlasU5BU5D_t3463999232* get_atlasArray_0() const { return ___atlasArray_0; }
	inline AtlasU5BU5D_t3463999232** get_address_of_atlasArray_0() { return &___atlasArray_0; }
	inline void set_atlasArray_0(AtlasU5BU5D_t3463999232* value)
	{
		___atlasArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASATTACHMENTLOADER_T3966790320_H
#ifndef ATLASREGION_T13903284_H
#define ATLASREGION_T13903284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasRegion
struct  AtlasRegion_t13903284  : public RuntimeObject
{
public:
	// Spine.AtlasPage Spine.AtlasRegion::page
	AtlasPage_t4077017671 * ___page_0;
	// System.String Spine.AtlasRegion::name
	String_t* ___name_1;
	// System.Int32 Spine.AtlasRegion::x
	int32_t ___x_2;
	// System.Int32 Spine.AtlasRegion::y
	int32_t ___y_3;
	// System.Int32 Spine.AtlasRegion::width
	int32_t ___width_4;
	// System.Int32 Spine.AtlasRegion::height
	int32_t ___height_5;
	// System.Single Spine.AtlasRegion::u
	float ___u_6;
	// System.Single Spine.AtlasRegion::v
	float ___v_7;
	// System.Single Spine.AtlasRegion::u2
	float ___u2_8;
	// System.Single Spine.AtlasRegion::v2
	float ___v2_9;
	// System.Single Spine.AtlasRegion::offsetX
	float ___offsetX_10;
	// System.Single Spine.AtlasRegion::offsetY
	float ___offsetY_11;
	// System.Int32 Spine.AtlasRegion::originalWidth
	int32_t ___originalWidth_12;
	// System.Int32 Spine.AtlasRegion::originalHeight
	int32_t ___originalHeight_13;
	// System.Int32 Spine.AtlasRegion::index
	int32_t ___index_14;
	// System.Boolean Spine.AtlasRegion::rotate
	bool ___rotate_15;
	// System.Int32[] Spine.AtlasRegion::splits
	Int32U5BU5D_t385246372* ___splits_16;
	// System.Int32[] Spine.AtlasRegion::pads
	Int32U5BU5D_t385246372* ___pads_17;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___page_0)); }
	inline AtlasPage_t4077017671 * get_page_0() const { return ___page_0; }
	inline AtlasPage_t4077017671 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(AtlasPage_t4077017671 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_u_6() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___u_6)); }
	inline float get_u_6() const { return ___u_6; }
	inline float* get_address_of_u_6() { return &___u_6; }
	inline void set_u_6(float value)
	{
		___u_6 = value;
	}

	inline static int32_t get_offset_of_v_7() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___v_7)); }
	inline float get_v_7() const { return ___v_7; }
	inline float* get_address_of_v_7() { return &___v_7; }
	inline void set_v_7(float value)
	{
		___v_7 = value;
	}

	inline static int32_t get_offset_of_u2_8() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___u2_8)); }
	inline float get_u2_8() const { return ___u2_8; }
	inline float* get_address_of_u2_8() { return &___u2_8; }
	inline void set_u2_8(float value)
	{
		___u2_8 = value;
	}

	inline static int32_t get_offset_of_v2_9() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___v2_9)); }
	inline float get_v2_9() const { return ___v2_9; }
	inline float* get_address_of_v2_9() { return &___v2_9; }
	inline void set_v2_9(float value)
	{
		___v2_9 = value;
	}

	inline static int32_t get_offset_of_offsetX_10() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___offsetX_10)); }
	inline float get_offsetX_10() const { return ___offsetX_10; }
	inline float* get_address_of_offsetX_10() { return &___offsetX_10; }
	inline void set_offsetX_10(float value)
	{
		___offsetX_10 = value;
	}

	inline static int32_t get_offset_of_offsetY_11() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___offsetY_11)); }
	inline float get_offsetY_11() const { return ___offsetY_11; }
	inline float* get_address_of_offsetY_11() { return &___offsetY_11; }
	inline void set_offsetY_11(float value)
	{
		___offsetY_11 = value;
	}

	inline static int32_t get_offset_of_originalWidth_12() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___originalWidth_12)); }
	inline int32_t get_originalWidth_12() const { return ___originalWidth_12; }
	inline int32_t* get_address_of_originalWidth_12() { return &___originalWidth_12; }
	inline void set_originalWidth_12(int32_t value)
	{
		___originalWidth_12 = value;
	}

	inline static int32_t get_offset_of_originalHeight_13() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___originalHeight_13)); }
	inline int32_t get_originalHeight_13() const { return ___originalHeight_13; }
	inline int32_t* get_address_of_originalHeight_13() { return &___originalHeight_13; }
	inline void set_originalHeight_13(int32_t value)
	{
		___originalHeight_13 = value;
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of_rotate_15() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___rotate_15)); }
	inline bool get_rotate_15() const { return ___rotate_15; }
	inline bool* get_address_of_rotate_15() { return &___rotate_15; }
	inline void set_rotate_15(bool value)
	{
		___rotate_15 = value;
	}

	inline static int32_t get_offset_of_splits_16() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___splits_16)); }
	inline Int32U5BU5D_t385246372* get_splits_16() const { return ___splits_16; }
	inline Int32U5BU5D_t385246372** get_address_of_splits_16() { return &___splits_16; }
	inline void set_splits_16(Int32U5BU5D_t385246372* value)
	{
		___splits_16 = value;
		Il2CppCodeGenWriteBarrier((&___splits_16), value);
	}

	inline static int32_t get_offset_of_pads_17() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___pads_17)); }
	inline Int32U5BU5D_t385246372* get_pads_17() const { return ___pads_17; }
	inline Int32U5BU5D_t385246372** get_address_of_pads_17() { return &___pads_17; }
	inline void set_pads_17(Int32U5BU5D_t385246372* value)
	{
		___pads_17 = value;
		Il2CppCodeGenWriteBarrier((&___pads_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASREGION_T13903284_H
#ifndef ATLAS_T4040192941_H
#define ATLAS_T4040192941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Atlas
struct  Atlas_t4040192941  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.AtlasPage> Spine.Atlas::pages
	List_1_t1254125117 * ___pages_0;
	// System.Collections.Generic.List`1<Spine.AtlasRegion> Spine.Atlas::regions
	List_1_t1485978026 * ___regions_1;
	// Spine.TextureLoader Spine.Atlas::textureLoader
	RuntimeObject* ___textureLoader_2;

public:
	inline static int32_t get_offset_of_pages_0() { return static_cast<int32_t>(offsetof(Atlas_t4040192941, ___pages_0)); }
	inline List_1_t1254125117 * get_pages_0() const { return ___pages_0; }
	inline List_1_t1254125117 ** get_address_of_pages_0() { return &___pages_0; }
	inline void set_pages_0(List_1_t1254125117 * value)
	{
		___pages_0 = value;
		Il2CppCodeGenWriteBarrier((&___pages_0), value);
	}

	inline static int32_t get_offset_of_regions_1() { return static_cast<int32_t>(offsetof(Atlas_t4040192941, ___regions_1)); }
	inline List_1_t1485978026 * get_regions_1() const { return ___regions_1; }
	inline List_1_t1485978026 ** get_address_of_regions_1() { return &___regions_1; }
	inline void set_regions_1(List_1_t1485978026 * value)
	{
		___regions_1 = value;
		Il2CppCodeGenWriteBarrier((&___regions_1), value);
	}

	inline static int32_t get_offset_of_textureLoader_2() { return static_cast<int32_t>(offsetof(Atlas_t4040192941, ___textureLoader_2)); }
	inline RuntimeObject* get_textureLoader_2() const { return ___textureLoader_2; }
	inline RuntimeObject** get_address_of_textureLoader_2() { return &___textureLoader_2; }
	inline void set_textureLoader_2(RuntimeObject* value)
	{
		___textureLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureLoader_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLAS_T4040192941_H
#ifndef ANIMATIONPAIRCOMPARER_T175378255_H
#define ANIMATIONPAIRCOMPARER_T175378255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPairComparer
struct  AnimationPairComparer_t175378255  : public RuntimeObject
{
public:

public:
};

struct AnimationPairComparer_t175378255_StaticFields
{
public:
	// Spine.AnimationStateData/AnimationPairComparer Spine.AnimationStateData/AnimationPairComparer::Instance
	AnimationPairComparer_t175378255 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationPairComparer_t175378255_StaticFields, ___Instance_0)); }
	inline AnimationPairComparer_t175378255 * get_Instance_0() const { return ___Instance_0; }
	inline AnimationPairComparer_t175378255 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AnimationPairComparer_t175378255 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPAIRCOMPARER_T175378255_H
#ifndef ANIMATIONSTATEDATA_T3010651567_H
#define ANIMATIONSTATEDATA_T3010651567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData
struct  AnimationStateData_t3010651567  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.AnimationStateData::skeletonData
	SkeletonData_t2032710716 * ___skeletonData_0;
	// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single> Spine.AnimationStateData::animationToMixTime
	Dictionary_2_t680659097 * ___animationToMixTime_1;
	// System.Single Spine.AnimationStateData::defaultMix
	float ___defaultMix_2;

public:
	inline static int32_t get_offset_of_skeletonData_0() { return static_cast<int32_t>(offsetof(AnimationStateData_t3010651567, ___skeletonData_0)); }
	inline SkeletonData_t2032710716 * get_skeletonData_0() const { return ___skeletonData_0; }
	inline SkeletonData_t2032710716 ** get_address_of_skeletonData_0() { return &___skeletonData_0; }
	inline void set_skeletonData_0(SkeletonData_t2032710716 * value)
	{
		___skeletonData_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_0), value);
	}

	inline static int32_t get_offset_of_animationToMixTime_1() { return static_cast<int32_t>(offsetof(AnimationStateData_t3010651567, ___animationToMixTime_1)); }
	inline Dictionary_2_t680659097 * get_animationToMixTime_1() const { return ___animationToMixTime_1; }
	inline Dictionary_2_t680659097 ** get_address_of_animationToMixTime_1() { return &___animationToMixTime_1; }
	inline void set_animationToMixTime_1(Dictionary_2_t680659097 * value)
	{
		___animationToMixTime_1 = value;
		Il2CppCodeGenWriteBarrier((&___animationToMixTime_1), value);
	}

	inline static int32_t get_offset_of_defaultMix_2() { return static_cast<int32_t>(offsetof(AnimationStateData_t3010651567, ___defaultMix_2)); }
	inline float get_defaultMix_2() const { return ___defaultMix_2; }
	inline float* get_address_of_defaultMix_2() { return &___defaultMix_2; }
	inline void set_defaultMix_2(float value)
	{
		___defaultMix_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATEDATA_T3010651567_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EVENT_T1378573841_H
#define EVENT_T1378573841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Event
struct  Event_t1378573841  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Event::data
	EventData_t724759987 * ___data_0;
	// System.Single Spine.Event::time
	float ___time_1;
	// System.Int32 Spine.Event::intValue
	int32_t ___intValue_2;
	// System.Single Spine.Event::floatValue
	float ___floatValue_3;
	// System.String Spine.Event::stringValue
	String_t* ___stringValue_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___data_0)); }
	inline EventData_t724759987 * get_data_0() const { return ___data_0; }
	inline EventData_t724759987 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(EventData_t724759987 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_intValue_2() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___intValue_2)); }
	inline int32_t get_intValue_2() const { return ___intValue_2; }
	inline int32_t* get_address_of_intValue_2() { return &___intValue_2; }
	inline void set_intValue_2(int32_t value)
	{
		___intValue_2 = value;
	}

	inline static int32_t get_offset_of_floatValue_3() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___floatValue_3)); }
	inline float get_floatValue_3() const { return ___floatValue_3; }
	inline float* get_address_of_floatValue_3() { return &___floatValue_3; }
	inline void set_floatValue_3(float value)
	{
		___floatValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___stringValue_4)); }
	inline String_t* get_stringValue_4() const { return ___stringValue_4; }
	inline String_t** get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(String_t* value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENT_T1378573841_H
#ifndef EVENTDATA_T724759987_H
#define EVENTDATA_T724759987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventData
struct  EventData_t724759987  : public RuntimeObject
{
public:
	// System.String Spine.EventData::name
	String_t* ___name_0;
	// System.Int32 Spine.EventData::<Int>k__BackingField
	int32_t ___U3CIntU3Ek__BackingField_1;
	// System.Single Spine.EventData::<Float>k__BackingField
	float ___U3CFloatU3Ek__BackingField_2;
	// System.String Spine.EventData::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CIntU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___U3CIntU3Ek__BackingField_1)); }
	inline int32_t get_U3CIntU3Ek__BackingField_1() const { return ___U3CIntU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIntU3Ek__BackingField_1() { return &___U3CIntU3Ek__BackingField_1; }
	inline void set_U3CIntU3Ek__BackingField_1(int32_t value)
	{
		___U3CIntU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFloatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___U3CFloatU3Ek__BackingField_2)); }
	inline float get_U3CFloatU3Ek__BackingField_2() const { return ___U3CFloatU3Ek__BackingField_2; }
	inline float* get_address_of_U3CFloatU3Ek__BackingField_2() { return &___U3CFloatU3Ek__BackingField_2; }
	inline void set_U3CFloatU3Ek__BackingField_2(float value)
	{
		___U3CFloatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___U3CStringU3Ek__BackingField_3)); }
	inline String_t* get_U3CStringU3Ek__BackingField_3() const { return ___U3CStringU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_3() { return &___U3CStringU3Ek__BackingField_3; }
	inline void set_U3CStringU3Ek__BackingField_3(String_t* value)
	{
		___U3CStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T724759987_H
#ifndef IKCONSTRAINTDATA_T459120129_H
#define IKCONSTRAINTDATA_T459120129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintData
struct  IkConstraintData_t459120129  : public RuntimeObject
{
public:
	// System.String Spine.IkConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.IkConstraintData::order
	int32_t ___order_1;
	// System.Collections.Generic.List`1<Spine.BoneData> Spine.IkConstraintData::bones
	List_1_t307281936 * ___bones_2;
	// Spine.BoneData Spine.IkConstraintData::target
	BoneData_t3130174490 * ___target_3;
	// System.Int32 Spine.IkConstraintData::bendDirection
	int32_t ___bendDirection_4;
	// System.Single Spine.IkConstraintData::mix
	float ___mix_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___bones_2)); }
	inline List_1_t307281936 * get_bones_2() const { return ___bones_2; }
	inline List_1_t307281936 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(List_1_t307281936 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___target_3)); }
	inline BoneData_t3130174490 * get_target_3() const { return ___target_3; }
	inline BoneData_t3130174490 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t3130174490 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}

	inline static int32_t get_offset_of_mix_5() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___mix_5)); }
	inline float get_mix_5() const { return ___mix_5; }
	inline float* get_address_of_mix_5() { return &___mix_5; }
	inline void set_mix_5(float value)
	{
		___mix_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTDATA_T459120129_H
#ifndef IKCONSTRAINT_T1675190269_H
#define IKCONSTRAINT_T1675190269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraint
struct  IkConstraint_t1675190269  : public RuntimeObject
{
public:
	// Spine.IkConstraintData Spine.IkConstraint::data
	IkConstraintData_t459120129 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.IkConstraint::bones
	ExposedList_1_t3793468194 * ___bones_1;
	// Spine.Bone Spine.IkConstraint::target
	Bone_t1086356328 * ___target_2;
	// System.Single Spine.IkConstraint::mix
	float ___mix_3;
	// System.Int32 Spine.IkConstraint::bendDirection
	int32_t ___bendDirection_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___data_0)); }
	inline IkConstraintData_t459120129 * get_data_0() const { return ___data_0; }
	inline IkConstraintData_t459120129 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IkConstraintData_t459120129 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___bones_1)); }
	inline ExposedList_1_t3793468194 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3793468194 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___target_2)); }
	inline Bone_t1086356328 * get_target_2() const { return ___target_2; }
	inline Bone_t1086356328 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t1086356328 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_mix_3() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___mix_3)); }
	inline float get_mix_3() const { return ___mix_3; }
	inline float* get_address_of_mix_3() { return &___mix_3; }
	inline void set_mix_3(float value)
	{
		___mix_3 = value;
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINT_T1675190269_H
#ifndef ANIMATIONPAIR_T1784808777_H
#define ANIMATIONPAIR_T1784808777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPair
struct  AnimationPair_t1784808777 
{
public:
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a1
	Animation_t615783283 * ___a1_0;
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a2
	Animation_t615783283 * ___a2_1;

public:
	inline static int32_t get_offset_of_a1_0() { return static_cast<int32_t>(offsetof(AnimationPair_t1784808777, ___a1_0)); }
	inline Animation_t615783283 * get_a1_0() const { return ___a1_0; }
	inline Animation_t615783283 ** get_address_of_a1_0() { return &___a1_0; }
	inline void set_a1_0(Animation_t615783283 * value)
	{
		___a1_0 = value;
		Il2CppCodeGenWriteBarrier((&___a1_0), value);
	}

	inline static int32_t get_offset_of_a2_1() { return static_cast<int32_t>(offsetof(AnimationPair_t1784808777, ___a2_1)); }
	inline Animation_t615783283 * get_a2_1() const { return ___a2_1; }
	inline Animation_t615783283 ** get_address_of_a2_1() { return &___a2_1; }
	inline void set_a2_1(Animation_t615783283 * value)
	{
		___a2_1 = value;
		Il2CppCodeGenWriteBarrier((&___a2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_t1784808777_marshaled_pinvoke
{
	Animation_t615783283 * ___a1_0;
	Animation_t615783283 * ___a2_1;
};
// Native definition for COM marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_t1784808777_marshaled_com
{
	Animation_t615783283 * ___a1_0;
	Animation_t615783283 * ___a2_1;
};
#endif // ANIMATIONPAIR_T1784808777_H
#ifndef SUBMESHINSTRUCTION_T52121370_H
#define SUBMESHINSTRUCTION_T52121370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SubmeshInstruction
struct  SubmeshInstruction_t52121370 
{
public:
	// Spine.Skeleton Spine.Unity.SubmeshInstruction::skeleton
	Skeleton_t3686076450 * ___skeleton_0;
	// System.Int32 Spine.Unity.SubmeshInstruction::startSlot
	int32_t ___startSlot_1;
	// System.Int32 Spine.Unity.SubmeshInstruction::endSlot
	int32_t ___endSlot_2;
	// UnityEngine.Material Spine.Unity.SubmeshInstruction::material
	Material_t340375123 * ___material_3;
	// System.Boolean Spine.Unity.SubmeshInstruction::forceSeparate
	bool ___forceSeparate_4;
	// System.Int32 Spine.Unity.SubmeshInstruction::preActiveClippingSlotSource
	int32_t ___preActiveClippingSlotSource_5;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawTriangleCount
	int32_t ___rawTriangleCount_6;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawVertexCount
	int32_t ___rawVertexCount_7;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawFirstVertexIndex
	int32_t ___rawFirstVertexIndex_8;
	// System.Boolean Spine.Unity.SubmeshInstruction::hasClipping
	bool ___hasClipping_9;

public:
	inline static int32_t get_offset_of_skeleton_0() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___skeleton_0)); }
	inline Skeleton_t3686076450 * get_skeleton_0() const { return ___skeleton_0; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_0() { return &___skeleton_0; }
	inline void set_skeleton_0(Skeleton_t3686076450 * value)
	{
		___skeleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_0), value);
	}

	inline static int32_t get_offset_of_startSlot_1() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___startSlot_1)); }
	inline int32_t get_startSlot_1() const { return ___startSlot_1; }
	inline int32_t* get_address_of_startSlot_1() { return &___startSlot_1; }
	inline void set_startSlot_1(int32_t value)
	{
		___startSlot_1 = value;
	}

	inline static int32_t get_offset_of_endSlot_2() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___endSlot_2)); }
	inline int32_t get_endSlot_2() const { return ___endSlot_2; }
	inline int32_t* get_address_of_endSlot_2() { return &___endSlot_2; }
	inline void set_endSlot_2(int32_t value)
	{
		___endSlot_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_forceSeparate_4() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___forceSeparate_4)); }
	inline bool get_forceSeparate_4() const { return ___forceSeparate_4; }
	inline bool* get_address_of_forceSeparate_4() { return &___forceSeparate_4; }
	inline void set_forceSeparate_4(bool value)
	{
		___forceSeparate_4 = value;
	}

	inline static int32_t get_offset_of_preActiveClippingSlotSource_5() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___preActiveClippingSlotSource_5)); }
	inline int32_t get_preActiveClippingSlotSource_5() const { return ___preActiveClippingSlotSource_5; }
	inline int32_t* get_address_of_preActiveClippingSlotSource_5() { return &___preActiveClippingSlotSource_5; }
	inline void set_preActiveClippingSlotSource_5(int32_t value)
	{
		___preActiveClippingSlotSource_5 = value;
	}

	inline static int32_t get_offset_of_rawTriangleCount_6() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___rawTriangleCount_6)); }
	inline int32_t get_rawTriangleCount_6() const { return ___rawTriangleCount_6; }
	inline int32_t* get_address_of_rawTriangleCount_6() { return &___rawTriangleCount_6; }
	inline void set_rawTriangleCount_6(int32_t value)
	{
		___rawTriangleCount_6 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_7() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___rawVertexCount_7)); }
	inline int32_t get_rawVertexCount_7() const { return ___rawVertexCount_7; }
	inline int32_t* get_address_of_rawVertexCount_7() { return &___rawVertexCount_7; }
	inline void set_rawVertexCount_7(int32_t value)
	{
		___rawVertexCount_7 = value;
	}

	inline static int32_t get_offset_of_rawFirstVertexIndex_8() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___rawFirstVertexIndex_8)); }
	inline int32_t get_rawFirstVertexIndex_8() const { return ___rawFirstVertexIndex_8; }
	inline int32_t* get_address_of_rawFirstVertexIndex_8() { return &___rawFirstVertexIndex_8; }
	inline void set_rawFirstVertexIndex_8(int32_t value)
	{
		___rawFirstVertexIndex_8 = value;
	}

	inline static int32_t get_offset_of_hasClipping_9() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___hasClipping_9)); }
	inline bool get_hasClipping_9() const { return ___hasClipping_9; }
	inline bool* get_address_of_hasClipping_9() { return &___hasClipping_9; }
	inline void set_hasClipping_9(bool value)
	{
		___hasClipping_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t52121370_marshaled_pinvoke
{
	Skeleton_t3686076450 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t340375123 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
// Native definition for COM marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t52121370_marshaled_com
{
	Skeleton_t3686076450 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t340375123 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
#endif // SUBMESHINSTRUCTION_T52121370_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef SETTINGS_T612870457_H
#define SETTINGS_T612870457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator/Settings
struct  Settings_t612870457 
{
public:
	// System.Boolean Spine.Unity.MeshGenerator/Settings::useClipping
	bool ___useClipping_0;
	// System.Single Spine.Unity.MeshGenerator/Settings::zSpacing
	float ___zSpacing_1;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::pmaVertexColors
	bool ___pmaVertexColors_2;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::tintBlack
	bool ___tintBlack_3;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::calculateTangents
	bool ___calculateTangents_4;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::addNormals
	bool ___addNormals_5;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::immutableTriangles
	bool ___immutableTriangles_6;

public:
	inline static int32_t get_offset_of_useClipping_0() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___useClipping_0)); }
	inline bool get_useClipping_0() const { return ___useClipping_0; }
	inline bool* get_address_of_useClipping_0() { return &___useClipping_0; }
	inline void set_useClipping_0(bool value)
	{
		___useClipping_0 = value;
	}

	inline static int32_t get_offset_of_zSpacing_1() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___zSpacing_1)); }
	inline float get_zSpacing_1() const { return ___zSpacing_1; }
	inline float* get_address_of_zSpacing_1() { return &___zSpacing_1; }
	inline void set_zSpacing_1(float value)
	{
		___zSpacing_1 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_2() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___pmaVertexColors_2)); }
	inline bool get_pmaVertexColors_2() const { return ___pmaVertexColors_2; }
	inline bool* get_address_of_pmaVertexColors_2() { return &___pmaVertexColors_2; }
	inline void set_pmaVertexColors_2(bool value)
	{
		___pmaVertexColors_2 = value;
	}

	inline static int32_t get_offset_of_tintBlack_3() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___tintBlack_3)); }
	inline bool get_tintBlack_3() const { return ___tintBlack_3; }
	inline bool* get_address_of_tintBlack_3() { return &___tintBlack_3; }
	inline void set_tintBlack_3(bool value)
	{
		___tintBlack_3 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_4() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___calculateTangents_4)); }
	inline bool get_calculateTangents_4() const { return ___calculateTangents_4; }
	inline bool* get_address_of_calculateTangents_4() { return &___calculateTangents_4; }
	inline void set_calculateTangents_4(bool value)
	{
		___calculateTangents_4 = value;
	}

	inline static int32_t get_offset_of_addNormals_5() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___addNormals_5)); }
	inline bool get_addNormals_5() const { return ___addNormals_5; }
	inline bool* get_address_of_addNormals_5() { return &___addNormals_5; }
	inline void set_addNormals_5(bool value)
	{
		___addNormals_5 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_6() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___immutableTriangles_6)); }
	inline bool get_immutableTriangles_6() const { return ___immutableTriangles_6; }
	inline bool* get_address_of_immutableTriangles_6() { return &___immutableTriangles_6; }
	inline void set_immutableTriangles_6(bool value)
	{
		___immutableTriangles_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t612870457_marshaled_pinvoke
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
// Native definition for COM marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t612870457_marshaled_com
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
#endif // SETTINGS_T612870457_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef MESHGENERATORBUFFERS_T1424700926_H
#define MESHGENERATORBUFFERS_T1424700926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorBuffers
struct  MeshGeneratorBuffers_t1424700926 
{
public:
	// System.Int32 Spine.Unity.MeshGeneratorBuffers::vertexCount
	int32_t ___vertexCount_0;
	// UnityEngine.Vector3[] Spine.Unity.MeshGeneratorBuffers::vertexBuffer
	Vector3U5BU5D_t1718750761* ___vertexBuffer_1;
	// UnityEngine.Vector2[] Spine.Unity.MeshGeneratorBuffers::uvBuffer
	Vector2U5BU5D_t1457185986* ___uvBuffer_2;
	// UnityEngine.Color32[] Spine.Unity.MeshGeneratorBuffers::colorBuffer
	Color32U5BU5D_t3850468773* ___colorBuffer_3;
	// Spine.Unity.MeshGenerator Spine.Unity.MeshGeneratorBuffers::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_4;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_1() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___vertexBuffer_1)); }
	inline Vector3U5BU5D_t1718750761* get_vertexBuffer_1() const { return ___vertexBuffer_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertexBuffer_1() { return &___vertexBuffer_1; }
	inline void set_vertexBuffer_1(Vector3U5BU5D_t1718750761* value)
	{
		___vertexBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_1), value);
	}

	inline static int32_t get_offset_of_uvBuffer_2() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___uvBuffer_2)); }
	inline Vector2U5BU5D_t1457185986* get_uvBuffer_2() const { return ___uvBuffer_2; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvBuffer_2() { return &___uvBuffer_2; }
	inline void set_uvBuffer_2(Vector2U5BU5D_t1457185986* value)
	{
		___uvBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_2), value);
	}

	inline static int32_t get_offset_of_colorBuffer_3() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___colorBuffer_3)); }
	inline Color32U5BU5D_t3850468773* get_colorBuffer_3() const { return ___colorBuffer_3; }
	inline Color32U5BU5D_t3850468773** get_address_of_colorBuffer_3() { return &___colorBuffer_3; }
	inline void set_colorBuffer_3(Color32U5BU5D_t3850468773* value)
	{
		___colorBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_3), value);
	}

	inline static int32_t get_offset_of_meshGenerator_4() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___meshGenerator_4)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_4() const { return ___meshGenerator_4; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_4() { return &___meshGenerator_4; }
	inline void set_meshGenerator_4(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_t1424700926_marshaled_pinvoke
{
	int32_t ___vertexCount_0;
	Vector3_t3722313464 * ___vertexBuffer_1;
	Vector2_t2156229523 * ___uvBuffer_2;
	Color32_t2600501292 * ___colorBuffer_3;
	MeshGenerator_t1354683548 * ___meshGenerator_4;
};
// Native definition for COM marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_t1424700926_marshaled_com
{
	int32_t ___vertexCount_0;
	Vector3_t3722313464 * ___vertexBuffer_1;
	Vector2_t2156229523 * ___uvBuffer_2;
	Color32_t2600501292 * ___colorBuffer_3;
	MeshGenerator_t1354683548 * ___meshGenerator_4;
};
#endif // MESHGENERATORBUFFERS_T1424700926_H
#ifndef VERTEXATTACHMENT_T4074366829_H
#define VERTEXATTACHMENT_T4074366829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.VertexAttachment
struct  VertexAttachment_t4074366829  : public Attachment_t3043756552
{
public:
	// System.Int32 Spine.VertexAttachment::id
	int32_t ___id_3;
	// System.Int32[] Spine.VertexAttachment::bones
	Int32U5BU5D_t385246372* ___bones_4;
	// System.Single[] Spine.VertexAttachment::vertices
	SingleU5BU5D_t1444911251* ___vertices_5;
	// System.Int32 Spine.VertexAttachment::worldVerticesLength
	int32_t ___worldVerticesLength_6;

public:
	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_bones_4() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___bones_4)); }
	inline Int32U5BU5D_t385246372* get_bones_4() const { return ___bones_4; }
	inline Int32U5BU5D_t385246372** get_address_of_bones_4() { return &___bones_4; }
	inline void set_bones_4(Int32U5BU5D_t385246372* value)
	{
		___bones_4 = value;
		Il2CppCodeGenWriteBarrier((&___bones_4), value);
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___vertices_5)); }
	inline SingleU5BU5D_t1444911251* get_vertices_5() const { return ___vertices_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(SingleU5BU5D_t1444911251* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_worldVerticesLength_6() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___worldVerticesLength_6)); }
	inline int32_t get_worldVerticesLength_6() const { return ___worldVerticesLength_6; }
	inline int32_t* get_address_of_worldVerticesLength_6() { return &___worldVerticesLength_6; }
	inline void set_worldVerticesLength_6(int32_t value)
	{
		___worldVerticesLength_6 = value;
	}
};

struct VertexAttachment_t4074366829_StaticFields
{
public:
	// System.Int32 Spine.VertexAttachment::nextID
	int32_t ___nextID_1;
	// System.Object Spine.VertexAttachment::nextIdLock
	RuntimeObject * ___nextIdLock_2;

public:
	inline static int32_t get_offset_of_nextID_1() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829_StaticFields, ___nextID_1)); }
	inline int32_t get_nextID_1() const { return ___nextID_1; }
	inline int32_t* get_address_of_nextID_1() { return &___nextID_1; }
	inline void set_nextID_1(int32_t value)
	{
		___nextID_1 = value;
	}

	inline static int32_t get_offset_of_nextIdLock_2() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829_StaticFields, ___nextIdLock_2)); }
	inline RuntimeObject * get_nextIdLock_2() const { return ___nextIdLock_2; }
	inline RuntimeObject ** get_address_of_nextIdLock_2() { return &___nextIdLock_2; }
	inline void set_nextIdLock_2(RuntimeObject * value)
	{
		___nextIdLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextIdLock_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXATTACHMENT_T4074366829_H
#ifndef REGIONATTACHMENT_T1770147391_H
#define REGIONATTACHMENT_T1770147391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RegionAttachment
struct  RegionAttachment_t1770147391  : public Attachment_t3043756552
{
public:
	// System.Single Spine.RegionAttachment::x
	float ___x_9;
	// System.Single Spine.RegionAttachment::y
	float ___y_10;
	// System.Single Spine.RegionAttachment::rotation
	float ___rotation_11;
	// System.Single Spine.RegionAttachment::scaleX
	float ___scaleX_12;
	// System.Single Spine.RegionAttachment::scaleY
	float ___scaleY_13;
	// System.Single Spine.RegionAttachment::width
	float ___width_14;
	// System.Single Spine.RegionAttachment::height
	float ___height_15;
	// System.Single Spine.RegionAttachment::regionOffsetX
	float ___regionOffsetX_16;
	// System.Single Spine.RegionAttachment::regionOffsetY
	float ___regionOffsetY_17;
	// System.Single Spine.RegionAttachment::regionWidth
	float ___regionWidth_18;
	// System.Single Spine.RegionAttachment::regionHeight
	float ___regionHeight_19;
	// System.Single Spine.RegionAttachment::regionOriginalWidth
	float ___regionOriginalWidth_20;
	// System.Single Spine.RegionAttachment::regionOriginalHeight
	float ___regionOriginalHeight_21;
	// System.Single[] Spine.RegionAttachment::offset
	SingleU5BU5D_t1444911251* ___offset_22;
	// System.Single[] Spine.RegionAttachment::uvs
	SingleU5BU5D_t1444911251* ___uvs_23;
	// System.Single Spine.RegionAttachment::r
	float ___r_24;
	// System.Single Spine.RegionAttachment::g
	float ___g_25;
	// System.Single Spine.RegionAttachment::b
	float ___b_26;
	// System.Single Spine.RegionAttachment::a
	float ___a_27;
	// System.String Spine.RegionAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_28;
	// System.Object Spine.RegionAttachment::RendererObject
	RuntimeObject * ___RendererObject_29;

public:
	inline static int32_t get_offset_of_x_9() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___x_9)); }
	inline float get_x_9() const { return ___x_9; }
	inline float* get_address_of_x_9() { return &___x_9; }
	inline void set_x_9(float value)
	{
		___x_9 = value;
	}

	inline static int32_t get_offset_of_y_10() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___y_10)); }
	inline float get_y_10() const { return ___y_10; }
	inline float* get_address_of_y_10() { return &___y_10; }
	inline void set_y_10(float value)
	{
		___y_10 = value;
	}

	inline static int32_t get_offset_of_rotation_11() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___rotation_11)); }
	inline float get_rotation_11() const { return ___rotation_11; }
	inline float* get_address_of_rotation_11() { return &___rotation_11; }
	inline void set_rotation_11(float value)
	{
		___rotation_11 = value;
	}

	inline static int32_t get_offset_of_scaleX_12() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___scaleX_12)); }
	inline float get_scaleX_12() const { return ___scaleX_12; }
	inline float* get_address_of_scaleX_12() { return &___scaleX_12; }
	inline void set_scaleX_12(float value)
	{
		___scaleX_12 = value;
	}

	inline static int32_t get_offset_of_scaleY_13() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___scaleY_13)); }
	inline float get_scaleY_13() const { return ___scaleY_13; }
	inline float* get_address_of_scaleY_13() { return &___scaleY_13; }
	inline void set_scaleY_13(float value)
	{
		___scaleY_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___height_15)); }
	inline float get_height_15() const { return ___height_15; }
	inline float* get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(float value)
	{
		___height_15 = value;
	}

	inline static int32_t get_offset_of_regionOffsetX_16() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOffsetX_16)); }
	inline float get_regionOffsetX_16() const { return ___regionOffsetX_16; }
	inline float* get_address_of_regionOffsetX_16() { return &___regionOffsetX_16; }
	inline void set_regionOffsetX_16(float value)
	{
		___regionOffsetX_16 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_17() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOffsetY_17)); }
	inline float get_regionOffsetY_17() const { return ___regionOffsetY_17; }
	inline float* get_address_of_regionOffsetY_17() { return &___regionOffsetY_17; }
	inline void set_regionOffsetY_17(float value)
	{
		___regionOffsetY_17 = value;
	}

	inline static int32_t get_offset_of_regionWidth_18() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionWidth_18)); }
	inline float get_regionWidth_18() const { return ___regionWidth_18; }
	inline float* get_address_of_regionWidth_18() { return &___regionWidth_18; }
	inline void set_regionWidth_18(float value)
	{
		___regionWidth_18 = value;
	}

	inline static int32_t get_offset_of_regionHeight_19() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionHeight_19)); }
	inline float get_regionHeight_19() const { return ___regionHeight_19; }
	inline float* get_address_of_regionHeight_19() { return &___regionHeight_19; }
	inline void set_regionHeight_19(float value)
	{
		___regionHeight_19 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_20() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOriginalWidth_20)); }
	inline float get_regionOriginalWidth_20() const { return ___regionOriginalWidth_20; }
	inline float* get_address_of_regionOriginalWidth_20() { return &___regionOriginalWidth_20; }
	inline void set_regionOriginalWidth_20(float value)
	{
		___regionOriginalWidth_20 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_21() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOriginalHeight_21)); }
	inline float get_regionOriginalHeight_21() const { return ___regionOriginalHeight_21; }
	inline float* get_address_of_regionOriginalHeight_21() { return &___regionOriginalHeight_21; }
	inline void set_regionOriginalHeight_21(float value)
	{
		___regionOriginalHeight_21 = value;
	}

	inline static int32_t get_offset_of_offset_22() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___offset_22)); }
	inline SingleU5BU5D_t1444911251* get_offset_22() const { return ___offset_22; }
	inline SingleU5BU5D_t1444911251** get_address_of_offset_22() { return &___offset_22; }
	inline void set_offset_22(SingleU5BU5D_t1444911251* value)
	{
		___offset_22 = value;
		Il2CppCodeGenWriteBarrier((&___offset_22), value);
	}

	inline static int32_t get_offset_of_uvs_23() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___uvs_23)); }
	inline SingleU5BU5D_t1444911251* get_uvs_23() const { return ___uvs_23; }
	inline SingleU5BU5D_t1444911251** get_address_of_uvs_23() { return &___uvs_23; }
	inline void set_uvs_23(SingleU5BU5D_t1444911251* value)
	{
		___uvs_23 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_23), value);
	}

	inline static int32_t get_offset_of_r_24() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___r_24)); }
	inline float get_r_24() const { return ___r_24; }
	inline float* get_address_of_r_24() { return &___r_24; }
	inline void set_r_24(float value)
	{
		___r_24 = value;
	}

	inline static int32_t get_offset_of_g_25() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___g_25)); }
	inline float get_g_25() const { return ___g_25; }
	inline float* get_address_of_g_25() { return &___g_25; }
	inline void set_g_25(float value)
	{
		___g_25 = value;
	}

	inline static int32_t get_offset_of_b_26() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___b_26)); }
	inline float get_b_26() const { return ___b_26; }
	inline float* get_address_of_b_26() { return &___b_26; }
	inline void set_b_26(float value)
	{
		___b_26 = value;
	}

	inline static int32_t get_offset_of_a_27() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___a_27)); }
	inline float get_a_27() const { return ___a_27; }
	inline float* get_address_of_a_27() { return &___a_27; }
	inline void set_a_27(float value)
	{
		___a_27 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___U3CPathU3Ek__BackingField_28)); }
	inline String_t* get_U3CPathU3Ek__BackingField_28() const { return ___U3CPathU3Ek__BackingField_28; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_28() { return &___U3CPathU3Ek__BackingField_28; }
	inline void set_U3CPathU3Ek__BackingField_28(String_t* value)
	{
		___U3CPathU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_RendererObject_29() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___RendererObject_29)); }
	inline RuntimeObject * get_RendererObject_29() const { return ___RendererObject_29; }
	inline RuntimeObject ** get_address_of_RendererObject_29() { return &___RendererObject_29; }
	inline void set_RendererObject_29(RuntimeObject * value)
	{
		___RendererObject_29 = value;
		Il2CppCodeGenWriteBarrier((&___RendererObject_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONATTACHMENT_T1770147391_H
#ifndef ATTACHMENTKEYTUPLE_T1548106539_H
#define ATTACHMENTKEYTUPLE_T1548106539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTuple
struct  AttachmentKeyTuple_t1548106539 
{
public:
	// System.Int32 Spine.Skin/AttachmentKeyTuple::slotIndex
	int32_t ___slotIndex_0;
	// System.String Spine.Skin/AttachmentKeyTuple::name
	String_t* ___name_1;
	// System.Int32 Spine.Skin/AttachmentKeyTuple::nameHashCode
	int32_t ___nameHashCode_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t1548106539, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t1548106539, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_nameHashCode_2() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t1548106539, ___nameHashCode_2)); }
	inline int32_t get_nameHashCode_2() const { return ___nameHashCode_2; }
	inline int32_t* get_address_of_nameHashCode_2() { return &___nameHashCode_2; }
	inline void set_nameHashCode_2(int32_t value)
	{
		___nameHashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_t1548106539_marshaled_pinvoke
{
	int32_t ___slotIndex_0;
	char* ___name_1;
	int32_t ___nameHashCode_2;
};
// Native definition for COM marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_t1548106539_marshaled_com
{
	int32_t ___slotIndex_0;
	Il2CppChar* ___name_1;
	int32_t ___nameHashCode_2;
};
#endif // ATTACHMENTKEYTUPLE_T1548106539_H
#ifndef SLOTMATERIALOVERRIDE_T1001979181_H
#define SLOTMATERIALOVERRIDE_T1001979181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct  SlotMaterialOverride_t1001979181 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// System.String Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::slotName
	String_t* ___slotName_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::material
	Material_t340375123 * ___material_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t1001979181, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_slotName_1() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t1001979181, ___slotName_1)); }
	inline String_t* get_slotName_1() const { return ___slotName_1; }
	inline String_t** get_address_of_slotName_1() { return &___slotName_1; }
	inline void set_slotName_1(String_t* value)
	{
		___slotName_1 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_1), value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t1001979181, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_t1001979181_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	char* ___slotName_1;
	Material_t340375123 * ___material_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_t1001979181_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Il2CppChar* ___slotName_1;
	Material_t340375123 * ___material_2;
};
#endif // SLOTMATERIALOVERRIDE_T1001979181_H
#ifndef ATLASMATERIALOVERRIDE_T2435041389_H
#define ATLASMATERIALOVERRIDE_T2435041389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct  AtlasMaterialOverride_t2435041389 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::originalMaterial
	Material_t340375123 * ___originalMaterial_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::replacementMaterial
	Material_t340375123 * ___replacementMaterial_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t2435041389, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_originalMaterial_1() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t2435041389, ___originalMaterial_1)); }
	inline Material_t340375123 * get_originalMaterial_1() const { return ___originalMaterial_1; }
	inline Material_t340375123 ** get_address_of_originalMaterial_1() { return &___originalMaterial_1; }
	inline void set_originalMaterial_1(Material_t340375123 * value)
	{
		___originalMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___originalMaterial_1), value);
	}

	inline static int32_t get_offset_of_replacementMaterial_2() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t2435041389, ___replacementMaterial_2)); }
	inline Material_t340375123 * get_replacementMaterial_2() const { return ___replacementMaterial_2; }
	inline Material_t340375123 ** get_address_of_replacementMaterial_2() { return &___replacementMaterial_2; }
	inline void set_replacementMaterial_2(Material_t340375123 * value)
	{
		___replacementMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___replacementMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_t2435041389_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	Material_t340375123 * ___originalMaterial_1;
	Material_t340375123 * ___replacementMaterial_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_t2435041389_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Material_t340375123 * ___originalMaterial_1;
	Material_t340375123 * ___replacementMaterial_2;
};
#endif // ATLASMATERIALOVERRIDE_T2435041389_H
#ifndef POINTATTACHMENT_T2275020146_H
#define POINTATTACHMENT_T2275020146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PointAttachment
struct  PointAttachment_t2275020146  : public Attachment_t3043756552
{
public:
	// System.Single Spine.PointAttachment::x
	float ___x_1;
	// System.Single Spine.PointAttachment::y
	float ___y_2;
	// System.Single Spine.PointAttachment::rotation
	float ___rotation_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(PointAttachment_t2275020146, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(PointAttachment_t2275020146, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(PointAttachment_t2275020146, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTATTACHMENT_T2275020146_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef FORMAT_T2262486444_H
#define FORMAT_T2262486444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Format
struct  Format_t2262486444 
{
public:
	// System.Int32 Spine.Format::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Format_t2262486444, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMAT_T2262486444_H
#ifndef TEXTUREFILTER_T1610186802_H
#define TEXTUREFILTER_T1610186802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureFilter
struct  TextureFilter_t1610186802 
{
public:
	// System.Int32 Spine.TextureFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFilter_t1610186802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFILTER_T1610186802_H
#ifndef TOKEN_T2243639121_H
#define TOKEN_T2243639121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer/Token
struct  Token_t2243639121 
{
public:
	// System.Int32 SharpJson.Lexer/Token::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Token_t2243639121, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2243639121_H
#ifndef LAYERFIELDATTRIBUTE_T3408802261_H
#define LAYERFIELDATTRIBUTE_T3408802261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/LayerFieldAttribute
struct  LayerFieldAttribute_t3408802261  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERFIELDATTRIBUTE_T3408802261_H
#ifndef EVENTTYPE_T1835192406_H
#define EVENTTYPE_T1835192406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventType
struct  EventType_t1835192406 
{
public:
	// System.Int32 Spine.EventQueue/EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t1835192406, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T1835192406_H
#ifndef U3CFADEU3EC__ITERATOR0_T1290633589_H
#define U3CFADEU3EC__ITERATOR0_T1290633589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0
struct  U3CFadeU3Ec__Iterator0_t1290633589  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<t>__1
	int32_t ___U3CtU3E__1_0;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<breakout>__2
	bool ___U3CbreakoutU3E__2_1;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<c>__3
	Color32_t2600501292  ___U3CcU3E__3_2;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$this
	SkeletonGhostRenderer_t2445315009 * ___U24this_3;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U3CtU3E__1_0)); }
	inline int32_t get_U3CtU3E__1_0() const { return ___U3CtU3E__1_0; }
	inline int32_t* get_address_of_U3CtU3E__1_0() { return &___U3CtU3E__1_0; }
	inline void set_U3CtU3E__1_0(int32_t value)
	{
		___U3CtU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CbreakoutU3E__2_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U3CbreakoutU3E__2_1)); }
	inline bool get_U3CbreakoutU3E__2_1() const { return ___U3CbreakoutU3E__2_1; }
	inline bool* get_address_of_U3CbreakoutU3E__2_1() { return &___U3CbreakoutU3E__2_1; }
	inline void set_U3CbreakoutU3E__2_1(bool value)
	{
		___U3CbreakoutU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U3CcU3E__3_2)); }
	inline Color32_t2600501292  get_U3CcU3E__3_2() const { return ___U3CcU3E__3_2; }
	inline Color32_t2600501292 * get_address_of_U3CcU3E__3_2() { return &___U3CcU3E__3_2; }
	inline void set_U3CcU3E__3_2(Color32_t2600501292  value)
	{
		___U3CcU3E__3_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24this_3)); }
	inline SkeletonGhostRenderer_t2445315009 * get_U24this_3() const { return ___U24this_3; }
	inline SkeletonGhostRenderer_t2445315009 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SkeletonGhostRenderer_t2445315009 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3EC__ITERATOR0_T1290633589_H
#ifndef U3CFADEADDITIVEU3EC__ITERATOR1_T410398119_H
#define U3CFADEADDITIVEU3EC__ITERATOR1_T410398119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1
struct  U3CFadeAdditiveU3Ec__Iterator1_t410398119  : public RuntimeObject
{
public:
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<black>__0
	Color32_t2600501292  ___U3CblackU3E__0_0;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<t>__1
	int32_t ___U3CtU3E__1_1;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<breakout>__2
	bool ___U3CbreakoutU3E__2_2;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<c>__3
	Color32_t2600501292  ___U3CcU3E__3_3;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$this
	SkeletonGhostRenderer_t2445315009 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CblackU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CblackU3E__0_0)); }
	inline Color32_t2600501292  get_U3CblackU3E__0_0() const { return ___U3CblackU3E__0_0; }
	inline Color32_t2600501292 * get_address_of_U3CblackU3E__0_0() { return &___U3CblackU3E__0_0; }
	inline void set_U3CblackU3E__0_0(Color32_t2600501292  value)
	{
		___U3CblackU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_1() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CtU3E__1_1)); }
	inline int32_t get_U3CtU3E__1_1() const { return ___U3CtU3E__1_1; }
	inline int32_t* get_address_of_U3CtU3E__1_1() { return &___U3CtU3E__1_1; }
	inline void set_U3CtU3E__1_1(int32_t value)
	{
		___U3CtU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbreakoutU3E__2_2() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CbreakoutU3E__2_2)); }
	inline bool get_U3CbreakoutU3E__2_2() const { return ___U3CbreakoutU3E__2_2; }
	inline bool* get_address_of_U3CbreakoutU3E__2_2() { return &___U3CbreakoutU3E__2_2; }
	inline void set_U3CbreakoutU3E__2_2(bool value)
	{
		___U3CbreakoutU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_3() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CcU3E__3_3)); }
	inline Color32_t2600501292  get_U3CcU3E__3_3() const { return ___U3CcU3E__3_3; }
	inline Color32_t2600501292 * get_address_of_U3CcU3E__3_3() { return &___U3CcU3E__3_3; }
	inline void set_U3CcU3E__3_3(Color32_t2600501292  value)
	{
		___U3CcU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24this_4)); }
	inline SkeletonGhostRenderer_t2445315009 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonGhostRenderer_t2445315009 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonGhostRenderer_t2445315009 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEADDITIVEU3EC__ITERATOR1_T410398119_H
#ifndef TEXTUREWRAP_T2819935362_H
#define TEXTUREWRAP_T2819935362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureWrap
struct  TextureWrap_t2819935362 
{
public:
	// System.Int32 Spine.TextureWrap::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrap_t2819935362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAP_T2819935362_H
#ifndef MESHATTACHMENT_T1975337962_H
#define MESHATTACHMENT_T1975337962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MeshAttachment
struct  MeshAttachment_t1975337962  : public VertexAttachment_t4074366829
{
public:
	// System.Single Spine.MeshAttachment::regionOffsetX
	float ___regionOffsetX_7;
	// System.Single Spine.MeshAttachment::regionOffsetY
	float ___regionOffsetY_8;
	// System.Single Spine.MeshAttachment::regionWidth
	float ___regionWidth_9;
	// System.Single Spine.MeshAttachment::regionHeight
	float ___regionHeight_10;
	// System.Single Spine.MeshAttachment::regionOriginalWidth
	float ___regionOriginalWidth_11;
	// System.Single Spine.MeshAttachment::regionOriginalHeight
	float ___regionOriginalHeight_12;
	// Spine.MeshAttachment Spine.MeshAttachment::parentMesh
	MeshAttachment_t1975337962 * ___parentMesh_13;
	// System.Single[] Spine.MeshAttachment::uvs
	SingleU5BU5D_t1444911251* ___uvs_14;
	// System.Single[] Spine.MeshAttachment::regionUVs
	SingleU5BU5D_t1444911251* ___regionUVs_15;
	// System.Int32[] Spine.MeshAttachment::triangles
	Int32U5BU5D_t385246372* ___triangles_16;
	// System.Single Spine.MeshAttachment::r
	float ___r_17;
	// System.Single Spine.MeshAttachment::g
	float ___g_18;
	// System.Single Spine.MeshAttachment::b
	float ___b_19;
	// System.Single Spine.MeshAttachment::a
	float ___a_20;
	// System.Int32 Spine.MeshAttachment::hulllength
	int32_t ___hulllength_21;
	// System.Boolean Spine.MeshAttachment::inheritDeform
	bool ___inheritDeform_22;
	// System.String Spine.MeshAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_23;
	// System.Object Spine.MeshAttachment::RendererObject
	RuntimeObject * ___RendererObject_24;
	// System.Single Spine.MeshAttachment::<RegionU>k__BackingField
	float ___U3CRegionUU3Ek__BackingField_25;
	// System.Single Spine.MeshAttachment::<RegionV>k__BackingField
	float ___U3CRegionVU3Ek__BackingField_26;
	// System.Single Spine.MeshAttachment::<RegionU2>k__BackingField
	float ___U3CRegionU2U3Ek__BackingField_27;
	// System.Single Spine.MeshAttachment::<RegionV2>k__BackingField
	float ___U3CRegionV2U3Ek__BackingField_28;
	// System.Boolean Spine.MeshAttachment::<RegionRotate>k__BackingField
	bool ___U3CRegionRotateU3Ek__BackingField_29;
	// System.Int32[] Spine.MeshAttachment::<Edges>k__BackingField
	Int32U5BU5D_t385246372* ___U3CEdgesU3Ek__BackingField_30;
	// System.Single Spine.MeshAttachment::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_31;
	// System.Single Spine.MeshAttachment::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_regionOffsetX_7() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOffsetX_7)); }
	inline float get_regionOffsetX_7() const { return ___regionOffsetX_7; }
	inline float* get_address_of_regionOffsetX_7() { return &___regionOffsetX_7; }
	inline void set_regionOffsetX_7(float value)
	{
		___regionOffsetX_7 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_8() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOffsetY_8)); }
	inline float get_regionOffsetY_8() const { return ___regionOffsetY_8; }
	inline float* get_address_of_regionOffsetY_8() { return &___regionOffsetY_8; }
	inline void set_regionOffsetY_8(float value)
	{
		___regionOffsetY_8 = value;
	}

	inline static int32_t get_offset_of_regionWidth_9() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionWidth_9)); }
	inline float get_regionWidth_9() const { return ___regionWidth_9; }
	inline float* get_address_of_regionWidth_9() { return &___regionWidth_9; }
	inline void set_regionWidth_9(float value)
	{
		___regionWidth_9 = value;
	}

	inline static int32_t get_offset_of_regionHeight_10() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionHeight_10)); }
	inline float get_regionHeight_10() const { return ___regionHeight_10; }
	inline float* get_address_of_regionHeight_10() { return &___regionHeight_10; }
	inline void set_regionHeight_10(float value)
	{
		___regionHeight_10 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_11() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOriginalWidth_11)); }
	inline float get_regionOriginalWidth_11() const { return ___regionOriginalWidth_11; }
	inline float* get_address_of_regionOriginalWidth_11() { return &___regionOriginalWidth_11; }
	inline void set_regionOriginalWidth_11(float value)
	{
		___regionOriginalWidth_11 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_12() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOriginalHeight_12)); }
	inline float get_regionOriginalHeight_12() const { return ___regionOriginalHeight_12; }
	inline float* get_address_of_regionOriginalHeight_12() { return &___regionOriginalHeight_12; }
	inline void set_regionOriginalHeight_12(float value)
	{
		___regionOriginalHeight_12 = value;
	}

	inline static int32_t get_offset_of_parentMesh_13() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___parentMesh_13)); }
	inline MeshAttachment_t1975337962 * get_parentMesh_13() const { return ___parentMesh_13; }
	inline MeshAttachment_t1975337962 ** get_address_of_parentMesh_13() { return &___parentMesh_13; }
	inline void set_parentMesh_13(MeshAttachment_t1975337962 * value)
	{
		___parentMesh_13 = value;
		Il2CppCodeGenWriteBarrier((&___parentMesh_13), value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___uvs_14)); }
	inline SingleU5BU5D_t1444911251* get_uvs_14() const { return ___uvs_14; }
	inline SingleU5BU5D_t1444911251** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(SingleU5BU5D_t1444911251* value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_14), value);
	}

	inline static int32_t get_offset_of_regionUVs_15() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionUVs_15)); }
	inline SingleU5BU5D_t1444911251* get_regionUVs_15() const { return ___regionUVs_15; }
	inline SingleU5BU5D_t1444911251** get_address_of_regionUVs_15() { return &___regionUVs_15; }
	inline void set_regionUVs_15(SingleU5BU5D_t1444911251* value)
	{
		___regionUVs_15 = value;
		Il2CppCodeGenWriteBarrier((&___regionUVs_15), value);
	}

	inline static int32_t get_offset_of_triangles_16() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___triangles_16)); }
	inline Int32U5BU5D_t385246372* get_triangles_16() const { return ___triangles_16; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_16() { return &___triangles_16; }
	inline void set_triangles_16(Int32U5BU5D_t385246372* value)
	{
		___triangles_16 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_16), value);
	}

	inline static int32_t get_offset_of_r_17() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___r_17)); }
	inline float get_r_17() const { return ___r_17; }
	inline float* get_address_of_r_17() { return &___r_17; }
	inline void set_r_17(float value)
	{
		___r_17 = value;
	}

	inline static int32_t get_offset_of_g_18() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___g_18)); }
	inline float get_g_18() const { return ___g_18; }
	inline float* get_address_of_g_18() { return &___g_18; }
	inline void set_g_18(float value)
	{
		___g_18 = value;
	}

	inline static int32_t get_offset_of_b_19() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___b_19)); }
	inline float get_b_19() const { return ___b_19; }
	inline float* get_address_of_b_19() { return &___b_19; }
	inline void set_b_19(float value)
	{
		___b_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_hulllength_21() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___hulllength_21)); }
	inline int32_t get_hulllength_21() const { return ___hulllength_21; }
	inline int32_t* get_address_of_hulllength_21() { return &___hulllength_21; }
	inline void set_hulllength_21(int32_t value)
	{
		___hulllength_21 = value;
	}

	inline static int32_t get_offset_of_inheritDeform_22() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___inheritDeform_22)); }
	inline bool get_inheritDeform_22() const { return ___inheritDeform_22; }
	inline bool* get_address_of_inheritDeform_22() { return &___inheritDeform_22; }
	inline void set_inheritDeform_22(bool value)
	{
		___inheritDeform_22 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CPathU3Ek__BackingField_23)); }
	inline String_t* get_U3CPathU3Ek__BackingField_23() const { return ___U3CPathU3Ek__BackingField_23; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_23() { return &___U3CPathU3Ek__BackingField_23; }
	inline void set_U3CPathU3Ek__BackingField_23(String_t* value)
	{
		___U3CPathU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_RendererObject_24() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___RendererObject_24)); }
	inline RuntimeObject * get_RendererObject_24() const { return ___RendererObject_24; }
	inline RuntimeObject ** get_address_of_RendererObject_24() { return &___RendererObject_24; }
	inline void set_RendererObject_24(RuntimeObject * value)
	{
		___RendererObject_24 = value;
		Il2CppCodeGenWriteBarrier((&___RendererObject_24), value);
	}

	inline static int32_t get_offset_of_U3CRegionUU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionUU3Ek__BackingField_25)); }
	inline float get_U3CRegionUU3Ek__BackingField_25() const { return ___U3CRegionUU3Ek__BackingField_25; }
	inline float* get_address_of_U3CRegionUU3Ek__BackingField_25() { return &___U3CRegionUU3Ek__BackingField_25; }
	inline void set_U3CRegionUU3Ek__BackingField_25(float value)
	{
		___U3CRegionUU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CRegionVU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionVU3Ek__BackingField_26)); }
	inline float get_U3CRegionVU3Ek__BackingField_26() const { return ___U3CRegionVU3Ek__BackingField_26; }
	inline float* get_address_of_U3CRegionVU3Ek__BackingField_26() { return &___U3CRegionVU3Ek__BackingField_26; }
	inline void set_U3CRegionVU3Ek__BackingField_26(float value)
	{
		___U3CRegionVU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CRegionU2U3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionU2U3Ek__BackingField_27)); }
	inline float get_U3CRegionU2U3Ek__BackingField_27() const { return ___U3CRegionU2U3Ek__BackingField_27; }
	inline float* get_address_of_U3CRegionU2U3Ek__BackingField_27() { return &___U3CRegionU2U3Ek__BackingField_27; }
	inline void set_U3CRegionU2U3Ek__BackingField_27(float value)
	{
		___U3CRegionU2U3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CRegionV2U3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionV2U3Ek__BackingField_28)); }
	inline float get_U3CRegionV2U3Ek__BackingField_28() const { return ___U3CRegionV2U3Ek__BackingField_28; }
	inline float* get_address_of_U3CRegionV2U3Ek__BackingField_28() { return &___U3CRegionV2U3Ek__BackingField_28; }
	inline void set_U3CRegionV2U3Ek__BackingField_28(float value)
	{
		___U3CRegionV2U3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CRegionRotateU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionRotateU3Ek__BackingField_29)); }
	inline bool get_U3CRegionRotateU3Ek__BackingField_29() const { return ___U3CRegionRotateU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CRegionRotateU3Ek__BackingField_29() { return &___U3CRegionRotateU3Ek__BackingField_29; }
	inline void set_U3CRegionRotateU3Ek__BackingField_29(bool value)
	{
		___U3CRegionRotateU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CEdgesU3Ek__BackingField_30)); }
	inline Int32U5BU5D_t385246372* get_U3CEdgesU3Ek__BackingField_30() const { return ___U3CEdgesU3Ek__BackingField_30; }
	inline Int32U5BU5D_t385246372** get_address_of_U3CEdgesU3Ek__BackingField_30() { return &___U3CEdgesU3Ek__BackingField_30; }
	inline void set_U3CEdgesU3Ek__BackingField_30(Int32U5BU5D_t385246372* value)
	{
		___U3CEdgesU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CWidthU3Ek__BackingField_31)); }
	inline float get_U3CWidthU3Ek__BackingField_31() const { return ___U3CWidthU3Ek__BackingField_31; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_31() { return &___U3CWidthU3Ek__BackingField_31; }
	inline void set_U3CWidthU3Ek__BackingField_31(float value)
	{
		___U3CWidthU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CHeightU3Ek__BackingField_32)); }
	inline float get_U3CHeightU3Ek__BackingField_32() const { return ___U3CHeightU3Ek__BackingField_32; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_32() { return &___U3CHeightU3Ek__BackingField_32; }
	inline void set_U3CHeightU3Ek__BackingField_32(float value)
	{
		___U3CHeightU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHATTACHMENT_T1975337962_H
#ifndef PATHATTACHMENT_T3565151060_H
#define PATHATTACHMENT_T3565151060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathAttachment
struct  PathAttachment_t3565151060  : public VertexAttachment_t4074366829
{
public:
	// System.Single[] Spine.PathAttachment::lengths
	SingleU5BU5D_t1444911251* ___lengths_7;
	// System.Boolean Spine.PathAttachment::closed
	bool ___closed_8;
	// System.Boolean Spine.PathAttachment::constantSpeed
	bool ___constantSpeed_9;

public:
	inline static int32_t get_offset_of_lengths_7() { return static_cast<int32_t>(offsetof(PathAttachment_t3565151060, ___lengths_7)); }
	inline SingleU5BU5D_t1444911251* get_lengths_7() const { return ___lengths_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_lengths_7() { return &___lengths_7; }
	inline void set_lengths_7(SingleU5BU5D_t1444911251* value)
	{
		___lengths_7 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_7), value);
	}

	inline static int32_t get_offset_of_closed_8() { return static_cast<int32_t>(offsetof(PathAttachment_t3565151060, ___closed_8)); }
	inline bool get_closed_8() const { return ___closed_8; }
	inline bool* get_address_of_closed_8() { return &___closed_8; }
	inline void set_closed_8(bool value)
	{
		___closed_8 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_9() { return static_cast<int32_t>(offsetof(PathAttachment_t3565151060, ___constantSpeed_9)); }
	inline bool get_constantSpeed_9() const { return ___constantSpeed_9; }
	inline bool* get_address_of_constantSpeed_9() { return &___constantSpeed_9; }
	inline void set_constantSpeed_9(bool value)
	{
		___constantSpeed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHATTACHMENT_T3565151060_H
#ifndef BLENDMODE_T358635744_H
#define BLENDMODE_T358635744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t358635744 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendMode_t358635744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T358635744_H
#ifndef POSITIONMODE_T2435325583_H
#define POSITIONMODE_T2435325583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PositionMode
struct  PositionMode_t2435325583 
{
public:
	// System.Int32 Spine.PositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PositionMode_t2435325583, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONMODE_T2435325583_H
#ifndef SPACINGMODE_T3020059698_H
#define SPACINGMODE_T3020059698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SpacingMode
struct  SpacingMode_t3020059698 
{
public:
	// System.Int32 Spine.SpacingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpacingMode_t3020059698, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACINGMODE_T3020059698_H
#ifndef TRANSFORMMODE_T614987360_H
#define TRANSFORMMODE_T614987360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformMode
struct  TransformMode_t614987360 
{
public:
	// System.Int32 Spine.TransformMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformMode_t614987360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMODE_T614987360_H
#ifndef CLIPPINGATTACHMENT_T2586274570_H
#define CLIPPINGATTACHMENT_T2586274570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ClippingAttachment
struct  ClippingAttachment_t2586274570  : public VertexAttachment_t4074366829
{
public:
	// Spine.SlotData Spine.ClippingAttachment::endSlot
	SlotData_t154801902 * ___endSlot_7;

public:
	inline static int32_t get_offset_of_endSlot_7() { return static_cast<int32_t>(offsetof(ClippingAttachment_t2586274570, ___endSlot_7)); }
	inline SlotData_t154801902 * get_endSlot_7() const { return ___endSlot_7; }
	inline SlotData_t154801902 ** get_address_of_endSlot_7() { return &___endSlot_7; }
	inline void set_endSlot_7(SlotData_t154801902 * value)
	{
		___endSlot_7 = value;
		Il2CppCodeGenWriteBarrier((&___endSlot_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPINGATTACHMENT_T2586274570_H
#ifndef MESHGENERATOR_T1354683548_H
#define MESHGENERATOR_T1354683548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator
struct  MeshGenerator_t1354683548  : public RuntimeObject
{
public:
	// Spine.Unity.MeshGenerator/Settings Spine.Unity.MeshGenerator::settings
	Settings_t612870457  ___settings_0;
	// Spine.ExposedList`1<UnityEngine.Vector3> Spine.Unity.MeshGenerator::vertexBuffer
	ExposedList_1_t2134458034 * ___vertexBuffer_3;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uvBuffer
	ExposedList_1_t568374093 * ___uvBuffer_4;
	// Spine.ExposedList`1<UnityEngine.Color32> Spine.Unity.MeshGenerator::colorBuffer
	ExposedList_1_t1012645862 * ___colorBuffer_5;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Unity.MeshGenerator::submeshes
	ExposedList_1_t4070202189 * ___submeshes_6;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMin
	Vector2_t2156229523  ___meshBoundsMin_7;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMax
	Vector2_t2156229523  ___meshBoundsMax_8;
	// System.Single Spine.Unity.MeshGenerator::meshBoundsThickness
	float ___meshBoundsThickness_9;
	// System.Int32 Spine.Unity.MeshGenerator::submeshIndex
	int32_t ___submeshIndex_10;
	// Spine.SkeletonClipping Spine.Unity.MeshGenerator::clipper
	SkeletonClipping_t1669006083 * ___clipper_11;
	// System.Single[] Spine.Unity.MeshGenerator::tempVerts
	SingleU5BU5D_t1444911251* ___tempVerts_12;
	// System.Int32[] Spine.Unity.MeshGenerator::regionTriangles
	Int32U5BU5D_t385246372* ___regionTriangles_13;
	// UnityEngine.Vector3[] Spine.Unity.MeshGenerator::normals
	Vector3U5BU5D_t1718750761* ___normals_14;
	// UnityEngine.Vector4[] Spine.Unity.MeshGenerator::tangents
	Vector4U5BU5D_t934056436* ___tangents_15;
	// UnityEngine.Vector2[] Spine.Unity.MeshGenerator::tempTanBuffer
	Vector2U5BU5D_t1457185986* ___tempTanBuffer_16;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv2
	ExposedList_1_t568374093 * ___uv2_17;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv3
	ExposedList_1_t568374093 * ___uv3_18;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___settings_0)); }
	inline Settings_t612870457  get_settings_0() const { return ___settings_0; }
	inline Settings_t612870457 * get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(Settings_t612870457  value)
	{
		___settings_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_3() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___vertexBuffer_3)); }
	inline ExposedList_1_t2134458034 * get_vertexBuffer_3() const { return ___vertexBuffer_3; }
	inline ExposedList_1_t2134458034 ** get_address_of_vertexBuffer_3() { return &___vertexBuffer_3; }
	inline void set_vertexBuffer_3(ExposedList_1_t2134458034 * value)
	{
		___vertexBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_3), value);
	}

	inline static int32_t get_offset_of_uvBuffer_4() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___uvBuffer_4)); }
	inline ExposedList_1_t568374093 * get_uvBuffer_4() const { return ___uvBuffer_4; }
	inline ExposedList_1_t568374093 ** get_address_of_uvBuffer_4() { return &___uvBuffer_4; }
	inline void set_uvBuffer_4(ExposedList_1_t568374093 * value)
	{
		___uvBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_4), value);
	}

	inline static int32_t get_offset_of_colorBuffer_5() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___colorBuffer_5)); }
	inline ExposedList_1_t1012645862 * get_colorBuffer_5() const { return ___colorBuffer_5; }
	inline ExposedList_1_t1012645862 ** get_address_of_colorBuffer_5() { return &___colorBuffer_5; }
	inline void set_colorBuffer_5(ExposedList_1_t1012645862 * value)
	{
		___colorBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_5), value);
	}

	inline static int32_t get_offset_of_submeshes_6() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___submeshes_6)); }
	inline ExposedList_1_t4070202189 * get_submeshes_6() const { return ___submeshes_6; }
	inline ExposedList_1_t4070202189 ** get_address_of_submeshes_6() { return &___submeshes_6; }
	inline void set_submeshes_6(ExposedList_1_t4070202189 * value)
	{
		___submeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___submeshes_6), value);
	}

	inline static int32_t get_offset_of_meshBoundsMin_7() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___meshBoundsMin_7)); }
	inline Vector2_t2156229523  get_meshBoundsMin_7() const { return ___meshBoundsMin_7; }
	inline Vector2_t2156229523 * get_address_of_meshBoundsMin_7() { return &___meshBoundsMin_7; }
	inline void set_meshBoundsMin_7(Vector2_t2156229523  value)
	{
		___meshBoundsMin_7 = value;
	}

	inline static int32_t get_offset_of_meshBoundsMax_8() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___meshBoundsMax_8)); }
	inline Vector2_t2156229523  get_meshBoundsMax_8() const { return ___meshBoundsMax_8; }
	inline Vector2_t2156229523 * get_address_of_meshBoundsMax_8() { return &___meshBoundsMax_8; }
	inline void set_meshBoundsMax_8(Vector2_t2156229523  value)
	{
		___meshBoundsMax_8 = value;
	}

	inline static int32_t get_offset_of_meshBoundsThickness_9() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___meshBoundsThickness_9)); }
	inline float get_meshBoundsThickness_9() const { return ___meshBoundsThickness_9; }
	inline float* get_address_of_meshBoundsThickness_9() { return &___meshBoundsThickness_9; }
	inline void set_meshBoundsThickness_9(float value)
	{
		___meshBoundsThickness_9 = value;
	}

	inline static int32_t get_offset_of_submeshIndex_10() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___submeshIndex_10)); }
	inline int32_t get_submeshIndex_10() const { return ___submeshIndex_10; }
	inline int32_t* get_address_of_submeshIndex_10() { return &___submeshIndex_10; }
	inline void set_submeshIndex_10(int32_t value)
	{
		___submeshIndex_10 = value;
	}

	inline static int32_t get_offset_of_clipper_11() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___clipper_11)); }
	inline SkeletonClipping_t1669006083 * get_clipper_11() const { return ___clipper_11; }
	inline SkeletonClipping_t1669006083 ** get_address_of_clipper_11() { return &___clipper_11; }
	inline void set_clipper_11(SkeletonClipping_t1669006083 * value)
	{
		___clipper_11 = value;
		Il2CppCodeGenWriteBarrier((&___clipper_11), value);
	}

	inline static int32_t get_offset_of_tempVerts_12() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___tempVerts_12)); }
	inline SingleU5BU5D_t1444911251* get_tempVerts_12() const { return ___tempVerts_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_tempVerts_12() { return &___tempVerts_12; }
	inline void set_tempVerts_12(SingleU5BU5D_t1444911251* value)
	{
		___tempVerts_12 = value;
		Il2CppCodeGenWriteBarrier((&___tempVerts_12), value);
	}

	inline static int32_t get_offset_of_regionTriangles_13() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___regionTriangles_13)); }
	inline Int32U5BU5D_t385246372* get_regionTriangles_13() const { return ___regionTriangles_13; }
	inline Int32U5BU5D_t385246372** get_address_of_regionTriangles_13() { return &___regionTriangles_13; }
	inline void set_regionTriangles_13(Int32U5BU5D_t385246372* value)
	{
		___regionTriangles_13 = value;
		Il2CppCodeGenWriteBarrier((&___regionTriangles_13), value);
	}

	inline static int32_t get_offset_of_normals_14() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___normals_14)); }
	inline Vector3U5BU5D_t1718750761* get_normals_14() const { return ___normals_14; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_14() { return &___normals_14; }
	inline void set_normals_14(Vector3U5BU5D_t1718750761* value)
	{
		___normals_14 = value;
		Il2CppCodeGenWriteBarrier((&___normals_14), value);
	}

	inline static int32_t get_offset_of_tangents_15() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___tangents_15)); }
	inline Vector4U5BU5D_t934056436* get_tangents_15() const { return ___tangents_15; }
	inline Vector4U5BU5D_t934056436** get_address_of_tangents_15() { return &___tangents_15; }
	inline void set_tangents_15(Vector4U5BU5D_t934056436* value)
	{
		___tangents_15 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_15), value);
	}

	inline static int32_t get_offset_of_tempTanBuffer_16() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___tempTanBuffer_16)); }
	inline Vector2U5BU5D_t1457185986* get_tempTanBuffer_16() const { return ___tempTanBuffer_16; }
	inline Vector2U5BU5D_t1457185986** get_address_of_tempTanBuffer_16() { return &___tempTanBuffer_16; }
	inline void set_tempTanBuffer_16(Vector2U5BU5D_t1457185986* value)
	{
		___tempTanBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&___tempTanBuffer_16), value);
	}

	inline static int32_t get_offset_of_uv2_17() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___uv2_17)); }
	inline ExposedList_1_t568374093 * get_uv2_17() const { return ___uv2_17; }
	inline ExposedList_1_t568374093 ** get_address_of_uv2_17() { return &___uv2_17; }
	inline void set_uv2_17(ExposedList_1_t568374093 * value)
	{
		___uv2_17 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_17), value);
	}

	inline static int32_t get_offset_of_uv3_18() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___uv3_18)); }
	inline ExposedList_1_t568374093 * get_uv3_18() const { return ___uv3_18; }
	inline ExposedList_1_t568374093 ** get_address_of_uv3_18() { return &___uv3_18; }
	inline void set_uv3_18(ExposedList_1_t568374093 * value)
	{
		___uv3_18 = value;
		Il2CppCodeGenWriteBarrier((&___uv3_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATOR_T1354683548_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef ROTATEMODE_T2880286220_H
#define ROTATEMODE_T2880286220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateMode
struct  RotateMode_t2880286220 
{
public:
	// System.Int32 Spine.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2880286220, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2880286220_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ATTACHMENTTYPE_T1460157544_H
#define ATTACHMENTTYPE_T1460157544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentType
struct  AttachmentType_t1460157544 
{
public:
	// System.Int32 Spine.AttachmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttachmentType_t1460157544, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTYPE_T1460157544_H
#ifndef BOUNDINGBOXATTACHMENT_T2797506510_H
#define BOUNDINGBOXATTACHMENT_T2797506510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoundingBoxAttachment
struct  BoundingBoxAttachment_t2797506510  : public VertexAttachment_t4074366829
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXATTACHMENT_T2797506510_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef EVENTQUEUEENTRY_T351831961_H
#define EVENTQUEUEENTRY_T351831961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventQueueEntry
struct  EventQueueEntry_t351831961 
{
public:
	// Spine.EventQueue/EventType Spine.EventQueue/EventQueueEntry::type
	int32_t ___type_0;
	// Spine.TrackEntry Spine.EventQueue/EventQueueEntry::entry
	TrackEntry_t1316488441 * ___entry_1;
	// Spine.Event Spine.EventQueue/EventQueueEntry::e
	Event_t1378573841 * ___e_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(EventQueueEntry_t351831961, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_entry_1() { return static_cast<int32_t>(offsetof(EventQueueEntry_t351831961, ___entry_1)); }
	inline TrackEntry_t1316488441 * get_entry_1() const { return ___entry_1; }
	inline TrackEntry_t1316488441 ** get_address_of_entry_1() { return &___entry_1; }
	inline void set_entry_1(TrackEntry_t1316488441 * value)
	{
		___entry_1 = value;
		Il2CppCodeGenWriteBarrier((&___entry_1), value);
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(EventQueueEntry_t351831961, ___e_2)); }
	inline Event_t1378573841 * get_e_2() const { return ___e_2; }
	inline Event_t1378573841 ** get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(Event_t1378573841 * value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((&___e_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t351831961_marshaled_pinvoke
{
	int32_t ___type_0;
	TrackEntry_t1316488441 * ___entry_1;
	Event_t1378573841 * ___e_2;
};
// Native definition for COM marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t351831961_marshaled_com
{
	int32_t ___type_0;
	TrackEntry_t1316488441 * ___entry_1;
	Event_t1378573841 * ___e_2;
};
#endif // EVENTQUEUEENTRY_T351831961_H
#ifndef SPINEMESH_T2128742078_H
#define SPINEMESH_T2128742078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineMesh
struct  SpineMesh_t2128742078  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEMESH_T2128742078_H
#ifndef ATLASPAGE_T4077017671_H
#define ATLASPAGE_T4077017671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasPage
struct  AtlasPage_t4077017671  : public RuntimeObject
{
public:
	// System.String Spine.AtlasPage::name
	String_t* ___name_0;
	// Spine.Format Spine.AtlasPage::format
	int32_t ___format_1;
	// Spine.TextureFilter Spine.AtlasPage::minFilter
	int32_t ___minFilter_2;
	// Spine.TextureFilter Spine.AtlasPage::magFilter
	int32_t ___magFilter_3;
	// Spine.TextureWrap Spine.AtlasPage::uWrap
	int32_t ___uWrap_4;
	// Spine.TextureWrap Spine.AtlasPage::vWrap
	int32_t ___vWrap_5;
	// System.Object Spine.AtlasPage::rendererObject
	RuntimeObject * ___rendererObject_6;
	// System.Int32 Spine.AtlasPage::width
	int32_t ___width_7;
	// System.Int32 Spine.AtlasPage::height
	int32_t ___height_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_format_1() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___format_1)); }
	inline int32_t get_format_1() const { return ___format_1; }
	inline int32_t* get_address_of_format_1() { return &___format_1; }
	inline void set_format_1(int32_t value)
	{
		___format_1 = value;
	}

	inline static int32_t get_offset_of_minFilter_2() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___minFilter_2)); }
	inline int32_t get_minFilter_2() const { return ___minFilter_2; }
	inline int32_t* get_address_of_minFilter_2() { return &___minFilter_2; }
	inline void set_minFilter_2(int32_t value)
	{
		___minFilter_2 = value;
	}

	inline static int32_t get_offset_of_magFilter_3() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___magFilter_3)); }
	inline int32_t get_magFilter_3() const { return ___magFilter_3; }
	inline int32_t* get_address_of_magFilter_3() { return &___magFilter_3; }
	inline void set_magFilter_3(int32_t value)
	{
		___magFilter_3 = value;
	}

	inline static int32_t get_offset_of_uWrap_4() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___uWrap_4)); }
	inline int32_t get_uWrap_4() const { return ___uWrap_4; }
	inline int32_t* get_address_of_uWrap_4() { return &___uWrap_4; }
	inline void set_uWrap_4(int32_t value)
	{
		___uWrap_4 = value;
	}

	inline static int32_t get_offset_of_vWrap_5() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___vWrap_5)); }
	inline int32_t get_vWrap_5() const { return ___vWrap_5; }
	inline int32_t* get_address_of_vWrap_5() { return &___vWrap_5; }
	inline void set_vWrap_5(int32_t value)
	{
		___vWrap_5 = value;
	}

	inline static int32_t get_offset_of_rendererObject_6() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___rendererObject_6)); }
	inline RuntimeObject * get_rendererObject_6() const { return ___rendererObject_6; }
	inline RuntimeObject ** get_address_of_rendererObject_6() { return &___rendererObject_6; }
	inline void set_rendererObject_6(RuntimeObject * value)
	{
		___rendererObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___rendererObject_6), value);
	}

	inline static int32_t get_offset_of_width_7() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___width_7)); }
	inline int32_t get_width_7() const { return ___width_7; }
	inline int32_t* get_address_of_width_7() { return &___width_7; }
	inline void set_width_7(int32_t value)
	{
		___width_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___height_8)); }
	inline int32_t get_height_8() const { return ___height_8; }
	inline int32_t* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(int32_t value)
	{
		___height_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPAGE_T4077017671_H
#ifndef BONEDATA_T3130174490_H
#define BONEDATA_T3130174490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoneData
struct  BoneData_t3130174490  : public RuntimeObject
{
public:
	// System.Int32 Spine.BoneData::index
	int32_t ___index_0;
	// System.String Spine.BoneData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.BoneData::parent
	BoneData_t3130174490 * ___parent_2;
	// System.Single Spine.BoneData::length
	float ___length_3;
	// System.Single Spine.BoneData::x
	float ___x_4;
	// System.Single Spine.BoneData::y
	float ___y_5;
	// System.Single Spine.BoneData::rotation
	float ___rotation_6;
	// System.Single Spine.BoneData::scaleX
	float ___scaleX_7;
	// System.Single Spine.BoneData::scaleY
	float ___scaleY_8;
	// System.Single Spine.BoneData::shearX
	float ___shearX_9;
	// System.Single Spine.BoneData::shearY
	float ___shearY_10;
	// Spine.TransformMode Spine.BoneData::transformMode
	int32_t ___transformMode_11;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___parent_2)); }
	inline BoneData_t3130174490 * get_parent_2() const { return ___parent_2; }
	inline BoneData_t3130174490 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BoneData_t3130174490 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___rotation_6)); }
	inline float get_rotation_6() const { return ___rotation_6; }
	inline float* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(float value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scaleX_7() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___scaleX_7)); }
	inline float get_scaleX_7() const { return ___scaleX_7; }
	inline float* get_address_of_scaleX_7() { return &___scaleX_7; }
	inline void set_scaleX_7(float value)
	{
		___scaleX_7 = value;
	}

	inline static int32_t get_offset_of_scaleY_8() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___scaleY_8)); }
	inline float get_scaleY_8() const { return ___scaleY_8; }
	inline float* get_address_of_scaleY_8() { return &___scaleY_8; }
	inline void set_scaleY_8(float value)
	{
		___scaleY_8 = value;
	}

	inline static int32_t get_offset_of_shearX_9() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___shearX_9)); }
	inline float get_shearX_9() const { return ___shearX_9; }
	inline float* get_address_of_shearX_9() { return &___shearX_9; }
	inline void set_shearX_9(float value)
	{
		___shearX_9 = value;
	}

	inline static int32_t get_offset_of_shearY_10() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___shearY_10)); }
	inline float get_shearY_10() const { return ___shearY_10; }
	inline float* get_address_of_shearY_10() { return &___shearY_10; }
	inline void set_shearY_10(float value)
	{
		___shearY_10 = value;
	}

	inline static int32_t get_offset_of_transformMode_11() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___transformMode_11)); }
	inline int32_t get_transformMode_11() const { return ___transformMode_11; }
	inline int32_t* get_address_of_transformMode_11() { return &___transformMode_11; }
	inline void set_transformMode_11(int32_t value)
	{
		___transformMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEDATA_T3130174490_H
#ifndef PATHCONSTRAINTDATA_T981297034_H
#define PATHCONSTRAINTDATA_T981297034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintData
struct  PathConstraintData_t981297034  : public RuntimeObject
{
public:
	// System.String Spine.PathConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.PathConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.PathConstraintData::bones
	ExposedList_1_t1542319060 * ___bones_2;
	// Spine.SlotData Spine.PathConstraintData::target
	SlotData_t154801902 * ___target_3;
	// Spine.PositionMode Spine.PathConstraintData::positionMode
	int32_t ___positionMode_4;
	// Spine.SpacingMode Spine.PathConstraintData::spacingMode
	int32_t ___spacingMode_5;
	// Spine.RotateMode Spine.PathConstraintData::rotateMode
	int32_t ___rotateMode_6;
	// System.Single Spine.PathConstraintData::offsetRotation
	float ___offsetRotation_7;
	// System.Single Spine.PathConstraintData::position
	float ___position_8;
	// System.Single Spine.PathConstraintData::spacing
	float ___spacing_9;
	// System.Single Spine.PathConstraintData::rotateMix
	float ___rotateMix_10;
	// System.Single Spine.PathConstraintData::translateMix
	float ___translateMix_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___bones_2)); }
	inline ExposedList_1_t1542319060 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t1542319060 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t1542319060 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___target_3)); }
	inline SlotData_t154801902 * get_target_3() const { return ___target_3; }
	inline SlotData_t154801902 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(SlotData_t154801902 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_positionMode_4() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___positionMode_4)); }
	inline int32_t get_positionMode_4() const { return ___positionMode_4; }
	inline int32_t* get_address_of_positionMode_4() { return &___positionMode_4; }
	inline void set_positionMode_4(int32_t value)
	{
		___positionMode_4 = value;
	}

	inline static int32_t get_offset_of_spacingMode_5() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___spacingMode_5)); }
	inline int32_t get_spacingMode_5() const { return ___spacingMode_5; }
	inline int32_t* get_address_of_spacingMode_5() { return &___spacingMode_5; }
	inline void set_spacingMode_5(int32_t value)
	{
		___spacingMode_5 = value;
	}

	inline static int32_t get_offset_of_rotateMode_6() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___rotateMode_6)); }
	inline int32_t get_rotateMode_6() const { return ___rotateMode_6; }
	inline int32_t* get_address_of_rotateMode_6() { return &___rotateMode_6; }
	inline void set_rotateMode_6(int32_t value)
	{
		___rotateMode_6 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_7() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___offsetRotation_7)); }
	inline float get_offsetRotation_7() const { return ___offsetRotation_7; }
	inline float* get_address_of_offsetRotation_7() { return &___offsetRotation_7; }
	inline void set_offsetRotation_7(float value)
	{
		___offsetRotation_7 = value;
	}

	inline static int32_t get_offset_of_position_8() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___position_8)); }
	inline float get_position_8() const { return ___position_8; }
	inline float* get_address_of_position_8() { return &___position_8; }
	inline void set_position_8(float value)
	{
		___position_8 = value;
	}

	inline static int32_t get_offset_of_spacing_9() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___spacing_9)); }
	inline float get_spacing_9() const { return ___spacing_9; }
	inline float* get_address_of_spacing_9() { return &___spacing_9; }
	inline void set_spacing_9(float value)
	{
		___spacing_9 = value;
	}

	inline static int32_t get_offset_of_rotateMix_10() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___rotateMix_10)); }
	inline float get_rotateMix_10() const { return ___rotateMix_10; }
	inline float* get_address_of_rotateMix_10() { return &___rotateMix_10; }
	inline void set_rotateMix_10(float value)
	{
		___rotateMix_10 = value;
	}

	inline static int32_t get_offset_of_translateMix_11() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___translateMix_11)); }
	inline float get_translateMix_11() const { return ___translateMix_11; }
	inline float* get_address_of_translateMix_11() { return &___translateMix_11; }
	inline void set_translateMix_11(float value)
	{
		___translateMix_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTDATA_T981297034_H
#ifndef SLOTDATA_T154801902_H
#define SLOTDATA_T154801902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SlotData
struct  SlotData_t154801902  : public RuntimeObject
{
public:
	// System.Int32 Spine.SlotData::index
	int32_t ___index_0;
	// System.String Spine.SlotData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.SlotData::boneData
	BoneData_t3130174490 * ___boneData_2;
	// System.Single Spine.SlotData::r
	float ___r_3;
	// System.Single Spine.SlotData::g
	float ___g_4;
	// System.Single Spine.SlotData::b
	float ___b_5;
	// System.Single Spine.SlotData::a
	float ___a_6;
	// System.Single Spine.SlotData::r2
	float ___r2_7;
	// System.Single Spine.SlotData::g2
	float ___g2_8;
	// System.Single Spine.SlotData::b2
	float ___b2_9;
	// System.Boolean Spine.SlotData::hasSecondColor
	bool ___hasSecondColor_10;
	// System.String Spine.SlotData::attachmentName
	String_t* ___attachmentName_11;
	// Spine.BlendMode Spine.SlotData::blendMode
	int32_t ___blendMode_12;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_boneData_2() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___boneData_2)); }
	inline BoneData_t3130174490 * get_boneData_2() const { return ___boneData_2; }
	inline BoneData_t3130174490 ** get_address_of_boneData_2() { return &___boneData_2; }
	inline void set_boneData_2(BoneData_t3130174490 * value)
	{
		___boneData_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneData_2), value);
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___r_3)); }
	inline float get_r_3() const { return ___r_3; }
	inline float* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(float value)
	{
		___r_3 = value;
	}

	inline static int32_t get_offset_of_g_4() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___g_4)); }
	inline float get_g_4() const { return ___g_4; }
	inline float* get_address_of_g_4() { return &___g_4; }
	inline void set_g_4(float value)
	{
		___g_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___b_5)); }
	inline float get_b_5() const { return ___b_5; }
	inline float* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(float value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___a_6)); }
	inline float get_a_6() const { return ___a_6; }
	inline float* get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(float value)
	{
		___a_6 = value;
	}

	inline static int32_t get_offset_of_r2_7() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___r2_7)); }
	inline float get_r2_7() const { return ___r2_7; }
	inline float* get_address_of_r2_7() { return &___r2_7; }
	inline void set_r2_7(float value)
	{
		___r2_7 = value;
	}

	inline static int32_t get_offset_of_g2_8() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___g2_8)); }
	inline float get_g2_8() const { return ___g2_8; }
	inline float* get_address_of_g2_8() { return &___g2_8; }
	inline void set_g2_8(float value)
	{
		___g2_8 = value;
	}

	inline static int32_t get_offset_of_b2_9() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___b2_9)); }
	inline float get_b2_9() const { return ___b2_9; }
	inline float* get_address_of_b2_9() { return &___b2_9; }
	inline void set_b2_9(float value)
	{
		___b2_9 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_10() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___hasSecondColor_10)); }
	inline bool get_hasSecondColor_10() const { return ___hasSecondColor_10; }
	inline bool* get_address_of_hasSecondColor_10() { return &___hasSecondColor_10; }
	inline void set_hasSecondColor_10(bool value)
	{
		___hasSecondColor_10 = value;
	}

	inline static int32_t get_offset_of_attachmentName_11() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___attachmentName_11)); }
	inline String_t* get_attachmentName_11() const { return ___attachmentName_11; }
	inline String_t** get_address_of_attachmentName_11() { return &___attachmentName_11; }
	inline void set_attachmentName_11(String_t* value)
	{
		___attachmentName_11 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentName_11), value);
	}

	inline static int32_t get_offset_of_blendMode_12() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___blendMode_12)); }
	inline int32_t get_blendMode_12() const { return ___blendMode_12; }
	inline int32_t* get_address_of_blendMode_12() { return &___blendMode_12; }
	inline void set_blendMode_12(int32_t value)
	{
		___blendMode_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTDATA_T154801902_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ATLASUTILITIES_T1517471311_H
#define ATLASUTILITIES_T1517471311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AtlasUtilities
struct  AtlasUtilities_t1517471311  : public RuntimeObject
{
public:

public:
};

struct AtlasUtilities_t1517471311_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTextures
	Dictionary_2_t47542197 * ___CachedRegionTextures_4;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTexturesList
	List_1_t1017553631 * ___CachedRegionTexturesList_5;

public:
	inline static int32_t get_offset_of_CachedRegionTextures_4() { return static_cast<int32_t>(offsetof(AtlasUtilities_t1517471311_StaticFields, ___CachedRegionTextures_4)); }
	inline Dictionary_2_t47542197 * get_CachedRegionTextures_4() const { return ___CachedRegionTextures_4; }
	inline Dictionary_2_t47542197 ** get_address_of_CachedRegionTextures_4() { return &___CachedRegionTextures_4; }
	inline void set_CachedRegionTextures_4(Dictionary_2_t47542197 * value)
	{
		___CachedRegionTextures_4 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTextures_4), value);
	}

	inline static int32_t get_offset_of_CachedRegionTexturesList_5() { return static_cast<int32_t>(offsetof(AtlasUtilities_t1517471311_StaticFields, ___CachedRegionTexturesList_5)); }
	inline List_1_t1017553631 * get_CachedRegionTexturesList_5() const { return ___CachedRegionTexturesList_5; }
	inline List_1_t1017553631 ** get_address_of_CachedRegionTexturesList_5() { return &___CachedRegionTexturesList_5; }
	inline void set_CachedRegionTexturesList_5(List_1_t1017553631 * value)
	{
		___CachedRegionTexturesList_5 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTexturesList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASUTILITIES_T1517471311_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ATLASASSET_T1167231206_H
#define ATLASASSET_T1167231206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AtlasAsset
struct  AtlasAsset_t1167231206  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.TextAsset Spine.Unity.AtlasAsset::atlasFile
	TextAsset_t3022178571 * ___atlasFile_2;
	// UnityEngine.Material[] Spine.Unity.AtlasAsset::materials
	MaterialU5BU5D_t561872642* ___materials_3;
	// Spine.Atlas Spine.Unity.AtlasAsset::atlas
	Atlas_t4040192941 * ___atlas_4;

public:
	inline static int32_t get_offset_of_atlasFile_2() { return static_cast<int32_t>(offsetof(AtlasAsset_t1167231206, ___atlasFile_2)); }
	inline TextAsset_t3022178571 * get_atlasFile_2() const { return ___atlasFile_2; }
	inline TextAsset_t3022178571 ** get_address_of_atlasFile_2() { return &___atlasFile_2; }
	inline void set_atlasFile_2(TextAsset_t3022178571 * value)
	{
		___atlasFile_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasFile_2), value);
	}

	inline static int32_t get_offset_of_materials_3() { return static_cast<int32_t>(offsetof(AtlasAsset_t1167231206, ___materials_3)); }
	inline MaterialU5BU5D_t561872642* get_materials_3() const { return ___materials_3; }
	inline MaterialU5BU5D_t561872642** get_address_of_materials_3() { return &___materials_3; }
	inline void set_materials_3(MaterialU5BU5D_t561872642* value)
	{
		___materials_3 = value;
		Il2CppCodeGenWriteBarrier((&___materials_3), value);
	}

	inline static int32_t get_offset_of_atlas_4() { return static_cast<int32_t>(offsetof(AtlasAsset_t1167231206, ___atlas_4)); }
	inline Atlas_t4040192941 * get_atlas_4() const { return ___atlas_4; }
	inline Atlas_t4040192941 ** get_address_of_atlas_4() { return &___atlas_4; }
	inline void set_atlas_4(Atlas_t4040192941 * value)
	{
		___atlas_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASASSET_T1167231206_H
#ifndef SKELETONDATAASSET_T3748144825_H
#define SKELETONDATAASSET_T3748144825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonDataAsset
struct  SkeletonDataAsset_t3748144825  : public ScriptableObject_t2528358522
{
public:
	// Spine.Unity.AtlasAsset[] Spine.Unity.SkeletonDataAsset::atlasAssets
	AtlasAssetU5BU5D_t395880643* ___atlasAssets_2;
	// System.Single Spine.Unity.SkeletonDataAsset::scale
	float ___scale_3;
	// UnityEngine.TextAsset Spine.Unity.SkeletonDataAsset::skeletonJSON
	TextAsset_t3022178571 * ___skeletonJSON_4;
	// System.String[] Spine.Unity.SkeletonDataAsset::fromAnimation
	StringU5BU5D_t1281789340* ___fromAnimation_5;
	// System.String[] Spine.Unity.SkeletonDataAsset::toAnimation
	StringU5BU5D_t1281789340* ___toAnimation_6;
	// System.Single[] Spine.Unity.SkeletonDataAsset::duration
	SingleU5BU5D_t1444911251* ___duration_7;
	// System.Single Spine.Unity.SkeletonDataAsset::defaultMix
	float ___defaultMix_8;
	// UnityEngine.RuntimeAnimatorController Spine.Unity.SkeletonDataAsset::controller
	RuntimeAnimatorController_t2933699135 * ___controller_9;
	// Spine.SkeletonData Spine.Unity.SkeletonDataAsset::skeletonData
	SkeletonData_t2032710716 * ___skeletonData_10;
	// Spine.AnimationStateData Spine.Unity.SkeletonDataAsset::stateData
	AnimationStateData_t3010651567 * ___stateData_11;

public:
	inline static int32_t get_offset_of_atlasAssets_2() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___atlasAssets_2)); }
	inline AtlasAssetU5BU5D_t395880643* get_atlasAssets_2() const { return ___atlasAssets_2; }
	inline AtlasAssetU5BU5D_t395880643** get_address_of_atlasAssets_2() { return &___atlasAssets_2; }
	inline void set_atlasAssets_2(AtlasAssetU5BU5D_t395880643* value)
	{
		___atlasAssets_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssets_2), value);
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___scale_3)); }
	inline float get_scale_3() const { return ___scale_3; }
	inline float* get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(float value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_skeletonJSON_4() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___skeletonJSON_4)); }
	inline TextAsset_t3022178571 * get_skeletonJSON_4() const { return ___skeletonJSON_4; }
	inline TextAsset_t3022178571 ** get_address_of_skeletonJSON_4() { return &___skeletonJSON_4; }
	inline void set_skeletonJSON_4(TextAsset_t3022178571 * value)
	{
		___skeletonJSON_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonJSON_4), value);
	}

	inline static int32_t get_offset_of_fromAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___fromAnimation_5)); }
	inline StringU5BU5D_t1281789340* get_fromAnimation_5() const { return ___fromAnimation_5; }
	inline StringU5BU5D_t1281789340** get_address_of_fromAnimation_5() { return &___fromAnimation_5; }
	inline void set_fromAnimation_5(StringU5BU5D_t1281789340* value)
	{
		___fromAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___fromAnimation_5), value);
	}

	inline static int32_t get_offset_of_toAnimation_6() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___toAnimation_6)); }
	inline StringU5BU5D_t1281789340* get_toAnimation_6() const { return ___toAnimation_6; }
	inline StringU5BU5D_t1281789340** get_address_of_toAnimation_6() { return &___toAnimation_6; }
	inline void set_toAnimation_6(StringU5BU5D_t1281789340* value)
	{
		___toAnimation_6 = value;
		Il2CppCodeGenWriteBarrier((&___toAnimation_6), value);
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___duration_7)); }
	inline SingleU5BU5D_t1444911251* get_duration_7() const { return ___duration_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(SingleU5BU5D_t1444911251* value)
	{
		___duration_7 = value;
		Il2CppCodeGenWriteBarrier((&___duration_7), value);
	}

	inline static int32_t get_offset_of_defaultMix_8() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___defaultMix_8)); }
	inline float get_defaultMix_8() const { return ___defaultMix_8; }
	inline float* get_address_of_defaultMix_8() { return &___defaultMix_8; }
	inline void set_defaultMix_8(float value)
	{
		___defaultMix_8 = value;
	}

	inline static int32_t get_offset_of_controller_9() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___controller_9)); }
	inline RuntimeAnimatorController_t2933699135 * get_controller_9() const { return ___controller_9; }
	inline RuntimeAnimatorController_t2933699135 ** get_address_of_controller_9() { return &___controller_9; }
	inline void set_controller_9(RuntimeAnimatorController_t2933699135 * value)
	{
		___controller_9 = value;
		Il2CppCodeGenWriteBarrier((&___controller_9), value);
	}

	inline static int32_t get_offset_of_skeletonData_10() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___skeletonData_10)); }
	inline SkeletonData_t2032710716 * get_skeletonData_10() const { return ___skeletonData_10; }
	inline SkeletonData_t2032710716 ** get_address_of_skeletonData_10() { return &___skeletonData_10; }
	inline void set_skeletonData_10(SkeletonData_t2032710716 * value)
	{
		___skeletonData_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_10), value);
	}

	inline static int32_t get_offset_of_stateData_11() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___stateData_11)); }
	inline AnimationStateData_t3010651567 * get_stateData_11() const { return ___stateData_11; }
	inline AnimationStateData_t3010651567 ** get_address_of_stateData_11() { return &___stateData_11; }
	inline void set_stateData_11(AnimationStateData_t3010651567 * value)
	{
		___stateData_11 = value;
		Il2CppCodeGenWriteBarrier((&___stateData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATAASSET_T3748144825_H
#ifndef UPDATEBONESDELEGATE_T735903178_H
#define UPDATEBONESDELEGATE_T735903178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.UpdateBonesDelegate
struct  UpdateBonesDelegate_t735903178  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEBONESDELEGATE_T735903178_H
#ifndef MESHGENERATORDELEGATE_T1654156803_H
#define MESHGENERATORDELEGATE_T1654156803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorDelegate
struct  MeshGeneratorDelegate_t1654156803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATORDELEGATE_T1654156803_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SKELETONRAGDOLL_T480923817_H
#define SKELETONRAGDOLL_T480923817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll
struct  SkeletonRagdoll_t480923817  : public MonoBehaviour_t3962482529
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll::startingBoneName
	String_t* ___startingBoneName_3;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll::stopBoneNames
	List_1_t3319525431 * ___stopBoneNames_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::applyOnStart
	bool ___applyOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableIK
	bool ___disableIK_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableOtherConstraints
	bool ___disableOtherConstraints_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::pinStartBone
	bool ___pinStartBone_8;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::enableJointCollision
	bool ___enableJointCollision_9;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::useGravity
	bool ___useGravity_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::thickness
	float ___thickness_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rotationLimit
	float ___rotationLimit_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rootMass
	float ___rootMass_13;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::massFalloffFactor
	float ___massFalloffFactor_14;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll::colliderLayer
	int32_t ___colliderLayer_15;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::mix
	float ___mix_16;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_17;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll::skeleton
	Skeleton_t3686076450 * ___skeleton_18;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll::boneTable
	Dictionary_2_t2478265129 * ___boneTable_19;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::ragdollRoot
	Transform_t3600365921 * ___ragdollRoot_20;
	// UnityEngine.Rigidbody Spine.Unity.Modules.SkeletonRagdoll::<RootRigidbody>k__BackingField
	Rigidbody_t3916780224 * ___U3CRootRigidbodyU3Ek__BackingField_21;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll::<StartingBone>k__BackingField
	Bone_t1086356328 * ___U3CStartingBoneU3Ek__BackingField_22;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonRagdoll::rootOffset
	Vector3_t3722313464  ___rootOffset_23;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::isActive
	bool ___isActive_24;

public:
	inline static int32_t get_offset_of_startingBoneName_3() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___startingBoneName_3)); }
	inline String_t* get_startingBoneName_3() const { return ___startingBoneName_3; }
	inline String_t** get_address_of_startingBoneName_3() { return &___startingBoneName_3; }
	inline void set_startingBoneName_3(String_t* value)
	{
		___startingBoneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_3), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___stopBoneNames_4)); }
	inline List_1_t3319525431 * get_stopBoneNames_4() const { return ___stopBoneNames_4; }
	inline List_1_t3319525431 ** get_address_of_stopBoneNames_4() { return &___stopBoneNames_4; }
	inline void set_stopBoneNames_4(List_1_t3319525431 * value)
	{
		___stopBoneNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_4), value);
	}

	inline static int32_t get_offset_of_applyOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___applyOnStart_5)); }
	inline bool get_applyOnStart_5() const { return ___applyOnStart_5; }
	inline bool* get_address_of_applyOnStart_5() { return &___applyOnStart_5; }
	inline void set_applyOnStart_5(bool value)
	{
		___applyOnStart_5 = value;
	}

	inline static int32_t get_offset_of_disableIK_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___disableIK_6)); }
	inline bool get_disableIK_6() const { return ___disableIK_6; }
	inline bool* get_address_of_disableIK_6() { return &___disableIK_6; }
	inline void set_disableIK_6(bool value)
	{
		___disableIK_6 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___disableOtherConstraints_7)); }
	inline bool get_disableOtherConstraints_7() const { return ___disableOtherConstraints_7; }
	inline bool* get_address_of_disableOtherConstraints_7() { return &___disableOtherConstraints_7; }
	inline void set_disableOtherConstraints_7(bool value)
	{
		___disableOtherConstraints_7 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___pinStartBone_8)); }
	inline bool get_pinStartBone_8() const { return ___pinStartBone_8; }
	inline bool* get_address_of_pinStartBone_8() { return &___pinStartBone_8; }
	inline void set_pinStartBone_8(bool value)
	{
		___pinStartBone_8 = value;
	}

	inline static int32_t get_offset_of_enableJointCollision_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___enableJointCollision_9)); }
	inline bool get_enableJointCollision_9() const { return ___enableJointCollision_9; }
	inline bool* get_address_of_enableJointCollision_9() { return &___enableJointCollision_9; }
	inline void set_enableJointCollision_9(bool value)
	{
		___enableJointCollision_9 = value;
	}

	inline static int32_t get_offset_of_useGravity_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___useGravity_10)); }
	inline bool get_useGravity_10() const { return ___useGravity_10; }
	inline bool* get_address_of_useGravity_10() { return &___useGravity_10; }
	inline void set_useGravity_10(bool value)
	{
		___useGravity_10 = value;
	}

	inline static int32_t get_offset_of_thickness_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___thickness_11)); }
	inline float get_thickness_11() const { return ___thickness_11; }
	inline float* get_address_of_thickness_11() { return &___thickness_11; }
	inline void set_thickness_11(float value)
	{
		___thickness_11 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___rotationLimit_12)); }
	inline float get_rotationLimit_12() const { return ___rotationLimit_12; }
	inline float* get_address_of_rotationLimit_12() { return &___rotationLimit_12; }
	inline void set_rotationLimit_12(float value)
	{
		___rotationLimit_12 = value;
	}

	inline static int32_t get_offset_of_rootMass_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___rootMass_13)); }
	inline float get_rootMass_13() const { return ___rootMass_13; }
	inline float* get_address_of_rootMass_13() { return &___rootMass_13; }
	inline void set_rootMass_13(float value)
	{
		___rootMass_13 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___massFalloffFactor_14)); }
	inline float get_massFalloffFactor_14() const { return ___massFalloffFactor_14; }
	inline float* get_address_of_massFalloffFactor_14() { return &___massFalloffFactor_14; }
	inline void set_massFalloffFactor_14(float value)
	{
		___massFalloffFactor_14 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___colliderLayer_15)); }
	inline int32_t get_colliderLayer_15() const { return ___colliderLayer_15; }
	inline int32_t* get_address_of_colliderLayer_15() { return &___colliderLayer_15; }
	inline void set_colliderLayer_15(int32_t value)
	{
		___colliderLayer_15 = value;
	}

	inline static int32_t get_offset_of_mix_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___mix_16)); }
	inline float get_mix_16() const { return ___mix_16; }
	inline float* get_address_of_mix_16() { return &___mix_16; }
	inline void set_mix_16(float value)
	{
		___mix_16 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___targetSkeletonComponent_17)); }
	inline RuntimeObject* get_targetSkeletonComponent_17() const { return ___targetSkeletonComponent_17; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_17() { return &___targetSkeletonComponent_17; }
	inline void set_targetSkeletonComponent_17(RuntimeObject* value)
	{
		___targetSkeletonComponent_17 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_17), value);
	}

	inline static int32_t get_offset_of_skeleton_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___skeleton_18)); }
	inline Skeleton_t3686076450 * get_skeleton_18() const { return ___skeleton_18; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_18() { return &___skeleton_18; }
	inline void set_skeleton_18(Skeleton_t3686076450 * value)
	{
		___skeleton_18 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_18), value);
	}

	inline static int32_t get_offset_of_boneTable_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___boneTable_19)); }
	inline Dictionary_2_t2478265129 * get_boneTable_19() const { return ___boneTable_19; }
	inline Dictionary_2_t2478265129 ** get_address_of_boneTable_19() { return &___boneTable_19; }
	inline void set_boneTable_19(Dictionary_2_t2478265129 * value)
	{
		___boneTable_19 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_19), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___ragdollRoot_20)); }
	inline Transform_t3600365921 * get_ragdollRoot_20() const { return ___ragdollRoot_20; }
	inline Transform_t3600365921 ** get_address_of_ragdollRoot_20() { return &___ragdollRoot_20; }
	inline void set_ragdollRoot_20(Transform_t3600365921 * value)
	{
		___ragdollRoot_20 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_20), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___U3CRootRigidbodyU3Ek__BackingField_21)); }
	inline Rigidbody_t3916780224 * get_U3CRootRigidbodyU3Ek__BackingField_21() const { return ___U3CRootRigidbodyU3Ek__BackingField_21; }
	inline Rigidbody_t3916780224 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_21() { return &___U3CRootRigidbodyU3Ek__BackingField_21; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_21(Rigidbody_t3916780224 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___U3CStartingBoneU3Ek__BackingField_22)); }
	inline Bone_t1086356328 * get_U3CStartingBoneU3Ek__BackingField_22() const { return ___U3CStartingBoneU3Ek__BackingField_22; }
	inline Bone_t1086356328 ** get_address_of_U3CStartingBoneU3Ek__BackingField_22() { return &___U3CStartingBoneU3Ek__BackingField_22; }
	inline void set_U3CStartingBoneU3Ek__BackingField_22(Bone_t1086356328 * value)
	{
		___U3CStartingBoneU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_rootOffset_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___rootOffset_23)); }
	inline Vector3_t3722313464  get_rootOffset_23() const { return ___rootOffset_23; }
	inline Vector3_t3722313464 * get_address_of_rootOffset_23() { return &___rootOffset_23; }
	inline void set_rootOffset_23(Vector3_t3722313464  value)
	{
		___rootOffset_23 = value;
	}

	inline static int32_t get_offset_of_isActive_24() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___isActive_24)); }
	inline bool get_isActive_24() const { return ___isActive_24; }
	inline bool* get_address_of_isActive_24() { return &___isActive_24; }
	inline void set_isActive_24(bool value)
	{
		___isActive_24 = value;
	}
};

struct SkeletonRagdoll_t480923817_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::parentSpaceHelper
	Transform_t3600365921 * ___parentSpaceHelper_2;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_2() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817_StaticFields, ___parentSpaceHelper_2)); }
	inline Transform_t3600365921 * get_parentSpaceHelper_2() const { return ___parentSpaceHelper_2; }
	inline Transform_t3600365921 ** get_address_of_parentSpaceHelper_2() { return &___parentSpaceHelper_2; }
	inline void set_parentSpaceHelper_2(Transform_t3600365921 * value)
	{
		___parentSpaceHelper_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL_T480923817_H
#ifndef SKELETONGHOSTRENDERER_T2445315009_H
#define SKELETONGHOSTRENDERER_T2445315009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer
struct  SkeletonGhostRenderer_t2445315009  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Spine.Unity.Modules.SkeletonGhostRenderer::fadeSpeed
	float ___fadeSpeed_2;
	// UnityEngine.Color32[] Spine.Unity.Modules.SkeletonGhostRenderer::colors
	Color32U5BU5D_t3850468773* ___colors_3;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer::black
	Color32_t2600501292  ___black_4;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhostRenderer::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_5;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhostRenderer::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_6;

public:
	inline static int32_t get_offset_of_fadeSpeed_2() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___fadeSpeed_2)); }
	inline float get_fadeSpeed_2() const { return ___fadeSpeed_2; }
	inline float* get_address_of_fadeSpeed_2() { return &___fadeSpeed_2; }
	inline void set_fadeSpeed_2(float value)
	{
		___fadeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_colors_3() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___colors_3)); }
	inline Color32U5BU5D_t3850468773* get_colors_3() const { return ___colors_3; }
	inline Color32U5BU5D_t3850468773** get_address_of_colors_3() { return &___colors_3; }
	inline void set_colors_3(Color32U5BU5D_t3850468773* value)
	{
		___colors_3 = value;
		Il2CppCodeGenWriteBarrier((&___colors_3), value);
	}

	inline static int32_t get_offset_of_black_4() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___black_4)); }
	inline Color32_t2600501292  get_black_4() const { return ___black_4; }
	inline Color32_t2600501292 * get_address_of_black_4() { return &___black_4; }
	inline void set_black_4(Color32_t2600501292  value)
	{
		___black_4 = value;
	}

	inline static int32_t get_offset_of_meshFilter_5() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___meshFilter_5)); }
	inline MeshFilter_t3523625662 * get_meshFilter_5() const { return ___meshFilter_5; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_5() { return &___meshFilter_5; }
	inline void set_meshFilter_5(MeshFilter_t3523625662 * value)
	{
		___meshFilter_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_5), value);
	}

	inline static int32_t get_offset_of_meshRenderer_6() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___meshRenderer_6)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_6() const { return ___meshRenderer_6; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_6() { return &___meshRenderer_6; }
	inline void set_meshRenderer_6(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOSTRENDERER_T2445315009_H
#ifndef SKELETONGHOST_T1898327037_H
#define SKELETONGHOST_T1898327037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhost
struct  SkeletonGhost_t1898327037  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::ghostingEnabled
	bool ___ghostingEnabled_4;
	// System.Single Spine.Unity.Modules.SkeletonGhost::spawnRate
	float ___spawnRate_5;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhost::color
	Color32_t2600501292  ___color_6;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::additive
	bool ___additive_7;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::maximumGhosts
	int32_t ___maximumGhosts_8;
	// System.Single Spine.Unity.Modules.SkeletonGhost::fadeSpeed
	float ___fadeSpeed_9;
	// UnityEngine.Shader Spine.Unity.Modules.SkeletonGhost::ghostShader
	Shader_t4151988712 * ___ghostShader_10;
	// System.Single Spine.Unity.Modules.SkeletonGhost::textureFade
	float ___textureFade_11;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::sortWithDistanceOnly
	bool ___sortWithDistanceOnly_12;
	// System.Single Spine.Unity.Modules.SkeletonGhost::zOffset
	float ___zOffset_13;
	// System.Single Spine.Unity.Modules.SkeletonGhost::nextSpawnTime
	float ___nextSpawnTime_14;
	// Spine.Unity.Modules.SkeletonGhostRenderer[] Spine.Unity.Modules.SkeletonGhost::pool
	SkeletonGhostRendererU5BU5D_t513912860* ___pool_15;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::poolIndex
	int32_t ___poolIndex_16;
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonGhost::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_17;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhost::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_18;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhost::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_19;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.Modules.SkeletonGhost::materialTable
	Dictionary_2_t3700682020 * ___materialTable_20;

public:
	inline static int32_t get_offset_of_ghostingEnabled_4() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___ghostingEnabled_4)); }
	inline bool get_ghostingEnabled_4() const { return ___ghostingEnabled_4; }
	inline bool* get_address_of_ghostingEnabled_4() { return &___ghostingEnabled_4; }
	inline void set_ghostingEnabled_4(bool value)
	{
		___ghostingEnabled_4 = value;
	}

	inline static int32_t get_offset_of_spawnRate_5() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___spawnRate_5)); }
	inline float get_spawnRate_5() const { return ___spawnRate_5; }
	inline float* get_address_of_spawnRate_5() { return &___spawnRate_5; }
	inline void set_spawnRate_5(float value)
	{
		___spawnRate_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___color_6)); }
	inline Color32_t2600501292  get_color_6() const { return ___color_6; }
	inline Color32_t2600501292 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Color32_t2600501292  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of_additive_7() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___additive_7)); }
	inline bool get_additive_7() const { return ___additive_7; }
	inline bool* get_address_of_additive_7() { return &___additive_7; }
	inline void set_additive_7(bool value)
	{
		___additive_7 = value;
	}

	inline static int32_t get_offset_of_maximumGhosts_8() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___maximumGhosts_8)); }
	inline int32_t get_maximumGhosts_8() const { return ___maximumGhosts_8; }
	inline int32_t* get_address_of_maximumGhosts_8() { return &___maximumGhosts_8; }
	inline void set_maximumGhosts_8(int32_t value)
	{
		___maximumGhosts_8 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_9() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___fadeSpeed_9)); }
	inline float get_fadeSpeed_9() const { return ___fadeSpeed_9; }
	inline float* get_address_of_fadeSpeed_9() { return &___fadeSpeed_9; }
	inline void set_fadeSpeed_9(float value)
	{
		___fadeSpeed_9 = value;
	}

	inline static int32_t get_offset_of_ghostShader_10() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___ghostShader_10)); }
	inline Shader_t4151988712 * get_ghostShader_10() const { return ___ghostShader_10; }
	inline Shader_t4151988712 ** get_address_of_ghostShader_10() { return &___ghostShader_10; }
	inline void set_ghostShader_10(Shader_t4151988712 * value)
	{
		___ghostShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___ghostShader_10), value);
	}

	inline static int32_t get_offset_of_textureFade_11() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___textureFade_11)); }
	inline float get_textureFade_11() const { return ___textureFade_11; }
	inline float* get_address_of_textureFade_11() { return &___textureFade_11; }
	inline void set_textureFade_11(float value)
	{
		___textureFade_11 = value;
	}

	inline static int32_t get_offset_of_sortWithDistanceOnly_12() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___sortWithDistanceOnly_12)); }
	inline bool get_sortWithDistanceOnly_12() const { return ___sortWithDistanceOnly_12; }
	inline bool* get_address_of_sortWithDistanceOnly_12() { return &___sortWithDistanceOnly_12; }
	inline void set_sortWithDistanceOnly_12(bool value)
	{
		___sortWithDistanceOnly_12 = value;
	}

	inline static int32_t get_offset_of_zOffset_13() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___zOffset_13)); }
	inline float get_zOffset_13() const { return ___zOffset_13; }
	inline float* get_address_of_zOffset_13() { return &___zOffset_13; }
	inline void set_zOffset_13(float value)
	{
		___zOffset_13 = value;
	}

	inline static int32_t get_offset_of_nextSpawnTime_14() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___nextSpawnTime_14)); }
	inline float get_nextSpawnTime_14() const { return ___nextSpawnTime_14; }
	inline float* get_address_of_nextSpawnTime_14() { return &___nextSpawnTime_14; }
	inline void set_nextSpawnTime_14(float value)
	{
		___nextSpawnTime_14 = value;
	}

	inline static int32_t get_offset_of_pool_15() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___pool_15)); }
	inline SkeletonGhostRendererU5BU5D_t513912860* get_pool_15() const { return ___pool_15; }
	inline SkeletonGhostRendererU5BU5D_t513912860** get_address_of_pool_15() { return &___pool_15; }
	inline void set_pool_15(SkeletonGhostRendererU5BU5D_t513912860* value)
	{
		___pool_15 = value;
		Il2CppCodeGenWriteBarrier((&___pool_15), value);
	}

	inline static int32_t get_offset_of_poolIndex_16() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___poolIndex_16)); }
	inline int32_t get_poolIndex_16() const { return ___poolIndex_16; }
	inline int32_t* get_address_of_poolIndex_16() { return &___poolIndex_16; }
	inline void set_poolIndex_16(int32_t value)
	{
		___poolIndex_16 = value;
	}

	inline static int32_t get_offset_of_skeletonRenderer_17() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___skeletonRenderer_17)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_17() const { return ___skeletonRenderer_17; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_17() { return &___skeletonRenderer_17; }
	inline void set_skeletonRenderer_17(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_17), value);
	}

	inline static int32_t get_offset_of_meshRenderer_18() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___meshRenderer_18)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_18() const { return ___meshRenderer_18; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_18() { return &___meshRenderer_18; }
	inline void set_meshRenderer_18(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_18), value);
	}

	inline static int32_t get_offset_of_meshFilter_19() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___meshFilter_19)); }
	inline MeshFilter_t3523625662 * get_meshFilter_19() const { return ___meshFilter_19; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_19() { return &___meshFilter_19; }
	inline void set_meshFilter_19(MeshFilter_t3523625662 * value)
	{
		___meshFilter_19 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_19), value);
	}

	inline static int32_t get_offset_of_materialTable_20() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___materialTable_20)); }
	inline Dictionary_2_t3700682020 * get_materialTable_20() const { return ___materialTable_20; }
	inline Dictionary_2_t3700682020 ** get_address_of_materialTable_20() { return &___materialTable_20; }
	inline void set_materialTable_20(Dictionary_2_t3700682020 * value)
	{
		___materialTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOST_T1898327037_H
#ifndef BOUNDINGBOXFOLLOWER_T4182505745_H
#define BOUNDINGBOXFOLLOWER_T4182505745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoundingBoxFollower
struct  BoundingBoxFollower_t4182505745  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoundingBoxFollower::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_3;
	// System.String Spine.Unity.BoundingBoxFollower::slotName
	String_t* ___slotName_4;
	// System.Boolean Spine.Unity.BoundingBoxFollower::isTrigger
	bool ___isTrigger_5;
	// System.Boolean Spine.Unity.BoundingBoxFollower::clearStateOnDisable
	bool ___clearStateOnDisable_6;
	// Spine.Slot Spine.Unity.BoundingBoxFollower::slot
	Slot_t3514940700 * ___slot_7;
	// Spine.BoundingBoxAttachment Spine.Unity.BoundingBoxFollower::currentAttachment
	BoundingBoxAttachment_t2797506510 * ___currentAttachment_8;
	// System.String Spine.Unity.BoundingBoxFollower::currentAttachmentName
	String_t* ___currentAttachmentName_9;
	// UnityEngine.PolygonCollider2D Spine.Unity.BoundingBoxFollower::currentCollider
	PolygonCollider2D_t57175488 * ___currentCollider_10;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D> Spine.Unity.BoundingBoxFollower::colliderTable
	Dictionary_2_t3714483914 * ___colliderTable_11;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String> Spine.Unity.BoundingBoxFollower::nameTable
	Dictionary_2_t1209791819 * ___nameTable_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}

	inline static int32_t get_offset_of_slotName_4() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___slotName_4)); }
	inline String_t* get_slotName_4() const { return ___slotName_4; }
	inline String_t** get_address_of_slotName_4() { return &___slotName_4; }
	inline void set_slotName_4(String_t* value)
	{
		___slotName_4 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_4), value);
	}

	inline static int32_t get_offset_of_isTrigger_5() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___isTrigger_5)); }
	inline bool get_isTrigger_5() const { return ___isTrigger_5; }
	inline bool* get_address_of_isTrigger_5() { return &___isTrigger_5; }
	inline void set_isTrigger_5(bool value)
	{
		___isTrigger_5 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_6() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___clearStateOnDisable_6)); }
	inline bool get_clearStateOnDisable_6() const { return ___clearStateOnDisable_6; }
	inline bool* get_address_of_clearStateOnDisable_6() { return &___clearStateOnDisable_6; }
	inline void set_clearStateOnDisable_6(bool value)
	{
		___clearStateOnDisable_6 = value;
	}

	inline static int32_t get_offset_of_slot_7() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___slot_7)); }
	inline Slot_t3514940700 * get_slot_7() const { return ___slot_7; }
	inline Slot_t3514940700 ** get_address_of_slot_7() { return &___slot_7; }
	inline void set_slot_7(Slot_t3514940700 * value)
	{
		___slot_7 = value;
		Il2CppCodeGenWriteBarrier((&___slot_7), value);
	}

	inline static int32_t get_offset_of_currentAttachment_8() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___currentAttachment_8)); }
	inline BoundingBoxAttachment_t2797506510 * get_currentAttachment_8() const { return ___currentAttachment_8; }
	inline BoundingBoxAttachment_t2797506510 ** get_address_of_currentAttachment_8() { return &___currentAttachment_8; }
	inline void set_currentAttachment_8(BoundingBoxAttachment_t2797506510 * value)
	{
		___currentAttachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachment_8), value);
	}

	inline static int32_t get_offset_of_currentAttachmentName_9() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___currentAttachmentName_9)); }
	inline String_t* get_currentAttachmentName_9() const { return ___currentAttachmentName_9; }
	inline String_t** get_address_of_currentAttachmentName_9() { return &___currentAttachmentName_9; }
	inline void set_currentAttachmentName_9(String_t* value)
	{
		___currentAttachmentName_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachmentName_9), value);
	}

	inline static int32_t get_offset_of_currentCollider_10() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___currentCollider_10)); }
	inline PolygonCollider2D_t57175488 * get_currentCollider_10() const { return ___currentCollider_10; }
	inline PolygonCollider2D_t57175488 ** get_address_of_currentCollider_10() { return &___currentCollider_10; }
	inline void set_currentCollider_10(PolygonCollider2D_t57175488 * value)
	{
		___currentCollider_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentCollider_10), value);
	}

	inline static int32_t get_offset_of_colliderTable_11() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___colliderTable_11)); }
	inline Dictionary_2_t3714483914 * get_colliderTable_11() const { return ___colliderTable_11; }
	inline Dictionary_2_t3714483914 ** get_address_of_colliderTable_11() { return &___colliderTable_11; }
	inline void set_colliderTable_11(Dictionary_2_t3714483914 * value)
	{
		___colliderTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliderTable_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___nameTable_12)); }
	inline Dictionary_2_t1209791819 * get_nameTable_12() const { return ___nameTable_12; }
	inline Dictionary_2_t1209791819 ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(Dictionary_2_t1209791819 * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}
};

struct BoundingBoxFollower_t4182505745_StaticFields
{
public:
	// System.Boolean Spine.Unity.BoundingBoxFollower::DebugMessages
	bool ___DebugMessages_2;

public:
	inline static int32_t get_offset_of_DebugMessages_2() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745_StaticFields, ___DebugMessages_2)); }
	inline bool get_DebugMessages_2() const { return ___DebugMessages_2; }
	inline bool* get_address_of_DebugMessages_2() { return &___DebugMessages_2; }
	inline void set_DebugMessages_2(bool value)
	{
		___DebugMessages_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXFOLLOWER_T4182505745_H
#ifndef SKELETONRENDERERCUSTOMMATERIALS_T1555313811_H
#define SKELETONRENDERERCUSTOMMATERIALS_T1555313811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials
struct  SkeletonRendererCustomMaterials_t1555313811  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRendererCustomMaterials::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_2;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customSlotMaterials
	List_1_t2474053923 * ___customSlotMaterials_3;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customMaterialOverrides
	List_1_t3907116131 * ___customMaterialOverrides_4;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t1555313811, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_3() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t1555313811, ___customSlotMaterials_3)); }
	inline List_1_t2474053923 * get_customSlotMaterials_3() const { return ___customSlotMaterials_3; }
	inline List_1_t2474053923 ** get_address_of_customSlotMaterials_3() { return &___customSlotMaterials_3; }
	inline void set_customSlotMaterials_3(List_1_t2474053923 * value)
	{
		___customSlotMaterials_3 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_3), value);
	}

	inline static int32_t get_offset_of_customMaterialOverrides_4() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t1555313811, ___customMaterialOverrides_4)); }
	inline List_1_t3907116131 * get_customMaterialOverrides_4() const { return ___customMaterialOverrides_4; }
	inline List_1_t3907116131 ** get_address_of_customMaterialOverrides_4() { return &___customMaterialOverrides_4; }
	inline void set_customMaterialOverrides_4(List_1_t3907116131 * value)
	{
		___customMaterialOverrides_4 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverrides_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERCUSTOMMATERIALS_T1555313811_H
#ifndef BONEFOLLOWER_T3069400636_H
#define BONEFOLLOWER_T3069400636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollower
struct  BoneFollower_t3069400636  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoneFollower::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_2;
	// System.String Spine.Unity.BoneFollower::boneName
	String_t* ___boneName_3;
	// System.Boolean Spine.Unity.BoneFollower::followZPosition
	bool ___followZPosition_4;
	// System.Boolean Spine.Unity.BoneFollower::followBoneRotation
	bool ___followBoneRotation_5;
	// System.Boolean Spine.Unity.BoneFollower::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.BoneFollower::followLocalScale
	bool ___followLocalScale_7;
	// System.Boolean Spine.Unity.BoneFollower::initializeOnAwake
	bool ___initializeOnAwake_8;
	// System.Boolean Spine.Unity.BoneFollower::valid
	bool ___valid_9;
	// Spine.Bone Spine.Unity.BoneFollower::bone
	Bone_t1086356328 * ___bone_10;
	// UnityEngine.Transform Spine.Unity.BoneFollower::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_11;
	// System.Boolean Spine.Unity.BoneFollower::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_boneName_3() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___boneName_3)); }
	inline String_t* get_boneName_3() const { return ___boneName_3; }
	inline String_t** get_address_of_boneName_3() { return &___boneName_3; }
	inline void set_boneName_3(String_t* value)
	{
		___boneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_3), value);
	}

	inline static int32_t get_offset_of_followZPosition_4() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followZPosition_4)); }
	inline bool get_followZPosition_4() const { return ___followZPosition_4; }
	inline bool* get_address_of_followZPosition_4() { return &___followZPosition_4; }
	inline void set_followZPosition_4(bool value)
	{
		___followZPosition_4 = value;
	}

	inline static int32_t get_offset_of_followBoneRotation_5() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followBoneRotation_5)); }
	inline bool get_followBoneRotation_5() const { return ___followBoneRotation_5; }
	inline bool* get_address_of_followBoneRotation_5() { return &___followBoneRotation_5; }
	inline void set_followBoneRotation_5(bool value)
	{
		___followBoneRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_7() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followLocalScale_7)); }
	inline bool get_followLocalScale_7() const { return ___followLocalScale_7; }
	inline bool* get_address_of_followLocalScale_7() { return &___followLocalScale_7; }
	inline void set_followLocalScale_7(bool value)
	{
		___followLocalScale_7 = value;
	}

	inline static int32_t get_offset_of_initializeOnAwake_8() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___initializeOnAwake_8)); }
	inline bool get_initializeOnAwake_8() const { return ___initializeOnAwake_8; }
	inline bool* get_address_of_initializeOnAwake_8() { return &___initializeOnAwake_8; }
	inline void set_initializeOnAwake_8(bool value)
	{
		___initializeOnAwake_8 = value;
	}

	inline static int32_t get_offset_of_valid_9() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___valid_9)); }
	inline bool get_valid_9() const { return ___valid_9; }
	inline bool* get_address_of_valid_9() { return &___valid_9; }
	inline void set_valid_9(bool value)
	{
		___valid_9 = value;
	}

	inline static int32_t get_offset_of_bone_10() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___bone_10)); }
	inline Bone_t1086356328 * get_bone_10() const { return ___bone_10; }
	inline Bone_t1086356328 ** get_address_of_bone_10() { return &___bone_10; }
	inline void set_bone_10(Bone_t1086356328 * value)
	{
		___bone_10 = value;
		Il2CppCodeGenWriteBarrier((&___bone_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_11() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___skeletonTransform_11)); }
	inline Transform_t3600365921 * get_skeletonTransform_11() const { return ___skeletonTransform_11; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_11() { return &___skeletonTransform_11; }
	inline void set_skeletonTransform_11(Transform_t3600365921 * value)
	{
		___skeletonTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_11), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_12() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___skeletonTransformIsParent_12)); }
	inline bool get_skeletonTransformIsParent_12() const { return ___skeletonTransformIsParent_12; }
	inline bool* get_address_of_skeletonTransformIsParent_12() { return &___skeletonTransformIsParent_12; }
	inline void set_skeletonTransformIsParent_12(bool value)
	{
		___skeletonTransformIsParent_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWER_T3069400636_H
#ifndef SKELETONRAGDOLL2D_T2972821364_H
#define SKELETONRAGDOLL2D_T2972821364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D
struct  SkeletonRagdoll2D_t2972821364  : public MonoBehaviour_t3962482529
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll2D::startingBoneName
	String_t* ___startingBoneName_3;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll2D::stopBoneNames
	List_1_t3319525431 * ___stopBoneNames_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::applyOnStart
	bool ___applyOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableIK
	bool ___disableIK_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableOtherConstraints
	bool ___disableOtherConstraints_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::pinStartBone
	bool ___pinStartBone_8;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::gravityScale
	float ___gravityScale_9;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::thickness
	float ___thickness_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rotationLimit
	float ___rotationLimit_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rootMass
	float ___rootMass_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::massFalloffFactor
	float ___massFalloffFactor_13;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D::colliderLayer
	int32_t ___colliderLayer_14;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::mix
	float ___mix_15;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll2D::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_16;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll2D::skeleton
	Skeleton_t3686076450 * ___skeleton_17;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll2D::boneTable
	Dictionary_2_t2478265129 * ___boneTable_18;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::ragdollRoot
	Transform_t3600365921 * ___ragdollRoot_19;
	// UnityEngine.Rigidbody2D Spine.Unity.Modules.SkeletonRagdoll2D::<RootRigidbody>k__BackingField
	Rigidbody2D_t939494601 * ___U3CRootRigidbodyU3Ek__BackingField_20;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll2D::<StartingBone>k__BackingField
	Bone_t1086356328 * ___U3CStartingBoneU3Ek__BackingField_21;
	// UnityEngine.Vector2 Spine.Unity.Modules.SkeletonRagdoll2D::rootOffset
	Vector2_t2156229523  ___rootOffset_22;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::isActive
	bool ___isActive_23;

public:
	inline static int32_t get_offset_of_startingBoneName_3() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___startingBoneName_3)); }
	inline String_t* get_startingBoneName_3() const { return ___startingBoneName_3; }
	inline String_t** get_address_of_startingBoneName_3() { return &___startingBoneName_3; }
	inline void set_startingBoneName_3(String_t* value)
	{
		___startingBoneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_3), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___stopBoneNames_4)); }
	inline List_1_t3319525431 * get_stopBoneNames_4() const { return ___stopBoneNames_4; }
	inline List_1_t3319525431 ** get_address_of_stopBoneNames_4() { return &___stopBoneNames_4; }
	inline void set_stopBoneNames_4(List_1_t3319525431 * value)
	{
		___stopBoneNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_4), value);
	}

	inline static int32_t get_offset_of_applyOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___applyOnStart_5)); }
	inline bool get_applyOnStart_5() const { return ___applyOnStart_5; }
	inline bool* get_address_of_applyOnStart_5() { return &___applyOnStart_5; }
	inline void set_applyOnStart_5(bool value)
	{
		___applyOnStart_5 = value;
	}

	inline static int32_t get_offset_of_disableIK_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___disableIK_6)); }
	inline bool get_disableIK_6() const { return ___disableIK_6; }
	inline bool* get_address_of_disableIK_6() { return &___disableIK_6; }
	inline void set_disableIK_6(bool value)
	{
		___disableIK_6 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___disableOtherConstraints_7)); }
	inline bool get_disableOtherConstraints_7() const { return ___disableOtherConstraints_7; }
	inline bool* get_address_of_disableOtherConstraints_7() { return &___disableOtherConstraints_7; }
	inline void set_disableOtherConstraints_7(bool value)
	{
		___disableOtherConstraints_7 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___pinStartBone_8)); }
	inline bool get_pinStartBone_8() const { return ___pinStartBone_8; }
	inline bool* get_address_of_pinStartBone_8() { return &___pinStartBone_8; }
	inline void set_pinStartBone_8(bool value)
	{
		___pinStartBone_8 = value;
	}

	inline static int32_t get_offset_of_gravityScale_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___gravityScale_9)); }
	inline float get_gravityScale_9() const { return ___gravityScale_9; }
	inline float* get_address_of_gravityScale_9() { return &___gravityScale_9; }
	inline void set_gravityScale_9(float value)
	{
		___gravityScale_9 = value;
	}

	inline static int32_t get_offset_of_thickness_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___thickness_10)); }
	inline float get_thickness_10() const { return ___thickness_10; }
	inline float* get_address_of_thickness_10() { return &___thickness_10; }
	inline void set_thickness_10(float value)
	{
		___thickness_10 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___rotationLimit_11)); }
	inline float get_rotationLimit_11() const { return ___rotationLimit_11; }
	inline float* get_address_of_rotationLimit_11() { return &___rotationLimit_11; }
	inline void set_rotationLimit_11(float value)
	{
		___rotationLimit_11 = value;
	}

	inline static int32_t get_offset_of_rootMass_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___rootMass_12)); }
	inline float get_rootMass_12() const { return ___rootMass_12; }
	inline float* get_address_of_rootMass_12() { return &___rootMass_12; }
	inline void set_rootMass_12(float value)
	{
		___rootMass_12 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___massFalloffFactor_13)); }
	inline float get_massFalloffFactor_13() const { return ___massFalloffFactor_13; }
	inline float* get_address_of_massFalloffFactor_13() { return &___massFalloffFactor_13; }
	inline void set_massFalloffFactor_13(float value)
	{
		___massFalloffFactor_13 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___colliderLayer_14)); }
	inline int32_t get_colliderLayer_14() const { return ___colliderLayer_14; }
	inline int32_t* get_address_of_colliderLayer_14() { return &___colliderLayer_14; }
	inline void set_colliderLayer_14(int32_t value)
	{
		___colliderLayer_14 = value;
	}

	inline static int32_t get_offset_of_mix_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___mix_15)); }
	inline float get_mix_15() const { return ___mix_15; }
	inline float* get_address_of_mix_15() { return &___mix_15; }
	inline void set_mix_15(float value)
	{
		___mix_15 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___targetSkeletonComponent_16)); }
	inline RuntimeObject* get_targetSkeletonComponent_16() const { return ___targetSkeletonComponent_16; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_16() { return &___targetSkeletonComponent_16; }
	inline void set_targetSkeletonComponent_16(RuntimeObject* value)
	{
		___targetSkeletonComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_16), value);
	}

	inline static int32_t get_offset_of_skeleton_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___skeleton_17)); }
	inline Skeleton_t3686076450 * get_skeleton_17() const { return ___skeleton_17; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_17() { return &___skeleton_17; }
	inline void set_skeleton_17(Skeleton_t3686076450 * value)
	{
		___skeleton_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_17), value);
	}

	inline static int32_t get_offset_of_boneTable_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___boneTable_18)); }
	inline Dictionary_2_t2478265129 * get_boneTable_18() const { return ___boneTable_18; }
	inline Dictionary_2_t2478265129 ** get_address_of_boneTable_18() { return &___boneTable_18; }
	inline void set_boneTable_18(Dictionary_2_t2478265129 * value)
	{
		___boneTable_18 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_18), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___ragdollRoot_19)); }
	inline Transform_t3600365921 * get_ragdollRoot_19() const { return ___ragdollRoot_19; }
	inline Transform_t3600365921 ** get_address_of_ragdollRoot_19() { return &___ragdollRoot_19; }
	inline void set_ragdollRoot_19(Transform_t3600365921 * value)
	{
		___ragdollRoot_19 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_19), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___U3CRootRigidbodyU3Ek__BackingField_20)); }
	inline Rigidbody2D_t939494601 * get_U3CRootRigidbodyU3Ek__BackingField_20() const { return ___U3CRootRigidbodyU3Ek__BackingField_20; }
	inline Rigidbody2D_t939494601 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_20() { return &___U3CRootRigidbodyU3Ek__BackingField_20; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_20(Rigidbody2D_t939494601 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___U3CStartingBoneU3Ek__BackingField_21)); }
	inline Bone_t1086356328 * get_U3CStartingBoneU3Ek__BackingField_21() const { return ___U3CStartingBoneU3Ek__BackingField_21; }
	inline Bone_t1086356328 ** get_address_of_U3CStartingBoneU3Ek__BackingField_21() { return &___U3CStartingBoneU3Ek__BackingField_21; }
	inline void set_U3CStartingBoneU3Ek__BackingField_21(Bone_t1086356328 * value)
	{
		___U3CStartingBoneU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_rootOffset_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___rootOffset_22)); }
	inline Vector2_t2156229523  get_rootOffset_22() const { return ___rootOffset_22; }
	inline Vector2_t2156229523 * get_address_of_rootOffset_22() { return &___rootOffset_22; }
	inline void set_rootOffset_22(Vector2_t2156229523  value)
	{
		___rootOffset_22 = value;
	}

	inline static int32_t get_offset_of_isActive_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___isActive_23)); }
	inline bool get_isActive_23() const { return ___isActive_23; }
	inline bool* get_address_of_isActive_23() { return &___isActive_23; }
	inline void set_isActive_23(bool value)
	{
		___isActive_23 = value;
	}
};

struct SkeletonRagdoll2D_t2972821364_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::parentSpaceHelper
	Transform_t3600365921 * ___parentSpaceHelper_2;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_2() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364_StaticFields, ___parentSpaceHelper_2)); }
	inline Transform_t3600365921 * get_parentSpaceHelper_2() const { return ___parentSpaceHelper_2; }
	inline Transform_t3600365921 ** get_address_of_parentSpaceHelper_2() { return &___parentSpaceHelper_2; }
	inline void set_parentSpaceHelper_2(Transform_t3600365921 * value)
	{
		___parentSpaceHelper_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL2D_T2972821364_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (EventQueueEntry_t351831961)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[3] = 
{
	EventQueueEntry_t351831961::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t351831961::get_offset_of_entry_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t351831961::get_offset_of_e_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (EventType_t1835192406)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3601[7] = 
{
	EventType_t1835192406::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (AnimationStateData_t3010651567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3604[3] = 
{
	AnimationStateData_t3010651567::get_offset_of_skeletonData_0(),
	AnimationStateData_t3010651567::get_offset_of_animationToMixTime_1(),
	AnimationStateData_t3010651567::get_offset_of_defaultMix_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (AnimationPair_t1784808777)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[2] = 
{
	AnimationPair_t1784808777::get_offset_of_a1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationPair_t1784808777::get_offset_of_a2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (AnimationPairComparer_t175378255), -1, sizeof(AnimationPairComparer_t175378255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3606[1] = 
{
	AnimationPairComparer_t175378255_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (Atlas_t4040192941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3607[3] = 
{
	Atlas_t4040192941::get_offset_of_pages_0(),
	Atlas_t4040192941::get_offset_of_regions_1(),
	Atlas_t4040192941::get_offset_of_textureLoader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (Format_t2262486444)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3608[8] = 
{
	Format_t2262486444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (TextureFilter_t1610186802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3609[8] = 
{
	TextureFilter_t1610186802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (TextureWrap_t2819935362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3610[4] = 
{
	TextureWrap_t2819935362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (AtlasPage_t4077017671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3611[9] = 
{
	AtlasPage_t4077017671::get_offset_of_name_0(),
	AtlasPage_t4077017671::get_offset_of_format_1(),
	AtlasPage_t4077017671::get_offset_of_minFilter_2(),
	AtlasPage_t4077017671::get_offset_of_magFilter_3(),
	AtlasPage_t4077017671::get_offset_of_uWrap_4(),
	AtlasPage_t4077017671::get_offset_of_vWrap_5(),
	AtlasPage_t4077017671::get_offset_of_rendererObject_6(),
	AtlasPage_t4077017671::get_offset_of_width_7(),
	AtlasPage_t4077017671::get_offset_of_height_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (AtlasRegion_t13903284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3612[18] = 
{
	AtlasRegion_t13903284::get_offset_of_page_0(),
	AtlasRegion_t13903284::get_offset_of_name_1(),
	AtlasRegion_t13903284::get_offset_of_x_2(),
	AtlasRegion_t13903284::get_offset_of_y_3(),
	AtlasRegion_t13903284::get_offset_of_width_4(),
	AtlasRegion_t13903284::get_offset_of_height_5(),
	AtlasRegion_t13903284::get_offset_of_u_6(),
	AtlasRegion_t13903284::get_offset_of_v_7(),
	AtlasRegion_t13903284::get_offset_of_u2_8(),
	AtlasRegion_t13903284::get_offset_of_v2_9(),
	AtlasRegion_t13903284::get_offset_of_offsetX_10(),
	AtlasRegion_t13903284::get_offset_of_offsetY_11(),
	AtlasRegion_t13903284::get_offset_of_originalWidth_12(),
	AtlasRegion_t13903284::get_offset_of_originalHeight_13(),
	AtlasRegion_t13903284::get_offset_of_index_14(),
	AtlasRegion_t13903284::get_offset_of_rotate_15(),
	AtlasRegion_t13903284::get_offset_of_splits_16(),
	AtlasRegion_t13903284::get_offset_of_pads_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (AtlasAttachmentLoader_t3966790320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[1] = 
{
	AtlasAttachmentLoader_t3966790320::get_offset_of_atlasArray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (Attachment_t3043756552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[1] = 
{
	Attachment_t3043756552::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (AttachmentType_t1460157544)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3617[8] = 
{
	AttachmentType_t1460157544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (BoundingBoxAttachment_t2797506510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (ClippingAttachment_t2586274570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3619[1] = 
{
	ClippingAttachment_t2586274570::get_offset_of_endSlot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (MeshAttachment_t1975337962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[26] = 
{
	MeshAttachment_t1975337962::get_offset_of_regionOffsetX_7(),
	MeshAttachment_t1975337962::get_offset_of_regionOffsetY_8(),
	MeshAttachment_t1975337962::get_offset_of_regionWidth_9(),
	MeshAttachment_t1975337962::get_offset_of_regionHeight_10(),
	MeshAttachment_t1975337962::get_offset_of_regionOriginalWidth_11(),
	MeshAttachment_t1975337962::get_offset_of_regionOriginalHeight_12(),
	MeshAttachment_t1975337962::get_offset_of_parentMesh_13(),
	MeshAttachment_t1975337962::get_offset_of_uvs_14(),
	MeshAttachment_t1975337962::get_offset_of_regionUVs_15(),
	MeshAttachment_t1975337962::get_offset_of_triangles_16(),
	MeshAttachment_t1975337962::get_offset_of_r_17(),
	MeshAttachment_t1975337962::get_offset_of_g_18(),
	MeshAttachment_t1975337962::get_offset_of_b_19(),
	MeshAttachment_t1975337962::get_offset_of_a_20(),
	MeshAttachment_t1975337962::get_offset_of_hulllength_21(),
	MeshAttachment_t1975337962::get_offset_of_inheritDeform_22(),
	MeshAttachment_t1975337962::get_offset_of_U3CPathU3Ek__BackingField_23(),
	MeshAttachment_t1975337962::get_offset_of_RendererObject_24(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionUU3Ek__BackingField_25(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionVU3Ek__BackingField_26(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionU2U3Ek__BackingField_27(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionV2U3Ek__BackingField_28(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionRotateU3Ek__BackingField_29(),
	MeshAttachment_t1975337962::get_offset_of_U3CEdgesU3Ek__BackingField_30(),
	MeshAttachment_t1975337962::get_offset_of_U3CWidthU3Ek__BackingField_31(),
	MeshAttachment_t1975337962::get_offset_of_U3CHeightU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (PathAttachment_t3565151060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[3] = 
{
	PathAttachment_t3565151060::get_offset_of_lengths_7(),
	PathAttachment_t3565151060::get_offset_of_closed_8(),
	PathAttachment_t3565151060::get_offset_of_constantSpeed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (PointAttachment_t2275020146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[3] = 
{
	PointAttachment_t2275020146::get_offset_of_x_1(),
	PointAttachment_t2275020146::get_offset_of_y_2(),
	PointAttachment_t2275020146::get_offset_of_rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (RegionAttachment_t1770147391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RegionAttachment_t1770147391::get_offset_of_x_9(),
	RegionAttachment_t1770147391::get_offset_of_y_10(),
	RegionAttachment_t1770147391::get_offset_of_rotation_11(),
	RegionAttachment_t1770147391::get_offset_of_scaleX_12(),
	RegionAttachment_t1770147391::get_offset_of_scaleY_13(),
	RegionAttachment_t1770147391::get_offset_of_width_14(),
	RegionAttachment_t1770147391::get_offset_of_height_15(),
	RegionAttachment_t1770147391::get_offset_of_regionOffsetX_16(),
	RegionAttachment_t1770147391::get_offset_of_regionOffsetY_17(),
	RegionAttachment_t1770147391::get_offset_of_regionWidth_18(),
	RegionAttachment_t1770147391::get_offset_of_regionHeight_19(),
	RegionAttachment_t1770147391::get_offset_of_regionOriginalWidth_20(),
	RegionAttachment_t1770147391::get_offset_of_regionOriginalHeight_21(),
	RegionAttachment_t1770147391::get_offset_of_offset_22(),
	RegionAttachment_t1770147391::get_offset_of_uvs_23(),
	RegionAttachment_t1770147391::get_offset_of_r_24(),
	RegionAttachment_t1770147391::get_offset_of_g_25(),
	RegionAttachment_t1770147391::get_offset_of_b_26(),
	RegionAttachment_t1770147391::get_offset_of_a_27(),
	RegionAttachment_t1770147391::get_offset_of_U3CPathU3Ek__BackingField_28(),
	RegionAttachment_t1770147391::get_offset_of_RendererObject_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (VertexAttachment_t4074366829), -1, sizeof(VertexAttachment_t4074366829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3624[6] = 
{
	VertexAttachment_t4074366829_StaticFields::get_offset_of_nextID_1(),
	VertexAttachment_t4074366829_StaticFields::get_offset_of_nextIdLock_2(),
	VertexAttachment_t4074366829::get_offset_of_id_3(),
	VertexAttachment_t4074366829::get_offset_of_bones_4(),
	VertexAttachment_t4074366829::get_offset_of_vertices_5(),
	VertexAttachment_t4074366829::get_offset_of_worldVerticesLength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (BlendMode_t358635744)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3625[5] = 
{
	BlendMode_t358635744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (Bone_t1086356328), -1, sizeof(Bone_t1086356328_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3626[27] = 
{
	Bone_t1086356328_StaticFields::get_offset_of_yDown_0(),
	Bone_t1086356328::get_offset_of_data_1(),
	Bone_t1086356328::get_offset_of_skeleton_2(),
	Bone_t1086356328::get_offset_of_parent_3(),
	Bone_t1086356328::get_offset_of_children_4(),
	Bone_t1086356328::get_offset_of_x_5(),
	Bone_t1086356328::get_offset_of_y_6(),
	Bone_t1086356328::get_offset_of_rotation_7(),
	Bone_t1086356328::get_offset_of_scaleX_8(),
	Bone_t1086356328::get_offset_of_scaleY_9(),
	Bone_t1086356328::get_offset_of_shearX_10(),
	Bone_t1086356328::get_offset_of_shearY_11(),
	Bone_t1086356328::get_offset_of_ax_12(),
	Bone_t1086356328::get_offset_of_ay_13(),
	Bone_t1086356328::get_offset_of_arotation_14(),
	Bone_t1086356328::get_offset_of_ascaleX_15(),
	Bone_t1086356328::get_offset_of_ascaleY_16(),
	Bone_t1086356328::get_offset_of_ashearX_17(),
	Bone_t1086356328::get_offset_of_ashearY_18(),
	Bone_t1086356328::get_offset_of_appliedValid_19(),
	Bone_t1086356328::get_offset_of_a_20(),
	Bone_t1086356328::get_offset_of_b_21(),
	Bone_t1086356328::get_offset_of_worldX_22(),
	Bone_t1086356328::get_offset_of_c_23(),
	Bone_t1086356328::get_offset_of_d_24(),
	Bone_t1086356328::get_offset_of_worldY_25(),
	Bone_t1086356328::get_offset_of_sorted_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (BoneData_t3130174490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[12] = 
{
	BoneData_t3130174490::get_offset_of_index_0(),
	BoneData_t3130174490::get_offset_of_name_1(),
	BoneData_t3130174490::get_offset_of_parent_2(),
	BoneData_t3130174490::get_offset_of_length_3(),
	BoneData_t3130174490::get_offset_of_x_4(),
	BoneData_t3130174490::get_offset_of_y_5(),
	BoneData_t3130174490::get_offset_of_rotation_6(),
	BoneData_t3130174490::get_offset_of_scaleX_7(),
	BoneData_t3130174490::get_offset_of_scaleY_8(),
	BoneData_t3130174490::get_offset_of_shearX_9(),
	BoneData_t3130174490::get_offset_of_shearY_10(),
	BoneData_t3130174490::get_offset_of_transformMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (TransformMode_t614987360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3628[6] = 
{
	TransformMode_t614987360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (Event_t1378573841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[5] = 
{
	Event_t1378573841::get_offset_of_data_0(),
	Event_t1378573841::get_offset_of_time_1(),
	Event_t1378573841::get_offset_of_intValue_2(),
	Event_t1378573841::get_offset_of_floatValue_3(),
	Event_t1378573841::get_offset_of_stringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (EventData_t724759987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[4] = 
{
	EventData_t724759987::get_offset_of_name_0(),
	EventData_t724759987::get_offset_of_U3CIntU3Ek__BackingField_1(),
	EventData_t724759987::get_offset_of_U3CFloatU3Ek__BackingField_2(),
	EventData_t724759987::get_offset_of_U3CStringU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (IkConstraint_t1675190269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[5] = 
{
	IkConstraint_t1675190269::get_offset_of_data_0(),
	IkConstraint_t1675190269::get_offset_of_bones_1(),
	IkConstraint_t1675190269::get_offset_of_target_2(),
	IkConstraint_t1675190269::get_offset_of_mix_3(),
	IkConstraint_t1675190269::get_offset_of_bendDirection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (IkConstraintData_t459120129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[6] = 
{
	IkConstraintData_t459120129::get_offset_of_name_0(),
	IkConstraintData_t459120129::get_offset_of_order_1(),
	IkConstraintData_t459120129::get_offset_of_bones_2(),
	IkConstraintData_t459120129::get_offset_of_target_3(),
	IkConstraintData_t459120129::get_offset_of_bendDirection_4(),
	IkConstraintData_t459120129::get_offset_of_mix_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (Json_t1494079119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (Lexer_t3897031801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[6] = 
{
	Lexer_t3897031801::get_offset_of_U3ClineNumberU3Ek__BackingField_0(),
	Lexer_t3897031801::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	Lexer_t3897031801::get_offset_of_json_2(),
	Lexer_t3897031801::get_offset_of_index_3(),
	Lexer_t3897031801::get_offset_of_success_4(),
	Lexer_t3897031801::get_offset_of_stringBuffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (Token_t2243639121)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3639[13] = 
{
	Token_t2243639121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (JsonDecoder_t2143190644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3640[3] = 
{
	JsonDecoder_t2143190644::get_offset_of_U3CerrorMessageU3Ek__BackingField_0(),
	JsonDecoder_t2143190644::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	JsonDecoder_t2143190644::get_offset_of_lexer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (MathUtils_t3604673275), -1, sizeof(MathUtils_t3604673275_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3641[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MathUtils_t3604673275_StaticFields::get_offset_of_sin_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (PathConstraint_t980854910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[17] = 
{
	0,
	0,
	0,
	0,
	PathConstraint_t980854910::get_offset_of_data_4(),
	PathConstraint_t980854910::get_offset_of_bones_5(),
	PathConstraint_t980854910::get_offset_of_target_6(),
	PathConstraint_t980854910::get_offset_of_position_7(),
	PathConstraint_t980854910::get_offset_of_spacing_8(),
	PathConstraint_t980854910::get_offset_of_rotateMix_9(),
	PathConstraint_t980854910::get_offset_of_translateMix_10(),
	PathConstraint_t980854910::get_offset_of_spaces_11(),
	PathConstraint_t980854910::get_offset_of_positions_12(),
	PathConstraint_t980854910::get_offset_of_world_13(),
	PathConstraint_t980854910::get_offset_of_curves_14(),
	PathConstraint_t980854910::get_offset_of_lengths_15(),
	PathConstraint_t980854910::get_offset_of_segments_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (PathConstraintData_t981297034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3643[12] = 
{
	PathConstraintData_t981297034::get_offset_of_name_0(),
	PathConstraintData_t981297034::get_offset_of_order_1(),
	PathConstraintData_t981297034::get_offset_of_bones_2(),
	PathConstraintData_t981297034::get_offset_of_target_3(),
	PathConstraintData_t981297034::get_offset_of_positionMode_4(),
	PathConstraintData_t981297034::get_offset_of_spacingMode_5(),
	PathConstraintData_t981297034::get_offset_of_rotateMode_6(),
	PathConstraintData_t981297034::get_offset_of_offsetRotation_7(),
	PathConstraintData_t981297034::get_offset_of_position_8(),
	PathConstraintData_t981297034::get_offset_of_spacing_9(),
	PathConstraintData_t981297034::get_offset_of_rotateMix_10(),
	PathConstraintData_t981297034::get_offset_of_translateMix_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (PositionMode_t2435325583)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3644[3] = 
{
	PositionMode_t2435325583::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (SpacingMode_t3020059698)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3645[4] = 
{
	SpacingMode_t3020059698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (RotateMode_t2880286220)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3646[4] = 
{
	RotateMode_t2880286220::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (Skeleton_t3686076450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[19] = 
{
	Skeleton_t3686076450::get_offset_of_data_0(),
	Skeleton_t3686076450::get_offset_of_bones_1(),
	Skeleton_t3686076450::get_offset_of_slots_2(),
	Skeleton_t3686076450::get_offset_of_drawOrder_3(),
	Skeleton_t3686076450::get_offset_of_ikConstraints_4(),
	Skeleton_t3686076450::get_offset_of_transformConstraints_5(),
	Skeleton_t3686076450::get_offset_of_pathConstraints_6(),
	Skeleton_t3686076450::get_offset_of_updateCache_7(),
	Skeleton_t3686076450::get_offset_of_updateCacheReset_8(),
	Skeleton_t3686076450::get_offset_of_skin_9(),
	Skeleton_t3686076450::get_offset_of_r_10(),
	Skeleton_t3686076450::get_offset_of_g_11(),
	Skeleton_t3686076450::get_offset_of_b_12(),
	Skeleton_t3686076450::get_offset_of_a_13(),
	Skeleton_t3686076450::get_offset_of_time_14(),
	Skeleton_t3686076450::get_offset_of_flipX_15(),
	Skeleton_t3686076450::get_offset_of_flipY_16(),
	Skeleton_t3686076450::get_offset_of_x_17(),
	Skeleton_t3686076450::get_offset_of_y_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (SkeletonBinary_t1796686580), -1, sizeof(SkeletonBinary_t1796686580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3648[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SkeletonBinary_t1796686580::get_offset_of_U3CScaleU3Ek__BackingField_13(),
	SkeletonBinary_t1796686580::get_offset_of_attachmentLoader_14(),
	SkeletonBinary_t1796686580::get_offset_of_buffer_15(),
	SkeletonBinary_t1796686580::get_offset_of_linkedMeshes_16(),
	SkeletonBinary_t1796686580_StaticFields::get_offset_of_TransformModeValues_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (Vertices_t807797978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[2] = 
{
	Vertices_t807797978::get_offset_of_bones_0(),
	Vertices_t807797978::get_offset_of_vertices_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (SkeletonBounds_t352077915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[7] = 
{
	SkeletonBounds_t352077915::get_offset_of_polygonPool_0(),
	SkeletonBounds_t352077915::get_offset_of_minX_1(),
	SkeletonBounds_t352077915::get_offset_of_minY_2(),
	SkeletonBounds_t352077915::get_offset_of_maxX_3(),
	SkeletonBounds_t352077915::get_offset_of_maxY_4(),
	SkeletonBounds_t352077915::get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5(),
	SkeletonBounds_t352077915::get_offset_of_U3CPolygonsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (Polygon_t3315116257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[2] = 
{
	Polygon_t3315116257::get_offset_of_U3CVerticesU3Ek__BackingField_0(),
	Polygon_t3315116257::get_offset_of_U3CCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (SkeletonClipping_t1669006083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[9] = 
{
	SkeletonClipping_t1669006083::get_offset_of_triangulator_0(),
	SkeletonClipping_t1669006083::get_offset_of_clippingPolygon_1(),
	SkeletonClipping_t1669006083::get_offset_of_clipOutput_2(),
	SkeletonClipping_t1669006083::get_offset_of_clippedVertices_3(),
	SkeletonClipping_t1669006083::get_offset_of_clippedTriangles_4(),
	SkeletonClipping_t1669006083::get_offset_of_clippedUVs_5(),
	SkeletonClipping_t1669006083::get_offset_of_scratch_6(),
	SkeletonClipping_t1669006083::get_offset_of_clipAttachment_7(),
	SkeletonClipping_t1669006083::get_offset_of_clippingPolygons_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (SkeletonData_t2032710716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[16] = 
{
	SkeletonData_t2032710716::get_offset_of_name_0(),
	SkeletonData_t2032710716::get_offset_of_bones_1(),
	SkeletonData_t2032710716::get_offset_of_slots_2(),
	SkeletonData_t2032710716::get_offset_of_skins_3(),
	SkeletonData_t2032710716::get_offset_of_defaultSkin_4(),
	SkeletonData_t2032710716::get_offset_of_events_5(),
	SkeletonData_t2032710716::get_offset_of_animations_6(),
	SkeletonData_t2032710716::get_offset_of_ikConstraints_7(),
	SkeletonData_t2032710716::get_offset_of_transformConstraints_8(),
	SkeletonData_t2032710716::get_offset_of_pathConstraints_9(),
	SkeletonData_t2032710716::get_offset_of_width_10(),
	SkeletonData_t2032710716::get_offset_of_height_11(),
	SkeletonData_t2032710716::get_offset_of_version_12(),
	SkeletonData_t2032710716::get_offset_of_hash_13(),
	SkeletonData_t2032710716::get_offset_of_fps_14(),
	SkeletonData_t2032710716::get_offset_of_imagesPath_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (SkeletonJson_t4049771867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3654[3] = 
{
	SkeletonJson_t4049771867::get_offset_of_U3CScaleU3Ek__BackingField_0(),
	SkeletonJson_t4049771867::get_offset_of_attachmentLoader_1(),
	SkeletonJson_t4049771867::get_offset_of_linkedMeshes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (LinkedMesh_t1696919459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[4] = 
{
	LinkedMesh_t1696919459::get_offset_of_parent_0(),
	LinkedMesh_t1696919459::get_offset_of_skin_1(),
	LinkedMesh_t1696919459::get_offset_of_slotIndex_2(),
	LinkedMesh_t1696919459::get_offset_of_mesh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (Skin_t1174584606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[2] = 
{
	Skin_t1174584606::get_offset_of_name_0(),
	Skin_t1174584606::get_offset_of_attachments_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (AttachmentKeyTuple_t1548106539)+ sizeof (RuntimeObject), sizeof(AttachmentKeyTuple_t1548106539_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3657[3] = 
{
	AttachmentKeyTuple_t1548106539::get_offset_of_slotIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_t1548106539::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_t1548106539::get_offset_of_nameHashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (AttachmentKeyTupleComparer_t1167996044), -1, sizeof(AttachmentKeyTupleComparer_t1167996044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3658[1] = 
{
	AttachmentKeyTupleComparer_t1167996044_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (Slot_t3514940700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[13] = 
{
	Slot_t3514940700::get_offset_of_data_0(),
	Slot_t3514940700::get_offset_of_bone_1(),
	Slot_t3514940700::get_offset_of_r_2(),
	Slot_t3514940700::get_offset_of_g_3(),
	Slot_t3514940700::get_offset_of_b_4(),
	Slot_t3514940700::get_offset_of_a_5(),
	Slot_t3514940700::get_offset_of_r2_6(),
	Slot_t3514940700::get_offset_of_g2_7(),
	Slot_t3514940700::get_offset_of_b2_8(),
	Slot_t3514940700::get_offset_of_hasSecondColor_9(),
	Slot_t3514940700::get_offset_of_attachment_10(),
	Slot_t3514940700::get_offset_of_attachmentTime_11(),
	Slot_t3514940700::get_offset_of_attachmentVertices_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (SlotData_t154801902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[13] = 
{
	SlotData_t154801902::get_offset_of_index_0(),
	SlotData_t154801902::get_offset_of_name_1(),
	SlotData_t154801902::get_offset_of_boneData_2(),
	SlotData_t154801902::get_offset_of_r_3(),
	SlotData_t154801902::get_offset_of_g_4(),
	SlotData_t154801902::get_offset_of_b_5(),
	SlotData_t154801902::get_offset_of_a_6(),
	SlotData_t154801902::get_offset_of_r2_7(),
	SlotData_t154801902::get_offset_of_g2_8(),
	SlotData_t154801902::get_offset_of_b2_9(),
	SlotData_t154801902::get_offset_of_hasSecondColor_10(),
	SlotData_t154801902::get_offset_of_attachmentName_11(),
	SlotData_t154801902::get_offset_of_blendMode_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (TransformConstraint_t454030229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[7] = 
{
	TransformConstraint_t454030229::get_offset_of_data_0(),
	TransformConstraint_t454030229::get_offset_of_bones_1(),
	TransformConstraint_t454030229::get_offset_of_target_2(),
	TransformConstraint_t454030229::get_offset_of_rotateMix_3(),
	TransformConstraint_t454030229::get_offset_of_translateMix_4(),
	TransformConstraint_t454030229::get_offset_of_scaleMix_5(),
	TransformConstraint_t454030229::get_offset_of_shearMix_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (TransformConstraintData_t529073346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[16] = 
{
	TransformConstraintData_t529073346::get_offset_of_name_0(),
	TransformConstraintData_t529073346::get_offset_of_order_1(),
	TransformConstraintData_t529073346::get_offset_of_bones_2(),
	TransformConstraintData_t529073346::get_offset_of_target_3(),
	TransformConstraintData_t529073346::get_offset_of_rotateMix_4(),
	TransformConstraintData_t529073346::get_offset_of_translateMix_5(),
	TransformConstraintData_t529073346::get_offset_of_scaleMix_6(),
	TransformConstraintData_t529073346::get_offset_of_shearMix_7(),
	TransformConstraintData_t529073346::get_offset_of_offsetRotation_8(),
	TransformConstraintData_t529073346::get_offset_of_offsetX_9(),
	TransformConstraintData_t529073346::get_offset_of_offsetY_10(),
	TransformConstraintData_t529073346::get_offset_of_offsetScaleX_11(),
	TransformConstraintData_t529073346::get_offset_of_offsetScaleY_12(),
	TransformConstraintData_t529073346::get_offset_of_offsetShearY_13(),
	TransformConstraintData_t529073346::get_offset_of_relative_14(),
	TransformConstraintData_t529073346::get_offset_of_local_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (Triangulator_t2502879214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3663[7] = 
{
	Triangulator_t2502879214::get_offset_of_convexPolygons_0(),
	Triangulator_t2502879214::get_offset_of_convexPolygonsIndices_1(),
	Triangulator_t2502879214::get_offset_of_indicesArray_2(),
	Triangulator_t2502879214::get_offset_of_isConcaveArray_3(),
	Triangulator_t2502879214::get_offset_of_triangles_4(),
	Triangulator_t2502879214::get_offset_of_polygonPool_5(),
	Triangulator_t2502879214::get_offset_of_polygonIndicesPool_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (AtlasAsset_t1167231206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[3] = 
{
	AtlasAsset_t1167231206::get_offset_of_atlasFile_2(),
	AtlasAsset_t1167231206::get_offset_of_materials_3(),
	AtlasAsset_t1167231206::get_offset_of_atlas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (MaterialsTextureLoader_t1402074808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[1] = 
{
	MaterialsTextureLoader_t1402074808::get_offset_of_atlasAsset_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (SkeletonDataAsset_t3748144825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[10] = 
{
	SkeletonDataAsset_t3748144825::get_offset_of_atlasAssets_2(),
	SkeletonDataAsset_t3748144825::get_offset_of_scale_3(),
	SkeletonDataAsset_t3748144825::get_offset_of_skeletonJSON_4(),
	SkeletonDataAsset_t3748144825::get_offset_of_fromAnimation_5(),
	SkeletonDataAsset_t3748144825::get_offset_of_toAnimation_6(),
	SkeletonDataAsset_t3748144825::get_offset_of_duration_7(),
	SkeletonDataAsset_t3748144825::get_offset_of_defaultMix_8(),
	SkeletonDataAsset_t3748144825::get_offset_of_controller_9(),
	SkeletonDataAsset_t3748144825::get_offset_of_skeletonData_10(),
	SkeletonDataAsset_t3748144825::get_offset_of_stateData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (BoneFollower_t3069400636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[11] = 
{
	BoneFollower_t3069400636::get_offset_of_skeletonRenderer_2(),
	BoneFollower_t3069400636::get_offset_of_boneName_3(),
	BoneFollower_t3069400636::get_offset_of_followZPosition_4(),
	BoneFollower_t3069400636::get_offset_of_followBoneRotation_5(),
	BoneFollower_t3069400636::get_offset_of_followSkeletonFlip_6(),
	BoneFollower_t3069400636::get_offset_of_followLocalScale_7(),
	BoneFollower_t3069400636::get_offset_of_initializeOnAwake_8(),
	BoneFollower_t3069400636::get_offset_of_valid_9(),
	BoneFollower_t3069400636::get_offset_of_bone_10(),
	BoneFollower_t3069400636::get_offset_of_skeletonTransform_11(),
	BoneFollower_t3069400636::get_offset_of_skeletonTransformIsParent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (UpdateBonesDelegate_t735903178), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (SpineMesh_t2128742078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (SubmeshInstruction_t52121370)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[10] = 
{
	SubmeshInstruction_t52121370::get_offset_of_skeleton_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_startSlot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_endSlot_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_forceSeparate_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_preActiveClippingSlotSource_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_rawTriangleCount_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_rawVertexCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_rawFirstVertexIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_hasClipping_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (MeshGeneratorDelegate_t1654156803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (MeshGeneratorBuffers_t1424700926)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[5] = 
{
	MeshGeneratorBuffers_t1424700926::get_offset_of_vertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_vertexBuffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_uvBuffer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_colorBuffer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_meshGenerator_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (MeshGenerator_t1354683548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[19] = 
{
	MeshGenerator_t1354683548::get_offset_of_settings_0(),
	0,
	0,
	MeshGenerator_t1354683548::get_offset_of_vertexBuffer_3(),
	MeshGenerator_t1354683548::get_offset_of_uvBuffer_4(),
	MeshGenerator_t1354683548::get_offset_of_colorBuffer_5(),
	MeshGenerator_t1354683548::get_offset_of_submeshes_6(),
	MeshGenerator_t1354683548::get_offset_of_meshBoundsMin_7(),
	MeshGenerator_t1354683548::get_offset_of_meshBoundsMax_8(),
	MeshGenerator_t1354683548::get_offset_of_meshBoundsThickness_9(),
	MeshGenerator_t1354683548::get_offset_of_submeshIndex_10(),
	MeshGenerator_t1354683548::get_offset_of_clipper_11(),
	MeshGenerator_t1354683548::get_offset_of_tempVerts_12(),
	MeshGenerator_t1354683548::get_offset_of_regionTriangles_13(),
	MeshGenerator_t1354683548::get_offset_of_normals_14(),
	MeshGenerator_t1354683548::get_offset_of_tangents_15(),
	MeshGenerator_t1354683548::get_offset_of_tempTanBuffer_16(),
	MeshGenerator_t1354683548::get_offset_of_uv2_17(),
	MeshGenerator_t1354683548::get_offset_of_uv3_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (Settings_t612870457)+ sizeof (RuntimeObject), sizeof(Settings_t612870457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3679[7] = 
{
	Settings_t612870457::get_offset_of_useClipping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_zSpacing_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_pmaVertexColors_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_tintBlack_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_calculateTangents_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_addNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_immutableTriangles_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (MeshRendererBuffers_t756429994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[3] = 
{
	MeshRendererBuffers_t756429994::get_offset_of_doubleBufferedMesh_0(),
	MeshRendererBuffers_t756429994::get_offset_of_submeshMaterials_1(),
	MeshRendererBuffers_t756429994::get_offset_of_sharedMaterials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (SmartMesh_t524811292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[2] = 
{
	SmartMesh_t524811292::get_offset_of_mesh_0(),
	SmartMesh_t524811292::get_offset_of_instructionUsed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (SkeletonRendererInstruction_t651787775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[5] = 
{
	SkeletonRendererInstruction_t651787775::get_offset_of_immutableTriangles_0(),
	SkeletonRendererInstruction_t651787775::get_offset_of_submeshInstructions_1(),
	SkeletonRendererInstruction_t651787775::get_offset_of_hasActiveClipping_2(),
	SkeletonRendererInstruction_t651787775::get_offset_of_rawVertexCount_3(),
	SkeletonRendererInstruction_t651787775::get_offset_of_attachments_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (AttachmentRegionExtensions_t1591891965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (AtlasUtilities_t1517471311), -1, sizeof(AtlasUtilities_t1517471311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3684[6] = 
{
	0,
	0,
	0,
	0,
	AtlasUtilities_t1517471311_StaticFields::get_offset_of_CachedRegionTextures_4(),
	AtlasUtilities_t1517471311_StaticFields::get_offset_of_CachedRegionTexturesList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (SkinUtilities_t498157063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (AttachmentCloneExtensions_t460514876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (BoundingBoxFollower_t4182505745), -1, sizeof(BoundingBoxFollower_t4182505745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3687[11] = 
{
	BoundingBoxFollower_t4182505745_StaticFields::get_offset_of_DebugMessages_2(),
	BoundingBoxFollower_t4182505745::get_offset_of_skeletonRenderer_3(),
	BoundingBoxFollower_t4182505745::get_offset_of_slotName_4(),
	BoundingBoxFollower_t4182505745::get_offset_of_isTrigger_5(),
	BoundingBoxFollower_t4182505745::get_offset_of_clearStateOnDisable_6(),
	BoundingBoxFollower_t4182505745::get_offset_of_slot_7(),
	BoundingBoxFollower_t4182505745::get_offset_of_currentAttachment_8(),
	BoundingBoxFollower_t4182505745::get_offset_of_currentAttachmentName_9(),
	BoundingBoxFollower_t4182505745::get_offset_of_currentCollider_10(),
	BoundingBoxFollower_t4182505745::get_offset_of_colliderTable_11(),
	BoundingBoxFollower_t4182505745::get_offset_of_nameTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (SkeletonRendererCustomMaterials_t1555313811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[3] = 
{
	SkeletonRendererCustomMaterials_t1555313811::get_offset_of_skeletonRenderer_2(),
	SkeletonRendererCustomMaterials_t1555313811::get_offset_of_customSlotMaterials_3(),
	SkeletonRendererCustomMaterials_t1555313811::get_offset_of_customMaterialOverrides_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (SlotMaterialOverride_t1001979181)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[3] = 
{
	SlotMaterialOverride_t1001979181::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_t1001979181::get_offset_of_slotName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_t1001979181::get_offset_of_material_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (AtlasMaterialOverride_t2435041389)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3690[3] = 
{
	AtlasMaterialOverride_t2435041389::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_t2435041389::get_offset_of_originalMaterial_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_t2435041389::get_offset_of_replacementMaterial_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (SkeletonGhost_t1898327037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[19] = 
{
	0,
	0,
	SkeletonGhost_t1898327037::get_offset_of_ghostingEnabled_4(),
	SkeletonGhost_t1898327037::get_offset_of_spawnRate_5(),
	SkeletonGhost_t1898327037::get_offset_of_color_6(),
	SkeletonGhost_t1898327037::get_offset_of_additive_7(),
	SkeletonGhost_t1898327037::get_offset_of_maximumGhosts_8(),
	SkeletonGhost_t1898327037::get_offset_of_fadeSpeed_9(),
	SkeletonGhost_t1898327037::get_offset_of_ghostShader_10(),
	SkeletonGhost_t1898327037::get_offset_of_textureFade_11(),
	SkeletonGhost_t1898327037::get_offset_of_sortWithDistanceOnly_12(),
	SkeletonGhost_t1898327037::get_offset_of_zOffset_13(),
	SkeletonGhost_t1898327037::get_offset_of_nextSpawnTime_14(),
	SkeletonGhost_t1898327037::get_offset_of_pool_15(),
	SkeletonGhost_t1898327037::get_offset_of_poolIndex_16(),
	SkeletonGhost_t1898327037::get_offset_of_skeletonRenderer_17(),
	SkeletonGhost_t1898327037::get_offset_of_meshRenderer_18(),
	SkeletonGhost_t1898327037::get_offset_of_meshFilter_19(),
	SkeletonGhost_t1898327037::get_offset_of_materialTable_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (SkeletonGhostRenderer_t2445315009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[5] = 
{
	SkeletonGhostRenderer_t2445315009::get_offset_of_fadeSpeed_2(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_colors_3(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_black_4(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_meshFilter_5(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_meshRenderer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (U3CFadeU3Ec__Iterator0_t1290633589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3693[7] = 
{
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U3CtU3E__1_0(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U3CbreakoutU3E__2_1(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U3CcU3E__3_2(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24this_3(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24current_4(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24disposing_5(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (U3CFadeAdditiveU3Ec__Iterator1_t410398119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[8] = 
{
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CblackU3E__0_0(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CtU3E__1_1(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CbreakoutU3E__2_2(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CcU3E__3_3(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24this_4(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24current_5(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24disposing_6(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (SkeletonRagdoll_t480923817), -1, sizeof(SkeletonRagdoll_t480923817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3695[23] = 
{
	SkeletonRagdoll_t480923817_StaticFields::get_offset_of_parentSpaceHelper_2(),
	SkeletonRagdoll_t480923817::get_offset_of_startingBoneName_3(),
	SkeletonRagdoll_t480923817::get_offset_of_stopBoneNames_4(),
	SkeletonRagdoll_t480923817::get_offset_of_applyOnStart_5(),
	SkeletonRagdoll_t480923817::get_offset_of_disableIK_6(),
	SkeletonRagdoll_t480923817::get_offset_of_disableOtherConstraints_7(),
	SkeletonRagdoll_t480923817::get_offset_of_pinStartBone_8(),
	SkeletonRagdoll_t480923817::get_offset_of_enableJointCollision_9(),
	SkeletonRagdoll_t480923817::get_offset_of_useGravity_10(),
	SkeletonRagdoll_t480923817::get_offset_of_thickness_11(),
	SkeletonRagdoll_t480923817::get_offset_of_rotationLimit_12(),
	SkeletonRagdoll_t480923817::get_offset_of_rootMass_13(),
	SkeletonRagdoll_t480923817::get_offset_of_massFalloffFactor_14(),
	SkeletonRagdoll_t480923817::get_offset_of_colliderLayer_15(),
	SkeletonRagdoll_t480923817::get_offset_of_mix_16(),
	SkeletonRagdoll_t480923817::get_offset_of_targetSkeletonComponent_17(),
	SkeletonRagdoll_t480923817::get_offset_of_skeleton_18(),
	SkeletonRagdoll_t480923817::get_offset_of_boneTable_19(),
	SkeletonRagdoll_t480923817::get_offset_of_ragdollRoot_20(),
	SkeletonRagdoll_t480923817::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_21(),
	SkeletonRagdoll_t480923817::get_offset_of_U3CStartingBoneU3Ek__BackingField_22(),
	SkeletonRagdoll_t480923817::get_offset_of_rootOffset_23(),
	SkeletonRagdoll_t480923817::get_offset_of_isActive_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (LayerFieldAttribute_t3408802261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (U3CStartU3Ec__Iterator0_t294813647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[4] = 
{
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[8] = 
{
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U3CstartMixU3E__0_1(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_target_2(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_duration_3(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24this_4(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24current_5(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24disposing_6(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (SkeletonRagdoll2D_t2972821364), -1, sizeof(SkeletonRagdoll2D_t2972821364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3699[22] = 
{
	SkeletonRagdoll2D_t2972821364_StaticFields::get_offset_of_parentSpaceHelper_2(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_startingBoneName_3(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_stopBoneNames_4(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_applyOnStart_5(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_disableIK_6(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_disableOtherConstraints_7(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_pinStartBone_8(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_gravityScale_9(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_thickness_10(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_rotationLimit_11(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_rootMass_12(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_massFalloffFactor_13(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_colliderLayer_14(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_mix_15(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_targetSkeletonComponent_16(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_skeleton_17(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_boneTable_18(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_ragdollRoot_19(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_20(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_U3CStartingBoneU3Ek__BackingField_21(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_rootOffset_22(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_isActive_23(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
