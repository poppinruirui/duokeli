﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<System.Threading.Thread>
struct List_1_t3772910811;
// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate
struct IntegerMillisecondsDelegate_t651311252;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// ExitGames.Client.Photon.NCommand
struct NCommand_t1230688399;
// ExitGames.Client.Photon.EnetPeer
struct EnetPeer_t430442630;
// Photon.SocketServer.Numeric.BigInteger
struct BigInteger_t956758543;
// System.Security.Cryptography.Rijndael
struct Rijndael_t2986313634;
// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.NCommand>
struct Dictionary_2_t119401730;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>
struct Queue_1_t1076947893;
// ExitGames.Client.Photon.TPeer
struct TPeer_t1497954812;
// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_1
struct U3CU3Ec__DisplayClass41_1_t2028097817;
// System.Random
struct Random_t108471755;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1405253484;
// System.Type
struct Type_t;
// ExitGames.Client.Photon.SerializeMethod
struct SerializeMethod_t1264674278;
// ExitGames.Client.Photon.DeserializeMethod
struct DeserializeMethod_t3915517082;
// ExitGames.Client.Photon.SerializeStreamMethod
struct SerializeStreamMethod_t2169445464;
// ExitGames.Client.Photon.DeserializeStreamMethod
struct DeserializeStreamMethod_t3070531629;
// ExitGames.Client.IProtocol
struct IProtocol_t3448746462;
// System.Collections.Generic.Dictionary`2<System.Type,ExitGames.Client.Photon.CustomType>
struct Dictionary_2_t2175443087;
// System.Collections.Generic.Dictionary`2<System.Byte,ExitGames.Client.Photon.CustomType>
struct Dictionary_2_t2351210639;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1
struct U3CU3Ec__DisplayClass56_1_t3313335349;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// ExitGames.Client.Photon.IPhotonSocket
struct IPhotonSocket_t2066969247;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4209139644;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t3163629820;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// ExitGames.Client.Photon.PeerBase/MyAction
struct MyAction_t2462891903;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Void
struct Void_t1185182177;
// System.Net.Sockets.Socket
struct Socket_t1119025450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t1608153861;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>
struct Queue_1_t2309151397;
// Photon.SocketServer.Security.ICryptoProvider
struct ICryptoProvider_t1662250179;
// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>
struct LinkedList_1_t1884284488;
// ExitGames.Client.Photon.NetworkSimulationSet
struct NetworkSimulationSet_t2000596048;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem>
struct Queue_1_t4063950034;
// ExitGames.Client.Photon.StreamBuffer
struct StreamBuffer_t3827669789;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<ExitGames.Client.Photon.NCommand>
struct List_1_t2702763141;
// ExitGames.Client.Photon.EnetChannel[]
struct EnetChannelU5BU5D_t2709601441;
// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t2797205247;
// System.Collections.Generic.Queue`1<System.Byte[]>
struct Queue_1_t3962907151;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t3436254912;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_t3211687919;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t445758600;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t3848515759;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;

struct ContactPoint_t3758755253 ;



#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745541_H
#define U3CMODULEU3E_T692745541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745541 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745541_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745542_H
#define U3CMODULEU3E_T692745542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745542 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745542_H
#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef CMDLOGITEM_T4217690540_H
#define CMDLOGITEM_T4217690540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogItem
struct  CmdLogItem_t4217690540  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::TimeInt
	int32_t ___TimeInt_0;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::Channel
	int32_t ___Channel_1;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::SequenceNumber
	int32_t ___SequenceNumber_2;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::Rtt
	int32_t ___Rtt_3;
	// System.Int32 ExitGames.Client.Photon.CmdLogItem::Variance
	int32_t ___Variance_4;

public:
	inline static int32_t get_offset_of_TimeInt_0() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___TimeInt_0)); }
	inline int32_t get_TimeInt_0() const { return ___TimeInt_0; }
	inline int32_t* get_address_of_TimeInt_0() { return &___TimeInt_0; }
	inline void set_TimeInt_0(int32_t value)
	{
		___TimeInt_0 = value;
	}

	inline static int32_t get_offset_of_Channel_1() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___Channel_1)); }
	inline int32_t get_Channel_1() const { return ___Channel_1; }
	inline int32_t* get_address_of_Channel_1() { return &___Channel_1; }
	inline void set_Channel_1(int32_t value)
	{
		___Channel_1 = value;
	}

	inline static int32_t get_offset_of_SequenceNumber_2() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___SequenceNumber_2)); }
	inline int32_t get_SequenceNumber_2() const { return ___SequenceNumber_2; }
	inline int32_t* get_address_of_SequenceNumber_2() { return &___SequenceNumber_2; }
	inline void set_SequenceNumber_2(int32_t value)
	{
		___SequenceNumber_2 = value;
	}

	inline static int32_t get_offset_of_Rtt_3() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___Rtt_3)); }
	inline int32_t get_Rtt_3() const { return ___Rtt_3; }
	inline int32_t* get_address_of_Rtt_3() { return &___Rtt_3; }
	inline void set_Rtt_3(int32_t value)
	{
		___Rtt_3 = value;
	}

	inline static int32_t get_offset_of_Variance_4() { return static_cast<int32_t>(offsetof(CmdLogItem_t4217690540, ___Variance_4)); }
	inline int32_t get_Variance_4() const { return ___Variance_4; }
	inline int32_t* get_address_of_Variance_4() { return &___Variance_4; }
	inline void set_Variance_4(int32_t value)
	{
		___Variance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGITEM_T4217690540_H
#ifndef SUPPORTCLASS_T2974952451_H
#define SUPPORTCLASS_T2974952451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass
struct  SupportClass_t2974952451  : public RuntimeObject
{
public:

public:
};

struct SupportClass_t2974952451_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Threading.Thread> ExitGames.Client.Photon.SupportClass::threadList
	List_1_t3772910811 * ___threadList_0;
	// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate ExitGames.Client.Photon.SupportClass::IntegerMilliseconds
	IntegerMillisecondsDelegate_t651311252 * ___IntegerMilliseconds_1;

public:
	inline static int32_t get_offset_of_threadList_0() { return static_cast<int32_t>(offsetof(SupportClass_t2974952451_StaticFields, ___threadList_0)); }
	inline List_1_t3772910811 * get_threadList_0() const { return ___threadList_0; }
	inline List_1_t3772910811 ** get_address_of_threadList_0() { return &___threadList_0; }
	inline void set_threadList_0(List_1_t3772910811 * value)
	{
		___threadList_0 = value;
		Il2CppCodeGenWriteBarrier((&___threadList_0), value);
	}

	inline static int32_t get_offset_of_IntegerMilliseconds_1() { return static_cast<int32_t>(offsetof(SupportClass_t2974952451_StaticFields, ___IntegerMilliseconds_1)); }
	inline IntegerMillisecondsDelegate_t651311252 * get_IntegerMilliseconds_1() const { return ___IntegerMilliseconds_1; }
	inline IntegerMillisecondsDelegate_t651311252 ** get_address_of_IntegerMilliseconds_1() { return &___IntegerMilliseconds_1; }
	inline void set_IntegerMilliseconds_1(IntegerMillisecondsDelegate_t651311252 * value)
	{
		___IntegerMilliseconds_1 = value;
		Il2CppCodeGenWriteBarrier((&___IntegerMilliseconds_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTCLASS_T2974952451_H
#ifndef VERSION_T2916202802_H
#define VERSION_T2916202802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Version
struct  Version_t2916202802  : public RuntimeObject
{
public:

public:
};

struct Version_t2916202802_StaticFields
{
public:
	// System.Byte[] ExitGames.Client.Photon.Version::clientVersion
	ByteU5BU5D_t4116647657* ___clientVersion_0;

public:
	inline static int32_t get_offset_of_clientVersion_0() { return static_cast<int32_t>(offsetof(Version_t2916202802_StaticFields, ___clientVersion_0)); }
	inline ByteU5BU5D_t4116647657* get_clientVersion_0() const { return ___clientVersion_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_clientVersion_0() { return &___clientVersion_0; }
	inline void set_clientVersion_0(ByteU5BU5D_t4116647657* value)
	{
		___clientVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T2916202802_H
#ifndef IPROTOCOL_T3448746462_H
#define IPROTOCOL_T3448746462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.IProtocol
struct  IProtocol_t3448746462  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROTOCOL_T3448746462_H
#ifndef BIGINTEGER_T956758543_H
#define BIGINTEGER_T956758543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.SocketServer.Numeric.BigInteger
struct  BigInteger_t956758543  : public RuntimeObject
{
public:
	// System.UInt32[] Photon.SocketServer.Numeric.BigInteger::data
	UInt32U5BU5D_t2770800703* ___data_1;
	// System.Int32 Photon.SocketServer.Numeric.BigInteger::dataLength
	int32_t ___dataLength_2;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(BigInteger_t956758543, ___data_1)); }
	inline UInt32U5BU5D_t2770800703* get_data_1() const { return ___data_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(UInt32U5BU5D_t2770800703* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_dataLength_2() { return static_cast<int32_t>(offsetof(BigInteger_t956758543, ___dataLength_2)); }
	inline int32_t get_dataLength_2() const { return ___dataLength_2; }
	inline int32_t* get_address_of_dataLength_2() { return &___dataLength_2; }
	inline void set_dataLength_2(int32_t value)
	{
		___dataLength_2 = value;
	}
};

struct BigInteger_t956758543_StaticFields
{
public:
	// System.Int32[] Photon.SocketServer.Numeric.BigInteger::primesBelow2000
	Int32U5BU5D_t385246372* ___primesBelow2000_0;

public:
	inline static int32_t get_offset_of_primesBelow2000_0() { return static_cast<int32_t>(offsetof(BigInteger_t956758543_StaticFields, ___primesBelow2000_0)); }
	inline Int32U5BU5D_t385246372* get_primesBelow2000_0() const { return ___primesBelow2000_0; }
	inline Int32U5BU5D_t385246372** get_address_of_primesBelow2000_0() { return &___primesBelow2000_0; }
	inline void set_primesBelow2000_0(Int32U5BU5D_t385246372* value)
	{
		___primesBelow2000_0 = value;
		Il2CppCodeGenWriteBarrier((&___primesBelow2000_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T956758543_H
#ifndef U3CU3EC__DISPLAYCLASS62_0_T982511824_H
#define U3CU3EC__DISPLAYCLASS62_0_T982511824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass62_0
struct  U3CU3Ec__DisplayClass62_0_t982511824  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass62_0::readCommand
	NCommand_t1230688399 * ___readCommand_0;
	// ExitGames.Client.Photon.EnetPeer ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass62_0::<>4__this
	EnetPeer_t430442630 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_readCommand_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t982511824, ___readCommand_0)); }
	inline NCommand_t1230688399 * get_readCommand_0() const { return ___readCommand_0; }
	inline NCommand_t1230688399 ** get_address_of_readCommand_0() { return &___readCommand_0; }
	inline void set_readCommand_0(NCommand_t1230688399 * value)
	{
		___readCommand_0 = value;
		Il2CppCodeGenWriteBarrier((&___readCommand_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t982511824, ___U3CU3E4__this_1)); }
	inline EnetPeer_t430442630 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline EnetPeer_t430442630 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(EnetPeer_t430442630 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS62_0_T982511824_H
#ifndef DIFFIEHELLMANCRYPTOPROVIDER_T915317458_H
#define DIFFIEHELLMANCRYPTOPROVIDER_T915317458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.SocketServer.Security.DiffieHellmanCryptoProvider
struct  DiffieHellmanCryptoProvider_t915317458  : public RuntimeObject
{
public:
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::prime
	BigInteger_t956758543 * ___prime_1;
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::secret
	BigInteger_t956758543 * ___secret_2;
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::publicKey
	BigInteger_t956758543 * ___publicKey_3;
	// System.Security.Cryptography.Rijndael Photon.SocketServer.Security.DiffieHellmanCryptoProvider::crypto
	Rijndael_t2986313634 * ___crypto_4;
	// System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::sharedKey
	ByteU5BU5D_t4116647657* ___sharedKey_5;

public:
	inline static int32_t get_offset_of_prime_1() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___prime_1)); }
	inline BigInteger_t956758543 * get_prime_1() const { return ___prime_1; }
	inline BigInteger_t956758543 ** get_address_of_prime_1() { return &___prime_1; }
	inline void set_prime_1(BigInteger_t956758543 * value)
	{
		___prime_1 = value;
		Il2CppCodeGenWriteBarrier((&___prime_1), value);
	}

	inline static int32_t get_offset_of_secret_2() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___secret_2)); }
	inline BigInteger_t956758543 * get_secret_2() const { return ___secret_2; }
	inline BigInteger_t956758543 ** get_address_of_secret_2() { return &___secret_2; }
	inline void set_secret_2(BigInteger_t956758543 * value)
	{
		___secret_2 = value;
		Il2CppCodeGenWriteBarrier((&___secret_2), value);
	}

	inline static int32_t get_offset_of_publicKey_3() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___publicKey_3)); }
	inline BigInteger_t956758543 * get_publicKey_3() const { return ___publicKey_3; }
	inline BigInteger_t956758543 ** get_address_of_publicKey_3() { return &___publicKey_3; }
	inline void set_publicKey_3(BigInteger_t956758543 * value)
	{
		___publicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_3), value);
	}

	inline static int32_t get_offset_of_crypto_4() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___crypto_4)); }
	inline Rijndael_t2986313634 * get_crypto_4() const { return ___crypto_4; }
	inline Rijndael_t2986313634 ** get_address_of_crypto_4() { return &___crypto_4; }
	inline void set_crypto_4(Rijndael_t2986313634 * value)
	{
		___crypto_4 = value;
		Il2CppCodeGenWriteBarrier((&___crypto_4), value);
	}

	inline static int32_t get_offset_of_sharedKey_5() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458, ___sharedKey_5)); }
	inline ByteU5BU5D_t4116647657* get_sharedKey_5() const { return ___sharedKey_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_sharedKey_5() { return &___sharedKey_5; }
	inline void set_sharedKey_5(ByteU5BU5D_t4116647657* value)
	{
		___sharedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___sharedKey_5), value);
	}
};

struct DiffieHellmanCryptoProvider_t915317458_StaticFields
{
public:
	// Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::primeRoot
	BigInteger_t956758543 * ___primeRoot_0;

public:
	inline static int32_t get_offset_of_primeRoot_0() { return static_cast<int32_t>(offsetof(DiffieHellmanCryptoProvider_t915317458_StaticFields, ___primeRoot_0)); }
	inline BigInteger_t956758543 * get_primeRoot_0() const { return ___primeRoot_0; }
	inline BigInteger_t956758543 ** get_address_of_primeRoot_0() { return &___primeRoot_0; }
	inline void set_primeRoot_0(BigInteger_t956758543 * value)
	{
		___primeRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___primeRoot_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIFFIEHELLMANCRYPTOPROVIDER_T915317458_H
#ifndef ENETCHANNEL_T2207795168_H
#define ENETCHANNEL_T2207795168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetChannel
struct  EnetChannel_t2207795168  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.EnetChannel::ChannelNumber
	uint8_t ___ChannelNumber_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::incomingReliableCommandsList
	Dictionary_2_t119401730 * ___incomingReliableCommandsList_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::incomingUnreliableCommandsList
	Dictionary_2_t119401730 * ___incomingUnreliableCommandsList_2;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::outgoingReliableCommandsList
	Queue_1_t1076947893 * ___outgoingReliableCommandsList_3;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetChannel::outgoingUnreliableCommandsList
	Queue_1_t1076947893 * ___outgoingUnreliableCommandsList_4;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::incomingReliableSequenceNumber
	int32_t ___incomingReliableSequenceNumber_5;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::incomingUnreliableSequenceNumber
	int32_t ___incomingUnreliableSequenceNumber_6;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::outgoingReliableSequenceNumber
	int32_t ___outgoingReliableSequenceNumber_7;
	// System.Int32 ExitGames.Client.Photon.EnetChannel::outgoingUnreliableSequenceNumber
	int32_t ___outgoingUnreliableSequenceNumber_8;

public:
	inline static int32_t get_offset_of_ChannelNumber_0() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___ChannelNumber_0)); }
	inline uint8_t get_ChannelNumber_0() const { return ___ChannelNumber_0; }
	inline uint8_t* get_address_of_ChannelNumber_0() { return &___ChannelNumber_0; }
	inline void set_ChannelNumber_0(uint8_t value)
	{
		___ChannelNumber_0 = value;
	}

	inline static int32_t get_offset_of_incomingReliableCommandsList_1() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingReliableCommandsList_1)); }
	inline Dictionary_2_t119401730 * get_incomingReliableCommandsList_1() const { return ___incomingReliableCommandsList_1; }
	inline Dictionary_2_t119401730 ** get_address_of_incomingReliableCommandsList_1() { return &___incomingReliableCommandsList_1; }
	inline void set_incomingReliableCommandsList_1(Dictionary_2_t119401730 * value)
	{
		___incomingReliableCommandsList_1 = value;
		Il2CppCodeGenWriteBarrier((&___incomingReliableCommandsList_1), value);
	}

	inline static int32_t get_offset_of_incomingUnreliableCommandsList_2() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingUnreliableCommandsList_2)); }
	inline Dictionary_2_t119401730 * get_incomingUnreliableCommandsList_2() const { return ___incomingUnreliableCommandsList_2; }
	inline Dictionary_2_t119401730 ** get_address_of_incomingUnreliableCommandsList_2() { return &___incomingUnreliableCommandsList_2; }
	inline void set_incomingUnreliableCommandsList_2(Dictionary_2_t119401730 * value)
	{
		___incomingUnreliableCommandsList_2 = value;
		Il2CppCodeGenWriteBarrier((&___incomingUnreliableCommandsList_2), value);
	}

	inline static int32_t get_offset_of_outgoingReliableCommandsList_3() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingReliableCommandsList_3)); }
	inline Queue_1_t1076947893 * get_outgoingReliableCommandsList_3() const { return ___outgoingReliableCommandsList_3; }
	inline Queue_1_t1076947893 ** get_address_of_outgoingReliableCommandsList_3() { return &___outgoingReliableCommandsList_3; }
	inline void set_outgoingReliableCommandsList_3(Queue_1_t1076947893 * value)
	{
		___outgoingReliableCommandsList_3 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingReliableCommandsList_3), value);
	}

	inline static int32_t get_offset_of_outgoingUnreliableCommandsList_4() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingUnreliableCommandsList_4)); }
	inline Queue_1_t1076947893 * get_outgoingUnreliableCommandsList_4() const { return ___outgoingUnreliableCommandsList_4; }
	inline Queue_1_t1076947893 ** get_address_of_outgoingUnreliableCommandsList_4() { return &___outgoingUnreliableCommandsList_4; }
	inline void set_outgoingUnreliableCommandsList_4(Queue_1_t1076947893 * value)
	{
		___outgoingUnreliableCommandsList_4 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingUnreliableCommandsList_4), value);
	}

	inline static int32_t get_offset_of_incomingReliableSequenceNumber_5() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingReliableSequenceNumber_5)); }
	inline int32_t get_incomingReliableSequenceNumber_5() const { return ___incomingReliableSequenceNumber_5; }
	inline int32_t* get_address_of_incomingReliableSequenceNumber_5() { return &___incomingReliableSequenceNumber_5; }
	inline void set_incomingReliableSequenceNumber_5(int32_t value)
	{
		___incomingReliableSequenceNumber_5 = value;
	}

	inline static int32_t get_offset_of_incomingUnreliableSequenceNumber_6() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___incomingUnreliableSequenceNumber_6)); }
	inline int32_t get_incomingUnreliableSequenceNumber_6() const { return ___incomingUnreliableSequenceNumber_6; }
	inline int32_t* get_address_of_incomingUnreliableSequenceNumber_6() { return &___incomingUnreliableSequenceNumber_6; }
	inline void set_incomingUnreliableSequenceNumber_6(int32_t value)
	{
		___incomingUnreliableSequenceNumber_6 = value;
	}

	inline static int32_t get_offset_of_outgoingReliableSequenceNumber_7() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingReliableSequenceNumber_7)); }
	inline int32_t get_outgoingReliableSequenceNumber_7() const { return ___outgoingReliableSequenceNumber_7; }
	inline int32_t* get_address_of_outgoingReliableSequenceNumber_7() { return &___outgoingReliableSequenceNumber_7; }
	inline void set_outgoingReliableSequenceNumber_7(int32_t value)
	{
		___outgoingReliableSequenceNumber_7 = value;
	}

	inline static int32_t get_offset_of_outgoingUnreliableSequenceNumber_8() { return static_cast<int32_t>(offsetof(EnetChannel_t2207795168, ___outgoingUnreliableSequenceNumber_8)); }
	inline int32_t get_outgoingUnreliableSequenceNumber_8() const { return ___outgoingUnreliableSequenceNumber_8; }
	inline int32_t* get_address_of_outgoingUnreliableSequenceNumber_8() { return &___outgoingUnreliableSequenceNumber_8; }
	inline void set_outgoingUnreliableSequenceNumber_8(int32_t value)
	{
		___outgoingUnreliableSequenceNumber_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENETCHANNEL_T2207795168_H
#ifndef OAKLEYGROUPS_T1704371988_H
#define OAKLEYGROUPS_T1704371988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.SocketServer.Security.OakleyGroups
struct  OakleyGroups_t1704371988  : public RuntimeObject
{
public:

public:
};

struct OakleyGroups_t1704371988_StaticFields
{
public:
	// System.Int32 Photon.SocketServer.Security.OakleyGroups::Generator
	int32_t ___Generator_0;
	// System.Byte[] Photon.SocketServer.Security.OakleyGroups::OakleyPrime768
	ByteU5BU5D_t4116647657* ___OakleyPrime768_1;
	// System.Byte[] Photon.SocketServer.Security.OakleyGroups::OakleyPrime1024
	ByteU5BU5D_t4116647657* ___OakleyPrime1024_2;
	// System.Byte[] Photon.SocketServer.Security.OakleyGroups::OakleyPrime1536
	ByteU5BU5D_t4116647657* ___OakleyPrime1536_3;

public:
	inline static int32_t get_offset_of_Generator_0() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___Generator_0)); }
	inline int32_t get_Generator_0() const { return ___Generator_0; }
	inline int32_t* get_address_of_Generator_0() { return &___Generator_0; }
	inline void set_Generator_0(int32_t value)
	{
		___Generator_0 = value;
	}

	inline static int32_t get_offset_of_OakleyPrime768_1() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___OakleyPrime768_1)); }
	inline ByteU5BU5D_t4116647657* get_OakleyPrime768_1() const { return ___OakleyPrime768_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_OakleyPrime768_1() { return &___OakleyPrime768_1; }
	inline void set_OakleyPrime768_1(ByteU5BU5D_t4116647657* value)
	{
		___OakleyPrime768_1 = value;
		Il2CppCodeGenWriteBarrier((&___OakleyPrime768_1), value);
	}

	inline static int32_t get_offset_of_OakleyPrime1024_2() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___OakleyPrime1024_2)); }
	inline ByteU5BU5D_t4116647657* get_OakleyPrime1024_2() const { return ___OakleyPrime1024_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_OakleyPrime1024_2() { return &___OakleyPrime1024_2; }
	inline void set_OakleyPrime1024_2(ByteU5BU5D_t4116647657* value)
	{
		___OakleyPrime1024_2 = value;
		Il2CppCodeGenWriteBarrier((&___OakleyPrime1024_2), value);
	}

	inline static int32_t get_offset_of_OakleyPrime1536_3() { return static_cast<int32_t>(offsetof(OakleyGroups_t1704371988_StaticFields, ___OakleyPrime1536_3)); }
	inline ByteU5BU5D_t4116647657* get_OakleyPrime1536_3() const { return ___OakleyPrime1536_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_OakleyPrime1536_3() { return &___OakleyPrime1536_3; }
	inline void set_OakleyPrime1536_3(ByteU5BU5D_t4116647657* value)
	{
		___OakleyPrime1536_3 = value;
		Il2CppCodeGenWriteBarrier((&___OakleyPrime1536_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OAKLEYGROUPS_T1704371988_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T3066328396_H
#define U3CU3EC__DISPLAYCLASS30_0_T3066328396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.TPeer/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t3066328396  : public RuntimeObject
{
public:
	// System.Byte[] ExitGames.Client.Photon.TPeer/<>c__DisplayClass30_0::data
	ByteU5BU5D_t4116647657* ___data_0;
	// ExitGames.Client.Photon.TPeer ExitGames.Client.Photon.TPeer/<>c__DisplayClass30_0::<>4__this
	TPeer_t1497954812 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t3066328396, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t3066328396, ___U3CU3E4__this_1)); }
	inline TPeer_t1497954812 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TPeer_t1497954812 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TPeer_t1497954812 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T3066328396_H
#ifndef NCOMMAND_T1230688399_H
#define NCOMMAND_T1230688399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.NCommand
struct  NCommand_t1230688399  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.NCommand::commandFlags
	uint8_t ___commandFlags_0;
	// System.Byte ExitGames.Client.Photon.NCommand::commandType
	uint8_t ___commandType_6;
	// System.Byte ExitGames.Client.Photon.NCommand::commandChannelID
	uint8_t ___commandChannelID_17;
	// System.Int32 ExitGames.Client.Photon.NCommand::reliableSequenceNumber
	int32_t ___reliableSequenceNumber_18;
	// System.Int32 ExitGames.Client.Photon.NCommand::unreliableSequenceNumber
	int32_t ___unreliableSequenceNumber_19;
	// System.Int32 ExitGames.Client.Photon.NCommand::unsequencedGroupNumber
	int32_t ___unsequencedGroupNumber_20;
	// System.Byte ExitGames.Client.Photon.NCommand::reservedByte
	uint8_t ___reservedByte_21;
	// System.Int32 ExitGames.Client.Photon.NCommand::startSequenceNumber
	int32_t ___startSequenceNumber_22;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentCount
	int32_t ___fragmentCount_23;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentNumber
	int32_t ___fragmentNumber_24;
	// System.Int32 ExitGames.Client.Photon.NCommand::totalLength
	int32_t ___totalLength_25;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentOffset
	int32_t ___fragmentOffset_26;
	// System.Int32 ExitGames.Client.Photon.NCommand::fragmentsRemaining
	int32_t ___fragmentsRemaining_27;
	// System.Int32 ExitGames.Client.Photon.NCommand::commandSentTime
	int32_t ___commandSentTime_28;
	// System.Byte ExitGames.Client.Photon.NCommand::commandSentCount
	uint8_t ___commandSentCount_29;
	// System.Int32 ExitGames.Client.Photon.NCommand::roundTripTimeout
	int32_t ___roundTripTimeout_30;
	// System.Int32 ExitGames.Client.Photon.NCommand::timeoutTime
	int32_t ___timeoutTime_31;
	// System.Int32 ExitGames.Client.Photon.NCommand::ackReceivedReliableSequenceNumber
	int32_t ___ackReceivedReliableSequenceNumber_32;
	// System.Int32 ExitGames.Client.Photon.NCommand::ackReceivedSentTime
	int32_t ___ackReceivedSentTime_33;
	// System.Int32 ExitGames.Client.Photon.NCommand::Size
	int32_t ___Size_45;
	// System.Byte[] ExitGames.Client.Photon.NCommand::commandHeader
	ByteU5BU5D_t4116647657* ___commandHeader_46;
	// System.Int32 ExitGames.Client.Photon.NCommand::SizeOfHeader
	int32_t ___SizeOfHeader_47;
	// System.Byte[] ExitGames.Client.Photon.NCommand::Payload
	ByteU5BU5D_t4116647657* ___Payload_48;

public:
	inline static int32_t get_offset_of_commandFlags_0() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandFlags_0)); }
	inline uint8_t get_commandFlags_0() const { return ___commandFlags_0; }
	inline uint8_t* get_address_of_commandFlags_0() { return &___commandFlags_0; }
	inline void set_commandFlags_0(uint8_t value)
	{
		___commandFlags_0 = value;
	}

	inline static int32_t get_offset_of_commandType_6() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandType_6)); }
	inline uint8_t get_commandType_6() const { return ___commandType_6; }
	inline uint8_t* get_address_of_commandType_6() { return &___commandType_6; }
	inline void set_commandType_6(uint8_t value)
	{
		___commandType_6 = value;
	}

	inline static int32_t get_offset_of_commandChannelID_17() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandChannelID_17)); }
	inline uint8_t get_commandChannelID_17() const { return ___commandChannelID_17; }
	inline uint8_t* get_address_of_commandChannelID_17() { return &___commandChannelID_17; }
	inline void set_commandChannelID_17(uint8_t value)
	{
		___commandChannelID_17 = value;
	}

	inline static int32_t get_offset_of_reliableSequenceNumber_18() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___reliableSequenceNumber_18)); }
	inline int32_t get_reliableSequenceNumber_18() const { return ___reliableSequenceNumber_18; }
	inline int32_t* get_address_of_reliableSequenceNumber_18() { return &___reliableSequenceNumber_18; }
	inline void set_reliableSequenceNumber_18(int32_t value)
	{
		___reliableSequenceNumber_18 = value;
	}

	inline static int32_t get_offset_of_unreliableSequenceNumber_19() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___unreliableSequenceNumber_19)); }
	inline int32_t get_unreliableSequenceNumber_19() const { return ___unreliableSequenceNumber_19; }
	inline int32_t* get_address_of_unreliableSequenceNumber_19() { return &___unreliableSequenceNumber_19; }
	inline void set_unreliableSequenceNumber_19(int32_t value)
	{
		___unreliableSequenceNumber_19 = value;
	}

	inline static int32_t get_offset_of_unsequencedGroupNumber_20() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___unsequencedGroupNumber_20)); }
	inline int32_t get_unsequencedGroupNumber_20() const { return ___unsequencedGroupNumber_20; }
	inline int32_t* get_address_of_unsequencedGroupNumber_20() { return &___unsequencedGroupNumber_20; }
	inline void set_unsequencedGroupNumber_20(int32_t value)
	{
		___unsequencedGroupNumber_20 = value;
	}

	inline static int32_t get_offset_of_reservedByte_21() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___reservedByte_21)); }
	inline uint8_t get_reservedByte_21() const { return ___reservedByte_21; }
	inline uint8_t* get_address_of_reservedByte_21() { return &___reservedByte_21; }
	inline void set_reservedByte_21(uint8_t value)
	{
		___reservedByte_21 = value;
	}

	inline static int32_t get_offset_of_startSequenceNumber_22() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___startSequenceNumber_22)); }
	inline int32_t get_startSequenceNumber_22() const { return ___startSequenceNumber_22; }
	inline int32_t* get_address_of_startSequenceNumber_22() { return &___startSequenceNumber_22; }
	inline void set_startSequenceNumber_22(int32_t value)
	{
		___startSequenceNumber_22 = value;
	}

	inline static int32_t get_offset_of_fragmentCount_23() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentCount_23)); }
	inline int32_t get_fragmentCount_23() const { return ___fragmentCount_23; }
	inline int32_t* get_address_of_fragmentCount_23() { return &___fragmentCount_23; }
	inline void set_fragmentCount_23(int32_t value)
	{
		___fragmentCount_23 = value;
	}

	inline static int32_t get_offset_of_fragmentNumber_24() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentNumber_24)); }
	inline int32_t get_fragmentNumber_24() const { return ___fragmentNumber_24; }
	inline int32_t* get_address_of_fragmentNumber_24() { return &___fragmentNumber_24; }
	inline void set_fragmentNumber_24(int32_t value)
	{
		___fragmentNumber_24 = value;
	}

	inline static int32_t get_offset_of_totalLength_25() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___totalLength_25)); }
	inline int32_t get_totalLength_25() const { return ___totalLength_25; }
	inline int32_t* get_address_of_totalLength_25() { return &___totalLength_25; }
	inline void set_totalLength_25(int32_t value)
	{
		___totalLength_25 = value;
	}

	inline static int32_t get_offset_of_fragmentOffset_26() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentOffset_26)); }
	inline int32_t get_fragmentOffset_26() const { return ___fragmentOffset_26; }
	inline int32_t* get_address_of_fragmentOffset_26() { return &___fragmentOffset_26; }
	inline void set_fragmentOffset_26(int32_t value)
	{
		___fragmentOffset_26 = value;
	}

	inline static int32_t get_offset_of_fragmentsRemaining_27() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___fragmentsRemaining_27)); }
	inline int32_t get_fragmentsRemaining_27() const { return ___fragmentsRemaining_27; }
	inline int32_t* get_address_of_fragmentsRemaining_27() { return &___fragmentsRemaining_27; }
	inline void set_fragmentsRemaining_27(int32_t value)
	{
		___fragmentsRemaining_27 = value;
	}

	inline static int32_t get_offset_of_commandSentTime_28() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandSentTime_28)); }
	inline int32_t get_commandSentTime_28() const { return ___commandSentTime_28; }
	inline int32_t* get_address_of_commandSentTime_28() { return &___commandSentTime_28; }
	inline void set_commandSentTime_28(int32_t value)
	{
		___commandSentTime_28 = value;
	}

	inline static int32_t get_offset_of_commandSentCount_29() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandSentCount_29)); }
	inline uint8_t get_commandSentCount_29() const { return ___commandSentCount_29; }
	inline uint8_t* get_address_of_commandSentCount_29() { return &___commandSentCount_29; }
	inline void set_commandSentCount_29(uint8_t value)
	{
		___commandSentCount_29 = value;
	}

	inline static int32_t get_offset_of_roundTripTimeout_30() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___roundTripTimeout_30)); }
	inline int32_t get_roundTripTimeout_30() const { return ___roundTripTimeout_30; }
	inline int32_t* get_address_of_roundTripTimeout_30() { return &___roundTripTimeout_30; }
	inline void set_roundTripTimeout_30(int32_t value)
	{
		___roundTripTimeout_30 = value;
	}

	inline static int32_t get_offset_of_timeoutTime_31() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___timeoutTime_31)); }
	inline int32_t get_timeoutTime_31() const { return ___timeoutTime_31; }
	inline int32_t* get_address_of_timeoutTime_31() { return &___timeoutTime_31; }
	inline void set_timeoutTime_31(int32_t value)
	{
		___timeoutTime_31 = value;
	}

	inline static int32_t get_offset_of_ackReceivedReliableSequenceNumber_32() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___ackReceivedReliableSequenceNumber_32)); }
	inline int32_t get_ackReceivedReliableSequenceNumber_32() const { return ___ackReceivedReliableSequenceNumber_32; }
	inline int32_t* get_address_of_ackReceivedReliableSequenceNumber_32() { return &___ackReceivedReliableSequenceNumber_32; }
	inline void set_ackReceivedReliableSequenceNumber_32(int32_t value)
	{
		___ackReceivedReliableSequenceNumber_32 = value;
	}

	inline static int32_t get_offset_of_ackReceivedSentTime_33() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___ackReceivedSentTime_33)); }
	inline int32_t get_ackReceivedSentTime_33() const { return ___ackReceivedSentTime_33; }
	inline int32_t* get_address_of_ackReceivedSentTime_33() { return &___ackReceivedSentTime_33; }
	inline void set_ackReceivedSentTime_33(int32_t value)
	{
		___ackReceivedSentTime_33 = value;
	}

	inline static int32_t get_offset_of_Size_45() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___Size_45)); }
	inline int32_t get_Size_45() const { return ___Size_45; }
	inline int32_t* get_address_of_Size_45() { return &___Size_45; }
	inline void set_Size_45(int32_t value)
	{
		___Size_45 = value;
	}

	inline static int32_t get_offset_of_commandHeader_46() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___commandHeader_46)); }
	inline ByteU5BU5D_t4116647657* get_commandHeader_46() const { return ___commandHeader_46; }
	inline ByteU5BU5D_t4116647657** get_address_of_commandHeader_46() { return &___commandHeader_46; }
	inline void set_commandHeader_46(ByteU5BU5D_t4116647657* value)
	{
		___commandHeader_46 = value;
		Il2CppCodeGenWriteBarrier((&___commandHeader_46), value);
	}

	inline static int32_t get_offset_of_SizeOfHeader_47() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___SizeOfHeader_47)); }
	inline int32_t get_SizeOfHeader_47() const { return ___SizeOfHeader_47; }
	inline int32_t* get_address_of_SizeOfHeader_47() { return &___SizeOfHeader_47; }
	inline void set_SizeOfHeader_47(int32_t value)
	{
		___SizeOfHeader_47 = value;
	}

	inline static int32_t get_offset_of_Payload_48() { return static_cast<int32_t>(offsetof(NCommand_t1230688399, ___Payload_48)); }
	inline ByteU5BU5D_t4116647657* get_Payload_48() const { return ___Payload_48; }
	inline ByteU5BU5D_t4116647657** get_address_of_Payload_48() { return &___Payload_48; }
	inline void set_Payload_48(ByteU5BU5D_t4116647657* value)
	{
		___Payload_48 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NCOMMAND_T1230688399_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T3594181758_H
#define U3CU3EC__DISPLAYCLASS41_0_T3594181758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t3594181758  : public RuntimeObject
{
public:
	// System.Byte[] ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_0::inBufferCopy
	ByteU5BU5D_t4116647657* ___inBufferCopy_0;
	// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_1 ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_0::CS$<>8__locals1
	U3CU3Ec__DisplayClass41_1_t2028097817 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_inBufferCopy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t3594181758, ___inBufferCopy_0)); }
	inline ByteU5BU5D_t4116647657* get_inBufferCopy_0() const { return ___inBufferCopy_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_inBufferCopy_0() { return &___inBufferCopy_0; }
	inline void set_inBufferCopy_0(ByteU5BU5D_t4116647657* value)
	{
		___inBufferCopy_0 = value;
		Il2CppCodeGenWriteBarrier((&___inBufferCopy_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t3594181758, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass41_1_t2028097817 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass41_1_t2028097817 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass41_1_t2028097817 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T3594181758_H
#ifndef THREADSAFERANDOM_T1204416265_H
#define THREADSAFERANDOM_T1204416265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/ThreadSafeRandom
struct  ThreadSafeRandom_t1204416265  : public RuntimeObject
{
public:

public:
};

struct ThreadSafeRandom_t1204416265_StaticFields
{
public:
	// System.Random ExitGames.Client.Photon.SupportClass/ThreadSafeRandom::_r
	Random_t108471755 * ____r_0;

public:
	inline static int32_t get_offset_of__r_0() { return static_cast<int32_t>(offsetof(ThreadSafeRandom_t1204416265_StaticFields, ____r_0)); }
	inline Random_t108471755 * get__r_0() const { return ____r_0; }
	inline Random_t108471755 ** get_address_of__r_0() { return &____r_0; }
	inline void set__r_0(Random_t108471755 * value)
	{
		____r_0 = value;
		Il2CppCodeGenWriteBarrier((&____r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSAFERANDOM_T1204416265_H
#ifndef OPERATIONRESPONSE_T423627973_H
#define OPERATIONRESPONSE_T423627973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.OperationResponse
struct  OperationResponse_t423627973  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.OperationResponse::OperationCode
	uint8_t ___OperationCode_0;
	// System.Int16 ExitGames.Client.Photon.OperationResponse::ReturnCode
	int16_t ___ReturnCode_1;
	// System.String ExitGames.Client.Photon.OperationResponse::DebugMessage
	String_t* ___DebugMessage_2;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.OperationResponse::Parameters
	Dictionary_2_t1405253484 * ___Parameters_3;

public:
	inline static int32_t get_offset_of_OperationCode_0() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___OperationCode_0)); }
	inline uint8_t get_OperationCode_0() const { return ___OperationCode_0; }
	inline uint8_t* get_address_of_OperationCode_0() { return &___OperationCode_0; }
	inline void set_OperationCode_0(uint8_t value)
	{
		___OperationCode_0 = value;
	}

	inline static int32_t get_offset_of_ReturnCode_1() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___ReturnCode_1)); }
	inline int16_t get_ReturnCode_1() const { return ___ReturnCode_1; }
	inline int16_t* get_address_of_ReturnCode_1() { return &___ReturnCode_1; }
	inline void set_ReturnCode_1(int16_t value)
	{
		___ReturnCode_1 = value;
	}

	inline static int32_t get_offset_of_DebugMessage_2() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___DebugMessage_2)); }
	inline String_t* get_DebugMessage_2() const { return ___DebugMessage_2; }
	inline String_t** get_address_of_DebugMessage_2() { return &___DebugMessage_2; }
	inline void set_DebugMessage_2(String_t* value)
	{
		___DebugMessage_2 = value;
		Il2CppCodeGenWriteBarrier((&___DebugMessage_2), value);
	}

	inline static int32_t get_offset_of_Parameters_3() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___Parameters_3)); }
	inline Dictionary_2_t1405253484 * get_Parameters_3() const { return ___Parameters_3; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_3() { return &___Parameters_3; }
	inline void set_Parameters_3(Dictionary_2_t1405253484 * value)
	{
		___Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONRESPONSE_T423627973_H
#ifndef OPERATIONREQUEST_T597637232_H
#define OPERATIONREQUEST_T597637232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.OperationRequest
struct  OperationRequest_t597637232  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.OperationRequest::OperationCode
	uint8_t ___OperationCode_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.OperationRequest::Parameters
	Dictionary_2_t1405253484 * ___Parameters_1;

public:
	inline static int32_t get_offset_of_OperationCode_0() { return static_cast<int32_t>(offsetof(OperationRequest_t597637232, ___OperationCode_0)); }
	inline uint8_t get_OperationCode_0() const { return ___OperationCode_0; }
	inline uint8_t* get_address_of_OperationCode_0() { return &___OperationCode_0; }
	inline void set_OperationCode_0(uint8_t value)
	{
		___OperationCode_0 = value;
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(OperationRequest_t597637232, ___Parameters_1)); }
	inline Dictionary_2_t1405253484 * get_Parameters_1() const { return ___Parameters_1; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(Dictionary_2_t1405253484 * value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONREQUEST_T597637232_H
#ifndef EVENTDATA_T3728223374_H
#define EVENTDATA_T3728223374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EventData
struct  EventData_t3728223374  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.EventData::Code
	uint8_t ___Code_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.EventData::Parameters
	Dictionary_2_t1405253484 * ___Parameters_1;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(EventData_t3728223374, ___Code_0)); }
	inline uint8_t get_Code_0() const { return ___Code_0; }
	inline uint8_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(uint8_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(EventData_t3728223374, ___Parameters_1)); }
	inline Dictionary_2_t1405253484 * get_Parameters_1() const { return ___Parameters_1; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(Dictionary_2_t1405253484 * value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T3728223374_H
#ifndef CUSTOMTYPE_T4026063319_H
#define CUSTOMTYPE_T4026063319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CustomType
struct  CustomType_t4026063319  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.CustomType::Code
	uint8_t ___Code_0;
	// System.Type ExitGames.Client.Photon.CustomType::Type
	Type_t * ___Type_1;
	// ExitGames.Client.Photon.SerializeMethod ExitGames.Client.Photon.CustomType::SerializeFunction
	SerializeMethod_t1264674278 * ___SerializeFunction_2;
	// ExitGames.Client.Photon.DeserializeMethod ExitGames.Client.Photon.CustomType::DeserializeFunction
	DeserializeMethod_t3915517082 * ___DeserializeFunction_3;
	// ExitGames.Client.Photon.SerializeStreamMethod ExitGames.Client.Photon.CustomType::SerializeStreamFunction
	SerializeStreamMethod_t2169445464 * ___SerializeStreamFunction_4;
	// ExitGames.Client.Photon.DeserializeStreamMethod ExitGames.Client.Photon.CustomType::DeserializeStreamFunction
	DeserializeStreamMethod_t3070531629 * ___DeserializeStreamFunction_5;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(CustomType_t4026063319, ___Code_0)); }
	inline uint8_t get_Code_0() const { return ___Code_0; }
	inline uint8_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(uint8_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(CustomType_t4026063319, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}

	inline static int32_t get_offset_of_SerializeFunction_2() { return static_cast<int32_t>(offsetof(CustomType_t4026063319, ___SerializeFunction_2)); }
	inline SerializeMethod_t1264674278 * get_SerializeFunction_2() const { return ___SerializeFunction_2; }
	inline SerializeMethod_t1264674278 ** get_address_of_SerializeFunction_2() { return &___SerializeFunction_2; }
	inline void set_SerializeFunction_2(SerializeMethod_t1264674278 * value)
	{
		___SerializeFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeFunction_2), value);
	}

	inline static int32_t get_offset_of_DeserializeFunction_3() { return static_cast<int32_t>(offsetof(CustomType_t4026063319, ___DeserializeFunction_3)); }
	inline DeserializeMethod_t3915517082 * get_DeserializeFunction_3() const { return ___DeserializeFunction_3; }
	inline DeserializeMethod_t3915517082 ** get_address_of_DeserializeFunction_3() { return &___DeserializeFunction_3; }
	inline void set_DeserializeFunction_3(DeserializeMethod_t3915517082 * value)
	{
		___DeserializeFunction_3 = value;
		Il2CppCodeGenWriteBarrier((&___DeserializeFunction_3), value);
	}

	inline static int32_t get_offset_of_SerializeStreamFunction_4() { return static_cast<int32_t>(offsetof(CustomType_t4026063319, ___SerializeStreamFunction_4)); }
	inline SerializeStreamMethod_t2169445464 * get_SerializeStreamFunction_4() const { return ___SerializeStreamFunction_4; }
	inline SerializeStreamMethod_t2169445464 ** get_address_of_SerializeStreamFunction_4() { return &___SerializeStreamFunction_4; }
	inline void set_SerializeStreamFunction_4(SerializeStreamMethod_t2169445464 * value)
	{
		___SerializeStreamFunction_4 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeStreamFunction_4), value);
	}

	inline static int32_t get_offset_of_DeserializeStreamFunction_5() { return static_cast<int32_t>(offsetof(CustomType_t4026063319, ___DeserializeStreamFunction_5)); }
	inline DeserializeStreamMethod_t3070531629 * get_DeserializeStreamFunction_5() const { return ___DeserializeStreamFunction_5; }
	inline DeserializeStreamMethod_t3070531629 ** get_address_of_DeserializeStreamFunction_5() { return &___DeserializeStreamFunction_5; }
	inline void set_DeserializeStreamFunction_5(DeserializeStreamMethod_t3070531629 * value)
	{
		___DeserializeStreamFunction_5 = value;
		Il2CppCodeGenWriteBarrier((&___DeserializeStreamFunction_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTYPE_T4026063319_H
#ifndef PROTOCOL_T1622296502_H
#define PROTOCOL_T1622296502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Protocol
struct  Protocol_t1622296502  : public RuntimeObject
{
public:

public:
};

struct Protocol_t1622296502_StaticFields
{
public:
	// ExitGames.Client.IProtocol ExitGames.Client.Photon.Protocol::GpBinaryV16
	IProtocol_t3448746462 * ___GpBinaryV16_0;
	// ExitGames.Client.IProtocol ExitGames.Client.Photon.Protocol::ProtocolDefault
	IProtocol_t3448746462 * ___ProtocolDefault_1;
	// System.Collections.Generic.Dictionary`2<System.Type,ExitGames.Client.Photon.CustomType> ExitGames.Client.Photon.Protocol::TypeDict
	Dictionary_2_t2175443087 * ___TypeDict_2;
	// System.Collections.Generic.Dictionary`2<System.Byte,ExitGames.Client.Photon.CustomType> ExitGames.Client.Photon.Protocol::CodeDict
	Dictionary_2_t2351210639 * ___CodeDict_3;
	// System.Single[] ExitGames.Client.Photon.Protocol::memFloatBlock
	SingleU5BU5D_t1444911251* ___memFloatBlock_4;
	// System.Byte[] ExitGames.Client.Photon.Protocol::memDeserialize
	ByteU5BU5D_t4116647657* ___memDeserialize_5;

public:
	inline static int32_t get_offset_of_GpBinaryV16_0() { return static_cast<int32_t>(offsetof(Protocol_t1622296502_StaticFields, ___GpBinaryV16_0)); }
	inline IProtocol_t3448746462 * get_GpBinaryV16_0() const { return ___GpBinaryV16_0; }
	inline IProtocol_t3448746462 ** get_address_of_GpBinaryV16_0() { return &___GpBinaryV16_0; }
	inline void set_GpBinaryV16_0(IProtocol_t3448746462 * value)
	{
		___GpBinaryV16_0 = value;
		Il2CppCodeGenWriteBarrier((&___GpBinaryV16_0), value);
	}

	inline static int32_t get_offset_of_ProtocolDefault_1() { return static_cast<int32_t>(offsetof(Protocol_t1622296502_StaticFields, ___ProtocolDefault_1)); }
	inline IProtocol_t3448746462 * get_ProtocolDefault_1() const { return ___ProtocolDefault_1; }
	inline IProtocol_t3448746462 ** get_address_of_ProtocolDefault_1() { return &___ProtocolDefault_1; }
	inline void set_ProtocolDefault_1(IProtocol_t3448746462 * value)
	{
		___ProtocolDefault_1 = value;
		Il2CppCodeGenWriteBarrier((&___ProtocolDefault_1), value);
	}

	inline static int32_t get_offset_of_TypeDict_2() { return static_cast<int32_t>(offsetof(Protocol_t1622296502_StaticFields, ___TypeDict_2)); }
	inline Dictionary_2_t2175443087 * get_TypeDict_2() const { return ___TypeDict_2; }
	inline Dictionary_2_t2175443087 ** get_address_of_TypeDict_2() { return &___TypeDict_2; }
	inline void set_TypeDict_2(Dictionary_2_t2175443087 * value)
	{
		___TypeDict_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeDict_2), value);
	}

	inline static int32_t get_offset_of_CodeDict_3() { return static_cast<int32_t>(offsetof(Protocol_t1622296502_StaticFields, ___CodeDict_3)); }
	inline Dictionary_2_t2351210639 * get_CodeDict_3() const { return ___CodeDict_3; }
	inline Dictionary_2_t2351210639 ** get_address_of_CodeDict_3() { return &___CodeDict_3; }
	inline void set_CodeDict_3(Dictionary_2_t2351210639 * value)
	{
		___CodeDict_3 = value;
		Il2CppCodeGenWriteBarrier((&___CodeDict_3), value);
	}

	inline static int32_t get_offset_of_memFloatBlock_4() { return static_cast<int32_t>(offsetof(Protocol_t1622296502_StaticFields, ___memFloatBlock_4)); }
	inline SingleU5BU5D_t1444911251* get_memFloatBlock_4() const { return ___memFloatBlock_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_memFloatBlock_4() { return &___memFloatBlock_4; }
	inline void set_memFloatBlock_4(SingleU5BU5D_t1444911251* value)
	{
		___memFloatBlock_4 = value;
		Il2CppCodeGenWriteBarrier((&___memFloatBlock_4), value);
	}

	inline static int32_t get_offset_of_memDeserialize_5() { return static_cast<int32_t>(offsetof(Protocol_t1622296502_StaticFields, ___memDeserialize_5)); }
	inline ByteU5BU5D_t4116647657* get_memDeserialize_5() const { return ___memDeserialize_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_memDeserialize_5() { return &___memDeserialize_5; }
	inline void set_memDeserialize_5(ByteU5BU5D_t4116647657* value)
	{
		___memDeserialize_5 = value;
		Il2CppCodeGenWriteBarrier((&___memDeserialize_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOL_T1622296502_H
#ifndef PHOTONCODES_T543425440_H
#define PHOTONCODES_T543425440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonCodes
struct  PhotonCodes_t543425440  : public RuntimeObject
{
public:

public:
};

struct PhotonCodes_t543425440_StaticFields
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonCodes::ClientKey
	uint8_t ___ClientKey_0;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::ModeKey
	uint8_t ___ModeKey_1;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::ServerKey
	uint8_t ___ServerKey_2;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::InitEncryption
	uint8_t ___InitEncryption_3;
	// System.Byte ExitGames.Client.Photon.PhotonCodes::Ping
	uint8_t ___Ping_4;

public:
	inline static int32_t get_offset_of_ClientKey_0() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___ClientKey_0)); }
	inline uint8_t get_ClientKey_0() const { return ___ClientKey_0; }
	inline uint8_t* get_address_of_ClientKey_0() { return &___ClientKey_0; }
	inline void set_ClientKey_0(uint8_t value)
	{
		___ClientKey_0 = value;
	}

	inline static int32_t get_offset_of_ModeKey_1() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___ModeKey_1)); }
	inline uint8_t get_ModeKey_1() const { return ___ModeKey_1; }
	inline uint8_t* get_address_of_ModeKey_1() { return &___ModeKey_1; }
	inline void set_ModeKey_1(uint8_t value)
	{
		___ModeKey_1 = value;
	}

	inline static int32_t get_offset_of_ServerKey_2() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___ServerKey_2)); }
	inline uint8_t get_ServerKey_2() const { return ___ServerKey_2; }
	inline uint8_t* get_address_of_ServerKey_2() { return &___ServerKey_2; }
	inline void set_ServerKey_2(uint8_t value)
	{
		___ServerKey_2 = value;
	}

	inline static int32_t get_offset_of_InitEncryption_3() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___InitEncryption_3)); }
	inline uint8_t get_InitEncryption_3() const { return ___InitEncryption_3; }
	inline uint8_t* get_address_of_InitEncryption_3() { return &___InitEncryption_3; }
	inline void set_InitEncryption_3(uint8_t value)
	{
		___InitEncryption_3 = value;
	}

	inline static int32_t get_offset_of_Ping_4() { return static_cast<int32_t>(offsetof(PhotonCodes_t543425440_StaticFields, ___Ping_4)); }
	inline uint8_t get_Ping_4() const { return ___Ping_4; }
	inline uint8_t* get_address_of_Ping_4() { return &___Ping_4; }
	inline void set_Ping_4(uint8_t value)
	{
		___Ping_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONCODES_T543425440_H
#ifndef PHOTONPING_T2371975946_H
#define PHOTONPING_T2371975946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPing
struct  PhotonPing_t2371975946  : public RuntimeObject
{
public:
	// System.String ExitGames.Client.Photon.PhotonPing::DebugString
	String_t* ___DebugString_0;
	// System.Boolean ExitGames.Client.Photon.PhotonPing::Successful
	bool ___Successful_1;
	// System.Boolean ExitGames.Client.Photon.PhotonPing::GotResult
	bool ___GotResult_2;
	// System.Int32 ExitGames.Client.Photon.PhotonPing::PingLength
	int32_t ___PingLength_3;
	// System.Byte[] ExitGames.Client.Photon.PhotonPing::PingBytes
	ByteU5BU5D_t4116647657* ___PingBytes_4;
	// System.Byte ExitGames.Client.Photon.PhotonPing::PingId
	uint8_t ___PingId_5;

public:
	inline static int32_t get_offset_of_DebugString_0() { return static_cast<int32_t>(offsetof(PhotonPing_t2371975946, ___DebugString_0)); }
	inline String_t* get_DebugString_0() const { return ___DebugString_0; }
	inline String_t** get_address_of_DebugString_0() { return &___DebugString_0; }
	inline void set_DebugString_0(String_t* value)
	{
		___DebugString_0 = value;
		Il2CppCodeGenWriteBarrier((&___DebugString_0), value);
	}

	inline static int32_t get_offset_of_Successful_1() { return static_cast<int32_t>(offsetof(PhotonPing_t2371975946, ___Successful_1)); }
	inline bool get_Successful_1() const { return ___Successful_1; }
	inline bool* get_address_of_Successful_1() { return &___Successful_1; }
	inline void set_Successful_1(bool value)
	{
		___Successful_1 = value;
	}

	inline static int32_t get_offset_of_GotResult_2() { return static_cast<int32_t>(offsetof(PhotonPing_t2371975946, ___GotResult_2)); }
	inline bool get_GotResult_2() const { return ___GotResult_2; }
	inline bool* get_address_of_GotResult_2() { return &___GotResult_2; }
	inline void set_GotResult_2(bool value)
	{
		___GotResult_2 = value;
	}

	inline static int32_t get_offset_of_PingLength_3() { return static_cast<int32_t>(offsetof(PhotonPing_t2371975946, ___PingLength_3)); }
	inline int32_t get_PingLength_3() const { return ___PingLength_3; }
	inline int32_t* get_address_of_PingLength_3() { return &___PingLength_3; }
	inline void set_PingLength_3(int32_t value)
	{
		___PingLength_3 = value;
	}

	inline static int32_t get_offset_of_PingBytes_4() { return static_cast<int32_t>(offsetof(PhotonPing_t2371975946, ___PingBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_PingBytes_4() const { return ___PingBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_PingBytes_4() { return &___PingBytes_4; }
	inline void set_PingBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___PingBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___PingBytes_4), value);
	}

	inline static int32_t get_offset_of_PingId_5() { return static_cast<int32_t>(offsetof(PhotonPing_t2371975946, ___PingId_5)); }
	inline uint8_t get_PingId_5() const { return ___PingId_5; }
	inline uint8_t* get_address_of_PingId_5() { return &___PingId_5; }
	inline void set_PingId_5(uint8_t value)
	{
		___PingId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPING_T2371975946_H
#ifndef U3CU3EC__DISPLAYCLASS56_0_T3313269813_H
#define U3CU3EC__DISPLAYCLASS56_0_T3313269813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_0
struct  U3CU3Ec__DisplayClass56_0_t3313269813  : public RuntimeObject
{
public:
	// System.Byte[] ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_0::dataCopy
	ByteU5BU5D_t4116647657* ___dataCopy_0;
	// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1 ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_0::CS$<>8__locals1
	U3CU3Ec__DisplayClass56_1_t3313335349 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_dataCopy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t3313269813, ___dataCopy_0)); }
	inline ByteU5BU5D_t4116647657* get_dataCopy_0() const { return ___dataCopy_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_dataCopy_0() { return &___dataCopy_0; }
	inline void set_dataCopy_0(ByteU5BU5D_t4116647657* value)
	{
		___dataCopy_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataCopy_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t3313269813, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass56_1_t3313335349 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass56_1_t3313335349 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass56_1_t3313335349 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_0_T3313269813_H
#ifndef U3CU3EC_T356392828_H
#define U3CU3EC_T356392828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/<>c
struct  U3CU3Ec_t356392828  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t356392828_StaticFields
{
public:
	// ExitGames.Client.Photon.SupportClass/<>c ExitGames.Client.Photon.SupportClass/<>c::<>9
	U3CU3Ec_t356392828 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t356392828_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t356392828 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t356392828 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t356392828 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T356392828_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T926758450_H
#define U3CU3EC__DISPLAYCLASS7_0_T926758450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t926758450  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.SupportClass/<>c__DisplayClass7_0::millisecondsInterval
	int32_t ___millisecondsInterval_0;
	// System.Func`1<System.Boolean> ExitGames.Client.Photon.SupportClass/<>c__DisplayClass7_0::myThread
	Func_1_t3822001908 * ___myThread_1;

public:
	inline static int32_t get_offset_of_millisecondsInterval_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t926758450, ___millisecondsInterval_0)); }
	inline int32_t get_millisecondsInterval_0() const { return ___millisecondsInterval_0; }
	inline int32_t* get_address_of_millisecondsInterval_0() { return &___millisecondsInterval_0; }
	inline void set_millisecondsInterval_0(int32_t value)
	{
		___millisecondsInterval_0 = value;
	}

	inline static int32_t get_offset_of_myThread_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t926758450, ___myThread_1)); }
	inline Func_1_t3822001908 * get_myThread_1() const { return ___myThread_1; }
	inline Func_1_t3822001908 ** get_address_of_myThread_1() { return &___myThread_1; }
	inline void set_myThread_1(Func_1_t3822001908 * value)
	{
		___myThread_1 = value;
		Il2CppCodeGenWriteBarrier((&___myThread_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T926758450_H
#ifndef U3CU3EC__DISPLAYCLASS56_1_T3313335349_H
#define U3CU3EC__DISPLAYCLASS56_1_T3313335349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1
struct  U3CU3Ec__DisplayClass56_1_t3313335349  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1::length
	int32_t ___length_0;
	// ExitGames.Client.Photon.EnetPeer ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass56_1::<>4__this
	EnetPeer_t430442630 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_1_t3313335349, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_1_t3313335349, ___U3CU3E4__this_1)); }
	inline EnetPeer_t430442630 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline EnetPeer_t430442630 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(EnetPeer_t430442630 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_1_T3313335349_H
#ifndef U3CU3EC__DISPLAYCLASS41_1_T2028097817_H
#define U3CU3EC__DISPLAYCLASS41_1_T2028097817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_1
struct  U3CU3Ec__DisplayClass41_1_t2028097817  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_1::length
	int32_t ___length_0;
	// System.Byte[] ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_1::inBuffer
	ByteU5BU5D_t4116647657* ___inBuffer_1;
	// ExitGames.Client.Photon.IPhotonSocket ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass41_1::<>4__this
	IPhotonSocket_t2066969247 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_1_t2028097817, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_inBuffer_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_1_t2028097817, ___inBuffer_1)); }
	inline ByteU5BU5D_t4116647657* get_inBuffer_1() const { return ___inBuffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_inBuffer_1() { return &___inBuffer_1; }
	inline void set_inBuffer_1(ByteU5BU5D_t4116647657* value)
	{
		___inBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___inBuffer_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_1_t2028097817, ___U3CU3E4__this_2)); }
	inline IPhotonSocket_t2066969247 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline IPhotonSocket_t2066969247 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(IPhotonSocket_t2066969247 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_1_T2028097817_H
#ifndef REMOTESETTINGS_T1718627291_H
#define REMOTESETTINGS_T1718627291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t1718627291  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t1718627291_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t1027848393 * ___Updated_0;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t1027848393 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t1027848393 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t1027848393 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T1718627291_H
#ifndef DICTIONARY_2_T132545152_H
#define DICTIONARY_2_T132545152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct  Dictionary_2_t132545152  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t2843939325* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___keySlots_6)); }
	inline ObjectU5BU5D_t2843939325* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t2843939325* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t132545152_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4209139644 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4209139644 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4209139644 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4209139644 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T132545152_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef ANALYTICSSESSIONINFO_T2322308579_H
#define ANALYTICSSESSIONINFO_T2322308579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_t2322308579  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_t2322308579_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t3163629820 * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_t2322308579_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t3163629820 * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t3163629820 ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t3163629820 * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_T2322308579_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SIMULATIONITEM_T3044638479_H
#define SIMULATIONITEM_T3044638479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SimulationItem
struct  SimulationItem_t3044638479  : public RuntimeObject
{
public:
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.SimulationItem::stopw
	Stopwatch_t305734070 * ___stopw_0;
	// System.Int32 ExitGames.Client.Photon.SimulationItem::TimeToExecute
	int32_t ___TimeToExecute_1;
	// ExitGames.Client.Photon.PeerBase/MyAction ExitGames.Client.Photon.SimulationItem::ActionToExecute
	MyAction_t2462891903 * ___ActionToExecute_2;
	// System.Int32 ExitGames.Client.Photon.SimulationItem::<Delay>k__BackingField
	int32_t ___U3CDelayU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_stopw_0() { return static_cast<int32_t>(offsetof(SimulationItem_t3044638479, ___stopw_0)); }
	inline Stopwatch_t305734070 * get_stopw_0() const { return ___stopw_0; }
	inline Stopwatch_t305734070 ** get_address_of_stopw_0() { return &___stopw_0; }
	inline void set_stopw_0(Stopwatch_t305734070 * value)
	{
		___stopw_0 = value;
		Il2CppCodeGenWriteBarrier((&___stopw_0), value);
	}

	inline static int32_t get_offset_of_TimeToExecute_1() { return static_cast<int32_t>(offsetof(SimulationItem_t3044638479, ___TimeToExecute_1)); }
	inline int32_t get_TimeToExecute_1() const { return ___TimeToExecute_1; }
	inline int32_t* get_address_of_TimeToExecute_1() { return &___TimeToExecute_1; }
	inline void set_TimeToExecute_1(int32_t value)
	{
		___TimeToExecute_1 = value;
	}

	inline static int32_t get_offset_of_ActionToExecute_2() { return static_cast<int32_t>(offsetof(SimulationItem_t3044638479, ___ActionToExecute_2)); }
	inline MyAction_t2462891903 * get_ActionToExecute_2() const { return ___ActionToExecute_2; }
	inline MyAction_t2462891903 ** get_address_of_ActionToExecute_2() { return &___ActionToExecute_2; }
	inline void set_ActionToExecute_2(MyAction_t2462891903 * value)
	{
		___ActionToExecute_2 = value;
		Il2CppCodeGenWriteBarrier((&___ActionToExecute_2), value);
	}

	inline static int32_t get_offset_of_U3CDelayU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SimulationItem_t3044638479, ___U3CDelayU3Ek__BackingField_3)); }
	inline int32_t get_U3CDelayU3Ek__BackingField_3() const { return ___U3CDelayU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDelayU3Ek__BackingField_3() { return &___U3CDelayU3Ek__BackingField_3; }
	inline void set_U3CDelayU3Ek__BackingField_3(int32_t value)
	{
		___U3CDelayU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATIONITEM_T3044638479_H
#ifndef NETWORKSIMULATIONSET_T2000596048_H
#define NETWORKSIMULATIONSET_T2000596048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.NetworkSimulationSet
struct  NetworkSimulationSet_t2000596048  : public RuntimeObject
{
public:
	// System.Boolean ExitGames.Client.Photon.NetworkSimulationSet::isSimulationEnabled
	bool ___isSimulationEnabled_0;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::outgoingLag
	int32_t ___outgoingLag_1;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::outgoingJitter
	int32_t ___outgoingJitter_2;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::outgoingLossPercentage
	int32_t ___outgoingLossPercentage_3;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::incomingLag
	int32_t ___incomingLag_4;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::incomingJitter
	int32_t ___incomingJitter_5;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::incomingLossPercentage
	int32_t ___incomingLossPercentage_6;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.NetworkSimulationSet::peerBase
	PeerBase_t2956237011 * ___peerBase_7;
	// System.Threading.Thread ExitGames.Client.Photon.NetworkSimulationSet::netSimThread
	Thread_t2300836069 * ___netSimThread_8;
	// System.Threading.ManualResetEvent ExitGames.Client.Photon.NetworkSimulationSet::NetSimManualResetEvent
	ManualResetEvent_t451242010 * ___NetSimManualResetEvent_9;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::<LostPackagesOut>k__BackingField
	int32_t ___U3CLostPackagesOutU3Ek__BackingField_10;
	// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::<LostPackagesIn>k__BackingField
	int32_t ___U3CLostPackagesInU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_isSimulationEnabled_0() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___isSimulationEnabled_0)); }
	inline bool get_isSimulationEnabled_0() const { return ___isSimulationEnabled_0; }
	inline bool* get_address_of_isSimulationEnabled_0() { return &___isSimulationEnabled_0; }
	inline void set_isSimulationEnabled_0(bool value)
	{
		___isSimulationEnabled_0 = value;
	}

	inline static int32_t get_offset_of_outgoingLag_1() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___outgoingLag_1)); }
	inline int32_t get_outgoingLag_1() const { return ___outgoingLag_1; }
	inline int32_t* get_address_of_outgoingLag_1() { return &___outgoingLag_1; }
	inline void set_outgoingLag_1(int32_t value)
	{
		___outgoingLag_1 = value;
	}

	inline static int32_t get_offset_of_outgoingJitter_2() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___outgoingJitter_2)); }
	inline int32_t get_outgoingJitter_2() const { return ___outgoingJitter_2; }
	inline int32_t* get_address_of_outgoingJitter_2() { return &___outgoingJitter_2; }
	inline void set_outgoingJitter_2(int32_t value)
	{
		___outgoingJitter_2 = value;
	}

	inline static int32_t get_offset_of_outgoingLossPercentage_3() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___outgoingLossPercentage_3)); }
	inline int32_t get_outgoingLossPercentage_3() const { return ___outgoingLossPercentage_3; }
	inline int32_t* get_address_of_outgoingLossPercentage_3() { return &___outgoingLossPercentage_3; }
	inline void set_outgoingLossPercentage_3(int32_t value)
	{
		___outgoingLossPercentage_3 = value;
	}

	inline static int32_t get_offset_of_incomingLag_4() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___incomingLag_4)); }
	inline int32_t get_incomingLag_4() const { return ___incomingLag_4; }
	inline int32_t* get_address_of_incomingLag_4() { return &___incomingLag_4; }
	inline void set_incomingLag_4(int32_t value)
	{
		___incomingLag_4 = value;
	}

	inline static int32_t get_offset_of_incomingJitter_5() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___incomingJitter_5)); }
	inline int32_t get_incomingJitter_5() const { return ___incomingJitter_5; }
	inline int32_t* get_address_of_incomingJitter_5() { return &___incomingJitter_5; }
	inline void set_incomingJitter_5(int32_t value)
	{
		___incomingJitter_5 = value;
	}

	inline static int32_t get_offset_of_incomingLossPercentage_6() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___incomingLossPercentage_6)); }
	inline int32_t get_incomingLossPercentage_6() const { return ___incomingLossPercentage_6; }
	inline int32_t* get_address_of_incomingLossPercentage_6() { return &___incomingLossPercentage_6; }
	inline void set_incomingLossPercentage_6(int32_t value)
	{
		___incomingLossPercentage_6 = value;
	}

	inline static int32_t get_offset_of_peerBase_7() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___peerBase_7)); }
	inline PeerBase_t2956237011 * get_peerBase_7() const { return ___peerBase_7; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_7() { return &___peerBase_7; }
	inline void set_peerBase_7(PeerBase_t2956237011 * value)
	{
		___peerBase_7 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_7), value);
	}

	inline static int32_t get_offset_of_netSimThread_8() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___netSimThread_8)); }
	inline Thread_t2300836069 * get_netSimThread_8() const { return ___netSimThread_8; }
	inline Thread_t2300836069 ** get_address_of_netSimThread_8() { return &___netSimThread_8; }
	inline void set_netSimThread_8(Thread_t2300836069 * value)
	{
		___netSimThread_8 = value;
		Il2CppCodeGenWriteBarrier((&___netSimThread_8), value);
	}

	inline static int32_t get_offset_of_NetSimManualResetEvent_9() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___NetSimManualResetEvent_9)); }
	inline ManualResetEvent_t451242010 * get_NetSimManualResetEvent_9() const { return ___NetSimManualResetEvent_9; }
	inline ManualResetEvent_t451242010 ** get_address_of_NetSimManualResetEvent_9() { return &___NetSimManualResetEvent_9; }
	inline void set_NetSimManualResetEvent_9(ManualResetEvent_t451242010 * value)
	{
		___NetSimManualResetEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___NetSimManualResetEvent_9), value);
	}

	inline static int32_t get_offset_of_U3CLostPackagesOutU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___U3CLostPackagesOutU3Ek__BackingField_10)); }
	inline int32_t get_U3CLostPackagesOutU3Ek__BackingField_10() const { return ___U3CLostPackagesOutU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CLostPackagesOutU3Ek__BackingField_10() { return &___U3CLostPackagesOutU3Ek__BackingField_10; }
	inline void set_U3CLostPackagesOutU3Ek__BackingField_10(int32_t value)
	{
		___U3CLostPackagesOutU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CLostPackagesInU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(NetworkSimulationSet_t2000596048, ___U3CLostPackagesInU3Ek__BackingField_11)); }
	inline int32_t get_U3CLostPackagesInU3Ek__BackingField_11() const { return ___U3CLostPackagesInU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CLostPackagesInU3Ek__BackingField_11() { return &___U3CLostPackagesInU3Ek__BackingField_11; }
	inline void set_U3CLostPackagesInU3Ek__BackingField_11(int32_t value)
	{
		___U3CLostPackagesInU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSIMULATIONSET_T2000596048_H
#ifndef TRAFFICSTATSGAMELEVEL_T4013908777_H
#define TRAFFICSTATSGAMELEVEL_T4013908777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.TrafficStatsGameLevel
struct  TrafficStatsGameLevel_t4013908777  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::timeOfLastDispatchCall
	int32_t ___timeOfLastDispatchCall_0;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::timeOfLastSendCall
	int32_t ___timeOfLastSendCall_1;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<OperationByteCount>k__BackingField
	int32_t ___U3COperationByteCountU3Ek__BackingField_2;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<OperationCount>k__BackingField
	int32_t ___U3COperationCountU3Ek__BackingField_3;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<ResultByteCount>k__BackingField
	int32_t ___U3CResultByteCountU3Ek__BackingField_4;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<ResultCount>k__BackingField
	int32_t ___U3CResultCountU3Ek__BackingField_5;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<EventByteCount>k__BackingField
	int32_t ___U3CEventByteCountU3Ek__BackingField_6;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<EventCount>k__BackingField
	int32_t ___U3CEventCountU3Ek__BackingField_7;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<LongestOpResponseCallback>k__BackingField
	int32_t ___U3CLongestOpResponseCallbackU3Ek__BackingField_8;
	// System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::<LongestOpResponseCallbackOpCode>k__BackingField
	uint8_t ___U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<LongestEventCallback>k__BackingField
	int32_t ___U3CLongestEventCallbackU3Ek__BackingField_10;
	// System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::<LongestEventCallbackCode>k__BackingField
	uint8_t ___U3CLongestEventCallbackCodeU3Ek__BackingField_11;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<LongestDeltaBetweenDispatching>k__BackingField
	int32_t ___U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<LongestDeltaBetweenSending>k__BackingField
	int32_t ___U3CLongestDeltaBetweenSendingU3Ek__BackingField_13;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<DispatchIncomingCommandsCalls>k__BackingField
	int32_t ___U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14;
	// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::<SendOutgoingCommandsCalls>k__BackingField
	int32_t ___U3CSendOutgoingCommandsCallsU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_timeOfLastDispatchCall_0() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___timeOfLastDispatchCall_0)); }
	inline int32_t get_timeOfLastDispatchCall_0() const { return ___timeOfLastDispatchCall_0; }
	inline int32_t* get_address_of_timeOfLastDispatchCall_0() { return &___timeOfLastDispatchCall_0; }
	inline void set_timeOfLastDispatchCall_0(int32_t value)
	{
		___timeOfLastDispatchCall_0 = value;
	}

	inline static int32_t get_offset_of_timeOfLastSendCall_1() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___timeOfLastSendCall_1)); }
	inline int32_t get_timeOfLastSendCall_1() const { return ___timeOfLastSendCall_1; }
	inline int32_t* get_address_of_timeOfLastSendCall_1() { return &___timeOfLastSendCall_1; }
	inline void set_timeOfLastSendCall_1(int32_t value)
	{
		___timeOfLastSendCall_1 = value;
	}

	inline static int32_t get_offset_of_U3COperationByteCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3COperationByteCountU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationByteCountU3Ek__BackingField_2() const { return ___U3COperationByteCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationByteCountU3Ek__BackingField_2() { return &___U3COperationByteCountU3Ek__BackingField_2; }
	inline void set_U3COperationByteCountU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationByteCountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3COperationCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3COperationCountU3Ek__BackingField_3)); }
	inline int32_t get_U3COperationCountU3Ek__BackingField_3() const { return ___U3COperationCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3COperationCountU3Ek__BackingField_3() { return &___U3COperationCountU3Ek__BackingField_3; }
	inline void set_U3COperationCountU3Ek__BackingField_3(int32_t value)
	{
		___U3COperationCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CResultByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CResultByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CResultByteCountU3Ek__BackingField_4() const { return ___U3CResultByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CResultByteCountU3Ek__BackingField_4() { return &___U3CResultByteCountU3Ek__BackingField_4; }
	inline void set_U3CResultByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CResultByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CResultCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CResultCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CResultCountU3Ek__BackingField_5() const { return ___U3CResultCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CResultCountU3Ek__BackingField_5() { return &___U3CResultCountU3Ek__BackingField_5; }
	inline void set_U3CResultCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CResultCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CEventByteCountU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CEventByteCountU3Ek__BackingField_6)); }
	inline int32_t get_U3CEventByteCountU3Ek__BackingField_6() const { return ___U3CEventByteCountU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CEventByteCountU3Ek__BackingField_6() { return &___U3CEventByteCountU3Ek__BackingField_6; }
	inline void set_U3CEventByteCountU3Ek__BackingField_6(int32_t value)
	{
		___U3CEventByteCountU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CEventCountU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CEventCountU3Ek__BackingField_7)); }
	inline int32_t get_U3CEventCountU3Ek__BackingField_7() const { return ___U3CEventCountU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CEventCountU3Ek__BackingField_7() { return &___U3CEventCountU3Ek__BackingField_7; }
	inline void set_U3CEventCountU3Ek__BackingField_7(int32_t value)
	{
		___U3CEventCountU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CLongestOpResponseCallbackU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CLongestOpResponseCallbackU3Ek__BackingField_8)); }
	inline int32_t get_U3CLongestOpResponseCallbackU3Ek__BackingField_8() const { return ___U3CLongestOpResponseCallbackU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CLongestOpResponseCallbackU3Ek__BackingField_8() { return &___U3CLongestOpResponseCallbackU3Ek__BackingField_8; }
	inline void set_U3CLongestOpResponseCallbackU3Ek__BackingField_8(int32_t value)
	{
		___U3CLongestOpResponseCallbackU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9)); }
	inline uint8_t get_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9() const { return ___U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9; }
	inline uint8_t* get_address_of_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9() { return &___U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9; }
	inline void set_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9(uint8_t value)
	{
		___U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLongestEventCallbackU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CLongestEventCallbackU3Ek__BackingField_10)); }
	inline int32_t get_U3CLongestEventCallbackU3Ek__BackingField_10() const { return ___U3CLongestEventCallbackU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CLongestEventCallbackU3Ek__BackingField_10() { return &___U3CLongestEventCallbackU3Ek__BackingField_10; }
	inline void set_U3CLongestEventCallbackU3Ek__BackingField_10(int32_t value)
	{
		___U3CLongestEventCallbackU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CLongestEventCallbackCodeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CLongestEventCallbackCodeU3Ek__BackingField_11)); }
	inline uint8_t get_U3CLongestEventCallbackCodeU3Ek__BackingField_11() const { return ___U3CLongestEventCallbackCodeU3Ek__BackingField_11; }
	inline uint8_t* get_address_of_U3CLongestEventCallbackCodeU3Ek__BackingField_11() { return &___U3CLongestEventCallbackCodeU3Ek__BackingField_11; }
	inline void set_U3CLongestEventCallbackCodeU3Ek__BackingField_11(uint8_t value)
	{
		___U3CLongestEventCallbackCodeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12)); }
	inline int32_t get_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12() const { return ___U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12() { return &___U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12; }
	inline void set_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12(int32_t value)
	{
		___U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CLongestDeltaBetweenSendingU3Ek__BackingField_13)); }
	inline int32_t get_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13() const { return ___U3CLongestDeltaBetweenSendingU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13() { return &___U3CLongestDeltaBetweenSendingU3Ek__BackingField_13; }
	inline void set_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13(int32_t value)
	{
		___U3CLongestDeltaBetweenSendingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14)); }
	inline int32_t get_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14() const { return ___U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14() { return &___U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14; }
	inline void set_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14(int32_t value)
	{
		___U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(TrafficStatsGameLevel_t4013908777, ___U3CSendOutgoingCommandsCallsU3Ek__BackingField_15)); }
	inline int32_t get_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15() const { return ___U3CSendOutgoingCommandsCallsU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15() { return &___U3CSendOutgoingCommandsCallsU3Ek__BackingField_15; }
	inline void set_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15(int32_t value)
	{
		___U3CSendOutgoingCommandsCallsU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAFFICSTATSGAMELEVEL_T4013908777_H
#ifndef TRAFFICSTATS_T1302902347_H
#define TRAFFICSTATS_T1302902347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.TrafficStats
struct  TrafficStats_t1302902347  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<PackageHeaderSize>k__BackingField
	int32_t ___U3CPackageHeaderSizeU3Ek__BackingField_0;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<ReliableCommandCount>k__BackingField
	int32_t ___U3CReliableCommandCountU3Ek__BackingField_1;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<UnreliableCommandCount>k__BackingField
	int32_t ___U3CUnreliableCommandCountU3Ek__BackingField_2;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<FragmentCommandCount>k__BackingField
	int32_t ___U3CFragmentCommandCountU3Ek__BackingField_3;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<ControlCommandCount>k__BackingField
	int32_t ___U3CControlCommandCountU3Ek__BackingField_4;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<TotalPacketCount>k__BackingField
	int32_t ___U3CTotalPacketCountU3Ek__BackingField_5;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<TotalCommandsInPackets>k__BackingField
	int32_t ___U3CTotalCommandsInPacketsU3Ek__BackingField_6;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<ReliableCommandBytes>k__BackingField
	int32_t ___U3CReliableCommandBytesU3Ek__BackingField_7;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<UnreliableCommandBytes>k__BackingField
	int32_t ___U3CUnreliableCommandBytesU3Ek__BackingField_8;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<FragmentCommandBytes>k__BackingField
	int32_t ___U3CFragmentCommandBytesU3Ek__BackingField_9;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<ControlCommandBytes>k__BackingField
	int32_t ___U3CControlCommandBytesU3Ek__BackingField_10;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<TimestampOfLastAck>k__BackingField
	int32_t ___U3CTimestampOfLastAckU3Ek__BackingField_11;
	// System.Int32 ExitGames.Client.Photon.TrafficStats::<TimestampOfLastReliableCommand>k__BackingField
	int32_t ___U3CTimestampOfLastReliableCommandU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CPackageHeaderSizeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CPackageHeaderSizeU3Ek__BackingField_0)); }
	inline int32_t get_U3CPackageHeaderSizeU3Ek__BackingField_0() const { return ___U3CPackageHeaderSizeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPackageHeaderSizeU3Ek__BackingField_0() { return &___U3CPackageHeaderSizeU3Ek__BackingField_0; }
	inline void set_U3CPackageHeaderSizeU3Ek__BackingField_0(int32_t value)
	{
		___U3CPackageHeaderSizeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReliableCommandCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CReliableCommandCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CReliableCommandCountU3Ek__BackingField_1() const { return ___U3CReliableCommandCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CReliableCommandCountU3Ek__BackingField_1() { return &___U3CReliableCommandCountU3Ek__BackingField_1; }
	inline void set_U3CReliableCommandCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CReliableCommandCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUnreliableCommandCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CUnreliableCommandCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CUnreliableCommandCountU3Ek__BackingField_2() const { return ___U3CUnreliableCommandCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CUnreliableCommandCountU3Ek__BackingField_2() { return &___U3CUnreliableCommandCountU3Ek__BackingField_2; }
	inline void set_U3CUnreliableCommandCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CUnreliableCommandCountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFragmentCommandCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CFragmentCommandCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CFragmentCommandCountU3Ek__BackingField_3() const { return ___U3CFragmentCommandCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFragmentCommandCountU3Ek__BackingField_3() { return &___U3CFragmentCommandCountU3Ek__BackingField_3; }
	inline void set_U3CFragmentCommandCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CFragmentCommandCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CControlCommandCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CControlCommandCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CControlCommandCountU3Ek__BackingField_4() const { return ___U3CControlCommandCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CControlCommandCountU3Ek__BackingField_4() { return &___U3CControlCommandCountU3Ek__BackingField_4; }
	inline void set_U3CControlCommandCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CControlCommandCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CTotalPacketCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CTotalPacketCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CTotalPacketCountU3Ek__BackingField_5() const { return ___U3CTotalPacketCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CTotalPacketCountU3Ek__BackingField_5() { return &___U3CTotalPacketCountU3Ek__BackingField_5; }
	inline void set_U3CTotalPacketCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CTotalPacketCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTotalCommandsInPacketsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CTotalCommandsInPacketsU3Ek__BackingField_6)); }
	inline int32_t get_U3CTotalCommandsInPacketsU3Ek__BackingField_6() const { return ___U3CTotalCommandsInPacketsU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CTotalCommandsInPacketsU3Ek__BackingField_6() { return &___U3CTotalCommandsInPacketsU3Ek__BackingField_6; }
	inline void set_U3CTotalCommandsInPacketsU3Ek__BackingField_6(int32_t value)
	{
		___U3CTotalCommandsInPacketsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CReliableCommandBytesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CReliableCommandBytesU3Ek__BackingField_7)); }
	inline int32_t get_U3CReliableCommandBytesU3Ek__BackingField_7() const { return ___U3CReliableCommandBytesU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CReliableCommandBytesU3Ek__BackingField_7() { return &___U3CReliableCommandBytesU3Ek__BackingField_7; }
	inline void set_U3CReliableCommandBytesU3Ek__BackingField_7(int32_t value)
	{
		___U3CReliableCommandBytesU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUnreliableCommandBytesU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CUnreliableCommandBytesU3Ek__BackingField_8)); }
	inline int32_t get_U3CUnreliableCommandBytesU3Ek__BackingField_8() const { return ___U3CUnreliableCommandBytesU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CUnreliableCommandBytesU3Ek__BackingField_8() { return &___U3CUnreliableCommandBytesU3Ek__BackingField_8; }
	inline void set_U3CUnreliableCommandBytesU3Ek__BackingField_8(int32_t value)
	{
		___U3CUnreliableCommandBytesU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CFragmentCommandBytesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CFragmentCommandBytesU3Ek__BackingField_9)); }
	inline int32_t get_U3CFragmentCommandBytesU3Ek__BackingField_9() const { return ___U3CFragmentCommandBytesU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CFragmentCommandBytesU3Ek__BackingField_9() { return &___U3CFragmentCommandBytesU3Ek__BackingField_9; }
	inline void set_U3CFragmentCommandBytesU3Ek__BackingField_9(int32_t value)
	{
		___U3CFragmentCommandBytesU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CControlCommandBytesU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CControlCommandBytesU3Ek__BackingField_10)); }
	inline int32_t get_U3CControlCommandBytesU3Ek__BackingField_10() const { return ___U3CControlCommandBytesU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CControlCommandBytesU3Ek__BackingField_10() { return &___U3CControlCommandBytesU3Ek__BackingField_10; }
	inline void set_U3CControlCommandBytesU3Ek__BackingField_10(int32_t value)
	{
		___U3CControlCommandBytesU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CTimestampOfLastAckU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CTimestampOfLastAckU3Ek__BackingField_11)); }
	inline int32_t get_U3CTimestampOfLastAckU3Ek__BackingField_11() const { return ___U3CTimestampOfLastAckU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CTimestampOfLastAckU3Ek__BackingField_11() { return &___U3CTimestampOfLastAckU3Ek__BackingField_11; }
	inline void set_U3CTimestampOfLastAckU3Ek__BackingField_11(int32_t value)
	{
		___U3CTimestampOfLastAckU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TrafficStats_t1302902347, ___U3CTimestampOfLastReliableCommandU3Ek__BackingField_12)); }
	inline int32_t get_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12() const { return ___U3CTimestampOfLastReliableCommandU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12() { return &___U3CTimestampOfLastReliableCommandU3Ek__BackingField_12; }
	inline void set_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12(int32_t value)
	{
		___U3CTimestampOfLastReliableCommandU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAFFICSTATS_T1302902347_H
#ifndef UISYSTEMPROFILERAPI_T2230074258_H
#define UISYSTEMPROFILERAPI_T2230074258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_t2230074258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_T2230074258_H
#ifndef RECTTRANSFORMUTILITY_T1743242446_H
#define RECTTRANSFORMUTILITY_T1743242446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t1743242446  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t1743242446_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t1718750761* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t1743242446_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_t1718750761* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_t1718750761* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T1743242446_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef PROTOCOL16_T1856210005_H
#define PROTOCOL16_T1856210005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Protocol16
struct  Protocol16_t1856210005  : public IProtocol_t3448746462
{
public:
	// System.Byte[] ExitGames.Client.Photon.Protocol16::versionBytes
	ByteU5BU5D_t4116647657* ___versionBytes_0;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memShort
	ByteU5BU5D_t4116647657* ___memShort_1;
	// System.Int64[] ExitGames.Client.Photon.Protocol16::memLongBlock
	Int64U5BU5D_t2559172825* ___memLongBlock_2;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memLongBlockBytes
	ByteU5BU5D_t4116647657* ___memLongBlockBytes_3;
	// System.Double[] ExitGames.Client.Photon.Protocol16::memDoubleBlock
	DoubleU5BU5D_t3413330114* ___memDoubleBlock_6;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memDoubleBlockBytes
	ByteU5BU5D_t4116647657* ___memDoubleBlockBytes_7;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memInteger
	ByteU5BU5D_t4116647657* ___memInteger_8;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memLong
	ByteU5BU5D_t4116647657* ___memLong_9;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memFloat
	ByteU5BU5D_t4116647657* ___memFloat_10;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memDouble
	ByteU5BU5D_t4116647657* ___memDouble_11;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memString
	ByteU5BU5D_t4116647657* ___memString_12;

public:
	inline static int32_t get_offset_of_versionBytes_0() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___versionBytes_0)); }
	inline ByteU5BU5D_t4116647657* get_versionBytes_0() const { return ___versionBytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_versionBytes_0() { return &___versionBytes_0; }
	inline void set_versionBytes_0(ByteU5BU5D_t4116647657* value)
	{
		___versionBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___versionBytes_0), value);
	}

	inline static int32_t get_offset_of_memShort_1() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memShort_1)); }
	inline ByteU5BU5D_t4116647657* get_memShort_1() const { return ___memShort_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_memShort_1() { return &___memShort_1; }
	inline void set_memShort_1(ByteU5BU5D_t4116647657* value)
	{
		___memShort_1 = value;
		Il2CppCodeGenWriteBarrier((&___memShort_1), value);
	}

	inline static int32_t get_offset_of_memLongBlock_2() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memLongBlock_2)); }
	inline Int64U5BU5D_t2559172825* get_memLongBlock_2() const { return ___memLongBlock_2; }
	inline Int64U5BU5D_t2559172825** get_address_of_memLongBlock_2() { return &___memLongBlock_2; }
	inline void set_memLongBlock_2(Int64U5BU5D_t2559172825* value)
	{
		___memLongBlock_2 = value;
		Il2CppCodeGenWriteBarrier((&___memLongBlock_2), value);
	}

	inline static int32_t get_offset_of_memLongBlockBytes_3() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memLongBlockBytes_3)); }
	inline ByteU5BU5D_t4116647657* get_memLongBlockBytes_3() const { return ___memLongBlockBytes_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_memLongBlockBytes_3() { return &___memLongBlockBytes_3; }
	inline void set_memLongBlockBytes_3(ByteU5BU5D_t4116647657* value)
	{
		___memLongBlockBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___memLongBlockBytes_3), value);
	}

	inline static int32_t get_offset_of_memDoubleBlock_6() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memDoubleBlock_6)); }
	inline DoubleU5BU5D_t3413330114* get_memDoubleBlock_6() const { return ___memDoubleBlock_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_memDoubleBlock_6() { return &___memDoubleBlock_6; }
	inline void set_memDoubleBlock_6(DoubleU5BU5D_t3413330114* value)
	{
		___memDoubleBlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___memDoubleBlock_6), value);
	}

	inline static int32_t get_offset_of_memDoubleBlockBytes_7() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memDoubleBlockBytes_7)); }
	inline ByteU5BU5D_t4116647657* get_memDoubleBlockBytes_7() const { return ___memDoubleBlockBytes_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_memDoubleBlockBytes_7() { return &___memDoubleBlockBytes_7; }
	inline void set_memDoubleBlockBytes_7(ByteU5BU5D_t4116647657* value)
	{
		___memDoubleBlockBytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___memDoubleBlockBytes_7), value);
	}

	inline static int32_t get_offset_of_memInteger_8() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memInteger_8)); }
	inline ByteU5BU5D_t4116647657* get_memInteger_8() const { return ___memInteger_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_memInteger_8() { return &___memInteger_8; }
	inline void set_memInteger_8(ByteU5BU5D_t4116647657* value)
	{
		___memInteger_8 = value;
		Il2CppCodeGenWriteBarrier((&___memInteger_8), value);
	}

	inline static int32_t get_offset_of_memLong_9() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memLong_9)); }
	inline ByteU5BU5D_t4116647657* get_memLong_9() const { return ___memLong_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_memLong_9() { return &___memLong_9; }
	inline void set_memLong_9(ByteU5BU5D_t4116647657* value)
	{
		___memLong_9 = value;
		Il2CppCodeGenWriteBarrier((&___memLong_9), value);
	}

	inline static int32_t get_offset_of_memFloat_10() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memFloat_10)); }
	inline ByteU5BU5D_t4116647657* get_memFloat_10() const { return ___memFloat_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_memFloat_10() { return &___memFloat_10; }
	inline void set_memFloat_10(ByteU5BU5D_t4116647657* value)
	{
		___memFloat_10 = value;
		Il2CppCodeGenWriteBarrier((&___memFloat_10), value);
	}

	inline static int32_t get_offset_of_memDouble_11() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memDouble_11)); }
	inline ByteU5BU5D_t4116647657* get_memDouble_11() const { return ___memDouble_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_memDouble_11() { return &___memDouble_11; }
	inline void set_memDouble_11(ByteU5BU5D_t4116647657* value)
	{
		___memDouble_11 = value;
		Il2CppCodeGenWriteBarrier((&___memDouble_11), value);
	}

	inline static int32_t get_offset_of_memString_12() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005, ___memString_12)); }
	inline ByteU5BU5D_t4116647657* get_memString_12() const { return ___memString_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_memString_12() { return &___memString_12; }
	inline void set_memString_12(ByteU5BU5D_t4116647657* value)
	{
		___memString_12 = value;
		Il2CppCodeGenWriteBarrier((&___memString_12), value);
	}
};

struct Protocol16_t1856210005_StaticFields
{
public:
	// System.Single[] ExitGames.Client.Photon.Protocol16::memFloatBlock
	SingleU5BU5D_t1444911251* ___memFloatBlock_4;
	// System.Byte[] ExitGames.Client.Photon.Protocol16::memFloatBlockBytes
	ByteU5BU5D_t4116647657* ___memFloatBlockBytes_5;

public:
	inline static int32_t get_offset_of_memFloatBlock_4() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005_StaticFields, ___memFloatBlock_4)); }
	inline SingleU5BU5D_t1444911251* get_memFloatBlock_4() const { return ___memFloatBlock_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_memFloatBlock_4() { return &___memFloatBlock_4; }
	inline void set_memFloatBlock_4(SingleU5BU5D_t1444911251* value)
	{
		___memFloatBlock_4 = value;
		Il2CppCodeGenWriteBarrier((&___memFloatBlock_4), value);
	}

	inline static int32_t get_offset_of_memFloatBlockBytes_5() { return static_cast<int32_t>(offsetof(Protocol16_t1856210005_StaticFields, ___memFloatBlockBytes_5)); }
	inline ByteU5BU5D_t4116647657* get_memFloatBlockBytes_5() const { return ___memFloatBlockBytes_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_memFloatBlockBytes_5() { return &___memFloatBlockBytes_5; }
	inline void set_memFloatBlockBytes_5(ByteU5BU5D_t4116647657* value)
	{
		___memFloatBlockBytes_5 = value;
		Il2CppCodeGenWriteBarrier((&___memFloatBlockBytes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOL16_T1856210005_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef CMDLOGRECEIVEDRELIABLE_T4090183889_H
#define CMDLOGRECEIVEDRELIABLE_T4090183889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogReceivedReliable
struct  CmdLogReceivedReliable_t4090183889  : public CmdLogItem_t4217690540
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogReceivedReliable::TimeSinceLastSend
	int32_t ___TimeSinceLastSend_5;
	// System.Int32 ExitGames.Client.Photon.CmdLogReceivedReliable::TimeSinceLastSendAck
	int32_t ___TimeSinceLastSendAck_6;

public:
	inline static int32_t get_offset_of_TimeSinceLastSend_5() { return static_cast<int32_t>(offsetof(CmdLogReceivedReliable_t4090183889, ___TimeSinceLastSend_5)); }
	inline int32_t get_TimeSinceLastSend_5() const { return ___TimeSinceLastSend_5; }
	inline int32_t* get_address_of_TimeSinceLastSend_5() { return &___TimeSinceLastSend_5; }
	inline void set_TimeSinceLastSend_5(int32_t value)
	{
		___TimeSinceLastSend_5 = value;
	}

	inline static int32_t get_offset_of_TimeSinceLastSendAck_6() { return static_cast<int32_t>(offsetof(CmdLogReceivedReliable_t4090183889, ___TimeSinceLastSendAck_6)); }
	inline int32_t get_TimeSinceLastSendAck_6() const { return ___TimeSinceLastSendAck_6; }
	inline int32_t* get_address_of_TimeSinceLastSendAck_6() { return &___TimeSinceLastSendAck_6; }
	inline void set_TimeSinceLastSendAck_6(int32_t value)
	{
		___TimeSinceLastSendAck_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGRECEIVEDRELIABLE_T4090183889_H
#ifndef CMDLOGRECEIVEDACK_T580412049_H
#define CMDLOGRECEIVEDACK_T580412049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogReceivedAck
struct  CmdLogReceivedAck_t580412049  : public CmdLogItem_t4217690540
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogReceivedAck::ReceivedSentTime
	int32_t ___ReceivedSentTime_5;

public:
	inline static int32_t get_offset_of_ReceivedSentTime_5() { return static_cast<int32_t>(offsetof(CmdLogReceivedAck_t580412049, ___ReceivedSentTime_5)); }
	inline int32_t get_ReceivedSentTime_5() const { return ___ReceivedSentTime_5; }
	inline int32_t* get_address_of_ReceivedSentTime_5() { return &___ReceivedSentTime_5; }
	inline void set_ReceivedSentTime_5(int32_t value)
	{
		___ReceivedSentTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGRECEIVEDACK_T580412049_H
#ifndef CMDLOGSENTRELIABLE_T3437548410_H
#define CMDLOGSENTRELIABLE_T3437548410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.CmdLogSentReliable
struct  CmdLogSentReliable_t3437548410  : public CmdLogItem_t4217690540
{
public:
	// System.Int32 ExitGames.Client.Photon.CmdLogSentReliable::Resend
	int32_t ___Resend_5;
	// System.Int32 ExitGames.Client.Photon.CmdLogSentReliable::RoundtripTimeout
	int32_t ___RoundtripTimeout_6;
	// System.Int32 ExitGames.Client.Photon.CmdLogSentReliable::Timeout
	int32_t ___Timeout_7;
	// System.Boolean ExitGames.Client.Photon.CmdLogSentReliable::TriggeredTimeout
	bool ___TriggeredTimeout_8;

public:
	inline static int32_t get_offset_of_Resend_5() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___Resend_5)); }
	inline int32_t get_Resend_5() const { return ___Resend_5; }
	inline int32_t* get_address_of_Resend_5() { return &___Resend_5; }
	inline void set_Resend_5(int32_t value)
	{
		___Resend_5 = value;
	}

	inline static int32_t get_offset_of_RoundtripTimeout_6() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___RoundtripTimeout_6)); }
	inline int32_t get_RoundtripTimeout_6() const { return ___RoundtripTimeout_6; }
	inline int32_t* get_address_of_RoundtripTimeout_6() { return &___RoundtripTimeout_6; }
	inline void set_RoundtripTimeout_6(int32_t value)
	{
		___RoundtripTimeout_6 = value;
	}

	inline static int32_t get_offset_of_Timeout_7() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___Timeout_7)); }
	inline int32_t get_Timeout_7() const { return ___Timeout_7; }
	inline int32_t* get_address_of_Timeout_7() { return &___Timeout_7; }
	inline void set_Timeout_7(int32_t value)
	{
		___Timeout_7 = value;
	}

	inline static int32_t get_offset_of_TriggeredTimeout_8() { return static_cast<int32_t>(offsetof(CmdLogSentReliable_t3437548410, ___TriggeredTimeout_8)); }
	inline bool get_TriggeredTimeout_8() const { return ___TriggeredTimeout_8; }
	inline bool* get_address_of_TriggeredTimeout_8() { return &___TriggeredTimeout_8; }
	inline void set_TriggeredTimeout_8(bool value)
	{
		___TriggeredTimeout_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDLOGSENTRELIABLE_T3437548410_H
#ifndef JOINTLIMITS_T2681268900_H
#define JOINTLIMITS_T2681268900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointLimits
struct  JointLimits_t2681268900 
{
public:
	// System.Single UnityEngine.JointLimits::m_Min
	float ___m_Min_0;
	// System.Single UnityEngine.JointLimits::m_Max
	float ___m_Max_1;
	// System.Single UnityEngine.JointLimits::m_Bounciness
	float ___m_Bounciness_2;
	// System.Single UnityEngine.JointLimits::m_BounceMinVelocity
	float ___m_BounceMinVelocity_3;
	// System.Single UnityEngine.JointLimits::m_ContactDistance
	float ___m_ContactDistance_4;
	// System.Single UnityEngine.JointLimits::minBounce
	float ___minBounce_5;
	// System.Single UnityEngine.JointLimits::maxBounce
	float ___maxBounce_6;

public:
	inline static int32_t get_offset_of_m_Min_0() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_Min_0)); }
	inline float get_m_Min_0() const { return ___m_Min_0; }
	inline float* get_address_of_m_Min_0() { return &___m_Min_0; }
	inline void set_m_Min_0(float value)
	{
		___m_Min_0 = value;
	}

	inline static int32_t get_offset_of_m_Max_1() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_Max_1)); }
	inline float get_m_Max_1() const { return ___m_Max_1; }
	inline float* get_address_of_m_Max_1() { return &___m_Max_1; }
	inline void set_m_Max_1(float value)
	{
		___m_Max_1 = value;
	}

	inline static int32_t get_offset_of_m_Bounciness_2() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_Bounciness_2)); }
	inline float get_m_Bounciness_2() const { return ___m_Bounciness_2; }
	inline float* get_address_of_m_Bounciness_2() { return &___m_Bounciness_2; }
	inline void set_m_Bounciness_2(float value)
	{
		___m_Bounciness_2 = value;
	}

	inline static int32_t get_offset_of_m_BounceMinVelocity_3() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_BounceMinVelocity_3)); }
	inline float get_m_BounceMinVelocity_3() const { return ___m_BounceMinVelocity_3; }
	inline float* get_address_of_m_BounceMinVelocity_3() { return &___m_BounceMinVelocity_3; }
	inline void set_m_BounceMinVelocity_3(float value)
	{
		___m_BounceMinVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_ContactDistance_4() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_ContactDistance_4)); }
	inline float get_m_ContactDistance_4() const { return ___m_ContactDistance_4; }
	inline float* get_address_of_m_ContactDistance_4() { return &___m_ContactDistance_4; }
	inline void set_m_ContactDistance_4(float value)
	{
		___m_ContactDistance_4 = value;
	}

	inline static int32_t get_offset_of_minBounce_5() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___minBounce_5)); }
	inline float get_minBounce_5() const { return ___minBounce_5; }
	inline float* get_address_of_minBounce_5() { return &___minBounce_5; }
	inline void set_minBounce_5(float value)
	{
		___minBounce_5 = value;
	}

	inline static int32_t get_offset_of_maxBounce_6() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___maxBounce_6)); }
	inline float get_maxBounce_6() const { return ___maxBounce_6; }
	inline float* get_address_of_maxBounce_6() { return &___maxBounce_6; }
	inline void set_maxBounce_6(float value)
	{
		___maxBounce_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTLIMITS_T2681268900_H
#ifndef INT16_T2552820387_H
#define INT16_T2552820387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t2552820387 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int16_t2552820387, ___m_value_2)); }
	inline int16_t get_m_value_2() const { return ___m_value_2; }
	inline int16_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int16_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T2552820387_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef HASHTABLE_T1048209202_H
#define HASHTABLE_T1048209202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Hashtable
struct  Hashtable_t1048209202  : public Dictionary_2_t132545152
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLE_T1048209202_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PINGNATIVESTATIC_T269944269_H
#define PINGNATIVESTATIC_T269944269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PingNativeStatic
struct  PingNativeStatic_t269944269  : public PhotonPing_t2371975946
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGNATIVESTATIC_T269944269_H
#ifndef STREAMBUFFER_T3827669789_H
#define STREAMBUFFER_T3827669789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.StreamBuffer
struct  StreamBuffer_t3827669789  : public Stream_t1273022909
{
public:
	// System.Int32 ExitGames.Client.Photon.StreamBuffer::pos
	int32_t ___pos_2;
	// System.Int32 ExitGames.Client.Photon.StreamBuffer::len
	int32_t ___len_3;
	// System.Byte[] ExitGames.Client.Photon.StreamBuffer::buf
	ByteU5BU5D_t4116647657* ___buf_4;

public:
	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(StreamBuffer_t3827669789, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_len_3() { return static_cast<int32_t>(offsetof(StreamBuffer_t3827669789, ___len_3)); }
	inline int32_t get_len_3() const { return ___len_3; }
	inline int32_t* get_address_of_len_3() { return &___len_3; }
	inline void set_len_3(int32_t value)
	{
		___len_3 = value;
	}

	inline static int32_t get_offset_of_buf_4() { return static_cast<int32_t>(offsetof(StreamBuffer_t3827669789, ___buf_4)); }
	inline ByteU5BU5D_t4116647657* get_buf_4() const { return ___buf_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_4() { return &___buf_4; }
	inline void set_buf_4(ByteU5BU5D_t4116647657* value)
	{
		___buf_4 = value;
		Il2CppCodeGenWriteBarrier((&___buf_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMBUFFER_T3827669789_H
#ifndef PINGNATIVEDYNAMIC_T2826633900_H
#define PINGNATIVEDYNAMIC_T2826633900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PingNativeDynamic
struct  PingNativeDynamic_t2826633900  : public PhotonPing_t2371975946
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGNATIVEDYNAMIC_T2826633900_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PINGMONO_T2784932916_H
#define PINGMONO_T2784932916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PingMono
struct  PingMono_t2784932916  : public PhotonPing_t2371975946
{
public:
	// System.Net.Sockets.Socket ExitGames.Client.Photon.PingMono::sock
	Socket_t1119025450 * ___sock_6;

public:
	inline static int32_t get_offset_of_sock_6() { return static_cast<int32_t>(offsetof(PingMono_t2784932916, ___sock_6)); }
	inline Socket_t1119025450 * get_sock_6() const { return ___sock_6; }
	inline Socket_t1119025450 ** get_address_of_sock_6() { return &___sock_6; }
	inline void set_sock_6(Socket_t1119025450 * value)
	{
		___sock_6 = value;
		Il2CppCodeGenWriteBarrier((&___sock_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGMONO_T2784932916_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef PLAYABLEHANDLE_T1095853803_H
#define PLAYABLEHANDLE_T1095853803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1095853803 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1095853803_H
#ifndef PHOTONSOCKETERROR_T821309465_H
#define PHOTONSOCKETERROR_T821309465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonSocketError
struct  PhotonSocketError_t821309465 
{
public:
	// System.Int32 ExitGames.Client.Photon.PhotonSocketError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonSocketError_t821309465, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSOCKETERROR_T821309465_H
#ifndef PHOTONSOCKETSTATE_T2742032721_H
#define PHOTONSOCKETSTATE_T2742032721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonSocketState
struct  PhotonSocketState_t2742032721 
{
public:
	// System.Int32 ExitGames.Client.Photon.PhotonSocketState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonSocketState_t2742032721, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSOCKETSTATE_T2742032721_H
#ifndef GPTYPE_T2340543107_H
#define GPTYPE_T2340543107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Protocol16/GpType
struct  GpType_t2340543107 
{
public:
	// System.Byte ExitGames.Client.Photon.Protocol16/GpType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GpType_t2340543107, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPTYPE_T2340543107_H
#ifndef CONNECTIONSTATEVALUE_T1954099360_H
#define CONNECTIONSTATEVALUE_T1954099360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/ConnectionStateValue
struct  ConnectionStateValue_t1954099360 
{
public:
	// System.Byte ExitGames.Client.Photon.PeerBase/ConnectionStateValue::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionStateValue_t1954099360, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATEVALUE_T1954099360_H
#ifndef SAMPLETYPE_T1208595618_H
#define SAMPLETYPE_T1208595618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi/SampleType
struct  SampleType_t1208595618 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi/SampleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleType_t1208595618, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T1208595618_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef PEERSTATEVALUE_T1289417078_H
#define PEERSTATEVALUE_T1289417078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerStateValue
struct  PeerStateValue_t1289417078 
{
public:
	// System.Byte ExitGames.Client.Photon.PeerStateValue::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PeerStateValue_t1289417078, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERSTATEVALUE_T1289417078_H
#ifndef STATUSCODE_T823606708_H
#define STATUSCODE_T823606708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.StatusCode
struct  StatusCode_t823606708 
{
public:
	// System.Int32 ExitGames.Client.Photon.StatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StatusCode_t823606708, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSCODE_T823606708_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef EGMESSAGETYPE_T1130059189_H
#define EGMESSAGETYPE_T1130059189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/EgMessageType
struct  EgMessageType_t1130059189 
{
public:
	// System.Byte ExitGames.Client.Photon.PeerBase/EgMessageType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EgMessageType_t1130059189, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGMESSAGETYPE_T1130059189_H
#ifndef RIGIDBODYCONSTRAINTS_T3348042902_H
#define RIGIDBODYCONSTRAINTS_T3348042902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_t3348042902 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_t3348042902, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS_T3348042902_H
#ifndef FORCEMODE_T3656391766_H
#define FORCEMODE_T3656391766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode
struct  ForceMode_t3656391766 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode_t3656391766, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE_T3656391766_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef CONTROLLERCOLLIDERHIT_T240592346_H
#define CONTROLLERCOLLIDERHIT_T240592346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ControllerColliderHit
struct  ControllerColliderHit_t240592346  : public RuntimeObject
{
public:
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t1138636865 * ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1773347010 * ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t3722313464  ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t3722313464  ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t3722313464  ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;

public:
	inline static int32_t get_offset_of_m_Controller_0() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Controller_0)); }
	inline CharacterController_t1138636865 * get_m_Controller_0() const { return ___m_Controller_0; }
	inline CharacterController_t1138636865 ** get_address_of_m_Controller_0() { return &___m_Controller_0; }
	inline void set_m_Controller_0(CharacterController_t1138636865 * value)
	{
		___m_Controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_0), value);
	}

	inline static int32_t get_offset_of_m_Collider_1() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Collider_1)); }
	inline Collider_t1773347010 * get_m_Collider_1() const { return ___m_Collider_1; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_1() { return &___m_Collider_1; }
	inline void set_m_Collider_1(Collider_t1773347010 * value)
	{
		___m_Collider_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_1), value);
	}

	inline static int32_t get_offset_of_m_Point_2() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Point_2)); }
	inline Vector3_t3722313464  get_m_Point_2() const { return ___m_Point_2; }
	inline Vector3_t3722313464 * get_address_of_m_Point_2() { return &___m_Point_2; }
	inline void set_m_Point_2(Vector3_t3722313464  value)
	{
		___m_Point_2 = value;
	}

	inline static int32_t get_offset_of_m_Normal_3() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Normal_3)); }
	inline Vector3_t3722313464  get_m_Normal_3() const { return ___m_Normal_3; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_3() { return &___m_Normal_3; }
	inline void set_m_Normal_3(Vector3_t3722313464  value)
	{
		___m_Normal_3 = value;
	}

	inline static int32_t get_offset_of_m_MoveDirection_4() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveDirection_4)); }
	inline Vector3_t3722313464  get_m_MoveDirection_4() const { return ___m_MoveDirection_4; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDirection_4() { return &___m_MoveDirection_4; }
	inline void set_m_MoveDirection_4(Vector3_t3722313464  value)
	{
		___m_MoveDirection_4 = value;
	}

	inline static int32_t get_offset_of_m_MoveLength_5() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveLength_5)); }
	inline float get_m_MoveLength_5() const { return ___m_MoveLength_5; }
	inline float* get_address_of_m_MoveLength_5() { return &___m_MoveLength_5; }
	inline void set_m_MoveLength_5(float value)
	{
		___m_MoveLength_5 = value;
	}

	inline static int32_t get_offset_of_m_Push_6() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Push_6)); }
	inline int32_t get_m_Push_6() const { return ___m_Push_6; }
	inline int32_t* get_address_of_m_Push_6() { return &___m_Push_6; }
	inline void set_m_Push_6(int32_t value)
	{
		___m_Push_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_pinvoke
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_com
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
#endif // CONTROLLERCOLLIDERHIT_T240592346_H
#ifndef COLLISION_T4262080450_H
#define COLLISION_T4262080450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision
struct  Collision_t4262080450  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t3722313464  ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	// UnityEngine.Rigidbody UnityEngine.Collision::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1773347010 * ___m_Collider_3;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_Contacts
	ContactPointU5BU5D_t872956888* ___m_Contacts_4;

public:
	inline static int32_t get_offset_of_m_Impulse_0() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Impulse_0)); }
	inline Vector3_t3722313464  get_m_Impulse_0() const { return ___m_Impulse_0; }
	inline Vector3_t3722313464 * get_address_of_m_Impulse_0() { return &___m_Impulse_0; }
	inline void set_m_Impulse_0(Vector3_t3722313464  value)
	{
		___m_Impulse_0 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_1() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_RelativeVelocity_1)); }
	inline Vector3_t3722313464  get_m_RelativeVelocity_1() const { return ___m_RelativeVelocity_1; }
	inline Vector3_t3722313464 * get_address_of_m_RelativeVelocity_1() { return &___m_RelativeVelocity_1; }
	inline void set_m_RelativeVelocity_1(Vector3_t3722313464  value)
	{
		___m_RelativeVelocity_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Rigidbody_2)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_2), value);
	}

	inline static int32_t get_offset_of_m_Collider_3() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Collider_3)); }
	inline Collider_t1773347010 * get_m_Collider_3() const { return ___m_Collider_3; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_3() { return &___m_Collider_3; }
	inline void set_m_Collider_3(Collider_t1773347010 * value)
	{
		___m_Collider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_3), value);
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Contacts_4)); }
	inline ContactPointU5BU5D_t872956888* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPointU5BU5D_t872956888** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPointU5BU5D_t872956888* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_com
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
#endif // COLLISION_T4262080450_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef RENDERMODE_T4077056833_H
#define RENDERMODE_T4077056833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4077056833 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4077056833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4077056833_H
#ifndef QUERYTRIGGERINTERACTION_T962663221_H
#define QUERYTRIGGERINTERACTION_T962663221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.QueryTriggerInteraction
struct  QueryTriggerInteraction_t962663221 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t962663221, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYTRIGGERINTERACTION_T962663221_H
#ifndef U3CU3EC__DISPLAYCLASS146_0_T1573695292_H
#define U3CU3EC__DISPLAYCLASS146_0_T1573695292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/<>c__DisplayClass146_0
struct  U3CU3Ec__DisplayClass146_0_t1573695292  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.StatusCode ExitGames.Client.Photon.PeerBase/<>c__DisplayClass146_0::statusValue
	int32_t ___statusValue_0;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PeerBase/<>c__DisplayClass146_0::<>4__this
	PeerBase_t2956237011 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_statusValue_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass146_0_t1573695292, ___statusValue_0)); }
	inline int32_t get_statusValue_0() const { return ___statusValue_0; }
	inline int32_t* get_address_of_statusValue_0() { return &___statusValue_0; }
	inline void set_statusValue_0(int32_t value)
	{
		___statusValue_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass146_0_t1573695292, ___U3CU3E4__this_1)); }
	inline PeerBase_t2956237011 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PeerBase_t2956237011 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PeerBase_t2956237011 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS146_0_T1573695292_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef VIDEOCLIPPLAYABLE_T2598186649_H
#define VIDEOCLIPPLAYABLE_T2598186649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Video.VideoClipPlayable
struct  VideoClipPlayable_t2598186649 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(VideoClipPlayable_t2598186649, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIPPLAYABLE_T2598186649_H
#ifndef IPHOTONSOCKET_T2066969247_H
#define IPHOTONSOCKET_T2066969247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.IPhotonSocket
struct  IPhotonSocket_t2066969247  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.IPhotonSocket::peerBase
	PeerBase_t2956237011 * ___peerBase_0;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.IPhotonSocket::<Protocol>k__BackingField
	uint8_t ___U3CProtocolU3Ek__BackingField_1;
	// System.Boolean ExitGames.Client.Photon.IPhotonSocket::PollReceive
	bool ___PollReceive_2;
	// ExitGames.Client.Photon.PhotonSocketState ExitGames.Client.Photon.IPhotonSocket::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_3;
	// System.String ExitGames.Client.Photon.IPhotonSocket::<ServerAddress>k__BackingField
	String_t* ___U3CServerAddressU3Ek__BackingField_4;
	// System.Int32 ExitGames.Client.Photon.IPhotonSocket::<ServerPort>k__BackingField
	int32_t ___U3CServerPortU3Ek__BackingField_5;
	// System.Boolean ExitGames.Client.Photon.IPhotonSocket::<AddressResolvedAsIpv6>k__BackingField
	bool ___U3CAddressResolvedAsIpv6U3Ek__BackingField_6;
	// System.String ExitGames.Client.Photon.IPhotonSocket::<UrlProtocol>k__BackingField
	String_t* ___U3CUrlProtocolU3Ek__BackingField_7;
	// System.String ExitGames.Client.Photon.IPhotonSocket::<UrlPath>k__BackingField
	String_t* ___U3CUrlPathU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_peerBase_0() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___peerBase_0)); }
	inline PeerBase_t2956237011 * get_peerBase_0() const { return ___peerBase_0; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_0() { return &___peerBase_0; }
	inline void set_peerBase_0(PeerBase_t2956237011 * value)
	{
		___peerBase_0 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_0), value);
	}

	inline static int32_t get_offset_of_U3CProtocolU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CProtocolU3Ek__BackingField_1)); }
	inline uint8_t get_U3CProtocolU3Ek__BackingField_1() const { return ___U3CProtocolU3Ek__BackingField_1; }
	inline uint8_t* get_address_of_U3CProtocolU3Ek__BackingField_1() { return &___U3CProtocolU3Ek__BackingField_1; }
	inline void set_U3CProtocolU3Ek__BackingField_1(uint8_t value)
	{
		___U3CProtocolU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_PollReceive_2() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___PollReceive_2)); }
	inline bool get_PollReceive_2() const { return ___PollReceive_2; }
	inline bool* get_address_of_PollReceive_2() { return &___PollReceive_2; }
	inline void set_PollReceive_2(bool value)
	{
		___PollReceive_2 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CStateU3Ek__BackingField_3() const { return ___U3CStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_3() { return &___U3CStateU3Ek__BackingField_3; }
	inline void set_U3CStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CServerAddressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CServerAddressU3Ek__BackingField_4)); }
	inline String_t* get_U3CServerAddressU3Ek__BackingField_4() const { return ___U3CServerAddressU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServerAddressU3Ek__BackingField_4() { return &___U3CServerAddressU3Ek__BackingField_4; }
	inline void set_U3CServerAddressU3Ek__BackingField_4(String_t* value)
	{
		___U3CServerAddressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerAddressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CServerPortU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CServerPortU3Ek__BackingField_5)); }
	inline int32_t get_U3CServerPortU3Ek__BackingField_5() const { return ___U3CServerPortU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CServerPortU3Ek__BackingField_5() { return &___U3CServerPortU3Ek__BackingField_5; }
	inline void set_U3CServerPortU3Ek__BackingField_5(int32_t value)
	{
		___U3CServerPortU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CAddressResolvedAsIpv6U3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CAddressResolvedAsIpv6U3Ek__BackingField_6)); }
	inline bool get_U3CAddressResolvedAsIpv6U3Ek__BackingField_6() const { return ___U3CAddressResolvedAsIpv6U3Ek__BackingField_6; }
	inline bool* get_address_of_U3CAddressResolvedAsIpv6U3Ek__BackingField_6() { return &___U3CAddressResolvedAsIpv6U3Ek__BackingField_6; }
	inline void set_U3CAddressResolvedAsIpv6U3Ek__BackingField_6(bool value)
	{
		___U3CAddressResolvedAsIpv6U3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CUrlProtocolU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CUrlProtocolU3Ek__BackingField_7)); }
	inline String_t* get_U3CUrlProtocolU3Ek__BackingField_7() const { return ___U3CUrlProtocolU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CUrlProtocolU3Ek__BackingField_7() { return &___U3CUrlProtocolU3Ek__BackingField_7; }
	inline void set_U3CUrlProtocolU3Ek__BackingField_7(String_t* value)
	{
		___U3CUrlProtocolU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlProtocolU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CUrlPathU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(IPhotonSocket_t2066969247, ___U3CUrlPathU3Ek__BackingField_8)); }
	inline String_t* get_U3CUrlPathU3Ek__BackingField_8() const { return ___U3CUrlPathU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CUrlPathU3Ek__BackingField_8() { return &___U3CUrlPathU3Ek__BackingField_8; }
	inline void set_U3CUrlPathU3Ek__BackingField_8(String_t* value)
	{
		___U3CUrlPathU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlPathU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPHOTONSOCKET_T2066969247_H
#ifndef U3CU3EC__DISPLAYCLASS145_0_T1573695289_H
#define U3CU3EC__DISPLAYCLASS145_0_T1573695289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0
struct  U3CU3Ec__DisplayClass145_0_t1573695289  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0::level
	uint8_t ___level_0;
	// System.String ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0::debugReturn
	String_t* ___debugReturn_1;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PeerBase/<>c__DisplayClass145_0::<>4__this
	PeerBase_t2956237011 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass145_0_t1573695289, ___level_0)); }
	inline uint8_t get_level_0() const { return ___level_0; }
	inline uint8_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(uint8_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_debugReturn_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass145_0_t1573695289, ___debugReturn_1)); }
	inline String_t* get_debugReturn_1() const { return ___debugReturn_1; }
	inline String_t** get_address_of_debugReturn_1() { return &___debugReturn_1; }
	inline void set_debugReturn_1(String_t* value)
	{
		___debugReturn_1 = value;
		Il2CppCodeGenWriteBarrier((&___debugReturn_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass145_0_t1573695289, ___U3CU3E4__this_2)); }
	inline PeerBase_t2956237011 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PeerBase_t2956237011 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PeerBase_t2956237011 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS145_0_T1573695289_H
#ifndef PEERBASE_T2956237011_H
#define PEERBASE_T2956237011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase
struct  PeerBase_t2956237011  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.PhotonPeer ExitGames.Client.Photon.PeerBase::ppeer
	PhotonPeer_t1608153861 * ___ppeer_0;
	// ExitGames.Client.IProtocol ExitGames.Client.Photon.PeerBase::protocol
	IProtocol_t3448746462 * ___protocol_1;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PeerBase::usedProtocol
	uint8_t ___usedProtocol_2;
	// ExitGames.Client.Photon.IPhotonSocket ExitGames.Client.Photon.PeerBase::rt
	IPhotonSocket_t2066969247 * ___rt_3;
	// System.String ExitGames.Client.Photon.PeerBase::<ServerAddress>k__BackingField
	String_t* ___U3CServerAddressU3Ek__BackingField_4;
	// System.String ExitGames.Client.Photon.PeerBase::<HttpUrlParameters>k__BackingField
	String_t* ___U3CHttpUrlParametersU3Ek__BackingField_5;
	// System.Int32 ExitGames.Client.Photon.PeerBase::ByteCountLastOperation
	int32_t ___ByteCountLastOperation_6;
	// System.Int32 ExitGames.Client.Photon.PeerBase::ByteCountCurrentDispatch
	int32_t ___ByteCountCurrentDispatch_7;
	// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.PeerBase::CommandInCurrentDispatch
	NCommand_t1230688399 * ___CommandInCurrentDispatch_8;
	// System.Int32 ExitGames.Client.Photon.PeerBase::TrafficPackageHeaderSize
	int32_t ___TrafficPackageHeaderSize_9;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetLossByCrc
	int32_t ___packetLossByCrc_10;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetLossByChallenge
	int32_t ___packetLossByChallenge_11;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction> ExitGames.Client.Photon.PeerBase::ActionQueue
	Queue_1_t2309151397 * ___ActionQueue_12;
	// System.Int16 ExitGames.Client.Photon.PeerBase::peerID
	int16_t ___peerID_13;
	// ExitGames.Client.Photon.PeerBase/ConnectionStateValue ExitGames.Client.Photon.PeerBase::peerConnectionState
	uint8_t ___peerConnectionState_14;
	// System.Int32 ExitGames.Client.Photon.PeerBase::serverTimeOffset
	int32_t ___serverTimeOffset_15;
	// System.Boolean ExitGames.Client.Photon.PeerBase::serverTimeOffsetIsAvailable
	bool ___serverTimeOffsetIsAvailable_16;
	// System.Int32 ExitGames.Client.Photon.PeerBase::roundTripTime
	int32_t ___roundTripTime_17;
	// System.Int32 ExitGames.Client.Photon.PeerBase::roundTripTimeVariance
	int32_t ___roundTripTimeVariance_18;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lastRoundTripTime
	int32_t ___lastRoundTripTime_19;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lowestRoundTripTime
	int32_t ___lowestRoundTripTime_20;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lastRoundTripTimeVariance
	int32_t ___lastRoundTripTimeVariance_21;
	// System.Int32 ExitGames.Client.Photon.PeerBase::highestRoundTripTimeVariance
	int32_t ___highestRoundTripTimeVariance_22;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timestampOfLastReceive
	int32_t ___timestampOfLastReceive_23;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetThrottleInterval
	int32_t ___packetThrottleInterval_24;
	// System.Int64 ExitGames.Client.Photon.PeerBase::bytesOut
	int64_t ___bytesOut_26;
	// System.Int64 ExitGames.Client.Photon.PeerBase::bytesIn
	int64_t ___bytesIn_27;
	// System.Int32 ExitGames.Client.Photon.PeerBase::commandBufferSize
	int32_t ___commandBufferSize_28;
	// Photon.SocketServer.Security.ICryptoProvider ExitGames.Client.Photon.PeerBase::CryptoProvider
	RuntimeObject* ___CryptoProvider_29;
	// System.Random ExitGames.Client.Photon.PeerBase::lagRandomizer
	Random_t108471755 * ___lagRandomizer_30;
	// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem> ExitGames.Client.Photon.PeerBase::NetSimListOutgoing
	LinkedList_1_t1884284488 * ___NetSimListOutgoing_31;
	// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem> ExitGames.Client.Photon.PeerBase::NetSimListIncoming
	LinkedList_1_t1884284488 * ___NetSimListIncoming_32;
	// ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PeerBase::networkSimulationSettings
	NetworkSimulationSet_t2000596048 * ___networkSimulationSettings_33;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem> ExitGames.Client.Photon.PeerBase::CommandLog
	Queue_1_t4063950034 * ___CommandLog_34;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem> ExitGames.Client.Photon.PeerBase::InReliableLog
	Queue_1_t4063950034 * ___InReliableLog_35;
	// System.Object ExitGames.Client.Photon.PeerBase::CustomInitData
	RuntimeObject * ___CustomInitData_36;
	// System.String ExitGames.Client.Photon.PeerBase::AppId
	String_t* ___AppId_37;
	// System.Byte[] ExitGames.Client.Photon.PeerBase::<TcpConnectionPrefix>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CTcpConnectionPrefixU3Ek__BackingField_38;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeBase
	int32_t ___timeBase_39;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeInt
	int32_t ___timeInt_40;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeoutInt
	int32_t ___timeoutInt_41;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastAckReceive
	int32_t ___timeLastAckReceive_42;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastSendAck
	int32_t ___timeLastSendAck_43;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastSendOutgoing
	int32_t ___timeLastSendOutgoing_44;
	// System.Boolean ExitGames.Client.Photon.PeerBase::ApplicationIsInitialized
	bool ___ApplicationIsInitialized_48;
	// System.Boolean ExitGames.Client.Photon.PeerBase::isEncryptionAvailable
	bool ___isEncryptionAvailable_49;
	// System.Int32 ExitGames.Client.Photon.PeerBase::outgoingCommandsInStream
	int32_t ___outgoingCommandsInStream_50;
	// ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::SerializeMemStream
	StreamBuffer_t3827669789 * ___SerializeMemStream_51;

public:
	inline static int32_t get_offset_of_ppeer_0() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ppeer_0)); }
	inline PhotonPeer_t1608153861 * get_ppeer_0() const { return ___ppeer_0; }
	inline PhotonPeer_t1608153861 ** get_address_of_ppeer_0() { return &___ppeer_0; }
	inline void set_ppeer_0(PhotonPeer_t1608153861 * value)
	{
		___ppeer_0 = value;
		Il2CppCodeGenWriteBarrier((&___ppeer_0), value);
	}

	inline static int32_t get_offset_of_protocol_1() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___protocol_1)); }
	inline IProtocol_t3448746462 * get_protocol_1() const { return ___protocol_1; }
	inline IProtocol_t3448746462 ** get_address_of_protocol_1() { return &___protocol_1; }
	inline void set_protocol_1(IProtocol_t3448746462 * value)
	{
		___protocol_1 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_1), value);
	}

	inline static int32_t get_offset_of_usedProtocol_2() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___usedProtocol_2)); }
	inline uint8_t get_usedProtocol_2() const { return ___usedProtocol_2; }
	inline uint8_t* get_address_of_usedProtocol_2() { return &___usedProtocol_2; }
	inline void set_usedProtocol_2(uint8_t value)
	{
		___usedProtocol_2 = value;
	}

	inline static int32_t get_offset_of_rt_3() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___rt_3)); }
	inline IPhotonSocket_t2066969247 * get_rt_3() const { return ___rt_3; }
	inline IPhotonSocket_t2066969247 ** get_address_of_rt_3() { return &___rt_3; }
	inline void set_rt_3(IPhotonSocket_t2066969247 * value)
	{
		___rt_3 = value;
		Il2CppCodeGenWriteBarrier((&___rt_3), value);
	}

	inline static int32_t get_offset_of_U3CServerAddressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___U3CServerAddressU3Ek__BackingField_4)); }
	inline String_t* get_U3CServerAddressU3Ek__BackingField_4() const { return ___U3CServerAddressU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServerAddressU3Ek__BackingField_4() { return &___U3CServerAddressU3Ek__BackingField_4; }
	inline void set_U3CServerAddressU3Ek__BackingField_4(String_t* value)
	{
		___U3CServerAddressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerAddressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___U3CHttpUrlParametersU3Ek__BackingField_5)); }
	inline String_t* get_U3CHttpUrlParametersU3Ek__BackingField_5() const { return ___U3CHttpUrlParametersU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CHttpUrlParametersU3Ek__BackingField_5() { return &___U3CHttpUrlParametersU3Ek__BackingField_5; }
	inline void set_U3CHttpUrlParametersU3Ek__BackingField_5(String_t* value)
	{
		___U3CHttpUrlParametersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHttpUrlParametersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_ByteCountLastOperation_6() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ByteCountLastOperation_6)); }
	inline int32_t get_ByteCountLastOperation_6() const { return ___ByteCountLastOperation_6; }
	inline int32_t* get_address_of_ByteCountLastOperation_6() { return &___ByteCountLastOperation_6; }
	inline void set_ByteCountLastOperation_6(int32_t value)
	{
		___ByteCountLastOperation_6 = value;
	}

	inline static int32_t get_offset_of_ByteCountCurrentDispatch_7() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ByteCountCurrentDispatch_7)); }
	inline int32_t get_ByteCountCurrentDispatch_7() const { return ___ByteCountCurrentDispatch_7; }
	inline int32_t* get_address_of_ByteCountCurrentDispatch_7() { return &___ByteCountCurrentDispatch_7; }
	inline void set_ByteCountCurrentDispatch_7(int32_t value)
	{
		___ByteCountCurrentDispatch_7 = value;
	}

	inline static int32_t get_offset_of_CommandInCurrentDispatch_8() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CommandInCurrentDispatch_8)); }
	inline NCommand_t1230688399 * get_CommandInCurrentDispatch_8() const { return ___CommandInCurrentDispatch_8; }
	inline NCommand_t1230688399 ** get_address_of_CommandInCurrentDispatch_8() { return &___CommandInCurrentDispatch_8; }
	inline void set_CommandInCurrentDispatch_8(NCommand_t1230688399 * value)
	{
		___CommandInCurrentDispatch_8 = value;
		Il2CppCodeGenWriteBarrier((&___CommandInCurrentDispatch_8), value);
	}

	inline static int32_t get_offset_of_TrafficPackageHeaderSize_9() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___TrafficPackageHeaderSize_9)); }
	inline int32_t get_TrafficPackageHeaderSize_9() const { return ___TrafficPackageHeaderSize_9; }
	inline int32_t* get_address_of_TrafficPackageHeaderSize_9() { return &___TrafficPackageHeaderSize_9; }
	inline void set_TrafficPackageHeaderSize_9(int32_t value)
	{
		___TrafficPackageHeaderSize_9 = value;
	}

	inline static int32_t get_offset_of_packetLossByCrc_10() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___packetLossByCrc_10)); }
	inline int32_t get_packetLossByCrc_10() const { return ___packetLossByCrc_10; }
	inline int32_t* get_address_of_packetLossByCrc_10() { return &___packetLossByCrc_10; }
	inline void set_packetLossByCrc_10(int32_t value)
	{
		___packetLossByCrc_10 = value;
	}

	inline static int32_t get_offset_of_packetLossByChallenge_11() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___packetLossByChallenge_11)); }
	inline int32_t get_packetLossByChallenge_11() const { return ___packetLossByChallenge_11; }
	inline int32_t* get_address_of_packetLossByChallenge_11() { return &___packetLossByChallenge_11; }
	inline void set_packetLossByChallenge_11(int32_t value)
	{
		___packetLossByChallenge_11 = value;
	}

	inline static int32_t get_offset_of_ActionQueue_12() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ActionQueue_12)); }
	inline Queue_1_t2309151397 * get_ActionQueue_12() const { return ___ActionQueue_12; }
	inline Queue_1_t2309151397 ** get_address_of_ActionQueue_12() { return &___ActionQueue_12; }
	inline void set_ActionQueue_12(Queue_1_t2309151397 * value)
	{
		___ActionQueue_12 = value;
		Il2CppCodeGenWriteBarrier((&___ActionQueue_12), value);
	}

	inline static int32_t get_offset_of_peerID_13() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___peerID_13)); }
	inline int16_t get_peerID_13() const { return ___peerID_13; }
	inline int16_t* get_address_of_peerID_13() { return &___peerID_13; }
	inline void set_peerID_13(int16_t value)
	{
		___peerID_13 = value;
	}

	inline static int32_t get_offset_of_peerConnectionState_14() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___peerConnectionState_14)); }
	inline uint8_t get_peerConnectionState_14() const { return ___peerConnectionState_14; }
	inline uint8_t* get_address_of_peerConnectionState_14() { return &___peerConnectionState_14; }
	inline void set_peerConnectionState_14(uint8_t value)
	{
		___peerConnectionState_14 = value;
	}

	inline static int32_t get_offset_of_serverTimeOffset_15() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___serverTimeOffset_15)); }
	inline int32_t get_serverTimeOffset_15() const { return ___serverTimeOffset_15; }
	inline int32_t* get_address_of_serverTimeOffset_15() { return &___serverTimeOffset_15; }
	inline void set_serverTimeOffset_15(int32_t value)
	{
		___serverTimeOffset_15 = value;
	}

	inline static int32_t get_offset_of_serverTimeOffsetIsAvailable_16() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___serverTimeOffsetIsAvailable_16)); }
	inline bool get_serverTimeOffsetIsAvailable_16() const { return ___serverTimeOffsetIsAvailable_16; }
	inline bool* get_address_of_serverTimeOffsetIsAvailable_16() { return &___serverTimeOffsetIsAvailable_16; }
	inline void set_serverTimeOffsetIsAvailable_16(bool value)
	{
		___serverTimeOffsetIsAvailable_16 = value;
	}

	inline static int32_t get_offset_of_roundTripTime_17() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___roundTripTime_17)); }
	inline int32_t get_roundTripTime_17() const { return ___roundTripTime_17; }
	inline int32_t* get_address_of_roundTripTime_17() { return &___roundTripTime_17; }
	inline void set_roundTripTime_17(int32_t value)
	{
		___roundTripTime_17 = value;
	}

	inline static int32_t get_offset_of_roundTripTimeVariance_18() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___roundTripTimeVariance_18)); }
	inline int32_t get_roundTripTimeVariance_18() const { return ___roundTripTimeVariance_18; }
	inline int32_t* get_address_of_roundTripTimeVariance_18() { return &___roundTripTimeVariance_18; }
	inline void set_roundTripTimeVariance_18(int32_t value)
	{
		___roundTripTimeVariance_18 = value;
	}

	inline static int32_t get_offset_of_lastRoundTripTime_19() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lastRoundTripTime_19)); }
	inline int32_t get_lastRoundTripTime_19() const { return ___lastRoundTripTime_19; }
	inline int32_t* get_address_of_lastRoundTripTime_19() { return &___lastRoundTripTime_19; }
	inline void set_lastRoundTripTime_19(int32_t value)
	{
		___lastRoundTripTime_19 = value;
	}

	inline static int32_t get_offset_of_lowestRoundTripTime_20() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lowestRoundTripTime_20)); }
	inline int32_t get_lowestRoundTripTime_20() const { return ___lowestRoundTripTime_20; }
	inline int32_t* get_address_of_lowestRoundTripTime_20() { return &___lowestRoundTripTime_20; }
	inline void set_lowestRoundTripTime_20(int32_t value)
	{
		___lowestRoundTripTime_20 = value;
	}

	inline static int32_t get_offset_of_lastRoundTripTimeVariance_21() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lastRoundTripTimeVariance_21)); }
	inline int32_t get_lastRoundTripTimeVariance_21() const { return ___lastRoundTripTimeVariance_21; }
	inline int32_t* get_address_of_lastRoundTripTimeVariance_21() { return &___lastRoundTripTimeVariance_21; }
	inline void set_lastRoundTripTimeVariance_21(int32_t value)
	{
		___lastRoundTripTimeVariance_21 = value;
	}

	inline static int32_t get_offset_of_highestRoundTripTimeVariance_22() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___highestRoundTripTimeVariance_22)); }
	inline int32_t get_highestRoundTripTimeVariance_22() const { return ___highestRoundTripTimeVariance_22; }
	inline int32_t* get_address_of_highestRoundTripTimeVariance_22() { return &___highestRoundTripTimeVariance_22; }
	inline void set_highestRoundTripTimeVariance_22(int32_t value)
	{
		___highestRoundTripTimeVariance_22 = value;
	}

	inline static int32_t get_offset_of_timestampOfLastReceive_23() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timestampOfLastReceive_23)); }
	inline int32_t get_timestampOfLastReceive_23() const { return ___timestampOfLastReceive_23; }
	inline int32_t* get_address_of_timestampOfLastReceive_23() { return &___timestampOfLastReceive_23; }
	inline void set_timestampOfLastReceive_23(int32_t value)
	{
		___timestampOfLastReceive_23 = value;
	}

	inline static int32_t get_offset_of_packetThrottleInterval_24() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___packetThrottleInterval_24)); }
	inline int32_t get_packetThrottleInterval_24() const { return ___packetThrottleInterval_24; }
	inline int32_t* get_address_of_packetThrottleInterval_24() { return &___packetThrottleInterval_24; }
	inline void set_packetThrottleInterval_24(int32_t value)
	{
		___packetThrottleInterval_24 = value;
	}

	inline static int32_t get_offset_of_bytesOut_26() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___bytesOut_26)); }
	inline int64_t get_bytesOut_26() const { return ___bytesOut_26; }
	inline int64_t* get_address_of_bytesOut_26() { return &___bytesOut_26; }
	inline void set_bytesOut_26(int64_t value)
	{
		___bytesOut_26 = value;
	}

	inline static int32_t get_offset_of_bytesIn_27() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___bytesIn_27)); }
	inline int64_t get_bytesIn_27() const { return ___bytesIn_27; }
	inline int64_t* get_address_of_bytesIn_27() { return &___bytesIn_27; }
	inline void set_bytesIn_27(int64_t value)
	{
		___bytesIn_27 = value;
	}

	inline static int32_t get_offset_of_commandBufferSize_28() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___commandBufferSize_28)); }
	inline int32_t get_commandBufferSize_28() const { return ___commandBufferSize_28; }
	inline int32_t* get_address_of_commandBufferSize_28() { return &___commandBufferSize_28; }
	inline void set_commandBufferSize_28(int32_t value)
	{
		___commandBufferSize_28 = value;
	}

	inline static int32_t get_offset_of_CryptoProvider_29() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CryptoProvider_29)); }
	inline RuntimeObject* get_CryptoProvider_29() const { return ___CryptoProvider_29; }
	inline RuntimeObject** get_address_of_CryptoProvider_29() { return &___CryptoProvider_29; }
	inline void set_CryptoProvider_29(RuntimeObject* value)
	{
		___CryptoProvider_29 = value;
		Il2CppCodeGenWriteBarrier((&___CryptoProvider_29), value);
	}

	inline static int32_t get_offset_of_lagRandomizer_30() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___lagRandomizer_30)); }
	inline Random_t108471755 * get_lagRandomizer_30() const { return ___lagRandomizer_30; }
	inline Random_t108471755 ** get_address_of_lagRandomizer_30() { return &___lagRandomizer_30; }
	inline void set_lagRandomizer_30(Random_t108471755 * value)
	{
		___lagRandomizer_30 = value;
		Il2CppCodeGenWriteBarrier((&___lagRandomizer_30), value);
	}

	inline static int32_t get_offset_of_NetSimListOutgoing_31() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___NetSimListOutgoing_31)); }
	inline LinkedList_1_t1884284488 * get_NetSimListOutgoing_31() const { return ___NetSimListOutgoing_31; }
	inline LinkedList_1_t1884284488 ** get_address_of_NetSimListOutgoing_31() { return &___NetSimListOutgoing_31; }
	inline void set_NetSimListOutgoing_31(LinkedList_1_t1884284488 * value)
	{
		___NetSimListOutgoing_31 = value;
		Il2CppCodeGenWriteBarrier((&___NetSimListOutgoing_31), value);
	}

	inline static int32_t get_offset_of_NetSimListIncoming_32() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___NetSimListIncoming_32)); }
	inline LinkedList_1_t1884284488 * get_NetSimListIncoming_32() const { return ___NetSimListIncoming_32; }
	inline LinkedList_1_t1884284488 ** get_address_of_NetSimListIncoming_32() { return &___NetSimListIncoming_32; }
	inline void set_NetSimListIncoming_32(LinkedList_1_t1884284488 * value)
	{
		___NetSimListIncoming_32 = value;
		Il2CppCodeGenWriteBarrier((&___NetSimListIncoming_32), value);
	}

	inline static int32_t get_offset_of_networkSimulationSettings_33() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___networkSimulationSettings_33)); }
	inline NetworkSimulationSet_t2000596048 * get_networkSimulationSettings_33() const { return ___networkSimulationSettings_33; }
	inline NetworkSimulationSet_t2000596048 ** get_address_of_networkSimulationSettings_33() { return &___networkSimulationSettings_33; }
	inline void set_networkSimulationSettings_33(NetworkSimulationSet_t2000596048 * value)
	{
		___networkSimulationSettings_33 = value;
		Il2CppCodeGenWriteBarrier((&___networkSimulationSettings_33), value);
	}

	inline static int32_t get_offset_of_CommandLog_34() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CommandLog_34)); }
	inline Queue_1_t4063950034 * get_CommandLog_34() const { return ___CommandLog_34; }
	inline Queue_1_t4063950034 ** get_address_of_CommandLog_34() { return &___CommandLog_34; }
	inline void set_CommandLog_34(Queue_1_t4063950034 * value)
	{
		___CommandLog_34 = value;
		Il2CppCodeGenWriteBarrier((&___CommandLog_34), value);
	}

	inline static int32_t get_offset_of_InReliableLog_35() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___InReliableLog_35)); }
	inline Queue_1_t4063950034 * get_InReliableLog_35() const { return ___InReliableLog_35; }
	inline Queue_1_t4063950034 ** get_address_of_InReliableLog_35() { return &___InReliableLog_35; }
	inline void set_InReliableLog_35(Queue_1_t4063950034 * value)
	{
		___InReliableLog_35 = value;
		Il2CppCodeGenWriteBarrier((&___InReliableLog_35), value);
	}

	inline static int32_t get_offset_of_CustomInitData_36() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___CustomInitData_36)); }
	inline RuntimeObject * get_CustomInitData_36() const { return ___CustomInitData_36; }
	inline RuntimeObject ** get_address_of_CustomInitData_36() { return &___CustomInitData_36; }
	inline void set_CustomInitData_36(RuntimeObject * value)
	{
		___CustomInitData_36 = value;
		Il2CppCodeGenWriteBarrier((&___CustomInitData_36), value);
	}

	inline static int32_t get_offset_of_AppId_37() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___AppId_37)); }
	inline String_t* get_AppId_37() const { return ___AppId_37; }
	inline String_t** get_address_of_AppId_37() { return &___AppId_37; }
	inline void set_AppId_37(String_t* value)
	{
		___AppId_37 = value;
		Il2CppCodeGenWriteBarrier((&___AppId_37), value);
	}

	inline static int32_t get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___U3CTcpConnectionPrefixU3Ek__BackingField_38)); }
	inline ByteU5BU5D_t4116647657* get_U3CTcpConnectionPrefixU3Ek__BackingField_38() const { return ___U3CTcpConnectionPrefixU3Ek__BackingField_38; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CTcpConnectionPrefixU3Ek__BackingField_38() { return &___U3CTcpConnectionPrefixU3Ek__BackingField_38; }
	inline void set_U3CTcpConnectionPrefixU3Ek__BackingField_38(ByteU5BU5D_t4116647657* value)
	{
		___U3CTcpConnectionPrefixU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTcpConnectionPrefixU3Ek__BackingField_38), value);
	}

	inline static int32_t get_offset_of_timeBase_39() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeBase_39)); }
	inline int32_t get_timeBase_39() const { return ___timeBase_39; }
	inline int32_t* get_address_of_timeBase_39() { return &___timeBase_39; }
	inline void set_timeBase_39(int32_t value)
	{
		___timeBase_39 = value;
	}

	inline static int32_t get_offset_of_timeInt_40() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeInt_40)); }
	inline int32_t get_timeInt_40() const { return ___timeInt_40; }
	inline int32_t* get_address_of_timeInt_40() { return &___timeInt_40; }
	inline void set_timeInt_40(int32_t value)
	{
		___timeInt_40 = value;
	}

	inline static int32_t get_offset_of_timeoutInt_41() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeoutInt_41)); }
	inline int32_t get_timeoutInt_41() const { return ___timeoutInt_41; }
	inline int32_t* get_address_of_timeoutInt_41() { return &___timeoutInt_41; }
	inline void set_timeoutInt_41(int32_t value)
	{
		___timeoutInt_41 = value;
	}

	inline static int32_t get_offset_of_timeLastAckReceive_42() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeLastAckReceive_42)); }
	inline int32_t get_timeLastAckReceive_42() const { return ___timeLastAckReceive_42; }
	inline int32_t* get_address_of_timeLastAckReceive_42() { return &___timeLastAckReceive_42; }
	inline void set_timeLastAckReceive_42(int32_t value)
	{
		___timeLastAckReceive_42 = value;
	}

	inline static int32_t get_offset_of_timeLastSendAck_43() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeLastSendAck_43)); }
	inline int32_t get_timeLastSendAck_43() const { return ___timeLastSendAck_43; }
	inline int32_t* get_address_of_timeLastSendAck_43() { return &___timeLastSendAck_43; }
	inline void set_timeLastSendAck_43(int32_t value)
	{
		___timeLastSendAck_43 = value;
	}

	inline static int32_t get_offset_of_timeLastSendOutgoing_44() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___timeLastSendOutgoing_44)); }
	inline int32_t get_timeLastSendOutgoing_44() const { return ___timeLastSendOutgoing_44; }
	inline int32_t* get_address_of_timeLastSendOutgoing_44() { return &___timeLastSendOutgoing_44; }
	inline void set_timeLastSendOutgoing_44(int32_t value)
	{
		___timeLastSendOutgoing_44 = value;
	}

	inline static int32_t get_offset_of_ApplicationIsInitialized_48() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___ApplicationIsInitialized_48)); }
	inline bool get_ApplicationIsInitialized_48() const { return ___ApplicationIsInitialized_48; }
	inline bool* get_address_of_ApplicationIsInitialized_48() { return &___ApplicationIsInitialized_48; }
	inline void set_ApplicationIsInitialized_48(bool value)
	{
		___ApplicationIsInitialized_48 = value;
	}

	inline static int32_t get_offset_of_isEncryptionAvailable_49() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___isEncryptionAvailable_49)); }
	inline bool get_isEncryptionAvailable_49() const { return ___isEncryptionAvailable_49; }
	inline bool* get_address_of_isEncryptionAvailable_49() { return &___isEncryptionAvailable_49; }
	inline void set_isEncryptionAvailable_49(bool value)
	{
		___isEncryptionAvailable_49 = value;
	}

	inline static int32_t get_offset_of_outgoingCommandsInStream_50() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___outgoingCommandsInStream_50)); }
	inline int32_t get_outgoingCommandsInStream_50() const { return ___outgoingCommandsInStream_50; }
	inline int32_t* get_address_of_outgoingCommandsInStream_50() { return &___outgoingCommandsInStream_50; }
	inline void set_outgoingCommandsInStream_50(int32_t value)
	{
		___outgoingCommandsInStream_50 = value;
	}

	inline static int32_t get_offset_of_SerializeMemStream_51() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011, ___SerializeMemStream_51)); }
	inline StreamBuffer_t3827669789 * get_SerializeMemStream_51() const { return ___SerializeMemStream_51; }
	inline StreamBuffer_t3827669789 ** get_address_of_SerializeMemStream_51() { return &___SerializeMemStream_51; }
	inline void set_SerializeMemStream_51(StreamBuffer_t3827669789 * value)
	{
		___SerializeMemStream_51 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeMemStream_51), value);
	}
};

struct PeerBase_t2956237011_StaticFields
{
public:
	// System.Int16 ExitGames.Client.Photon.PeerBase::peerCount
	int16_t ___peerCount_25;

public:
	inline static int32_t get_offset_of_peerCount_25() { return static_cast<int32_t>(offsetof(PeerBase_t2956237011_StaticFields, ___peerCount_25)); }
	inline int16_t get_peerCount_25() const { return ___peerCount_25; }
	inline int16_t* get_address_of_peerCount_25() { return &___peerCount_25; }
	inline void set_peerCount_25(int16_t value)
	{
		___peerCount_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERBASE_T2956237011_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_6;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_7;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_8;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_9;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_10;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_11;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_12;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_13;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_14;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::commandLogSize
	int32_t ___commandLogSize_15;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_16;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_17;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_21;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_22;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_23;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_24;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_25;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_26;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_27;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_29;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_30;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_31;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_34;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_35;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::encryptor
	Encryptor_t200327285 * ___encryptor_36;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::decryptor
	Decryptor_t2116099858 * ___decryptor_37;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_5), value);
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_6)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_6() const { return ___SocketImplementationConfig_6; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_6() { return &___SocketImplementationConfig_6; }
	inline void set_SocketImplementationConfig_6(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_6), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_7)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_7() const { return ___U3CSocketImplementationU3Ek__BackingField_7; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_7() { return &___U3CSocketImplementationU3Ek__BackingField_7; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_7(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_DebugOut_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_8)); }
	inline uint8_t get_DebugOut_8() const { return ___DebugOut_8; }
	inline uint8_t* get_address_of_DebugOut_8() { return &___DebugOut_8; }
	inline void set_DebugOut_8(uint8_t value)
	{
		___DebugOut_8 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_9() const { return ___U3CListenerU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_9() { return &___U3CListenerU3Ek__BackingField_9; }
	inline void set_U3CListenerU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_10)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_10() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_10; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_10() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_10; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_10(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_11)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_11() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_11; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_11() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_11; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_11(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_12)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_12() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_12; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_12() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_12; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_12(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_13)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_13() const { return ___trafficStatsStopwatch_13; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_13() { return &___trafficStatsStopwatch_13; }
	inline void set_trafficStatsStopwatch_13(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_13 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_13), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_14)); }
	inline bool get_trafficStatsEnabled_14() const { return ___trafficStatsEnabled_14; }
	inline bool* get_address_of_trafficStatsEnabled_14() { return &___trafficStatsEnabled_14; }
	inline void set_trafficStatsEnabled_14(bool value)
	{
		___trafficStatsEnabled_14 = value;
	}

	inline static int32_t get_offset_of_commandLogSize_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___commandLogSize_15)); }
	inline int32_t get_commandLogSize_15() const { return ___commandLogSize_15; }
	inline int32_t* get_address_of_commandLogSize_15() { return &___commandLogSize_15; }
	inline void set_commandLogSize_15(int32_t value)
	{
		___commandLogSize_15 = value;
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_16)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_16() const { return ___U3CEnableServerTracingU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_16() { return &___U3CEnableServerTracingU3Ek__BackingField_16; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_16(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_17)); }
	inline uint8_t get_quickResendAttempts_17() const { return ___quickResendAttempts_17; }
	inline uint8_t* get_address_of_quickResendAttempts_17() { return &___quickResendAttempts_17; }
	inline void set_quickResendAttempts_17(uint8_t value)
	{
		___quickResendAttempts_17 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_18)); }
	inline int32_t get_RhttpMinConnections_18() const { return ___RhttpMinConnections_18; }
	inline int32_t* get_address_of_RhttpMinConnections_18() { return &___RhttpMinConnections_18; }
	inline void set_RhttpMinConnections_18(int32_t value)
	{
		___RhttpMinConnections_18 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_19)); }
	inline int32_t get_RhttpMaxConnections_19() const { return ___RhttpMaxConnections_19; }
	inline int32_t* get_address_of_RhttpMaxConnections_19() { return &___RhttpMaxConnections_19; }
	inline void set_RhttpMaxConnections_19(int32_t value)
	{
		___RhttpMaxConnections_19 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_21)); }
	inline uint8_t get_ChannelCount_21() const { return ___ChannelCount_21; }
	inline uint8_t* get_address_of_ChannelCount_21() { return &___ChannelCount_21; }
	inline void set_ChannelCount_21(uint8_t value)
	{
		___ChannelCount_21 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_22)); }
	inline bool get_crcEnabled_22() const { return ___crcEnabled_22; }
	inline bool* get_address_of_crcEnabled_22() { return &___crcEnabled_22; }
	inline void set_crcEnabled_22(bool value)
	{
		___crcEnabled_22 = value;
	}

	inline static int32_t get_offset_of_WarningSize_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_23)); }
	inline int32_t get_WarningSize_23() const { return ___WarningSize_23; }
	inline int32_t* get_address_of_WarningSize_23() { return &___WarningSize_23; }
	inline void set_WarningSize_23(int32_t value)
	{
		___WarningSize_23 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_24)); }
	inline int32_t get_SentCountAllowance_24() const { return ___SentCountAllowance_24; }
	inline int32_t* get_address_of_SentCountAllowance_24() { return &___SentCountAllowance_24; }
	inline void set_SentCountAllowance_24(int32_t value)
	{
		___SentCountAllowance_24 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_25)); }
	inline int32_t get_TimePingInterval_25() const { return ___TimePingInterval_25; }
	inline int32_t* get_address_of_TimePingInterval_25() { return &___TimePingInterval_25; }
	inline void set_TimePingInterval_25(int32_t value)
	{
		___TimePingInterval_25 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_26)); }
	inline int32_t get_DisconnectTimeout_26() const { return ___DisconnectTimeout_26; }
	inline int32_t* get_address_of_DisconnectTimeout_26() { return &___DisconnectTimeout_26; }
	inline void set_DisconnectTimeout_26(int32_t value)
	{
		___DisconnectTimeout_26 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_27)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_27() const { return ___U3CTransportProtocolU3Ek__BackingField_27; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_27() { return &___U3CTransportProtocolU3Ek__BackingField_27; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_27(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_mtu_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_29)); }
	inline int32_t get_mtu_29() const { return ___mtu_29; }
	inline int32_t* get_address_of_mtu_29() { return &___mtu_29; }
	inline void set_mtu_29(int32_t value)
	{
		___mtu_29 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_30)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_30() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_30() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_30; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_30(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_peerBase_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_31)); }
	inline PeerBase_t2956237011 * get_peerBase_31() const { return ___peerBase_31; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_31() { return &___peerBase_31; }
	inline void set_peerBase_31(PeerBase_t2956237011 * value)
	{
		___peerBase_31 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_31), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_32)); }
	inline RuntimeObject * get_SendOutgoingLockObject_32() const { return ___SendOutgoingLockObject_32; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_32() { return &___SendOutgoingLockObject_32; }
	inline void set_SendOutgoingLockObject_32(RuntimeObject * value)
	{
		___SendOutgoingLockObject_32 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_32), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_33)); }
	inline RuntimeObject * get_DispatchLockObject_33() const { return ___DispatchLockObject_33; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_33() { return &___DispatchLockObject_33; }
	inline void set_DispatchLockObject_33(RuntimeObject * value)
	{
		___DispatchLockObject_33 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_33), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_34)); }
	inline RuntimeObject * get_EnqueueLock_34() const { return ___EnqueueLock_34; }
	inline RuntimeObject ** get_address_of_EnqueueLock_34() { return &___EnqueueLock_34; }
	inline void set_EnqueueLock_34(RuntimeObject * value)
	{
		___EnqueueLock_34 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_34), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_35)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_35() const { return ___PayloadEncryptionSecret_35; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_35() { return &___PayloadEncryptionSecret_35; }
	inline void set_PayloadEncryptionSecret_35(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_35 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_35), value);
	}

	inline static int32_t get_offset_of_encryptor_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___encryptor_36)); }
	inline Encryptor_t200327285 * get_encryptor_36() const { return ___encryptor_36; }
	inline Encryptor_t200327285 ** get_address_of_encryptor_36() { return &___encryptor_36; }
	inline void set_encryptor_36(Encryptor_t200327285 * value)
	{
		___encryptor_36 = value;
		Il2CppCodeGenWriteBarrier((&___encryptor_36), value);
	}

	inline static int32_t get_offset_of_decryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___decryptor_37)); }
	inline Decryptor_t2116099858 * get_decryptor_37() const { return ___decryptor_37; }
	inline Decryptor_t2116099858 ** get_address_of_decryptor_37() { return &___decryptor_37; }
	inline void set_decryptor_37(Decryptor_t2116099858 * value)
	{
		___decryptor_37 = value;
		Il2CppCodeGenWriteBarrier((&___decryptor_37), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_28;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_28)); }
	inline int32_t get_OutgoingStreamBufferSize_28() const { return ___OutgoingStreamBufferSize_28; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_28() { return &___OutgoingStreamBufferSize_28; }
	inline void set_OutgoingStreamBufferSize_28(int32_t value)
	{
		___OutgoingStreamBufferSize_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef MYACTION_T2462891903_H
#define MYACTION_T2462891903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase/MyAction
struct  MyAction_t2462891903  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYACTION_T2462891903_H
#ifndef WILLRENDERCANVASES_T3309123499_H
#define WILLRENDERCANVASES_T3309123499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas/WillRenderCanvases
struct  WillRenderCanvases_t3309123499  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_T3309123499_H
#ifndef DESERIALIZESTREAMMETHOD_T3070531629_H
#define DESERIALIZESTREAMMETHOD_T3070531629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DeserializeStreamMethod
struct  DeserializeStreamMethod_t3070531629  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESERIALIZESTREAMMETHOD_T3070531629_H
#ifndef DESERIALIZEMETHOD_T3915517082_H
#define DESERIALIZEMETHOD_T3915517082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DeserializeMethod
struct  DeserializeMethod_t3915517082  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESERIALIZEMETHOD_T3915517082_H
#ifndef SERIALIZESTREAMMETHOD_T2169445464_H
#define SERIALIZESTREAMMETHOD_T2169445464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializeStreamMethod
struct  SerializeStreamMethod_t2169445464  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZESTREAMMETHOD_T2169445464_H
#ifndef SERIALIZEMETHOD_T1264674278_H
#define SERIALIZEMETHOD_T1264674278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializeMethod
struct  SerializeMethod_t1264674278  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEMETHOD_T1264674278_H
#ifndef ENETPEER_T430442630_H
#define ENETPEER_T430442630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.EnetPeer
struct  EnetPeer_t430442630  : public PeerBase_t2956237011
{
public:
	// System.Collections.Generic.List`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetPeer::sentReliableCommands
	List_1_t2702763141 * ___sentReliableCommands_55;
	// ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.EnetPeer::outgoingAcknowledgementsPool
	StreamBuffer_t3827669789 * ___outgoingAcknowledgementsPool_56;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::windowSize
	int32_t ___windowSize_57;
	// System.Byte ExitGames.Client.Photon.EnetPeer::udpCommandCount
	uint8_t ___udpCommandCount_58;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::udpBuffer
	ByteU5BU5D_t4116647657* ___udpBuffer_59;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::udpBufferIndex
	int32_t ___udpBufferIndex_60;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::udpBufferLength
	int32_t ___udpBufferLength_61;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::bufferForEncryption
	ByteU5BU5D_t4116647657* ___bufferForEncryption_62;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::challenge
	int32_t ___challenge_63;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::reliableCommandsRepeated
	int32_t ___reliableCommandsRepeated_64;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::reliableCommandsSent
	int32_t ___reliableCommandsSent_65;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::serverSentTime
	int32_t ___serverSentTime_66;
	// System.Boolean ExitGames.Client.Photon.EnetPeer::datagramEncryptedConnection
	bool ___datagramEncryptedConnection_69;
	// ExitGames.Client.Photon.EnetChannel[] ExitGames.Client.Photon.EnetPeer::channelArray
	EnetChannelU5BU5D_t2709601441* ___channelArray_70;
	// System.Collections.Generic.Queue`1<System.Int32> ExitGames.Client.Photon.EnetPeer::commandsToRemove
	Queue_1_t2797205247 * ___commandsToRemove_71;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand> ExitGames.Client.Photon.EnetPeer::commandsToResend
	Queue_1_t1076947893 * ___commandsToResend_72;

public:
	inline static int32_t get_offset_of_sentReliableCommands_55() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___sentReliableCommands_55)); }
	inline List_1_t2702763141 * get_sentReliableCommands_55() const { return ___sentReliableCommands_55; }
	inline List_1_t2702763141 ** get_address_of_sentReliableCommands_55() { return &___sentReliableCommands_55; }
	inline void set_sentReliableCommands_55(List_1_t2702763141 * value)
	{
		___sentReliableCommands_55 = value;
		Il2CppCodeGenWriteBarrier((&___sentReliableCommands_55), value);
	}

	inline static int32_t get_offset_of_outgoingAcknowledgementsPool_56() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___outgoingAcknowledgementsPool_56)); }
	inline StreamBuffer_t3827669789 * get_outgoingAcknowledgementsPool_56() const { return ___outgoingAcknowledgementsPool_56; }
	inline StreamBuffer_t3827669789 ** get_address_of_outgoingAcknowledgementsPool_56() { return &___outgoingAcknowledgementsPool_56; }
	inline void set_outgoingAcknowledgementsPool_56(StreamBuffer_t3827669789 * value)
	{
		___outgoingAcknowledgementsPool_56 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingAcknowledgementsPool_56), value);
	}

	inline static int32_t get_offset_of_windowSize_57() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___windowSize_57)); }
	inline int32_t get_windowSize_57() const { return ___windowSize_57; }
	inline int32_t* get_address_of_windowSize_57() { return &___windowSize_57; }
	inline void set_windowSize_57(int32_t value)
	{
		___windowSize_57 = value;
	}

	inline static int32_t get_offset_of_udpCommandCount_58() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpCommandCount_58)); }
	inline uint8_t get_udpCommandCount_58() const { return ___udpCommandCount_58; }
	inline uint8_t* get_address_of_udpCommandCount_58() { return &___udpCommandCount_58; }
	inline void set_udpCommandCount_58(uint8_t value)
	{
		___udpCommandCount_58 = value;
	}

	inline static int32_t get_offset_of_udpBuffer_59() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpBuffer_59)); }
	inline ByteU5BU5D_t4116647657* get_udpBuffer_59() const { return ___udpBuffer_59; }
	inline ByteU5BU5D_t4116647657** get_address_of_udpBuffer_59() { return &___udpBuffer_59; }
	inline void set_udpBuffer_59(ByteU5BU5D_t4116647657* value)
	{
		___udpBuffer_59 = value;
		Il2CppCodeGenWriteBarrier((&___udpBuffer_59), value);
	}

	inline static int32_t get_offset_of_udpBufferIndex_60() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpBufferIndex_60)); }
	inline int32_t get_udpBufferIndex_60() const { return ___udpBufferIndex_60; }
	inline int32_t* get_address_of_udpBufferIndex_60() { return &___udpBufferIndex_60; }
	inline void set_udpBufferIndex_60(int32_t value)
	{
		___udpBufferIndex_60 = value;
	}

	inline static int32_t get_offset_of_udpBufferLength_61() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___udpBufferLength_61)); }
	inline int32_t get_udpBufferLength_61() const { return ___udpBufferLength_61; }
	inline int32_t* get_address_of_udpBufferLength_61() { return &___udpBufferLength_61; }
	inline void set_udpBufferLength_61(int32_t value)
	{
		___udpBufferLength_61 = value;
	}

	inline static int32_t get_offset_of_bufferForEncryption_62() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___bufferForEncryption_62)); }
	inline ByteU5BU5D_t4116647657* get_bufferForEncryption_62() const { return ___bufferForEncryption_62; }
	inline ByteU5BU5D_t4116647657** get_address_of_bufferForEncryption_62() { return &___bufferForEncryption_62; }
	inline void set_bufferForEncryption_62(ByteU5BU5D_t4116647657* value)
	{
		___bufferForEncryption_62 = value;
		Il2CppCodeGenWriteBarrier((&___bufferForEncryption_62), value);
	}

	inline static int32_t get_offset_of_challenge_63() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___challenge_63)); }
	inline int32_t get_challenge_63() const { return ___challenge_63; }
	inline int32_t* get_address_of_challenge_63() { return &___challenge_63; }
	inline void set_challenge_63(int32_t value)
	{
		___challenge_63 = value;
	}

	inline static int32_t get_offset_of_reliableCommandsRepeated_64() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___reliableCommandsRepeated_64)); }
	inline int32_t get_reliableCommandsRepeated_64() const { return ___reliableCommandsRepeated_64; }
	inline int32_t* get_address_of_reliableCommandsRepeated_64() { return &___reliableCommandsRepeated_64; }
	inline void set_reliableCommandsRepeated_64(int32_t value)
	{
		___reliableCommandsRepeated_64 = value;
	}

	inline static int32_t get_offset_of_reliableCommandsSent_65() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___reliableCommandsSent_65)); }
	inline int32_t get_reliableCommandsSent_65() const { return ___reliableCommandsSent_65; }
	inline int32_t* get_address_of_reliableCommandsSent_65() { return &___reliableCommandsSent_65; }
	inline void set_reliableCommandsSent_65(int32_t value)
	{
		___reliableCommandsSent_65 = value;
	}

	inline static int32_t get_offset_of_serverSentTime_66() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___serverSentTime_66)); }
	inline int32_t get_serverSentTime_66() const { return ___serverSentTime_66; }
	inline int32_t* get_address_of_serverSentTime_66() { return &___serverSentTime_66; }
	inline void set_serverSentTime_66(int32_t value)
	{
		___serverSentTime_66 = value;
	}

	inline static int32_t get_offset_of_datagramEncryptedConnection_69() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___datagramEncryptedConnection_69)); }
	inline bool get_datagramEncryptedConnection_69() const { return ___datagramEncryptedConnection_69; }
	inline bool* get_address_of_datagramEncryptedConnection_69() { return &___datagramEncryptedConnection_69; }
	inline void set_datagramEncryptedConnection_69(bool value)
	{
		___datagramEncryptedConnection_69 = value;
	}

	inline static int32_t get_offset_of_channelArray_70() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___channelArray_70)); }
	inline EnetChannelU5BU5D_t2709601441* get_channelArray_70() const { return ___channelArray_70; }
	inline EnetChannelU5BU5D_t2709601441** get_address_of_channelArray_70() { return &___channelArray_70; }
	inline void set_channelArray_70(EnetChannelU5BU5D_t2709601441* value)
	{
		___channelArray_70 = value;
		Il2CppCodeGenWriteBarrier((&___channelArray_70), value);
	}

	inline static int32_t get_offset_of_commandsToRemove_71() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___commandsToRemove_71)); }
	inline Queue_1_t2797205247 * get_commandsToRemove_71() const { return ___commandsToRemove_71; }
	inline Queue_1_t2797205247 ** get_address_of_commandsToRemove_71() { return &___commandsToRemove_71; }
	inline void set_commandsToRemove_71(Queue_1_t2797205247 * value)
	{
		___commandsToRemove_71 = value;
		Il2CppCodeGenWriteBarrier((&___commandsToRemove_71), value);
	}

	inline static int32_t get_offset_of_commandsToResend_72() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630, ___commandsToResend_72)); }
	inline Queue_1_t1076947893 * get_commandsToResend_72() const { return ___commandsToResend_72; }
	inline Queue_1_t1076947893 ** get_address_of_commandsToResend_72() { return &___commandsToResend_72; }
	inline void set_commandsToResend_72(Queue_1_t1076947893 * value)
	{
		___commandsToResend_72 = value;
		Il2CppCodeGenWriteBarrier((&___commandsToResend_72), value);
	}
};

struct EnetPeer_t430442630_StaticFields
{
public:
	// System.Int32 ExitGames.Client.Photon.EnetPeer::HMAC_SIZE
	int32_t ___HMAC_SIZE_52;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::BLOCK_SIZE
	int32_t ___BLOCK_SIZE_53;
	// System.Int32 ExitGames.Client.Photon.EnetPeer::IV_SIZE
	int32_t ___IV_SIZE_54;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::udpHeader0xF3
	ByteU5BU5D_t4116647657* ___udpHeader0xF3_67;
	// System.Byte[] ExitGames.Client.Photon.EnetPeer::messageHeader
	ByteU5BU5D_t4116647657* ___messageHeader_68;

public:
	inline static int32_t get_offset_of_HMAC_SIZE_52() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___HMAC_SIZE_52)); }
	inline int32_t get_HMAC_SIZE_52() const { return ___HMAC_SIZE_52; }
	inline int32_t* get_address_of_HMAC_SIZE_52() { return &___HMAC_SIZE_52; }
	inline void set_HMAC_SIZE_52(int32_t value)
	{
		___HMAC_SIZE_52 = value;
	}

	inline static int32_t get_offset_of_BLOCK_SIZE_53() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___BLOCK_SIZE_53)); }
	inline int32_t get_BLOCK_SIZE_53() const { return ___BLOCK_SIZE_53; }
	inline int32_t* get_address_of_BLOCK_SIZE_53() { return &___BLOCK_SIZE_53; }
	inline void set_BLOCK_SIZE_53(int32_t value)
	{
		___BLOCK_SIZE_53 = value;
	}

	inline static int32_t get_offset_of_IV_SIZE_54() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___IV_SIZE_54)); }
	inline int32_t get_IV_SIZE_54() const { return ___IV_SIZE_54; }
	inline int32_t* get_address_of_IV_SIZE_54() { return &___IV_SIZE_54; }
	inline void set_IV_SIZE_54(int32_t value)
	{
		___IV_SIZE_54 = value;
	}

	inline static int32_t get_offset_of_udpHeader0xF3_67() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___udpHeader0xF3_67)); }
	inline ByteU5BU5D_t4116647657* get_udpHeader0xF3_67() const { return ___udpHeader0xF3_67; }
	inline ByteU5BU5D_t4116647657** get_address_of_udpHeader0xF3_67() { return &___udpHeader0xF3_67; }
	inline void set_udpHeader0xF3_67(ByteU5BU5D_t4116647657* value)
	{
		___udpHeader0xF3_67 = value;
		Il2CppCodeGenWriteBarrier((&___udpHeader0xF3_67), value);
	}

	inline static int32_t get_offset_of_messageHeader_68() { return static_cast<int32_t>(offsetof(EnetPeer_t430442630_StaticFields, ___messageHeader_68)); }
	inline ByteU5BU5D_t4116647657* get_messageHeader_68() const { return ___messageHeader_68; }
	inline ByteU5BU5D_t4116647657** get_address_of_messageHeader_68() { return &___messageHeader_68; }
	inline void set_messageHeader_68(ByteU5BU5D_t4116647657* value)
	{
		___messageHeader_68 = value;
		Il2CppCodeGenWriteBarrier((&___messageHeader_68), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENETPEER_T430442630_H
#ifndef TPEER_T1497954812_H
#define TPEER_T1497954812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.TPeer
struct  TPeer_t1497954812  : public PeerBase_t2956237011
{
public:
	// System.Collections.Generic.Queue`1<System.Byte[]> ExitGames.Client.Photon.TPeer::incomingList
	Queue_1_t3962907151 * ___incomingList_52;
	// System.Collections.Generic.List`1<System.Byte[]> ExitGames.Client.Photon.TPeer::outgoingStream
	List_1_t1293755103 * ___outgoingStream_53;
	// System.Int32 ExitGames.Client.Photon.TPeer::lastPingResult
	int32_t ___lastPingResult_54;
	// System.Byte[] ExitGames.Client.Photon.TPeer::pingRequest
	ByteU5BU5D_t4116647657* ___pingRequest_55;
	// System.Byte[] ExitGames.Client.Photon.TPeer::messageHeader
	ByteU5BU5D_t4116647657* ___messageHeader_58;
	// System.Boolean ExitGames.Client.Photon.TPeer::DoFraming
	bool ___DoFraming_59;

public:
	inline static int32_t get_offset_of_incomingList_52() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___incomingList_52)); }
	inline Queue_1_t3962907151 * get_incomingList_52() const { return ___incomingList_52; }
	inline Queue_1_t3962907151 ** get_address_of_incomingList_52() { return &___incomingList_52; }
	inline void set_incomingList_52(Queue_1_t3962907151 * value)
	{
		___incomingList_52 = value;
		Il2CppCodeGenWriteBarrier((&___incomingList_52), value);
	}

	inline static int32_t get_offset_of_outgoingStream_53() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___outgoingStream_53)); }
	inline List_1_t1293755103 * get_outgoingStream_53() const { return ___outgoingStream_53; }
	inline List_1_t1293755103 ** get_address_of_outgoingStream_53() { return &___outgoingStream_53; }
	inline void set_outgoingStream_53(List_1_t1293755103 * value)
	{
		___outgoingStream_53 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingStream_53), value);
	}

	inline static int32_t get_offset_of_lastPingResult_54() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___lastPingResult_54)); }
	inline int32_t get_lastPingResult_54() const { return ___lastPingResult_54; }
	inline int32_t* get_address_of_lastPingResult_54() { return &___lastPingResult_54; }
	inline void set_lastPingResult_54(int32_t value)
	{
		___lastPingResult_54 = value;
	}

	inline static int32_t get_offset_of_pingRequest_55() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___pingRequest_55)); }
	inline ByteU5BU5D_t4116647657* get_pingRequest_55() const { return ___pingRequest_55; }
	inline ByteU5BU5D_t4116647657** get_address_of_pingRequest_55() { return &___pingRequest_55; }
	inline void set_pingRequest_55(ByteU5BU5D_t4116647657* value)
	{
		___pingRequest_55 = value;
		Il2CppCodeGenWriteBarrier((&___pingRequest_55), value);
	}

	inline static int32_t get_offset_of_messageHeader_58() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___messageHeader_58)); }
	inline ByteU5BU5D_t4116647657* get_messageHeader_58() const { return ___messageHeader_58; }
	inline ByteU5BU5D_t4116647657** get_address_of_messageHeader_58() { return &___messageHeader_58; }
	inline void set_messageHeader_58(ByteU5BU5D_t4116647657* value)
	{
		___messageHeader_58 = value;
		Il2CppCodeGenWriteBarrier((&___messageHeader_58), value);
	}

	inline static int32_t get_offset_of_DoFraming_59() { return static_cast<int32_t>(offsetof(TPeer_t1497954812, ___DoFraming_59)); }
	inline bool get_DoFraming_59() const { return ___DoFraming_59; }
	inline bool* get_address_of_DoFraming_59() { return &___DoFraming_59; }
	inline void set_DoFraming_59(bool value)
	{
		___DoFraming_59 = value;
	}
};

struct TPeer_t1497954812_StaticFields
{
public:
	// System.Byte[] ExitGames.Client.Photon.TPeer::tcpFramedMessageHead
	ByteU5BU5D_t4116647657* ___tcpFramedMessageHead_56;
	// System.Byte[] ExitGames.Client.Photon.TPeer::tcpMsgHead
	ByteU5BU5D_t4116647657* ___tcpMsgHead_57;

public:
	inline static int32_t get_offset_of_tcpFramedMessageHead_56() { return static_cast<int32_t>(offsetof(TPeer_t1497954812_StaticFields, ___tcpFramedMessageHead_56)); }
	inline ByteU5BU5D_t4116647657* get_tcpFramedMessageHead_56() const { return ___tcpFramedMessageHead_56; }
	inline ByteU5BU5D_t4116647657** get_address_of_tcpFramedMessageHead_56() { return &___tcpFramedMessageHead_56; }
	inline void set_tcpFramedMessageHead_56(ByteU5BU5D_t4116647657* value)
	{
		___tcpFramedMessageHead_56 = value;
		Il2CppCodeGenWriteBarrier((&___tcpFramedMessageHead_56), value);
	}

	inline static int32_t get_offset_of_tcpMsgHead_57() { return static_cast<int32_t>(offsetof(TPeer_t1497954812_StaticFields, ___tcpMsgHead_57)); }
	inline ByteU5BU5D_t4116647657* get_tcpMsgHead_57() const { return ___tcpMsgHead_57; }
	inline ByteU5BU5D_t4116647657** get_address_of_tcpMsgHead_57() { return &___tcpMsgHead_57; }
	inline void set_tcpMsgHead_57(ByteU5BU5D_t4116647657* value)
	{
		___tcpMsgHead_57 = value;
		Il2CppCodeGenWriteBarrier((&___tcpMsgHead_57), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPEER_T1497954812_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TIMEEVENTHANDLER_T445758600_H
#define TIMEEVENTHANDLER_T445758600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct  TimeEventHandler_t445758600  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEEVENTHANDLER_T445758600_H
#ifndef SOCKETUDP_T1337106072_H
#define SOCKETUDP_T1337106072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SocketUdp
struct  SocketUdp_t1337106072  : public IPhotonSocket_t2066969247
{
public:
	// System.Net.Sockets.Socket ExitGames.Client.Photon.SocketUdp::sock
	Socket_t1119025450 * ___sock_9;
	// System.Object ExitGames.Client.Photon.SocketUdp::syncer
	RuntimeObject * ___syncer_10;

public:
	inline static int32_t get_offset_of_sock_9() { return static_cast<int32_t>(offsetof(SocketUdp_t1337106072, ___sock_9)); }
	inline Socket_t1119025450 * get_sock_9() const { return ___sock_9; }
	inline Socket_t1119025450 ** get_address_of_sock_9() { return &___sock_9; }
	inline void set_sock_9(Socket_t1119025450 * value)
	{
		___sock_9 = value;
		Il2CppCodeGenWriteBarrier((&___sock_9), value);
	}

	inline static int32_t get_offset_of_syncer_10() { return static_cast<int32_t>(offsetof(SocketUdp_t1337106072, ___syncer_10)); }
	inline RuntimeObject * get_syncer_10() const { return ___syncer_10; }
	inline RuntimeObject ** get_address_of_syncer_10() { return &___syncer_10; }
	inline void set_syncer_10(RuntimeObject * value)
	{
		___syncer_10 = value;
		Il2CppCodeGenWriteBarrier((&___syncer_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETUDP_T1337106072_H
#ifndef SOCKETTCP_T182200829_H
#define SOCKETTCP_T182200829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SocketTcp
struct  SocketTcp_t182200829  : public IPhotonSocket_t2066969247
{
public:
	// System.Net.Sockets.Socket ExitGames.Client.Photon.SocketTcp::sock
	Socket_t1119025450 * ___sock_9;
	// System.Object ExitGames.Client.Photon.SocketTcp::syncer
	RuntimeObject * ___syncer_10;

public:
	inline static int32_t get_offset_of_sock_9() { return static_cast<int32_t>(offsetof(SocketTcp_t182200829, ___sock_9)); }
	inline Socket_t1119025450 * get_sock_9() const { return ___sock_9; }
	inline Socket_t1119025450 ** get_address_of_sock_9() { return &___sock_9; }
	inline void set_sock_9(Socket_t1119025450 * value)
	{
		___sock_9 = value;
		Il2CppCodeGenWriteBarrier((&___sock_9), value);
	}

	inline static int32_t get_offset_of_syncer_10() { return static_cast<int32_t>(offsetof(SocketTcp_t182200829, ___syncer_10)); }
	inline RuntimeObject * get_syncer_10() const { return ___syncer_10; }
	inline RuntimeObject ** get_address_of_syncer_10() { return &___syncer_10; }
	inline void set_syncer_10(RuntimeObject * value)
	{
		___syncer_10 = value;
		Il2CppCodeGenWriteBarrier((&___syncer_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETTCP_T182200829_H
#ifndef ERROREVENTHANDLER_T3211687919_H
#define ERROREVENTHANDLER_T3211687919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct  ErrorEventHandler_t3211687919  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_T3211687919_H
#ifndef EVENTHANDLER_T3436254912_H
#define EVENTHANDLER_T3436254912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/EventHandler
struct  EventHandler_t3436254912  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T3436254912_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef CANVASRENDERER_T2598313366_H
#define CANVASRENDERER_T2598313366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_t2598313366  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_T2598313366_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef UPDATEDEVENTHANDLER_T1027848393_H
#define UPDATEDEVENTHANDLER_T1027848393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t1027848393  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T1027848393_H
#ifndef INTEGERMILLISECONDSDELEGATE_T651311252_H
#define INTEGERMILLISECONDSDELEGATE_T651311252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate
struct  IntegerMillisecondsDelegate_t651311252  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGERMILLISECONDSDELEGATE_T651311252_H
#ifndef FRAMEREADYEVENTHANDLER_T3848515759_H
#define FRAMEREADYEVENTHANDLER_T3848515759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct  FrameReadyEventHandler_t3848515759  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEREADYEVENTHANDLER_T3848515759_H
#ifndef VIDEOPLAYER_T1683042537_H
#define VIDEOPLAYER_T1683042537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer
struct  VideoPlayer_t1683042537  : public Behaviour_t1437897464
{
public:
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t3436254912 * ___prepareCompleted_2;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t3436254912 * ___loopPointReached_3;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t3436254912 * ___started_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t3436254912 * ___frameDropped_5;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_t3211687919 * ___errorReceived_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t3436254912 * ___seekCompleted_7;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_t445758600 * ___clockResyncOccurred_8;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t3848515759 * ___frameReady_9;

public:
	inline static int32_t get_offset_of_prepareCompleted_2() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___prepareCompleted_2)); }
	inline EventHandler_t3436254912 * get_prepareCompleted_2() const { return ___prepareCompleted_2; }
	inline EventHandler_t3436254912 ** get_address_of_prepareCompleted_2() { return &___prepareCompleted_2; }
	inline void set_prepareCompleted_2(EventHandler_t3436254912 * value)
	{
		___prepareCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___prepareCompleted_2), value);
	}

	inline static int32_t get_offset_of_loopPointReached_3() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___loopPointReached_3)); }
	inline EventHandler_t3436254912 * get_loopPointReached_3() const { return ___loopPointReached_3; }
	inline EventHandler_t3436254912 ** get_address_of_loopPointReached_3() { return &___loopPointReached_3; }
	inline void set_loopPointReached_3(EventHandler_t3436254912 * value)
	{
		___loopPointReached_3 = value;
		Il2CppCodeGenWriteBarrier((&___loopPointReached_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___started_4)); }
	inline EventHandler_t3436254912 * get_started_4() const { return ___started_4; }
	inline EventHandler_t3436254912 ** get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(EventHandler_t3436254912 * value)
	{
		___started_4 = value;
		Il2CppCodeGenWriteBarrier((&___started_4), value);
	}

	inline static int32_t get_offset_of_frameDropped_5() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameDropped_5)); }
	inline EventHandler_t3436254912 * get_frameDropped_5() const { return ___frameDropped_5; }
	inline EventHandler_t3436254912 ** get_address_of_frameDropped_5() { return &___frameDropped_5; }
	inline void set_frameDropped_5(EventHandler_t3436254912 * value)
	{
		___frameDropped_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameDropped_5), value);
	}

	inline static int32_t get_offset_of_errorReceived_6() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___errorReceived_6)); }
	inline ErrorEventHandler_t3211687919 * get_errorReceived_6() const { return ___errorReceived_6; }
	inline ErrorEventHandler_t3211687919 ** get_address_of_errorReceived_6() { return &___errorReceived_6; }
	inline void set_errorReceived_6(ErrorEventHandler_t3211687919 * value)
	{
		___errorReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&___errorReceived_6), value);
	}

	inline static int32_t get_offset_of_seekCompleted_7() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___seekCompleted_7)); }
	inline EventHandler_t3436254912 * get_seekCompleted_7() const { return ___seekCompleted_7; }
	inline EventHandler_t3436254912 ** get_address_of_seekCompleted_7() { return &___seekCompleted_7; }
	inline void set_seekCompleted_7(EventHandler_t3436254912 * value)
	{
		___seekCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___seekCompleted_7), value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_8() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___clockResyncOccurred_8)); }
	inline TimeEventHandler_t445758600 * get_clockResyncOccurred_8() const { return ___clockResyncOccurred_8; }
	inline TimeEventHandler_t445758600 ** get_address_of_clockResyncOccurred_8() { return &___clockResyncOccurred_8; }
	inline void set_clockResyncOccurred_8(TimeEventHandler_t445758600 * value)
	{
		___clockResyncOccurred_8 = value;
		Il2CppCodeGenWriteBarrier((&___clockResyncOccurred_8), value);
	}

	inline static int32_t get_offset_of_frameReady_9() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameReady_9)); }
	inline FrameReadyEventHandler_t3848515759 * get_frameReady_9() const { return ___frameReady_9; }
	inline FrameReadyEventHandler_t3848515759 ** get_address_of_frameReady_9() { return &___frameReady_9; }
	inline void set_frameReady_9(FrameReadyEventHandler_t3848515759 * value)
	{
		___frameReady_9 = value;
		Il2CppCodeGenWriteBarrier((&___frameReady_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYER_T1683042537_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef CHARACTERCONTROLLER_T1138636865_H
#define CHARACTERCONTROLLER_T1138636865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterController
struct  CharacterController_t1138636865  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLER_T1138636865_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (RaycastHit_t1056001966)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[6] = 
{
	RaycastHit_t1056001966::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (CharacterController_t1138636865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (RigidbodyConstraints_t3348042902)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2002[11] = 
{
	RigidbodyConstraints_t3348042902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (ForceMode_t3656391766)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[5] = 
{
	ForceMode_t3656391766::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (JointLimits_t2681268900)+ sizeof (RuntimeObject), sizeof(JointLimits_t2681268900 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[7] = 
{
	JointLimits_t2681268900::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_Bounciness_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_BounceMinVelocity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_ContactDistance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_minBounce_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_maxBounce_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (ControllerColliderHit_t240592346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[7] = 
{
	ControllerColliderHit_t240592346::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t240592346::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t240592346::get_offset_of_m_Point_2(),
	ControllerColliderHit_t240592346::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t240592346::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (Collision_t4262080450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[5] = 
{
	Collision_t4262080450::get_offset_of_m_Impulse_0(),
	Collision_t4262080450::get_offset_of_m_RelativeVelocity_1(),
	Collision_t4262080450::get_offset_of_m_Rigidbody_2(),
	Collision_t4262080450::get_offset_of_m_Collider_3(),
	Collision_t4262080450::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (CollisionFlags_t1776808576)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2007[8] = 
{
	CollisionFlags_t1776808576::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (QueryTriggerInteraction_t962663221)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2008[4] = 
{
	QueryTriggerInteraction_t962663221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U3CModuleU3E_t692745541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (RenderMode_t4077056833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[4] = 
{
	RenderMode_t4077056833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (Canvas_t3310196443), -1, sizeof(Canvas_t3310196443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2011[1] = 
{
	Canvas_t3310196443_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (WillRenderCanvases_t3309123499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (UISystemProfilerApi_t2230074258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (SampleType_t1208595618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[3] = 
{
	SampleType_t1208595618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (CanvasGroup_t4083511760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (CanvasRenderer_t2598313366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (RectTransformUtility_t1743242446), -1, sizeof(RectTransformUtility_t1743242446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	RectTransformUtility_t1743242446_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CModuleU3E_t692745542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2020[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (AnalyticsSessionState_t681173134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2022[5] = 
{
	AnalyticsSessionState_t681173134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (AnalyticsSessionInfo_t2322308579), -1, sizeof(AnalyticsSessionInfo_t2322308579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2023[1] = 
{
	AnalyticsSessionInfo_t2322308579_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U3CModuleU3E_t692745543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (WWW_t3688466362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[1] = 
{
	WWW_t3688466362::get_offset_of__uwr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (VideoPlayer_t1683042537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[8] = 
{
	VideoPlayer_t1683042537::get_offset_of_prepareCompleted_2(),
	VideoPlayer_t1683042537::get_offset_of_loopPointReached_3(),
	VideoPlayer_t1683042537::get_offset_of_started_4(),
	VideoPlayer_t1683042537::get_offset_of_frameDropped_5(),
	VideoPlayer_t1683042537::get_offset_of_errorReceived_6(),
	VideoPlayer_t1683042537::get_offset_of_seekCompleted_7(),
	VideoPlayer_t1683042537::get_offset_of_clockResyncOccurred_8(),
	VideoPlayer_t1683042537::get_offset_of_frameReady_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (EventHandler_t3436254912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (ErrorEventHandler_t3211687919), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (FrameReadyEventHandler_t3848515759), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (TimeEventHandler_t445758600), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (VideoClipPlayable_t2598186649)+ sizeof (RuntimeObject), sizeof(VideoClipPlayable_t2598186649 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[1] = 
{
	VideoClipPlayable_t2598186649::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (OakleyGroups_t1704371988), -1, sizeof(OakleyGroups_t1704371988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[4] = 
{
	OakleyGroups_t1704371988_StaticFields::get_offset_of_Generator_0(),
	OakleyGroups_t1704371988_StaticFields::get_offset_of_OakleyPrime768_1(),
	OakleyGroups_t1704371988_StaticFields::get_offset_of_OakleyPrime1024_2(),
	OakleyGroups_t1704371988_StaticFields::get_offset_of_OakleyPrime1536_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (DiffieHellmanCryptoProvider_t915317458), -1, sizeof(DiffieHellmanCryptoProvider_t915317458_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2038[6] = 
{
	DiffieHellmanCryptoProvider_t915317458_StaticFields::get_offset_of_primeRoot_0(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_prime_1(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_secret_2(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_publicKey_3(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_crypto_4(),
	DiffieHellmanCryptoProvider_t915317458::get_offset_of_sharedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (BigInteger_t956758543), -1, sizeof(BigInteger_t956758543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2039[3] = 
{
	BigInteger_t956758543_StaticFields::get_offset_of_primesBelow2000_0(),
	BigInteger_t956758543::get_offset_of_data_1(),
	BigInteger_t956758543::get_offset_of_dataLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (IProtocol_t3448746462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (Version_t2916202802), -1, sizeof(Version_t2916202802_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	Version_t2916202802_StaticFields::get_offset_of_clientVersion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (Hashtable_t1048209202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (SupportClass_t2974952451), -1, sizeof(SupportClass_t2974952451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2043[2] = 
{
	SupportClass_t2974952451_StaticFields::get_offset_of_threadList_0(),
	SupportClass_t2974952451_StaticFields::get_offset_of_IntegerMilliseconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (IntegerMillisecondsDelegate_t651311252), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (ThreadSafeRandom_t1204416265), -1, sizeof(ThreadSafeRandom_t1204416265_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2045[1] = 
{
	ThreadSafeRandom_t1204416265_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (U3CU3Ec__DisplayClass7_0_t926758450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[2] = 
{
	U3CU3Ec__DisplayClass7_0_t926758450::get_offset_of_millisecondsInterval_0(),
	U3CU3Ec__DisplayClass7_0_t926758450::get_offset_of_myThread_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U3CU3Ec_t356392828), -1, sizeof(U3CU3Ec_t356392828_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	U3CU3Ec_t356392828_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (StatusCode_t823606708)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2048[20] = 
{
	StatusCode_t823606708::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (StreamBuffer_t3827669789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[4] = 
{
	0,
	StreamBuffer_t3827669789::get_offset_of_pos_2(),
	StreamBuffer_t3827669789::get_offset_of_len_3(),
	StreamBuffer_t3827669789::get_offset_of_buf_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (PeerStateValue_t1289417078)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[6] = 
{
	PeerStateValue_t1289417078::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ConnectionProtocol_t2586603950)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2052[5] = 
{
	ConnectionProtocol_t2586603950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (DebugLevel_t3671880145)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2053[6] = 
{
	DebugLevel_t3671880145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (PhotonPeer_t1608153861), -1, sizeof(PhotonPeer_t1608153861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2054[38] = 
{
	0,
	0,
	0,
	PhotonPeer_t1608153861::get_offset_of_ClientSdkId_3(),
	PhotonPeer_t1608153861_StaticFields::get_offset_of_AsyncKeyExchange_4(),
	PhotonPeer_t1608153861::get_offset_of_clientVersion_5(),
	PhotonPeer_t1608153861::get_offset_of_SocketImplementationConfig_6(),
	PhotonPeer_t1608153861::get_offset_of_U3CSocketImplementationU3Ek__BackingField_7(),
	PhotonPeer_t1608153861::get_offset_of_DebugOut_8(),
	PhotonPeer_t1608153861::get_offset_of_U3CListenerU3Ek__BackingField_9(),
	PhotonPeer_t1608153861::get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_10(),
	PhotonPeer_t1608153861::get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_11(),
	PhotonPeer_t1608153861::get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_12(),
	PhotonPeer_t1608153861::get_offset_of_trafficStatsStopwatch_13(),
	PhotonPeer_t1608153861::get_offset_of_trafficStatsEnabled_14(),
	PhotonPeer_t1608153861::get_offset_of_commandLogSize_15(),
	PhotonPeer_t1608153861::get_offset_of_U3CEnableServerTracingU3Ek__BackingField_16(),
	PhotonPeer_t1608153861::get_offset_of_quickResendAttempts_17(),
	PhotonPeer_t1608153861::get_offset_of_RhttpMinConnections_18(),
	PhotonPeer_t1608153861::get_offset_of_RhttpMaxConnections_19(),
	PhotonPeer_t1608153861::get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_20(),
	PhotonPeer_t1608153861::get_offset_of_ChannelCount_21(),
	PhotonPeer_t1608153861::get_offset_of_crcEnabled_22(),
	PhotonPeer_t1608153861::get_offset_of_WarningSize_23(),
	PhotonPeer_t1608153861::get_offset_of_SentCountAllowance_24(),
	PhotonPeer_t1608153861::get_offset_of_TimePingInterval_25(),
	PhotonPeer_t1608153861::get_offset_of_DisconnectTimeout_26(),
	PhotonPeer_t1608153861::get_offset_of_U3CTransportProtocolU3Ek__BackingField_27(),
	PhotonPeer_t1608153861_StaticFields::get_offset_of_OutgoingStreamBufferSize_28(),
	PhotonPeer_t1608153861::get_offset_of_mtu_29(),
	PhotonPeer_t1608153861::get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_30(),
	PhotonPeer_t1608153861::get_offset_of_peerBase_31(),
	PhotonPeer_t1608153861::get_offset_of_SendOutgoingLockObject_32(),
	PhotonPeer_t1608153861::get_offset_of_DispatchLockObject_33(),
	PhotonPeer_t1608153861::get_offset_of_EnqueueLock_34(),
	PhotonPeer_t1608153861::get_offset_of_PayloadEncryptionSecret_35(),
	PhotonPeer_t1608153861::get_offset_of_encryptor_36(),
	PhotonPeer_t1608153861::get_offset_of_decryptor_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (PhotonCodes_t543425440), -1, sizeof(PhotonCodes_t543425440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2055[5] = 
{
	PhotonCodes_t543425440_StaticFields::get_offset_of_ClientKey_0(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_ModeKey_1(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_ServerKey_2(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_InitEncryption_3(),
	PhotonCodes_t543425440_StaticFields::get_offset_of_Ping_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (PeerBase_t2956237011), -1, sizeof(PeerBase_t2956237011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2056[52] = 
{
	PeerBase_t2956237011::get_offset_of_ppeer_0(),
	PeerBase_t2956237011::get_offset_of_protocol_1(),
	PeerBase_t2956237011::get_offset_of_usedProtocol_2(),
	PeerBase_t2956237011::get_offset_of_rt_3(),
	PeerBase_t2956237011::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	PeerBase_t2956237011::get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5(),
	PeerBase_t2956237011::get_offset_of_ByteCountLastOperation_6(),
	PeerBase_t2956237011::get_offset_of_ByteCountCurrentDispatch_7(),
	PeerBase_t2956237011::get_offset_of_CommandInCurrentDispatch_8(),
	PeerBase_t2956237011::get_offset_of_TrafficPackageHeaderSize_9(),
	PeerBase_t2956237011::get_offset_of_packetLossByCrc_10(),
	PeerBase_t2956237011::get_offset_of_packetLossByChallenge_11(),
	PeerBase_t2956237011::get_offset_of_ActionQueue_12(),
	PeerBase_t2956237011::get_offset_of_peerID_13(),
	PeerBase_t2956237011::get_offset_of_peerConnectionState_14(),
	PeerBase_t2956237011::get_offset_of_serverTimeOffset_15(),
	PeerBase_t2956237011::get_offset_of_serverTimeOffsetIsAvailable_16(),
	PeerBase_t2956237011::get_offset_of_roundTripTime_17(),
	PeerBase_t2956237011::get_offset_of_roundTripTimeVariance_18(),
	PeerBase_t2956237011::get_offset_of_lastRoundTripTime_19(),
	PeerBase_t2956237011::get_offset_of_lowestRoundTripTime_20(),
	PeerBase_t2956237011::get_offset_of_lastRoundTripTimeVariance_21(),
	PeerBase_t2956237011::get_offset_of_highestRoundTripTimeVariance_22(),
	PeerBase_t2956237011::get_offset_of_timestampOfLastReceive_23(),
	PeerBase_t2956237011::get_offset_of_packetThrottleInterval_24(),
	PeerBase_t2956237011_StaticFields::get_offset_of_peerCount_25(),
	PeerBase_t2956237011::get_offset_of_bytesOut_26(),
	PeerBase_t2956237011::get_offset_of_bytesIn_27(),
	PeerBase_t2956237011::get_offset_of_commandBufferSize_28(),
	PeerBase_t2956237011::get_offset_of_CryptoProvider_29(),
	PeerBase_t2956237011::get_offset_of_lagRandomizer_30(),
	PeerBase_t2956237011::get_offset_of_NetSimListOutgoing_31(),
	PeerBase_t2956237011::get_offset_of_NetSimListIncoming_32(),
	PeerBase_t2956237011::get_offset_of_networkSimulationSettings_33(),
	PeerBase_t2956237011::get_offset_of_CommandLog_34(),
	PeerBase_t2956237011::get_offset_of_InReliableLog_35(),
	PeerBase_t2956237011::get_offset_of_CustomInitData_36(),
	PeerBase_t2956237011::get_offset_of_AppId_37(),
	PeerBase_t2956237011::get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_38(),
	PeerBase_t2956237011::get_offset_of_timeBase_39(),
	PeerBase_t2956237011::get_offset_of_timeInt_40(),
	PeerBase_t2956237011::get_offset_of_timeoutInt_41(),
	PeerBase_t2956237011::get_offset_of_timeLastAckReceive_42(),
	PeerBase_t2956237011::get_offset_of_timeLastSendAck_43(),
	PeerBase_t2956237011::get_offset_of_timeLastSendOutgoing_44(),
	0,
	0,
	0,
	PeerBase_t2956237011::get_offset_of_ApplicationIsInitialized_48(),
	PeerBase_t2956237011::get_offset_of_isEncryptionAvailable_49(),
	PeerBase_t2956237011::get_offset_of_outgoingCommandsInStream_50(),
	PeerBase_t2956237011::get_offset_of_SerializeMemStream_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (MyAction_t2462891903), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (ConnectionStateValue_t1954099360)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2058[7] = 
{
	ConnectionStateValue_t1954099360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (EgMessageType_t1130059189)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2059[10] = 
{
	EgMessageType_t1130059189::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (U3CU3Ec__DisplayClass145_0_t1573695289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[3] = 
{
	U3CU3Ec__DisplayClass145_0_t1573695289::get_offset_of_level_0(),
	U3CU3Ec__DisplayClass145_0_t1573695289::get_offset_of_debugReturn_1(),
	U3CU3Ec__DisplayClass145_0_t1573695289::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (U3CU3Ec__DisplayClass146_0_t1573695292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[2] = 
{
	U3CU3Ec__DisplayClass146_0_t1573695292::get_offset_of_statusValue_0(),
	U3CU3Ec__DisplayClass146_0_t1573695292::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (CmdLogItem_t4217690540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[5] = 
{
	CmdLogItem_t4217690540::get_offset_of_TimeInt_0(),
	CmdLogItem_t4217690540::get_offset_of_Channel_1(),
	CmdLogItem_t4217690540::get_offset_of_SequenceNumber_2(),
	CmdLogItem_t4217690540::get_offset_of_Rtt_3(),
	CmdLogItem_t4217690540::get_offset_of_Variance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (CmdLogReceivedReliable_t4090183889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[2] = 
{
	CmdLogReceivedReliable_t4090183889::get_offset_of_TimeSinceLastSend_5(),
	CmdLogReceivedReliable_t4090183889::get_offset_of_TimeSinceLastSendAck_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (CmdLogReceivedAck_t580412049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[1] = 
{
	CmdLogReceivedAck_t580412049::get_offset_of_ReceivedSentTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (CmdLogSentReliable_t3437548410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[4] = 
{
	CmdLogSentReliable_t3437548410::get_offset_of_Resend_5(),
	CmdLogSentReliable_t3437548410::get_offset_of_RoundtripTimeout_6(),
	CmdLogSentReliable_t3437548410::get_offset_of_Timeout_7(),
	CmdLogSentReliable_t3437548410::get_offset_of_TriggeredTimeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (EnetPeer_t430442630), -1, sizeof(EnetPeer_t430442630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2066[21] = 
{
	EnetPeer_t430442630_StaticFields::get_offset_of_HMAC_SIZE_52(),
	EnetPeer_t430442630_StaticFields::get_offset_of_BLOCK_SIZE_53(),
	EnetPeer_t430442630_StaticFields::get_offset_of_IV_SIZE_54(),
	EnetPeer_t430442630::get_offset_of_sentReliableCommands_55(),
	EnetPeer_t430442630::get_offset_of_outgoingAcknowledgementsPool_56(),
	EnetPeer_t430442630::get_offset_of_windowSize_57(),
	EnetPeer_t430442630::get_offset_of_udpCommandCount_58(),
	EnetPeer_t430442630::get_offset_of_udpBuffer_59(),
	EnetPeer_t430442630::get_offset_of_udpBufferIndex_60(),
	EnetPeer_t430442630::get_offset_of_udpBufferLength_61(),
	EnetPeer_t430442630::get_offset_of_bufferForEncryption_62(),
	EnetPeer_t430442630::get_offset_of_challenge_63(),
	EnetPeer_t430442630::get_offset_of_reliableCommandsRepeated_64(),
	EnetPeer_t430442630::get_offset_of_reliableCommandsSent_65(),
	EnetPeer_t430442630::get_offset_of_serverSentTime_66(),
	EnetPeer_t430442630_StaticFields::get_offset_of_udpHeader0xF3_67(),
	EnetPeer_t430442630_StaticFields::get_offset_of_messageHeader_68(),
	EnetPeer_t430442630::get_offset_of_datagramEncryptedConnection_69(),
	EnetPeer_t430442630::get_offset_of_channelArray_70(),
	EnetPeer_t430442630::get_offset_of_commandsToRemove_71(),
	EnetPeer_t430442630::get_offset_of_commandsToResend_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (U3CU3Ec__DisplayClass56_0_t3313269813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[2] = 
{
	U3CU3Ec__DisplayClass56_0_t3313269813::get_offset_of_dataCopy_0(),
	U3CU3Ec__DisplayClass56_0_t3313269813::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (U3CU3Ec__DisplayClass56_1_t3313335349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[2] = 
{
	U3CU3Ec__DisplayClass56_1_t3313335349::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass56_1_t3313335349::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (U3CU3Ec__DisplayClass62_0_t982511824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[2] = 
{
	U3CU3Ec__DisplayClass62_0_t982511824::get_offset_of_readCommand_0(),
	U3CU3Ec__DisplayClass62_0_t982511824::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (EnetChannel_t2207795168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[9] = 
{
	EnetChannel_t2207795168::get_offset_of_ChannelNumber_0(),
	EnetChannel_t2207795168::get_offset_of_incomingReliableCommandsList_1(),
	EnetChannel_t2207795168::get_offset_of_incomingUnreliableCommandsList_2(),
	EnetChannel_t2207795168::get_offset_of_outgoingReliableCommandsList_3(),
	EnetChannel_t2207795168::get_offset_of_outgoingUnreliableCommandsList_4(),
	EnetChannel_t2207795168::get_offset_of_incomingReliableSequenceNumber_5(),
	EnetChannel_t2207795168::get_offset_of_incomingUnreliableSequenceNumber_6(),
	EnetChannel_t2207795168::get_offset_of_outgoingReliableSequenceNumber_7(),
	EnetChannel_t2207795168::get_offset_of_outgoingUnreliableSequenceNumber_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (NCommand_t1230688399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[49] = 
{
	NCommand_t1230688399::get_offset_of_commandFlags_0(),
	0,
	0,
	0,
	0,
	0,
	NCommand_t1230688399::get_offset_of_commandType_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t1230688399::get_offset_of_commandChannelID_17(),
	NCommand_t1230688399::get_offset_of_reliableSequenceNumber_18(),
	NCommand_t1230688399::get_offset_of_unreliableSequenceNumber_19(),
	NCommand_t1230688399::get_offset_of_unsequencedGroupNumber_20(),
	NCommand_t1230688399::get_offset_of_reservedByte_21(),
	NCommand_t1230688399::get_offset_of_startSequenceNumber_22(),
	NCommand_t1230688399::get_offset_of_fragmentCount_23(),
	NCommand_t1230688399::get_offset_of_fragmentNumber_24(),
	NCommand_t1230688399::get_offset_of_totalLength_25(),
	NCommand_t1230688399::get_offset_of_fragmentOffset_26(),
	NCommand_t1230688399::get_offset_of_fragmentsRemaining_27(),
	NCommand_t1230688399::get_offset_of_commandSentTime_28(),
	NCommand_t1230688399::get_offset_of_commandSentCount_29(),
	NCommand_t1230688399::get_offset_of_roundTripTimeout_30(),
	NCommand_t1230688399::get_offset_of_timeoutTime_31(),
	NCommand_t1230688399::get_offset_of_ackReceivedReliableSequenceNumber_32(),
	NCommand_t1230688399::get_offset_of_ackReceivedSentTime_33(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t1230688399::get_offset_of_Size_45(),
	NCommand_t1230688399::get_offset_of_commandHeader_46(),
	NCommand_t1230688399::get_offset_of_SizeOfHeader_47(),
	NCommand_t1230688399::get_offset_of_Payload_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (TPeer_t1497954812), -1, sizeof(TPeer_t1497954812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2072[8] = 
{
	TPeer_t1497954812::get_offset_of_incomingList_52(),
	TPeer_t1497954812::get_offset_of_outgoingStream_53(),
	TPeer_t1497954812::get_offset_of_lastPingResult_54(),
	TPeer_t1497954812::get_offset_of_pingRequest_55(),
	TPeer_t1497954812_StaticFields::get_offset_of_tcpFramedMessageHead_56(),
	TPeer_t1497954812_StaticFields::get_offset_of_tcpMsgHead_57(),
	TPeer_t1497954812::get_offset_of_messageHeader_58(),
	TPeer_t1497954812::get_offset_of_DoFraming_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (U3CU3Ec__DisplayClass30_0_t3066328396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[2] = 
{
	U3CU3Ec__DisplayClass30_0_t3066328396::get_offset_of_data_0(),
	U3CU3Ec__DisplayClass30_0_t3066328396::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (OperationRequest_t597637232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[2] = 
{
	OperationRequest_t597637232::get_offset_of_OperationCode_0(),
	OperationRequest_t597637232::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (OperationResponse_t423627973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[4] = 
{
	OperationResponse_t423627973::get_offset_of_OperationCode_0(),
	OperationResponse_t423627973::get_offset_of_ReturnCode_1(),
	OperationResponse_t423627973::get_offset_of_DebugMessage_2(),
	OperationResponse_t423627973::get_offset_of_Parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (EventData_t3728223374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[2] = 
{
	EventData_t3728223374::get_offset_of_Code_0(),
	EventData_t3728223374::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (SerializeMethod_t1264674278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (SerializeStreamMethod_t2169445464), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (DeserializeMethod_t3915517082), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (DeserializeStreamMethod_t3070531629), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (CustomType_t4026063319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[6] = 
{
	CustomType_t4026063319::get_offset_of_Code_0(),
	CustomType_t4026063319::get_offset_of_Type_1(),
	CustomType_t4026063319::get_offset_of_SerializeFunction_2(),
	CustomType_t4026063319::get_offset_of_DeserializeFunction_3(),
	CustomType_t4026063319::get_offset_of_SerializeStreamFunction_4(),
	CustomType_t4026063319::get_offset_of_DeserializeStreamFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (Protocol_t1622296502), -1, sizeof(Protocol_t1622296502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2082[6] = 
{
	Protocol_t1622296502_StaticFields::get_offset_of_GpBinaryV16_0(),
	Protocol_t1622296502_StaticFields::get_offset_of_ProtocolDefault_1(),
	Protocol_t1622296502_StaticFields::get_offset_of_TypeDict_2(),
	Protocol_t1622296502_StaticFields::get_offset_of_CodeDict_3(),
	Protocol_t1622296502_StaticFields::get_offset_of_memFloatBlock_4(),
	Protocol_t1622296502_StaticFields::get_offset_of_memDeserialize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (Protocol16_t1856210005), -1, sizeof(Protocol16_t1856210005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2083[13] = 
{
	Protocol16_t1856210005::get_offset_of_versionBytes_0(),
	Protocol16_t1856210005::get_offset_of_memShort_1(),
	Protocol16_t1856210005::get_offset_of_memLongBlock_2(),
	Protocol16_t1856210005::get_offset_of_memLongBlockBytes_3(),
	Protocol16_t1856210005_StaticFields::get_offset_of_memFloatBlock_4(),
	Protocol16_t1856210005_StaticFields::get_offset_of_memFloatBlockBytes_5(),
	Protocol16_t1856210005::get_offset_of_memDoubleBlock_6(),
	Protocol16_t1856210005::get_offset_of_memDoubleBlockBytes_7(),
	Protocol16_t1856210005::get_offset_of_memInteger_8(),
	Protocol16_t1856210005::get_offset_of_memLong_9(),
	Protocol16_t1856210005::get_offset_of_memFloat_10(),
	Protocol16_t1856210005::get_offset_of_memDouble_11(),
	Protocol16_t1856210005::get_offset_of_memString_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (GpType_t2340543107)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2084[22] = 
{
	GpType_t2340543107::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (PhotonPing_t2371975946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[6] = 
{
	PhotonPing_t2371975946::get_offset_of_DebugString_0(),
	PhotonPing_t2371975946::get_offset_of_Successful_1(),
	PhotonPing_t2371975946::get_offset_of_GotResult_2(),
	PhotonPing_t2371975946::get_offset_of_PingLength_3(),
	PhotonPing_t2371975946::get_offset_of_PingBytes_4(),
	PhotonPing_t2371975946::get_offset_of_PingId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (PingMono_t2784932916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[1] = 
{
	PingMono_t2784932916::get_offset_of_sock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (PingNativeStatic_t269944269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (PingNativeDynamic_t2826633900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (PhotonSocketState_t2742032721)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2089[5] = 
{
	PhotonSocketState_t2742032721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (PhotonSocketError_t821309465)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2090[5] = 
{
	PhotonSocketError_t821309465::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (IPhotonSocket_t2066969247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[9] = 
{
	IPhotonSocket_t2066969247::get_offset_of_peerBase_0(),
	IPhotonSocket_t2066969247::get_offset_of_U3CProtocolU3Ek__BackingField_1(),
	IPhotonSocket_t2066969247::get_offset_of_PollReceive_2(),
	IPhotonSocket_t2066969247::get_offset_of_U3CStateU3Ek__BackingField_3(),
	IPhotonSocket_t2066969247::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	IPhotonSocket_t2066969247::get_offset_of_U3CServerPortU3Ek__BackingField_5(),
	IPhotonSocket_t2066969247::get_offset_of_U3CAddressResolvedAsIpv6U3Ek__BackingField_6(),
	IPhotonSocket_t2066969247::get_offset_of_U3CUrlProtocolU3Ek__BackingField_7(),
	IPhotonSocket_t2066969247::get_offset_of_U3CUrlPathU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (U3CU3Ec__DisplayClass41_0_t3594181758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[2] = 
{
	U3CU3Ec__DisplayClass41_0_t3594181758::get_offset_of_inBufferCopy_0(),
	U3CU3Ec__DisplayClass41_0_t3594181758::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (U3CU3Ec__DisplayClass41_1_t2028097817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[3] = 
{
	U3CU3Ec__DisplayClass41_1_t2028097817::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass41_1_t2028097817::get_offset_of_inBuffer_1(),
	U3CU3Ec__DisplayClass41_1_t2028097817::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (SocketUdp_t1337106072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[2] = 
{
	SocketUdp_t1337106072::get_offset_of_sock_9(),
	SocketUdp_t1337106072::get_offset_of_syncer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (SocketTcp_t182200829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[2] = 
{
	SocketTcp_t182200829::get_offset_of_sock_9(),
	SocketTcp_t182200829::get_offset_of_syncer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (SimulationItem_t3044638479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[4] = 
{
	SimulationItem_t3044638479::get_offset_of_stopw_0(),
	SimulationItem_t3044638479::get_offset_of_TimeToExecute_1(),
	SimulationItem_t3044638479::get_offset_of_ActionToExecute_2(),
	SimulationItem_t3044638479::get_offset_of_U3CDelayU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (NetworkSimulationSet_t2000596048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[12] = 
{
	NetworkSimulationSet_t2000596048::get_offset_of_isSimulationEnabled_0(),
	NetworkSimulationSet_t2000596048::get_offset_of_outgoingLag_1(),
	NetworkSimulationSet_t2000596048::get_offset_of_outgoingJitter_2(),
	NetworkSimulationSet_t2000596048::get_offset_of_outgoingLossPercentage_3(),
	NetworkSimulationSet_t2000596048::get_offset_of_incomingLag_4(),
	NetworkSimulationSet_t2000596048::get_offset_of_incomingJitter_5(),
	NetworkSimulationSet_t2000596048::get_offset_of_incomingLossPercentage_6(),
	NetworkSimulationSet_t2000596048::get_offset_of_peerBase_7(),
	NetworkSimulationSet_t2000596048::get_offset_of_netSimThread_8(),
	NetworkSimulationSet_t2000596048::get_offset_of_NetSimManualResetEvent_9(),
	NetworkSimulationSet_t2000596048::get_offset_of_U3CLostPackagesOutU3Ek__BackingField_10(),
	NetworkSimulationSet_t2000596048::get_offset_of_U3CLostPackagesInU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (TrafficStatsGameLevel_t4013908777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[16] = 
{
	TrafficStatsGameLevel_t4013908777::get_offset_of_timeOfLastDispatchCall_0(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_timeOfLastSendCall_1(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3COperationByteCountU3Ek__BackingField_2(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3COperationCountU3Ek__BackingField_3(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CResultByteCountU3Ek__BackingField_4(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CResultCountU3Ek__BackingField_5(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CEventByteCountU3Ek__BackingField_6(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CEventCountU3Ek__BackingField_7(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CLongestOpResponseCallbackU3Ek__BackingField_8(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CLongestEventCallbackU3Ek__BackingField_10(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CLongestEventCallbackCodeU3Ek__BackingField_11(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14(),
	TrafficStatsGameLevel_t4013908777::get_offset_of_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (TrafficStats_t1302902347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[13] = 
{
	TrafficStats_t1302902347::get_offset_of_U3CPackageHeaderSizeU3Ek__BackingField_0(),
	TrafficStats_t1302902347::get_offset_of_U3CReliableCommandCountU3Ek__BackingField_1(),
	TrafficStats_t1302902347::get_offset_of_U3CUnreliableCommandCountU3Ek__BackingField_2(),
	TrafficStats_t1302902347::get_offset_of_U3CFragmentCommandCountU3Ek__BackingField_3(),
	TrafficStats_t1302902347::get_offset_of_U3CControlCommandCountU3Ek__BackingField_4(),
	TrafficStats_t1302902347::get_offset_of_U3CTotalPacketCountU3Ek__BackingField_5(),
	TrafficStats_t1302902347::get_offset_of_U3CTotalCommandsInPacketsU3Ek__BackingField_6(),
	TrafficStats_t1302902347::get_offset_of_U3CReliableCommandBytesU3Ek__BackingField_7(),
	TrafficStats_t1302902347::get_offset_of_U3CUnreliableCommandBytesU3Ek__BackingField_8(),
	TrafficStats_t1302902347::get_offset_of_U3CFragmentCommandBytesU3Ek__BackingField_9(),
	TrafficStats_t1302902347::get_offset_of_U3CControlCommandBytesU3Ek__BackingField_10(),
	TrafficStats_t1302902347::get_offset_of_U3CTimestampOfLastAckU3Ek__BackingField_11(),
	TrafficStats_t1302902347::get_offset_of_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
